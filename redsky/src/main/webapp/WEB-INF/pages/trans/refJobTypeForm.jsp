<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<head>
<title><fmt:message key="refJobTypeDetail.title" /></title>
<meta name="heading" content="<fmt:message key='refJobTypeDetail.heading'/>" />
<script type="text/javascript">

function getPortCountryCode(){
	var country=document.forms['refJobTypeForm'].elements['refJobType.country'].value;
	var url="portCountryCode.html?ajax=1&decorator=simple&popup=true&country=" + encodeURI(country);
    http4.open("GET", url, true);
    http4.onreadystatechange = httpPortCountryCode;
    http4.send(null);
}

function httpPortCountryCode()
        {
             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                if(results.length>=1)
                	{
 					document.forms['refJobTypeForm'].elements['refJobType.countryCode'].value = results;
 					document.forms['refJobTypeForm'].elements['refJobType.country'].select();
					}
					else{
                    document.forms['refJobTypeForm'].elements['refJobType.countryCode'].value = '';
 					document.forms['refJobTypeForm'].elements['refJobType.country'].select();
                 }
             }
        }

function getHTTPObject4()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject4();



function checkValidate(){
	if(document.forms['refJobTypeForm'].elements['refJobType.job'].value == ''){
		alert('Please select job.');
		return false;
		}
	else if(document.forms['refJobTypeForm'].elements['refJobType.companyDivision'].value == ''){
		alert('Please select companydivision.');
		return false;
	}
}   



var httpRefSale = getHTTPObjectRefSale();
function getHTTPObjectRefSale(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}



var http50 = getHTTPObject50();
function getHTTPObject50(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var http52 = getHTTPObject52();
function getHTTPObject52(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}


var http53 = getHTTPObject53();
function getHTTPObject53(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}


var http54 = getHTTPObject54();
function getHTTPObject54(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var http55 = getHTTPObject55();
function getHTTPObject55(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 
function geCoordList(){
    var job = document.forms['refJobTypeForm'].elements['refJobType.job'].value;
   	if(job!=''){
     	var url = "findCoord.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http50.open("GET", url, true);
     	http50.onreadystatechange = handleHttpResponse50;
     	http50.send(null);
    }  
}
function handleHttpResponse50(){	
		if (http50.readyState == 4){
                var results = http50.responseText
                results = results.trim();
                res = results.split("~");
              	targetElement = document.forms['refJobTypeForm'].elements['refJobType.coordinator'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['refJobTypeForm'].elements['refJobType.coordinator'].options[i].text = '';
						document.forms['refJobTypeForm'].elements['refJobType.coordinator'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
						document.forms['refJobTypeForm'].elements['refJobType.coordinator'].options[i].text = stateVal[1];
						document.forms['refJobTypeForm'].elements['refJobType.coordinator'].options[i].value = stateVal[0];
					}
				}
				getPricing();
        }
}

function getPricing(){
    var job = document.forms['refJobTypeForm'].elements['refJobType.job'].value;
   	if(job!=''){
     	var url = "findPricing.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http52.open("GET", url, true);
     	http52.onreadystatechange = handleHttpResponse52;
     	http52.send(null);
    }  
}
function handleHttpResponse52(){
		if (http52.readyState == 4){
                var results = http52.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['refJobTypeForm'].elements['refJobType.personPricing'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['refJobTypeForm'].elements['refJobType.personPricing'].options[i].text = '';
						document.forms['refJobTypeForm'].elements['refJobType.personPricing'].options[i].value = '';
					}else{
						stateVal = res[i].split("#");
						document.forms['refJobTypeForm'].elements['refJobType.personPricing'].options[i].text = stateVal[1];
						document.forms['refJobTypeForm'].elements['refJobType.personPricing'].options[i].value = stateVal[0];
					}
				}
        getBilling();
        }
}
 
function getBilling(){
    var job = document.forms['refJobTypeForm'].elements['refJobType.job'].value;
   	if(job!=''){
     	var url = "findBilling.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http53.open("GET", url, true);
     	http53.onreadystatechange = handleHttpResponse53;
     	http53.send(null);
    }
    
}
function handleHttpResponse53(){
		
		if (http53.readyState == 4){
                var results = http53.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['refJobTypeForm'].elements['refJobType.personBilling'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['refJobTypeForm'].elements['refJobType.personBilling'].options[i].text = '';
						document.forms['refJobTypeForm'].elements['refJobType.personBilling'].options[i].value = '';
					}else{
						stateVal = res[i].split("#");
						document.forms['refJobTypeForm'].elements['refJobType.personBilling'].options[i].text = stateVal[1];
						document.forms['refJobTypeForm'].elements['refJobType.personBilling'].options[i].value = stateVal[0];
					}
				}
        getPayable();
        }
}
  
function getPayable(){
    var job = document.forms['refJobTypeForm'].elements['refJobType.job'].value;
   	if(job!=''){
     	var url = "findPayable.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http54.open("GET", url, true);
     	http54.onreadystatechange = handleHttpResponse54;
     	http54.send(null);
    }
    
}
function handleHttpResponse54(){
		
		if (http54.readyState == 4){
                var results = http54.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['refJobTypeForm'].elements['refJobType.personPayable'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['refJobTypeForm'].elements['refJobType.personPayable'].options[i].text = '';
						document.forms['refJobTypeForm'].elements['refJobType.personPayable'].options[i].value = '';
					}else{
						stateVal = res[i].split("#");
						document.forms['refJobTypeForm'].elements['refJobType.personPayable'].options[i].text = stateVal[1];
						document.forms['refJobTypeForm'].elements['refJobType.personPayable'].options[i].value = stateVal[0];
					}
				}
			getAuditor();
          }
}

function getAuditor(){
    var job = document.forms['refJobTypeForm'].elements['refJobType.job'].value;
   	if(job!=''){
     	var url = "findAuditor.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http55.open("GET", url, true);
     	http55.onreadystatechange = handleHttpResponse55;
     	http55.send(null);
    }
    
}
function handleHttpResponse55(){
		
		if (http55.readyState == 4){
                var results = http55.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['refJobTypeForm'].elements['refJobType.personAuditor'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['refJobTypeForm'].elements['refJobType.personAuditor'].options[i].text = '';
						document.forms['refJobTypeForm'].elements['refJobType.personAuditor'].options[i].value = '';
					}else{
						stateVal = res[i].split("#");
						document.forms['refJobTypeForm'].elements['refJobType.personAuditor'].options[i].text = stateVal[1];
						document.forms['refJobTypeForm'].elements['refJobType.personAuditor'].options[i].value = stateVal[0];
					}
				}
			getRefSale();
          }
}

function getRefSale(){
	if(document.forms['refJobTypeForm'].elements['refJobType.estimator'].value == ''){
		var job = document.forms['refJobTypeForm'].elements['refJobType.job'].value;
	   	if(job!=''){
	     	var url = "findSaleRef.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
	     	httpRefSale.open("GET", url, true);
	     	httpRefSale.onreadystatechange = handleHttpResponseRefSale;
	     	httpRefSale.send(null);
	    } 
	}
}
function handleHttpResponseRefSale(){	
	if (httpRefSale.readyState == 4){
		   var saleVal = "";
           var results = httpRefSale.responseText
           results = results.trim();
           res = results.split("@");
           for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['refJobTypeForm'].elements['refJobType.estimator'].options[i].text = '';
				document.forms['refJobTypeForm'].elements['refJobType.estimator'].options[i].value = '';
			}else{
				saleVal = res[i].split("#");
				document.forms['refJobTypeForm'].elements['refJobType.estimator'].options[i].text = saleVal[1];
				document.forms['refJobTypeForm'].elements['refJobType.estimator'].options[i].value = saleVal[0];
			}
		}
    }
}
</script>
</head>
<s:form id="refJobTypeForm" action="saveRefJobType" method="post" validate="true">
 <s:hidden name="refJobType.id"/>
 <s:hidden name="validateStringjob"/>
 <s:hidden name="validatecompanyDivision"/>

	<s:hidden name="refJobType.countryCode"/>
	

	<div id="Layer1" style="width:100%;">

	<div id="layer4" style="width:100%;">
		<div id="newmnav">
			<ul>
				<li id="newmnav1" style="background:#FFF "><a class="current"><span>Ref Job Type Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				<li><a href="refJobTypeList.html"><span>Ref Job Type List</span></a></li>
				<c:if test="${not empty refJobType.id}">
				<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${refJobType.id}&tableName=refjobtype&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
				</c:if>
 			</ul>
		</div>
		<div class="spn">&nbsp;</div>
	</div>
	<div id="content" align="center">
	<div id="liquid-round-top">
	<div class="top" style="margin-top:2px; "><span></span></div>
	<div class="center-content">
	<table cellspacing="1" cellpadding="1" border="0" >
		<tbody>
			<tr>
				<td align="left" class="listwhitetext">
				<table class="detailTabLabel" cellspacing="2" cellpadding="2" border="0">
					<tbody>
						<tr>
							<td align="left" height="5px"></td>
						</tr>
						<tr>
							<td align="right">Job<font color="red" size="2">*</font></td>
							<c:if test="${empty refJobType.id}">
								<td><s:select name="refJobType.job" id="jobs" cssClass="list-menu" list="%{jobs}" cssStyle="width:140px" headerKey="" headerValue="" onchange="geCoordList();" /></td>
							</c:if>
							<c:if test="${not empty refJobType.id}">
								<td><s:textfield readonly="true" cssClass="input-textUpper" name="refJobType.job"  cssStyle="width:140px"/></td>
							</c:if>
							
							
							<td  width="95px" align="right">Company&nbsp;Division<font color="red" size="2">*</font> </td>						
							<td align="right"><s:select name="refJobType.companyDivision" id="companyDivision" cssClass="list-menu" list="%{division}" cssStyle="width:140px" headerKey="" headerValue=""/> </td>  						
							
							<td  width="95px" align="right">Country</td>						
							<td align="right"><s:select name="refJobType.country" id="country" cssClass="list-menu" list="%{ocountry}" cssStyle="width:140px" headerKey="" headerValue="" onchange="getPortCountryCode();"/> </td>  						
							<configByCorp:fieldVisibility componentId="component.standard.surveyTool">
							<td  width="75px" align="right">Survey Tool</td>						
							<td align="right"><s:select name="refJobType.surveyTool" id="surveyTool" cssClass="list-menu" list="%{surveyToolMap}" cssStyle="width:140px" headerKey="" headerValue="" /> </td>  						
						</configByCorp:fieldVisibility>
						</tr>
						
						<tr>
							<td  width="65px" align="right">Person&nbsp;Billing</td>
							<td><s:select name="refJobType.personBilling" id="personBilling" cssClass="list-menu" list="%{billing}" cssStyle="width:140px" headerKey="" headerValue=""/> </td>
							<td align="right">Person Pricing</td>
							<td><s:select name="refJobType.personPricing" id="personPricing" cssClass="list-menu" list="%{pricing}" cssStyle="width:140px" headerKey="" headerValue=""/> </td>
							<td width="95px" align="right">Person Payable</td>
							<td><s:select name="refJobType.personPayable" id="personPayable" cssClass="list-menu" list="%{payable}" cssStyle="width:140px" headerKey="" headerValue=""/> </td>
							<td width="95px" align="right">Person Claims</td>
							<td><s:select name="refJobType.personClaims" id="personClaims" cssClass="list-menu" list="%{personClaimsList}" cssStyle="width:140px" headerKey="" headerValue=""/> </td>
                            </tr>
                            <tr>
                            <td align="right">Coordinator</td>
							<td><s:select name="refJobType.coordinator" id="coordinator" cssClass="list-menu" list="%{coordinator}" cssStyle="width:140px" headerKey="" headerValue=""/> </td>
							<td align="right">Estimator</td>
							<td><s:select name="refJobType.estimator" id="estimator" cssClass="list-menu" list="%{estimator}" cssStyle="width:140px" headerKey="" headerValue=""/> </td>
							<td align="right">Person Auditor</td>
							<td><s:select name="refJobType.personAuditor" id="personAuditor" cssClass="list-menu" list="%{auditor}" cssStyle="width:140px" headerKey="" headerValue=""/> </td> 
							<configByCorp:fieldVisibility componentId="component.field.billing.internalBillingPerson">
								<td align="right">Internal&nbsp;Billing</td>
								<td><s:select name="refJobType.internalBillingPerson" cssClass="list-menu" list="%{internalBillingPersonList}" cssStyle="width:140px" headerKey="" headerValue=""/> </td>
							</configByCorp:fieldVisibility>
							</tr>
                            <tr>
                            <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.opsPerson">
								<td align="right">Ops&nbsp;Person</td>
								<td><s:select name="refJobType.opsPerson" cssClass="list-menu" list="%{opsPerson}" cssStyle="width:140px" headerKey="" headerValue=""/> </td>
							</configByCorp:fieldVisibility>
							<configByCorp:fieldVisibility componentId="component.field.billing.forwarderPerson">
								<td align="right">Forwarder</td>
								<td><s:select name="refJobType.personForwarder" cssClass="list-menu" list="%{forwarderList}" cssStyle="width:140px" headerKey="" headerValue=""/> </td>
							</configByCorp:fieldVisibility>
                            
							<c:if test="${accountLineAccountPortalFlag}">
							<td width="95px" align="right">Account&nbsp;Line&nbsp;Account&nbsp;Portal</td>
		   					<td align="left" ><s:checkbox name="refJobType.accAccountPortal" required="true" /></td>
							</c:if>
							</tr>
						<tr>
							<td align="left" height="5px"></td>
						</tr>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
	
	</div>
	<div class="bottom-header"><span></span></div>
	</div>
	</div>
	
	<table border="0">
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='refJobType.createdOn' /></b></td>
				<td valign="top"></td>
				<td style="width:120px"><fmt:formatDate var="refJobTypeCreatedOnFormattedValue" value="${refJobType.createdOn}" pattern="${displayDateTimeEditFormat}" /> 
					<s:hidden name="refJobType.createdOn" value="${refJobTypeCreatedOnFormattedValue}" /><fmt:formatDate value="${refJobType.createdOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='refJobType.createdBy' /></b></td>
				<c:if test="${not empty refJobType.id}">
					<s:hidden name="refJobType.createdBy" />
					<td style="width:85px"><s:label name="createdBy" value="%{refJobType.createdBy}" /></td>
				</c:if>
				<c:if test="${empty refJobType.id}">
					<s:hidden name="refJobType.createdBy" value="${pageContext.request.remoteUser}" />
					<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
				</c:if>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='refJobType.updatedOn' /></b></td>
				<fmt:formatDate var="refJobTypeUpdatedOnFormattedValue" value="${refJobType.updatedOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="refJobType.updatedOn" value="${refJobTypeUpdatedOnFormattedValue}" />
				<td style="width:120px"><fmt:formatDate value="${refJobType.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='refJobType.updatedBy' /></b></td>
				<c:if test="${not empty refJobType.id}">
					<s:hidden name="refJobType.updatedBy" />
					<td style="width:85px"><s:label name="updatedBy" value="%{refJobType.updatedBy}" /></td>
				</c:if>
				<c:if test="${empty refJobType.id}">
					<s:hidden name="refJobType.updatedBy" value="${pageContext.request.remoteUser}" />
					<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}" /></td>
				</c:if>
			</tr>
		</tbody>
	</table>
	
	
	<table class="detailTabLabel" border="0">
		<tbody>
			<tr>
				<td align="left" height="3px"></td>
			</tr>
			<tr>
				<td align="left"><s:submit cssClass="cssbutton1" type="button" method="save" key="button.save" onclick="return checkValidate();" /></td>
				<td align="right"><s:reset cssClass="cssbutton1" type="button" key="Reset" /></td>
				<td align="right"><c:if test="${not empty refJobType.id}">
					<input type="button" class="cssbutton1" value="Add" onclick="location.href='<c:url value="/editRefJobType.html"/>'" />
				</c:if></td>
			</tr>
		</tbody>
	</table>
	</div>
</s:form>
<script type="text/javascript"> 
	Form.focusFirstElement($("refJobTypeForm"));
	window.onload = function() {
	      document.forms['refJobTypeForm'].elements['refJobTypeForm.accAccountPortal'].value='${systemDefault.accountLineAccountPortalFlag}';
	     }   
</script>
