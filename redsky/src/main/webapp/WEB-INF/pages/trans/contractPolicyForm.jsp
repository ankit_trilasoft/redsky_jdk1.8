<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="contractPolicyDetail.title"/></title>
<style>

</style>   
    <meta name="heading" content="<fmt:message key='contractPolicyDetail.heading'/>"/>  

</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>"/>

<s:form id="contractPolicyForm" action="saveContractPolicy" method="post" validate="true"> 
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<c:set var="isExcludeFromParentPolicyUpdate" value="false"/>
<c:if test="${partnerPrivate.excludeFromParentPolicyUpdate}">
<c:set var="isExcludeFromParentPolicyUpdate" value="true"/>
</c:if>													
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
  <s:hidden name="btnType" value="<%= request.getParameter("btnType") %>" />
  <c:set var="btnType" value="<%= request.getParameter("btnType") %>" />
<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
<s:hidden name="contractPolicy.id" />
<s:hidden name="partner.id" />
<s:hidden name="contractPolicy.partnerCode" value="%{partner.partnerCode}"/>
<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.accountlist' }">
	<c:if test='${paramValue == "View"}'>
		<c:redirect url="/searchPartnerView.html"/>
	</c:if>
	<c:if test='${paramValue != "View"}'>
		<c:redirect url="/partnerPublics.html?partnerType=AC"/>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountdetail' }">
	<c:redirect url="/editPartnerPublic.html?id=${partner.id}&partnerType=AC"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountprofile' }">
	<c:redirect url="/editNewAccountProfile.html?id=${partner.id}&partnerType=AC"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountcontact' }">
	<c:redirect url="/accountContactList.html?id=${partner.id}&partnerType=AC"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.partnerReloSvcs' }">
				<c:redirect url="/partnerReloSvcs.html?id=${partner.id}&partnerType=AC" />
			</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<div id="layer4" style="width:100%;">
<div id="newmnav">
				<ul>
				  <c:if test="${param.popup}" >
				    <%-- <li><a href='${empty param.popup?"partners.html":"javascript:history.go(-4)"}' ><span>List</span></a></li>  --%>
				    <li><a href="searchPartner.html?partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account List</span></a></li>
			  	    <li><a href="editPartnerAddFormPopup.html?id=<%=request.getParameter("id") %>&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Detail</span></a></li>
			  	    <li><a href="accountInfoPage.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Info</span></a></li>
			  	    <li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Profile</span></a></li>
			  		<configByCorp:fieldVisibility componentId="component.standard.accountcontact"><li><a href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Contact</span></a></li></configByCorp:fieldVisibility>
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Policy</span></a></li>
			  	  </c:if>
					<c:url value="frequentlyAskedQuestionsList.html" var="url">
					<c:param name="partnerCode" value="${partner.partnerCode}"/>
					<c:param name="partnerType" value="AC"/>
					<c:param name="partnerId" value="${partner.id}"/>
					<c:param name="lastName" value="${partner.lastName}"/>
					<c:param name="status" value="${partner.status}"/>
					</c:url>			  	  
			  	  <c:if test="${empty param.popup}" >
			  	     <c:if test="${usertype!='ACCOUNT'}">
				    <li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}"><span>Account Detail</span></a></li>
				    <li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"><span>Account Profile</span></a></li>	
				    <c:if test="${sessionCorpID!='TSFT' }">
				    <li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
					</c:if>
			  		<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			  		<configByCorp:fieldVisibility componentId="component.standard.accountContactTab">
			  		<li><a href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}"><span>Account Contact</span></a></li>
			  		</configByCorp:fieldVisibility>
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Policy<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  		<%-- Added Portal Users Tab By Kunal for ticket number: 6176 --%>
			  		<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
						<c:if test="${usertype!='ACCOUNT'}">							
							<c:if test="${partner.partnerPortalActive == true && partnerType == 'AG'}">
						<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
					</c:if>
					<c:if test="${partner.partnerPortalActive == true && partnerType == 'AC'}">
					<configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">	<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users</span></a></li></configByCorp:fieldVisibility>
					</c:if>
						</c:if>
					</sec-auth:authComponent>
					<%-- Modification closed here for ticket number: 6176 --%>
			  		<c:if test='${paramValue == "View"}'>
						<li><a href="partnerPublics.html?partnerType=${partnerType}"><span>Partner List</span></a></li>
					</c:if>
					<c:if test='${paramValue != "View"}'>
						<li><a href="partnerPublics.html?partnerType=${partnerType}"><span>Partner List</span></a></li>
					</c:if>
			  		<!--<c:if test="${not empty partnerPrivate.id}">
						<li><a onclick="openNotesPopupTab(this);"><span>Notes</span></a></li>
					</c:if>
			  	  -->
			  	  <li><a href="partnerReloSvcs.html?id=${partner.id}&partnerType=${partnerType}"><span>Services</span></a></li>
				<c:if test="${partnerType == 'AC'}">
				 <c:if test="${not empty partner.id}">
				<li><a href="${url}"><span>FAQ</span></a></li>
				<c:if test="${partner.partnerPortalActive == true}">
					<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
				</c:if>
				  </c:if>
				</c:if>		
				</c:if>
				<c:if test="${usertype=='ACCOUNT'}">
				<c:if test="${partnerType == 'AC'}">
				 <c:if test="${not empty partner.id}">
				    <li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}"><span>Account Detail</span></a></li>
   			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Policy<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    <li><a href="${url}"><span>FAQ</span></a></li>
				 <li><a href="accountPortalPartnerFiles.html?id=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}&status=${partner.status}&lastName=${partner.lastName}"><span>Partner File Cabinet</span></a></li>
				 </c:if>
				 </c:if>   			  						
				</c:if>		  	   
			  	  </c:if>
			  	</ul>
		</div><div class="spn">&nbsp;</div>
		
		</div> 
<div id="Layer1" onkeydown="changeStatus();" style="width:100%;">
<div id="content" align="center">
<div id="liquid-round-top">
<div class="top"><span></span></div>
  <div class="center-content">
    <table style="margin-bottom: 3px">
    <tr>
	  		<td align="right" class="listwhitetext" width="60px">Name</td>
			<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.lastName" required="true" cssClass="input-textUpper" size="50" maxlength="80" readonly="true" /></td>
			<td align="right" class="listwhitetext"><fmt:message key='partner.partnerCode' /></td>
			<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="8" readonly="true" /></td>
			<td align="right" class="listwhitetext">Status</td>
			<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.status" required="true" cssClass="input-textUpper" size="15" readonly="true" /></td>
		</tr>
  </table>
  
  </div>
<div class="bottom-header" style="margin-top:30px;"><span></span></div>

<!--<div class="center-content">
<table class="" cellspacing="0" cellpadding="0"	border="0" width="100%">
	<tbody>
		<tr>
		<td>
		<table cellpadding="2" cellspacing="2" width="70%" border="0"  style="margin: 0px;">
		<tr>
	  		<td align="right" class="listwhitetext" width="60px">Name</td>
			<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.lastName" required="true" cssClass="input-textUpper" size="50" maxlength="80" readonly="true" /></td>
			<td align="right" class="listwhitetext"><fmt:message key='partner.partnerCode' /></td>
			<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="8" readonly="true" /></td>
			<td align="right" class="listwhitetext">Status</td>
			<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.status" required="true" cssClass="input-textUpper" size="15" readonly="true" /></td>
		</tr>
		<tr><td height="15"></td></tr>
		</table>
		</td>
		</tr>
	  	<tr>
		  	<td>
			  	<table cellpadding="0" cellspacing="0" width="70%" border="0"  style="margin: 0px;" >
					<tr>
						<td class="headtab_left"></td>
						<td class="headtab_center" >&nbsp;Details</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_special" >&nbsp;</td>
						<td class="headtab_right"></td>
					</tr>
				</table>
			  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					  <tbody> 
					  		<tr>
								<td height="8px"></td>
							</tr>
							<tr>
								<td align="right" class="listwhitetext" valign="top">&nbsp;&nbsp;</td>
								<td align="left" colspan="6"><s:textarea name="contractPolicy.policy"   cols="90" rows="20" cssClass="textarea"/></td>
							</tr>
						</tbody>
				</table>
			</td>
		</tr>
		<tr>
				<td height="10" align="left" class="listwhitetext"></td>
		</tr>		
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
--></div>
															 		  	   	 	               	

<div id="liquid-round-top">
<div class="top"><span></span></div>
  <div class="center-content">
    <table style="margin-bottom:3px;width:100%;">
    <tr>						
						<td colspan="20" width="" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('infoPackage')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Info Package
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_special">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					</div>
					  <div id="infoPackage">
											<display:table name="policyFileList" id="policyFileList" class="table" pagesize="10"  requestURI="" style="margin-top:15px">
											<display:column style="width:10%" property="docSequenceNumber" title="Sequence #"></display:column>
											<display:column property="sectionName" title="Section Name" href="addContractPolicyFile.html?id1=${partner.id}&partnerType=AC"  paramId="id" paramProperty="id"/>
											<display:column property="language" title="Language"/>
											<display:column property="updatedOn"  title="Updated On" format="{0,date,dd-MMM-yyyy}" />
											<display:column property="updatedBy"  title="Updated By"/>
											<c:if test="${usertype!='ACCOUNT'}">
											<c:choose>
												<c:when test="${partner.partnerType == 'DMM' or partner.partnerType == 'CMM'}">
													<configByCorp:fieldVisibility componentId="component.field.CmmDmm.visibility">
														<display:column title="Remove" style="width: 15px;" >
															<a><img align="middle" title="Remove" onclick="confirmSubmit('${policyFileList.id}','${policyFileList.sectionName}','${policyFileList.language}');"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a> 
														</display:column>
													</configByCorp:fieldVisibility>
												</c:when>
												<c:otherwise>
													<display:column title="Remove" style="width: 15px;" >
															<a><img align="middle" title="Remove" onclick="confirmSubmit('${policyFileList.id}','${policyFileList.sectionName}','${policyFileList.language}');"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a> 
													</display:column>
												</c:otherwise>
											</c:choose>
											</c:if>
											</display:table>
											</div>
											</td>
											</tr>
											
  </table><c:if test="${usertype!='ACCOUNT'}">
											<table width="100%">
											<tr align="right">
											<td align="right"><input type="button" class="cssbutton1" value="Update Child Accounts" style="width:145px; height:25px;" onclick="updateChildFromParent();"/></td><td></td>
											</tr>
											<tr align="right">
											<td align="right" class="listwhitetext">Exclude from Parent updates</td>
											<td class="listwhitetext" width="8px" align="right"><s:checkbox key="partnerPrivate.excludeFromParentPolicyUpdate" id="excludeFPU" value="${isExcludeFromParentPolicyUpdate}" onclick="updatePartnerPrivate();" fieldValue="true"/></td>			 		  	   	 	               	
											</tr>												
											</table> 	</c:if>  </div>
 
<div class="bottom-header"><span></span></div>
</div>




</div>
</div>
<!--<table width="700px">
			<tbody>
				<tr>
				
				      <td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="containerCreatedOnFormattedValue" value="${contractPolicy.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="contractPolicy.createdOn" value="${containerCreatedOnFormattedValue}" />
						<td><fmt:formatDate value="${contractPolicy.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
					
					
					<td align="right" class="listwhitetext" width="100"><b><fmt:message key='contractPolicy.createdBy' /></b></td>
					
					<c:if test="${not empty contractPolicy.id}">
						<s:hidden name="contractPolicy.createdBy"/>
						<td ><s:label name="createdBy" value="%{contractPolicy.createdBy}"/></td>
					</c:if>
					<c:if test="${empty contractPolicy.id}">
						<s:hidden name="contractPolicy.createdBy" value="${pageContext.request.remoteUser}"/>
						<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
					</c:if>
					<td align="right" class="listwhitetext" width="90"><b><fmt:message key='contractPolicy.updatedOn'/></b></td>
					<s:hidden name="contractPolicy.updatedOn"/>
					<td width="130"><fmt:formatDate value="${contractPolicy.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
					<td align="right" class="listwhitetext" width="90"><b><fmt:message key='contractPolicy.updatedBy' /></b></td>
					<c:if test="${not empty contractPolicy.id}">
								<s:hidden name="contractPolicy.updatedBy"/>
								<td><s:label name="updatedBy" value="%{contractPolicy.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty contractPolicy.id}">
								<s:hidden name="contractPolicy.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							
							
				</tr>
			</tbody>
			</table>  
--><c:if test="${empty param.popup}" >
<c:if test="${usertype!='ACCOUNT'}">
	<input type="button" class="cssbutton" style="width:55px; height:25px" onclick="contractPolicyFile();" value="<fmt:message key="button.add"/>"/>
</c:if>	
	
</c:if>
</s:form>

<script>
function checkdate(clickType){
	if(!(clickType == 'save')){		
		var id = document.forms['contractPolicyForm'].elements['contractPolicy.id'].value;
		var id1 = document.forms['contractPolicyForm'].elements['partner.id'].value;
		var partnerType = document.forms['contractPolicyForm'].elements['partnerType'].value;
		if (document.forms['contractPolicyForm'].elements['formStatus'].value == '1'){
			var agree = confirm("Do you want to save the contact detail and continue? press OK to continue with save OR press CANCEL");
			if(agree){
				document.forms['contractPolicyForm'].action = 'saveContractPolicy!saveOnTabChange.html';
				document.forms['contractPolicyForm'].submit();
			}else{
				if(id != ''){
				if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
					location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AC';
					}
				if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountprofile'){
					location.href = 'editNewAccountProfile.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
					location.href = 'accountContactList.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
					location.href = 'editContractPolicy.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
					if('<%=session.getAttribute("paramView")%>' == 'View'){
							location.href = 'searchPartnerView.html';
						}else{
							location.href = 'partnerPublics.html?partnerType=AC';
						}
					}
					if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.partnerReloSvcs'){
					location.href = 'partnerReloSvcs.html?id='+id1+'&partnerType='+partnerType;
					}
			}
			}
		}else{
			
		if(id != ''){
			alert(clickType);		
				if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
					location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AC';
					}
				if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountprofile'){
					location.href = 'editNewAccountProfile.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
					location.href = 'accountContactList.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
					location.href = 'editContractPolicy.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
					if('<%=session.getAttribute("paramView")%>' == 'View'){
							location.href = 'searchPartnerView.html';
						}else{
							location.href = 'partnerPublics.html?partnerType=AC';
						}
					}
					if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.partnerReloSvcs'){
					location.href = 'partnerReloSvcs.html?id='+id1+'&partnerType='+partnerType;
					}
		}
		}
	}
}
function changeStatus(){
	document.forms['contractPolicyForm'].elements['formStatus'].value = '1';
}
function openNotesPopupTab(targetElement){
	openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&subType=VipReason&imageId=partnerNotesImage&fieldId=partnerNotes&decorator=popup&popup=true',755,500);
}
function disabledAll(){
	var elementsLen=document.forms['contractPolicyForm'].elements.length;
	for(i=0;i<=elementsLen-1;i++){
		if(document.forms['contractPolicyForm'].elements[i].type=='text'){
			document.forms['contractPolicyForm'].elements[i].readOnly =true;
			document.forms['contractPolicyForm'].elements[i].className = 'input-textUpper';
		}else{
			document.forms['contractPolicyForm'].elements[i].disabled=true;
		} 
	}
}
function confirmSubmit(targetElement,documentSectionName,documentLanguage){
	var agree=confirm("Are you sure you wish to remove this Info Package section?");
	var did = targetElement;
	var dSectionName=documentSectionName;
	var dLanguage=documentLanguage;
	if (agree){
		window.location='policyFileDelete.html?id=${partner.id}&documentSectionName='+dSectionName+'&documentLanguage='+dLanguage+'&partnerCode=${partner.partnerCode}&id2='+did+'&partnerType=${partnerType}';
	}else{
		return false;
	}
}
function contractPolicyFile(){
	 location.href="addContractPolicyFile.html?id1=${partner.id}&partnerType=AC";	
}
function show(){
	var redirect = document.forms['contractPolicyForm'].elements['btnType'].value;
	if(redirect=='del'){
		location.href = "/editContractPolicy.html?id=${partner.id}&partnerType=AC"
	}
}
function updateChildFromParent()
{
	  var url="updateChildFromParent.html?decorator=simple&popup=true&partnerCode=${partner.partnerCode}&pageListType=CPF";
	  http11.open("GET", url, true); 
	  http11.onreadystatechange = handleHttpResponse1; 
      http11.send(null); 
}
function handleHttpResponse1(){
    if (http11.readyState == 4)
    {
		var results = http11.responseText
	    results = results.trim();					               
		if(results!="")	{
			alert(results);				
		} 
    } 
}

function updatePartnerPrivate(){
	 var excludeFPU = document.getElementById("excludeFPU");	
	 if(excludeFPU.checked==true)
	 {
		  var url="updatePartnerPrivate.html?decorator=simple&popup=true&excludeOption=CPF&excludeFPU=TRUE&partnerCode=${partner.partnerCode}";
		  http10.open("GET", url, true); 
		  http10.onreadystatechange = handleHttpResponse; 
	      http10.send(null); 
	 }else{
		  var url="updatePartnerPrivate.html?decorator=simple&popup=true&excludeOption=CPF&excludeFPU=FALSE&partnerCode=${partner.partnerCode}";
		  http10.open("GET", url, true); 
	      http10.onreadystatechange = handleHttpResponse; 
	      http10.send(null); 
	 }
}

function handleHttpResponse(){
   if(http10.readyState == 4){
       var results = http10.responseText
       results = results.trim();					               
	   if(results=="")	{}		
   } 
}
var http10 = getHTTPObject();
var http11 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
</script>

<script type="text/javascript"> 
try{
<c:if test="${param.popup}" >
	for(i=0;i<100;i++){
		document.forms['contractPolicyForm'].elements[i].disabled = true;
	}
	Form.focusFirstElement($("contractPolicyForm"));   
</c:if>  
}
catch(e){}
    <c:if test="${not empty partner.id}">
    <c:if test="${hitFlag=='5'}">
     <c:redirect url="/editContractPolicy.html?id=${partner.id}&partnerType=AC"/>
    </c:if>
    </c:if>
  
    
</script>  
<script type="text/javascript">
try{	
	 <sec-auth:authComponent componentId="module.script.form.corpAccountScript">
	 disabledAll();
	 	</sec-auth:authComponent>
}
catch(e){}
try{
	  <c:if test="${not empty partner.id}">
	    <c:if test="${hitFlag=='7'}">
	     <c:redirect url="/editContractPolicy.html?id=${partner.id}&partnerType=AC"/>
	    </c:if>
	    </c:if>
}catch(e){}
try{
	<c:if test="${not empty partner.id}">
    <c:if test="${hitFlag=='4'}">
     <c:redirect url="/editContractPolicy.html?id=${partner.id}&partnerType=AC"/>
    </c:if>
    </c:if>
}catch(e){
}
</script>
<script type="text/javascript">
		  	<c:if test="${excludeFPU==true}">
							document.getElementById("excludeFPU").checked=true;
			</c:if>
		  	<c:if test="${excludeFPU==false}">
			document.getElementById("excludeFPU").checked=false;
			</c:if>

			
			function checkCmmDmmDisable(sessionCorpID){
				if(sessionCorpID != 'TSFT'){
					if(sessionCorpID != 'UTSI'){
						if('${partner.partnerType}' == 'DMM' || '${partner.partnerType}' == 'CMM'){
								var elementsLen=document.forms['contractPolicyForm'].elements.length;
								for(i=0;i<=elementsLen-1;i++){
									if(document.forms['contractPolicyForm'].elements[i].type=='text'){
										document.forms['contractPolicyForm'].elements[i].readOnly =true;
										document.forms['contractPolicyForm'].elements[i].className = 'input-textUpper';
									}else if(document.forms['contractPolicyForm'].elements[i].type=='textarea'){
										document.forms['contractPolicyForm'].elements[i].readOnly =true;
										document.forms['contractPolicyForm'].elements[i].className = 'textareaUpper';
									}else if(document.forms['contractPolicyForm'].elements[i].type=='submit' || document.forms['contractPolicyForm'].elements[i].type=='reset' || document.forms['contractPolicyForm'].elements[i].type=='button'){
										document.forms['contractPolicyForm'].elements[i].style.display='none';
									}else{
										document.forms['contractPolicyForm'].elements[i].disabled=true;
									} 
							}
						}
					}
				}
			}

			<c:if test="${not empty partner.id && partnerType=='AC'}">
					checkCmmDmmDisable('${userCorpID}');
			</c:if>
</script>
