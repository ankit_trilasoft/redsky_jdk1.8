<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="partnerDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerDetail.heading'/>"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
<style type="text/css">
		h2 {background-color: #FBBFFF}
	.ui-autocomplete {
    max-height: 250px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }	
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>

<script language="javascript" type="text/javascript">
<%--function clear_fields(){
	document.forms['partnerAddForm'].elements['partner.mailingAddress1'].value="${partner.mailingAddress1}";	
	document.forms['partnerAddForm'].elements['partner.mailingAddress2'].value="${partner.mailingAddress2}";		
	document.forms['partnerAddForm'].elements['partner.mailingAddress3'].value="${partner.mailingAddress3}";
	document.forms['partnerAddForm'].elements['partner.mailingAddress4'].value="${partner.mailingAddress4}";
	document.forms['partnerAddForm'].elements['partner.mailingCountry'].value="${partner.mailingCountry}";
	document.forms['partnerAddForm'].elements['partner.mailingCity'].value="${partner.mailingCity}";
	document.forms['partnerAddForm'].elements['partner.mailingZip'].value="${partner.mailingZip}";	
	document.forms['partnerAddForm'].elements['partner.mailingPhone'].value="${partner.mailingPhone}";	
	document.forms['partnerAddForm'].elements['partner.mailingEmail'].value="${partner.mailingEmail}";		
	document.forms['partnerAddForm'].elements['partner.mailingFax'].value="${partner.mailingFax}";
	document.forms['partnerAddForm'].elements['partner.billingAddress1'].value="${partner.billingAddress1}";	
	document.forms['partnerAddForm'].elements['partner.billingAddress2'].value="${partner.billingAddress2}";		
	document.forms['partnerAddForm'].elements['partner.billingAddress3'].value="${partner.billingAddress3}";	
	document.forms['partnerAddForm'].elements['partner.billingAddress4'].value="${partner.billingAddress4}";
	document.forms['partnerAddForm'].elements['partner.billingCountry'].value="${partner.billingCountry}";
	document.forms['partnerAddForm'].elements['partner.billingCity'].value="${partner.billingCity}";
	document.forms['partnerAddForm'].elements['partner.billingZip'].value="${partner.billingZip}";
	document.forms['partnerAddForm'].elements['partner.billingPhone'].value="${partner.billingPhone}";		
	document.forms['partnerAddForm'].elements['partner.billingEmail'].value="${partner.billingEmail}";
	document.forms['partnerAddForm'].elements['partner.billingFax'].value="${partner.billingFax}";
	document.forms['partnerAddForm'].elements['partner.mailingState'].value = "${partner.mailingState}";
    document.forms['partnerAddForm'].elements['partner.billingState'].value ="${partner.billingState}";
    document.forms['partnerAddForm'].elements['partner.firstName'].value = "${partner.firstName}";
    document.forms['partnerAddForm'].elements['partner.OMNINumber'].value ="${partner.OMNINumber}";
    document.forms['partnerAddForm'].elements['partner.lastName'].value ="${partner.lastName}";
    document.forms['partnerAddForm'].elements['partnerPrefix'].value ="${partnerPrefix}";
    document.forms['partnerAddForm'].elements['creditTerm'].value ="${creditTerm}";
    }--%>
function backData(){
	document.forms['partnerAddForm'].elements['partner.billingAddress1'].value="";	
	document.forms['partnerAddForm'].elements['partner.billingAddress2'].value="";		
	document.forms['partnerAddForm'].elements['partner.billingAddress3'].value="";	
	document.forms['partnerAddForm'].elements['partner.billingCountry'].value="";
	document.forms['partnerAddForm'].elements['partner.billingCity'].value="";
	document.forms['partnerAddForm'].elements['partner.billingZip'].value="";
	document.forms['partnerAddForm'].elements['partner.billingPhone'].value="";		
	document.forms['partnerAddForm'].elements['partner.billingEmail'].value="";
	document.forms['partnerAddForm'].elements['partner.billingFax'].value="";
	document.forms['partnerAddForm'].elements['partner.mailingState'].value = "";
    document.forms['partnerAddForm'].elements['partner.billingState'].value ="";
    document.forms['partnerAddForm'].elements['partner.firstName'].value = "";
    document.forms['partnerAddForm'].elements['partner.lastName'].value ="";
    document.forms['partnerAddForm'].elements['partnerPrefix'].value ="";
}

function copyBillToState2(){
var abc=document.forms['partnerAddForm'].elements['partner.billingState'].value;
document.forms['partnerAddForm'].elements['partner.mailingState'].value=abc;
}


function copyBillToMail2(){ 
			document.forms['partnerAddForm'].elements['partner.mailingAddress1'].value=document.forms['partnerAddForm'].elements['partner.billingAddress1'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress2'].value=document.forms['partnerAddForm'].elements['partner.billingAddress2'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress3'].value=document.forms['partnerAddForm'].elements['partner.billingAddress3'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress4'].value=document.forms['partnerAddForm'].elements['partner.billingAddress4'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingEmail'].value=document.forms['partnerAddForm'].elements['partner.billingEmail'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingCity'].value=document.forms['partnerAddForm'].elements['partner.billingCity'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingFax'].value=document.forms['partnerAddForm'].elements['partner.billingFax'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingZip'].value=document.forms['partnerAddForm'].elements['partner.billingZip'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingCountry'].value=document.forms['partnerAddForm'].elements['partner.billingCountry'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingPhone'].value=document.forms['partnerAddForm'].elements['partner.billingPhone'].value;
			
			//document.forms['partnerAddForm'].elements['partner.mailingTelex'].value=document.forms['partnerAddForm'].elements['partner.billingTelex'].value;	
			
			//autoPopulate_partner_mailingCountry(document.forms['partnerAddForm'].elements['partner.mailingCountry']);        
			
			
			var oriCountry = document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
			if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
				var billingStateIndex = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				var billingStateText = document.forms['partnerAddForm'].elements['partner.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerAddForm'].elements['partner.billingState'].value;
				//alert(billingStateText+" --- "+billingStateValue);
				var x=document.getElementById("partnerAddForm_partner_mailingState")	
				//var combo1 = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				getMailingState(document.forms['partnerAddForm'].elements['partner.mailingCountry']);
				document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = false;
				setTimeout("copyBillToState()",1000);
			}else{
				var billingStateIndex = document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex;
				var billingStateText = document.forms['partnerAddForm'].elements['partner.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['partnerAddForm'].elements['partner.billingState'].value;
				//alert(billingStateText+" --- "+billingStateValue);
				var x=document.getElementById("partnerAddForm_partner_mailingState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = true;
			}
			//alert(document.forms['partnerAddForm'].elements['partner.mailingState'].value);
			//getMailingState(document.forms['partnerAddForm'].elements['partner.mailingCountry']);
}
function backData(){
	document.forms['partnerAddForm'].elements['partner.billingAddress1'].value="";	
	document.forms['partnerAddForm'].elements['partner.billingAddress2'].value="";		
	document.forms['partnerAddForm'].elements['partner.billingAddress3'].value="";	
	document.forms['partnerAddForm'].elements['partner.billingCountry'].value="";
	document.forms['partnerAddForm'].elements['partner.billingCity'].value="";
	document.forms['partnerAddForm'].elements['partner.billingZip'].value="";
	document.forms['partnerAddForm'].elements['partner.billingPhone'].value="";		
	document.forms['partnerAddForm'].elements['partner.billingEmail'].value="";
	document.forms['partnerAddForm'].elements['partner.billingFax'].value="";
	document.forms['partnerAddForm'].elements['partner.mailingState'].value = "";
    document.forms['partnerAddForm'].elements['partner.billingState'].value ="";
    document.forms['partnerAddForm'].elements['partner.firstName'].value = "";
    document.forms['partnerAddForm'].elements['partner.lastName'].value ="";
    document.forms['partnerAddForm'].elements['partnerPrefix'].value ="";
}

function copyBillToState() {
	var country=document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
		var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=='${entry.value}'){
			countryCode='${entry.key}';
		}
		</c:forEach>
		var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	     http66.open("GET", url, true);
	     http66.onreadystatechange = handleHttpResponse99;
	     http66.send(null);
	}
	  
	 function handleHttpResponse99()
	 {
	     if (http66.readyState == 4)
	      {
	         var results = http66.responseText
	         results = results.trim();
	         res = results.split("@");
	        	targetElement = document.forms['partnerAddForm'].elements['partner.mailingState'];
				targetElement.length = res.length;
				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].text = '';
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].text = stateVal[1];
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].value = stateVal[0];
					}
					}
					var billState=document.forms['partnerAddForm'].elements['partner.billingState'].value;
					document.forms['partnerAddForm'].elements['partner.mailingState'].selectedIndex=true;
	 	 			document.forms['partnerAddForm'].elements['partner.mailingState'].value = billState;
				}
	      }
	 function getHTTPObject33()
	 {
	     var xmlhttp;
	     if(window.XMLHttpRequest)
	     {
	         xmlhttp = new XMLHttpRequest();
	     }
	     else if (window.ActiveXObject)
	     {
	         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	         if (!xmlhttp)
	         {
	             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	         }
	     }
	     return xmlhttp;
	 }
	 var http66 = getHTTPObject33();
 function copyBillToMail(){ 
			document.forms['partnerAddForm'].elements['partner.mailingAddress1'].value=document.forms['partnerAddForm'].elements['partner.billingAddress1'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress2'].value=document.forms['partnerAddForm'].elements['partner.billingAddress2'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress3'].value=document.forms['partnerAddForm'].elements['partner.billingAddress3'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress4'].value=document.forms['partnerAddForm'].elements['partner.billingAddress4'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingEmail'].value=document.forms['partnerAddForm'].elements['partner.billingEmail'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingCity'].value=document.forms['partnerAddForm'].elements['partner.billingCity'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingFax'].value=document.forms['partnerAddForm'].elements['partner.billingFax'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingZip'].value=document.forms['partnerAddForm'].elements['partner.billingZip'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingCountry'].value=document.forms['partnerAddForm'].elements['partner.billingCountry'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingPhone'].value=document.forms['partnerAddForm'].elements['partner.billingPhone'].value;		
						
			var oriCountry = document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
			var enbState = '${enbState}';
			var index = (enbState.indexOf(oriCountry)> -1);
			if(index != ''){
				document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = false;
				setTimeout("copyBillToState()");
			}else{
				document.forms['partnerAddForm'].elements['partner.mailingState'].value = '';
				document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = true;
			}
	}


function onLoad() {
	//document.forms['partnerAddForm'].elements['partner.billingCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.billingCountry'].value;
	//document.forms['partnerAddForm'].elements['partner.terminalCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.terminalCountry'].value;
	//document.forms['partnerAddForm'].elements['partner.mailingCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
	//document.forms['partnerAddForm'].elements['partner.billingInstructionCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.billingInstructionCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.billingInstruction'].value;
	if(navigator.appName == 'Microsoft Internet Explorer'){
			document.forms['partnerAddForm'].elements['dateFormat'].value="MM/dd/yyyy";
		}
		if(navigator.appName == 'Netscape'){
			document.forms['partnerAddForm'].elements['dateFormat'].value="MM/dd/yyyy";
		}
	var f = document.getElementById('partnerAddForm'); 
	f.setAttribute("autocomplete", "off");
	autoPopulate_partner_mailingCountry(document.forms['partnerAddForm'].elements['partner.mailingCountry']);
	autoPopulate_partner_billingCountry(document.forms['partnerAddForm'].elements['partner.billingCountry']);
	}

function submitForm(){
	var partnerCode= document.forms['partnerAddForm'].elements['partner.partnerCode'].value;
	var lastName= document.forms['partnerAddForm'].elements['partner.lastName'].value;
	document.forms['partnerAddForm'].action="savePartnerPopup.html?decorator=popup&popup=true&listCode="+partnerCode+"&listDescription="+lastName+"&listSecondDescription=&listThirdDescription=null&listFourthDescription=null&listFifthDescription=null&listSixthDescription="+lastName+"&fld_sixthDescription=customerFile.customerEmployer&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.billToName&fld_code=customerFile.billToCode";
	document.forms['partnerAddForm'].submit();
	document.forms['partnerAddForm'].elements['saveButton'].disabled=true;
}
</script>
	

	<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==35) || (keyCode==36); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	
	function onlyPhoneNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
	}
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
	
	
	function disabledAll(){
	if(!(document.forms['partnerAddForm'].elements['partner.id'].value == '')){
	var i;
			for(i=0;i<=13;i++)
			{
					document.forms['partnerAddForm'].elements[i].disabled = true;
			}
		}
	}
	
</script>	
<script type="text/javascript">
	function autoPopulate_partner_billingCountry(targetElement) {
		var oriCountry = targetElement.value;
		if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
			document.forms['partnerAddForm'].elements['partner.billingState'].disabled = false;
		}else{
			document.forms['partnerAddForm'].elements['partner.billingState'].disabled = true;
			document.forms['partnerAddForm'].elements['partner.billingState'].value = '';
		}
	}
	function autoPopulate_partner_terminalCountry(targetElement) {
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['partnerAddForm'].elements['partner.terminalState'].disabled = false;
		}else{
			document.forms['partnerAddForm'].elements['partner.terminalState'].disabled = true;
			document.forms['partnerAddForm'].elements['partner.terminalState'].value = '';
		}
	}
	function autoPopulate_partner_mailingCountry(targetElement) {
		var oriCountry = targetElement.value;
		if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
			document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = false;
		}else{
			document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = true;
			document.forms['partnerAddForm'].elements['partner.mailingState'].value = '';
		}
	}
</script>	
<SCRIPT LANGUAGE="JavaScript">
function getMailingCountryCode(targetElement){
	var countryName=document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http5.open("GET", url, true);
    http5.onreadystatechange = handleHttpResponseMailingCountry;
    http5.send(null);
}
function handleHttpResponseMailingCountry()
        {

             if (http5.readyState == 4)
             {
                var results = http5.responseText
                results = results.trim();
                if(results.length>=1)
                	{
 					document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value = results;
 					//document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].select();
 					 }
                 else
                 {
                     
                 }
             }
        }

function getMailingState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=='${entry.value}'){
	countryCode='${entry.key}';
	}
	</c:forEach>

	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse3;
     http3.send(null);
	
	}
	
function handleHttpResponse3()
        {
	  if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['partnerAddForm'].elements['partner.mailingState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].text = '';
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					// document.forms['partnerAddForm'].elements['partner.mailingState'].selectedIndex=false;
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].text = stateVal[1];
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].value = stateVal[0];
				
					}
					}
			             }
        } 	

function getBillingState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	
	<c:forEach var="entry" items="${countryCod}">
	if(country=='${entry.value}'){
		countryCode='${entry.key}';
	}
	</c:forEach>	
	
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
}
  
 function handleHttpResponse2()
 {
      if (http2.readyState == 4)
      {
         var results = http2.responseText
         results = results.trim();
         res = results.split("@");
         targetElement = document.forms['partnerAddForm'].elements['partner.billingState'];
			targetElement.length = res.length;
			for(i=0;i<res.length;i++)
				{
				if(res[i] == ''){
				document.forms['partnerAddForm'].elements['partner.billingState'].options[i].text = '';
				document.forms['partnerAddForm'].elements['partner.billingState'].options[i].value = '';
				}else{
				stateVal = res[i].split("#");
				// document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex=true;
				document.forms['partnerAddForm'].elements['partner.billingState'].options[i].text = stateVal[1];
				document.forms['partnerAddForm'].elements['partner.billingState'].options[i].value = stateVal[0];
				}
				}

      }

 } 
 function getCopyAddress(from) {
	 var country = "";
	 var countryCode = "";
	 if(from == 'OA'){
		var orgAddress=document.forms['partnerAddForm'].elements['orgAddress'].value;
		var orgArray=orgAddress.split('~');
		var country=orgArray[3];
		var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=='${entry.value}'){
			countryCode='${entry.key}';
		}
		</c:forEach>
	 }
	 if(from == 'DA'){
		var destAddress=document.forms['partnerAddForm'].elements['destAddress'].value;
		var destArray=destAddress.split('~')
		var country=destArray[3];
		var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=='${entry.value}'){
			countryCode='${entry.key}';
		}
		</c:forEach>	
	 }
		var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	     http6.open("GET", url, true);
	     http6.onreadystatechange = handleHttpResponse9;
	     http6.send(null);
	}
	  
	 function handleHttpResponse9()
	 {
	     if (http6.readyState == 4)
	      {
	         var results = http6.responseText
	         results = results.trim();
	         res = results.split("@");
	        targetElement = document.forms['partnerAddForm'].elements['partner.billingState'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++){
					if(res[i] == ''){
					document.forms['partnerAddForm'].elements['partner.billingState'].options[i].text = '';
					document.forms['partnerAddForm'].elements['partner.billingState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['partnerAddForm'].elements['partner.billingState'].options[i].text = stateVal[1];
					document.forms['partnerAddForm'].elements['partner.billingState'].options[i].value = stateVal[0];
					}
				}
				targetElement = document.forms['partnerAddForm'].elements['partner.mailingState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].text = '';
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].text = stateVal[1];
					document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].value = stateVal[0];
					}
					}
				var hidAddress=document.forms['partnerAddForm'].elements['hidAddress'].value;
				if(hidAddress=='true'){
				var orgAddress=document.forms['partnerAddForm'].elements['orgAddress'].value;
					var orgArray=orgAddress.split('~');
						document.forms['partnerAddForm'].elements['partner.mailingState'].selectedIndex=true;
						document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex=true;
	 	 				document.forms['partnerAddForm'].elements['partner.mailingState'].value = orgArray[4];
					    document.forms['partnerAddForm'].elements['partner.billingState'].value = orgArray[4];
				}else{
					var destAddress=document.forms['partnerAddForm'].elements['destAddress'].value;
					var destArray=destAddress.split('~');
					document.forms['partnerAddForm'].elements['partner.mailingState'].selectedIndex=true;
					document.forms['partnerAddForm'].elements['partner.billingState'].selectedIndex=true;
 	 				document.forms['partnerAddForm'].elements['partner.mailingState'].value = destArray[4];
				    document.forms['partnerAddForm'].elements['partner.billingState'].value = destArray[4];
				}
	      }
	 } 

	 function getHTTPObject3()
	 {
	     var xmlhttp;
	     if(window.XMLHttpRequest)
	     {
	         xmlhttp = new XMLHttpRequest();
	     }
	     else if (window.ActiveXObject)
	     {
	         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	         if (!xmlhttp)
	         {
	             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	         }
	     }
	     return xmlhttp;
	 }
	     var http6 = getHTTPObject3();
function getBillingCountryCode(targetElement){
	var countryName=document.forms['partnerAddForm'].elements['partner.billingCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseBillingCountry;
    http4.send(null);
}
function handleHttpResponseBillingCountry()
        {

             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value = results;
 					//document.forms['partnerAddForm'].elements['partner.billingCountryCode'].select();
					 }
                 else
                 {
                     
                 }
             }
        }        
        

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    
    function changeStatus(){
    document.forms['partnerAddForm'].elements['formStatus'].value = '1';
}

function getHTTPObject2()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject2(); 
  
function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1();   
    var http5 = getHTTPObject1() 
    
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

</script>
<script type="text/javascript">
function copyBillToOriginAdd(){
	var orgAddress=document.forms['partnerAddForm'].elements['orgAddress'].value;
	    orgAddress = orgAddress.replace('%22',"\"");
	var orgArray=orgAddress.split('~');
	var orgCountry=orgArray[3];
	var enbState = '${enbState}';
	var index = (enbState.indexOf(orgCountry)> -1);
	requiredStateOrigin();
	if(index != ''){
	document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = false;
	document.forms['partnerAddForm'].elements['partner.billingState'].disabled = false;
	document.forms['partnerAddForm'].elements['hidAddress'].value=true;
	setTimeout("getCopyAddress('OA')");
	}else{
	document.forms['partnerAddForm'].elements['partner.mailingState'].value='';
	document.forms['partnerAddForm'].elements['partner.billingState'].value='';
	document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = true;
	document.forms['partnerAddForm'].elements['partner.billingState'].disabled = true;
	}
	if(orgArray != ''){
	document.forms['partnerAddForm'].elements['partner.mailingAddress1'].value=orgArray[0];		
	document.forms['partnerAddForm'].elements['partner.mailingAddress2'].value=orgArray[1];		
	document.forms['partnerAddForm'].elements['partner.mailingAddress3'].value=orgArray[2];
	document.forms['partnerAddForm'].elements['partner.mailingCountry'].value=orgArray[3];	
	document.forms['partnerAddForm'].elements['partner.mailingCity'].value=orgArray[5];
	document.forms['partnerAddForm'].elements['partner.mailingZip'].value=orgArray[6];	
	document.forms['partnerAddForm'].elements['partner.mailingPhone'].value=orgArray[7];	
	document.forms['partnerAddForm'].elements['partner.mailingEmail'].value=orgArray[8];		
	document.forms['partnerAddForm'].elements['partner.mailingFax'].value=orgArray[9];
	document.forms['partnerAddForm'].elements['partner.billingAddress1'].value=orgArray[0];		
	document.forms['partnerAddForm'].elements['partner.billingAddress2'].value=orgArray[1];		
	document.forms['partnerAddForm'].elements['partner.billingAddress3'].value=orgArray[2];	
	document.forms['partnerAddForm'].elements['partner.billingCountry'].value=orgArray[3];
	document.forms['partnerAddForm'].elements['partner.billingCity'].value=orgArray[5];
	document.forms['partnerAddForm'].elements['partner.billingZip'].value=orgArray[6];
	document.forms['partnerAddForm'].elements['partner.billingPhone'].value=orgArray[7];		
	document.forms['partnerAddForm'].elements['partner.billingEmail'].value=orgArray[8];
	document.forms['partnerAddForm'].elements['partner.billingFax'].value=orgArray[9];
	}
}
function copyBillToDestinationAdd(){
	var destAddress=document.forms['partnerAddForm'].elements['destAddress'].value;
	    destAddress = destAddress.replace('%22',"\"");
	var destArray=destAddress.split('~')
	var destCountry=destArray[3];
	 var enbState = '${enbState}';
	 var index = (enbState.indexOf(destCountry)> -1);
	 requiredStateDest();
	if(index != ''){
	document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = false;
	document.forms['partnerAddForm'].elements['partner.billingState'].disabled = false;
	document.forms['partnerAddForm'].elements['hidAddress'].value=false;
		setTimeout("getCopyAddress('DA')");
		}else{
			document.forms['partnerAddForm'].elements['partner.mailingState'].value='';
			document.forms['partnerAddForm'].elements['partner.billingState'].value='';
			document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = true;
			document.forms['partnerAddForm'].elements['partner.billingState'].disabled = true;
			}
	if(destArray != ''){
	document.forms['partnerAddForm'].elements['partner.mailingAddress1'].value=destArray[0];		
	document.forms['partnerAddForm'].elements['partner.mailingAddress2'].value=destArray[1];		
	document.forms['partnerAddForm'].elements['partner.mailingAddress3'].value=destArray[2];
	document.forms['partnerAddForm'].elements['partner.mailingCountry'].value=destArray[3];	
	document.forms['partnerAddForm'].elements['partner.mailingCity'].value=destArray[5];
	document.forms['partnerAddForm'].elements['partner.mailingZip'].value=destArray[6];	
	document.forms['partnerAddForm'].elements['partner.mailingPhone'].value=destArray[7];	
	document.forms['partnerAddForm'].elements['partner.mailingEmail'].value=destArray[8];
	document.forms['partnerAddForm'].elements['partner.mailingFax'].value=destArray[9];	
	document.forms['partnerAddForm'].elements['partner.billingAddress1'].value=destArray[0];		
	document.forms['partnerAddForm'].elements['partner.billingAddress2'].value=destArray[1];		
	document.forms['partnerAddForm'].elements['partner.billingAddress3'].value=destArray[2];	
	document.forms['partnerAddForm'].elements['partner.billingCountry'].value=destArray[3];
	document.forms['partnerAddForm'].elements['partner.billingCity'].value=destArray[5];
	document.forms['partnerAddForm'].elements['partner.billingZip'].value=destArray[6];
	document.forms['partnerAddForm'].elements['partner.billingPhone'].value=destArray[7];		
	document.forms['partnerAddForm'].elements['partner.billingEmail'].value=destArray[8];
	document.forms['partnerAddForm'].elements['partner.billingFax'].value=destArray[9];
	}

}

function resetBillingState(target) {
 	var country = target;
 	var countryCode = "";
 	
 	<c:forEach var="entry" items="${countryCod}">
	if(country=='${entry.value}'){
		countryCode='${entry.key}';
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse12;
     http2.send(null);
	
	}
function handleHttpResponse12(){
	if (http2.readyState == 4){
        var results = http2.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['partnerAddForm'].elements['partner.billingState'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['partnerAddForm'].elements['partner.billingState'].options[i].text = '';
				document.forms['partnerAddForm'].elements['partner.billingState'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				//document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].selectedIndex=true;
				document.forms['partnerAddForm'].elements['partner.billingState'].options[i].text = stateVal[1];
				document.forms['partnerAddForm'].elements['partner.billingState'].options[i].value = stateVal[0];
				document.forms['partnerAddForm'].elements['partner.billingState'].value='${partner.billingState}';
			}
		}	
		
	}
}  
function resetmailingState(target) {
	var country = target;
 	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=='${entry.value}'){
		countryCode='${entry.key}';
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse13;
     http4.send(null);
	
	}
function handleHttpResponse13(){
	if (http4.readyState == 4){
        var results = http4.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['partnerAddForm'].elements['partner.mailingState'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].text = '';
				document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				//document.forms['partnerPublicForm'].elements['partnerPublic.billingState'].selectedIndex=true;
				document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].text = stateVal[1];
				document.forms['partnerAddForm'].elements['partner.mailingState'].options[i].value = stateVal[0];
				document.forms['partnerAddForm'].elements['partner.mailingState'].value='${partner.mailingState}';
			}
		}	
		
	}
}
function reset_partner_billingCountry() {

   	var oriCountry ='${partner.billingCountry}';
   	var red=document.getElementById("redHidden");
   	var enbState = '${enbState}';
   	var r2=document.getElementById('redHiddenFalse');
	var index = (enbState.indexOf(oriCountry)> -1);
	
	if(index != '' && oriCountry!=''){
		document.forms['partnerAddForm'].elements['partner.billingState'].disabled = false;
		try{
		red.style.display='block';
		r2.style.display = 'none';
		}catch(e){}
		resetBillingState(oriCountry);
	}else{
		document.forms['partnerAddForm'].elements['partner.billingState'].disabled = true;
		try{
		red.style.display='none';
		r2.style.display = 'block';
		}catch(e){}
		document.forms['partnerAddForm'].elements['partner.billingState'].value = '';		
	}
}
function reset_partner_mailingCountry() {   
	var dCountry ='${partner.mailingCountry}';
	var enbState = '${enbState}';
	var index = (enbState.indexOf(dCountry)> -1);
	if(index != ''){
			document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = false;
			resetmailingState(dCountry);	
		}else{
			document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = true;
			document.forms['partnerAddForm'].elements['partner.mailingState'].value = '';
		}
	}
</script>
<script type="text/javascript">

function buttondisabled(){
	var disableButton=document.forms['partnerAddForm'].elements['disable'].value;
	if(disableButton=='D'){
	document.forms['partnerAddForm'].elements['billingaddress'].disabled =true;	
	document.forms['partnerAddForm'].elements['originaddress'].disabled = true;
	document.forms['partnerAddForm'].elements['destinationaddress'].disabled = true;
}else{
 document.forms['partnerAddForm'].elements['billingaddress'].disabled = false;
 document.forms['partnerAddForm'].elements['originaddress'].disabled = false;
 document.forms['partnerAddForm'].elements['destinationaddress'].disabled = false;
}
}
</script>
<script type="text/javascript">
function validateForm(){	
	var billAdd = document.forms['partnerAddForm'].elements['partner.billingAddress1'].value.trim();
	var billPhone = document.forms['partnerAddForm'].elements['partner.billingPhone'].value.trim();
	var billCity = document.forms['partnerAddForm'].elements['partner.billingCity'].value.trim();
	var billCountry = document.forms['partnerAddForm'].elements['partner.billingCountry'].value;
	var billState = document.forms['partnerAddForm'].elements['partner.billingState'].value;
    var enbState = '${enbState}';
	var index = (enbState.indexOf(billCountry)> -1);
	  	
	if(billAdd==''){
		alert('Please enter the billing address.');
		return false;		
	}else if(billCountry==''){
		alert('Please select the billing country.');
		return false;
	}else if((billCountry == 'United States' || billCountry == 'Canada' || billCountry == 'India') && index != '' && billState==''){
		alert('Please select the billing State.');
		return false;		
	}else if(billCity==''){
		alert('Please enter the billing city.');
		return false;
	}else if(billPhone==''){
		alert('Please enter the billing phone.');
		return false;
	}else{
		return true;
	}
}
function requiredState(){
    var billCountry = document.forms['partnerAddForm'].elements['partner.billingCountry'].value; 
	var r2=document.getElementById('redHiddenFalse');
	var r1=document.getElementById('redHidden');
	if(billCountry == 'United States' || billCountry == 'Canada' || billCountry == 'India'){	
	    var enbState = '${enbState}';
		var index = (enbState.indexOf(billCountry)> -1);
		if(index != ''){
		r1.style.display = 'block';
		r2.style.display = 'none';
		}else{	
			r1.style.display = 'none';
			r2.style.display = 'block';		
		}
	}else{	
		r1.style.display = 'none';
		r2.style.display = 'block';		
	}
}
function requiredStateDest(){
	var destAddress=document.forms['partnerAddForm'].elements['destAddress'].value;
    destAddress = destAddress.replace('%22',"\"");
	var destArray=destAddress.split('~')
	var destCountry=destArray[3];
	var r2=document.getElementById('redHiddenFalse');
	var r1=document.getElementById('redHidden');
	if(destCountry == 'United States' || destCountry == 'Canada' || destCountry == 'India'){	
	    var enbState = '${enbState}';
		var index = (enbState.indexOf(destCountry)> -1);
		if(index != ''){
		r1.style.display = 'block';
		r2.style.display = 'none';
		}else{	
			r1.style.display = 'none';
			r2.style.display = 'block';		
		}
	}else{	
		r1.style.display = 'none';
		r2.style.display = 'block';		
	}
}
function requiredStateOrigin(){
	var orgAddress=document.forms['partnerAddForm'].elements['orgAddress'].value;
    orgAddress = orgAddress.replace('%22',"\"");
	var orgArray=orgAddress.split('~');
	var orgCountry=orgArray[3];
	var r2=document.getElementById('redHiddenFalse');
	var r1=document.getElementById('redHidden');
	if(orgCountry == 'United States' || orgCountry == 'Canada' || orgCountry == 'India'){	
	    var enbState = '${enbState}';
		var index = (enbState.indexOf(orgCountry)> -1);
		if(index != ''){
		r1.style.display = 'block';
		r2.style.display = 'none';
		}else{	
			r1.style.display = 'none';
			r2.style.display = 'block';		
		}
	}else{	
		r1.style.display = 'none';
		r2.style.display = 'block';		
	}
}
function autoCompleterAjaxBillingCity(){
	var countryName = document.forms['partnerAddForm'].elements['partner.billingCountry'].value;
	var stateNameOri = document.forms['partnerAddForm'].elements['partner.billingState'].value;
	var cityNameOri = document.forms['partnerAddForm'].elements['partner.billingCity'].value;
	var countryNameOri = "";
	<c:forEach var="entry" items="${countryCod}">
	if(countryName=="${entry.value}"){
		countryNameOri="${entry.key}";
	}
	</c:forEach>
	if(cityNameOri!=''){
		var data = 'leadCaptureAutocompleteOriAjax.html?ajax=1&stateNameOri='+stateNameOri+'&countryNameOri='+countryNameOri+'&cityNameOri='+cityNameOri+'&decorator=simple&popup=true';
		$("#billingCity").autocomplete({				 
		      source: data		      
		    });
	}
}
function autoCompleterAjaxMailingCity(){
	var countryName = document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
	var stateNameDes = document.forms['partnerAddForm'].elements['partner.mailingState'].value;
	var cityNameDes = document.forms['partnerAddForm'].elements['partner.mailingCity'].value;		
	var countryNameDes = "";
	<c:forEach var="entry" items="${countryCod}">
	if(countryName=="${entry.value}"){
		countryNameDes="${entry.key}";
	}
	</c:forEach>
	if(cityNameDes!=''){
	var data = 'leadCaptureAutocompleteDestAjax.html?ajax=1&stateNameDes='+stateNameDes+'&countryNameDes='+countryNameDes+'&cityNameDes='+cityNameDes+'&decorator=simple&popup=true';
	$( "#mailingCity" ).autocomplete({				 
	      source: data		      
	    });
	}
}
</script>
</head> 

<s:form id="partnerAddForm" name="partnerAddForm" onsubmit="return validateForm();" action="savePopUp.html?decorator=popup&popup=true" method="post" validate="true">   
<s:hidden name="partner.id" />
<s:hidden name="partner.corpID" />
<s:hidden name="partner.billingCountryCode"/>
<s:hidden name="partner.status" value="Approved" />
<s:hidden id="dateFormat" name="dateFormat" />
<s:hidden id="formClose" name="formClose" />
<s:hidden name="formStatus" value=""/>
<s:hidden name="partner.partnerPortalActive" />
<s:hidden name="partner.associatedAgents" />
<s:hidden name="partner.viewChild" />
<s:hidden name="hidAddress" />
<s:hidden name="CAddress" />
<s:hidden name="partner.aliasName" />

<c:if test="${flag != '8' && flag != '7' && flag != '6'}">
	<s:hidden key="partner.isPrivateParty" value="true"/>
	<s:hidden name="partner.isAccount"  value="false"/>
</c:if>

<c:if test="${flag == '8' || flag == '7' || flag == '6'}">
	<c:if test="${partnerType == 'PP'}">
		<s:hidden key="partner.isPrivateParty" value="true"/>
		<s:hidden name="partner.isAccount"  value="false"/>
	</c:if>
	<c:if test="${partnerType == 'AC'}">
		<s:hidden key="partner.isPrivateParty" value="false"/>
		<s:hidden name="partner.isAccount"  value="true"/>
	</c:if>
</c:if>

<s:hidden name="partner.isAgent"  value="false"/>
<s:hidden name="partner.isVendor" value="false"/>
<s:hidden name="partner.isCarrier" value="false"/>
<s:hidden name="partner.isOwnerOp" value="false"/>

<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<s:hidden name="disable" value="<%=request.getParameter("disable") %>"/>
<c:set var="disable" value="<%=request.getParameter("disable") %>"/>

<s:hidden name="firstName" value="<%=request.getParameter("firstName") %>"/>
<c:set var="firstName" value="<%=request.getParameter("firstName") %>" />

<s:hidden name="lastName" value="<%=request.getParameter("lastName") %>"/>
<c:set var="lastName" value="<%=request.getParameter("lastName") %>"/>

<s:hidden name="mode" value="<%=request.getParameter("mode") %>"/>
<c:set var="mode" value="<%=request.getParameter("mode") %>"/>

<s:hidden name="flag" value="<%=request.getParameter("flag") %>"/>
<c:set var="flag" value="<%=request.getParameter("flag") %>"/>

<s:hidden name="compDiv" value="<%=request.getParameter("compDiv") %>"/>
<c:set var="compDiv" value="<%=request.getParameter("compDiv") %>"/>

<s:hidden name="phone" value="<%=request.getParameter("phone") %>"/>
<c:set var="phone" value="<%=request.getParameter("phone") %>"/>

<s:hidden name="email" value="<%=request.getParameter("email") %>"/>
<c:set var="email" value="<%=request.getParameter("email") %>"/>

<s:hidden name="orgAddress" value="<%=request.getParameter("orgAddress") %>"/>
<c:set var="orgAddress" value="<%=request.getParameter("orgAddress") %>"/>

<s:hidden name="destAddress" value="<%=request.getParameter("destAddress") %>"/>
<c:set var="destAddress" value="<%=request.getParameter("destAddress") %>"/>

<s:hidden name="orgAdd" value="<%=request.getParameter("orgAddress") %>"/>
<c:set var="orgAdd" value="<%=request.getParameter("orgAddress") %>"/>

<s:hidden name="destAdd" value="<%=request.getParameter("destAddress") %>"/>
<c:set var="destAdd" value="<%=request.getParameter("destAddress") %>"/>


<s:hidden name="popupval" value="<%=request.getParameter("popup")%>"/>
<c:if test="${not empty partner.id}">
<div id="newmnav">
		<ul>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Partner Detail</span></a></li>
			
			<li><a href="partnersPopup.html?partnerType=PP&firstName=${firstName}&lastName=${lastName}&phone=${phone}&email=${email}&compDiv=${compDiv}&flag=${flag}&type=TT&orgAddress=${orgAddress}&destAddress=${destAddress}&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Private Party List</span></a></li>
		</ul>
</div>
<div class="spn" style="width: 750px;">&nbsp;</div>

</c:if>
<c:if test="${empty partner.id}">
<div id="newmnav">
		<ul>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Partner Detail</span></a></li>
			<c:if test="${flag == '8' || flag == '7'}">
				<li><a href="destinationPartnersVanline.html?destination=${destination}&partnerType=AG&flag=${flag}&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Partner List</span></a></li>
			</c:if>
			<c:if test="${flag == '6'}">
				<li><a href="originPartnersVanline.html?origin=${origin}&partnerType=AG&flag=${flag}&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Partner List</span></a></li>
			</c:if>
			<c:if test="${flag != '8' && flag != '7' && flag != '6'}">
			<li><a href="partnersPopup.html?partnerType=PP&firstName=${firstName}&lastName=${lastName}&phone=${phone}&email=${email}&compDiv=${compDiv}&orgAddress=${orgAdd}&destAddress=${destAdd}&flag=${flag}&type=TT&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Private Party List</span></a></li>
			</c:if>
		</ul>
</div>
<div class="spn" style="width: 750px;">&nbsp;</div>

</c:if>

<div id="Layer1" >
<table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0" style="width: 750px;">
	<tbody>
		
			<tr>
			<td colspan="10">
				<table cellspacing="2" cellpadding="2" border="0" class="detailTabLabel">
							<tbody>
								<tr>
									<td width="12px"></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerPrefix'/></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.firstName'/></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.lastName'/><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode'/><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext">Company Division</td>
									<td align="left" class="listwhitetext"><fmt:message key='accountInfo.creditTerms' /></td>
								</tr>
								<tr>
									<td width="12px"></td>
									<td><s:select cssClass="list-menu" name="partnerPrefix" list="%{prefixList}" cssStyle="width:50px" headerKey="" headerValue=""/></td>
									<c:if test="${mode =='view'}">
										<td> <s:textfield name="partner.firstName" required="true" cssClass="input-text" cssStyle="width:130px" maxlength="25" onkeydown="return onlyCharsAllowed(event)"/> </td>
									</c:if>
									<c:if test="${mode !='view'}">
										<td> <s:textfield name="partner.firstName" required="true" cssClass="input-text" cssStyle="width:130px" maxlength="25" value="${firstName}" onkeydown="return onlyCharsAllowed(event)"/> </td>
									</c:if>
									<c:if test="${mode =='view'}">
										<td> <s:textfield name="partner.lastName" required="true" cssClass="input-text" cssStyle="width:130px"maxlength="40" onchange="noQuote(this);" onkeydown="return onlyCharsAllowed(event)"/> </td>
									</c:if>
									<c:if test="${mode !='view'}">
										<td><s:textfield key="partner.lastName" required="true" cssClass="input-text"  cssStyle="width:130px"maxlength="40" value="${lastName}" onchange="noQuote(this);" onkeydown="return onlyCharsAllowed(event)"/> </td>
									</c:if>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerCode" required="true" cssClass="input-textUpper" size="15"  maxlength="8" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" tabindex="1"/> </td>
									<c:if test="${mode =='view'}">
										<td> <s:textfield name="partner.companyDivision" cssClass="input-textUpper" readonly="true"/> </td>
									</c:if>
									<c:if test="${mode !='view'}">
										<td> <s:textfield name="partner.companyDivision" cssClass="input-textUpper" value="${compDiv}" readonly="true"/> </td>
									</c:if>
									<td><s:select cssClass="list-menu" name="creditTerm" list="%{creditTerms}" cssStyle="width:100px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
								</tr>
							</tbody>
						</table>
								
								
						<table cellspacing="3" cellpadding="2" border="0">
						<tbody>
						
							<tr><td width="10px"></td><td class="listwhitetext"><b>Billing Address</b><font color="red" size="2">*</font></td></tr>
							<tr><td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress1"  size="50" maxlength="30" tabindex="4" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingCountryCode'/><font color="red" size="2">*</font></td>
								<td ><s:select cssClass="list-menu" name="partner.billingCountry" list="%{countryDesc}" cssStyle="width:162px"  headerKey="" headerValue="" onchange="requiredState();changeStatus();getBillingCountryCode(this);autoPopulate_partner_billingCountry(this);getBillingState(this); enableStateListBill();" tabindex="8" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingFax'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingFax" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="12" /></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress2" size="50" maxlength="30" tabindex="5"/></td>
								<td align="right" id="redHiddenFalse" class="listwhitetext"><fmt:message key='partner.billingState' /></td>
								<td align="right" id="redHidden" class="listwhitetext"><fmt:message key='partner.billingState' /><font  color="red" size="2">*</font></td>
								<td><s:select id="partnerAddForm_partner_billingState" name="partner.billingState" cssClass="list-menu" list="%{bstates}" cssStyle="width:162px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="9" /></td>
								
								
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingPhone'/><font color="red" size="2">*</font></td>
								<c:if test="${mode =='view'}">
									<td> <s:textfield name="partner.billingPhone" required="true" cssClass="input-text" size="22" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event)"/> </td>
								</c:if>
								<c:if test="${mode !='view'}">
									<td> <s:textfield name="partner.billingPhone" required="true" cssClass="input-text" size="22" maxlength="20" value="${phone}" onkeydown="return onlyPhoneNumsAllowed(event)"/> </td>
								</c:if>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress3" size="50" maxlength="30" tabindex="6" /></td>
								
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingCity'/><font color="red" size="2">*</font></td>
								<td><s:textfield cssClass="input-text" name="partner.billingCity" id="billingCity" cssStyle="width:160px" maxlength="20" onkeydown="return onlyCharsAllowed(event)" onkeyup="autoCompleterAjaxBillingCity();" tabindex="10" /></td>
															
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingEmail'/></td>
								<c:if test="${mode =='view'}">
									<td> <s:textfield name="partner.billingEmail" required="true" cssClass="input-text" size="22" maxlength="65" tabindex="15"/> </td>
								</c:if>
								<c:if test="${mode !='view'}">
									<td> <s:textfield name="partner.billingEmail" required="true" cssClass="input-text" size="22" maxlength="65" value="${email}" tabindex="15"/> </td>
								</c:if>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress4" size="50" maxlength="30" tabindex="7" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingZip'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingZip" cssStyle="width:160px"  maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="11" /></td>
								
							</tr>
						</tbody>
					</table>
					
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tbody>
							<tr><td align="center" colspan="10" class="vertlinedata"></tr>
								<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
							</tbody>
						</table>
					<table cellspacing="3" cellpadding="2" border="0" class="detailTabLabel">
						<tbody>
								<tr><td width="10px"></td>
								<td class="listwhitetext"><b>Mailing Address</b></td></tr>
								<tr><td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress1"  size="50" maxlength="30"  tabindex="28" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCountryCode'/></td>
									<td><s:select cssClass="list-menu" name="partner.mailingCountry" list="%{countryDesc}" cssStyle="width:162px"  headerKey="" headerValue="" onchange="changeStatus();getMailingCountryCode(this);autoPopulate_partner_mailingCountry(this);getMailingState(this); enableStateListMail();"  tabindex="32"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingFax'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingFax" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="36" /></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress2" size="50" maxlength="30"  tabindex="29" /></td>
									
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingState'/></td>
									<td><s:select id="partnerAddForm_partner_mailingState"  name="partner.mailingState" cssClass="list-menu"list="%{mstates}" cssStyle="width:162px"  headerKey="" headerValue="" onchange="changeStatus();"  tabindex="33" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingPhone'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingPhone" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)"  tabindex="37"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress3" size="50" maxlength="30"  tabindex="30" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCity'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingCity" id="mailingCity" cssStyle="width:160px" maxlength="15" onkeydown="return onlyCharsAllowed(event)" onkeyup="autoCompleterAjaxMailingCity();"  tabindex="34" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingEmail'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingEmail"  size="22" maxlength="65" tabindex="39" /></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress4" size="50" maxlength="30" tabindex="31" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingZip'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingZip" cssStyle="width:160px"  maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)"  tabindex="35" /></td>
									<td align="right" class="listwhitetext" width="">ID#</td>
								   <td><s:textfield cssClass="input-text" name="partner.OMNINumber" size="18" maxlength="10"/></td>
									
								</tr>
								<tr>
								<td width="10px"></td>
								<td align="right"><input type="button" class="cssbutton" style="width:170px; height:25px" name="billingaddress" value="Copy from billing address" onClick="copyBillToMail();changeStatus();" /></td>
								<td width="10px" colspan="2"></td>
								<td align="right"><input type="button" class="cssbutton" style="width:170px; height:25px" name="originaddress" value="Copy from origin address" onClick="copyBillToOriginAdd(); changeStatus();" /></td>
								<td width="10px" ></td>
								<td align="right"><input type="button" class="cssbutton" style="width:185px; height:25px" name="destinationaddress" value="Copy from destination address" onClick="copyBillToDestinationAdd();changeStatus();" /></td>
								<tr>								
						</tbody>
					</table>
			</td>
		</tr>
<tr>
</tr>

<tr>
	<td height="10" align="left" class="listwhitetext" colspan="10"></td>
</tr>
<tr>
<td colspan="10" align="center">
</td>
</tr>
</tbody>
</table>
<table width="760">
		<tbody>
		
					<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:130px">
								<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${partner.createdOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="partner.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
								<fmt:formatDate value="${partner.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty partner.id}">
								<s:hidden name="partner.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{partner.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="partner.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${partner.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="partner.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:130px"><fmt:formatDate value="${partner.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty partner.id}">
								<s:hidden name="partner.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{partner.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="partner.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
		</tbody>
</table>  
<c:if test="${empty partner.id}">
<c:set var="button1">
    <s:submit cssClass="cssbutton" key="button.save" cssStyle="width:60px; height:25px " onclick="return userauth('this');changeStatus()"/>
   <%--  <input type="button" class="cssbutton" value="Reset" onclick="clear_fields()" cssStyle="width:60px; height:25px "/>--%>
    <s:reset cssClass="cssbutton" key="Reset" onclick="reset_partner_billingCountry();reset_partner_mailingCountry();"  cssStyle="width:60px; height:25px " /> 
</c:set>
</c:if>
<c:if test="${not empty partner.id}">
	<c:if test="${partnerType == 'PP'}">
		<c:set var="button1">
			<s:submit cssClass="cssbutton" key="button.save" cssStyle="width:60px; height:25px " onclick="return userauth('this');changeStatus()"/>
		   <%--  <input type="button" class="cssbutton" value="Reset" onclick="clear_fields()" cssStyle="width:60px; height:25px "/> --%> 
		   <s:reset cssClass="cssbutton" key="Reset" onclick="reset_partner_billingCountry();reset_partner_mailingCountry();"  cssStyle="width:60px; height:25px " />
		</c:set>
	</c:if>	
</c:if>
    
</div>
<c:out value="${button1}" escapeXml="false" /> 
<c:set var="isTrue" value="false" scope="session"/>

</s:form>

<script type="text/javascript">
function userauth(){
	try{
	if(document.forms['partnerAddForm'].elements['partner.lastName'].value == ''){
		alert("Last Name of Customer is required field");
		return false;
	}
	if(document.forms['partnerAddForm'].elements['partner.billingEmail'].value !=''){
		var billingEmail=document.forms['partnerAddForm'].elements['partner.billingEmail'];
		if (emailcheck(billingEmail.value) == false){
			billingEmail.value="";
			billingEmail.focus();
			return false;
		}
	}
	if(document.forms['partnerAddForm'].elements['partner.mailingEmail'].value !=''){
		var mailingEmail=document.forms['partnerAddForm'].elements['partner.mailingEmail'];
		if (emailcheck(mailingEmail.value) == false){
			mailingEmail.value="";
			mailingEmail.focus();
			return false;
		}
	}
}
catch(e){}
}

function emailcheck(str) {
	try{
		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		str=str.trim();
		if (str.indexOf(at)==-1){
		   alert("Please enter a valid email address")
		   return false;
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert("Please enter a valid email address")
		   return false;
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("Please enter a valid email address")
		    return false;
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    alert("Please enter a valid email address")
		    return false;
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Please enter a valid email address")
		    return false;
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    alert("Please enter a valid email address")
		    return false;
		 }
		
		 if (str.indexOf(" ")!=-1){
		    alert("Please enter a valid email address")
		    return false;
		 }

 		 return true;					
}
catch(e){}
}
</script>
<script type="text/javascript">  
	//getBillingState(document.forms['partnerAddForm'].elements['partner.billingCountryCode']);
</script>
<script type="text/javascript">
	try{
	autoPopulate_partner_billingCountry(document.forms['partnerAddForm'].elements['partner.billingCountry']);
	}
	catch(e){}
	try{
	getBillingState(document.forms['partnerAddForm'].elements['partner.billingCountry']); 
	}
	catch(e){}
	try{
	autoPopulate_partner_mailingCountry(document.forms['partnerAddForm'].elements['partner.mailingCountry']);        
	}
	catch(e){}
	try{
	getMailingState(document.forms['partnerAddForm'].elements['partner.mailingCountry']);
    }
    catch(e){}
 </script> 
<script type="text/javascript">   
	Form.focusFirstElement($("partnerAddForm"));   
</script>  

<script> 
//try{
<c:if test="${not empty partner.id}">
	var billCon=document.forms['partnerAddForm'].elements['partner.billingCountry'].value;
	var enbState = '${enbState}';
	var index = (enbState.indexOf(billCon)> -1);
	if(index != ''){
			document.forms['partnerAddForm'].elements['partner.billingState'].disabled = false;
	}else{
			document.forms['partnerAddForm'].elements['partner.billingState'].disabled = true;
	}

	var mailCon=document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(mailCon)> -1);
	
	if(index != ''){
			document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = false;
	}else{
			document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = true;
	}
	
	<c:if test="${partnerType != 'PP'}">
		disabledAll();
	</c:if>
	<c:if test="${not empty partner.id && formClose =='close'}">
		var swapPartnerCode=encodeURIComponent("${partner.partnerCode}");
		var swapLastName=encodeURIComponent("${partner.lastName}");
	
		<c:if test="${flag==3}">
		try{
			opener.document.forms['customerFileForm'].elements['customerFile.billToCode'].value=swapPartnerCode;
			opener.document.forms['customerFileForm'].elements['customerFile.billToName'].value=swapLastName;
		}catch(e){}
		</c:if>
		<c:if test="${flag==0}">
			opener.document.forms['billingForm'].elements['billing.billToCode'].value=swapPartnerCode;
			opener.document.forms['billingForm'].elements['billing.billToName'].value=swapLastName;
		</c:if>
		<c:if test="${flag==1}">
			opener.document.forms['billingForm'].elements['billing.billTo2Code'].value=swapPartnerCode;
			opener.document.forms['billingForm'].elements['billing.billTo2Name'].value=swapLastName;
		</c:if>
		<c:if test="${flag==2}">
			opener.document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value=swapPartnerCode;
			opener.document.forms['billingForm'].elements['billing.privatePartyBillingName'].value=swapLastName;
		</c:if>
		<c:if test="${flag==4}">
			opener.document.forms['customerFileForm'].elements['customerFile.accountCode'].value=swapPartnerCode;
			opener.document.forms['customerFileForm'].elements['customerFile.accountName'].value=swapLastName;
		</c:if>
		<c:if test="${flag==5}">
			opener.document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].value=swapPartnerCode;
			opener.document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToName'].value=swapLastName;
		</c:if>
		<c:if test="${flag==6}">
			opener.document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value=swapPartnerCode;
			opener.document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value=swapLastName;
		</c:if>
		<c:if test="${flag==7}">
			opener.document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value=swapPartnerCode;
			opener.document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value=swapLastName;
		</c:if>
		<c:if test="${flag==8}">
			opener.document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentCode'].value=swapPartnerCode;
			opener.document.forms['miscellaneousForm'].elements['miscellaneous.haulerName'].value=swapLastName;
		</c:if>		
		<c:if test="${flag==3}">
			try{
				opener.document.forms['billingForm'].elements['billing.billToCode'].value=swapPartnerCode;
				opener.document.forms['billingForm'].elements['billing.billToName'].value=swapLastName;
			}catch(e){}
		</c:if>	
	self.close();	
	</c:if>
</c:if>
//}
//catch(e){}
	
//var formObject = self.opener.document.forms[0];
//formObject.elements['<c:out value="${param.fld_code}"/>'].value = '<c:out value="${partner.partnerCode}"/>';
//formObject.elements['<c:out value="${param.fld_description}"/>'].value = '<c:out value="${param.listDescription}"/>';
//formObject.elements['<c:out value="${param.fld_secondDescription}"/>'].value = '<c:out value="${param.listSecondDescription}"/>';
//formObject.elements['<c:out value="${param.fld_thirdDescription}"/>'].value = '<c:out value="${param.listThirdDescription}"/>';
//formObject.elements['<c:out value="${param.fld_fourthDescription}"/>'].value = '<c:out value="${param.listFourthDescription}"/>';
//formObject.elements['<c:out value="${param.fld_fifthDescription}"/>'].value = '<c:out value="${param.listFifthDescription}"/>';
//formObject.elements['<c:out value="${param.fld_sixthDescription}"/>'].value = '<c:out value="${param.listSixthDescription}"/>';

</script>
<script type="text/javascript">  
	try{
	autoPopulate_partner_mailingCountry(document.forms['partnerAddForm'].elements['partner.mailingCountry']);        
	}
	catch(e){}
	try{
		 var enbState = '${enbState}';
		 var mailCon=document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
		 if(mailCon=="")	 {
				document.getElementById('partnerAddForm_partner_mailingState').disabled = true;
				document.getElementById('partnerAddForm_partner_mailingState').value ="";
		 }else{
		  		if(enbState.indexOf(mailCon)> -1){
					document.getElementById('partnerAddForm_partner_mailingState').disabled = false; 
					document.getElementById('partnerAddForm_partner_mailingState').value='${partner.mailingState}';
				}else{
					document.getElementById('partnerAddForm_partner_mailingState').disabled = true;
					document.getElementById('partnerAddForm_partner_mailingState').value ="";
				} 
			}
		 }
	 catch(e){}	
	 
	 function enableStateListMail(){ 
			var mailCon=document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
			  var enbState = '${enbState}';
			  var index = (enbState.indexOf(mailCon)> -1);
			  if(index != ''){
			  		document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = false;
			  }else{
				  document.forms['partnerAddForm'].elements['partner.mailingState'].disabled = true;
			  }	        
		}
	 
	 </script>
<script type="text/javascript">
	try{
	requiredState();
	autoPopulate_partner_billingCountry(document.forms['partnerAddForm'].elements['partner.billingCountry']);
	}
	catch(e){}

	setTimeout("setBillingState()",500);
	function setBillingState(){
	try{
		 var enbState = '${enbState}';
		 var billCon=document.forms['partnerAddForm'].elements['partner.billingCountry'].value;
		 if(billCon=="")	 {
			 document.forms['partnerAddForm'].elements['partner.billingState'].disabled = true;
			 document.forms['partnerAddForm'].elements['partner.billingState'].value ="";
		 }else{
		  		if(enbState.indexOf(billCon)> -1){
		  			document.forms['partnerAddForm'].elements['partner.billingState'].disabled = false; 
		  			document.forms['partnerAddForm'].elements['partner.billingState'].value='${partner.billingState}';
				}else{
					document.forms['partnerAddForm'].elements['partner.billingState'].disabled = true;
					document.forms['partnerAddForm'].elements['partner.billingState'] ="";
				} 
			}
		 }
	 catch(e){}
	}
	 function enableStateListBill(){ 
			var billCon=document.forms['partnerAddForm'].elements['partner.billingCountry'].value;
			  var enbState = '${enbState}';
			  var index = (enbState.indexOf(billCon)> -1);
			  if(index != ''){
			  		document.forms['partnerAddForm'].elements['partner.billingState'].disabled = false;
			  }else{
				  document.forms['partnerAddForm'].elements['partner.billingState'].disabled = true;
			  }	        
		}
 </script>
<script type="text/javascript">
try{
	buttondisabled();
}catch(e){alert(e)}
</script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>