package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.AgentBaseDao;
import com.trilasoft.app.model.AgentBase;
import com.trilasoft.app.service.AgentBaseManager;

public class AgentBaseManagerImpl extends GenericManagerImpl<AgentBase, Long> implements AgentBaseManager {
	AgentBaseDao agentBaseDao;

	public AgentBaseManagerImpl(AgentBaseDao agentBaseDao) {
		super(agentBaseDao);
		this.agentBaseDao = agentBaseDao;
	}

	public List getbaseListByPartnerCode(String partnerCode) {
		return agentBaseDao.getbaseListByPartnerCode(partnerCode);
	}

	public List isExisted(String baseCode, String sessionCorpID, String partnerCode) {
		return agentBaseDao.isExisted(baseCode, sessionCorpID, partnerCode);
	}

	public void deleteBaseSCAC(String partnerCode, String baseCode, String sessionCorpID) {
		agentBaseDao.deleteBaseSCAC(partnerCode, baseCode, sessionCorpID);
	}
}
