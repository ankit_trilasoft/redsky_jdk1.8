<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>

<title>Order Initiation Detail</title>
<meta name="heading"
	content="Order Initiation Detail" />

<style type="text/css">	
legend {
font-family:arial,verdana,sans-serif;
font-size:11px;
font-weight:bold;
margin:0;
}
.listgreytext {
	font-family: arial,verdana;
	font-size: 11px;
	color: #484848;
	text-decoration: none;
	background-color: none;
	font-weight: normal;
}
.OverFlowPr {
	overflow-x:auto;
	overflow-y:auto;
	!overflow-x:scroll;
	!overflow-y:visible;			
}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
    <script language="javascript" type="text/javascript" src="/common/formCalender.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	<script type="text/javascript" src="scripts/prototype.js"></script>
	<script type="text/javascript" src="scripts/effects.js"></script>
	<script type="text/javascript" src="scripts/scriptaculous.js?load=effects"></script>
	<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
	<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
window.onload = function() { 
var fieldName = document.forms['customerFileForm'].elements['field'].value;
var fieldName1 = document.forms['customerFileForm'].elements['field1'].value;
	if(fieldName!=''){
		document.forms['customerFileForm'].elements[fieldName].className = 'rules-textUpper';
	}
	if(fieldName1!=''){
		document.forms['customerFileForm'].elements[fieldName1].className = 'rules-textUpper';
	}
}  

function focusDate(target){
	document.forms['customerFileForm'].elements[target].focus();
}
function EmailPortDate(){
	var EmailPort = document.forms['customerFileForm'].elements['customerFile.email'].value;
	document.forms['customerFileForm'].elements['customerFileemail'].value = EmailPort;
	if(document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value==''){
		document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value=EmailPort;
	}	
}

</script>
<script language="JavaScript" type="text/javascript">
function myDate() {
	var HH2;
	var HH1;
	var MM2;
	var MM1;
	var tim1=document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
	var tim2=document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
		tim1=tim1.replace(":","");
		tim2=tim2.replace(":","");
	if((tim1.substring(0,tim1.length-2)).length == 1){
		HH1 = '0'+tim1.substring(0,tim1.length-2);
	}else {
		HH1 = tim1.substring(0,tim1.length-2);
	}
	MM1 = tim1.substring(tim1.length-2,tim1.length);
	if((tim2.substring(0,tim1.length-2)).length == 1){
		HH2 = '0'+tim2.substring(0,tim2.length-2);
	}else {
		HH2 = tim2.substring(0,tim2.length-2);
	}
	MM2 = tim2.substring(tim2.length-2,tim2.length);
	document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = HH1 + ':' + MM1;
	document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = HH2 + ':' + MM2;

	var f = document.getElementById('customerFileForm'); 
	f.setAttribute("autocomplete", "off"); 
} 
 

var digits = "0123456789";
var phoneNumberDelimiters = "()- ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;

function autoPopulate_customerFile_originCountry(targetElement) {		
		var oriCountry = targetElement.value;
		var oriCountryCode = '';
		if(oriCountry == 'United States')
		{
			oriCountryCode = "USA";
		}
		if(oriCountry == 'India')
		{
			oriCountryCode = "IND";
		}
		if(oriCountry == 'Canada')
		{
			oriCountryCode = "CAN";
		}
		if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
			document.getElementById('originStateRequired').style.display = 'block';
		    document.getElementById('originStateNotRequired').style.display = 'none';
			if(oriCountry == 'United States'){
			document.forms['customerFileForm'].elements['customerFile.originZip'].focus();
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.originState'].value = '';
			document.getElementById('originStateRequired').style.display = 'none';
		    document.getElementById('originStateNotRequired').style.display = 'block';
			autoPopulate_customerFile_originCityCode(oriCountryCode,'special');
		}
}
	function autoPopulate_customerFile_destinationCountry(targetElement) {   
	
		var dCountry = targetElement.value;
		var dCountryCode = '';
		if(dCountry == 'United States')
		{
			dCountryCode = "USA";
		}
		if(dCountry == 'India')
		{
			dCountryCode = "IND";
		}
		if(dCountry == 'Canada')
		{
			dCountryCode = "CAN";
		}
		if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;
			document.getElementById('destinationStateRequired').style.display = 'block';
		    document.getElementById('destinationStateNotRequired').style.display = 'none';
			if(dCountry == 'United States'){
			document.forms['customerFileForm'].elements['customerFile.destinationZip'].focus(); 
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.destinationState'].value = '';
			document.getElementById('destinationStateRequired').style.display = 'none';
		    document.getElementById('destinationStateNotRequired').style.display = 'block';
			autoPopulate_customerFile_destinationCityCode(dCountryCode,'special');
		}
	}
	
		function autoPopulate_customerFile_homeCountry(targetElement) {   
	
		var dCountry = targetElement.value;
		var dCountryCode = '';
		if(dCountry == 'United States')
		{
			dCountryCode = "USA";
		}
		if(dCountry == 'India')
		{
			dCountryCode = "IND";
		}
		if(dCountry == 'Canada')
		{
			dCountryCode = "CAN";
		}
		if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = false;
			//document.getElementById('destinationStateRequired').style.display = 'block';
		    //document.getElementById('destinationStateNotRequired').style.display = 'none';
			if(dCountry == 'United States'){
			//document.forms['customerFileForm'].elements['customerFile.destinationZip'].focus(); 
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.homeState'].value = '';
			//document.getElementById('destinationStateRequired').style.display = 'none';
		    //document.getElementById('destinationStateNotRequired').style.display = 'block';
			//autoPopulate_customerFile_destinationCityCode(dCountryCode,'special');
		}
	}
	
	function autoPopulate_customerFile_destinationCityCode(targetElement, w) {
	var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value != ''){
			if(document.forms['customerFileForm'].elements['customerFile.destinationState'].value == ''){
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
			}else{
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value+','+document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
			}
		}
	}
	function autoPopulate_customerFile_originCityCode(targetElement, w) {
		var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};

		if(document.forms['customerFileForm'].elements['customerFile.originCity'].value != '')
		{
			if(document.forms['customerFileForm'].elements['customerFile.originState'].value != '')
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value+','+document.forms['customerFileForm'].elements['customerFile.originState'].value;
				
			}
			else
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
				
			}
		}
	} 
	
	function openHomeCountryLocation(){
	    var city = document.forms['customerFileForm'].elements['customerFile.homeCity'].value;
        var country = document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
        var zip = "";
        var address = "";
        if(country == 'United States')
        {
        	//address = document.forms['customerFileForm'].elements['customerFile.originAddress1'].value+","+document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
        }
        else
        {
        	//address = document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
        }
        address = address.replace("#","-");
        var state = "";
       
 		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
	}
	 
   function openOriginLocation() {
        var city = document.forms['customerFileForm'].elements['customerFile.originCity'].value;
        var country = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
        var zip = document.forms['customerFileForm'].elements['customerFile.originZip'].value;
        var address = "";
        if(country == 'United States')
        {
        	address = document.forms['customerFileForm'].elements['customerFile.originAddress1'].value+","+document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
        }
        else
        {
        	address = document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
        }
        address = address.replace("#","-");
        var state = document.forms['customerFileForm'].elements['customerFile.originState'].value;
       
 		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
	}
	 function openDestinationLocation() {
        var city = document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
        var country = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
        var zip = document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
       	var address = "";
        if(country == 'United States')
        {
        	address = document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value+","+document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
        }
        else
        {
        	address = document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
        }
       	address = address.replace("#","-");
        var state = document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
    	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
	}
	function generatePortalId() {
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked = true ;
		var checkPortalBox = document.forms['customerFileForm'].elements['customerFile.secondaryEmailFlag'].checked;
		var emailID = document.forms['customerFileForm'].elements['customerFile.email'].value;
		if(checkPortalBox==true && emailID !='')
		{
		var emailID2 = document.forms['customerFileForm'].elements['customerFile.email2'].value;
		emailID=emailID+','+emailID2;
		}
		var portalIdName = document.forms['customerFileForm'].elements['customerFile.customerPortalId'].value;
		
		var checkPortalStatus=document.forms['customerFileForm'].elements['customerFile.status'].value;
		if(checkPortalStatus !='CNCL' && checkPortalStatus !='CLOSED'){
		if(emailID!='')
		{
		if(portalIdName!=''){
		window.open('sendMail.html?id=${customerFile.id}&emailTo='+emailID+'&userID='+portalIdName+'&decorator=popup&popup=true&from=file','','width=550,height=170') ;
		}
		else{
		alert("Please create ID for Customer Portal");
		}
		}else
		{
			alert("Please enter primary email");
			document.forms['customerFileForm'].elements['customerFile.email'].focus();
		}
		}
		else{
		alert("You can't Reset password for Customer Portal ID because job is Closed/Cancelled");
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked=false;
		}
	}     

function notExists(){
	alert("The customer information has not been saved yet, please save customer information to continue");
} 

</script>

<SCRIPT LANGUAGE="JavaScript"> 


function findCityStateOfZipCode(targetElement,targetField){
      var zipCode = targetElement.value;
     
      if(targetField=='OZ'){
       var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
        if(document.forms['customerFileForm'].elements['originCountryFlex3Value'].value=='Y'){
         var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
          http33.open("GET", url, true);
          http33.onreadystatechange = function(){ handleHttpResponseCityState('OZ');};
          http33.send(null);
        }else{
        
         }
       }else if(targetField=='DZ'){
        var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
          if( document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y'){
           var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
              http33.open("GET", url, true);
              http33.onreadystatechange = function(){ handleHttpResponseCityState('DZ');};
              http33.send(null);
             }else{
        
             }
      
        }  
      } 

function findCityState(targetElement,targetField){ 
     var zipCode = targetElement.value; 
     if(targetField=='OZ'){ 
     var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
 
     } else if (targetField=='DZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
     }
     if(zipCode!='' && countryCode=='USA'){
     var url="findCityState.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode);
     http33.open("GET", url, true);
     http33.onreadystatechange = function(){ handleHttpResponseCityState(targetField);};
     http33.send(null);
     }  }
function handleHttpResponseCityState(targetField){
             if (http33.readyState == 4){
                var results = http33.responseText
                results = results.trim();
                var resu = results.replace("[",'');
                resu = resu.replace("]",'');
                resu = resu.split(",");
				for(var i = 0; i < resu.length+1; i++) {
                var res = resu[i];
                res = res.split("#");
                if(targetField=='OZ'){
	           	if(res[3]=='P') {	            
	           	 document.forms['customerFileForm'].elements['customerFile.originCity'].value=res[0];
	           	 document.forms['customerFileForm'].elements['customerFile.originState'].value=res[2];
	            if (document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value=='') {
	           	 document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value=(res[4].substring(0,19));
	             } 
	              //document.getElementById('zipCodeList').style.display = 'none';
	             } else if(res[3]!='P') {
	             //document.getElementById('zipCodeList').style.display = 'block';
	           	 } 
	           	 document.forms['customerFileForm'].elements['customerFile.originCity'].focus();
	           	 } 	else if (targetField=='DZ'){
	           	if(res[3]=='P') {
	           	document.forms['customerFileForm'].elements['customerFile.destinationCity'].value=res[0];
	           	 document.forms['customerFileForm'].elements['customerFile.destinationState'].value=res[2];
	            if (document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value=='') {
	           	 document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value=(res[4].substring(0,19));
	           }
	            //document.getElementById('zipDestCodeList').style.display = 'none';
	           	 }	else if(res[3]!='P') {
	           	//document.getElementById('zipDestCodeList').style.display = 'block';
	           	}
	           	document.forms['customerFileForm'].elements['customerFile.destinationCity'].focus();
	           	} } } else { } }
// End Of Method 

function getOriginCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCountryName;
    http4.send(null);
}
function getDestinationCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http44.open("GET", url, true);
    http44.onreadystatechange = httpDestinationCountryName;
    http44.send(null);
}

function getHomeCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http444.open("GET", url, true);
    http444.onreadystatechange = httpHomeCountryName;
    http444.send(null);
}
function httpHomeCountryName(){
     if (http444.readyState == 4){
                var results = http444.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['customerFileForm'].elements['customerFile.homeCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['customerFileForm'].elements['HomeCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['customerFileForm'].elements['HomeCountryFlex3Value'].value='N';
 					}	
 					document.forms['customerFileForm'].elements['customerFile.homeCountry'].select();
				}else{
                     
                 }
             }
}


function httpDestinationCountryName(){
             if (http44.readyState == 4){
                var results = http44.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='N';
 					}	
 					document.forms['customerFileForm'].elements['customerFile.destinationCountry'].select();
				}else{
                     
                 }
             }
        } 


function getState(targetElement, calledOnLoad) {
	var country = targetElement.value;
	var countryCode = "";
	if(country == 'United States'){
		countryCode = "USA";
	}
	if(country == 'India'){
		countryCode = "IND";
	}
	if(country == 'Canada'){
		countryCode = "CAN";
	}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);

     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse5;
     httpState.send(null);
}
	
function getDestinationState(targetElement){

	var country = targetElement.value;

	var countryCode = "";
	if(country == 'United States'){
		countryCode = "USA";
	}
	if(country == 'India'){
		countryCode = "IND";
	}
	if(country == 'Canada'){
		countryCode = "CAN";
	}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse6;
     http3.send(null);
}

function gethomeState(targetElement){

	var country = targetElement.value;

	var countryCode = "";
	if(country == 'United States'){
		countryCode = "USA";
	}
	if(country == 'India'){
		countryCode = "IND";
	}
	if(country == 'Canada'){
		countryCode = "CAN";
	}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponsehomeState;
     http3.send(null);
}
function handleHttpResponsehomeState(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.homeState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[i].value = stateVal[0];
					
					if (document.getElementById("homeState").options[i].value == '${customerFile.homeState}'){
					   document.getElementById("homeState").options[i].defaultSelected = true;
					}
					}
					}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("homeState").value = '';
					}
					else{ document.getElementById("homeState").value == '${customerFile.homeState}';
					   }
        }
}  






String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
} 

function handleHttpResponseCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res=results.split('#');
                if(res.length>=1){
                	document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value = res[0]; 
                	if(res[1]=='Y'){
                	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y';				
				 	}else{
				 	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='N';
				 	}
				 	document.forms['customerFileForm'].elements['customerFile.originCountry'].select();		     	 
 				}else{
                     
                 }
             }
}  
        
 function handleHttpResponse5(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
					
					if (document.getElementById("originState").options[i].value == '${customerFile.originState}'){
					   document.getElementById("originState").options[i].defaultSelected = true;
					}
					}
					}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("originState").value = '';
					}
					else{ document.getElementById("originState").value == '${customerFile.originState}';
					}
             }
        }  
       
function handleHttpResponse6(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${customerFile.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					}
					}
					}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("destinationState").value = '';
					}
					else{ document.getElementById("destinationState").value == '${customerFile.destinationState}';
					   }
        }
} 

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
                  
        
	var http2 = getHTTPObject();
	var http50 = getHTTPObject50();
	var http51 = getHTTPObject51();
    var http3 = getHTTPObject1();
	var http4 = getHTTPObject3();
	var http5 = getHTTPObject();
	var http6 = getHTTPObject2();
    var http8 = getHTTPObject8();
     var http9 = getHTTPObject9();
    var httpState = getHTTPObjectState()
    var http22 = getHTTPObject22();
    var http33 = getHTTPObject33();
    var httpBilltoCode = getHTTPObjectBilltoCode();
    var http44 = getHTTPObject3();
    var http444 = getHTTPObject3();
    
function getHTTPObjectBilltoCode()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getHTTPObject22()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject50()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject51()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject33()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    function getHTTPObject9()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

 var http555 = getHTTPObject555();   
function getHTTPObject555()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    
    

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
  function getHTTPObjectState()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject2(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject3(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    
 function getHTTPObject8(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    
function changeStatus(){
	document.forms['customerFileForm'].elements['formStatus'].value = '1';
}  
     
function sendEmail1(target){
     	var originEmail = target;
   		var subject = 'C/F# ${customerFile.sequenceNumber}';
   		subject = subject+" ${customerFile.firstName}";
   		subject = subject+" ${customerFile.lastName}";
   		
		var mailto_link = 'mailto:'+encodeURI(originEmail)+'?subject='+encodeURI(subject);		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close(); 
} 

function openStandardAddressesPopWindow(){
var jobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
if((document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value!="Accepted" )&&(document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value!="Rejected")&& (document.forms['customerFileForm'].elements['customerFile.orderIntiationStatus'].value!="Submitted" )){ 
javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=customerFile.originMobile&fld_ninthDescription=customerFile.originCity&fld_eigthDescription=customerFile.originFax&fld_seventhDescription=customerFile.originHomePhone&fld_sixthDescription=customerFile.originDayPhone&fld_fifthDescription=customerFile.originZip&fld_fourthDescription=stdAddOriginState&fld_thirdDescription=customerFile.originCountry&fld_secondDescription=customerFile.originAddress3&fld_description=customerFile.originAddress2&fld_code=customerFile.originAddress1');
document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value="originAddSection";
}
}
function openStandardAddressesDestinationPopWindow(){
var jobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
if((document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value!="Accepted" )&&(document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value!="Rejected")&& (document.forms['customerFileForm'].elements['customerFile.orderIntiationStatus'].value!="Submitted" )){ 
javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=customerFile.destinationMobile&fld_ninthDescription=customerFile.destinationCity&fld_eigthDescription=customerFile.destinationFax&fld_seventhDescription=customerFile.destinationHomePhone&fld_sixthDescription=customerFile.destinationDayPhone&fld_fifthDescription=customerFile.destinationZip&fld_fourthDescription=stdAddDestinState&fld_thirdDescription=customerFile.destinationCountry&fld_secondDescription=customerFile.destinationAddress3&fld_description=customerFile.destinationAddress2&fld_code=customerFile.destinationAddress1');
document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value="destinAddSection";
}
}  

var http52 = getHTTPObject52();
	function getHTTPObject52()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http53 = getHTTPObject53();
	function getHTTPObject53()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http54 = getHTTPObject54();
	function getHTTPObject54()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http55 = getHTTPObject55();
	function getHTTPObject55()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var httpRef = getHTTPObjectRef();
function getHTTPObjectRef(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var httpRefSale = getHTTPObjectRefSale();
function getHTTPObjectRefSale(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}




  
var httpJob = getHTTPObjectJob();
function getHTTPObjectJob(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}   


</script>
<script type="text/javascript">
 //animatedcollapse.addDiv('employee', 'fade=1,hide=0,show=1' )
animatedcollapse.addDiv('presentaddress', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('removal', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('family', 'fade=1,hide=0,show=0')
animatedcollapse.addDiv('hr', 'fade=1,hide=0,show=1')


animatedcollapse.init()

function cheackVisaRequired()
{   

      var customerFileStatus = document.forms['customerFileForm'].elements['customerFile.visaRequired'].value; 
      if(customerFileStatus=='Yes' ) { 
        document.getElementById("hidVisaStatusReg").style.display="block";
	    document.getElementById("hidVisaStatusReg1").style.display="block"; 
	 }else{
	    document.getElementById("hidVisaStatusReg").style.display="none";
        document.getElementById("hidVisaStatusReg1").style.display="none";
         
     } 
}

function cheackQuotationStatus(temp)
{     
      var QuotationStatus = temp; 
      if(QuotationStatus=='QR' ) {
      if(document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value=='Submitted'){ 
        document.getElementById("hidQuotationStatus").style.display="block"; 
	 }else{
	    document.getElementById("hidQuotationStatus").style.display="none"; 
	 }
	 }
	 else{
	    document.getElementById("hidQuotationStatus").style.display="none"; 
     } 
     
}
function selectColumn(targetElement) 
   {  
   var rowId=targetElement.value;  
   if(targetElement.checked)
     {
      var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;
      if(userCheckStatus == '')
      {
	  	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = rowId;
      }
      else
      {
       var userCheckStatus=	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus + '#' + rowId;
      document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( '##' , '#' );
      }
    }
   if(targetElement.checked==false)
    {
     var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;
     var userCheckStatus=document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( rowId , '' );
     document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( '##' , '#' );
     } 
    }


function checkColumn(){
var userCheckStatus ="";
  userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value; 
if(userCheckStatus!=""){ 
document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value="";
var column = userCheckStatus.split(",");  
for(i = 0; i<column.length ; i++)
{    
     var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;  
     var userCheckStatus=	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus + '#' +column[i];   
     document.getElementById(column[i]).checked=true;
} 
}
if(userCheckStatus==""){

}
}
 function allReadOnly(temp){ 
     if((document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value=="Accepted" )||(document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value=="Rejected")|| (document.forms['customerFileForm'].elements['customerFile.orderIntiationStatus'].value=="Submitted" )){ 
        var numOfElements = document.forms['customerFileForm'].elements.length;  
        for(var i=0; i<numOfElements;i++)
        {
         var textName = document.forms['customerFileForm'].elements[i].name;  
         document.forms['customerFileForm'].elements[i].disabled=true; 
         }
     }
     var QuotationStatus=temp;  
     if(QuotationStatus=='QR' ) {
      if(document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value=='Submitted'){ 
        document.forms['customerFileForm'].elements['acceptQuote'].disabled=false; 
        document.forms['customerFileForm'].elements['rejectQuote'].disabled=false; 
	 }
	 }
	 document.forms['customerFileForm'].elements['Cancel'].disabled=false; 
	 
    }
  function updateOrderInitiationStatus(temp) { 
     var quotationStatus=temp;
     var id=document.forms['customerFileForm'].elements['customerFile.id'].value;
     window.location.href="updateOrderInitiationHoStatus.html?id="+id+"&quotationStatus="+quotationStatus;
      
 } 
function checkAllRelo(){ 
var len = document.forms['customerFileForm'].elements['checkV'].length;
var check=true;
for (i = 0; i < len; i++){ 
        if(document.forms['customerFileForm'].elements['checkV'][i].checked == false ){
        check=false;
        i=len+1
        }
    } 
    if(check){
    document.forms['customerFileForm'].elements['checkAllRelo'].checked=true;
    }else{
    document.forms['customerFileForm'].elements['checkAllRelo'].checked=false;
    }
}

function checkAll(temp){
    document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = "";
    var len = document.forms['customerFileForm'].elements['checkV'].length;
    if(temp.checked){
    for (i = 0; i < len; i++){

        document.forms['customerFileForm'].elements['checkV'][i].checked = true ;
        selectColumn(document.forms['customerFileForm'].elements['checkV'][i]);
    }
    }
    else{
    for (i = 0; i < len; i++){

        document.forms['customerFileForm'].elements['checkV'][i].checked = false ;
        selectColumn(document.forms['customerFileForm'].elements['checkV'][i]);
    }
    }
}

function submitOrderInitiation(){  
 document.forms['customerFileForm'].action ='submitHoOrderInitiation.html';
 document.forms['customerFileForm'].submit();  
 }  
 function massageFamily(){
 alert("Please Save Order Initiation Detail first before adding Family members.");
 } 
 function onlyNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
function qryHowOld(varAsOfDate, varBirthDate)
   {
   var dtAsOfDate;
   var dtBirth;
   var dtAnniversary;
   var intSpan;
   var intYears;
   var intMonths;
   var intWeeks;
   var intDays;
   var intHours;
   var intMinutes;
   var intSeconds;
   var strHowOld;

   // get born date
   dtBirth = new Date(varBirthDate);
   
   // get as of date
   dtAsOfDate = new Date(varAsOfDate);

   // if as of date is on or after born date
   if ( dtAsOfDate >= dtBirth )
      {

      // get time span between as of time and birth time
      intSpan = ( dtAsOfDate.getUTCHours() * 3600000 +
                  dtAsOfDate.getUTCMinutes() * 60000 +
                  dtAsOfDate.getUTCSeconds() * 1000    ) -
                ( dtBirth.getUTCHours() * 3600000 +
                  dtBirth.getUTCMinutes() * 60000 +
                  dtBirth.getUTCSeconds() * 1000       )

      // start at as of date and look backwards for anniversary 

      // if as of day (date) is after birth day (date) or
      //    as of day (date) is birth day (date) and
      //    as of time is on or after birth time
      if ( dtAsOfDate.getUTCDate() > dtBirth.getUTCDate() ||
           ( dtAsOfDate.getUTCDate() == dtBirth.getUTCDate() && intSpan >= 0 ) )
         {

         // most recent day (date) anniversary is in as of month
         dtAnniversary = 
            new Date( Date.UTC( dtAsOfDate.getUTCFullYear(),
                                dtAsOfDate.getUTCMonth(),
                                dtBirth.getUTCDate(),
                                dtBirth.getUTCHours(),
                                dtBirth.getUTCMinutes(),
                                dtBirth.getUTCSeconds() ) );

         }

      // if as of day (date) is before birth day (date) or
      //    as of day (date) is birth day (date) and
      //    as of time is before birth time
      else
         {

         // most recent day (date) anniversary is in month before as of month
         dtAnniversary = 
            new Date( Date.UTC( dtAsOfDate.getUTCFullYear(),
                                dtAsOfDate.getUTCMonth() - 1,
                                dtBirth.getUTCDate(),
                                dtBirth.getUTCHours(),
                                dtBirth.getUTCMinutes(),
                                dtBirth.getUTCSeconds() ) );

         // get previous month
         intMonths = dtAsOfDate.getUTCMonth() - 1;
         if ( intMonths == -1 )
            intMonths = 11;

         // while month is not what it is supposed to be (it will be higher)
         while ( dtAnniversary.getUTCMonth() != intMonths )

            // move back one day
            dtAnniversary.setUTCDate( dtAnniversary.getUTCDate() - 1 );

         }

      // if anniversary month is on or after birth month
      if ( dtAnniversary.getUTCMonth() >= dtBirth.getUTCMonth() )
         {

         // months elapsed is anniversary month - birth month
         intMonths = dtAnniversary.getUTCMonth() - dtBirth.getUTCMonth();

         // years elapsed is anniversary year - birth year
         intYears = dtAnniversary.getUTCFullYear() - dtBirth.getUTCFullYear();

         }

      // if birth month is after anniversary month
      else
         {

         // months elapsed is months left in birth year + anniversary month
         intMonths = (11 - dtBirth.getUTCMonth()) + dtAnniversary.getUTCMonth() + 1;

         // years elapsed is year before anniversary year - birth year
         intYears = (dtAnniversary.getUTCFullYear() - 1) - dtBirth.getUTCFullYear();

         }

      // to calculate weeks, days, hours, minutes and seconds
      // we can take the difference from anniversary date and as of date

      // get time span between two dates in milliseconds
      intSpan = dtAsOfDate - dtAnniversary;

      // get number of weeks
     // intWeeks = Math.floor(intSpan / 604800000);

      // subtract weeks from time span
    //  intSpan = intSpan - (intWeeks * 604800000);
      
      // get number of days
      intDays = Math.floor(intSpan / 86400000);

      // subtract days from time span
      intSpan = intSpan - (intDays * 86400000);

      // get number of hours
      intHours = Math.floor(intSpan / 3600000);
    
      // subtract hours from time span
      intSpan = intSpan - (intHours * 3600000);

      // get number of minutes
      intMinutes = Math.floor(intSpan / 60000);

      // subtract minutes from time span
      intSpan = intSpan - (intMinutes * 60000);

      // get number of seconds
      intSeconds = Math.floor(intSpan / 1000);

      // create output string     
      if ( intYears >= 0 )
         if ( intYears > 1 )
            strHowOld = intYears.toString() + 'Y';
         else
            strHowOld = intYears.toString() + 'Y';
      else
         strHowOld = '';

      if ( intMonths > 0 )
         if ( intMonths > 1 )
            strHowOld = strHowOld + '-' + intMonths.toString() + 'M';
         else
            strHowOld = strHowOld + '-' + intMonths.toString() + 'M';
           
     // if ( intWeeks > 0 )
      //   if ( intWeeks > 1 )
       //     strHowOld = strHowOld + ' ' + intWeeks.toString() + ' W';
       //  else
        //    strHowOld = strHowOld + ' ' + intWeeks.toString() + ' W';

      if ( intDays > 0 )
         if ( intDays > 1 )
            strHowOld = strHowOld + '-' + intDays.toString() + 'D';
         else
            strHowOld = strHowOld + '-' + intDays.toString() + 'D';
      }
   else
      strHowOld = ''

   // return string representation
   return strHowOld
   }   
   		
function calcDays() {
 document.forms['customerFileForm'].elements['customerFile.duration'].value="";
 var date2 = document.forms['customerFileForm'].elements['customerFile.contractStart'].value;	 
 var date1 = document.forms['customerFileForm'].elements['customerFile.contractEnd'].value; 
  var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = document.forms['customerFileForm'].elements['contractEndYear'].value; 
   if(year==''){var yearbreak='${customerFile.contractEnd}';
                  yearbreak=yearbreak.split("-");year=yearbreak[0];       
        }
  if(month == 'Jan')
   {
       month = "01";
   } else if(month == 'Feb')
   {
       month = "02";
   } else if(month == 'Mar')
   {
       month = "03"
   } else if(month == 'Apr')
   {
       month = "04"
   }  else if(month == 'May')
   {
       month = "05"
   } else if(month == 'Jun')
   {
       month = "06"
   }  else if(month == 'Jul')
   {
       month = "07"
   } else if(month == 'Aug')
   {
       month = "08"
   }  else if(month == 'Sep')
   {
       month = "09"
   }  else if(month == 'Oct')
   {
       month = "10"
   }  else if(month == 'Nov')
   {
       month = "11"
   }  else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = document.forms['customerFileForm'].elements['contractStartYear'].value;
    if(year2==''){var yearbreak='${customerFile.contractStart}';
                  yearbreak=yearbreak.split("-");year2=yearbreak[0];       
        }
   if(month2 == 'Jan')
   {
       month2 = "01";
   }  else if(month2 == 'Feb')
   {
       month2 = "02";
   }  else if(month2 == 'Mar')
   {
       month2 = "03"
   }  else if(month2 == 'Apr')
   {
       month2 = "04"
   }  else if(month2 == 'May')
   {
       month2 = "05"
   }  else if(month2 == 'Jun')
   {
       month2 = "06"
   }   else if(month2 == 'Jul')
   {
       month2 = "07"
   }  else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }  else if(month2 == 'Oct')
   {
       month2 = "10"
   }  else if(month2 == 'Nov')
   {
       month2 = "11"
   }  else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var diff = Math.round((sDate-eDate)/86400000);
  var  daysApart=0;
  if(diff<0)
  {
    alert("Contract Start Date must be less than Contract End Date");
    document.forms['customerFileForm'].elements['customerFile.duration'].value='';  
    document.forms['customerFileForm'].elements['customerFile.contractEnd'].value=""; 
    }else{
  daysApart=qryHowOld(sDate, eDate);
  }
  //var daysApart = Math.round((sDate-eDate)/86400000);
  document.forms['customerFileForm'].elements['customerFile.duration'].value = daysApart; 

  if(document.forms['customerFileForm'].elements['customerFile.duration'].value=='NaN')
   {
     document.forms['customerFileForm'].elements['customerFile.duration'].value = '';
   } 
 document.forms['customerFileForm'].elements['orderInitiationASMLForDaysClick'].value = '';
}



function forDays(){
 document.forms['customerFileForm'].elements['orderInitiationASMLForDaysClick'].value ='1';
} 
function checkAssignmentType(){ 
 if(document.forms['customerFileForm'].elements['customerFile.assignmentType'].value=="Other"){ 
 document.getElementById('otherContract').style.display = 'block'; 
 }else
 {
 document.getElementById('otherContract').style.display ='none'; 
 } 
 } 	
 function secImage1(){ 
if(document.getElementById('userImage1').height > 150)
{
 document.getElementById('userImage1').style.height = "120px";
 document.getElementById('userImage1').style.width = "auto";
}
}
function secImage2(){

if(document.getElementById('userImage2').height > 150 )
{
 document.getElementById('userImage2').style.height = "120px";
 document.getElementById('userImage2').style.width = "auto";
}        

}
function secImage3(){
if(document.getElementById('userImage3').height > 150)
{
 document.getElementById('userImage3').style.height = "120px";
 document.getElementById('userImage3').style.width = "auto";
} 
}
function secImage4(){
if(document.getElementById('userImage4').height > 150)
{
 document.getElementById('userImage4').style.height = "120px";
 document.getElementById('userImage4').style.width = "auto";
} 
}
function saveValidation(temp){
submitRelo();
var originCountry= document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
var originState= document.forms['customerFileForm'].elements['customerFile.originState'].value;
var destinationCountry= document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value
var destinationState= document.forms['customerFileForm'].elements['customerFile.destinationState'].value
//var btn = valButton(document.forms['customerFileForm'].elements['customerFile.orderAction']); 
//var visaRequired= document.forms['customerFileForm'].elements['customerFile.visaRequired'].value
//var visaStatus= document.forms['customerFileForm'].elements['customerFile.visaStatus'].value
var lastName= document.forms['customerFileForm'].elements['customerFile.lastName'].value
<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}">
		if(((originCountry=="United States") || (originCountry=="India")||(originCountry=="Canada"))&& (originState=="")) {
			alert("State is a required field in Present Address."); 
            return false;
		} 
		if(((destinationCountry=="United States") || (destinationCountry=="India")||(destinationCountry=="Canada"))&& (destinationState=="")) {
			alert("State of destination country is a required field in Employee Details.");
			return false;
		}
		/* if (btn == null)  {
			alert("Action Requested is a required field .");
			return false;
		}
		if(visaRequired==""){
			alert("Visa Required is a required field .");
			return false;
		}
		if(visaRequired!= "" && visaRequired=="Yes"){
			if(visaStatus==""){
			alert("Visa Status is a required field .");
			return false;
			}
		}*/
		
		if(originCountry==""){
			alert("Country is a required field in Present Address.");
			return false;
		} 
		if(document.forms['customerFileForm'].elements['customerFile.prefix'].value==""){
			alert("Prefix is a required field in Employee Details");
			return false;
		}
		
		if(document.forms['customerFileForm'].elements['customerFile.firstName'].value.trim()==""){
			alert("First Name is a required field in Employee Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value.trim()==""){
			alert("Employee Number is a required field in Employee Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.billToReference'].value.trim()==""){
			alert("Cost Centre is a required field in Employee Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.contractStart'].value==""){
			alert("Start Date of Contract/Stay is a required field in Employee Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.contractEnd'].value==""){
			alert("End Date of Contract/Stay is a required field in Employee Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.assignmentType'].value.trim()==""){
			alert("Type of Contract is a required field in Employee Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.homeCountry'].value==""){
			alert("Origin Country is a required field in Employee Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.homeCity'].value.trim()==""){
			alert("Origin City is a required field in Employee Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value.trim()==""){
			alert("Destination City is a required field in Employee Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.familySitiation'].value==""){
			alert("Family Situation is a required field in Employee Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.originAddress1'].value.trim()==""){
			alert("Transferee Address is a required field in Present Address");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.originCity'].value.trim()==""){
			alert("City is a required field in Present Address");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.originZip'].value.trim()==""){
			alert("Postal Code is a required field in Present Address");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.originDayPhone'].value.trim()==""){
			alert("Work Phone is a required field in Present Address");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value.trim()==""){
			alert("Home Phone is a required field in Present Address");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.originMobile'].value.trim()==""){
			alert("Mobile Phone is a required field in Present Address");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.email'].value.trim()==""){
			alert("Primary Email is a required field in Present Address");
			return false;
		} 
		if(document.forms['customerFileForm'].elements['customerFile.petsInvolved'].checked==true){
		if(document.forms['customerFileForm'].elements['customerFile.petType'].value==""){
			alert("Type of Pet is a required field in Present Address");
			return false;
			}
		}
		if(document.forms['customerFileForm'].elements['customerFile.orderBy'].value.trim()==""){
			alert("Origin HR Person is a required field in HR Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.orderPhone'].value.trim()==""){
			alert("Origin Country HR Person Phone is a required field in HR Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.orderEmail'].value.trim()==""){
			alert("Origin Country HR Person Email Address is a required field in HR Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.localHr'].value.trim()==""){
			alert("Destination HR Person is a required field in HR Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.localHrPhone'].value.trim()==""){
			alert("Destination HR Person Phone is a required field in HR Details");
			return false;
		}
		if(document.forms['customerFileForm'].elements['customerFile.localHrEmail'].value.trim()==""){
			alert("Destination HR Person Email Address is a required field in HR Details");
			return false;
		}
		if(destinationCountry==""){
			alert("Destination Country is a required field .");
			return false;
		} 
		if(lastName==""){
			alert("Last Name is a required field.");
			return false;
		}else
		{
		if(temp=="submit1")
		submitOrderInitiation(); 
		}
	</c:if>
	<c:if test="${validatorPartnerPrivate.portalDefaultAccess}">
	if(document.forms['customerFileForm'].elements['customerFile.email'].value.trim()==""){
		alert("Primary Email is a required field in Present Address");
		return false;
	}
	</c:if>
}
 function valButton(btn) {
    var cnt = -1;
    for (var i=btn.length-1; i > -1; i--) {
        if (btn[i].checked) {cnt = i; i = -1;}
    }
    if (cnt > -1) return btn[cnt].value;
    else return null;
}
function submitRelo()
{ 
document.forms['customerFileForm'].elements['removalRelocationService.isIta60'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isIta60'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isIta1m3'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isIta1m3'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isIta2m3'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isIta2m3'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isIta3m3'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isIta3m3'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtExpat20'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtExpat20'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtExpat40'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtExpat40'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isKgExpat30'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isKgExpat30'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isTransportAllow'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isTransportAllow'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtSingle20'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtSingle20'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtFamily40'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtFamily40'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isStorage'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isStorage'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeFurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeFurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeUnfurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeUnfurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isUsdollar'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isUsdollar'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isDollar'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isDollar'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOtherCurrency'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOtherCurrency'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.id'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.id'].value;
document.forms['customerFileForm'].elements['removalRelocationService.customerFileId'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.customerFileId'].value;
document.forms['customerFileForm'].elements['removalRelocationService.corpId'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.corpId'].value;
document.forms['customerFileForm'].elements['removalRelocationService.usdollarAmounts'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.usdollarAmounts'].value;
document.forms['customerFileForm'].elements['removalRelocationService.dollarAmounts'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.dollarAmounts'].value;
document.forms['customerFileForm'].elements['removalRelocationService.otherCurrencyAmounts'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.otherCurrencyAmounts'].value;
document.forms['customerFileForm'].elements['removalRelocationService.otherServicesRequired'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.otherServicesRequired'].value;
document.forms['customerFileForm'].elements['removalRelocationService.ftExpat40ExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.ftExpat40ExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.kgExpat30ExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.kgExpat30ExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.transportAllowExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.transportAllowExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.orientationTrip'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.orientationTrip'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.homeSearch'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.homeSearch'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.settlingInAssistance'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.settlingInAssistance'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.schoolSearch'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.schoolSearch'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.authorizedDays'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.authorizedDays'].value;
document.forms['customerFileForm'].elements['removalRelocationService.additionalNotes'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.additionalNotes'].value;
document.forms['customerFileForm'].elements['removalRelocationService.furnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.furnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.unFurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.unFurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.entryVisa'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.entryVisa'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.longTermStayVisa'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.longTermStayVisa'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.workPermit'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.workPermit'].checked;


}
function submitReloNew()
{ 

document.forms['customerFileForm'].elements['removalRelocationService.isFtExpat20'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtExpat20'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtExpat40'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtExpat40'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isKgExpat30'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isKgExpat30'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtSingle20'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtSingle20'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtFamily40'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtFamily40'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeFurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeFurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeUnfurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeUnfurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.id'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.id'].value;
document.forms['customerFileForm'].elements['removalRelocationService.customerFileId'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.customerFileId'].value;
document.forms['customerFileForm'].elements['removalRelocationService.corpId'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.corpId'].value;
document.forms['customerFileForm'].elements['removalRelocationService.ftExpat40ExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.ftExpat40ExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.kgExpat30ExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.kgExpat30ExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.homeSearch'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.homeSearch'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.settlingInAssistance'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.settlingInAssistance'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.schoolSearch'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.schoolSearch'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.furnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.furnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.unFurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.unFurnished'].checked;
<configByCorp:fieldVisibility componentId="component.field.removalRelocationSer.immigration">
document.forms['customerFileForm'].elements['removalRelocationService.entryVisa'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.entryVisa'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.longTermStayVisa'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.longTermStayVisa'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.workPermit'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.workPermit'].checked;
</configByCorp:fieldVisibility>

}
function setContractValidFlag(temp){
document.forms['customerFileForm'].elements['contractValidFlag'].value=temp;
}

function emailValidate(form_id,email,message) {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   var address = document.forms[form_id].elements[email].value;
    if(address!=''){
   if(reg.test(address) == false) {
      alert('Invalid '+message+' Email Address');
      document.forms[form_id].elements[email].value='';
      return false;
     }
   }
}
function letternumber(e)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27)|| (key==37) || (key==38) || (key==39)|| (key==40))
   return true;

// alphas and numbers
else if ((("abcdefghijklmnopqrstuvwxyz0123456789").indexOf(keychar) > -1))
   return true;
else
   return false;
}

function checkPetInvolve(result){
if(result.checked==true){
document.getElementById('petTypeId').style.display ='block';
}if(result.checked!=true){document.getElementById('petTypeId').style.display ='none';
}
}
</script>
<script type="text/javascript" src="scripts/prototype.js"></script>
<script type="text/javascript" src="scripts/effects.js"></script>
<script type="text/javascript" src="scripts/scriptaculous.js?load=effects"></script>


</head> 
<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>
<s:hidden name="fileID" id ="fileID" value="%{customerFile.id}" />
<c:set var="fileID" value="%{customerFile.id}"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="customerFileForm" name="customerFileForm" action="saveHoOrderInitiation.html" onsubmit="return submit_form()" method="post" validate="true" >
<div id="Layer1" onkeydown="changeStatus();" style="width:100%;">		    
		<s:hidden name="contractValidFlag" id="contractValidFlag" value="Y"/>
		<s:hidden name="contractStartYear" id="contractStartYear"/>
		<s:hidden name="contractEndYear" id="contractEndYear"/>
		<s:hidden name="originCountryFlex3Value"  id="originCountryFlex3Value"/>
		 <s:hidden name="DestinationCountryFlex3Value" id="DestinationCountryFlex3Value" />
		<s:hidden name="orderInitiationASMLForDaysClick" id="orderInitiationASMLForDaysClick"/>
		<s:hidden name="controlFlagBnt" value="<%=request.getParameter("controlFlagBnt") %>"/>
		<c:set var="controlFlagBnt" value="<%=request.getParameter("controlFlagBnt") %>" />
		<s:hidden name="customerFile.id" value="%{customerFile.id}" /> 
		<s:hidden name="customerFile.isNetworkRecord" />
		<s:hidden name="custJobType" value="<%=request.getParameter("custJobType") %>" />
		<s:hidden name="contractBillCode" value="<%=request.getParameter("contractBillCode") %>" />
		<s:hidden name="custCreatedOn" value="<%=request.getParameter("custCreatedOn") %>" />
		<s:hidden name="custStatus"/>
		<s:hidden name="oldCustStatus" value="${customerFile.status}"/>
		<s:hidden name="customerFile.corpID" />
	    <s:hidden name="stdAddDestinState" />
	    <s:hidden name="stdAddOriginState" />
	    <s:hidden name="checkConditionForOriginDestin" />
		<s:hidden name="id" value="<%=request.getParameter("id") %>" /> 
		<s:hidden name="gotoPageString" id="gotoPageString" value="" />
		<s:hidden name="idOfWhom" value="${customerFile.id}" /> 
		<s:hidden id="countNotes" name="countNotes" value="<%=request.getParameter("countNotes") %>"/>
		<s:hidden id="countOriginNotes" name="countOriginNotes" value="<%=request.getParameter("countOriginNotes") %>"/>
		<s:hidden id="countDestinationNotes" name="countDestinationNotes" value="<%=request.getParameter("countDestinationNotes") %>"/>
		<s:hidden id="countSurveyNotes" name="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>"/>
		<s:hidden id="countVipNotes" name="countVipNotes" value="<%=request.getParameter("countVipNotes") %>"/>
		<s:hidden id="countBillingNotes" name="countBillingNotes" value="<%=request.getParameter("countBillingNotes") %>"/>
		<s:hidden id="countSpouseNotes" name="countSpouseNotes" value="<%=request.getParameter("countSpouseNotes") %>"/>
		<s:hidden id="countContactNotes" name="countContactNotes" value="<%=request.getParameter("countContactNotes") %>"/>
		<s:hidden id="countCportalNotes" name="countCportalNotes" value="<%=request.getParameter("countCportalNotes") %>"/>
		<s:hidden id="countEntitlementNotes" name="countEntitlementNotes" value="<%=request.getParameter("countEntitlementNotes") %>"/>
		<c:set var="countNotes" value="<%=request.getParameter("countNotes") %>" />
		<c:set var="countOriginNotes" value="<%=request.getParameter("countOriginNotes") %>" />
		<c:set var="countDestinationNotes" value="<%=request.getParameter("countDestinationNotes") %>" />
		<c:set var="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>" />
		<c:set var="countVipNotes" value="<%=request.getParameter("countVipNotes") %>" />
		<c:set var="countBillingNotes" value="<%=request.getParameter("countBillingNotes") %>"/>
		<c:set var="countSpouseNotes" value="<%=request.getParameter("countSpouseNotes") %>"/>
		<c:set var="countContactNotes" value="<%=request.getParameter("countContactNotes") %>"/>
		<c:set var="countCportalNotes" value="<%=request.getParameter("countCportalNotes") %>"/>
		<c:set var="countEntitlementNotes" value="<%=request.getParameter("countEntitlementNotes") %>"/>
		<s:hidden name="dCountry" value="${dCountry}" id="dCountry" />
		<s:hidden name="oCountry" value="${oCountry}" id="oCountry"/> 
		<s:hidden id="countDSNotes" name="countDSNotes" value="<%=request.getParameter("countDSNotes") %>"/>
		<c:set var="countDSNotes" value="<%=request.getParameter("countDSNotes") %>" /> 
		<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
	    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	    <s:hidden name="formStatus" value=""/>  
	    <s:hidden  name="customerFile.quotationStatus"  /> 
	    <s:hidden  name="customerFile.orderIntiationStatus"  />
	    <s:hidden  name="customerFile.originCityCode" />
		<s:hidden   name="customerFile.originCountryCode" /> 
		<s:hidden  name="customerFile.destinationCityCode" /> 
		<s:hidden  name="customerFile.destinationCountryCode" />
		<s:hidden  name="customerFile.homeCountryCode" />
		<s:hidden name="HomeCountryFlex3Value" id="HomeCountryFlex3Value" />
		<s:hidden  name="customerFile.serviceRelo" />
		<s:hidden  name="customerFile.orderAction"  value="IO"/>
		<s:hidden  name="customerFile.visaRequired"  value="No"/>
		<s:hidden  name="customerFile.status" /> 
		<s:hidden  key="customerFile.customerEmployer" /> 
		<s:hidden  name="customerFile.bookingAgentCode" /> 
		<s:hidden  name="customerFile.bookingAgentName" /> 
        <s:hidden   name="customerFile.companyDivision" />
        <s:hidden name="partnerAbbreviation" value="${partnerAbbreviation}"/>
		<c:if test="${not empty customerFile.bookingDate}">
		<s:text id="customerFileBookingFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.bookingDate"/></s:text>
		<s:hidden id="bookingDate" name="customerFile.bookingDate" value="%{customerFileBookingFormattedValue}" />
		</c:if>
	    <c:if test="${empty customerFile.bookingDate}">
	    <s:hidden id="bookingDate"  name="customerFile.bookingDate" />
	    </c:if> 			
        <c:if test="${not empty customerFile.initialContactDate}">
        <s:text id="customerFileInitialFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.initialContactDate"/></s:text>
	    <s:hidden id="initialContactDate"  name="customerFile.initialContactDate" value="%{customerFileInitialFormattedValue}" />
	    </c:if>
	    <c:if test="${empty customerFile.initialContactDate}">
	    <s:hidden id="initialContactDate"  name="customerFile.initialContactDate" />
	   </c:if> 
	  <s:hidden  id="job" name="customerFile.job"  />
	  <s:hidden id="coordinator" name="customerFile.coordinator" /> 
	  <s:hidden  name="customerFile.salesStatus" />  
	  <s:hidden  name="customerFile.salesMan"    />
      <s:hidden   name="customerFile.comptetive"  /> 
	  <s:hidden  name="customerFile.source"  />  
	  <s:hidden  name="customerFile.estimator" id="estimator" /> 
	  <c:if test="${not empty customerFile.survey}">
	  <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.survey"/></s:text>
	  <s:hidden  id="survey" name="customerFile.survey" value="%{customerFileSurveyFormattedValue}" />
	  </c:if>
	  <c:if test="${empty customerFile.survey}">
	  <s:hidden  id="survey" name="customerFile.survey" />
	  </c:if> 
	  <s:hidden  name="customerFile.surveyTime" id="surveyTime" /> 
	  <s:hidden name="customerFile.surveyTime2" id="surveyTime2" /> 
	  <s:hidden name="timeZone" value="${sessionTimeZone}" />  
	  <c:if test="${not empty customerFile.priceSubmissionToAccDate}">
	  <s:text id="customerFileSubmissionToAccFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToAccDate"/></s:text>
	  <s:hidden id="priceSubmissionToAccDate" name="customerFile.priceSubmissionToAccDate" value="%{customerFileSubmissionToAccFormattedValue}" />
	  </c:if>
	  <c:if test="${empty customerFile.priceSubmissionToAccDate}">
	  <s:hidden id="priceSubmissionToAccDate"  name="customerFile.priceSubmissionToAccDate" />
	  </c:if> 
	  <c:if test="${not empty customerFile.priceSubmissionToTranfDate}">
	  <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToTranfDate"/></s:text>
	  <s:hidden id="priceSubmissionToTranfDate" name="customerFile.priceSubmissionToTranfDate" value="%{customerFileSubmissionToTranfFormattedValue}" />
	  </c:if>
	  <c:if test="${empty customerFile.priceSubmissionToTranfDate}">
	  <s:hidden id="priceSubmissionToTranfDate" name="customerFile.priceSubmissionToTranfDate" />
	  </c:if> 
	  <c:if test="${not empty customerFile.quoteAcceptenceDate}">
	  <s:text id="customerFileAcceptenceFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.quoteAcceptenceDate"/></s:text>
	  <s:hidden id="quoteAcceptenceDate"  name="customerFile.quoteAcceptenceDate" value="%{customerFileAcceptenceFormattedValue}" />
	  </c:if>
	  <c:if test="${empty customerFile.quoteAcceptenceDate}">
	  <s:hidden id="quoteAcceptenceDate"  name="customerFile.quoteAcceptenceDate" />
	  </c:if> 
	  <s:hidden name="customerFile.contactName" /> 
	  <s:hidden  name="customerFile.organization"  />  
	  <s:hidden name="customerFile.contactEmail"  /> 
	  <s:hidden  name="customerFile.contactPhone"   /> 
	  <s:hidden  name="customerFile.contactNameExt"  /> 
	  <s:hidden  name="customerFile.destinationContactName"   />
	  <s:hidden  name="customerFile.destinationOrganization" />  
	  <s:hidden  name="customerFile.destinationContactEmail"   /> 
	  <s:hidden name="customerFile.destinationContactPhone" /> 
	  <s:hidden name="customerFile.destinationContactNameExt"  />  
	  <s:hidden name="customerFile.billToCode"   /> 
	  <s:hidden  name="customerFile.billToName" />  
	  <s:hidden  id="contract" name="customerFile.contract"  />  
	  <s:hidden id="billPayMethod"  name="customerFile.billPayMethod" />  
	  <s:hidden name="customerFile.accountCode"  /> 
	  <s:hidden  name="customerFile.accountName" />  
	  <c:if test="${not empty customerFile.billApprovedDate}">
		<s:text id="customerFileApprovedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.billApprovedDate"/></s:text>
		<s:hidden id="billApprovedDate" name="customerFile.billApprovedDate" value="%{customerFileApprovedDateFormattedValue}" />
	  </c:if>
	  <c:if test="${empty customerFile.billApprovedDate}">
		<s:hidden id="billApprovedDate" name="customerFile.billApprovedDate" />
	  </c:if> 
	  <s:hidden name="customerFile.personPricing"   /> 
	  <s:hidden name="customerFile.personBilling" /> 
	  <s:hidden name="customerFile.personPayable"  /> 
	  <s:hidden name="customerFile.auditor" /> 
	  <s:hidden name="customerFile.noCharge" /> 
	  <s:hidden name="customerFile.approvedBy" /> 
	  <s:hidden  name="customerFile.originCompany"  />
	  <s:hidden name="customerFile.originAddress3" />
   	  <c:if test="${not empty customerFile.approvedOn}">
		<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="customerFile.approvedOn"/></s:text>
	    <s:hidden id="dateApprovedOn" name="customerFile.approvedOn" value="%{FormatedInvoiceDate}" />
	  </c:if>
	  <c:if test="${empty customerFile.approvedOn}">
		<s:hidden id="dateApprovedOn" name="customerFile.approvedOn" /> 
	  </c:if> 
	  <s:hidden  name="customerFile.destinationAddress1" />
      <s:hidden name="customerFile.destinationCompany" />
	  <s:hidden  name="customerFile.destinationAddress2" />
      <s:hidden  name="customerFile.destinationAddress3" />
      <s:hidden  name="customerFile.destinationZip" />
      <s:hidden  name="customerFile.destinationDayPhone"  />
      <s:hidden  name="customerFile.destinationDayPhoneExt" />
      <s:hidden  name="customerFile.destinationHomePhone"  />
      <s:hidden  name="customerFile.destinationMobile"  />
      <s:hidden  name="customerFile.destinationFax" />
      <s:hidden  name="customerFile.destinationEmail" />
      <s:hidden  name="customerFile.destinationEmail2"  />
      <s:hidden name="customerFile.custPartnerPrefix" />
      <s:hidden  name="customerFile.custPartnerFirstName" />
      <s:hidden name="customerFile.custPartnerLastName" /> 
	  <s:hidden name="customerFile.privileges" />
	  <s:hidden  name="customerFile.custPartnerEmail"  /> 
	  <s:hidden name="customerFile.custPartnerContact"/> 
	  <s:hidden  name="customerFile.custPartnerMobile"/> 
	  <s:hidden name="customerFile.portalIdActive"  /> 	
	  <s:hidden name="customerFileemail"   />
      <s:hidden name="customerFile.secondaryEmailFlag" /> 
      <s:hidden name="customerFile.customerPortalId" />
      <s:hidden  name="customerFile.sequenceNumber"/> 
      <c:if test="${not empty customerFile.statusDate}">
        <s:text id="customerFileStatusDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.statusDate" /></s:text>
        <s:hidden  name="customerFile.statusDate" value="%{customerFileStatusDateFormattedValue}" />
      </c:if>
      <c:if test="${empty customerFile.statusDate}">
      <s:hidden   name="customerFile.statusDate"  />
      </c:if>
      <s:hidden  name="customerFile.statusReason" />	
	 <s:hidden name="customerFile.statusNumber" />	
     <s:hidden name="secondDescription" />
     <s:hidden name="thirdDescription" />
     <s:hidden name="fourthDescription" />
     <s:hidden name="fifthDescription" />
     <s:hidden name="sixthDescription" />
     <s:hidden name="customerFile.socialSecurityNumber" /> 
	 <s:hidden name="customerFile.vip" />
	 <s:hidden name="customerFile.controlFlag" />
	<s:hidden name="customerFile.parentAgent" />														
							
	 <c:choose>
			<c:when test="${gotoPageString == 'gototab.serviceorder' }">
				<c:redirect url="/customerServiceOrders.html?id=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.raterequest' }">
				<c:redirect url="/customerRateOrders.html?id=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.surveys' }">
				<c:redirect url="/surveysList.html?id1=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.accountpolicy' }">
				<c:redirect url="/showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.imfEntitlements' }">
				<c:redirect url="/editImfEntitlement.html?cid=${customerFile.id}" />
			</c:when> 
			<c:otherwise>
			</c:otherwise>
	  </c:choose> 
		
		<c:set var="from" value="<%=request.getParameter("from") %>"/>
		<c:set var="field" value="<%=request.getParameter("field") %>"/>
		<s:hidden name="field" value="<%=request.getParameter("field") %>" />
		<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
		<c:set var="field1" value="<%=request.getParameter("field1") %>"/> 
	    <s:hidden name="jobNumber" value="%{customerFile.sequenceNumber}"/>
		<c:set var="jobNumber" value="<%=request.getParameter("sequenceNumber") %>" />
		<s:set name="reportss" value="reportss" scope="request"/>  
	<table><tr>
	 <c:if test="${!(partnerPublic.location1 == null || partnerPublic.location1 == '')}">      
		<td>
		 <div class="thumbline">
		 <img id="userImage1" class="urClass"  src="UserImage?location=${partnerPublic.location1}" alt=""  onload="secImage1()" style="border:none; vertical-align: middle; "/>
	     </div></td>
     </c:if>
	<c:if test="${!(partnerPublic.location2 == null || partnerPublic.location2 == '')}">
		<td>
		<div class="thumbline">
			<img id="userImage2" class="urClass" src="UserImage?location=${partnerPublic.location2}" alt=""  onload="secImage2()"    style="border:none; vertical-align: middle; "/>
		</div></td>
	</c:if>
	<c:if test="${!(partnerPublic.location3 == null || partnerPublic.location3 == '')}">
		<td>
		<div class="thumbline">
		 <img id="userImage3" class="urClass" src="UserImage?location=${partnerPublic.location3}" alt="" onload="secImage3()"  style="border:none; vertical-align: middle; "/>
		</div></td>
	</c:if>
	<c:if test="${!(partnerPublic.location4 == null || partnerPublic.location4 == '')}">
		<td><div class="thumbline">
		 <img id="userImage4" class="urClass" src="UserImage?location=${partnerPublic.location4}" alt="" onload="secImage4()"   style="border:none; vertical-align: middle;"/>
		</div></td>
	</c:if>
	</tr></table>

	  <c:if test="${not empty customerFile.id}">
		<div id="newmnav">
		  <ul> 
		    <li id="newmnav1" style="background:#FFF "><a href="editAgentHoOrder.html?id=${customerFile.id}" class="current"><span>Order Detail</span></a></li>
		    <li><a href="accountPortalFiles.html?id=${customerFile.id}&agentHoFlag=yes" /><span>File Documents</span></a></li>
		    <li><a href="agentHoOrders.html" /><span>Order List</span></a></li>
		    <li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=accPortal&reportSubModule=${partnerAbbreviation}&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		 </div><div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;"></div>
	  </c:if>	
	  <c:if test="${empty customerFile.id}">
		<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a onmouseover="" onclick=" " class="current"><span>Order Detail</span></a></li>
		    <li><a onmouseover="" onclick=" "><span>File Documents</span></a></li> 
		    <li><a href="agentHoOrders.html" /><span>Order List</span></a></li>  
		    <li><a onclick=" "><span>Forms</span></a></li>
		 </div><div class="spn">&nbsp;</div>
	  </c:if> 

      <div id="content" align="center">
       <div id="liquid-round">
       <div class="top"><span></span></div>
       <div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0" width="100%" >
<!--
<tr><td>       
<table class="" cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tbody>
		<tr>
          <td height="10" width="100%" align="left" style="margin: 0px">
  		  <div  onClick="javascript:animatedcollapse.toggle('address')" style="margin: 0px">
      	  <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 <tr>
			 <td class="headtab_left"> </td>
			 <td NOWRAP class="headtab_center">&nbsp;&nbsp;Order Instructions </td>
		     <td width="28" valign="top" class="headtab_bg"></td>
			 <td class="headtab_bg_center">&nbsp;</td>
			 <td class="headtab_right">
			 </td>
			</tr>
		  </table>
		  </div>
		  <div id="Initiation" class="switchgroup1">
		   <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
			 <tbody>
			 <tr><td height="10px"></td></tr>

<tr>
<td>
			   <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="74%" style="margin: 0px;padding: 0px;">
				   <tr>
				   <td align="right" width="10%" class="listwhitetext" valign="bottom">Action Requested&nbsp;<font color="red" size="2">*&nbsp;</font></td>
				   <td width="6%"></td>
				   <td width="3%"></td>
				   <td width="35%"></td> 
				   </tr>
			       <c:choose>
				   <c:when test="${customerFile.orderAction == 'IO'}" >
					  <tr>
					  <td></td>
					  <td class="listwhitetext" width="80px">Order Intiation</td>
					  <td><INPUT type="radio" name="customerFile.orderAction" checked="checked" value="IO" onchange="cheackQuotationStatus('IO')" ></td>
					  <td  align="left" class="listwhitetext" ><fmt:message key='customerFile.moveDate'/>
					  <c:if test="${not empty customerFile.moveDate}">
					  <s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
					  <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" size="7" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
					  <img id="calender" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.moveDate'),cal.select(document.forms['customerFileForm'].moveDate,'calender',document.forms['customerFileForm'].dateFormat.value); return false;"/>
					  </c:if>
					  <c:if test="${empty customerFile.moveDate}">
					  <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" size="7" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
					  <img id="calender" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.moveDate'),cal.select(document.forms['customerFileForm'].moveDate,'calender',document.forms['customerFileForm'].dateFormat.value); return false;"/>
					  </c:if>
					  </td>   			
					  </tr>
					  <tr><td height="1"></td></tr>
					  <tr>  
					  <td></td>
					  <td class="listwhitetext">Quote Request</td>
			  		  <td><INPUT type="radio" name="customerFile.orderAction"  value="QR" onchange="cheackQuotationStatus('QR')"  ></td>
			  		  <td><table style="margin: 0px;padding: 0px;"><tr>
			  		  <td id="hidQuotationStatus">
			  		  <input type="button" class="cssbutton1" value="Accept Quote" name="acceptQuote" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderInitiationStatus('Accepted')"/> 
			  		  <input type="button" class="cssbutton1" value="Reject Quote" name="rejectQuote" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderInitiationStatus('Rejected')"/> 
			  		  </td>
			  		  </tr></table></td>
					  </tr>
				   </c:when>
				   <c:when test="${customerFile.orderAction == 'QR'}" >
					  <tr>
					  <td></td>
		  		      <td class="listwhitetext" width="80px">Order Intiation</td>
		  		      <td><INPUT type="radio" name="customerFile.orderAction"  value="IO"  onchange="cheackQuotationStatus('IO')" tabindex="" ></td>
		  		      <td  align="left" class="listwhitetext" ><fmt:message key='customerFile.moveDate'/>
		  			  <c:if test="${not empty customerFile.moveDate}">
			          <s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
			          <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" size="7" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
			          <img id="calender" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.moveDate'),cal.select(document.forms['customerFileForm'].moveDate,'calender',document.forms['customerFileForm'].dateFormat.value); return false;"/>
			          </c:if>
			          <c:if test="${empty customerFile.moveDate}">
			          <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" size="7" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
			          <img id="calender" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.moveDate'),cal.select(document.forms['customerFileForm'].moveDate,'calender',document.forms['customerFileForm'].dateFormat.value); return false;"/>
			          </c:if>
		  		      </td> 	  
		  		      </tr>
					  <tr><td height="1"></td></tr>
			  		  <tr>
			  		  <td></td>
			  		  <td class="listwhitetext">Quote Request</td>
			  		  <td><INPUT type="radio" name="customerFile.orderAction" checked="checked" value="QR" onchange="cheackQuotationStatus('QR')" tabindex=""></td>
			  		   <td><table style="margin: 0px;padding: 0px;"><tr>
			  		  <td id="hidQuotationStatus">
			  		  <input type="button" class="cssbutton1" value="Accept Quote" name="acceptQuote" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderInitiationStatus('Accepted')"/> 
			  		  <input type="button" class="cssbutton1" value="Reject Quote" name="rejectQuote" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderInitiationStatus('Rejected')"/> 
			  		  </td>
			  		  </tr></table></td>
			  		  </tr>
		  		   </c:when>
				   <c:otherwise>
			  		  <tr>
		  			  <td></td>
		  			  <td class="listwhitetext" width="80px">Order Intiation</td>
		  			  <td><INPUT type="radio" name="customerFile.orderAction"  value="IO" onchange="cheackQuotationStatus('IO')" tabindex="" ></td>
		  		      <td  align="left" class="listwhitetext" ><fmt:message key='customerFile.moveDate'/>
		  			  <c:if test="${not empty customerFile.moveDate}">
			          <s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
			          <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" size="7" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
			          <img id="calender" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.moveDate'),cal.select(document.forms['customerFileForm'].moveDate,'calender',document.forms['customerFileForm'].dateFormat.value); return false;"/>
			          </c:if>
			          <c:if test="${empty customerFile.moveDate}">
			          <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" size="7" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
			          <img id="calender" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.moveDate'),cal.select(document.forms['customerFileForm'].moveDate,'calender',document.forms['customerFileForm'].dateFormat.value); return false;"/>
			          </c:if>
		  		      </td> 
		  		      </tr>
		  		      <tr><td height="1"></td></tr>
		  		      <tr>
		  		      <td></td>
		  		      <td class="listwhitetext">Quote Request</td>
		  		      <td><INPUT type="radio" name="customerFile.orderAction"  value="QR" onchange="cheackQuotationStatus('QR')" tabindex=""></td>
		  		      <td><table style="margin: 0px;padding: 0px;"><tr>
		  		      <td id="hidQuotationStatus">
		  		      <input type="button" class="cssbutton1" value="Accept Quote" name="acceptQuote" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderInitiationStatus('Accepted')"/> 
		  		      <input type="button" class="cssbutton1" value="Reject Quote" name="rejectQuote" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderInitiationStatus('Rejected')"/> 
		  		      </td>
		  		      </tr></table></td>
					  </tr>
				   </c:otherwise>
				   </c:choose> 
    	       </table>
    	       

</td>
</tr>
<tr>
<td>
    	       <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
    	              <tr>
					  <td align="right" width="112"  class="listwhitetext" valign="bottom">Service Requested&nbsp;</td>
					  </tr>
					  <tr>
					  <td></td>
					  <td colspan="11"><fieldset style="margin:2px; padding:5px; width:320px;">
					  <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
						  <tr>	
						  <td align="right" class="listwhitebox" width="2%" valign="middle">Air</td>
						  <td align="left" width="3%" ><s:checkbox name="customerFile.serviceAir" value="${customerFile.serviceAir}" fieldValue="true" disabled="disabled" tabindex="" /></td>
						  <td align="right" class="listwhitebox" width="2%" valign="middle">Sea</td>
						  <td align="left" width="3%" ><s:checkbox name="customerFile.serviceSurface" value="${customerFile.serviceSurface}" fieldValue="true" disabled="disabled" tabindex="" /></td>
						  <td align="right" class="listwhitebox" width="2%" valign="middle">Auto</td>
						  <td align="left" width="3%" ><s:checkbox name="customerFile.serviceAuto" value="${customerFile.serviceAuto}" fieldValue="true" disabled="disabled" tabindex="" /></td>
						  <td align="right" class="listwhitebox" width="2%" valign="middle">Storage</td>
						  <td align="left"  width="3%"><s:checkbox name="customerFile.serviceStorage" value="${customerFile.serviceStorage}" fieldValue="true" disabled="disabled" tabindex="" /></td>
						  <s:hidden name="customerFile.servicePet" value="${customerFile.servicePet}" />
						  <td width="2%"></td>					 
						  </tr>
					  </table>
					  </fieldset>
					  </td>
                      </tr>
               </table>
</td>
</tr>
<tr>
<td>
               <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
                   <tr>
                   <td align="left" height="10"  class="listwhitetext" valign="bottom"></td>
                   <td></td>
                   </tr>
                   <tr>
			       <td align="right" width="114"  class="listwhitetext" valign="top">Relocation&nbsp;Services&nbsp;</td>
                   <s:hidden  name="customerFile.serviceRelo"   />
                   <td class="listwhitetext" valign="middle"><input type="checkbox" style="margin-left:80px " name="checkAllRelo"  onclick="checkAll(this)" tabindex="" />Check&nbsp;All&nbsp;</td>
                   </tr>
                   <tr>
                   <td></td>
                   <td colspan="2" valign="top">
                     <fieldset style="margin:3px 0 0 0; padding:2px 0 0 2px; width:60%;">                      
                     <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
                       <tr>
                       <td width="53%" valign="top">
                       <table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0"> 
	                       <c:forEach var="entry" items="${serviceRelos1}">
	                       <tr>
	                       <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" tabindex=""/> </td> 
	                       <td width="5px"></td>
	                       <td align="left" class="listwhitetext" > ${entry.value}</td>
	                       </tr>
	                       </c:forEach>
                       </table> 
                       </td>
                       <td valign="top">
                       <table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0"> 
                           <c:forEach var="entry" items="${serviceRelos2}">
                           <tr> 
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" tabindex=""/> </td>
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td>
                          </tr>
                          </c:forEach>
                       </table>                    
                       </td>
                       </tr>
                     </table>
                     </fieldset>
                   </td> 
                   </tr>
                   <tr>
	               <td align="left" height="10"  class="listwhitetext" valign="bottom"></td>
	               <td></td>
                   </tr>
                   </table>
</td>
</tr>
<tr>
<td>
               <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="">
                  <tr><td></td></tr>
                  <tr><td width="30"></td>
                  <td align="right" width=""  class="listwhitetext" >Visa Required<font color="red" size="2">*&nbsp;</font></td><td width="5"></td>
                  <td align="left" width="115"><s:select cssClass="list-menu"  name="customerFile.visaRequired"  list="%{visaRequiredList}" cssStyle="width:100px" headerKey="" headerValue=""  onchange="cheackVisaRequired();" tabindex=""/></td>
                  <td align="right"  class="listwhitetext" id="hidVisaStatusReg" >Visa Status<font color="red" size="2">*&nbsp;</font></td><td width="5"></td>
                  <td align="left"  id="hidVisaStatusReg1"><s:select cssClass="list-menu"  name="customerFile.visaStatus"  list="%{visaStatusList}" cssStyle="width:100px" headerKey="" headerValue="" tabindex="" /></td>
                  <td width=""></td>
                  </tr>
                  <tr><td></td></tr>
               </table>
</td>
</tr>
<tr>
<td>
               <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
                   <tr>
                   <td align="left" width=""  height="5" class="listwhitetext" valign="bottom"></td>
                   </tr>
                   <tr>
                   <td align="right" width="20%"  class="listwhitetext" valign="top" >Comments&nbsp;&nbsp;</td>
                   <td  align="left" width="80%" ><s:textarea name="customerFile.orderComment" cols="80" rows="8" cssClass="textarea" readonly="false" tabindex=""/></td>
                   </tr>
             </table>
    	    
</td></tr>
</tbody></table></div>
</td></tr></tbody></table>

</td></tr> -->
<%--Employee Details div --%>
<tr>
<td> 
 <div  style="margin:0px;cursor:hand;">
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;padding:0px;" >
        <tr>
        <td class="headtab_left"></td>
	    <td NOWRAP class="headtab_center" style="cursor:hand; ">&nbsp;&nbsp;Employee Details </td>
        <td width="28" valign="top" class="headtab_bg" style="cursor:hand; "></td>
        <td class="headtab_bg_center" style="cursor:hand;"> <a href="javascript:animatedcollapse.hide(['presentaddress', 'removal', 'family','hr'])">Collapse All<img src="${pageContext.request.contextPath}/images/section_contract.png" HEIGHT=14 WIDTH=14 align="top"></a> | <a href="javascript:animatedcollapse.show([ 'presentaddress', 'removal', 'family','hr'])">Expand All<img src="${pageContext.request.contextPath}/images/section_expanded.png" HEIGHT=14 WIDTH=14 align="top"></a></td>
        <td class="headtab_right"></td>
	    </tr>
</table>
</div>
	<div id="employee" class="switchgroup1">
<table width="100%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="0" style="margin:0px;padding:0px;">
        <tr><td align="center" height="5px"></td></tr>		
        <%--<tr><td class="listwhitetext" style="padding-left:140px;"><b><u>Name of Transferee</u></b></td></tr>
        <tr><td align="center" colspan="15" height="5px"></td></tr>		
        --%><tr>
        <td align="left">
		<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px;padding:0px;" >
			 <tbody>			 	
			 <tr>
			 <td colspan="7"><table class="detailTabLabel" cellspacing="2" cellpadding="1" border="0" width="">
			 <tr>
			 <td style="width:137px;!width:138px;">&nbsp;</td>
				<td align="left" width="" class="listwhitetext" valign="bottom" ><fmt:message key='customerFile.prefix'/><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
				
				<td align="left" width="" class="listwhitetext" valign="bottom">Title / Rank</td>
				
				<td align="left" width="" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.firstName' /><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
				
				<td align="left" width="" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.middleInitial'/></td>
				
				<td align="left" width="" class="listwhitetext" valign="bottom">Last&nbsp;Name<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
			 </tr>
			 <tr> 
			 <td width="90">&nbsp;</td>
				<td align="left" class="listwhitetext" valign="top" > <s:select cssClass="list-menu" name="customerFile.prefix" cssStyle="width:47px" list="%{preffix}" headerKey="" headerValue="" onchange="changeStatus();" onfocus = "myDate();" tabindex=""/></td>
				
				<td align="left" class="listwhitetext" valign="top"><s:textfield cssClass="input-text" key="customerFile.rank" size="7" maxlength="7"  onblur="valid(this,'special')" tabindex=""/> </td>
				
				<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text" name="customerFile.firstName" onchange="noQuote(this);" required="true" cssStyle="width:165px" maxlength="80" onfocus = "myDate();" tabindex=""/> </td>
				
				<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text" key="customerFile.middleInitial" 	required="true" cssStyle="width:13px" maxlength="1" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex=""/> </td>
				
				<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text" key="customerFile.lastName" onchange="noQuote(this);" required="true" 	 size="25" maxlength="80" onfocus = "myDate();" tabindex=""/> </td>
				<c:if test="${empty customerFile.id}">
				<td align="right" style="width:247px;!width:267px;"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
				</c:if>
				<c:if test="${not empty customerFile.id}"> 
				<c:choose>
					<c:when test="${countAccountPortalNotes == '0' || countAccountPortalNotes == '' || countAccountPortalNotes == null}">
					<td align="right" style="width:247px;!width:267px;"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',755,500);" ></a></td>
					</c:when>
					<c:otherwise>
					<td align="right" style="width:247px;!width:267px;"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',755,500);" ></a></td>
					</c:otherwise>
				</c:choose> 
				</c:if>
			 </tr></table></td></tr>
			 
			  <tr>
			  <td colspan="15">
			 <table class="detailTabLabel" cellspacing="2" cellpadding="1" border="0" width="">
			
			 
			 <tr>
			    <td align="right" class="listwhitetext"  >Employee Number<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
			    <td width="100" ><s:textfield cssClass="input-text" name="customerFile.billToAuthorization"   maxlength="50" onblur="valid(this,'special')" cssStyle="width:165px"  tabindex="" /></td>
			 </tr> 
			 <tr>
			    
			    <td align="right" class="listwhitetext">Cost Centre <c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
			    <td colspan="4"   class="listwhitetext" ><s:textfield cssClass="input-text" name="customerFile.billToReference"   maxlength="15"  onblur="valid(this,'special')" cssStyle="width:165px" tabindex=""/></td>
                
             </tr>
                
             <tr> 
			    <td align="right" class="listwhitetext"   width="">Start Date of Contract/Stay<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
			     <td colspan="9">
               <table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0">
               <tbody>  
			    <tr>
			    <c:if test="${not empty customerFile.contractStart}">
		           <s:text id="customerFileContractStartFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.contractStart"/></s:text>
		           <td> <s:textfield cssClass="input-text" id="contractStart" name="customerFile.contractStart" value="%{customerFileContractStartFormattedValue}" required="true" size="8" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
		           <img id="contractStart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="setContractValidFlag('S');focusDate('customerFile.contractStart');forDays();"/>
                   </td>
                 </c:if>
                 <c:if test="${empty customerFile.contractStart}">
	               <td><s:textfield cssClass="input-text" id="contractStart" name="customerFile.contractStart" required="true" size="8" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
		           <img id="contractStart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="setContractValidFlag('S');focusDate('customerFile.contractStart');forDays();"/>
	             </td>
	             </c:if>
                <td  align="right" width="10"  class="listwhitetext" ></td> 
	         <td align="right" class="listwhitetext"  >End Date of Contract/Stay<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if>&nbsp;</td> 
             <c:if test="${not empty customerFile.contractEnd}">
		           <s:text id="customerFileContractEndFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.contractEnd"/></s:text>
		           <td> <s:textfield cssClass="input-text" id="contractEnd" name="customerFile.contractEnd" value="%{customerFileContractEndFormattedValue}" required="true" size="8" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
		           <img id="contractEnd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="setContractValidFlag('E');focusDate('customerFile.contractEnd'); forDays();"/>
                   </td>
             </c:if>
             <c:if test="${empty customerFile.contractEnd}">
		           <td><s:textfield cssClass="input-text" id="contractEnd" name="customerFile.contractEnd" required="true" size="8" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
		           <img id="contractEnd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="setContractValidFlag('E');focusDate('customerFile.contractEnd'); forDays();"/>
	               </td>
	         </c:if>
	         </tr>
	         </table>
	         </td>
	         </tr>
	         
	         <tr>
	         <td align="right" class="listwhitetext"  >Duration of Assignment</td>
             <td><s:textfield cssClass="input-text" name="customerFile.duration" id="orderInitiationASMLDuration" size="15" maxlength="12"  tabindex="" onselect="" onfocus="calcDays()"/></td>
             </tr> 
             <tr> 
             <td class="listwhitetext" align="right" >Type of Contract<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
             <td class="listwhitetext" colspan="2"><s:select  cssClass="list-menu" name="customerFile.assignmentType" list="%{assignmentTypesASML}" headerKey=" " headerValue=" "  onclick="changeStatus();checkAssignmentType();" tabindex=""/></td>
             <td align="left"  colspan="5"><s:textfield  id="otherContract" cssClass="input-text" name="customerFile.otherContract"  size="22" maxlength="30" tabindex=""/></td>
             </tr>
             <tr>
             <td align="right"  class="listwhitetext" >Origin Country<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if>&nbsp;</td> 
             <td style="width:220px;!width:235px;"><s:select cssClass="list-menu" name="customerFile.homeCountry" list="%{ocountry}" cssStyle="width:195px"  onchange="changeStatus(); getHomeCountryCode(this);autoPopulate_customerFile_homeCountry(this); gethomeState(this);"  headerKey="" headerValue="" tabindex=""/></td>
             <td  align="right" class="listwhitetext" >State</td> 
             <td width="60"><s:select cssClass="list-menu" id="homeState" name="customerFile.homeState" list="%{homeStates}" cssStyle="width:140px"  onchange="changeStatus(); " headerKey="" headerValue="" tabindex=""/></td>
             <td  align="right" class="listwhitetext" >City<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td> 
             <td width="60"><s:textfield cssClass="input-text" name="customerFile.homeCity" size="15" maxlength="30"  onblur="" tabindex=""/></td>
             </tr>
             <tr>
             <td align="right" class="listwhitetext">Destination&nbsp;Country&nbsp;<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*&nbsp;</font></c:if></td>
             <td><s:select cssClass="list-menu" name="customerFile.destinationCountry" list="%{dcountry}" id="dcountry" cssStyle="width:195px"  onchange="changeStatus();getDestinationCountryCode(this);getDestinationState(this);autoPopulate_customerFile_destinationCountry(this);"  headerKey="" headerValue="" tabindex=""/></td>
             <td align="right" class="listwhitetext" ><div id="destinationStateNotRequired"><fmt:message key='customerFile.destinationState'/></div><div id="destinationStateRequired"><fmt:message key='customerFile.destinationState'/><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*&nbsp;</font></c:if></div></td>
             <td><s:select cssClass="list-menu" id="destinationState" name="customerFile.destinationState" list="%{dstates}" cssStyle="width:140px" value="%{customerFile.destinationState}"  onchange="changeStatus();autoPopulate_customerFile_destinationCityCode(this);" headerKey="" headerValue="" tabindex=""/></td>
             <td align="right" class="listwhitetext"><fmt:message key='customerFile.destinationCity'/><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*&nbsp;</font></c:if></td>
             <td width="260"><s:textfield cssClass="input-text" name="customerFile.destinationCity" size="15" maxlength="30"  onblur="autoPopulate_customerFile_destinationCityCode(this,'special');" tabindex=""/></td>
             </tr>
             <tr>
             <td  align="right"  class="listwhitetext" >Family Situation<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*&nbsp;</font></c:if></td>  
             <td><s:select cssClass="list-menu" name="customerFile.familySitiation" list="%{familySitiationList}" cssStyle="width:195px"   onchange="" headerKey="" headerValue="" tabindex=""/></td>
             </tr>
             <tr>			
				<td align="left" class="listwhitetext"></td>
				</tr>
         	</table>
         	</td>
         	</tr>
             
            
             <tr> 
             <td height="10" width="100%" colspan="6" align="left" style="margin:0px">
               <div onClick="javascript:animatedcollapse.toggle('family')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px"> 
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">&nbsp;&nbsp;Family&nbsp;Details </td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
               </div>           
       
                <div id="family" class="OverFlowPr" style="margin:0px;padding:0px;margin-top:5px;margin-bottom:5px;;width:910px;">
                <s:set name="dsFamilyDetailsList" value="dsFamilyDetailsList" scope="request"/>
                  <display:table name="dsFamilyDetailsList" class="table" requestURI="" id="dsFamilyDetailsList"  defaultsort="1" pagesize="10" style="width:1350px;margin-bottom:20px;">
                     <c:if test="${(customerFile.quotationStatus=='Accepted' )||(customerFile.quotationStatus=='Rejected')|| (customerFile.quotationStatus=='Submitted' ) || (customerFile.orderIntiationStatus=='Submitted')}">
                     <display:column title="First Name"> 
                     <c:out value="${dsFamilyDetailsList.firstName}"></c:out>
                     </display:column> 
                     </c:if>
                     <c:if test="${(customerFile.quotationStatus!='Accepted' )&&(customerFile.quotationStatus!='Rejected')&& (customerFile.quotationStatus!='Submitted')&& (customerFile.orderIntiationStatus!='Submitted') }">
                     <display:column title="First Name">
                     <a href="javascript:openWindow('editDsFamilyDetails.html?isOrderInitiation=true&id=${dsFamilyDetailsList.id}&customerFileId=${customerFile.id}&decorator=popup&popup=true',1350,400)">
                     <c:out value="${dsFamilyDetailsList.firstName}"></c:out></a>
                     </display:column>
                     </c:if>
                  <display:column property="lastName"  title="Last Name" />
			      <display:column property="relationship"  title="Relationship" />   
			      <display:column property="cellNumber"  title="Mobile Phone"  />
			      <display:column property="email" title="Email"  />v
			      <display:column property="dateOfBirth" title="Date of Birth" format="{0,date,dd-MMM-yyyy}" />
			      <display:column property="age" title="Age"  />
			      <display:column property="countryOfBirth" title="Country of Birth"  />
			      <display:column property="passportNumber" title="Passport #"  /> 
				  <display:column property="expiryDate" title="Expiry Date" format="{0,date,dd-MMM-yyyy}" />
				  <display:column property="countryOfIssue"  title="Country of Issue" /> 
				  </display:table>
				  
	              <c:if test="${not empty customerFile.id}">
	               <input type="button" class="cssbutton1" onclick="window.open('editDsFamilyDetails.html?isOrderInitiation=true&decorator=popup&popup=true&customerFileId=${customerFile.id}','sqlExtractInputForm','height=500,width=1350,top=0, scrollbars=yes,resizable=yes').focus();" 
	                value="Add Family Details" style="width:120px; height:25px;margin-bottom:10px;" tabindex=""/> 
                  </c:if>
                  <c:if test="${empty customerFile.id}">
	                <input type="button" class="cssbutton1" onclick="massageFamily();"  value="Add Family Details" style="width:120px; height:25px;margin-bottom:10px;" tabindex=""/> 
                   </c:if>
                
                </div>
	         </td> 
	         </tr>
	         
	         <tr>
	         <td colspan="6">
	         <div onClick="javascript:animatedcollapse.toggle('presentaddress')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">                   
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">&nbsp;&nbsp;Present Address</td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
               </div>
            
	         <div id="presentaddress">
               <table class="detailTabLabel" border="0" width="100%" style="margin: 0px;padding:0px;">
 		        <tbody>
	            <tr>
	            <td align="left" class="listwhitetext"><fmt:message key='customerFile.originAddress1'/><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*&nbsp;</font></c:if></td>
	            <td></td>
	            </tr>
	            <tr>
	            <td align="left" width="10%" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.originAddress1" size="35" maxlength="100" tabindex="" /></td>
			    <td width="22"></td>
	            </tr>
	            <tr>
			    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.originAddress2" size="35" maxlength="100" tabindex="" /></td>
				<td align="left" class="listwhitetext" colspan="2" style="padding-left:3px"><font color="gray">Address2</font></td>
				</tr>
				
				<tr>
				<td colspan="9">
				<table class="detailTabLabel" cellpadding="1" cellspacing="1" border="0" style="margin: 0px;padding:0px;" >
											
				<tr>
				<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCountry'/><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*&nbsp;</font></c:if></td>
				<td align="left" class="listwhitetext" style="width:13px"></td>
				<td align="left" class="listwhitetext" ><div id="originStateNotRequired"><fmt:message key='customerFile.originState'/></div><div id="originStateRequired"><fmt:message key='customerFile.originState'/><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*&nbsp;</font></c:if></div></td>
				<td align="left" class="listwhitetext" style="width:7px"></td>
				<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCity'/><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*&nbsp;</font></c:if></td>
				<td align="left" class="listwhitetext" style="width:7px"></td>
				<td align="left" id="zipCodeRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originZip' /><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*&nbsp;</font></c:if></td> 
                </tr>
				
				<tr>
				<td><s:select cssClass="list-menu" name="customerFile.originCountry" list="%{ocountry}" id="ocountry" cssStyle="width:205px"  onchange="changeStatus();getOriginCountryCode(this);autoPopulate_customerFile_originCountry(this);getState(this);"  headerKey="" headerValue="" tabindex=""/></td>
				<td><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation();"/></td>
				
				<td><s:select cssClass="list-menu" id="originState" name="customerFile.originState" list="%{ostates}" cssStyle="width:140px"  onchange="changeStatus();autoPopulate_customerFile_originCityCode(this); " headerKey="" headerValue="" tabindex=""/></td>
				<td align="left" class="listwhitetext" style="width:7px"></td>
				<td><s:textfield cssClass="input-text" name="customerFile.originCity" size="15" maxlength="30"  onblur="autoPopulate_customerFile_originCityCode(this,'special');" tabindex=""/></td>
				<td align="left" class="listwhitetext" style="width:7px"></td>
				<td><s:textfield cssClass="input-text" name="customerFile.originZip" size="7" maxlength="10" onchange="findCityStateOfZipCode(this,'OZ');" onkeydown="return letternumber(event)" onblur="valid(this,'special')" tabindex=""/></td>
				</tr>
				</table>
				</td>
				</tr>
				
				
				<tr>
				<td colspan="9">
				<table class="detailTabLabel" cellpadding="1" cellspacing="1" border="0" style="margin: 0px;padding:0px;">
				<tr>
				<td align="left" class="listwhitetext"><fmt:message key='customerFile.originDayPhone'/><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td align="left" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.originDayPhoneExt'/></td>
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td align="left" class="listwhitetext"><fmt:message key='customerFile.originHomePhone'/><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
			    <td align="left" class="listwhitetext" style="width:5px"></td>
			    <td align="left" class="listwhitetext"><fmt:message key='customerFile.originMobile'/><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
			    <td align="left" class="listwhitetext" style="width:5px"></td>
				<td align="left" class="listwhitetext"><fmt:message key='customerFile.originFax'/></td>
				</tr>
				<tr>
				<td><s:textfield cssClass="input-text" name="customerFile.originDayPhone" size="15" maxlength="20"  tabindex="" /></td>
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td><s:textfield cssClass="input-text" name="customerFile.originDayPhoneExt" size="7" maxlength="4" onchange="onlyNumeric(this);"  tabindex=""/></td>
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td><s:textfield cssClass="input-text" name="customerFile.originHomePhone" size="15" maxlength="16"  tabindex="" /></td>
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td><s:textfield cssClass="input-text" name="customerFile.originMobile" size="15" maxlength="25"  tabindex="" /></td>
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td><s:textfield cssClass="input-text" name="customerFile.originFax" size="15" maxlength="16"  tabindex="" /></td>
				</tr>
				</table>
				</td>
				</tr>
				
				<tr>
	         <td colspan="9">
               <table class="detailTabLabel" border="0" style="margin: 0px;padding:0px;">
 		        <tbody>
				<tr>
				<td align="left" class="listwhitetext"><fmt:message key='customerFile.email'/><font color="red" size="2">*</font></td>
				<td align="left" class="listwhitetext"><fmt:message key='customerFile.email2'/></td>
				</tr>
				<tr>
					
					<td ><s:textfield cssClass="input-text" name="customerFile.email" size="30" maxlength="65" tabindex="" onchange="emailValidate('customerFileForm','customerFile.email','Primary');EmailPortDate();" />
					<img class="openpopup" style="vertical-align:top;" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.email'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.email}"/></td>
					
					<td ><s:textfield cssClass="input-text" name="customerFile.email2" size="30" maxlength="65" tabindex="" onchange="emailValidate('customerFileForm','customerFile.email2','Secondary');"/></td>
					<td align="left" class="listwhitetext" style="width:15px"><img class="openpopup" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.email2'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.email2}"/>
					</td>
					<td style="width:52%;" colspan="3" align="right"></td>
					<%--<c:if test="${empty customerFile.id}">
					<td style="width:52%;" colspan="3" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
					</c:if>
					<c:if test="${not empty customerFile.id}">
					<c:choose>
						<c:when test="${countAccountPortalNotes == '0' || countAccountPortalNotes == '' || countAccountPortalNotes == null}">
							<td style="width:52%;" colspan="3" align="right"><img id="countOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',755,500);" ></a></td>
						</c:when>
						<c:otherwise>
							<td style="width:52%;" colspan="3" align="right"><img id="countOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',755,500);" ></a></td>
						</c:otherwise>
					</c:choose>
					</c:if>
					--%></tr>
				</table>
				</td>
				</tr>
				<tr>				
				<td align="left" class="listwhitetext">Pets Involved&nbsp;&nbsp;
				<s:checkbox name="customerFile.petsInvolved" value="${customerFile.petsInvolved}" fieldValue="true" onclick="changeStatus();checkPetInvolve(this);" /></td>	
				
				<td colspan="3" id="petTypeId">
		<table width="" style="margin:0px;">
			<tr>
			 <td align="left" class="listwhitetext" style="">Type of Pet</td>
			 <td style="width: 8px; color: red;" >*</td>
			 <td>
		     <s:select cssClass="list-menu"  name="customerFile.petType"  list="%{petTypeList}" cssStyle="width:100px"   tabindex=""/>
		     </td>
		  </tr>
	    </table>
	</td>	
		</tr>
	            </tbody>
	           </table>
	           </div>
	        
			 </tbody>
	    </table>
	        </div>
        </td>
        </tr> 	
</table> 
</td>
</tr>
<%--HR Details div --%>
<tr>
<td> 
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;padding:0px;">
<tr><td height="10" width="100%" align="left" style="margin: 0px">
<div  onClick="javascript:animatedcollapse.toggle('hr')" style="margin: 0px">
 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
 <tr>
 <td class="headtab_left"></td>
 <td NOWRAP  class="headtab_center" style="cursor:default; ">&nbsp;&nbsp;HR Details </td>
 <td width="28" valign="top" class="headtab_bg"></td>
 <td class="headtab_bg_center">&nbsp; </td>
 <td class="headtab_right"> </td>
 </tr>
 </table>
 </div>
 <div id="hr" class="switchgroup1">
     <table border="0" class="detailTabLabel" >
	 <tbody>
	 <tr>
	 <td colspan="5" >
		 <table width="100%" style="margin:0px;padding:0px;">
		 <tr> 
		  <td align="center"  class="listwhitetext" width="50%" ><b><u>Origin Country</u></b></td>
		  <td align="center"  class="listwhitetext" width="50%"><b><u>Destination Country</u></b></td> 
		 </tr>
		 </table>
	 </td>
	 </tr>
	 <tr>
        <td align="right" class="listwhitetext" width="225px">Origin HR Person<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
		<td><s:textfield cssClass="input-text" name="customerFile.orderBy" size="30" maxlength="30" onkeydown="return onlyCharsAllowed1(event,this)" tabindex="" /></td>
		<td width="10px"></td>
		<td align="right" class="listwhitetext" width="250px" >Destination HR Person<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
		<td><s:textfield cssClass="input-text" name="customerFile.localHr" size="30" maxlength="30" onkeydown="return onlyCharsAllowed1(event,this)" tabindex="" /></td>
		</tr>
		<tr>
		<td align="right" class="listwhitetext">Phone<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>   
		<td><s:textfield cssClass="input-text" name="customerFile.orderPhone"  size="15" maxlength="20"  tabindex=""/></td>
		<td width="10px"></td>
		<td align="right" class="listwhitetext">Phone<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>   
		<td><s:textfield cssClass="input-text" name="customerFile.localHrPhone"  size="15" maxlength="20"  tabindex=""/></td>
	    <td width="10px"></td>
	    
	    </tr>
	    <tr>
        <td align="right" class="listwhitetext">Email Address<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
		<td colspan="2"><s:textfield cssClass="input-text" name="customerFile.orderEmail" size="30" maxlength="65" tabindex=""  onkeydown="" onchange="emailValidate('customerFileForm','customerFile.orderEmail','Origin Country');"/>
		<img class="openpopup" style="vertical-align:top;" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.orderEmail'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.orderEmail}"/></td>
		
		<td align="right" class="listwhitetext">Email Address<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" size="2">*</font></c:if></td>
		<td ><s:textfield cssClass="input-text" name="customerFile.localHrEmail" size="30" maxlength="65" tabindex=""  onkeydown="" onchange="emailValidate('customerFileForm','customerFile.localHrEmail','Destination Country');"/>
		<img class="openpopup" style="vertical-align:top;" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.localHrEmail'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.localHrEmail}"/></td>
		<td width="10px"></td>

	 </tr>
	 <tr>
        <td align="right" class="listwhitetext"></td>
     </tr>
	 </tbody>
	 </table> 
 </div>
 </td>
 </tr>
 <tr>
 <td height="10" width="100%" colspan="9" align="left" style="margin:0px">
               <div onClick="javascript:animatedcollapse.toggle('removal')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px"> 
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">&nbsp;&nbsp;Removal/ Relocation&nbsp;Services </td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
               </div>
         <div id="removal" class="switchgroup1">
     <table border="0" class="detailTabLabel" cellpadding="0" width="100%">
	 <tbody>
	 <tr>
	<td >
	 <div id="activity" style="margin:0px;padding:0px;border:1px solid 	#C0C0C0; margin-left:0px; margin-bottom:0.3em;">
	 <c:if test="${empty customerFile.id}">
     <c:if test="${customerFile.quotationStatus=='Accepted' || customerFile.quotationStatus=='Rejected' || customerFile.orderIntiationStatus=='Submitted'}">
     <iframe src ="editHoRemovalRelocationService.html?decorator=asml&popup=true&submitCheck=true" height="250px" style="margin-top:0px;padding-top:0px;"  WIDTH="100%" FRAMEBORDER=0 name="relo">
	   <p>Your browser does not support iframes.</p>
	 </iframe>
	 </c:if>
	 <c:if test="${customerFile.quotationStatus!='Accepted' && customerFile.quotationStatus!='Rejected' && customerFile.orderIntiationStatus !='Submitted'}">
     <iframe src ="editHoRemovalRelocationService.html?decorator=asml&popup=true" height="250px" style="margin-top:0px;padding-top:0px;"  WIDTH="100%" FRAMEBORDER=0 name="relo">
	   <p>Your browser does not support iframes.</p>
	 </iframe>
	 </c:if>
	 </c:if>
	 <c:if test="${not empty customerFile.id}">
	 <c:if test="${customerFile.quotationStatus!='Accepted' && customerFile.quotationStatus!='Rejected' && customerFile.orderIntiationStatus!='Submitted'}">
	 <iframe src ="editHoRemovalRelocationService.html?decorator=asml&popup=true&customerFileId=${customerFile.id}" height="250px" style="margin-top:0px;padding-top:0px;" WIDTH="100%" FRAMEBORDER=0 name="relo">
	   <p>Your browser does not support iframes.</p>
	 </iframe>
	 </c:if>
	  <c:if test="${customerFile.quotationStatus=='Accepted' || customerFile.quotationStatus=='Rejected' || customerFile.orderIntiationStatus=='Submitted'}">
	  <iframe src ="editHoRemovalRelocationService.html?decorator=asml&popup=true&customerFileId=${customerFile.id}&submitCheck=true" height="250px" style="margin-top:0px;padding-top:0px;" WIDTH="100%" FRAMEBORDER=0 name="relo">
	   <p>Your browser does not support iframes.</p>
	 </iframe>
	  </c:if>
	 </c:if>
	 </div>
	 </td>
	</tr>
        </table>
         </div>
   </td> 
 </tr>
 
 
               
     
 </div>
   </td> 
 </tr>
 
 
 
 
 </table>
 </td>
 </tr>
</table>
<s:hidden name="customerFile.entitled" /> 
</div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div>
</div>
	</div>
	
	
				<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${customerFile.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${customerFile.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{customerFile.createdBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${customerFile.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${customerFile.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{customerFile.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
				</div>
<s:hidden name="componentId" value="customerFile" /> 

<s:hidden name="hitFlag" />
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/editAgentHoOrder.html?id=${customerFile.id}"  />
		</c:if> 
<s:submit cssClass="cssbuttonA"  key="button.save" tabindex="" onclick="return saveValidation('save') ;"/>	
<input type="button" class="cssbuttonA"  name="submit1" value="Submit" onclick="return saveValidation('submit1') " tabindex=""/>	
<input type="button" class="cssbuttonA"  name="Cancel" value="Cancel" onclick="location.href='<c:url value="/agentHoOrders.html"/>' " tabindex=""/>	





<s:text id="customerFileSystemDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.systemDate" /></s:text>
<td valign="top"><s:hidden name="customerFile.systemDate" value="%{customerFileSystemDateFormattedValue}" /></td>
							
<s:hidden name="sendEmail" />
<s:hidden name="removalRelocationService.isIta60" />
<s:hidden name="removalRelocationService.isIta1m3" />
<s:hidden name="removalRelocationService.isIta2m3" />
<s:hidden name="removalRelocationService.isIta3m3" />
<s:hidden name="removalRelocationService.isFtExpat20" />
<s:hidden name="removalRelocationService.isFtExpat40" />
<s:hidden name="removalRelocationService.isKgExpat30" />
<s:hidden name="removalRelocationService.isTransportAllow" />
<s:hidden name="removalRelocationService.isFtSingle20" />
<s:hidden name="removalRelocationService.isFtFamily40" />
<s:hidden name="removalRelocationService.isStorage" />
<s:hidden name="removalRelocationService.isOrientation8" />
<s:hidden name="removalRelocationService.isOrientation16" />
<s:hidden name="removalRelocationService.isOrientation24" />
<s:hidden name="removalRelocationService.isHomeSearch8" />
<s:hidden name="removalRelocationService.isHomeSearch16" />
<s:hidden name="removalRelocationService.isHomeSearch24" />
<s:hidden name="removalRelocationService.isHomeFurnished" />
<s:hidden name="removalRelocationService.isHomeUnfurnished" />
<s:hidden name="removalRelocationService.isUsdollar" />
<s:hidden name="removalRelocationService.isDollar" />
<s:hidden name="removalRelocationService.isOtherCurrency" />
<s:hidden name="removalRelocationService.isSchoolSearch8" />
<s:hidden name="removalRelocationService.isSchoolSearch16" />
<s:hidden name="removalRelocationService.isSchoolSearch24" />
<s:hidden name="removalRelocationService.isSelfInSearch8" />
<s:hidden name="removalRelocationService.isSelfInSearch16" />
<s:hidden name="removalRelocationService.isSelfInSearch24" />
<s:hidden name="removalRelocationService.id" />
<s:hidden name="removalRelocationService.customerFileId" />
<s:hidden name="removalRelocationService.corpId" />
<s:hidden name="removalRelocationService.usdollarAmounts" />
<s:hidden name="removalRelocationService.dollarAmounts" />
<s:hidden name="removalRelocationService.otherCurrencyAmounts" />
<s:hidden name="removalRelocationService.otherServicesRequired" />
<s:hidden name="removalRelocationService.ftExpat40ExceptionalItems" />
<s:hidden name="removalRelocationService.kgExpat30ExceptionalItems" />
<s:hidden name="removalRelocationService.transportAllowExceptionalItems" />

<s:hidden name="removalRelocationService.orientationTrip" />
<s:hidden name="removalRelocationService.homeSearch" />
<s:hidden name="removalRelocationService.settlingInAssistance" />
<s:hidden name="removalRelocationService.schoolSearch" />
<s:hidden name="removalRelocationService.authorizedDays" />
<s:hidden name="removalRelocationService.additionalNotes" />

<s:hidden name="removalRelocationService.furnished" />
<s:hidden name="removalRelocationService.unFurnished" />
<s:hidden name="removalRelocationService.entryVisa" />
<s:hidden name="removalRelocationService.longTermStayVisa" />
<s:hidden name="removalRelocationService.workPermit" />


<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>

<input type="hidden" name="encryptPass" value="true" />

<s:hidden key="user.id"/>
<s:hidden key="user.version"/> 
</s:form>
<script type="text/javascript"> 
 /*
try{
checkColumn();
}
catch(e){} 
try{
checkAllRelo();
}
catch(e){} 
try{
cheackQuotationStatus('${customerFile.orderAction}');
}
catch(e){} 
try{
cheackVisaRequired();
}
catch(e){} */
checkPetInvolve(document.forms['customerFileForm'].elements['customerFile.petsInvolved']);
try{
	var f = document.getElementById('customerFileForm'); 
	f.setAttribute("autocomplete", "off");
	}
	catch(e){}
try{
	if('${stateshitFlag}'!= 1){
	getNewCoordAndSale();
	}
	}
	catch(e){}

try{ 
 var ocountry=document.forms['customerFileForm'].elements['ocountry'].value; 
 }
 catch(e){}

try{
 var dcountry=document.forms['customerFileForm'].elements['dcountry'].value;
 }
 catch(e){}
 try{
 var hcountry=document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
 }
 catch(e){}

 try{
    if(ocountry == 'India' || ocountry == 'Canada' || ocountry == 'United States'){ 
		document.getElementById('originState').disabled = false; 
		document.getElementById('originState').value='${customerFile.originState}';
		document.getElementById('originStateRequired').style.display = 'block';
		document.getElementById('originStateNotRequired').style.display = 'none';
		
		}else{
			document.getElementById('originState').disabled = true;
			document.getElementById('originState').value ="";
			document.getElementById('originStateRequired').style.display = 'none';
		    document.getElementById('originStateNotRequired').style.display = 'block';
		}
		}
		catch(e){}
try{
 if(dcountry == 'India' || dcountry == 'Canada' || dcountry == 'United States'){ 
		document.getElementById('destinationState').disabled = false; 
		document.getElementById('destinationState').value='${customerFile.destinationState}';
		document.getElementById('destinationStateRequired').style.display = 'block';
		document.getElementById('destinationStateNotRequired').style.display = 'none';
		
		}else{
			document.getElementById('destinationState').disabled = true;
			document.getElementById('destinationState').value ="";
			document.getElementById('destinationStateRequired').style.display = 'none';
		    document.getElementById('destinationStateNotRequired').style.display = 'block';
		}
		}
	    catch(e){}	
	    
try{
 if(hcountry == 'India' || hcountry == 'Canada' || hcountry == 'United States'){ 
		document.getElementById('homeState').disabled = false; 
		document.getElementById('homeState').value='${customerFile.homeState}';
		//document.getElementById('destinationStateRequired').style.display = 'block';
		//document.getElementById('destinationStateNotRequired').style.display = 'none';
		
		}else{
			document.getElementById('homeState').disabled = true;
			document.getElementById('homeState').value ="";
			//document.getElementById('destinationStateRequired').style.display = 'none';
		    //document.getElementById('destinationStateNotRequired').style.display = 'block';
		}
		}
	    catch(e){}
 try  {
 if(document.forms['customerFileForm'].elements['customerFile.assignmentType'].value=="Other"){ 
 document.getElementById('otherContract').style.display = 'block'; 
 }else
 {
 document.getElementById('otherContract').style.display ='none'; 
 }
 
 } 
 catch(e){}   		     
try{
allReadOnly('${customerFile.orderAction}');
}
catch(e){}
function showOrHide(value) {
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	}
}    
showOrHide(0);
setOnSelectBasedMethods([]);
setCalendarFunctionality();
 </script>