<%@ include file="/common/taglibs.jsp"%>  
<head>   
<title><fmt:message key="vanLineList.title"/></title>   
<meta name="heading" content="<fmt:message key='vanLineList.heading'/>"/>   
<script language="javascript" type="text/javascript">
function clear_fields(){
	var i;
		for(i=0;i<=1;i++){
			document.forms['searchForm'].elements['vanLine.agent'].value = "";
			document.forms['searchForm'].elements['vanLine.weekEnding'].value = "";			
			document.forms['searchForm'].elements['vanLine.glType'].value = "";
		}
}
function vanLineRegNumList(regNum,controlFlag,position){
	var agent=document.forms['searchForm'].elements['agent'].value
	var weekEnding=document.forms['searchForm'].elements['weekEnding'].value
	//alert(weekEnding);
	var url="vanLineRegNum.html?vanRegNum="+regNum+"&agent="+agent+"&weekEnding="+weekEnding+"&controlFlag="+controlFlag+"&decorator=simple&popup=true";
	//var url="serviceOrderBySeqNumber.html?sequenceNumber="+sequenceNumber+"&controlFlag="+controlFlag+"&decorator=simple&popup=true";
	ajax_SoTooltip(url,position);	
}

function vanLineListStatusCheck(StatementCategory ,targetElement) 
{
	 
var rowId =targetElement.id;
var StatementCategorycheck = StatementCategory;	 
StatementCategorycheck=StatementCategorycheck.trim()	 
if(targetElement.checked)
  {
	   
   var userCheckStatus = document.forms['searchForm'].elements['vanLineIdCheck'].value;
   var vanLineStatementStatus = document.forms['searchForm'].elements['vanLineStatementCategoryCheck'].value;
   if(vanLineStatementStatus ==',' || vanLineStatementStatus ==',,'){
	   vanLineStatementStatus=document.forms['searchForm'].elements['vanLineStatementCategoryCheck'].value = vanLineStatementStatus.replace( ',' , '' );
	   vanLineStatementStatus=document.forms['searchForm'].elements['vanLineStatementCategoryCheck'].value = vanLineStatementStatus.replace( ',,' , ',' );
	   }
   if(StatementCategorycheck==''){
	   alert("Cannot select blank category.")
	   document.getElementById(rowId).checked=false;
   }else{
   if(vanLineStatementStatus !=''  && vanLineStatementStatus!=StatementCategory){
	   alert("Please select records which belong to similar category.")
	   document.getElementById(rowId).checked=false;
   }else{
   if(userCheckStatus == '')
   {
	  	document.forms['searchForm'].elements['vanLineIdCheck'].value = rowId;
   }
   else
   {
    var userCheckStatus = document.forms['searchForm'].elements['vanLineIdCheck'].value = userCheckStatus + ',' + rowId;
   document.forms['searchForm'].elements['vanLineIdCheck'].value = userCheckStatus.replace( ',,' , ',' );
   }
 
   if(vanLineStatementStatus == '')
   {
	   
	  	document.forms['searchForm'].elements['vanLineStatementCategoryCheck'].value = StatementCategory;
   }
   
   }
 }
 }
if(targetElement.checked==false)
 {
  var userCheckStatus = document.forms['searchForm'].elements['vanLineIdCheck'].value;
  var userCheckStatus=document.forms['searchForm'].elements['vanLineIdCheck'].value = userCheckStatus.replace( rowId , '' );
  document.forms['searchForm'].elements['vanLineIdCheck'].value = userCheckStatus.replace( ',,' , ',' );
  
  var accountIdCheck1 = document.forms['searchForm'].elements['vanLineIdCheck'].value;
  accountIdCheck1=accountIdCheck1.trim(); 
  if(accountIdCheck1=='')
  {
	  document.forms['searchForm'].elements['vanLineStatementCategoryCheck'].value="";
  }
  else if(accountIdCheck1==',')
  {
	  document.forms['searchForm'].elements['vanLineStatementCategoryCheck'].value="";
  }
  else if(accountIdCheck1==',,')
  {
	  document.forms['searchForm'].elements['vanLineStatementCategoryCheck'].value="";
  }

  } 
accountIdCheck(); 
 }
function accountIdCheck(){
    var accountIdCheck = document.forms['searchForm'].elements['vanLineIdCheck'].value;
    accountIdCheck=accountIdCheck.trim(); 
    if(accountIdCheck=='')
    {
    document.forms['searchForm'].elements['bulkProcess'].disabled=true;
    }
    else if(accountIdCheck==',')
    {
    document.forms['searchForm'].elements['bulkProcess'].disabled=true;
    }
    else if(accountIdCheck!='')
    {
      document.forms['searchForm'].elements['bulkProcess'].disabled=false;
    }
    }

function vanLineListBulkProcess() 
{
	var vanLineStatementStatus = document.forms['searchForm'].elements['vanLineStatementCategoryCheck'].value;
	var res = vanLineStatementStatus.split(","); 
	var statementCategory="";
	statementCategory=res[0] 
	for(i=0;i<=res.length-1;i++) { 
	if(statementCategory!=res[i]){
		statementCategory=statementCategory+""+res[i]	
	}	 
	}
	if(statementCategory.length>6){
		alert("Please select records which belong to similar category.")
	}else{
		var chkComp="";
  	    <configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
  	    chkComp=document.forms['searchForm'].elements['glCodeFlag'].value;
  	    </configByCorp:fieldVisibility>
		var vanLineIdCheck = document.forms['searchForm'].elements['vanLineIdCheck'].value;
		var agent=document.forms['searchForm'].elements['agent'].value
		var weekEnding=document.forms['searchForm'].elements['weekEnding'].value
		openWindow('editMiscellaneousSattlementList.html?vanLineIdCheck='+vanLineIdCheck+'&agent='+agent+'&weekEnding='+weekEnding+'&glCodeFlag='+chkComp+'&decorator=popup&popup=true');
		 
	}
}

function checkColumn(){
	var userCheckStatus ="";
	  userCheckStatus = document.forms['searchForm'].elements['vanLineIdCheck'].value; 
	if(userCheckStatus!=""){ 
	document.forms['searchForm'].elements['vanLineIdCheck'].value="";
	var column = userCheckStatus.split(",");  
	for(i = 0; i<column.length ; i++)
	{
	     var userCheckStatus = document.forms['searchForm'].elements['vanLineIdCheck'].value;  
	     var userCheckStatus=	document.forms['searchForm'].elements['vanLineIdCheck'].value = userCheckStatus + ',' + column[i];   
	     document.getElementById(column[i]).checked=true;
	} 
	}
	if(userCheckStatus==""){

	}
	}
function pageReload(){ 
	resetCategoryCheck();
	window.location.reload(true);
}

function resetCategoryCheck(){
	document.forms['searchForm'].elements['vanLineIdCheck'].value='';
	document.forms['searchForm'].elements['vanLineStatementCategoryCheck'].value='';
	var len = document.forms['searchForm'].elements['checkV'].length;
	for (i = 0; i < len; i++){	
	document.forms['searchForm'].elements['checkV'][i].checked = false ;  
	}
	accountIdCheck();
}
</script>
<style>
	<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="JavaScript" type="text/javascript">
	var cal = new CalendarPopup(); 
	cal.showYearNavigation(); 
	cal.showYearNavigationInput();  
</script>

<style>
 span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:95%;
!width:95%;
}
</style>
</head>   
  
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editMiscellaneousWithoutVanLine.html?glCodeFlag=${glCodeFlag}"/>'"  value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px" align="top" method="searchVanLine" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>   

     
<s:form id="searchForm" name="vanLineWithoutSearchForm" action="searchVanLine" method="post" validate="true" >
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
	<s:hidden name="glCodeFlag" value="YES"/>
</configByCorp:fieldVisibility>
<s:hidden name="vanLineIdCheck" />
<s:hidden name="vanLineStatementCategoryCheck" /> 
<c:set var="totalamt" value="0.00"/>
<c:set var="totalamt1" value="0.00"/>
<c:set var="totalamt111" value="0.00"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="agent" value="<%=request.getParameter("agent") %>"/>
<c:set var="agent" value="<%=request.getParameter("agent") %>"/>
<s:hidden name="weekEnding" value="<%=request.getParameter("weekEnding") %>"/>
<c:set var="weekEnding" value="<%=request.getParameter("weekEnding") %>"/>
		<div id="newmnav">
		  <ul>
		  <li><a href="vanLineListWithoutReconcile.html?chkWith=${chkWith}&agent=${agent}&weekEnding=${weekEnding}"><span>Order Settlement</span></a></li>
		      <li id="newmnav1" style="background:#FFF "><a class="current"><span>Miscellaneous Settlement<img src="images/navarrow.gif" align="absmiddle" /></span></a>
		      <li><a href="categoryWithSettlementList.html?chkWith=${chkWith}&agent=${agent}&weekEnding=${weekEnding}"><span>Category Settlement</span></a></li>
		      </li>
		  	
		  </ul>
</div><div class="spn" style="!margin-bottom:0px;">&nbsp;</div>
      <!--<input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/orderSettlementList.html"/>'"  value="Order Settlement"/> 
        <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/miscellaneousSettlementList.html"/>'"  value="Miscellaneous Settlement"/> 
   <s:set name="vanLineList" value="vanLineList" scope="request"/>   
     -->
<display:table name="vanLineList" class="table" requestURI=""   id="vanLineList" export="fale "   style="width:100%;margin:0px;" >   
<display:column title=""   style="width:10px">  
	   <input type="checkbox" value="${vanLineList.id}" name="checkV" id="${vanLineList.id}" style="margin-left:10px;"  onclick="vanLineListStatusCheck('${vanLineList.statementCategory}',this)" />
        </display:column>
<display:column property="statementCategory" sortable="true"  titleKey="vanLine.statementCategory" style="width:80px" />
	<display:column titleKey="vanLine.agent" style="text-align: center;width:60px">
	<a href="#" onclick="setUrl(this,${vanLineList.id})" target="_blank" ><c:out value="${vanLineList.agent}" /></a> 
	</display:column>	
	<display:column property="weekEnding" sortable="true" titleKey="vanLine.weekEnding" style="width:30px" format="{0,date,dd-MMM-yyyy}"/>	
	<display:column property="description" sortable="true" titleKey="vanLine.description" style="width:250px"/>
	<display:column property="driverName" sortable="true" title="Driver Name" style="width:150px" ></display:column>
	<display:column property="reconcileStatus"  title="Reconcile Status" style="width:200px"/>
	<display:column property="amtDueAgent" sortable="true" titleKey="vanLine.amtDueAgent" style="text-align: right; width:80px;">
	<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${vanLineList.amtDueAgent}"  /> </display:column>
	<c:if test="${flagvanLineData ne null && flagvanLineData=='1'}">
	 <c:set var="totalamt" value="${(vanLineList.totalAllStatusAmount)}"/>
	 </c:if> 
	 <c:if test="${flagvanLineData ne null && flagvanLineData=='1'}">
	<c:set var="totalamt1" value="${(vanLineList.reconcileStatusAmount)}"/> 
	</c:if>
	<c:if test="${flagvanLineData ne null && flagvanLineData=='1'}">
	<c:set var="totalamt111" value="${(vanLineList.suspenseStatusAmount)}"/> 
	</c:if> 
	<display:column property="reconcileAmount" sortable="true" titleKey="vanLine.reconcileAmount" style="text-align: right; width:80px;"/>		
	
    <display:setProperty name="paging.banner.item_name" value="vanLine"/>   
    <display:setProperty name="paging.banner.items_name" value="vanLines"/>    
    <display:setProperty name="export.excel.filename" value="Van Line List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Van Line List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Van Line List.pdf"/>   
</display:table> 
<!-- Modification Start For #7921 29 Aprl -->
<div style="background:url(images/bg_listheader.png)repeat-x scroll 0 0 #BCD2EF;height:53px;font-size:13px;font-weight:bold;margin-bottom:5px;border:2px solid #74B3DC;border-top:none;">
 	<table width="100%">
    <tr>
     <td colspan="4" align="right" class="tdheight-footer" style="border-right:hidden;">&nbsp;</td>     
     <td align="right" style="padding:0px;width:80.5%;" class="tdheight-footer">Amount(All Statuses)&nbsp;</td>
     <td class="tdheight-footer" width="12%" align="right" style="padding-right:14px;">
     <%-- <fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${totalamt}" /> --%>
             <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalamt}"  />
            </td>  	     
     <td align="right" width="12%" class="tdheight-footer"></td>  
   </tr>
    <tr>
     <td colspan="4" align="right" class="tdheight-footer" style="border-right:hidden;">&nbsp;</td>     
     <td align="right" style="padding:0px;width:80.5%;" class="tdheight-footer">Amount(Reconciled/Approved)&nbsp;</td>
     <td class="tdheight-footer" width="12%" align="right" style="padding-right:14px;">
    <%--  <fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${totalamt1}" /> --%>
            <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalamt1}"  />
            </td>  	     
     <td align="right" width="12%" class="tdheight-footer"></td>  
   </tr>
    <tr>
     <td colspan="4" align="right" class="tdheight-footer" style="border-right:hidden;">&nbsp;</td>     
     <td align="right" style="padding:0px;width:80.5%;" class="tdheight-footer">Amount(Suspense)&nbsp;</td>
     <td class="tdheight-footer" width="12%" align="right" style="padding-right:14px;">
    <%--  <fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${totalamt1}" /> --%>
            <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalamt111}"  />
            </td>  	     
     <td align="right" width="12%" class="tdheight-footer"></td>  
   </tr>
   </table> 
    </div>
<!-- Modification End -->  
  
<c:out value="${buttons}" escapeXml="false" />
 <input type="button" class="cssbuttonA" style="width:100px; height:25px" name="bulkProcess" onclick="vanLineListBulkProcess();"  value="Bulk Process"/> 
 <input type="button" class="cssbuttonA" style="width:50px; height:25px" name="Reset" onclick="resetCategoryCheck();"  value="Reset"/>         
<c:set var="isTrue" value="false" scope="session"/>
</s:form>  
 <script type="text/javascript">      
      function setUrl(aref,id11){
    	  var chkComp="";
    	  <configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
    	  chkComp=document.forms['vanLineWithoutSearchForm'].elements['glCodeFlag'].value;
    	  </configByCorp:fieldVisibility>
    	  aref.href='editMiscellaneousWithoutVanLine.html?id='+id11+'&glCodeFlag='+chkComp;
      }
     
      
      </script>
 
<script type="text/javascript">   
accountIdCheck();
//highlightTableRows("vanLineList");  
</script> 