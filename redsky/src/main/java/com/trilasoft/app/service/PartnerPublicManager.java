package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.PartnerPublic;

public interface PartnerPublicManager extends GenericManager<PartnerPublic, Long> {

	public List getPartnerPublicList(String partnerType,String corpID,String firstName, String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode, String status, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, Boolean isIgnoreInactive, String extRefernece,String vanlineCode ,String typeOfVendor, boolean cmmdmmflag, String fidiNumber,String OMNINumber,String AMSANumber,String WERCNumber,String IAMNumber,String utsNumber ,String eurovanNetwork ,String PAIMA,String LACMA);
	public List getPartnerPublicListByPartnerView(String partnerType,String corpID,String firstName, String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode, String status, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, Boolean isIgnoreInactive, String extRefernece);
		
	public List findPartnerCode(String partnerCode);
	public List findPartnerpublicCode(String partnerCode);
	
	public List findMaxByCode(String initial);
	
	public List findPartnerId(String partnerCode, String sessionCorpID);

	public PartnerPublic getByID(Long id);

	public List getPartnerPublicMergeList(String partnerType, String sessionCorpID, String firstName, String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String status, Boolean isPrivateParty, Boolean isAccount, Boolean isAgent, Boolean isVendor, Boolean isCarrier, Boolean isOwnerOp, String city);

	public StringBuffer updateMaregedParter(String userCheck, String partnerCode, String corpID);

	public PartnerPublic getPartnerByCode(String partnerCode);

	public List getAgentBaseByPartnerCode(String partnerCode, String corpID);

	public List getAgentBaseSCACByPartnerCode(String partnerCode, String baseCode, String corpID);

	public List getAccLineRefByPartnerCode(String partnerCode, String corpID);

	public List getAccContactByPartnerCode(String partnerCode, String corpID);

	public List getAccProfileByPartnerCode(String partnerCode, String corpID);

	public List getContPolicyByPartnerCode(String partnerCode, String corpID);
	
	public List getFAQByPartnerCode(String partnerCode, String corpID);
	
	public List getPolicyByPartnerCode(String partnerCode, String corpID);

	public StringBuffer updatePartnerVanLineRef(String userCheck, String partnerCode, String corpID, String partnerCodewithoutMaster, String masterPartnerCode);

	public StringBuffer updateCodes(String userCheck, String partnerCode, String lastName, String corpID);

	public List isExistAccContact(String corpID, String partnerCode);

	public List isExistAccProfile(String corpID, String partnerCode);

	public List isExistContractPolicy(String corpID, String partnerCode);

	public void updateStatus(String partnerCode);
	public List  findOwnerOperatorList(String parentId,String corpId);

	public StringBuffer updateMaregedParterPrivate(String userCheck, String partnerCode, Long id);

	public StringBuffer updateMaregedParterSameCorpid(String userCheck, String partnerCode, String corpID);
	public void deleteAgentParent(Long id,String partnerCode,String corpID);

	public int updateConvertPartnerCorpid(String userCheck);
	public List getPartnerPublicDefaultAccount(String partnerCode,String corpId);
	public String findPartnerCodeCount(String partnerCode);
	public List getFindDontNerge(String corpId,String partnerCode);
	public String getCountryCode(String billingCountryCode, String parameter,String sessionCorpID);

	public List getUserNameList(String sessionCorpID, String partnerCode);
	public List getPartnerChildList(String sessionCorpID, String partnerCode);
	public Map<String, String> getPartnerChildMap(String sessionCorpID, Long partnerCode);
	public Boolean getExcludeFPU(String partnerCode,String corpId);
	public List getPartnerPublicChildAccount(String partnerCode,String corpId);
	public List getVanLine(String  partnerVanLineCode, String sessionCorpID);
	public Map<String, String> getCountryWithBranch(String parameter,String sessionCorpID);
	public Boolean getPrivatePartyStatus(String billToCode, String corpId);
	public List agentUpdatedIn24Hour();
	public void updateDefaultAccountLine(String corpId,String partnerCode,String partnerType,String userName);
	public PartnerPublic checkById(Long id);
	public int updateAgentParentName(String lastName,String partnerCode,String updatedBy) ;
	public void updatePartnerPortal(String partnerCode);
}
