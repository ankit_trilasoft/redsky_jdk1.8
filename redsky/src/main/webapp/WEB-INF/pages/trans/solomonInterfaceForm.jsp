<%@ include file="/common/taglibs.jsp"%> 

<head>

	<title>Dynamic&nbsp;Interface</title>   
    <meta name="heading" content="Dynamic Interface"/> 
<style type="text/css">
		h2 {background-color: #FBBFFF}
		
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">

</script>
	<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 
<style type="text/css"> 
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; } 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;} 
</style> 
</head>
<body style="background-color:#444444;">
<s:form id="imfEstimateNew" name="imfEstimateNew" action="" method="post" validate="true">
<c:set var="dynamicInterfaceMerge" value="F"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.dynamicInterfaceMerge">
<c:set var="dynamicInterfaceMerge" value="T"/>
</configByCorp:fieldVisibility>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:85% " >
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top: 10px;!margin-top: -2px"><span></span></div>
   <div class="center-content">
<table cellspacing="1" cellpadding="1" border="0" > 
<tr><td height="10px"></td></tr>
<tr>
<td width="10px;"></td>
<td class="bgblue" >Dynamic&nbsp;Interface</td>
</tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px" border="0">
<tr>
<td width="10px"></td>  
<td>
					<c:if test="${dynamicInterfaceMerge!='T'}">
<input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/invoiceCompanyCode.html"/>'"  
        			value="Invoice Data Extract"/>
        			</c:if> 
        			<c:if test="${dynamicInterfaceMerge=='T'}">
<input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/invoiceMergeCompanyCode.html"/>'"  
        			value="Invoice Data Extract"/> 
        			</c:if>
        			</td>

<td width="10px"></td>
<td><input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/partnerExtracts.html"/>'"  
        			value="Partner Data Extract"/> </td>


</tr>
<tr><td height="3px"></td></tr>
<tr>     			
<td width="10px"></td>
					<td>
					<c:if test="${dynamicInterfaceMerge!='T'}">
					<input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/payableCompanyCode.html"/>'"  
        			value="Payable Data Extract "/>
        			</c:if>
        			<c:if test="${dynamicInterfaceMerge=='T'}">
					<input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/payableMergeCompanyCode.html"/>'"  
        			value="Payable Data Extract"/>
        			</c:if>
        			</td>
<td width="10px"></td>
<td><input type="button" class="cssbutton" style="width:180px; height:25px" 
        			onclick="location.href='<c:url value="/paymentApplicationExtract.html"/>'"  
        			value="Payment Application Extract"/> </td>        			
</tr>
<tr><td height="3px"></td></tr>
<tr>       			
<td width="10px"></td>
<td><input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/solomonsPmtUpdate.html"/>'"  
        			value="Dynamic Pmt Update "/></td>
<td width="10px"></td>
<td><input type="button" class="cssbutton" style="width:180px; height:25px" 
        			onclick="location.href='<c:url value="/creditMemoExtract.html"/>'"  
        			value="Credit Memo Extract "/></td>
</tr>
<tr><td height="3px"></td></tr>
<tr>
<td width="10px"></td>
<td>
<c:if test="${dynamicInterfaceMerge!='T'}">
<input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/subcontrCompanyCode.html"/>'"  
        			value="Driver Settlement"/>
</c:if> 
<c:if test="${dynamicInterfaceMerge=='T'}">
<input type="button" class="cssbutton" style="width:145px; height:25px" 
        			onclick="location.href='<c:url value="/subcontrMergeCompanyCode.html"/>'"  
        			value="Driver Settlement"/>
</c:if>       			
</td>
        			
</tr>
<tr>
<td width="10px"></td>
<td width="10px"></td>
<td width="10px"></td>
</tr>

</table>
</td></tr>
 <tr>
<td width="10px"></td>
<td class="bgblue" >Regenerate Extract</td>
</tr>
<tr><td height="5px"></td></tr>
<tr>
<td width="20px"></td>
<td>
<table>
	<tr>
		<td width="10px"></td>
		<td><input type="button" class="cssbutton" style="width:200px; height:25px" 
        	onclick="location.href='<c:url value="/regenerateSolomonSalesInvoiceForm.html"/>'"  
        	value="Regenerate Invoice Data Extract " disabled="disabled"/></td>
	</tr>
	<tr>
<td width="10px"></td>
</tr>
	<tr><td width="10px"></td>
		<td><input type="button" class="cssbutton" style="width:200px; height:25px" 
        	onclick="location.href='<c:url value="/regeneratePayableSolomonProcessing.html"/>'"  
        	value="Regenerate Payable Data Extract " disabled="disabled"/></td>
	</tr>
</table>
</td>
</tr>

</table></div>
<div class="bottom-header"><span></span></div> 

</div></div></div></s:form></body> 