package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.UserDataSecurityDao;
import com.trilasoft.app.model.UserDataSecurity;

public class UserDataSecurityDaoHibernate extends GenericDaoHibernate<UserDataSecurity, Long> implements UserDataSecurityDao{

	public UserDataSecurityDaoHibernate() {
		super(UserDataSecurity.class);
		
	}

	public List findById(Long id) {
		return null;
	}

}
