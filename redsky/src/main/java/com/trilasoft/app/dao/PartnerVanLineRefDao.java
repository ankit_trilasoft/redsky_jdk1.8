package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.PartnerVanLineRef;

public interface PartnerVanLineRefDao extends GenericDao<PartnerVanLineRef, Long> {

	public List getPartnerVanLineReferenceList(String corpId, String partnerCodeForRef);

	public List getpartner(String partnerCode, String sessionCorpID);

	public List checkJobType(String vanLineCode, String corpID, String jobType);

	public int updateDeleteStatus(Long ids);
}
