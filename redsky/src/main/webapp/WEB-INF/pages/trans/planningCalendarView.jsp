<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Planning Calendar</title>
<meta name="heading" content="Planning Calendar" charset='utf-8'/>	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<head >
<!-- Modified script 12-Dec-2013 --> 
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/fullcalendar/fullcalendarcustom.css'/>" /> 
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/fullcalendar/fullcalendarcustom.print.css'/>" media='print'/> 
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.9.2/themes/cupertino/jquery-ui.css" />	
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/lib/moment.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/lib/jquerycustom.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/styles/fullcalendar/fullcalendarcustom.min.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />	
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/font-awesome.min.css'/>" />
<!-- Modification closed here -->
<script type="text/javascript">
$(document).ready(function() {
	$('#calendar').fullCalendar({
		editable: false,
					
		defaultView: 'basicWeek',
	   
	theme:true,
		header: {
		left: 'prev,next today',
		center: 'title',
		right: 'month,basicWeek,basicDay'
		},
	    eventSources: [
	       	        {
	    	            events: [ 
	    	                    
	    	                          
	    				<c:forEach var="entry" items="${planningCalendarViewList}" varStatus="rowCounter"> 
	    				<c:if test="${rowCounter.count>1}">
	    				,
	    				</c:if>
	    	                {
		                    title  : "${entry.title}",
		                    start  : "${entry.start}T00:00:00",
		                    end    : "${entry.end}T23:58:00",
			                color  : "#C3CCF1"
	    	                }
	    	            </c:forEach>
	    	            
	    	            ]

	    	        }
	    ],
	    eventRender: function(event, element) {
	        element.find(element.find(".fc-title").html("<span style='color:#000;font-size:11px;line-height:14px; '>"+event.title+"</span>"));
	    }
		});
	
	var mycal = $('#calendar td.fc-today');
	if(mycal){
	 var position = $(mycal).position();
	 if(position){
	window.scrollTo(0, position.top);
	 }
	}
	$("#calendar .fc-button").click(function(){ 
		var mycal1 = $('#calendar td.fc-today');
		if(mycal1){
		 	var position1 = $(mycal1).position();
		 	if(position1){
			window.scrollTo(0, position1.top);
		 	}
		}
		var view = $('#calendar').fullCalendar('getView');
		document.getElementById("type").value=view.name;
	    var view = $('#calendar').fullCalendar('getView');
	    if(view.name!='month'){
	    	$(window).scrollTop( 0 );
	    }
	})
	});
	</script>	
</head>
<style> <%@ include file="/common/calenderStyle.css"%> 

</style>
<style type="text/css">

.fc-popover div.fc-content{text-align:left !important}

.fc-day-grid-container { height : auto !important}

.fc-time{ display:none !important}
.fc-popover {width:385px !important; }

.fc-popover .fc-more-popover { margin-right:10px !important}

.fc-toolbar .fc-state-active, .fc-toolbar .ui-state-active {
    z-index: 1 !important;
}
.fc-view-container {
	overflow-y: auto !important;
	overflow-x: hidden !important;
    height: 652px !important;
}
.fc-more-popover .fc-event-container {
    padding: 10px !important;
    padding-right: 2px !important;
    overflow-y: auto !important;
    height: 400px !important;
}
.fc button {
    
    width: auto !important;
}

td.ui-widget-header table{ margin:0px !important; }

.fc-title{ white-space: normal !important;}

 #calendar {
  width: 100%; /*  width: 900px; to 100% */
/*  height: 350px; */
  height: auto;
 /* margin: 0 auto;
  margin-top: 75px; */
  margin:0px;
  text-align: center;
  font-size: 12px;
  font-family: verdana,arial,helvetica,sans-serif;
  font-color: 'blue';
  } 
  
  a{color: #2779aa}
  a:hover{color: #ff0000}
  a:active{color: #2779aa}
	h2 {
    background: #d7ebf9 url("images/ui-bg_glass_80_d7ebf9_1x400.png") repeat-x scroll 50% 50%;
    border: 1px solid #aed0ea;
    border-radius: 3px;
    color: #2779aa;
    font-size: 17px !important;
    padding: 3px;
}
	.subcontent-tab {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;font-weight: bold;
		color: #15428B;text-decoration: none;
		background:url(images/collapsebg.gif) #BCD2EF;
		padding:4px 3px 1px 5px; height:15px;
		width:598px; 
		border:1px solid #99BBE8; 
		border-right:none; 
		border-left:none
	} 
	a.dsphead{
		text-decoration:none;
		color:#000000;
	}
	.dspchar2{
		padding-left:0px;
	}
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}
	.bgblue{
		background:url(images/blue_band.jpg); 
		height: 30px; 
		width:630px; 
		background-repeat: no-repeat;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		font-weight: bold;
		color: #25383c; 
		padding-left: 40px; 
	}
	.fc-state-highlight {
	background: #808080;
	}
	.fc-event-inner {
    text-align: left;
	}
	.fc-grid .fc-day-content {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0 !important;
}
	.fc-event {
   /* background-color: #CDDEE7 !important;
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#f0f9ff+0,cddee7+0,b9d6e5+100 */
background: #f0f9ff; 
background: -moz-linear-gradient(top,  #f0f9ff 0%, #dae6f2 47%, #b6d1ee 100%) !important; 
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f0f9ff), color-stop(47%,#dae6f2), color-stop(100%,#b6d1ee)) !important; 
background: -webkit-linear-gradient(top,  #f0f9ff 0%,#dae6f2 47%,#b6d1ee 100%) !important;
background: -o-linear-gradient(top,  #f0f9ff 0%,#dae6f2 47%,#b6d1ee 100%) !important; 
background: -ms-linear-gradient(top,  #f0f9ff 0%,#dae6f2 47%,#b6d1ee 100%) !important; 
background: linear-gradient(to bottom,  #f0f9ff 0%,#dae6f2 47%,#b6d1ee 100%) !important; 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f0f9ff', endColorstr='#b6d1ee',GradientType=0 ) !important; 
border :1px solid #6a859a !important;    
    
    border: 1px solid #6BA5C1 !important;
    color: #000000;
    cursor: default;
    font-size: 0.85em;
    margin-top:1px !important;
    margin-bottom:1px !important;
}

.fc-grid .fc-day-content { background: #fff none repeat scroll 0 0;}

.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
background: #edd8a9 !important; 
background: -moz-linear-gradient(top,  #edd8a9 0%, #f4ab44 48%, #ffd672 100%) !important; 
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#edd8a9), color-stop(48%,#f4ab44), color-stop(100%,#ffd672)) !important; 
background: -webkit-linear-gradient(top,  #edd8a9 0%,#f4ab44 48%,#ffd672 100%) !important; 
background: -o-linear-gradient(top,  #edd8a9 0%,#f4ab44 48%,#ffd672 100%) !important; 
background: -ms-linear-gradient(top,  #edd8a9 0%,#f4ab44 48%,#ffd672 100%) !important; 
background: linear-gradient(to bottom,  #edd8a9 0%,#f4ab44 48%,#ffd672 100%) !important; 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#edd8a9', endColorstr='#ffd672',GradientType=0 ) !important;
 border: 1px solid orange;
 color: #444;
 font-weight: normal;
}
form { margin-top:18px;}
.scrollup{ position:fixed; right:20px; top:500px  }
.fc-center h2{line-height:1.5 !important;}
</style>

<script type="text/javascript">
function opsCalendar(){
	
	location.href = 'opsCalendar.html?decorator=popup&popup=true';
}
function dynamicPricingForm(){
	location.href = 'dynamicPricingCalenderView.html?decorator=popup&popup=true';
}
function crewCalendarForm(){
	location.href = 'crewCalenderView.html?decorator=popup&popup=true';
}

function planningCalendar(){
	var checkForAppendFields = 'No';
	<configByCorp:fieldVisibility componentId="component.partner.rutTaxNumber">
		checkForAppendFields ="Yes";
	</configByCorp:fieldVisibility>
	location.href = 'planningCalendarList.html?decorator=popup&popup=true&checkForAppendFields='+checkForAppendFields;
}
function workPlanning(){
	
	location.href = 'workPlan.html?decorator=popup&popup=true';
}
function refreshCalendar(){
	var wHouse=document.forms['priceCalendarForm'].elements['wHouse'].value;
	var hubID=document.forms['priceCalendarForm'].elements['hubID'].value;
	var type=document.forms['priceCalendarForm'].elements['calendarViewStyle'].value;


	var checkForAppendFields = 'No';
	<configByCorp:fieldVisibility componentId="component.partner.rutTaxNumber">
		checkForAppendFields ="Yes";
	</configByCorp:fieldVisibility>
	location.href = 'planningCalendarList.html?decorator=popup&popup=true&wHouse='+wHouse+'&hubID='+hubID+'&type='+type+'&checkForAppendFields='+checkForAppendFields;
}
function makeWarehouseList(tValue){
	var target=tValue;
	var key='';
	var value='';
	var lineFile='';
	var tempFile='';
	<c:forEach var="entry" items="${houseHub}">
	key="${entry.key}";
	value="${entry.value}";
	
	if(key.trim()==target.trim()){
		tempFile=value.split(",");
		for(var i=0;i<tempFile.length;i++){
			if(lineFile==''){
				value=tempFile[i];
				value=value.replace("'","");
				value=value.replace("'","");
				lineFile=value;
			}else{
				value=tempFile[i];
				value=value.replace("'","");
				value=value.replace("'","");
				lineFile=lineFile+","+value;				
			}
		}
	}
	</c:forEach>
	if(lineFile!=''){
		var finalList=lineFile.split(",");
		var targetElement = document.forms['priceCalendarForm'].elements['wHouse'];
		targetElement.length = finalList.length+1;
		document.forms['priceCalendarForm'].elements['wHouse'].options[0].text = '';
		document.forms['priceCalendarForm'].elements['wHouse'].options[0].value = '';
		for(i=0;i<finalList.length;i++){
			 key=finalList[i];
			 value=findValueInHouse(finalList[i]);			
			   document.forms['priceCalendarForm'].elements['wHouse'].options[i+1].text=value;
			   document.forms['priceCalendarForm'].elements['wHouse'].options[i+1].value=key;
		}
	}else{
		var targetElement = document.forms['priceCalendarForm'].elements['wHouse'];
		targetElement.length = houseLength()+1;
		document.forms['priceCalendarForm'].elements['wHouse'].options[0].text = '';
		document.forms['priceCalendarForm'].elements['wHouse'].options[0].value = '';
		var j=0;
		<c:forEach var="entry" items="${house}">
				key="${entry.key}";
				value="${entry.value}";
			   document.forms['priceCalendarForm'].elements['wHouse'].options[j+1].text=value;
			   document.forms['priceCalendarForm'].elements['wHouse'].options[j+1].value=key;
			   j++;
		</c:forEach>	
		document.forms['priceCalendarForm'].elements['wHouse'].value='';
	}
	<c:if test="${not empty wHouse}">
	document.forms['priceCalendarForm'].elements['wHouse'].value='${wHouse}';
	</c:if>
}
function houseLength(){
	var cont=0;
	var key='';
	<c:forEach var="entry" items="${house}">
	key="${entry.key}";
	if(key!=''){
		cont++;
	}	
	</c:forEach>	
	return cont;
}
function findValueInHouse(key1){
	var key='';
	var value='';
	<c:forEach var="entry" items="${house}">
	if(value==''){
		key="${entry.key}";
		if(key==key1){
		value="${entry.value}";	
		}
	}	
	</c:forEach>	
	return value;
}

function printPlanningCalendar(){
    var view = $('#calendar').fullCalendar('getView');
	var momentss = $('#calendar').fullCalendar('getDate');
	var end_date  =   $('#calendar').fullCalendar('getView').end;
	var end=moment(end_date).subtract(1, "days").format('YYYY-MM-DD');
    var checkForAppendFields = 'No';
	<configByCorp:fieldVisibility componentId="component.partner.rutTaxNumber">
		checkForAppendFields ="Yes";
	</configByCorp:fieldVisibility>
	var wHouse = document.forms['priceCalendarForm'].elements['wHouse'].value;
	var hubID = document.forms['priceCalendarForm'].elements['hubID'].value;
	var calendarViewStyle = document.forms['priceCalendarForm'].elements['calendarViewStyle'].value;
	javascript:openWindow("printPlanningCalendarList.html?checkForAppendFields="+checkForAppendFields+"&wHouse="+wHouse+"&printStart="+momentss.format('YYYY-MM-DD')+"&printEnd="+end+"&hubID="+hubID+"&calendarViewStyle="+calendarViewStyle+"&decorator=popup&popup=true",1000,600)
}
</script>

</head>

<s:form name="priceCalendarForm" id="priceCalendarForm" action="" method="post" validate="true">

<s:hidden name="calendarViewStyle" />
<c:set var="FormDateValue" value="dd-NNN-yy"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="type" id="type"/>
<div id="layer1" style="width:100%; margin: 0px auto;">
<div id="newmnav" style="float:left;">
	<ul>
		<li><a onclick="opsCalendar();"><span>Ops Calendar</span></a></li>
		<configByCorp:fieldVisibility componentId="component.field.Resource.DynamicPricingView">
		<li><a onclick="dynamicPricingForm();"><span>Price Calendar</span></a></li>
		</configByCorp:fieldVisibility>
		<c:if test="${checkCrewListRole == true && hubWarehouseLimit=='Crw'}">
			<li><a onclick="crewCalendarForm();"><span>Crew Calendar</span></a></li>
		</c:if>
		<li><a onclick="workPlanning();"><span>Work Planning</span></a></li>	
		<li id="newmnav1" style="background:#FFF"><a class="current"><span>Planning Calendar</span></a></li>
	</ul>	
	</div>
	<div style="float:right;position: relative;top: -8px;right: 20px;">
	<table class="" cellspacing="1" cellpadding="2" border="0" style="margin:0px" width="100%">
	<tr>		  		

		  	<td align="right" width="20px" class="listwhitetext">Hub</td>
	  		<td align="left" class="listwhitetext" width="100px"><s:select cssClass="list-menu" name="hubID" list="%{hub}" cssStyle="width:100px;height:21px;" headerKey="" headerValue="" onchange="makeWarehouseList(this.value);refreshCalendar();"/></td>
	  		
	  		
			<td align="right" width="63px" class="listwhitetext" >Warehouse</td>		  
	  	  	<td width="160px" > 
	  	  	<s:select cssClass="list-menu" name="wHouse" list="%{house}" cssStyle="width:175px;height:21px;" headerKey="" headerValue=""  onchange="refreshCalendar();"/>	 
		  		</td>
			<!-- <td width="90"> <input type="button" class="cssbutton" style="margin-right: 5px;width:85px;" value="Reload" onclick="refreshCalendar();"/></td>
			<td><input type="button" class="cssbutton" style="margin-right:5px;width:65px;" value="Print" onclick="printPlanningCalendar();"/></td>	 -->
			<td width="70"><button type="button" class="btn btn-xs btn-success btn-blue" style="width:auto;padding:3px 8px;box-shadow: 0 2px 3px 0 rgba(0,0,0,.16),0 2px 5px 0 rgba(0,0,0,.12) !important;"  title="Reload" onclick="refreshCalendar();">Reload <i class="fa fa-refresh"></i></button></td>
			<td><button type="button" class="btn btn-xs btn-success" style="width:auto;padding:6px 8px;box-shadow: 0 2px 3px 0 rgba(0,0,0,.16),0 2px 5px 0 rgba(0,0,0,.12) !important;"  title="Print" onclick="printPlanningCalendar();"><i class="fa fa-print"></i></button></td>  		
	</tr>	 
		 				
</table>
	</div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 12px;!margin-top: -4px"><span></span></div>
   <div class="center-content">


   	<div id="calendar" style="min-height:130px;">
<div width="100%" style="border:0px">
<img id="scrollup" style="float:right" src="${pageContext.request.contextPath}/images/top.png" title="Go To Top" class="scrollup" />
</div>
</div>

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
<script type="text/javascript">
<%--<c:if test="${empty hubID}">
document.forms['priceCalendarForm'].elements['hubID'].value='${defaultHub}';
</c:if>--%>
<c:if test="${not empty hubID}">
	makeWarehouseList('${hubID}');
</c:if>
$(document).ready(function () {
	var type="${type}";
	if(type!=null && type !=undefined && type!=''){
		 $('#calendar').fullCalendar('changeView', type);
	}else{
		 $('#calendar').fullCalendar('changeView', 'basicWeek');
			var view = $('#calendar').fullCalendar('getView');
			document.getElementById("type").value=view.name;
	}
	var offset = 220;
	var duration = 500;
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() > offset) {
			jQuery('#calendar .scrollup').fadeIn(duration);
		} else {
			jQuery('#calendar .scrollup').fadeOut(duration);
		}
	});
	
    $('#calendar .scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 2000);        
        return false;
    });
    var view = $('#calendar').fullCalendar('getView');
    if(view.name!='month'){
    	$(window).scrollTop( 0 );
    }
});
$('body').on('click', 'button.fc-month-button', function() {
	document.forms['priceCalendarForm'].elements['calendarViewStyle'].value = 'month';
});
	$('body').on('click', 'button.fc-basicWeek-button', function() {
	
		document.forms['priceCalendarForm'].elements['calendarViewStyle'].value = 'basicWeek';
	});                           
	$('body').on('click', 'button.fc-basicDay-button', function() {
	
		document.forms['priceCalendarForm'].elements['calendarViewStyle'].value = 'basicDay';
	});
 	

</script>
</s:form>
