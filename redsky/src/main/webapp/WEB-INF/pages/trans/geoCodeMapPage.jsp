<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<head>   
 <title>Geo Code Map</title>   
  <meta name="heading" content="Geo Code Map"/>  
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:3px;margin-top:-18px;!margin-top:-15px;
padding:2px 0px;text-align:right;width:99%;}
form {margin-top:-5px;}
div#main {margin:-5px 0 0;}
.listheadred {color: red; font-family: arial,verdana; font-size: 11px;font-weight: bold;text-decoration: none;}
input[type="checkbox"]
{vertical-align:middle;}
</style> 
<!--<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>-->
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>" djConfig="parseOnLoad:true, isDebug:false"></script>
<script>

function waitForMapToLoad(){
}
var geocoder;
var map;
var letter1 = "";
var k = 0;
var letter = "";
var flag1="0";
var flag2="0";
var count1=-1;
var count2=-1;

window.onload = function() { 
	  var script = document.createElement('script');
	  script.type = 'text/javascript';
	  script.src = 'https://maps.googleapis.com/maps/api/js?v=3&callback=loadMap&key='+googlekey; 
	  document.body.appendChild(script);
}
var map;

function loadMap() { 

	var address1=document.forms['geoMapForm'].elements['geoAddress'].value;
	var terminalCity=document.forms['geoMapForm'].elements['city'].value;
	var terminalZip=document.forms['geoMapForm'].elements['zipCode'].value;
	var terminalState=document.forms['geoMapForm'].elements['geoState'].value;
	var lastName=document.forms['geoMapForm'].elements['lastName'].value;

	var dd = document.forms['geoMapForm'].elements['geoCountry'].selectedIndex;
	var terminalCountry = document.forms['geoMapForm'].elements['geoCountry'].options[dd].text;
	var address = lastName+" "+address1+" "+terminalCity+"  "+terminalZip+" "+terminalState+" "+terminalCountry;
	map = new google.maps.Map(document.getElementById("map"));
	var geocoder = new google.maps.Geocoder();
	    geocoder.geocode({'address': address}, function(results, status) {
	    	 if ( status == google.maps.GeocoderStatus.OK ) 
	    	 document.forms['geoMapForm'].elements['geoLatitude'].value =results[0].geometry.location.lat();
	      	 document.forms['geoMapForm'].elements['geoLongitude'].value = results[0].geometry.location.lng();
	      	
	      	addToMap(results[0].geometry.location.lat(),results[0].geometry.location.lng(),address);
	    
                   });
	  }
  var point1;
  var point2;
  var address;
  	function addToMap(point1,point2,address)
   {
 
 	 var uluru = {lat: point1, lng:  point2};
	       map = new google.maps.Map(document.getElementById('map'), {zoom: 16,  center: uluru});
           marker = new google.maps.Marker({position: uluru, map: map});
	     getPortsPoint();
}
var addrArray =new Array();
var latArray =new Array();
var lngArray =new Array();

function getPortsPoint(){
		dojo.xhrPost({
	       form: "geoMapForm",
	       url:"geoPortsPoint.html",
	       timeout: 900000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	        		//var jsonData = dojo.toJson(data)
	        var d=document.getElementById("partnerList");	        		
	        d.innerHTML='';	  
  			var i=0;
  			var oldEstimator;
			var j = 0;
	           for(var i=0; i < jsonData.ports.length; i++){
	             	   letter = String.fromCharCode("A".charCodeAt(0) + j);
	             	   j++;
		   var tempLetter = letter;	             	   
 	   		   if(flag1=="1")
 	   		   {
 	   		   var temp=String.fromCharCode("A".charCodeAt(0) + count1);
 	   		   letter = temp+letter;
 	   		   }	
	             	   
	        	   console.log("lattitude: " + jsonData.ports[i].latitude + ", longitude: " +  jsonData.ports[i].longitude);
	        	   addrArray[i] = jsonData.ports[i].address; 
	        	   var lat= (jsonData.ports[i].latitude);    
	        	   var lng = (jsonData.ports[i].longitude); 
	        	   latArray[i]=lat;
	        	   lngArray[i]=lng
	        	   var timeout = i * 225; 
	        	   addry[i]=jsonData.ports[i];
	   			   window.setTimeout(function() { geoCodeLookup(); }, timeout);    
	               var id =	jsonData.ports[i].id;	
				   var code =	jsonData.ports[i].name;		   
	  			   var carrierStatus = jsonData.ports[i].carrierStatus;
		  		   var vendorStatus = jsonData.ports[i].vendorStatus;
		  		   var accountStatus = jsonData.ports[i].accountStatus;
		           var agentStatus = jsonData.ports[i].agentStatus; 
  		           var partnerOption = document.forms['geoMapForm'].elements['partnerOption'].value; 
	  	
	           		 d.innerHTML+='<table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext" width="200px" >'+jsonData.ports[i].partnerName+' - <a href="javascript:putSelectedPortName(\''+id+'\',\''+code+'\',\''+carrierStatus+'\',\''+accountStatus+'\',\''+vendorStatus+'\',\''+agentStatus+'\',\''+partnerOption+'\')"><u>'+jsonData.ports[i].name+'</u></a><b class="listheadred">('+letter+')</b> </td></tr></table>';
	           		           	
	        	   var delme = 0;
		if(tempLetter=="Z")
		{
		flag1="1";
		j=0;
		count1++;
		}
	        	   
	        	}
	       }	       
	   });
	        
	}
	
	var addrCtr = 0;
	var address;
	var addry =new Array();
  	function 	geoCodeLookup() {
  		var portDetails=addry[addrCtr]
		var addr = addrArray[addrCtr];
  		var lat= latArray[addrCtr];
  		var lng= lngArray[addrCtr];
		addrCtr++;
	    geocoder =  new google.maps.Geocoder(); 
	    var latlng = {lat:parseFloat(lat), lng: parseFloat(lng)};
		geocoder.geocode({'location': latlng}, function(results, status) {
          if ( status == google.maps.GeocoderStatus.OK ) 
          var partnerRateGridStatus = addry.partnerRateGridStatus;	
          var baseIcon = "http://www.google.com/mapfiles/shadow50.png";
          var letteredIconR = new google.maps.MarkerImage(
        		  baseIcon,
        	      // The shadow image is larger in the horizontal dimension
        	      // while the position and offset are the same as for the main image.
        	      new google.maps.Size(37, 32),
        	      new google.maps.Point(0,0),
        	      new google.maps.Point(0, 32));
          var letteredIconY = new google.maps.MarkerImage(
         		  baseIcon,
         	      // The shadow image is larger in the horizontal dimension
         	      // while the position and offset are the same as for the main image.
         	      new google.maps.Size(37, 32),
         	      new google.maps.Point(0,0),
         	      new google.maps.Point(0, 32));
			  
          	   letter1 = String.fromCharCode("A".charCodeAt(0) + k);
	             	   k++;
		   var tempLetter = letter1;
	 		   if(flag2=="1")
	   		   {
	   		   var temp=String.fromCharCode("A".charCodeAt(0) + count2);
	   		   letter1 = temp+letter1;
	   		   }
	   		var bounds = new google.maps.LatLngBounds();
		if(partnerRateGridStatus=='Submitted')
		{     letteredIconR = "http://chart.apis.google.com/chart?chst=d_map_spin&chld=.5|0|FF0000|10|b|"+letter1;	
		      marker =  new google.maps.Marker({
			  position: latlng,
			  icon: letteredIconR,
	          map: map
	      });
			  
		 
		}
		else
		{
			letteredIconY = "http://chart.apis.google.com/chart?chst=d_map_spin&chld=.8|0|FFFF00|20|b|"+letter1;	
			
			marker =  new google.maps.Marker({
	 			position: latlng,
	 			icon: letteredIconY,
	 	        map: map
	 	      });
			  
			
		}
		 bounds.extend(marker.position);
		 map.fitBounds(bounds);
		 map.setZoom(3);
		if(tempLetter=="Z")
		{
		flag2="1";
		k=0;
		count2++;
		}
	     //var marker =google.maps.Marker(point, markerOptions);
	      
		 //map.addOverlay(marker, markerOptions);
		var fn = markerClickFnPorts(latlng, portDetails,marker);
		  google.maps.event.addListener(marker, "click", fn);
	    //  GEvent.addListener(marker, "click", fn);

        }); 
	}
  
 	  function putSelectedPortName(id,code,carrierStatus,accountStatus,vendorStatus,agentStatus,partnerOption) {

		   if(agentStatus=='AG')
		   {
			    if(partnerOption=='view')
		    	{
					window.open('findPartnerProfileList.html?from=view&code='+code+'&partnerType=AG&id='+id, '_blank');	   		   
				}
				else
				{
					window.open('editPartnerPublic.html?id='+id+'&partnerType=AG', '_blank');	   		   			
				}
		   }
		   else if(vendorStatus=='VN')
		   {
				if(partnerOption=='view')
		    	{
		  		    window.open('viewPartner.html?from=view&code='+code+'&partnerType=VN&id='+id, '_blank');		   
				}
				else
				{
					window.open('editPartnerPublic.html?id='+id+'&partnerType=VN', '_blank');	   		   			
				}
		   }
		   else if(carrierStatus=='CR')
		   {
				if(partnerOption=='view')
		    	{
  			        window.open('viewPartner.html?from=view&code='+code+'&partnerType=CR&id='+id, '_blank');		 
				}
				else
				{
					window.open('editPartnerPublic.html?id='+id+'&partnerType=CR', '_blank');	   		   			
				}
		   }
		   else if(accountStatus=='AC')
		   {
				if(partnerOption=='view')
		    	{
  			        window.open('viewPartner.html?from=view&code='+code+'&partnerType=AC&id='+id, '_blank');		 
				}
				else
				{
					window.open('editPartnerPublic.html?id='+id+'&partnerType=AC', '_blank');	   		   			
				}
		   }
	  }
	  function markerClickFnPorts(point, portDetails,marker) {
	   return function() {
		   if (!portDetails) return;
	       var name = portDetails.name;
	       var address = portDetails.address;
		   var partnerName = portDetails.partnerName;
		   var city = portDetails.city;
		   var zip = portDetails.zip;
		   var state = portDetails.state;
		   var country = portDetails.country;
		   var id =	portDetails.id;	

		   var carrierStatus = portDetails.carrierStatus;
		   var vendorStatus = portDetails.vendorStatus;
		   var agentStatus = portDetails.agentStatus; 
		   var accountStatus = portDetails.accountStatus; 
		   var partnerOption = document.forms['geoMapForm'].elements['partnerOption'].value; 
	
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px; margin-top:15px;">('+name+') '+ partnerName+'</h4><br style="line-height:11px;"><h4 style="line-height:0px;">'+address+',</h4><br style="line-height:11px;"><h4 style="line-height:0px;">'+city+', '+ state+', '+ zip+', '+ country+'</h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"><a align="right" href="javascript:putSelectedPortName(\''+id+'\',\''+name+'\',\''+carrierStatus+'\',\''+accountStatus+'\',\''+vendorStatus+'\',\''+agentStatus+'\',\''+partnerOption+'\')"><u>View Details</u></a></span>';
	            
	       infoHtml += '</div></div>';
	       var infowindow = new google.maps.InfoWindow({
	           content: infoHtml
	         });
	       infowindow.open(map, marker);
	   }
   }


function clear_fields(){
		document.forms['geoMapForm'].elements['partner.isAgent'].checked = false;
		document.forms['geoMapForm'].elements['partner.isVendor'].checked = false;
		document.forms['geoMapForm'].elements['partner.isCarrier'].checked = false;
		document.forms['geoMapForm'].elements['isUts'].checked = false;
		document.forms['geoMapForm'].elements['isFidi'].checked = false;
		document.forms['geoMapForm'].elements['isOmniNo'].checked = false;
		document.forms['geoMapForm'].elements['isAmsaNo'].checked = false;
		document.forms['geoMapForm'].elements['isWercNo'].checked = false;
		document.forms['geoMapForm'].elements['isIamNo'].checked = false;
		
		document.forms['geoMapForm'].elements['geoAddress'].value = '';
		document.forms['geoMapForm'].elements['lastName'].value = '';
		document.forms['geoMapForm'].elements['city'].value = '';
		document.forms['geoMapForm'].elements['geoState'].value = '';
		document.forms['geoMapForm'].elements['zipCode'].value = '';
		document.forms['geoMapForm'].elements['geoCountry'].value = '';
}
   function validateParam()
   {
   		var geoAddress = document.forms['geoMapForm'].elements['geoAddress'].value;
		var lastName = document.forms['geoMapForm'].elements['lastName'].value;
		var city = document.forms['geoMapForm'].elements['city'].value;
		var geoState = document.forms['geoMapForm'].elements['geoState'].value;
		var zipCode = document.forms['geoMapForm'].elements['zipCode'].value;
		var geoCountry = document.forms['geoMapForm'].elements['geoCountry'].value;
		if((geoAddress=='')&&(lastName=='')&&(city=='')&&(geoState=='')&&(zipCode=='')&&(geoCountry==''))
		{
		alert('Please select an appropriate search parameter');
		return false;
		}
		else
		{
		return submit_form();
		}
   }
   function setRedSky(){	
		document.forms['geoMapForm'].action = 'searchPartnerRS.html?partnerType=RSKY&search=YES';
		document.forms['geoMapForm'].submit();
	}
</script>  
</head>
<s:form id="geoMapForm" action="searchGeoPartner" method="post" validate="true" onsubmit="return validateParam();" >
<s:hidden name="geoLatitude"/>
<s:hidden name="partnerOption"/>
<c:set var="partnerOption" value="${partnerOption}"/>
<s:hidden name="geoLongitude"/>
<s:hidden name="id1"/>
<s:hidden name="pCode"/>
<s:hidden name="carrierStatus"/>
<s:hidden name="vendorStatus"/>
<s:hidden name="agentStatus"/>
<s:hidden name="accountStatus"/>

<div id="layer1" style="width:100%;">
<div id="newmnav" style="!padding-bottom:5px;">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Geo Search</span></a></li>	  	
   		<c:if test="${partnerOption=='view'}" >
		  <li><a href="partnerView.html" ><span>Partner View</span></a></li>					  	
		</c:if>		  
   		<c:if test="${partnerOption!='view'}" >
		  <li><a href="partnerPublics.html" ><span>Partner</span></a></li>					  	
		</c:if>
		<c:if test="${partnerOption=='view'}" >
		<li><a onclick="setRedSky();"><span><font color="#fe000c">Red</font>Sky</span></a></li>
		</c:if>
	  </ul>
</div>
<div class="spn">&nbsp;</div> 	
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>     
</c:set>
<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
    <div class="top"><span></span></div>
    <div class="center-content">
		<table class="table" border="0">
		<thead>
		<tr>
		<th>Address</th>
		<th>Last/Company Name</th>
		<th><fmt:message key="refZipGeoCodeMap.city"/></th>
		<th><fmt:message key="refZipGeoCodeMap.state"/></th>
		<th><fmt:message key="refZipGeoCodeMap.zipcode"/></th>
		<th width="165px" align="left">Country</th>
		<th>Range Within (<span class="listheadred">miles</span>)</th>
		</tr></thead>	
		<tbody>
		<tr>
		<td align="left">
			<s:textfield name="geoAddress" required="true" cssClass="input-text" size="20"/>
			</td>
				<td align="left">
			    <s:textfield name="lastName" required="true" cssClass="input-text" size="20"/>
			</td>
			<td align="left">
			    <s:textfield name="city" required="true" cssClass="input-text" size="20"/>
			</td>					
			<td align="left">
			    <s:textfield name="geoState" required="true" cssClass="input-text" size="20"/>
			</td>					
			<td align="left">
			    <s:textfield name="zipCode" required="true" cssClass="input-text" size="15"/>					    
			</td>					
			<td width="165px" align="left"><s:select cssClass="list-menu" name="geoCountry" list="%{ocountry}" cssStyle="width:165px"   headerKey="" headerValue=""/>
			</td>					
			<td align="left">
				<s:select cssClass="list-menu" name="rangeWithin" list="{'25','50','100','200','300','400','500','600','700','800','900','1000','1100'}" cssStyle="width:80px" />
			</td>
			</tr>					
			<tr>
			<td colspan="8" style="padding:0.2em;">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="detailTabLabel">
			<tbody>
				<tr>
					<td style="border:none;"></td>
					<c:set var="ischecked" value="false"/>
					<c:if test="${isUts}">
					<c:set var="ischecked" value="true"/>
					</c:if>						
					<td align="right" class="listwhitetext" width="" style="border:none;">UTS<s:checkbox key="isUts" value="${ischecked}" fieldValue="true"/></td>
					
					<td style="border:hidden;"></td>
					<c:set var="ischecked" value="false"/>
					<c:if test="${isFidi}">
					<c:set var="ischecked" value="true"/>
					</c:if>						
					<td align="right" class="listwhitetext" width="" style="border:none;">FIDI<s:checkbox key="isFidi" value="${ischecked}" fieldValue="true"/></td>
					
					<td style="border:hidden;"></td>
					<c:set var="ischecked" value="false"/>
					<c:if test="${isOmniNo}">
					<c:set var="ischecked" value="true"/>
					</c:if>						
					<td align="right" class="listwhitetext" width="" style="border:none;">OMNI<s:checkbox key="isOmniNo" value="${ischecked}" fieldValue="true"/></td>
					
					<td style="border:hidden;"></td>
					<c:set var="ischecked" value="false"/>
					<c:if test="${isAmsaNo}">
					<c:set var="ischecked" value="true"/>
					</c:if>						
					<td align="right" class="listwhitetext" width="" style="border:none;">AMSA<s:checkbox key="isAmsaNo" value="${ischecked}" fieldValue="true"/></td>
					
					<td style="border:hidden;"></td>
					<c:set var="ischecked" value="false"/>
					<c:if test="${isWercNo}">
					<c:set var="ischecked" value="true"/>
					</c:if>						
					<td align="right" class="listwhitetext" width="" style="border:none;">WERC<s:checkbox key="isWercNo" value="${ischecked}" fieldValue="true"/></td>
					
					<td style="border:hidden;"></td>
					<c:set var="ischecked" value="false"/>
					<c:if test="${isIamNo}">
					<c:set var="ischecked" value="true"/>
					</c:if>						
					<td align="right" class="listwhitetext" width="" style="border:none;">IAM<s:checkbox key="isIamNo" value="${ischecked}" fieldValue="true"/></td>
					

					<td style="border:hidden;"></td>
					<c:set var="ischecked" value="false"/>
					<c:if test="${partner.isAgent}">
					<c:set var="ischecked" value="true"/>
					</c:if>						
					<td align="right" class="listwhitetext" width="" style="border:none;">Agents<s:checkbox key="partner.isAgent" value="${ischecked}" fieldValue="true"/></td>
			
					<c:set var="ischecked" value="false"/>
					<c:if test="${partner.isVendor}">
					<c:set var="ischecked" value="true"/>
					</c:if>					
					<td align="center" class="listwhitetext" width="" style="border:none;">Vendors<s:checkbox key="partner.isVendor" value="${ischecked}" fieldValue="true"/></td>
													
					<c:set var="ischecked" value="false"/>
					<c:if test="${partner.isCarrier}">
					<c:set var="ischecked" value="true"/>
					</c:if>						
					<td align="center" class="listwhitetext" width="" style="border:none;">Carriers<s:checkbox key="partner.isCarrier" value="${ischecked}" fieldValue="true"/></td>
					<c:set var="ischecked" value="false"/>
					<c:if test="${partner.isAccount}">
					<c:set var="ischecked" value="true"/>
					</c:if>					
					<td align="center" class="listwhitetext" width="" style="border:none;">Account<s:checkbox key="partner.isAccount" value="${ischecked}" fieldValue="true"/></td>
					
					<td style="border:hidden;!border:none;width:120px;!width:150px;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
				</tr>
			</tbody>	
		</table>
	</td>
	</tr>
	</tbody>
	</table>			
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
  <table width="100%" style="margin:0px;padding:0px;" border="0" cellpadding="0" cellspacing="0"> 
  <tr>
      <td align="left" width="25%" valign="top">
       <div id="priceinfotab_bg" style="width:99%;padding-right:0px;!padding-top:14px;"><div class="price_tleft">Partner List:</div></div>
      <div class="price_tleft" id="partnerList" style="width:98%;border:1px dotted #219DD1;padding-right:0px;border-right:none;height:520px;overflow-y:scroll;"></div>
      </td>
      <td align="left" width="75%" valign="top">
      <div id="map" style="width:100%;border:1px dotted #219DD1; height: 550px"></div>
      </td>
  </tr>
  </table>
</s:form>
<script type="text/javascript">
try{
initLoader();
}
catch(e){}
</script>