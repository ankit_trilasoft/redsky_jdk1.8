package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import org.appfuse.model.BaseObject;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="itemsjbkequip")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class ItemsJbkEquip extends BaseObject implements Comparable<ItemsJbkEquip> {
	private Long id;
	private String corpID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String shipNum;
	private Long ticket;
	private String descript;
	private String type;
	private Integer qty;
	private Integer actualQty;
	private Double charge;
	private Double actual;
	private Double otCharge;
	private Double overQuantity;
	private Double packLabour;
	private Double dtCharge;
	private Integer idNum;
	private String gl;
	private Double returned;
	private String billCrew;
	private String jobNumber;
	private String seqNum;
	private String ship;
	private Double cost;
	private Double salePrice;
	private String flag;
	private Long workTicketID;
	private String comments;
	private String day;
	private Date beginDate;
	private Date endDate;
	private Boolean ticketTransferStatus;
	private Boolean revisionInvoice;
	private Double estHour;
	private String refferedBy;
	private Long equipMaterialsId;
	private String customerName;
	private String payMethod;
	private String invoiceSeqNumber;
	private BigDecimal contractRate= new BigDecimal(0);
	
	//private WorkTicket workTicket;
	
	

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("createdBy", createdBy).append("createdOn",
				createdOn).append("updatedBy", updatedBy).append("updatedOn",
				updatedOn).append("shipNum", shipNum).append("ticket", ticket).append("ticketTransferStatus",ticketTransferStatus)
				.append("day", day).append("beginDate", beginDate).append("endDate", endDate).append("estHour",estHour)
				.append("descript", descript).append("type", type).append(
						"qty", qty).append("actualQty", actualQty).append(
						"charge", charge).append("actual", actual).append(
						"otCharge", otCharge).append("overQuantity",
						overQuantity).append("packLabour", packLabour).append(
						"dtCharge", dtCharge).append("idNum", idNum).append(
						"gl", gl).append("returned", returned).append(
						"billCrew", billCrew).append("jobNumber", jobNumber)
				.append("seqNum", seqNum).append("ship", ship).append("cost",
						cost).append("comments",comments).append("revisionInvoice",revisionInvoice)
						.append("flag", flag).append(refferedBy).append(equipMaterialsId).append(customerName).append(payMethod).append(contractRate).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ItemsJbkEquip))
			return false;
		ItemsJbkEquip castOther = (ItemsJbkEquip) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(createdBy, castOther.createdBy).append(ticketTransferStatus, castOther.ticketTransferStatus)
				.append(day, castOther.day).append(beginDate, castOther.beginDate).append(endDate, castOther.endDate)
				.append(createdOn, castOther.createdOn).append(updatedBy,
						castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(shipNum, castOther.shipNum).append(revisionInvoice, castOther.revisionInvoice)
				.append(ticket, castOther.ticket).append(descript,
						castOther.descript).append(type, castOther.type).append(estHour, castOther.estHour)
				.append(qty, castOther.qty)
				.append(actualQty, castOther.actualQty).append(charge,
						castOther.charge).append(actual, castOther.actual)
				.append(otCharge, castOther.otCharge).append(overQuantity,
						castOther.overQuantity).append(packLabour,
						castOther.packLabour).append(dtCharge,
						castOther.dtCharge).append(idNum, castOther.idNum)
				.append(gl, castOther.gl).append(returned, castOther.returned)
				.append(billCrew, castOther.billCrew).append(jobNumber,
						castOther.jobNumber).append(seqNum, castOther.seqNum)
				.append(ship, castOther.ship).append(cost, castOther.cost).append(comments,castOther.comments)
				.append(flag, castOther.flag).append(customerName, castOther.customerName).append(payMethod, castOther.payMethod).append(equipMaterialsId, castOther.equipMaterialsId).append(refferedBy, castOther.refferedBy).append(contractRate, castOther.contractRate).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(comments).append(ticketTransferStatus)
				.append(day).append(beginDate).append(endDate).append(revisionInvoice)
				.append(createdBy).append(createdOn).append(updatedBy).append(
						updatedOn).append(shipNum).append(ticket).append(
						descript).append(type).append(qty).append(actualQty)
				.append(charge).append(actual).append(otCharge).append(
						overQuantity).append(packLabour).append(dtCharge)
				.append(idNum).append(gl).append(returned).append(billCrew).append(estHour)
				.append(jobNumber).append(seqNum).append(ship).append(cost)
				.append(flag).append(customerName).append(payMethod).append(refferedBy).append(equipMaterialsId).append(equipMaterialsId).toHashCode();
	}
	public int compareTo(final ItemsJbkEquip other) {
		return new CompareToBuilder().append(id, other.id).append(corpID,
				other.corpID).append(createdBy, other.createdBy).append(
				createdOn, other.createdOn).append(updatedBy, other.updatedBy).append(ticketTransferStatus,other.ticketTransferStatus)
				.append(day, other.day).append(beginDate, other.beginDate).append(endDate, other.endDate)
				.append(updatedOn, other.updatedOn).append(shipNum,
						other.shipNum).append(ticket, other.ticket).append(
						descript, other.descript).append(type, other.type).append(revisionInvoice,other.revisionInvoice)
				.append(qty, other.qty).append(actualQty, other.actualQty)
				.append(charge, other.charge).append(actual, other.actual).append(estHour,other.estHour)
				.append(otCharge, other.otCharge).append(overQuantity,
						other.overQuantity)
				.append(packLabour, other.packLabour).append(dtCharge,
						other.dtCharge).append(idNum, other.idNum).append(gl,
						other.gl).append(returned, other.returned).append(
						billCrew, other.billCrew).append(jobNumber,
						other.jobNumber).append(seqNum, other.seqNum).append(
						ship, other.ship).append(cost, other.cost).append(comments,other.comments)
				.append(refferedBy,other.refferedBy).append(payMethod,other.payMethod).append(customerName,other.customerName).append(equipMaterialsId,other.equipMaterialsId).append(contractRate,other.contractRate).toComparison();
	}
	/*@ManyToOne
	@JoinColumn(name="workTicketID",nullable=false, updatable=false,insertable=false,
	referencedColumnName="id")
	public WorkTicket getWorkTicket() {
	    return workTicket;
	}
	    
		 public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	} 
		*/ 
	@Column
	public Double getActual() {
		return actual;
	}
	public void setActual(Double actual) {
		this.actual = actual;
	}
	@Column
	public Double getOtCharge() {
		return otCharge;
	}
	public void setOtCharge(Double otCharge) {
		this.otCharge = otCharge;
	}
	@Column
	public Double getOverQuantity() {
		return overQuantity;
	}
	public void setOverQuantity(Double overQuantity) {
		this.overQuantity = overQuantity;
	}
	@Column
	public Double getPackLabour() {
		return packLabour;
	}
	public void setPackLabour(Double packLabour) {
		this.packLabour = packLabour;
	}
	@Column(length=2)
	public String getBillCrew() {
		return billCrew;
	}
	public void setBillCrew(String billCrew) {
		this.billCrew = billCrew;
	}
	@Column
	public Double getCharge() {
		return charge;
	}
	public void setCharge(Double charge) {
		this.charge = charge;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	@Column(length=50)
	public String getDescript() {
		return descript;
	}
	public void setDescript(String descript) {
		this.descript = descript;
	}
	@Column
	public Double getDtCharge() {
		return dtCharge;
	}
	public void setDtCharge(Double dtCharge) {
		this.dtCharge = dtCharge;
	}
	@Column(length=20)
	public String getGl() {
		return gl;
	}
	public void setGl(String gl) {
		this.gl = gl;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Integer getIdNum() {
		return idNum;
	}
	public void setIdNum(Integer idNum) {
		this.idNum = idNum;
	}
	@Column(length=12)
	public String getJobNumber() {
		return jobNumber;
	}
	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}
	@Column
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	@Column
	public Integer getActualQty() {
		return actualQty;
	}
	public void setActualQty(Integer actualQty) {
		this.actualQty = actualQty;
	}
	@Column
	public Double getReturned() {
		return returned;
	}
	public void setReturned(Double returned) {
		this.returned = returned;
	}
	@Column(length=7)
	public String getSeqNum() {
		return seqNum;
	}
	public void setSeqNum(String seqNum) {
		this.seqNum = seqNum;
	}
	@Column(length=2)
	public String getShip() {
		return ship;
	}
	public void setShip(String ship) {
		this.ship = ship;
	}
	@Column(length=15)
	public String getShipNum() {
		return shipNum;
	}
	public void setShipNum(String shipNum) {
		this.shipNum = shipNum;
	}
	@Column
	public Long getTicket() {
		return ticket;
	}
	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}
	@Column(length=1)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column(length=2)
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public Long getWorkTicketID() {
		return workTicketID;
	}
	public void setWorkTicketID(Long workTicketID) {
		this.workTicketID = workTicketID;
	}
	@Column
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	@Column
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	@Column
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	@Column
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Boolean getTicketTransferStatus() {
		return ticketTransferStatus;
	}
	public void setTicketTransferStatus(Boolean ticketTransferStatus) {
		this.ticketTransferStatus = ticketTransferStatus;
	}
	public Boolean getRevisionInvoice() {
		return revisionInvoice;
	}
	public void setRevisionInvoice(Boolean revisionInvoice) {
		this.revisionInvoice = revisionInvoice;
	}
	@Column
	public Double getEstHour() {
		return estHour;
	}
	public void setEstHour(Double estHour) {
		this.estHour = estHour;
	}
	@Column
	public String getRefferedBy() {
		return refferedBy;
	}
	public void setRefferedBy(String refferedBy) {
		this.refferedBy = refferedBy;
	}
	@Column
	public Long getEquipMaterialsId() {
		return equipMaterialsId;
	}
	public void setEquipMaterialsId(Long equipMaterialsId) {
		this.equipMaterialsId = equipMaterialsId;
	}
	@Column
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	@Column
	public String getPayMethod() {
		return payMethod;
	}
	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}
	@Column
	public String getInvoiceSeqNumber() {
		return invoiceSeqNumber;
	}
	public void setInvoiceSeqNumber(String invoiceSeqNumber) {
		this.invoiceSeqNumber = invoiceSeqNumber;
	}
	@Column
	public Double getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}
	@Column(length=20)
	public BigDecimal getContractRate() {
		return contractRate;
	}
	public void setContractRate(BigDecimal contractRate) {
		this.contractRate = contractRate;
	}
}