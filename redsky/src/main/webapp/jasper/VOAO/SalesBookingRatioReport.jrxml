<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="salesBookingRatioReport"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="1034"
		 pageHeight="595"
		 columnWidth="974"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Begin_Date" isForPrompting="true" class="java.util.Date"/>
	<parameter name="End_Date" isForPrompting="true" class="java.util.Date"/>
	<queryString><![CDATA[SELECT
	'SalesBookingRatioReport-VOAO.jrxml',
	customerfile.`sequenceNumber` AS 'customerfile_sequenceNumber',
	serviceorder.`shipNumber` AS 'serviceorder_shipNumber',
	customerfile.`salesStatus` AS 'customerfile_salesStatus',
	customerfile.`estimator` AS 'customerfile_estimator',
	customerfile.`originCompany` AS 'customerfile_originCompany',
	if(customerfile.`firstName` is null or customerfile.`firstName`='',customerfile.`firstName`,concat(customerfile.`lastName`,', ',customerfile.`firstName`)) AS 'customerfile_first_last_name',
	DATE_FORMAT(customerfile.`quoteAcceptenceDate`,'%d %b %Y') AS 'customerfile_quoteAcceptenceDate',
	serviceorder.`job` AS 'serviceorder_job',
	concat(if(serviceorder.`originCity` is not null,concat(serviceorder.`originCity`,","),"")," ",if(!(trim(refmaster_so.`description`) is null || trim(refmaster_so.`description`)=""),concat(trim(refmaster_so.`description`),","),"")," ",if(serviceorder.`originCountry` is not null,serviceorder.`originCountry`,"")) AS 'serviceorder_origin_From',
	concat(if(serviceorder.`destinationCity` is not null,concat(serviceorder.`destinationCity`,","),"")," ",if(!(trim(refmaster_sd.`description`) is null || trim(refmaster_sd.`description`)=""),concat(trim(refmaster_sd.`description`),","),"")," ",if(serviceorder.`destinationCountry` is not null,serviceorder.`destinationCountry`,"")) AS 'serviceorder_destination_To',
	serviceorder.`actualRevenue` AS 'serviceorder_actualRevenue',
	sum(if(accountline.`chargeCode`='INSUR' and accountline.actualRevenue!=0,accountline.`actualRevenue`,0)) AS 'accountline_insurance_revenue',
	serviceorder.`actualGrossMargin` AS 'serviceorder_actualGrossMargin',
	(serviceorder.`actualGrossMargin`-sum(if(accountline.`chargeCode`='INSUR' and accountline.actualRevenue!=0,accountline.`actualRevenue`,0))) AS 'serviceorder_grossMargin_without_ins',
	round(serviceorder.`actualGrossMarginPercentage`,1) AS 'serviceorder_actualGrossMarginPercentage',
	round((serviceorder.`actualGrossMargin`-sum(if(accountline.`chargeCode`='INSUR' and accountline.actualRevenue!=0,accountline.`actualRevenue`,0)))/(serviceorder.`actualRevenue`-sum(if(accountline.`chargeCode`='INSUR' and accountline.actualRevenue!=0,accountline.`actualRevenue`,0)))*100,1) AS 'serviceorder_grossMargin_without_Ins.%',
	DATE_FORMAT(now(),'%d %b %Y') As 'Current_Dates',
	if(company.`corpID`='VOER','A',(if(company.`corpID`='VORU','B',(if(company.`corpID`='VOCZ','C',""))))) As 'company_CorpID'
FROM 
	`customerfile` customerfile 
	left outer join `company` company on company.parentCorpId = $P{Corporate ID}
	left outer join `serviceorder` serviceorder on serviceorder.`customerFileId` = customerfile.id and serviceorder.corpid in (company.corpid)
	left outer join `refmaster` refmaster_so on serviceorder.`originState`= refmaster_so.`code` and refmaster_so.`parameter` = 'STATE' and refmaster_so.`corpID` in (company.corpid,'TSFT')
	left outer join `refmaster` refmaster_sd on serviceorder.`destinationState`= refmaster_sd.`code` and refmaster_sd.`parameter` = 'STATE' and refmaster_sd.`corpID` in (company.corpid,'TSFT')
	left outer join `accountline` accountline on serviceorder.`id`=accountline.`serviceorderId`
where customerfile.corpid in (company.corpid)
and customerfile.`controlFlag` ='C'
and customerfile.`status` not in('CLSD','closed','CLOSED')
and customerfile.`quoteAcceptenceDate` is not null
and customerfile.`source`= 'AS'
and customerfile.`quoteAcceptenceDate` between $P{Begin_Date} AND $P{End_Date}
group by serviceorder.`shipNumber`]]></queryString>

	<field name="SalesBookingRatioReport-VOAO.jrxml" class="java.lang.String"/>
	<field name="customerfile_sequenceNumber" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="customerfile_salesStatus" class="java.lang.String"/>
	<field name="customerfile_estimator" class="java.lang.String"/>
	<field name="customerfile_originCompany" class="java.lang.String"/>
	<field name="customerfile_first_last_name" class="java.lang.String"/>
	<field name="customerfile_quoteAcceptenceDate" class="java.lang.String"/>
	<field name="serviceorder_job" class="java.lang.String"/>
	<field name="serviceorder_origin_From" class="java.lang.String"/>
	<field name="serviceorder_destination_To" class="java.lang.String"/>
	<field name="serviceorder_actualRevenue" class="java.math.BigDecimal"/>
	<field name="accountline_insurance_revenue" class="java.math.BigDecimal"/>
	<field name="serviceorder_actualGrossMargin" class="java.math.BigDecimal"/>
	<field name="serviceorder_grossMargin_without_ins" class="java.math.BigDecimal"/>
	<field name="serviceorder_actualGrossMarginPercentage" class="java.math.BigDecimal"/>
	<field name="serviceorder_grossMargin_without_Ins.%" class="java.math.BigDecimal"/>
	<field name="Current_Dates" class="java.lang.String"/>
	<field name="company_CorpID" class="java.lang.String"/>

	<sortField name="company_CorpID" />
	<sortField name="customerfile_sequenceNumber" />
	<sortField name="customerfile_quoteAcceptenceDate" />


		<group  name="SequenceNumber" >
			<groupExpression><![CDATA[$F{customerfile_sequenceNumber}]]></groupExpression>
			<groupHeader>
			<band height="20"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="2"
						width="122"
						height="18"
						forecolor="#0000FF"
						key="staticText-31"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[Customer File Number : ]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="122"
						y="2"
						width="95"
						height="18"
						forecolor="#0000FF"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_sequenceNumber}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="43"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="1"
						width="974"
						height="42"
						key="staticText-1"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="18" isBold="true"/>
					</textElement>
				<text><![CDATA[SALES BOOKING RATIO REPORT]]></text>
				</staticText>
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="37"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Opaque"
						x="475"
						y="1"
						width="26"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-3"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Job ]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="501"
						y="1"
						width="75"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-6"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[From]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="89"
						y="1"
						width="91"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-7"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Client Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="410"
						y="1"
						width="65"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-8"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Acceptance]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="1"
						width="89"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-9"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[S/O Number]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="260"
						y="1"
						width="55"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-10"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Status]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="315"
						y="1"
						width="95"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-11"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Surveyors]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="180"
						y="1"
						width="80"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-12"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Employer Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="576"
						y="1"
						width="75"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-13"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[To]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="751"
						y="1"
						width="44"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-15"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Gross]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="859"
						y="1"
						width="50"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-16"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Gross]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="475"
						y="19"
						width="26"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-17"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="501"
						y="19"
						width="75"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-19"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="315"
						y="19"
						width="95"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-20"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="410"
						y="19"
						width="65"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-21"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="19"
						width="89"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-22"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="89"
						y="19"
						width="91"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-24"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="180"
						y="19"
						width="135"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-25"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="576"
						y="19"
						width="75"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-26"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="751"
						y="19"
						width="44"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-28"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Margin]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="859"
						y="19"
						width="50"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-29"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Margin %]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="651"
						y="1"
						width="50"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-32"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Actual]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="651"
						y="19"
						width="50"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-33"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Revenue]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="701"
						y="1"
						width="50"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-34"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Insurance]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="701"
						y="19"
						width="50"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-35"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Revenue]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="795"
						y="1"
						width="64"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-36"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Gross Margin]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="795"
						y="19"
						width="64"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-37"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[without Ins.]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="909"
						y="1"
						width="65"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-38"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Gross Margin]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="909"
						y="19"
						width="65"
						height="18"
						forecolor="#000033"
						backcolor="#CCCCCC"
						key="staticText-39"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="10" isBold="false" isUnderline="false"/>
					</textElement>
				<text><![CDATA[without Ins.%]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="89"
						height="18"
						key="textField"/>
					<box leftPadding="1"></box>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="260"
						y="0"
						width="55"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_salesStatus}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="315"
						y="0"
						width="95"
						height="18"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_estimator}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="180"
						y="0"
						width="80"
						height="18"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_originCompany}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="89"
						y="0"
						width="91"
						height="18"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_first_last_name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd MMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="410"
						y="0"
						width="65"
						height="18"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_quoteAcceptenceDate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="475"
						y="0"
						width="26"
						height="18"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_job}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="501"
						y="0"
						width="75"
						height="18"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_origin_From}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="576"
						y="0"
						width="75"
						height="18"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_destination_To}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="751"
						y="0"
						width="44"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{serviceorder_actualGrossMargin}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="859"
						y="0"
						width="50"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{serviceorder_actualGrossMarginPercentage}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="651"
						y="0"
						width="50"
						height="18"
						key="textField-4"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{serviceorder_actualRevenue}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="701"
						y="0"
						width="50"
						height="18"
						key="textField-5"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{accountline_insurance_revenue}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="795"
						y="0"
						width="64"
						height="18"
						key="textField-6"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{serviceorder_grossMargin_without_ins}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="909"
						y="0"
						width="65"
						height="18"
						key="textField-7"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{serviceorder_grossMargin_without_Ins.%}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="16"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="180"
						y="1"
						width="679"
						height="15"
						key="staticText-30"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[salesBookingRatioReport.jrxml]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="859"
						y="1"
						width="50"
						height="15"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="909"
						y="1"
						width="65"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Helvetica" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="CP1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd MMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="1"
						width="179"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Current_Dates}]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
