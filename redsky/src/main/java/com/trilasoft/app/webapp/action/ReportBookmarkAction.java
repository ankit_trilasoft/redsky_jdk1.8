package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.util.List;

import com.trilasoft.app.model.ReportBookmark;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ReportBookmarkManager;


	public class ReportBookmarkAction extends BaseAction {

	private Long id;
	private Long reportId;
	private String corpId;
	private Boolean bookMark;
    private ReportBookmark  reportBookmark;
	private ReportBookmarkManager reportBookmarkManager;
	private RefMasterManager refMasterManager;
	private String createdBy;
	private String updateddBy;
	private Date createdOn;
	private Date updatedOn;
	private String userName;
	private String sessionCorpID;
	private List isExist;
	private List reportss;
	private String marked;
	private String  userCheck;
	private String  userCheck1;
	private String sessionUserName;
	private Map<String, String> moduleReport;
	private Map<String, String> subModuleReport;
	private User user;
	private Set<Role> roles;
	public ReportBookmarkAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		this.sessionUserName= user.getUsername();
	}


	public String getComboList(String corpId) {
		moduleReport=refMasterManager.findByParameter(sessionCorpID, "MODULE_REPORT");
		subModuleReport=refMasterManager.findByParameter(sessionCorpID, "SUBMODULE_REPORT");;
		return SUCCESS;
	}
	public String save(){
		getComboList(sessionCorpID);
				userCheck = userCheck.trim();
				if (userCheck.indexOf(",") == 0) {
					userCheck = userCheck.substring(1);
				}
				if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
					userCheck = userCheck.substring(0, userCheck.length() - 1);
				}
				String[] arrayid = userCheck.split(",");
				int arrayLength = arrayid.length;
				
				for (int i = 0; i < arrayLength; i++) {
					Long reportId = Long .parseLong(arrayid[i].toString());
					List isExist = reportBookmarkManager.isExist(reportId, sessionUserName);
					if(!isExist.isEmpty()){
						/*String key =  "Reports are allready Bookmrked" ;
						saveMessage(getText(key));*/	
					}else {
					reportBookmark= new ReportBookmark();
					reportBookmark.setReportId(reportId);
					reportBookmark.setCorpId(sessionCorpID);
					reportBookmark.setCreatedBy(sessionUserName); 
					reportBookmark.setUpdatedBy(sessionUserName);
					reportBookmark.setCreatedOn(new Date());
					reportBookmark.setUpdatedOn(new Date());
					reportBookmark = reportBookmarkManager.save(reportBookmark);	
					
					}
				}
		
				String key =  "Selected Reports has been Bookmarked." ;
				saveMessage(getText(key));	
				return SUCCESS;
	}
	
	public String remove(){
		getComboList(sessionCorpID);
		userCheck = userCheck.trim();
		if (userCheck.indexOf(",") == 0) {
			userCheck = userCheck.substring(1);
		}
		if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
			userCheck = userCheck.substring(0, userCheck.length() - 1);
		}
		
		String[] arrayid = userCheck.split(",");
		int arrayLength = arrayid.length;
		
		for (int i = 0; i < arrayLength; i++) {
			Long reportId = Long .parseLong(arrayid[i].toString());
			reportBookmarkManager.remove(reportId);
		
		}
		
		List isExist = reportBookmarkManager.getAll();
		if(! isExist.isEmpty())
		{
			marked = "yes";
			String key =  "Bookmarked Reports has been deleted successfully." ;
			saveMessage(getText(key));
			return SUCCESS;
		}
		String key =  "Bookmarked Reports has been deleted successfully." ;
		saveMessage(getText(key));
		return INPUT;
	}
	
	
	public String bookMarkedList(){
		getComboList(sessionCorpID);
		user = userManager.getUserByUsername(sessionUserName);
		roles = user.getRoles();
		String moduleSession="";
		String subModuleSession="";
		String menuSession="";
		String descriptionSession="";
		try{
		moduleSession = (String) getRequest().getSession().getAttribute("Module");
		subModuleSession = (String) getRequest().getSession().getAttribute("SubModule");
		menuSession = (String) getRequest().getSession().getAttribute("Menu");
		descriptionSession = (String) getRequest().getSession().getAttribute("Description");
			reportss = reportBookmarkManager.getBookMarkedReportList(sessionUserName,moduleSession,subModuleSession,menuSession,descriptionSession,sessionCorpID,roles);
		}catch(Exception ex)
		{
			reportss = reportBookmarkManager.getBookMarkedReportList(sessionUserName,"","","","",sessionCorpID,roles);
		}
		
		
		if(reportss.isEmpty())
		{
			return INPUT;
		}
		else
		{
		marked = "yes";
		return SUCCESS;
		}
	}
	
public String bookMarkedListF(){
	getComboList(sessionCorpID);
		reportss = reportBookmarkManager.getBookMarkedFormList(sessionUserName,"","","","",sessionCorpID);
		
		if(reportss.isEmpty())
		{
			return INPUT;
		}
		else
		{
		marked = "yes";
		return SUCCESS;
		}
	}
	
	
	public Boolean getBookMark() {
		return bookMark;
	}
	public void setBookMark(Boolean bookMark) {
		this.bookMark = bookMark;
	}
	public void setReportBookmarkManager(ReportBookmarkManager reportBookmarkManager) {
		this.reportBookmarkManager = reportBookmarkManager;
	}
	public ReportBookmark getReportBookmark() {
		return reportBookmark;
	}
	public void setReportBookmark(ReportBookmark reportBookmark) {
		this.reportBookmark = reportBookmark;
	}

	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getUpdateddBy() {
		return updateddBy;
	}
	public void setUpdateddBy(String updateddBy) {
		this.updateddBy = updateddBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getReportId() {
		return reportId;
	}
	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}



	public String getUserCheck() {
		return userCheck;
	}



	public void setUserCheck(String userCheck) {
		this.userCheck = userCheck;
	}



	public List getIsExist() {
		return isExist;
	}



	public void setIsExist(List isExist) {
		this.isExist = isExist;
	}



	public List getReportss() {
		return reportss;
	}



	public void setReportss(List reportss) {
		this.reportss = reportss;
	}



	public String getMarked() {
		return marked;
	}



	public void setMarked(String marked) {
		this.marked = marked;
	}



	public String getUserCheck1() {
		return userCheck1;
	}



	public void setUserCheck1(String userCheck1) {
		this.userCheck1 = userCheck1;
	}



	/**
	 * @return the moduleReport
	 */
	public Map<String, String> getModuleReport() {
		return moduleReport;
	}



	/**
	 * @param moduleReport the moduleReport to set
	 */
	public void setModuleReport(Map<String, String> moduleReport) {
		this.moduleReport = moduleReport;
	}



	/**
	 * @return the subModuleReport
	 */
	public Map<String, String> getSubModuleReport() {
		return subModuleReport;
	}



	/**
	 * @param subModuleReport the subModuleReport to set
	 */
	public void setSubModuleReport(Map<String, String> subModuleReport) {
		this.subModuleReport = subModuleReport;
	}


	/**
	 * @param refMasterManager the refMasterManager to set
	 */
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	
	}
