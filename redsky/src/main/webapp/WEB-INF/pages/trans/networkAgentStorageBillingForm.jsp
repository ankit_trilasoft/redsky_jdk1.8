<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head>
    <title><fmt:message key="storageBillingForm.title"/></title>
    <meta name="heading" content="<fmt:message key='storageBillingForm.heading'/>"/>
	<style type="text/css">	
	#overlay111 { filter:alpha(opacity=70);	-moz-opacity:0.7;-khtml-opacity: 0.7;opacity: 0.7;	position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
	background:url(images/over-load.png);	}			
	#loader {	filter:alpha(opacity=70);	-moz-opacity:0.7;	-khtml-opacity: 0.7;opacity: 0.7;	position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
	background:url(images/over-load.png);	}	
	.list-menu{margin-top:5px;}
	input[type=radio]{margin:0px;vertical-align:top;}
	</style>
	
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/calenderStyle.css"%>
	</script>
	<script language="JavaScript">
	function onlyNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36)|| (keyCode==110) ; 
	}
	function onlyFloatNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==190) || (keyCode==110) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36)|| (keyCode==110) ; 
	}
	
	function onlyCharsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	
	
	function onlyTimeFormatAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	function onlyPhoneNumsAllowed(evt){
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
	}
</script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

<script language="javascript" type="text/javascript">
	function valButton(btn) {
	    var cnt = -1;
	    for (var i=btn.length-1; i > -1; i--) {
	        if (btn[i].checked) {cnt = i; i = -1;}
	    }
	    if (cnt > -1) return btn[cnt].value;
	    else return null;
	}

function findPreviewBillingVatReport(){
   var date1 = document.forms['storageBillingForm'].elements['billTo2'].value;	 
   var date2 = document.forms['storageBillingForm'].elements['billTo'].value; 
   var daysApart = getDateCompare(date1,date2);
   
  if(daysApart<0){
    alert("Bill Through To should be greater than  or equal to Bill Through From");
    document.forms['storageBillingForm'].elements['billTo'].value='';
    return false;
  }
	
      var StoInv =document.forms['storageBillingForm'].elements['billing.previewVat'].value ;
      var btn = valButton(document.forms['storageBillingForm'].elements['radiobilling']);
      var  btn1 = valButton(document.forms['storageBillingForm'].elements['radiobExcInc']);
      if(btn1==null){
    	  btn1="";  
      }
      //var storageBillingList = document.forms['storageBillingForm'].elements['storageBillingList'].value ;
      var fld = document.getElementById('storageBillingListId');
      var storageBillingList = [];
      for (var i = 0; i < fld.options.length; i++) {
        if (fld.options[i].selected) {
        	storageBillingList.push(fld.options[i].value);
        }
      } 
      var bookingCodeType = document.forms['storageBillingForm'].elements['bookingCodeType'].value;
      var date1 = document.forms['storageBillingForm'].elements['billTo2'].value;	 
      var date2 = document.forms['storageBillingForm'].elements['billTo'].value; 
      var invoiceBillingDate = document.forms['storageBillingForm'].elements['invoiceBillingDate'].value
      var radioContractType = document.forms['storageBillingForm'].elements['radioContractType'].value; 		
       if (btn == null){
	          alert("Please select the Storage Billing/SIT Billing/TPS radio button"); 
	       	  return false;
	   }else if(document.forms['storageBillingForm'].elements['billTo2'].value==''){
	       	alert("Please enter the Bill Through From date"); 
	       	return false;
	   }else if(document.forms['storageBillingForm'].elements['billTo'].value==''){
	        alert("Please enter the Bill Through To date "); 
	       	return false;
	   }else{
	       	if(StoInv == 'Preview Billing Report'){
        		//showHide("block");
        		window.open('networkAgentBillingStoragesVatList.html?radiobilling='+btn+'&storageBillingList='+storageBillingList+'&radiobExcInc='+btn1+'&bookingCodeType='+bookingCodeType+'&billTo2='+date1+'&billTo='+date2+'&invoiceBillingDate='+invoiceBillingDate+'&radioContractType='+radioContractType,'_blank')
           		//document.forms['storageBillingForm'].submit();
           	}
	        return true;
	   	  }
}

function ajexFunction() {
	var billToCode = document.forms['storageBillingForm'].elements['billToCodeType'].value;
    var url="getTotalNumberOfCount.html?decorator=simple&popup=true";
     http2.open("GET", url, true);
     http2.onreadystatechange = getTotalNumberOfCount;
     http2.send(null);
 }
 
 function getTotalNumberOfCount(){
	if (http2.readyState == 4){
        var results = http2.responseText
        results = results.trim();
        document.forms['storageBillingForm'].elements['count'].value=results
    }
 }
 
 function validationDate(){
		var date1 = document.forms['storageBillingForm'].elements['billTo2'].value;	 
	    var date2 = document.forms['storageBillingForm'].elements['billTo'].value; 
	    
	    if(date1 == ''){
	       	alert("Please enter the bill through from date"); 
	       	return false;
		}else if(date2 == ''){
	        alert("Please enter the bill through to date "); 
	       	return false;
		}
	    
	    var daysApart = getDateCompare(date1,date2);
	    
	  if(daysApart<0){
	    alert("Bill Through To should be greater than  or equal to Bill Through From");
	    document.forms['storageBillingForm'].elements['billTo'].value='';
	    return false;
	  }
	  return true;
 }
	     
function MgmtFeeCalculate(){
	 if(validationDate()){
		 if(document.forms['storageBillingForm'].elements['invoiceBillingDate'].value==''){
		        alert("Please enter the invoice billing date "); 
		       	return false;
		}else if(document.forms['storageBillingForm'].elements['postDate'].value==''){
		        alert("Please enter the post date "); 
		       	return false;
		}
		 document.getElementById('invoiceFlag').checked = true;
		 document.forms['storageBillingForm'].action ='MGMTFeeCalculationNetworkAgentStorageBilling.html';
         document.forms['storageBillingForm'].submit();
	 }
}

function createAccountLineVatStoIns(){ 
	var url="findStorageBillingStatus.html?ajax=1&decorator=simple&popup=true"; 
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponseStorageBillingStatus;
    http2.send(null);
     
  
}

function  handleHttpResponseStorageBillingStatus(){
	if (http2.readyState == 4){
        var results = http2.responseText
        results = results.trim(); 
        if(results=='true'){
        	alert("The Storage Billing Batch is already running. Please wait till that job finish");
         }else{ 
        	 if(!validationDate()){
        		    return false;
        		  }
        		  var btn = valButton(document.forms['storageBillingForm'].elements['radiobilling']);
        		  if (btn == null){
        		      alert("Please select the Storage Billing/SIT Billing/TPS radio button"); 
        		   	  return false;
        		 }
        		  if(document.getElementById('invoiceFlag').checked){
        			  var StoInv =document.forms['storageBillingForm'].elements['billing.invoiceVat'].value ;
        		      var btn = valButton(document.forms['storageBillingForm'].elements['radiobilling']);
        		      var  btn1 = valButton(document.forms['storageBillingForm'].elements['radiobExcInc']);
        		      var invoiceDt = document.forms['storageBillingForm'].elements['invoiceBillingDate'].value;
        		      var postingDt = document.forms['storageBillingForm'].elements['postDate'].value;
        		      
        		    if (btn == null){
        			          alert("Please select the Storage Billing/SIT Billing/TPS radio button"); 
        			       	  return false;
        			}else if(invoiceDt == '' && postingDt == ''){
        				 alert("Please Enter Invoice Billing Date and Posting Date to Proceed Further."); 
        			     return false;
        			}else if(invoiceDt == ''){
        			        alert("Please enter the invoice billing date "); 
        			       	return false;
        			}else if(postingDt == ''){
        			        alert("Please enter the post date "); 
        			       	return false;
        			}else{
        			    if(StoInv == 'Create Invoice Lines'){
        			       	agree= confirm("This Process will create accounting lines. Hit OK to continue or cancel to stop");
        					if(agree){
        			             document.forms['storageBillingForm'].action ='invoiceVatNetworkAgentStorages.html';
        			             document.forms['storageBillingForm'].submit();
        			             document.forms['storageBillingForm'].elements['storageErrorFlagValue'].value=""; 
        			             my_window = window.open('previewSuccessTemp.html?decorator=simple&popup=true','mywin','location=1,status=1,scrollbars=1,width=550,height=350, top=300, left=300');
        		             }
        			        return true;
        			    }else{
        			    	return false;
        			    }
        			}
        		  }else{
        			  agree= confirm("This Process will create accounting lines. Hit OK to continue or cancel to stop");
        				if(agree){
        		           document.forms['storageBillingForm'].action ='invoiceVatNetworkAgentStorages.html';
        		           document.forms['storageBillingForm'].submit();
        		           document.forms['storageBillingForm'].elements['storageErrorFlagValue'].value=""; 
        		           my_window = window.open('previewSuccessTemp.html?decorator=simple&popup=true','mywin','location=1,status=1,scrollbars=1,width=550,height=350, top=300, left=300');
        		       }
        		      return true;
        		  }   
              
         }
     }
}

var http21 = getHTTPObject();
    
    
function MgmtFeeCalculate1(){
	 if(validationDate()){
		 if(document.forms['storageBillingForm'].elements['invoiceBillingDate'].value==''){
		        alert("Please enter the invoice billing date "); 
		       	return false;
		}else if(document.forms['storageBillingForm'].elements['postDate'].value==''){
		        alert("Please enter the post date "); 
		       	return false;
		}
		 document.getElementById('invoiceFlag').checked = true;
		 document.forms['storageBillingForm'].elements['type'].value="MGMT";
		 showOrHide(1);
        document.forms['storageBillingForm'].action ='invoiceVatNetworkAgentStorages1.html';
        document.forms['storageBillingForm'].submit();
        
	 }
}    
    
function createAccountLineVatStoIns1(type){ 
	var url="findStorageBillingStatus1.html?ajax=1&decorator=simple&popup=true"; 
    http21.open("GET", url, true);
    http21.onreadystatechange = function(){  handleHttpResponseStorageBillingStatus1(type);};
    http21.send(null);
}
function  handleHttpResponseStorageBillingStatus1(type){
	if (http21.readyState == 4){
        var results = http21.responseText
        results = results.trim(); 
        if(results=='true'){
        	alert("The Storage Billing Batch is already running. Please wait till that job finish.");
         }else{         	
        	 if(type=='MGMT'){
        	 	MgmtFeeCalculate1();
        	 }else if(type=='NOTMGMT'){
        		 createAccountLineVatStorageType();
        	 } 
         }
     }
}

var http2 = getHTTPObject();    
    
function createAccountLineVatStorageType(){
	 if(!validationDate()){
		    return false;
		  }
		  var btn = valButton(document.forms['storageBillingForm'].elements['radiobilling']);
		  if (btn == null){
		      alert("Please select the Storage Billing/SIT Billing/TPS radio button"); 
		   	  return false;
		 }
		  if(document.getElementById('invoiceFlag').checked){
			  var StoInv =document.forms['storageBillingForm'].elements['billing.invoiceShedulde'].value ;
		      var btn = valButton(document.forms['storageBillingForm'].elements['radiobilling']);
		      var  btn1 = valButton(document.forms['storageBillingForm'].elements['radiobExcInc']);
		      var invoiceDt = document.forms['storageBillingForm'].elements['invoiceBillingDate'].value;
		      var postingDt = document.forms['storageBillingForm'].elements['postDate'].value;
		    if (btn == null){
			          alert("Please select the Storage Billing/SIT Billing/TPS radio button"); 
			       	  return false;
			}else if(invoiceDt == '' && postingDt == ''){
				 alert("Please Enter Invoice Billing Date and Posting Date to Proceed Further."); 
			     return false;
			}else if(invoiceDt == ''){
			        alert("Please enter the invoice billing date "); 
			       	return false;
			}else if(postingDt == ''){
			        alert("Please enter the post date "); 
			       	return false;
			}else{				
			    if(StoInv == 'Schedule Invoice Lines'){
			       	agree= confirm("This Process will schedule accounting lines. Hit OK to continue or cancel to stop");
					if(agree){
						showOrHide(1);
						document.forms['storageBillingForm'].elements['type'].value="NOTMGMT";
			             document.forms['storageBillingForm'].action ='invoiceVatNetworkAgentStorages1.html';
			             document.forms['storageBillingForm'].submit();
		             }
			        return true;
			    }else{
			    	return false;
			    }
			}
		  }else{
			  agree= confirm("This Process will schedule accounting lines. Hit OK to continue or cancel to stop");
				if(agree){
					showOrHide(1);
					document.forms['storageBillingForm'].elements['type'].value="NOTMGMT";
		           document.forms['storageBillingForm'].action ='invoiceVatNetworkAgentStorages1.html';
		           document.forms['storageBillingForm'].submit();
		       }
		      return true;
		  }  
}    
function findBillToCode(){
     var billToCode = document.forms['storageBillingForm'].elements['billToCodeType'].value;
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(billToCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
}
function handleHttpResponse2(){
     if (http2.readyState == 4){
        var results = http2.responseText
        results = results.trim();
        if(results.length>=1){
         }else{
             alert("Invalid Bill To code, please select another");
             document.forms['storageBillingForm'].elements['billToCodeType'].value = "";
         }
     }
}
        
 function findBookingAgentCode(){
         var bookingAgentCode = document.forms['storageBillingForm'].elements['bookingCodeType'].value;
         bookingAgentCode=bookingAgentCode.trim();
         if(bookingAgentCode!=''){
         showHide("block");
         var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(bookingAgentCode);
         http2.open("GET", url, true);
         http2.onreadystatechange = handleHttpResponse4;
         http2.send(null);
         }
  }
  function handleHttpResponse4(){
             if (http2.readyState == 4){
            	 showHide("none");
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1){
 				}else{
                     alert("Invalid Booking Agent code, please select another");
                     document.forms['storageBillingForm'].elements['bookingCodeType'].value="";
                 }
             } 
    }  
     

function openPopWindow(){
		javascript:openWindow('partnersPopup.html?partnerType=PP&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=billToCodeType');
}
function openBookingAgentPopWindow(){		
		javascript:openWindow('bookingAgentPopup.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=bookingCodeType');
} 
function showOrHide(value) {
    if (value == 0) {
       	if (document.layers)
           document.layers["overlay111"].visibility='hide';
        else
           document.getElementById("overlay111").style.visibility='hidden';
   	}else if (value == 1) {
   		if (document.layers)
          document.layers["overlay111"].visibility='show';
       	else
          document.getElementById("overlay111").style.visibility='visible';
   	}
}
</script>
</head>

<s:form id="storageBillingForm" name="storageBillingForm" action="billingStoragesList" method="post" validate="true">
<div id="overlay111">

<div id="layerLoading">

<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
  
   </div>
   </div>
   
<s:hidden name="type" /> 
<s:hidden name="count"/> 
<c:set var="autoMaticStorageBill" value="false"/>
<configByCorp:fieldVisibility componentId="component.field.autoMaticStorageBill">
<c:set var="autoMaticStorageBill" value="true"/>
</configByCorp:fieldVisibility>

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:90%;">

<s:hidden name="reports.id" value="%{reports.id}"/> 
		 <c:choose>
		 <c:when test="${autoMaticStorageBill==true || autoMaticStorageBill=='true'}">
			<div id="newmnav">
			  <ul>
			    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Storage Billing</span></a></li>
			    <li><a href="getStorageBillingMonitorList.html" ><span>Storage Billing Monitor</span></a></li>
			  </ul>
			</div>
		 </c:when>
	 	 <c:otherwise>
			<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Storage Billing</span></a></li>
			  </ul>
			</div>			   	 
		 </c:otherwise>	 
		</c:choose>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="" style="width:750px;" cellspacing="2" cellpadding="0" border="0">	
	  		<tr>
	  		<td  class="listwhitetext" align="right" width="130px" style="!vertical-align:middle;">
	  		   <label for="r1">Storage Billing&nbsp;</td><td><input type="radio" name="radiobilling" id="r1" value="1"/></label> 
               </td>
               
               <td  class="listwhitetext" align="right" width="130px" style="!vertical-align:middle;">
               <label for="r2">SIT Billing&nbsp;</td><td><input type="radio" name="radiobilling" id="r2" value="2"/></label>
               </td>
               
               <td  class="listwhitetext" align="left" colspan="2" style="!vertical-align:middle;">
               <label for="r3">&nbsp;TPS&nbsp;&nbsp;&nbsp;<input type="radio" name="radiobilling" id="r3" value="3"/></label>
               </td>
	  		    </tr>
	  		    <tr><td style="height:3px;"></td></tr>
	  		    <tr>
	  			<td class="listwhitetext" align="right" width="130px">Bill Through From</td>
	  					<c:if test="${not empty billTo2}">
						<s:text id="billTo2" name="${FormDateValue}"> <s:param name="value" value="billTo2" /></s:text>
						<td width="130px" style="padding-left:2px"><s:textfield cssClass="input-text" id="billTo2" name="billTo2" value="%{billTo2}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="billTo2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty billTo2}" >
						<td width="130px" style="padding-left:2px"><s:textfield cssClass="input-text" id="billTo2" name="billTo2" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="billTo2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>
	  		
	  		
	  		<td class="listwhitetext" align="right" >Bill Through To</td>
	  		<c:if test="${not empty billTo}">
						<s:text id="billTo" name="${FormDateValue}"> <s:param name="value" value="billTo" /></s:text>
						<td  style="padding-left:2px"><s:textfield cssClass="input-text" id="billTo" name="billTo" value="%{billTo}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="billTo_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty billTo}" >
						<td style="padding-left:2px"><s:textfield cssClass="input-text" id="billTo" name="billTo" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="billTo_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
	  		</tr>
	  		<tr>
		  		<td class="listwhitetext" align="right" >Booker's Code</td>
		  		<td class="listwhitetext" style="padding-left:2px"><s:textfield  cssClass="input-text" name="bookingCodeType" size="11" maxlength="6" onchange="findBookingAgentCode()"/>
		  		<img class="openpopup" align="absmiddle" width="17" height="20" onclick="openBookingAgentPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
		  		<td class="listwhitetext" align="right" >Storage Billing Group</td>
		  		<td style="padding-left:2px">
		  			<s:select cssClass="list-menu" cssStyle="width:130px; height:100px" id="storageBillingListId" name="storageBillingList" list="%{storageBillingGroup}"  multiple="true" headerKey="" headerValue=""  />
		  		</td>
		  		
		  		<td colspan="5">
		  			<table>
		  				<tr>
		  					<td class="listwhitetext" align="right" width="130px" style="vertical-align:calc();font-size:12px;">
               					<label for="r2"><b>CMM</b></label>
               				</td>
               				<td>
               					<input type="radio"   name="radioContractType" id="r2" value="CMM"/>
               				</td>
               				<td>               					
							 <c:choose>
							 <c:when test="${autoMaticStorageBill==true || autoMaticStorageBill=='true'}">
								  <input type="button" onclick="return createAccountLineVatStoIns1('MGMT')" name="mgmtFee" value="MGMT FEE" class="cssbutton" style="width:80px; height:19px" align="top" />	 
							 </c:when>
						 	 <c:otherwise>
								  <input type="button" onclick="return MgmtFeeCalculate();" name="mgmtFee" value="MGMT FEE" class="cssbutton" style="width:80px; height:19px" align="top" />	 
							 </c:otherwise>	 
							</c:choose>
               					
               					
               				</td>
		  				</tr>
		  				<tr><td style="height:15px;"></td></tr>
		  				<tr>
		  					<td class="listwhitetext" align="right" style="vertical-align:bottom;font-size:12px;">
			               		<label for="r3"><b>DMM</b></label>
			               	</td>
			               	<td>
			               		<input type="radio" name="radioContractType" id="r3" value="DMM"/>
			               	</td>
			               	<td>
			               		
			               	</td>
		  				</tr>
		  			</table>
		  		</td>
	  		</tr>
	  		<tr><td style="height:3px;"></td></tr>
	  		<tr>
	  		<td class="listwhitetext" align="right">
	  		<label for="i1">Include Group&nbsp; 
	  		 <td width="185" class="listwhitetext" align="left"><input type="radio" name="radiobExcInc" id="i1" value="1" />	  		  
              </td></label>
	  		
	  	 
	  	   <td  class="listwhitetext" align="right">
	  	   <label for="i1">Exclude Group&nbsp;
	  	   <td width="200" class="listwhitetext" align="left"><input type="radio" name="radiobExcInc" id="i2" value="2" /></td></label>
	  	   </tr>
	  	   <tr><td style="height:5px;"></td></tr>
	  		<tr>
	  			<td class="listwhitetext" align="right" >Invoice Date for Billing</td>
	  			<c:if test="${not empty invoiceBillingDate}">
						<s:text id="invoiceBillingDate" name="${FormDateValue}"> <s:param name="value" value="invoiceBillingDate" /></s:text>
						<td style="padding-left:2px"><s:textfield cssClass="input-text" id="invoiceBillingDate" name="invoiceBillingDate" value="%{invoiceBillingDate}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="invoiceBillingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty invoiceBillingDate}" >
						<td style="padding-left:2px"><s:textfield cssClass="input-text" id="invoiceBillingDate" name="invoiceBillingDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="invoiceBillingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>
			
	  			<td class="listwhitetext" align="right" >Posting Date</td>
	  			
	  		<c:if test="${not empty postDate}">
						<s:text id="postDate" name="${FormDateValue}"> <s:param name="value" value="postDate" /></s:text>
						<td style="padding-left:2px;" ><s:textfield cssClass="input-text" id="postDate" name="postDate" value="%{postDate}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="postDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty postDate}" >
						<td style="padding-left:2px"><s:textfield cssClass="input-text" id="postDate" name="postDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="postDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
				<td class="listwhitetext" align="left" width="130px" style="font-size:12px;">
               		<label for="r2"><b>Invoice</b></label>
               		<s:checkbox id="invoiceFlag" cssStyle="vertical-align:sub;margin:0px;" name="invoiceFlag"></s:checkbox>
               	</td>
	  		</tr>
			<tr><td style="!height:20px;height:15px;"></td></tr>

	  		</table>
 	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
 	<tr>
	 <c:if test="${systemDefaultVatCalculation=='true'}">
	 <td align="center"><input type="button" class="cssbutton" style="width:150px; height:25px" align="top" onclick="return findPreviewBillingVatReport()" name="billing.previewVat" value="Preview Billing Report" /></td>
		 <sec-auth:authComponent componentId="module.button.storageBilling.createInvoice">	 
		 <c:choose>
		 <c:when test="${autoMaticStorageBill==true || autoMaticStorageBill=='true'}">
		 	  <td align="center"><input type="button" class="cssbutton" style="width:150px; height:25px" align="top" onclick="return createAccountLineVatStoIns1('NOTMGMT')" name="billing.invoiceShedulde" value="Schedule Invoice Lines" /></td>	 
		 </c:when>
	 	 <c:otherwise>
			  <td align="center"><input type="button" class="cssbutton" style="width:150px; height:25px" align="top" onclick="return createAccountLineVatStoIns()" name="billing.invoiceVat" value="Create Invoice Lines" /></td> 	 
		 </c:otherwise>	 
		</c:choose>
		</sec-auth:authComponent>
	 </c:if>
	 </tr>
</div>
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />

<c:if test="${storageErrorFlag=='Error'}">
<s:hidden name="storageErrorFlagValue" value="${storageErrorFlag}"/>
</c:if> 
<c:if test="${storageErrorFlag!='Error'}">
<s:hidden name="storageErrorFlagValue" value=""/>
</c:if>

<div id="loader" style="text-align:center; display:none">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tr>
			<td align="center">
				<table cellspacing="0" cellpadding="3" align="center">
					<tr><td height="200px"></td></tr>
					<tr>
				       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
				           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
				       </td>
				    </tr>
				    <tr>
				      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
				           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
				       </td>
				    </tr>
		       </table>
		     </td>
	  	</tr>
	</table>
</div>

</s:form>

<script type="text/javascript">
Form.focusFirstElement($("storageBillingForm"));   
</script> 

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>

<script type="text/javascript">
	function showHide(action){
		document.getElementById("loader").style.display = action;
	}
	showOrHide(0);
</script>