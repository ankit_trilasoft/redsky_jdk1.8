package com.trilasoft.app.webapp.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.EmailSetup;
import com.trilasoft.app.model.ProposalManagement;
import com.trilasoft.app.service.EmailSetupManager;

public class EmailSetupAction extends BaseAction implements Preparable {
	private Long id;
	private String sessionCorpID;
	private EmailSetup emailSetup;
	private EmailSetupManager emailSetupManager;
	private List emailList;
	private List emailSetUpList;
	private String emailStatusVal;
	private String emailFlag;
	private String recipientTo;
	private String recipientCc;
	private String subject;
	private String signature;
	private Date dateSent;
	private String recipientBcc;
	private String body;
	private Integer retryCount;
	private String signaturePart;
	private Map<String, String> emailStatusList;
	private String FileNumber;

	 static final Logger logger = Logger.getLogger(EmailSetupAction.class);
	    Date currentdate = new Date();
	public void prepare() throws Exception { 

	}
	public EmailSetupAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
	private String emailStatusType;
	
	@SkipValidation
	public String list(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(emailStatusType==null){
			emailStatusType="";
		}
		emailSetUpList = emailSetupManager.findEmailSetUpList("","" ,"","",null,emailStatusType,sessionCorpID);
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   		return SUCCESS;
	}
	
	private List attachedFileNameList = new ArrayList();
	@SkipValidation
	public String emailSetUpView() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 if (id != null) {
			emailSetup=emailSetupManager.get(id);
			emailFlag=emailSetup.getEmailStatus();		
			recipientTo=emailSetup.getRecipientTo();
			recipientCc=emailSetup.getRecipientCc();
			subject=emailSetup.getSubject();
			signature=emailSetup.getSignature();
			body=emailSetup.getBody();
			recipientBcc=emailSetup.getRecipientBcc();
			dateSent=emailSetup.getDateSent();
			
			String fileName="";
			if(emailSetup.getAttchedFileLocation()!=null && !emailSetup.getAttchedFileLocation().equalsIgnoreCase("")) {
				for(String str:emailSetup.getAttchedFileLocation().split("~")){
					if((str!=null && !str.equalsIgnoreCase("")) && !str.contains("#image")){
						
						if(fileName!=null && !fileName.equalsIgnoreCase("")){
		        			if(!str.contains("C:")){
		        				fileName = fileName+"^"+str.substring(str.lastIndexOf("/")+1);
		        			}else{
		        				fileName = fileName+"^"+str.substring(str.lastIndexOf("\\")+1);
		        			}
		        		}else{
		        			if(!str.contains("C:")){
		        				fileName = str.substring(str.lastIndexOf("/")+1);
		        			}else{
		        				fileName = str.substring(str.lastIndexOf("\\")+1);
		        			}
		        		}
					}
	        	}
	        }
			attachedFileNameList = Arrays.asList(fileName.split("^"));	
		} else{
			 emailSetup=new EmailSetup();
			 emailSetup.setCreatedOn(new Date());
			 emailSetup.setUpdatedOn(new Date());
		 }
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}	
	
	   @SkipValidation
	    public String searchEmailSetUp() {
		   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try {
			if(emailStatusType==null){
				emailStatusType="";
			}
			emailSetUpList = emailSetupManager.findEmailSetUpList(
					emailSetup.getRecipientTo(),emailSetup.getFileNumber() ,emailSetup.getSignature(),emailSetup.getSubject(),
					emailSetup.getDateSent(),emailStatusType,sessionCorpID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		 return SUCCESS;
	}
	   
	   
	   public String save() throws Exception {
		   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		   boolean isNew = (emailSetup.getId() == null);
		   if(isNew){
		    	 emailSetup.setCreatedOn(new Date());
		        }
		     emailSetup.setCorpId(sessionCorpID);		
		     emailSetup.setCreatedBy(getRequest().getRemoteUser());
		     emailSetup.setUpdatedOn(new Date());
		     emailSetup.setUpdatedBy(getRequest().getRemoteUser());
		     emailSetupManager.save(emailSetup);
			 String key = "Email Setup details have been saved.";
		     saveMessage(getText(key));
		     String fileName="";
				if(emailSetup.getAttchedFileLocation()!=null && !emailSetup.getAttchedFileLocation().equalsIgnoreCase("")) {
					for(String str:emailSetup.getAttchedFileLocation().split("~")){
						if((str!=null && !str.equalsIgnoreCase("")) && !str.contains("#image")){
							
							if(fileName!=null && !fileName.equalsIgnoreCase("")){
			        			if(!str.contains("C:")){
			        				fileName = fileName+"^"+str.substring(str.lastIndexOf("/")+1);
			        			}else{
			        				fileName = fileName+"^"+str.substring(str.lastIndexOf("\\")+1);
			        			}
			        		}else{
			        			if(!str.contains("C:")){
			        				fileName = str.substring(str.lastIndexOf("/")+1);
			        			}else{
			        				fileName = str.substring(str.lastIndexOf("\\")+1);
			        			}
			        		}
						}
		        	}
		        }
				attachedFileNameList = Arrays.asList(fileName.split("^"));
		     logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		     return SUCCESS;
		}
	   
		
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public EmailSetup getEmailSetup() {
		return emailSetup;
	}
	public void setEmailSetup(EmailSetup emailSetup) {
		this.emailSetup = emailSetup;
	}
	public List getEmailList() {
		return emailList;
	}
	public void setEmailList(List emailList) {
		this.emailList = emailList;
	}
	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}
	public List getEmailSetUpList() {
		return emailSetUpList;
	}
	public void setEmailSetUpList(List emailSetUpList) {
		this.emailSetUpList = emailSetUpList;
	}
	public String getEmailStatusVal() {
		return emailStatusVal;
	}
	public void setEmailStatusVal(String emailStatusVal) {
		this.emailStatusVal = emailStatusVal;
	}
	public String getEmailFlag() {
		return emailFlag;
	}
	public void setEmailFlag(String emailFlag) {
		this.emailFlag = emailFlag;
	}
	public String getRecipientTo() {
		return recipientTo;
	}
	public String getRecipientCc() {
		return recipientCc;
	}
	public void setRecipientTo(String recipientTo) {
		this.recipientTo = recipientTo;
	}
	public void setRecipientCc(String recipientCc) {
		this.recipientCc = recipientCc;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public Map<String, String> getEmailStatusList() {
		return emailStatusList;
	}
	public void setEmailStatusList(Map<String, String> emailStatusList) {
		this.emailStatusList = emailStatusList;
	}
	public String getRecipientBcc() {
		return recipientBcc;
	}
	public String getBody() {
		return body;
	}
	public void setRecipientBcc(String recipientBcc) {
		this.recipientBcc = recipientBcc;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Date getDateSent() {
		return dateSent;
	}
	public void setDateSent(Date dateSent) {
		this.dateSent = dateSent;
	}
	public Integer getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}
	public String getSignaturePart() {
		return signaturePart;
	}
	public void setSignaturePart(String signaturePart) {
		this.signaturePart = signaturePart;
	}
	public String getFileNumber() {
		return FileNumber;
	}
	public void setFileNumber(String fileNumber) {
		FileNumber = fileNumber;
	}
	public String getEmailStatusType() {
		return emailStatusType;
	}
	public void setEmailStatusType(String emailStatusType) {
		this.emailStatusType = emailStatusType;
	}
	public List getAttachedFileNameList() {
		return attachedFileNameList;
	}
	public void setAttachedFileNameList(List attachedFileNameList) {
		this.attachedFileNameList = attachedFileNameList;
	}
	
}
