<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="contractPolicyDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='contractPolicyDetail.heading'/>"/>  

</head>
<div id="Layer4" style="width:715px">

<s:form id="contractPolicyForm" action="saveContractPolicy" method="post" validate="true"> 
<s:hidden name="contractPolicy.id" />
<s:hidden name="partner.id" />
<s:hidden name="partner.partnerCode" />
<s:hidden name="contractPolicy.partnerCode" value="%{partner.partnerCode}"/>
<c:set var="policyfrom" value="<%=request.getParameter("policyfrom") %>"/>
<c:if test="${policyfrom!= 'pportal'}">
<div id="newmnav">
		  <ul>
		    <li><a href="QuotationFileForm.html?id=<%=request.getParameter("id") %>" ><span>Quotation File</span></a></li>
		    <li><a href="quotationServiceOrders.html?id=<%=request.getParameter("id") %> "><span>Quotes</span></a></li>		    
		     <li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Policy<img src="images/navarrow.gif" align="absmiddle" /></span></a></li> 
		    </ul>
		</div><div class="spn">&nbsp;</div>
		<div style="padding-bottom:3px;"></div>
</c:if> 	

	<c:if test="${policyfrom== 'pportal'}">
		<div id="newmnav">
		  <ul>
		    <li><a><span>Account Policy</span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div>
		<div style="padding-bottom:3px;"></div>
	</c:if>
		
</div>			
		
		
<div id="Layer1" style="width:95%">
	<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:-3px "><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
				<table cellspacing="0" cellpadding="3" border="0">
					<tbody>
						<tr>
							<c:if test="${not empty contractPolicy.policy}">
							<tr>
								<td align="right" class="listwhitetext" valign="top"><fmt:message key="contractPolicy.policy"/></td>
								<td align="left" class="listwhitetext" colspan="6">
							 <div style="overflow-x:scroll; border:1px solid #74B3DC; overflow-y:scroll;width:640px;height:180px;">
							 <table border="0" style="width:100%;height:180px;margin:0px;padding:0px;" >
							
							 <tr>
							 <td style="vertical-align:top;">
							 ${contractPolicy.policy }
							 </td></tr>
							 
							 </table>
							 </div>	
								</td>
					</tr>
					
					<tr>
					<td></td>
					<td style="margin:0px;padding:0px;">
					<table style="margin:0px;padding:0px;width:100%;">
					      <tr>					      
					      <td colspan="10" align="left">
					      <c:if test="${not empty policyDocumentList}">
					        <display:table name="policyDocumentList" id="policyDocumentList" class="table" pagesize="10" requestURI="" style="width:100%;">
					        <display:column style="width:5%" title="#"><a onclick=""><c:out value="${policyDocumentList_rowNum}"/></a></display:column>
					        <display:column  title="Document Name" >
					        <a onclick="javascript:openWindow('PolicyDocumentImageServletAction.html?id=${policyDocumentList.id}&decorator=popup&popup=true',900,600);">
					        <c:out value="${policyDocumentList.documentName}" escapeXml="false"/></a>
					        </display:column>
					        <display:column property="fileName" title="File Name" maxLength="10" />
					        <display:column property="fileSize" title="Size"/>
					        <display:column property="language" title="Language"/>
					        <display:column property="sectionName" title="Section Name"/>
					        <display:column property="updatedOn"  title="Updated On"  format="{0,date,dd-MMM-yyyy}" />
						    <display:column property="updatedBy"  title="Updated By"/>
					         </display:table>
					       </c:if>
					       </td>
					      </tr>
						</table>							
						</td>
						</tr>
							</c:if>
							<tr>
							<c:if test="${empty contractPolicy.policy}">
							    <td width="50px"></td>
								<td align="center" width="700px">
								<span class="listwhitetext" style="font-size:18px;">No Contract Policy Available.</span>
								</td>
							</c:if>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
			</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
		<table class="detailTabLabel" border="0" style="width:750px">
		<tbody>
		<tr>
			<td align="center">
			<c:if test="${not empty contractPolicy.policy}">
			<table class="detailTabLabel" border="0">
			<tbody>
			<tr>
					   <td align="left" class="listwhitetext" width="30px"></td>
						<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
					<tr>
					<td align="right" class="listwhitetext"><b><fmt:message key='contractPolicy.createdOn'/></b></td>
					<s:hidden name="contractPolicy.createdOn" />
					<td style="width:130px"><fmt:formatDate value="${contractPolicy.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
					<td align="right" class="listwhitetext"><b><fmt:message key='contractPolicy.createdBy' /></b></td>
					<c:if test="${not empty contractPolicy.id}">
						<s:hidden name="contractPolicy.createdBy"/>
						<td style="width:90px"><s:label name="createdBy" value="%{contractPolicy.createdBy}"/></td>
					</c:if>
					<c:if test="${empty contractPolicy.id}">
						<s:hidden name="contractPolicy.createdBy" value="${pageContext.request.remoteUser}"/>
						<td style="width:90px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
					</c:if>
					<td align="right" class="listwhitetext"><b><fmt:message key='contractPolicy.updatedOn'/></b></td>
					<s:hidden name="contractPolicy.updatedOn"/>
					<td style="width:130px"><fmt:formatDate value="${contractPolicy.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
					<td align="right" class="listwhitetext"><b><fmt:message key='contractPolicy.updatedBy' /></b></td>
					<s:hidden name="contractPolicy.updatedBy" value="${pageContext.request.remoteUser}"  />
					<td style="width:100px"><s:label name="contractPolicy.updatedBy" value="${pageContext.request.remoteUser}"/></td>
				</tr>
			</tbody>
			</table> 
			</c:if> 
			</td>
		</tr>
	</tbody>
</table>
<table>
<tr><td style="height:160px;"></td></tr>
</table>
</div>
  <c:set var="idOfWhom" value="<%=request.getParameter("id") %>" scope="session"/>
<c:set var="noteID" value="<%=request.getParameter("jobNumber") %>" scope="session"/>
<c:set var="custID" value="<%=request.getParameter("jobNumber") %>" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
<c:if test="${empty contractPolicy.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty contractPolicy.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
</s:form>

<script type="text/javascript">   
    Form.focusFirstElement($("contractPolicyForm"));   
</script>  
