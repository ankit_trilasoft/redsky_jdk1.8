//----Created By Bibhash---

package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.AuditSetup;
import com.trilasoft.app.model.DataCatalog;


public interface AuditSetupDao extends GenericDao<AuditSetup, Long>{
	
	public List findById(Long id);
	public List<AuditSetup> searchAuditSetup(String corpID, String tableName, String fieldName, String auditable, String isAlertValue);
	public List auditSetupTable(String b);
    public List auditSetupField(String tableName,String b);
    public List getList(String corpID);
    public List findMaximumId();
    public List checkData(String tableName, String fieldName, String sessionCorpID);
}
