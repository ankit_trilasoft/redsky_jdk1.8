package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.QualitySurveySettings;
public interface QualitySurveySettingsDao extends GenericDao<QualitySurveySettings, Long> {
	public List getQualitySurveySettingsDate(String accountId,String sessionCorpID,String mode);
	public List getEmailConfigrationSoList(String sessionCorpID,String partnerCode,String partnerName);
	public Boolean getExcludeFPU(String partnerCode,String corpId);
	public List getQualityEmailListByPartnerCode(String partnerCode, String corpID);
	public int getQualityListReference(String partnerCode, String corpID,String mode);
}
