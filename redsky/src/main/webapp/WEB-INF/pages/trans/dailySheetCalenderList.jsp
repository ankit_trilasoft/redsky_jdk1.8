<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.lang.*"%>

<head>   
    <title>Daily Sheet Dispatch</title>   
    <meta name="heading" content="Daily Sheet Dispatch"/> 
    <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>  
   <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<style>
.table td{
    border: 1px solid #74B3DC;
    padding: 0.4em;
}
.tableHeaderTable2 {
    background: url("images/bg_listheader.png") repeat-x scroll 0 0 #BCD2EF;
    border-color: #3DAFCB;
    border-style: solid;
    border-width: 1px;
    color: #15428B;
    font-family: arial,verdana;
    font-size: 11px;
    font-weight: bold;
    height: 25px;
    padding: 2px 3px 3px 5px;
    text-decoration: none;
}

hr {
    display: block;
}
.list-columnmain3 {
    background-color: #CCCCCC;
    border-bottom: 2px solid #99BBE8;
    border-right: 2px solid #99BBE8;
    border-top: 2px solid #99BBE8;
    color: #DF5F00;
    font-family: arial,verdana;
    font-size: 11px;    
    padding-left: 3px;
}
</style>
<script language="javascript" type="text/javascript">
function selectSearchField(){		
	var wareHouse= document.forms['dailySheetCalenderForm'].elements['dailySheetwRHouse'].value;	
	var date= document.forms['dailySheetCalenderForm'].elements['dailySheetCalenderDate'].value;
		
	if(wareHouse==''){
		alert('Please select Warehouse to continue.....!');	
		return false;
	}else if(date==''){
		alert('Please select Date to continue.....!');	
		return false;
	}else{
		return true;
	}
}
function goToSearch(){
	var selectedSearch= selectSearchField();
	if(selectedSearch){	
		 document.forms['dailySheetCalenderForm'].action = 'dailySheetDispatchSearch.html';
		 document.forms['dailySheetCalenderForm'].submit();					
	}
}
</script>
</head>

<s:form id="dailySheetCalenderForm" action="" method="post" onsubmit="" > 


<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<table class="table" style="width:100%;margin-top:2px;" border="1" cellpadding="0" cellspacing="0">
		
	<tbody>
		<tr>
		<td style="text-align:right;width:3%;" class="listwhitetext" >View:</td>			
			<td width="14%" style="border-right:hidden !important;">
			     <s:select cssClass="list-menu" name="dailySheetwRHouse" list="%{house}" cssStyle="width:115px" headerKey="" headerValue="" />
			</td>		
				<td style="width:14%;text-align:right;" class="listwhitetext">Select date:</td>				
			 <c:if test="${not empty dailySheetCalenderDate}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="dailySheetCalenderDate" /></s:text>
				<td width="67" ><s:textfield cssClass="input-text" id="date1" name="dailySheetCalenderDate" value="%{customerFiledate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
				<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty dailySheetCalenderDate}">
				<td width="100"><s:textfield cssClass="input-text" id="date1" name="dailySheetCalenderDate" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
				<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>				
	<td width="200" >  
		<input type="button" class="cssbutton" style="width:65px;"  value="Search" onclick="return goToSearch();"/> 		
	</td>	
		</tr>
	</tbody>
</table>	
	<table style="margin:0px;padding:0px;border:none;width:100%" cellpadding="0" cellspacing="0">
<tr>
<td valign="top">


		<table class="table" id="dataTable" style="width:100%;margin-top:0px;vertical-align:top;">
		<tr > 
		<td  class="listwhitetext tableHeaderTable2"><b>Truck</b></td>			
			<%	
				SortedMap <Integer, String> dataMap = (SortedMap <Integer, String>)request.getAttribute("dataMap");	
					if(dataMap!=null && !dataMap.isEmpty()){
				Iterator dateIterator1 = dataMap.entrySet().iterator(); 
				while (dateIterator1.hasNext()) {
					Map.Entry entry1 = (Map.Entry) dateIterator1.next();
					String date = (String) entry1.getValue();	
			%>				
			<td class="listwhitetext tableHeaderTable2"><b><%=date%></b></td>			
			<%}%>
			<%}%>
			</tr>
			
			<%			
				Map <String, List> dailySheetCalenderList1 = (Map <String, List>)request.getAttribute("dailySheetCalenderList1");	
					if(dailySheetCalenderList1!=null && !dailySheetCalenderList1.isEmpty()){
				Iterator dateIterator = dailySheetCalenderList1.entrySet().iterator(); 
				while (dateIterator.hasNext()) {
					Map.Entry entry = (Map.Entry) dateIterator.next();
					String truckNo = (String) entry.getKey();
					List d = (List) entry.getValue();
					Iterator it2=d.iterator();
				%>
				<tr > 	
				<td class="listwhitetext" style="background:#CCCCCC;"><b><%=truckNo%></b></td>					
				<%						
				while(it2.hasNext()){
					String dateByValue=(String)it2.next();					
				%>			
				<td class="list-columnmain3"><%=dateByValue%></td>
			<%}%>
			</tr>	
			<%}%>
			<%}%>				
			
		</table>
		
</td></tr></table>

</s:form>
<script type="text/javascript"> 
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
		
</script>

  