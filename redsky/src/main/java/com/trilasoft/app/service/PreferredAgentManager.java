package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.PreferredAgent;

import java.util.List;

public interface PreferredAgentManager extends GenericManager<PreferredAgent, Long> {
	public List getPreferredAgentListByPartnerCode(String sessionCorpID, String partnerCode);
	public List findPreferredAgentsList(String partnerCode,String agentCode,String agentName,String status,String corpID,String agentGroup);
	public List findSearchAgentDetailsAutocompleteList(String searchName, String corpID,String fieldType,String partnerCode);
	public List findPartnerCode(String partnerCode, String sessionCorpID);
	public List checkForPartnerCodeExists(String partnerCode, String sessionCorpID,String agentCode);
	public List findAgentGroupList(String sessionCorpID);
	public List getAgentGroupDataList(String sessionCorpID,String agentGroup);
	public void updateAgentStatus(Long preIdNum, Boolean status);
	
}