package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;


import com.trilasoft.app.dao.TaskPlanningDao;
import com.trilasoft.app.model.TaskPlanning;

import com.trilasoft.app.service.TaskPlanningManager;

public class TaskPlanningManagerImpl extends GenericManagerImpl<TaskPlanning, Long> implements TaskPlanningManager{

	TaskPlanningDao taskPlanningDao;
	public TaskPlanningManagerImpl(TaskPlanningDao taskPlanningDao) {
		super(taskPlanningDao);
		this.taskPlanningDao =taskPlanningDao;
	}
	
	public List<TaskPlanning> findTaskPlanningList(String corpId,Long id){
		return taskPlanningDao.findTaskPlanningList(corpId, id);
	}

}
