package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="accountline")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class AccountLine extends BaseObject  implements ListLinkData{
	//Mandatory field's
	private String corpID; 
	private Long id;
	private Long serviceOrderId;
	private String sequenceNumber;
	private String shipNumber;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String accountLineNumber;
	private String category;
    private String vendorCode;
    private String name;
    private String chargeCode;
    private String reference;
    private String billToCode;
	private String billToName;
    private BigDecimal estimateRate= new BigDecimal(0);
    private BigDecimal estimateSellRate= new BigDecimal(0);
    private BigDecimal estimateExpense = new BigDecimal(0);
    private String estimateVendorName;
    private BigDecimal estimateRevenueAmount = new BigDecimal(0);
    private BigDecimal estimateQuantity = new BigDecimal(0);
    private Integer estimatePassPercentage;
    private BigDecimal revisionRate= new BigDecimal(0);
    private BigDecimal revisionSellRate= new BigDecimal(0);
    private BigDecimal revisionExpense = new BigDecimal(0);
    private BigDecimal revisionRevenueAmount = new BigDecimal(0);
    private BigDecimal revisionQuantity = new BigDecimal(0);
    private Integer revisionPassPercentage;
    private BigDecimal entitlementAmount = new BigDecimal(0);
    private boolean status;
    private boolean ignoreForBilling;
    private String contract;
    private String basis;
    private String basisNew;
    private String itemNew;
    private String checkNew;
    private String basisNewType;
    private BigDecimal actualExpense = new BigDecimal(0);
    private BigDecimal actualRevenue = new BigDecimal(0);
    private Date receivedInvoiceDate;
    private BigDecimal recQuantity;
    private BigDecimal recRate;
    private String description;
    private String recGl;
    private String payGl;
    private Date invoiceDate;
    private Date payPostDate;
    private Date recPostDate;
    private Date payAccDate;
    private Date recAccDate;
    private String recXfer;
    private String payXfer;
    private Date receivedDate;
    private Date valueDate;
    private String country;
    private String note;
    private String payingStatus;
    private String invoiceNumber;
    private BigDecimal localAmount= new BigDecimal(0);
    private double exchangeRate=1;
    private double convertedAmount;
    private ServiceOrder serviceOrder;
    private String glType;
    private String commission;
    private String externalReference;
    private String paymentStatus;
    private Date statusDate;
    private Date storageDateRangeFrom;
    private Date storageDateRangeTo;
    private Integer storageDays;
    private BigDecimal distributionAmount = new BigDecimal(0);
    private String payPayableStatus;
    private Date payPayableDate;
    private String payPayableVia;
    private Date sendEstToClient;
    private Date sendActualToClient;
    private Date doNotSendtoClient;
    private String branchCode;
    private String branchName;
    private String authorization;
    private Date accrueRevenue;
    private Date accruePayable;
    private String actgCode;
    private Date estimateDate;
    private String quoteDescription;
    private String recXferUser ;
    private String payXferUser ;
    private Date accrueRevenueReverse;
    private Date accruePayableReverse;
    private String recRateCurrency;
    private BigDecimal recCurrencyRate = new BigDecimal(0);
    private BigDecimal recRateExchange = new BigDecimal(1);
    private BigDecimal actualRevenueForeign = new BigDecimal(0); 
    private Date racValueDate;
    private boolean accrueRevenueManual;
    private boolean accruePayableManual;
    private BigDecimal receivedAmount = new BigDecimal(0);
    private String invoiceCreatedBy;
    private BigDecimal payPayableAmount= new BigDecimal(0);
    private String creditInvoiceNumber;
    private String truckNumber;
    private String oldBillToCode;
    private Boolean includeLHF;
    private String companyDivision; 
    private boolean  displayOnQuote;
    private String recVatPercent;
    private String  recVatDescr;
    private BigDecimal recVatAmt= new BigDecimal(0);
    private String payVatPercent;
    private String  payVatDescr;
    private BigDecimal payVatAmt= new BigDecimal(0);
    
    private BigDecimal estExpVatAmt= new BigDecimal(0);
    private BigDecimal revisionExpVatAmt= new BigDecimal(0);
    private BigDecimal qstRecVatAmt= new BigDecimal(0);
    private BigDecimal qstPayVatAmt= new BigDecimal(0);
    private String qstRecVatGl;
    private String qstPayVatGl;
    private String estCurrency;
    private BigDecimal estLocalAmount = new BigDecimal(0);
    private BigDecimal estExchangeRate = new BigDecimal(1);
    private Date estValueDate;
    private String revisionCurrency;
   
    private BigDecimal revisionExchangeRate = new BigDecimal(1);
    private Date revisionValueDate;
    private String uvlControlNumber;
    private String ddrControlNumber;
    private BigDecimal estLocalRate = new BigDecimal(0);
    private BigDecimal revisionLocalRate = new BigDecimal(0);
    private Long  groupAccount;
    private BigDecimal estimateSellDeviation = new BigDecimal(0);
   	private BigDecimal estimateDeviation = new BigDecimal(0);
    private BigDecimal revisionSellDeviation = new BigDecimal(0);
    private BigDecimal revisionDeviation = new BigDecimal(0);
    private String buyDependSell=new String("N"); 
    private Boolean VATExclude= new Boolean(false);
    private String estVatPercent;
    private String  estVatDescr;
    private BigDecimal estVatAmt= new BigDecimal(0);
    private String  deviation=new String("");
    private Boolean additionalService= new Boolean(false);
    private BigDecimal receivableSellDeviation = new BigDecimal(0);
    private Date settledDate;
    private String revisionVatPercent;
    private String  revisionVatDescr;
    private BigDecimal revisionVatAmt= new BigDecimal(0);
    private BigDecimal payDeviation = new BigDecimal(0);
    private String contractCurrency;
    private BigDecimal contractRate = new BigDecimal(0);
    private BigDecimal contractExchangeRate = new BigDecimal(1);
    private BigDecimal contractRateAmmount = new BigDecimal(0); 
    private Date contractValueDate;
    private Long networkSynchedId;
    private String agentFeesNo ="";
    
    
    private String payableContractCurrency; 
    private BigDecimal payableContractExchangeRate = new BigDecimal(1);
    private BigDecimal payableContractRateAmmount = new BigDecimal(0); 
    private Date payableContractValueDate;
    
    private String estSellCurrency;
    private BigDecimal estSellLocalAmount = new BigDecimal(0);
    private BigDecimal estSellExchangeRate = new BigDecimal(1);
    private Date estSellValueDate;
    private BigDecimal estSellLocalRate = new BigDecimal(0);
    
    private String revisionSellCurrency;
    private BigDecimal revisionSellLocalAmount = new BigDecimal(0);
    private BigDecimal revisionSellExchangeRate = new BigDecimal(1);
    private Date revisionSellValueDate;
    private BigDecimal revisionSellLocalRate = new BigDecimal(0);
    
    private String estimateContractCurrency;
    private BigDecimal estimateContractRate = new BigDecimal(0);
    private BigDecimal estimateContractExchangeRate = new BigDecimal(1);
    private BigDecimal estimateContractRateAmmount = new BigDecimal(0); 
    private Date estimateContractValueDate;
    
    private String estimatePayableContractCurrency;
    private BigDecimal estimatePayableContractRate = new BigDecimal(0);
    private BigDecimal estimatePayableContractExchangeRate = new BigDecimal(1);
    private BigDecimal estimatePayableContractRateAmmount = new BigDecimal(0); 
    private Date estimatePayableContractValueDate;
    
    private String revisionContractCurrency;
    private BigDecimal revisionContractRate = new BigDecimal(0);
    private BigDecimal revisionContractExchangeRate = new BigDecimal(1);
    private BigDecimal revisionContractRateAmmount = new BigDecimal(0); 
    private Date revisionContractValueDate;
    private BigDecimal estimateDiscount=new BigDecimal("0.00");
    private BigDecimal revisionDiscount=new BigDecimal("0.00");
    private BigDecimal actualDiscount=new BigDecimal("0.00");
    private String revisionPayableContractCurrency;
    private BigDecimal revisionPayableContractRate = new BigDecimal(0);
    private BigDecimal revisionPayableContractExchangeRate = new BigDecimal(1);
    private BigDecimal revisionPayableContractRateAmmount = new BigDecimal(0); 
    private Date revisionPayableContractValueDate;
    private Boolean costTransferred = new Boolean(false);
    private String categoryResource; 
    private String categoryResourceName; 
    private Boolean t20Process = new Boolean(false);
    private Boolean download= new Boolean(true);
    private String recInvoiceNumber="";  
    private BigDecimal revisionLocalAmount = new BigDecimal(0); 
    /* #6683 By kunal */
    private Boolean activateAccPortal=new Boolean(true);
    private Date vanlineSettleDate;
    /* End */
    private Boolean rollUpInInvoice;
    private BigDecimal onHand=new BigDecimal("0.00");
    private String estimateStatus;
    private String division;
    private Date paymentSent ;
    private Boolean invoiceAutoUpload=new Boolean(false);
    private Boolean driverCompanyDivision=new Boolean(false);
    private String invoiceType;
    private String recVatGl;
    private String payVatGl;
    private Boolean selectiveInvoice = new Boolean(true);
    private Boolean serviceTaxInput=new Boolean(false);
    private String myFileFileName;
    private String  externalIntegrationReference;   
    private BigDecimal estimateSellQuantity = new BigDecimal(0);
    private BigDecimal revisionSellQuantity = new BigDecimal(0);
	private String accountLineCostElement;
	private String accountLineScostElementDescription;
	private Boolean dynamicNavisionflag;
	private Boolean deleteDynamicNavisionflag;
	private String oldCompanyDivision;
	private String dynamicNavXfer;
	private Date sendDynamicNavisionDate;
	private Date auditCompleteDate;
	private String networkBillToCode;
    private String networkBillToName;
    private BigDecimal varianceRevenueAmount = new BigDecimal(0.00);
    private BigDecimal varianceExpenseAmount = new BigDecimal(0.00);
    private Date accrueRevenueVariance;
    private Date accrueExpenseVariance;
    private String revisionDescription;
    private String writeOffReason;
    private boolean utsiRecAccDateFlag=false;
    private boolean utsiPayAccDateFlag=false;
    private Date managerApprovalDate;
    private String managerApprovalName;
    private Boolean authorizedCreditNote = new Boolean(false);
    private String invoicereportparameter;
    private BigDecimal payQuantity;
    private BigDecimal payRate;
    
    
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("serviceOrderId", serviceOrderId)
				.append("sequenceNumber", sequenceNumber)
				.append("shipNumber", shipNumber)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("accountLineNumber", accountLineNumber)
				.append("category", category).append("vendorCode", vendorCode)
				.append("name", name).append("chargeCode", chargeCode)
				.append("reference", reference)
				.append("billToCode", billToCode)
				.append("billToName", billToName)
				.append("estimateRate", estimateRate)
				.append("estimateSellRate", estimateSellRate)
				.append("estimateExpense", estimateExpense)
				.append("estimateVendorName", estimateVendorName)
				.append("estimateRevenueAmount", estimateRevenueAmount)
				.append("estimateQuantity", estimateQuantity)
				.append("estimatePassPercentage", estimatePassPercentage)
				.append("revisionRate", revisionRate)
				.append("revisionSellRate", revisionSellRate)
				.append("revisionExpense", revisionExpense)
				.append("revisionRevenueAmount", revisionRevenueAmount)
				.append("revisionQuantity", revisionQuantity)
				.append("revisionPassPercentage", revisionPassPercentage)
				.append("entitlementAmount", entitlementAmount)
				.append("status", status)
				.append("ignoreForBilling", ignoreForBilling)
				.append("contract", contract).append("basis", basis)
				.append("basisNew", basisNew).append("itemNew", itemNew)
				.append("checkNew", checkNew)
				.append("basisNewType", basisNewType)
				.append("actualExpense", actualExpense)
				.append("actualRevenue", actualRevenue)
				.append("recInvoiceNumber", recInvoiceNumber)
				.append("receivedInvoiceDate", receivedInvoiceDate)
				.append("recQuantity", recQuantity).append("recRate", recRate)
				.append("description", description).append("recGl", recGl)
				.append("payGl", payGl).append("invoiceDate", invoiceDate)
				.append("payPostDate", payPostDate)
				.append("recPostDate", recPostDate)
				.append("payAccDate", payAccDate)
				.append("recAccDate", recAccDate).append("recXfer", recXfer)
				.append("payXfer", payXfer)
				.append("receivedDate", receivedDate)
				.append("valueDate", valueDate).append("country", country)
				.append("note", note).append("payingStatus", payingStatus)
				.append("invoiceNumber", invoiceNumber)
				.append("localAmount", localAmount)
				.append("exchangeRate", exchangeRate)
				.append("convertedAmount", convertedAmount)
				.append("glType", glType)
				.append("commission", commission)
				.append("externalReference", externalReference)
				.append("paymentStatus", paymentStatus)
				.append("download", download)
				.append("statusDate", statusDate)
				.append("storageDateRangeFrom", storageDateRangeFrom)
				.append("storageDateRangeTo", storageDateRangeTo)
				.append("storageDays", storageDays)
				.append("distributionAmount", distributionAmount)
				.append("payPayableStatus", payPayableStatus)
				.append("payPayableDate", payPayableDate)
				.append("payPayableVia", payPayableVia)
				.append("sendEstToClient", sendEstToClient)
				.append("sendActualToClient", sendActualToClient)
				.append("doNotSendtoClient", doNotSendtoClient)
				.append("branchCode", branchCode)
				.append("branchName", branchName)
				.append("authorization", authorization)
				.append("accrueRevenue", accrueRevenue)
				.append("accruePayable", accruePayable)
				.append("actgCode", actgCode)
				.append("estimateDate", estimateDate)
				.append("quoteDescription", quoteDescription)
				.append("recXferUser", recXferUser)
				.append("payXferUser", payXferUser)
				.append("accrueRevenueReverse", accrueRevenueReverse)
				.append("accruePayableReverse", accruePayableReverse)
				.append("recRateCurrency", recRateCurrency)
				.append("recCurrencyRate", recCurrencyRate)
				.append("recRateExchange", recRateExchange)
				.append("actualRevenueForeign", actualRevenueForeign)
				.append("racValueDate", racValueDate)
				.append("accrueRevenueManual", accrueRevenueManual)
				.append("accruePayableManual", accruePayableManual)
				.append("receivedAmount", receivedAmount)
				.append("invoiceCreatedBy", invoiceCreatedBy)
				.append("payPayableAmount", payPayableAmount)
				.append("creditInvoiceNumber", creditInvoiceNumber)
				.append("truckNumber", truckNumber)
				.append("oldBillToCode", oldBillToCode)
				.append("includeLHF", includeLHF)
				.append("companyDivision", companyDivision)
				.append("displayOnQuote", displayOnQuote)
				.append("recVatPercent", recVatPercent)
				.append("recVatDescr", recVatDescr)
				.append("recVatAmt", recVatAmt)
				.append("payVatPercent", payVatPercent)
				.append("payVatDescr", payVatDescr)
				.append("payVatAmt", payVatAmt)
				.append("revisionExpVatAmt",revisionExpVatAmt)
				.append("qstPayVatGl",qstPayVatGl)
				.append("qstRecVatGl",qstRecVatGl)
				.append("qstPayVatAmt",qstPayVatAmt)
				.append("qstRecVatAmt",qstRecVatAmt)
				.append("estExpVatAmt",estExpVatAmt)
				.append("estCurrency", estCurrency)
				.append("estLocalAmount", estLocalAmount)
				.append("estExchangeRate", estExchangeRate)
				.append("estValueDate", estValueDate)
				.append("revisionCurrency", revisionCurrency)
				.append("revisionLocalAmount", revisionLocalAmount)
				.append("revisionExchangeRate", revisionExchangeRate)
				.append("revisionValueDate", revisionValueDate)
				.append("uvlControlNumber", uvlControlNumber)
				.append("ddrControlNumber", ddrControlNumber)
				.append("estLocalRate", estLocalRate)
				.append("revisionLocalRate", revisionLocalRate)
				.append("groupAccount", groupAccount)
				.append("estimateSellDeviation", estimateSellDeviation)
				.append("estimateDeviation", estimateDeviation)
				.append("revisionSellDeviation", revisionSellDeviation)
				.append("revisionDeviation", revisionDeviation)
				.append("buyDependSell", buyDependSell)
				.append("VATExclude", VATExclude)
				.append("estVatPercent", estVatPercent)
				.append("estVatDescr", estVatDescr)
				.append("estVatAmt", estVatAmt).append("deviation", deviation)
				.append("additionalService", additionalService)
				.append("settledDate", settledDate)
				.append("receivableSellDeviation", receivableSellDeviation)
				.append("revisionVatPercent", revisionVatPercent)
				.append("revisionVatDescr", revisionVatDescr)
				.append("revisionVatAmt", revisionVatAmt)
				.append("payDeviation", payDeviation)
				.append("contractCurrency", contractCurrency)
				.append("contractRate", contractRate)
				.append("contractExchangeRate", contractExchangeRate)
				.append("contractRateAmmount", contractRateAmmount)
				.append("contractValueDate", contractValueDate) 
				.append("networkSynchedId", networkSynchedId) 
				.append("agentFeesNo", agentFeesNo)
				.append("payableContractCurrency", payableContractCurrency) 
				.append("payableContractExchangeRate", payableContractExchangeRate)
				.append("payableContractRateAmmount", payableContractRateAmmount)
				.append("payableContractValueDate", payableContractValueDate)
				.append("estSellCurrency", estSellCurrency)
				.append("estSellLocalAmount", estSellLocalAmount)
				.append("estSellExchangeRate", estSellExchangeRate)
				.append("estSellValueDate", estSellValueDate)
				.append("estSellLocalRate", estSellLocalRate)
				.append("revisionSellCurrency", revisionSellCurrency)
				.append("revisionSellLocalAmount", revisionSellLocalAmount)
				.append("revisionSellExchangeRate", revisionSellExchangeRate)
				.append("revisionSellValueDate", revisionSellValueDate)
				.append("revisionSellLocalRate", revisionSellLocalRate)
				.append("estimateContractCurrency", estimateContractCurrency)
				.append("estimateContractRate", estimateContractRate)
                .append("estimateContractExchangeRate", estimateContractExchangeRate)
				.append("estimateContractRateAmmount", estimateContractRateAmmount)
				.append("estimateContractValueDate", estimateContractValueDate)
				.append("estimatePayableContractCurrency", estimatePayableContractCurrency)
				.append("estimatePayableContractRate", estimatePayableContractRate)
				.append("estimatePayableContractExchangeRate", estimatePayableContractExchangeRate)
				.append("estimatePayableContractRateAmmount", estimatePayableContractRateAmmount)
				.append("estimatePayableContractValueDate", estimatePayableContractValueDate)
				.append("revisionContractCurrency", revisionContractCurrency)
				.append("revisionContractRate", revisionContractRate)
				.append("revisionContractExchangeRate", revisionContractExchangeRate)
				.append("revisionContractRateAmmount", revisionContractRateAmmount)
				.append("revisionContractValueDate", revisionContractValueDate)
				.append("revisionPayableContractCurrency", revisionPayableContractCurrency)
				.append("revisionPayableContractRate", revisionPayableContractRate)
				.append("revisionPayableContractExchangeRate", revisionPayableContractExchangeRate)
				.append("revisionPayableContractRateAmmount", revisionPayableContractRateAmmount)
				.append("revisionPayableContractValueDate", revisionPayableContractValueDate)
				.append("costTransferred", costTransferred)
				.append("categoryResource", categoryResource)
				.append("categoryResourceName",categoryResourceName)
				.append("t20Process",t20Process)
				.append("activateAccPortal",activateAccPortal)
				.append("vanlineSettleDate",vanlineSettleDate)
				.append("onHand",onHand) 
				.append("estimateStatus",estimateStatus)
				.append("division",division)
				.append("paymentSent",paymentSent)
		        .append("invoiceAutoUpload",invoiceAutoUpload) 
		        .append("invoiceType",invoiceType)
		        .append("driverCompanyDivision",driverCompanyDivision)
		        .append("recVatGl",recVatGl)
		        .append("payVatGl",recVatGl)
		        .append("selectiveInvoice",selectiveInvoice)
		        .append("rollUpInInvoice", rollUpInInvoice)
		        .append("serviceTaxInput",serviceTaxInput)
		        .append("myFileFileName",myFileFileName)
		        .append("externalIntegrationReference",externalIntegrationReference)
		        .append("estimateSellQuantity", estimateSellQuantity)
		        .append("revisionSellQuantity", revisionSellQuantity)
 		        .append("estimateDiscount", estimateDiscount)
			    .append("revisionDiscount", revisionDiscount)
			    .append("actualDiscount", actualDiscount)
			    .append("accountLineCostElement",accountLineCostElement)
			    .append("accountLineScostElementDescription",accountLineScostElementDescription)
 		        .append("dynamicNavisionflag", dynamicNavisionflag)
			    .append("deleteDynamicNavisionflag", deleteDynamicNavisionflag)
			    .append("oldCompanyDivision", oldCompanyDivision)
			    .append("dynamicNavXfer",dynamicNavXfer)
			    .append("sendDynamicNavisionDate",sendDynamicNavisionDate)
			    .append("auditCompleteDate",auditCompleteDate)
		        .append("networkBillToCode",networkBillToCode)
				.append("networkBillToName",networkBillToName)
				.append("varianceRevenueAmount",varianceRevenueAmount)
				.append("varianceExpenseAmount",varianceExpenseAmount)
				.append("accrueRevenueVariance",accrueRevenueVariance)
				.append("accrueExpenseVariance",accrueExpenseVariance)
				.append("revisionDescription",revisionDescription)
				.append("writeOffReason",writeOffReason)
				.append("managerApprovalDate",managerApprovalDate)
				.append("managerApprovalName",managerApprovalName)
				.append("authorizedCreditNote",authorizedCreditNote)
				.append("invoicereportparameter",invoicereportparameter)
				.append("payQuantity",payQuantity)
				.append("payRate",payRate) 
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AccountLine))
			return false;
		AccountLine castOther = (AccountLine) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID).append(categoryResource,castOther.categoryResource)
				.append(id, castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(shipNumber, castOther.shipNumber)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(accountLineNumber, castOther.accountLineNumber)
				.append(category, castOther.category)
				.append(vendorCode, castOther.vendorCode)
				.append(name, castOther.name)
				.append(chargeCode, castOther.chargeCode)
				.append(reference, castOther.reference)
				.append(billToCode, castOther.billToCode)
				.append(billToName, castOther.billToName)
				.append(estimateRate, castOther.estimateRate)
				.append(estimateSellRate, castOther.estimateSellRate)
				.append(estimateExpense, castOther.estimateExpense)
				.append(estimateVendorName, castOther.estimateVendorName)
				.append(estimateRevenueAmount, castOther.estimateRevenueAmount)
				.append(estimateQuantity, castOther.estimateQuantity)
				.append(estimatePassPercentage,
						castOther.estimatePassPercentage)
				.append(download, castOther.download)
				.append(revisionRate, castOther.revisionRate)
				.append(revisionSellRate, castOther.revisionSellRate)
				.append(revisionExpense, castOther.revisionExpense)
				.append(revisionRevenueAmount, castOther.revisionRevenueAmount)
				.append(revisionQuantity, castOther.revisionQuantity)
				.append(revisionPassPercentage,
						castOther.revisionPassPercentage)
				.append(entitlementAmount, castOther.entitlementAmount)
				.append(status, castOther.status)
				.append(ignoreForBilling, castOther.ignoreForBilling)
				.append(contract, castOther.contract)
				.append(basis, castOther.basis)
				.append(basisNew, castOther.basisNew)
				.append(itemNew, castOther.itemNew)
				.append(checkNew, castOther.checkNew)
				.append(basisNewType, castOther.basisNewType)
				.append(actualExpense, castOther.actualExpense)
				.append(actualRevenue, castOther.actualRevenue)
				.append(recInvoiceNumber, castOther.recInvoiceNumber)
				.append(receivedInvoiceDate, castOther.receivedInvoiceDate)
				.append(recQuantity, castOther.recQuantity)
				.append(recRate, castOther.recRate)
				.append(description, castOther.description)
				.append(recGl, castOther.recGl)
				.append(payGl, castOther.payGl)
				.append(invoiceDate, castOther.invoiceDate)
				.append(payPostDate, castOther.payPostDate)
				.append(recPostDate, castOther.recPostDate)
				.append(payAccDate, castOther.payAccDate)
				.append(recAccDate, castOther.recAccDate)
				.append(recXfer, castOther.recXfer)
				.append(payXfer, castOther.payXfer)
				.append(receivedDate, castOther.receivedDate)
				.append(valueDate, castOther.valueDate)
				.append(country, castOther.country)
				.append(note, castOther.note)
				.append(payingStatus, castOther.payingStatus)
				.append(invoiceNumber, castOther.invoiceNumber)
				.append(localAmount, castOther.localAmount)
				.append(exchangeRate, castOther.exchangeRate)
				.append(convertedAmount, castOther.convertedAmount) 
				.append(glType, castOther.glType)
				.append(commission, castOther.commission)
				.append(externalReference, castOther.externalReference)
				.append(paymentStatus, castOther.paymentStatus)
				.append(statusDate, castOther.statusDate)
				.append(storageDateRangeFrom, castOther.storageDateRangeFrom)
				.append(storageDateRangeTo, castOther.storageDateRangeTo)
				.append(storageDays, castOther.storageDays)
				.append(distributionAmount, castOther.distributionAmount)
				.append(payPayableStatus, castOther.payPayableStatus)
				.append(payPayableDate, castOther.payPayableDate)
				.append(payPayableVia, castOther.payPayableVia)
				.append(sendEstToClient, castOther.sendEstToClient)
				.append(sendActualToClient, castOther.sendActualToClient)
				.append(doNotSendtoClient, castOther.doNotSendtoClient)
				.append(branchCode, castOther.branchCode)
				.append(branchName, castOther.branchName)
				.append(authorization, castOther.authorization)
				.append(accrueRevenue, castOther.accrueRevenue)
				.append(accruePayable, castOther.accruePayable)
				.append(actgCode, castOther.actgCode)
				.append(estimateDate, castOther.estimateDate)
				.append(quoteDescription, castOther.quoteDescription)
				.append(recXferUser, castOther.recXferUser)
				.append(payXferUser, castOther.payXferUser)
				.append(accrueRevenueReverse, castOther.accrueRevenueReverse)
				.append(accruePayableReverse, castOther.accruePayableReverse)
				.append(recRateCurrency, castOther.recRateCurrency)
				.append(recCurrencyRate, castOther.recCurrencyRate)
				.append(recRateExchange, castOther.recRateExchange)
				.append(actualRevenueForeign, castOther.actualRevenueForeign)
				.append(racValueDate, castOther.racValueDate)
				.append(accrueRevenueManual, castOther.accrueRevenueManual)
				.append(accruePayableManual, castOther.accruePayableManual)
				.append(receivedAmount, castOther.receivedAmount)
				.append(invoiceCreatedBy, castOther.invoiceCreatedBy)
				.append(payPayableAmount, castOther.payPayableAmount)
				.append(creditInvoiceNumber, castOther.creditInvoiceNumber)
				.append(truckNumber, castOther.truckNumber)
				.append(oldBillToCode, castOther.oldBillToCode)
				.append(includeLHF, castOther.includeLHF)
				.append(companyDivision, castOther.companyDivision)
				.append(displayOnQuote, castOther.displayOnQuote)
				.append(recVatPercent, castOther.recVatPercent)
				.append(recVatDescr, castOther.recVatDescr)
				.append(recVatAmt, castOther.recVatAmt)
				.append(payVatPercent, castOther.payVatPercent)
				.append(payVatDescr, castOther.payVatDescr)
				.append(payVatAmt, castOther.payVatAmt)
				.append(revisionExpVatAmt, castOther.revisionExpVatAmt)
				.append(qstPayVatGl, castOther.qstPayVatGl)
				.append(qstRecVatGl, castOther.qstRecVatGl)
				.append(qstPayVatAmt, castOther.qstPayVatAmt)
				.append(qstRecVatAmt, castOther.qstRecVatAmt)
				.append(estExpVatAmt, castOther.estExpVatAmt)
				.append(estCurrency, castOther.estCurrency)
				.append(estLocalAmount, castOther.estLocalAmount)
				.append(estExchangeRate, castOther.estExchangeRate)
				.append(estValueDate, castOther.estValueDate)
				.append(revisionCurrency, castOther.revisionCurrency)
				.append(revisionLocalAmount, castOther.revisionLocalAmount)
				.append(revisionExchangeRate, castOther.revisionExchangeRate)
				.append(revisionValueDate, castOther.revisionValueDate)
				.append(uvlControlNumber, castOther.uvlControlNumber)
				.append(ddrControlNumber, castOther.ddrControlNumber)
				.append(estLocalRate, castOther.estLocalRate)
				.append(revisionLocalRate, castOther.revisionLocalRate)
				.append(groupAccount, castOther.groupAccount)
				.append(estimateSellDeviation, castOther.estimateSellDeviation)
				.append(estimateDeviation, castOther.estimateDeviation)
				.append(revisionSellDeviation, castOther.revisionSellDeviation)
				.append(revisionDeviation, castOther.revisionDeviation)
				.append(buyDependSell, castOther.buyDependSell)
				.append(VATExclude, castOther.VATExclude)
				.append(estVatPercent, castOther.estVatPercent)
				.append(estVatDescr, castOther.estVatDescr)
				.append(estVatAmt, castOther.estVatAmt)
				.append(deviation, castOther.deviation)
				.append(additionalService, castOther.additionalService)
				.append(settledDate, castOther.settledDate)
				.append(receivableSellDeviation, castOther.receivableSellDeviation) 
				.append(revisionVatPercent, castOther.revisionVatPercent) 
				.append(revisionVatDescr, castOther.revisionVatDescr) 
				.append(revisionVatAmt, castOther.revisionVatAmt) 
				.append(payDeviation, castOther.payDeviation) 
				.append(contractCurrency, castOther.contractCurrency)
				.append(contractRate, castOther.contractRate)
				.append(contractExchangeRate, castOther.contractExchangeRate)
				.append(contractRateAmmount, castOther.contractRateAmmount)
				.append(contractValueDate, castOther.contractValueDate)
				.append(networkSynchedId, castOther.networkSynchedId) 
				.append(agentFeesNo, castOther.agentFeesNo)
				.append(payableContractCurrency, castOther.payableContractCurrency)  
				.append(payableContractExchangeRate, castOther.payableContractExchangeRate) 
				.append(payableContractRateAmmount, castOther.payableContractRateAmmount) 
				.append(payableContractValueDate, castOther.payableContractValueDate)
				.append(estSellCurrency, castOther.estSellCurrency)
				.append(estSellLocalAmount, castOther.estSellLocalAmount)
				.append(estSellExchangeRate, castOther.estSellExchangeRate)
				.append(estSellValueDate, castOther.estSellValueDate)
				.append(estSellLocalRate, castOther.estSellLocalRate)
				.append(revisionSellCurrency, castOther.revisionSellCurrency)
				.append(revisionSellLocalAmount, castOther.revisionSellLocalAmount)
				.append(revisionSellExchangeRate, castOther.revisionSellExchangeRate)
				.append(revisionSellValueDate, castOther.revisionSellValueDate)
				.append(revisionSellLocalRate, castOther.revisionSellLocalRate)
				.append(estimateContractCurrency, castOther.estimateContractCurrency)
				.append(estimateContractRate, castOther.estimateContractRate)
				.append(estimateContractExchangeRate, castOther.estimateContractExchangeRate)
				.append(estimateContractRateAmmount, castOther.estimateContractRateAmmount)
				.append(estimateContractValueDate, castOther.estimateContractValueDate)
				.append(estimatePayableContractCurrency, castOther.estimatePayableContractCurrency)
				.append(estimatePayableContractRate, castOther.estimatePayableContractRate)
				.append(estimatePayableContractExchangeRate, castOther.estimatePayableContractExchangeRate)
				.append(estimatePayableContractRateAmmount, castOther.estimatePayableContractRateAmmount)
				.append(estimatePayableContractValueDate, castOther.estimatePayableContractValueDate)
				.append(revisionContractCurrency, castOther.revisionContractCurrency)
				.append(revisionContractRate, castOther.revisionContractRate)
				.append(revisionContractExchangeRate, castOther.revisionContractExchangeRate)
				.append(revisionContractRateAmmount, castOther.revisionContractRateAmmount)
				.append(revisionContractValueDate, castOther.revisionContractValueDate)
				.append(revisionPayableContractCurrency, castOther.revisionPayableContractCurrency)
				.append(revisionPayableContractRate, castOther.revisionPayableContractRate)
				.append(revisionPayableContractExchangeRate, castOther.revisionPayableContractExchangeRate)
				.append(revisionPayableContractRateAmmount, castOther.revisionPayableContractRateAmmount)
				.append(revisionPayableContractValueDate, castOther.revisionPayableContractValueDate)
				.append(costTransferred, castOther.costTransferred).append(categoryResourceName,castOther.categoryResourceName)
				.append(t20Process,castOther.t20Process)
				.append(activateAccPortal,castOther.activateAccPortal)
				.append(vanlineSettleDate,castOther.vanlineSettleDate)
				.append(onHand,castOther.onHand) 
				.append(estimateStatus,castOther.estimateStatus)
				.append(division, castOther.division)
				.append(invoiceAutoUpload, castOther.invoiceAutoUpload)
				.append(invoiceType, castOther.invoiceType)
				.append(paymentSent,castOther.paymentSent)
				.append(driverCompanyDivision,castOther.driverCompanyDivision)
				.append(recVatGl,castOther.recVatGl)
				.append(payVatGl,castOther.payVatGl)
				.append(selectiveInvoice,castOther.selectiveInvoice)
				.append(rollUpInInvoice, castOther.rollUpInInvoice)
				.append(serviceTaxInput,castOther.serviceTaxInput)
				.append(myFileFileName,castOther.myFileFileName)
				.append(externalIntegrationReference,castOther.externalIntegrationReference)
				.append(estimateSellQuantity,castOther.estimateSellQuantity)
				.append(revisionSellQuantity,castOther.revisionSellQuantity)
		        .append(estimateDiscount,castOther.estimateDiscount)
		        .append(revisionDiscount,castOther.revisionDiscount)
		        .append(actualDiscount,castOther.actualDiscount)
		        .append(accountLineCostElement,castOther.accountLineCostElement)
		        .append(accountLineScostElementDescription,castOther.accountLineScostElementDescription)
		        .append(dynamicNavisionflag,castOther.dynamicNavisionflag)
		        .append(deleteDynamicNavisionflag,castOther.deleteDynamicNavisionflag)
		        .append(oldCompanyDivision,castOther.oldCompanyDivision)
		        .append(dynamicNavXfer,castOther.dynamicNavXfer)
		        .append(sendDynamicNavisionDate,castOther.sendDynamicNavisionDate)
		        .append(auditCompleteDate,castOther.auditCompleteDate)
		        .append("networkBillToCode", castOther.networkBillToCode)
				.append("networkBillToName", castOther.networkBillToName)
				.append("varianceRevenueAmount", castOther.varianceRevenueAmount)
				.append("varianceExpenseAmount", castOther.varianceExpenseAmount)
				.append("accrueRevenueVariance", castOther.accrueRevenueVariance)
				.append("accrueExpenseVariance", castOther.accrueExpenseVariance)
				.append("revisionDescription", castOther.revisionDescription)
				.append("writeOffReason", castOther.writeOffReason)
				.append("managerApprovalDate", castOther.managerApprovalDate)
				.append("managerApprovalName", castOther.managerApprovalName)
				.append("authorizedCreditNote", castOther.authorizedCreditNote)
				.append("invoicereportparameter", castOther.invoicereportparameter)
				.append("payQuantity", castOther.payQuantity)
				.append("payRate", castOther.payRate) 
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(categoryResource).append(categoryResourceName)
				.append(serviceOrderId).append(sequenceNumber)
				.append(shipNumber).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(accountLineNumber)
				.append(category).append(vendorCode).append(name)
				.append(chargeCode).append(reference).append(billToCode)
				.append(billToName).append(estimateRate)
				.append(estimateSellRate).append(estimateExpense)
				.append(estimateVendorName).append(estimateRevenueAmount)
				.append(estimateQuantity).append(estimatePassPercentage)
				.append(revisionRate).append(revisionSellRate)
				.append(revisionExpense).append(revisionRevenueAmount)
				.append(revisionQuantity).append(revisionPassPercentage)
				.append(entitlementAmount).append(status)
				.append(ignoreForBilling).append(contract).append(basis)
				.append(basisNew).append(itemNew).append(checkNew)
				.append(basisNewType).append(actualExpense)
				.append(actualRevenue).append(download).append(recInvoiceNumber)
				.append(receivedInvoiceDate).append(recQuantity)
				.append(recRate).append(description).append(recGl)
				.append(payGl).append(invoiceDate).append(payPostDate)
				.append(recPostDate).append(payAccDate).append(recAccDate)
				.append(recXfer).append(payXfer).append(receivedDate)
				.append(valueDate).append(country).append(note)
				.append(payingStatus).append(invoiceNumber).append(localAmount)
				.append(exchangeRate).append(convertedAmount)
				.append(glType).append(commission)
				.append(externalReference).append(paymentStatus)
				.append(statusDate).append(storageDateRangeFrom)
				.append(storageDateRangeTo).append(storageDays)
				.append(distributionAmount).append(payPayableStatus)
				.append(payPayableDate).append(payPayableVia)
				.append(sendEstToClient).append(sendActualToClient)
				.append(doNotSendtoClient).append(branchCode)
				.append(branchName).append(authorization).append(accrueRevenue)
				.append(accruePayable).append(actgCode).append(estimateDate)
				.append(quoteDescription).append(recXferUser)
				.append(payXferUser).append(accrueRevenueReverse)
				.append(accruePayableReverse).append(recRateCurrency)
				.append(recCurrencyRate).append(recRateExchange)
				.append(actualRevenueForeign).append(racValueDate)
				.append(accrueRevenueManual).append(accruePayableManual)
				.append(receivedAmount).append(invoiceCreatedBy)
				.append(payPayableAmount).append(creditInvoiceNumber)
				.append(truckNumber).append(oldBillToCode).append(includeLHF)
				.append(companyDivision).append(displayOnQuote)
				.append(recVatPercent).append(recVatDescr).append(recVatAmt)
				.append(payVatPercent).append(payVatDescr).append(payVatAmt).append(estExpVatAmt).append(revisionExpVatAmt).append(qstRecVatAmt)
				.append(qstPayVatGl)
				.append(qstRecVatGl)
				.append(qstPayVatAmt)
				.append(estCurrency).append(estLocalAmount)
				.append(estExchangeRate).append(estValueDate)
				.append(revisionCurrency).append(revisionLocalAmount)
				.append(revisionExchangeRate).append(revisionValueDate)
				.append(uvlControlNumber).append(ddrControlNumber)
				.append(estLocalRate).append(revisionLocalRate)
				.append(groupAccount).append(estimateSellDeviation)
				.append(estimateDeviation).append(revisionSellDeviation)
				.append(revisionDeviation).append(buyDependSell)
				.append(VATExclude).append(estVatPercent).append(estVatDescr)
				.append(estVatAmt).append(deviation).append(additionalService)
				.append(receivableSellDeviation).append(settledDate) 
				.append(revisionVatPercent) 
				.append(revisionVatDescr) 
				.append(revisionVatAmt)
				.append(payDeviation) 
				.append(contractCurrency)
				.append(contractRate)
				.append(contractExchangeRate)
				.append(contractRateAmmount)
				.append(contractValueDate) 
				.append(networkSynchedId) 
				.append(agentFeesNo)
				.append(payableContractCurrency) 
				.append(payableContractExchangeRate)
				.append(payableContractRateAmmount)
				.append(payableContractValueDate)
				.append(estSellCurrency)
				.append(estSellLocalAmount)
				.append(estSellExchangeRate)
				.append(estSellValueDate)
				.append(estSellLocalRate)
				.append(revisionSellCurrency)
				.append(revisionSellLocalAmount)
				.append(revisionSellExchangeRate)
				.append(revisionSellValueDate)
				.append(revisionSellLocalRate)
				.append(estimateContractCurrency)
				.append(estimateContractRate)
				.append(estimateContractExchangeRate)
				.append(estimateContractRateAmmount)
				.append(estimateContractValueDate)
				.append(estimatePayableContractCurrency)
				.append(estimatePayableContractRate)
				.append(estimatePayableContractExchangeRate)
				.append(estimatePayableContractRateAmmount)
				.append(estimatePayableContractValueDate)
				.append(revisionContractCurrency)
				.append(revisionContractRate)
				.append(revisionContractExchangeRate)
				.append(revisionContractRateAmmount)
				.append(revisionContractValueDate)
				.append(revisionPayableContractCurrency)
				.append(revisionPayableContractRate)
				.append(revisionPayableContractExchangeRate)
				.append(revisionPayableContractRateAmmount)
				.append(revisionPayableContractValueDate)
				.append(costTransferred)
				.append(t20Process)
				.append(activateAccPortal)
				.append(vanlineSettleDate)
				.append(onHand) 
				.append(estimateStatus)
				.append(division)
				.append(invoiceAutoUpload)
				.append(invoiceType)
				.append(paymentSent)
				.append(driverCompanyDivision)
				.append(recVatGl)
				.append(payVatGl)
				.append(selectiveInvoice)
				.append(rollUpInInvoice)
				.append(serviceTaxInput)
				.append(myFileFileName)
				.append(externalIntegrationReference)
				.append(estimateSellQuantity)
				.append(revisionSellQuantity)
				.append(estimateDiscount)
		        .append(revisionDiscount)
		        .append(actualDiscount)
		        .append(accountLineCostElement)
		        .append(accountLineScostElementDescription)
				.append(dynamicNavisionflag)
		        .append(deleteDynamicNavisionflag)
		        .append(oldCompanyDivision)
		        .append(dynamicNavXfer)
		        .append(sendDynamicNavisionDate)
		        .append(auditCompleteDate)
		        .append(networkBillToCode)
		        .append(networkBillToName)
		        .append(varianceRevenueAmount)
		        .append(varianceExpenseAmount)
		        .append(accrueRevenueVariance)
		        .append(accrueExpenseVariance).append(revisionDescription)
		        .append(writeOffReason).append(managerApprovalDate)
		        .append(managerApprovalName).append(authorizedCreditNote)
		        .append(invoicereportparameter)
		        .append(payQuantity)
		        .append(payRate) 
				.toHashCode();
	}

	@Column
	public Date getStorageDateRangeFrom() {
		return storageDateRangeFrom;
	}

	public void setStorageDateRangeFrom(Date storageDateRangeFrom) {
		this.storageDateRangeFrom = storageDateRangeFrom;
	}
	@Column
	public Date getStorageDateRangeTo() {
		return storageDateRangeTo;
	}

	public void setStorageDateRangeTo(Date storageDateRangeTo) {
		this.storageDateRangeTo = storageDateRangeTo;
	}
	@Column(length=5)
	public Integer getStorageDays() {
		return storageDays;
	}

	public void setStorageDays(Integer storageDays) {
		this.storageDays = storageDays;
	}

	@Column(length=50)
	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}
	@Column(length=50)
	public String getGlType() {
		return glType;
	}

	public void setGlType(String glType) {
		this.glType = glType;
	}

	@ManyToOne
	@JoinColumn(name="serviceOrderId", nullable=false, updatable=false,
			referencedColumnName="id")
			public ServiceOrder getServiceOrder() {
			        return serviceOrder;
			    }
			    
				 public void setServiceOrder(ServiceOrder serviceOrder) {
						this.serviceOrder = serviceOrder;
				}
                @Column(length=25)
				public BigDecimal getActualExpense() {
					return actualExpense;
				}

				public void setActualExpense(BigDecimal actualExpense) {
					this.actualExpense = actualExpense;
				}
				@Column(length=25)
				public BigDecimal getActualRevenue() {
					return actualRevenue;
				}

				public void setActualRevenue(BigDecimal actualRevenue) {
					this.actualRevenue = actualRevenue;
				}
				@Column(length=15)
				public String getBasis() {
					return basis;
				}

				public void setBasis(String basis) {
					this.basis = basis;
				}
				@Column(length=25)
				public String getCategory() {
					return category;
				}

				public void setCategory(String category) {
					this.category = category;
				}
				@Column(length=25)
				public String getChargeCode() {
					return chargeCode;
				}

				public void setChargeCode(String chargeCode) {
					this.chargeCode = chargeCode;
				}
				@Column(length=30)
				public String getContract() {
					return contract;
				}

				public void setContract(String contract) {
					this.contract = contract;
				}
				@Column(length=25)
				public String getCorpID() {
					return corpID;
				}

				public void setCorpID(String corpID) {
					this.corpID = corpID;
				}
				@Column(length=82)
				public String getCreatedBy() {
					return createdBy;
				}

				public void setCreatedBy(String createdBy) {
					this.createdBy = createdBy;
				}
				@Column(length=25)
				public Date getCreatedOn() {
					return createdOn;
				}

				public void setCreatedOn(Date createdOn) {
					this.createdOn = createdOn;
				}
				/*@Column(precision=15,scale=2)
				public BigDecimal getExpense() {
					return estimateExpense;
				}

				public void setExpense(BigDecimal estimateExpense) {
					this.estimateExpense = estimateExpense;
				}*/
				@Id @GeneratedValue(strategy = GenerationType.AUTO)
				public Long getId() {
					return id;
				}

				public void setId(Long id) {
					this.id = id;
				}
				@Column(length=25)
				public String getName() {
					return name;
				}

				public void setName(String name) {
					this.name = name;
				}
				/*@Column(length=25)
				public BigDecimal getRate() {
					return estimateRate;
				}

				public void setRate(BigDecimal estimateRate) {
					this.estimateRate = estimateRate;
				}*/
				@Column(length=25)
				public String getReference() {
					return reference;
				}

				public void setReference(String reference) {
					this.reference = reference;
				}
				
				@Column(length=15)
				public String getSequenceNumber() {
					return sequenceNumber;
				}

				public void setSequenceNumber(String sequenceNumber) {
					this.sequenceNumber = sequenceNumber;
				}
				@Column(length=82)
				public String getUpdatedBy() {
					return updatedBy;
				}

				public void setUpdatedBy(String updatedBy) {
					this.updatedBy = updatedBy;
				}
				@Column(length=25)
				public Date getUpdatedOn() {
					return updatedOn;
				}

				public void setUpdatedOn(Date updatedOn) {
					this.updatedOn = updatedOn;
				}
				@Column(length=25)
				public String getVendorCode() {
					return vendorCode;
				}

				public void setVendorCode(String vendorCode) {
					this.vendorCode = vendorCode;
				}
				@Column(length=25)
				public BigDecimal getEntitlementAmount() {
					return entitlementAmount;
				}

				public void setEntitlementAmount(BigDecimal entitlementAmount) {
					this.entitlementAmount = entitlementAmount;
				}
				@Column(length=15)
				public String getShipNumber() {
					return shipNumber;
				}

				public void setShipNumber(String shipNumber) {
					this.shipNumber = shipNumber;
				}
				@Column(length=25)
				public BigDecimal getEstimateExpense() {
					return estimateExpense;
				}

				public void setEstimateExpense(BigDecimal estimateExpense) {
					this.estimateExpense = estimateExpense;
				}
				@Column(length=25)
				public BigDecimal getEstimateQuantity() {
					return estimateQuantity;
				}

				public void setEstimateQuantity(BigDecimal estimateQuantity) {
					this.estimateQuantity = estimateQuantity;
				}
				@Column(length=25)
				public BigDecimal getEstimateRate() {
					return estimateRate;
				}

				public void setEstimateRate(BigDecimal estimateRate) {
					this.estimateRate = estimateRate;
				}
				@Column(length=25)
				public BigDecimal getEstimateRevenueAmount() {
					return estimateRevenueAmount;
				}

				public void setEstimateRevenueAmount(BigDecimal estimateRevenueAmount) {
					this.estimateRevenueAmount = estimateRevenueAmount;
				}
				@Column(length=255)
				public String getEstimateVendorName() {
					return estimateVendorName;
				}

				public void setEstimateVendorName(String estimateVendorName) {
					this.estimateVendorName = estimateVendorName;
				}
				@Column(length=25)
				public BigDecimal getRevisionExpense() {
					return revisionExpense;
				}

				public void setRevisionExpense(BigDecimal revisionExpense) {
					this.revisionExpense = revisionExpense;
				}
				@Column(length=25)
				public BigDecimal getRevisionQuantity() {
					return revisionQuantity;
				}

				public void setRevisionQuantity(BigDecimal revisionQuantity) {
					this.revisionQuantity = revisionQuantity;
				}
				@Column(length=25)
				public BigDecimal getRevisionRate() {
					return revisionRate;
				}

				public void setRevisionRate(BigDecimal revisionRate) {
					this.revisionRate = revisionRate;
				}
				@Column(length=25)
				public BigDecimal getRevisionRevenueAmount() {
					return revisionRevenueAmount;
				}

				public void setRevisionRevenueAmount(BigDecimal revisionRevenueAmount) {
					this.revisionRevenueAmount = revisionRevenueAmount;
				}
				@Column(length=10)
				public double getConvertedAmount() {
					return convertedAmount;
				}

				public void setConvertedAmount(double convertedAmount) {
					this.convertedAmount = convertedAmount;
				}
				@Column(length=25)
				public String getCountry() {
					return country;
				}

				public void setCountry(String country) {
					this.country = country;
				}
				@Column(length=10)
				public double getExchangeRate() {
					return exchangeRate;
				}

				public void setExchangeRate(double exchangeRate) {
					this.exchangeRate = exchangeRate;
				}
				@Column
				public Date getInvoiceDate() {
					return invoiceDate;
				}

				public void setInvoiceDate(Date invoiceDate) {
					this.invoiceDate = invoiceDate;
				}
				
				@Column(length=20)
				public BigDecimal getLocalAmount() {
					return localAmount;
				}

				public void setLocalAmount(BigDecimal localAmount) {
					this.localAmount = localAmount;
				}
				@Column(length=5000)
				public String getNote() {
					return note;
				}

				public void setNote(String note) {
					this.note = note;
				}
				@Column
				public Date getReceivedDate() {
					return receivedDate;
				}

				public void setReceivedDate(Date receivedDate) {
					this.receivedDate = receivedDate;
				}
				@Column(length=20)
				public String getPayingStatus() {
					return payingStatus;
				}

				public void setPayingStatus(String payingStatus) {
					this.payingStatus = payingStatus;
				}
				@Column
				public Date getValueDate() {
					return valueDate;
				}

				public void setValueDate(Date valueDate) {
					this.valueDate = valueDate;
				}
				@Column(length=5000)
				public String getDescription() {
					return description;
				}

				public void setDescription(String description) {
					this.description = description;
				}
				@Column
				public Date getReceivedInvoiceDate() {
					return receivedInvoiceDate;
				}

				public void setReceivedInvoiceDate(Date receivedInvoiceDate) {
					this.receivedInvoiceDate = receivedInvoiceDate;
				}
				@Column(length=25)
				public String getInvoiceNumber() {
					return invoiceNumber;
				}

				public void setInvoiceNumber(String invoiceNumber) {
					this.invoiceNumber = invoiceNumber;
				}
				@Column(length=20)
				public String getRecInvoiceNumber() {
					return recInvoiceNumber;
				}

				public void setRecInvoiceNumber(String recInvoiceNumber) {
					this.recInvoiceNumber = recInvoiceNumber;
				}

				@Column(length=20)
				public BigDecimal getRecQuantity() {
					return recQuantity;
				}

				public void setRecQuantity(BigDecimal recQuantity) {
					this.recQuantity = recQuantity;
				}
				@Column(length=24)
				public BigDecimal getRecRate() {
					return recRate;
				}

				public void setRecRate(BigDecimal recRate) {
					this.recRate = recRate;
				}
				@Column(length=4)
				public String getAccountLineNumber() {
					return accountLineNumber;
				}

				public void setAccountLineNumber(String accountLineNumber) {
					this.accountLineNumber = accountLineNumber;
				}
				@Column(length=5)
				public Integer getEstimatePassPercentage() {
					return estimatePassPercentage;
				}

				public void setEstimatePassPercentage(Integer estimatePassPercentage) {
					this.estimatePassPercentage = estimatePassPercentage;
				}
				@Column(length=5)
				public Integer getRevisionPassPercentage() {
					return revisionPassPercentage;
				}

				public void setRevisionPassPercentage(Integer revisionPassPercentage) {
					this.revisionPassPercentage = revisionPassPercentage;
				}  
				@Column(length=8)
				public String getBillToCode() {
					return billToCode;
				}

				public void setBillToCode(String billToCode) {
					this.billToCode = billToCode;
				}
				@Column(length=255)
				public String getBillToName() {
					return billToName;
				}

				public void setBillToName(String billToName) {
					this.billToName = billToName;
				} 
				@Column(length=20)
				public String getPayGl() {
					return payGl;
				}

				public void setPayGl(String payGl) {
					this.payGl = payGl;
				}
				@Column(length=20)
				public String getRecGl() {
					return recGl;
				}

				public void setRecGl(String recGl) {
					this.recGl = recGl;
				}
				@Column
				public Date getPayAccDate() {
					return payAccDate;
				}

				public void setPayAccDate(Date payAccDate) {
					this.payAccDate = payAccDate;
				}
				@Column
				public Date getPayPostDate() {
					return payPostDate;
				}

				public void setPayPostDate(Date payPostDate) {
					this.payPostDate = payPostDate;
				}
				@Column(length=50)
				public String getPayXfer() {
					return payXfer;
				}

				public void setPayXfer(String payXfer) {
					this.payXfer = payXfer;
				}
				@Column
				public Date getRecAccDate() {
					return recAccDate;
				}

				public void setRecAccDate(Date recAccDate) {
					this.recAccDate = recAccDate;
				}
				@Column
				public Date getRecPostDate() {
					return recPostDate;
				}

				public void setRecPostDate(Date recPostDate) {
					this.recPostDate = recPostDate;
				}
				@Column(length=50)
				public String getRecXfer() {
					return recXfer;
				}

				public void setRecXfer(String recXfer) {
					this.recXfer = recXfer;
				}
				@Column
				public String getBasisNew() {
					return basisNew;
				}

				public void setBasisNew(String basisNew) {
					this.basisNew = basisNew;
				}
				@Column
				public String getBasisNewType() {
					return basisNewType;
				}

				public void setBasisNewType(String basisNewType) {
					this.basisNewType = basisNewType;
				}
				@Column
				public String getCheckNew() {
					return checkNew;
				}

				public void setCheckNew(String checkNew) {
					this.checkNew = checkNew;
				}
				@Column
				public String getItemNew() {
					return itemNew;
				}

				public void setItemNew(String itemNew) {
					this.itemNew = itemNew;
				}

				
				
				
				@Transient
				public String getListCode() {		
					return vendorCode;
				}

				@Transient
				public String getListDescription() {
					return this.estimateVendorName;
				}
				
				@Transient
				public String getListSecondDescription() {
					return null;
				} 
				
				@Transient
				public String getListThirdDescription() {
					return null;
				}
				
				@Transient
				public String getListFourthDescription() {
					return null;
				}
				
				@Transient
				public String getListFifthDescription() {
					return null;
				}
				
				@Transient
				public String getListSixthDescription() {
					return null;
				}
				@Column
				public boolean isStatus() {
					return status;
				}

				public void setStatus(boolean status) {
					this.status = status;
				}
				@Column(length=25)
				public BigDecimal getEstimateSellRate() {
					return estimateSellRate;
				}

				public void setEstimateSellRate(BigDecimal estimateSellRate) {
					this.estimateSellRate = estimateSellRate;
				}
				@Column(length=25)
				public BigDecimal getRevisionSellRate() {
					return revisionSellRate;
				}

				public void setRevisionSellRate(BigDecimal revisionSellRate) {
					this.revisionSellRate = revisionSellRate;
				}
				@Column(length=25)
				public String getExternalReference() {
					return externalReference;
				}

				public void setExternalReference(String externalReference) {
					this.externalReference = externalReference;
				}
				@Column(length=20)
				public String getPaymentStatus() {
					return paymentStatus;
				}

				public void setPaymentStatus(String paymentStatus) {
					this.paymentStatus = paymentStatus;
				}
				@Column
				public Date getStatusDate() {
					return statusDate;
				}

				public void setStatusDate(Date statusDate) {
					this.statusDate = statusDate;
				}
				@Column(length=25)
				public BigDecimal getDistributionAmount() {
					return distributionAmount;
				}

				public void setDistributionAmount(BigDecimal distributionAmount) {
					this.distributionAmount = distributionAmount;
				}
				@Column
				public Date getAccruePayable() {
					return accruePayable;
				}

				public void setAccruePayable(Date accruePayable) {
					this.accruePayable = accruePayable;
				}
				@Column
				public Date getAccrueRevenue() {
					return accrueRevenue;
				}

				public void setAccrueRevenue(Date accrueRevenue) {
					this.accrueRevenue = accrueRevenue;
				}
			  
				@Column(length=50)
                public String getAuthorization() {
					return authorization;
				}

				public void setAuthorization(String authorization) {
					this.authorization = authorization;
				}
 
				@Column
				public Date getDoNotSendtoClient() {
					return doNotSendtoClient;
				}

				public void setDoNotSendtoClient(Date doNotSendtoClient) {
					this.doNotSendtoClient = doNotSendtoClient;
				}
				@Column
				public Date getPayPayableDate() {
					return payPayableDate;
				}

				public void setPayPayableDate(Date payPayableDate) {
					this.payPayableDate = payPayableDate;
				}
				@Column(length=20)
				public String getPayPayableStatus() {
					return payPayableStatus;
				}

				public void setPayPayableStatus(String payPayableStatus) {
					this.payPayableStatus = payPayableStatus;
				}
				@Column
				public String getPayPayableVia() {
					return payPayableVia;
				}

				public void setPayPayableVia(String payPayableVia) {
					this.payPayableVia = payPayableVia;
				}
				@Column
				public Date getSendActualToClient() {
					return sendActualToClient;
				}

				public void setSendActualToClient(Date sendActualToClient) {
					this.sendActualToClient = sendActualToClient;
				}
				@Column
				public Date getSendEstToClient() {
					return sendEstToClient;
				}

				public void setSendEstToClient(Date sendEstToClient) {
					this.sendEstToClient = sendEstToClient;
				}
				@Column(length=20)
				public String getBranchCode() {
					return branchCode;
				}

				public void setBranchCode(String branchCode) {
					this.branchCode = branchCode;
				}
				@Column(length=225)
				public String getBranchName() {
					return branchName;
				}

				public void setBranchName(String branchName) {
					this.branchName = branchName;
				}
				
				@Transient
				public String getListSeventhDescription() { 
					return this.actgCode;
				}
				@Column( length=20 )
				public String getActgCode() {
					return actgCode;
				}

				public void setActgCode(String actgCode) {
					this.actgCode = actgCode;
				}
				@Column
				public Date getEstimateDate() {
					return estimateDate;
				}

				public void setEstimateDate(Date estimateDate) {
					this.estimateDate = estimateDate;
				}
				@Column(length=5000)
				public String getQuoteDescription() {
					return quoteDescription;
				}

				public void setQuoteDescription(String quoteDescription) {
					this.quoteDescription = quoteDescription;
				}

				/**
				 * @return the serviceOrderId
				 */
				@Column(length=20, insertable = false, updatable = false)
				public Long getServiceOrderId() {
					return serviceOrderId;
				}

				/**
				 * @param serviceOrderId the serviceOrderId to set
				 */
				public void setServiceOrderId(Long serviceOrderId) {
					this.serviceOrderId = serviceOrderId;
				}
				@Column(length=25)
				public String getPayXferUser() {
					return payXferUser;
				}

				public void setPayXferUser(String payXferUser) {
					this.payXferUser = payXferUser;
				}
				@Column(length=25)
				public String getRecXferUser() {
					return recXferUser;
				}

				public void setRecXferUser(String recXferUser) {
					this.recXferUser = recXferUser;
				}
				@Column
				public Date getAccruePayableReverse() {
					return accruePayableReverse;
				}

				public void setAccruePayableReverse(Date accruePayableReverse) {
					this.accruePayableReverse = accruePayableReverse;
				}
				@Column
				public Date getAccrueRevenueReverse() {
					return accrueRevenueReverse;
				}

				public void setAccrueRevenueReverse(Date accrueRevenueReverse) {
					this.accrueRevenueReverse = accrueRevenueReverse;
				}
				
				@Column(length=25)
				public BigDecimal getRecCurrencyRate() {
					return recCurrencyRate;
				}

				public void setRecCurrencyRate(BigDecimal recCurrencyRate) {
					this.recCurrencyRate = recCurrencyRate;
				}
				@Column(length=25)
				public BigDecimal getRecRateExchange() {
					return recRateExchange;
				}

				public void setRecRateExchange(BigDecimal recRateExchange) {
					this.recRateExchange = recRateExchange;
				}
				@Column
				public Date getRacValueDate() {
					return racValueDate;
				}

				public void setRacValueDate(Date racValueDate) {
					this.racValueDate = racValueDate;
				}
				@Column(length=25)
				public BigDecimal getActualRevenueForeign() {
					return actualRevenueForeign;
				}

				public void setActualRevenueForeign(BigDecimal actualRevenueForeign) {
					this.actualRevenueForeign = actualRevenueForeign;
				}
				@Column(length=25)
				public String getRecRateCurrency() {
					return recRateCurrency;
				}

				public void setRecRateCurrency(String recRateCurrency) {
					this.recRateCurrency = recRateCurrency;
				}

				/**
				 * @return the ignoreForBilling
				 */
				@Column
				public boolean isIgnoreForBilling() {
					return ignoreForBilling;
				}

				/**
				 * @param ignoreForBilling the ignoreForBilling to set
				 */
				public void setIgnoreForBilling(boolean ignoreForBilling) {
					this.ignoreForBilling = ignoreForBilling;
				}
				@Column
				public boolean isAccrueRevenueManual() {
					return accrueRevenueManual;
				}

				public void setAccrueRevenueManual(boolean accrueRevenueManual) {
					this.accrueRevenueManual = accrueRevenueManual;
				}
				@Column
				public boolean isAccruePayableManual() {
					return accruePayableManual;
				}

				public void setAccruePayableManual(boolean accruePayableManual) {
					this.accruePayableManual = accruePayableManual;
				}
				@Column(length=25)
				public BigDecimal getReceivedAmount() {
					return receivedAmount;
				}

				public void setReceivedAmount(BigDecimal receivedAmount) {
					this.receivedAmount = receivedAmount;
				}
				@Column(length=25)
				public String getInvoiceCreatedBy() {
					return invoiceCreatedBy;
				}

				public void setInvoiceCreatedBy(String invoiceCreatedBy) {
					this.invoiceCreatedBy = invoiceCreatedBy;
				}
				@Transient
				public String getListEigthDescription() {
					// TODO Auto-generated method stub
					return null;
				}
				@Transient
				public String getListNinthDescription() {
					// TODO Auto-generated method stub
					return null;
				}
				@Transient
				public String getListTenthDescription() {
					// TODO Auto-generated method stub
					return null;
				}
				@Column(length=25)
				public BigDecimal getPayPayableAmount() {
					return payPayableAmount;
				}

				public void setPayPayableAmount(BigDecimal payPayableAmount) {
					this.payPayableAmount = payPayableAmount;
				}
				
				@Column(length=20)
				public String getCreditInvoiceNumber() {
					return creditInvoiceNumber;
				}

				public void setCreditInvoiceNumber(String creditInvoiceNumber) {
					this.creditInvoiceNumber = creditInvoiceNumber;
				}
				@Column(length=25)
				public String getTruckNumber() {
					return truckNumber;
				}

				public void setTruckNumber(String truckNumber) {
					this.truckNumber = truckNumber;
				}

				@Column(length=8)
				public String getOldBillToCode() {
					return oldBillToCode;
				}

				public void setOldBillToCode(String oldBillToCode) {
					this.oldBillToCode = oldBillToCode;
				}
				@Column
				public Boolean getIncludeLHF() {
					return includeLHF;
				}

				public void setIncludeLHF(Boolean includeLHF) {
					this.includeLHF = includeLHF;
				}
				@Column(length=10)
				public String getCompanyDivision() {
					return companyDivision;
				}

				public void setCompanyDivision(String companyDivision) {
					this.companyDivision = companyDivision;
				}
				
				@Column
				public boolean isDisplayOnQuote() {
					return displayOnQuote;
				}

				public void setDisplayOnQuote(boolean displayOnQuote) {
					this.displayOnQuote = displayOnQuote;
				}

				@Column(length=25)
				public BigDecimal getRecVatAmt() {
					return recVatAmt;
				}

				public void setRecVatAmt(BigDecimal recVatAmt) {
					this.recVatAmt = recVatAmt;
				}

				@Column(length=30)
				public String getRecVatDescr() {
					return recVatDescr;
				}

				public void setRecVatDescr(String recVatDescr) {
					this.recVatDescr = recVatDescr;
				}
				
				@Column(length=6)
				public String getRecVatPercent() {
					return recVatPercent;
				}

				public void setRecVatPercent(String recVatPercent) {
					this.recVatPercent = recVatPercent;
				}
				@Column(length=25)
				public BigDecimal getPayVatAmt() {
					return payVatAmt;
				}

				public void setPayVatAmt(BigDecimal payVatAmt) {
					this.payVatAmt = payVatAmt;
				}
				@Column(length=30)
				public String getPayVatDescr() {
					return payVatDescr;
				}

				public void setPayVatDescr(String payVatDescr) {
					this.payVatDescr = payVatDescr;
				}
				@Column(length=6)
				public String getPayVatPercent() {
					return payVatPercent;
				}

				public void setPayVatPercent(String payVatPercent) {
					this.payVatPercent = payVatPercent;
				}
				
				@Column(length=3)
				public String getEstCurrency() {
					return estCurrency;
				}

				public void setEstCurrency(String estCurrency) {
					this.estCurrency = estCurrency;
				}
				
				@Column(length=25)
				public BigDecimal getEstExchangeRate() {
					return estExchangeRate;
				}

				public void setEstExchangeRate(BigDecimal estExchangeRate) {
					this.estExchangeRate = estExchangeRate;
				}
				
				@Column(length=25)
				public BigDecimal getEstLocalAmount() {
					return estLocalAmount;
				}
				
				@Column(length=25)
				public void setEstLocalAmount(BigDecimal estLocalAmount) {
					this.estLocalAmount = estLocalAmount;
				}
				
				@Column
				public Date getEstValueDate() {
					return estValueDate;
				}

				public void setEstValueDate(Date estValueDate) {
					this.estValueDate = estValueDate;
				}
				
				@Column(length=3)
				public String getRevisionCurrency() {
					return revisionCurrency;
				}

				public void setRevisionCurrency(String revisionCurrency) {
					this.revisionCurrency = revisionCurrency;
				}
				
				@Column(length=25)
				public BigDecimal getRevisionExchangeRate() {
					return revisionExchangeRate;
				}

				public void setRevisionExchangeRate(BigDecimal revisionExchangeRate) {
					this.revisionExchangeRate = revisionExchangeRate;
				}

				@Column(length=25)
				public BigDecimal getRevisionLocalAmount() {
					return revisionLocalAmount;
				}

				public void setRevisionLocalAmount(BigDecimal revisionLocalAmount) {
					this.revisionLocalAmount = revisionLocalAmount;
				}

				@Column
				public Date getRevisionValueDate() {
					return revisionValueDate;
				}

				public void setRevisionValueDate(Date revisionValueDate) {
					this.revisionValueDate = revisionValueDate;
				}
                @Column
				public String getUvlControlNumber() {
					return uvlControlNumber;
				}

				public void setUvlControlNumber(String uvlControlNumber) {
					this.uvlControlNumber = uvlControlNumber;
				}

				public String getDdrControlNumber() {
					return ddrControlNumber;
				}
				@Column
				public void setDdrControlNumber(String ddrControlNumber) {
					this.ddrControlNumber = ddrControlNumber;
				}
				@Column(length=25)
				public BigDecimal getEstLocalRate() {
					return estLocalRate;
				}

				public void setEstLocalRate(BigDecimal estLocalRate) {
					this.estLocalRate = estLocalRate;
				}
				@Column(length=25)
				public BigDecimal getRevisionLocalRate() {
					return revisionLocalRate;
				}

				public void setRevisionLocalRate(BigDecimal revisionLocalRate) {
					this.revisionLocalRate = revisionLocalRate;
				}

				public Long getGroupAccount() {
					return groupAccount;
				}

				public void setGroupAccount(Long groupAccount) {
					this.groupAccount = groupAccount;
				}
			
				@Column
				 public BigDecimal getEstimateSellDeviation() {
						return estimateSellDeviation;
					}

					public void setEstimateSellDeviation(BigDecimal estimateSellDeviation) {
						this.estimateSellDeviation = estimateSellDeviation;
					}

					@Column
					public BigDecimal getEstimateDeviation() {
						return estimateDeviation;
					}

					public void setEstimateDeviation(BigDecimal estimateDeviation) {
						this.estimateDeviation = estimateDeviation;
					}

					@Column
					public BigDecimal getRevisionSellDeviation() {
						return revisionSellDeviation;
					}

					public void setRevisionSellDeviation(BigDecimal revisionSellDeviation) {
						this.revisionSellDeviation = revisionSellDeviation;
					}

					@Column
					public BigDecimal getRevisionDeviation() {
						return revisionDeviation;
					}

					public void setRevisionDeviation(BigDecimal revisionDeviation) {
						this.revisionDeviation = revisionDeviation;
					}
					@Column(length=1)
					public String getBuyDependSell() {
						return buyDependSell;
					}

					public void setBuyDependSell(String buyDependSell) {
						this.buyDependSell = buyDependSell;
					}
					@Column
					public Boolean getVATExclude() {
						return VATExclude;
					}

					public void setVATExclude(Boolean exclude) {
						VATExclude = exclude;
					} 
					@Column(length=7)
					public String getEstVatPercent() {
						return estVatPercent;
					}

					public void setEstVatPercent(String estVatPercent) {
						this.estVatPercent = estVatPercent;
					}
					@Column(length=30)
					public String getEstVatDescr() {
						return estVatDescr;
					}

					public void setEstVatDescr(String estVatDescr) {
						this.estVatDescr = estVatDescr;
					}
					@Column(length=25)
					public BigDecimal getEstVatAmt() {
						return estVatAmt;
					}

					public void setEstVatAmt(BigDecimal estVatAmt) {
						this.estVatAmt = estVatAmt;
					}
					@Column(length=4)
					public String getDeviation() {
						return deviation;
					}

					public void setDeviation(String deviation) {
						this.deviation = deviation;
					}
					@Column
					public Boolean getAdditionalService() {
						return additionalService;
					}

					public void setAdditionalService(Boolean additionalService) {
						this.additionalService = additionalService;
					}
                    
					@Column
					public BigDecimal getReceivableSellDeviation() {
						return receivableSellDeviation;
					}

					public void setReceivableSellDeviation(BigDecimal receivableSellDeviation) {
						this.receivableSellDeviation = receivableSellDeviation;
					}

                    @Column
					public Date getSettledDate() {
						return settledDate;
					}

					public void setSettledDate(Date settledDate) {
						this.settledDate = settledDate;
					}
					@Column(length=25)
					public BigDecimal getRevisionVatAmt() {
						return revisionVatAmt;
					}

					public void setRevisionVatAmt(BigDecimal revisionVatAmt) {
						this.revisionVatAmt = revisionVatAmt;
					}

					@Column(length=30)
					public String getRevisionVatDescr() {
						return revisionVatDescr;
					}

					public void setRevisionVatDescr(String revisionVatDescr) {
						this.revisionVatDescr = revisionVatDescr;
					}
					
					@Column(length=7)
					public String getRevisionVatPercent() {
						return revisionVatPercent;
					}

					public void setRevisionVatPercent(String revisionVatPercent) {
						this.revisionVatPercent = revisionVatPercent;
					}
					@Column
					public BigDecimal getPayDeviation() {
						return payDeviation;
					}

					public void setPayDeviation(BigDecimal payDeviation) {
						this.payDeviation = payDeviation;
					}

					@Column(length=25)
					public String getContractCurrency() {
						return contractCurrency;
					}

					public void setContractCurrency(String contractCurrency) {
						this.contractCurrency = contractCurrency;
					}

					@Column(length=25)
					public BigDecimal getContractExchangeRate() {
						return contractExchangeRate;
					}

					public void setContractExchangeRate(BigDecimal contractExchangeRate) {
						this.contractExchangeRate = contractExchangeRate;
					}

					@Column(length=25)
					public BigDecimal getContractRate() {
						return contractRate;
					}

					public void setContractRate(BigDecimal contractRate) {
						this.contractRate = contractRate;
					}
					@Column(length=25)
					public BigDecimal getContractRateAmmount() {
						return contractRateAmmount;
					}

					public void setContractRateAmmount(BigDecimal contractRateAmmount) {
						this.contractRateAmmount = contractRateAmmount;
					}
					@Column
					public Date getContractValueDate() {
						return contractValueDate;
					}

					public void setContractValueDate(Date contractValueDate) {
						this.contractValueDate = contractValueDate;
					}
					@Column
					public Long getNetworkSynchedId() {
						return networkSynchedId;
					}

					public void setNetworkSynchedId(Long networkSynchedId) {
						this.networkSynchedId = networkSynchedId;
					}
					
					@Column(length=20)
					public String getAgentFeesNo() {
						return agentFeesNo;
					}

					public void setAgentFeesNo(String agentFeesNo) {
						this.agentFeesNo = agentFeesNo;
					}

					@Column(length=25)
					public String getPayableContractCurrency() {
						return payableContractCurrency;
					}

					public void setPayableContractCurrency(String payableContractCurrency) {
						this.payableContractCurrency = payableContractCurrency;
					}
					@Column
					public BigDecimal getEstimateDiscount() {
						return estimateDiscount;
					}

					public void setEstimateDiscount(BigDecimal estimateDiscount) {
						this.estimateDiscount = estimateDiscount;
					}
					@Column
					public BigDecimal getRevisionDiscount() {
						return revisionDiscount;
					}

					public void setRevisionDiscount(BigDecimal revisionDiscount) {
						this.revisionDiscount = revisionDiscount;
					}
					@Column
					public BigDecimal getActualDiscount() {
						return actualDiscount;
					}

					public void setActualDiscount(BigDecimal actualDiscount) {
						this.actualDiscount = actualDiscount;
					}					 

					@Column(length=25)
					public BigDecimal getPayableContractExchangeRate() {
						return payableContractExchangeRate;
					}

					public void setPayableContractExchangeRate(
							BigDecimal payableContractExchangeRate) {
						this.payableContractExchangeRate = payableContractExchangeRate;
					}

					@Column(length=25)
					public BigDecimal getPayableContractRateAmmount() {
						return payableContractRateAmmount;
					}

					public void setPayableContractRateAmmount(BigDecimal payableContractRateAmmount) {
						this.payableContractRateAmmount = payableContractRateAmmount;
					}
					@Column
					public Date getPayableContractValueDate() {
						return payableContractValueDate;
					}

					public void setPayableContractValueDate(Date payableContractValueDate) {
						this.payableContractValueDate = payableContractValueDate;
					}
					
					@Column(length=25)
					public String getEstSellCurrency() {
						return estSellCurrency;
					}

					public void setEstSellCurrency(String estSellCurrency) {
						this.estSellCurrency = estSellCurrency;
					}
					
					@Column(length=25)
					public BigDecimal getEstSellLocalAmount() {
						return estSellLocalAmount;
					}

					public void setEstSellLocalAmount(BigDecimal estSellLocalAmount) {
						this.estSellLocalAmount = estSellLocalAmount;
					}
					
					@Column(length=25)
					public BigDecimal getEstSellExchangeRate() {
						return estSellExchangeRate;
					}

					public void setEstSellExchangeRate(BigDecimal estSellExchangeRate) {
						this.estSellExchangeRate = estSellExchangeRate;
					}

					@Column
					public Date getEstSellValueDate() {
						return estSellValueDate;
					}

					public void setEstSellValueDate(Date estSellValueDate) {
						this.estSellValueDate = estSellValueDate;
					}

					@Column(length=25)
					public BigDecimal getEstSellLocalRate() {
						return estSellLocalRate;
					}

					public void setEstSellLocalRate(BigDecimal estSellLocalRate) {
						this.estSellLocalRate = estSellLocalRate;
					}
                    
					@Column(length=25)
					public String getRevisionSellCurrency() {
						return revisionSellCurrency;
					} 
					
					public void setRevisionSellCurrency(String revisionSellCurrency) {
						this.revisionSellCurrency = revisionSellCurrency;
					}

					@Column(length=25)
					public BigDecimal getRevisionSellLocalAmount() {
						return revisionSellLocalAmount;
					} 
					
					public void setRevisionSellLocalAmount(BigDecimal revisionSellLocalAmount) {
						this.revisionSellLocalAmount = revisionSellLocalAmount;
					}

					
					@Column(length=25)
					public BigDecimal getRevisionSellExchangeRate() {
						return revisionSellExchangeRate;
					}

					public void setRevisionSellExchangeRate(BigDecimal revisionSellExchangeRate) {
						this.revisionSellExchangeRate = revisionSellExchangeRate;
					}

					
					@Column
					public Date getRevisionSellValueDate() {
						return revisionSellValueDate;
					}

					public void setRevisionSellValueDate(Date revisionSellValueDate) {
						this.revisionSellValueDate = revisionSellValueDate;
					}

					@Column(length=25)
					public BigDecimal getRevisionSellLocalRate() {
						return revisionSellLocalRate;
					}

					public void setRevisionSellLocalRate(BigDecimal revisionSellLocalRate) {
						this.revisionSellLocalRate = revisionSellLocalRate;
					}

					
					@Column(length=25)
					public String getEstimateContractCurrency() {
						return estimateContractCurrency;
					}

					public void setEstimateContractCurrency(String estimateContractCurrency) {
						this.estimateContractCurrency = estimateContractCurrency;
					} 
					
					@Column(length=25)
					public BigDecimal getEstimateContractRate() {
						return estimateContractRate;
					}

					public void setEstimateContractRate(BigDecimal estimateContractRate) {
						this.estimateContractRate = estimateContractRate;
					}

					@Column(length=25)
					public BigDecimal getEstimateContractExchangeRate() {
						return estimateContractExchangeRate;
					}

					public void setEstimateContractExchangeRate(
							BigDecimal estimateContractExchangeRate) {
						this.estimateContractExchangeRate = estimateContractExchangeRate;
					}

					@Column(length=25)
					public BigDecimal getEstimateContractRateAmmount() {
						return estimateContractRateAmmount;
					}

					public void setEstimateContractRateAmmount(
							BigDecimal estimateContractRateAmmount) {
						this.estimateContractRateAmmount = estimateContractRateAmmount;
					}

					@Column
					public Date getEstimateContractValueDate() {
						return estimateContractValueDate;
					}

					public void setEstimateContractValueDate(Date estimateContractValueDate) {
						this.estimateContractValueDate = estimateContractValueDate;
					}

					@Column(length=25)
					public String getEstimatePayableContractCurrency() {
						return estimatePayableContractCurrency;
					}

					public void setEstimatePayableContractCurrency(
							String estimatePayableContractCurrency) {
						this.estimatePayableContractCurrency = estimatePayableContractCurrency;
					}

					@Column(length=25)
					public BigDecimal getEstimatePayableContractRate() {
						return estimatePayableContractRate;
					}

					public void setEstimatePayableContractRate(
							BigDecimal estimatePayableContractRate) {
						this.estimatePayableContractRate = estimatePayableContractRate;
					}

					@Column(length=25)
					public BigDecimal getEstimatePayableContractExchangeRate() {
						return estimatePayableContractExchangeRate;
					}

					public void setEstimatePayableContractExchangeRate(
							BigDecimal estimatePayableContractExchangeRate) {
						this.estimatePayableContractExchangeRate = estimatePayableContractExchangeRate;
					}

					@Column(length=25)
					public BigDecimal getEstimatePayableContractRateAmmount() {
						return estimatePayableContractRateAmmount;
					}

					public void setEstimatePayableContractRateAmmount(
							BigDecimal estimatePayableContractRateAmmount) {
						this.estimatePayableContractRateAmmount = estimatePayableContractRateAmmount;
					}

					@Column
					public Date getEstimatePayableContractValueDate() {
						return estimatePayableContractValueDate;
					}

					public void setEstimatePayableContractValueDate(
							Date estimatePayableContractValueDate) {
						this.estimatePayableContractValueDate = estimatePayableContractValueDate;
					}

					@Column(length=25)
					public String getRevisionContractCurrency() {
						return revisionContractCurrency;
					}

					public void setRevisionContractCurrency(String revisionContractCurrency) {
						this.revisionContractCurrency = revisionContractCurrency;
					}

					@Column(length=25)
					public BigDecimal getRevisionContractRate() {
						return revisionContractRate;
					}

					public void setRevisionContractRate(BigDecimal revisionContractRate) {
						this.revisionContractRate = revisionContractRate;
					}

					@Column(length=25)
					public BigDecimal getRevisionContractExchangeRate() {
						return revisionContractExchangeRate;
					}

					public void setRevisionContractExchangeRate(
							BigDecimal revisionContractExchangeRate) {
						this.revisionContractExchangeRate = revisionContractExchangeRate;
					}

					@Column(length=25)
					public BigDecimal getRevisionContractRateAmmount() {
						return revisionContractRateAmmount;
					}

					public void setRevisionContractRateAmmount(
							BigDecimal revisionContractRateAmmount) {
						this.revisionContractRateAmmount = revisionContractRateAmmount;
					}

					@Column
					public Date getRevisionContractValueDate() {
						return revisionContractValueDate;
					}

					public void setRevisionContractValueDate(Date revisionContractValueDate) {
						this.revisionContractValueDate = revisionContractValueDate;
					}

					@Column(length=25)
					public String getRevisionPayableContractCurrency() {
						return revisionPayableContractCurrency;
					}

					public void setRevisionPayableContractCurrency(
							String revisionPayableContractCurrency) {
						this.revisionPayableContractCurrency = revisionPayableContractCurrency;
					}

					@Column(length=25)
					public BigDecimal getRevisionPayableContractRate() {
						return revisionPayableContractRate;
					}

					public void setRevisionPayableContractRate(
							BigDecimal revisionPayableContractRate) {
						this.revisionPayableContractRate = revisionPayableContractRate;
					}

					@Column(length=25)
					public BigDecimal getRevisionPayableContractExchangeRate() {
						return revisionPayableContractExchangeRate;
					}

					public void setRevisionPayableContractExchangeRate(
							BigDecimal revisionPayableContractExchangeRate) {
						this.revisionPayableContractExchangeRate = revisionPayableContractExchangeRate;
					}

					@Column(length=25)
					public BigDecimal getRevisionPayableContractRateAmmount() {
						return revisionPayableContractRateAmmount;
					}

					public void setRevisionPayableContractRateAmmount(
							BigDecimal revisionPayableContractRateAmmount) {
						this.revisionPayableContractRateAmmount = revisionPayableContractRateAmmount;
					}

					@Column
					public Date getRevisionPayableContractValueDate() {
						return revisionPayableContractValueDate;
					}

					public void setRevisionPayableContractValueDate(
							Date revisionPayableContractValueDate) {
						this.revisionPayableContractValueDate = revisionPayableContractValueDate;
					}

					@Column
					public Boolean getCostTransferred() {
						return costTransferred;
					}

					public void setCostTransferred(Boolean costTransferred) {
						this.costTransferred = costTransferred;
					}

					@Column
					public String getCategoryResource() {
						return categoryResource;
					}

					public void setCategoryResource(String categoryResource) {
						this.categoryResource = categoryResource;
					}

					@Column
					public String getCategoryResourceName() {
						return categoryResourceName;
					}

					public void setCategoryResourceName(String categoryResourceName) {
						this.categoryResourceName = categoryResourceName;
					}

					public Boolean getT20Process() {
						return t20Process;
					}

					public void setT20Process(Boolean t20Process) {
						this.t20Process = t20Process;
					}

					public Boolean getActivateAccPortal() {
						return activateAccPortal;
					}

					public void setActivateAccPortal(Boolean activateAccPortal) {
						this.activateAccPortal = activateAccPortal;
					}
					@Column
					public Date getVanlineSettleDate() {
						return vanlineSettleDate;
					}

					public void setVanlineSettleDate(Date vanlineSettleDate) {
						this.vanlineSettleDate = vanlineSettleDate;
					}

					@Column(length=15)
					public BigDecimal getOnHand() {
						return onHand;
					}

					@Column
					public Boolean getDownload() {
						return download;
					}
				
					public void setDownload(Boolean download) {
						this.download = download;
					}
					public void setOnHand(BigDecimal onHand) {
						this.onHand = onHand;
					}

					public String getEstimateStatus() {
						return estimateStatus;
					}

					public void setEstimateStatus(String estimateStatus) {
						this.estimateStatus = estimateStatus;
					}
					@Column(length=10)
					public String getDivision() {
						return division;
					}

					public void setDivision(String division) {
						this.division = division;
					}

					@Column
					public Date getPaymentSent() {
						return paymentSent;
					}

					public void setPaymentSent(Date paymentSent) {
						this.paymentSent = paymentSent;
					}
					@Column
					public Boolean getInvoiceAutoUpload() {
						return invoiceAutoUpload;
					}

					public void setInvoiceAutoUpload(Boolean invoiceAutoUpload) {
						this.invoiceAutoUpload = invoiceAutoUpload;
					}
					
					@Column(length=25)
					public String getInvoiceType() {
						return invoiceType;
					}

					public void setInvoiceType(String invoiceType) {
						this.invoiceType = invoiceType;
					}
					@Column
					public Boolean getDriverCompanyDivision() {
						return driverCompanyDivision;
					}

					public void setDriverCompanyDivision(Boolean driverCompanyDivision) {
						this.driverCompanyDivision = driverCompanyDivision;
					}
					@Column
					public String getRecVatGl() {
						return recVatGl;
					}

					public void setRecVatGl(String recVatGl) {
						this.recVatGl = recVatGl;
					}
					@Column
					public String getPayVatGl() {
						return payVatGl;
					}

					public void setPayVatGl(String payVatGl) {
						this.payVatGl = payVatGl;
					}
					@Column
					public Boolean getSelectiveInvoice() {
						return selectiveInvoice;
					}

					public void setSelectiveInvoice(Boolean selectiveInvoice) {
						this.selectiveInvoice = selectiveInvoice;
					}
					@Column
					public Boolean getRollUpInInvoice() {
						return rollUpInInvoice;
					}

					public void setRollUpInInvoice(Boolean rollUpInInvoice) {
						this.rollUpInInvoice = rollUpInInvoice;
					}

					@Column
					public Boolean getServiceTaxInput() {
						return serviceTaxInput;
					}

					public void setServiceTaxInput(Boolean serviceTaxInput) {
						this.serviceTaxInput = serviceTaxInput;
					}
					@Column
					public String getMyFileFileName() {
						return myFileFileName;
					}

					public void setMyFileFileName(String myFileFileName) {
						this.myFileFileName = myFileFileName;
					}
					@Column
					public String getExternalIntegrationReference() {
						return externalIntegrationReference;
					}

					public void setExternalIntegrationReference(String externalIntegrationReference) {
						this.externalIntegrationReference = externalIntegrationReference;
					}
					@Column
					public BigDecimal getEstimateSellQuantity() {
						return estimateSellQuantity;
					}

					public void setEstimateSellQuantity(BigDecimal estimateSellQuantity) {
						this.estimateSellQuantity = estimateSellQuantity;
					}
					@Column
					public BigDecimal getRevisionSellQuantity() {
						return revisionSellQuantity;
					}

					public void setRevisionSellQuantity(BigDecimal revisionSellQuantity) {
						this.revisionSellQuantity = revisionSellQuantity;
					}
					@Column
					public String getAccountLineCostElement() {
						return accountLineCostElement;
					}
					public void setAccountLineCostElement(String accountLineCostElement) {
						this.accountLineCostElement = accountLineCostElement;
					}
					@Column
					public String getAccountLineScostElementDescription() {
						return accountLineScostElementDescription;
					}
					public void setAccountLineScostElementDescription(
							String accountLineScostElementDescription) {
						this.accountLineScostElementDescription = accountLineScostElementDescription;
					}
					@Column
					public Boolean getDynamicNavisionflag() {
						return dynamicNavisionflag;
					}

					public void setDynamicNavisionflag(Boolean dynamicNavisionflag) {
						this.dynamicNavisionflag = dynamicNavisionflag;
					}
					@Column
					public Boolean getDeleteDynamicNavisionflag() {
						return deleteDynamicNavisionflag;
					}

					public void setDeleteDynamicNavisionflag(Boolean deleteDynamicNavisionflag) {
						this.deleteDynamicNavisionflag = deleteDynamicNavisionflag;
					}
					@Column
					public String getOldCompanyDivision() {
						return oldCompanyDivision;
					}

					public void setOldCompanyDivision(String oldCompanyDivision) {
						this.oldCompanyDivision = oldCompanyDivision;
					}
					@Column
					public String getDynamicNavXfer() {
						return dynamicNavXfer;
					}

					public void setDynamicNavXfer(String dynamicNavXfer) {
						this.dynamicNavXfer = dynamicNavXfer;
					}
					@Column
					public Date getSendDynamicNavisionDate() {
						return sendDynamicNavisionDate;
					}

					public void setSendDynamicNavisionDate(Date sendDynamicNavisionDate) {
						this.sendDynamicNavisionDate = sendDynamicNavisionDate;
					}
					@Column(length=25)
					public BigDecimal getEstExpVatAmt() {
						return estExpVatAmt;
					}

					public void setEstExpVatAmt(BigDecimal estExpVatAmt) {
						this.estExpVatAmt = estExpVatAmt;
					}
					@Column(length=25)
					public BigDecimal getRevisionExpVatAmt() {
						return revisionExpVatAmt;
					}

					public void setRevisionExpVatAmt(BigDecimal revisionExpVatAmt) {
						this.revisionExpVatAmt = revisionExpVatAmt;
					}
					@Column(length=25)
					public BigDecimal getQstRecVatAmt() {
						return qstRecVatAmt;
					}

					public void setQstRecVatAmt(BigDecimal qstRecVatAmt) {
						this.qstRecVatAmt = qstRecVatAmt;
					}
					@Column(length=25)
					public BigDecimal getQstPayVatAmt() {
						return qstPayVatAmt;
					}

					public void setQstPayVatAmt(BigDecimal qstPayVatAmt) {
						this.qstPayVatAmt = qstPayVatAmt;
					}
					@Column(length=20)
					public String getQstRecVatGl() {
						return qstRecVatGl;
					}

					public void setQstRecVatGl(String qstRecVatGl) {
						this.qstRecVatGl = qstRecVatGl;
					}
					@Column(length=20)
					public String getQstPayVatGl() {
						return qstPayVatGl;
					}

					public void setQstPayVatGl(String qstPayVatGl) {
						this.qstPayVatGl = qstPayVatGl;
					}
					@Column
					public Date getAuditCompleteDate() {
						return auditCompleteDate;
					}

					public void setAuditCompleteDate(Date auditCompleteDate) {
						this.auditCompleteDate = auditCompleteDate;
					}

					@Column(length=8)
					public String getNetworkBillToCode() {
						return networkBillToCode;
					}

					public void setNetworkBillToCode(String networkBillToCode) {
						this.networkBillToCode = networkBillToCode;
					}

					@Column(length=225)
					public String getNetworkBillToName() {
						return networkBillToName;
					}

					public void setNetworkBillToName(String networkBillToName) {
						this.networkBillToName = networkBillToName;
					}
					@Column
					public BigDecimal getVarianceRevenueAmount() {
						return varianceRevenueAmount;
					}

					public void setVarianceRevenueAmount(BigDecimal varianceRevenueAmount) {
						this.varianceRevenueAmount = varianceRevenueAmount;
					}
					@Column
					public BigDecimal getVarianceExpenseAmount() {
						return varianceExpenseAmount;
					}

					public void setVarianceExpenseAmount(BigDecimal varianceExpenseAmount) {
						this.varianceExpenseAmount = varianceExpenseAmount;
					}
					@Column
					public Date getAccrueRevenueVariance() {
						return accrueRevenueVariance;
					}

					public void setAccrueRevenueVariance(Date accrueRevenueVariance) {
						this.accrueRevenueVariance = accrueRevenueVariance;
					}

					public Date getAccrueExpenseVariance() {
						return accrueExpenseVariance;
					}

					public void setAccrueExpenseVariance(Date accrueExpenseVariance) {
						this.accrueExpenseVariance = accrueExpenseVariance;
					}

					public String getRevisionDescription() {
						return revisionDescription;
					}

					public void setRevisionDescription(String revisionDescription) {
						this.revisionDescription = revisionDescription;
					}

					@Column
					public String getWriteOffReason() {
						return writeOffReason;
					}

					public void setWriteOffReason(String writeOffReason) {
						this.writeOffReason = writeOffReason;
					}
					@Transient
					public boolean isUtsiRecAccDateFlag() {
						return utsiRecAccDateFlag;
					}

					public void setUtsiRecAccDateFlag(boolean utsiRecAccDateFlag) {
						this.utsiRecAccDateFlag = utsiRecAccDateFlag;
					}
					@Transient
					public boolean isUtsiPayAccDateFlag() {
						return utsiPayAccDateFlag;
					}

					public void setUtsiPayAccDateFlag(boolean utsiPayAccDateFlag) {
						this.utsiPayAccDateFlag = utsiPayAccDateFlag;
					}

					public Date getManagerApprovalDate() {
						return managerApprovalDate;
					}

					public void setManagerApprovalDate(Date managerApprovalDate) {
						this.managerApprovalDate = managerApprovalDate;
					}

					public String getManagerApprovalName() {
						return managerApprovalName;
					}

					public void setManagerApprovalName(String managerApprovalName) {
						this.managerApprovalName = managerApprovalName;
					}

					public Boolean getAuthorizedCreditNote() {
						return authorizedCreditNote;
					}

					public void setAuthorizedCreditNote(Boolean authorizedCreditNote) {
						this.authorizedCreditNote = authorizedCreditNote;
					}

					public String getInvoicereportparameter() {
						return invoicereportparameter;
					}

					public void setInvoicereportparameter(String invoicereportparameter) {
						this.invoicereportparameter = invoicereportparameter;
					}

					@Column
					public BigDecimal getPayQuantity() {
						return payQuantity;
					}

					public void setPayQuantity(BigDecimal payQuantity) {
						this.payQuantity = payQuantity;
					}

					@Column
					public BigDecimal getPayRate() {
						return payRate;
					}

					public void setPayRate(BigDecimal payRate) {
						this.payRate = payRate;
					}

}
