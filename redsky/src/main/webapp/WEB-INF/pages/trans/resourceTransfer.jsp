<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Resource Transfer Details</title>   
    <meta name="heading" content="ResourceTransfer"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
    <script type="text/javascript">
   
    function closeWIN(){
    	window.close();
    }
    </script>

</head>

<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}

#loader {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>

<c:set var="buttons"> 
    <input type="button" id="btnTransfer" name="btnTransfer" class="cssbutton1" onclick="getSelectedTicket();" value="Transfer" style="width:60px; height:25px" tabindex="83"/> 
	<input type="button" class="cssbutton1" onclick="closeWIN();" value="Cancel" style="width:60px; height:25px" tabindex="83"/> 
</c:set>

<s:form name="resourceTransferForm" id="resourceTransferForm" action="" method="post" validate="true">
<input type="hidden" name="btnType" value="<%=request.getParameter("btnType") %>"/>
<div id="layer1" style="width:700px; margin-top: 10px;">
<div id="newmnav">
			<ul>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Resource Transfer<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div>
      
<div id="Layer5" style="width:800px" onkeydown="">
<table class="" cellspacing="0" cellpadding="0" border="0" width="700px" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0"  style="margin-left:30px">
<tbody>
<tr>
<td>
<div id="para1" >
<s:set name="resourceTransfer" value="resourceTransfer" scope="request"/>
<display:table name="resourceTransfer" class="table" requestURI="" id="resourceTransfer" defaultsort="6"  style="width:700px;">
    <display:column property="ticket"  title="Work Ticket" maxLength="35"/>
    <display:column property="service"  title="Service" />   
    <display:column property="warehouse"  title="Warehouse" maxLength="25" />
    <display:column property="date1"  title="Begin Date" maxLength="25" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="date2"  title="End Date" maxLength="25" format="{0,date,dd-MMM-yyyy}" />
    <display:column property="day"  title="Day" maxLength="25" />
    <display:column title="Select"  style="width:40px">
    	<c:choose>
			<c:when test="${resourceTransfer.day == 'No Match' || resourceTransfer.ticketTransferStatus == 'true'}">
      			<input type="checkbox"  name="transStatus${resourceTransfer.id}" id="transStatus${resourceTransfer.id}" disabled="true" value="${resourceTransfer.id}-${resourceTransfer.ticket}" onchange=""/>
      		</c:when>
      		<c:otherwise>
				<input type="checkbox"  name="transStatus${resourceTransfer.id}" id="transStatus${resourceTransfer.id}" onchange="checkTargetActual('${resourceTransfer.ticket}','transStatus${resourceTransfer.id}','${resourceTransfer.targetActual}');" value="${resourceTransfer.id}-${resourceTransfer.ticket}-${resourceTransfer.day}"/>
			</c:otherwise> 
		</c:choose>
    </display:column>
</display:table>

</div>
</td>
</tr>

<tr>
     <td align="center" class="listwhitetext" colspan="5" valign="bottom" style="">						
		<c:out value="${buttons}" escapeXml="false" />					
	</td>
</tr>
	
</tbody>
</table>
</div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div>
</div>
</div> 
</div>
</td>
</tr>
</tbody>
</table> 

</div> 
 
</div>

<div id="loader" style="text-align:center; display:none">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tr>
			<td align="center">
				<table cellspacing="0" cellpadding="3" align="center">
					<tr><td height="200px"></td></tr>
					<tr>
				       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
				           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Work In Progress...Please Wait</font>
				       </td>
				    </tr>
				    <tr>
				      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
				           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
				       </td>
				    </tr>
		       </table>
		     </td>
	  	</tr>
	</table>
</div>

</s:form> 

<script type="text/javascript">  
try{
	 
}catch(e){
}
</script>

<script type="text/javascript">
var shipNumber ='';
var workTickets='';

function getSelectedTicket(){
	shipNumber = "<%=request.getParameter("shipNumber") %>";
	workTickets='';
	var workTicket='';
	var ticketID='';
	var ticket='';
	var day='';
	<c:forEach var="resourceTransferMapDTO" items="${resourceTransferMapDTO}" >
		if(document.getElementById('transStatus${resourceTransferMapDTO.key}').checked){
			workTicket = document.getElementById('transStatus${resourceTransferMapDTO.key}').value;
			
			if(workTicket.length == 0){
				workTickets = workTicket;
			}else {
				workTickets = workTicket+','+workTickets;
			}
<%--		
			workTicket = workTicket.split('-');
			if(workTicket.length == 3){
				 if(ticket.length ==0){
					 ticketID=workTicket[0];
					 ticket = workTicket[1];
					 day = workTicket[2];
				 }else{
					 ticketID=workTicket[0]+','+ticketID;
					 ticket = workTicket[1]+','+ticket;
				 }
			 }	--%>
		}
		</c:forEach>
	if(workTickets.length > 0){
		document.getElementById('btnTransfer').disabled = true; 
		
		var url="checkHubLimitForResource.html?ajax=1&decorator=simple&popup=true&shipNumber="+shipNumber+"&tickets="+workTickets; 
		httpHub.open("GET", url, true);
		httpHub.onreadystatechange = handleHttpHubResponse;
		httpHub.send(null);	

	}else{
		alert('Please Select any Ticket');
		return false;
	}
}

function handleHttpHubResponse(){
    if(httpHub.readyState == 4){ 
      var results = httpHub.responseText;
      results = results.trim();
      var status = results.charAt(0);
      results=results.replace("Y","");
     	if(status == 'Y'){
     		document.getElementById('btnTransfer').disabled = false; 
          	var agree = confirm(results+" ticket(s) are exceeding the limit & will be sent in Dispatch Queue for Approval");
          	if(agree){
          		document.getElementById('btnTransfer').disabled = true; 
          		var url ="saveTransferResourceToWorkTicket.html?ajax=1&decorator=simple&popup=true&shipNumber="+shipNumber+"&tickets="+workTickets+"&ticketStatus=P"+results;
          		document.forms['resourceTransferForm'].action =url;
          		document.forms['resourceTransferForm'].submit();
          		return true;
          	}else{
          		return false;	
          	}
     	 }else{
     		var url ="saveTransferResourceToWorkTicket.html?ajax=1&decorator=simple&popup=true&shipNumber="+shipNumber+"&tickets="+workTickets;
     		 document.forms['resourceTransferForm'].action =url;
     		 document.forms['resourceTransferForm'].submit();
       		return false; 
     	 }
      }
}
var httpHub = getHTTPObjectHub();
function getHTTPObjectHub(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}	
</script>

<script type="text/javascript">
window.onload=function(){
	var btnType = "<%=request.getParameter("btnType") %>";
	if(btnType == 'YES'){
		window.opener.showSuccessMsg(); 
		closeWIN();
	}
	try{
	}catch(e){}
}
</script>
<script type="text/javascript">
var ticketChkId;
function checkTargetActual2(ticket,id){
	if(document.getElementById(id).checked){
		showHide("block");
		ticketChkId=id;
		document.getElementById('btnTransfer').disabled = true;
		var url="checkWorkTicketTargetActualAjax.html?ajax=1&decorator=simple&popup=true&ticket="+ticket; 
		httpHub.open("GET", url, true);
		httpHub.onreadystatechange = handleHttpHubResponseForWorkTicket;
		httpHub.send(null);	
	}
}

function handleHttpHubResponseForWorkTicket(){
    if(httpHub.readyState == 4){
    	showHide("none");
      var results = httpHub.responseText;
      results = results.trim();
     	if(results != 'OK'){
     		alert('The work ticket is Cancelled or Actualized. You cannot transfer the resource to this ticket.');
     		document.getElementById(ticketChkId).checked = false; 
     	 }
     	ticketChkId='';
     	document.getElementById('btnTransfer').disabled = false;
      }
}

function showHide(action){
	document.getElementById("loader").style.display = action;
}

function checkTargetActual(ticket,id,targetActual){
	if(targetActual=='A' || targetActual == 'C'){
		alert('The work ticket is Cancelled or Actualized. You cannot transfer the resource to this ticket.');
		document.getElementById(id).checked = false; 
	}
}
</script>	

<%-- 
<script type="text/javascript">
function checkTargetActual1(ticket,id,targetActual){
	if(targetActual=='A' || targetActual == 'C'){
		alert('The work ticket is Cancelled or Actualized. You cannot transfer the resource to this ticket.');
		document.getElementById(id).checked = false; 
	}else if(document.getElementById(id).checked){
		checkHubLimit(id);
	}else if(pendingTicket.length > 0){
		pendingTicket = pendingTicket.replace(','+ticket,'');
		pendingTicket = pendingTicket.replace(ticket+',','');
		pendingTicket = pendingTicket.replace(ticket,'');
	}
}
function checkHubLimit(id){
	shipNumber = "<%=request.getParameter("shipNumber") %>";
	var workTickets = document.getElementById(id).value;
	ticketChkId=id;
	showHide("block");
	var url="checkHubLimitForResource.html?ajax=1&decorator=simple&popup=true&shipNumber="+shipNumber+"&tickets="+workTickets; 
	httpHub.open("GET", url, true);
	httpHub.onreadystatechange = handleHttpHubResponse1;
	httpHub.send(null);
}
var pendingTicket;
var ticketChkId;
function handleHttpHubResponse1(){
    if(httpHub.readyState == 4){
      showHide("none");
      var results = httpHub.responseText;
      results = results.trim();
      var status = results.charAt(0);
      results=results.replace("Y","");
     	if(status == 'Y'){
          	var agree = confirm(results+" ticket is exceeding the limit & will be sent in Dispatch Queue for Approval");
          	if(agree){
          		if(pendingTicket!='' && pendingTicket != null){
          			pendingTicket = pendingTicket+','+results;
          		}else{
          			pendingTicket = results;
          		}
          	}else{
          		document.getElementById(ticketChkId).checked = false;
          	}
     	 }
      }
}

function getSelectedTicket1(){
	shipNumber = "<%=request.getParameter("shipNumber") %>";
	workTickets='';
	var workTicket='';
	var ticketID='';
	var ticket='';
	var day='';
	<c:forEach var="resourceTransferMapDTO" items="${resourceTransferMapDTO}" >
		if(document.getElementById('transStatus${resourceTransferMapDTO.key}').checked){
			workTicket = document.getElementById('transStatus${resourceTransferMapDTO.key}').value;
			
			if(workTicket.length == 0){
				workTickets = workTicket;
			}else {
				workTickets = workTicket+','+workTickets;
			}
		}
	</c:forEach>
	if(workTickets.length > 0){
		var url ="saveTransferResourceToWorkTicket.html?decorator=simple&popup=true&shipNumber="+shipNumber+"&tickets="+workTickets+"&ticketStatus=P"+pendingTicket;
  		document.forms['resourceTransferForm'].action =url;
  		document.forms['resourceTransferForm'].submit();
	}else{
		alert('Please Select any Ticket.');
		return false;
	}
}
</script>
 --%>
	