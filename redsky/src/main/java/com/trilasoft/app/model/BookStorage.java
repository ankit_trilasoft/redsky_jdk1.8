package com.trilasoft.app.model;

import org.appfuse.model.BaseObject;   
import java.util.*;
import javax.persistence.Entity;
import javax.persistence.GenerationType;   
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;


@Entity
@Table(name="bookstorage")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class BookStorage extends BaseObject implements Comparable<BookStorage> {
	
	private Long id;
	private Long serviceOrderId;
	private Long idNum;
	private Long ticket;
	private String corpID;
	private String itemNumber;
	private String locationId;
	private String shipNumber;
	private String what;
	private String oldLocation;
	private String bookedBy;
	private Date dated;
	private String sequenceNumber;
	private String ship;
	private String jobNumber;
	private int pieces;
	private Double price;
	private String containerId;
	private String description;
	private String type;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String itemTag;
	private String storageId;
	private String warehouse;
	private String oldStorage;
	//private WorkTicket workTicket;
	private Storage storage;

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof BookStorage))
			return false;
		BookStorage castOther = (BookStorage) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(idNum, castOther.idNum)
				.append(ticket, castOther.ticket)
				.append(corpID, castOther.corpID)
				.append(itemNumber, castOther.itemNumber)
				.append(locationId, castOther.locationId)
				.append(shipNumber, castOther.shipNumber)
				.append(what, castOther.what)
				.append(oldLocation, castOther.oldLocation)
				.append(bookedBy, castOther.bookedBy)
				.append(dated, castOther.dated)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(ship, castOther.ship)
				.append(jobNumber, castOther.jobNumber)
				.append(pieces, castOther.pieces)
				.append(price, castOther.price)
				.append(containerId, castOther.containerId)
				.append(description, castOther.description)
				.append(type, castOther.type)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(itemTag, castOther.itemTag)
				.append(storageId, castOther.storageId)
				.append(warehouse, castOther.warehouse)
				.append(oldStorage, castOther.oldStorage).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(serviceOrderId)
				.append(idNum).append(ticket).append(corpID).append(itemNumber)
				.append(locationId).append(shipNumber).append(what)
				.append(oldLocation).append(bookedBy).append(dated)
				.append(sequenceNumber).append(ship).append(jobNumber)
				.append(pieces).append(price).append(containerId)
				.append(description).append(type).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(itemTag).append(storageId).append(warehouse).append(oldStorage).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append(
				"serviceOrderId", serviceOrderId).append("idNum", idNum)
				.append("ticket", ticket).append("corpID", corpID).append(
						"itemNumber", itemNumber).append("locationId",
						locationId).append("shipNumber", shipNumber).append(
						"what", what).append("oldLocation", oldLocation)
				.append("bookedBy", bookedBy).append("dated", dated).append(
						"sequenceNumber", sequenceNumber).append("ship", ship)
				.append("jobNumber", jobNumber).append("pieces", pieces)
				.append("price", price).append("containerId", containerId)
				.append("description", description).append("type", type)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("itemTag", itemTag).append("warehouse", warehouse).append("oldStorage", oldStorage).toString();
	}

	public int compareTo(final BookStorage other) {
		return new CompareToBuilder().append(id, other.id).append(
				serviceOrderId, other.serviceOrderId)
				.append(idNum, other.idNum).append(ticket, other.ticket)
				.append(corpID, other.corpID).append(itemNumber,
						other.itemNumber).append(locationId, other.locationId)
				.append(shipNumber, other.shipNumber).append(what, other.what)
				.append(oldLocation, other.oldLocation).append(bookedBy,
						other.bookedBy).append(dated, other.dated).append(
						sequenceNumber, other.sequenceNumber).append(ship,
						other.ship).append(jobNumber, other.jobNumber).append(
						pieces, other.pieces).append(price, other.price)
				.append(containerId, other.containerId).append(description,
						other.description).append(type, other.type).append(
						createdBy, other.createdBy).append(updatedBy,
						other.updatedBy).append(createdOn, other.createdOn)
				.append(updatedOn, other.updatedOn).append(itemTag,
						other.itemTag).append(warehouse,
								other.warehouse).append(oldStorage,
										other.oldStorage).toComparison();
	}

	@OneToOne
	    @JoinColumn(name="idNum",
	    referencedColumnName="idNum", insertable=false, updatable=false)
	    public Storage getStorage() {
	        return storage;
	    }
	    
		public void setStorage(Storage storage) {
				this.storage = storage;
		 } 
		
		
	//@ManyToOne
	//@JoinColumn(name="ticket", nullable=false, updatable=false,
	//referencedColumnName="ticket")
	/*public WorkTicket getWorkTicket() {
		return workTicket;
	}
	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}*/
				
	@Column(length=15)
	public String getBookedBy() {
		return bookedBy;
	}
	public void setBookedBy(String bookedBy) {
		this.bookedBy = bookedBy;
	}
	
	@Column(length=10)
	public String getContainerId() {
		return containerId;
	}
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}
	@Column
	public Date getDated() {
		return dated;
	}
	public void setDated(Date dated) {
		this.dated = dated;
	}
	
	@Column
	public Long getIdNum() {
		return idNum;
	}
	public void setIdNum(Long idNum) {
		this.idNum = idNum;
	}
	
	@Column(length=1000)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	
	public Long getId() {
		return id;
	}
	
	
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=15)
	public String getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}
	@Column(length=15)
	public String getJobNumber() {
		return jobNumber;
	}
	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}
	
	@Column(length=40)
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	@Column(length=12)
	public String getOldLocation() {
		return oldLocation;
	}
	public void setOldLocation(String oldLocation) {
		this.oldLocation = oldLocation;
	}
	
	@Column
	public int getPieces() {
		return pieces;
	}
	public void setPieces(int pieces) {
		this.pieces = pieces;
	}

	@Column
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@Column(length=15)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column(length=2)
	public String getShip() {
		return ship;
	}
	public void setShip(String ship) {
		this.ship = ship;
	}
	@Column(length=15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	/*
	@Column
	public Long getTicket() {
		return ticket;
	}
	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}
	*/
	@Column(length=1)
	public String getWhat() {
		return what;
	}
	public void setWhat(String what) {
		this.what = what;
	}

	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=1)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column(length=82)  
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length = 10)
	public String getItemTag() {
		return itemTag;
	}

	public void setItemTag(String itemTag) {
		this.itemTag = itemTag;
	}

	@Column(length=20)  
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}

	public Long getTicket() {
		return ticket;
	}

	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}
	@Column(length=65)
	public String getStorageId() {
		return storageId;
	}

	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}
	@Column(length=20)
	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	@Column(length=12)
	public String getOldStorage() {
		return oldStorage;
	}

	public void setOldStorage(String oldStorage) {
		this.oldStorage = oldStorage;
	}

}