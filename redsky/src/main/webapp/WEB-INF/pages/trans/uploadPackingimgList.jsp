<%@ include file="/common/taglibs.jsp"%>
<head>
<title>Inventory Image List</title>
<STYLE type=text/css>
#trailimageid
{
font-size: 0.75em;
font-family:Arial, Helvetica, sans-serif;
position: absolute;
visibility: hidden;
left: 0px;
top: 0px;
width: 400px;
height:0px;
z-index: 100;
}
</STYLE>
<meta name="heading"
	content="Inventory Image List" />
	
<script language="javascript" type="text/javascript">
/*
Simple Image Trail script- By JavaScriptKit.com
Visit http://www.javascriptkit.com for this script and more
This notice must stay intact
*/

var offsetfrommouse=[15,15]; //image x,y offsets from cursor position in pixels. Enter 0,0 for no offset
var displayduration=0; //duration in seconds image should remain visible. 0 for always.
var currentimageheight = 150; // maximum image size.

if (document.getElementById || document.all){
document.write('<div id="trailimageid">');
document.write('</div>');
}

function gettrailobj(){
if (document.getElementById)
return document.getElementById("trailimageid").style
else if (document.all)
return document.all.trailimagid.style
}

function gettrailobjnostyle(){
if (document.getElementById)
return document.getElementById("trailimageid")
else if (document.all)
return document.all.trailimagid
}
function truebody(){
return (!window.opera && document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}

function showtrail(imagename,title,description,showthumb,height){

if (height > 0){
currentimageheight = height;
}

document.onmousemove=followmouse;

newHTML = '<div style="padding:2px;">';
newHTML = newHTML + '<h2>' + title + '</h2>';
newHTML = newHTML + description + '<br/>';

if (showthumb > 0){
newHTML = newHTML + '<div align="center" style="padding: 2px 2px 2px 2px;">';
newHTML = newHTML + '<img src="' + imagename + '" border="0" width="420" hight="420"></div>';
}

gettrailobjnostyle().innerHTML = newHTML;
gettrailobj().visibility="visible";

}

function hidetrail(){
gettrailobj().visibility="hidden"
document.onmousemove=""
gettrailobj().left="-500px"

}

function followmouse(e){

var xcoord=offsetfrommouse[0]
var ycoord=offsetfrommouse[1]

var docwidth=document.all? truebody().scrollLeft+truebody().clientWidth : pageXOffset+window.innerWidth-15
var docheight=document.all? Math.min(truebody().scrollHeight, truebody().clientHeight) : Math.min(window.innerHeight)

//if (document.all){
// gettrailobjnostyle().innerHTML = 'A = ' + truebody().scrollHeight + '<br>B = ' + truebody().clientHeight;
//} else {
// gettrailobjnostyle().innerHTML = 'C = ' + document.body.offsetHeight + '<br>D = ' + window.innerHeight;
//}

if (typeof e != "undefined"){
if (docwidth - e.pageX < 380){
xcoord = e.pageX - xcoord - 400; // Move to the left side of the cursor
} else {
xcoord += e.pageX;
}
if (docheight - e.pageY < (currentimageheight + 110)){
ycoord += e.pageY - Math.max(0,(110 + currentimageheight + e.pageY - docheight - truebody().scrollTop));
} else {
ycoord += e.pageY;
}

} else if (typeof window.event != "undefined"){
if (docwidth - event.clientX < 380){
xcoord = event.clientX + truebody().scrollLeft - xcoord - 400; // Move to the left side of the cursor
} else {
xcoord += truebody().scrollLeft+event.clientX
}
if (docheight - event.clientY < (currentimageheight + 110)){
ycoord += event.clientY + truebody().scrollTop - Math.max(0,(110 + currentimageheight + event.clientY - docheight));
} else {
ycoord += truebody().scrollTop + event.clientY;
}
}

var docwidth=document.all? truebody().scrollLeft+truebody().clientWidth : pageXOffset+window.innerWidth-15
var docheight=document.all? Math.max(truebody().scrollHeight, truebody().clientHeight) : Math.max(document.body.offsetHeight, window.innerHeight)
if(ycoord < 0) { ycoord = ycoord*-1; }
gettrailobj().left=xcoord+"px"
gettrailobj().top=ycoord+"px"

}

function loadFirst(){
	if(document.forms['imageUpload'].elements['chkSaveTxt'].value=='saved'){
		parent.window.opener.document.location.reload();
	}
}

window.onload = function(){
	try{
		<c:if test="${ usertype== 'AGENT' || ( not empty inventoryData.id && not empty inventoryData.networkSynchedId && inventoryData.id != inventoryData.networkSynchedId ) }">
		document.getElementById('save').style.display="none";
		document.getElementById('attach').style.display="none";
		
		</c:if>
	}catch(e){}
}
</script>	
	
	
</head>
<body>
<div id="content" align="center" >
            <div id="liquid-round">
            <div class="top" style="margin-top:11px;!margin-top:-14px;"><span></span></div>
            <div class="center-content">  
  <div style="float:left;width:auto;">          
<table cellspacing="0" cellpadding="2" border="0" style="float:left;">  	
	  	    
	  	    <tr>
	  	    <c:forEach var="articlesList" items="${upLoadedImages2}" >
	  			<td height="5px" colspan="4" ><img src="UserImage?location=${articlesList.path}" alt="" class="image"  style="border:thin solid #219DD1;" height="300px" width="300px" /> </td>
	  		</c:forEach>
	  		</tr>
	  		
</table>
</div>
  <div style="float:left;width:auto;">   
<table cellspacing="0" cellpadding="2" border="0" style="float:left;">  	
	  	    
	  	    <tr>
	  	    <c:forEach var="articlesList2" items="${upLoadedImages}" >
	  			<td height="5px" colspan="4" >
	  			
	  			<!--<img src="UserImage?location=${articlesList2.path}" alt=""  class="image"  style="border:thin solid #219DD1;" height="250px" width="250px" onMouseOver="showtrail(this.src,'','',150);" onMouseOut="hidetrail();"/> 
	  			
	  			--><img src="UserImage?location=${articlesList2.path}" alt=""  class="image"  style="border:thin solid #219DD1;" height="300px" width="300px" />
				</td>
				</c:forEach>
	  		</tr>
	  		
</table>
</div>



<div style="clear:both;"></div>
<div>
<s:form name="imageUpload" action="packingImageUpload.html?chkSave=saved"  method="post" enctype="multipart/form-data"> 
<s:hidden name="id" value="${id}"/>
<s:hidden name="cid" value="${cid}"/>
<s:hidden name="chkSaveTxt" value="${chkSave}" />
<s:hidden name="sid" value="${sid }" />
<table cellspacing="0" cellpadding="2" border="0">  	
	  	    
	  	    <tr>
	  			<td class="listwhitetext">Upload Photograph</td><td><s:file id="attach" name="attachment" label="Attachment"/></td>
	  		</tr>
	  		
</table>

</div>
<div style="clear:both;"></div>
</div> 
<div style="heigt:35px;">&nbsp;</div>   
<div class="bottom-header"><span></span></div>
</div>	
<s:submit  id = "save" cssClass="cssbutton" cssStyle="width:55px; height:25px;margin-left:35px;"  key="button.save"/> 
</s:form>
<script language="javascript" type="text/javascript">
try{
	loadFirst();
}
catch(e){}
</script>
</body>	  		
<script language="javascript" type="text/javascript">
<c:if test="${hitflag=='1'}">
 	<c:redirect url="/packingImageListUpload.html?cid=${cid}&id=${id}&sid=${sid}&chkSave=saved"/>
</c:if>

</script>