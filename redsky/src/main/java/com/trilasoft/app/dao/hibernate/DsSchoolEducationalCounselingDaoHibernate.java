package com.trilasoft.app.dao.hibernate;

import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.DsSchoolEducationalCounselingDao;
import com.trilasoft.app.model.DsSchoolEducationalCounseling;
public class DsSchoolEducationalCounselingDaoHibernate extends GenericDaoHibernate<DsSchoolEducationalCounseling, Long> implements DsSchoolEducationalCounselingDao {	
	public DsSchoolEducationalCounselingDaoHibernate(){
		super(DsSchoolEducationalCounseling.class);
	}
	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsSchoolEducationalCounseling where id = "+id+" ");
	}
	
}

