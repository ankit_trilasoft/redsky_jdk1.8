package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.IntegrationLogInfo;

public interface IntegrationLogInfoManager extends GenericManager<IntegrationLogInfo, Long> {
	public String findSendMoveCloudStatus(String sessionCorpID ,String fileNumber);
	public String findSendMoveCloudStatusCrew(String sessionCorpID ,String fileNumber);
	public String findSendMoveCloudStatusClaim(String sessionCorpID ,String fileNumber);
	public String findSendMoveCloudGlobalStatus(String sessionCorpID ,String fileNumber,String taskType);
}
