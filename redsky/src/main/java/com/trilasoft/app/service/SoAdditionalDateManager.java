package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.SoAdditionalDate;


public interface SoAdditionalDateManager extends GenericManager<SoAdditionalDate, Long>{
	public List findId(Long sid, String sessionCorpID);

}
