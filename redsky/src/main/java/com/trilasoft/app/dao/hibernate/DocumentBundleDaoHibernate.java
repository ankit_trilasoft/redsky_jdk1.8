package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Query;

import com.trilasoft.app.dao.DocumentBundleDao;
import com.trilasoft.app.model.DocumentBundle;
import com.trilasoft.app.model.Reports;


public class DocumentBundleDaoHibernate extends GenericDaoHibernate<DocumentBundle, Long> implements DocumentBundleDao{

	public DocumentBundleDaoHibernate() {
		super(DocumentBundle.class);
	}
	
	public List findFormsByFormsId(String formsId) {
		List list = getHibernateTemplate().find("from Reports where id in ("+formsId+") order by id");
		return list;
	}
	
	public List getListByCorpId(String corpId, String tabId){
		String condition = " and status is true ";
		if(tabId != null && "wasteBasket".equals(tabId)){
			condition = " and status is not true ";
		}
		List list = getHibernateTemplate().find("from DocumentBundle where corpId='"+corpId+"' "+condition+" order by id");
		return list;
	}
	
	public String getReportDescAndIdByJrxmlName(String jrxmlName,String corpId){
		String accType="";
		String query="select concat(description,'~',id) from reports where reportName='"+jrxmlName+"' and corpID='"+corpId+"'";
			List al = this.getSession().createSQLQuery(query).list();
			if ((al != null) && (!al.isEmpty()) && (al.get(0) != null)) {
				accType = al.get(0).toString();
			}
		return accType;
	}
	
	public void updateFormsIdInDocumentBundle(String reportsId,long id){
			String query = "update documentbundle set formsId='"+reportsId+"' where id='"+id+"' ";
		    this.getSession().createSQLQuery(query).executeUpdate();
	}
	
	public List searchDocumentBundle(String bundleName,String serviceType,String serviceMode,String job,String tableName,String fieldName,String sessionCorpID,String routingType){
		String query = "from DocumentBundle where bundleName like '"+bundleName+"%' and corpId='"+sessionCorpID+"' and serviceType like '%"+serviceType+"%' and mode like '"+serviceMode+"%' and job like '%"+job+"%' and routing like '%"+routingType+"%' and sendPointTableName like '"+tableName+"%' and sendPointFieldName like '"+fieldName+"%' and status is true";
		return getHibernateTemplate().find(query);
	}
	
	public List getRecordForEmail(String routingType,String serviceType,String serviceMode,String job,String fieldName,String sessionCorpID,String jobNumber,String sequenceNumber,String firstName,String lastName,String clickedField){
		String emailVal="";
		List al1 = new ArrayList();
		List emailList = new ArrayList();
		String emailTable="";
		String emailField="";
		if(fieldName!=null && !fieldName.equals("")){
			String[] emailValue = fieldName.split("\\.");
				emailTable= emailValue[0];
				emailField = emailValue[1];
		}
		String query="select formsId,id,email,bundleName from documentbundle where corpId='"+sessionCorpID+"' and serviceType like '%"+serviceType+"%' and mode = '"+serviceMode+"' and job like '%"+job+"%' and routing like '%"+routingType+"%' and sendPointTableName like '"+emailTable+"%' and sendPointFieldName like '"+emailField+"%' and status is true order by bundleName";
		List al = this.getSession().createSQLQuery(query).list();
		EmailDTO dto =null;
		Iterator it = al.iterator();
		
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dto = new EmailDTO();
			dto.setFormsId(row[0]);
			dto.setDbId(row[1]);
			if(row[2]!=null && !row[2].equals("")){
				String[] emailValue = row[2].toString().split("\\.");
				emailTable= emailValue[0];
				emailField = emailValue[1];
				emailTable = emailTable.substring(0,emailTable.length()).toLowerCase();
				if(emailTable.equalsIgnoreCase("customerfile")){
					String query1="select "+emailField+" from "+emailTable+" where corpId='"+sessionCorpID+"' and sequenceNumber like '%"+sequenceNumber+"%' ";
					al1 = this.getSession().createSQLQuery(query1).list();
				}else{
					try {
					String query1="select "+emailField+" from "+emailTable+" where corpId='"+sessionCorpID+"' and shipNumber like '%"+jobNumber+"%' ";
					al1 = this.getSession().createSQLQuery(query1).list();
					} catch (Exception e) {
				    	 e.printStackTrace();
					} 
				}
				 if(al1!=null && !al1.isEmpty() && al1.get(0)!=null){
					emailVal = al1.get(0).toString();
				}
			}
			dto.setEmailValue(emailVal);
			dto.setBundleName(row[3]);
			dto.setShipNumber(jobNumber);
			dto.setFirstName(firstName);
			dto.setLastName(lastName);
			dto.setSequenceNumber(sequenceNumber);
			dto.setClickedField(clickedField);
			emailList.add(dto);	
		}
		return emailList;	
	}	
	
	public class EmailDTO{
			
			private Object formsId;
			private Object dbId;
			private Object emailValue;
			private Object bundleName;
			private Object shipNumber;
			private Object firstName;
			private Object lastName;
			private Object sequenceNumber;
			private Object clickedField;
			
			public Object getFormsId() {
				return formsId;
			}
			public void setFormsId(Object formsId) {
				this.formsId = formsId;
			}
			public Object getDbId() {
				return dbId;
			}
			public void setDbId(Object dbId) {
				this.dbId = dbId;
			}
			public Object getEmailValue() {
				return emailValue;
			}
			public void setEmailValue(Object emailValue) {
				this.emailValue = emailValue;
			}
			public Object getBundleName() {
				return bundleName;
			}
			public void setBundleName(Object bundleName) {
				this.bundleName = bundleName;
			}
			public Object getShipNumber() {
				return shipNumber;
			}
			public void setShipNumber(Object shipNumber) {
				this.shipNumber = shipNumber;
			}
			public Object getFirstName() {
				return firstName;
			}
			public void setFirstName(Object firstName) {
				this.firstName = firstName;
			}
			public Object getLastName() {
				return lastName;
			}
			public void setLastName(Object lastName) {
				this.lastName = lastName;
			}
			public Object getSequenceNumber() {
				return sequenceNumber;
			}
			public void setSequenceNumber(Object sequenceNumber) {
				this.sequenceNumber = sequenceNumber;
			}
			public Object getClickedField() {
				return clickedField;
			}
			public void setClickedField(Object clickedField) {
				this.clickedField = clickedField;
			}
	}
	
	public List findFileFromFileCabinet(String corpId, String jobNumber,String myFileIdTemp){
		List emailList = new ArrayList();
		String query = "";
		if(myFileIdTemp!=null && !myFileIdTemp.equalsIgnoreCase("")){
			query="select id,filefilename from myfile where corpId='"+corpId+"' and fileid = '"+jobNumber+"' and id in("+myFileIdTemp+") and active is true order by filefilename";
		}else{
			query="select id,filefilename from myfile where corpId='"+corpId+"' and fileid = '"+jobNumber+"' and active is true order by filefilename";
		}
		List al = this.getSession().createSQLQuery(query).list();
		EmailDTO dto =null;
		Iterator it = al.iterator();
		
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dto = new EmailDTO();
			dto.setDbId(row[0]);
			dto.setBundleName(row[1]);
			emailList.add(dto);	
		}
		return emailList;
	}
	
}
