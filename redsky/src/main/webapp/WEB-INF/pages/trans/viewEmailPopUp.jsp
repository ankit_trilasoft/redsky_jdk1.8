<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">
<head>   
<title><fmt:message key="reportsDetail.title"/></title>   
<meta name="heading" content="<fmt:message key='reportsDetail.heading'/>"/>   
<style type="text/css">
#overlay111 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}

.emailAttach {
background: rgba(255,255,255,1);
background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(212,226,246,1) 100%);
background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(255,255,255,1)), color-stop(100%, rgba(212,226,246,1)));
background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(212,226,246,1) 100%);
background: -o-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(212,226,246,1) 100%);
background: -ms-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(212,226,246,1) 100%);
background: linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(212,226,246,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#d4e2f6', GradientType=0 );
}
 input[type="radio"] {margin:0px;}
 img {
cursor:pointer;
}

.setBG{width: 600px;  background: transparent -moz-linear-gradient(top, #feffff 0%, #cce2ea 100%); height: auto; max-height: 200px; padding-left:3px;line-height: 18px; border: 1px solid rgb(33, 157, 209);}
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
  <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>    
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="javascript" type="text/javascript">

function validateFields(){
	var recipientTo=document.forms['EmailForm'].elements['receipientTo'].value;
	var reportSubject=document.forms['EmailForm'].elements['reportSubject'].value;	
	 if((recipientTo.trim()==null)||(recipientTo.trim()=='')){
			alert('Please enter values for Recipient To.');
			  return false;  
		}
	if((reportSubject.trim()==null)||(reportSubject.trim()=='')){
		alert('Please enter values for Subject To.');
		  return false;  
	}
	/* var url = "sendEmailPDFDocumentBundle.html?decorator=popup&popup=true";
	document.forms['EmailForm'].action =url;
	document.forms['EmailForm'].submit(); */
	setTimeout(function(){
		refreshParent();
          }, 2000); 
	 return true;
}

function refreshParent() {
	window.close();
}
</script>


</head>  
<body>

<s:form id="EmailForm" method="post" action="sendEmailPDFDocumentBundle" validate="true" onsubmit="return validateFields()" enctype="multipart/form-data">
<s:hidden name="jobNumber" value="<%=request.getParameter("jobNumber") %>"/>
<s:hidden name="sequenceNumber" value="<%=request.getParameter("sequenceNumber") %>"/>
<s:hidden name="clickedField" value="<%=request.getParameter("clickedField") %>"/>
<s:hidden name="reportsId" />
<s:hidden name="fileTypeName" />
<s:hidden name="myFileId" id="myFileId" />
<s:hidden name="tempFileName" id="tempFileName" />

<table class="notesDetailTable emailAttach" cellspacing="0" cellpadding="0" border="0" style="width:740px;padding-bottom:0px;margin:0px;">
		<tbody>
			<tr>
				<td>
		 			<div class="subcontent-tab">Preview Email</div>
		   				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
		    				<tbody>
		    				<tr><td style="height:10px;"></td></tr> 
								<tr>
                                <td align="right" class="listwhitetext" width="100px">
                                From :&nbsp;&nbsp;</td><td> <s:textfield name="fromName" cssClass="input-textUpper" readonly="true" cssStyle="width:170px" />
                                </td>
                                <td style="height:10px;"></td>            
								</tr>
		    					<tr style="height: 10px;"><td></td></tr>
								<tr>
                                <td align="right" class="listwhitetext" width="100px">
                                Recipients :&nbsp;To :<font color="red" size="2">*</font>&nbsp;&nbsp;
                                </td>
                                <td>
                                <s:textfield name="receipientTo" cssClass="input-text"  cssStyle="width:170px" />
                                </td>
                                <td align="right" class="listwhitetext" width="45px">
                                &nbsp;&nbsp;&nbsp;&nbsp;CC :&nbsp;&nbsp;
                                </td>
                                <td>
                                <s:textfield name="recipientCC" cssClass="input-text" cssStyle="width:170px" />
                                </td>
								</tr>
						     </tbody>
						</table>
						
						<table class="detailTabLabel">
						<tr>
			  			<td class="listwhitetext" width="93" align="right">Subject : </td>
			  			<td colspan="5">
			            <s:textfield name="reportSubject" cssClass="input-text"  cssStyle="width:387px;" />
			            </td>
			  			</tr>
						<tr>
			  			<td class="listwhitetext" width="93" align="right">Body : </td>
			  			<td colspan="5">
			            <s:textarea id="reportBody" name="reportBody" cssClass="input-text"  cssStyle="width:620px;height:280px" />
			            </td>
			  			</tr>
						<tr>
			  			<td class="listwhitetext" width="93" align="right">Signature : </td>
			  			<td colspan="5">
			            <s:textarea name="reportSignaturePart" cssClass="input-text"  cssStyle="width:620px;height:70px" />
			            </td>			            
			  			</tr>
			  			<tr>
			  			
			  			<td class="listwhitetext" width="500" colspan="10">
                         
                        <div style="float:left">
                        <b>&nbsp;&nbsp;Preview Form&nbsp;&nbsp;:</b>
			  			<c:forEach var="individualItem"	items="${documentBundleFormsList}" >
			  				<c:if test="${individualItem.pdf=='true'}">
			  					<c:set var="reportName" value="${fn:replace(individualItem.reportName,'jrxml', 'pdf')}" />
			  					<img alt="PDF File" style="vertical-align: text-bottom" src="<c:url value='/images/pdf1.gif'/>" onclick="generateFile('${individualItem.id}','${individualItem.pdf}','PDF');"/>${reportName}&nbsp;
			  				</c:if>
			  				<c:if test="${individualItem.docx=='true'}">
			  					<c:set var="reportName" value="${fn:replace(individualItem.reportName,'jrxml', 'docx')}" />
			  					<img alt="RTF File" style="vertical-align: text-bottom" src="<c:url value='/images/doc.gif'/>" onclick="generateFile('${individualItem.id}','${individualItem.docx}','DOCX')"/>${reportName}&nbsp;
			  				</c:if>
			  			</c:forEach>
			  			</div>
			  			
			  			<div style="padding-left:13px;margin-top:9px;" id="fileFromFileCabinet">
			  			<br><b>&nbsp;&nbsp;File Cabinet&nbsp;&nbsp;:</b>
                         	<img onclick="findFileFromFileCabinet('${jobNumber}');" style="vertical-align: top;" title="Attach file from file cabinet" src="${pageContext.request.contextPath}/images/file-attach.gif"/></br>
                         </div>
                        <div style="padding-left:113px;">
                         <c:if test="${fileCabinetList!='[]'}">
                         <s:set name="fileCabinetList" value="fileCabinetList" scope="request"/>
							<display:table name="fileCabinetList" class="setBG" id="fileCabinetList" style="line-height:13px;" pagesize="50" >   
									 <display:column title="" style="width:100px" ><c:out value="${fileCabinetList_rowNum}" />....<c:out value="${fileCabinetList.bundleName}" /> </display:column>
							</display:table>
                         </c:if>
                        </div>
                        
			  			<div style="float:left;padding-left:20px;margin-top:5px;">
			  			<b>&nbsp;&nbsp;Attach&nbsp;File&nbsp;&nbsp;:</b>
                         <img src="<c:url value='/images/attachIcon.png'/>" onclick="generateFileForDoc('${sequenceNumber}','${jobNumber}','${clickedField}','${reportsId}','${dbId}','${lastName}','${firstName}','${receipientTo}','${recipientCC}');" style="vertical-align: top;"/>
                        </div>
                        <div id="resourcMapAjax" style="display:none;float:left;" class="answer_list">
                          <input id="files" name="fileUpload" type="file" multiple/>                         
                          <div id="selectedFiles" >
                          </div>
                         </div>                         
                         <div id="tempFiles" style="float:left;"></div>
			  			</td>
			  			</tr>
			  			<tr><td class="" style="height:10px;"></td></tr>  	
						</table>
				</td>	
  			</tr>  		
  		</tbody>
</table>
                
				<table  cellspacing="0" cellpadding="0" border="0" style="width:510px;margin: 0px;padding: 0px;">
				<tbody>				
									<tr ><td class="listwhitetext" style="height:10px;"></td></tr>
									
									<tr>
										<td align="center" class="listwhitetext" style="height:10px"></td>
										<td style="text-align:right;margin-right:50px;">
										 <s:submit id="save" cssClass="cssbutton" method="" cssStyle="width:60px;" value="Send" theme="simple" />
										
										 <input type="button" id="cancel" class="cssbutton" method="" name="cancel" value="Cancel" style="width:60px; height:25px;margin:0px 50px 0px 10px;" onclick="refreshParent();" /></td>
										
										</td>
									</tr>
									
								</tbody>
					</table>
		 		
</s:form>
<script language="javascript" type="text/javascript">
function generateFileForDoc(seqNum,jobNum,clickedField,reportsId,dbId,lastName,firstName,receipientTo,recipientCC){
	 document.getElementById('resourcMapAjax').style.display = "block";
}

function generateFile(id,val,type){
	document.forms['EmailForm'].elements['reportsId'].value=id;
	 if(type=='PDF'){
		 document.forms['EmailForm'].elements['fileTypeName'].value=type; 
	 }else{
		 document.forms['EmailForm'].elements['fileTypeName'].value=type;
	 }
	 
	 var url = 'generateFileDocumentBundle.html?decorator=popup&popup=true';
		document.forms['EmailForm'].action =url;
		document.forms['EmailForm'].submit();
		
}

function findFileFromFileCabinet(jobNum){
	var myFileId = document.forms['EmailForm'].elements['myFileId'].value;
	var tempFiles = document.forms['EmailForm'].elements['tempFileName'].value;
	var receipientTo = document.forms['EmailForm'].elements['receipientTo'].value;
	var recipientCC = document.forms['EmailForm'].elements['recipientCC'].value;
	var reportSubject = document.forms['EmailForm'].elements['reportSubject'].value;
	var reportBody = document.forms['EmailForm'].elements['reportBody'].value;
	var reportSignaturePart = document.forms['EmailForm'].elements['reportSignaturePart'].value;
	var url = "getFileFromFileCabinetPopUp.html?decorator=popup&popup=true&jobNumber="+jobNum+"&tempFiles="+tempFiles+"&myFileId="+myFileId+"&receipientToVal="+encodeURIComponent(receipientTo)+"&recipientCCVal="+encodeURIComponent(recipientCC)+"&reportSubjectVal="+encodeURIComponent(reportSubject)+"&reportBodyVal="+encodeURIComponent(reportBody)+"&reportSignaturePartVal="+encodeURIComponent(reportSignaturePart);
	window.open(url,"", "top=100,left=500,width=550,height=300");
}
</script>

  <script>
  
  var selDiv = "";
        
    document.addEventListener("DOMContentLoaded", init, false);
    
    function init() {
        document.querySelector('#files').addEventListener('change', handleFileSelect, false);
        selDiv = document.querySelector("#selectedFiles");
    }
    var tempFileStore="";    
    function handleFileSelect(e) {
    	$('#selectedFiles').addClass('setBG');
        if(!e.target.files) return;
        
        selDiv.innerHTML = "";
        
        var files = e.target.files;
        for(var i=1; i<files.length+1; i++) {
            var f = files[i-1];
            if(tempFileStore==''){
            	tempFileStore = f.name;
            }else{
            	tempFileStore = tempFileStore+","+f.name;
            }
            
            if(i%3==0){
          		selDiv.innerHTML += "<b style='color:#003366;'>"+i+".) "+"</b>"+f.name + "<br/>";
            }else{
            	selDiv.innerHTML += "<b style='color:#003366;'>"+i+".) "+"</b>"+ f.name + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            }
        }
      document.forms['EmailForm'].elements['tempFileName'].value=tempFileStore;
      var div = document.getElementById('tempFiles');
	  div.innerHTML = "";
	  div.style.border = '0px';
    }
    
    function populateFileName(file){
    	$('#tempFiles').addClass('setBG');
    	var div = document.getElementById('tempFiles');
    	div.innerHTML = "";
    	var sel = document.getElementById('selectedFiles');
        sel.innerHTML = "";
        //sel.style.border = '0px';
        var files = file;
        var filess = files.split(",");
        var j=1;
        for(var i=0; i<filess.length; i++) {
            	div.innerHTML += "<b style='color:#003366;'>"+j+++".) "+"</b>"+filess[i] + "<br/>";
        }
        document.forms['EmailForm'].elements['tempFileName'].value = file;
        document.forms['EmailForm'].elements['fileUpload'].value = file;
    }
    
    </script>

</body>	


