
delimiter $$

CREATE trigger redsky.trigger_add_history_documentaccesscontrol
BEFORE UPDATE on redsky.documentaccesscontrol
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";






   IF (OLD.fileType <> NEW.fileType ) THEN
      select CONCAT(fieldNameList,'documentaccesscontrol.fileType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fileType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fileType,'~') into newValueList;
  END IF;





    IF (OLD.isCportal <> NEW.isCportal ) THEN
       select CONCAT(fieldNameList,'documentaccesscontrol.isCportal~') into fieldNameList;
       IF(OLD.isCportal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isCportal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isCportal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isCportal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



    IF (OLD.isAccportal <> NEW.isAccportal ) THEN
       select CONCAT(fieldNameList,'documentaccesscontrol.isAccportal~') into fieldNameList;
       IF(OLD.isAccportal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isAccportal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isAccportal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isAccportal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




    IF (OLD.isPartnerPortal <> NEW.isPartnerPortal ) THEN
       select CONCAT(fieldNameList,'documentaccesscontrol.isPartnerPortal~') into fieldNameList;
       IF(OLD.isPartnerPortal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isPartnerPortal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isPartnerPortal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isPartnerPortal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




    IF (OLD.isBookingAgent <> NEW.isBookingAgent ) THEN
       select CONCAT(fieldNameList,'documentaccesscontrol.isBookingAgent~') into fieldNameList;
       IF(OLD.isBookingAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isBookingAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isBookingAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isBookingAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





    IF (OLD.isNetworkAgent <> NEW.isNetworkAgent ) THEN
       select CONCAT(fieldNameList,'documentaccesscontrol.isNetworkAgent~') into fieldNameList;
       IF(OLD.isNetworkAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isNetworkAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isNetworkAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isNetworkAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




    IF (OLD.isOriginAgent <> NEW.isOriginAgent ) THEN
       select CONCAT(fieldNameList,'documentaccesscontrol.isOriginAgent~') into fieldNameList;
       IF(OLD.isOriginAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isOriginAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isOriginAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isOriginAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




    IF (OLD.isSubOriginAgent <> NEW.isSubOriginAgent ) THEN
       select CONCAT(fieldNameList,'documentaccesscontrol.isSubOriginAgent~') into fieldNameList;
       IF(OLD.isSubOriginAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isSubOriginAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isSubOriginAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isSubOriginAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





    IF (OLD.isDestAgent <> NEW.isDestAgent ) THEN
       select CONCAT(fieldNameList,'documentaccesscontrol.isDestAgent~') into fieldNameList;
       IF(OLD.isDestAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isDestAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isDestAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isDestAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





    IF (OLD.isSubDestAgent <> NEW.isSubDestAgent ) THEN
       select CONCAT(fieldNameList,'documentaccesscontrol.isSubDestAgent~') into fieldNameList;
       IF(OLD.isSubDestAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isSubDestAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isSubDestAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isSubDestAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






  CALL add_tblHistory (OLD.id,"documentaccesscontrol", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;