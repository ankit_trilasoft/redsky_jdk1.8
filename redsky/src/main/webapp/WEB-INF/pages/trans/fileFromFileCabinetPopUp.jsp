<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">  
<head>   
  <title>File List</title>  
    <style type="text/css"> 

</style> 
</head>

<s:form name="fileCabinetListForm" id="fileCabinetListForm">
<s:hidden name="fid" id="fid" />
<s:hidden name="tempFiles" value="<%=request.getParameter("tempFiles") %>" />
<s:hidden name="receipientToVal" value="<%=request.getParameter("receipientToVal") %>" />
<s:hidden name="recipientCCVal" value="<%=request.getParameter("recipientCCVal") %>" />
<s:hidden name="reportSubjectVal" value="<%=request.getParameter("reportSubjectVal") %>" />
<s:hidden name="reportBodyVal" value="<%=request.getParameter("reportBodyVal") %>" />
<s:hidden name="reportSignaturePartVal" value="<%=request.getParameter("reportSignaturePartVal") %>" />

<c:if test="${fileCabinetList=='[]'}">
<table class="table" id="dataTable" style="width:550px;margin-top:0px;">
	<thead>					 
			<tr>
					 <th width="">File</th>
			</tr>
	</thead>
			<tbody>
			<td  class="listwhitetextWhite" colspan="3">Nothing Found To Display.</td>
			</tbody>
</table>
</c:if>


<c:if test="${fileCabinetList!='[]'}"> 
<s:set name="fileCabinetList" value="fileCabinetList" scope="request"/>
<display:table name="fileCabinetList" class="table" requestURI="" id="fileCabinetList" style="width:500px" pagesize="50" >   
		 <display:column title="File" style="width:100px" >
			 <a onclick="downloadSelectedFile('${fileCabinetList.dbId}');">
				<c:out value="${fileCabinetList.bundleName}" escapeXml="false"/>
			</a>
		 </display:column>		 
	    <display:column   title="" style="width:10px" >
 			<input style="vertical-align:bottom;" type="checkbox" name="radioButton" id="radioButton${fileCabinetList.dbId}" value="${fileCabinetList.dbId}" onclick="setValues('${fileCabinetList.dbId}',this)"/>
 		</display:column>
</display:table>

<table  cellspacing="0" cellpadding="0" border="0" style="width:510px;margin: 0px;padding: 0px;">
				<tbody>				
									<tr ><td class="listwhitetext" style="height:10px;"></td></tr>
									
									<tr>
										<td align="center" class="listwhitetext" style="height:10px"></td>
										<td style="text-align:left;">
										 <input type="button" id="save" class="cssbutton" method="" name="save" value="Attach File"  style="width:80px; height:25px;margin:0px 10px 0px 10px;" onclick="attachFileToparent();" />
										
										 <input type="button" id="cancel" class="cssbutton" method="" name="cancel" value="Cancel" style="width:60px; height:25px;margin:0px 50px 0px 10px;" onclick="closeWindow();" /></td>
										
										</td>
									</tr>
									
								</tbody>
					</table>
</c:if>


</s:form>
</div></div></div>

<script type="text/javascript">
	
	function closeWindow() {
		window.close();
	}
   
   function setValues(rowId,targetElement){
	   var userCheckStatus = document.getElementById("fid").value
	  if(targetElement.checked){
		  if(userCheckStatus == '' ){
			  document.getElementById("fid").value = rowId;
			}else{
				if(userCheckStatus.indexOf(rowId)>=0){
				}else{
					userCheckStatus = userCheckStatus + "~" +rowId;
				}
				document.getElementById("fid").value = userCheckStatus.replace('~ ~',"~");
			}
	   }else{
			if(userCheckStatus == ''){
				document.getElementById("fid").value = rowId;
		     }else{
		    	 var check = userCheckStatus.indexOf(rowId);
		 		if(check > -1){
		 			var values = userCheckStatus.split("~");
		 		   for(var i = 0 ; i < values.length ; i++) {
		 		      if(values[i]== rowId) {
		 		    	 values.splice(i, 1);
		 		    	userCheckStatus = values.join("~");
		 		    	document.getElementById("fid").value = userCheckStatus;
		 		      }
		 		   }
		 		}else{
		 			userCheckStatus = userCheckStatus + "~" +rowId;
		 			document.getElementById("fid").value = userCheckStatus.replace('~ ~',"~");
		 		}
		     }
	   }
  	}
   
   function attachFileToparent(){
	   try{
		   	window.opener.document.forms['EmailForm'].elements['myFileId'].value=document.forms['fileCabinetListForm'].elements['fid'].value;
			var parentUrl =window.opener.location.href;
			var replaceURL = "";
			if(parentUrl.indexOf('myFileId') > 0){
				replaceURL = parentUrl;
				replaceURL = updateURLParameter(replaceURL, 'myFileId', document.getElementById("fid").value);
			}else{
				replaceURL = parentUrl+"&myFileId="+document.getElementById("fid").value;
			}
			
			window.opener.location.href=replaceURL;
			setTimeout(function(){
				refreshParent();
		    }, 500);
	   }catch(e){}
   }
   
   function updateURLParameter(url, param, paramVal){
	    var newAdditionalURL = "";
	    var tempArray = url.split("?");
	    var baseURL = tempArray[0];
	    var additionalURL = tempArray[1];
	    var temp = "";
	    if (additionalURL) {
	        tempArray = additionalURL.split("&");
	        for (i=0; i<tempArray.length; i++){
	            if(tempArray[i].split('=')[0] != param){
	                newAdditionalURL += temp + tempArray[i];
	                temp = "&";
	            }
	        }
	    }
	    var rows_txt = temp + "" + param + "=" + paramVal;
	    return baseURL + "?" + newAdditionalURL + rows_txt;
	}
   
   function refreshParent(){
	   	//parent.window.opener.document.location.reload();
	   	try{
		   	var receipientToVal = document.forms['fileCabinetListForm'].elements['receipientToVal'].value;
		   	receipientToVal = decodeURIComponent(receipientToVal);
		   	var recipientCCVal = document.forms['fileCabinetListForm'].elements['recipientCCVal'].value;
		   	recipientCCVal = decodeURIComponent(recipientCCVal);
		   	var reportSubjectVal = document.forms['fileCabinetListForm'].elements['reportSubjectVal'].value;
		   	reportSubjectVal = decodeURIComponent(reportSubjectVal);
		   	var reportBodyVal = document.forms['fileCabinetListForm'].elements['reportBodyVal'].value;
		   	reportBodyVal = decodeURIComponent(reportBodyVal);
		   	var reportSignaturePartVal = document.forms['fileCabinetListForm'].elements['reportSignaturePartVal'].value;
		   	reportSignaturePartVal = decodeURIComponent(reportSignaturePartVal);
		   	window.opener.document.forms['EmailForm'].elements['receipientTo'].value=receipientToVal;
		   	window.opener.document.forms['EmailForm'].elements['recipientCC'].value=recipientCCVal;
		   	window.opener.document.forms['EmailForm'].elements['reportSubject'].value=reportSubjectVal;
		   	window.opener.document.forms['EmailForm'].elements['reportBody'].value=reportBodyVal;
		   	window.opener.document.forms['EmailForm'].elements['reportSignaturePart'].value=reportSignaturePartVal;
		   	var file = document.forms['fileCabinetListForm'].elements['tempFiles'].value;
		   	if(file!=null && file!=''){
		   		window.opener.populateFileName(file);
		   	}
	   	}catch(e){}
	window.close();
   }
   
   function downloadSelectedFile(id){
		url="ImageServletAction.html?id="+id+"";
		location.href=url;
	}
   
  </script>
 		  	