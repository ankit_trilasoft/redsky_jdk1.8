package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;


import com.trilasoft.app.dao.RefFreightRatesDao;

import com.trilasoft.app.model.RefFreightRates;

import com.trilasoft.app.service.RefFreightRatesManager;

public class RefFreightRatesManagerImpl extends GenericManagerImpl<RefFreightRates, Long> implements RefFreightRatesManager {
	RefFreightRatesDao refFreightRatesDao;
	public RefFreightRatesManagerImpl(RefFreightRatesDao refFreightRatesDao) {
		super(refFreightRatesDao); 
        this.refFreightRatesDao = refFreightRatesDao; 
	}
	public String saveData(String sessionCorpID, String file, String userName) {
		return	refFreightRatesDao.saveData(sessionCorpID, file,userName);
		
	}
	public List getExistingContracts(String sessionCorpID) {
		
		return	refFreightRatesDao.getExistingContracts(sessionCorpID);
	}
	public List getMissingPort(String sessionCorpID) {
		
		return	refFreightRatesDao.getMissingPort(sessionCorpID);
	}

}
