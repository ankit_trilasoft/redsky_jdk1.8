<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
<title>Work Planning Calendar</title>
<meta name="heading" content="Work Planning" charset='utf-8'/>
</head>
<div>

<c:if test="${workListDetails!='[]'}">
	<display:table name="workListDetails" class="table" requestURI="" id="workListDetails" export="true" pagesize="200" defaultsort="1"> 
	    <display:column property="dateDisplay" group="1" sortable="true" title="Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column property="ticket" sortable="true" title=" Total Ticket"/>
	    <display:column property="date1" sortable="true" title="Begin Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column property="date2" sortable="true" title="End Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column property="warehouse" sortable="true" title="Warehouse"/>
	    <display:column property="estimatedweight" headerClass="containeralign" sortable="true" style="text-align:right;" title="Est. Weight"/>
	    <display:column property="service" sortable="true" title="Service"/>
	    <c:if test="${checkCrewListRole == true && hubWarehouseLimit=='Crw'}">
	    	<display:column property="requiredcrew" sortable="true" title="Crew Required"/>
	    </c:if>
	 	<display:column property="duration" sortable="true" title="Duration"/>
	 	<display:column property="avgWt" sortable="true" headerClass="containeralign" style="text-align:right;" title="Total Avg. Weight"/> 	
	 	<configByCorp:fieldVisibility componentId="component.field.Alternative.showForUghkOnly">		 	
	 	<c:choose>
 			<c:when test="${workListDetails.service=='LD'}">
			     <display:column title="Origin&nbsp;Country" sortable="true" ><c:out value="${workListDetails.originCountry}" /></display:column>
			</c:when>
			<c:otherwise> 
 				<display:column title="Origin&nbsp;Country" sortable="true" ><c:out value=""></c:out></display:column>
		    </c:otherwise>			   
		</c:choose> 
	 	<c:choose>
			<c:when test="${workListDetails.service=='DL'}" >
			   <display:column title="Destination&nbsp;Country" sortable="true" ><c:out value="${workListDetails.destinationCountry}" /></display:column>
		    </c:when>
			<c:otherwise> 
 				<display:column title="Destination&nbsp;Country" sortable="true" ><c:out value=""></c:out></display:column>
		    </c:otherwise>			   
		</c:choose> 
		</configByCorp:fieldVisibility>
	    <display:setProperty name="paging.banner.item_name" value="workListDetails"/>
	    <display:setProperty name="paging.banner.items_name" value="people"/>	
	    <display:setProperty name="export.excel.filename" value="Work Planning List.xls"/>
	    <display:setProperty name="export.csv.filename" value="Work Planning List.csv"/>
	    <display:setProperty name="export.pdf.filename" value="Work Planning List.pdf"/>
	</display:table>
	</c:if>

<c:if test="${workListDetails=='[]'}">
<display:table name="workListDetails" class="table" requestURI="" id="workListDetails" export="true" pagesize="200" defaultsort="1"> 
	    <display:column sortable="true" title="Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column sortable="true" title=" Total Ticket"/>
	    <display:column sortable="true" title="Begin Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column sortable="true" title="End Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column sortable="true" title="Warehouse"/>
	    <display:column sortable="true" title="Est. Weight"/>
	    <display:column sortable="true" title="Service"/>
	    <c:if test="${checkCrewListRole == true && hubWarehouseLimit=='Crw'}">
	    	<display:column sortable="true" title="Crew Required"/>
	    </c:if>
	 	<display:column sortable="true" title="Duration"/>
	 	<display:column sortable="true" title="Total Avg. Weight"/> 	 		
	 	<configByCorp:fieldVisibility componentId="component.field.Alternative.showForUghkOnly">
	 	<display:column sortable="true" title="Origin&nbsp;Country"/>
	 	<display:column sortable="true" title="Destination&nbsp;Country"/> 		 	
		</configByCorp:fieldVisibility>
	    <display:setProperty name="paging.banner.item_name" value="workListDetails"/>
	    <display:setProperty name="paging.banner.items_name" value="people"/>	
	    <display:setProperty name="export.excel.filename" value="Work Planning List.xls"/>
	    <display:setProperty name="export.csv.filename" value="Work Planning List.csv"/>
	    <display:setProperty name="export.pdf.filename" value="Work Planning List.pdf"/>
	</display:table>

</c:if>
</div>

	<script type="text/javascript">
try{
	window.print();
}catch(e){}
</script>