<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="LeaveManagementReport"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="829"
		 pageHeight="784"
		 columnWidth="769"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="false" class="java.lang.String"/>
	<parameter name="End_Date" isForPrompting="true" class="java.util.Date"/>
	<parameter name="Company Division" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[select "LeaveManagementReport.jrxml", c.warehouse AS Warehouse,
(CASE c.warehouse
      when ("1") then "01"
  when ("2") then "02"
  when ("3") then "03"
  when ("4") then "04"
  when ("9") then "09"
  when ("12") then "12"
  when ("14") then "14"
  when ("15") then "15"
  else " "
  end )  AS Wh,
 c.username AS 'User Name' , c.firstname AS 'First Name', c.lastname AS 'Last Name', Concat(c.lastname,', ',c.firstname) AS FullName, t.action AS Action, c.companyDivision AS 'CompanyDivision',
date(t.workdate) AS Date, t.regularhours AS RegularHours, c.carryoverallowed AS 'Carry Over Allowed', c.vacationhrs AS 'Vacation hrs', 
c.vacationused AS 'Vacation Used',c.unscheduledLeave AS 'Unscheduled Leave',if((c.carryoverallowed + c.vacationhrs)>=(c.vacationused+c.unscheduledLeave),((c.carryoverallowed + c.vacationhrs) - (c.vacationused+c.unscheduledLeave)),null) AS 'BALANCE', if((c.carryoverallowed + c.vacationhrs)<(c.vacationused+c.unscheduledLeave),((c.carryoverallowed + c.vacationhrs) - (c.vacationused+c.unscheduledLeave)),null) AS 'BALANCES',
date(c.hired) AS Hired, date(c.lastAnniversaryDate) AS 'Last Anniversary Date'
FROM crew c left outer join timesheet t on t.username = c.username and t.workdate between  date_format(c.lastanniversarydate,'%Y-%m-%d') and $P{End_Date} and t.action in ('V') and t.corpid=$P{Corporate ID}
where c.corpid = $P{Corporate ID} and c.companydivision like $P{Company Division}
and terminateddate is null and c.unionname is not null 
and c.unionname <= sysdate()
order by wh,FullName, t.action, t.workdate]]></queryString>

	<field name="LeaveManagementReport.jrxml" class="java.lang.String"/>
	<field name="Warehouse" class="java.lang.String"/>
	<field name="Wh" class="java.lang.String"/>
	<field name="User Name" class="java.lang.String"/>
	<field name="First Name" class="java.lang.String"/>
	<field name="Last Name" class="java.lang.String"/>
	<field name="FullName" class="java.lang.String"/>
	<field name="Action" class="java.lang.String"/>
	<field name="CompanyDivision" class="java.lang.String"/>
	<field name="Date" class="java.sql.Date"/>
	<field name="RegularHours" class="java.lang.Integer"/>
	<field name="Carry Over Allowed" class="java.lang.Integer"/>
	<field name="Vacation hrs" class="java.lang.Integer"/>
	<field name="Vacation Used" class="java.lang.Integer"/>
	<field name="Unscheduled Leave" class="java.lang.Integer"/>
	<field name="BALANCE" class="java.lang.Integer"/>
	<field name="BALANCES" class="java.lang.Integer"/>
	<field name="Hired" class="java.sql.Date"/>
	<field name="Last Anniversary Date" class="java.sql.Date"/>


		<group  name="Warehouse" isStartNewPage="true" >
			<groupExpression><![CDATA[$F{Warehouse}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="CompanyDivision" >
			<groupExpression><![CDATA[$F{CompanyDivision}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="UserName" >
			<groupExpression><![CDATA[$F{User Name}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="46"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="0"
						width="769"
						height="28"
						key="staticText-1"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="18" isBold="true"/>
					</textElement>
				<text><![CDATA[Vacation Hours Management Report]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="352"
						y="28"
						width="100"
						height="18"
						key="textField"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{End_Date}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Opaque"
						x="267"
						y="28"
						width="85"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-35"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Up To  :  ]]></text>
				</staticText>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="42"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Opaque"
						x="636"
						y="3"
						width="40"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-9"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Action]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="729"
						y="3"
						width="40"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-10"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Hours]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="190"
						y="3"
						width="78"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-20"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Carry Over]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="676"
						y="3"
						width="53"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-22"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="323"
						y="3"
						width="56"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-23"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Vacation]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="31"
						y="3"
						width="159"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-28"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Full Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="455"
						y="3"
						width="51"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-30"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Balance]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="506"
						y="3"
						width="60"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-31"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Hired]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="268"
						y="3"
						width="55"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-32"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Vacation]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="566"
						y="3"
						width="70"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-33"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Last]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						mode="Transparent"
						x="0"
						y="1"
						width="768"
						height="1"
						key="line-5"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="40"
						width="768"
						height="1"
						key="line-6"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="3"
						width="31"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-34"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[W/H]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="636"
						y="21"
						width="40"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-37"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="729"
						y="21"
						width="40"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-38"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="190"
						y="21"
						width="78"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-39"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Allowed]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="676"
						y="21"
						width="53"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-40"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="323"
						y="21"
						width="56"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-41"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Used]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="31"
						y="21"
						width="159"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-42"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="455"
						y="21"
						width="51"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-43"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="506"
						y="21"
						width="60"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-44"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="268"
						y="21"
						width="55"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-45"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Hrs]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="566"
						y="21"
						width="70"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-46"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Anniversary]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="21"
						width="31"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-47"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="379"
						y="3"
						width="76"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-48"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Unscheduled]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="379"
						y="21"
						width="76"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-49"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Leave]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="37"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" pattern="###0.0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="190"
						y="19"
						width="78"
						height="18"
						key="textField"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="UserName"/>
					<box rightPadding="21">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$F{Carry Over Allowed}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="###0.0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="323"
						y="19"
						width="56"
						height="18"
						key="textField"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="UserName"/>
					<box rightPadding="19">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$F{Vacation Used}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="676"
						y="19"
						width="53"
						height="18"
						key="textField"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{Date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="636"
						y="19"
						width="40"
						height="18"
						key="textField"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Action}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="###0.0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="729"
						y="19"
						width="40"
						height="18"
						key="textField"/>
					<box rightPadding="7">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$F{RegularHours}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="31"
						y="19"
						width="159"
						height="18"
						key="textField-13"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{FullName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="###0.0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="455"
						y="19"
						width="51"
						height="18"
						key="textField-15"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="UserName">
							<property name="Neg" value="&lt;0" />
							<property name="Pos" value="&gt;0" />
						</reportElement>
					<box rightPadding="14"></box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$F{BALANCE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="506"
						y="19"
						width="60"
						height="18"
						key="textField-16"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="UserName"/>
					<box rightPadding="1">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{Hired}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="###0.0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="268"
						y="19"
						width="55"
						height="18"
						key="textField-17"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="UserName"/>
					<box rightPadding="19">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$F{Vacation hrs}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="566"
						y="19"
						width="70"
						height="18"
						key="textField-18"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="UserName"/>
					<box rightPadding="1">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{Last Anniversary Date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="19"
						width="31"
						height="18"
						key="textField"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Wh}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="109"
						y="1"
						width="86"
						height="18"
						key="textField-19"
						isPrintRepeatedValues="false"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="CompanyDivision"/>
					<box></box>
					<textElement textAlignment="Left">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{CompanyDivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="1"
						width="109"
						height="18"
						forecolor="#3300CC"
						backcolor="#FFFFFF"
						key="staticText-36"
						isPrintRepeatedValues="false"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="CompanyDivision"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Company Division :]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" pattern="###0.0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="455"
						y="19"
						width="51"
						height="18"
						forecolor="#CC0033"
						key="textField-20"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="UserName">
							<property name="Neg" value="&lt;0" />
							<property name="Pos" value="&gt;0" />
						</reportElement>
					<box rightPadding="14"></box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$F{BALANCES}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="###0.0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="379"
						y="19"
						width="76"
						height="18"
						key="textField-21"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"
						printWhenGroupChanges="UserName"/>
					<box rightPadding="19">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$F{Unscheduled Leave}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="15"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="5"
						y="0"
						width="137"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" size="10"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="142"
						y="0"
						width="415"
						height="15"
						key="staticText-14"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial"/>
					</textElement>
				<text><![CDATA[LeaveManagementReport.jrxml]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="557"
						y="0"
						width="50"
						height="15"
						key="textField-11"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="607"
						y="0"
						width="96"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-12"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Helvetica" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="CP1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
