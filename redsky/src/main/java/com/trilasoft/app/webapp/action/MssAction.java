package com.trilasoft.app.webapp.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.InventoryData;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.Mss;
import com.trilasoft.app.model.MssDestinationService;
import com.trilasoft.app.model.MssGrid;
import com.trilasoft.app.model.MssOriginService;
import com.trilasoft.app.model.ResourceGrid;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TableCatalog;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.InventoryDataManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.MssDestinationServiceManager;
import com.trilasoft.app.service.MssGridManager;
import com.trilasoft.app.service.MssManager;
import com.trilasoft.app.service.MssOriginServiceManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.WorkTicketManager;
//import com.trilasoft.mss.service.GetCustomerBillTos;
import com.trilasoft.mss.MSSServiceCallClient;
import com.trilasoft.mss.MssXmlDto;
import com.trilasoft.mss.webservices.MSSOrder;


public class MssAction extends BaseAction implements Preparable {
	
	private Long id;
	private String sessionCorpID;
	private Mss mss;
	private MssManager mssManager;
	private WorkTicket workTicket;
	private WorkTicketManager workTicketManager;
	private Long id1;
	private ServiceOrder serviceOrder;
	private CustomerFile customerFile;
	private MiscellaneousManager miscellaneousManager;
	private BillingManager billingManager;
	private Miscellaneous miscellaneous;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private ServiceOrderManager serviceOrderManager;
	private Billing billing;
	private RefMasterManager refMasterManager;
	private  Map<String, String> submitAs;
	private  Map<String, String> billToDivision;
	private  Map<String, String> affiliatedTo;
	private List transportTypeValue;
	private String mssRowlist;
	private Map<String, String> transportTypeRadio;
	private MssGridManager mssGridManager;
	private MssGrid mssGrid;
	private String transportListServer;
	private String descriptionListServer;
	private String lengthListServer;
	private String widthListServer;
	private String heightListServer;
	private String plyListServer;
	private String intlIspmisListServer;
	private String apprListServer;
	private String codListServer;
	private  Map<String, String> originCat;
	private  Map<String, String> originCatSubtype;
	private  Map<String, String> destinationCat;
	private  Map<String, String> destinationCatSubtype;
	private String oriServicelist;
	private MssOriginService mssOriginService;
	private MssOriginServiceManager mssOriginServiceManager;
	private String oriItemListServer;
	private String oriApprListServer;
	private String oriCodListServer;
	private String destItemListServer;
	private String destApprListServer;
	private String destCodListServer;
	private String desServicelist;
	private MssDestinationService mssDestinationService;
	private MssDestinationServiceManager mssDestinationServiceManager;
	private InventoryDataManager inventoryDataManager;
	private int rowCountvalue;
	private InventoryData inventoryData;
	private String returnAjaxStringValue;
	private String inventoryFlag;
	//private GetCustomerBillTos getCustomerBillTos;
	String env;
	private List mssDestinationServList;
	private String imageId;
	private String noteFor;
	private List notess;
	private String subType;
	private List mssItemList;
	private Long wid;
	private List mssOriServList;
	static final Logger logger = Logger.getLogger(MssAction.class);
    Date currentdate = new Date();
    
	public void prepare() throws Exception {		
	}
	
	public MssAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
	 env = System.getProperty("env");
		
	} 
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		workTicket = workTicketManager.get(wid);
		serviceOrder=workTicket.getServiceOrder();
		//getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus=trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		List mssIdList = mssManager.checkById(wid);	
		if(mssIdList!=null && !mssIdList.isEmpty()&& mssIdList.get(0)!=null && !mssIdList.get(0).toString().equalsIgnoreCase("")){
			id = Long.parseLong(mssIdList.get(0).toString());
		}else{
			id = null;
		}
		if (id != null) {
			subType = "";
			noteFor="Mss";
			mss = mssManager.get(id);
			mss.setVip(customerFile.getVip());
			if(mss.getPurchaseOrderNumber()!=null && mss.getPurchaseOrderNumber().equalsIgnoreCase("") ){
				mss.setPurchaseOrderNumber(serviceOrder.getShipNumber());
			}
			if(mss.getSoNumber()!=null && mss.getSoNumber().equalsIgnoreCase("")){
				mss.setSoNumber(serviceOrder.getSequenceNumber());	
			}
			if(mss.getShipperFirstName()!=null && mss.getShipperFirstName().equalsIgnoreCase("")){
				 mss.setShipperFirstName(serviceOrder.getFirstName());
			}
			if(mss.getShipperLastName()!=null && mss.getShipperLastName().equalsIgnoreCase("")){
				mss.setShipperLastName(serviceOrder.getLastName());
			}
		    if(mss.getShipperEmailAddress()!=null && mss.getShipperEmailAddress().equalsIgnoreCase("")){
		      mss.setShipperEmailAddress(serviceOrder.getEmail()); 
		     }
		    mss.setCompanyDivision(serviceOrder.getCompanyDivision());
			mss.setShipperOriginAddress1(workTicket.getAddress1());
			mss.setShipperOriginAddress2(workTicket.getAddress2());
			mss.setShipperOriginCity(workTicket.getCity());
			mss.setShipperOriginState(workTicket.getState());
			mss.setShipperOriginZip(workTicket.getZip());
			mss.setWorkTicketNumber(workTicket.getTicket().toString());
			mss.setShipperDestinationAddress1(workTicket.getDestinationAddress1());
			mss.setShipperDestinationAddress2(workTicket.getDestinationAddress2());
			mss.setShipperDestinationCity(workTicket.getDestinationCity());
			mss.setShipperDestinationState(workTicket.getDestinationState());
			mss.setShipperDestinationZip(workTicket.getDestinationZip());
			mss.setMssOrderOriginPhoneNumbers(workTicket.getHomePhone()+ workTicket.getOriginMobile());
			mss.setMssOrderDestinationPhoneNumbers(workTicket.getDestinationHomePhone() + workTicket.getDestinationMobile());
			
			mssItemList=mssManager.findRecord(mss.getId(), sessionCorpID);
			rowCountvalue=mssItemList.size();
			notess = notesManager.getListForMss(mss.getId(), subType);
			mssOriServList=mssManager.findAllOriServiceRecord(mss.getId(), sessionCorpID);
			mssDestinationServList=mssManager.findAllDestinationServiceList(mss.getId(), sessionCorpID);
		} else {
			mss = new Mss(); 
			mss.setCreatedOn(new Date());
			mss.setUpdatedOn(new Date());
			//mss.setBillingDivision(15);			
			//mss.setSubmitAs(workTicket.getCreatedBy());
			mss.setLoadingStartDate(trackingStatus.getBeginLoad());
			mss.setPackingStartDate(trackingStatus.getBeginPacking());
			mss.setDeliveryStartDate(trackingStatus.getDeliveryShipper());
			if (miscellaneous.getEstimatedNetWeight() == null) {
				mss.setWeight(0);
			} else {
				mss.setWeight(miscellaneous.getEstimatedNetWeight().intValue());
			}			
			mss.setVip(customerFile.getVip());
			mss.setPurchaseOrderNumber(serviceOrder.getShipNumber());
			mss.setSoNumber(serviceOrder.getSequenceNumber());
			mss.setShipperFirstName(serviceOrder.getFirstName());
			mss.setShipperLastName(serviceOrder.getLastName());
			mss.setShipperEmailAddress(serviceOrder.getEmail());
			mss.setCompanyDivision(serviceOrder.getCompanyDivision());
			mss.setShipperOriginAddress1(workTicket.getAddress1());
			mss.setShipperOriginAddress2(workTicket.getAddress2());
			mss.setShipperOriginCity(workTicket.getCity());
			mss.setShipperOriginState(workTicket.getState());
			mss.setShipperOriginZip(workTicket.getZip());
			mss.setShipperDestinationAddress1(workTicket.getDestinationAddress1());
			mss.setShipperDestinationAddress2(workTicket.getDestinationAddress2());
			mss.setShipperDestinationCity(workTicket.getDestinationCity());
			mss.setShipperDestinationState(workTicket.getDestinationState());
			mss.setShipperDestinationZip(workTicket.getDestinationZip());
			mss.setMssOrderOriginPhoneNumbers(workTicket.getHomePhone()+ workTicket.getOriginMobile());
			mss.setMssOrderDestinationPhoneNumbers(workTicket.getDestinationHomePhone() + workTicket.getDestinationMobile());
			List defaultsub=mssManager.getdefaultSubmitAs(getRequest().getRemoteUser(),sessionCorpID);
			if(defaultsub!=null && !defaultsub.isEmpty() && defaultsub.get(0)!=null){
				mss.setSubmitAs(defaultsub.get(0).toString());
			}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	 private String mssURL;
	 
	 @SkipValidation
	 public String updateMssCratingGridAjax(){
		 MssGrid mssGrid = mssGridManager.get(id);
		 mssGrid.setCod(Boolean.parseBoolean(codListServer));
		 mssGrid.setAppr(Boolean.parseBoolean(apprListServer));
		 mssGrid.setTransportType(transportListServer);
		 mssGrid.setDescription(descriptionListServer);
		 mssGrid.setLength(Double.parseDouble(lengthListServer));
		 mssGrid.setHeight(Double.parseDouble(heightListServer));
		 mssGrid.setWidth(Double.parseDouble(widthListServer));
		 mssGrid.setPly(Boolean.parseBoolean(plyListServer));
		 mssGrid.setIntlIspmis(Boolean.parseBoolean(intlIspmisListServer));
		 
		 mssGridManager.save(mssGrid);
		 return SUCCESS;
	 }

	public String save() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		mssURL = "?wid="+wid;
		mss.setCorpId(sessionCorpID);
		boolean isNew = (mss.getId() == null);
		workTicket = workTicketManager.get(wid);
		serviceOrder=workTicket.getServiceOrder();
		//getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus=trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		mss.setWorkTicketId(wid);		
				if (isNew) { 
					mss.setCreatedOn(new Date());
					mss.setCreatedBy(getRequest().getRemoteUser());
		        }
				mss.setUpdatedOn(new Date());
				mss.setUpdatedBy(getRequest().getRemoteUser());
				mss=mssManager.save(mss);  
		    	String key = (isNew) ? "MSS has been added Sucessfully" : "MSS has been updated Sucessfully";
			    saveMessage(getText(key));
			    
				if(mssRowlist!=null && (!(mssRowlist.trim().equals("")))){
					String[] str = mssRowlist.split("~");
					for (String string : str) {
					String[] strRes=string.split(":");
					
					String tempTrans = strRes[0].toString().trim();
					String tempDes = strRes[1].toString().trim();
					String tempLen = strRes[2].toString().trim();
					String tempWid = strRes[3].toString().trim();
					String tempHei = strRes[4].toString().trim();
					Boolean tempPly = Boolean.parseBoolean(strRes[5].toString().trim());
					Boolean tempInt = Boolean.parseBoolean(strRes[6].toString().trim());
					Boolean tempApp = Boolean.parseBoolean(strRes[7].toString().trim());
					Boolean tempCod = Boolean.parseBoolean(strRes[8].toString().trim());
					
					mssGrid = new MssGrid();
					mssGrid.setCorpId(sessionCorpID);
					mssGrid.setTransportType(tempTrans);
					mssGrid.setDescription(tempDes);
					mssGrid.setLength(Double.parseDouble(tempLen));
					mssGrid.setWidth(Double.parseDouble(tempWid));
					mssGrid.setHeight(Double.parseDouble(tempHei));
					mssGrid.setPly(tempPly);
					mssGrid.setIntlIspmis(tempInt);
					mssGrid.setAppr(tempApp);
					mssGrid.setCod(tempCod);
					mssGrid.setMssId(mss.getId());
					mssGrid.setCreatedBy(getRequest().getRemoteUser());
					mssGrid.setCreatedOn(new Date());
					mssGrid.setUpdatedBy(getRequest().getRemoteUser());
					mssGrid.setUpdatedOn(new Date());
					mssGrid=mssGridManager.save(mssGrid);
				}
			}	
				try{
					if(transportListServer!=null && (!transportListServer.equalsIgnoreCase(""))){
						String [] temp = transportListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssGrid resObj = mssGridManager.get(tempResId);
							resObj.setTransportType(resArr[1].toString().trim());
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssGridManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				
				try{
					if(descriptionListServer!=null && (!descriptionListServer.equalsIgnoreCase(""))){
						String [] temp = descriptionListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssGrid resObj = mssGridManager.get(tempResId);
							resObj.setDescription(resArr[1].toString().trim());
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssGridManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				
				try{
					if(lengthListServer!=null && (!lengthListServer.equalsIgnoreCase(""))){
						String [] temp = lengthListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssGrid resObj = mssGridManager.get(tempResId);
							resObj.setLength(Double.parseDouble(resArr[1].toString().trim()));
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssGridManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				
				try{
					if(widthListServer!=null && (!widthListServer.equalsIgnoreCase(""))){
						String [] temp = widthListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssGrid resObj = mssGridManager.get(tempResId);
							resObj.setWidth(Double.parseDouble(resArr[1].toString().trim()));
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssGridManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				
				try{
					if(heightListServer!=null && (!heightListServer.equalsIgnoreCase(""))){
						String [] temp = heightListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssGrid resObj = mssGridManager.get(tempResId);
							resObj.setHeight(Double.parseDouble(resArr[1].toString().trim()));
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssGridManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				
				try{
					if(plyListServer!=null && (!plyListServer.equalsIgnoreCase(""))){
						String [] temp = plyListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssGrid resObj = mssGridManager.get(tempResId);
							resObj.setPly(Boolean.parseBoolean(resArr[1].toString().trim()));
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssGridManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				
				try{
					if(intlIspmisListServer!=null && (!intlIspmisListServer.equalsIgnoreCase(""))){
						String [] temp = intlIspmisListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssGrid resObj = mssGridManager.get(tempResId);
							resObj.setIntlIspmis(Boolean.parseBoolean(resArr[1].toString().trim()));
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssGridManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				
				try{
					if(apprListServer!=null && (!apprListServer.equalsIgnoreCase(""))){
						String [] temp = apprListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssGrid resObj = mssGridManager.get(tempResId);
							resObj.setAppr(Boolean.parseBoolean(resArr[1].toString().trim()));
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssGridManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				
				try{
					if(codListServer!=null && (!codListServer.equalsIgnoreCase(""))){
						String [] temp = codListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssGrid resObj = mssGridManager.get(tempResId);
							resObj.setCod(Boolean.parseBoolean(resArr[1].toString().trim()));
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssGridManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				
				if(oriServicelist!=null && (!(oriServicelist.trim().equals("")))){
					String[] str = oriServicelist.split("~");
					for (String string : str) {
					String[] strRes=string.split(":");
					
					String tempItems = strRes[0].toString().trim();					
					Boolean tempApp = (Boolean.parseBoolean(strRes[1].toString().trim()));
					Boolean tempCod = (Boolean.parseBoolean(strRes[2].toString().trim()));
					
					mssOriginService = new MssOriginService();
					mssOriginService.setCorpId(sessionCorpID);
					mssOriginService.setOriItems(tempItems);					
					mssOriginService.setMssId(mss.getId());
					mssOriginService.setOriAppr(tempApp);
					mssOriginService.setOriCod(tempCod);
					mssOriginService.setCreatedBy(getRequest().getRemoteUser());
					mssOriginService.setCreatedOn(new Date());
					mssOriginService.setUpdatedBy(getRequest().getRemoteUser());
					mssOriginService.setUpdatedOn(new Date());
					mssOriginService=mssOriginServiceManager.save(mssOriginService);
				}
			}	
				try{
					if(oriItemListServer!=null && (!oriItemListServer.equalsIgnoreCase(""))){
						String [] temp = oriItemListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssOriginService resObj = mssOriginServiceManager.get(tempResId);
							resObj.setOriItems(resArr[1].toString().trim());
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssOriginServiceManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				try{
					if(oriApprListServer!=null && (!oriApprListServer.equalsIgnoreCase(""))){
						String [] temp = oriApprListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssOriginService resObj = mssOriginServiceManager.get(tempResId);
							resObj.setOriAppr(Boolean.parseBoolean(resArr[1].toString().trim()));
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssOriginServiceManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				try{
					if(oriCodListServer!=null && (!oriCodListServer.equalsIgnoreCase(""))){
						String [] temp = oriCodListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssOriginService resObj = mssOriginServiceManager.get(tempResId);
							resObj.setOriCod(Boolean.parseBoolean(resArr[1].toString().trim()));
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssOriginServiceManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				
				if(desServicelist!=null && (!(desServicelist.trim().equals("")))){
					String[] str = desServicelist.split("~");
					for (String string : str) {
					String[] strRes=string.split(":");
					
					String tempItems = strRes[0].toString().trim();					
					Boolean tempApp = (Boolean.parseBoolean(strRes[1].toString().trim()));
					Boolean tempCod = (Boolean.parseBoolean(strRes[2].toString().trim()));
					
					mssDestinationService = new MssDestinationService();
					mssDestinationService.setCorpId(sessionCorpID);
					mssDestinationService.setDestinationItems(tempItems);					
					mssDestinationService.setMssId(mss.getId());
					mssDestinationService.setDestinationAppr(tempApp);
					mssDestinationService.setDestinationCod(tempCod);
					mssDestinationService.setCreatedBy(getRequest().getRemoteUser());
					mssDestinationService.setCreatedOn(new Date());
					mssDestinationService.setUpdatedBy(getRequest().getRemoteUser());
					mssDestinationService.setUpdatedOn(new Date());
					mssDestinationService=mssDestinationServiceManager.save(mssDestinationService);
				}
			}	
				try{
					if(destItemListServer!=null && (!destItemListServer.equalsIgnoreCase(""))){
						String [] temp = destItemListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssDestinationService resObj = mssDestinationServiceManager.get(tempResId);
							resObj.setDestinationItems(resArr[1].toString().trim());
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssDestinationServiceManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				try{
					if(oriApprListServer!=null && (!oriApprListServer.equalsIgnoreCase(""))){
						String [] temp = oriApprListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssDestinationService resObj = mssDestinationServiceManager.get(tempResId);
							resObj.setDestinationAppr(Boolean.parseBoolean(resArr[1].toString().trim()));
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssDestinationServiceManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				try{
					if(oriCodListServer!=null && (!oriCodListServer.equalsIgnoreCase(""))){
						String [] temp = oriCodListServer.split("~");
						for(String res : temp){
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							MssDestinationService resObj = mssDestinationServiceManager.get(tempResId);
							resObj.setDestinationCod(Boolean.parseBoolean(resArr[1].toString().trim()));
							resObj.setUpdatedBy(getRequest().getRemoteUser());
							resObj.setUpdatedOn(new Date());
							mssDestinationServiceManager.save(resObj);
						}
					}
				}catch(Exception e){					
				}
				mssManager.updateRefmaster(mss.getSubmitAs(),getRequest().getRemoteUser(),sessionCorpID);
				mssDestinationServList=mssManager.findAllDestinationServiceList(mss.getId(), sessionCorpID);
				mssOriServList=mssManager.findAllOriServiceRecord(mss.getId(), sessionCorpID);
			    mssItemList=mssManager.findRecord(mss.getId(), sessionCorpID);
			    rowCountvalue=mssItemList.size();
			    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
	     }
	
	public String list(){ 
		return SUCCESS;
	}
	
	private Long gId;
	public String delete(){		
		mssGridManager.remove(gId);
		mssURL = "?wid="+wid;
  	return SUCCESS;		    
	}
	
	private String parentCode;
	public String getComboList(String corpId){
		workTicket = workTicketManager.get(wid);
		serviceOrder=workTicket.getServiceOrder();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		submitAs = refMasterManager.findByParameter(corpId, "SUBMITAS",serviceOrder.getCompanyDivision());
		billToDivision = refMasterManager.findByParameter(corpId, "BILLTODIVISION",serviceOrder.getCompanyDivision()); 
		affiliatedTo = refMasterManager.findByParameter(corpId, "AFFILIATEDAS",serviceOrder.getCompanyDivision());
		originCat = mssManager.findParentRecord(corpId, "MSS_SERVICE_CAT");
		//originCatSubtype = mssManager.findChildRecord(corpId, "MSS_ORIGIN_CAT",parentCode);
		destinationCat = mssManager.findDestinationParentRecord(corpId, "MSS_SERVICE_CAT");
		//submitAs.put(workTicket.getCreatedBy(), workTicket.getCreatedBy());
		//billToDivision.put("15", "My Company");
		//destinationCatSubtype = refMasterManager.findByParameter(corpId, "MSS_Destination_CAT_SUBTYPE ");
		transportTypeValue = new ArrayList();
		transportTypeValue.add("O");
		transportTypeValue.add("D");
		transportTypeValue.add("B");
		
		transportTypeRadio = new HashMap();
		//transportTypeRadio = new ArrayList();
		transportTypeRadio.put("O","Origin");
		transportTypeRadio.put("D","Destination");
		transportTypeRadio.put("B","Both");
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String findChildRecord(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		originCatSubtype = mssManager.findChildRecord(sessionCorpID , "MSS_ORIGIN_CAT",parentCode);	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	private String countPayNotes;
	private NotesManager notesManager;
	@SkipValidation
	public String getNotesForIconChange() {
		List noteMss =  notesManager.countForMssNotes(mss.getId());
		
		if (noteMss.isEmpty()) {
			countPayNotes = "0";
		} else {
			countPayNotes = ((noteMss).get(0)).toString();
		}
		return SUCCESS;
	}
	private Long oId;
	public String deleteOriServiceRow(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		mssOriginServiceManager.remove(oId);
		mssURL = "?wid="+wid;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
  	return SUCCESS;		    
	}
	
	@SkipValidation
	public String findDestinationChildRecord(){	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		destinationCatSubtype = mssManager.findDestinationChildRecord(sessionCorpID , "MSS_Destination_CAT",parentCode);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	private Long dId;
	public String deleteDestServiceRow(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		mssDestinationServiceManager.remove(dId);
		mssURL = "?wid="+wid;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
  	return SUCCESS;		    
	}
	private byte[] PdfData;
	private List quotePDFValues;
	private List cratingList;
	private List originList;
	private List destinationList;
	private List originNotesList;
	private List destinationNotesList;
	private List originPhoneList;
	private List destinationPhoneList;
	private List customerContact;
	String customerID="";
	String password="";

	@SkipValidation
	public String getValueForQuotePDF() throws Exception{
		try {
			String uploadDir="";
			String fileName="";
			/*if(env!=null && !env.equals("PROD") ){*/	
			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "MssDoc" + "/");
			
			File newFile = new File(uploadDir);
			if (!newFile.exists()) {
				newFile.mkdirs();
			}
			 String filePath="";
			 filePath = (uploadDir);
			 mss=mssManager.get(id);
			 List companyList=mssManager.getMsscustomerID(mss.getCompanyDivision(),sessionCorpID);
			 if(companyList!=null && !companyList.isEmpty()){
				 Iterator it = companyList.iterator();
					while (it.hasNext()) {
					Object[] abc=(Object[])it.next();
						 customerID=abc[0].toString();
						 password=abc[1].toString();
					}
			 }
			 if(mss.getTransportTypeRadio().equalsIgnoreCase("O")){
				quotePDFValues = mssManager.findValuesForQuotePDF(mss.getId(),mss.getTransportTypeRadio(),mss.getPurchaseOrderNumber(),mss.getWorkTicketId(),sessionCorpID);
				cratingList=mssManager.findCratingValue(mss.getId(),sessionCorpID);
				originList=mssManager.findOriginValue(mss.getId(),sessionCorpID);
				originNotesList=mssManager.getMssNotes(mss.getId(),mss.getPurchaseOrderNumber(),"O",sessionCorpID);
				originPhoneList=mssManager.getOriginPhone(mss.getWorkTicketId(),mss.getPurchaseOrderNumber(),sessionCorpID);
			 }else if(mss.getTransportTypeRadio().equalsIgnoreCase("D")){
				 quotePDFValues = mssManager.findValuesForQuotePDF(mss.getId(),mss.getTransportTypeRadio(),mss.getPurchaseOrderNumber(),mss.getWorkTicketId(),sessionCorpID);
				 cratingList=mssManager.findCratingValue(mss.getId(),sessionCorpID);
				 destinationList=mssManager.findDestinationValue(mss.getId(),sessionCorpID); 
				 destinationNotesList=mssManager.getMssNotes(mss.getId(),mss.getPurchaseOrderNumber(),"D",sessionCorpID);
				 destinationPhoneList=mssManager.getdestinationPhone(mss.getWorkTicketId(),mss.getPurchaseOrderNumber(),sessionCorpID);
			 }else{
				 quotePDFValues = mssManager.findValuesForQuotePDF(mss.getId(),mss.getTransportTypeRadio(),mss.getPurchaseOrderNumber(),mss.getWorkTicketId(),sessionCorpID);
				 cratingList=mssManager.findCratingValue(mss.getId(),sessionCorpID);
				 originList=mssManager.findOriginValue(mss.getId(),sessionCorpID);
				 destinationList=mssManager.findDestinationValue(mss.getId(),sessionCorpID); 
				 originNotesList=mssManager.getMssNotes(mss.getId(),mss.getPurchaseOrderNumber(),"O",sessionCorpID);
				 destinationNotesList=mssManager.getMssNotes(mss.getId(),mss.getPurchaseOrderNumber(),"D",sessionCorpID);
				 originPhoneList=mssManager.getOriginPhone(mss.getWorkTicketId(),mss.getPurchaseOrderNumber(),sessionCorpID);
				 destinationPhoneList=mssManager.getdestinationPhone(mss.getWorkTicketId(),mss.getPurchaseOrderNumber(),sessionCorpID);
			 }
			 customerContact=mssManager.getSoDetails(mss.getPurchaseOrderNumber(),sessionCorpID);
			if(quotePDFValues!=null && !quotePDFValues.isEmpty() && quotePDFValues.get(0)!=null){
				MssXmlDto mssXmlDto= new MssXmlDto();
				try {
				Object mssobj=mssXmlDto.creatXMLRequest(quotePDFValues,cratingList,originList,destinationList,originNotesList,destinationNotesList,originPhoneList,destinationPhoneList,mss.getTransportTypeRadio(),customerContact);
				//ServiceMSSOrder ss = new ServiceMSSOrder(wsdlURL,SERVICE_NAME);
				//ServiceMSSOrder ss = new ServiceMSSOrder(wsdlURL,SERVICE_NAME);
				 // port = ss.getServiceMSSOrderSoap();
				// PdfData=port.getQuotePdf(customerID, password,(com.trilasoft.mss.webservices.MSSOrder)mssobj);
				PdfData=MSSServiceCallClient.callMSSService((MSSOrder)mssobj ,customerID , password);
				fileName=""+mss.getWorkTicketNumber()+"_"+mss.getCorpId()+"_"+"MSS";
				fileName = fileName+".pdf";
				filePath=filePath+fileName;
				 try {		
					 String validatePdfData = new String(PdfData);
					 if(PdfData!=null && !validatePdfData.startsWith("Invalid")){
						    OutputStream out = new FileOutputStream(filePath);
							//mssXmlDto.setGetQuotePDFProperty(customerID,password,mssobj);
							out.write(PdfData);
							out.flush();
							out.close();
							String key = "Successfully sent to MSS.";
							saveMessage(getText(key));
							mssManager.updatefilePath(mss.getId(),mss.getSoNumber(),mss.getWorkTicketNumber(),filePath,sessionCorpID); 
					 }else{
						 String key = "Failed to send to MSS. Please contact RedSky team if issue persists";
						 errorMessage(getText(validatePdfData)); 
					 }
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				} catch (JAXBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			 /*}else{
				 String key = "Failed to send to MSS. Please contact RedSky team if issue persists";
				 errorMessage(getText(key)); 
			 }*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		return SUCCESS;
	}
	private String placeOrderNumber;
	@SkipValidation
	public String placeOrderNumberValuesAjax(){
		try {
			mss=mssManager.get(id);
			 List companyList=mssManager.getMsscustomerID(mss.getCompanyDivision(),sessionCorpID);
			 if(companyList!=null && !companyList.isEmpty()){
				 Iterator it = companyList.iterator();
					while (it.hasNext()) {
					Object[] abc=(Object[])it.next();
						 customerID=abc[0].toString();
						 password=abc[1].toString();
					}
			 }
			 if(mss.getTransportTypeRadio().equalsIgnoreCase("O")){
				quotePDFValues = mssManager.findValuesForQuotePDF(mss.getId(),mss.getTransportTypeRadio(),mss.getPurchaseOrderNumber(),mss.getWorkTicketId(),sessionCorpID);
				cratingList=mssManager.findCratingValue(mss.getId(),sessionCorpID);
				originList=mssManager.findOriginValue(mss.getId(),sessionCorpID);
				originNotesList=mssManager.getMssNotes(mss.getId(),mss.getPurchaseOrderNumber(),"O",sessionCorpID);
				originPhoneList=mssManager.getOriginPhone(mss.getWorkTicketId(),mss.getPurchaseOrderNumber(),sessionCorpID);
			 }else if(mss.getTransportTypeRadio().equalsIgnoreCase("D")){
				 quotePDFValues = mssManager.findValuesForQuotePDF(mss.getId(),mss.getTransportTypeRadio(),mss.getPurchaseOrderNumber(),mss.getWorkTicketId(),sessionCorpID);
				 cratingList=mssManager.findCratingValue(mss.getId(),sessionCorpID);
				 destinationList=mssManager.findDestinationValue(mss.getId(),sessionCorpID); 
				 destinationNotesList=mssManager.getMssNotes(mss.getId(),mss.getPurchaseOrderNumber(),"D",sessionCorpID);
				 destinationPhoneList=mssManager.getdestinationPhone(mss.getWorkTicketId(),mss.getPurchaseOrderNumber(),sessionCorpID);
			 }else{
				 quotePDFValues = mssManager.findValuesForQuotePDF(mss.getId(),mss.getTransportTypeRadio(),mss.getPurchaseOrderNumber(),mss.getWorkTicketId(),sessionCorpID);
				 cratingList=mssManager.findCratingValue(mss.getId(),sessionCorpID);
				 originList=mssManager.findOriginValue(mss.getId(),sessionCorpID);
				 destinationList=mssManager.findDestinationValue(mss.getId(),sessionCorpID); 
				 originNotesList=mssManager.getMssNotes(mss.getId(),mss.getPurchaseOrderNumber(),"O",sessionCorpID);
				 destinationNotesList=mssManager.getMssNotes(mss.getId(),mss.getPurchaseOrderNumber(),"D",sessionCorpID);
				 originPhoneList=mssManager.getOriginPhone(mss.getWorkTicketId(),mss.getPurchaseOrderNumber(),sessionCorpID);
				 destinationPhoneList=mssManager.getdestinationPhone(mss.getWorkTicketId(),mss.getPurchaseOrderNumber(),sessionCorpID);
			 }
			 customerContact=mssManager.getSoDetails(mss.getPurchaseOrderNumber(),sessionCorpID);
			 if(quotePDFValues!=null && !quotePDFValues.isEmpty() && quotePDFValues.get(0)!=null){
				 MssXmlDto mssXmlDto= new MssXmlDto();
				 try {
					Object mssobj=mssXmlDto.creatXMLRequest(quotePDFValues,cratingList,originList,destinationList,originNotesList,destinationNotesList,originPhoneList,destinationPhoneList,mss.getTransportTypeRadio(),customerContact);
				
					placeOrderNumber=MSSServiceCallClient.CallPlaceOrderService((MSSOrder)mssobj,customerID,password);
					 
				 } catch (JAXBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					if(placeOrderNumber!=null &&(!placeOrderNumber.equalsIgnoreCase("") && !placeOrderNumber.contains("@@@@"))){
						String key = "Successfully sent to MSS.";
						saveMessage(getText(key));
					mssManager.updateplaceOrderNumber(mss.getId(),mss.getSoNumber(),mss.getWorkTicketNumber(),placeOrderNumber,sessionCorpID);
					}else{
						String key = "Failed to send to MSS. Please contact RedSky team if issue persists.";
						errorMessage(getText(placeOrderNumber.replaceAll("@","")));
					}
				}
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String findImportFromVoxme(){
		workTicket = workTicketManager.get(wid);
		serviceOrder=workTicket.getServiceOrder();
		Long sid=serviceOrder.getId();
		customerFile=serviceOrder.getCustomerFile();
		Long cid=customerFile.getId();
		mss=mssManager.get(id);
		mssGrid=new MssGrid();
		if(inventoryFlag.equalsIgnoreCase("True")){
			mssGrid.setInventoryFlag(inventoryFlag);
			mssGridManager.save(mssGrid);
			inventoryFlag=mssGrid.getInventoryFlag();
			mssGridManager.deleteDataOfInventoryData(inventoryFlag);
		}
	   List inventoryDataList=inventoryDataManager.getInventoryItemForMss(sid, cid, sessionCorpID);
		if(inventoryDataList!=null && !(inventoryDataList.isEmpty()))
		{
			Iterator it1=inventoryDataList.iterator();
				while(it1.hasNext()){
					 	inventoryData=(InventoryData)it1.next();
					 	mssGrid.setTransportType(mss.getTransportTypeRadio());
					 	mssGrid.setCorpId(mss.getCorpId());
					 	mssGrid.setMssId(mss.getId());
					    mssGrid.setDescription(inventoryData.getAtricle());
						mssGrid.setHeight(inventoryData.getHeight());
						mssGrid.setLength(inventoryData.getLength());
						mssGrid.setWidth(inventoryData.getWidth());
						mssGrid.setCreatedOn(new Date());
						mssGrid.setCreatedBy(getRequest().getRemoteUser());
						mssGrid.setUpdatedBy(getRequest().getRemoteUser());
						mssGrid.setUpdatedOn(new Date());
						mssGridManager.save(mssGrid);
				}
				returnAjaxStringValue="y";
		}	
	    return SUCCESS;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Mss getMss() {
		return mss;
	}

	public void setMss(Mss mss) {
		this.mss = mss;
	}

	public WorkTicket getWorkTicket() {
		return workTicket;
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}

	public Long getId1() {
		return id1;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public void setMssManager(MssManager mssManager) {
		this.mssManager = mssManager;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getSubmitAs() {
		return submitAs;
	}

	public void setSubmitAs(Map<String, String> submitAs) {
		this.submitAs = submitAs;
	}

	public Map<String, String> getBillToDivision() {
		return billToDivision;
	}

	public void setBillToDivision(Map<String, String> billToDivision) {
		this.billToDivision = billToDivision;
	}

	public Map<String, String> getAffiliatedTo() {
		return affiliatedTo;
	}

	public void setAffiliatedTo(Map<String, String> affiliatedTo) {
		this.affiliatedTo = affiliatedTo;
	}

	public String getMssURL() {
		return mssURL;
	}

	public void setMssURL(String mssURL) {
		this.mssURL = mssURL;
	}

	public Long getWid() {
		return wid;
	}

	public void setWid(Long wid) {
		this.wid = wid;
	}

	public String getCountPayNotes() {
		return countPayNotes;
	}

	public void setCountPayNotes(String countPayNotes) {
		this.countPayNotes = countPayNotes;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public List getMssItemList() {
		return mssItemList;
	}

	public void setMssItemList(List mssItemList) {
		this.mssItemList = mssItemList;
	}

	public List getTransportTypeValue() {
		return transportTypeValue;
	}

	public void setTransportTypeValue(List transportTypeValue) {
		this.transportTypeValue = transportTypeValue;
	}

	public String getMssRowlist() {
		return mssRowlist;
	}

	public void setMssRowlist(String mssRowlist) {
		this.mssRowlist = mssRowlist;
	}

	public Map<String, String> getTransportTypeRadio() {
		return transportTypeRadio;
	}

	public void setTransportTypeRadio(Map<String, String> transportTypeRadio) {
		this.transportTypeRadio = transportTypeRadio;
	}

	public MssGrid getMssGrid() {
		return mssGrid;
	}

	public void setMssGrid(MssGrid mssGrid) {
		this.mssGrid = mssGrid;
	}

	public String getTransportListServer() {
		return transportListServer;
	}

	public void setTransportListServer(String transportListServer) {
		this.transportListServer = transportListServer;
	}

	public String getDescriptionListServer() {
		return descriptionListServer;
	}

	public void setDescriptionListServer(String descriptionListServer) {
		this.descriptionListServer = descriptionListServer;
	}

	public String getLengthListServer() {
		return lengthListServer;
	}

	public void setLengthListServer(String lengthListServer) {
		this.lengthListServer = lengthListServer;
	}

	public String getWidthListServer() {
		return widthListServer;
	}

	public void setWidthListServer(String widthListServer) {
		this.widthListServer = widthListServer;
	}

	public String getHeightListServer() {
		return heightListServer;
	}

	public void setHeightListServer(String heightListServer) {
		this.heightListServer = heightListServer;
	}

	public String getPlyListServer() {
		return plyListServer;
	}

	public void setPlyListServer(String plyListServer) {
		this.plyListServer = plyListServer;
	}

	public String getIntlIspmisListServer() {
		return intlIspmisListServer;
	}

	public void setIntlIspmisListServer(String intlIspmisListServer) {
		this.intlIspmisListServer = intlIspmisListServer;
	}

	public String getApprListServer() {
		return apprListServer;
	}

	public void setApprListServer(String apprListServer) {
		this.apprListServer = apprListServer;
	}

	public String getCodListServer() {
		return codListServer;
	}

	public void setCodListServer(String codListServer) {
		this.codListServer = codListServer;
	}

	public void setMssGridManager(MssGridManager mssGridManager) {
		this.mssGridManager = mssGridManager;
	}

	public Long getgId() {
		return gId;
	}

	public void setgId(Long gId) {
		this.gId = gId;
	}

	public List getNotess() {
		return notess;
	}

	public void setNotess(List notess) {
		this.notess = notess;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getNoteFor() {
		return noteFor;
	}

	public void setNoteFor(String noteFor) {
		this.noteFor = noteFor;
	}

	public Map<String, String> getOriginCat() {
		return originCat;
	}

	public void setOriginCat(Map<String, String> originCat) {
		this.originCat = originCat;
	}

	public Map<String, String> getOriginCatSubtype() {
		return originCatSubtype;
	}

	public void setOriginCatSubtype(Map<String, String> originCatSubtype) {
		this.originCatSubtype = originCatSubtype;
	}

	public Map<String, String> getDestinationCat() {
		return destinationCat;
	}

	public void setDestinationCat(Map<String, String> destinationCat) {
		this.destinationCat = destinationCat;
	}

	public Map<String, String> getDestinationCatSubtype() {
		return destinationCatSubtype;
	}

	public void setDestinationCatSubtype(Map<String, String> destinationCatSubtype) {
		this.destinationCatSubtype = destinationCatSubtype;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public List getMssOriServList() {
		return mssOriServList;
	}

	public void setMssOriServList(List mssOriServList) {
		this.mssOriServList = mssOriServList;
	}

	public String getOriServicelist() {
		return oriServicelist;
	}

	public void setOriServicelist(String oriServicelist) {
		this.oriServicelist = oriServicelist;
	}

	public MssOriginService getMssOriginService() {
		return mssOriginService;
	}

	public void setMssOriginService(MssOriginService mssOriginService) {
		this.mssOriginService = mssOriginService;
	}

	public void setMssOriginServiceManager(
			MssOriginServiceManager mssOriginServiceManager) {
		this.mssOriginServiceManager = mssOriginServiceManager;
	}

	public String getOriItemListServer() {
		return oriItemListServer;
	}

	public void setOriItemListServer(String oriItemListServer) {
		this.oriItemListServer = oriItemListServer;
	}

	public String getOriApprListServer() {
		return oriApprListServer;
	}

	public void setOriApprListServer(String oriApprListServer) {
		this.oriApprListServer = oriApprListServer;
	}

	public Long getoId() {
		return oId;
	}

	public void setoId(Long oId) {
		this.oId = oId;
	}

	public List getMssDestinationServList() {
		return mssDestinationServList;
	}

	public void setMssDestinationServList(List mssDestinationServList) {
		this.mssDestinationServList = mssDestinationServList;
	}

	public String getDestItemListServer() {
		return destItemListServer;
	}

	public void setDestItemListServer(String destItemListServer) {
		this.destItemListServer = destItemListServer;
	}

	public String getDestApprListServer() {
		return destApprListServer;
	}

	public void setDestApprListServer(String destApprListServer) {
		this.destApprListServer = destApprListServer;
	}

	public String getDestCodListServer() {
		return destCodListServer;
	}

	public void setDestCodListServer(String destCodListServer) {
		this.destCodListServer = destCodListServer;
	}

	public String getDesServicelist() {
		return desServicelist;
	}

	public void setDesServicelist(String desServicelist) {
		this.desServicelist = desServicelist;
	}

	public MssDestinationService getMssDestinationService() {
		return mssDestinationService;
	}

	public void setMssDestinationService(MssDestinationService mssDestinationService) {
		this.mssDestinationService = mssDestinationService;
	}

	public String getOriCodListServer() {
		return oriCodListServer;
	}

	public void setOriCodListServer(String oriCodListServer) {
		this.oriCodListServer = oriCodListServer;
	}

	public void setMssDestinationServiceManager(
			MssDestinationServiceManager mssDestinationServiceManager) {
		this.mssDestinationServiceManager = mssDestinationServiceManager;
	}

	public Long getdId() {
		return dId;
	}

	public void setdId(Long dId) {
		this.dId = dId;
	}

	/*public GetCustomerBillTos getGetCustomerBillTos() {
		return getCustomerBillTos;
	}

	public void setGetCustomerBillTos(GetCustomerBillTos getCustomerBillTos) {
		this.getCustomerBillTos = getCustomerBillTos;
	}*/

	public List getQuotePDFValues() {
		return quotePDFValues;
	}

	public void setQuotePDFValues(List quotePDFValues) {
		this.quotePDFValues = quotePDFValues;
	}

	public int getRowCountvalue() {
		return rowCountvalue;
	}

	public void setRowCountvalue(int rowCountvalue) {
		this.rowCountvalue = rowCountvalue;
	}

	public void setInventoryDataManager(InventoryDataManager inventoryDataManager) {
		this.inventoryDataManager = inventoryDataManager;
	}

	public InventoryData getInventoryData() {
		return inventoryData;
	}

	public void setInventoryData(InventoryData inventoryData) {
		this.inventoryData = inventoryData;
	}
	public String getInventoryFlag() {
		return inventoryFlag;
	}

	public void setInventoryFlag(String inventoryFlag) {
		this.inventoryFlag = inventoryFlag;
	}

	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}

	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}
}
