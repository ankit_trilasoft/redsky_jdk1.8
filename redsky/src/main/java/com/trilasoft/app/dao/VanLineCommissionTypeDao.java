package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.VanLineCommissionType;
import com.trilasoft.app.model.VanLineGLType;

public interface VanLineCommissionTypeDao extends GenericDao<VanLineCommissionType, Long> {
	
	public List<VanLineCommissionType> vanLineCommissionTypeList();
	
	public List<VanLineCommissionType> searchVanLineCommissionType(String code, String type, String description, String glCode);
	
	public List isExisted(String code, String corpId);
	
	public List<VanLineCommissionType> getGLCodePopupList(String chargesGl);
	
	public List glList(String chargesGl);
	public List searchVanLineCommissionPopup(String code, String type, String description, String glCode);
	
}