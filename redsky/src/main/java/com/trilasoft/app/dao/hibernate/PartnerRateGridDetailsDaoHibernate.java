package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.PartnerRateGridDao;
import com.trilasoft.app.dao.PartnerRateGridDetailsDao;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerRateGrid;
import com.trilasoft.app.model.PartnerRateGridDetails;

public class PartnerRateGridDetailsDaoHibernate extends GenericDaoHibernate<PartnerRateGridDetails, Long> implements PartnerRateGridDetailsDao{ 
	public PartnerRateGridDetailsDaoHibernate() {   
        super(PartnerRateGridDetails.class);   
    }

	public List getRateGridDetais(Long id, String sessionCorpID) {
		return getHibernateTemplate().find("from PartnerRateGridDetails where partnerRateGridID='"+id+"' order by sortRateGrid");
	}

	public int updateRateGridData(Long id, String sessionCorpID, HashMap saveDataDetailsMap) {
		Iterator mapIterator = saveDataDetailsMap.entrySet().iterator();
	     while (mapIterator.hasNext()) {
	    	  Map.Entry entry = (Map.Entry) mapIterator.next();
	          String key = (String) entry.getKey();
	          String[] str1 = key.toString().split("_"); 
	          String grid=str1[0];
	          String gridSection=str1[1];
	          String parameter=str1[2];
	          String display="";
	          try
	          {
	        	  display = (String)saveDataDetailsMap.get(key); 
	        	  if(display.toString().equals("")){
		        	  display="0.00" ;
		          }
	          }
	          catch(Exception ex)
	          {
	        	  display="0.00" ;
	          }
	          
	          getHibernateTemplate().bulkUpdate("update PartnerRateGridDetails set value='" +display+ "' where grid='"+grid+"' and gridSection='"+gridSection+"' and parameter='"+parameter+"' and partnerRateGridID='"+id+"'  "); 
	}
	     return 0;
	}

	public List checkPartnerRateGridId(Long id, String sessionCorpID) {
		
		return getHibernateTemplate().find("select partnerRateGridID from PartnerRateGridDetails where partnerRateGridID='"+id+"' ");
	}

	public int updateRateGridBasis(Long id, String sessionCorpID, HashMap saveBasisMap) { 
		Iterator mapIterator = saveBasisMap.entrySet().iterator();
	     while (mapIterator.hasNext()) {
	    	  Map.Entry entry = (Map.Entry) mapIterator.next();
	          String key = (String) entry.getKey();
	          String[] str1 = key.toString().split("_"); 
	          String gridSection=str1[0];
	          String parameter=str1[1]; 
	          String basis= (String)saveBasisMap.get(key); 
	          if(basis!=null){
	          try
	          {
	          if(basis.toString().equals("")|| basis.toString().equals("blank") ){
	        	  basis= "";  
	          }
	          }catch(Exception ex)
	          {
	        	  basis= "";  
	          }
	          getHibernateTemplate().bulkUpdate("update PartnerRateGridDetails set basis='" +basis+ "' where gridSection='"+gridSection+"' and parameter='"+parameter+"' and partnerRateGridID='"+id+"'  "); 
	          }
	  }
	     return 0;
	
	}

	public int updateRateGridLabel(Long id, String sessionCorpID, HashMap saveLabelMap) {
		 
		Iterator mapIterator = saveLabelMap.entrySet().iterator();
	     while (mapIterator.hasNext()) {
	    	  Map.Entry entry = (Map.Entry) mapIterator.next();
	          String key = (String) entry.getKey();
	          String[] str1 = key.toString().split("_"); 
	          String gridSection=str1[0];
	          String parameter=str1[1];
	          String label= (String)saveLabelMap.get(key); 
	          if(label!=null){
	          getHibernateTemplate().bulkUpdate("update PartnerRateGridDetails set label='" +label+ "' where gridSection='"+gridSection+"' and parameter='"+parameter+"' and partnerRateGridID='"+id+"'  "); 
	          }
	 }
	     return 0;
	
	
	}

	public List getRateGridDetaisData(long id, String sessionCorpID) {
		List <Object> dataList = new ArrayList<Object>();
		List rateGridList = this.getSession().createSQLQuery("select id,partnerRateGridID,corpID,grid,gridSection,parameter,label,basis,weight1,weight2,value,sortRateGrid,sortGrid,editable from partnerrategriddetails where partnerRateGridID='"+id+"'  order by sortRateGrid").list();
		PartnerRateGridDetails partnerRateGridDetails;
		Iterator it=rateGridList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			partnerRateGridDetails=new PartnerRateGridDetails();
			partnerRateGridDetails.setId(new Long ((new BigInteger( obj[0].toString())).toString()));
			partnerRateGridDetails.setPartnerRateGridID(new Long ((new BigInteger( obj[1].toString())).toString()));
			partnerRateGridDetails.setCorpID((String) obj[2]);
			partnerRateGridDetails.setGrid((String) obj[3]);
			partnerRateGridDetails.setGridSection((String) obj[4]);
			partnerRateGridDetails.setParameter((String) obj[5]);
			partnerRateGridDetails.setLabel((String) obj[6]);
			partnerRateGridDetails.setBasis((String) obj[7]);
			partnerRateGridDetails.setWeight1((BigDecimal)obj[8]);
			partnerRateGridDetails.setWeight2((BigDecimal)obj[9]);
			partnerRateGridDetails.setValue((BigDecimal) obj[10]);
			partnerRateGridDetails.setSortRateGrid(new Long ((new BigInteger( obj[11].toString())).toString()));
			partnerRateGridDetails.setSortGrid(new Long ((new BigInteger( obj[12].toString())).toString()));
			partnerRateGridDetails.setEditable((String) obj[13]);
			dataList.add(partnerRateGridDetails);
		}
		return dataList;
	} 
}
 