package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PricingControlDao;
import com.trilasoft.app.model.PricingControl;
import com.trilasoft.app.service.PricingControlManager;

public class PricingControlManagerImpl extends GenericManagerImpl<PricingControl, Long> implements PricingControlManager {

	PricingControlDao pricingControlDao;

	public PricingControlManagerImpl(PricingControlDao pricingControlDao) {
		super(pricingControlDao);
		this.pricingControlDao=pricingControlDao;
	}

	public List checkById(Long id) {
		return null;
	}

	public List findPricingId(String code) {
		return pricingControlDao.findPricingId(code);
	}

	public List maxPricingIdCode(String sessionCorpID) {
		return pricingControlDao.maxPricingIdCode(sessionCorpID);
	}

	public List getAddress(Long serviceOrderId, String tariffApplicability) {
		return pricingControlDao.getAddress(serviceOrderId, tariffApplicability);
	}

	public List getList(String shipNumber) {
		return pricingControlDao.getList(shipNumber);
	}

	public List search(String pricingID, String shipNumber, String tariffApplicability, String packing, String country, String customerName, String parentAgent) {
		return pricingControlDao.search(pricingID, shipNumber, tariffApplicability, packing, country, customerName, parentAgent);
	}

	public String locateAgents(BigDecimal weight, String latitude, String longitude, String tariffApplicability, String country, String packing, String poe, String fidi, String omni, Double minimumFeedbackRating, String faim, String iso9002, String iso27001, String iso1400, String rim, String dos, String gsa, String military, BigDecimal volume) {
		return pricingControlDao.locateAgents(weight, latitude, longitude, tariffApplicability, country, packing, poe, fidi, omni, minimumFeedbackRating, faim, iso9002, iso27001, iso1400, rim, dos, gsa, military,  volume);
	}

	public List searchAgents(BigDecimal weight, String destinationLatitude, String destinationLongitude, String tarrifApplicability, String destinationCountry, String packing, String destinationPOE, String sessionCorpID, Long pricingControlID, Date createdOn, String createdBy, String updatedBy, Date updatedOn, Date expectedLoadDate, BigDecimal volume, String baseCurrency, String contract, boolean applyCompletemarkUp, String prefferedPortCode) {
		return pricingControlDao.searchAgents(weight, destinationLatitude, destinationLongitude, tarrifApplicability, destinationCountry, packing, destinationPOE, sessionCorpID, pricingControlID, createdOn,  createdBy,  updatedBy,  updatedOn,  expectedLoadDate, volume, baseCurrency, contract, applyCompletemarkUp, prefferedPortCode);
	}

	public String locateAgents1(Long id, String tarrifApplicability, String fidi, String omni, Double minimumFeedbackRating, String faim, String iso9002, String iso27001, String iso1400, String rim, String dos, String gsa, String military) {
		return pricingControlDao.locateAgents1(id, tarrifApplicability, fidi,  omni,  minimumFeedbackRating,  faim,  iso9002,  iso27001,  iso1400,  rim,  dos,  gsa,  military);
	}

	public void deletePricingDetails(Long id, String tarrifApplicability) {
		pricingControlDao.deletePricingDetails(id, tarrifApplicability)	;
	}

	public void updateSelectedResult(Long pricingDetailsId, Long pricingControlID, String tarrifApplicability) {
		pricingControlDao.updateSelectedResult(pricingDetailsId,  pricingControlID,  tarrifApplicability);
		
	}

	public int getCount(Long pricingControlID) {
		return pricingControlDao.getCount(pricingControlID);
	}

	public String getPorts(String countryCode, String latitude, String longitude, String mode) {
		return pricingControlDao.getPorts(countryCode, latitude, longitude, mode);
	}

	public List getContract(String sessionCorpID) {
		return pricingControlDao.getContract(sessionCorpID);
	}

	public String locateFreightDetails(String originPOE, String destinationPOE, String containerSize, String sessionCorpID) {
		return pricingControlDao.locateFreightDetails(originPOE, destinationPOE, containerSize, sessionCorpID);
	}

	public void updateFreightResult(Long freightId, Long pricingControlID, String tarrifApplicability, String freightCurrency, String baseCurrency, String mode, String contract, String freightPrice) {
		pricingControlDao.updateFreightResult(freightId, pricingControlID, tarrifApplicability, freightCurrency, baseCurrency, mode, contract, freightPrice);
	}

	public List getCurrency(String sessionCorpID) {
		return pricingControlDao.getCurrency(sessionCorpID);
	}

	public List getAcceptedQuotes(String sessionCorpID, String parentAgent) {
		return pricingControlDao.getAcceptedQuotes(sessionCorpID, parentAgent);
	}

	public List getAgentList(String parentAgent, String sessionCorpID) {
		return pricingControlDao.getAgentList(parentAgent, sessionCorpID);
	}

	public List getPortLocation(String originPOE, String sessionCorpID) {
		return pricingControlDao.getPortLocation(originPOE, sessionCorpID);
	}

	public void updateServiceOrder(Long id, String shipNumber, String sessionCorpID) {
		pricingControlDao.updateServiceOrder(id, shipNumber, sessionCorpID);
	}

	public List findRateDeptEmailAddress(String sessionCorpID) {
		return pricingControlDao.findRateDeptEmailAddress(sessionCorpID);
	}

	public List getRecentTariffs(String sessionCorpID) {
		return pricingControlDao.getRecentTariffs(sessionCorpID);
	}

	public void updateCustomerFileNumber(Long id, String sequenceNumber, String shipNumber, Long serviceOrderId, String sessionCorpID) {
		pricingControlDao.updateCustomerFileNumber(id, sequenceNumber, shipNumber, serviceOrderId, sessionCorpID);
	}

	public void updateDTHCFlag(Long pricingControlID, String tarrifApplicability, Boolean dthcFlag) {
		pricingControlDao.updateDTHCFlag(pricingControlID, tarrifApplicability, dthcFlag);
	}

	public List getPricingControId(String shipnumber, String sessionCorpID) {
		return pricingControlDao.getPricingControId(shipnumber, sessionCorpID);
	}

	public List checkControlFlag(String sequenceNumber, String sessionCorpID) {
		return pricingControlDao.checkControlFlag(sequenceNumber, sessionCorpID);
	}

	public List searchRecentTariffs(String sessionCorpID, String let) {
		return pricingControlDao.searchRecentTariffs(sessionCorpID, let);
	}

	public boolean checkMarkUp(String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude, String tarrifApplicability, String originCountry, String destinationCountry, String packing, String originPOE, String destinationPOE, String sessionCorpID, Date expectedLoadDate, String contract) {
		return pricingControlDao.checkMarkUp(originLatitude, originLongitude, destinationLatitude, destinationLongitude, tarrifApplicability, originCountry, destinationCountry, packing, originPOE, destinationPOE, sessionCorpID, expectedLoadDate, contract); 
	}

	public void updateFreightMarkUpFlag(Long pricingControlID, String tarrifApplicability, Boolean freightMarkUpFlag) {
		pricingControlDao.updateFreightMarkUpFlag(pricingControlID, tarrifApplicability, freightMarkUpFlag);
	}

	public void updatePricingControlPorts(Long id, String freightOriginPortCode, String freightDestinationPortCode, String sessionCorpID, String freightOriginPortCodeName, String freightDestinationPortCodeName) {
		pricingControlDao.updatePricingControlPorts(id, freightOriginPortCode, freightDestinationPortCode, sessionCorpID, freightOriginPortCodeName, freightDestinationPortCodeName);
	}

	public void deletePricingFreightDetails(Long pricingControlId) {
		pricingControlDao.deletePricingFreightDetails(pricingControlId)	;
	}

	public List getRecordList(Long pricingControlId, String sessionCorpID, String tarrifApplicability, String fidi, String omni, Double minimumFeedbackRating, String faim, String iso9002, String iso27001, String iso1400, String rim, String dos, String gsa, String military) {
		return pricingControlDao.getRecordList(pricingControlId, sessionCorpID, tarrifApplicability, fidi, omni,  minimumFeedbackRating, faim, iso9002, iso27001, iso1400, rim, dos, gsa, military); 
	}

	public String getPortsPerSection(Long id, String tarrifApplicability) {
		return pricingControlDao.getPortsPerSection(id, tarrifApplicability);
	}

	
	

	

}
