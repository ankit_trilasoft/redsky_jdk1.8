<%@ include file="/common/taglibs.jsp"%>
<%
    // response.setContentType("text/xml");
	// response.setHeader("Cache-Control", "no-cache");
%>

<script language="javascript">
$(document).ready(function (){
	$('span.pagelinks a').bind('click',function(event){
		document.getElementById('loading').style.display = 'block'
			event.preventDefault();
			var lname = escape("<%= request.getParameter("lname") %>") ;
			var regno = escape("<%= request.getParameter("regno") %>") ;
			var shipno = escape("<%= request.getParameter("shipno") %>") ;
			var fname = escape("<%= request.getParameter("fname") %>") ;
			var page = getParameterByName("page",this.href) ;
			if(page=='')
				page="1";
			var url = "ajaxDemoSearch.html?lname="+lname+"&page="+page+"&decorator=simple&popup=true&ajax=1&regno="+regno+"&shipno="+shipno+"&fname="+fname;
   			$.get(url,{},function(response){ 
		    	 $('#result').html(response)
		    	 document.getElementById('loading').style.display = 'none'
		    })	
		}) 
		
		$('th.sortable a').bind('click',function(event){
			document.getElementById('loading').style.display = 'block'
				event.preventDefault();
				var lname = escape("<%= request.getParameter("lname") %>") ;
				var regno = escape("<%= request.getParameter("regno") %>") ;
				var shipno =escape("<%= request.getParameter("shipno") %>") ;
				var fname = escape("<%= request.getParameter("fname") %>") ;
				var page = getParameterByName("page",this.href) ;
				if(page=='')
					page="1";
				var dir = getParameterByName("dir",this.href) ;
				var sort = getParameterByName("sort",this.href) ;
				 var url = "ajaxDemoSearch.html?lname="+lname+"&sort="+sort+"&dir="+dir+"&page="+page+"&decorator=simple&popup=true&ajax=1&regno="+regno+"&shipno="+shipno+"&fname="+fname;
	   			
				 
				 $.get(url,{},function(response){ 
					
	   				$('#result').html(response)
			    	 document.getElementById('loading').style.display = 'none'
			    }) 	
			}) 
			$('div.exportlinks a').click(function(e){
				
				
				 window.open('data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent($('#soList').html()));
				 e.preventDefault();
				<%-- document.getElementById('loading').style.display = 'block'
					event.preventDefault();
					var lname = '<%= request.getParameter("lname") %>' ;
					var regno = '<%= request.getParameter("regno") %>' ;
					var shipno = '<%= request.getParameter("shipno") %>' ;
					var fname = '<%= request.getParameter("fname") %>' ;
					var page = getParameterByName("page",this.href) ;
					if(page=='')
						page="1";
					/* var dir = getParameterByName("dir",this.href) ;
					var sort = getParameterByName("sort",this.href) ; */
					
					var tblid = '6578706f7274';
					var exp = getParameterByName("d-447176-e",this.href) ;
					 var url = "ajaxDemoSearch.html?lname="+lname+"&"+tblid+"=1&d-447176-e="+exp+"&page="+page+"&decorator=simple&popup=true&ajax=1&regno="+regno+"&shipno="+shipno+"&fname="+fname;
					 $.get(url,{},function(response){ 
						 var uri = 'data:application/csv;charset=UTF-8,' + response;
						 window.open(uri);
						 /* alert(response); */
		   				/*  $('#result').html(response);
				    	 document.getElementById('loading').style.display = 'none';  */
				    }) 	 --%>
				}) 	
})
function getParameterByName( name,href )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( href );
  if( results == null )
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}
</script>
<display:table name="SOListExt" class="table" id="soList" pagesize="100" >
<c:if test="${soList.job=='RLO'}">
				 <display:column sortable="true" style="width:50px" title="Ship Number" sortProperty="shipNumber">
               <a href="editDspDetails.html?id=${soList.id}">
                 <c:out value="${soList.shipNumber}" />
                  </a> 
                </display:column> 
			</c:if>
           <c:if test="${soList.job!='RLO'}">
           <display:column sortable="true" style="width:50px" title="Ship Number" sortProperty="shipNumber">
               <a href="editTrackingStatus.html?id=${soList.id}">
                 <c:out value="${soList.shipNumber}" />
                  </a> 
           </display:column>  
 </c:if>
	<display:column property="registrationNumber"  title="Registration #" sortable="true" />
	<display:column property="lastName"  title="Last Name" sortable="true"/>
	<display:column property="firstName"  title="First Name" sortable="true"/>
</display:table>
