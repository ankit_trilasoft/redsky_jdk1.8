package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.InlandAgentDao;
import com.trilasoft.app.model.InlandAgent;
import com.trilasoft.app.service.InlandAgentManager;

public class InlandAgentManagerImpl extends GenericManagerImpl<InlandAgent, Long> implements InlandAgentManager {
    InlandAgentDao inlandAgentDao;
	public InlandAgentManagerImpl(InlandAgentDao inlandAgentDao) {
		super(inlandAgentDao);
		this.inlandAgentDao = inlandAgentDao;
	}
	
	public List getByShipNumber(String shipNumber) {
		return inlandAgentDao.getByShipNumber(shipNumber);
	}
	public List<InlandAgent> findByInLandAgentList(String shipNumber){
		return inlandAgentDao.findByInLandAgentList(shipNumber);
	}
	public List findMaximumChild(String shipNm) {
		return inlandAgentDao.findMaximumChild(shipNm);
	}

	public List findMinimumChild(String shipNm) {
		return inlandAgentDao.findMinimumChild(shipNm);
	}
	public List findCountChild(String shipNm) {
		return inlandAgentDao.findCountChild(shipNm);
	}
	public List findMaximumIdNumber(String shipNum){
    	return inlandAgentDao.findMaximumIdNumber(shipNum);
    }
    public List findMaxId() {
 		return inlandAgentDao.findMaxId();
	}
    public void updateInlandAgentDetailsFromList(Long id,String fieldName,String fieldValue,String sessionCorpID,String shipNumber, String userName,String fieldType){
    	inlandAgentDao.updateInlandAgentDetailsFromList(id,fieldName,fieldValue,sessionCorpID,shipNumber, userName,fieldType);
	}
    public List getContainerNumber(String shipNumber) {
		return inlandAgentDao.getContainerNumber(shipNumber);
	}
	
}
