<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
<style>
input[type="checkbox"] {
	vertical-align: middle;
} 

.bottom-header {
	margin-top: 50px !important
}
</style>
	<s:hidden name="newPage"/>
<tr>
	<td height="10" width="100%" align="left" style="margin: 0px">
		<div onClick="javascript:animatedcollapse.toggle('additional')"
			style="margin: 0px">
			<table cellpadding="0" cellspacing="0" border="0" width="100%"
				style="margin: 0px">
				<tr>
					<td class="headtab_left"></td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Additional&nbsp;Addresses</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;</td>
					<td class="headtab_right"></td>
				</tr>
			</table>
		</div> 
		<div id="additional" class="switchgroup1">
			<table class="detailTabLabel" border="0" width="100%">
				<tbody>
					<tr>
						<td align="left" colspan="10">
							<div style="width: 100%; overflow-x: auto; overflow-y: auto;">
								<s:set name="adAddressesDetailsList"
									value="adAddressesDetailsList" scope="request" />
								<display:table name="adAddressesDetailsList" class="table"
									requestURI="" id="adAddressesDetailsList" defaultsort="1"
									pagesize="10" style="margin:0 0 5px 0;">
									<display:column title="Description">
										<a
											href="javascript:openWindow('editAdAddressesDetails.html?id=${adAddressesDetailsList.id}&customerFileId=${customerFile.id}&decorator=popup&popup=true',800,420)">
											<c:out value="${adAddressesDetailsList.description}"></c:out>
										</a>
									</display:column>
									<display:column property="addressType" title="Address Type" />
									<display:column title="Address" class="upper-case"
										maxLength="15">${adAddressesDetailsList.address1}&nbsp;${adAddressesDetailsList.address2}&nbsp;${adAddressesDetailsList.address3}</display:column>
									<display:column property="country" title="Country" />
									<display:column property="state" title="State" />
									<display:column property="city" class="upper-case" title="City" />
									<display:column property="phone" title="Phone" />
								</display:table>
								<c:if test="${not empty customerFile.id}">
									<input type="button" class="cssbutton1"
										onclick="window.open('editAdAddressesDetails.html?decorator=popup&popup=true&customerFileId=${customerFile.id}','customerFileForm','height=450,width=800,top=50,left=230, scrollbars=yes,resizable=yes').focus();"
										value="Add Addresses" style="width: 110px;" />
								</c:if>
								<c:if test="${empty customerFile.id}">
									<input type="button" class="cssbutton1"
										onclick="massageAddress();" value="Add Addresses"
										style="width: 110px;" />
								</c:if>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</td>
</tr>

<configByCorp:fieldVisibility
	componentId="component.field.Alternative.hideForVoerman">
	<tr>
		<td height="10" width="100%" align="left" style="margin: 0px">
			<div onClick="javascript:animatedcollapse.toggle('alternative')"
				style="margin: 0px">
				<table cellpadding="0" cellspacing="0" width="100%"
					style="margin: 0px">
					<tr>
						<td class="headtab_left"></td>
						<td NOWRAP class="headtab_center">&nbsp;&nbsp;Alternative&nbsp;/&nbsp;Spouse&nbsp;Contact</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_special">&nbsp;</td>
						<td class="headtab_right"></td>
					</tr>
				</table>
			</div>
			<div id="alternative" class="switchgroup1">
				<table class="detailTabLabel" cellspacing="0" cellpadding="0"
					width="100%">
					<tbody>
						<tr>
							<td class="subcontenttabChild" style="width: 100%; height: 19px;"><font
								color="black"><b>&nbsp;At Origin</b></font></td>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="2"
					border="0">
					<tbody>
						<tr>
							<td align="left" style="width: 93px">&nbsp;</td>
							<td align="left" class="listwhitetext"><fmt:message
									key='customerFile.contactName' /></td>
							<td align="left" style="width: 15px"></td>
							<td align="left" class="listwhitetext"><fmt:message
									key='customerFile.organization' /></td>
						</tr>
						<tr>
							<td align="left" style="width: 93px">&nbsp;</td>
							<td><s:textfield cssClass="input-text" id="contactName"
									name="customerFile.contactName" cssStyle="width:267px;"
									maxlength="95"
									onkeydown="return onlyCharsAllowed(event, this, 'special')"
									onblur="valid(this,'special')" /></td>
							<td align="left" style="width: 23px"></td>
							<td colspan="3"><configByCorp:customDropDown listType="map"
									list="${organiza_isactive}"
									fieldValue="${customerFile.organization}"
									attribute="class=list-menu style=width:219px; name=customerFile.organization onchange=changeStatus();" />
							</td>
						</tr>
						<tr>
							<td align="left" style="width: 93px">&nbsp;</td>
							<td align="left" class="listwhitetext"><fmt:message
									key='customerFile.contactEmail' /></td>
							<td align="left" style="width: 15px"></td>
							<td align="left" class="listwhitetext"><fmt:message
									key='customerFile.contactPhone' /></td>
							<td align="left" style="width: 15px"></td>
							<td align="left" class="listwhitetext" style="width: 25px"><fmt:message
									key='customerFile.originDayPhoneExt' /></td>
							<td align="left" style="width: 15px"></td>
						</tr>
						<tr>
							<td align="left" style="width: 93px">&nbsp;</td>
							<td><s:textfield cssClass="input-text"
									name="customerFile.contactEmail" cssStyle="width:267px;"
									maxlength="65" /></td>
							<td align="left" style="width: 15px"></td>
							<td><s:textfield cssClass="input-text"
									name="customerFile.contactPhone" cssStyle="width:130px;"
									maxlength="20" /></td>
							<td align="left" style="width: 15px"></td>
							<td><s:textfield cssClass="input-text"
									name="customerFile.contactNameExt" cssStyle="width:54px;"
									maxlength="4" onchange="onlyNumeric(this);" /></td>
							<td align="left" style="width: 15px"></td>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td style="width: 50px; height: 5px;"></td>
						</tr>
						<tr></tr>
						<tr></tr>
						<tr>
							<td style="width: 50px; height: 5px;"></td>
						</tr>
						<tr></tr>
						<tr></tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0"
					width="100%">
					<tbody>
						<tr>
						<tr>
							<td align="center" colspan="10" class="vertlinedata"></td>
						</tr>
						</tr>
						<tr>
							<td class="subcontenttabChild" style="width: 100%; height: 19px;"><font
								color="black"><b>&nbsp;At Destination</b></font></td>
						</tr>
						<tr>
							<td></td>
						</tr>
					</tbody>
				</table>
				<table width="100%" border="0" cellpadding="2" cellspacing="0"
					class="detailTabLabel">
					<tr>
						<td align="left">
							<table border="0" cellpadding="2" cellspacing="0"
								class="detailTabLabel">
								<tbody>
									<tr>
										<td width="93px">&nbsp;</td>
										<td width="16" align="left" class="listwhitetext"><fmt:message
												key='customerFile.contactName' /></td>
										<td width="10" align="left" style="width: 15px"></td>
										<td width="21" align="left" class="listwhitetext"><fmt:message
												key='customerFile.organization' /></td>
									</tr>
									<tr>
										<td width="93px">&nbsp;</td>
										<td><s:textfield cssClass="input-text"
												name="customerFile.destinationContactName"
												cssStyle="width:267px;" maxlength="95"
												onkeydown="return onlyCharsAllowed(event, this, 'special')"
												onblur="valid(this,'special')" /></td>
										<td align="left" style="width: 23px;"></td>
										<td colspan="3"><configByCorp:customDropDown
												listType="map" list="${organiza_isactive}"
												fieldValue="${customerFile.destinationOrganization}"
												attribute="class=list-menu style=width:219px; name=customerFile.destinationOrganization onchange=changeStatus();" />
										</td>
									</tr>
									<tr>
										<td width="93px">&nbsp;</td>
										<td align="left" class="listwhitetext"><fmt:message
												key='customerFile.contactEmail' /></td>
										<td align="left"></td>
										<td align="left" class="listwhitetext"><fmt:message
												key='customerFile.contactPhone' /></td>
										<td width="10" align="left" style="width: 15px"></td>
										<td width="25" align="left" class="listwhitetext"><fmt:message
												key='customerFile.destinationDayPhoneExt' /></td>
										<td width="10" align="left" style="width: 15px"></td>
									</tr>
									<tr>
										<td width="93px">&nbsp;</td>
										<td><s:textfield cssClass="input-text"
												name="customerFile.destinationContactEmail"
												cssStyle="width:267px;" maxlength="65" /></td>
										<td align="left"></td>
										<td><s:textfield cssClass="input-text"
												name="customerFile.destinationContactPhone"
												cssStyle="width:130px;" maxlength="20" /></td>
										<td align="left"></td>
										<td><s:textfield cssClass="input-text"
												name="customerFile.destinationContactNameExt"
												cssStyle="width:54px;" maxlength="4"
												onchange="onlyNumeric(this);" /></td>
										<td align="left"></td>
										<td width="7" align="left" style="width: 175px"></td>
										<c:if test="${empty customerFile.id}">
											<td style="position: absolute; right: 45px;" align="right"
												valign="bottom"><img
												src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
												HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
										</c:if>
										<c:if test="${not empty customerFile.id}">
											<c:choose>
												<c:when
													test="${countContactNotes == '0' || countContactNotes == '' || countContactNotes == null}">
													<td style="position: absolute; right: 45px;" align="right"
														valign="bottom"><img id="countContactNotesImage"
														src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
														HEIGHT=17 WIDTH=50
														onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',800,600);" /><a
														onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',800,600);"></a></td>
												</c:when>
												<c:otherwise>
													<td style="position: absolute; right: 45px;" align="right"
														valign="bottom"><img id="countContactNotesImage"
														src="${pageContext.request.contextPath}/images/notes_open1.jpg"
														HEIGHT=17 WIDTH=50
														onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',800,600);" /><a
														onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',800,600);"></a></td>
												</c:otherwise>
											</c:choose>
										</c:if>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr></tr>
						<tr></tr>
						<tr>
							<td class="listwhitetext" style="width: 50px; height: 5px;"></td>
						</tr>
						<tr></tr>
						<tr></tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0"
					width="100%">
					<tbody>
						<tr>
						<tr>
							<td align="center" colspan="10" class="vertlinedata"></td>
						</tr>
						</tr>
						<tr>
							<td class="subcontenttabChild" style="width: 100%; height: 19px;"><font
								color="black"><b>&nbsp;Spouse Contact</b></font></td>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<c:set var="privilegeFlag" value="false" />
							<c:if test="${customerFile.privileges}">
								<c:set var="privilegeFlag" value="true" />
							</c:if>
							<td class="listwhitetext" style="width: 50px; height: 5px;"></td>
						</tr>
						<tr>
							<td align="left" style="width: 100px">&nbsp;</td>
							<td align="left" class="listwhitetext"><fmt:message
									key='customerFile.custPartnerPrefix' /></td>
							<td align="left" style="width: 15px"></td>
							<td align="left" class="listwhitetext"><fmt:message
									key='customerFile.custPartnerFirstName' /></td>
							<td align="left" style="width: 15px"></td>
							<td align="left" class="listwhitetext"><fmt:message
									key='customerFile.custPartnerLastName' /></td>
						</tr>
						<tr>
							<td align="left" valign="top" style="width: 100px">&nbsp;</td>
							<td align="left"><configByCorp:customDropDown listType="map" list="${preffix_isactive}"
									fieldValue="${customerFile.custPartnerPrefix}"
									attribute="class=list-menu name=customerFile.custPartnerPrefix style=width:47px; headerKey='' headerValue='' onchange=changeStatus();" />
							</td>
							<td align="left" style="width: 15px"></td>
							<%-- <td align="left"> <s:textfield cssClass="input-text" name="customerFile.custPartnerFirstName" required="true"
																cssStyle="width:205px" maxlength="15" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" /> </td> --%>
							<td align="left"><s:textfield cssClass="input-text"
									name="customerFile.custPartnerFirstName" required="true"
									cssStyle="width:209px" maxlength="80"
									onkeyup="return onlyCharsAllowSpouse(event,this,'specials')"
									onblur="valid(this,'special')" /></td>
							<td align="left" style="width: 31px"></td>
							<td align="left"><s:textfield cssClass="input-text"
									key="customerFile.custPartnerLastName" required="true"
									cssStyle="width:219px;" maxlength="80"
									onkeyup="return onlyCharsAllowSpouse(event,this,'specials')"
									onblur="valid(this,'special')" /></td>
							<td align="left">&nbsp;&nbsp;<s:checkbox
									key="customerFile.privileges" value="${privilegeFlag}"
									fieldValue="true" onclick="changeStatus()" /></td>
							<td align="left" class="listwhitetext" style="width: 65px"><a
								href="#" style="cursor: help"
								onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.privileges',this);return false">Joint&nbsp;Account</a></td>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td style="width: 50px; height: 5px;" colspan="7"></td>
						</tr>
						<tr>
							<td align="left" style="width: 100px">&nbsp;</td>
							<td align="left" class="listwhitetext"><fmt:message
									key='customerFile.custPartnerEmail' /></td>
							<td align="left" style="width: 15px"></td>
							<td align="left" class="listwhitetext"><fmt:message
									key='customerFile.custPartnerContact' /></td>
							<td align="left" style="width: 15px"></td>
							<td align="left" class="listwhitetext"><fmt:message
									key='customerFile.custPartnerMobile' /></td>
						</tr>
						<tr>
							<td align="left" style="width: 100px">&nbsp;</td>
							<td align="left"><s:textfield cssClass="input-text"
									name="customerFile.custPartnerEmail" cssStyle="width:269px;"
									maxlength="65" /></td>
							<td align="left" style="width: 31px"></td>
							<td align="left"><s:textfield cssClass="input-text"
									name="customerFile.custPartnerContact" cssStyle="width:100px;"
									maxlength="20" /></td>
							<td align="left" style="width: 15px"></td>
							<td align="left"><s:textfield cssClass="input-text"
									name="customerFile.custPartnerMobile" cssStyle="width:100px;"
									maxlength="25" /></td>
							<c:if test="${empty customerFile.id}">
								<td align="right" style="position: absolute; right: 45px;"
									valign="bottom"><img
									src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
									HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
								<c:choose>
									<c:when
										test="${countSpouseNotes == '0' || countSpouseNotes == '' || countSpouseNotes == null}">
										<td align="right" style="position: absolute; right: 45px;"
											valign="bottom"><img id="countSpouseNotesImage"
											src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
											HEIGHT=17 WIDTH=50
											onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',800,600);" /><a
											onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',800,600);"></a></td>
									</c:when>
									<c:otherwise>
										<td align="right" style="position: absolute; right: 45px;"
											valign="bottom"><img id="countSpouseNotesImage"
											src="${pageContext.request.contextPath}/images/notes_open1.jpg"
											HEIGHT=17 WIDTH=50
											onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',800,600);" /><a
											onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',800,600);"></a></td>
									</c:otherwise>
								</c:choose>
							</c:if>
						</tr>
						<tr>
							<td style="width: 50px; height: 5px;" colspan="7"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</td>
	</tr>
	<%-- </c:if> --%>
</configByCorp:fieldVisibility>
<%-- code named as "customerfilesecform.jsp--topposition script" in customerfilejavascript.js --%>
<td height="10" width="100%" align="left" style="margin: 0px;">
	<div onClick="javascript:animatedcollapse.toggle('billing')"
		style="margin: 0px;">
		<table cellpadding="0" cellspacing="0" width="100%"
			style="margin: 0px;">
			<tr>
				<td class="headtab_left"></td>
				<td NOWRAP class="headtab_center">&nbsp;&nbsp;Billing Details</td>
				<td width="28" valign="top" class="headtab_bg"></td>
				<td class="headtab_bg_center">&nbsp;</td>
				<td class="headtab_right"></td>
			</tr>
		</table>
	</div>
	<div id="billing" class="switchgroup1">
		<table class="detailTabLabel" cellspacing="0" cellpadding="0"
			border="0">
			<tbody>
				<tr>
					<td style="width: 100px; height: 10px;"></td>
				</tr>
				<tr></tr>
				<tr></tr>
				<c:set var="accountCodeValidation" value="N" />
				<configByCorp:fieldVisibility
					componentId="component.field.accountCode.Validation">
					<c:set var="accountCodeValidation" value="Y" />
				</configByCorp:fieldVisibility>
				<tr>
					<td align="left" style="width: 100px">&nbsp;</td>
					<td align="left" style="width:66px;" class="listwhitetext"><fmt:message
							key='customerFile.billToCode' /><font color="red" size="2">*</font></td>
					<td align="left" style="width: 15px"></td>
					<td align="left" class="listwhitetext" style="width: 70px">Bill
						To Name</td>
					<td align="left" style="width: 12px"></td>
					<td align="left" style="width: 5px"></td>
					<td align="left" class="listwhitetext" width="165px" colspan="2"><a
						href="#" style="cursor: help"
						onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.billToAuthorization',this);return false">Account
							PO#/SAP Number</a></td>
					<td align="left" style="width: 45px"></td>
					<td align="left"></td>
				</tr>
				<tr>
					<td align="right" class="listwhitetext" style="width: 100px">Billing:&nbsp;</td>
					<c:choose>
						<c:when test="${hasDataSecuritySet}">
							<td width="60px"><s:textfield cssClass="input-text"
									id="billToCodeId" name="customerFile.billToCode"
									cssStyle="width:80px;" maxlength="8"
									onchange="billtoCheck();valid(this,'special');checkForBlank();checkAccountCodeForAssignmentType();fillBillToAuthority();billToCodeFieldValueCopy();fillAccountCodeByBillToCode();" /></td>
							<td width="22"><img class="openpopup"
								id="billToCode.popupImage" width="17" height="20"
								onclick="openOrderInitiationPopWindow();document.forms['customerFileForm'].elements['customerFile.billToCode'].focus();"
								src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td align="left" width="218">
                           <c:if test="${accountSearchValidation}">
							<s:textfield cssClass="input-textUpper" id="billToNameId"name="customerFile.billToName"onkeyup="findPartnerDetails('billToNameId','billToCodeId','billToNameDiv',' and (isAccount is true or isPrivateParty is true)',document.forms['customerFileForm'].elements['customerFile.companyDivision'].value,event);"onchange="findPartnerDetailsByName('billToCodeId','billToNameId');"cssStyle="width:190px;" maxlength="250" />
							<div id="billToNameDiv" class="autocomplete"style="z-index: 9999; position: absolute; margin-top: 49px; left: 201px;"></div>
							</c:if>
									 <c:if test="${!accountSearchValidation}">
							<s:textfield cssClass="input-textUpper" id="billToNameId"name="customerFile.billToName"onkeyup="findPartnerDetails('billToNameId','billToCodeId','billToNameDiv',' and (isAccount is true or isAgent is true or isCarrier is true or isPrivateParty is true)',document.forms['customerFileForm'].elements['customerFile.companyDivision'].value,event);"onchange="findPartnerDetailsByName('billToCodeId','billToNameId');"cssStyle="width:190px;" maxlength="250" />
							<div id="billToNameDiv" class="autocomplete"style="z-index: 9999; position: absolute; margin-top: 49px; left: 201px;"></div>
							</c:if>
								<img align="top" id="viewDetails" class="openpopup" width="17"
								height="20" title="View details"
								onclick="viewPartnerDetailsForBillToCode(this);"
								src="<c:url value='/images/address2.png'/>" /></td>
						</c:when>
						<c:otherwise>
							<td width="60px"><s:textfield cssClass="input-text"
									id="billToCodeId" name="customerFile.billToCode"
									cssStyle="width:60px;" maxlength="8"
									onchange="billtoCheck();valid(this,'special');checkForBlank();checkAccountCodeForAssignmentType();fillBillToAuthority();billToCodeFieldValueCopy();fillAccountCodeByBillToCode();" /></td>
							<td width="22"><img class="openpopup" width="17"
								id="billToCode.popupImage" height="20"
								onclick="openPopWindow(this);document.forms['customerFileForm'].elements['customerFile.billToCode'].focus();"
								src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td align="left" width="218">
							<c:if test="${accountSearchValidation}">
					        <s:textfield cssClass="input-text" id="billToNameId"name="customerFile.billToName" onkeyup="findPartnerDetails('billToNameId','billToCodeId','billToNameDiv',' and (isAccount is true  or isPrivateParty is true)',document.forms['customerFileForm'].elements['customerFile.companyDivision'].value,event);"onchange="findPartnerDetailsByName('billToCodeId','billToNameId');"cssStyle="width:190px;" maxlength="250" /> 
									</c:if>
								<c:if test="${!accountSearchValidation}">
					        <s:textfield cssClass="input-text" id="billToNameId"name="customerFile.billToName" onkeyup="findPartnerDetails('billToNameId','billToCodeId','billToNameDiv',' and (isAccount is true or isAgent is true or isCarrier is true or isPrivateParty is true)',document.forms['customerFileForm'].elements['customerFile.companyDivision'].value,event);"onchange="findPartnerDetailsByName('billToCodeId','billToNameId');"cssStyle="width:190px;" maxlength="250" /> 
								</c:if>
							<img align="top"id="viewDetails" class="openpopup" width="17" height="20"
								title="View details"
								onclick="viewPartnerDetailsForBillToCode(this);"
								src="<c:url value='/images/address2.png'/>" /></td>
							<div id="billToNameDiv" class="autocomplete"
								style="z-index: 9999; position: absolute; margin-top: 49px; left: 201px;"></div>

						</c:otherwise>
					</c:choose>
					<td align="left" width="26">
						<div id="hidBillTo" style="vertical-align: middle;">
							<a><img class="openpopup" id="noteCustBToNavigations"
								onclick="getPartnerAlert(document.forms['customerFileForm'].elements['customerFile.billToCode'].value,'noteCustBToNavigations');"
								src="${pageContext.request.contextPath}/images/navarrows_05.png"
								alt="Note List" title="Note List" /></a>
						</div>
					</td>
					<script type="text/javascript">
						setTimeout(
								"showOnLoadPartnerAlert('onload','${customerFile.billToCode}','hidBillTo')",
								1000);
					</script>
					<td align="left" style="width: 5px"></td>
					<td width="130" style="padding-right: 10px;"><s:textfield
							cssClass="input-text" name="customerFile.billToAuthorization"
							cssStyle="width:250px;" maxlength="50"
							onblur="valid(this,'special')" /></td>
					<c:if test="${not hasDataSecuritySet}">
						<td><input type="button" class="cssbutton1"
							onclick="openPopWindow(this)" name="P"
							value="Create Private Bill To Code"
							style="width: 155px; margin-left: 5px;" /></td>
					</c:if>
					<c:choose>
						<c:when test="${usertype=='ACCOUNT' || usertype=='AGENT'}">
						</c:when>
						<c:otherwise>
							<td align="right" class="listwhitetext"><s:checkbox
									name="destinationcopyaddress" fieldValue="true" /></td>
							<td align="left" class="listwhitetext" style="width: 15px">&nbsp;Destination</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</tbody>
		</table>
		<table class="detailTabLabel" cellspacing="0" cellpadding="0"
			border="0">
			<tbody>
				<tr>
					<td style="width: 100px; height: 5px;" colspan="7"></td>
				</tr>
				<tr>
					<c:set var="noChargeFlag" value="false" />
					<c:if test="${customerFile.noCharge}">
						<c:set var="noChargeFlag" value="true" />
					</c:if>
					<td align="left" style="width: 100px">&nbsp;</td>
					<td align="left" class="listwhitetext" colspan="2"><fmt:message
							key='customerFile.billPayMethod' /></td>
					<td>&nbsp;</td>
					<td colspan="" align="left" class="listwhitetext">Account&nbsp;Reference&nbsp;#/Cost&nbsp;Centre&nbsp;#
					</td>
					<td>&nbsp;</td>
					<td align="left" class="listwhitetext" colspan="3"><div
							style="margin: 0px; float: left;">
							<fmt:message key='customerFile.contract' />
						</div> <c:if test="${checkContractChargesMandatory=='1'}">
							<div id="contractMandatory" style="margin: 0px;">
								<font color="red" size="2">*</font>
							</div>
						</c:if></td>
				</tr>
				<tr>
					<td align="left" style="width: 100px">&nbsp;</td>
					<td colspan="2"><configByCorp:customDropDown listType="map"
							list="${paytype_isactive}"
							fieldValue="${customerFile.billPayMethod}"
							attribute="id=billPayMethod class=list-menu name=customerFile.billPayMethod cssStyle=width:272px  headerKey='' headerValue='' onchange=changeStatus();" />
					</td>
					<td align="left" style="width: 31px">&nbsp;</td>
					<td><s:textfield cssClass="input-text"
							name="customerFile.billToReference" cssStyle="width:184px;"
							maxlength="20" onblur="valid(this,'special')" /></td>
					<td width="15">&nbsp;</td>
					<td align="left" colspan="3"><s:select cssClass="list-menu"
							id="contract" name="customerFile.contract" list="%{contracts}"
							cssStyle="width:159px"
							onchange="changeStatus();checkPriceContract();" /></td>
				</tr>
				<tr>
					<td style="width: 100px; height: 5px;" colspan="10"></td>
				</tr>
				<tr>
					<td align="left" style="width: 100px">&nbsp;</td>
					<td align="left" class="listwhitetext" style="width: 79px">Account&nbsp;Code<c:if
							test="${accountCodeValidation=='Y'}">
							<div id="accountValidation" style="float: right;">
								<font color="red" size="2">*&nbsp;</font>
							</div>
						</c:if>
						<configByCorp:fieldVisibility componentId="component.field.accountCode.allFile.validation">
						<div id="accountUNIPValidation" style="float: right;">
								<font color="red" size="2">*&nbsp;</font>
								</div>
							</configByCorp:fieldVisibility></td>
					<td align="left" class="listwhitetext">&nbsp;&nbsp;&nbsp;&nbsp;Account
						Name</td>
					<td align="left" colspan="5">&nbsp;</td>
				</tr>
				<tr>
					<td align="right" class="listwhitetext" style="width: 100px;">Account:&nbsp;</td>
					<td colspan="7">
						<table class="detailTabLabel" cellspacing="0" cellpadding="0"
							width="">
							<tr>
								<td align="left" width="92"><s:textfield
										cssClass="input-text" id="accountCodeIdCF"
										name="customerFile.accountCode" cssStyle="width:60px;"
										maxlength="8"
										onchange="valid(this,'special');changeStatus();showPartnerAlert('onchange',this.value,'hidAcc');checkAccountCodeForAssignmentType();findDMMTypeAccountCode();" />
									<img class="openpopup" style="vertical-align: top;" width="17"
									id="accountCodePopupImg" height="20"
									onclick="openAccountPopWindow();document.forms['customerFileForm'].elements['customerFile.accountName'].focus();"
									src="<c:url value='/images/open-popup.gif'/>" /></td>

								<td align="left" colspan="3">
							<c:if test="${accountSearchValidation}"> 
											<s:textfield
										cssClass="input-text" id="accountNameIdCF"
										name="customerFile.accountName" cssStyle="width:321px;"
										maxlength="250"
										onkeyup="findPartnerDetails('accountNameIdCF','accountCodeIdCF','accountNameDiv',' and (isAccount is true or  isPrivateParty is true)',document.forms['customerFileForm'].elements['customerFile.companyDivision'].value,event);"
										onchange="findPartnerDetailsByName('accountCodeIdCF','accountNameIdCF');"
										onfocus="showPartnerAlert('onchange',document.forms['customerFileForm'].elements['customerFile.accountCode'].value,'hidAcc');" />
									<div id="accountNameDiv" class="autocomplete"
										style="z-index: 9999; position: absolute; margin-top: 2px;"></div>
										</c:if>
						
							<c:if test="${!accountSearchValidation}"> 
											<s:textfield
										cssClass="input-text" id="accountNameIdCF"
										name="customerFile.accountName" cssStyle="width:321px;"
										maxlength="250"
										onkeyup="findPartnerDetails('accountNameIdCF','accountCodeIdCF','accountNameDiv',' and (isAccount is true or isAgent is true or isPrivateParty is true)',document.forms['customerFileForm'].elements['customerFile.companyDivision'].value,event);"
										onchange="findPartnerDetailsByName('accountCodeIdCF','accountNameIdCF');"
										onfocus="showPartnerAlert('onchange',document.forms['customerFileForm'].elements['customerFile.accountCode'].value,'hidAcc');" />
									<div id="accountNameDiv" class="autocomplete"
										style="z-index: 9999; position: absolute; margin-top: 2px;"></div>
										</c:if>
						
								</td>
								<td align="left" colspan="4">
									<div id="hidAcc" style="vertical-align: middle;">
										<a><img class="openpopup" id="noteCustAccNavigations"
											onclick="getPartnerAlert(document.forms['customerFileForm'].elements['customerFile.accountCode'].value,'noteCustAccNavigations');"
											src="${pageContext.request.contextPath}/images/navarrows_05.png"
											alt="Note List" title="Note List" /></a>
									</div>
								</td>
							</tr>
						</table>
					</td>
					<script type="text/javascript">
						setTimeout(
								"showOnLoadPartnerAlert('onload','${customerFile.accountCode}','hidAcc')",
								2000);
					</script>
				</tr>
			</tbody>
		</table>
		<table class="detailTabLabel" cellspacing="0" cellpadding="0"
			width="100%">
			<tbody>
				<tr>
					<td style="width: 100px; height: 5px;"></td>
				</tr>
				<tr>
					<td align="center" colspan="10" class="vertlinedata"></td>
				</tr>
				<tr>
					<td style="width: 100px; height: 5px;"></td>
				</tr>
			</tbody>
		</table>
		<table class="detailTabLabel" cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td style="width: 100px; height: 5px;" colspan="7"></td>
				</tr>
				<tr>
					<td align="left" style="width: 100px">&nbsp;</td>
					<td align="left" class="listwhitetext"><a href="#"
						style="cursor: help"
						onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.orderBy',this);return false"><fmt:message
								key='customerFile.orderBy' /></a></td>
					<td align="left" style="width: 15px"></td>
					<td align="left" class="listwhitetext"><fmt:message
							key='customerFile.billApprovedDate' /></td>
					<td align="left"></td>
					<td align="left" style="width: 15px"></td>
					<td align="left" class="listwhitetext"><fmt:message
							key='customerFile.orderPhone' /></td>
					<td align="left" style="width: 12px"></td>
					<td align="left" class="listwhitetext">Email</td>
				</tr>
				<tr>
					<td align="right" class="listwhitetext" style="width: 100px">Authorization:&nbsp;</td>
					<td><s:textfield cssClass="input-text"
							name="customerFile.orderBy" cssStyle="width:270px" maxlength="30"
							onkeydown="return onlyCharsAllowed1(event,this)"
							onblur="autoPopulate_customerFile_approvedDate(this,'special');" /></td>
					<td align="left" class="listwhitetext" style="width: 33px"></td>
					<c:if test="${not empty customerFile.billApprovedDate}">
						<s:text id="customerFileApprovedDateFormattedValue"
							name="${FormDateValue}">
							<s:param name="value" value="customerFile.billApprovedDate" />
						</s:text>
						<td><s:textfield cssClass="input-text" id="billApprovedDate"
								name="customerFile.billApprovedDate"
								value="%{customerFileApprovedDateFormattedValue}"
								required="true" cssStyle="width:65px" maxlength="11"
								readonly="true" onkeydown="onlyDel(event,this)"
								onfocus="changeStatus();" /></td>
						<td>&nbsp;<img id="billApprovedDate_trigger"
							style="vertical-align: bottom"
							src="${pageContext.request.contextPath}/images/calender.png"
							HEIGHT=20 WIDTH=20 /></td>
					</c:if>
					<c:if test="${empty customerFile.billApprovedDate}">
						<td><s:textfield cssClass="input-text" id="billApprovedDate"
								name="customerFile.billApprovedDate" required="true"
								cssStyle="width:65px" maxlength="11" readonly="true"
								onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td>
						<td>&nbsp;<img id="billApprovedDate_trigger"
							style="vertical-align: bottom"
							src="${pageContext.request.contextPath}/images/calender.png"
							HEIGHT=20 WIDTH=20 /></td>
					</c:if>
					<td align="left" style="width: 10px"></td>
					<td><s:textfield cssClass="input-text"
							name="customerFile.orderPhone" cssStyle="width:109px"
							maxlength="20" /></td>
					<td align="left" style="width: 15px"></td>
					<td><s:textfield cssClass="input-text"
							name="customerFile.orderEmail" cssStyle="width:162px"
							maxlength="65" /></td>
				</tr>
			</tbody>
		</table>
		<table class="detailTabLabel" cellspacing="0" cellpadding="0"
			border="0">
			<tbody>
				<tr>
					<td style="width: 100px; height: 5px;" colspan="7"></td>
				</tr>
				<tr>
					<td align="left" style="width: 100px">&nbsp;</td>
					<td align="left" class="listwhitetext"><a href="#"
						style="cursor: help"
						onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.personPricing',this);return false">Pricing</a></td>
					<td align="left" style="width: 14px"></td>
					<td align="left" class="listwhitetext"><a href="#"
						style="cursor: help"
						onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.personBilling',this);return false">Billing</a></td>
					<configByCorp:fieldVisibility
						componentId="component.field.billing.internalBillingPerson">
						<td align="left" class="listwhitetext" style="width: 14px"></td>
						<td align="left" class="listwhitetext">Internal&nbsp;Billing</td>
					</configByCorp:fieldVisibility>
					<td align="left" style="width: 14px"></td>
					<td align="left" class="listwhitetext"><configByCorp:fieldVisibility
							componentId="component.field.customerFile.payable">
							<a href="#" style="cursor: help"
								onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.personPayable',this);return false">Payable</a>
						</configByCorp:fieldVisibility></td>
					<td align="left" style="width: 14px"></td>
					<td align="left" class="listwhitetext">Auditor</td>
				</tr>
				<tr>
					<td align="right" class="listwhitetext" style="width: 100px">Responsible
						Person:</td>
					<td><s:select name="customerFile.personPricing"
							cssClass="list-menu" list="%{pricingList}" id="personPricing"
							cssStyle="width:165px" headerKey="" headerValue=""
							onchange="changeStatus();" /></td>
					<td align="left" class="listwhitetext" style="width: 14px"></td>
					<td><s:select name="customerFile.personBilling"
							cssClass="list-menu" list="%{billingList}" id="personBilling"
							cssStyle="width:165px" headerKey="" headerValue=""
							onchange="changeStatus();" /></td>
					<configByCorp:fieldVisibility
						componentId="component.field.billing.internalBillingPerson">
						<td align="left" class="listwhitetext" style="width: 10px"></td>
						<td><s:select name="customerFile.internalBillingPerson"
								cssClass="list-menu" list="%{internalBillingPersonList}"
								id="internalBillingPerson" cssStyle="width:165px" headerKey=""
								headerValue="" /></td>
					</configByCorp:fieldVisibility>
					<td align="left" class="listwhitetext" style="width: 14px"></td>
					<td><configByCorp:fieldVisibility
							componentId="component.field.customerFile.payable">
							<s:select cssClass="list-menu" name="customerFile.personPayable"
								id="personPayable" list="%{payableList}" cssStyle="width:172px"
								headerKey="" headerValue="" onchange="changeStatus();" />
						</configByCorp:fieldVisibility></td>
					<td align="left" style="width: 10px"></td>
					<td><s:select cssClass="list-menu" name="customerFile.auditor"
							id="auditor" list="%{auditorList}" cssStyle="width:165px"
							headerKey="" headerValue="" onchange="changeStatus();" /></td>
				</tr>
				<tr>
					<td style="width: 100px; height: 5px;"></td>
				</tr>
			</tbody>
		</table>
		<table class="detailTabLabel" cellspacing="0" cellpadding="0"
			border="0" width="">
			<tbody>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td align="left" width="182px" class="listwhitetext">Approved&nbsp;By&nbsp;</td>
					<td></td>
					<td align="left" width="" class="listwhitetext">Approved&nbsp;On&nbsp;</td>
				</tr>
				<tr>
					<td align="right" width="96" class="listwhitetext"><a href="#"
						style="cursor: help"
						onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.noCharge',this);return false"><fmt:message
								key='customerFile.noCharge' /></td>
					<td align="left" class="listwhitetext"><s:checkbox
							key="customerFile.noCharge" value="${noChargeFlag}"
							fieldValue="true" onclick="changeStatus()" /></td>
					<td width="10px"></td>
					<td align="left" colspan=""><s:select cssClass="list-menu"
							name="customerFile.approvedBy" id="approvedBy" list="%{execList}"
							cssStyle="width:139px" headerKey="" headerValue=""
							onchange="changeStatus();" /></td>
					<td width="15px"></td>

					<c:if test="${not empty customerFile.approvedOn}">
						<td align="left" width="100"><s:text id="FormatedInvoiceDate"
								name="${FormDateValue}">
								<s:param name="value" value="customerFile.approvedOn" />
							</s:text> <s:textfield id="dateApprovedOn" name="customerFile.approvedOn"
								value="%{FormatedInvoiceDate}" onkeydown="onlyDel(event,this)"
								onfocus="changeStatus();" readonly="true" cssClass="input-text"
								cssStyle="width:65px;" /> <img id="dateApprovedOn_trigger"
							style="vertical-align: bottom"
							src="${pageContext.request.contextPath}/images/calender.png"
							HEIGHT=20 WIDTH=20 /></td>
					</c:if>
					<c:if test="${empty customerFile.approvedOn}">
						<td align="left" width="100"><s:textfield id="dateApprovedOn"
								name="customerFile.approvedOn" onkeydown="onlyDel(event,this)"
								onfocus="changeStatus();" readonly="true" cssClass="input-text"
								cssStyle="width:65px;" /> <img id="dateApprovedOn_trigger"
							style="vertical-align: bottom"
							src="${pageContext.request.contextPath}/images/calender.png"
							HEIGHT=20 WIDTH=20 /></td>
					</c:if>
					<c:if test="${empty customerFile.id}">
						<td align="right" style="position: absolute; right: 45px;"><img
							src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
							HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
					</c:if>
					<c:if test="${not empty customerFile.id}">
						<c:choose>
							<c:when
								test="${countBillingNotes == '0' || countBillingNotes == '' || countBillingNotes == null}">
								<td align="right" style="position: absolute; right: 45px;"><img
									id="countBillingNotesImage"
									src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
									HEIGHT=17 WIDTH=50
									onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Billing&imageId=countBillingNotesImage&fieldId=countBillingNotes&decorator=popup&popup=true',800,600);" /><a
									onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Billing&imageId=countBillingNotesImage&fieldId=countBillingNotes&decorator=popup&popup=true',800,600);"></a></td>
							</c:when>
							<c:otherwise>
								<td align="right" style="position: absolute; right: 45px;"><img
									id="countBillingNotesImage"
									src="${pageContext.request.contextPath}/images/notes_open1.jpg"
									HEIGHT=17 WIDTH=50
									onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Billing&imageId=countBillingNotesImage&fieldId=countBillingNotes&decorator=popup&popup=true',800,600);" /><a
									onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Billing&imageId=countBillingNotesImage&fieldId=countBillingNotes&decorator=popup&popup=true',800,600);"></a></td>
							</c:otherwise>
						</c:choose>
					</c:if>
				</tr>
				<tr>
					<td colspan="8">&nbsp;</td>
				</tr>
		</table>

		<table cellspacing="0" cellpadding="0" width="100%"
			style="margin: 0px; padding: 0px;">
			<tbody>
				<tr>
					<td style="width: 100px; height: 5px;"></td>
				</tr>
				<tr>
					<td align="center" class="vertlinedata" colspan="10"></td>
				</tr>
				<tr>
					<td style="width: 100px; height: 5px;"></td>
				</tr>
			</tbody>
		</table>

		<table style="margin: 0px; padding: 0px;">
			<c:if test="${usertype!='ACCOUNT' && usertype!='AGENT'}">
				<tr>
					<td colspan="5">
						<table id="ins1" style="display: none; margin: 0px;">
							<tr>
								<td align="right" class="listwhitetext" style="width: 90px">Insurance&nbsp;Options:</td>
								<td align="left" class="listwhitetext"><s:select
										cssClass="list-menu" name="customerFile.noInsurance"
										id="noInsurance" list="{}" cssStyle="width:292px" headerKey=""
										headerValue="" /></td>
							</tr>
						</table>
					</td>
				</tr>
			</c:if>
			<tr>
				<td style="width: 100px; height: 5px;" colspan="8"></td>
			</tr>

		</table>

	</div>
</td>
</tr>
<tr>
	<td height="10" width="100%" align="left" style="margin: 0px">
		<div onClick="javascript:animatedcollapse.toggle('entitlement')"
			style="margin: 0px">
			<table cellpadding="0" cellspacing="0" width="100%"
				style="margin: 0px">
				<tr>
					<td class="headtab_left"></td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Entitlement/Service&nbsp;Authorized</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_special">&nbsp;</td>
					<td class="headtab_right"></td>
				</tr>
			</table>
		</div>
		<div id="entitlement" class="switchgroup1">
			<table class="detailTabLabel" cellspacing="0" cellpadding="0"
				border="0" width="100%">
				<tr>
					<td height="10" colspan="10" align="right" valign="top">&nbsp;</td>
				</tr>
				<c:if test="${serviceRelosCheck==''}">
					<s:hidden name="customerFile.serviceRelo"
						value="${customerFile.serviceRelo}" />
				</c:if>
				<c:if test="${serviceRelosCheck!=''}">
					<tr>
						<td align="right" class="listwhitetext" valign="top">Relocation&nbsp;Services&nbsp;</td>
						<s:hidden name="customerFile.serviceRelo"
							value="${customerFile.serviceRelo}" />
						<td class="listwhitetext" valign="middle"><input
							type="checkbox" style="margin-left: 80px" name="checkAllRelo"
							onclick="checkAll(this)" />Check&nbsp;All&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan="2" valign="top">
							<fieldset
								style="margin: 3px 0 0 0; padding: 2px 0 0 2px; width: 60%;">
								<table class="detailTabLabel" cellspacing="0" cellpadding="0"
									border="0">
									<tr>
										<td width="53%" valign="top">
											<table class="detailTabLabel" cellspacing="1" cellpadding="0"
												border="0">
												<c:forEach var="entry" items="${serviceRelos1}">
													<tr>
														<td><input type="checkbox" name="checkV"
															id="${entry.key}" value="${entry.key}"
															onclick="selectColumn(this)" /></td>
														<td width="5px"></td>
														<td align="left" class="listwhitetext">
															${entry.value}</td>
													</tr>
												</c:forEach>
											</table>
										</td>
										<td valign="top">
											<table class="detailTabLabel" cellspacing="1" cellpadding="0"
												border="0">
												<c:forEach var="entry" items="${serviceRelos2}">
													<tr>
														<td><input type="checkbox" name="checkV"
															id="${entry.key}" value="${entry.key}"
															onclick="selectColumn(this)" /></td>
														<td width="5px"></td>
														<td align="left" class="listwhitetext">
															${entry.value}</td>
													</tr>
												</c:forEach>
											</table>
										</td>
									</tr>
								</table>
							</fieldset>
						</td>
					</tr>
				</c:if>



				<tr>
					<td align="right" class="listwhitetext" valign="middle">Service&nbsp;Requested&nbsp;</td>
					<td colspan="11">
						<fieldset id="customerFileServiceAirAgentAccount"
							style="margin: 2px 0px 15px; padding: 5px; width: 388px;">
							<table class="detailTabLabel" cellspacing="0" cellpadding="0"
								border="0" width="100%">
								<tr>
									<td align="right" class="listwhitebox" width="2%"
										valign="middle">Air</td>
									<td align="left" width="3%"><s:checkbox
											name="customerFile.serviceAir"
											value="${customerFile.serviceAir}" fieldValue="true" /></td>
									<td align="right" class="listwhitebox" width="2%"
										valign="middle">Sea</td>
									<td align="left" width="3%"><s:checkbox
											name="customerFile.serviceSurface"
											value="${customerFile.serviceSurface}" fieldValue="true" /></td>
									<td align="right" class="listwhitebox" width="2%"
										valign="middle">Road</td>
									<td align="left" width="3%"><s:checkbox
											name="customerFile.serviceAuto"
											value="${customerFile.serviceAuto}" fieldValue="true" /></td>
									<td align="right" class="listwhitebox" width="2%"
										valign="middle">Storage</td>
									<td align="left" width="3%"><s:checkbox
											name="customerFile.serviceStorage"
											value="${customerFile.serviceStorage}" fieldValue="true" /></td>
									<td align="right" class="listwhitebox" width="2%"
										valign="middle">Domestic</td>
									<td align="left" width="3%"><s:checkbox
											name="customerFile.serviceDomestic"
											value="${customerFile.serviceDomestic}" fieldValue="true" /></td>
												<configByCorp:fieldVisibility componentId="component.tab.customerFile.servicePovCheck">
											<td align="right" class="listwhitebox" width="2%"
										valign="middle">POV</td>
									<td align="left" width="3%"><s:checkbox
											name="customerFile.servicePov"
											value="${customerFile.servicePov}" fieldValue="true" /></td>
									<td width="2%"></td>
									</configByCorp:fieldVisibility>
								</tr>
							</table>
						</fieldset>
					</td>
				</tr>

				<tr>
					<td align="right" width="" class="listwhitetext">Visa
						Required&nbsp;</td>
					<td>
						<table class="detailTabLabel" cellspacing="0" cellpadding="0"
							border="0" style="margin: 0px; padding: 0px;">
							<tr>
								<td></td>
							</tr>
							<tr>
								<td align="left" width="230"><s:select cssClass="list-menu"
										name="customerFile.visaRequired" list="%{visaRequiredList}"
										cssStyle="width:80px" onchange="cheackVisaRequired();" /></td>
								<td align="right" class="listwhitetext" id="hidVisaStatusReg">Visa
									Status<font color="red" size="2">*&nbsp;</font>
								</td>
								<td width="5"></td>
								<td align="left" id="hidVisaStatusReg1"><configByCorp:customDropDown
										listType="map" list="${visaStatusList_isactive}"
										fieldValue="${customerFile.visaStatus}"
										attribute="class=list-menu  name=customerFile.visaStatus style=width:100px headerKey='' headerValue=''" />
								</td>
								<td width=""></td>
							</tr>
							<tr>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="10" colspan="6" align="right" valign="top">&nbsp;</td>
				</tr>
				<tr>
					<td align="right" class="listwhitetext" width="8%">Pets
						Involved&nbsp;</td>
					<td>
						<table class="detailTabLabel" cellspacing="0" cellpadding="0"
							border="0" style="margin: 0px; padding: 0px;">
							<tr>
								<td></td>
							</tr>
							<tr>
								<td align="left" width="230"><s:checkbox
										name="customerFile.petsInvolved"
										value="${customerFile.petsInvolved}" fieldValue="true"
										onclick="changeStatus();checkPetInvolve(this);"
										cssStyle="margin-left:0px;" /></td>
								<td colspan="4" id="petTypeId">
									<table class="detailTabLabel" cellspacing="0" cellpadding="0"
										border="0" style="margin: 0px; padding: 0px;">
										<tr>
											<td align="right" class="listwhitetext">Type Of
												Pet&nbsp;</td>
											<td style="width: 8; color: red;">*</td>
											<td>&nbsp;<configByCorp:customDropDown listType="map"
													list="${petTypeList_isactive}"
													fieldValue="${customerFile.petType}"
													attribute=" class=list-menu  name=customerFile.petType  style=width:100px" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="10" colspan="6">&nbsp;</td>
				</tr>
				<tr>
					<td class="listwhitetext" align="right" valign="top">Entitlement&nbsp;&nbsp;</td>
					<td colspan="6" align="left">
						<table class="detailTabLabel" cellspacing="0" cellpadding="0"
							border="0" style="margin: 0px; paddding: 0px; width: 38%;">
							<tr>
								<td>
									<table class="detailTabLabel" cellspacing="0" cellpadding="0"
										style="margin: 0px; paddding: 0px;">
										<c:forEach var="entitlementMap" items="${entitlementMap}">
											<tr>
												<td align="left" width="" class="listwhitetext"><s:checkbox
														name="chk_${entitlementMap.key}"
														id="chk_${entitlementMap.key}"
														value="chk_${entitlementMap.key}" onclick="getValue3();"
														cssStyle="vertical-align:middle;margin:4px 0px;" /> <s:label
														title="${entitlementMap.value}"
														value="${entitlementMap.key}"></s:label></td>
											</tr>
										</c:forEach>

									</table>
								</td>
							</tr>
						</table>

					</td>
				</tr>


				<tr>
					<td height="10" colspan="6">&nbsp;</td>
				</tr>


				<tr>
					<td align="right" valign="top"></td>
					<td>
						<table class="detailTabLabel" cellspacing="0" cellpadding="0"
							border="0">
							<tr>
								<td align="left" width="" height="5" class="listwhitetext"
									valign="bottom"></td>
							</tr>
							<tr>
								<td align="left" width="80%"><s:textarea
										name="customerFile.orderComment"
										cssStyle="width:397px;height:75px;" cssClass="textarea"
										readonly="false" /></td>
							</tr>
						</table>
					</td>
				</tr>
				<configByCorp:fieldVisibility
					componentId="component.field.VisibilityEntitlements.show">
					<tr>
						<td align="center" class="vertlinedata" colspan="15"></td>
					</tr>
					<tr>
						<td colspan="25">
							<table width="100%" cellspacing="0" cellpadding="0"
								style="margin: 0px; padding: 0px;">
								<tbody>
									<tr class="subcontenttabChild">
										<td><b>&nbsp;Entitlement</b></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>

					<tr>
						<td colspan="25">
							<table width="90%" cellspacing="0" cellpadding="0"
								style="margin: 0px; padding: 0px;">
								<tr>
									<td><c:set var="ischecked" value="false" /> <c:if
											test="${imfEntitlements.checkAirEntitlementsWeight}">
											<c:set var="ischecked" value="true" />
										</c:if> <c:set var="ischecked1" value="false" /> <c:if
											test="${imfEntitlements.checkSurfaceEntitlementsWeight}">
											<c:set var="ischecked1" value="true" />
										</c:if> <c:set var="ischecked2" value="false" /> <c:if
											test="${imfEntitlements.checkAutoEntitlementsWeight}">
											<c:set var="ischecked2" value="true" />
										</c:if> <c:set var="ischecked3" value="false" /> <c:if
											test="${imfEntitlements.checkOfficeEntitlementsWeight}">
											<c:set var="ischecked3" value="true" />
										</c:if> <c:set var="ischecked4" value="false" /> <c:if
											test="${imfEntitlements.permStorage}">
											<c:set var="ischecked4" value="true" />
										</c:if>

										<table border="0">
											<tr>
												<td align="right" colspan="2" class="listwhitetext-h1"
													style="padding-right: 5px;"><B>Air&nbsp;PERS&nbsp;Entitlements</B></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.weight" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.airEntitlementsWeight"
														onkeydown="return onlyNumsAllowed(event)"
														onblur="return isFloat(this);"
														onkeyup="valid(this,'special')" maxlength="12" size="12"
														cssClass="input-text" readonly="false"
														cssStyle="text-align:right" /></td>
												<td align="left" valign="top" class="listwhitetext"><s:checkbox
														key="imfEntitlements.checkAirEntitlementsWeight"
														value="${ischecked}" fieldValue="true"
														onclick="changeStatus();" />Required&nbsp;?
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.freight" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.airEntitlementsFreight"
														onkeydown="return onlyNumsAllowed(event)"
														onblur="valid(this,'special');CalulateTotalNumber();CalulateTotalAirEntitlements();"
														maxlength="12" size="12" cssClass="input-text"
														cssStyle="text-align:right" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.origin" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.airEntitlementsOrigin"
														onkeydown="return onlyNumsAllowed(event)"
														onblur="valid(this,'special');CalulateTotalNumber();CalulateTotalAirEntitlements();"
														maxlength="12" size="12" cssClass="input-text"
														cssStyle="text-align:right" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.destination" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.airEntitlementsDestination"
														onkeydown="return onlyNumsAllowed(event)"
														onblur="valid(this,'special');CalulateTotalNumber();CalulateTotalAirEntitlements();"
														maxlength="12" size="12" cssClass="input-text"
														cssStyle="text-align:right" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;Air&nbsp;Entitlements</B></td>
												<td align="left"><s:textfield
														name="imfEntitlements.airEntitlementsTotal" maxlength="12"
														size="12" cssClass="input-textUpper" readonly="true"
														cssStyle="text-align:right" /></td>
											</tr>
										</table></td>
									<td>
										<table border="0" style="margin: 0px;">
											<tr>
												<td align="center" colspan="2" class="listwhitetext-h1"
													style="padding-left: 103px;"><B>Surface&nbsp;HHG&nbsp;Entitlements</B></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.weight" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.surfaceEntitlementsWeight"
														onkeydown="return onlyNumsAllowed(event)"
														onblur="return isFloat(this);"
														onkeyup="valid(this,'special')" maxlength="12" size="12"
														cssClass="input-text" readonly="false"
														cssStyle="text-align:right" /></td>
												<td align="left" valign="top" class="listwhitetext"><s:checkbox
														key="imfEntitlements.checkSurfaceEntitlementsWeight"
														value="${ischecked1}" fieldValue="true"
														onclick="changeStatus();" />Required&nbsp;?
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.freight" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.surfaceEntitlementsFreight"
														onkeydown="return onlyNumsAllowed(event)"
														onblur="valid(this,'special');CalulateTotalNumber();CalulateTotalSurfaceEntitlements();"
														maxlength="12" size="12" cssClass="input-text"
														cssStyle="text-align:right" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.origin" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.surfaceEntitlementsOrigin"
														onkeydown="return onlyNumsAllowed(event)"
														onblur="valid(this,'special');CalulateTotalNumber();CalulateTotalSurfaceEntitlements();"
														maxlength="12" size="12" cssClass="input-text"
														cssStyle="text-align:right" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.destination" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.surfaceEntitlementsDestination"
														onkeydown="return onlyNumsAllowed(event)"
														onblur="valid(this,'special');CalulateTotalNumber();CalulateTotalSurfaceEntitlements();"
														maxlength="12" size="12" cssClass="input-text"
														cssStyle="text-align:right" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><B>Total&nbsp;Surface&nbsp;Entitlements</B></td>
												<td align="left"><s:textfield
														name="imfEntitlements.surfaceEntitlementsTotal"
														maxlength="12" size="12" cssClass="input-textUpper"
														readonly="true" cssStyle="text-align:right" /></td>
											</tr>
										</table>
									</td>

									<td>
										<table style="margin: 0px;" border="0">
											<tr>
												<c:if test="${empty customerFile.id}">
													<td align="right" colspan="2"><img
														src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
														HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
												</c:if>
												<c:if test="${not empty customerFile.id}">
													<c:choose>
														<c:when
															test="${countImfEntitlementNotes == '0' || countImfEntitlementNotes == '' || countImfEntitlementNotes == null}">
															<td align="right" colspan="2"><img
																id="countImfEntitlementNotesImage"
																src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
																HEIGHT=17 WIDTH=50
																onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=IMF Entitlement Notes&imageId=countImfEntitlementNotesImage&fieldId=countImfEntitlementNotes&decorator=popup&popup=true',800,600);" /><a
																onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=IMF Entitlement Notes&imageId=countImfEntitlementNotesImage&fieldId=countImfEntitlementNotes&decorator=popup&popup=true',800,600);"></a></td>
														</c:when>
														<c:otherwise>
															<td align="right" colspan="2"><img
																id="countImfEntitlementNotesImage"
																src="${pageContext.request.contextPath}/images/notes_open1.jpg"
																HEIGHT=17 WIDTH=50
																onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=IMF Entitlement Notes&imageId=countImfEntitlementNotesImage&fieldId=countImfEntitlementNotes&decorator=popup&popup=true',800,600);" /><a
																onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=IMF Entitlement Notes&imageId=countImfEntitlementNotesImage&fieldId=countImfEntitlementNotes&decorator=popup&popup=true',800,600);"></a></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</tr>
											<tr>
												<td height="10"></td>
											</tr>
											<tr>
												<td align="right" class="listwhitebox"><B>Air&nbsp;+&nbsp;Sea&nbsp;$</B></td>
												<td align="left" width="250"><s:textfield
														name="imfEntitlements.totalFreight" required="true"
														maxlength="16" size="12" readonly="true"
														cssClass="input-textUpper" cssStyle="text-align:right" /></td>
											</tr>

											<tr>
												<td align="right" width="85" class="listwhitetext"><fmt:message
														key="imfEntitlements.readyToSend" /></td>
												<c:if test="${not empty imfEntitlements.readyToSend}">
													<s:text id="imfEntitlementsDateFormattedValue"
														name="${FormDateValue}">
														<s:param name="value" value="imfEntitlements.readyToSend" />
													</s:text>
													<td><s:textfield cssClass="input-textUpper"
															id="readyToSend" name="imfEntitlements.readyToSend"
															value="%{imfEntitlementsDateFormattedValue}"
															readonly="true" size="7" maxlength="11"
															onkeydown="return onlyDel(event,this)"
															onfocus="changeStatus();" /><img id="readyToSend_trigger"
														style="vertical-align: bottom"
														src="${pageContext.request.contextPath}/images/calender.png"
														HEIGHT=20 WIDTH=20 /></td>
												</c:if>
												<c:if test="${empty imfEntitlements.readyToSend}">
													<td><s:textfield cssClass="input-textUpper"
															id="readyToSend" name="imfEntitlements.readyToSend"
															size="7" maxlength="11" readonly="true"
															onkeydown="return onlyDel(event,this)"
															onfocus="changeStatus();" /><img id="readyToSend_trigger"
														style="vertical-align: bottom"
														src="${pageContext.request.contextPath}/images/calender.png"
														HEIGHT=20 WIDTH=20 /></td>
												</c:if>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.sent" /></td>
												<c:if test="${not empty imfEntitlements.sent}">
													<s:text id="sentDateFormattedValue" name="${FormDateValue}">
														<s:param name="value" value="imfEntitlements.sent" />
													</s:text>
													<td><s:textfield cssClass="input-textUpper" id="sent"
															name="imfEntitlements.sent"
															value="%{sentDateFormattedValue}" readonly="true"
															size="7" maxlength="11"
															onkeydown="return onlyDel(event,this)"
															onfocus="changeStatus();" /><img id="sent_trigger"
														style="vertical-align: bottom"
														src="${pageContext.request.contextPath}/images/calender.png"
														HEIGHT=20 WIDTH=20 /></td>
												</c:if>
												<c:if test="${empty imfEntitlements.sent}">
													<td><s:textfield cssClass="input-textUpper" id="sent"
															name="imfEntitlements.sent" size="7" maxlength="11"
															readonly="true" onkeydown="return onlyDel(event,this)"
															onfocus="changeStatus();" /><img id="sent_trigger"
														style="vertical-align: bottom"
														src="${pageContext.request.contextPath}/images/calender.png"
														HEIGHT=20 WIDTH=20 /></td>
												</c:if>
											</tr>



										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table border="0" style="margin: 0px;">
											<tr>
												<td align="right" colspan="2" class="listwhitetext-h1"
													style="padding-right: 5px;"><B>Office&nbsp;Entitlements</B></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.weight" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.officeEntitlementsWeight"
														onkeydown="return onlyNumsAllowed(event)"
														onblur="return isFloat(this);"
														onkeyup="valid(this,'special')" maxlength="12" size="12"
														cssClass="input-text" readonly="false"
														cssStyle="text-align:right" /></td>
												<td align="left" valign="top" class="listwhitetext"><s:checkbox
														key="imfEntitlements.checkOfficeEntitlementsWeight"
														value="${ischecked3}" fieldValue="true"
														onclick="changeStatus();" />Required&nbsp;?
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.freight" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.officeEntitlementsFreight"
														onblur="valid(this,'special');CalulateTotalOfficeEntitlements();"
														onkeydown="return onlyNumsAllowed(event)" maxlength="12"
														size="12" cssClass="input-text" readonly="false"
														cssStyle="text-align:right" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.origin" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.officeEntitlementsOrigin"
														onblur="valid(this,'special');CalulateTotalOfficeEntitlements();"
														onkeydown="return onlyNumsAllowed(event)" maxlength="12"
														size="12" cssClass="input-text" readonly="false"
														cssStyle="text-align:right" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.destination" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.officeEntitlementsDestination"
														onblur="valid(this,'special');CalulateTotalOfficeEntitlements();"
														onkeydown="return onlyNumsAllowed(event)" maxlength="12"
														size="12" cssClass="input-text" readonly="false"
														cssStyle="text-align:right" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><B>Total&nbsp;Office&nbsp;Entitlements</B></td>
												<td align="left"><s:textfield
														name="imfEntitlements.officeEntitlementsTotal"
														maxlength="12" size="12" cssClass="input-textUpper"
														readonly="true" cssStyle="text-align:right" /></td>
											</tr>
										</table>
									</td>

									<td style="">
										<table border="0" style="width: 340px; margin: 0px;">
											<tr>
												<td align="right" colspan="2" class="listwhitetext-h1"
													style="padding-right: 5px;"><B>Auto&nbsp;Entitlements</B></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.weight" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.autoEntitlementsWeight"
														onkeydown="return onlyNumsAllowed(event)"
														onblur="return isFloat(this);"
														onkeyup="valid(this,'special')" maxlength="12" size="12"
														cssClass="input-text" readonly="false"
														cssStyle="text-align:right" /></td>
												<td align="left" valign="top" class="listwhitetext"><s:checkbox
														key="imfEntitlements.checkAutoEntitlementsWeight"
														value="${ischecked2}" fieldValue="true"
														onclick="changeStatus();" />Required&nbsp;?
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.freight" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.autoEntitlementsFreight"
														onblur="valid(this,'special');CalulateTotalAutoEntitlements();"
														onkeydown="return onlyNumsAllowed(event)" maxlength="12"
														size="12" cssClass="input-text" readonly="false"
														cssStyle="text-align:right" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.origin" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.autoEntitlementsOrigin"
														onblur="valid(this,'special');CalulateTotalAutoEntitlements();"
														onkeydown="return onlyNumsAllowed(event)" maxlength="12"
														size="12" cssClass="input-text" readonly="false"
														cssStyle="text-align:right" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.destination" /></td>
												<td align="left"><s:textfield
														name="imfEntitlements.autoEntitlementsDestination"
														onblur="valid(this,'special');CalulateTotalAutoEntitlements();"
														onkeydown="return onlyNumsAllowed(event)" maxlength="12"
														size="12" cssClass="input-text" readonly="false"
														cssStyle="text-align:right" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext"><B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;Auto&nbsp;Entitlements</B></td>
												<td align="left"><s:textfield
														name="imfEntitlements.autoEntitlementsTotal"
														maxlength="12" size="12" cssClass="input-textUpper"
														readonly="true" cssStyle="text-align:right" /></td>
											</tr>




										</table>
									</td>

									<td style="">
										<table style="margin: 0px;">
											<tr>

												<td align="right" class="listwhitetext"><fmt:message
														key="imfEntitlements.permStorage" /></td>
												<td align="left" valign="top" class="listwhitetext"><s:checkbox
														key="imfEntitlements.permStorage" value="${ischecked4}"
														fieldValue="true" onclick="changeStatus();" />
											</tr>
											<tr>
												<td align="right" width="80px" class="listwhitetext"><fmt:message
														key="imfEntitlements.costShare" /></td>
												<td align="left"><s:select cssClass="list-menu"
														name="imfEntitlements.costShare"
														list="{'NO','ED','Res Rep'}" headerKey="" headerValue=""
														cssStyle="width:89px" onchange="changeStatus();" /></td>
											</tr>
											<tr>
												<td align="center" colspan="2" class="listwhitetext">&nbsp;</td>
											</tr>


										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</configByCorp:fieldVisibility>

<c:if test="${usertype!='AGENT'}">
				<tr>
					<td height="10" colspan="6">&nbsp;</td>
				</tr>

				<tr>
					<td height="10" width="100%" colspan="10" align="left"
						style="margin: 0px">
						<div style="margin: 0px">
							<table cellpadding="0" cellspacing="0" width="100%"
								style="margin: 0px">
								<tr>
									<td class="headtab_left"></td>
									<td NOWRAP class="headtab_center" style="cursor: default;">&nbsp;&nbsp;HR
										Details</td>
									<td width="28" valign="top" class="headtab_bg"></td>
									<td class="headtab_bg_center">&nbsp;</td>
									<td class="headtab_right"></td>
								</tr>
							</table>
						</div>
						<div id="hr" class="switchgroup1">
							<table border="0" class="no-mp">
							<tr>
							<td>
							<table border="0" class="detailTabLabel">
								<tbody>
									<tr>
										<td colspan="5">
											<table width="100%" style="margin: 0px;">
												<tr>
												<td align="center" class="listwhitetext" style="padding-left:11%;"><b><u>Local&nbsp;Country</u></b></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>

										<td align="right" class="listwhitetext" width="200px">Name&nbsp;Of&nbsp;Local&nbsp;responsible&nbsp;HR&nbsp;Person</td>
										<td><s:textfield cssClass="input-text"
												name="customerFile.localHr" cssStyle="width:220px;"
												maxlength="30"
												onkeydown="return onlyCharsAllowed1(event,this)" /></td>
										<configByCorp:fieldVisibility
											componentId="component.customerFile.HRPersonDetails">
											<td><img align="left" class="openpopup" width="17"
												height="20" onclick="getContactForHr();"
												src="<c:url value='/images/plus-small.png'/>" /></td>
										</configByCorp:fieldVisibility>
									</tr>
									<tr>

										<td align="right" class="listwhitetext">Phone</td>
										<td><s:textfield cssClass="input-text"
												name="customerFile.localHrPhone" cssStyle="width:220px;"
												maxlength="20" /></td>
										<td width="10px"></td>
									</tr>
									<tr>

										<td align="right" class="listwhitetext">Email&nbsp;Address</td>
										<td><s:textfield cssClass="input-text"
												name="customerFile.localHrEmail" cssStyle="width:220px;"
												maxlength="65" onkeydown="" /></td>
										<td width="10px"></td>
									</tr>
									<tr>
										<td align="right"></td>
									</tr>
								</tbody>
							</table>
							</td>
							<td>
							<table border="0" class="detailTabLabel">
							<tr>
								<td colspan="5">
									<table width="100%" style="margin: 0px;">
										<tr>
											<td align="center" class="listwhitetext" width="50%"><b><u>Home&nbsp;Country</u></b></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
					        	<td align="right" class="listwhitetext">Name&nbsp;of&nbsp;responsible&nbsp;HR&nbsp;Person</td>
								<td colspan="3"><s:textfield cssClass="input-text" name="customerFile.authorizedBy" cssStyle="width:220px" tabindex="" maxlength="30" onkeydown="return onlyCharsAllowed1(event,this)"  /></td>
								</tr>
								
								<tr>
								<td align="right" class="listwhitetext">Phone</td>   
								<td colspan="3"><s:textfield cssClass="input-text" name="customerFile.authorizedPhone"  cssStyle="width:220px" maxlength="20"  tabindex=""/></td>
								</tr>
								<tr>
						        <td align="right" class="listwhitetext">Email&nbsp;Address</td>
								<td colspan="3"><s:textfield cssClass="input-text" name="customerFile.authorizedEmail" cssStyle="width:220px" maxlength="65" tabindex=""  onkeydown="" onchange="validateEmail(this);"/>
								<a href="javascript:sendEmailNew(document.forms['customerFileForm'].elements['customerFile.authorizedEmail'].value)">
								<img class="openpopup" style="vertical-align:top;" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.authorizedEmail}"/></a></td>
								</tr>
							</table>
							</td>
							</tr>
							</table>
						</div>
					</td>
				</tr>
				
				
				
				<tr>
					<td height="10" colspan="6" align="right" valign="top"
						class="vertlinedata">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="15">
						<table class="detailTabLabel" border="0" cellpadding="1"
							cellspacing="2">
							<tbody>
								<tr>
									<td align="left">
										<table class="detailTabLabel" border="0" cellpadding="1"
											cellspacing="2">
											<tr>
												<td align="right" class="listwhitetext"
													style="width: 197px;">Start Date Of Contract/Stay</td>
												<c:if test="${not empty customerFile.contractStart}">
													<s:text id="customerFileContractStartFormattedValue"
														name="${FormDateValue}">
														<s:param name="value" value="customerFile.contractStart" />
													</s:text>
													<td width="95px"><s:textfield cssClass="input-text"
															id="contractStart" name="customerFile.contractStart"
															value="%{customerFileContractStartFormattedValue}"
															required="true" cssStyle="width:65px;" maxlength="11"
															readonly="true" onfocus="changeStatus();"
															onkeydown="onlyDel(event,this)" /> <img
														id="contractStart_trigger" style="vertical-align: bottom"
														src="${pageContext.request.contextPath}/images/calender.png"
														HEIGHT=20 WIDTH=20
														onclick="setContractValidFlag('S');forDays(); return false;" />
													</td>
												</c:if>
												<c:if test="${empty customerFile.contractStart}">
													<td width="95px"><s:textfield cssClass="input-text"
															id="contractStart" name="customerFile.contractStart"
															required="true" cssStyle="width:65px;" maxlength="11"
															readonly="true" onfocus="changeStatus();"
															onkeydown="onlyDel(event,this)" /> <img
														id="contractStart_trigger" style="vertical-align: bottom"
														src="${pageContext.request.contextPath}/images/calender.png"
														HEIGHT=20 WIDTH=20
														onclick="setContractValidFlag('S');forDays(); return false;" />
													</td>
												</c:if>
												<td align="right" class="listwhitetext" width="172px">End
													Date Of Contract/Stay</td>
												<c:if test="${not empty customerFile.contractEnd}">
													<s:text id="customerFileContractEndFormattedValue"
														name="${FormDateValue}">
														<s:param name="value" value="customerFile.contractEnd" />
													</s:text>
													<td><s:textfield cssClass="input-text"
															id="contractEnd" name="customerFile.contractEnd"
															value="%{customerFileContractEndFormattedValue}"
															required="true" cssStyle="width:65px;" maxlength="11"
															readonly="true" onfocus="changeStatus();"
															onkeydown="onlyDel(event,this)" /> <img
														id="contractEnd_trigger" style="vertical-align: bottom"
														src="${pageContext.request.contextPath}/images/calender.png"
														HEIGHT=20 WIDTH=20
														onclick="setContractValidFlag('E');forDays(); return false;" />
													</td>
												</c:if>
												<c:if test="${empty customerFile.contractEnd}">
													<td><s:textfield cssClass="input-text"
															id="contractEnd" name="customerFile.contractEnd"
															required="true" cssStyle="width:65px;" maxlength="11"
															readonly="true" onfocus="changeStatus();"
															onkeydown="onlyDel(event,this)" /> <img
														id="contractEnd_trigger" style="vertical-align: bottom"
														src="${pageContext.request.contextPath}/images/calender.png"
														HEIGHT=20 WIDTH=20
														onclick="setContractValidFlag('E');forDays(); return false;" />
													</td>
												</c:if>
												  <configByCorp:fieldVisibility componentId="component.customerFile.IkeaRequest">
			                                      <td class="listwhitetext" align="right">Assignment End Reason<font color="red" size="2"></font></td>
		                                          <td ><configByCorp:customDropDown listType="map" 	list="${assignmentEndReason}" fieldValue="${customerFile.assignmentEndReason}"
															attribute="class=list-menu style=width:147px;  name=customerFile.assignmentEndReason headerKey='' headerValue='' " />
											      </configByCorp:fieldVisibility>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="10">
										<table class="detailTabLabel" border="0" cellpadding="1"
											cellspacing="2">
											<tr>
												<td align="right" class="listwhitetext" width="198px">Duration
													Of Assignment</td>
												<td><s:textfield cssClass="input-text"
														name="customerFile.duration"
														id="orderInitiationASMLDuration" cssStyle="width:168px;"
														maxlength="12" onselect="" onfocus="calcDays()" /></td>
												<td align="right" colspan="2" class="listwhitetext"
													width="95px">Cost Center</td>
												<td colspan="5"><s:textfield cssClass="input-text"
														name="customerFile.costCenter"
														id="orderInitiationCostCenter" cssStyle="width:281px;"
														maxlength="50" value="${customerFile.costCenter}" /></td>
											</tr>
											<tr>
												<td class="listwhitetext" align="right">Type Of
													Contract</td>
												<td class="listwhitetext" colspan="3"><c:if
														test="${parentAgentForOrderinitiation=='true' || customerFile.accountCode=='T56292'}">
														<configByCorp:customDropDown listType="map"
															list="${assignmentTypesASML_isactive}"
															fieldValue="${customerFile.assignmentType}"
															attribute="class=list-menu style=width:173px; id=assignmentType name=customerFile.assignmentType headerKey='' headerValue='' onclick=changeStatus();checkAssignmentType();" />
													</c:if> <c:if
														test="${parentAgentForOrderinitiation!='true' && customerFile.accountCode!='T56292' }">
														<configByCorp:customDropDown listType="map"
															list="${assignmentTypes_isactive}"
															fieldValue="${customerFile.assignmentType}"
															attribute="class=list-menu style=width:173px; id=assignmentType name=customerFile.assignmentType headerKey='' headerValue='' onclick=changeStatus();checkAssignmentType();" />
													</c:if></td>
												<td align="left" colspan="5"><s:textfield
														id="otherContract" cssClass="input-text"
														name="customerFile.otherContract" cssStyle="width:110px;"
														maxlength="30" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext">Home
													Country&nbsp;</td>
												<td colspan="2"><configByCorp:customDropDown
														listType="map" list="${ocountry_isactive}"
														fieldValue="${customerFile.homeCountry}"
														attribute="class=list-menu name=customerFile.homeCountry style=width:173px  onchange=changeStatus();getHomeCountryCode(this);autoPopulate_customerFile_homeCountry(this);gethomeState(this);enableStateListHome();  headerKey='' headerValue=''" /><img
													src="${pageContext.request.contextPath}/images/globe.png"
													onclick="openHomeCountryLocation();"
													style="vertical-align: bottom;" "/></td>
												<td align="right" class="listwhitetext" width="40">State</td>
												<td width="60"><configByCorp:customDropDown
														listType="map" list="${homeStates_isactive}"
														fieldValue="${customerFile.homeState}"
														attribute="class=list-menu id=homeState name=customerFile.homeState style=width:140px onchange=changeStatus(); headerKey='' headerValue=''" />
												</td>
												<td align="right" class="listwhitetext" width="24">City</td>
												<td width="60"><s:textfield cssClass="input-text"
														name="customerFile.homeCity" cssStyle="width:110px;"
														maxlength="30" onblur="" /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext">Job Function</td>
												<td><s:textfield cssClass="input-text"
														name="customerFile.jobFunction" cssStyle="width:168px;"
														maxlength="65" onkeydown="" /></td>
												<td width="10px"></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext">Back To Back
													Assignment</td>
												<td align="left" width="115"><s:select
														cssClass="list-menu"
														name="customerFile.backToBackAssignment"
														list="%{visaRequiredList}" cssStyle="width:172px"
														headerKey="" headerValue="" /></td>
												<td width="10px"></td>
											</tr>
										</table>
									</td>
								</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="6">
						<div id="parentAgentForOrderinitiationDiv">
							<div onClick="javascript:animatedcollapse.toggle('removal')"
								style="margin: 0px">
								<table cellpadding="" cellspacing="0" border="0" width="100%"
									style="margin: 0px">
									<tr>
										<td class="headtab_left"></td>
										<td NOWRAP class="headtab_center">&nbsp;&nbsp;Removal/
											Relocation&nbsp;Services</td>
										<td width="28" valign="top" class="headtab_bg"></td>
										<td class="headtab_bg_center">&nbsp;</td>
										<td class="headtab_right"></td>
									</tr>
								</table>
							</div>
							<div id="removal" class="switchgroup1">
								<table border="0" class="detailTabLabel" cellpadding="0"
									width="100%">
									<tbody>
										<tr>
											<c:if test="${parentAgentForOrderinitiation=='true'}">
												<td>
													<div id="activity"
														style="margin: 0px; padding: 0px; border: 1px solid #C0C0C0; margin-left: 0px; margin-bottom: 0.3em;">
														<c:choose>  
	                                                    <c:when test="${newPage=='N'}">
														<c:if test="${empty customerFile.id}">
															<iframe
																src="editRemovalRelocationService.html?decorator=asml&popup=true"
																height="250px"
																style="margin-top: 0px; padding-top: 0px;" WIDTH="100%"
																FRAMEBORDER=0 name="relo">
																<p>Your browser does not support iframes.</p>
															</iframe>
														</c:if>
														<c:if test="${not empty customerFile.id}">
															<iframe
																src="editRemovalRelocationService.html?decorator=asml&popup=true&customerFileId=${customerFile.id}"
																height="250px"
																style="margin-top: 0px; padding-top: 0px;" WIDTH="100%"
																FRAMEBORDER=0 name="relo">
																<p>Your browser does not support iframes.</p>
															</iframe>
														</c:if>
														</c:when>
														
	                                                     <c:when test="${newPage=='Y'}">
	                                                     <c:if test="${empty customerFile.id}">
															<iframe
																src="editRemovalRelocationNewService.html?decorator=asml&popup=true"
																height="250px"
																style="margin-top: 0px; padding-top: 0px;" WIDTH="100%"
																FRAMEBORDER=0 name="relo">
																<p>Your browser does not support iframes.</p>
															</iframe>
														</c:if>
														<c:if test="${not empty customerFile.id}">
															<iframe
																src="editRemovalRelocationNewService.html?decorator=asml&popup=true&customerFileId=${customerFile.id}"
																height="250px"
																style="margin-top: 0px; padding-top: 0px;" WIDTH="100%"
																FRAMEBORDER=0 name="relo">
																<p>Your browser does not support iframes.</p>
															</iframe>
														</c:if>
														</c:when>
														</c:choose>
													</div>
												</td>
											</c:if>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="25"><div id="entitlementDiv">
							<table class="detailTabLabel" cellspacing="0" cellpadding="0"
								width="100%" border="0">
								<tr>
									<td width="208" align="right" valign="top"
										class="listwhitetext">Comments:&nbsp;</td>
									<td width="" align="left"><textarea
											name="customerFile.entitled"
											style="width:558px;height:55px;" class="textarea"  maxlength="250"><c:out value='${customerFile.entitled}'/></textarea></td>
									<td width="180"></td>
									<c:if test="${empty customerFile.id}">
										<td align="center" style="position: absolute; right: 45px;"
											valign="bottom"><img
											src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
											height=17 width=50 onclick="notExists();" /></td>
									</c:if>
									<c:if test="${not empty customerFile.id}">
										<c:choose>
											<c:when
												test="${countEntitlementNotes == '0' || countEntitlementNotes == '' || countEntitlementNotes == null}">
												<td align="center" style="position: absolute; right: 45px;"
													valign="bottom"><img id="countEntitlementNotesImage"
													src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
													height=17 width=50
													onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',800,600);" /><a
													onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',800,600);"></a></td>
											</c:when>
											<c:otherwise>
												<td align="center" style="position: absolute; right: 45px;"
													valign="bottom"><img id="countEntitlementNotesImage"
													src="${pageContext.request.contextPath}/images/notes_open1.jpg"
													height=17 width=50
													onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',800,600);" /><a
													onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',800,600);"></a></td>
											</c:otherwise>
										</c:choose>
									</c:if>
								</tr>
								<tr>
									<td height="4" colspan="6"></td>
								</tr>
							</table>
						</div></td>
				</tr>
				
				<!-- 13402 - Initiationpage in Account Portal Inter IKEA -->
				<tr>
				<td colspan="15">
				<table style="margin:0px 0px 0px 62px;padding:0px;">
				<tr>
				<td align="right" class="listwhitetext">Original&nbsp;Company&nbsp;Code</td>
		      	<td colspan="5" align="left">
		      	<table class="no-mp" cellspacing="0" cellpadding="0" border="0">
		      	<tr>
	 			<td><s:textfield cssClass="input-text"	id="originalCompanyCodeId" name="customerFile.originalCompanyCode" cssStyle="width:65px;" tabindex="" maxlength="8"
							onchange="valid(this,'special');findOrderInitiationBillToCode();" />
				<img style="vertical-align:top;padding-right:12px;" class="openpopup" id="originalCompanyCode.popupImage" width="17" height="20" onclick="openOriginalCompanyPopUp();document.forms['customerFileForm'].elements['customerFile.originalCompanyCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" />
				</td>
	 
	 			<td align="left" ><s:textfield	cssClass="input-text" id="originalCompanyNameId" name="customerFile.originalCompanyName" cssStyle="width:250px;" tabindex="" maxlength="250"
									onkeyup="findPartnerDetails('originalCompanyNameId','originalCompanyCodeId','originalCompanyNameDiv',' and (isAccount is true or isAgent is true or isCarrier is true or isPrivateParty is true)',document.forms['customerFileForm'].elements['customerFile.companyDivision'].value,event);"
									onchange="findPartnerDetailsByName('originalCompanyCodeId','originalCompanyNameId');" /> 
			
				</td>	
				<configByCorp:fieldVisibility componentId="component.customerFile.IkeaRequest">
				<td>
					<img align="top" align ="right" id="viewDetails" class="openpopup" width="17" height="20" title="View details"onclick="viewPartnerDetails(this,'originalCompanyCodeId');"src="<c:url value='/images/address2.png'/>" />
				</td>
				</configByCorp:fieldVisibility>		
					<div id="originalCompanyNameDiv" class="autocomplete" style="z-index: 9999; position: absolute; margin-top:2px;"></div>	
				</tr>
				</table>
				</td>
				</tr>
				<tr>
				<td align="right" class="listwhitetext">Original&nbsp;Company&nbsp;Hiring&nbsp;Date</td>
				<c:if test="${not empty customerFile.originalCompanyHiringDate}">
					<s:text id="customerFileOriginalCompanyFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.originalCompanyHiringDate"/></s:text>
					<td style="width:100px;"> <s:textfield cssClass="input-text" id="originalCompanyHiringDate" name="customerFile.originalCompanyHiringDate" value="%{customerFileOriginalCompanyFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/>
						<img id="originalCompanyHiringDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
			    	</td>
			    </c:if>
			    <c:if test="${empty customerFile.originalCompanyHiringDate}">
					 <td style="width:100px;"><s:textfield cssClass="input-text" id="originalCompanyHiringDate" name="customerFile.originalCompanyHiringDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/>
						<img id="originalCompanyHiringDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
					</td>
				</c:if>
				
				<c:set var="isfirstInternationalMoveFlag" value="false"/>
			<c:if test="${customerFile.firstInternationalMove}">
				<c:set var="isfirstInternationalMoveFlag" value="true"/>
			</c:if>
             
             <td style="text-align:right;width:10px;"><s:checkbox id="customerFile.firstInternationalMove" cssStyle="margin:0px;" tabindex="" key="customerFile.firstInternationalMove" value="${isfirstInternationalMoveFlag}" /></td>
			<td align="left" class="listwhitetext">1st International Move</td>
			</tr>
			<tr>		
			<configByCorp:fieldVisibility componentId="component.customerFile.IkeaRequest">
			             <td align="right" class="listwhitetext">Current company of employment</td>
	         <td width=""><s:textfield cssClass="input-text"	id="currentEmpCode" name="customerFile.currentEmploymentCode" tabindex="32" cssStyle="width:65px;" maxlength="8"
							onchange="valid(this,'special');findCureentEmployName();" />
		    <img style="vertical-align:top;" class="openpopup" id="currentEmpCode.popupImage" width="17" height="20" onclick="opencurrentEmployeeCodePopUp();document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	         <td align="left" colspan="2"><s:textfield	cssClass="input-text" id="curretEmpNameId"	name="customerFile.currentEmploymentName" cssStyle="width:250px;" tabindex="33" maxlength="250"
									onkeyup="findPartnerDetails('curretEmpNameId','currentEmpCode','currentEmployeeNameDiv',' and (isAccount is true or isAgent is true or isCarrier is true or isPrivateParty is true)',document.forms['customerFileForm'].elements['customerFile.companyDivision'].value,event);"
									onchange="findPartnerDetailsByName('currentEmpCode','curretEmpNameId');" />
									</td>
									<td>
					<img align="top" align = "right" id="viewDetails" class="openpopup" width="17" height="20" title="View details"onclick="viewPartnerDetails(this,'currentEmpCode');"src="<c:url value='/images/address2.png'/>" />
				</td>
		<div id="currentEmployeeNameDiv" class="autocomplete" style="z-index: 9999; position: absolute; margin-top: 25px; left: 226px;">
		</div>
		 	</configByCorp:fieldVisibility>
			</tr>
			</table>
			</td>
			</tr>
			<!-- End IKEA -->
			<tr><td height="4" colspan="6"></td></tr>
			</c:if>
				<c:if test="${usertype=='AGENT'}">
				<s:hidden name="customerFile.localHr" />
				<s:hidden name="customerFile.localHrPhone" />
				<s:hidden name="customerFile.localHrEmail" />
				<c:if test="${not empty customerFile.contractStart}">
		             <s:text id="customerFileContractStartFormattedValue" name="${FormDateValue}"> <s:param name="value" value="customerFile.contractStart" /></s:text>
			         <s:hidden  name="customerFile.contractStart" value="%{customerFileContractStartFormattedValue}" /> 
	            </c:if>
	            <c:if test="${empty customerFile.contractStart}">
		             <s:hidden   name="customerFile.contractStart"/> 
	            </c:if> 
				<c:if test="${not empty customerFile.contractEnd}">
		             <s:text id="customerFileContractEndFormattedValue" name="${FormDateValue}"> <s:param name="value" value="customerFile.contractEnd" /></s:text>
			         <s:hidden  name="customerFile.contractEnd" value="%{customerFileContractEndFormattedValue}" /> 
	            </c:if>
	            <c:if test="${empty customerFile.contractEnd}">
		             <s:hidden   name="customerFile.contractEnd"/> 
	           </c:if> 
				<s:hidden name="customerFile.duration" />
				<s:hidden name="customerFile.costCenter" />
				<s:hidden name="customerFile.assignmentType" />
				<s:hidden name="customerFile.otherContract" />
				<s:hidden name="customerFile.homeCountry" />
				<s:hidden name="customerFile.homeState" />
				<s:hidden name="customerFile.homeCity" />
				<s:hidden name="customerFile.jobFunction" />
				<s:hidden name="customerFile.backToBackAssignment" />
				<s:hidden name="customerFile.entitled" />
				<s:hidden name="customerFile.originalCompanyCode" />
				<s:hidden name="customerFile.originalCompanyName" />
					<c:if test="${not empty customerFile.originalCompanyHiringDate}">
		             <s:text id="customerFileOriginalCompanyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="customerFile.originalCompanyHiringDate" /></s:text>
			         <s:hidden  name="customerFile.originalCompanyHiringDate" value="%{customerFileOriginalCompanyFormattedValue}" /> 
	            </c:if>
	            <c:if test="${empty customerFile.originalCompanyHiringDate}">
		        <s:hidden   name="customerFile.originalCompanyHiringDate"/> 
	            </c:if> 
				<s:hidden name="customerFile.firstInternationalMove" />
				<s:hidden name="customerFile.currentEmploymentCode" />
				<s:hidden name="customerFile.currentEmploymentName" />
				</c:if>
			</table>
		</div>
	</td>
</tr> 
<tr>
	<td height="10" width="100%" align="left" style="margin: 0px">
		<div
			onClick="javascript:animatedcollapse.toggle('family');loadFamilyDetails('LOAD');"
			style="margin: 0px">
			<table cellpadding="0" cellspacing="0" border="0" width="100%"
				style="margin: 0px">
				<tr>
					<td class="headtab_left"></td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Family&nbsp;Details</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;</td>
					<td class="headtab_right"></td>
				</tr>
			</table>
		</div>
		<div id="family" class="switchgroup1">
		
			<table class="detailTabLabel" border="0" width="100%">
				<tbody>
					<tr>
						<td align="left" height="5px"></td>
					</tr>
					<tr>
						<td align="left" colspan="10">
							<%
								int count = 0;
							%>
							<div id="familyDetailsAjax"></div> <c:if
								test="${not empty customerFile.id}">
								<input type="button" class="cssbutton1"
									onclick="window.open('editDsFamilyDetails.html?decorator=popup&popup=true&customerFileId=${customerFile.id}','sqlExtractInputForm','height=500,width=1350,top=50,left=200, scrollbars=yes,resizable=yes').focus();"
									value="Add Family Details" style="width: 120px;" />
							</c:if> <c:if test="${empty customerFile.id}">
								<input type="button" class="cssbutton1"
									onclick="massageFamily();" value="Add Family Details"
									style="width: 120px;" />
							</c:if> <c:if test="${empty dsFamilyDetailsList}">
								<span style="padding-left: 20px;" class="listwhitetext">Family
									Size <s:textfield cssClass="input-text"
										name="customerFile.familysize" id="customerFile.familysize"
										onchange="getFamilysize('FS');" size="2" maxlength="2" />
								</span>
							</c:if> <c:if test="${not empty dsFamilyDetailsList}">
								<span style="padding-left: 20px;" class="listwhitetext">Family
									Size <s:textfield cssClass="input-textUpper"
										name="customerFile.familysize" id="customerFile.familysize"
										onchange="getFamilysize('FS');" size="2" maxlength="2" />
								</span>
							</c:if>
							<span style="padding-left: 20px;" class="listwhitetext">Family&nbsp;Situation  
                            <s:select cssClass="list-menu" name="customerFile.familySitiation" list="%{familySitiationList}" onchange="" cssStyle="width:163px" tabindex="17" headerKey="" headerValue="" />
                            </span>
							 <%--
<div id="para1" >
<s:set name="dsFamilyDetailsList" value="dsFamilyDetailsList" scope="request"/>

<display:table name="dsFamilyDetailsList" class="table" requestURI="" id="dsFamilyDetailsList"  defaultsort="1" pagesize="10" style="width:100%;">
    <display:column title="First Name">
    <a href="javascript:openWindow('editDsFamilyDetails.html?id=${dsFamilyDetailsList.id}&customerFileId=${customerFile.id}&decorator=popup&popup=true',750,400)">
    <c:out value="${dsFamilyDetailsList.firstName}"></c:out></a>
    <% count++; %>
    </display:column>
    <display:column property="lastName"  title="Last Name" />
    <display:column property="relationship"  title="Relationship" />   
    <display:column property="cellNumber"  title="Cell #"  />
    <display:column property="email" title="Email"  />
    <display:column property="passportNumber" title="Passport #"  /> 
    <display:column property="dateOfBirth" title="Date Of Birth" format="{0,date,dd-MMM-yyyy}" />
	<display:column property="expiryDate" title="Expiry Date" format="{0,date,dd-MMM-yyyy}" />
	<display:column property="countryOfIssue"  title="Country Of Issue" /> 
	<c:choose>
	<c:when test="${usertype=='ACCOUNT' || usertype=='AGENT'}">	
	</c:when>
	<c:otherwise>
	<display:column title="Remove" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="confirmSubmit('${dsFamilyDetailsList.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
	</display:column>
	</c:otherwise>
	</c:choose>
	
	</display:table>
	<c:if test="${not empty customerFile.id}">
	<input type="button" class="cssbutton1" onclick="window.open('editDsFamilyDetails.html?decorator=popup&popup=true&customerFileId=${customerFile.id}','sqlExtractInputForm','height=500,width=725,top=0, scrollbars=yes,resizable=yes').focus();" 
	        value="Add Family Details" style="width:120px; height:25px" /> 
     </c:if>
     <c:if test="${empty customerFile.id}">
	<input type="button" class="cssbutton1" onclick="massageFamily();" 
	        value="Add Family Details" style="width:120px; height:25px" /> 
     </c:if>
     <c:if test="${empty dsFamilyDetailsList}">
     <span style="padding-left:20px;" class="listwhitetext">Family Size <s:textfield cssClass="input-text" name="customerFile.familysize" id="customerFile.familysize" onchange="getFamilysize('FS');" size="2" maxlength="2"  /></span>
	</c:if>
	<c:if test="${not empty dsFamilyDetailsList}">
     <span style="padding-left:20px;" class="listwhitetext">Family Size <s:textfield cssClass="input-textUpper" name="customerFile.familysize" id="customerFile.familysize" onchange="getFamilysize('FS');" size="2" maxlength="2"  /></span>
	</c:if>
</div>
 --%>

						</td>
						
					</tr>
					<tr>

					</tr>
				</tbody>
			</table>
		</div>
	</td>
</tr>
<tr>
	<td height="10" width="100%" align="left" style="margin: 0px">
		<div onClick="javascript:animatedcollapse.toggle('benefits')"
			style="margin: 0px">
			<table cellpadding="0" cellspacing="0" border="0" width="100%"
				style="margin: 0px">
				<tr>
					<td class="headtab_left"></td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Benefits&nbsp;Review
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;</td>
					<td class="headtab_right"></td>
				</tr>
			</table>
		</div>
		<div id="benefits" class="switchgroup1">
			<table class="detailTabLabel" border="0"
				style="width: 100%; margin: 0px;">
				<tbody>
					<tr>
						<td align="left" height="5px"></td>
					</tr>
					<tr>
						<td align="left" colspan="10">
							<div style="width: 100%;">
								<table class="detailTabLabel" border="0">
									<tr>
										<td align="right" class="listwhitetext">Corporate
											Benefits Review</td>
										<c:if test="${not empty customerFile.corporateBenefitsReview}">
											<s:text id="customerFileSubmissionToTranfFormattedValue"
												name="${FormDateValue}">
												<s:param name="value"
													value="customerFile.corporateBenefitsReview" />
											</s:text>
											<td><s:textfield id="corporateBenefitsReview"
													cssClass="input-text"
													name="customerFile.corporateBenefitsReview"
													value="%{customerFileSubmissionToTranfFormattedValue}"
													cssStyle="width:65px" maxlength="11" readonly="true"
													onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td>
											<td><img id="corporateBenefitsReview_trigger"
												style="vertical-align: bottom"
												src="${pageContext.request.contextPath}/images/calender.png"
												HEIGHT=20 WIDTH=20 /></td>
										</c:if>
										<c:if test="${empty customerFile.corporateBenefitsReview}">
											<td><s:textfield id="corporateBenefitsReview"
													cssClass="input-text"
													name="customerFile.corporateBenefitsReview"
													cssStyle="width:65px" maxlength="11" readonly="true"
													onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td>
											<td><img id="corporateBenefitsReview_trigger"
												style="vertical-align: bottom"
												src="${pageContext.request.contextPath}/images/calender.png"
												HEIGHT=20 WIDTH=20 /></td>
										</c:if>
										<td width="10px"></td>
										<td align="right" class="listwhitetext">Exception Request</td>
										<c:if test="${not empty customerFile.exceptionRequest}">
											<s:text id="customerFileSubmissionToTranfFormattedValue"
												name="${FormDateValue}">
												<s:param name="value" value="customerFile.exceptionRequest" />
											</s:text>
											<td><s:textfield id="exceptionRequest"
													cssClass="input-text" name="customerFile.exceptionRequest"
													value="%{customerFileSubmissionToTranfFormattedValue}"
													cssStyle="width:65px" maxlength="11" readonly="true"
													onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td>
											<td><img id="exceptionRequest_trigger"
												style="vertical-align: bottom"
												src="${pageContext.request.contextPath}/images/calender.png"
												HEIGHT=20 WIDTH=20 /></td>
										</c:if>
										<c:if test="${empty customerFile.exceptionRequest}">
											<td><s:textfield id="exceptionRequest"
													cssClass="input-text" name="customerFile.exceptionRequest"
													cssStyle="width:65px" maxlength="11" readonly="true"
													onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td>
											<td><img id="exceptionRequest_trigger"
												style="vertical-align: bottom"
												src="${pageContext.request.contextPath}/images/calender.png"
												HEIGHT=20 WIDTH=20 /></td>
										</c:if>
									</tr>
									<tr>
										<td align="right" class="listwhitetext">Exception Request
											Sent To Corporate Contact</td>
										<c:if
											test="${not empty customerFile.exceptionRequestSentToCorporateContact}">
											<s:text id="customerFileSubmissionToTranfFormattedValue"
												name="${FormDateValue}">
												<s:param name="value"
													value="customerFile.exceptionRequestSentToCorporateContact" />
											</s:text>
											<td><s:textfield
													id="exceptionRequestSentToCorporateContact"
													cssClass="input-text"
													name="customerFile.exceptionRequestSentToCorporateContact"
													value="%{customerFileSubmissionToTranfFormattedValue}"
													cssStyle="width:65px" maxlength="11" readonly="true"
													onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td>
											<td><img
												id="exceptionRequestSentToCorporateContact_trigger"
												style="vertical-align: bottom"
												src="${pageContext.request.contextPath}/images/calender.png"
												HEIGHT=20 WIDTH=20 /></td>
										</c:if>
										<c:if
											test="${empty customerFile.exceptionRequestSentToCorporateContact}">
											<td><s:textfield
													id="exceptionRequestSentToCorporateContact"
													cssClass="input-text"
													name="customerFile.exceptionRequestSentToCorporateContact"
													cssStyle="width:65px" maxlength="11" readonly="true"
													onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td>
											<td><img
												id="exceptionRequestSentToCorporateContact_trigger"
												style="vertical-align: bottom"
												src="${pageContext.request.contextPath}/images/calender.png"
												HEIGHT=20 WIDTH=20 /></td>
										</c:if>
										<td></td>
										<td align="right" class="listwhitetext">Exception
											Response</td>
										<c:if test="${not empty customerFile.exceptionResponse}">
											<s:text id="customerFileSubmissionToTranfFormattedValue"
												name="${FormDateValue}">
												<s:param name="value" value="customerFile.exceptionResponse" />
											</s:text>
											<td><s:textfield id="exceptionResponse"
													cssClass="input-text" name="customerFile.exceptionResponse"
													value="%{customerFileSubmissionToTranfFormattedValue}"
													cssStyle="width:65px" maxlength="11" readonly="true"
													onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td>
											<td><img id="exceptionResponse_trigger"
												style="vertical-align: bottom"
												src="${pageContext.request.contextPath}/images/calender.png"
												HEIGHT=20 WIDTH=20 /></td>
										</c:if>
										<c:if test="${empty customerFile.exceptionResponse}">
											<td><s:textfield id="exceptionResponse"
													cssClass="input-text" name="customerFile.exceptionResponse"
													cssStyle="width:65px" maxlength="11" readonly="true"
													onkeydown="onlyDel(event,this)" onfocus="changeStatus();" /></td>
											<td><img id="exceptionResponse_trigger"
												style="vertical-align: bottom"
												src="${pageContext.request.contextPath}/images/calender.png"
												HEIGHT=20 WIDTH=20 /></td>
										</c:if>

										<c:if test="${empty customerFile.id}">
											<td align="right" style="position: absolute; right: 45px;"
												valign="bottom"><img
												src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
												HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
										</c:if>
										<c:if test="${not empty customerFile.id}">
											<c:choose>
												<c:when
													test="${countDSNotes == '0' || countDSNotes == '' || countDSNotes == null}">
													<td align="right" style="position: absolute; right: 45px;"
														valign="bottom"><img id="countDSNotesImage"
														src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
														HEIGHT=17 WIDTH=50
														onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=DSBenefit&imageId=countDSNotesImage&fieldId=countDSNotes&decorator=popup&popup=true',800,600);" /><a
														onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=DSBenefit&imageId=countDSNotesImage&fieldId=countDSNotes&decorator=popup&popup=true',800,600);"></a></td>
												</c:when>
												<c:otherwise>
													<td align="right" style="position: absolute; right: 45px;"
														valign="bottom"><img id="countDSNotesImage"
														src="${pageContext.request.contextPath}/images/notes_open1.jpg"
														HEIGHT=17 WIDTH=50
														onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=DSBenefit&imageId=countDSNotesImage&fieldId=countDSNotes&decorator=popup&popup=true',800,600);" /><a
														onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=DSBenefit&imageId=countDSNotesImage&fieldId=countDSNotes&decorator=popup&popup=true',800,600);"></a></td>
												</c:otherwise>
											</c:choose>
										</c:if>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</td>
</tr>
<configByCorp:fieldVisibility componentId="component.tab.billing.contractSystem">
<tr>
	<td height="10" width="100%" align="left" style="margin: 0px">
		<div
			onClick="javascript:animatedcollapse.toggle('crm');findCrmDetails();"
			style="margin: 0px">
			<table cellpadding="0" cellspacing="0" width="100%"
				style="margin: 0px">
				<tr>
					<td class="headtab_left"></td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;CRM Section</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;</td>
					<td class="headtab_right"></td>
				</tr>
			</table>
		</div>
		<div id="crm" class="switchgroup1">
			<table  cellpadding="1" cellspacing="0" class="detailTabLabel" border="0" width="100%" >
				<tbody>
					<tr>
					<td align="left"  height="100px">
                   <div id="crmDetailsAjax"></div>
                   </td>
                       </tr>
                         </tbody>
                              </table>
			               </div>
	</td>
</tr>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility
	componentId="component.standard.cPortalActivation">
	<tr>
		<td height="10" width="100%" align="left" style="margin: 0px">
			<div onClick="javascript:animatedcollapse.toggle('cportal')"
				style="margin: 0px">
				<table cellpadding="0" cellspacing="0" width="100%"
					style="margin: 0px">
					<tr>
						<td class="headtab_left"></td>
						<td NOWRAP class="headtab_center">&nbsp;&nbsp;Customer Portal
							Detail</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_special">&nbsp;</td>
						<td class="headtab_right"></td>
					</tr>
				</table>
			</div>
			<div id="cportal" class="switchgroup1">
				<table cellpadding="1" cellspacing="0" class="detailTabLabel">
					<tr>
						<c:set var="isActiveFlag" value="false" />
						<c:if test="${customerFile.portalIdActive}">
							<c:set var="isActiveFlag" value="true" />
						</c:if>
						<td width="119" align="right" class="listwhitetext">Customer
							Portal:</td>
						<c:if test="${cPortalIdValue==false}">
							<td width="16"><s:checkbox
									key="customerFile.portalIdActive" value="${isActiveFlag}"
									fieldValue="true" onclick="changeStatus(),checkPortalBoxId()" /></td>
						</c:if>
						<c:if test="${cPortalIdValue==true}">
							<td width="16"><s:checkbox
									key="customerFile.portalIdActive" value="${isActiveFlag}"
									disabled="true" /></td>
						</c:if>
						<td width="90" align="left" class="listwhitetext"><a href="#"
							style="cursor: help"
							onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.portalIdActive',this);return false">
								<fmt:message key='customerFile.portalIdActive' />
						</a></td>
						<td width="40" align="right" class="listwhitetext">Email:&nbsp;</td>
						<td width="50"><s:textfield cssClass="input-textUpper"
								name="customerFileemail" value="%{customerFile.email}"
								readonly="true" cssStyle="width:236px;" maxlength="65" /></td>

						<c:set var="isSecondaryEmailFlag" value="false" />
						<c:if test="${customerFile.secondaryEmailFlag}">
							<c:set var="isSecondaryEmailFlag" value="true" />
						</c:if>
						<td width="119" align="right" class="listwhitetext">Secondary
							Email:</td>
						<td width="16"><s:checkbox
								key="customerFile.secondaryEmailFlag"
								value="${isSecondaryEmailFlag}" fieldValue="true"
								onclick="changeStatus(),checkPortalBoxSecondaryId()" /></td>
						<td width="119" align="right" class="listwhitetext">Email
							Sent Date:</td>
						<td width="16"><s:textfield cssClass="input-textUpper"
								cssStyle="width:65px;" name="customerFile.emailSentDate"
								readonly="true" /></td>

					</tr>
				</table>

				<table border="0" cellpadding="1" cellspacing="0"
					class="detailTabLabel">
					<tr>
						<td width="119" align="right" class="listwhitetext"><fmt:message
								key='customerFile.customerPortalId' /></td>

						<td align="left" width="154"><s:textfield
								cssClass="input-textUpper" name="customerFile.customerPortalId"
								required="true" size="15" maxlength="80" readonly="true" /></td>

						<c:if test="${not empty customerFile.id}">
							<c:if test="${cPortalIdValue==false}">
								<td><input type="button"
									value="Reset password and email to customer"
									style="width: 240px;" class="cssbuttonB"
									onclick="generatePortalId();" /></td>
							</c:if>
							<c:if test="${cPortalIdValue==true}">
								<td><input type="button"
									value="Reset password and email to customer"
									style="width: 240px;" class="cssbuttonB" disabled="disabled" /></td>
							</c:if>
						</c:if>
						<c:if test="${empty customerFile.id}">
							<td align="left"><input type="button" disabled
								value="Reset password and email to customer"
								style="width: 240px;" class="cssbuttonB"
								onclick="generatePortalId();" /></td>
						</c:if>

						<c:if
							test="${customerFile.cportalEmailLanguage==null || customerFile.cportalEmailLanguage==''}">
							<c:set var="ischecked19" value="true" />
							<c:set var="ischecked18" value="false" />
						</c:if>
						<c:if
							test="${customerFile.cportalEmailLanguage!=null && customerFile.cportalEmailLanguage!=''}">
							<c:set var="ischecked19" value="false" />
							<c:set var="ischecked18" value="false" />
							<c:set var="splittedString1"
								value="${fn:split(customerFile.cportalEmailLanguage, ',')}" />
							<c:forEach var="lang" items="${splittedString1}">
								<c:if test="${fn:indexOf(lang,'ENGLISH')>-1}">
									<c:set var="ischecked18" value="true" />
								</c:if>
								<c:if test="${fn:indexOf(lang,'GERMAN')>-1}">
									<c:set var="ischecked19" value="true" />
								</c:if>
							</c:forEach>
						</c:if>
						<configByCorp:fieldVisibility
							componentId="component.field.Alternative.customerFileGerman">
							<td align="left" class="listwhitetext" style="width: 10px"></td>
							<td align="left" class="listwhitetext" style="width: 10px">English</td>
							<td align="left"><s:checkbox
									name="checkEnglishForCustomerSave" value="${ischecked18}"
									fieldValue="true" onclick="changeStatus()" /></td>
							<td align="left" class="listwhitetext" style="width: 10px"></td>
							<td align="left" class="listwhitetext" style="width: 10px">German</td>
							<td align="left"><s:checkbox
									name="checkGermanForCustomerSave" value="${ischecked19}"
									fieldValue="true" onclick="changeStatus()" /></td>
						</configByCorp:fieldVisibility>
						<c:if test="${empty customerFile.id}">
							<td align="right" style="position: absolute; right: 45px;"
								valign="bottom"><img
								src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
								HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
						</c:if>
						<c:if test="${not empty customerFile.id}">
							<c:choose>
								<c:when
									test="${countCportalNotes == '0' || countCportalNotes == '' || countCportalNotes == null}">
									<td align="right" style="position: absolute; right: 45px;"
										valign="bottom"><img id="countCportalNotesImage"
										src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
										HEIGHT=17 WIDTH=50
										onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);" /><a
										onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);"></a></td>
								</c:when>
								<c:otherwise>
									<td align="right" style="position: absolute; right: 45px;"
										valign="bottom"><img id="countCportalNotesImage"
										src="${pageContext.request.contextPath}/images/notes_open1.jpg"
										HEIGHT=17 WIDTH=50
										onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);" /><a
										onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);"></a></td>
								</c:otherwise>
							</c:choose>
						</c:if>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</configByCorp:fieldVisibility>

<tr height="15px"></tr>
</tbody>
</table>
</div>

<%-- code named as "customerfilesecform.jsp--bottomposition script" in customerfilejavascript.js --%>
<%@ include file="/common/CustomerFileSecJavaScript.js"%>
