package com.trilasoft.app.service.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest; 
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CheckListDao;
import com.trilasoft.app.dao.ToDoResultDao;
import com.trilasoft.app.dao.ToDoRuleDao;
import com.trilasoft.app.dao.RuleActionsDao;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.CheckListResult;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TaskCheckList;
import com.trilasoft.app.model.ToDoResult;
import com.trilasoft.app.model.ToDoRule;
import com.trilasoft.app.model.RuleActions;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.CheckListResultManager;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.TaskCheckListManager;
import com.trilasoft.app.service.ToDoResultManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.service.RuleActionsManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.VehicleManager;
import com.trilasoft.app.service.WorkTicketManager;
import com.trilasoft.app.webapp.action.ToDoRuleAction;

public class ToDoRuleManagerImpl extends GenericManagerImpl<ToDoRule, Long> implements ToDoRuleManager {     
	ToDoRuleDao toDoRuleDao; 
	private List toDoRules;
	private ToDoResultDao toDoResultDao;
	private ToDoResultManager toDoResultManager;
	private ToDoRuleManager toDoRuleManager;
	private ToDoRule toDoRule;
	private RuleActions ruleActions;
	private String mailStatus;
	private String mailFailure;
	private CheckListResultManager checkListResultManager;
	private TaskCheckListManager taskCheckListManager;
	private ServicePartnerManager servicePartnerManager;
	private AccountLineManager accountLineManager; 
	private ClaimManager claimManager;
	private WorkTicketManager workTicketManager;
	private VehicleManager vehicleManager;
	private  CheckListDao checkListDao;
	private TrackingStatusManager trackingStatusManager ;
	private RuleActionsDao ruleActionsDao;

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setCheckListResultManager(
			CheckListResultManager checkListResultManager) {
		this.checkListResultManager = checkListResultManager;
	}
	private UserManager userManager; 
	
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(ToDoRuleAction.class);
	
	
	protected HttpServletRequest getRequest() {
	        return ServletActionContext.getRequest();  
	    }
	
	public ToDoRuleManagerImpl(ToDoRuleDao toDoRuleDao) {   
        super(toDoRuleDao);   
        this.toDoRuleDao = toDoRuleDao;   
    } 
    	
    public List findMaximumId(){
     	return toDoRuleDao.findMaximumId(); 
     }

	public List checkById(Long id) {
		return toDoRuleDao.checkById(id);
	}

	public List <ToDoRule>searchById(Long id,String fieldToValidate,String testdate,String entitytablerequired,String messagedisplayed,String status, String toDoRuleCheckEnable, Boolean isAgentTdr) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		return toDoRuleDao.searchById(id,fieldToValidate,testdate, entitytablerequired, messagedisplayed, status, toDoRuleCheckEnable, isAgentTdr);

	}
	public List <ToDoRule>searchByDocCheckListId(Long id, String docType,String testdate,String entitytablerequired,String messagedisplayed,String status, String toDoRuleCheckEnable){
		return toDoRuleDao.searchByDocCheckListId(id, docType, testdate, entitytablerequired, messagedisplayed, status, toDoRuleCheckEnable);
	}
	
	// not in use
	public List executeRules(String entity, Long recordID,String sessionCorpID) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//toDoRuleDao.deleteAllResults();
		List<ToDoRule> toDoRules = toDoRuleDao.getRulesForEntity(entity);
		toDoRuleDao.deleteResultsForRecord(entity, recordID);
		for (ToDoRule todoRule: toDoRules){
			try{
			List<Object[]> todoResults = executeRule(todoRule, recordID, sessionCorpID);
			for (Object[] toDoRuleResult: todoResults){
				try{
				Object id = toDoRuleResult[1];
				Object id1=toDoRuleResult[2];
				Object testDate = toDoRuleResult[3];
				String assignedTo=getAssignedTo(toDoRuleResult);
				String shipper = getShipper(toDoRuleResult);
				String billing=getBilling(toDoRuleResult);
				String payable="";
				if(toDoRuleResult[8]!=null){
				 payable=(toDoRuleResult[8].toString());
				}
				String pricing="";
				if(toDoRuleResult[9]!=null){
					pricing=(toDoRuleResult[9].toString());
				}
				String consultant="";
				if(toDoRuleResult[10]!=null){
				 consultant=(toDoRuleResult[10].toString());
				}
				String salesMan="";
				if(toDoRuleResult[11]!=null){
				 salesMan=(toDoRuleResult[11].toString());
				}
				String email = "";
				if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
					if(toDoRuleResult[14]!=null){
						email = toDoRuleResult[14].toString();
					}
				}
				int duration= Integer.parseInt(todoRule.getDurationAddSub().toString());
				duration = -duration;
				Date compareWithDate = DateUtils.addDays(getCurrentDate(), duration);
				//System.out.println("date1 ="+compareWithDate);
				DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
				String compareWithDateStr = dfm.format(compareWithDate);
				//System.out.println("\n\n\n\ncompareWithDate todorule---> ="+compareWithDate);
				String compareWithDateStr1 = dfm.format(testDate);
				//System.out.println("\n\n\n\n\ncompareWithDateStr1 Result---> ="+compareWithDateStr1);
				Date d1=dfm.parse(compareWithDateStr, new ParsePosition(0));
				Date d2=dfm.parse(compareWithDateStr1, new ParsePosition(0));
				long i=d1.getTime();
				long j=d2.getTime();
				long k=(i-j);
				long days=k/(1000*60*60*24);
				//System.out.println("\n\ndueInDays = "+days);
				int dueInDays = 0;				
				//System.out.println("dueInDays =  (sysdate + duration + 7) � testDate");				
				ToDoResult toDoResult = new ToDoResult();
				toDoResult.setResultRecordId(Long.parseLong(id.toString()));
				toDoResult.setSupportingId(id1.toString());
				if(todoRule.getRolelist().equalsIgnoreCase("Coordinator")){
					toDoResult.setOwner(assignedTo);
				}
				if(todoRule.getRolelist().equalsIgnoreCase("Billing")){
					toDoResult.setOwner(billing);
				}
				if(todoRule.getRolelist().equalsIgnoreCase("Pricing")){
					toDoResult.setOwner(pricing);
				}
				if(todoRule.getRolelist().equalsIgnoreCase("Consultant")){
					toDoResult.setOwner(consultant);
				}
				if(todoRule.getRolelist().equalsIgnoreCase("Payable")){
					toDoResult.setOwner(payable);
				}
				if(todoRule.getRolelist().equalsIgnoreCase("SalesMan")){
					toDoResult.setOwner(salesMan);
				}
				if(toDoResult.getOwner()== null){
					toDoResult.setOwner("");
				}
				toDoResult.setTodoRuleId(todoRule.getId());
				toDoResult.setRolelist(todoRule.getRolelist());
				toDoResult.setMessagedisplayed(todoRule.getMessagedisplayed());
				//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		       // User user = (User)auth.getPrincipal();
		        toDoResult.setCorpID(sessionCorpID);
				toDoResult.setDurationAddSub(days);
				toDoResult.setTestdate(todoRule.getTestdate());
				toDoResult.setShipper(shipper);
				toDoResult.setResultRecordType(todoRule.getEntitytablerequired());
				toDoResult.setUrl(todoRule.getUrl());
				toDoResult.setFieldToValidate1(todoRule.getFieldToValidate1());
				if(toDoResult.getCorpID().equals("TSFT")){
					if(toDoResult.getAgentCode()!=null && !toDoResult.getAgentCode().equalsIgnoreCase("")){
						String chkAgentType=trackingStatusManager.checkIsRedSkyUser(toDoResult.getAgentCode());
						if((!"RUC".equalsIgnoreCase(chkAgentType)) && !chkAgentType.equals("")){
							toDoResult.setEmailNotification(email);
						}else{
							toDoResult.setEmailNotification("");
						}
					}else{
						toDoResult.setEmailNotification("");
					}
				}else{
					toDoResult.setEmailNotification(email);
				}
				toDoResult.setEmailOn(todoRule.getEmailOn());
				toDoResult.setManualEmail(todoRule.getManualEmail());
				toDoResult.setRecordToDel("0");
				toDoResult = toDoResultManager.save(toDoResult);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		sendAllNotifications(sessionCorpID,"");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return toDoRules;	
	}
	
	
	
	// New method for checklist result entry
	
	public synchronized List executeRulesForDocCheckList(String sessionCorpID) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List ruleNumber =new ArrayList();
		checkListDao.deleteThisResult(0L,sessionCorpID);
		List<ToDoRule> toDoRules = checkListDao.getAllRules(sessionCorpID);		
		for (ToDoRule todoRule: toDoRules){
			try{
			List<Object[]> todoResults = executeRule(todoRule, null, sessionCorpID);
			if(todoResults!=null){
			try{
				for (Object[] toDoRuleResult: todoResults){
					try{
					Object id = toDoRuleResult[1];
					Object id1=toDoRuleResult[2];
					Object testDate = toDoRuleResult[3];
					String assignedTo=getAssignedTo(toDoRuleResult);
					String shipper = getShipper(toDoRuleResult);
					String billing=getBilling(toDoRuleResult);
					String payable="";
					if(toDoRuleResult[8]!=null){
						payable=(toDoRuleResult[8].toString());
					}	
					String pricing="";
					if(toDoRuleResult[9]!=null)	{
						pricing=(toDoRuleResult[9].toString());
					}
					String consultant="";
					if(toDoRuleResult[10]!=null){
						consultant=(toDoRuleResult[10].toString());
					}
					String salesMan="";
					if(toDoRuleResult[11]!=null){
						salesMan=(toDoRuleResult[11].toString());
					}
					String auditor="";
					if(toDoRuleResult[12]!=null){
						auditor=(toDoRuleResult[12].toString());
					}
					String wareHouseManager="";
					if(todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket"))
					{
						if(toDoRuleResult[12]!=null){
							wareHouseManager=(toDoRuleResult[12].toString());
						}
						if(toDoRuleResult[13]!=null){
							auditor=(toDoRuleResult[13].toString());
						}
					}
					String email = "";
					if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
						if(toDoRuleResult[14]!=null){
							email = toDoRuleResult[14].toString();
						}
					}
					String BillToCodeName="";
					if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
						if(toDoRuleResult[15]!=null && toDoRuleResult[16]!=null){
							if(toDoRuleResult[16].toString().trim().equals("")){
								BillToCodeName = toDoRuleResult[15].toString();	
							}else{
								BillToCodeName = toDoRuleResult[15].toString() + ", " +  toDoRuleResult[16].toString();	
							}
							
						}
					}else{
						if(todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){
							if(toDoRuleResult[15]!=null && toDoRuleResult[16]!=null){
								if(toDoRuleResult[16].toString().trim().equals("")){
									BillToCodeName =toDoRuleResult[15].toString();
								}else{
									BillToCodeName = toDoRuleResult[15].toString() + ", " +  toDoRuleResult[16].toString();
								}
								
							}
						}else{
							if(toDoRuleResult[14]!=null && toDoRuleResult[15]!=null){
								if(toDoRuleResult[15].toString().trim().equals("")){
									BillToCodeName =toDoRuleResult[14].toString();
								}else{
									BillToCodeName = toDoRuleResult[14].toString() + ", " +  toDoRuleResult[15].toString();
								}
								
							}
						}
					}
					String OAgentCode ="";
					String DAgentCode ="";
					String OSAgentCode ="";
					String DSAgentCode ="";
					String claimHandler ="";
					String ForwarderAgentCode ="";
					String BrokerAgentCode ="";
					String mmCounselor ="";
					String forwarder ="";
					String wareHousereceiptDate=null;
					if((todoRule.getEntitytablerequired().equalsIgnoreCase("ServiceOrder") || todoRule.getEntitytablerequired().equalsIgnoreCase("TrackingStatus"))){
						if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
							if(toDoRuleResult[17]!=null){
								OAgentCode = toDoRuleResult[17].toString();
							}
							if(toDoRuleResult[18]!=null){
								DAgentCode = toDoRuleResult[18].toString();
							}
							if(toDoRuleResult[19]!=null){
								OSAgentCode = toDoRuleResult[19].toString();
							}
							if(toDoRuleResult[20]!=null){
								DSAgentCode = toDoRuleResult[20].toString();
							}
							if(toDoRuleResult[21]!=null){
								ForwarderAgentCode = toDoRuleResult[21].toString();
							}
							if(toDoRuleResult[22]!=null){
								BrokerAgentCode = toDoRuleResult[22].toString();
							}
							if(toDoRuleResult[23]!=null){
								mmCounselor = toDoRuleResult[23].toString();
							}else{
								mmCounselor = null;
							}							
						}else{
							if(toDoRuleResult[16]!=null){
								OAgentCode = toDoRuleResult[16].toString();
							}
							if(toDoRuleResult[17]!=null){
								DAgentCode = toDoRuleResult[17].toString();
							}
							if(toDoRuleResult[18]!=null){
								OSAgentCode = toDoRuleResult[18].toString();
							}
							if(toDoRuleResult[19]!=null){
								DSAgentCode = toDoRuleResult[19].toString();
							}
							if(toDoRuleResult[20]!=null){
								ForwarderAgentCode = toDoRuleResult[20].toString();
							}
							if(toDoRuleResult[21]!=null){
								BrokerAgentCode = toDoRuleResult[21].toString();
							}
							if(toDoRuleResult[22]!=null){
								mmCounselor = toDoRuleResult[22].toString();
							}else{
								mmCounselor = null;
							}							
						}
					}
					if((todoRule.getEntitytablerequired().equalsIgnoreCase("CustomerFile"))){
						if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
							if(toDoRuleResult[17]!=null){
								OAgentCode = toDoRuleResult[17].toString();
							}
						}else{
							if(toDoRuleResult[16]!=null){
								OAgentCode = toDoRuleResult[16].toString();
							}
						}
					}
					if((todoRule.getEntitytablerequired().equalsIgnoreCase("Billing"))){
						if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
							if(toDoRuleResult[17]!=null){
								claimHandler = toDoRuleResult[17].toString();
							}
							if(toDoRuleResult[18]!=null){
								wareHousereceiptDate = toDoRuleResult[18].toString();
							}
							if(toDoRuleResult[19]!=null){
								forwarder = toDoRuleResult[19].toString();
							}else{
								forwarder = null;
							}
						}else{
							if(toDoRuleResult[16]!=null){
								claimHandler = toDoRuleResult[16].toString();
							}
							if(toDoRuleResult[17]!=null){
								wareHousereceiptDate = toDoRuleResult[17].toString();
							}
							if(toDoRuleResult[18]!=null){
								forwarder = toDoRuleResult[18].toString();
							}else{
								forwarder = null;
							}
						}
					}	
					if((todoRule.getEntitytablerequired().equalsIgnoreCase("Claim"))){
						if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
							if(toDoRuleResult[17]!=null){
								claimHandler = toDoRuleResult[17].toString();
							}							
						}else{
							if(toDoRuleResult[16]!=null){
								claimHandler = toDoRuleResult[16].toString();
							}							
						}
					}
					int duration= Integer.parseInt(todoRule.getDurationAddSub().toString());
					duration = -duration;
					Date compareWithDate = DateUtils.addDays(getCurrentDate(), duration);
					DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
					String compareWithDateStr = dfm.format(compareWithDate);
					String compareWithDateStr1 = dfm.format(testDate);
					Date d1=dfm.parse(compareWithDateStr, new ParsePosition(0));
					Date d2=dfm.parse(compareWithDateStr1, new ParsePosition(0));
					long i=d1.getTime();
					long j=d2.getTime();
					long k=(i-j);
					long days=k/(1000*60*60*24);
					int dueInDays = 0;				
					//System.out.println("dueInDays =  (sysdate + duration + 7) � testDate");
					
					ToDoResult toDoResult = new ToDoResult();
					toDoResult.setCorpID(sessionCorpID);
					toDoResult.setResultRecordId(Long.parseLong(id.toString()));
					toDoResult.setSupportingId(id1.toString());
					if(todoRule.getRolelist().equalsIgnoreCase("Coordinator")){
						toDoResult.setOwner(assignedTo);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Billing")){
						toDoResult.setOwner(billing);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Pricing")){
						toDoResult.setOwner(pricing);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Consultant")){
						toDoResult.setOwner(consultant);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Payable")){
						toDoResult.setOwner(payable);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("SalesMan")){
						toDoResult.setOwner(salesMan);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Warehouse Manager")){
						toDoResult.setOwner(wareHouseManager);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Auditor")){
						toDoResult.setOwner(auditor);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("OriginAgent")){
						toDoResult.setOwner(assignedTo);
						toDoResult.setAgentCode(OAgentCode);
						toDoResult.setCorpID("TSFT");
					}
					if(todoRule.getRolelist().equalsIgnoreCase("DestinationAgent")){
						toDoResult.setOwner(assignedTo);
						toDoResult.setAgentCode(DAgentCode);
						toDoResult.setCorpID("TSFT");
					}
					if(todoRule.getRolelist().equalsIgnoreCase("OriginSubAgent")){
						toDoResult.setOwner(assignedTo);
						toDoResult.setAgentCode(OSAgentCode);
						toDoResult.setCorpID("TSFT");
					}
					if(todoRule.getRolelist().equalsIgnoreCase("DestinationSubAgent")){
						toDoResult.setOwner(assignedTo);
						toDoResult.setAgentCode(DSAgentCode);
						toDoResult.setCorpID("TSFT");
					}
					if(todoRule.getRolelist().equalsIgnoreCase("ForwarderAgent")){
						toDoResult.setOwner("PartnerPortal");
						toDoResult.setAgentCode(ForwarderAgentCode);
						
					}
					if(todoRule.getRolelist().equalsIgnoreCase("BrokerAgent")){
						toDoResult.setOwner("PartnerPortal");
						toDoResult.setAgentCode(BrokerAgentCode);
						
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Claims")){
						toDoResult.setOwner(claimHandler);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Claim Handler")){
						toDoResult.setOwner(claimHandler);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("MM Counselor")){
						toDoResult.setOwner(mmCounselor);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Forwarder")){
						toDoResult.setOwner(forwarder);
					}
					
					if(toDoResult.getOwner()== null){
						toDoResult.setOwner("");
					}
								
					toDoResult.setTodoRuleId(todoRule.getId());
					toDoResult.setRolelist(todoRule.getRolelist());
					toDoResult.setMessagedisplayed(todoRule.getMessagedisplayed());
					toDoResult.setRuleNumber(todoRule.getRuleNumber());
					//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			       // User user = (User)auth.getPrincipal();
			        toDoResult.setDurationAddSub(days);
					toDoResult.setTestdate(todoRule.getTestdate());
					toDoResult.setShipper(shipper);
					toDoResult.setBillToName(BillToCodeName);
					toDoResult.setResultRecordType(todoRule.getEntitytablerequired());
					toDoResult.setUrl(todoRule.getUrl());
					toDoResult.setFieldToValidate1(todoRule.getFieldToValidate1());
					toDoResult.setFieldToValidate2(todoRule.getFieldToValidate2());
					if(todoRule.getFieldDisplay()==null || todoRule.getFieldDisplay().equalsIgnoreCase(""))
					{
						toDoResult.setFieldDisplay(todoRule.getFieldToValidate1());
					}else{
					toDoResult.setFieldDisplay(todoRule.getFieldDisplay());
					}
					if(!todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){
						if(toDoRuleResult[13]!=null){
						toDoResult.setFileNumber(toDoRuleResult[13].toString());
						}
					}
					if(todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){
						if(toDoRuleResult[14]!=null){
						toDoResult.setFileNumber(toDoRuleResult[14].toString());
						}
					}
					toDoResult.setNoteType(todoRule.getNoteType());
					toDoResult.setNoteSubType(todoRule.getNoteSubType());
					if(toDoResult.getCorpID().equals("TSFT")){
						if(toDoResult.getAgentCode()!=null && !toDoResult.getAgentCode().equalsIgnoreCase("")){
							String chkAgentType=trackingStatusManager.checkIsRedSkyUser(toDoResult.getAgentCode());
							if((!"RUC".equalsIgnoreCase(chkAgentType)) && !chkAgentType.equals("")){
								toDoResult.setEmailNotification(email);
							}else{
								toDoResult.setEmailNotification("");
							}
						}else{
							toDoResult.setEmailNotification("");
						}
					}else{
						toDoResult.setEmailNotification(email);
					}
					toDoResult.setEmailOn(todoRule.getEmailOn());
					toDoResult.setManualEmail(todoRule.getManualEmail());
					toDoResult.setRecordToDel("0");

					
					CheckListResult checkListResult = new CheckListResult();
						checkListResult.setCreatedOn(new Date());
						checkListResult.setUpdatedOn(new Date());
						checkListResult
								.setCreatedBy("List Execution");
					
					checkListResult
							.setCheckListId(todoRule.getId());
					
					if(todoRule.getEntitytablerequired().equals("ServicePartner"))
					{
						Long tempId=servicePartnerManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
						checkListResult.setResultId(tempId);
					}
					else if(todoRule.getEntitytablerequired().equals("WorkTicket"))
					{
						Long tempId=workTicketManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
						checkListResult.setResultId(tempId);
					}
					else if(todoRule.getEntitytablerequired().equals("Vehicle"))
					{
						Long tempId=vehicleManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
						checkListResult.setResultId(tempId);
					}
					else if(todoRule.getEntitytablerequired().equals("Claim"))
					{
						Long tempId=claimManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
						checkListResult.setResultId(tempId);
					}else if(todoRule.getEntitytablerequired().equals("AccountLine"))
					{
						Long tempId=accountLineManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
						checkListResult.setResultId(tempId);
					}
					else{
					checkListResult.setResultId(toDoResult.getResultRecordId());
					}
					
					checkListResult.setCorpID(toDoResult
							.getCorpID());
					if (toDoResult.getDurationAddSub() != null) {
						checkListResult.setDuration(toDoResult
								.getDurationAddSub());
					} else {
						checkListResult.setDuration((long) 0);

					}
					checkListResult.setMessageDisplayed(toDoResult
							.getMessagedisplayed());
					checkListResult.setShipper(toDoResult
							.getShipper());
					if (toDoResult.getOwner() != null) {
						checkListResult.setOwner(toDoResult
								.getOwner());
					} else {
						checkListResult.setOwner("");
					}

					checkListResult.setResultNumber(toDoResult
							.getFileNumber());
					checkListResult.setUrl(todoRule.getEntitytablerequired());
					checkListResult.setUpdatedBy("List Execution");
					checkListResult.setRuleNumber(toDoResult
							.getRuleNumber().toString());
					checkListResult.setDocType(todoRule
							.getDocType());
					checkListResult.setToDoResultId(toDoResult
							.getId());
					
					checkListResult.setBillToName(toDoResult.getBillToName());
					checkListResult.setRolelist(toDoResult.getRolelist());
					checkListResult.setNoteType("Activity");
					checkListResult.setNoteSubType("ActivityManagement");
					checkListResult.setCheckResultForDel("1");
					checkListResult = checkListResultManager
							.save(checkListResult);
				
					}catch(Exception ex){
						logger.error("Error executing query "+ ex.getStackTrace()[0]);
						logger.error("Error in rule id "+todoRule.getId());
					System.out.println("\n\n\n\n\n todorule id exception--->"+todoRule.getId());
					ex.printStackTrace(); 			
				}
					
				  }
				}catch(Exception ex){
					logger.error("Error executing query "+ ex.getStackTrace()[0]);
					logger.error("Error in rule id "+todoRule.getId());
				System.out.println("\n\n\n\n\n todorule id exception--->"+todoRule.getId());
				ex.printStackTrace(); 			
			}
		}else{
			
			ruleNumber.add(todoRule.getRuleNumber());
		}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		sendAllNotifications(sessionCorpID,"");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return ruleNumber;
	}
	
	
	//**************************************************************************
	
	
	
	public synchronized List executeRules(String sessionCorpID) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List ruleNumber =new ArrayList();
		toDoRuleDao.deleteAllResults(sessionCorpID);
		List<ToDoRule> toDoRules = toDoRuleDao.getAllRules(sessionCorpID);		
		for (ToDoRule todoRule: toDoRules){
			try{
			toDoRuleDao.deleteThisResults(todoRule.getId(),sessionCorpID,todoRule.getRolelist());
			List<Object[]> todoResults = executeRule(todoRule, null, sessionCorpID);
			if(todoResults!=null){
			try{
				for (Object[] toDoRuleResult: todoResults){
					try{
					Object id = toDoRuleResult[1];
					Object id1=toDoRuleResult[2];
					Object testDate = toDoRuleResult[3];
					String assignedTo=getAssignedTo(toDoRuleResult);
					String shipper = getShipper(toDoRuleResult);
					String billing=getBilling(toDoRuleResult);
					//String opsPerson=getOp
					String payable="";
					if(toDoRuleResult[8]!=null){
						payable=(toDoRuleResult[8].toString());
					}	
					String pricing="";
					if(toDoRuleResult[9]!=null)	{
						pricing=(toDoRuleResult[9].toString());
					}
					String consultant="";
					if(toDoRuleResult[10]!=null){
						consultant=(toDoRuleResult[10].toString());
					}
					String salesMan="";
					if(toDoRuleResult[11]!=null){
						salesMan=(toDoRuleResult[11].toString());
					}
					String auditor="";
					if(toDoRuleResult[12]!=null){
						auditor=(toDoRuleResult[12].toString());
					}
					String wareHouseManager="";
					if(todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket"))
					{
						if(toDoRuleResult[12]!=null){
							wareHouseManager=(toDoRuleResult[12].toString());
						}
						if(toDoRuleResult[13]!=null){
							auditor=(toDoRuleResult[13].toString());
						}
					}
					String email = "";
					if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
						if(toDoRuleResult[14]!=null){
							email = toDoRuleResult[14].toString();
						}
					}
					String BillToCodeName="";
					if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
						if(toDoRuleResult[15]!=null && toDoRuleResult[16]!=null){
							if(toDoRuleResult[16].toString().trim().equals("")){
								BillToCodeName = toDoRuleResult[15].toString();	
							}else{
								BillToCodeName = toDoRuleResult[15].toString() + ", " +  toDoRuleResult[16].toString();	
							}
							
						}
					}else{
						if(todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){
							if(toDoRuleResult[15]!=null && toDoRuleResult[16]!=null){
								if(toDoRuleResult[16].toString().trim().equals("")){
									BillToCodeName =toDoRuleResult[15].toString();
								}else{
									BillToCodeName = toDoRuleResult[15].toString() + ", " +  toDoRuleResult[16].toString();
								}
								
							}
						}else{
							if(toDoRuleResult[14]!=null && toDoRuleResult[15]!=null){
								if(toDoRuleResult[15].toString().trim().equals("")){
									BillToCodeName =toDoRuleResult[14].toString();
								}else{
									BillToCodeName = toDoRuleResult[14].toString() + ", " +  toDoRuleResult[15].toString();
								}
								
							}
						}
					}
					String OAgentCode ="";
					String DAgentCode ="";
					String OSAgentCode ="";
					String DSAgentCode ="";
					String claimHandler ="";
					String ForwarderAgentCode ="";
					String BrokerAgentCode ="";
					String mmCounselor ="";
					String forwarder ="";
					String wareHousereceiptDate=null;
					String OAgentName ="";
					String DAgentName ="";
					String OSAgentName ="";
					String DSAgentName ="";
					String ForwarderAgentName="";
					String BrokerAgentName ="";
					String OpsPerson ="";
					if((todoRule.getEntitytablerequired().equalsIgnoreCase("ServiceOrder") || todoRule.getEntitytablerequired().equalsIgnoreCase("TrackingStatus"))){
						if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
							if(toDoRuleResult[17]!=null){
								OAgentCode = toDoRuleResult[17].toString();
							}
							if(toDoRuleResult[18]!=null){
								DAgentCode = toDoRuleResult[18].toString();
							}
							if(toDoRuleResult[19]!=null){
								OSAgentCode = toDoRuleResult[19].toString();
							}
							if(toDoRuleResult[20]!=null){
								DSAgentCode = toDoRuleResult[20].toString();
							}
							if(toDoRuleResult[21]!=null){
								ForwarderAgentCode = toDoRuleResult[21].toString();
							}
							if(toDoRuleResult[22]!=null){
								BrokerAgentCode = toDoRuleResult[22].toString();
							}
							if(toDoRuleResult[23]!=null){
								mmCounselor = toDoRuleResult[23].toString();
							}else{
								mmCounselor = null;
							}
							
							if(toDoRuleResult[24]!=null){
								OAgentName = toDoRuleResult[24].toString();
							}
							if(toDoRuleResult[25]!=null){
								DAgentName = toDoRuleResult[25].toString();
							}
							if(toDoRuleResult[26]!=null){
								OSAgentName = toDoRuleResult[26].toString();
							}
							if(toDoRuleResult[27]!=null){
								DSAgentName = toDoRuleResult[27].toString();
							}
							if(toDoRuleResult[28]!=null){
								ForwarderAgentName = toDoRuleResult[28].toString();
							}
							if(toDoRuleResult[29]!=null){
								BrokerAgentName = toDoRuleResult[29].toString();
							}
							if(toDoRuleResult[30]!=null){
								OpsPerson = toDoRuleResult[30].toString();
							}
							
						}else{
							if(toDoRuleResult[16]!=null){
								OAgentCode = toDoRuleResult[16].toString();
							}
							if(toDoRuleResult[17]!=null){
								DAgentCode = toDoRuleResult[17].toString();
							}
							if(toDoRuleResult[18]!=null){
								OSAgentCode = toDoRuleResult[18].toString();
							}
							if(toDoRuleResult[19]!=null){
								DSAgentCode = toDoRuleResult[19].toString();
							}
							if(toDoRuleResult[20]!=null){
								ForwarderAgentCode = toDoRuleResult[20].toString();
							}
							if(toDoRuleResult[21]!=null){
								BrokerAgentCode = toDoRuleResult[21].toString();
							}
							if(toDoRuleResult[22]!=null){
								mmCounselor = toDoRuleResult[22].toString();
							}else{
								mmCounselor = null;
							}	
							
							if(toDoRuleResult[23]!=null){
								OAgentName = toDoRuleResult[23].toString();
							}
							if(toDoRuleResult[24]!=null){
								DAgentName = toDoRuleResult[24].toString();
							}
							if(toDoRuleResult[25]!=null){
								OSAgentName = toDoRuleResult[25].toString();
							}
							if(toDoRuleResult[26]!=null){
								DSAgentName = toDoRuleResult[26].toString();
							}
							if(toDoRuleResult[27]!=null){
								ForwarderAgentName = toDoRuleResult[27].toString();
							}
							if(toDoRuleResult[28]!=null){
								BrokerAgentName = toDoRuleResult[28].toString();
							}
							if(toDoRuleResult[29]!=null){
								OpsPerson = toDoRuleResult[29].toString();
							}
							
						}
					}
					if((todoRule.getEntitytablerequired().equalsIgnoreCase("CustomerFile"))){
						if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
							if(toDoRuleResult[17]!=null){
								OAgentCode = toDoRuleResult[17].toString();
							}
							if(toDoRuleResult[18]!=null){
								OAgentName = toDoRuleResult[18].toString();
							}
						}else{
							if(toDoRuleResult[16]!=null){
								OAgentCode = toDoRuleResult[16].toString();
							}
							if(toDoRuleResult[17]!=null){
								OAgentName = toDoRuleResult[17].toString();
							}
						}
					}
					if ((todoRule.getEntitytablerequired()
							.equalsIgnoreCase("ServicePartner")) ) {
						if (todoRule.getEmailNotification() != null
								&& !(todoRule.getEmailNotification())
										.equalsIgnoreCase("")) {
							if (toDoRuleResult[17] != null) {
								OAgentCode = toDoRuleResult[17].toString();
							}
							if (toDoRuleResult[18] != null) {
								OAgentName = toDoRuleResult[18].toString();
							}
							if(toDoRuleResult[19]!=null){
								forwarder = toDoRuleResult[19].toString();
							}else{
								forwarder = null;
							}
							if(toDoRuleResult[20]!=null){
								DAgentCode = toDoRuleResult[20].toString();
							}

							if(toDoRuleResult[21]!=null){
								DAgentName = toDoRuleResult[21].toString();
							}
						} else {
							if (toDoRuleResult[16] != null) {
								OAgentCode = toDoRuleResult[16].toString();
							}
							if (toDoRuleResult[17] != null) {
								OAgentName = toDoRuleResult[17].toString();
							}
							if(toDoRuleResult[18]!=null){
								forwarder = toDoRuleResult[18].toString();
							}else{
								forwarder = null;
							}
							if(toDoRuleResult[19]!=null){
								DAgentCode = toDoRuleResult[19].toString();
							}

							if(toDoRuleResult[20]!=null){
								DAgentName = toDoRuleResult[20].toString();
							}
						}
					}
					if((todoRule.getEntitytablerequired().equalsIgnoreCase("Billing"))){
						if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
							if(toDoRuleResult[17]!=null){
								claimHandler = toDoRuleResult[17].toString();
							}
							if(toDoRuleResult[18]!=null){
								wareHousereceiptDate = toDoRuleResult[18].toString();
							}
							if(toDoRuleResult[19]!=null){
								forwarder = toDoRuleResult[19].toString();
							}else{
								forwarder = null;
							}
						}else{
							if(toDoRuleResult[16]!=null){
								claimHandler = toDoRuleResult[16].toString();
							}
							if(toDoRuleResult[17]!=null){
								wareHousereceiptDate = toDoRuleResult[17].toString();
							}
							if(toDoRuleResult[18]!=null){
								forwarder = toDoRuleResult[18].toString();
							}else{
								forwarder = null;
							}
						}
					}			
					if((todoRule.getEntitytablerequired().equalsIgnoreCase("Claim"))){
						if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
							if(toDoRuleResult[17]!=null){
								claimHandler = toDoRuleResult[17].toString();
							}							
						}else{
							if(toDoRuleResult[16]!=null){
								claimHandler = toDoRuleResult[16].toString();
							}							
						}
					}
					int duration= Integer.parseInt(todoRule.getDurationAddSub().toString());
					duration = -duration;
					Date compareWithDate = DateUtils.addDays(getCurrentDate(), duration);
					DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
					String compareWithDateStr = dfm.format(compareWithDate);
					String compareWithDateStr1 = dfm.format(testDate);
					Date d1=dfm.parse(compareWithDateStr, new ParsePosition(0));
					Date d2=dfm.parse(compareWithDateStr1, new ParsePosition(0));
					long i=d1.getTime();
					long j=d2.getTime();
					long k=(i-j);
					long days=k/(1000*60*60*24);
					int dueInDays = 0;				
					//System.out.println("dueInDays =  (sysdate + duration + 7) � testDate");
					
					ToDoResult toDoResult = new ToDoResult();
					toDoResult.setCorpID(sessionCorpID);
					/*toDoResult.setResultRecordId(Long.parseLong(id.toString()));
					if(todoRule.getEntitytablerequired().equals("ServicePartner")){
						Long tempId=servicePartnerManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
						toDoResult.setSupportingId(tempId.toString());
					}else if(todoRule.getEntitytablerequired().equals("Vehicle")){
						Long tempId=vehicleManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
						toDoResult.setSupportingId(tempId.toString());
					}else{
						toDoResult.setSupportingId(id1.toString());
					}
					*/
					if(id!=null){
						toDoResult.setResultRecordId(Long.parseLong(id.toString()));
						if(todoRule.getEntitytablerequired().equals("ServicePartner")){
							Long tempId=servicePartnerManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
							toDoResult.setSupportingId(tempId.toString());
						}else if(todoRule.getEntitytablerequired().equals("Vehicle")){
							Long tempId=vehicleManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
							toDoResult.setSupportingId(tempId.toString());
						}else{
							toDoResult.setSupportingId(id1.toString());
						}
						}else{
							toDoResult.setResultRecordId(Long.parseLong(id1.toString()));
							toDoResult.setSupportingId(id1.toString());
						}
					
					if(todoRule.getRolelist().equalsIgnoreCase("Coordinator")){
						toDoResult.setOwner(assignedTo);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Billing")){
						toDoResult.setOwner(billing);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Pricing")){
						toDoResult.setOwner(pricing);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Consultant")){
						toDoResult.setOwner(consultant);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Payable")){
						toDoResult.setOwner(payable);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("SalesMan")){
						toDoResult.setOwner(salesMan);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Warehouse Manager")){
						toDoResult.setOwner(wareHouseManager);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Auditor")){
						toDoResult.setOwner(auditor);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("OriginAgent")){
						toDoResult.setOwner(assignedTo);
						toDoResult.setAgentCode(OAgentCode);
						toDoResult.setAgentName(OAgentName);
						toDoResult.setCorpID("TSFT");
					}
					if(todoRule.getRolelist().equalsIgnoreCase("DestinationAgent")){
						toDoResult.setOwner(assignedTo);
						toDoResult.setAgentCode(DAgentCode);
						toDoResult.setAgentName(DAgentName);
						toDoResult.setCorpID("TSFT");
					}
					if(todoRule.getRolelist().equalsIgnoreCase("OriginSubAgent")){
						toDoResult.setOwner(assignedTo);
						toDoResult.setAgentCode(OSAgentCode);
						toDoResult.setAgentName(OSAgentName);
						toDoResult.setCorpID("TSFT");
					}
					if(todoRule.getRolelist().equalsIgnoreCase("DestinationSubAgent")){
						toDoResult.setOwner(assignedTo);
						toDoResult.setAgentCode(DSAgentCode);
						toDoResult.setAgentName(DSAgentName);
						toDoResult.setCorpID("TSFT");
					}
					if(todoRule.getRolelist().equalsIgnoreCase("ForwarderAgent")){
						toDoResult.setOwner("PartnerPortal");
						toDoResult.setAgentCode(ForwarderAgentCode);
						toDoResult.setAgentName(ForwarderAgentName);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("BrokerAgent")){
						toDoResult.setOwner("PartnerPortal");
						toDoResult.setAgentCode(BrokerAgentCode);
						toDoResult.setAgentName(BrokerAgentName);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Claims")){
						toDoResult.setOwner(claimHandler);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Claim Handler")){
						toDoResult.setOwner(claimHandler);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("MM Counselor")){
						toDoResult.setOwner(mmCounselor);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Forwarder")){
						toDoResult.setOwner(forwarder);
					}
					if(todoRule.getRolelist().equalsIgnoreCase("Ops Person")){
						toDoResult.setOwner(OpsPerson);
					}
					if(toDoResult.getOwner()== null){
						toDoResult.setOwner("");
					}
								
					toDoResult.setTodoRuleId(todoRule.getId());
					toDoResult.setRolelist(todoRule.getRolelist());
					toDoResult.setMessagedisplayed(todoRule.getMessagedisplayed());
					toDoResult.setRuleNumber(todoRule.getRuleNumber());
					//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			       // User user = (User)auth.getPrincipal();
			        toDoResult.setDurationAddSub(days);
					toDoResult.setTestdate(todoRule.getTestdate());
					toDoResult.setShipper(shipper);
					toDoResult.setBillToName(BillToCodeName);
					toDoResult.setResultRecordType(todoRule.getEntitytablerequired());
					toDoResult.setUrl(todoRule.getUrl());
					toDoResult.setFieldToValidate1(todoRule.getFieldToValidate1());
					toDoResult.setFieldToValidate2(todoRule.getFieldToValidate2());
					if(todoRule.getFieldDisplay()==null || todoRule.getFieldDisplay().equalsIgnoreCase(""))
					{
						toDoResult.setFieldDisplay(todoRule.getFieldToValidate1());
					}else{
					toDoResult.setFieldDisplay(todoRule.getFieldDisplay());
					}
					if(!todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){
						if(toDoRuleResult[13]!=null){
						toDoResult.setFileNumber(toDoRuleResult[13].toString());
						}
					}
					if(todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){
						if(toDoRuleResult[14]!=null){
						toDoResult.setFileNumber(toDoRuleResult[14].toString());
						}
					}
					toDoResult.setNoteType(todoRule.getNoteType());
					toDoResult.setNoteSubType(todoRule.getNoteSubType());
					
					if(toDoResult.getCorpID().equals("TSFT")){
						if(toDoResult.getAgentCode()!=null && !toDoResult.getAgentCode().equalsIgnoreCase("")){
							String chkAgentType=trackingStatusManager.checkIsRedSkyUser(toDoResult.getAgentCode());
							if((!"RUC".equalsIgnoreCase(chkAgentType)) && !chkAgentType.equals("")){
								toDoResult.setEmailNotification(email);
							}else{
								toDoResult.setEmailNotification("");
							}
						}else{
							toDoResult.setEmailNotification("");
						}
					}else{
						toDoResult.setEmailNotification(email);
					}
					
					toDoResult.setEmailOn(todoRule.getEmailOn());
					toDoResult.setManualEmail(todoRule.getManualEmail());
					toDoResult.setRecordToDel("0");
					toDoResult = toDoResultManager.save(toDoResult);
					}catch(Exception ex){
						logger.error("Error executing query "+ ex.getStackTrace()[0]);
						logger.error("Error in rule id "+todoRule.getId());
					System.out.println("\n\n\n\n\n todorule id exception--->"+todoRule.getId());
					ex.printStackTrace(); 			
				}
					
				  }
				}catch(Exception ex){
					logger.error("Error executing query "+ ex.getStackTrace()[0]);
					logger.error("Error in rule id "+todoRule.getId());
				System.out.println("\n\n\n\n\n todorule id exception--->"+todoRule.getId());
				ex.printStackTrace(); 			
			}
		}else{
			
			ruleNumber.add(todoRule.getRuleNumber());
		}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		sendAllNotifications(sessionCorpID,"");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return ruleNumber;
	}
	
	public String executeTestRule(ToDoRule todoRule, String sessionCorpID) {	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String message = "";
		List<Object[]> todoResults = null;
			try{
			todoResults = executeRule(todoRule, null, sessionCorpID);
				if(todoResults!=null && !todoResults.isEmpty() && todoResults.get(0)!=null){
				 Object[] tdr = todoResults.get(0);
				 return null;
				}
			}catch(Exception ex){
				message = ex.getMessage();
				 logger.error("Error executing query");
				return message;
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return null;			
	}
	public List executeForcast(ToDoRule todoRules, String sessionCorpID, String userName){

		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Map<String, Long> todoResultTempMap = new HashMap<String, Long>();
		Map<String, Long> checkListResultTempMap = new HashMap<String, Long>();

		List<ToDoRule> toDoRules = toDoRuleDao.getRuleByTypes("D/D", "Air", "EXP", "STO", "", "",sessionCorpID);
		for (ToDoRule todoRule: toDoRules){
			List<TaskCheckList> byRuleToDoResult = (List<TaskCheckList>) toDoRuleDao.getTaskCheckListByRuleNumber(todoRule.getRuleNumber().toString());
			for (TaskCheckList toDoResult : byRuleToDoResult) {
				String key = toDoResult.getFileNumber() + "-"
						+ toDoResult.getRuleNumber() + "-"
						+ toDoResult.getTodoRuleId() + "-" + toDoResult.getCorpID();
				todoResultTempMap.put(key, toDoResult.getId());
			}
		List<Object[]> todoResults = executeRuleForcast(todoRule, null, sessionCorpID);
		if(todoResults!=null){
			toDoRules=new ArrayList();	
		try{
			for (Object[] toDoRuleResult: todoResults){	
			try{	
			Object id = toDoRuleResult[1];
			Object id1=toDoRuleResult[2];
			Object testDate = toDoRuleResult[3];
			String assignedTo=getAssignedTo(toDoRuleResult);
			String shipper = getShipper(toDoRuleResult);
			String billing=getBilling(toDoRuleResult);
			String payable="";
			if(toDoRuleResult[8]!=null){
				payable=(toDoRuleResult[8].toString());
			}
			String fileNumber = "";
			if (!todoRule.getEntitytablerequired()
						.equalsIgnoreCase("WorkTicket")) {
					if (toDoRuleResult[13] != null) {
						fileNumber = toDoRuleResult[13].toString();
					}
				} else {
					if (toDoRuleResult[14] != null) {
						fileNumber = toDoRuleResult[14].toString();
					}
				}
			String key = fileNumber + "-"
					+ todoRule.getRuleNumber() + "-"
					+ todoRule.getId() + "-" + toDoRule.getCorpID();
			Boolean hasKey = todoResultTempMap.containsKey(key);
			Boolean hashCheckListKey = checkListResultTempMap.containsKey(key);
			String pricing="";
			if(toDoRuleResult[9]!=null)	{
				pricing=(toDoRuleResult[9].toString());
			}
			String consultant="";
			if(toDoRuleResult[10]!=null){
				consultant=(toDoRuleResult[10].toString());
			}
			String salesMan="";
			if(toDoRuleResult[11]!=null){
				salesMan=(toDoRuleResult[11].toString());
			}
			String auditor="";
			if(toDoRuleResult[12]!=null){
				auditor=(toDoRuleResult[12].toString());
			}
			String wareHouseManager="";
			if(todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket"))
			{
				if(toDoRuleResult[12]!=null){
					wareHouseManager=(toDoRuleResult[12].toString());
				}
				if(toDoRuleResult[13]!=null){
					auditor=(toDoRuleResult[13].toString());
				}
			}
			String email = "";
			if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
				if(toDoRuleResult[14]!=null){
					email = toDoRuleResult[14].toString();
				}
			}
			String BillToCodeName="";
			if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
				if(toDoRuleResult[15]!=null && toDoRuleResult[16]!=null){
					if(toDoRuleResult[16].toString().trim().equals("")){
						BillToCodeName = toDoRuleResult[15].toString();	
					}else{
						BillToCodeName = toDoRuleResult[15].toString() + ", " +  toDoRuleResult[16].toString();	
					}
					
				}
			}else{
				if(todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){
					if(toDoRuleResult[15]!=null && toDoRuleResult[16]!=null){
						if(toDoRuleResult[16].toString().trim().equals("")){
							BillToCodeName =toDoRuleResult[15].toString();
						}else{
							BillToCodeName = toDoRuleResult[15].toString() + ", " +  toDoRuleResult[16].toString();
						}
						
					}
				}else{
					if(toDoRuleResult[14]!=null && toDoRuleResult[15]!=null){
						if(toDoRuleResult[15].toString().trim().equals("")){
							BillToCodeName =toDoRuleResult[14].toString();
						}else{
							BillToCodeName = toDoRuleResult[14].toString() + ", " +  toDoRuleResult[15].toString();
						}
						
					}
				}
			}
			String OAgentCode ="";
			String DAgentCode ="";
			String OSAgentCode ="";
			String DSAgentCode ="";
			String claimHandler ="";
			String ForwarderAgentCode ="";
			String BrokerAgentCode ="";
			String mmCounselor ="";
			String forwarder ="";
			String wareHousereceiptDate=null;
			String OAgentName ="";
			String DAgentName ="";
			String OSAgentName ="";
			String DSAgentName ="";
			String ForwarderAgentName ="";
			String BrokerAgentName ="";
			String OpsPerson ="";
			if((todoRule.getEntitytablerequired().equalsIgnoreCase("ServiceOrder") || todoRule.getEntitytablerequired().equalsIgnoreCase("TrackingStatus"))){
				if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
					if(toDoRuleResult[17]!=null){
						OAgentCode = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						DAgentCode = toDoRuleResult[18].toString();
					}
					if(toDoRuleResult[19]!=null){
						OSAgentCode = toDoRuleResult[19].toString();
					}
					if(toDoRuleResult[20]!=null){
						DSAgentCode = toDoRuleResult[20].toString();
					}
					if(toDoRuleResult[21]!=null){
						ForwarderAgentCode = toDoRuleResult[21].toString();
					}
					if(toDoRuleResult[22]!=null){
						BrokerAgentCode = toDoRuleResult[22].toString();
					}
					if(toDoRuleResult[23]!=null){
						mmCounselor = toDoRuleResult[23].toString();
					}else{
						mmCounselor = null;
					}
					
					if(toDoRuleResult[24]!=null){
						OAgentName = toDoRuleResult[24].toString();
					}
					if(toDoRuleResult[25]!=null){
						DAgentName = toDoRuleResult[25].toString();
					}
					if(toDoRuleResult[26]!=null){
						OSAgentName = toDoRuleResult[26].toString();
					}
					if(toDoRuleResult[27]!=null){
						DSAgentName = toDoRuleResult[27].toString();
					}
					if(toDoRuleResult[28]!=null){
						ForwarderAgentName = toDoRuleResult[28].toString();
					}
					if(toDoRuleResult[29]!=null){
						BrokerAgentName = toDoRuleResult[29].toString();
					}
					if(toDoRuleResult[30]!=null){
						OpsPerson = toDoRuleResult[30].toString();
					}
					if (toDoRuleResult[31] != null) {
						forwarder = toDoRuleResult[31].toString();
					} else {
						forwarder = null;
					}
				}else{
					if(toDoRuleResult[16]!=null){
						OAgentCode = toDoRuleResult[16].toString();
					}
					if(toDoRuleResult[17]!=null){
						DAgentCode = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						OSAgentCode = toDoRuleResult[18].toString();
					}
					if(toDoRuleResult[19]!=null){
						DSAgentCode = toDoRuleResult[19].toString();
					}
					if(toDoRuleResult[20]!=null){
						ForwarderAgentCode = toDoRuleResult[20].toString();
					}
					if(toDoRuleResult[21]!=null){
						BrokerAgentCode = toDoRuleResult[21].toString();
					}
					if(toDoRuleResult[22]!=null){
						mmCounselor = toDoRuleResult[22].toString();
					}else{
						mmCounselor = null;
					}
					
					if(toDoRuleResult[23]!=null){
						OAgentName = toDoRuleResult[23].toString();
					}
					if(toDoRuleResult[24]!=null){
						DAgentName = toDoRuleResult[24].toString();
					}
					if(toDoRuleResult[25]!=null){
						OSAgentName = toDoRuleResult[25].toString();
					}
					if(toDoRuleResult[26]!=null){
						DSAgentName = toDoRuleResult[26].toString();
					}
					if(toDoRuleResult[27]!=null){
						ForwarderAgentName = toDoRuleResult[27].toString();
					}
					if(toDoRuleResult[28]!=null){
						BrokerAgentName = toDoRuleResult[28].toString();
					}
					if(toDoRuleResult[29]!=null){
						OpsPerson = toDoRuleResult[29].toString();
					}
					if (toDoRuleResult[30] != null) {
						forwarder = toDoRuleResult[30].toString();
					} else {
						forwarder = null;
					}
				}
			}
			if((todoRule.getEntitytablerequired().equalsIgnoreCase("CustomerFile"))){
				if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
					if(toDoRuleResult[17]!=null){
						OAgentCode = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						OAgentName = toDoRuleResult[18].toString();
					}
				}else{
					if(toDoRuleResult[16]!=null){
						OAgentCode = toDoRuleResult[16].toString();
					}
					if(toDoRuleResult[17]!=null){
						OAgentName = toDoRuleResult[17].toString();
					}
				}
			}
			if ((todoRule.getEntitytablerequired()
					.equalsIgnoreCase("ServicePartner")) ) {
				if (todoRule.getEmailNotification() != null
						&& !(todoRule.getEmailNotification())
								.equalsIgnoreCase("")) {
					if (toDoRuleResult[17] != null) {
						OAgentCode = toDoRuleResult[17].toString();
					}
					if (toDoRuleResult[18] != null) {
						OAgentName = toDoRuleResult[18].toString();
					}
					if(toDoRuleResult[19]!=null){
						forwarder = toDoRuleResult[19].toString();
					}else{
						forwarder = null;
					}
				} else {
					if (toDoRuleResult[16] != null) {
						OAgentCode = toDoRuleResult[16].toString();
					}
					if (toDoRuleResult[17] != null) {
						OAgentName = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						forwarder = toDoRuleResult[18].toString();
					}else{
						forwarder = null;
					}
				}
			}
			if((todoRule.getEntitytablerequired().equalsIgnoreCase("Billing"))){
				if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
					if(toDoRuleResult[17]!=null){
						claimHandler = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						wareHousereceiptDate = toDoRuleResult[18].toString();
					}
					if(toDoRuleResult[19]!=null){
						forwarder = toDoRuleResult[19].toString();
					}else{
						forwarder = null;
					}
				}else{
					if(toDoRuleResult[16]!=null){
						claimHandler = toDoRuleResult[16].toString();
						if(toDoRuleResult[17]!=null){
						wareHousereceiptDate=toDoRuleResult[17].toString();
						}
					}
					if(toDoRuleResult[17]!=null){
						wareHousereceiptDate = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						forwarder = toDoRuleResult[18].toString();
					}else{
						forwarder = null;
					}
				}
			}	
			if((todoRule.getEntitytablerequired().equalsIgnoreCase("Claim"))){
				if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
					if(toDoRuleResult[17]!=null){
						claimHandler = toDoRuleResult[17].toString();
					}							
				}else{
					if(toDoRuleResult[16]!=null){
						claimHandler = toDoRuleResult[16].toString();
					}							
				}
			}
			int duration= Integer.parseInt(todoRule.getDurationAddSub().toString());
			duration = -duration;
			Date compareWithDate = DateUtils.addDays(getCurrentDate(), duration);
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String compareWithDateStr = dfm.format(compareWithDate);
			String compareWithDateStr1 = dfm.format(testDate);
			Date d1=dfm.parse(compareWithDateStr, new ParsePosition(0));
			Date d2=dfm.parse(compareWithDateStr1, new ParsePosition(0));
			long i=d1.getTime();
			long j=d2.getTime();
			long k=(i-j);
			long days=k/(1000*60*60*24);
			int dueInDays = 0;				
			//System.out.println("dueInDays =  (sysdate + duration + 7) � testDate");
			TaskCheckList toDoResult = null;
			if(hasKey){
				Long toDoResultId = todoResultTempMap.get(key);
				toDoResult = taskCheckListManager.getForOtherCorpid(toDoResultId);
			} else {
				toDoResult = new TaskCheckList();
				toDoResult.setMailStauts("");
			}
			toDoResult.setCorpID(sessionCorpID);
			toDoResult.setResultRecordId(Long.parseLong(id.toString()));
			if(todoRule.getEntitytablerequired().equals("ServicePartner")){
				Long tempId=servicePartnerManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
				toDoResult.setSupportingId(tempId.toString());
			}else if(todoRule.getEntitytablerequired().equals("Vehicle")){
				Long tempId=vehicleManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
				toDoResult.setSupportingId(tempId.toString());
			}else{
				toDoResult.setSupportingId(id1.toString());
			}
			if(todoRule.getRolelist().equalsIgnoreCase("Coordinator")){
				toDoResult.setOwner(assignedTo);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("Billing")){
				toDoResult.setOwner(billing);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("Pricing")){
				toDoResult.setOwner(pricing);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("Consultant")){
				toDoResult.setOwner(consultant);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("Payable")){
				toDoResult.setOwner(payable);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("SalesMan")){
				toDoResult.setOwner(salesMan);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("Warehouse Manager")){
				toDoResult.setOwner(wareHouseManager);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("Auditor")){
				toDoResult.setOwner(auditor);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("OriginAgent")){
				toDoResult.setOwner(assignedTo);
				toDoResult.setAgentCode(OAgentCode);
				toDoResult.setAgentName(OAgentName);
				toDoResult.setCorpID("TSFT");
			}
			if(todoRule.getRolelist().equalsIgnoreCase("DestinationAgent")){
				toDoResult.setOwner(assignedTo);
				toDoResult.setAgentCode(DAgentCode);
				toDoResult.setAgentName(DAgentName);
				toDoResult.setCorpID("TSFT");
			}
			if(todoRule.getRolelist().equalsIgnoreCase("OriginSubAgent")){
				toDoResult.setOwner(assignedTo);
				toDoResult.setAgentCode(OSAgentCode);
				toDoResult.setAgentName(OSAgentName);
				toDoResult.setCorpID("TSFT");
			}
			if(todoRule.getRolelist().equalsIgnoreCase("DestinationSubAgent")){
				toDoResult.setOwner(assignedTo);
				toDoResult.setAgentCode(DSAgentCode);
				toDoResult.setAgentName(DSAgentName);
				toDoResult.setCorpID("TSFT");
			}
			if(todoRule.getRolelist().equalsIgnoreCase("ForwarderAgent")){
				toDoResult.setOwner("PartnerPortal");
				toDoResult.setAgentCode(ForwarderAgentCode);
				toDoResult.setAgentName(ForwarderAgentName);
				
			}
			if(todoRule.getRolelist().equalsIgnoreCase("BrokerAgent")){
				toDoResult.setOwner("PartnerPortal");
				toDoResult.setAgentCode(BrokerAgentCode);
				toDoResult.setAgentName(BrokerAgentName);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("Claims")){
				toDoResult.setOwner(claimHandler);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("Claim Handler")){
				toDoResult.setOwner(claimHandler);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("MM Counselor")){
				toDoResult.setOwner(mmCounselor);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("Forwarder")){
				toDoResult.setOwner(forwarder);
			}
			if(todoRule.getRolelist().equalsIgnoreCase("Ops Person")){
				toDoResult.setOwner(OpsPerson);
			}
			if(toDoResult.getOwner()== null){
				toDoResult.setOwner("");
			}
						
			toDoResult.setTodoRuleId(todoRule.getId());
			toDoResult.setRolelist(todoRule.getRolelist());
			toDoResult.setMessagedisplayed(todoRule.getMessagedisplayed());
			toDoResult.setRuleNumber(todoRule.getRuleNumber());
			//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	       // User user = (User)auth.getPrincipal();
	        toDoResult.setDurationAddSub(days);
			toDoResult.setTestdate(todoRule.getTestdate());
			toDoResult.setShipper(shipper);
			toDoResult.setBillToName(BillToCodeName);
			toDoResult.setResultRecordType(todoRule.getEntitytablerequired());
			toDoResult.setUrl(todoRule.getUrl());
			toDoResult.setFieldToValidate1(todoRule.getFieldToValidate1());
			toDoResult.setFieldToValidate2(todoRule.getFieldToValidate2());
			if(todoRule.getFieldDisplay()==null || todoRule.getFieldDisplay().equalsIgnoreCase(""))
			{
				toDoResult.setFieldDisplay(todoRule.getFieldToValidate1());
			}else{
			toDoResult.setFieldDisplay(todoRule.getFieldDisplay());
			}
			if(!todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){
				if(toDoRuleResult[13]!=null){
				toDoResult.setFileNumber(toDoRuleResult[13].toString());
				}
			}
			if(todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){
				if(toDoRuleResult[14]!=null){
				toDoResult.setFileNumber(toDoRuleResult[14].toString());
				}
			}
			toDoResult.setNoteType(todoRule.getNoteType());
			toDoResult.setNoteSubType(todoRule.getNoteSubType());
			if(toDoResult.getCorpID().equals("TSFT")){
				if(toDoResult.getAgentCode()!=null && !toDoResult.getAgentCode().equalsIgnoreCase("")){
					String chkAgentType=trackingStatusManager.checkIsRedSkyUser(toDoResult.getAgentCode());
					if((!"RUC".equalsIgnoreCase(chkAgentType)) && !chkAgentType.equals("")){
						toDoResult.setEmailNotification(email);
					}else{
						toDoResult.setEmailNotification("");
					}
				}else{
					toDoResult.setEmailNotification("");
				}
			}else{
				toDoResult.setEmailNotification(email);
			}
			toDoResult.setEmailOn(todoRule.getEmailOn());
			toDoResult.setManualEmail(todoRule.getManualEmail());
			toDoResult.setRecordToDel("1");
			CheckListResult checkListResult = null;
			if(toDoRule.getDocType()!=null && !toDoRule.getDocType().equals("")) {
				if(hashCheckListKey){
					Long checkListResultId = checkListResultTempMap.get(key);
					checkListResult = checkListResultManager.get(checkListResultId);
					checkListResult.setUpdatedOn(new Date());
				}
				else{
					checkListResult = new CheckListResult();
					checkListResult.setCreatedOn(new Date());
					checkListResult.setUpdatedOn(new Date());
					checkListResult.setCreatedBy("List Execution");
				}
				checkListResult.setCheckListId(toDoRule.getId());
				if(toDoRule.getEntitytablerequired().equals("ServicePartner"))
				{
					Long tempId=servicePartnerManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
					checkListResult.setResultId(tempId);
				}
				else if(toDoRule.getEntitytablerequired().equals("WorkTicket"))
				{
					Long tempId=workTicketManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
					checkListResult.setResultId(tempId);
				}
				else if(toDoRule.getEntitytablerequired().equals("Vehicle"))
				{
					Long tempId=vehicleManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
					checkListResult.setResultId(tempId);
				}
				else if(toDoRule.getEntitytablerequired().equals("Claim"))
				{
					Long tempId=claimManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
					checkListResult.setResultId(tempId);
				}else if(toDoRule.getEntitytablerequired().equals("AccountLine"))
				{
					Long tempId=accountLineManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
					checkListResult.setResultId(tempId);
				}
				else{
				checkListResult.setResultId(toDoResult.getResultRecordId());
				}
				checkListResult.setCorpID(toDoResult.getCorpID());
				if(toDoResult.getDurationAddSub()!=null){
					checkListResult.setDuration(toDoResult.getDurationAddSub());	
					}else{
						checkListResult.setDuration((long) 0);
						
					}
				checkListResult.setMessageDisplayed(toDoResult.getMessagedisplayed());
				checkListResult.setShipper(toDoResult.getShipper());
				if(toDoResult.getOwner()!= null){
					checkListResult.setOwner(toDoResult.getOwner());
				}else{
					checkListResult.setOwner("");	
				}
				
				checkListResult.setResultNumber(toDoResult.getFileNumber());
				checkListResult.setUrl(toDoRule.getEntitytablerequired());
				checkListResult.setUpdatedBy("List Execution");
				checkListResult.setRuleNumber(toDoResult.getRuleNumber().toString());
				checkListResult.setDocType(toDoRule.getDocType());
				checkListResult.setToDoResultId(toDoResult.getId());
				
				checkListResult.setBillToName(toDoResult.getBillToName());
				checkListResult.setRolelist(toDoResult.getRolelist());
				
				checkListResult.setCheckResultForDel("1");
				checkListResult.setEmailNotification(toDoResult.getEmailNotification());
				checkListResult.setManualEmail(toDoResult.getManualEmail());
				
				
				checkListResult.setNoteType("Activity");
				checkListResult.setNoteSubType("ActivityManagement");
				
				
				checkListResult = checkListResultManager.save(checkListResult);
			}else{
			toDoResult = taskCheckListManager.save(toDoResult);
			}
		  }catch(Exception ex){	
				logger.error("Error executing query "+ ex.getStackTrace()[0]);
				System.out.println("\n\n\n\n\n exception--->"+ex);	
				toDoRules=null;
		}
		}	
		}catch(Exception ex){	
			logger.error("Error executing query "+ ex.getStackTrace()[0]);
			System.out.println("\n\n\n\n\n exception--->"+ex);
			toDoRules=null;
		}
		
		}else{
			toDoRules=null;	
		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return toDoRules;			
	
	}
	public synchronized List executeThisRule(ToDoRule todoRule, String sessionCorpID, String userName) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Map<String, Long> todoResultTempMap = new HashMap<String, Long>();
		Map<String, Long> checkListResultTempMap = new HashMap<String, Long>();
		Map<String, Long> rule = new HashMap<String, Long>();
		List<ToDoResult> byRuleToDoResult = (List<ToDoResult>) toDoRuleDao.getToDoResultByRuleNumber(todoRule.getRuleNumber().toString());
		List<RuleActions> byRuleActions=(List<RuleActions>) ruleActionsDao.getToDoRuleByRuleNumber(todoRule.getId(), sessionCorpID);
     	for (ToDoResult toDoResult : byRuleToDoResult) {
			String key = toDoResult.getFileNumber() + "-"
					+ toDoResult.getRuleNumber() + "-"
					+ toDoResult.getTodoRuleId() + "-" + toDoResult.getCorpID()+ "-" + toDoResult.getToDoActionId();;
			todoResultTempMap.put(key, toDoResult.getId());
		}
     	for (ToDoResult toDoResult : byRuleToDoResult) {
     		String chcekListkey  = toDoResult.getFileNumber() + "-"
					+ toDoResult.getRuleNumber() + "-"
					+ toDoResult.getTodoRuleId() + "-" + toDoResult.getCorpID();
			todoResultTempMap.put(chcekListkey, toDoResult.getId());
		}

		List<CheckListResult> byCheckListResult = (List<CheckListResult>) toDoRuleDao.getCheckListResultByRuleNumber(todoRule.getRuleNumber().toString());
		for (CheckListResult checkListResult : byCheckListResult) {
			String key1 = checkListResult.getResultNumber() + "-"
					+ checkListResult.getRuleNumber() + "-"
					+ checkListResult.getCheckListId() + "-" + checkListResult.getCorpID();
			checkListResultTempMap.put(key1, checkListResult.getId());
		}
		//toDoRuleDao.deleteThisResults(todoRule.getId(),sessionCorpID,todoRule.getRolelist());
		//List<ToDoRule> toDoRules = toDoRuleDao.getThisRule(todoRule.getId());
		ToDoRule toDoRule = toDoRuleDao.getForOtherCorpid(todoRule.getId());
		//List<Object[]> todoResults = executeRule(toDoRules.get(0), null, sessionCorpID);
		List<Object[]> todoResults = executeRule(toDoRule, null, sessionCorpID);
		if(todoResults!=null){
			toDoRules=new ArrayList();	
		try{
			for (Object[] toDoRuleResult: todoResults){	
			try{	
			Object id = toDoRuleResult[1];
			Object id1=toDoRuleResult[2];
			Object testDate = toDoRuleResult[3];
			String assignedTo=getAssignedTo(toDoRuleResult);
			String shipper = getShipper(toDoRuleResult);
			String billing=getBilling(toDoRuleResult);
			String payable="";
			if(toDoRuleResult[8]!=null){
				payable=(toDoRuleResult[8].toString());
			}
			String fileNumber = "";
			if (!todoRule.getEntitytablerequired()
						.equalsIgnoreCase("WorkTicket")) {
					if (toDoRuleResult[13] != null) {
						fileNumber = toDoRuleResult[13].toString();
					}
				} else {
					if (toDoRuleResult[14] != null) {
						fileNumber = toDoRuleResult[14].toString();
					}
				}
			String key = fileNumber + "-"
					+ todoRule.getRuleNumber() + "-"
					+ todoRule.getId() + "-" + toDoRule.getCorpID();
			
			String checklistkey = fileNumber + "-"
					+ todoRule.getRuleNumber() + "-"
					+ todoRule.getId() + "-" + toDoRule.getCorpID();
			
			String checkKey = fileNumber + "-"
			+ todoRule.getRuleNumber() + "-"
			+ todoRule.getId() + "-" + toDoRule.getCorpID();
			Boolean haschklistkey=todoResultTempMap.containsKey(checkKey);
			Boolean hasKey = todoResultTempMap.containsKey(key);
			Boolean hashCheckListKey = checkListResultTempMap.containsKey(checklistkey);
			String pricing="";
			if(toDoRuleResult[9]!=null)	{
				pricing=(toDoRuleResult[9].toString());
			}
			String consultant="";
			if(toDoRuleResult[10]!=null){
				consultant=(toDoRuleResult[10].toString());
			}
			String salesMan="";
			if(toDoRuleResult[11]!=null){
				salesMan=(toDoRuleResult[11].toString());
			}
			String auditor="";
			if(toDoRuleResult[12]!=null){
				auditor=(toDoRuleResult[12].toString());
			}
			String wareHouseManager="";
			String bookingAgentCode="";
			if(todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket"))
			{
				if(toDoRuleResult[12]!=null){
					wareHouseManager=(toDoRuleResult[12].toString());
				}
				if(toDoRuleResult[13]!=null){
					auditor=(toDoRuleResult[13].toString());
				}
			}
			String email = "";
			if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
				if(toDoRuleResult[14]!=null){
					email = toDoRuleResult[14].toString();
				}
			}
			String BillToCodeName="";
			String billTocode="";
			if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
				if(toDoRuleResult[15]!=null && toDoRuleResult[16]!=null){
					if(toDoRuleResult[16].toString().trim().equals("")){
						BillToCodeName = toDoRuleResult[15].toString();	
						billTocode = toDoRuleResult[15].toString();
					}else{
						BillToCodeName = toDoRuleResult[15].toString() + ", " +  toDoRuleResult[16].toString();	
						billTocode = toDoRuleResult[15].toString();
					}
					
				}
				if(toDoRuleResult[17]!=null){
					bookingAgentCode = toDoRuleResult[17].toString();
				}
			}else{
				if(todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){
					if(toDoRuleResult[15]!=null && toDoRuleResult[16]!=null){
						if(toDoRuleResult[16].toString().trim().equals("")){
							BillToCodeName =toDoRuleResult[15].toString();
							billTocode = toDoRuleResult[15].toString();
						}else{
							BillToCodeName = toDoRuleResult[15].toString() + ", " +  toDoRuleResult[16].toString();
							billTocode = toDoRuleResult[15].toString();
						}
						
					}
					if(toDoRuleResult[17]!=null){
						bookingAgentCode = toDoRuleResult[17].toString();
					}
				}else{
					if(toDoRuleResult[14]!=null && toDoRuleResult[15]!=null){
						if(toDoRuleResult[15].toString().trim().equals("")){
							BillToCodeName =toDoRuleResult[14].toString();
							billTocode = toDoRuleResult[14].toString();
						}else{
							BillToCodeName = toDoRuleResult[14].toString() + ", " +  toDoRuleResult[15].toString();
							billTocode = toDoRuleResult[14].toString();
						}
						if(toDoRuleResult[16]!=null){
							bookingAgentCode =toDoRuleResult[16].toString();
						}
					}
				}
			}
			String OAgentCode ="";
			String DAgentCode ="";
			String OSAgentCode ="";
			String DSAgentCode ="";
			String claimHandler ="";
			String ForwarderAgentCode ="";
			String BrokerAgentCode ="";
			String mmCounselor ="";
			String forwarder ="";
			String wareHousereceiptDate=null;
			String OAgentName ="";
			String DAgentName ="";
			String OSAgentName ="";
			String DSAgentName ="";
			String ForwarderAgentName ="";
			String BrokerAgentName ="";
			String OpsPerson ="";
			
			if((todoRule.getEntitytablerequired().equalsIgnoreCase("ServiceOrder") || todoRule.getEntitytablerequired().equalsIgnoreCase("TrackingStatus"))){
				if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
					if(toDoRuleResult[17]!=null){
						OAgentCode = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						DAgentCode = toDoRuleResult[18].toString();
					}
					if(toDoRuleResult[19]!=null){
						OSAgentCode = toDoRuleResult[19].toString();
					}
					if(toDoRuleResult[20]!=null){
						DSAgentCode = toDoRuleResult[20].toString();
					}
					if(toDoRuleResult[21]!=null){
						ForwarderAgentCode = toDoRuleResult[21].toString();
					}
					if(toDoRuleResult[22]!=null){
						BrokerAgentCode = toDoRuleResult[22].toString();
					}
					if(toDoRuleResult[23]!=null){
						mmCounselor = toDoRuleResult[23].toString();
					}else{
						mmCounselor = null;
					}
					
					if(toDoRuleResult[24]!=null){
						OAgentName = toDoRuleResult[24].toString();
					}
					if(toDoRuleResult[25]!=null){
						DAgentName = toDoRuleResult[25].toString();
					}
					if(toDoRuleResult[26]!=null){
						OSAgentName = toDoRuleResult[26].toString();
					}
					if(toDoRuleResult[27]!=null){
						DSAgentName = toDoRuleResult[27].toString();
					}
					if(toDoRuleResult[28]!=null){
						ForwarderAgentName = toDoRuleResult[28].toString();
					}
					if(toDoRuleResult[29]!=null){
						BrokerAgentName = toDoRuleResult[29].toString();
					}
					if(toDoRuleResult[30]!=null){
						OpsPerson = toDoRuleResult[30].toString();
					}
					if (toDoRuleResult[31] != null) {
						forwarder = toDoRuleResult[31].toString();
					} else {
						forwarder = null;
					}
					if (toDoRuleResult[32] != null) {
						bookingAgentCode = toDoRuleResult[32].toString();
					} else {
						bookingAgentCode = null;
					}
				}else{
					if(toDoRuleResult[16]!=null){
						OAgentCode = toDoRuleResult[16].toString();
					}
					if(toDoRuleResult[17]!=null){
						DAgentCode = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						OSAgentCode = toDoRuleResult[18].toString();
					}
					if(toDoRuleResult[19]!=null){
						DSAgentCode = toDoRuleResult[19].toString();
					}
					if(toDoRuleResult[20]!=null){
						ForwarderAgentCode = toDoRuleResult[20].toString();
					}
					if(toDoRuleResult[21]!=null){
						BrokerAgentCode = toDoRuleResult[21].toString();
					}
					if(toDoRuleResult[22]!=null){
						mmCounselor = toDoRuleResult[22].toString();
					}else{
						mmCounselor = null;
					}
					
					if(toDoRuleResult[23]!=null){
						OAgentName = toDoRuleResult[23].toString();
					}
					if(toDoRuleResult[24]!=null){
						DAgentName = toDoRuleResult[24].toString();
					}
					if(toDoRuleResult[25]!=null){
						OSAgentName = toDoRuleResult[25].toString();
					}
					if(toDoRuleResult[26]!=null){
						DSAgentName = toDoRuleResult[26].toString();
					}
					if(toDoRuleResult[27]!=null){
						ForwarderAgentName = toDoRuleResult[27].toString();
					}
					if(toDoRuleResult[28]!=null){
						BrokerAgentName = toDoRuleResult[28].toString();
					}
					if(toDoRuleResult[29]!=null){
						OpsPerson = toDoRuleResult[29].toString();
					}
					if (toDoRuleResult[30] != null) {
						forwarder = toDoRuleResult[30].toString();
					} else {
						forwarder = null;
					}
					if (toDoRuleResult[31] != null) {
						bookingAgentCode = toDoRuleResult[31].toString();
					} else {
						bookingAgentCode = null;
					}
				}
			}
			if((todoRule.getEntitytablerequired().equalsIgnoreCase("CustomerFile"))){
				if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
					if(toDoRuleResult[17]!=null){
						OAgentCode = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						OAgentName = toDoRuleResult[18].toString();
					}
					if(toDoRuleResult[19]!=null){
						bookingAgentCode = toDoRuleResult[19].toString();
					}
				}else{
					if(toDoRuleResult[16]!=null){
						OAgentCode = toDoRuleResult[16].toString();
					}
					if(toDoRuleResult[17]!=null){
						OAgentName = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						bookingAgentCode = toDoRuleResult[18].toString();
					}
				}
			}
			if ((todoRule.getEntitytablerequired()
					.equalsIgnoreCase("ServicePartner")) ) {
				if (todoRule.getEmailNotification() != null
						&& !(todoRule.getEmailNotification())
								.equalsIgnoreCase("")) {
					if (toDoRuleResult[17] != null) {
						OAgentCode = toDoRuleResult[17].toString();
					}
					if (toDoRuleResult[18] != null) {
						OAgentName = toDoRuleResult[18].toString();
					}
					if(toDoRuleResult[19]!=null){
						forwarder = toDoRuleResult[19].toString();
					}else{
						forwarder = null;
					}
					if(toDoRuleResult[20]!=null){
						DAgentCode = toDoRuleResult[20].toString();
					}

					if(toDoRuleResult[21]!=null){
						DAgentName = toDoRuleResult[21].toString();
					}
					if(toDoRuleResult[22]!=null){
						bookingAgentCode = toDoRuleResult[22].toString();
					}
				} else {
					if (toDoRuleResult[16] != null) {
						OAgentCode = toDoRuleResult[16].toString();
					}
					if (toDoRuleResult[17] != null) {
						OAgentName = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						forwarder = toDoRuleResult[18].toString();
					}else{
						forwarder = null;
					}
					if(toDoRuleResult[19]!=null){
						DAgentCode = toDoRuleResult[19].toString();
					}

					if(toDoRuleResult[20]!=null){
						DAgentName = toDoRuleResult[20].toString();
					}
					if(toDoRuleResult[21]!=null){
						bookingAgentCode = toDoRuleResult[21].toString();
					}
				}
			}
			if((todoRule.getEntitytablerequired().equalsIgnoreCase("Billing"))){
				if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
					if(toDoRuleResult[17]!=null){
						claimHandler = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						wareHousereceiptDate = toDoRuleResult[18].toString();
					}
					if(toDoRuleResult[19]!=null){
						forwarder = toDoRuleResult[19].toString();
					}else{
						forwarder = null;
					}
					if(toDoRuleResult[20]!=null){
						bookingAgentCode = toDoRuleResult[20].toString();
					}else{
						bookingAgentCode = null;
					}
					
				}else{
					if(toDoRuleResult[16]!=null){
						claimHandler = toDoRuleResult[16].toString();
						if(toDoRuleResult[17]!=null){
						wareHousereceiptDate=toDoRuleResult[17].toString();
						}
					}
					if(toDoRuleResult[17]!=null){
						wareHousereceiptDate = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						forwarder = toDoRuleResult[18].toString();
					}else{
						forwarder = null;
					}
					if(toDoRuleResult[19]!=null){
						bookingAgentCode = toDoRuleResult[19].toString();
					}else{
						bookingAgentCode = null;
					}
				}
			}
			/*if(toDoRuleResult[14]!=null){
				billTocode = toDoRuleResult[14].toString();
			}else{
				billTocode = null;
			}*/
			
			
			if((todoRule.getEntitytablerequired().equalsIgnoreCase("Claim"))){
				if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification()).equalsIgnoreCase("")){
					if(toDoRuleResult[17]!=null){
						claimHandler = toDoRuleResult[17].toString();
					}
					if(toDoRuleResult[18]!=null){
						bookingAgentCode = toDoRuleResult[18].toString();
					}
				}else{
					if(toDoRuleResult[16]!=null){
						claimHandler = toDoRuleResult[16].toString();
					}	
					if(toDoRuleResult[17]!=null){
						bookingAgentCode = toDoRuleResult[17].toString();
					}
				}
			}
			int duration= Integer.parseInt(todoRule.getDurationAddSub().toString());
			duration = -duration;
			Date compareWithDate = DateUtils.addDays(getCurrentDate(), duration);
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String compareWithDateStr = dfm.format(compareWithDate);
			String compareWithDateStr1 = dfm.format(testDate);
			Date d1=dfm.parse(compareWithDateStr, new ParsePosition(0));
			Date d2=dfm.parse(compareWithDateStr1, new ParsePosition(0));
			long i=d1.getTime();
			long j=d2.getTime();
			long k=(i-j);
			long days=k/(1000*60*60*24);
			int dueInDays = 0;
			////////exrnal cordintor chevk resut ////
			
			CheckListResult checkListResult = null;
			if(toDoRule.getDocType()!=null && !toDoRule.getDocType().equals("")) {
				ToDoResult toDoResult1 = null;
				if(haschklistkey) {
					Long toDoResultId = todoResultTempMap.get(checkKey);
					toDoResult1 = toDoResultManager
							.getForOtherCorpid(toDoResultId);
				} else {
					toDoResult1 = new ToDoResult();
					toDoResult1.setMailStauts("");
				}
				if(hashCheckListKey){
					Long checkListResultId = checkListResultTempMap.get(checklistkey);
					checkListResult = checkListResultManager.get(checkListResultId);
					checkListResult.setUpdatedOn(new Date());
				}
				else{
					checkListResult = new CheckListResult();
					checkListResult.setCreatedOn(new Date());
					checkListResult.setUpdatedOn(new Date());
					checkListResult.setCreatedBy("List Execution");
				}
				checkListResult.setCheckListId(toDoRule.getId());
				if(toDoRule.getEntitytablerequired().equals("ServicePartner"))
				{
					Long tempId=servicePartnerManager.get(toDoResult1.getResultRecordId()).getServiceOrderId();
					checkListResult.setResultId(tempId);
				}
				else if(toDoRule.getEntitytablerequired().equals("WorkTicket"))
				{
					Long tempId=workTicketManager.get(toDoResult1.getResultRecordId()).getServiceOrderId();
					checkListResult.setResultId(tempId);
				}
				else if(toDoRule.getEntitytablerequired().equals("Vehicle"))
				{
					Long tempId=vehicleManager.get(toDoResult1.getResultRecordId()).getServiceOrderId();
					checkListResult.setResultId(tempId);
				}
				else if(toDoRule.getEntitytablerequired().equals("Claim"))
				{
					Long tempId=claimManager.get(toDoResult1.getResultRecordId()).getServiceOrderId();
					checkListResult.setResultId(tempId);
				}else if(toDoRule.getEntitytablerequired().equals("AccountLine"))
				{
					Long tempId=accountLineManager.get(toDoResult1.getResultRecordId()).getServiceOrderId();
					checkListResult.setResultId(tempId);
				}
				else{
					
				checkListResult.setResultId(Long.parseLong(toDoRuleResult[2].toString()));
				}
				checkListResult.setCorpID(sessionCorpID);
				if(toDoResult1.getDurationAddSub()!=null){
					checkListResult.setDuration(toDoResult1.getDurationAddSub());	
					}else{
						checkListResult.setDuration((long) 0);
						
					}
				checkListResult.setMessageDisplayed(toDoRule.getMessagedisplayed());
				checkListResult.setShipper((String)toDoRuleResult[5] + ", " +  (String)toDoRuleResult[4]);
				if(toDoResult1.getOwner()!= null){
					checkListResult.setOwner(toDoResult1.getOwner());
				}else{
					checkListResult.setOwner(toDoRuleResult[6].toString());	
				}
				System.out.println(toDoRuleResult[13].toString());
				checkListResult.setResultNumber(toDoRuleResult[13].toString());
				checkListResult.setUrl(toDoRule.getEntitytablerequired());
				checkListResult.setUpdatedBy("List Execution");
				checkListResult.setRuleNumber(toDoRuleResult[0].toString());
				checkListResult.setDocType(toDoRule.getDocType());
				checkListResult.setToDoResultId(todoRule.getId());
				
				checkListResult.setBillToName(toDoRuleResult[16].toString());
				checkListResult.setRolelist(toDoRule.getRolelist());
				
				checkListResult.setCheckResultForDel("1");
				checkListResult.setEmailNotification(toDoRule.getEmailNotification());
				checkListResult.setManualEmail(toDoRule.getManualEmail());
				checkListResult.setBillToCode(toDoRuleResult[14].toString());
				checkListResult.setBookingAgentCode(bookingAgentCode);
				
				checkListResult.setNoteType("Activity");
				checkListResult.setNoteSubType("ActivityManagement");
				if(todoRule.getIsAccount())
				{
					checkListResult.setIsAccount(true);
					
				}
				checkListResult = checkListResultManager.save(checkListResult);
			}else{
             for (RuleActions ruleActions : byRuleActions) {
				
				key = fileNumber + "-"
				+ todoRule.getRuleNumber() + "-"
				+ todoRule.getId() + "-" + toDoRule.getCorpID()+ "-" + ruleActions.getId();
		       hasKey = todoResultTempMap.containsKey(key);
			System.out.println("haskey----"+hasKey);
			ToDoResult toDoResult = null;
			if(hasKey){
				Long toDoResultId = todoResultTempMap.get(key);
				toDoResult = toDoResultManager
						.getForOtherCorpid(toDoResultId);
			} else {
				toDoResult = new ToDoResult();
				toDoResult.setMailStauts("");
			}
			toDoResult.setCorpID(sessionCorpID);
			if(id!=null){
			toDoResult.setResultRecordId(Long.parseLong(id.toString()));
			if(todoRule.getEntitytablerequired().equals("ServicePartner")){
				Long tempId=servicePartnerManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
				toDoResult.setSupportingId(tempId.toString());
			}else if(todoRule.getEntitytablerequired().equals("Vehicle")){
				Long tempId=vehicleManager.get(toDoResult.getResultRecordId()).getServiceOrderId();
				toDoResult.setSupportingId(tempId.toString());
			}else{
				toDoResult.setSupportingId(id1.toString());
			}
			}else{
				toDoResult.setResultRecordId(Long.parseLong(id1.toString()));
				toDoResult.setSupportingId(id1.toString());
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("Coordinator")){
				toDoResult.setOwner(assignedTo);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("Billing")){
				toDoResult.setOwner(billing);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("Pricing")){
				toDoResult.setOwner(pricing);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("Consultant")){
				toDoResult.setOwner(consultant);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("Payable")){
				toDoResult.setOwner(payable);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("SalesMan")){
				toDoResult.setOwner(salesMan);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("Warehouse Manager")){
				toDoResult.setOwner(wareHouseManager);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("Auditor")){
				toDoResult.setOwner(auditor);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("OriginAgent")){
				toDoResult.setOwner(assignedTo);
				toDoResult.setAgentCode(OAgentCode);
				toDoResult.setAgentName(OAgentName);
				toDoResult.setCorpID("TSFT");
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("DestinationAgent")){
				toDoResult.setOwner(assignedTo);
				toDoResult.setAgentCode(DAgentCode);
				toDoResult.setAgentName(DAgentName);
				toDoResult.setCorpID("TSFT");
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("OriginSubAgent")){
				toDoResult.setOwner(assignedTo);
				toDoResult.setAgentCode(OSAgentCode);
				toDoResult.setAgentName(OSAgentName);
				toDoResult.setCorpID("TSFT");
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("DestinationSubAgent")){
				toDoResult.setOwner(assignedTo);
				toDoResult.setAgentCode(DSAgentCode);
				toDoResult.setAgentName(DSAgentName);
				toDoResult.setCorpID("TSFT");
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("ForwarderAgent")){
				toDoResult.setOwner("PartnerPortal");
				toDoResult.setAgentCode(ForwarderAgentCode);
				toDoResult.setAgentName(ForwarderAgentName);
				
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("BrokerAgent")){
				toDoResult.setOwner("PartnerPortal");
				toDoResult.setAgentCode(BrokerAgentCode);
				toDoResult.setAgentName(BrokerAgentName);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("Claims")){
				toDoResult.setOwner(claimHandler);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("Claim Handler")){
				toDoResult.setOwner(claimHandler);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("MM Counselor")){
				toDoResult.setOwner(mmCounselor);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("Forwarder")){
				toDoResult.setOwner(forwarder);
			}
			if(ruleActions.getRolelist().equalsIgnoreCase("Ops Person")){
				toDoResult.setOwner(OpsPerson);
			}
			if(ruleActions.getDisplayToUser()!= null && !ruleActions.getDisplayToUser().equals("")){
				toDoResult.setOwner(ruleActions.getDisplayToUser());
			}
			if(toDoResult.getOwner()== null){
				toDoResult.setOwner("");
			}
			toDoResult.setToDoActionId(ruleActions.getId());
			toDoResult.setRolelist(ruleActions.getRolelist());
			toDoResult.setMessagedisplayed(ruleActions.getMessagedisplayed());
			
			toDoResult.setTodoRuleId(toDoRule.getId());
			
			//toDoResult.setMessagedisplayed(todoRule.getMessagedisplayed());
			toDoResult.setRuleNumber(todoRule.getRuleNumber());
			//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	       // User user = (User)auth.getPrincipal();
	        toDoResult.setDurationAddSub(days);
			toDoResult.setTestdate(todoRule.getTestdate());
			toDoResult.setShipper(shipper);
			toDoResult.setBillToName(BillToCodeName);
			toDoResult.setResultRecordType(todoRule.getEntitytablerequired());
			toDoResult.setUrl(todoRule.getUrl());
			toDoResult.setFieldToValidate1(todoRule.getFieldToValidate1());
			toDoResult.setFieldToValidate2(todoRule.getFieldToValidate2());
			if(todoRule.getFieldDisplay()==null || todoRule.getFieldDisplay().equalsIgnoreCase(""))
			{
				toDoResult.setFieldDisplay(todoRule.getFieldToValidate1());
			}else{
			toDoResult.setFieldDisplay(todoRule.getFieldDisplay());
			}
			if(!todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){
				if(toDoRuleResult[13]!=null){
				toDoResult.setFileNumber(toDoRuleResult[13].toString());
				}
			}
			if(todoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){
				if(toDoRuleResult[14]!=null){
				toDoResult.setFileNumber(toDoRuleResult[14].toString());
				}
			}
 			toDoResult.setNoteType(ruleActions.getNoteType());
			toDoResult.setNoteSubType(ruleActions.getNoteSubType());
			if(toDoResult.getCorpID().equals("TSFT")){
				if(toDoResult.getAgentCode()!=null && !toDoResult.getAgentCode().equalsIgnoreCase("")){
					String chkAgentType=trackingStatusManager.checkIsRedSkyUser(toDoResult.getAgentCode());
					if((!"RUC".equalsIgnoreCase(chkAgentType)) && !chkAgentType.equals("")){
						toDoResult.setEmailNotification(email);
					}else{
						toDoResult.setEmailNotification("");
					}
				}else{
					toDoResult.setEmailNotification("");
				}
			}else{
				toDoResult.setEmailNotification(email);
			}
			toDoResult.setEmailOn(todoRule.getEmailOn());
			toDoResult.setManualEmail(ruleActions.getManualEmail());
			toDoResult.setRecordToDel("1");
			toDoResult.setBillToCode(billTocode);
			toDoResult.setBookingAgentCode(bookingAgentCode);
			if(toDoRule.getIsAccount())
			{
				toDoResult.setIsAccount(true);
			}
			toDoResult = toDoResultManager.save(toDoResult);
             }
             }}catch(Exception ex){	
				logger.error("Error executing query "+ ex.getStackTrace()[0]);
				System.out.println("\n\n\n\n\n exception--->"+ex);	
				toDoRules=null;
		}
		}	
		}catch(Exception ex){	
			logger.error("Error executing query "+ ex.getStackTrace()[0]);
			System.out.println("\n\n\n\n\n exception--->"+ex);
			toDoRules=null;
		}
			/*if(toDoRule.getDocType()!=null && !toDoRule.getDocType().equals("")) {
				if(todoRule.getEmailNotification()!=null && !todoRule.getEmailNotification().equalsIgnoreCase("") && todoRule.getManualEmail()!=null && !todoRule.getManualEmail().equalsIgnoreCase("") && !todoRule.getCorpID().equalsIgnoreCase("TSFT")){
					sendAllDocCheckListNotifications(sessionCorpID,todoRule.getRuleNumber().toString());
				}else if(todoRule.getEmailNotification()!=null && !todoRule.getEmailNotification().equalsIgnoreCase("")  && !todoRule.getCorpID().equalsIgnoreCase("TSFT")){
					sendNotifications(sessionCorpID,todoRule.getRuleNumber().toString(),"",todoRule.getRolelist().toString());
				}else if(todoRule.getManualEmail()!=null && !todoRule.getManualEmail().equalsIgnoreCase("")  && !todoRule.getCorpID().equalsIgnoreCase("TSFT")){
					sendNotifications(sessionCorpID,todoRule.getRuleNumber().toString(),"manual",todoRule.getRolelist().toString());
				}
			}else{
				if(todoRule.getEmailNotification()!=null && !todoRule.getEmailNotification().equalsIgnoreCase("") && todoRule.getManualEmail()!=null && !todoRule.getManualEmail().equalsIgnoreCase("") && !todoRule.getCorpID().equalsIgnoreCase("TSFT")){
					sendAllNotifications(sessionCorpID,todoRule.getRuleNumber().toString());
				}else if(todoRule.getEmailNotification()!=null && !todoRule.getEmailNotification().equalsIgnoreCase("")  && !todoRule.getCorpID().equalsIgnoreCase("TSFT")){
					sendNotifications(sessionCorpID,todoRule.getRuleNumber().toString(),"",todoRule.getRolelist().toString());
				}else if(todoRule.getManualEmail()!=null && !todoRule.getManualEmail().equalsIgnoreCase("")  && !todoRule.getCorpID().equalsIgnoreCase("TSFT")){
					sendNotifications(sessionCorpID,todoRule.getRuleNumber().toString(),"manual",todoRule.getRolelist().toString());
				}
			}*/
		}else{
			toDoRules=null;	
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return toDoRules;			
	}

	public void sendNotifications(String sessionCorpID, String ruleNo, String eMailType, String entity){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	  List emailList = toDoRuleDao.getToDoResultEmailList(sessionCorpID, ruleNo, eMailType,entity); 
	  if(emailList!=null && !emailList.isEmpty()){
		Iterator itr = emailList.iterator();
		while(itr.hasNext()){
			String email = itr.next().toString(); 
			List toDoResultsIdList = toDoRuleDao.getToDoResults(sessionCorpID, ruleNo, email, eMailType,entity);
			if(toDoResultsIdList!=null && !toDoResultsIdList.isEmpty()){
			  String toAddress = email;
			  String subject = "RedSky Activity Management Notifications";
			  String msgText="<html><head></head><body>";
			  msgText = msgText + " Dear Sir or Madam ,"+"<br><br><br>"+"The following events require your attention."+"<br><br><br>"+"<table style='width:80%;border:2px solid #74B3DC; border-collapse:collapse; font-family:arial,verdana; font-size:12px;'><thead><tr style='background-color:#BCD2EF;border:1px solid #3DAFCB; color:#15428B;font-weight:bold;height:25px;text-align:left;'><th>File Number</th><th>Work List Message</th><th>Field To Validate</th></tr></thead><tbody>";
			  Iterator  itrToDoResult = toDoResultsIdList.iterator();
			  while(itrToDoResult.hasNext()){
				  Long TDRid = new Long(itrToDoResult.next().toString());
				  ToDoResult toDoResult = toDoResultManager.getForOtherCorpid(TDRid) ; 
			 // for (ToDoResult toDoResult: toDoResults){		
				if(!toDoResult.getResultRecordType().equalsIgnoreCase("AccountLineList")){
					if(toDoResult.getFieldDisplay()!=null && (!(toDoResult.getFieldDisplay().equalsIgnoreCase("")))){
						msgText = msgText + "<tr><td style='padding:2px;'><a href='"+getServerUrl()+"/"+toDoResult.getUrl()+".html?from=rule&field="+toDoResult.getFieldToValidate1()+"&field1="+toDoResult.getFieldToValidate2()+"&id="+toDoResult.getResultRecordId()+"&sid="+toDoResult.getSupportingId()+"&request_locale=en'>"+toDoResult.getFileNumber()+"</a></td><td style='padding:2px;'>"+toDoResult.getMessagedisplayed()+"</td><td style='padding:2px;'>"+toDoResult.getFieldDisplay()+"</td></tr>";							
					}else{
						msgText = msgText + "<tr><td style='padding:2px;'><a href='"+getServerUrl()+"/"+toDoResult.getUrl()+".html?from=rule&field="+toDoResult.getFieldToValidate1()+"&field1="+toDoResult.getFieldToValidate2()+"&id="+toDoResult.getResultRecordId()+"&sid="+toDoResult.getSupportingId()+"&request_locale=en'>"+toDoResult.getFileNumber()+"</a></td><td style='padding:2px;'>"+toDoResult.getMessagedisplayed()+"</td><td style='padding:2px;'>"+toDoResult.getFieldToValidate1()+"</td></tr>";
					}
				}else{
					if(toDoResult.getFieldDisplay()!=null && (!(toDoResult.getFieldDisplay().equalsIgnoreCase("")))){
						msgText = msgText + "<tr><td style='padding:2px;'><a href='"+getServerUrl()+"/"+toDoResult.getUrl()+".html?from=rule&field="+toDoResult.getFieldToValidate1()+"&field1="+toDoResult.getFieldToValidate2()+"&sid="+toDoResult.getSupportingId()+"&request_locale=en'>"+toDoResult.getFileNumber()+"</a></td><td style='padding:2px;'>"+toDoResult.getMessagedisplayed()+"</td><td style='padding:2px;'>"+toDoResult.getFieldDisplay()+"</td></tr>";
					}else{
						msgText = msgText + "<tr><td style='padding:2px;'><a href='"+getServerUrl()+"/"+toDoResult.getUrl()+".html?from=rule&field="+toDoResult.getFieldToValidate1()+"&field1="+toDoResult.getFieldToValidate2()+"&sid="+toDoResult.getSupportingId()+"&request_locale=en'>"+toDoResult.getFileNumber()+"</a></td><td style='padding:2px;'>"+toDoResult.getMessagedisplayed()+"</td><td style='padding:2px;'>"+toDoResult.getFieldToValidate1()+"</td></tr>";
					}
				}
				toDoResult.setMailStauts("send");
				toDoResultManager.save(toDoResult);
			  }
			  msgText= msgText + "</tbody></table>"+"<br><br><br><br>"+"Regards," +"<br>"+ " "+"<br>"+"RedSky Team";
			  msgText= msgText + "</body></html>";
				if((getEmailOut()!=null)&&(!getEmailOut().equalsIgnoreCase(""))&&(getEmailOut().equalsIgnoreCase("YES"))){
					  //Run for Dev1 & dev2
											if((getEmailDomain()!=null)&&(!getEmailDomain().equalsIgnoreCase(""))){
												 String chk[]= toAddress.split(",");
												 String strtemp="";
												 for(String str:chk){
													 if((getEmailDomain().indexOf(str.substring(str.indexOf("@")+1, str.length()))>-1)){
															if (!strtemp.equalsIgnoreCase("")) strtemp += ",";
															strtemp += str;
													 }
												 }	
												 toAddress=strtemp;
												}
					if(!toAddress.equalsIgnoreCase("")){
			  
									  try {
										String smtpHost = "localhost";
										String from = "support@redskymobility.com";
										Properties props = System.getProperties();
										props.put("mail.transport.protocol", "smtp");
										props.put("mail.smtp.host", smtpHost);
										Session session = Session.getInstance(props, null);
										session.setDebug(true);
										MimeMessage msg = new MimeMessage(session);
										msg.setFrom(new InternetAddress(from));
										String tempRecipient="";
										String tempRecipientArr[]=toAddress.split(",");
										for(String str:tempRecipientArr){
											if(!userManager.doNotEmailFlag(str).equalsIgnoreCase("YES")){
												if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
												tempRecipient += str;
											}
										}
										toAddress=tempRecipient;
										msg.setRecipients(Message.RecipientType.TO, toAddress);
										//msg.setRecipients(Message.RecipientType.CC, estimatorEmail);
										msg.setSubject(subject);
										msg.setContent(msgText, "text/html; charset=ISO-8859-1");
										msg.setSentDate(new Date());
										Transport.send(msg);
									  }catch (MessagingException mex){
										  logger.error("Error executing query "+ mex.getStackTrace()[0]);
										mex.printStackTrace();
										Exception ex = null;
										if ((ex = mex.getNextException()) != null) {
											ex.printStackTrace();
									    }else {}
									}}
					}
			  
		  }
		 }
	   }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	  }
	
	public void sendAllNotifications(String sessionCorpID, String ruleNumber){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String ipAddress = "";
		try {
			ipAddress = ""+InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			 logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		  List emailList = toDoRuleDao.getToDoResultEmailList(sessionCorpID, ruleNumber);
		  if(emailList!=null && !emailList.isEmpty()){
			Iterator itr = emailList.iterator();
			while(itr.hasNext()){
				String email = itr.next().toString(); 
				List<ToDoResult> toDoResults = toDoRuleDao.getToDoResults(sessionCorpID,email,ruleNumber);
				if(toDoResults!=null && !toDoResults.isEmpty()){
				  String toAddress = email;
				  String subject = "RedSky Activity Management Notifications";
				  String msgText="<html><head></head><body>";
				  msgText = msgText + " Dear Sir or Madam ,"+"<br><br><br>"+"The following events require your attention."+"<br><br><br>"+"<table style='width:80%;border:2px solid #74B3DC; border-collapse:collapse; font-family:arial,verdana; font-size:12px;'><thead><tr style='background-color:#BCD2EF;border:1px solid #3DAFCB; color:#15428B;font-weight:bold;height:25px;text-align:left;'><th>File Number</th><th>Work List Message</th><th>Field To Validate</th></tr></thead><tbody>";
				  for (ToDoResult toDoResult: toDoResults){
					if(!toDoResult.getResultRecordType().equalsIgnoreCase("AccountLineList")){	
						if(toDoResult.getFieldDisplay()!=null && (!(toDoResult.getFieldDisplay().equalsIgnoreCase("")))){
							msgText = msgText + "<tr><td style='padding:2px;'><a href='"+getServerUrl()+"/"+toDoResult.getUrl()+".html?from=rule&field="+toDoResult.getFieldToValidate1()+"&field1="+toDoResult.getFieldToValidate2()+"&id="+toDoResult.getResultRecordId()+"&sid="+toDoResult.getSupportingId()+"&request_locale=en'>"+toDoResult.getFileNumber()+"</a></td><td style='padding:2px;'>"+toDoResult.getMessagedisplayed()+"</td><td style='padding:2px;'>"+toDoResult.getFieldDisplay()+"</td></tr>";
						}else{
							msgText = msgText + "<tr><td style='padding:2px;'><a href='"+getServerUrl()+"/"+toDoResult.getUrl()+".html?from=rule&field="+toDoResult.getFieldToValidate1()+"&field1="+toDoResult.getFieldToValidate2()+"&id="+toDoResult.getResultRecordId()+"&sid="+toDoResult.getSupportingId()+"&request_locale=en'>"+toDoResult.getFileNumber()+"</a></td><td style='padding:2px;'>"+toDoResult.getMessagedisplayed()+"</td><td style='padding:2px;'>"+toDoResult.getFieldToValidate1()+"</td></tr>";	
						}
					}else{
						if(toDoResult.getFieldDisplay()!=null && (!(toDoResult.getFieldDisplay().equalsIgnoreCase("")))){
							msgText = msgText + "<tr><td style='padding:2px;'><a href='"+getServerUrl()+"/"+toDoResult.getUrl()+".html?from=rule&field="+toDoResult.getFieldToValidate1()+"&field1="+toDoResult.getFieldToValidate2()+"&sid="+toDoResult.getSupportingId()+"&request_locale=en'>"+toDoResult.getFileNumber()+"</a></td><td style='padding:2px;'>"+toDoResult.getMessagedisplayed()+"</td><td style='padding:2px;'>"+toDoResult.getFieldDisplay()+"</td></tr>";
						}else{
							msgText = msgText + "<tr><td style='padding:2px;'><a href='"+getServerUrl()+"/"+toDoResult.getUrl()+".html?from=rule&field="+toDoResult.getFieldToValidate1()+"&field1="+toDoResult.getFieldToValidate2()+"&sid="+toDoResult.getSupportingId()+"&request_locale=en'>"+toDoResult.getFileNumber()+"</a></td><td style='padding:2px;'>"+toDoResult.getMessagedisplayed()+"</td><td style='padding:2px;'>"+toDoResult.getFieldToValidate1()+"</td></tr>";
						}
					}
					toDoResult.setMailStauts("send");
					toDoResultManager.save(toDoResult);
				  }
				  msgText= msgText + "</tbody></table>"+"<br><br>IP Address:"+ipAddress+"<br><br>"+"Regards," +"<br>"+ " "+"<br>"+"RedSky Team";
				  msgText= msgText + "</body></html>";
					if((getEmailOut()!=null)&&(!getEmailOut().equalsIgnoreCase(""))&&(getEmailOut().equalsIgnoreCase("YES"))){
						  //Run for Dev1 & dev2
												if((getEmailDomain()!=null)&&(!getEmailDomain().equalsIgnoreCase(""))){
													 String chk[]= toAddress.split(",");
													 String strtemp="";
													 for(String str:chk){
														 if((getEmailDomain().indexOf(str.substring(str.indexOf("@")+1, str.length()))>-1)){
																if (!strtemp.equalsIgnoreCase("")) strtemp += ",";
																strtemp += str;
														 }
													 }	
													 toAddress=strtemp;
													}
						if(!toAddress.equalsIgnoreCase("")){
							  try {
									String smtpHost = "localhost";
									String from = "support@redskymobility.com";
									Properties props = System.getProperties();
									props.put("mail.transport.protocol", "smtp");
									props.put("mail.smtp.host", smtpHost);
									Session session = Session.getInstance(props, null);
									session.setDebug(true);
									MimeMessage msg = new MimeMessage(session);
									msg.setFrom(new InternetAddress(from));
									String tempRecipient="";
									String tempRecipientArr[]=toAddress.split(",");
									for(String str:tempRecipientArr){
										if(!userManager.doNotEmailFlag(str).equalsIgnoreCase("YES")){
											if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
											tempRecipient += str;
										}
									}
									toAddress=tempRecipient;
									msg.setRecipients(Message.RecipientType.TO, toAddress);
									//msg.setRecipients(Message.RecipientType.CC, estimatorEmail);
									msg.setSubject(subject);
									msg.setContent(msgText, "text/html; charset=ISO-8859-1");
									msg.setSentDate(new Date());
									Transport.send(msg);
								  }catch (MessagingException mex){
									  logger.error("Error executing query "+ mex.getStackTrace()[0]);
									mex.printStackTrace();
									Exception ex = null;
									if ((ex = mex.getNextException()) != null) {
										ex.printStackTrace();
								    }else {}
								}
						}
					}
				
			  }
			 }
		   }
		  }
	
	
	
	
	public void sendAllDocCheckListNotifications(String sessionCorpID, String ruleNumber){/*
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String ipAddress = "";
		try {
			ipAddress = ""+InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			 logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		  List emailList = toDoRuleDao.getDocCheckListResultEmailList(sessionCorpID, ruleNumber);
		  if(emailList!=null && !emailList.isEmpty()){
			Iterator itr = emailList.iterator();
			while(itr.hasNext()){
				String email = itr.next().toString(); 
				List<CheckListResult> checkListResults = toDoRuleDao.getCheckListResult(sessionCorpID,email,ruleNumber);
				if(checkListResults!=null && !checkListResults.isEmpty()){
				  String toAddress = email;
				  String subject = "RedSky Activity Management Notifications";
				  String msgText="<html><head></head><body>";
				  msgText = msgText + " Dear Sir or Madam ,"+"<br><br><br>"+"The following events require your attention."+"<br><br><br>"+"<table style='width:80%;border:2px solid #74B3DC; border-collapse:collapse; font-family:arial,verdana; font-size:12px;'><thead><tr style='background-color:#BCD2EF;border:1px solid #3DAFCB; color:#15428B;font-weight:bold;height:25px;text-align:left;'><th>File Number</th><th>Work List Message</th><th>Field To Validate</th></tr></thead><tbody>";
				  for (CheckListResult checkListResult: checkListResults){
					if(!checkListResult.getResultRecordType().equalsIgnoreCase("AccountLineList")){	
						if(checkListResult.getFieldDisplay()!=null && (!(checkListResult.getFieldDisplay().equalsIgnoreCase("")))){
							msgText = msgText + "<tr><td style='padding:2px;'><a href='"+getServerUrl()+"/"+checkListResult.getUrl()+".html?from=rule&field="+checkListResult.getFieldToValidate1()+"&field1="+checkListResult.getFieldToValidate2()+"&id="+checkListResult.getResultRecordId()+"&sid="+checkListResult.getSupportingId()+"'>"+checkListResult.getFileNumber()+"</a></td><td style='padding:2px;'>"+checkListResult.getMessagedisplayed()+"</td><td style='padding:2px;'>"+checkListResult.getFieldDisplay()+"</td></tr>";
						}else{
							msgText = msgText + "<tr><td style='padding:2px;'><a href='"+getServerUrl()+"/"+checkListResult.getUrl()+".html?from=rule&field="+checkListResult.getFieldToValidate1()+"&field1="+checkListResult.getFieldToValidate2()+"&id="+checkListResult.getResultRecordId()+"&sid="+checkListResult.getSupportingId()+"'>"+checkListResult.getFileNumber()+"</a></td><td style='padding:2px;'>"+checkListResult.getMessagedisplayed()+"</td><td style='padding:2px;'>"+checkListResult.getFieldToValidate1()+"</td></tr>";	
						}
					}else{
						if(checkListResult.getFieldDisplay()!=null && (!(checkListResult.getFieldDisplay().equalsIgnoreCase("")))){
							msgText = msgText + "<tr><td style='padding:2px;'><a href='"+getServerUrl()+"/"+checkListResult.getUrl()+".html?from=rule&field="+checkListResult.getFieldToValidate1()+"&field1="+checkListResult.getFieldToValidate2()+"&sid="+checkListResult.getSupportingId()+"'>"+checkListResult.getFileNumber()+"</a></td><td style='padding:2px;'>"+checkListResult.getMessagedisplayed()+"</td><td style='padding:2px;'>"+checkListResult.getFieldDisplay()+"</td></tr>";
						}else{
							msgText = msgText + "<tr><td style='padding:2px;'><a href='"+getServerUrl()+"/"+checkListResult.getUrl()+".html?from=rule&field="+checkListResult.getFieldToValidate1()+"&field1="+checkListResult.getFieldToValidate2()+"&sid="+checkListResult.getSupportingId()+"'>"+checkListResult.getFileNumber()+"</a></td><td style='padding:2px;'>"+checkListResult.getMessagedisplayed()+"</td><td style='padding:2px;'>"+checkListResult.getFieldToValidate1()+"</td></tr>";
						}
					}
					checkListResult.setMailStauts("send");
					checkListResultManager.save(checkListResult);
				  }
				  msgText= msgText + "</tbody></table>"+"<br><br>IP Address:"+ipAddress+"<br><br>"+"Regards," +"<br>"+ " "+"<br>"+"Redsky Team";
				  msgText= msgText + "</body></html>";
					if((getEmailOut()!=null)&&(!getEmailOut().equalsIgnoreCase(""))&&(getEmailOut().equalsIgnoreCase("YES"))){
						  //Run for Dev1 & dev2
												if((getEmailDomain()!=null)&&(!getEmailDomain().equalsIgnoreCase(""))){
													 String chk[]= toAddress.split(",");
													 String strtemp="";
													 for(String str:chk){
														 if((getEmailDomain().indexOf(str.substring(str.indexOf("@")+1, str.length()))>-1)){
																if (!strtemp.equalsIgnoreCase("")) strtemp += ",";
																strtemp += str;
														 }
													 }	
													 toAddress=strtemp;
													}
						if(!toAddress.equalsIgnoreCase("")){
							  try {
									String smtpHost = "localhost";
									String from = "support@redskymobility.com";
									Properties props = System.getProperties();
									props.put("mail.transport.protocol", "smtp");
									props.put("mail.smtp.host", smtpHost);
									Session session = Session.getInstance(props, null);
									session.setDebug(true);
									MimeMessage msg = new MimeMessage(session);
									msg.setFrom(new InternetAddress(from));
									String tempRecipient="";
									String tempRecipientArr[]=toAddress.split(",");
									for(String str:tempRecipientArr){
										if(!userManager.doNotEmailFlag(str).equalsIgnoreCase("YES")){
											if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
											tempRecipient += str;
										}
									}
									toAddress=tempRecipient;
									msg.setRecipients(Message.RecipientType.TO, toAddress);
									//msg.setRecipients(Message.RecipientType.CC, estimatorEmail);
									msg.setSubject(subject);
									msg.setContent(msgText, "text/html; charset=ISO-8859-1");
									msg.setSentDate(new Date());
									Transport.send(msg);
								  }catch (MessagingException mex){
									  logger.error("Error executing query "+ mex.getStackTrace()[0]);
									mex.printStackTrace();
									Exception ex = null;
									if ((ex = mex.getNextException()) != null) {
										ex.printStackTrace();
								    }else {}
								}
						}
					}
				
			  }
			 }
		   }
		  */}	 
	
	public void setToDoResultManager(ToDoResultManager toDoResultManager) {
		this.toDoResultManager = toDoResultManager;
	}

	public List getToDoRules() {
		return toDoRules;
	}

	public void setToDoRules(List toDoRules) {
		this.toDoRules = toDoRules;
	}
	
	private String getShipper(Object[] toDoRuleResult) {
		return (String)toDoRuleResult[5] + ", " +  (String)toDoRuleResult[4];
	}
	
	private String getAssignedTo(Object[] toDoRuleResult) {
		return (String)toDoRuleResult[6];
	}
	
	private String getBilling(Object[] toDoRuleResult) {
		return (String)toDoRuleResult[7];
	}

	private List executeRule(ToDoRule todoRule, Long recordID, String sessionCorpID) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		// 1. build rule expression
		String ruleExpression ="";
		try{
			ruleExpression= buildRuleExpression(todoRule, recordID, sessionCorpID);
		}catch(Exception e){
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return toDoRuleDao.executeRule(ruleExpression,todoRule);
	}
	
	private List executeRuleForcast(ToDoRule todoRule, Long recordID, String sessionCorpID) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		// 1. build rule expression
		String ruleExpression ="";
		try{
			ruleExpression= buildRuleExpressionForcast(todoRule, recordID, sessionCorpID);
		}catch(Exception e){
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return toDoRuleDao.executeRule(ruleExpression,todoRule);
	}

	public String buildRuleExpressionForcast(ToDoRule todoRule, Long recordID, String sessionCorpID){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String baseExpression = todoRule.getExpression();
		String entity = todoRule.getEntitytablerequired();
		String roleList = todoRule.getRolelist();
		if (entity.equalsIgnoreCase("TrackingStatus")) {
			entity = "ServiceOrder";		
		}else if (entity.equalsIgnoreCase("LeadCapture")) {
			entity = "CustomerFile";
		}
		
		String testDate = todoRule.getTestdate();
		String roleValue = todoRule.getRolelist();
		int duration= Integer.parseInt(todoRule.getDurationAddSub().toString());
		duration = -duration;
		Date compareWithDate = DateUtils.addDays(getCurrentDate(), duration);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr = dfm.format(compareWithDate);
		String docType = todoRule.getDocType();
		String dateExpression ="";
		/*if(todoRule.getService()!=null && !todoRule.getService().equals("") && todoRule.getServiceCondition()!=null && !todoRule.getServiceCondition().equals(""))
		{
			dateExpression = "and serviceorder.serviceType "+todoRule.getServiceCondition() +" "+todoRule.getService();
		}
		if(todoRule.getMode()!=null && !todoRule.getMode().equals("") && todoRule.getModeCondition()!=null && !todoRule.getModeCondition().equals(""))
		{
			dateExpression =dateExpression+ " and serviceorder.mode "+todoRule.getModeCondition() +" "+todoRule.getMode();
		}
		if(todoRule.getRouting()!=null && !todoRule.getRouting().equals("") && todoRule.getRoutingCondition()!=null && !todoRule.getRoutingCondition().equals(""))
		{
			dateExpression = dateExpression+ " and serviceorder.routing "+todoRule.getRoutingCondition() +" "+todoRule.getRouting();
		}
		if(todoRule.getJobType()!=null && !todoRule.getJobType().equals("") && todoRule.getJobTypeCondition()!=null && !todoRule.getJobTypeCondition().equals(""))
		{
			dateExpression = dateExpression+ " and serviceorder.job "+todoRule.getJobTypeCondition() +" "+todoRule.getJobType();
		}
		if(todoRule.getBillToCode()!=null && !todoRule.getBillToCode().equals("") && todoRule.getBillToCodeCondition()!=null && !todoRule.getBillToCodeCondition().equals(""))
		{
			dateExpression = dateExpression+ " and serviceorder.billToCode "+todoRule.getBillToCodeCondition() +" "+todoRule.getBillToCode();
		}
		if(todoRule.getBookingAgent()!=null && !todoRule.getBookingAgent().equals("") && todoRule.getBookingAgentCondition()!=null && !todoRule.getBookingAgentCondition().equals(""))
		{
			dateExpression = dateExpression+ " and serviceorder.bookingAgentCode "+todoRule.getBookingAgentCondition() +" "+todoRule.getBookingAgent();
		}*/
		System.out.println("dateExpression >>>>> "+dateExpression);
		/*if(sessionCorpID.equalsIgnoreCase("SSCW")){
			 dateExpression = "date_format("+testDate+" ,'%Y-%m-%d') >='2015-01-01' and " +"date_format("+testDate+" ,'%Y-%m-%d')" + " <= '" + compareWithDateStr+"'";	
		}else{
			 dateExpression = "date_format("+testDate+" ,'%Y-%m-%d')" + " <= '" + compareWithDateStr+"'";
		}*/
		
		String ruleExpression =  ToDoRuleExpressionBuilder.buildExpressionForcating(entity, baseExpression, dateExpression, recordID, sessionCorpID,roleValue,todoRule.getTestdate(),docType);
		if(!entity.equalsIgnoreCase("WorkTicket")){
			String tempRuleExpression ="";
			if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification().equalsIgnoreCase(""))){
				tempRuleExpression = "Select "+todoRule.getRuleNumber() + " " + " , " + getJoinid(entity) +" " +","+ // 0,1
				testDate + " ," + // 2
				ToDoRuleExpressionBuilder.getShipper(entity) + "," + // 3
				ToDoRuleExpressionBuilder.getAssignedToList(entity)+"," + //4
				ToDoRuleExpressionBuilder.getBilling(entity)+"," + //5
				ToDoRuleExpressionBuilder.getPayable(entity)+"," + //6
				ToDoRuleExpressionBuilder.getPricing(entity)+"," + //7
				ToDoRuleExpressionBuilder.getConsultant(entity)+"," + //8
				ToDoRuleExpressionBuilder.getSalesMan(entity)+ "," + //9
				ToDoRuleExpressionBuilder.getAuditor(entity)+ "," + //10
				ToDoRuleExpressionBuilder.getFileNumber(entity)+ "," + //11 
				todoRule.getEmailNotification()+","+//12
				ToDoRuleExpressionBuilder.getBillToCode(entity)+","+//13
				ToDoRuleExpressionBuilder.getBillToName(entity). toLowerCase();
				if(entity.equalsIgnoreCase("ServiceOrder")){
				tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getOrginAgent(entity)+ ","+
				ToDoRuleExpressionBuilder.getDestinationAgent(entity)+ ","+
				ToDoRuleExpressionBuilder.getOrginSubAgent(entity)+ ","+
				ToDoRuleExpressionBuilder.getDestinationSubAgent(entity)+ ","+
				ToDoRuleExpressionBuilder.getForwarderAgent(entity)+ ","+
				ToDoRuleExpressionBuilder.getBrokerAgent(entity)+ ","+
				ToDoRuleExpressionBuilder.getMmCounselor(entity)+ ","+
				ToDoRuleExpressionBuilder.getOrginAgentName(entity)+ ","+
				ToDoRuleExpressionBuilder.getDestinationAgentName(entity)+ ","+
				ToDoRuleExpressionBuilder.getOrginSubAgentName(entity)+ ","+
				ToDoRuleExpressionBuilder.getDestinationSubAgentName(entity)+ ","+
				ToDoRuleExpressionBuilder.getForwarderAgentName(entity)+ ","+
				ToDoRuleExpressionBuilder.getBrokerAgentName(entity)+","+
				ToDoRuleExpressionBuilder.getOpsPerson(entity)
				+ ","+
				ToDoRuleExpressionBuilder.getForwarder(entity);
				
				}
				if(entity.equalsIgnoreCase("CustomerFile")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getOrginAgent(entity)+ ","+ToDoRuleExpressionBuilder.getOrginAgentName(entity);
				}
				
				if(entity.equalsIgnoreCase("ServicePartner")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getOrginAgent(entity)+ ","+ToDoRuleExpressionBuilder.getOrginAgentName(entity)+ ","+
							ToDoRuleExpressionBuilder.getForwarder(entity);
				}
				
				if(entity.equalsIgnoreCase("Billing")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getClaimHandLer(entity,roleList)+","+
					ToDoRuleExpressionBuilder.getReceiptdate(entity)+ ","+
					ToDoRuleExpressionBuilder.getForwarder(entity);
				}
				if(entity.equalsIgnoreCase("Claim")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getClaimHandLer(entity,roleList);
				}
				ruleExpression  =tempRuleExpression+ ruleExpression;
			}else{
				tempRuleExpression = "Select "+todoRule.getRuleNumber() + " " + " , " + getJoinid(entity) +" " +","+ // 0,1
				testDate + " ," + // 2
				ToDoRuleExpressionBuilder.getShipper(entity) + "," + // 3
				ToDoRuleExpressionBuilder.getAssignedToList(entity)+"," + //4
				ToDoRuleExpressionBuilder.getBilling(entity)+"," + //5
				ToDoRuleExpressionBuilder.getPayable(entity)+"," + //6
				ToDoRuleExpressionBuilder.getPricing(entity)+"," + //7
				ToDoRuleExpressionBuilder.getConsultant(entity)+"," + //8
				ToDoRuleExpressionBuilder.getSalesMan(entity)+ "," + //9
				ToDoRuleExpressionBuilder.getAuditor(entity)+ "," + //10
				ToDoRuleExpressionBuilder.getFileNumber(entity)+","+ //11
				ToDoRuleExpressionBuilder.getBillToCode(entity)+","+ //12
				ToDoRuleExpressionBuilder.getBillToName(entity); //13
				if(entity.equalsIgnoreCase("ServiceOrder")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getOrginAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getDestinationAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getOrginSubAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getDestinationSubAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getForwarderAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getBrokerAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getMmCounselor(entity)+ ","+
					ToDoRuleExpressionBuilder.getOrginAgentName(entity)+ ","+
				    ToDoRuleExpressionBuilder.getDestinationAgentName(entity)+ ","+
				    ToDoRuleExpressionBuilder.getOrginSubAgentName(entity)+ ","+
				    ToDoRuleExpressionBuilder.getDestinationSubAgentName(entity)+ ","+
				    ToDoRuleExpressionBuilder.getForwarderAgentName(entity)+ ","+
				    ToDoRuleExpressionBuilder.getBrokerAgentName(entity)+","+
					ToDoRuleExpressionBuilder.getOpsPerson(entity)+ ","+
					ToDoRuleExpressionBuilder.getForwarder(entity);
					
				}
				if(entity.equalsIgnoreCase("CustomerFile")){
					tempRuleExpression = tempRuleExpression +" ,"+ //14
					ToDoRuleExpressionBuilder.getOrginAgent(entity)+ ","+ //15
					ToDoRuleExpressionBuilder.getOrginAgentName(entity); //16
				}
				
				if(entity.equalsIgnoreCase("ServicePartner")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getOrginAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getOrginAgentName(entity)+ ","+
					ToDoRuleExpressionBuilder.getForwarder(entity);
				}
				
				if(entity.equalsIgnoreCase("Billing")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getClaimHandLer(entity,roleList)+","+
					ToDoRuleExpressionBuilder.getReceiptdate(entity)+ ","+
					ToDoRuleExpressionBuilder.getForwarder(entity);
				}	
				if(entity.equalsIgnoreCase("Claim")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getClaimHandLer(entity,roleList);
				}
				ruleExpression  =tempRuleExpression+ ruleExpression;	
			} 
		}		 
		if(entity.equalsIgnoreCase("WorkTicket")){
			if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification().equalsIgnoreCase(""))){
				ruleExpression = " Select "+todoRule.getRuleNumber() + " " + " , " + getJoinid(entity) +" " +","+ testDate + " ," +
				ToDoRuleExpressionBuilder.getShipper(entity) + "," + ToDoRuleExpressionBuilder.getAssignedToList(entity)+"," + ToDoRuleExpressionBuilder.getBilling(entity)+"," + ToDoRuleExpressionBuilder.getPayable(entity)+"," + ToDoRuleExpressionBuilder.getPricing(entity)+"," + ToDoRuleExpressionBuilder.getConsultant(entity)+"," + ToDoRuleExpressionBuilder.getSalesMan(entity)+ "," + ToDoRuleExpressionBuilder.getWarehousemanager(entity)+ "," + ToDoRuleExpressionBuilder.getAuditor(entity)+ "," + ToDoRuleExpressionBuilder.getFileNumber(entity)+ "," + todoRule.getEmailNotification()+","+ToDoRuleExpressionBuilder.getBillToCode(entity)+","+ToDoRuleExpressionBuilder.getBillToName(entity).toLowerCase()+  
				 " "+ ruleExpression;
			}else{
				ruleExpression = " Select "+todoRule.getRuleNumber() + " " + " , " + getJoinid(entity) +" " +","+ testDate + " ," +
				ToDoRuleExpressionBuilder.getShipper(entity) + "," + ToDoRuleExpressionBuilder.getAssignedToList(entity)+"," + ToDoRuleExpressionBuilder.getBilling(entity)+"," + ToDoRuleExpressionBuilder.getPayable(entity)+"," + ToDoRuleExpressionBuilder.getPricing(entity)+"," + ToDoRuleExpressionBuilder.getConsultant(entity)+"," + ToDoRuleExpressionBuilder.getSalesMan(entity)+ "," + ToDoRuleExpressionBuilder.getWarehousemanager(entity)+ "," + ToDoRuleExpressionBuilder.getAuditor(entity)+ " ," + ToDoRuleExpressionBuilder.getFileNumber(entity)+","+ToDoRuleExpressionBuilder.getBillToCode(entity)+","+ToDoRuleExpressionBuilder.getBillToName(entity)+
				" "+ ruleExpression;
			}
		}
		System.out.println("\n\n\n ruleID-->>"+todoRule.getRuleNumber());
		System.out.println("\n\n\n Expression-->>"+ruleExpression);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return ruleExpression;			
	}
	
	
	public String buildRuleExpression(ToDoRule todoRule, Long recordID, String sessionCorpID){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String baseExpression = todoRule.getExpression();
		String entity = todoRule.getEntitytablerequired();
		String roleList = todoRule.getRolelist();
		if (entity.equalsIgnoreCase("TrackingStatus")) {
			entity = "ServiceOrder";		
		}else if (entity.equalsIgnoreCase("LeadCapture")) {
			entity = "CustomerFile";
		}
		
		String testDate = todoRule.getTestdate();
		String roleValue = todoRule.getRolelist();
		int duration= Integer.parseInt(todoRule.getDurationAddSub().toString());
		duration = -duration;
		Date compareWithDate = DateUtils.addDays(getCurrentDate(), duration);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr = dfm.format(compareWithDate);
		String docType = todoRule.getDocType();
		String dateExpression ="date_format("+testDate+" ,'%Y-%m-%d')" + " <= '" + compareWithDateStr+"'";
		/*if(sessionCorpID.equalsIgnoreCase("SSCW")){
			 dateExpression = "date_format("+testDate+" ,'%Y-%m-%d') >='2015-01-01' and " +"date_format("+testDate+" ,'%Y-%m-%d')" + " <= '" + compareWithDateStr+"'";	
		}else{
			 dateExpression = "date_format("+testDate+" ,'%Y-%m-%d')" + " <= '" + compareWithDateStr+"'";
		}*/String bookingagentname="";
		bookingagentname=","+ToDoRuleExpressionBuilder.getBookingAgentName(entity);
		
		String ruleExpression =  ToDoRuleExpressionBuilder.buildExpression(entity, baseExpression, dateExpression, recordID, sessionCorpID,roleValue,todoRule.getTestdate(),docType);
		if(!entity.equalsIgnoreCase("WorkTicket")){
			String tempRuleExpression ="";
			if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification().equalsIgnoreCase(""))){
				tempRuleExpression = "Select "+todoRule.getRuleNumber() + " " + " , " + getJoinid(entity) +" " +","+ // 0,1
				testDate + " ," + // 2
				ToDoRuleExpressionBuilder.getShipper(entity) + "," + // 3
				ToDoRuleExpressionBuilder.getAssignedToList(entity)+"," + //4
				ToDoRuleExpressionBuilder.getBilling(entity)+"," + //5
				ToDoRuleExpressionBuilder.getPayable(entity)+"," + //6
				ToDoRuleExpressionBuilder.getPricing(entity)+"," + //7
				ToDoRuleExpressionBuilder.getConsultant(entity)+"," + //8
				ToDoRuleExpressionBuilder.getSalesMan(entity)+ "," + //9
				ToDoRuleExpressionBuilder.getAuditor(entity)+ "," + //10
				ToDoRuleExpressionBuilder.getFileNumber(entity)+ "," + //11 
				todoRule.getEmailNotification()+","+//12
				ToDoRuleExpressionBuilder.getBillToCode(entity)+","+//13
				ToDoRuleExpressionBuilder.getBillToName(entity). toLowerCase();
				if(entity.equalsIgnoreCase("ServiceOrder")){
				tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getOrginAgent(entity)+ ","+
				ToDoRuleExpressionBuilder.getDestinationAgent(entity)+ ","+
				ToDoRuleExpressionBuilder.getOrginSubAgent(entity)+ ","+
				ToDoRuleExpressionBuilder.getDestinationSubAgent(entity)+ ","+
				ToDoRuleExpressionBuilder.getForwarderAgent(entity)+ ","+
				ToDoRuleExpressionBuilder.getBrokerAgent(entity)+ ","+
				ToDoRuleExpressionBuilder.getMmCounselor(entity)+ ","+
				ToDoRuleExpressionBuilder.getOrginAgentName(entity)+ ","+
				ToDoRuleExpressionBuilder.getDestinationAgentName(entity)+ ","+
				ToDoRuleExpressionBuilder.getOrginSubAgentName(entity)+ ","+
				ToDoRuleExpressionBuilder.getDestinationSubAgentName(entity)+ ","+
				ToDoRuleExpressionBuilder.getForwarderAgentName(entity)+ ","+
				ToDoRuleExpressionBuilder.getBrokerAgentName(entity)+","+
				ToDoRuleExpressionBuilder.getOpsPerson(entity)+ ","+
				ToDoRuleExpressionBuilder.getForwarder(entity);
				}
				if(entity.equalsIgnoreCase("CustomerFile")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getOrginAgent(entity)+ ","+ToDoRuleExpressionBuilder.getOrginAgentName(entity);
				}
				
				if(entity.equalsIgnoreCase("ServicePartner")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getOrginAgent(entity)+ ","+ToDoRuleExpressionBuilder.getOrginAgentName(entity)+ ","+
							ToDoRuleExpressionBuilder.getForwarder(entity)+ ","+ToDoRuleExpressionBuilder.getDestinationAgent(entity)+ ","+ToDoRuleExpressionBuilder.getDestinationAgentName(entity);
				}
				
				if(entity.equalsIgnoreCase("Billing")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getClaimHandLer(entity,roleList)+","+
					ToDoRuleExpressionBuilder.getReceiptdate(entity)+ ","+
					ToDoRuleExpressionBuilder.getForwarder(entity);
				}
				if(entity.equalsIgnoreCase("Claim")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getClaimHandLer(entity,roleList);
				}
				ruleExpression  =tempRuleExpression+bookingagentname+ ruleExpression;
			}else{
				tempRuleExpression = "Select "+todoRule.getRuleNumber() + " " + " , " + getJoinid(entity) +" " +","+ // 0,1
				testDate + " ," + // 2
				ToDoRuleExpressionBuilder.getShipper(entity) + "," + // 3
				ToDoRuleExpressionBuilder.getAssignedToList(entity)+"," + //4
				ToDoRuleExpressionBuilder.getBilling(entity)+"," + //5
				ToDoRuleExpressionBuilder.getPayable(entity)+"," + //6
				ToDoRuleExpressionBuilder.getPricing(entity)+"," + //7
				ToDoRuleExpressionBuilder.getConsultant(entity)+"," + //8
				ToDoRuleExpressionBuilder.getSalesMan(entity)+ "," + //9
				ToDoRuleExpressionBuilder.getAuditor(entity)+ "," + //10
				ToDoRuleExpressionBuilder.getFileNumber(entity)+","+ //11
				ToDoRuleExpressionBuilder.getBillToCode(entity)+","+ //12
				ToDoRuleExpressionBuilder.getBillToName(entity); //13
				if(entity.equalsIgnoreCase("ServiceOrder")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getOrginAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getDestinationAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getOrginSubAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getDestinationSubAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getForwarderAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getBrokerAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getMmCounselor(entity)+ ","+
					ToDoRuleExpressionBuilder.getOrginAgentName(entity)+ ","+
				    ToDoRuleExpressionBuilder.getDestinationAgentName(entity)+ ","+
				    ToDoRuleExpressionBuilder.getOrginSubAgentName(entity)+ ","+
				    ToDoRuleExpressionBuilder.getDestinationSubAgentName(entity)+ ","+
				    ToDoRuleExpressionBuilder.getForwarderAgentName(entity)+ ","+
				    ToDoRuleExpressionBuilder.getBrokerAgentName(entity)+","+
					ToDoRuleExpressionBuilder.getOpsPerson(entity)+ ","+
					ToDoRuleExpressionBuilder.getForwarder(entity);
					
				}
				if(entity.equalsIgnoreCase("CustomerFile")){
					tempRuleExpression = tempRuleExpression +" ,"+ //14
					ToDoRuleExpressionBuilder.getOrginAgent(entity)+ ","+ //15
					ToDoRuleExpressionBuilder.getOrginAgentName(entity); //16
				}
				
				if(entity.equalsIgnoreCase("ServicePartner")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getOrginAgent(entity)+ ","+
					ToDoRuleExpressionBuilder.getOrginAgentName(entity)+ ","+
					ToDoRuleExpressionBuilder.getForwarder(entity)+ ","+ToDoRuleExpressionBuilder.getDestinationAgent(entity)+ ","+ToDoRuleExpressionBuilder.getDestinationAgentName(entity);
				}
				
				if(entity.equalsIgnoreCase("Billing")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getClaimHandLer(entity,roleList)+","+
					ToDoRuleExpressionBuilder.getReceiptdate(entity)+ ","+
					ToDoRuleExpressionBuilder.getForwarder(entity);
				}	
				if(entity.equalsIgnoreCase("Claim")){
					tempRuleExpression = tempRuleExpression +" ,"+ ToDoRuleExpressionBuilder.getClaimHandLer(entity,roleList);
				}
				ruleExpression  =tempRuleExpression+bookingagentname+ ruleExpression;	
			} 
		}		 
		if(entity.equalsIgnoreCase("WorkTicket")){
			if(todoRule.getEmailNotification()!=null && !(todoRule.getEmailNotification().equalsIgnoreCase(""))){
				ruleExpression = " Select "+todoRule.getRuleNumber() + " " + " , " + getJoinid(entity) +" " +","+ testDate + " ," +
				ToDoRuleExpressionBuilder.getShipper(entity) + "," + ToDoRuleExpressionBuilder.getAssignedToList(entity)+"," + ToDoRuleExpressionBuilder.getBilling(entity)+"," + ToDoRuleExpressionBuilder.getPayable(entity)+"," + ToDoRuleExpressionBuilder.getPricing(entity)+"," + ToDoRuleExpressionBuilder.getConsultant(entity)+"," + ToDoRuleExpressionBuilder.getSalesMan(entity)+ "," + ToDoRuleExpressionBuilder.getWarehousemanager(entity)+ "," + ToDoRuleExpressionBuilder.getAuditor(entity)+ "," + ToDoRuleExpressionBuilder.getFileNumber(entity)+ "," + todoRule.getEmailNotification()+","+ToDoRuleExpressionBuilder.getBillToCode(entity)+","+ToDoRuleExpressionBuilder.getBillToName(entity).toLowerCase()+  
				bookingagentname+" "+ ruleExpression;
			}else{
				ruleExpression = " Select "+todoRule.getRuleNumber() + " " + " , " + getJoinid(entity) +" " +","+ testDate + " ," +
				ToDoRuleExpressionBuilder.getShipper(entity) + "," + ToDoRuleExpressionBuilder.getAssignedToList(entity)+"," + ToDoRuleExpressionBuilder.getBilling(entity)+"," + ToDoRuleExpressionBuilder.getPayable(entity)+"," + ToDoRuleExpressionBuilder.getPricing(entity)+"," + ToDoRuleExpressionBuilder.getConsultant(entity)+"," + ToDoRuleExpressionBuilder.getSalesMan(entity)+ "," + ToDoRuleExpressionBuilder.getWarehousemanager(entity)+ "," + ToDoRuleExpressionBuilder.getAuditor(entity)+ " ," + ToDoRuleExpressionBuilder.getFileNumber(entity)+","+ToDoRuleExpressionBuilder.getBillToCode(entity)+","+ToDoRuleExpressionBuilder.getBillToName(entity)+
				bookingagentname+" "+ ruleExpression;
			}
		}
		System.out.println("\n\n\n ruleID-->>"+todoRule.getRuleNumber());
		System.out.println("\n\n\n Expression-->>"+ruleExpression);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return ruleExpression;			
	}
	
	private java.util.Date getCurrentDate() {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = dfm.format(new Date());
		Date currentDate = null;
		try {
			currentDate = dfm.parse(dateStr);			
		} catch (java.text.ParseException e) {			
			e.printStackTrace();
			 logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return currentDate;		
	}
	
	public static String getJoinid(String entity) {
		String joinid = entity;		
		if (entity.equals("ServiceOrder")){
			joinid = " serviceorder.id, serviceorder.id";
		}else if (entity.equals("CustomerFile")){
			joinid = " customerfile.id, customerfile.id";
		}else if (entity.equals("QuotationFile")){
			joinid = " customerfile.id, customerfile.id";
		}else if (entity.equals("ServicePartner")){
			joinid = " servicepartner.id, serviceorder.id as id1";
		}else if (entity.equals("Vehicle")){
			joinid = " vehicle.id, serviceorder.id as id1";
		}else if (entity.equals("AccountLine")){
			joinid = " accountline.id, serviceorder.id as id1";
		}else if (entity.equals("WorkTicket")){
			joinid = " workticket.id, workticket.id as id1";
		}else if (entity.equals("AccountLineList")){
			joinid = " serviceorder.id, serviceorder.id";
		}else if (entity.equals("WorkTicketList")){
			joinid = " serviceorder.id, serviceorder.id";
		}else if (entity.equals("Billing")){
			joinid = " billing.id, billing.id";
		}else if (entity.equals("Miscellaneous")){
			joinid = " miscellaneous.id, miscellaneous.id";
		}else if (entity.equals("DspDetails")){
			joinid = " dspdetails.id, serviceorder.id";
		}else if (entity.equals("Claim")){
			joinid = " claim.id, claim.id";
		}
		else if (entity.equals("CreditCard")){
			joinid = " billing.id, billing.id";
		}
		return joinid;
	}

	public List<ToDoRule> getAllRules(String sessionCorpID) {
		return toDoRuleDao.getAllRules(sessionCorpID);
	}

	public ToDoResultDao getToDoResultDao() {
		return toDoResultDao;
	}

	public void setToDoResultDao(ToDoResultDao toDoResultDao) {
		this.toDoResultDao = toDoResultDao;
	}

	public ToDoRuleDao getToDoRuleDao() {
		return toDoRuleDao;
	}

	public void setToDoRuleDao(ToDoRuleDao toDoRuleDao) {
		this.toDoRuleDao = toDoRuleDao;
	}

	public List<Notes> findByStatus(String reminderStatus,String corpId,String createdBy) {
		return toDoRuleDao.findByStatus(reminderStatus,corpId,createdBy);
	}

	public List<Notes> findByStatus1(String reminderStatus,String corpId,String createdBy) {
		return toDoRuleDao.findByStatus1(reminderStatus,corpId,createdBy);
	}

	public List<Notes> findByStatus2(String reminderStatus,String corpId,String createdBy) {
		return toDoRuleDao.findByStatus2(reminderStatus,corpId,createdBy);
	}

	// By Corp ID 
	public List<Notes> findByStatus(String reminderStatus,String corpId) {
		return toDoRuleDao.findByStatus(reminderStatus,corpId);
	}

	//By Corp ID 
	public List<Notes> findByStatus1(String reminderStatus,String corpId) {
		return toDoRuleDao.findByStatus1(reminderStatus,corpId);
	}

	//By Corp ID 
	public List<Notes> findByStatus2(String reminderStatus,String corpId) {
		return toDoRuleDao.findByStatus2(reminderStatus,corpId);
	}
	
	//By Corp ID
	public List<ToDoResult> findByduration(String corpID) {
		return toDoRuleDao.findByduration(corpID);
	}

	//By Corp ID
	public List<ToDoResult> findBydurationdtoday(String corpID) {
		return toDoRuleDao.findBydurationdtoday(corpID);
	}
	
	//By Corp ID
	public List<ToDoResult> findBydurationdueweek(String corpID) {
		return toDoRuleDao.findBydurationdueweek(corpID);
	}

	public void deleteAllResults(String sessionCorpID) {
		 toDoRuleDao.deleteAllResults(sessionCorpID);
	}

	public void changestatus(String status, Long id) {
		toDoRuleDao.changestatus(status,id);
		
	}

	public List<DataCatalog> selectfield(String entity,String corpID) {
		return toDoRuleDao.selectfield(entity,corpID);
	}

	public List<DataCatalog> selectDatefield(String corpID) {
		return toDoRuleDao.selectDatefield(corpID);
	}

	public List<ToDoResult> findNumberOfResult(String corpID) {
		return toDoRuleDao.findNumberOfResult(corpID);
	}

	public List findUserForSupervisor(String supervisor, String corpID) {
		return toDoRuleDao.findUserForSupervisor(supervisor, corpID);
	}

	//	By Corp ID  & Supervisor
	public List<Notes> findByStatusForSupervisor(String reminderStatus, String corpId, String users) {
		return toDoRuleDao.findByStatusForSupervisor(reminderStatus, corpId, users);
	}

	//	By Corp ID  & Supervisor
	public List<Notes> findByStatusForSupervisor1(String reminderStatus, String corpId, String users) {
		return toDoRuleDao.findByStatusForSupervisor1(reminderStatus, corpId, users);
	}
	
	//	By Corp ID  & Supervisor
	public List<Notes> findByStatusForSupervisor2(String reminderStatus, String corpId, String users) {
		return toDoRuleDao.findByStatusForSupervisor2(reminderStatus, corpId, users);
	}

	
	//By Corp ID & User
	public List<ToDoResult> findBydurationUser(String corpID, String owner,	String messageDisplayed, String ruleId, String shipper, User appUser) {
		return toDoRuleDao.findBydurationUser(corpID, owner, messageDisplayed, ruleId, shipper,appUser);
	}
	//	By Corp ID & User
	public List<ToDoResult> findBydurationdtodayUser(String corpID,	String owner, String messageDisplayed, String ruleId, String shipper, User appUser) {
		return toDoRuleDao.findBydurationdtodayUser(corpID, owner, messageDisplayed, ruleId, shipper,appUser);
	}  
	//	By Corp ID & User
	public List<ToDoResult> findBydurationdueweekUser(String corpID, String owner) {
		return toDoRuleDao.findBydurationdueweekUser(corpID, owner);
	}

	public List<ToDoResult> findBydurationForSupervisor(String corpID, String owner) {
		return toDoRuleDao.findBydurationForSupervisor(corpID,owner);
	}

	public List<ToDoResult> findBydurationdtodayForSupervisor(String corpID, String owner) {
		return toDoRuleDao.findBydurationdtodayForSupervisor(corpID,owner);
	}

	public List<ToDoResult> findBydurationdueweekForSupervisor(String corpID, String owner) {
		return toDoRuleDao.findBydurationdueweekForSupervisor(corpID,owner);
	}

	public List checkSupervisor(String username) {
		return toDoRuleDao.checkSupervisor(username);
	}

	public void updateCompanyDivisionLastRunDate(String corpID) {
		toDoRuleDao.updateCompanyDivisionLastRunDate(corpID);
		
	}

	public List getTimeToExecute(String corpID) {
		return toDoRuleDao.getTimeToExecute(corpID);
	}

	public List getOwnersList(String sessionCorpID ,String owner,Long userId,String userFirstName,String userType) {
		return toDoRuleDao.getOwnersList(sessionCorpID , owner,userId,userFirstName, userType);
	}

	public List getOwnerResult(String owner, String sessionCorpID) {
		return toDoRuleDao.getOwnerResult(owner, sessionCorpID);
	}

	public List findResultByOwnerOverDue(String sessionCorpID, String owner) {
		return toDoRuleDao.findResultByOwnerOverDue(sessionCorpID, owner);
	}

	public void deleteThisResults(Long id, String sessionCorpID,String role) {
		toDoRuleDao.deleteThisResults(id, sessionCorpID,role);
	}

	public List findNumberOfResultEntered(String sessionCorpID, Long id, String role) {
		return toDoRuleDao.findNumberOfResultEntered(sessionCorpID, id,role);
	}

	public void updateUserCheck(String recordId, String userName, String fieldToValidate) {
		toDoRuleDao.updateUserCheck(recordId, userName, fieldToValidate);
	}
	public void updateUserCheckForCheck(String recordId, String userName){
		toDoRuleDao.updateUserCheckForCheck(recordId, userName);
	}
	public List getByCorpID(String sessionCorpID, Boolean isAgentTdr) {
		return toDoRuleDao.getByCorpID(sessionCorpID, isAgentTdr);
	}
	public List  getDocCheckListByCorpID(String sessionCorpID){
		return toDoRuleDao.getDocCheckListByCorpID(sessionCorpID);
	}
	
	public List getDescription(String tableName, String fieldname, String sessionCorpID) {
		return toDoRuleDao.getDescription(tableName, fieldname, sessionCorpID);
	}

	public List getTimeToExecute2(String corpID) {
		return toDoRuleDao.getTimeToExecute2(corpID);
	}

	public List getMessageList(String sessionCorpID, String userName,Long userId,String userType) {
		return toDoRuleDao.getMessageList(sessionCorpID, userName,userId, userType);
	}

	public List findNetwokLinkedFiles(String sessionCorpID){
		return toDoRuleDao.findNetwokLinkedFiles(sessionCorpID);
	}
	public List findMessageOverDue(String sessionCorpID, String messageDisplayed , String userName) {
		return toDoRuleDao.findMessageOverDue(sessionCorpID, messageDisplayed,  userName);
	}

	public List findMessageToday(String sessionCorpID, String messageDisplayed, String userName) {
		return toDoRuleDao.findMessageToday(sessionCorpID, messageDisplayed,  userName);
	}

	public List getRuleIdList(String sessionCorpID , String username,Long userId,String userType) {
		return toDoRuleDao.getRuleIdList(sessionCorpID,username,userId,userType);
	}

	public List findIDOverDue(String sessionCorpID, String ruleId, String username) {
		return toDoRuleDao.findIDOverDue(sessionCorpID, ruleId, username);
	}

	public List findIdToday(String sessionCorpID, String ruleId, String username) {
		return toDoRuleDao.findIdToday(sessionCorpID, ruleId, username);
	}

	public ToDoRule getToDoRule() {
		return toDoRule;
	}

	public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
		this.toDoRuleManager = toDoRuleManager;
	}

	public List getToDoRulesPersonSummary(String sessionCorpID,String username,Long userId,String userType) {
		return toDoRuleDao.getToDoRulesPersonSummary(sessionCorpID, username,userId,userType);
	}

	public List getToDoRulesSummary(String sessionCorpID,String username,Long userId,String userType) {
		return toDoRuleDao.getToDoRulesSummary(sessionCorpID, username,userId,userType);
	}

	public List findMaximum(String sessionCorpID) {
		return toDoRuleDao.findMaximum(sessionCorpID);
	}

	public List getToDoRulesShipperSummary(String sessionCorpID ,String username,Long userId,String userType) {
		return toDoRuleDao.getToDoRulesShipperSummary(sessionCorpID,username,userId,userType);
	}  
	public List findByDurationComingUpUser(String sessionCorpID, String ownerName, String messageDisplayed, String ruleId, String shipperName, User appUser){
		return toDoRuleDao.findByDurationComingUpUser(sessionCorpID, ownerName, messageDisplayed, ruleId, shipperName,appUser);
	}
	public List findByDurationComingUp(String sessionCorpID){
		return toDoRuleDao.findByDurationComingUp(sessionCorpID);
	}
	public void setViewFieldOfHistory(Long historyId){
		this.toDoRuleDao.setViewFieldOfHistory(historyId);
	}
	public void bulkUpdateViewFieldOfHistory(String idList){
		this.toDoRuleDao.bulkUpdateViewFieldOfHistory(idList);
	}

	public String getMailStatus() {
		return mailStatus;
	}

	public void setMailStatus(String mailStatus) {
		this.mailStatus = mailStatus;
	}

	public String getMailFailure() {
		return mailFailure;
	}

	public void setMailFailure(String mailFailure) {
		this.mailFailure = mailFailure;
	}

	public List getFilterName(String sessionCorpID ,Long userId){
		return toDoRuleDao.getFilterName(sessionCorpID,userId);
		
	}
	public void updateNetworkControlSO(String sessionCorpID, String linkShipNumber , String sourceShipNumber, String action,String childAgentType){
		toDoRuleDao.updateNetworkControlSO(sessionCorpID, linkShipNumber , sourceShipNumber , action, childAgentType);
	}
	public List linkSequenceNumberList(String sessionCorpID,String bookingAgentcode, String agentType, String childAgentType,String childAgentCode, ServiceOrder soObj,String listTarget){
		return toDoRuleDao.linkSequenceNumberList(sessionCorpID, bookingAgentcode,agentType,childAgentType , childAgentCode ,soObj, listTarget);
	}
	public List findLinkSO(String sessionCorpID, String seqNumber,String bookingAgentcode, String agentType, ServiceOrder soObj,String hitFlag){
		return toDoRuleDao.findLinkSO(sessionCorpID, seqNumber, bookingAgentcode,agentType, soObj, hitFlag); 
	}
	public List getIntegrationLogInfoLog(){
		return toDoRuleDao.getIntegrationLogInfoLog();
	}
	public List <ServiceOrder> getServiceOrderByCFId(Long cid){
		return toDoRuleDao.getServiceOrderByCFId(cid);
	}
	public String getLinkedShipnumber(String sequenceNumber, String sessionCorpID){
		return toDoRuleDao.getLinkedShipnumber(sequenceNumber,sessionCorpID);
	}
	public String getNetworkAgentType(String networkPartnerCode,String sessionCorpID){
		return toDoRuleDao.getNetworkAgentType(networkPartnerCode, sessionCorpID); 
	}
	public List getComingUpList(String ShipNumber,String sessionCorpID,String relatedTask){
		return toDoRuleDao.getComingUpList(ShipNumber,sessionCorpID,relatedTask);
		
	}
	public List getDueToday(String ShipNumber,String sessionCorpID,String relatedTask){
		return toDoRuleDao.getDueToday(ShipNumber,sessionCorpID,relatedTask);
		
	}
	public List getOverDue(String ShipNumber,String sessionCorpID,String relatedTask){
		return toDoRuleDao.getOverDue(ShipNumber,sessionCorpID,relatedTask);
		
	}
	public List getResultToday(String ShipNumber,Long id,String sessionCorpID,String relatedTask){
		return toDoRuleDao.getResultToday(ShipNumber,id,sessionCorpID,relatedTask);
		
	}
	public List getToDoOverDue(String ShipNumber,Long id,String sessionCorpID,String relatedTask){
		return toDoRuleDao.getToDoOverDue(ShipNumber,id,sessionCorpID,relatedTask);
		
	}
	public List getResultListDueToday(String ShipNumber,Long id,String sessionCorpID,String owner, String duration,String relatedTask){
		return toDoRuleDao.getResultListDueToday(ShipNumber, id, sessionCorpID, owner,  duration,relatedTask);
		
	}
	public List getLinkSOs(String sequenceNumber, String sessionCorpID){
		return toDoRuleDao.getLinkSOs(sequenceNumber, sessionCorpID);
	}
	public List getBillToCode(String companydivisionCode,String sessionCorpID){
		return toDoRuleDao.getBillToCode(companydivisionCode,sessionCorpID);
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
	public Map<String, List<String>> getNetworkDataFields(){
		return toDoRuleDao.getNetworkDataFields();
	}
	public String getUserLastFirstName(String coordinator){
		return toDoRuleDao.getUserLastFirstName( coordinator);
	}

	public  List<Long> getModelIDsByShip(String... ships) {
		return toDoRuleDao.getModelIDsByShip( ships); 
	}
	public int removeAll(String shipNumber, String IDs, String Model){
		return toDoRuleDao.removeAll(shipNumber, IDs, Model);
	}
	public int updateCompanyRulesRunning(String corpId,Boolean useDistAmt){
		return toDoRuleDao.updateCompanyRulesRunning(corpId, useDistAmt);
	}
	public int getCount(String model,String shipNumber){
		return toDoRuleDao.getCount(model, shipNumber);
	}
	public <ToDoResult> List getAllToDoResult(){
		return toDoRuleDao.getAllToDoResult();
	}
	public <ToDoResult> List getToDoResultByRuleNumber(String ruleNumber)
	{
		return toDoRuleDao.getToDoResultByRuleNumber(ruleNumber);
	}
	public void delAndUpdateResult(String corpId){
		toDoRuleDao.delAndUpdateResult(corpId);
	}
	public void updateToDoResult(String toDoResultId,String toDoResultUrl,String toDoRecordId,String roleForTransfer,String roleType,String shipnumber){
		toDoRuleDao.updateToDoResult(toDoResultId, toDoResultUrl, toDoRecordId, roleForTransfer,roleType,shipnumber);
	}
	public int findCheckListEnterd(String corpID, Long id){
		return toDoRuleDao.findCheckListEnterd(corpID, id);
	}
	public String getEmailOut() {
		return (String)AppInitServlet.redskyConfigMap.get("email.out");
	}

	

	public String getEmailDomain() {
		return (String)AppInitServlet.redskyConfigMap.get("email.domainfilter");
	}

	
	public String getServerUrl() {
		return (String)AppInitServlet.redskyConfigMap.get("server.url");
	}
	public void deleteUpdateByRuleNumber(Long ruleNumber,String corpId){
		toDoRuleDao.deleteUpdateByRuleNumber(ruleNumber, corpId);
	}

	public <CheckListResult> List getCheckListResultByRuleNumber(
			String ruleNumber) {
		return toDoRuleDao.getCheckListResultByRuleNumber(ruleNumber);
	}

	public void setCheckListDao(CheckListDao checkListDao) {
		this.checkListDao = checkListDao;
	}

	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public void setVehicleManager(VehicleManager vehicleManager) {
		this.vehicleManager = vehicleManager;
	}
	
	public List findByAgentTdrEmail(String corpID,String owner) {
		return toDoRuleDao.findByAgentTdrEmail(corpID,owner);
	}
	public List findByAgentTdrEmail(String ShipNumber,String corpID,String owner) {
		return toDoRuleDao.findByAgentTdrEmail(ShipNumber,corpID,owner);
	}

	public void setTaskCheckListManager(TaskCheckListManager taskCheckListManager) {
		this.taskCheckListManager = taskCheckListManager;
	}
	public <TaskCheckList> List getTaskCheckListByRuleNumber(String ruleNumber){
		return toDoRuleDao.getTaskCheckListByRuleNumber(ruleNumber);
	}
	public List <ToDoRule> getRuleByTypes (String service,String mode,String routing,String jobType,String billToCode, String bookingAgent,String corpId){
		return toDoRuleDao.getRuleByTypes(service, mode, routing, jobType, billToCode, bookingAgent,corpId);
	}

	public <RuleActions> List getToDoActionsByRuleId(Long ruleId, String corpId){
		return toDoRuleDao.getToDoActionsByRuleId(ruleId, corpId);
	}

	public TrackingStatusManager getTrackingStatusManager() {
		return trackingStatusManager;
	}

	 public RuleActionsDao getRuleActionsDao() {
			return ruleActionsDao;
		}

		public void setRuleActionsDao(RuleActionsDao ruleActionsDao) {
			this.ruleActionsDao = ruleActionsDao;
		}
		public List getOwnersListExtCordinator(String sessionCorpID ,String owner,Long userId,String userFirstName,String userType,String bookingAgentPopup,String billToCodePopup) {
			return toDoRuleDao.getOwnersListExtCordinator(sessionCorpID , owner,userId,userFirstName, userType,bookingAgentPopup,billToCodePopup);
		}
		
		 public List findByAgentTdrEmailExtCordinator(String corpID,String owner,String bookingAgentPopup,String billToCodePopup,String userType) {
				return toDoRuleDao.findByAgentTdrEmailExtCordinator(corpID , owner,bookingAgentPopup,billToCodePopup,userType);

		 }
		
		public List getMessageListExtCordinator(String sessionCorpID, String userName,Long userId,String userType,String bookingAgentPopup,String billToCodePopup) {
			return toDoRuleDao.getMessageListExtCordinator(sessionCorpID, userName,userId, userType,bookingAgentPopup,billToCodePopup);
		}
		public List getRuleIdListExtCordinator(String sessionCorpID, String userName,Long userId,String userType,String bookingAgentPopup,String billToCodePopup) {
			return toDoRuleDao.getRuleIdListExtCordinator(sessionCorpID, userName,userId, userType,bookingAgentPopup,billToCodePopup);
		}
		
		public List<ToDoResult> findBydurationUserExtCordinator(String corpID, String owner,	String messageDisplayed, String ruleId, String shipper, User appUser,String bookingAgentPopup,String billToCodePopup) {
			return toDoRuleDao.findBydurationUserExtCordinator(corpID, owner, messageDisplayed, ruleId, shipper,appUser,bookingAgentPopup,billToCodePopup);
		}
		public List<ToDoResult> findBydurationdtodayUserExtCordinator(String corpID,	String owner, String messageDisplayed, String ruleId, String shipper, User appUser,String bookingAgentPopup,String billToCodePopup) {
			return toDoRuleDao.findBydurationdtodayUserExtCordinator(corpID, owner, messageDisplayed, ruleId, shipper,appUser,bookingAgentPopup,billToCodePopup);
		} 
		public List<ToDoResult> findBydurationForSupervisorExtCordinator(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String userType) {
			return toDoRuleDao.findBydurationForSupervisorExtCordinator(corpID,owner,bookingAgentPopup,billToCodePopup,userType);
		}
		 public List<ToDoResult> findBydurationdtodayForSupervisorExtCordinator(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String userType) {
			 return toDoRuleDao.findBydurationdtodayForSupervisorExtCordinator(corpID,owner,bookingAgentPopup,billToCodePopup,userType);
			 
		 }
		 public List<ToDoResult> findBydurationdueweekForSupervisorExtCordinator(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String userType) {
			 return toDoRuleDao.findBydurationdtodayForSupervisorExtCordinator(corpID,owner,bookingAgentPopup,billToCodePopup,userType);
		 }
		 public List<Notes> findByStatusExtCordinator(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup) {
			 return toDoRuleDao.findByStatusExtCordinator(reminderStatus,corpId,createdBy,bookingAgentPopup,billToCodePopup);
		 }
		 public List<Notes> findByStatus1ExtCordinator(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup) {	
			 return toDoRuleDao.findByStatus1ExtCordinator(reminderStatus,corpId,createdBy,bookingAgentPopup,billToCodePopup);
		 }
		 public List<Notes> findByStatus2ExtCordinator(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup) {
			 return toDoRuleDao.findByStatus2ExtCordinator(reminderStatus,corpId,createdBy,bookingAgentPopup,billToCodePopup);
		 }
		 public List<Notes> findByStatusForSupervisor2ExtCordinator(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup)
		 {return toDoRuleDao.findByStatusForSupervisor2ExtCordinator(reminderStatus,corpId,users,bookingAgentPopup,billToCodePopup);
			 
		 }
		 public List<Notes> findByStatusForSupervisor1ExtCordinator(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup)
		 {return toDoRuleDao.findByStatusForSupervisor1ExtCordinator(reminderStatus,corpId,users,bookingAgentPopup,billToCodePopup);
			 
		 }
		 public List<Notes> findByStatusForSupervisorExtCordinator(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup)
		 {
			 return toDoRuleDao.findByStatusForSupervisorExtCordinator(reminderStatus,corpId,users,bookingAgentPopup,billToCodePopup);
			 
		 }
		 public List<Notes> findByStatusForSupervisorExtCordinatorForTeam(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup,String username,String allradio) {
		 
			 return toDoRuleDao.findByStatusForSupervisorExtCordinatorForTeam(reminderStatus,corpId,users,bookingAgentPopup,billToCodePopup,username,allradio);
		 }
		 public List<Notes> findByStatusForSupervisor1ExtCordinatorForTeam(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup,String username,String allradio)
		 {
			 return toDoRuleDao.findByStatusForSupervisor1ExtCordinatorForTeam(reminderStatus,corpId,users,bookingAgentPopup,billToCodePopup,username,allradio);

		 }
		 public List<Notes> findByStatusForSupervisor2ExtCordinatorForTeam(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup,String username,String allradio) {
			 return toDoRuleDao.findByStatusForSupervisor2ExtCordinatorForTeam(reminderStatus,corpId,users,bookingAgentPopup,billToCodePopup,username,allradio);

		 }
		 public List<ToDoResult> findBydurationForSupervisorExtCordinatorForTeam(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType)
		 {
			 return toDoRuleDao.findBydurationForSupervisorExtCordinatorForTeam(corpID,owner,bookingAgentPopup,billToCodePopup,username,allradio,userType);
			 
		 }
		 public List<ToDoResult> findBydurationdtodayForSupervisorExtCordinatorForTeam(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType) {
				return toDoRuleDao.findBydurationdtodayForSupervisorExtCordinatorForTeam(corpID,owner,bookingAgentPopup,billToCodePopup,username,allradio,userType);
			}
		 public List<ToDoResult> findBydurationdueweekForSupervisorExtCordinatorForTeam(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType) {
			 return toDoRuleDao.findBydurationdueweekForSupervisorExtCordinatorForTeam(corpID,owner,bookingAgentPopup,billToCodePopup,username,allradio,userType);
		 }
		 
		public List findByAgentTdrEmailExtCordinatorForTeam(String corpID,String owner,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType){
				return toDoRuleDao.findByAgentTdrEmailExtCordinatorForTeam(corpID,owner,bookingAgentPopup,billToCodePopup,username,allradio,userType);
				
				
		}
		public List getOwnersListExtCordinatorForTeam(String sessionCorpID ,String username,Long userId,String userFirstName,String userType,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) {
			 
					return toDoRuleDao.getOwnersListExtCordinatorForTeam(sessionCorpID,username,userId,userFirstName,userType,bookingAgentPopup,billToCodePopup,loggedInusername,allradio);

		}
		public List getMessageListExtCordinatorForTeam(String sessionCorpID, String userName,Long userId,String userType,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) 
			 {
					return toDoRuleDao.getMessageListExtCordinatorForTeam(sessionCorpID,userName,userId,userType,bookingAgentPopup,billToCodePopup,loggedInusername,allradio);

				 
	    }
	   public List getRuleIdListExtCordinatorForTeam(String sessionCorpID, String userName,Long userId,String userType,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio)
			 {
				 return toDoRuleDao.getRuleIdListExtCordinatorForTeam(sessionCorpID,userName,userId,userType,bookingAgentPopup,billToCodePopup,loggedInusername,allradio);
				 
			 }
			  
	   public List getSupervisorForTeam(String username, String corpID) {
					return toDoRuleDao.getSupervisorForTeam(username, corpID);
				}
		
		public List<Notes> findByStatus1ExtCordinatorForTeam(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) 
				  {
					  return toDoRuleDao.findByStatus1ExtCordinatorForTeam(reminderStatus, corpId,createdBy,bookingAgentPopup,billToCodePopup,loggedInusername,allradio);
				  }
	    public List<Notes> findByStatus2ExtCordinatorForTeam(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) {
			  
				  return toDoRuleDao.findByStatus2ExtCordinatorForTeam(reminderStatus, corpId,createdBy,bookingAgentPopup,billToCodePopup,loggedInusername,allradio);
			  }
		public List<ToDoResult> findBydurationUserExtCordinatorForTeam(String corpID, String owner, String messageDisplayed, String ruleId, String shipper,User appUser,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) 

				  {
				  return toDoRuleDao.findBydurationUserExtCordinatorForTeam(corpID, owner,messageDisplayed,ruleId,shipper,appUser,bookingAgentPopup,billToCodePopup,loggedInusername,allradio);
				  
				  }
		public List<ToDoResult> findBydurationdtodayUserExtCordinatorForTeam(String corpID, String owner, String messageDisplayed, String ruleId, String shipper,User appUser,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio)
			  {
				  return toDoRuleDao.findBydurationdtodayUserExtCordinatorForTeam(corpID, owner,messageDisplayed,ruleId,shipper,appUser,bookingAgentPopup,billToCodePopup,loggedInusername,allradio);

				  
			  }
		  public List<Notes> findByStatusExtCordinatorForTeam(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup,String allradio) {
			  
			  return toDoRuleDao.findByStatusExtCordinatorForTeam(reminderStatus, corpId,createdBy,bookingAgentPopup,billToCodePopup,allradio);

		  }
		  public List getToDoRulesSummaryForGroupByRules(String sessionCorpID,String userLogged,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String userType)
		  {

		  return toDoRuleDao.getToDoRulesSummaryForGroupByRules(sessionCorpID,userLogged,bookingAgentPopup,billToCodePopup, loggedInusername,userType);
          }
		  public List getToDoRulesPersonSummaryForGroupByPerson(String sessionCorpID,String userLogged,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String userType) 
		  {
			  
		 return toDoRuleDao.getToDoRulesPersonSummaryForGroupByPerson(sessionCorpID,userLogged,bookingAgentPopup,billToCodePopup, loggedInusername,userType);
			  
		  }
			public List getToDoRulesShipperSummaryForGroupByShipper(String sessionCorpID ,String username,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String userType)

			{
		return toDoRuleDao.getToDoRulesShipperSummaryForGroupByShipper(sessionCorpID,username,bookingAgentPopup,billToCodePopup,loggedInusername,userType);
				
			}
		 }
