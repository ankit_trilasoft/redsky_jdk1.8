package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.UniversalDao;
import org.displaytag.properties.SortOrderEnum;

import com.trilasoft.app.webapp.helper.SearchCriterion;

public interface PagingLookupDao extends UniversalDao {
     List getAllRecordsPage(Class clazz, int firstResult, int maxResults,
            SortOrderEnum sortDirection, String sortCriterion, List<SearchCriterion> searchCriteria);
     int getAllRecordsCount(Class clazz);
     int getAllRecordsCount(Class clazz, List<SearchCriterion> searchCriteria) ;
}
