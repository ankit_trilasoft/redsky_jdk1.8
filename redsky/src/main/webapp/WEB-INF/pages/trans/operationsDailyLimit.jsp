<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<meta name="heading" content="Operations Daily Limit Control"/> 
<title>Operations Daily Limit Control</title> 
</head>

<style type="text/css">h2 {background-color: #FBBFFF}</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>	
	
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<!-- Modification closed here -->	
	


<script language="JavaScript">	
function onlyNumberAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==36); 
}
function validate(){
	<c:if test="${hubWarehouseLimit=='Crw'}">
	if(document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyCrewCapacityLimit'].value==''){
		alert('Please enter the Daily Crew limit.');
		return false;		
	}
	</c:if>
	if(document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyMaxEstVol'].value=='' || document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyMaxEstWt'].value=='0' || document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyMaxEstWt'].value=='0.00'){
		alert('Please enter the weight greater than 0.');
		return false;
	}else if(document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyMaxEstVol'].value=='' || document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyMaxEstVol'].value=='0' || document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyMaxEstVol'].value=='0.00'){
		alert('Please enter the volume greater than 0.');
		return false;
	}else if(document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyLimits'].value==''){
		alert('Please enter the Daily Operational limit.');
		return false;
	}else	if(document.forms['dailyLimitControl'].elements['operationsDailyLimits.workDate'].value==''){
		alert('Please select the work date.');
		return false;
	}else	if(document.forms['dailyLimitControl'].elements['operationsDailyLimits.comment'].value.length >200){
		alert('Comments allowed only Less than 200 characters.');
		return false;
	
	}else{
		return saveResourceGridList();	
	}
	
} 
var count=0;
var shiftCount=0;
function onlyDoubleValue(evt,targetElement){
  var keyCode = evt.keyCode || evt.which;
  targetVal=targetElement.value;
  if(keyCode==16){
  shiftCount++;
  return true;
  }
  if(targetVal.indexOf('.')>-1){
   
  }
  else{
  count=0;
  }
  if(((keyCode==110)||((keyCode==190)))  && (count==0)){
   count++;
   return true;
  }
  else if((keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ||  (keyCode==35) || (keyCode==36)){
    return true;
  }
  else if(((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105)) && shiftCount ==0){
  return true;
  }
  else{
   return false;
  }
}

function shiftValueUpdate(evt){
var keyCode = evt.which ? evt.which : evt.keyCode;
if(keyCode==16){
shiftCount=0;
}
}

function onlyNumberValue(evt){
	  var keyCode = evt.keyCode || evt.which;
	  if(keyCode==16){
	  shiftCount++;
	  return true;
	  }	  
	  if((keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ||  (keyCode==35) || (keyCode==36)){
	    return true;
	  }
	  else if(((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105)) && shiftCount ==0){
	  return true;
	  }
	  else{
	   return false;
	  }
	}
function dotNotAllowed(target,fieldName){
	var dotValue=target.value;
	if(dotValue=='.' && dotValue.length==1 ){
		alert("Please enter valid Weight or Volume.");
		document.forms['dailyLimitControl'].elements[fieldName].value="";
		document.forms['dailyLimitControl'].elements[fieldName].value.focus();
		return false;
	    }
     }

function assignResources(){
	var category = document.forms['dailyLimitControl'].elements['operationsDailyLimits.category'].value;
	if(category==''){
		alert('Please select the category.');
		return false;
	}
	window.open("assignResources.html?category="+category+"&flagType=Daily&decorator=popup&popup=true","forms","scrollbars=1,resizable=yes, status=1,width=500, height=500,top=0, left=0,menubar=no");
}
function assignResourcesNew(result){
		  var resultvalue=result; 
		  var result2=resultvalue.replace(resultvalue.substring(0,3),'');
		  var category=document.getElementById('cat'+result2).value;
		  var resource=document.getElementById('res'+result2).value;
			if(category==''){
			alert('Please select Category First.');
			return false;
	}
	window.open("assignResourcesNew.html?category="+category+"&resource="+resource+"&flagType=Daily&rowIndex="+result2+"&decorator=popup&popup=true","forms","scrollbars=1,resizable=yes, status=1,width=500, height=500,top=0, left=0,menubar=no");
	document.getElementById('res'+result2).select();
}
function addRow(tableID) {

	    var table = document.getElementById(tableID);
	    var rowCount = table.rows.length;
		var row = table.insertRow(rowCount); 
	    var newlineid=document.getElementById('newlineid').value;              
	    if(newlineid==''){
	        newlineid=rowCount;
	      }else{
	      newlineid=newlineid+"~"+rowCount;
	      }
	    document.getElementById('newlineid').value=newlineid;
     	var cell15 = row.insertCell(0);
     	var element3 = document.createElement("select");
         element3.setAttribute("class", "list-menu" );
         element3.id="cat"+rowCount;
         element3.style.width="100px"
	    var catogeryList='${resourceCategory}';
	    catogeryList=catogeryList.replace('{','').replace('}','');
	    var catogeryarray=catogeryList.split(",");
        var optioneleBlankCategory= document.createElement("option");
        optioneleBlankCategory.value="";
        optioneleBlankCategory.text=""; 
        element3.options.add(optioneleBlankCategory);  
    	for(var i=0;i<catogeryarray.length;i++){
       	var value=catogeryarray[i].split("=");
        var optionele= document.createElement("option");
        optionele.value=value[0];
        optionele.text=value[1]; 
        element3.options.add(optionele);             
     }
     cell15.appendChild(element3);

    var cell2 = row.insertCell(1);
    var element999 = document.createElement("input");
    element999.type = "text";
    element999.style.width="100px";
    element999.setAttribute("class", "input-text" );
    element999.id='res'+rowCount;
    element999.setAttribute("onblur","checkDupliResource(\"res"+rowCount+"\")");
    cell2.appendChild(element999);
    
    var elementimg=document.createElement("img");
    elementimg.setAttribute("onclick","assignResourcesNew(\"res"+rowCount+"\")");
    elementimg.setAttribute("class","openpopup");
    elementimg.setAttribute("width","17");
    elementimg.setAttribute("height","20");
    elementimg.setAttribute("style","vertical-align:top;padding-left:2px");
    elementimg.setAttribute("src","<c:url value='/images/open-popup.gif'/>");
    cell2.appendChild(elementimg);
	
    var cell1 = row.insertCell(2);
    var element1 = document.createElement("input");
    element1.type = "text";
    element1.style.width="100px";
    element1.setAttribute("class", "input-text" );    
    element1.setAttribute("style", "text-align:right; width:100px" );
    element1.id='qty'+rowCount;
    element1.setAttribute("onkeydown","return onlyNumberValue(event)");
    element1.setAttribute("onkeyup","return shiftValueUpdate(event)");
    cell1.appendChild(element1);      
}
</script> 
<script language="JavaScript">	
function saveResourceGridList(){
		if(document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyLimits'].value==''){
			alert('Please enter the Daily Operational limit.');
			return false;
		}else  if(document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyMaxEstVol'].value=='' || document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyMaxEstWt'].value=='0' || document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyMaxEstWt'].value=='0.00'){
			alert('Please enter the weight greater than 0.');
			return false;
		}else if(document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyMaxEstVol'].value=='' || document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyMaxEstVol'].value=='0' || document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyMaxEstVol'].value=='0.00'){
			alert('Please enter the volume greater than 0.');
			return false;	
		}else	if(document.forms['dailyLimitControl'].elements['operationsDailyLimits.workDate'].value==''){
			alert('Please select the work date.');
			return false;
		}else	if(document.forms['dailyLimitControl'].elements['operationsDailyLimits.comment'].value.length >200){
			alert('Comments allowed only Less than 200 characters.');
			return false;
		}	
		<c:if test="${hubWarehouseLimit=='Crw'}">
			if(document.forms['dailyLimitControl'].elements['operationsDailyLimits.dailyCrewCapacityLimit'].value==''){
				alert('Please enter the Daily Crew limit.');
				return false;		
			}
			</c:if>
		
	var resourceListServer="";
    var categoryListServer="";
   	var resourceLimitListServer="";	     
   	var id='';
	 var flag=checkMandatoryAddLineField(); 
	 if(flag==true){
		   if(document.forms['dailyLimitControl'].resourceList!=undefined){
		        if(document.forms['dailyLimitControl'].resourceList.size!=0){
		 	      for (i=0; i<document.forms['dailyLimitControl'].resourceList.length; i++){	
		 	    	 id=document.forms['dailyLimitControl'].resourceList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(resourceListServer==''){
	 	            	resourceListServer=id+": "+document.forms['dailyLimitControl'].resourceList[i].value;
	 	             }else{
	 	            	resourceListServer= resourceListServer+"~"+id+": "+document.forms['dailyLimitControl'].resourceList[i].value;
	 	             }
		 	        }	
		 	      }else{
		 	    	 id=document.forms['dailyLimitControl'].resourceList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	    	 resourceListServer=id+": "+document.forms['dailyLimitControl'].resourceList.value;
		 	      }	        
		       }  
		    if(document.forms['dailyLimitControl'].categoryList!=undefined){
		        if(document.forms['dailyLimitControl'].categoryList.size!=0){
		 	      for (i=0; i<document.forms['dailyLimitControl'].categoryList.length; i++){	
		 	    	 id=document.forms['dailyLimitControl'].categoryList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(categoryListServer==''){
	 	            	categoryListServer=id+": "+document.forms['dailyLimitControl'].categoryList[i].value;
	 	             }else{
	 	            	categoryListServer= categoryListServer+"~"+id+": "+document.forms['dailyLimitControl'].categoryList[i].value;
	 	             }
		 	        }	
		 	      }else{	
		 	    	 id=document.forms['dailyLimitControl'].categoryList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	    	 categoryListServer=id+": "+document.forms['dailyLimitControl'].categoryList.value;
		 	      }	        
		       }
		    
		    if(document.forms['dailyLimitControl'].resourceLimitList!=undefined){
		        if(document.forms['dailyLimitControl'].resourceLimitList.length!=undefined){
		 	      for (i=0; i<document.forms['dailyLimitControl'].resourceLimitList.length; i++){	        	           
	             	   id=document.forms['dailyLimitControl'].resourceLimitList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             	   if(resourceLimitListServer==''){
	             		 resourceLimitListServer=id+": "+document.forms['dailyLimitControl'].resourceLimitList[i].value;
	                   }else{
	                	  resourceLimitListServer= resourceLimitListServer+"~"+id+": "+document.forms['dailyLimitControl'].resourceLimitList[i].value;
	                   }
		 	       }	
		 	     }else{	   
		 	          id=document.forms['dailyLimitControl'].resourceLimitList.id;
		 	          id=id.replace(id.substring(0,3),'');     
		 	         resourceLimitListServer=id+": "+document.forms['dailyLimitControl'].resourceLimitList.value;
		 	     }	        
		 	 }  			   
			 document.getElementById('resourceListServer').value=resourceListServer;
			 document.getElementById('categoryListServer').value=categoryListServer;  
			 document.getElementById('resourceLimitListServer').value=resourceLimitListServer; 		     		       

		     var resourceslist="";
		     var newlineid=document.getElementById('newlineid').value;
		     if(newlineid!=''){
		            var arrayLine=newlineid.split("~");
		            for(var i=0;i<arrayLine.length;i++){		            	              
		                if(resourceslist==''){
		                	resourceslist=document.getElementById('cat'+arrayLine[i]).value+":"+document.getElementById('res'+arrayLine[i]).value+":"+document.getElementById('qty'+arrayLine[i]).value;
		                }else{
		                	resourceslist=resourceslist+"~"+document.getElementById('cat'+arrayLine[i]).value+" :"+document.getElementById('res'+arrayLine[i]).value+" :"+document.getElementById('qty'+arrayLine[i]).value;
		                }
		              }
		         }
		     
		    document.getElementById('resourceslist').value =resourceslist;
		    var id = document.forms['dailyLimitControl'].elements['id'].value;
		    var hub='${hub}';
		    document.forms['dailyLimitControl'].action="saveDailyLimit.html?hub="+hub;
		    document.forms['dailyLimitControl'].submit();
	 
}	
}   

function checkMandatoryAddLineField(){
	  var newlineid=document.getElementById('newlineid').value; 	
	  if(newlineid!=''){
	     var arrayLine=newlineid.split("~");        
	        for(var i=0;i<arrayLine.length;i++){
	   		 var resource=document.getElementById('res'+arrayLine[i]).value;
			 var category=document.getElementById('cat'+arrayLine[i]).value;																
			 var resourceLimit=document.getElementById('qty'+arrayLine[i]).value;			
		       if(category==""){
			    	 alert("Please select catagory for line # "+arrayLine[i]);
			    	 return false;
		        }
		       if(resource==""){
			    	 alert("Please Enter Resource for line # "+arrayLine[i]);
			    	 return false;
		        }
			   if(resourceLimit==""){
			    	 alert("Please Enter Limit for line # "+arrayLine[i]);
			    	 return false;
		       }
			  
          }
		    return true;
     }else{
	        return true;
     }
}

function confirmSubmit(oId){
	var agree=confirm("Are you sure you wish to remove this Row ?");
	if (agree){
		location.href="deleteDailyLimit.html?id="+oId+"&hub=${hub}&oId=${operationsDailyLimits.id}";
	}else{
		return false;
	}
}

function checkDupliResource(ctarget){
	var existingId="";
	ctarget=ctarget.replace(ctarget.substring(0,3),'').trim();
	if(document.forms['dailyLimitControl'].resourceList!=undefined){     
        if(document.forms['dailyLimitControl'].resourceList.length!=undefined){
        for (i=0; i<document.forms['dailyLimitControl'].resourceList.length; i++){
        id=document.forms['dailyLimitControl'].resourceList[i].id;
               id=id.replace(id.substring(0,3),'').trim();
               if(existingId.trim()=="") {
                existingId=id;
               }else{
                existingId=existingId+"~"+id;
               } }}}
	  
       var newlineid=document.getElementById('newlineid').value;
	   if((newlineid=="")&&(existingId=="")){    newlineid="";    }
	   else if((newlineid!="")&&(existingId=="")){    newlineid=newlineid;    }
	   else if((newlineid=="")&&(existingId!="")){    newlineid=existingId;    }
	   else if((newlineid!="")&&(existingId!="")){    newlineid=existingId+"~"+newlineid;    }
	   
  if(newlineid!=''){
	  newlineid="~"+newlineid+"~";
	  newlineid=newlineid.replace("~"+ctarget+"~","~").trim();
	  newlineid=newlineid.substring(1,newlineid.length-1);
	     var arrayLine=newlineid.split("~");
	     var tempRes=document.getElementById('res'+ctarget).value;
	     var resource="";
	     var flag="notFound";
	     for(i=0;i<arrayLine.length;i++){
			 resource = document.getElementById('res'+arrayLine[i]).value;
			 if(resource==tempRes){
				 flag="found"; 
			 }}
	if(flag=="found"){
		alert("The limit for this resource has already been assigned. Please select another resource.");		
		document.getElementById('res'+ctarget).value="";
		document.getElementById('res'+ctarget).focus();
	}
  }
}
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>


<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<s:form id="dailyLimitControl" name="dailyLimitControl" action="" method="post" validate="true" >
<s:hidden name="operationsDailyLimits.id" />
<s:hidden name="id" value="${operationsDailyLimits.id}"/>
<s:hidden name="operationsDailyLimits.hubID" value="${hub}"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="newlineid" id="newlineid" value=""/>
<s:hidden name="resourceListServer" id="resourceListServer" />
<s:hidden name="categoryListServer" id="categoryListServer" />
<s:hidden name="resourceLimitListServer" id="resourceLimitListServer" />
<s:hidden name="resourceslist" id="resourceslist" />
<s:hidden name="dummsectionName" />
<s:hidden name="opWorkDate" value="${operationsDailyLimits.workDate}"/>
<s:hidden name="operationDailyLimitValue" value="%{operationsDailyLimits.dailyLimits}"/>
<s:hidden name="operationDailyMaxEstWtValue" value="%{operationsDailyLimits.dailyMaxEstWt}"/>
<s:hidden name="operationDailyMaxEstVolValue" value="%{operationsDailyLimits.dailyMaxEstVol}"/>
<s:hidden name="operationDailyCrewValue" value="%{operationsDailyLimits.dailyCrewCapacityLimit}"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.CrewCapacityShow">
		<s:hidden name="CrewCapacityFlag" value="Y"/>
</configByCorp:fieldVisibility>

<div id="Layer1" style="width:100%;">
<div id="newmnav">
		  <ul>
		  		<li><a href="hubList.html"><span>Hub&nbsp;/&nbsp;WH List</span></a></li>
		  		<li><a href="editHubLimitOps.html?hubID=${hub}"><span>Operational&nbsp;/&nbsp;WH Management</span></a></li>
		  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Daily Limit<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  		<li><a href="dailyControlList.html?hub=${hub}"><span>Daily Limit List</span></a></li>		  		
		  </ul>
</div><div class="spn">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
 <table cellspacing="1" cellpadding="1" border="0" style="width:100%;">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
	  	<table class="detailTabLabel" border="0">
			<tbody>  				
					<tr>				  	
				  		<td align="left">Hub&nbsp;/&nbsp;WH Selected : <strong>
				  					<c:forEach var="entry" items="${distinctHubDescription}">
									<c:if test="${hub==entry.key}">
									<c:out value="${entry.value}" />
									</c:if>
									</c:forEach>
				  		</strong></td>
				  	</tr>
				  	<tr>
					  	    <td align="right" width=""></td>
					  		<td align="left" width="">#&nbsp;of&nbsp;Tickets</td>
					  		<td align="right" width="10px"></td>
					  		<td align="left" colspan="2">Max&nbsp;Est&nbsp;Wt<font color="red" size="2">*</font></td>
							<td align="left" colspan="2">Max&nbsp;Est&nbsp;vol<font color="red" size="2">*</font></td>
						<c:if test="${hubWarehouseLimit=='Crw'}">
						 	<td align="right" colspan="2">#&nbsp;of&nbsp;Members<font color="red" size="2">*</font></td>
						</c:if>	
					</tr>
					
					<tr>
					  <c:if test="${hubWarehouseLimit=='Crw'}">
					  	<td align="right">Enter Daily Operational Limit For (${typeService})<font color="red" size="2">*</font></td>
					  	</c:if>
					  	<c:if test="${hubWarehouseLimit=='Tkt'}">
					  	<td align="right">Enter Daily Operational Limit For (${typeService})<font color="red" size="2">*</font></td>
					  	<s:hidden name="operationsDailyLimits.dailyCrewCapacityLimit" />
					  	</c:if>
						  	<td align="left" style="width:75px" colspan="2"><s:textfield name="operationsDailyLimits.dailyLimits"  maxlength="10" size="12" cssClass="input-text" cssStyle="text-align:right; width:88px;" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
						  	<td align="left" width="100px" colspan="2"><s:textfield name="operationsDailyLimits.dailyMaxEstWt"  maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right; width:120px;" onkeydown="return onlyDoubleValue(event,this);" onkeyup="shiftValueUpdate(event)" onchange="return dotNotAllowed(this,'operationsDailyLimits.dailyMaxEstWt');"/></td>
						  	<td align="left" width="100px" colspan="2"><s:textfield name="operationsDailyLimits.dailyMaxEstVol"  maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right; width:120px;" onkeydown="return onlyDoubleValue(event,this);" onkeyup="shiftValueUpdate(event)" onchange="return dotNotAllowed(this,'operationsDailyLimits.dailyMaxEstVol');"/></td>
						<c:if test="${hubWarehouseLimit=='Crw'}">
							<td align="left" style="width:75px" colspan="2"><s:textfield name="operationsDailyLimits.dailyCrewCapacityLimit"  maxlength="10" size="12" cssClass="input-text" cssStyle="text-align:right;" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
						</c:if>
					</tr>
					<tr>
					  	<td align="left" height="5px"></td>
					</tr>
				  	<tr>
				  		
					  	<td align="right">Limit&nbsp;Date<font color="red" size="2">*</font></td>
					  	<c:if test="${not empty operationsDailyLimits.workDate}">
						<s:text id="workDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="operationsDailyLimits.workDate"/></s:text>
						<td><s:textfield cssClass="input-text" id="workDate" name="operationsDailyLimits.workDate" value="%{workDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" />
						<img id="workDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty operationsDailyLimits.workDate}">
						<td><s:textfield cssClass="input-text" id="workDate" name="operationsDailyLimits.workDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" />
						<img id="workDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					
					  	<td align="right">Comments</td>
					  	<td align="left" colspan="5"><s:textarea name="operationsDailyLimits.comment" cssClass="textarea" cols="5" rows="2" cssStyle="width:250px;height:30px;"/></td>
					</tr>					
					<div class="spn">&nbsp;</div>	
			
					<div id="para1" style="clear:both;">
			
					 <table class="table" id="dataTable" style="width:100%;">
					 <thead>
					 <tr>
					<th width="50px">Category</th>
					 <th width="70px">Resource</th>
					 <th width="70px">Limit</th>
					 <th width="10px">Remove</th>
					 </tr>
					 </thead>
					 <tbody>
					 <c:if test="${not empty itemList}"> 
					 <c:forEach var="individualItem" items="${itemList}" >
					 <tr>
					<td><s:select cssClass="list-menu" list="%{resourceCategory}" name="categoryList"  value="'${individualItem.category}'" id="cat${individualItem.id}" cssStyle="width:100px;"/></td>
				    <td><s:textfield cssClass="input-text" name="resourceList" value="${individualItem.resource}" id="res${individualItem.id}" onblur="checkDupliResource('res${individualItem.id}')" cssStyle="width:100px;" /><img id="openpopup4${individualItem.id}.img"  name="openpopup4.img" class="openpopup" width="17" height="20" style="vertical-align:bottom;" onclick="assignResourcesNew('res${individualItem.id}');changeStatus();"  src="<c:url value='/images/open-popup.gif'/>" /> </td>
					<td><s:textfield cssClass="input-text" name="resourceLimitList" value="${individualItem.resourceLimit}" id="qty${individualItem.id}" maxlength="10" size="10" cssStyle="width:100px;text-align:right;" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)" /></td>				    
					<td><a><img align="middle" onclick="confirmSubmit('${individualItem.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a></td>
					 </tr>					 
					 </c:forEach>
					  </c:if> 
					 </tbody>		 		  
					 </table>
					 </div>
			       <input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:70px; height:25px;" value="Add Line" onclick="addRow('dataTable');" /> 					
					
				  	<tr>
					  	<td align="left" height="5px"></td>
					</tr>
				</tbody>
			</table>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<s:hidden name="btntype"/>
</div>

<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${operationsDailyLimits.createdOn}" pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="operationsDailyLimits.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${operationsDailyLimits.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty operationsDailyLimits.id}">
								<s:hidden name="operationsDailyLimits.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{operationsDailyLimits.createdBy}"/></td>
							</c:if>
							<c:if test="${empty operationsDailyLimits.id}">
								<s:hidden name="operationsDailyLimits.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${operationsDailyLimits.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="operationsDailyLimits.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${operationsDailyLimits.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty operationsDailyLimits.id}">
								<s:hidden name="operationsDailyLimits.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{operationsDailyLimits.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty operationsDailyLimits.id}">
								<s:hidden name="operationsDailyLimits.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
		<table class="detailTabLabel" border="0">
		<tbody> 	
			<tr>
				<td align="left" height="3px"></td>
			</tr>	  	
			<tr>
				<td align="left"><input type="button" class="cssbutton1"  style="width:55px; height:25px;margin-left:10px;" value="Save" onclick="return saveResourceGridList();"/></td>
								
				<c:set var="hitFlag" value="<%=request.getParameter("hitFlag") %>" />
				<c:if test="${hitFlag == 1}" >
					<c:redirect url="/dailyControlList.html?hub=${hub}"  />
				</c:if>
			</tr>		  	
		</tbody>
	</table>
	<%-- <c:set var="hitFlag" value="<%=request.getParameter("hitFlag") %>" />
	 <c:if test="${hitFlag == 'd'}" >
			<c:redirect url="/editDailyLimit.html?hub=${hub}&id=${operationsDailyLimits.id}"  />
	</c:if>--%>
</s:form>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
<script type="text/javascript">
	var hub = '${hub}';
	 if(hub=='0'){
		window.onload = function()
		{
			//Disable fields
			 	var inputs = document.getElementsByTagName("input");
			    for (var i = 0; i < inputs.length; i++) {			    	
			    	inputs[i].disabled = true;														    	
			    }			    
			    var selects = document.getElementsByTagName("select");
			    for (var i = 0; i < selects.length; i++) {
			    	selects[i].disabled = true;
			    }
			    var textareas = document.getElementsByTagName("textarea");
			    for (var i = 0; i < textareas.length; i++) {
			    	textareas[i].disabled = true;
			    }
			    document.getElementById('workDate_trigger').style.display="none";
			    var totalImages = document.images.length;
		      	for (var i=0;i<totalImages;i++)
					{
						if(document.images[i].src.indexOf('open-popup.gif')>0)
						{
							var el = document.getElementById(document.images[i].id);
							//alert(el)
							try{
							el.onclick = false;
							}catch(e){}
						    document.images[i].src = 'images/navarrow.gif';
						    el.onclick = false;
						}
						if(document.images[i].src.indexOf('recycle.gif')>0)
						{
							var el = document.getElementById(document.images[i].id);
							//alert(el)
							try{
							el.onclick = false;
							}catch(e){}
						    document.images[i].src = 'images/navarrow.gif';
						}
					}
			}
	}
	</script>