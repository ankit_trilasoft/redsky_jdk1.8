<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="findMilitaryBaseList.title"/></title>   
    <meta name="heading" content="<fmt:message key='findMilitaryBaseList.heading'/>"/> 
 <script language="javascript" type="text/javascript">
  function clear_fields(){
  
			    
			    document.forms['searchForm'].elements['baseCode'].value = "";
			    document.forms['searchForm'].elements['description'].value = "";
		        document.forms['searchForm'].elements['SCACCode'].value = "";
		        document.forms['searchForm'].elements['terminalState'].value = "";
}
</script>     
</head>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-10px;
margin-top:-12px;
padding:2px;
text-align:right;
width:100%;
!width:100%;
}

div.error, span.error, li.error, div.message {
width:450px;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>   
<s:form id="searchForm" name="searchForm" action="searchMilitaryBaseList" method="post" validate="true" >
<div id="layer4" style="width:100%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" align="top" method="searchMilitaryBaseList" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
</c:set>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
		<table class="table" style="width:98%"  >
		<thead>
		<tr>
		<th>Base</th>
		<th>Base Name</th>
		<th>SCAC</th>
		<th>State</th>
		<th colspan="2">&nbsp;</th>
		</tr></thead>	
				<tbody>
				<tr>
				<td width="" align="left">
					    <s:textfield name="baseCode" required="true" cssClass="input-text" size="20"/>
					</td>
					<td width="" align="left">
					    <s:textfield name="description" required="true" cssClass="input-text" size="20"/>
					</td>
					<td width="" align="left">
					    <s:textfield name="SCACCode" required="true" cssClass="input-text" size="20"/>
					</td>
					<td width="" align="left">
					    <s:textfield name="terminalState" required="true" cssClass="input-text" size="20"/>
					</td>
					<td></td>
					<td width="" align="center" style="border-left: hidden;">
					    <c:out value="${searchbuttons}" escapeXml="false" />   
					</td>
				</tr>
				</tbody>
			</table>
			</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
			<s:set name="findMilitaryBaseList" value="findMilitaryBaseList" scope="request"/>
			
<div id="Layer1" style="width:100%" >
	<div id="otabs" style="margin-top:-15px;">
		  <ul>
		    <li><a class="current"><span>Find Military Base List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<display:table name="findMilitaryBaseList" class="table" requestURI="" id="findMilitaryBaseList" style="width:100%; margin-top: 1px;!margin-top: -1px;" defaultsort="1"  pagesize="10" >
     <display:column property="basecode"  sortable="true" title="Base"  style="width:5%"/>       
     <display:column property="description" sortable="true" title="Base Name" style="width:15%"/> 
   	 <display:column property="partnercode" sortable="true" title="Code"  style="width:5%"/>
	 <display:column property="lastname" sortable="true" title="Name"  style="width:20%"/>
	 <display:column property="terminalstate" style="width:5%" sortable="true" title="State" />
	 <display:column property="terminalcountrycode" sortable="true" title="Country"  style="width:5%"/>
	 <display:column property="scaccode" sortable="true" style="width:5%" title="SCAC"/>
	 <display:column property="internationalloiflag" sortable="true" style="width:5%" title="INTL.LOI"/>
	 <display:column property="domesticloiflag" sortable="true" style="width:5%" title="DOM.LOI"/>	
</display:table>
</div>
</div>
</s:form>

<script type="text/javascript"> 
//highlightTableRows("findMilitaryBaseList");  
</script>  
		  	