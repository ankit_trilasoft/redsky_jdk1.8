<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<head>   
    <title><fmt:message key="previewStorageList.title"/></title>   
    <meta name="heading" content="<fmt:message key='previewStorageList.heading'/>"/> 
 <script language="javascript" type="text/javascript">
  function clear_fields(){
  
			    document.forms['searchForm'].elements['shipNumbers'].value = "";
			    document.forms['searchForm'].elements['firstName'].value = "";
		        // document.forms['searchForm'].elements['lastName'].value = "";
		        //document.forms['searchForm'].elements['billsTo'].value = "";
			    document.forms['searchForm'].elements['cycles'].value = "";
		        document.forms['searchForm'].elements['charges'].value = "";
}

  function quotesRemoval(fieldValue){
		if(fieldValue.value.indexOf("'")>-1){
			var val =document.getElementsByName('shipNumbers')[0].value;
			document.getElementsByName('shipNumbers')[0].value = val.substring(0,val.indexOf("'")) +  val.substring(val.indexOf("'")+1,val.length);
		}

		if(fieldValue.value.indexOf("*")>-1){
			var val =document.getElementsByName('shipNumbers')[0].value;
			document.getElementsByName('shipNumbers')[0].value = val.substring(0,val.indexOf("*")) +  val.substring(val.indexOf("*")+1,val.length);
		}
	  }
  
function quotesNotAllowed(evt){
  var keyCode = evt.which ? evt.which : evt.keyCode;
  if(keyCode==222){
  return false;
  }
  else{
  return true;
  }
}
</script>     
</head>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 150px;
text-align:right;
width:84%;
!width:84%;
} 
</style>   
<s:form id="searchForm"  action="billingStoragesList" method="post" validate="true"  >
<s:set name="previewStorageList" value="previewStorageList" scope="request"/>
<c:if test="${previewStorageList[0].forChecking =='y'}"/>

<div id="">
<ul>
<li><a class="current"><span><b>Total Number of Records are  ${fn:length(previewStorageList)} with total amount ${currencyOut}</b></span></a></li>
</ul>
</div>  
<div>
	<table class="table" style="width:40%"  >
	<thead>
		<tr>
		<th>Division</th>
		<th>Job</th>
		<th style=" text-align: right">Months To Bill</th>
		<th style=" text-align: right"># of Invoice</th>
		<th style=" text-align: right">Amount</th> 
		</tr></thead>	
			<tbody>
			<c:forEach var="entry" items="${Revenue_Map}" >
				<tr>
				<c:set var="str1" value="${entry.key}"/> 
				<c:if test="${str1 =='~' || str1 ==''}">
				  <td></td>
				  <td></td> 
				</c:if>
				<c:if test="${str1 !=''}">
				 <c:forEach var="num" varStatus="rightAlign" items="${fn:split(str1, '~')}">
				 <c:choose> 
				 <c:when test="${rightAlign.last }">
				 <td style="text-align: right">${num} </td> 
				 </c:when>
				 <c:otherwise>
                      <td>${num} </td> 
                  </c:otherwise>
                  </c:choose>   
                    </c:forEach> 
                    </c:if>
			    <td style="text-align: right">${count_Map[entry.key]} </td>
			    <td style="text-align: right">${entry.value}</td>
		</tr>
		</c:forEach>
		</tbody>
	</table>
</div>





<div id="Layer1" style="width: 100%;margin-top:-20px;!margin-top:-5px; padding:0px;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<c:set var="searchbuttons">   
			<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="previewStorageSearchList" onclick="return quotesRemoval(document.getElementsByName('shipNumbers')[0]);" key="button.search"/>   
    		<input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
		</c:set>
		<div id="content" align="center">
<div id="liquid-round">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
	<table class="table" style="width:100%"  >
	<thead>
		<tr>
		<th><fmt:message key="serviceOrder.shipNumber"/></th>
		<th><fmt:message key="serviceOrder.shipperName"/></th>
		<th><fmt:message key="billing.cycles"/></th>
		<th><fmt:message key="charges.charge"/></th>
        <th></th>
			<%-- <th><fmt:message key="billing.billsTo"/></th>
			<th><fmt:message key="serviceOrder.lastName"/></th>
			<li><a class="current"><span><b>Total Number of Records are  ${fn:length(previewStorageList)}.</span></a></li>
			--%>
		</tr></thead>	
			<tbody>
				<tr>
				<td width="20" align="left">
			    	<s:textfield name="shipNumbers" required="true" cssClass="input-text" size="12" onkeydown="return quotesNotAllowed(event);"/>
				</td>
				<td width="20" align="left">
			     <s:textfield name="firstName" required="true" cssClass="input-text" size="12"/>
			 </td>
			 <%--
			 <td width="20" align="left">
			    	<s:textfield name="lastName" required="true" cssClass="input-text" size="12"/>
				</td>
				
				<td width="20" align="left">
			     <s:textfield name="billsTo" required="true" cssClass="input-text" size="12"/>
			 </td>
			 --%>
			 <td width="20" align="left">
			     <s:textfield name="cycles" required="true" cssClass="input-text" size="12"/>
			 </td>
			 <td width="20" align="left">
			     <s:textfield name="charges" required="true" cssClass="input-text" size="12"/>
			 </td>
			<td width="130px" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>
</div>
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Storage Invoice Preview</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<display:table name="previewStorageList" class="table" requestURI="" id="previewStorageListID" style="width:100%" defaultsort="1" export="true"  pagesize="5000" >
    
	<c:choose>
	
	<c:when test="${previewStorageListID.wording=='Contract Mismatch' || previewStorageListID.worldBankAuthorizationNumber=='No Auth avail' || previewStorageListID.cycle=='0' || (fn:indexOf(previewStorageListID.wording,'Error in Charge Description')>=0) }">
	   <display:column  sortable="true" title="S/O #" style="color:red;">${previewStorageListID.shipNumber}</display:column>
	 	<display:column  sortable="true" title="Lot #" style="color:red;">${previewStorageListID.registrationNumber}</display:column> 
   	    <display:column  sortable="true" title="Shipper"  style="color:red;">${previewStorageListID.lastName}</display:column>
	    <display:column  sortable="true" title="BillTo"  style="color:red;">${previewStorageListID.billToCode}</display:column>
	    <display:column  sortable="true" title="Account Name"  style="color:red;">${previewStorageListID.billToName}</display:column>
		<display:column  sortable="true" title="Billing Amount" style="color:red;text-align: right" >
	    	 <fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="2" groupingUsed="true" value="${previewStorageListID.storagePerMonth}"/>
  	    </display:column>
  	    <display:column  sortable="true" title="Vendor Code"  style="color:red;">${previewStorageListID.vendorCode1}</display:column>
	 	<display:column  sortable="true" title="Vendor Name"  style="color:red;">${previewStorageListID.vendorName1}</display:column>
  	    <display:column  sortable="true" title="Pay Amount" style="color:red;text-align: right" >
	    	 <fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="2" groupingUsed="true" value="${previewStorageListID.vendorStoragePerMonth}"/>
  	    </display:column>
	    <display:column  sortable="true" title="Cycle"  style="color:red;">${previewStorageListID.cycle}</display:column>
	 	<display:column  sortable="true" title="Charge"  style="color:red;">${previewStorageListID.charge}</display:column>
	 	<display:column  sortable="true" title="Description"  style="color:red;">${previewStorageListID.wording}</display:column>
	 	<display:column  sortable="true" title="Auth No."  style="color:red;">${previewStorageListID.billToAuthority}</display:column>
	 	<display:column  sortable="true" title="Dlvr Shipper"  style="color:red;">${previewStorageListID.deliveryShipper}</display:column>
	 	<display:column  sortable="true" title="Dlvr AC"  style="color:red;">${previewStorageListID.deliveryA}</display:column>
	 	<configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
	 	<display:column  sortable="true" title="World Bank Auth"  style="color:red;">${previewStorageListID.worldBankAuthorizationNumber}</display:column>
	    </configByCorp:fieldVisibility>
	  </c:when>
	  <c:otherwise>
	    <display:column  sortable="true" title="S/O #" style="color:black;">${previewStorageListID.shipNumber}</display:column>
	 	<display:column  sortable="true" title="Lot #" style="color:black;">${previewStorageListID.registrationNumber}</display:column> 
   	    <display:column  sortable="true" title="Shipper"  style="color:black;">${previewStorageListID.lastName}</display:column>
	    <display:column  sortable="true" title="BillTo"  style="color:black;">${previewStorageListID.billToCode}</display:column>
	    <display:column  sortable="true" title="Account Name"  style="color:black;">${previewStorageListID.billToName}</display:column>
		<display:column  sortable="true" title="Billing Amount"  style="color:black;text-align: right">
	    	 <fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="2" groupingUsed="true" value="${previewStorageListID.storagePerMonth}" />
  	    </display:column>
  	    <display:column  sortable="true" title="Vendor Code"  style="color:black;">
		 		<c:out value="${previewStorageListID.vendorCode1}"></c:out>
	 	</display:column>
	 	<display:column  sortable="true" title="Vendor Name"  style="color:black;">
		 		<c:out value="${previewStorageListID.vendorName1}"></c:out>
		</display:column>
  	    <display:column  sortable="true" title="Pay Amount"  style="color:black;text-align: right">
	    	 <fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="2" groupingUsed="true" value="${previewStorageListID.vendorStoragePerMonth}" />
  	    </display:column>
  	    <display:column  sortable="true" title="Cycle"  style="color:black;">${previewStorageListID.cycle}</display:column>
	 	<display:column  sortable="true" title="Charge"  style="color:black;">${previewStorageListID.charge}</display:column>
	 	<display:column  sortable="true" title="Description"  style="color:black;">${previewStorageListID.wording}</display:column>
	 	<display:column  sortable="true" title="Auth No."  style="color:black;">${previewStorageListID.billToAuthority}</display:column>
	 	<display:column  sortable="true" title="Dlvr Shipper"  style="color:black;">${previewStorageListID.deliveryShipper}</display:column>
	 	<display:column  sortable="true" title="Dlvr AC"  style="color:black;">${previewStorageListID.deliveryA}</display:column>
	 	<configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
	 	<display:column  sortable="true" title="World Bank Auth"  style="color:black;">${previewStorageListID.worldBankAuthorizationNumber}</display:column>
		</configByCorp:fieldVisibility>
	</c:otherwise>
	</c:choose> 
	
    <display:setProperty name="export.excel.filename" value="previewStorageListID List.xls"/>   
    <display:setProperty name="export.csv.filename" value="previewStorageListID List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="previewStorageListID List.pdf"/> 
  
</display:table>


	<s:hidden name="radiobExcInc" value='<%=request.getParameter("radiobExcInc") %>'/>
	<s:hidden name="storageBillingList" value='<%=request.getParameter("storageBillingList") %>'/>
	<s:hidden name="radiobilling" value='<%=request.getParameter("radiobilling") %>' />
	<s:hidden name="bookingCodeType" value='<%=request.getParameter("bookingCodeType") %>'/>
	<s:hidden name="billTo2" value='<%=request.getParameter("billTo2") %>'/>
	<s:hidden name="billTo" value='<%=request.getParameter("billTo") %>'/>
</s:form>

<script type="text/javascript">
/*  <c:forEach var="entry" items="${Revenue_Map}">
	alert("${entry.key}");
	alert("${str1}");
	<c:if test="${str1 !=''}">
		<c:forEach var="num" varStatus="rightAlign"
			items="${fn:split(str1, '~')}">
			<c:choose>
				<c:when test="${rightAlign.last}">
					alert("${num}");
				</c:when>
				<c:otherwise>
					alert("${num}");
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</c:if>
	alert("${count_Map[entry.key]}");
	alert("${entry.value}");
</c:forEach>  */

</script>
				