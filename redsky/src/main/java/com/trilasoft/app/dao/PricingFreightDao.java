package com.trilasoft.app.dao;


import java.math.BigDecimal;
import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.PricingFreight;


public interface PricingFreightDao extends GenericDao<PricingFreight, Long>{
	
	 public List checkById(Long id);
	 public List getFreightList(Long pricingControlId);
	 public List getMarkUpValue(BigDecimal price, String contract, String mode); 
	 public List getPricingFreightRecord(Long id, String sessionCorpID);
  
}
