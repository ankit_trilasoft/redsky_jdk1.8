package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ErrorLogDao;
import com.trilasoft.app.model.ErrorLog;
import com.trilasoft.app.service.ErrorLogManager;



public class ErrorLogManagerImpl extends GenericManagerImpl<ErrorLog,Long> implements ErrorLogManager
{

         ErrorLogDao errorLogDao;

	public ErrorLogManagerImpl(ErrorLogDao errorLogDao ) {
		super(errorLogDao);
		this.errorLogDao=errorLogDao;
	}
	public List findDistinct(){
    	return errorLogDao.findDistinct();
    }
	public void saveLogError(String corpID, Date createdOn ,String message , String methods , String createdBy , String module ){
		ErrorLog errorLog =new ErrorLog();
	          errorLog.setCreatedOn(createdOn);
	          errorLog.setMessage(message);
	          errorLog.setCorpId(corpID);
	          errorLog.setMethods(methods);
	          errorLog.setCreatedBy(createdBy);
	          errorLog.setModule(module);
	          /*errorLogManager.save(errorLog);*/
	          errorLogDao.save(errorLog);
	      }
	 public List searchErrorLogFile(String corpid,String module,String createdBy,Date createdOn){
		 return errorLogDao.searchErrorLogFile(corpid, module, createdBy, createdOn);
	 }
	}
	



