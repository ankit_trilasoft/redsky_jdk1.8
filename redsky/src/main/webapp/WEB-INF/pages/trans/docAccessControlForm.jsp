<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="documentAccessControlDetail.title" /></title>
<meta name="heading" content="<fmt:message key='documentAccessControlDetail.heading'/>" />
<style>
input[type="checkbox"] {vertical-align:inherit;}
</style>
<script>
function validateFields(){
	if(document.forms['docAccessForm'].elements['documentAccessControl.fileType'].value==''){
		alert("Select a document type");
		return false;
	}
}
function checkAllGroupBox1(obj)
{
var ss;
var kk;
kk= obj;
ss=document.docAccessForm.checkbox1.checked;
if (ss == false){	kk ="no";}
var frm = document.docAccessForm;
var el = frm.elements;
for(i=0;i<el.length;i++) {
	if((el[i].type == "checkbox") && ((el[i].id == "isBookingAgent") || (el[i].id == "isNetworkAgent") || (el[i].id == "isOriginAgent") || (el[i].id == "isSubOriginAgent") || (el[i].id == "isDestAgent") || (el[i].id == "isSubDestAgent"))) {
    if(kk == "yes"){
    el[i].checked = true;
    }else{
    el[i].checked = false;
    }
    }
  }
}
window.onload=function(){
var ss=document.docAccessForm.isBookingAgent.checked;
var ss1=document.docAccessForm.isNetworkAgent.checked;
var ss2=document.docAccessForm.isOriginAgent.checked;
var ss3=document.docAccessForm.isSubOriginAgent.checked;
var ss4=document.docAccessForm.isDestAgent.checked;
var ss5=document.docAccessForm.isSubDestAgent.checked;
if (ss == true && ss1 == true && ss2 == true && ss3 == true && ss4 == true && ss5 == true){	
	checkbox1.checked = true;	
}

}
function copyFileDescription(){
	var fileDes="";
	var fileType = document.forms['docAccessForm'].elements['documentAccessControl.fileType'].value
	<c:forEach var="entry" items="${docsList}">
	if (fileType=="${entry.key}"){
		fileDes="${entry.value}";							
	}
	</c:forEach>
	document.forms['docAccessForm'].elements['documentAccessControl.fileDescription'].value=fileDes;
}
</script>
</head>
<s:form id="docAccessForm" name="docAccessForm" action="saveDocControl.html" method="post" validate="true">
<s:hidden name="documentAccessControl.corpID"/>
<s:hidden name="documentAccessControl.id"/>
<div id="Layer1" style="width:100%;">

	<div id="newmnav">
		<ul>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Document Access Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<li><a href="docControl.html"><span>Document Access List</span></a></li>
			<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${documentAccessControl.id}&tableName=documentaccesscontrol&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		</ul>
	</div>	
	
	<div class="spn">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="0" cellpadding="0" border="0" style="width:800px">
		<tbody >
			<tr>
				<td>
				<table class="detailTabLabel" cellspacing="2" cellpadding="2" border="0">
					<tbody>
					<tr>
					<td width="10px"></td>
					<td align="left" class="listwhitetext">Document Type</td>					
					<td align="left" class="listwhitetext">Document Description</td>
					<td colspan="5"></td>
					</tr>
						<tr>
							<td width="10px"></td>
							<td width="120px">
								<c:if test="${empty  documentAccessControl.id}">
									<s:select key="documentAccessControl.fileType" cssClass="list-menu" cssStyle="width:160px;" list="%{docsList}" headerKey="" headerValue="" onchange="copyFileDescription();"/>
								</c:if>
								<c:if test="${not empty  documentAccessControl.id}">
									<s:textfield key="documentAccessControl.fileType" cssClass="input-textUpper" cssStyle="width:160px;" readonly="true"/>
								</c:if>
							</td>							
							<td>						
								<s:textfield key="documentAccessControl.fileDescription" cssClass="input-textUpper" cssStyle="width:178px;" readonly="true"/>
							</td>
						
							<c:set var="ischeckedIA" value="false"/>
								<c:if test="${documentAccessControl.invoiceAttachment}">
									<c:set var="ischeckedIA" value="true"/>
								</c:if>
							<td class="listwhitetext"> &nbsp;Invoice Attachment<s:checkbox key="documentAccessControl.invoiceAttachment" value="${ischeckedIA}" fieldValue="true"/></td>				
							
							<c:set var="isVendorCodeP" value="false"/>
								<c:if test="${documentAccessControl.isvendorCode}">
									<c:set var="isVendorCodeP" value="true"/>
								</c:if>
							<td class="listwhitetext"> &nbsp;Partner Code&nbsp;<s:checkbox key="documentAccessControl.isvendorCode" value="${isVendorCodeP}" fieldValue="true"/></td>				
								
								<c:set var="isPaymentStatusP" value="false"/>
								<c:if test="${documentAccessControl.isPaymentStatus}">
									<c:set var="isPaymentStatusP" value="true"/>
								</c:if>
							<td class="listwhitetext"> &nbsp;Paid&nbsp;<s:checkbox key="documentAccessControl.isPaymentStatus" value="${isPaymentStatusP}" fieldValue="true"/></td>				
							</tr>
						<tr>							
						</tr>						
						<tr>
						<td width="10px"></td>
					
						<td class="listwhitetext" valign="top">
				<fieldset style="width:137px; !width:138px; !padding-top:2px;height:102px; !height:90px;">
				<legend>Access Control</legend>
				<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
				<tr>
					
							<c:set var="ischecked" value="false"/>
								<c:if test="${documentAccessControl.isCportal}">
									<c:set var="ischecked" value="true"/>
								</c:if>	
							<td align="left" class="listwhitetext"><s:checkbox key="documentAccessControl.isCportal" value="${ischecked}" fieldValue="true"/> &nbsp;Cportal</td>
							</tr>
							<tr>
							<c:set var="ischeckedACC" value="false"/>
								<c:if test="${documentAccessControl.isAccportal}">
									<c:set var="ischeckedACC" value="true"/>
								</c:if>
							<td class="listwhitetext"><s:checkbox key="documentAccessControl.isAccportal" value="${ischeckedACC}" fieldValue="true"/> &nbsp;Accont Portal</td>
							</tr>
							<tr>
							<c:set var="ischeckedPP" value="false"/>
								<c:if test="${documentAccessControl.isPartnerPortal}">
									<c:set var="ischeckedPP" value="true"/>
								</c:if>
							<td class="listwhitetext"><s:checkbox key="documentAccessControl.isPartnerPortal" value="${ischeckedPP}" fieldValue="true"/> &nbsp;Partner Portal</td>				
							</tr>
							<tr>
							<c:set var="ischeckedSP" value="false"/>
								<c:if test="${documentAccessControl.isServiceProvider}">
									<c:set var="ischeckedSP" value="true"/>
								</c:if>
							<td class="listwhitetext"><s:checkbox key="documentAccessControl.isServiceProvider" value="${ischeckedSP}" fieldValue="true"/> &nbsp;Service Provider</td>				
							</tr>
						  		
				</table>
				
				</fieldset>
				</td>				
				<td class="listwhitetext" valign="top">
				<fieldset style="width:157px; !width:158px; !padding-top:2px; !height:90px;">
				<legend>Network Access Control</legend>
				<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1" width="100%">
										<tr>
											<td align="left" class="listwhitetext" colspan="2"><s:checkbox id="checkbox1" name="checkbox1" onclick="checkAllGroupBox1('yes');"/><b>Check/Uncheck All</b></td>               
										</tr>
										<tr>

									<c:set var="isBookingchecked" value="false"/>
								     <c:if test="${documentAccessControl.isBookingAgent}">
									<c:set var="isBookingchecked" value="true"/>
								       </c:if>
											<td align="left" class="listwhitetext" width="100px"><s:checkbox id="isBookingAgent" key="documentAccessControl.isBookingAgent" value="${isBookingchecked}" fieldValue="true"  />&nbsp;BA</td>
									
										<c:set var="isNetworkchecked" value="false"/>
								     <c:if test="${documentAccessControl.isNetworkAgent}">
									<c:set var="isNetworkchecked" value="true"/>
								       </c:if>
											<td align="left" class="listwhitetext" width="100px"><s:checkbox id="isNetworkAgent" key="documentAccessControl.isNetworkAgent" value="${isNetworkchecked}" fieldValue="true" />&nbsp;NA</td>
										
																				</tr>
										<tr>
										<c:set var="isOriginchecked" value="false"/>
								     <c:if test="${documentAccessControl.isOriginAgent}">
									<c:set var="isOriginchecked" value="true"/>
								       </c:if>
											<td class="listwhitetext" align="left" width="100px"><s:checkbox id="isOriginAgent" key="documentAccessControl.isOriginAgent" value="${isOriginchecked}" fieldValue="true" />&nbsp;OA</td>	
										
										<c:set var="isSubOriginchecked" value="false"/>
								     <c:if test="${documentAccessControl.isSubOriginAgent}">
									<c:set var="isSubOriginchecked" value="true"/>
								       </c:if>
											<td align="left" class="listwhitetext" width="100px"><s:checkbox id="isSubOriginAgent" key="documentAccessControl.isSubOriginAgent" value="${isSubOriginchecked}" fieldValue="true" />&nbsp;SOA</td>
										
										</tr>
										<tr>
											<c:set var="isDestchecked" value="false"/>
								     <c:if test="${documentAccessControl.isDestAgent}">
									<c:set var="isDestchecked" value="true"/>
								       </c:if>
											<td class="listwhitetext" align="left" width="100px"><s:checkbox id="isDestAgent" key="documentAccessControl.isDestAgent" value="${isDestchecked}" fieldValue="true"  />&nbsp;DA</td>
									
											<c:set var="isSubDestchecked" value="false"/>
								     <c:if test="${documentAccessControl.isSubDestAgent}">
									<c:set var="isSubDestchecked" value="true"/>
								       </c:if>
											<td align="left" class="listwhitetext" width="100px"><s:checkbox id="isSubDestAgent" key="documentAccessControl.isSubDestAgent" value="${isSubDestchecked}" fieldValue="true"  />&nbsp;SDA</td>	
										
										</tr>
									</table>
									</fieldset>
				</td>
						</tr>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
	 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<table border="0">
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='documentAccessControl.createdOn' /></b></td>

				
				<td style="width:130px"><fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${documentAccessControl.createdOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="documentAccessControl.createdOn" value="${customerFileCreatedOnFormattedValue}" /> <fmt:formatDate value="${documentAccessControl.createdOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="right" class="listwhitetext" style="width:65px"><b><fmt:message key='billing.createdBy' /></b></td>
				<c:if test="${not empty documentAccessControl.id}">
					<s:hidden name="documentAccessControl.createdBy" />
					<td style="width:65px"><s:label name="createdBy" value="%{documentAccessControl.createdBy}" /></td>
				</c:if>
				<c:if test="${empty documentAccessControl.id}">
					<s:hidden name="documentAccessControl.createdBy" value="${pageContext.request.remoteUser}" />
					<td style="width:65px"><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
				</c:if>				
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.updatedOn' /></b></td>
				<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${documentAccessControl.updatedOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="documentAccessControl.updatedOn" value="${customerFileupdatedOnFormattedValue}" />
				<td style="width:120px"><fmt:formatDate value="${documentAccessControl.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.updatedBy' /></b></td>
				<s:hidden name="documentAccessControl.updatedBy" value="${pageContext.request.remoteUser}" />
				<td style="width:85px"><s:label name="documentAccessControl.updatedBy" value="${pageContext.request.remoteUser}" /></td>
			</tr>
		</tbody>
	</table>
			
		<tr>
			<td><s:submit id="saveButton" key="button.save" cssClass="cssbutton" cssStyle="margin-right: 5px;height: 25px;width:60px; font-size: 15" onclick="return validateFields();"/></td>
		</tr>
</s:form>

<script type="text/javascript">
try{
<c:if test="${hitFlag == 1}" >
	<c:redirect url="/docControl.html"/>
</c:if>
}
catch(e){}
</script>


