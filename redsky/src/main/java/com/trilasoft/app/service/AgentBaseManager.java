package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.AgentBase;

public interface AgentBaseManager  extends GenericManager<AgentBase, Long> {
	
	public List getbaseListByPartnerCode(String partnerCode);

	public List isExisted(String baseCode, String sessionCorpID, String partnerCode);

	public void deleteBaseSCAC(String partnerCode, String baseCode, String sessionCorpID);
}
