<%@ include file="/common/taglibs.jsp"%>
<head>
<title>Inventory Search</title>
<STYLE type=text/css>
</style>
<script type="text/javascript">

function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
function selVal(){
	<c:forEach items="${inventoryDataListFromPieceId}" var="piece"> 
		var temp = '${piece.location}';
		document.forms['pieceIDDetailForm'].elements['inventoryDataListFromPieceId.location'].value=temp;
	</c:forEach>
}
function imageList(sid,id,position){
	var url="inventoryPackingImageList.html?sid="+sid+"&id="+id+"&decorator=simple&popup=true";
	ajax_SoTooltip(url,position);	
}
</script>
<meta name="heading"
	content="Inventory Details" />
</head>
<div id="otabs" style="margin-bottom:25px;">
		  <ul>
		    <li style="margin-bottom:-12px;"><a href="inventoryDetails.html?sid=${sid}"><span>Inventory Data List</span></a></li>
		    <li ><a class="current"><span>Inventory Data</span></a></li>
		  </ul>
		</div>
<s:form name="pieceIDDetailForm" action="">
<div id="content" align="center" style="width:80%">
<div id="liquid-round-top">
    <div class="top" ><span></span></div>   
    <s:hidden name="countdown"  value="250"/>
    <s:hidden name="sid"  value="<%= request.getParameter("sid") %>"/>
    <c:set var="sid" value="<%= request.getParameter("sid") %>"/>
   <div class="center-content" style="padding-left:15px;">
 
   <display:table name="inventoryDataListFromPieceId" id="inventoryDataListFromPieceId" class="table">
	   <display:column title="ID#"><s:textfield name="inventoryDataListFromPieceId.pieceID" value="${inventoryDataListFromPieceId.pieceID }"  size="3" readonly="true" cssClass="input-textUpper"/></display:column>
	  <display:column title="Room"><s:select cssClass="list-menu" cssStyle="width:100px; !width:50px;" name="inventoryDataListFromPieceId.location" list="%{roomType}" value="${inventoryDataListFromPieceId.location}"   headerKey="" headerValue="" disabled="true"/></display:column>
	  <display:column title="PKG"><s:textfield name="inventoryDataListFromPieceId.boxName" value="${inventoryDataListFromPieceId.boxName }"  size="5" readonly="true" cssClass="input-textUpper"/></display:column>
	  <display:column title="Packer"><s:textfield name="Packer" value=""  size="5" readonly="true" cssClass="input-textUpper"/></display:column>
	   <display:column title="Volume"><s:textfield name="inventoryDataListFromPieceId.volume" value="${inventoryDataListFromPieceId.volume}"  size="5" readonly="true" cssClass="input-textUpper"/></display:column>
	   <display:column title="Weight"><s:textfield name="inventoryDataListFromPieceId.weight" value="${inventoryDataListFromPieceId.weight}"  size="5" readonly="true" cssClass="input-textUpper"/></display:column>
  </display:table>
  			<c:set var="quantitysum" value="0" />
			<display:table name="inventoryPackingList" class="table" id="inventoryPackingList">
			<display:column    title="#" paramId="id" paramProperty="id" sortProperty="id">
  			<c:out value='${inventoryPackingList_rowNum}'/>
  			</display:column>
				<display:column title="Item" >
					<a href="editInventoryDetails.html?id=${inventoryPackingList.id}&sid=${sid}" paramId="id" paramProperty="id" />${inventoryPackingList.item}
				</display:column>
				<display:column property="conditon" title="Condition"/>
				<display:column property="itemQuantity" title="Quantity"  style="text-align:right;" />
				<c:set var="quantitysum" value="${inventoryPackingList.itemQuantity+quantitysum }" />
				<display:column property="value" title="Value" style="text-align:right;"/>
				<display:column title="Dismantling">
				<c:if test="${ inventoryPackingList.dismantling == true}">
					<input type="checkbox" value="${inventoryPackingList.dismantling}"  disabled="true" checked="true" />
				</c:if>
				<c:if test="${ inventoryPackingList.dismantling == false}">
					<input type="checkbox" value="${inventoryPackingList.dismantling}"  disabled="true" />
				</c:if>
				</display:column>
				<display:column title="Assembling">
				<c:if test="${ inventoryPackingList.assembling == true}">
					<input type="checkbox" value="${inventoryPackingList.assembling}" disabled="true"  checked="true" />
				</c:if>
				<c:if test="${ inventoryPackingList.assembling == false}">
					<input type="checkbox" value="${inventoryPackingList.assembling}" disabled="true" />
				</c:if>
				</display:column>
				<display:column title="Container">
				<c:if test="${ inventoryPackingList.specialContainer == true}">
					<input type="checkbox" value="${inventoryPackingList.specialContainer}" disabled="true"  checked="true" />
				</c:if>
				<c:if test="${ inventoryPackingList.specialContainer == false}">
					<input type="checkbox" value="${inventoryPackingList.specialContainer}" disabled="true" />
				</c:if>
					
				</display:column>
				<display:column property="comment" title="Comment"/>
				<display:column title="Images">
				  <c:if test="${inventoryPackingList.imagesAvailablityFlag!=null && inventoryPackingList.imagesAvailablityFlag!='' && inventoryPackingList.imagesAvailablityFlag=='1000'}" >
             		<img id="userImage" src="${pageContext.request.contextPath}/images/camera24.png" alt="" width="24" height="24" onclick="imageList('${sid}','${inventoryPackingList.id}',this)" />                             
               	</c:if>
               	</display:column>
               	<display:footer >
            <tr> 
           
 	  	          <td align="right"  colspan="3" ><b><div align="right">Total</div></b></td>
		          <td align="right" class="tdheight-footer"> <fmt:formatNumber type="number"  maxFractionDigits="0" minFractionDigits="0"
                  groupingUsed="true" value="${quantitysum}"  />
		          </td>
		         
		          <td align="left" colspan="6"></td>
           </tr>
	</display:footer>
				</display:table>
    </div>
      <div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>

</div>
</div> 
 
</s:form>
<script>
try{selVal();}
catch(e){}
</script>


