package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Entitlement;

public interface EntitlementDao extends GenericDao<Entitlement, Long> {
	
	public Map<String,String> findEntitlementAll();
	
	public List findEntitlementByPartnerId(String corpId, Long partnerId);
	public Map<String, String> findEntitlementByParentAgent(String corpId,String parentAgent, Boolean networkRecord, String contractType);
	public List checkOptionDuplicate(String PartnerCode, String corpId,String Option);
	public List getEntitlementReference(String partnerCode, String corpID,Long parentId);
	public List findLinkEntitlementByPartner(long parseLong);
}
