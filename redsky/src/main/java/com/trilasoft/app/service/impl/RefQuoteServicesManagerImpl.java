package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;


import com.trilasoft.app.dao.RefQuoteServicesDao;
import com.trilasoft.app.model.RefQuoteServices;
import com.trilasoft.app.service.RefQuoteServicesManager;
public class RefQuoteServicesManagerImpl extends GenericManagerImpl<RefQuoteServices,Long> implements RefQuoteServicesManager
{
	RefQuoteServicesDao refQuoteServicesDao;
	public RefQuoteServicesManagerImpl(RefQuoteServicesDao refQuoteServicesDao) {
        super(refQuoteServicesDao);
        this.refQuoteServicesDao = refQuoteServicesDao;
    
	}
	public List searchList(String job, String mode,String routing, String langauge, String sessionCorpID ,String description,String checkIncludeExclude,String companyDivision,String commodity,String packingMode,String originCountry,String destinationCountry,String serviceType,String contract){
		return refQuoteServicesDao.searchList(job, mode, routing, langauge, sessionCorpID, description, checkIncludeExclude, companyDivision, commodity, packingMode, originCountry, destinationCountry,serviceType,contract);
	}
	public void autoSaveRefQuotesSevisesAjax(String sessionCorpID,String fieldName,String fieldVal,Long serviceId){
		refQuoteServicesDao.autoSaveRefQuotesSevisesAjax(sessionCorpID, fieldName, fieldVal, serviceId);
	}
	public Map<String, String> findForContract(String corpID) {
		
		return refQuoteServicesDao.findForContract(corpID);
		
	}
	/*public String findUniqueCode(String code, String sessionCorpID) {
		return refQuoteServicesDao.findUniqueCode(code , sessionCorpID);
	}
	*/
}

