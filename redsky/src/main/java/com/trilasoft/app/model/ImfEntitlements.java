package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "imfentitlements")
public class ImfEntitlements extends BaseObject{
	
	private Long id;
	private String corpID;
	private BigDecimal totalFreight = new BigDecimal("0.00");
	private BigDecimal airEntitlementsWeight;
	private BigDecimal airEntitlementsFreight;
	private BigDecimal airEntitlementsOrigin;
	private BigDecimal airEntitlementsDestination;
	private BigDecimal airEntitlementsTotal = new BigDecimal("0.00");
	private Boolean checkAirEntitlementsWeight;
	
	private BigDecimal surfaceEntitlementsWeight;
	private BigDecimal surfaceEntitlementsFreight;
	private BigDecimal surfaceEntitlementsOrigin;
	private BigDecimal surfaceEntitlementsDestination;
	private BigDecimal surfaceEntitlementsTotal = new BigDecimal("0.00");
	private Boolean checkSurfaceEntitlementsWeight;
	
	private BigDecimal autoEntitlementsWeight;
	private BigDecimal autoEntitlementsFreight;
	private BigDecimal autoEntitlementsOrigin;
	private BigDecimal autoEntitlementsDestination;
	private BigDecimal autoEntitlementsTotal = new BigDecimal("0.00");
	private Boolean checkAutoEntitlementsWeight;
	
	private BigDecimal officeEntitlementsWeight;
	private BigDecimal officeEntitlementsFreight;
	private BigDecimal officeEntitlementsOrigin;
	private BigDecimal officeEntitlementsDestination;
	private BigDecimal officeEntitlementsTotal =  new BigDecimal("0.00");;
	private Boolean checkOfficeEntitlementsWeight;
	
	private String insurance;
	private String rate;
	private Boolean permStorage;
	private String costShare;
	private Date readyToSend;
	private Date sent;
	private String sequenceNumber; 
	
	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ImfEntitlements))
			return false;
		ImfEntitlements castOther = (ImfEntitlements) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(totalFreight, castOther.totalFreight)
				.append(airEntitlementsWeight, castOther.airEntitlementsWeight)
				.append(airEntitlementsFreight,
						castOther.airEntitlementsFreight).append(
						airEntitlementsOrigin, castOther.airEntitlementsOrigin)
				.append(airEntitlementsDestination,
						castOther.airEntitlementsDestination).append(
						airEntitlementsTotal, castOther.airEntitlementsTotal)
				.append(checkAirEntitlementsWeight,
						castOther.checkAirEntitlementsWeight).append(
						surfaceEntitlementsWeight,
						castOther.surfaceEntitlementsWeight).append(
						surfaceEntitlementsFreight,
						castOther.surfaceEntitlementsFreight).append(
						surfaceEntitlementsOrigin,
						castOther.surfaceEntitlementsOrigin).append(
						surfaceEntitlementsDestination,
						castOther.surfaceEntitlementsDestination).append(
						surfaceEntitlementsTotal,
						castOther.surfaceEntitlementsTotal).append(
						checkSurfaceEntitlementsWeight,
						castOther.checkSurfaceEntitlementsWeight).append(
						autoEntitlementsWeight,
						castOther.autoEntitlementsWeight).append(
						autoEntitlementsFreight,
						castOther.autoEntitlementsFreight).append(
						autoEntitlementsOrigin,
						castOther.autoEntitlementsOrigin).append(
						autoEntitlementsDestination,
						castOther.autoEntitlementsDestination).append(
						autoEntitlementsTotal, castOther.autoEntitlementsTotal)
				.append(checkAutoEntitlementsWeight,
						castOther.checkAutoEntitlementsWeight).append(
						officeEntitlementsWeight,
						castOther.officeEntitlementsWeight).append(
						officeEntitlementsFreight,
						castOther.officeEntitlementsFreight).append(
						officeEntitlementsOrigin,
						castOther.officeEntitlementsOrigin).append(
						officeEntitlementsDestination,
						castOther.officeEntitlementsDestination).append(
						officeEntitlementsTotal,
						castOther.officeEntitlementsTotal).append(
						checkOfficeEntitlementsWeight,
						castOther.checkOfficeEntitlementsWeight).append(
						insurance, castOther.insurance).append(rate,
						castOther.rate).append(permStorage,
						castOther.permStorage).append(costShare,
						castOther.costShare).append(readyToSend,
						castOther.readyToSend).append(sent, castOther.sent)
				.append(sequenceNumber, castOther.sequenceNumber).append(
						createdBy, castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				totalFreight).append(airEntitlementsWeight).append(
				airEntitlementsFreight).append(airEntitlementsOrigin).append(
				airEntitlementsDestination).append(airEntitlementsTotal)
				.append(checkAirEntitlementsWeight).append(
						surfaceEntitlementsWeight).append(
						surfaceEntitlementsFreight).append(
						surfaceEntitlementsOrigin).append(
						surfaceEntitlementsDestination).append(
						surfaceEntitlementsTotal).append(
						checkSurfaceEntitlementsWeight).append(
						autoEntitlementsWeight).append(autoEntitlementsFreight)
				.append(autoEntitlementsOrigin).append(
						autoEntitlementsDestination).append(
						autoEntitlementsTotal).append(
						checkAutoEntitlementsWeight).append(
						officeEntitlementsWeight).append(
						officeEntitlementsFreight).append(
						officeEntitlementsOrigin).append(
						officeEntitlementsDestination).append(
						officeEntitlementsTotal).append(
						checkOfficeEntitlementsWeight).append(insurance)
				.append(rate).append(permStorage).append(costShare).append(
						readyToSend).append(sent).append(sequenceNumber)
				.append(createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).toHashCode();
	}
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("totalFreight", totalFreight).append(
				"airEntitlementsWeight", airEntitlementsWeight).append(
				"airEntitlementsFreight", airEntitlementsFreight).append(
				"airEntitlementsOrigin", airEntitlementsOrigin).append(
				"airEntitlementsDestination", airEntitlementsDestination)
				.append("airEntitlementsTotal", airEntitlementsTotal).append(
						"checkAirEntitlementsWeight",
						checkAirEntitlementsWeight).append(
						"surfaceEntitlementsWeight", surfaceEntitlementsWeight)
				.append("surfaceEntitlementsFreight",
						surfaceEntitlementsFreight).append(
						"surfaceEntitlementsOrigin", surfaceEntitlementsOrigin)
				.append("surfaceEntitlementsDestination",
						surfaceEntitlementsDestination).append(
						"surfaceEntitlementsTotal", surfaceEntitlementsTotal)
				.append("checkSurfaceEntitlementsWeight",
						checkSurfaceEntitlementsWeight).append(
						"autoEntitlementsWeight", autoEntitlementsWeight)
				.append("autoEntitlementsFreight", autoEntitlementsFreight)
				.append("autoEntitlementsOrigin", autoEntitlementsOrigin)
				.append("autoEntitlementsDestination",
						autoEntitlementsDestination).append(
						"autoEntitlementsTotal", autoEntitlementsTotal).append(
						"checkAutoEntitlementsWeight",
						checkAutoEntitlementsWeight).append(
						"officeEntitlementsWeight", officeEntitlementsWeight)
				.append("officeEntitlementsFreight", officeEntitlementsFreight)
				.append("officeEntitlementsOrigin", officeEntitlementsOrigin)
				.append("officeEntitlementsDestination",
						officeEntitlementsDestination).append(
						"officeEntitlementsTotal", officeEntitlementsTotal)
				.append("checkOfficeEntitlementsWeight",
						checkOfficeEntitlementsWeight).append("insurance",
						insurance).append("rate", rate).append("permStorage",
						permStorage).append("costShare", costShare).append(
						"readyToSend", readyToSend).append("sent", sent)
				.append("sequenceNumber", sequenceNumber).append("createdBy",
						createdBy).append("updatedBy", updatedBy).append(
						"createdOn", createdOn).append("updatedOn", updatedOn)
				.toString();
	}
	@Column(length = 50)
	public BigDecimal getAirEntitlementsDestination() {
		return airEntitlementsDestination;
	}
	public void setAirEntitlementsDestination(BigDecimal airEntitlementsDestination) {
		this.airEntitlementsDestination = airEntitlementsDestination;
	}
	@Column(length = 50)
	public BigDecimal getAirEntitlementsFreight() {
		return airEntitlementsFreight;
	}
	public void setAirEntitlementsFreight(BigDecimal airEntitlementsFreight) {
		this.airEntitlementsFreight = airEntitlementsFreight;
	}
	@Column(length = 50)
	public BigDecimal getAirEntitlementsOrigin() {
		return airEntitlementsOrigin;
	}
	public void setAirEntitlementsOrigin(BigDecimal airEntitlementsOrigin) {
		this.airEntitlementsOrigin = airEntitlementsOrigin;
	}
	
	@Column(length = 50)
	public BigDecimal getAutoEntitlementsDestination() {
		return autoEntitlementsDestination;
	}
	public void setAutoEntitlementsDestination(
			BigDecimal autoEntitlementsDestination) {
		this.autoEntitlementsDestination = autoEntitlementsDestination;
	}
	@Column(length = 50)
	public BigDecimal getAutoEntitlementsFreight() {
		return autoEntitlementsFreight;
	}
	public void setAutoEntitlementsFreight(BigDecimal autoEntitlementsFreight) {
		this.autoEntitlementsFreight = autoEntitlementsFreight;
	}
	@Column(length = 50)
	public BigDecimal getAutoEntitlementsOrigin() {
		return autoEntitlementsOrigin;
	}
	public void setAutoEntitlementsOrigin(BigDecimal autoEntitlementsOrigin) {
		this.autoEntitlementsOrigin = autoEntitlementsOrigin;
	}
	
	@Column(length = 50)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length = 50)
	public String getCostShare() {
		return costShare;
	}
	public void setCostShare(String costShare) {
		this.costShare = costShare;
	}
	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length = 50)
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length = 50)
	public String getInsurance() {
		return insurance;
	}
	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}
	@Column(length = 50)
	public BigDecimal getOfficeEntitlementsDestination() {
		return officeEntitlementsDestination;
	}
	public void setOfficeEntitlementsDestination(
			BigDecimal officeEntitlementsDestination) {
		this.officeEntitlementsDestination = officeEntitlementsDestination;
	}
	@Column(length = 50)
	public BigDecimal getOfficeEntitlementsFreight() {
		return officeEntitlementsFreight;
	}
	public void setOfficeEntitlementsFreight(BigDecimal officeEntitlementsFreight) {
		this.officeEntitlementsFreight = officeEntitlementsFreight;
	}
	@Column(length = 50)
	public BigDecimal getOfficeEntitlementsOrigin() {
		return officeEntitlementsOrigin;
	}
	public void setOfficeEntitlementsOrigin(BigDecimal officeEntitlementsOrigin) {
		this.officeEntitlementsOrigin = officeEntitlementsOrigin;
	}
	
	@Column(length = 50)
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	@Column(length = 50)
	public BigDecimal getSurfaceEntitlementsDestination() {
		return surfaceEntitlementsDestination;
	}
	public void setSurfaceEntitlementsDestination(
			BigDecimal surfaceEntitlementsDestination) {
		this.surfaceEntitlementsDestination = surfaceEntitlementsDestination;
	}
	@Column(length = 50)
	public BigDecimal getSurfaceEntitlementsFreight() {
		return surfaceEntitlementsFreight;
	}
	public void setSurfaceEntitlementsFreight(BigDecimal surfaceEntitlementsFreight) {
		this.surfaceEntitlementsFreight = surfaceEntitlementsFreight;
	}
	@Column(length = 50)
	public BigDecimal getSurfaceEntitlementsOrigin() {
		return surfaceEntitlementsOrigin;
	}
	public void setSurfaceEntitlementsOrigin(BigDecimal surfaceEntitlementsOrigin) {
		this.surfaceEntitlementsOrigin = surfaceEntitlementsOrigin;
	}
	
	@Column(length = 50)
	public BigDecimal getTotalFreight() {
		return totalFreight;
	}
	public void setTotalFreight(BigDecimal totalFreight) {
		this.totalFreight = totalFreight;
	}
	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column(length = 50)
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length = 50)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public void setReadyToSend(Date readyToSend) {
		this.readyToSend = readyToSend;
	}
	public void setSent(Date sent) {
		this.sent = sent;
	}
	@Column(length = 50)
	public Date getReadyToSend() {
		return readyToSend;
	}
	@Column(length = 50)
	public Date getSent() {
		return sent;
	}
	public Boolean getCheckAirEntitlementsWeight() {
		return checkAirEntitlementsWeight;
	}
	public void setCheckAirEntitlementsWeight(Boolean checkAirEntitlementsWeight) {
		this.checkAirEntitlementsWeight = checkAirEntitlementsWeight;
	}
	public Boolean getCheckAutoEntitlementsWeight() {
		return checkAutoEntitlementsWeight;
	}
	public void setCheckAutoEntitlementsWeight(Boolean checkAutoEntitlementsWeight) {
		this.checkAutoEntitlementsWeight = checkAutoEntitlementsWeight;
	}
	public Boolean getCheckOfficeEntitlementsWeight() {
		return checkOfficeEntitlementsWeight;
	}
	public void setCheckOfficeEntitlementsWeight(
			Boolean checkOfficeEntitlementsWeight) {
		this.checkOfficeEntitlementsWeight = checkOfficeEntitlementsWeight;
	}
	public Boolean getCheckSurfaceEntitlementsWeight() {
		return checkSurfaceEntitlementsWeight;
	}
	public void setCheckSurfaceEntitlementsWeight(
			Boolean checkSurfaceEntitlementsWeight) {
		this.checkSurfaceEntitlementsWeight = checkSurfaceEntitlementsWeight;
	}
	public Boolean getPermStorage() {
		return permStorage;
	}
	public void setPermStorage(Boolean permStorage) {
		this.permStorage = permStorage;
	}
	@Column(length = 50)
	public BigDecimal getAirEntitlementsTotal() {
		return airEntitlementsTotal;
	}
	public void setAirEntitlementsTotal(BigDecimal airEntitlementsTotal) {
		this.airEntitlementsTotal = airEntitlementsTotal;
	}
	@Column(length = 50)
	public BigDecimal getAirEntitlementsWeight() {
		return airEntitlementsWeight;
	}
	public void setAirEntitlementsWeight(BigDecimal airEntitlementsWeight) {
		this.airEntitlementsWeight = airEntitlementsWeight;
	}
	@Column(length = 50)
	public BigDecimal getAutoEntitlementsTotal() {
		return autoEntitlementsTotal;
	}
	public void setAutoEntitlementsTotal(BigDecimal autoEntitlementsTotal) {
		this.autoEntitlementsTotal = autoEntitlementsTotal;
	}
	@Column(length = 50)
	public BigDecimal getAutoEntitlementsWeight() {
		return autoEntitlementsWeight;
	}
	public void setAutoEntitlementsWeight(BigDecimal autoEntitlementsWeight) {
		this.autoEntitlementsWeight = autoEntitlementsWeight;
	}
	@Column(length = 50)
	public BigDecimal getOfficeEntitlementsTotal() {
		return officeEntitlementsTotal;
	}
	public void setOfficeEntitlementsTotal(BigDecimal officeEntitlementsTotal) {
		this.officeEntitlementsTotal = officeEntitlementsTotal;
	}
	@Column(length = 50)
	public BigDecimal getOfficeEntitlementsWeight() {
		return officeEntitlementsWeight;
	}
	public void setOfficeEntitlementsWeight(BigDecimal officeEntitlementsWeight) {
		this.officeEntitlementsWeight = officeEntitlementsWeight;
	}
	@Column(length = 50)
	public BigDecimal getSurfaceEntitlementsTotal() {
		return surfaceEntitlementsTotal;
	}
	public void setSurfaceEntitlementsTotal(BigDecimal surfaceEntitlementsTotal) {
		this.surfaceEntitlementsTotal = surfaceEntitlementsTotal;
	}
	@Column(length = 50)
	public BigDecimal getSurfaceEntitlementsWeight() {
		return surfaceEntitlementsWeight;
	}
	public void setSurfaceEntitlementsWeight(BigDecimal surfaceEntitlementsWeight) {
		this.surfaceEntitlementsWeight = surfaceEntitlementsWeight;
	}


}
