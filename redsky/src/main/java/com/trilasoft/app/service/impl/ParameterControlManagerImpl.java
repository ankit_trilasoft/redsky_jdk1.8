package com.trilasoft.app.service.impl;
import java.util.List;

import com.trilasoft.app.model.ParameterControl;
import com.trilasoft.app.service.ParameterControlManager;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.ParameterControlDao;
public class ParameterControlManagerImpl extends GenericManagerImpl<ParameterControl,Long> implements ParameterControlManager{
	ParameterControlDao parameterControlDao;
	
	ParameterControlManagerImpl(ParameterControlDao parameterControlDao){
		super(parameterControlDao);
		this.parameterControlDao=parameterControlDao;
	}
	public List findUseCorpId(){
		  return parameterControlDao.findUseCorpId();
		  
	  }
	public Boolean checkTsftFlag(String parameter2, String sessionCorpID2) {
		return parameterControlDao.checkTsftFlag(parameter2, sessionCorpID2);
	}
	public Boolean checkHybridFlag(String parameter2, String sessionCorpID2) {
		return parameterControlDao.checkHybridFlag(parameter2, sessionCorpID2);
	}

	public List<ParameterControl> findByFields(String parameter,String customType,Boolean active,Boolean tsftFlag,Boolean hybridFlag){
		return parameterControlDao.findByFields(parameter, customType, active, tsftFlag, hybridFlag);
		
	}
	public List getAllParameter(String paramater){
		  return parameterControlDao.getAllParameter(paramater);
		  
	  }
	public String getDescription(String parameter,String corpId){
		return parameterControlDao.getDescription(parameter,corpId);
	}
	
}
