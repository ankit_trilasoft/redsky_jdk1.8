delimiter $$

CREATE
DEFINER=`root`@`localhost`
TRIGGER `redsky`.`trigger_add_history_customerfile`
BEFORE UPDATE ON `redsky`.`customerfile`
FOR EACH ROW
BEGIN
DECLARE fieldNameList LONGTEXT;
DECLARE oldValueList LONGTEXT;
DECLARE newValueList LONGTEXT;
DECLARE cid LONGTEXT;
DECLARE updatedon DATETIME;
  set NEW.updatedon=now();

SET fieldNameList = " ";
SET oldValueList = " ";
SET newValueList = " ";


IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
    select CONCAT(fieldNameList,'customerfile.sequenceNumber~') into fieldNameList;
    select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
    select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
END IF;

IF (OLD.surveyReferenceNumber <> NEW.surveyReferenceNumber ) THEN
    select CONCAT(fieldNameList,'customerfile.surveyReferenceNumber~') into fieldNameList;
    select CONCAT(oldValueList,OLD.surveyReferenceNumber,'~') into oldValueList;
    select CONCAT(newValueList,NEW.surveyReferenceNumber,'~') into newValueList;
END IF;
IF (OLD.customerLanguagePreference <> NEW.customerLanguagePreference ) THEN
    select CONCAT(fieldNameList,'customerfile.customerLanguagePreference~') into fieldNameList;
    select CONCAT(oldValueList,OLD.customerLanguagePreference,'~') into oldValueList;
    select CONCAT(newValueList,NEW.customerLanguagePreference,'~') into newValueList;
END IF;

IF (OLD.orgLocId <> NEW.orgLocId ) THEN
    select CONCAT(fieldNameList,'customerfile.orgLocId~') into fieldNameList;
    select CONCAT(oldValueList,OLD.orgLocId,'~') into oldValueList;
    select CONCAT(newValueList,NEW.orgLocId,'~') into newValueList;
END IF;

IF (OLD.destLocId <> NEW.destLocId ) THEN
    select CONCAT(fieldNameList,'customerfile.destLocId~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destLocId,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destLocId,'~') into newValueList;
END IF;

IF (OLD.status != NEW.status)  THEN
    IF (OLD.status ='CNCL' AND NEW.status ='REOPEN') THEN 
       set NEW.jimExtract=false;
    END IF; 
    IF (NEW.status ='CNCL' ) THEN 
    set NEW.jimExtract=false;
    END IF;
    END IF;
    
IF (OLD.status <> NEW.status ) THEN
    select CONCAT(fieldNameList,'customerfile.status~') into fieldNameList;
    select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
    select CONCAT(newValueList,NEW.status,'~') into newValueList;
END IF;
IF (OLD.salesMan <> NEW.salesMan ) THEN
    select CONCAT(fieldNameList,'customerfile.salesMan~') into fieldNameList;
    select CONCAT(oldValueList,OLD.salesMan,'~') into oldValueList;
    select CONCAT(newValueList,NEW.salesMan,'~') into newValueList;
END IF;
IF (OLD.prefix <> NEW.prefix ) THEN
    select CONCAT(fieldNameList,'customerfile.prefix~') into fieldNameList;
    select CONCAT(oldValueList,OLD.prefix,'~') into oldValueList;
    select CONCAT(newValueList,NEW.prefix,'~') into newValueList;
END IF;
IF (OLD.firstName <> NEW.firstName ) THEN
    select CONCAT(fieldNameList,'customerfile.firstName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.firstName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.firstName,'~') into newValueList;
END IF;

IF (OLD.lastName <> NEW.lastName ) THEN
    select CONCAT(fieldNameList,'customerfile.lastName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.lastName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.lastName,'~') into newValueList;
END IF;

IF (OLD.middleInitial <> NEW.middleInitial ) THEN
    select CONCAT(fieldNameList,'customerfile.middleInitial~') into fieldNameList;
    select CONCAT(oldValueList,OLD.middleInitial,'~') into oldValueList;
    select CONCAT(newValueList,NEW.middleInitial,'~') into newValueList;
END IF;


IF (OLD.contactName <> NEW.contactName ) THEN
    select CONCAT(fieldNameList,'customerfile.contactName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.contactName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.contactName,'~') into newValueList;
END IF;

IF (OLD.contactPhone <> NEW.contactPhone ) THEN
    select CONCAT(fieldNameList,'customerfile.contactPhone~') into fieldNameList;
    select CONCAT(oldValueList,OLD.contactPhone,'~') into oldValueList;
    select CONCAT(newValueList,NEW.contactPhone,'~') into newValueList;
END IF;

IF (OLD.billToCode <> NEW.billToCode ) THEN
    select CONCAT(fieldNameList,'customerfile.billToCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.billToCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.billToCode,'~') into newValueList;
END IF;

IF (OLD.billToName <> NEW.billToName ) THEN
    select CONCAT(fieldNameList,'customerfile.billToName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.billToName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.billToName,'~') into newValueList;
END IF;

IF (OLD.billToReference <> NEW.billToReference ) THEN
    select CONCAT(fieldNameList,'customerfile.billToReference~') into fieldNameList;
    select CONCAT(oldValueList,OLD.billToReference,'~') into oldValueList;
    select CONCAT(newValueList,NEW.billToReference,'~') into newValueList;
END IF;

IF (OLD.billToAuthorization <> NEW.billToAuthorization ) THEN
    select CONCAT(fieldNameList,'customerfile.billToAuthorization~') into fieldNameList;
    select CONCAT(oldValueList,OLD.billToAuthorization,'~') into oldValueList;
    select CONCAT(newValueList,NEW.billToAuthorization,'~') into newValueList;
END IF;

IF (OLD.surveyTime <> NEW.surveyTime ) THEN
    select CONCAT(fieldNameList,'customerfile.surveyTime~') into fieldNameList;
    select CONCAT(oldValueList,OLD.surveyTime,'~') into oldValueList;
    select CONCAT(newValueList,NEW.surveyTime,'~') into newValueList;
END IF;
IF (OLD.surveyTime2 <> NEW.surveyTime2 ) THEN
    select CONCAT(fieldNameList,'customerfile.surveyTime2~') into fieldNameList;
    select CONCAT(oldValueList,OLD.surveyTime2,'~') into oldValueList;
    select CONCAT(newValueList,NEW.surveyTime2,'~') into newValueList;
END IF;
IF (OLD.sourceCode <> NEW.sourceCode ) THEN
    select CONCAT(fieldNameList,'customerfile.sourceCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.sourceCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.sourceCode,'~') into newValueList;
END IF;
IF (OLD.source <> NEW.source ) THEN
    select CONCAT(fieldNameList,'customerfile.source~') into fieldNameList;
    select CONCAT(oldValueList,OLD.source,'~') into oldValueList;
    select CONCAT(newValueList,NEW.source,'~') into newValueList;
END IF;
IF (OLD.comptetive <> NEW.comptetive ) THEN
    select CONCAT(fieldNameList,'customerfile.comptetive~') into fieldNameList;
    select CONCAT(oldValueList,OLD.comptetive,'~') into oldValueList;
    select CONCAT(newValueList,NEW.comptetive,'~') into newValueList;
END IF;
IF (OLD.entitled <> NEW.entitled ) THEN
    select CONCAT(fieldNameList,'customerfile.entitled~') into fieldNameList;
    select CONCAT(oldValueList,OLD.entitled,'~') into oldValueList;
    select CONCAT(newValueList,NEW.entitled,'~') into newValueList;
END IF;
IF (OLD.orderComment <> NEW.orderComment ) THEN
    select CONCAT(fieldNameList,'customerfile.orderComment~') into fieldNameList;
    select CONCAT(oldValueList,OLD.orderComment,'~') into oldValueList;
    select CONCAT(newValueList,NEW.orderComment,'~') into newValueList;
END IF;
IF (OLD.visaStatus <> NEW.visaStatus ) THEN
    select CONCAT(fieldNameList,'customerfile.visaStatus~') into fieldNameList;
    select CONCAT(oldValueList,OLD.visaStatus,'~') into oldValueList;
    select CONCAT(newValueList,NEW.visaStatus,'~') into newValueList;
END IF;
IF (OLD.orderIntiationStatus <> NEW.orderIntiationStatus ) THEN
    select CONCAT(fieldNameList,'customerfile.orderIntiationStatus~') into fieldNameList;
    select CONCAT(oldValueList,OLD.orderIntiationStatus,'~') into oldValueList;
    select CONCAT(newValueList,NEW.orderIntiationStatus,'~') into newValueList;
END IF;
IF (OLD.homeCountry <> NEW.homeCountry ) THEN
    select CONCAT(fieldNameList,'customerfile.homeCountry~') into fieldNameList;
    select CONCAT(oldValueList,OLD.homeCountry,'~') into oldValueList;
    select CONCAT(newValueList,NEW.homeCountry,'~') into newValueList;
END IF;
IF (OLD.homeCity <> NEW.homeCity ) THEN
    select CONCAT(fieldNameList,'customerfile.homeCity~') into fieldNameList;
    select CONCAT(oldValueList,OLD.homeCity,'~') into oldValueList;
    select CONCAT(newValueList,NEW.homeCity,'~') into newValueList;
END IF;
IF (OLD.voxmeFlag <> NEW.voxmeFlag ) THEN
    select CONCAT(fieldNameList,'customerfile.voxmeFlag~') into fieldNameList;
    select CONCAT(oldValueList,OLD.voxmeFlag,'~') into oldValueList;
    select CONCAT(newValueList,NEW.voxmeFlag,'~') into newValueList;
END IF;
IF (OLD.parentAgent <> NEW.parentAgent ) THEN
    select CONCAT(fieldNameList,'customerfile.parentAgent~') into fieldNameList;
    select CONCAT(oldValueList,OLD.parentAgent,'~') into oldValueList;
    select CONCAT(newValueList,NEW.parentAgent,'~') into newValueList;
END IF;
IF (OLD.homeState <> NEW.homeState ) THEN
    select CONCAT(fieldNameList,'customerfile.homeState~') into fieldNameList;
    select CONCAT(oldValueList,OLD.homeState,'~') into oldValueList;
    select CONCAT(newValueList,NEW.homeState,'~') into newValueList;
END IF;
IF (OLD.duration <> NEW.duration ) THEN
    select CONCAT(fieldNameList,'customerfile.duration~') into fieldNameList;
    select CONCAT(oldValueList,OLD.duration,'~') into oldValueList;
    select CONCAT(newValueList,NEW.duration,'~') into newValueList;
END IF;
IF (OLD.orderEmail <> NEW.orderEmail ) THEN
    select CONCAT(fieldNameList,'customerfile.orderEmail~') into fieldNameList;
    select CONCAT(oldValueList,OLD.orderEmail,'~') into oldValueList;
    select CONCAT(newValueList,NEW.orderEmail,'~') into newValueList;
END IF;
IF (OLD.iLeadNumber <> NEW.iLeadNumber ) THEN
    select CONCAT(fieldNameList,'customerfile.iLeadNumber~') into fieldNameList;
    select CONCAT(oldValueList,OLD.iLeadNumber,'~') into oldValueList;
    select CONCAT(newValueList,NEW.iLeadNumber,'~') into newValueList;
END IF;
IF (OLD.visaRequired <> NEW.visaRequired ) THEN
    select CONCAT(fieldNameList,'customerfile.visaRequired~') into fieldNameList;
    select CONCAT(oldValueList,OLD.visaRequired,'~') into oldValueList;
    select CONCAT(newValueList,NEW.visaRequired,'~') into newValueList;
END IF;
IF (OLD.billPayMethod <> NEW.billPayMethod ) THEN
    select CONCAT(fieldNameList,'customerfile.billPayMethod~') into fieldNameList;
    select CONCAT(oldValueList,OLD.billPayMethod,'~') into oldValueList;
    select CONCAT(newValueList,NEW.billPayMethod,'~') into newValueList;
END IF;
IF (OLD.originAddress1 <> NEW.originAddress1 ) THEN
    select CONCAT(fieldNameList,'customerfile.originAddress1~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originAddress1,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originAddress1,'~') into newValueList;
END IF;
IF (OLD.originAddress2 <> NEW.originAddress2 ) THEN
    select CONCAT(fieldNameList,'customerfile.originAddress2~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originAddress2,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originAddress2,'~') into newValueList;
END IF;
IF (OLD.originAddress3 <> NEW.originAddress3 ) THEN
    select CONCAT(fieldNameList,'customerfile.originAddress3~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originAddress3,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originAddress3,'~') into newValueList;
END IF;
IF (OLD.originCity <> NEW.originCity ) THEN
    select CONCAT(fieldNameList,'customerfile.originCity~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originCity,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originCity,'~') into newValueList;
END IF;
IF (OLD.originState <> NEW.originState ) THEN
    select CONCAT(fieldNameList,'customerfile.originState~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originState,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originState,'~') into newValueList;
END IF;
IF (OLD.originZip <> NEW.originZip ) THEN
    select CONCAT(fieldNameList,'customerfile.originZip~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originZip,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originZip,'~') into newValueList;
END IF;
IF (OLD.originCountryCode <> NEW.originCountryCode ) THEN
    select CONCAT(fieldNameList,'customerfile.originCountryCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originCountryCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originCountryCode,'~') into newValueList;
END IF;
IF (OLD.originCountry <> NEW.originCountry ) THEN
    select CONCAT(fieldNameList,'customerfile.originCountry~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originCountry,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originCountry,'~') into newValueList;
END IF;
IF (OLD.destinationAddress1 <> NEW.destinationAddress1 ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationAddress1~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationAddress1,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationAddress1,'~') into newValueList;
END IF;
IF (OLD.destinationAddress2 <> NEW.destinationAddress2 ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationAddress2~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationAddress2,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationAddress2,'~') into newValueList;
END IF;
IF (OLD.destinationAddress3 <> NEW.destinationAddress3 ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationAddress3~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationAddress3,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationAddress3,'~') into newValueList;
END IF;
IF (OLD.destinationCity <> NEW.destinationCity ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationCity~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationCity,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationCity,'~') into newValueList;
END IF;
IF (OLD.destinationState <> NEW.destinationState ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationState~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationState,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationState,'~') into newValueList;
END IF;
IF (OLD.destinationZip <> NEW.destinationZip ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationZip~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationZip,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationZip,'~') into newValueList;
END IF;
IF (OLD.destinationCountryCode <> NEW.destinationCountryCode ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationCountryCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationCountryCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationCountryCode,'~') into newValueList;
END IF;
IF (OLD.destinationCountry <> NEW.destinationCountry ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationCountry~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationCountry,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationCountry,'~') into newValueList;
END IF;
IF (OLD.originDayPhone <> NEW.originDayPhone ) THEN
    select CONCAT(fieldNameList,'customerfile.originDayPhone~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originDayPhone,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originDayPhone,'~') into newValueList;
END IF;
IF (OLD.originHomePhone <> NEW.originHomePhone ) THEN
    select CONCAT(fieldNameList,'customerfile.originHomePhone~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originHomePhone,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originHomePhone,'~') into newValueList;
END IF;
IF (OLD.originFax <> NEW.originFax ) THEN
    select CONCAT(fieldNameList,'customerfile.originFax~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originFax,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originFax,'~') into newValueList;
END IF;
IF (OLD.destinationDayPhone <> NEW.destinationDayPhone ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationDayPhone~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationDayPhone,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationDayPhone,'~') into newValueList;
END IF;
IF (OLD.destinationHomePhone <> NEW.destinationHomePhone ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationHomePhone~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationHomePhone,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationHomePhone,'~') into newValueList;
END IF;
IF (OLD.destinationFax <> NEW.destinationFax ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationFax~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationFax,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationFax,'~') into newValueList;
END IF;
IF (OLD.job <> NEW.job ) THEN
    select CONCAT(fieldNameList,'customerfile.job~') into fieldNameList;
    select CONCAT(oldValueList,OLD.job,'~') into oldValueList;
    select CONCAT(newValueList,NEW.job,'~') into newValueList;
END IF;
IF (OLD.contactEmail <> NEW.contactEmail ) THEN
    select CONCAT(fieldNameList,'customerfile.contactEmail~') into fieldNameList;
    select CONCAT(oldValueList,OLD.contactEmail,'~') into oldValueList;
    select CONCAT(newValueList,NEW.contactEmail,'~') into newValueList;
END IF;
IF (OLD.contract <> NEW.contract ) THEN
    select CONCAT(fieldNameList,'customerfile.contract~') into fieldNameList;
    select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
    select CONCAT(newValueList,NEW.contract,'~') into newValueList;
END IF;
IF (OLD.socialSecurityNumber <> NEW.socialSecurityNumber ) THEN
    select CONCAT(fieldNameList,'customerfile.socialSecurityNumber~') into fieldNameList;
    select CONCAT(oldValueList,OLD.socialSecurityNumber,'~') into oldValueList;
    select CONCAT(newValueList,NEW.socialSecurityNumber,'~') into newValueList;
END IF;
IF (OLD.estimator <> NEW.estimator ) THEN
    select CONCAT(fieldNameList,'customerfile.estimator~') into fieldNameList;
    select CONCAT(oldValueList,OLD.estimator,'~') into oldValueList;
    select CONCAT(newValueList,NEW.estimator,'~') into newValueList;
END IF;
IF (OLD.orderBy <> NEW.orderBy ) THEN
    select CONCAT(fieldNameList,'customerfile.orderBy~') into fieldNameList;
    select CONCAT(oldValueList,OLD.orderBy,'~') into oldValueList;
    select CONCAT(newValueList,NEW.orderBy,'~') into newValueList;
END IF;
IF (OLD.orderPhone <> NEW.orderPhone ) THEN
    select CONCAT(fieldNameList,'customerfile.orderPhone~') into fieldNameList;
    select CONCAT(oldValueList,OLD.orderPhone,'~') into oldValueList;
    select CONCAT(newValueList,NEW.orderPhone,'~') into newValueList;
END IF;
IF (OLD.email <> NEW.email ) THEN
    select CONCAT(fieldNameList,'customerfile.email~') into fieldNameList;
    select CONCAT(oldValueList,OLD.email,'~') into oldValueList;
    select CONCAT(newValueList,NEW.email,'~') into newValueList;
END IF;
IF (OLD.organization <> NEW.organization ) THEN
    select CONCAT(fieldNameList,'customerfile.organization~') into fieldNameList;
    select CONCAT(oldValueList,OLD.organization,'~') into oldValueList;
    select CONCAT(newValueList,NEW.organization,'~') into newValueList;
END IF;
IF (OLD.originAgentContact <> NEW.originAgentContact ) THEN
    select CONCAT(fieldNameList,'customerfile.originAgentContact~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originAgentContact,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originAgentContact,'~') into newValueList;
END IF;
IF (OLD.originAgentPhoneNumber <> NEW.originAgentPhoneNumber ) THEN
    select CONCAT(fieldNameList,'customerfile.originAgentPhoneNumber~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originAgentPhoneNumber,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originAgentPhoneNumber,'~') into newValueList;
END IF;
IF (OLD.surveyObservation <> NEW.surveyObservation ) THEN
    select CONCAT(fieldNameList,'customerfile.surveyObservation~') into fieldNameList;
    select CONCAT(oldValueList,OLD.surveyObservation,'~') into oldValueList;
    select CONCAT(newValueList,NEW.surveyObservation,'~') into newValueList;
END IF;

IF (OLD.originCompany <> NEW.originCompany ) THEN
    select CONCAT(fieldNameList,'customerfile.originCompany~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originCompany,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originCompany,'~') into newValueList;
END IF;
IF (OLD.destinationCompany <> NEW.destinationCompany ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationCompany~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationCompany,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationCompany,'~') into newValueList;
END IF;
IF (OLD.email2 <> NEW.email2 ) THEN
    select CONCAT(fieldNameList,'customerfile.email2~') into fieldNameList;
    select CONCAT(oldValueList,OLD.email2,'~') into oldValueList;
    select CONCAT(newValueList,NEW.email2,'~') into newValueList;
END IF;
IF (OLD.jobFunction <> NEW.jobFunction ) THEN
    select CONCAT(fieldNameList,'customerfile.jobFunction~') into fieldNameList;
    select CONCAT(oldValueList,OLD.jobFunction,'~') into oldValueList;
    select CONCAT(newValueList,NEW.jobFunction,'~') into newValueList;
END IF;
IF (OLD.originalCompanyCode <> NEW.originalCompanyCode ) THEN
    select CONCAT(fieldNameList,'customerfile.originalCompanyCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originalCompanyCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originalCompanyCode,'~') into newValueList;
END IF;

IF ((date_format(OLD.originalCompanyHiringDate,'%Y-%m-%d') <> date_format(NEW.originalCompanyHiringDate,'%Y-%m-%d')) or (date_format(OLD.originalCompanyHiringDate,'%Y-%m-%d') is null and date_format(NEW.originalCompanyHiringDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.originalCompanyHiringDate,'%Y-%m-%d') is not null and date_format(NEW.originalCompanyHiringDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.originalCompanyHiringDate~') into fieldNameList;
    IF(OLD.originalCompanyHiringDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.originalCompanyHiringDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.originalCompanyHiringDate is not null) then
        select CONCAT(oldValueList,OLD.originalCompanyHiringDate,'~') into oldValueList;
     END IF;
      IF(NEW.originalCompanyHiringDate is not null) then
        select CONCAT(newValueList,NEW.originalCompanyHiringDate,'~') into newValueList;
      END IF;
END IF;
IF (OLD.firstInternationalMove <> NEW.firstInternationalMove ) THEN
    select CONCAT(fieldNameList,'customerfile.firstInternationalMove~') into fieldNameList;
    
    IF(OLD.firstInternationalMove = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.firstInternationalMove = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.firstInternationalMove = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.firstInternationalMove = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
END IF;   
    IF (OLD.authorizedBy <> NEW.authorizedBy ) THEN
    select CONCAT(fieldNameList,'customerfile.authorizedBy~') into fieldNameList;
    select CONCAT(oldValueList,OLD.authorizedBy,'~') into oldValueList;
    select CONCAT(newValueList,NEW.authorizedBy,'~') into newValueList;
END IF;
IF (OLD.authorizedEmail <> NEW.authorizedEmail ) THEN
    select CONCAT(fieldNameList,'customerfile.authorizedEmail~') into fieldNameList;
    select CONCAT(oldValueList,OLD.authorizedEmail,'~') into oldValueList;
    select CONCAT(newValueList,NEW.authorizedEmail,'~') into newValueList;
END IF;
IF (OLD.assignmentEndReason <> NEW.assignmentEndReason ) THEN
    select CONCAT(fieldNameList,'customerfile.assignmentEndReason~') into fieldNameList;
    select CONCAT(oldValueList,OLD.assignmentEndReason,'~') into oldValueList;
    select CONCAT(newValueList,NEW.assignmentEndReason,'~') into newValueList;
END IF;
IF (OLD.currentEmploymentName <> NEW.currentEmploymentName ) THEN
    select CONCAT(fieldNameList,'customerfile.currentEmploymentName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.currentEmploymentName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.currentEmploymentName,'~') into newValueList;
END IF;
IF (OLD.authorizedPhone <> NEW.authorizedPhone ) THEN
    select CONCAT(fieldNameList,'customerfile.authorizedPhone~') into fieldNameList;
    select CONCAT(oldValueList,OLD.authorizedPhone,'~') into oldValueList;
    select CONCAT(newValueList,NEW.authorizedPhone,'~') into newValueList;
END IF;
IF (OLD.familySitiation <> NEW.familySitiation ) THEN
    select CONCAT(fieldNameList,'customerfile.familySitiation~') into fieldNameList;
    select CONCAT(oldValueList,OLD.familySitiation,'~') into oldValueList;
    select CONCAT(newValueList,NEW.familySitiation,'~') into newValueList;
END IF;
IF (OLD.currentEmploymentCode <> NEW.currentEmploymentCode ) THEN
    select CONCAT(fieldNameList,'customerfile.currentEmploymentCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.currentEmploymentCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.currentEmploymentCode,'~') into newValueList;
END IF;
IF (OLD.originCompanyCode <> NEW.originCompanyCode ) THEN
    select CONCAT(fieldNameList,'customerfile.originCompanyCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originCompanyCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originCompanyCode,'~') into newValueList;
END IF;
IF (OLD.destinationCompanyCode <> NEW.destinationCompanyCode ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationCompanyCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationCompanyCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationCompanyCode,'~') into newValueList;
END IF;
IF (OLD.localHr <> NEW.localHr ) THEN
    select CONCAT(fieldNameList,'customerfile.localHr~') into fieldNameList;
    select CONCAT(oldValueList,OLD.localHr,'~') into oldValueList;
    select CONCAT(newValueList,NEW.localHr,'~') into newValueList;
END IF;
IF (OLD.localHrPhone <> NEW.localHrPhone ) THEN
    select CONCAT(fieldNameList,'customerfile.localHrPhone~') into fieldNameList;
    select CONCAT(oldValueList,OLD.localHrPhone,'~') into oldValueList;
    select CONCAT(newValueList,NEW.localHrPhone,'~') into newValueList;
END IF;
IF (OLD.localHrEmail <> NEW.localHrEmail ) THEN
    select CONCAT(fieldNameList,'customerfile.localHrEmail~') into fieldNameList;
    select CONCAT(oldValueList,OLD.localHrEmail,'~') into oldValueList;
    select CONCAT(newValueList,NEW.localHrEmail,'~') into newValueList;
END IF;
IF ((date_format(OLD.statusDate,'%Y-%m-%d') <> date_format(NEW.statusDate,'%Y-%m-%d')) or (date_format(OLD.statusDate,'%Y-%m-%d') is null and date_format(NEW.statusDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.statusDate,'%Y-%m-%d') is not null and date_format(NEW.statusDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.statusDate~') into fieldNameList;
    IF(OLD.statusDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.statusDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.statusDate is not null) then
        select CONCAT(oldValueList,OLD.statusDate,'~') into oldValueList;
     END IF;
      IF(NEW.statusDate is not null) then
        select CONCAT(newValueList,NEW.statusDate,'~') into newValueList;
      END IF;
END IF;

IF ((date_format(OLD.survey,'%Y-%m-%d') <> date_format(NEW.survey,'%Y-%m-%d')) or (date_format(OLD.survey,'%Y-%m-%d') is null and date_format(NEW.survey,'%Y-%m-%d') is not null)
  or (date_format(OLD.survey,'%Y-%m-%d') is not null and date_format(NEW.survey,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.survey~') into fieldNameList;
    IF(OLD.survey is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.survey is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.survey is not null) then
        select CONCAT(oldValueList,date_format(OLD.survey,'%Y-%m-%d'),'~') into oldValueList;
     END IF;
      IF(NEW.survey is not null) then
        select CONCAT(newValueList,date_format(NEW.survey,'%Y-%m-%d'),'~') into newValueList;
      END IF;

  update sodashboard set estimateSurvey=NEW.survey where customerfileid=NEW.id;


END IF;

IF ((date_format(OLD.moveDate,'%Y-%m-%d') <> date_format(NEW.moveDate,'%Y-%m-%d')) or (date_format(OLD.moveDate,'%Y-%m-%d') is null and date_format(NEW.moveDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.moveDate,'%Y-%m-%d') is not null and date_format(NEW.moveDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.moveDate~') into fieldNameList;
    
    IF(OLD.moveDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.moveDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.moveDate is not null) then
        select CONCAT(oldValueList,OLD.moveDate,'~') into oldValueList;
     END IF;
      IF(NEW.moveDate is not null) then
        select CONCAT(newValueList,NEW.moveDate,'~') into newValueList;
      END IF;


  END IF;


IF ((date_format(OLD.actualSurveyDate,'%Y-%m-%d') <> date_format(NEW.actualSurveyDate,'%Y-%m-%d')) or (date_format(OLD.actualSurveyDate,'%Y-%m-%d') is null and date_format(NEW.actualSurveyDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.actualSurveyDate,'%Y-%m-%d') is not null and date_format(NEW.actualSurveyDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.actualSurveyDate~') into fieldNameList;
    
    IF(OLD.actualSurveyDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.actualSurveyDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.actualSurveyDate is not null) then
        select CONCAT(oldValueList,OLD.actualSurveyDate,'~') into oldValueList;
     END IF;
      IF(NEW.actualSurveyDate is not null) then
        select CONCAT(newValueList,NEW.actualSurveyDate,'~') into newValueList;
      END IF;

  update sodashboard set actualSurvey=NEW.actualsurveydate where customerfileid=NEW.id;

END IF;

IF (OLD.originDayPhoneExt <> NEW.originDayPhoneExt ) THEN
    select CONCAT(fieldNameList,'customerfile.originDayPhoneExt~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originDayPhoneExt,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originDayPhoneExt,'~') into newValueList;
END IF;
IF (OLD.destinationDayPhoneExt <> NEW.destinationDayPhoneExt ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationDayPhoneExt~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationDayPhoneExt,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationDayPhoneExt,'~') into newValueList;
END IF;

IF ((OLD.statusNumber <> NEW.statusNumber) or (OLD.statusNumber is null and NEW.statusNumber is not null) 
  or (OLD.statusNumber is not null and NEW.statusNumber is null)) THEN
select CONCAT(fieldNameList,'customerfile.statusNumber~') into fieldNameList;
IF(OLD.statusNumber is null) THEN
select CONCAT(oldValueList,"0",'~') into oldValueList;
END IF;
IF(NEW.statusNumber is null) THEN
select CONCAT(newValueList,"0",'~') into newValueList;
END IF;

IF(OLD.statusNumber is not null) then
select CONCAT(oldValueList,OLD.statusNumber,'~') into oldValueList;
END IF;
IF(NEW.statusNumber is not null) then
select CONCAT(newValueList,NEW.statusNumber,'~') into newValueList;
END IF; 
END IF;
IF (OLD.statusReason <> NEW.statusReason ) THEN
    select CONCAT(fieldNameList,'customerfile.statusReason~') into fieldNameList;
    select CONCAT(oldValueList,OLD.statusReason,'~') into oldValueList;
    select CONCAT(newValueList,NEW.statusReason,'~') into newValueList;
END IF;
IF (OLD.customerPortalId <> NEW.customerPortalId ) THEN
    select CONCAT(fieldNameList,'customerfile.customerPortalId~') into fieldNameList;
    select CONCAT(oldValueList,OLD.customerPortalId,'~') into oldValueList;
    select CONCAT(newValueList,NEW.customerPortalId,'~') into newValueList;
END IF;
IF (OLD.portalIdActive <> NEW.portalIdActive ) THEN
    select CONCAT(fieldNameList,'customerfile.portalIdActive~') into fieldNameList;

    IF(OLD.portalIdActive = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.portalIdActive = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.portalIdActive = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.portalIdActive = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF; 
IF (OLD.secondaryEmailFlag <> NEW.secondaryEmailFlag ) THEN
    select CONCAT(fieldNameList,'customerfile.secondaryEmailFlag~') into fieldNameList;

    IF(OLD.secondaryEmailFlag = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.secondaryEmailFlag = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.secondaryEmailFlag = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.secondaryEmailFlag = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF;

IF (OLD.isNetworkRecord <> NEW.isNetworkRecord ) THEN
    select CONCAT(fieldNameList,'customerfile.isNetworkRecord~') into fieldNameList;

    IF(OLD.isNetworkRecord = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.isNetworkRecord = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.isNetworkRecord = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.isNetworkRecord = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF;

IF (OLD.servicePov <> NEW.servicePov ) THEN
    select CONCAT(fieldNameList,'customerfile.servicePov~') into fieldNameList;

    IF(OLD.servicePov = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.servicePov = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.servicePov = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.servicePov = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF;

IF (OLD.serviceAir <> NEW.serviceAir ) THEN
    select CONCAT(fieldNameList,'customerfile.serviceAir~') into fieldNameList;

    IF(OLD.serviceAir = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.serviceAir = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.serviceAir = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.serviceAir = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF;

IF (OLD.serviceSurface <> NEW.serviceSurface ) THEN
    select CONCAT(fieldNameList,'customerfile.serviceSurface~') into fieldNameList;

    IF(OLD.serviceSurface = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.serviceSurface = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.serviceSurface = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.serviceSurface = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF;

IF (OLD.serviceAuto <> NEW.serviceAuto ) THEN
    select CONCAT(fieldNameList,'customerfile.serviceAuto~') into fieldNameList;

    IF(OLD.serviceAuto = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.serviceAuto = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.serviceAuto = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.serviceAuto = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF;

IF (OLD.serviceStorage <> NEW.serviceStorage ) THEN
    select CONCAT(fieldNameList,'customerfile.serviceStorage~') into fieldNameList;

    IF(OLD.serviceStorage = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.serviceStorage = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.serviceStorage = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.serviceStorage = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF;

IF (OLD.servicePet <> NEW.servicePet ) THEN
    select CONCAT(fieldNameList,'customerfile.servicePet~') into fieldNameList;

    IF(OLD.servicePet = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.servicePet = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.servicePet = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.servicePet = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF;

IF (OLD.isUpdater <> NEW.isUpdater ) THEN
    select CONCAT(fieldNameList,'customerfile.isUpdater~') into fieldNameList;

    IF(OLD.isUpdater = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.isUpdater = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.isUpdater = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.isUpdater = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF;

IF (OLD.isNetworkGroup <> NEW.isNetworkGroup ) THEN
    select CONCAT(fieldNameList,'customerfile.isNetworkGroup~') into fieldNameList;

    IF(OLD.isNetworkGroup = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.isNetworkGroup = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.isNetworkGroup = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.isNetworkGroup = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF;


IF (OLD.customerEmployer <> NEW.customerEmployer ) THEN
    select CONCAT(fieldNameList,'customerfile.customerEmployer~') into fieldNameList;
    select CONCAT(oldValueList,OLD.customerEmployer,'~') into oldValueList;
    select CONCAT(newValueList,NEW.customerEmployer,'~') into newValueList;
END IF;
IF (OLD.destinationEmail <> NEW.destinationEmail ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationEmail~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationEmail,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationEmail,'~') into newValueList;
END IF;
IF (OLD.destinationEmail2 <> NEW.destinationEmail2 ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationEmail2~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationEmail2,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationEmail2,'~') into newValueList;
END IF;
IF (OLD.custPartnerLastName <> NEW.custPartnerLastName ) THEN
    select CONCAT(fieldNameList,'customerfile.custPartnerLastName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.custPartnerLastName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.custPartnerLastName,'~') into newValueList;
END IF;
IF (OLD.custPartnerFirstName <> NEW.custPartnerFirstName ) THEN
    select CONCAT(fieldNameList,'customerfile.custPartnerFirstName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.custPartnerFirstName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.custPartnerFirstName,'~') into newValueList;
END IF;
IF (OLD.custPartnerPrefix <> NEW.custPartnerPrefix ) THEN
    select CONCAT(fieldNameList,'customerfile.custPartnerPrefix~') into fieldNameList;
    select CONCAT(oldValueList,OLD.custPartnerPrefix,'~') into oldValueList;
    select CONCAT(newValueList,NEW.custPartnerPrefix,'~') into newValueList;
END IF;
IF (OLD.custPartnerEmail <> NEW.custPartnerEmail ) THEN
    select CONCAT(fieldNameList,'customerfile.custPartnerEmail~') into fieldNameList;
    select CONCAT(oldValueList,OLD.custPartnerEmail,'~') into oldValueList;
    select CONCAT(newValueList,NEW.custPartnerEmail,'~') into newValueList;
END IF;
IF (OLD.originCityCode <> NEW.originCityCode ) THEN
    select CONCAT(fieldNameList,'customerfile.originCityCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originCityCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originCityCode,'~') into newValueList;
END IF;
IF (OLD.destinationCityCode <> NEW.destinationCityCode ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationCityCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationCityCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationCityCode,'~') into newValueList;
END IF;
IF (OLD.vip <> NEW.vip ) THEN
    select CONCAT(fieldNameList,'customerfile.vip~') into fieldNameList;

    IF(OLD.vip = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.vip = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.vip = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.vip = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF;
IF (OLD.destinationContactName <> NEW.destinationContactName ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationContactName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationContactName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationContactName,'~') into newValueList;
END IF;
IF (OLD.destinationContactPhone <> NEW.destinationContactPhone ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationContactPhone~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationContactPhone,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationContactPhone,'~') into newValueList;
END IF;
IF (OLD.destinationContactEmail <> NEW.destinationContactEmail ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationContactEmail~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationContactEmail,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationContactEmail,'~') into newValueList;
END IF;
IF (OLD.destinationOrganization <> NEW.destinationOrganization ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationOrganization~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationOrganization,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationOrganization,'~') into newValueList;
END IF;
IF (OLD.originMobile <> NEW.originMobile ) THEN
    select CONCAT(fieldNameList,'customerfile.originMobile~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originMobile,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originMobile,'~') into newValueList;
END IF;
IF (OLD.destinationMobile <> NEW.destinationMobile ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationMobile~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationMobile,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationMobile,'~') into newValueList;
END IF;
IF (OLD.noCharge <> NEW.noCharge ) THEN
    select CONCAT(fieldNameList,'customerfile.noCharge~') into fieldNameList;
    
    IF(OLD.noCharge = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.noCharge = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.noCharge = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.noCharge = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    
END IF;
IF ((date_format(OLD.billApprovedDate,'%Y-%m-%d') <> date_format(NEW.billApprovedDate,'%Y-%m-%d')) or (date_format(OLD.billApprovedDate,'%Y-%m-%d') is null and date_format(NEW.billApprovedDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.billApprovedDate,'%Y-%m-%d') is not null and date_format(NEW.billApprovedDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.billApprovedDate~') into fieldNameList;

    IF(OLD.billApprovedDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.billApprovedDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.billApprovedDate is not null) then
        select CONCAT(oldValueList,OLD.billApprovedDate,'~') into oldValueList;
     END IF;
      IF(NEW.billApprovedDate is not null) then
        select CONCAT(newValueList,NEW.billApprovedDate,'~') into newValueList;
      END IF;
      
END IF;
IF (OLD.custPartnerContact <> NEW.custPartnerContact ) THEN
    select CONCAT(fieldNameList,'customerfile.custPartnerContact~') into fieldNameList;
    select CONCAT(oldValueList,OLD.custPartnerContact,'~') into oldValueList;
    select CONCAT(newValueList,NEW.custPartnerContact,'~') into newValueList;
END IF;
IF (OLD.custPartnerMobile <> NEW.custPartnerMobile ) THEN
    select CONCAT(fieldNameList,'customerfile.custPartnerMobile~') into fieldNameList;
    select CONCAT(oldValueList,OLD.custPartnerMobile,'~') into oldValueList;
    select CONCAT(newValueList,NEW.custPartnerMobile,'~') into newValueList;
END IF;
IF ((date_format(OLD.bookingDate,'%Y-%m-%d') <> date_format(NEW.bookingDate,'%Y-%m-%d')) or (date_format(OLD.bookingDate,'%Y-%m-%d') is null and date_format(NEW.bookingDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.bookingDate,'%Y-%m-%d') is not null and date_format(NEW.bookingDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.bookingDate~') into fieldNameList;
    
    IF(OLD.bookingDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.bookingDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.bookingDate is not null) then
        select CONCAT(oldValueList,OLD.bookingDate,'~') into oldValueList;
     END IF;
      IF(NEW.bookingDate is not null) then
        select CONCAT(newValueList,NEW.bookingDate,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.initialContactDate,'%Y-%m-%d') <> date_format(NEW.initialContactDate,'%Y-%m-%d')) or (date_format(OLD.initialContactDate,'%Y-%m-%d') is null and date_format(NEW.initialContactDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.initialContactDate,'%Y-%m-%d') is not null and date_format(NEW.initialContactDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.initialContactDate~') into fieldNameList;
    
    IF(OLD.initialContactDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.initialContactDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.initialContactDate is not null) then
        select CONCAT(oldValueList,OLD.initialContactDate,'~') into oldValueList;
     END IF;
      IF(NEW.initialContactDate is not null) then
        select CONCAT(newValueList,NEW.initialContactDate,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.priceSubmissionToAccDate,'%Y-%m-%d') <> date_format(NEW.priceSubmissionToAccDate,'%Y-%m-%d')) or (date_format(OLD.priceSubmissionToAccDate,'%Y-%m-%d') is null and date_format(NEW.priceSubmissionToAccDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.priceSubmissionToAccDate,'%Y-%m-%d') is not null and date_format(NEW.priceSubmissionToAccDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.priceSubmissionToAccDate~') into fieldNameList;
    
    IF(OLD.priceSubmissionToAccDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.priceSubmissionToAccDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.priceSubmissionToAccDate is not null) then
        select CONCAT(oldValueList,OLD.priceSubmissionToAccDate,'~') into oldValueList;
     END IF;
      IF(NEW.priceSubmissionToAccDate is not null) then
        select CONCAT(newValueList,NEW.priceSubmissionToAccDate,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.priceSubmissionToTranfDate,'%Y-%m-%d') <> date_format(NEW.priceSubmissionToTranfDate,'%Y-%m-%d')) or (date_format(OLD.priceSubmissionToTranfDate,'%Y-%m-%d') is null and date_format(NEW.priceSubmissionToTranfDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.priceSubmissionToTranfDate,'%Y-%m-%d') is not null and date_format(NEW.priceSubmissionToTranfDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.priceSubmissionToTranfDate~') into fieldNameList;
    
    IF(OLD.priceSubmissionToTranfDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.priceSubmissionToTranfDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.priceSubmissionToTranfDate is not null) then
        select CONCAT(oldValueList,OLD.priceSubmissionToTranfDate,'~') into oldValueList;
     END IF;
      IF(NEW.priceSubmissionToTranfDate is not null) then
        select CONCAT(newValueList,NEW.priceSubmissionToTranfDate,'~') into newValueList;
      END IF;

END IF;
IF ((date_format(OLD.quoteAcceptenceDate,'%Y-%m-%d') <> date_format(NEW.quoteAcceptenceDate,'%Y-%m-%d')) or (date_format(OLD.quoteAcceptenceDate,'%Y-%m-%d') is null and date_format(NEW.quoteAcceptenceDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.quoteAcceptenceDate,'%Y-%m-%d') is not null and date_format(NEW.quoteAcceptenceDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.quoteAcceptenceDate~') into fieldNameList;
    
    IF(OLD.quoteAcceptenceDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.quoteAcceptenceDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.quoteAcceptenceDate is not null) then
        select CONCAT(oldValueList,OLD.quoteAcceptenceDate,'~') into oldValueList;
     END IF;
      IF(NEW.quoteAcceptenceDate is not null) then
        select CONCAT(newValueList,NEW.quoteAcceptenceDate,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.SystemDate,'%Y-%m-%d') <> date_format(NEW.SystemDate,'%Y-%m-%d')) or (date_format(OLD.SystemDate,'%Y-%m-%d') is null and date_format(NEW.SystemDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.SystemDate,'%Y-%m-%d') is not null and date_format(NEW.SystemDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.SystemDate~') into fieldNameList;
    IF(OLD.SystemDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.SystemDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.SystemDate is not null) then
        select CONCAT(oldValueList,OLD.SystemDate,'~') into oldValueList;
     END IF;
      IF(NEW.SystemDate is not null) then
        select CONCAT(newValueList,NEW.SystemDate,'~') into newValueList;
      END IF;
END IF;
IF (OLD.personPricing <> NEW.personPricing ) THEN
    select CONCAT(fieldNameList,'customerfile.personPricing~') into fieldNameList;
    select CONCAT(oldValueList,OLD.personPricing,'~') into oldValueList;
    select CONCAT(newValueList,NEW.personPricing,'~') into newValueList;
END IF;
IF (OLD.personBilling <> NEW.personBilling ) THEN
    select CONCAT(fieldNameList,'customerfile.personBilling~') into fieldNameList;
    select CONCAT(oldValueList,OLD.personBilling,'~') into oldValueList;
    select CONCAT(newValueList,NEW.personBilling,'~') into newValueList;
END IF;
IF (OLD.personPayable <> NEW.personPayable ) THEN
    select CONCAT(fieldNameList,'customerfile.personPayable~') into fieldNameList;
    select CONCAT(oldValueList,OLD.personPayable,'~') into oldValueList;
    select CONCAT(newValueList,NEW.personPayable,'~') into newValueList;
END IF;
IF (OLD.coordinatorName <> NEW.coordinatorName ) THEN
    select CONCAT(fieldNameList,'customerfile.coordinatorName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.coordinatorName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.coordinatorName,'~') into newValueList;
END IF;
IF (OLD.coordinator <> NEW.coordinator ) THEN
    select CONCAT(fieldNameList,'customerfile.coordinator~') into fieldNameList;
    select CONCAT(oldValueList,OLD.coordinator,'~') into oldValueList;
    select CONCAT(newValueList,NEW.coordinator,'~') into newValueList;
END IF;
IF (OLD.estimatorName <> NEW.estimatorName ) THEN
    select CONCAT(fieldNameList,'customerfile.estimatorName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.estimatorName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.estimatorName,'~') into newValueList;
END IF;
IF (OLD.assignmentType <> NEW.assignmentType ) THEN
    select CONCAT(fieldNameList,'customerfile.assignmentType~') into fieldNameList;
    select CONCAT(oldValueList,OLD.assignmentType,'~') into oldValueList;
    select CONCAT(newValueList,NEW.assignmentType,'~') into newValueList;
END IF;
IF (OLD.privileges <> NEW.privileges ) THEN
    select CONCAT(fieldNameList,'customerfile.privileges~') into fieldNameList;

    IF(OLD.privileges = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.privileges = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.privileges = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.privileges = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
END IF;

IF (OLD.doNotSendQRequest <> NEW.doNotSendQRequest ) THEN
    select CONCAT(fieldNameList,'customerfile.doNotSendQRequest~') into fieldNameList;

    IF(OLD.doNotSendQRequest = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.doNotSendQRequest = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.doNotSendQRequest = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.doNotSendQRequest = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
END IF;


IF (OLD.bookingAgentCode <> NEW.bookingAgentCode ) THEN
    select CONCAT(fieldNameList,'customerfile.bookingAgentCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.bookingAgentCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.bookingAgentCode,'~') into newValueList;
END IF;
IF (OLD.bookingAgentName <> NEW.bookingAgentName ) THEN
    select CONCAT(fieldNameList,'customerfile.bookingAgentName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.bookingAgentName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.bookingAgentName,'~') into newValueList;
END IF;
IF (OLD.rank <> NEW.rank ) THEN
    select CONCAT(fieldNameList,'customerfile.rank~') into fieldNameList;
    select CONCAT(oldValueList,OLD.rank,'~') into oldValueList;
    select CONCAT(newValueList,NEW.rank,'~') into newValueList;
END IF;
IF (OLD.controlFlag <> NEW.controlFlag ) THEN
    select CONCAT(fieldNameList,'customerfile.controlFlag~') into fieldNameList;
    select CONCAT(oldValueList,OLD.controlFlag,'~') into oldValueList;
    select CONCAT(newValueList,NEW.controlFlag,'~') into newValueList;
END IF;
IF (OLD.quotationStatus <> NEW.quotationStatus ) THEN
    select CONCAT(fieldNameList,'customerfile.quotationStatus~') into fieldNameList;
    select CONCAT(oldValueList,OLD.quotationStatus,'~') into oldValueList;
    select CONCAT(newValueList,NEW.quotationStatus,'~') into newValueList;
END IF;
IF (OLD.approvedBy <> NEW.approvedBy ) THEN
    select CONCAT(fieldNameList,'customerfile.approvedBy~') into fieldNameList;
    select CONCAT(oldValueList,OLD.approvedBy,'~') into oldValueList;
    select CONCAT(newValueList,NEW.approvedBy,'~') into newValueList;
END IF;
IF ((date_format(OLD.approvedOn,'%Y-%m-%d') <> date_format(NEW.approvedOn,'%Y-%m-%d')) or (date_format(OLD.approvedOn,'%Y-%m-%d') is null and date_format(NEW.approvedOn,'%Y-%m-%d') is not null)
  or (date_format(OLD.approvedOn,'%Y-%m-%d') is not null and date_format(NEW.approvedOn,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.approvedOn~') into fieldNameList;
    
    IF(OLD.approvedOn is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.approvedOn is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.approvedOn is not null) then
        select CONCAT(oldValueList,OLD.approvedOn,'~') into oldValueList;
     END IF;
      IF(NEW.approvedOn is not null) then
        select CONCAT(newValueList,NEW.approvedOn,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.corporateBenefitsReview,'%Y-%m-%d') <> date_format(NEW.corporateBenefitsReview,'%Y-%m-%d')) or (date_format(OLD.corporateBenefitsReview,'%Y-%m-%d') is null and date_format(NEW.corporateBenefitsReview,'%Y-%m-%d') is not null)
  or (date_format(OLD.corporateBenefitsReview,'%Y-%m-%d') is not null and date_format(NEW.corporateBenefitsReview,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.corporateBenefitsReview~') into fieldNameList;
    
    IF(OLD.corporateBenefitsReview is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.corporateBenefitsReview is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.corporateBenefitsReview is not null) then
        select CONCAT(oldValueList,OLD.corporateBenefitsReview,'~') into oldValueList;
     END IF;
      IF(NEW.corporateBenefitsReview is not null) then
        select CONCAT(newValueList,NEW.corporateBenefitsReview,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.exceptionRequest,'%Y-%m-%d') <> date_format(NEW.exceptionRequest,'%Y-%m-%d')) or (date_format(OLD.exceptionRequest,'%Y-%m-%d') is null and date_format(NEW.exceptionRequest,'%Y-%m-%d') is not null)
  or (date_format(OLD.exceptionRequest,'%Y-%m-%d') is not null and date_format(NEW.exceptionRequest,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.exceptionRequest~') into fieldNameList;

    IF(OLD.exceptionRequest is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.exceptionRequest is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.exceptionRequest is not null) then
        select CONCAT(oldValueList,OLD.exceptionRequest,'~') into oldValueList;
     END IF;
      IF(NEW.exceptionRequest is not null) then
        select CONCAT(newValueList,NEW.exceptionRequest,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.exceptionRequestSentToCorporateContact,'%Y-%m-%d') <> date_format(NEW.exceptionRequestSentToCorporateContact,'%Y-%m-%d')) or (date_format(OLD.exceptionRequestSentToCorporateContact,'%Y-%m-%d') is null and date_format(NEW.exceptionRequestSentToCorporateContact,'%Y-%m-%d') is not null)
  or (date_format(OLD.exceptionRequestSentToCorporateContact,'%Y-%m-%d') is not null and date_format(NEW.exceptionRequestSentToCorporateContact,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.exceptionRequestSentToCorporateContact~') into fieldNameList;
    
    IF(OLD.exceptionRequestSentToCorporateContact is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.exceptionRequestSentToCorporateContact is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.exceptionRequestSentToCorporateContact is not null) then
        select CONCAT(oldValueList,OLD.exceptionRequestSentToCorporateContact,'~') into oldValueList;
     END IF;
      IF(NEW.exceptionRequestSentToCorporateContact is not null) then
        select CONCAT(newValueList,NEW.exceptionRequestSentToCorporateContact,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.exceptionResponse,'%Y-%m-%d') <> date_format(NEW.exceptionResponse,'%Y-%m-%d')) or (date_format(OLD.exceptionResponse,'%Y-%m-%d') is null and date_format(NEW.exceptionResponse,'%Y-%m-%d') is not null)
  or (date_format(OLD.exceptionResponse,'%Y-%m-%d') is not null and date_format(NEW.exceptionResponse,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.exceptionResponse~') into fieldNameList;
    
    IF(OLD.exceptionResponse is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.exceptionResponse is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.exceptionResponse is not null) then
        select CONCAT(oldValueList,OLD.exceptionResponse,'~') into oldValueList;
     END IF;
      IF(NEW.exceptionResponse is not null) then
        select CONCAT(newValueList,NEW.exceptionResponse,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.contractStart,'%Y-%m-%d') <> date_format(NEW.contractStart,'%Y-%m-%d')) or (date_format(OLD.contractStart,'%Y-%m-%d') is null and date_format(NEW.contractStart,'%Y-%m-%d') is not null)
  or (date_format(OLD.contractStart,'%Y-%m-%d') is not null and date_format(NEW.contractStart,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.contractStart~') into fieldNameList;
    
    IF(OLD.contractStart is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.contractStart is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.contractStart is not null) then
        select CONCAT(oldValueList,OLD.contractStart,'~') into oldValueList;
     END IF;
      IF(NEW.contractStart is not null) then
        select CONCAT(newValueList,NEW.contractStart,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.contractEnd,'%Y-%m-%d') <> date_format(NEW.contractEnd,'%Y-%m-%d')) or (date_format(OLD.contractEnd,'%Y-%m-%d') is null and date_format(NEW.contractEnd,'%Y-%m-%d') is not null)
  or (date_format(OLD.contractEnd,'%Y-%m-%d') is not null and date_format(NEW.contractEnd,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.contractEnd~') into fieldNameList;
    
    IF(OLD.contractEnd is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.contractEnd is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.contractEnd is not null) then
        select CONCAT(oldValueList,OLD.contractEnd,'~') into oldValueList;
     END IF;
      IF(NEW.contractEnd is not null) then
        select CONCAT(newValueList,NEW.contractEnd,'~') into newValueList;
      END IF;

END IF;
IF ((date_format(OLD.leadStatusDate,'%Y-%m-%d') <> date_format(NEW.leadStatusDate,'%Y-%m-%d')) or (date_format(OLD.leadStatusDate,'%Y-%m-%d') is null and date_format(NEW.leadStatusDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.leadStatusDate,'%Y-%m-%d') is not null and date_format(NEW.leadStatusDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.leadStatusDate~') into fieldNameList;

    IF(OLD.leadStatusDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.leadStatusDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.leadStatusDate is not null) then
        select CONCAT(oldValueList,OLD.leadStatusDate,'~') into oldValueList;
     END IF;
      IF(NEW.leadStatusDate is not null) then
        select CONCAT(newValueList,NEW.leadStatusDate,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.prefSurveyDate1,'%Y-%m-%d') <> date_format(NEW.prefSurveyDate1,'%Y-%m-%d')) or (date_format(OLD.prefSurveyDate1,'%Y-%m-%d') is null and date_format(NEW.prefSurveyDate1,'%Y-%m-%d') is not null)
  or (date_format(OLD.prefSurveyDate1,'%Y-%m-%d') is not null and date_format(NEW.prefSurveyDate1,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.prefSurveyDate1~') into fieldNameList;
    
    IF(OLD.prefSurveyDate1 is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.prefSurveyDate1 is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.prefSurveyDate1 is not null) then
        select CONCAT(oldValueList,OLD.prefSurveyDate1,'~') into oldValueList;
     END IF;
      IF(NEW.prefSurveyDate1 is not null) then
        select CONCAT(newValueList,NEW.prefSurveyDate1,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.lastRunMMTime,'%Y-%m-%d') <> date_format(NEW.lastRunMMTime,'%Y-%m-%d')) or (date_format(OLD.lastRunMMTime,'%Y-%m-%d') is null and date_format(NEW.lastRunMMTime,'%Y-%m-%d') is not null)
  or (date_format(OLD.lastRunMMTime,'%Y-%m-%d') is not null and date_format(NEW.lastRunMMTime,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.lastRunMMTime~') into fieldNameList;
    
    IF(OLD.lastRunMMTime is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.lastRunMMTime is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.lastRunMMTime is not null) then
        select CONCAT(oldValueList,OLD.lastRunMMTime,'~') into oldValueList;
     END IF;
      IF(NEW.lastRunMMTime is not null) then
        select CONCAT(newValueList,NEW.lastRunMMTime,'~') into newValueList;
      END IF;
      
END IF;
IF ((date_format(OLD.priceSubmissionToBookerDate,'%Y-%m-%d') <> date_format(NEW.priceSubmissionToBookerDate,'%Y-%m-%d')) or (date_format(OLD.priceSubmissionToBookerDate,'%Y-%m-%d') is null and date_format(NEW.priceSubmissionToBookerDate,'%Y-%m-%d') is not null)
  or (date_format(OLD.priceSubmissionToBookerDate,'%Y-%m-%d') is not null and date_format(NEW.priceSubmissionToBookerDate,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.priceSubmissionToBookerDate~') into fieldNameList;
    
    IF(OLD.priceSubmissionToBookerDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.priceSubmissionToBookerDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.priceSubmissionToBookerDate is not null) then
        select CONCAT(oldValueList,OLD.priceSubmissionToBookerDate,'~') into oldValueList;
     END IF;
      IF(NEW.priceSubmissionToBookerDate is not null) then
        select CONCAT(newValueList,NEW.priceSubmissionToBookerDate,'~') into newValueList;
      END IF;
      
END IF; 

IF ((date_format(OLD.prefSurveyDate2,'%Y-%m-%d') <> date_format(NEW.prefSurveyDate2,'%Y-%m-%d')) or (date_format(OLD.prefSurveyDate2,'%Y-%m-%d') is null and date_format(NEW.prefSurveyDate2,'%Y-%m-%d') is not null)
  or (date_format(OLD.prefSurveyDate2,'%Y-%m-%d') is not null and date_format(NEW.prefSurveyDate2,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.prefSurveyDate2~') into fieldNameList;
    
    IF(OLD.prefSurveyDate2 is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.prefSurveyDate2 is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.prefSurveyDate2 is not null) then
        select CONCAT(oldValueList,OLD.prefSurveyDate2,'~') into oldValueList;
     END IF;
      IF(NEW.prefSurveyDate2 is not null) then
        select CONCAT(newValueList,NEW.prefSurveyDate2,'~') into newValueList;
      END IF;
      
END IF;

IF ((date_format(OLD.welcomeEmailOn,'%Y-%m-%d') <> date_format(NEW.welcomeEmailOn,'%Y-%m-%d')) or (date_format(OLD.welcomeEmailOn,'%Y-%m-%d') is null and date_format(NEW.welcomeEmailOn,'%Y-%m-%d') is not null)
  or (date_format(OLD.welcomeEmailOn,'%Y-%m-%d') is not null and date_format(NEW.welcomeEmailOn,'%Y-%m-%d') is null)) THEN
    select CONCAT(fieldNameList,'customerfile.welcomeEmailOn~') into fieldNameList;
    
    IF(OLD.welcomeEmailOn is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.welcomeEmailOn is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.welcomeEmailOn is not null) then
        select CONCAT(oldValueList,OLD.welcomeEmailOn,'~') into oldValueList;
     END IF;
      IF(NEW.welcomeEmailOn is not null) then
        select CONCAT(newValueList,NEW.welcomeEmailOn,'~') into newValueList;
      END IF;
      
END IF;

IF (OLD.accountCode <> NEW.accountCode ) THEN
    select CONCAT(fieldNameList,'customerfile.accountCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.accountCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.accountCode,'~') into newValueList;
END IF;
IF (OLD.accountName <> NEW.accountName ) THEN
    select CONCAT(fieldNameList,'customerfile.accountName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.accountName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.accountName,'~') into newValueList;
END IF;

  IF (OLD.companyDivision <> NEW.companyDivision ) THEN
    select CONCAT(fieldNameList,'customerfile.companyDivision~') into fieldNameList;
    select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
    select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
END IF;

IF (OLD.contactNameExt <> NEW.contactNameExt ) THEN
    select CONCAT(fieldNameList,'customerfile.contactNameExt~') into fieldNameList;
    select CONCAT(oldValueList,OLD.contactNameExt,'~') into oldValueList;
    select CONCAT(newValueList,NEW.contactNameExt,'~') into newValueList;
END IF;

IF (OLD.destinationContactNameExt <> NEW.destinationContactNameExt ) THEN
    select CONCAT(fieldNameList,'customerfile.destinationContactNameExt~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destinationContactNameExt,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destinationContactNameExt,'~') into newValueList;
END IF;

IF (OLD.salesStatus <> NEW.salesStatus ) THEN
    select CONCAT(fieldNameList,'customerfile.salesStatus~') into fieldNameList;
    select CONCAT(oldValueList,OLD.salesStatus,'~') into oldValueList;
    select CONCAT(newValueList,NEW.salesStatus,'~') into newValueList;
END IF;
IF (OLD.originAgentCode <> NEW.originAgentCode ) THEN
    select CONCAT(fieldNameList,'customerfile.originAgentCode~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originAgentCode,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originAgentCode,'~') into newValueList;
END IF;

IF (OLD.originAgentName <> NEW.originAgentName ) THEN
    select CONCAT(fieldNameList,'customerfile.originAgentName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originAgentName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originAgentName,'~') into newValueList;
END IF;
IF (OLD.originAgentEmail <> NEW.originAgentEmail ) THEN
    select CONCAT(fieldNameList,'customerfile.originAgentEmail~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originAgentEmail,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originAgentEmail,'~') into newValueList;
END IF;
IF (OLD.costCenter <> NEW.costCenter ) THEN
    select CONCAT(fieldNameList,'customerfile.costCenter~') into fieldNameList;
    select CONCAT(oldValueList,OLD.costCenter,'~') into oldValueList;
    select CONCAT(newValueList,NEW.costCenter,'~') into newValueList;
END IF;
IF (OLD.partnerEntitle <> NEW.partnerEntitle ) THEN
    select CONCAT(fieldNameList,'customerfile.partnerEntitle~') into fieldNameList;
    select CONCAT(oldValueList,OLD.partnerEntitle,'~') into oldValueList;
    select CONCAT(newValueList,NEW.partnerEntitle,'~') into newValueList;
END IF;
IF (OLD.contractType <> NEW.contractType ) THEN
    select CONCAT(fieldNameList,'customerfile.contractType~') into fieldNameList;
    select CONCAT(oldValueList,OLD.contractType,'~') into oldValueList;
    select CONCAT(newValueList,NEW.contractType,'~') into newValueList;
END IF;
IF (OLD.emailSentDate <> NEW.emailSentDate ) THEN
    select CONCAT(fieldNameList,'customerfile.emailSentDate~') into fieldNameList;
    select CONCAT(oldValueList,OLD.emailSentDate,'~') into oldValueList;
    select CONCAT(newValueList,NEW.emailSentDate,'~') into newValueList;
END IF;
IF (OLD.qfStatusReason <> NEW.qfStatusReason ) THEN
    select CONCAT(fieldNameList,'customerfile.qfStatusReason~') into fieldNameList;
    select CONCAT(oldValueList,OLD.qfStatusReason,'~') into oldValueList;
    select CONCAT(newValueList,NEW.qfStatusReason,'~') into newValueList;
END IF;
IF (OLD.noInsurance <> NEW.noInsurance ) THEN
    select CONCAT(fieldNameList,'customerfile.noInsurance~') into fieldNameList;
    select CONCAT(oldValueList,OLD.noInsurance,'~') into oldValueList;
    select CONCAT(newValueList,NEW.noInsurance,'~') into newValueList;
END IF;
IF (OLD.surveyEmailLanguage <> NEW.surveyEmailLanguage ) THEN
    select CONCAT(fieldNameList,'customerfile.surveyEmailLanguage~') into fieldNameList;
    select CONCAT(oldValueList,OLD.surveyEmailLanguage,'~') into oldValueList;
    select CONCAT(newValueList,NEW.surveyEmailLanguage,'~') into newValueList;
END IF;
IF (OLD.cportalEmailLanguage <> NEW.cportalEmailLanguage ) THEN
    select CONCAT(fieldNameList,'customerfile.cportalEmailLanguage~') into fieldNameList;
    select CONCAT(oldValueList,OLD.cportalEmailLanguage,'~') into oldValueList;
    select CONCAT(newValueList,NEW.cportalEmailLanguage,'~') into newValueList;
END IF;
IF (OLD.prefSurveyTime1 <> NEW.prefSurveyTime1 ) THEN
    select CONCAT(fieldNameList,'customerfile.prefSurveyTime1~') into fieldNameList;
    select CONCAT(oldValueList,OLD.prefSurveyTime1,'~') into oldValueList;
    select CONCAT(newValueList,NEW.prefSurveyTime1,'~') into newValueList;
END IF;
IF (OLD.prefSurveyTime2 <> NEW.prefSurveyTime2 ) THEN
    select CONCAT(fieldNameList,'customerfile.prefSurveyTime2~') into fieldNameList;
    select CONCAT(oldValueList,OLD.prefSurveyTime2,'~') into oldValueList;
    select CONCAT(newValueList,NEW.prefSurveyTime2,'~') into newValueList;
END IF;
IF (OLD.prefSurveyTime3 <> NEW.prefSurveyTime3 ) THEN
    select CONCAT(fieldNameList,'customerfile.prefSurveyTime3~') into fieldNameList;
    select CONCAT(oldValueList,OLD.prefSurveyTime3,'~') into oldValueList;
    select CONCAT(newValueList,NEW.prefSurveyTime3,'~') into newValueList;
END IF;
IF (OLD.prefSurveyTime4 <> NEW.prefSurveyTime4 ) THEN
    select CONCAT(fieldNameList,'customerfile.prefSurveyTime4~') into fieldNameList;
    select CONCAT(oldValueList,OLD.prefSurveyTime4,'~') into oldValueList;
    select CONCAT(newValueList,NEW.prefSurveyTime4,'~') into newValueList;
END IF;
IF (OLD.moveType <> NEW.moveType ) THEN
    select CONCAT(fieldNameList,'customerfile.moveType~') into fieldNameList;
    select CONCAT(oldValueList,OLD.moveType,'~') into oldValueList;
    select CONCAT(newValueList,NEW.moveType,'~') into newValueList;
END IF;
IF (OLD.internalBillingPerson <> NEW.internalBillingPerson ) THEN
    select CONCAT(fieldNameList,'customerfile.internalBillingPerson~') into fieldNameList;
    select CONCAT(oldValueList,OLD.internalBillingPerson,'~') into oldValueList;
    select CONCAT(newValueList,NEW.internalBillingPerson,'~') into newValueList;
END IF; 
IF (OLD.originalCompanyName <> NEW.originalCompanyName ) THEN
    select CONCAT(fieldNameList,'customerfile.originalCompanyName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originalCompanyName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originalCompanyName,'~') into newValueList;
END IF;
IF (OLD.surveyType <> NEW.surveyType ) THEN
    select CONCAT(fieldNameList,'customerfile.surveyType~') into fieldNameList;
    select CONCAT(oldValueList,OLD.surveyType,'~') into oldValueList;
    select CONCAT(newValueList,NEW.surveyType,'~') into newValueList;
END IF;
IF (OLD.serviceRelo <> NEW.serviceRelo ) THEN
    select CONCAT(fieldNameList,'customerfile.serviceRelo~') into fieldNameList;
    select CONCAT(oldValueList,OLD.serviceRelo,'~') into oldValueList;
    select CONCAT(newValueList,NEW.serviceRelo,'~') into newValueList;
END IF;
IF (OLD.bookingAgentSequenceNumber <> NEW.bookingAgentSequenceNumber ) THEN
    select CONCAT(fieldNameList,'customerfile.bookingAgentSequenceNumber~') into fieldNameList;
    select CONCAT(oldValueList,OLD.bookingAgentSequenceNumber,'~') into oldValueList;
    select CONCAT(newValueList,NEW.bookingAgentSequenceNumber,'~') into newValueList;
END IF;

IF (OLD.backToBackAssignment <> NEW.backToBackAssignment ) THEN
    select CONCAT(fieldNameList,'customerfile.backToBackAssignment~') into fieldNameList;
    select CONCAT(oldValueList,OLD.backToBackAssignment,'~') into oldValueList;
    select CONCAT(newValueList,NEW.backToBackAssignment,'~') into newValueList;
END IF;


IF (OLD.taskId <> NEW.taskId ) THEN
    select CONCAT(fieldNameList,'customerfile.taskId~') into fieldNameList;
    select CONCAT(oldValueList,OLD.taskId,'~') into oldValueList;
    select CONCAT(newValueList,NEW.taskId,'~') into newValueList;
END IF;

IF (OLD.destPreferredContactTime <> NEW.destPreferredContactTime ) THEN
    select CONCAT(fieldNameList,'customerfile.destPreferredContactTime~') into fieldNameList;
    select CONCAT(oldValueList,OLD.destPreferredContactTime,'~') into oldValueList;
    select CONCAT(newValueList,NEW.destPreferredContactTime,'~') into newValueList;
END IF;
IF (OLD.originPreferredContactTime <> NEW.originPreferredContactTime ) THEN
    select CONCAT(fieldNameList,'customerfile.originPreferredContactTime~') into fieldNameList;
    select CONCAT(oldValueList,OLD.originPreferredContactTime,'~') into oldValueList;
    select CONCAT(newValueList,NEW.originPreferredContactTime,'~') into newValueList;
END IF;

IF (OLD.registrationNumber <> NEW.registrationNumber ) THEN
    select CONCAT(fieldNameList,'customerfile.registrationNumber~') into fieldNameList;
    select CONCAT(oldValueList,OLD.registrationNumber,'~') into oldValueList;
    select CONCAT(newValueList,NEW.registrationNumber,'~') into newValueList;
END IF;

if(NEW.customerEmployer<>NEW.billtoname) then
set NEW.customerEmployer=NEW.billtoname;
end if;



  if(date_format(OLD.updatedOn,'%Y-%m-%d %H:%i:%s') <> date_format(NEW.updatedOn,'%Y-%m-%d %H:%i:%s'))then
CALL add_tblHistory (OLD.id,"customerfile", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
end if;
if(date_format(OLD.updatedOn,'%Y-%m-%d %H:%i:%s') = date_format(NEW.updatedOn,'%Y-%m-%d %H:%i:%s'))then
CALL add_tblHistory (OLD.id,"customerfile", fieldNameList, oldValueList, newValueList, 'System', OLD.corpID, now());
end if;
END
$$


