package com.trilasoft.app.webapp.action;

import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;

import com.trilasoft.app.service.CorpComponentPermissionManager;
import com.trilasoft.app.service.DataCatalogManager;

public class CorpComponentPermissionUtil {
	
	private CorpComponentPermissionManager corpComponentPermissionManager;
	
	public void setCorpComponentPermissionManager(CorpComponentPermissionManager corpComponentPermissionManager) {
		this.corpComponentPermissionManager = corpComponentPermissionManager;
	}
	
	public boolean getFieldVisibilityForComponent(String userCorpID, String componentId){
		return corpComponentPermissionManager.getFieldVisibilityForComponent(userCorpID, componentId);
	}

}
