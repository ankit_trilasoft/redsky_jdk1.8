package com.trilasoft.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.*;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "surveyresult")
public class CustomerServiceSurvey {
	private Long id;
	private Long soID;
	private Long questionID;
	private Long answerID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String corpID;
	//private ServiceOrder serviceOrder;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("sOID", soID)
				.append("questionID", questionID).append("answerID", answerID)
				.append("createdBy", createdBy).append("createdOn", createdOn)
				.append("updatedBy", updatedBy).append("updatedOn", updatedOn)
				.append("corpID", corpID).toString();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	@Column
	public Long getsoID() {
		return soID;
	}
	@Column
	public Long getQuestionID() {
		return questionID;
	}
	@Column
	public Long getAnswerID() {
		return answerID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setsoID(Long sOID) {
		this.soID = sOID;
	}
	public void setQuestionID(Long questionID) {
		this.questionID = questionID;
	}
	public void setAnswerID(Long answerID) {
		this.answerID = answerID;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((answerID == null) ? 0 : answerID.hashCode());
		result = prime * result + ((corpID == null) ? 0 : corpID.hashCode());
		result = prime * result
				+ ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result
				+ ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((questionID == null) ? 0 : questionID.hashCode());
		result = prime * result + ((soID == null) ? 0 : soID.hashCode());
		result = prime * result
				+ ((updatedBy == null) ? 0 : updatedBy.hashCode());
		result = prime * result
				+ ((updatedOn == null) ? 0 : updatedOn.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerServiceSurvey other = (CustomerServiceSurvey) obj;
		if (answerID == null) {
			if (other.answerID != null)
				return false;
		} else if (!answerID.equals(other.answerID))
			return false;
		if (corpID == null) {
			if (other.corpID != null)
				return false;
		} else if (!corpID.equals(other.corpID))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (questionID == null) {
			if (other.questionID != null)
				return false;
		} else if (!questionID.equals(other.questionID))
			return false;
		if (soID == null) {
			if (other.soID != null)
				return false;
		} else if (!soID.equals(other.soID))
			return false;
		if (updatedBy == null) {
			if (other.updatedBy != null)
				return false;
		} else if (!updatedBy.equals(other.updatedBy))
			return false;
		if (updatedOn == null) {
			if (other.updatedOn != null)
				return false;
		} else if (!updatedOn.equals(other.updatedOn))
			return false;
		return true;
	}
	/*@ManyToOne
	@JoinColumn(name="sOID", nullable=false, updatable=false,
			referencedColumnName="id")
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}*/
	
	
	
	

}
