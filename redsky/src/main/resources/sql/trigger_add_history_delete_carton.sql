


DELIMITER $$
CREATE TRIGGER trigger_add_history_delete_carton BEFORE DELETE ON carton
FOR EACH ROW BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

   IF (OLD.type <> '') THEN
       select CONCAT(fieldNameList,'carton.type~') into fieldNameList;
       select CONCAT(oldValueList,OLD.type,'~') into oldValueList;
   END IF;
   IF (OLD.length <> '' or OLD.length is not null) THEN
       select CONCAT(fieldNameList,'carton.length~') into fieldNameList;
       select CONCAT(oldValueList,OLD.length,'~') into oldValueList;
   END IF;
   IF (OLD.container <> '') THEN
       select CONCAT(fieldNameList,'carton.container~') into fieldNameList;
       select CONCAT(oldValueList,OLD.container,'~') into oldValueList;
   END IF;
  
   IF (OLD.cntnrNumber <> '') THEN
       select CONCAT(fieldNameList,'carton.cntnrNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cntnrNumber,'~') into oldValueList;
   END IF;
   IF (OLD.shipNumber <> '' ) THEN
       select CONCAT(fieldNameList,'carton.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
   END IF;
   IF (OLD.sequenceNumber <> '' ) THEN
       select CONCAT(fieldNameList,'carton.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
   END IF;
   IF (OLD.volume <> '' or OLD.volume is not null) THEN
       select CONCAT(fieldNameList,'carton.volume~') into fieldNameList;
       select CONCAT(oldValueList,OLD.volume,'~') into oldValueList;
   END IF;
   IF (OLD.width <> '' or OLD.width is not null) THEN
       select CONCAT(fieldNameList,'carton.width~') into fieldNameList;
       select CONCAT(oldValueList,OLD.width,'~') into oldValueList;
   END IF;
    IF (OLD.idNumber <> '') THEN
       select CONCAT(fieldNameList,'carton.idNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.idNumber,'~') into oldValueList;
   END IF;
   IF (OLD.unit1 <> '' ) THEN
       select CONCAT(fieldNameList,'carton.unit1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit1,'~') into oldValueList;
   END IF;
   IF (OLD.unit2 <> '') THEN
       select CONCAT(fieldNameList,'carton.unit2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit2,'~') into oldValueList;
   END IF;
   IF (OLD.emptyContWeight <> '' or OLD.emptyContWeight is not null) THEN
       select CONCAT(fieldNameList,'carton.emptyContWeight~') into fieldNameList;
       select CONCAT(oldValueList,OLD.emptyContWeight,'~') into oldValueList;
   END IF;
   IF (OLD.grossWeight <> '' or OLD.grossWeight is not null) THEN
       select CONCAT(fieldNameList,'carton.grossWeight~') into fieldNameList;
       select CONCAT(oldValueList,OLD.grossWeight,'~') into oldValueList;
   END IF;
   IF (OLD.netWeight <> '' or OLD.netWeight is not null) THEN
       select CONCAT(fieldNameList,'carton.netWeight~') into fieldNameList;
       select CONCAT(oldValueList,OLD.netWeight,'~') into oldValueList;
   END IF;
    IF (OLD.pieces <> '' or OLD.pieces is not null) THEN
       select CONCAT(fieldNameList,'carton.pieces~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pieces,'~') into oldValueList;
   END IF;
   IF (OLD.totalLine <> '' ) THEN
       select CONCAT(fieldNameList,'carton.totalLine~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalLine,'~') into oldValueList;
   END IF;
   IF (OLD.unit3 <> '') THEN
       select CONCAT(fieldNameList,'carton.unit3~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit3,'~') into oldValueList;
   END IF;
    IF (OLD.cartonType <> '') THEN
       select CONCAT(fieldNameList,'carton.cartonType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cartonType,'~') into oldValueList;
   END IF;

    IF (OLD.crateit <> '' or OLD.crateit is not null) THEN
       select CONCAT(fieldNameList,'carton.crateit~') into fieldNameList;
       select CONCAT(oldValueList,OLD.crateit,'~') into oldValueList;
   END IF;
    IF (OLD.height <> '' or OLD.height is not null) THEN
       select CONCAT(fieldNameList,'carton.height~') into fieldNameList;
       select CONCAT(oldValueList,OLD.height,'~') into oldValueList;
   END IF;
   IF (OLD.numberOfCarton <> '') THEN
       select CONCAT(fieldNameList,'carton.numberOfCarton~') into fieldNameList;
       select CONCAT(oldValueList,OLD.numberOfCarton,'~') into oldValueList;
   END IF;

    IF (OLD.uncrate <> '' or OLD.uncrate is not null) THEN
       select CONCAT(fieldNameList,'carton.uncrate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.uncrate,'~') into oldValueList;
   END IF;
    IF (OLD.totalVolume <> '' or OLD.totalVolume is not null) THEN
       select CONCAT(fieldNameList,'carton.totalVolume~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalVolume,'~') into oldValueList;
   END IF;
    IF (OLD.totalGrossWeight <> '' or OLD.totalGrossWeight is not null) THEN
       select CONCAT(fieldNameList,'carton.totalGrossWeight~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalGrossWeight,'~') into oldValueList;
   END IF;
    IF (OLD.totalNetWeight <> '' or OLD.totalNetWeight is not null) THEN
       select CONCAT(fieldNameList,'carton.totalNetWeight~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalNetWeight,'~') into oldValueList;
   END IF;
    IF (OLD.density <> '' or OLD.density is not null) THEN
       select CONCAT(fieldNameList,'carton.density~') into fieldNameList;
       select CONCAT(oldValueList,OLD.density,'~') into oldValueList;
   END IF;
    IF (OLD.serviceOrderId <> '' or OLD.serviceOrderId is not null) THEN
       select CONCAT(fieldNameList,'carton.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
   END IF;
  IF (OLD.totalPieces <> '' or OLD.totalPieces is not null) THEN
       select CONCAT(fieldNameList,'carton.totalPieces~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalPieces,'~') into oldValueList;
   END IF;
 IF (OLD.grossWeightKilo <> '' or OLD.grossWeightKilo is not null) THEN
       select CONCAT(fieldNameList,'carton.grossWeightKilo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.grossWeightKilo,'~') into oldValueList;
   END IF;
    IF (OLD.emptyContWeightKilo <> '' or OLD.emptyContWeightKilo is not null) THEN
       select CONCAT(fieldNameList,'carton.emptyContWeightKilo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.emptyContWeightKilo,'~') into oldValueList;
   END IF;
    IF (OLD.netWeightKilo <> '' or OLD.netWeightKilo is not null) THEN
       select CONCAT(fieldNameList,'carton.netWeightKilo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.netWeightKilo,'~') into oldValueList;
   END IF;
    IF (OLD.volumeCbm <> '' or OLD.volumeCbm is not null) THEN
       select CONCAT(fieldNameList,'carton.volumeCbm~') into fieldNameList;
       select CONCAT(oldValueList,OLD.volumeCbm,'~') into oldValueList;
   END IF;
    IF (OLD.densityMetric <> '' or OLD.densityMetric is not null) THEN
       select CONCAT(fieldNameList,'carton.densityMetric~') into fieldNameList;
       select CONCAT(oldValueList,OLD.densityMetric,'~') into oldValueList;
   END IF;
    IF (OLD.totalTareWeight <> '' or OLD.totalTareWeight is not null) THEN
       select CONCAT(fieldNameList,'carton.totalTareWeight~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalTareWeight,'~') into oldValueList;
   END IF;
   CALL add_tblHistory (OLD.serviceOrderId,"carton", fieldNameList, oldValueList, newValueList, OLD.updatedby, OLD.corpID, now());
END
$$
