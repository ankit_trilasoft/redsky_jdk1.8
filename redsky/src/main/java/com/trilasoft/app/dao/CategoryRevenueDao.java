package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CategoryRevenue;

public interface CategoryRevenueDao extends GenericDao<CategoryRevenue,Long>{
	
	public List findById(Long id);
	public List findMaximumId(); 

	public List searchCategoryRevenue(String secondSet, String totalCrew, String numberOfCrew1, String numberOfCrew2, String numberOfCrew3, String numberOfCrew4);

}
