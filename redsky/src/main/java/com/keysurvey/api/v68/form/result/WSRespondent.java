/**
 * WSRespondent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.result;

public class WSRespondent  extends com.keysurvey.api.v68.WSObject  implements java.io.Serializable {
    private boolean anonym;

    private java.lang.String code;

    private boolean deleted;

    private java.lang.String email;

    private long formId;

    private java.lang.String ipAddress;

    private long respondentId;

    private boolean resubmit;

    private long score;

    private com.keysurvey.api.v68.form.result.WSRespondentStatus status;

    private java.util.Calendar submitDate;

    private java.util.Calendar submitStartDate;

    private boolean test;

    public WSRespondent() {
    }

    public WSRespondent(
           long versionId,
           boolean anonym,
           java.lang.String code,
           boolean deleted,
           java.lang.String email,
           long formId,
           java.lang.String ipAddress,
           long respondentId,
           boolean resubmit,
           long score,
           com.keysurvey.api.v68.form.result.WSRespondentStatus status,
           java.util.Calendar submitDate,
           java.util.Calendar submitStartDate,
           boolean test) {
        super(
            versionId);
        this.anonym = anonym;
        this.code = code;
        this.deleted = deleted;
        this.email = email;
        this.formId = formId;
        this.ipAddress = ipAddress;
        this.respondentId = respondentId;
        this.resubmit = resubmit;
        this.score = score;
        this.status = status;
        this.submitDate = submitDate;
        this.submitStartDate = submitStartDate;
        this.test = test;
    }


    /**
     * Gets the anonym value for this WSRespondent.
     * 
     * @return anonym
     */
    public boolean isAnonym() {
        return anonym;
    }


    /**
     * Sets the anonym value for this WSRespondent.
     * 
     * @param anonym
     */
    public void setAnonym(boolean anonym) {
        this.anonym = anonym;
    }


    /**
     * Gets the code value for this WSRespondent.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this WSRespondent.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the deleted value for this WSRespondent.
     * 
     * @return deleted
     */
    public boolean isDeleted() {
        return deleted;
    }


    /**
     * Sets the deleted value for this WSRespondent.
     * 
     * @param deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }


    /**
     * Gets the email value for this WSRespondent.
     * 
     * @return email
     */
    public java.lang.String getEmail() {
        return email;
    }


    /**
     * Sets the email value for this WSRespondent.
     * 
     * @param email
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    }


    /**
     * Gets the formId value for this WSRespondent.
     * 
     * @return formId
     */
    public long getFormId() {
        return formId;
    }


    /**
     * Sets the formId value for this WSRespondent.
     * 
     * @param formId
     */
    public void setFormId(long formId) {
        this.formId = formId;
    }


    /**
     * Gets the ipAddress value for this WSRespondent.
     * 
     * @return ipAddress
     */
    public java.lang.String getIpAddress() {
        return ipAddress;
    }


    /**
     * Sets the ipAddress value for this WSRespondent.
     * 
     * @param ipAddress
     */
    public void setIpAddress(java.lang.String ipAddress) {
        this.ipAddress = ipAddress;
    }


    /**
     * Gets the respondentId value for this WSRespondent.
     * 
     * @return respondentId
     */
    public long getRespondentId() {
        return respondentId;
    }


    /**
     * Sets the respondentId value for this WSRespondent.
     * 
     * @param respondentId
     */
    public void setRespondentId(long respondentId) {
        this.respondentId = respondentId;
    }


    /**
     * Gets the resubmit value for this WSRespondent.
     * 
     * @return resubmit
     */
    public boolean isResubmit() {
        return resubmit;
    }


    /**
     * Sets the resubmit value for this WSRespondent.
     * 
     * @param resubmit
     */
    public void setResubmit(boolean resubmit) {
        this.resubmit = resubmit;
    }


    /**
     * Gets the score value for this WSRespondent.
     * 
     * @return score
     */
    public long getScore() {
        return score;
    }


    /**
     * Sets the score value for this WSRespondent.
     * 
     * @param score
     */
    public void setScore(long score) {
        this.score = score;
    }


    /**
     * Gets the status value for this WSRespondent.
     * 
     * @return status
     */
    public com.keysurvey.api.v68.form.result.WSRespondentStatus getStatus() {
        return status;
    }


    /**
     * Sets the status value for this WSRespondent.
     * 
     * @param status
     */
    public void setStatus(com.keysurvey.api.v68.form.result.WSRespondentStatus status) {
        this.status = status;
    }


    /**
     * Gets the submitDate value for this WSRespondent.
     * 
     * @return submitDate
     */
    public java.util.Calendar getSubmitDate() {
        return submitDate;
    }


    /**
     * Sets the submitDate value for this WSRespondent.
     * 
     * @param submitDate
     */
    public void setSubmitDate(java.util.Calendar submitDate) {
        this.submitDate = submitDate;
    }


    /**
     * Gets the submitStartDate value for this WSRespondent.
     * 
     * @return submitStartDate
     */
    public java.util.Calendar getSubmitStartDate() {
        return submitStartDate;
    }


    /**
     * Sets the submitStartDate value for this WSRespondent.
     * 
     * @param submitStartDate
     */
    public void setSubmitStartDate(java.util.Calendar submitStartDate) {
        this.submitStartDate = submitStartDate;
    }


    /**
     * Gets the test value for this WSRespondent.
     * 
     * @return test
     */
    public boolean isTest() {
        return test;
    }


    /**
     * Sets the test value for this WSRespondent.
     * 
     * @param test
     */
    public void setTest(boolean test) {
        this.test = test;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSRespondent)) return false;
        WSRespondent other = (WSRespondent) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.anonym == other.isAnonym() &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            this.deleted == other.isDeleted() &&
            ((this.email==null && other.getEmail()==null) || 
             (this.email!=null &&
              this.email.equals(other.getEmail()))) &&
            this.formId == other.getFormId() &&
            ((this.ipAddress==null && other.getIpAddress()==null) || 
             (this.ipAddress!=null &&
              this.ipAddress.equals(other.getIpAddress()))) &&
            this.respondentId == other.getRespondentId() &&
            this.resubmit == other.isResubmit() &&
            this.score == other.getScore() &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.submitDate==null && other.getSubmitDate()==null) || 
             (this.submitDate!=null &&
              this.submitDate.equals(other.getSubmitDate()))) &&
            ((this.submitStartDate==null && other.getSubmitStartDate()==null) || 
             (this.submitStartDate!=null &&
              this.submitStartDate.equals(other.getSubmitStartDate()))) &&
            this.test == other.isTest();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += (isAnonym() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        _hashCode += (isDeleted() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getEmail() != null) {
            _hashCode += getEmail().hashCode();
        }
        _hashCode += new Long(getFormId()).hashCode();
        if (getIpAddress() != null) {
            _hashCode += getIpAddress().hashCode();
        }
        _hashCode += new Long(getRespondentId()).hashCode();
        _hashCode += (isResubmit() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += new Long(getScore()).hashCode();
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getSubmitDate() != null) {
            _hashCode += getSubmitDate().hashCode();
        }
        if (getSubmitStartDate() != null) {
            _hashCode += getSubmitStartDate().hashCode();
        }
        _hashCode += (isTest() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSRespondent.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://result.form.v68.api.keysurvey.com", "WSRespondent"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anonym");
        elemField.setXmlName(new javax.xml.namespace.QName("", "anonym"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deleted");
        elemField.setXmlName(new javax.xml.namespace.QName("", "deleted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email");
        elemField.setXmlName(new javax.xml.namespace.QName("", "email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("formId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "formId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ipAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ipAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respondentId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "respondentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resubmit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resubmit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("score");
        elemField.setXmlName(new javax.xml.namespace.QName("", "score"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://result.form.v68.api.keysurvey.com", "WSRespondentStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("submitDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "submitDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("submitStartDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "submitStartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("test");
        elemField.setXmlName(new javax.xml.namespace.QName("", "test"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
