<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
  
<head>   
    <title><fmt:message key="defaultAccountLineList.title"/></title>   
    <meta name="heading" content="<fmt:message key='defaultAccountLineList.heading'/>"/>
    <style type="text/css">


/* collapse */


</style>

<style>
 span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;
!margin-bottom:1px;
margin-top:0px;
padding:2px 0px;
!padding-left:50px;
text-align:right;
width:100%;
!width:94%;
}

.table tbody td a {
background:transparent none repeat scroll 0% 50%;
cursor:pointer;
text-decoration:underline;
}
</style>
<script language="javascript" type="text/javascript"> 
   //function checkReadOnly(){
     //  var chkCorpId= '${objContract.owner}';
      // var corpId='${OrgId}'; 
      // if(corpId!=chkCorpId){
      // 		document.forms['defaultAccountLineForm'].elements['copy'].disabled = true;
      // }
  // }
   </script>
</head>

<s:form name ="defaultAccountLineForm" id="defaultAccountLineForm" action="searchDefaultAccountLines" method="post"  validate="true">
<%-- <div id="Layer1"  style="width:100%">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
	<table class="table" cellspacing="0" cellpadding="0" style="width:99%">
		<thead>
			<tr>
			<th><fmt:message key="defaultAccountLine.contract" /></th>
			<th><fmt:message key="defaultAccountLine.jobType" /></th>
			<th><fmt:message key="defaultAccountLine.route" /></th>
			<th><fmt:message key="defaultAccountLine.mode" /></th>
			<th><fmt:message key="defaultAccountLine.serviceType" /></th>
			<%-- <th><fmt:message key="defaultAccountLine.categories" /></th>
			<th><fmt:message key="defaultAccountLine.billToCode" /></th>
			</tr>
		</thead>
		<tbody>
			<tr>
			    <td width=""><s:select name="defaultAccountLine.contract" list="%{contractList}" cssStyle="width:185px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();"/></td>
				<td width=""><s:select name="defaultAccountLine.jobType" list="%{job}" cssStyle="width:180px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();"/></td>
				<td width=""><s:select name="defaultAccountLine.route" list="%{routing}" cssStyle="width:140px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();"/></td>
				<td width=""><s:select name="defaultAccountLine.mode" list="%{mode}" cssStyle="width:80px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();validService();"/></td>
				<td width=""><s:select name="defaultAccountLine.serviceType" list="%{service}" cssStyle="width:180px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();"/></td>
				<s:hidden name="defaultAccountLine.categories"/>
				<s:hidden name="defaultAccountLine.billToCode"/>
				<%--<td width=""><s:select name="defaultAccountLine.categories" list="%{category}" cssStyle="width:100px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();validService();"/></td>
				<td width=""><s:select name="defaultAccountLine.billToCode" list="%{billToCodeList}" cssStyle="width:120px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();"/></td>
				</tr>
				<tr>
				<td colspan="4"></td>
				<td width="" style="border-left: hidden;"><s:submit cssClass="cssbutton" method="search" cssStyle="width:60px; height:25px;!margin-bottom:10px;" key="button.search"  />
			    <input type="button" class="cssbutton" value="Clear" style="width:60px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/></td> 
			</tr>
		</tbody>
		
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	--%>
<s:hidden name="defaultAccountLine.id" value="%{defaultAccountLine.id}"/>
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<c:set var="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="contract" value="<%=request.getParameter("contract") %>" />
<c:set var="contract" value="<%=request.getParameter("contract") %>" />
<c:set var="button1">
     <input type="button" class="cssbutton" style="width:70px; height:25px" onclick="location.href='<c:url value="/editDefaultAccountLine.html"/>'"  
        value="<fmt:message key="button.add"/>"/>  
</c:set>

<div id="layer4" style="width:100%;">
<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Acct Template<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a href="editContract.html?id=${id}"><span>Contract Details</span></a></li>
		    <li><a href="charge23.html?id=${id}"><span>Charge List</span></a></li>
	 </ul>
		</div><div class="spn">&nbsp;</div>
		
		</div>
		<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
    <table style="margin-bottom: 3px">
    <tr>
        <td align="right" class="listwhitetext">Contract</td>
  <td><s:textfield  value="${contract}" required="true" readonly="true" size="30" cssClass="input-textUpper"/></td>
  </tr>
  </table>
  
  </div>
  <div style = "position:relative; left:400px; top:2px;">
<font color="red" style="text-align:center;left:100px;width:100%;font:13px arial,verdana;top:2px;"><b>
              ${showMessage} 
            </font></b>
             </div>
<div class="bottom-header" style="margin-top:40px;"><span></span></div>
</div>
<s:set name="defaultAccountLines" value="defaultAccountLines" scope="request"/>
<display:table name="defaultAccountLines" class="table" requestURI="" id="defaultAccountLines" export="true" defaultsort="1" style="width:100%" pagesize="25">   
    <display:column property="jobType" sortable="true" titleKey="defaultAccountLine.jobType" />
    <display:column property="route" sortable="true" titleKey="defaultAccountLine.route"  /> 
    <display:column property="mode" sortable="true" titleKey="defaultAccountLine.mode" />
    <display:column property="contract" sortable="true" titleKey="defaultAccountLine.contract" />
    <display:column property="serviceType" sortable="true" titleKey="defaultAccountLine.serviceType" /> 
    <display:column property="categories" sortable="true" titleKey="defaultAccountLine.categories" /> 
    <display:column property="basis" sortable="true" titleKey="defaultAccountLine.basis" /> 
    <display:column headerClass="containeralign" style="text-align: right" property="quantity" sortable="true" titleKey="defaultAccountLine.quantity" /> 
    <display:column headerClass="containeralign" style="text-align: right" property="rate" sortable="true" titleKey="defaultAccountLine.rate" /> 
    <display:column property="chargeCode" sortable="true" titleKey="defaultAccountLine.chargeCode" />
    <display:column property="billToCode" sortable="true" titleKey="defaultAccountLine.billToCode" />  
    <display:column headerClass="containeralign" style="text-align: right" property="amount" sortable="true" titleKey="defaultAccountLine.amount" />
    <display:column property="vendorCode" sortable="true" titleKey="defaultAccountLine.vendorCode" /> 
<display:setProperty name="export.excel.filename" value="Acct Template.xls"/>
<display:setProperty name="export.csv.filename" value="Acct Template.csv"/>
</display:table>  
</div>
<c:if test="${countAcctTemplate == 0}">
     <input type="button"  name="copy" class="cssbutton1" style="width:250px; height:25px" onclick="location.href='<c:url   value="/addAndCopyAcctTemplate.html?id=${id}&contract=${contract}"/>'" value="Copy Template From Another Contract " style="width:90px; height:25px"/> 
</c:if>  
</s:form> 
<script language="javascript" type="text/javascript">
function clear_fields(){
			document.forms['defaultAccountLineForm'].elements['defaultAccountLine.jobType'].value = "";
			document.forms['defaultAccountLineForm'].elements['defaultAccountLine.route'].value = "";
			document.forms['defaultAccountLineForm'].elements['defaultAccountLine.mode'].value = "";
			document.forms['defaultAccountLineForm'].elements['defaultAccountLine.serviceType'].value = "";
			document.forms['defaultAccountLineForm'].elements['defaultAccountLine.categories'].value = "";
			document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].value = "";
			document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].value = "";
			}

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Accounting Template?");
	var did = targetElement;
	if (agree){
		location.href="defaultAccountLinedelete.html?id="+did;
	}else{
		return false;
	}
}		
</script>  
<script type="text/javascript">   
     Form.focusFirstElement($("defaultAccountLineForm"));  
 	//try{checkReadOnly();}
   //	catch(e){} 
</script>  
 