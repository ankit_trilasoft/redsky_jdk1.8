package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DataSecuritySetDao;
import com.trilasoft.app.model.DataSecuritySet;
import com.trilasoft.app.service.DataSecuritySetManager;

public class DataSecuritySetManagerImpl extends GenericManagerImpl<DataSecuritySet, Long> implements DataSecuritySetManager{

	DataSecuritySetDao dataSecuritySetDao;
	
	public DataSecuritySetManagerImpl(DataSecuritySetDao dataSecuritySetDao)
	{
		super(dataSecuritySetDao);
		this.dataSecuritySetDao=dataSecuritySetDao;
	}

	public List findById(Long id) {
		return dataSecuritySetDao.findById(id);
	}

	public List findMaximumId() {
		return dataSecuritySetDao.findMaximumId();
	}

	public List getDataSecurityFilterList() {
		return dataSecuritySetDao.getDataSecurityFilterList();
	}

	public List getPermission(String permissionName) {
		return dataSecuritySetDao.getPermission(permissionName);
	}

	public List getPermissionList() {
		return dataSecuritySetDao.getPermissionList();
	}

	public List getSetList() {
		return dataSecuritySetDao.getSetList();
	}

	public void saveDataSecuritySet(DataSecuritySet dataSecuritySet) {
		dataSecuritySetDao.saveDataSecuritySet(dataSecuritySet)	;
	}

	public void deleteFilters(String prefixDataSet, String sessionCorpID) {
		dataSecuritySetDao.deleteFilters(prefixDataSet, sessionCorpID);
	}

	public List getPartnerUsersList(String prefixDataSet, String sessionCorpID) {
		return dataSecuritySetDao.getPartnerUsersList(prefixDataSet, sessionCorpID);
	}

	public List getUnassignedPartnerUsersList(String prefixDataSet, String sessionCorpID) {
		return dataSecuritySetDao.getUnassignedPartnerUsersList(prefixDataSet, sessionCorpID);
	}

	public List getDataSecuritySet(String prefixDataSet, String sessionCorpID) {
		return dataSecuritySetDao.getDataSecuritySet(prefixDataSet, sessionCorpID);
	}

	public List getAssociatedAgents(String parentAgentCode, String sessionCorpID) {
		return dataSecuritySetDao.getAssociatedAgents(parentAgentCode, sessionCorpID);
	}

	public List search(String name, String description) {
		return dataSecuritySetDao.search(name, description);
	}

	public List validateUserName(String userName, String sessionCorpID) {
		return dataSecuritySetDao.validateUserName(userName, sessionCorpID);
	}

	public List getUsers(String roleName, String userName, String sessionCorpID) {
		return dataSecuritySetDao.getUsers(roleName, userName, sessionCorpID);
	}

	public List getRoleList(String sessionCorpID) {
		return dataSecuritySetDao.getRoleList(sessionCorpID);
	}

	public List getUserList(String sessionCorpID, String name) {
		
		return dataSecuritySetDao.getUserList(sessionCorpID, name);
	}

	public List getUserRoleSet(String userName, String sessionCorpID) {
		return dataSecuritySetDao.getUserRoleSet(userName, sessionCorpID);
	}

	public List getRequestedPartner(String partnerCode, String sessionCorpID) {
		return dataSecuritySetDao.getRequestedPartner(partnerCode, sessionCorpID);
	}

	public List getAgentsUsersList(String partnerCode, String sessionCorpID,String checkOption) {
		return dataSecuritySetDao.getAgentsUsersList(partnerCode, sessionCorpID,checkOption);
	}

	public Map<String, List> getChildAgentCodeMap() {
		return dataSecuritySetDao.getChildAgentCodeMap();
	}

}
