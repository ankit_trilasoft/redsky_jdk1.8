package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.RefSurveyEmailBodyDao;
import com.trilasoft.app.model.RefSurveyEmailBody;
import com.trilasoft.app.service.RefSurveyEmailBodyManager;

public class RefSurveyEmailBodyManagerImpl extends GenericManagerImpl<RefSurveyEmailBody, Long> implements RefSurveyEmailBodyManager{
	RefSurveyEmailBodyDao refSurveyEmailBodyDao;
	public RefSurveyEmailBodyManagerImpl(RefSurveyEmailBodyDao refSurveyEmailBodyDao)
	{
		super(refSurveyEmailBodyDao);
		this.refSurveyEmailBodyDao=refSurveyEmailBodyDao;
	}
	
}
