package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "vanline")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class VanLine extends BaseObject {

	private String corpID;

	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;

	private Long id;

	private String regNum;

	private String glType;

	private BigDecimal net = new BigDecimal(0);

	private String type;

	private Date tranDate;

	private String agent;

	private String accountingAgent;

	private Date uploaded;

	private String shipper;

	private String reconcileStatus;

	private BigDecimal amtDueAgent = new BigDecimal(0);

	private BigDecimal amtDueHq = new BigDecimal(0);

	private Date weekEnding;

	private Integer lineNumber;

	private String description;

	private String glCode;

	private String notes;

	private String driverID;
	
	private String driverName;
	
	private String statementCategory;

	private String distributionCodeDescription;

	private BigDecimal reconcileAmount = new BigDecimal(0);
	private Date accCreatedOn;
	private String companyDivision;
	private String division;
	private String accountLineNumber;
	private String billToCode;
    private String billToName;

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("corpID", corpID)
				.append("createdBy", createdBy)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("id", id)
				.append("regNum", regNum)
				.append("glType", glType)
				.append("net", net)
				.append("type", type)
				.append("tranDate", tranDate)
				.append("agent", agent)
				.append("accountingAgent", accountingAgent)
				.append("uploaded", uploaded)
				.append("shipper", shipper)
				.append("reconcileStatus", reconcileStatus)
				.append("amtDueAgent", amtDueAgent)
				.append("amtDueHq", amtDueHq)
				.append("weekEnding", weekEnding)
				.append("lineNumber", lineNumber)
				.append("description", description)
				.append("glCode", glCode)
				.append("notes", notes)
				.append("driverID", driverID)
				.append("driverName", driverName)
				.append("statementCategory", statementCategory)
				.append("distributionCodeDescription",
						distributionCodeDescription)
				.append("reconcileAmount", reconcileAmount)
				.append("accCreatedOn", accCreatedOn)
				.append("companyDivision", companyDivision)
				.append("division", division)
				.append("accountLineNumber", accountLineNumber)
				.append("billToCode", billToCode)
				.append("billToName", billToName).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof VanLine))
			return false;
		VanLine castOther = (VanLine) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(id, castOther.id)
				.append(regNum, castOther.regNum)
				.append(glType, castOther.glType)
				.append(net, castOther.net)
				.append(type, castOther.type)
				.append(tranDate, castOther.tranDate)
				.append(agent, castOther.agent)
				.append(accountingAgent, castOther.accountingAgent)
				.append(uploaded, castOther.uploaded)
				.append(shipper, castOther.shipper)
				.append(reconcileStatus, castOther.reconcileStatus)
				.append(amtDueAgent, castOther.amtDueAgent)
				.append(amtDueHq, castOther.amtDueHq)
				.append(weekEnding, castOther.weekEnding)
				.append(lineNumber, castOther.lineNumber)
				.append(description, castOther.description)
				.append(glCode, castOther.glCode)
				.append(notes, castOther.notes)
				.append(driverID, castOther.driverID)
				.append(driverName, castOther.driverName)
				.append(statementCategory, castOther.statementCategory)
				.append(distributionCodeDescription,
						castOther.distributionCodeDescription)
				.append(reconcileAmount, castOther.reconcileAmount)
				.append(accCreatedOn, castOther.accCreatedOn)
				.append(companyDivision, castOther.companyDivision)
				.append(division, castOther.division)
				.append(accountLineNumber, castOther.accountLineNumber)
				.append(billToCode, castOther.billToCode)
				.append(billToName, castOther.billToName).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(id).append(regNum).append(glType).append(net)
				.append(type).append(tranDate).append(agent)
				.append(accountingAgent).append(uploaded).append(shipper)
				.append(reconcileStatus).append(amtDueAgent).append(amtDueHq)
				.append(weekEnding).append(lineNumber).append(description)
				.append(glCode).append(notes).append(driverID)
				.append(driverName).append(statementCategory)
				.append(distributionCodeDescription).append(reconcileAmount)
				.append(accCreatedOn).append(companyDivision).append(division)
				.append(accountLineNumber).append(billToCode)
				.append(billToName).toHashCode();
	}

	@Column(length = 15)
	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	@Column(precision = 15, scale = 2)
	public BigDecimal getNet() {
		return net;
	}

	public void setNet(BigDecimal net) {
		this.net = net;
	}

	@Column
	public Date getTranDate() {
		return tranDate;
	}

	public void setTranDate(Date tranDate) {
		this.tranDate = tranDate;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 255)
	public String getGlType() {
		return glType;
	}

	public void setGlType(String glType) {
		this.glType = glType;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 25, unique = true)
	public String getRegNum() {
		return regNum;
	}

	public void setRegNum(String regNum) {
		this.regNum = regNum;
	}

	@Column(length = 15)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column
	public Date getUploaded() {
		return uploaded;
	}

	public void setUploaded(Date uploaded) {
		this.uploaded = uploaded;
	}

	@Column(length = 255)
	public String getShipper() {
		return shipper;
	}

	public void setShipper(String shipper) {
		this.shipper = shipper;
	}

	@Column(length = 50)
	public String getReconcileStatus() {
		return reconcileStatus;
	}

	public void setReconcileStatus(String reconcileStatus) {
		this.reconcileStatus = reconcileStatus;
	}

	@Column(precision = 15, scale = 2)
	public BigDecimal getAmtDueAgent() {
		return amtDueAgent;
	}

	public void setAmtDueAgent(BigDecimal amtDueAgent) {
		this.amtDueAgent = amtDueAgent;
	}

	@Column(precision = 15, scale = 2)
	public BigDecimal getAmtDueHq() {
		return amtDueHq;
	}

	public void setAmtDueHq(BigDecimal amtDueHq) {
		this.amtDueHq = amtDueHq;
	}

	@Column
	public Date getWeekEnding() {
		return weekEnding;
	}

	public void setWeekEnding(Date weekEnding) {
		this.weekEnding = weekEnding;
	}

	@Column(length = 255)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column
	public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

	@Column(length = 50)
	public String getGlCode() {
		return glCode;
	}

	public void setGlCode(String glCode) {
		this.glCode = glCode;
	}

	@Column(length = 1500)
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Column(length = 225)
	public String getDistributionCodeDescription() {
		return distributionCodeDescription;
	}

	public void setDistributionCodeDescription(String distributionCodeDescription) {
		this.distributionCodeDescription = distributionCodeDescription;
	}

	@Column(length = 10)
	public String getDriverID() {
		return driverID;
	}

	public void setDriverID(String driverID) {
		this.driverID = driverID;
	}

	@Column(precision = 15, scale = 2)
	public BigDecimal getReconcileAmount() {
		return reconcileAmount;
	}

	public void setReconcileAmount(BigDecimal reconcileAmount) {
		this.reconcileAmount = reconcileAmount;
	}

	@Column(length = 15)
	public String getAccountingAgent() {
		return accountingAgent;
	}

	public void setAccountingAgent(String accountingAgent) {
		this.accountingAgent = accountingAgent;
	}
	@Column
	public Date getAccCreatedOn() {
		return accCreatedOn;
	}

	public void setAccCreatedOn(Date accCreatedOn) {
		this.accCreatedOn = accCreatedOn;
	}
	@Column
	public String getDriverName() {
		return driverName;
	}
	@Column
	public String getStatementCategory() {
		return statementCategory;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public void setStatementCategory(String statementCategory) {
		this.statementCategory = statementCategory;
	}

	@Column( length=10)
	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	@Column
	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}
	@Column
	public String getAccountLineNumber() {
		return accountLineNumber;
	}

	public void setAccountLineNumber(String accountLineNumber) {
		this.accountLineNumber = accountLineNumber;
	}
   @Column(length=8)
	public String getBillToCode() {
		return billToCode;
	}

	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
  @Column(length=255)
	public String getBillToName() {
		return billToName;
	}

	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	 

}
