<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<head>
    <title>Truck Trailers</title>
    <meta name="heading" content="Truck Trailers:"/>
    
  <script language="javascript" type="text/javascript">  
   function validation(){
     if(document.forms['truckTrailerForm'].elements['truckTrailers.trailerCode'].value==''){
      alert("Trailer Code is a required field ")
      return false;
     }	
	}
  function truckTrailersList(){
	var localNumber = document.forms['truckTrailerForm'].elements['truckNumber'].value;
	var ownerPayTo = document.forms['truckTrailerForm'].elements['partnerCode'].value;
	var truckId = document.forms['truckTrailerForm'].elements['truckId'].value;
	var truckDriverType = document.forms['truckTrailerForm'].elements['truckDriverType'].value;
	var truckDescription = document.forms['truckTrailerForm'].elements['truckDescription'].value;
	var url="trailerList.html?truckNumber=" + encodeURI(localNumber) + "&partnerCode="+ encodeURI(ownerPayTo)+ "&truckId="+ encodeURI(truckId)+ "&truckDriverType="+ encodeURI(truckDriverType)+ "&truckDescription="+ encodeURI(truckDescription);
	location.href = url;
} 
 function addTrailerList(){
    var truckId = document.forms['truckTrailerForm'].elements['truckId'].value;
	var truckNumber = document.forms['truckTrailerForm'].elements['truckNumber'].value;
	var truckDescription = document.forms['truckTrailerForm'].elements['truckDescription'].value;
	var truckDriverType = document.forms['truckTrailerForm'].elements['truckDriverType'].value;
	var ownerPayTo = document.forms['truckTrailerForm'].elements['partnerCode'].value;
	var url="editTrailersDetail.html?truckId=" + encodeURI(truckId) +"&partnerCode="+ encodeURI(ownerPayTo)+ "&truckNumber="+ encodeURI(truckNumber)+ "&truckDescription="+ encodeURI(truckDescription)+ "&truckDriverType="+ encodeURI(truckDriverType);
	location.href = url;
	
}	

function truckTrailerPopup(){
 openWindow('truckTrailerPopup.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=truckTrailers.trailerPayTo&fld_description=description&fld_code=truckTrailers.trailerCode');
 }
</script>   
</head>


<s:form id="truckTrailerForm" action="saveTruckTrailer.html" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat1" value="dd-NNN-yy"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="truckTrailers.id" />
<s:hidden name="validateFlagDriver"/>
<s:hidden name="deliveryLastDay"/>
<s:hidden name="primaryFlagId"   value="${primaryFlagId}"/>
<c:set var="primaryFlagId"  value="${primaryFlagId}"/>
<s:hidden name="primaryFlags" value="${primaryFlags}"/>
<s:hidden name="truckNumber" value="<%=request.getParameter("truckNumber") %>" />
<c:set var="truckNumber" value="<%=request.getParameter("truckNumber") %>" />
<s:hidden name="truckId" value="<%=request.getParameter("truckId") %>" />
<c:set var="truckId" value="<%=request.getParameter("truckId") %>" />
<s:hidden name="truckDriverType" value="<%=request.getParameter("truckDriverType") %>" />
<c:set var="truckDriverType" value="<%=request.getParameter("truckDriverType") %>" />
<s:hidden name="truckDescription" value="<%=request.getParameter("truckDescription") %>" />
<c:set var="truckDescription" value="<%=request.getParameter("truckDescription") %>" />
<s:hidden name="partnerCode" value="<%=request.getParameter("partnerCode") %>" />
<c:set var="partnerCode" value="<%=request.getParameter("partnerCode") %>" />
<c:set var="buttons">   
    <input type="button" value="Add" class="cssbuttonA" style="width:55px; height:25px" onclick="addTrailerList();"/>
</c:set>
<s:hidden name="id" value="${truckTrailers.id}"/>

<div id="layer1" style="width:100%; margin-top: 10px;">
<div id="newmnav">
			<ul>
           <li><a href="editTruck.html?id=${truckId}"><span>Truck Details</span></a></li>
           <li><a href="#" onclick="truckTrailersList();"><span>Trailers</span></a></li>
           <li id="newmnav1" style="background:#FFF"><a class="current"><span>Trailers Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div>
      
<div id="Layer5" style="width:100%" onkeydown="changeStatus();">
<table class="" cellspacing="0" cellpadding="0" border="0" width="100%" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr><td height="5px"></td></tr>
			<tr>
				<td align="right" class="listwhitetext" width="15px">ty</td>
		  		<td align="right" class="listwhitetext" width="50px">Truck #</td>
				<td colspan="2"><s:textfield cssClass="input-textUpper" name="truckTrailers.truckNumber"  value="${truckNumber}" readonly="true" size="20"/></td>
				<td align="right" class="listwhitetext" width="30px"></td>
				<td align="right" class="listwhitetext">Truck&nbsp;Description</td>
				<td><s:textfield value="${truckDescription}" cssClass="input-textUpper" readonly="true" size="30"/></td>
			</tr>			
			<tr>
				<td align="right" class="listwhitetext" width="15px"></td>
		  		<td align="right" class="listwhitetext">Truck&nbsp;Type</td>
				<td colspan="2"><s:textfield  value="${truckDriverType}" cssClass="input-textUpper" size="30" readonly="true"/></td>
				<td align="right" class="listwhitetext" width="30px"></td>
				<td align="right" class="listwhitetext">Owner/Pay To</td>
				<td><s:textfield cssClass="input-textUpper" name="truckTrailers.ownerPayTo"  value="${partnerCode}" readonly="true" size="10"/></td>
			</tr>			
			<tr>
				<td align="right" class="listwhitetext" width="15px"></td>
				<td align="right" class="listwhitetext" >Trailer&nbsp;Code<font color="red" size="2">*</font></td> 
				
				<td colspan="2">
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
				<tr>
				<td align="left" class="listwhitetext" valign="top"><s:textfield name="truckTrailers.trailerCode" maxlength="30" required="true" readonly="true" cssClass="input-textUpper" size="10" /></td>
				<td align="left"><img class="openpopup" width="17" height="20" onclick="truckTrailerPopup();" src="<c:url value='/images/open-popup.gif'/>" /></td>
				</tr>
				</table>
				</td>
				<td align="right" class="listwhitetext" ></td>
				<td align="right" class="listwhitetext">Trailer Pay To</td> 
				<td align="left" class="listwhitetext" valign="top"><s:textfield name="truckTrailers.trailerPayTo" maxlength="30"  required="true" readonly="true" cssClass="input-textUpper" size="10" /></td>
			</tr>
			<tr>
			<c:if test="${primaryFlagId==truckTrailers.id && primaryFlags=='true'}">
				<c:set var="ischeckedddd" value="false"/>
				<c:if test="${truckTrailers.primaryTrailer}">
					<c:set var="ischeckedddd" value="true"/>
				</c:if>
				<td align="right" colspan="6"  class="listwhitetext">Primary</td>
				<td align="left" colspan="1"><s:checkbox key="truckTrailers.primaryTrailer"  value="${ischeckedddd}" fieldValue="true" /></td>
		    </c:if>
		    
		    <c:if test="${primaryFlags=='show' || primaryFlags=='false'}">
				<c:set var="ischeckeddddsss" value="false"/>
				<c:if test="${truckTrailers.primaryTrailer}">
					<c:set var="ischeckedddd" value="true"/>
				</c:if>
				<td align="right" colspan="6"  class="listwhitetext">Primary</td>
				<td align="left" colspan="1"><s:checkbox key="truckTrailers.primaryTrailer" value="${ischeckeddddsss}" fieldValue="true" /></td>
		    </c:if>
			</tr>
<tr>

<td align="right" class="listwhitetext" width="15px"></td>

</tr>
 
<tr><td align="left" height="15px"></td></tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
</div>
</td>
</tr>
</tbody>
</table> 
<table width="780px">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${truckTrailers.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="truckTrailers.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${truckTrailers.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty truckTrailers.id}">
								<s:hidden name="truckTrailers.createdBy"/>
								<td ><s:label name="createdBy" value="%{truckTrailers.createdBy}"/></td>
							</c:if>
							<c:if test="${empty truckTrailers.id}">
								<s:hidden name="truckTrailers.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${truckTrailers.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="truckTrailers.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${truckTrailers.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty truckTrailers.id}">
							<s:hidden name="truckTrailers.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{truckTrailers.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty truckTrailers.id}">
							<s:hidden name="truckTrailers.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
</div> 
   <table><tr><td>   
        <s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple" onclick="return validation();"/>
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" />
        <c:if test="${not empty truckTrailers.id}">
	    <c:out value="${buttons}" escapeXml="false" />
     </c:if>
	</td></tr></table>
</div>   
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="firstDescription" />
<s:hidden name="description" />
</s:form> 
<div id="mydiv" style="position:absolute;top:110px;"></div>
<script type="text/javascript">
 </script>