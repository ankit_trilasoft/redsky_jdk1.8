
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for MSSOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MSSOrder">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerContactID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CustomerContactEmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AffiliateID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="BillToID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PurchaseOrderNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EstimatedWeight" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PackDateStart" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="PackDateEnd" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="LoadDateStart" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="LoadDateEnd" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DeliveryDateStart" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DeliveryDateEnd" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="OriginRequestDateStart" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="OriginRequestDateEnd" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DestinationRequestDateStart" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DestinationRequestDateEnd" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Urgent" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="VIP" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ShipperFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperEmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperOriginAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperOriginAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperOriginCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperOriginState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperOriginZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperDestinationAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperDestinationAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperDestinationCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperDestinationState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperDestinationZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSSOrderOriginServices" type="{http://www.mss1.com/webservices/}ArrayOfMSSOrderService" minOccurs="0"/>
 *         &lt;element name="MSSOrderDestinationServices" type="{http://www.mss1.com/webservices/}ArrayOfMSSOrderService" minOccurs="0"/>
 *         &lt;element name="MSSOrderCrates" type="{http://www.mss1.com/webservices/}ArrayOfMSSOrderCrate" minOccurs="0"/>
 *         &lt;element name="MSSOrderOriginPhoneNumbers" type="{http://www.mss1.com/webservices/}ArrayOfMSSOrderPhoneNumber" minOccurs="0"/>
 *         &lt;element name="MSSOrderDestinationPhoneNumbers" type="{http://www.mss1.com/webservices/}ArrayOfMSSOrderPhoneNumber" minOccurs="0"/>
 *         &lt;element name="MSSOrderOriginNotes" type="{http://www.mss1.com/webservices/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="MSSOrderDestinationNotes" type="{http://www.mss1.com/webservices/}ArrayOfString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MSSOrder", propOrder = {
    "customerContactID",
    "customerContactEmailAddress",
    "affiliateID",
    "billToID",
    "purchaseOrderNumber",
    "estimatedWeight",
    "packDateStart",
    "packDateEnd",
    "loadDateStart",
    "loadDateEnd",
    "deliveryDateStart",
    "deliveryDateEnd",
    "originRequestDateStart",
    "originRequestDateEnd",
    "destinationRequestDateStart",
    "destinationRequestDateEnd",
    "urgent",
    "vip",
    "shipperFirstName",
    "shipperLastName",
    "shipperEmailAddress",
    "shipperOriginAddress1",
    "shipperOriginAddress2",
    "shipperOriginCity",
    "shipperOriginState",
    "shipperOriginZip",
    "shipperDestinationAddress1",
    "shipperDestinationAddress2",
    "shipperDestinationCity",
    "shipperDestinationState",
    "shipperDestinationZip",
    "mssOrderOriginServices",
    "mssOrderDestinationServices",
    "mssOrderCrates",
    "mssOrderOriginPhoneNumbers",
    "mssOrderDestinationPhoneNumbers",
    "mssOrderOriginNotes",
    "mssOrderDestinationNotes",
    "mssCustomerContact"
})
public class MSSOrder {

    @XmlElement(name = "CustomerContactID")
    protected int customerContactID;
    @XmlElement(name = "CustomerContactEmailAddress")
    protected String customerContactEmailAddress;
    @XmlElement(name = "AffiliateID")
    protected int affiliateID;
    @XmlElement(name = "BillToID")
    protected int billToID;
    @XmlElement(name = "PurchaseOrderNumber")
    protected String purchaseOrderNumber;
    @XmlElement(name = "EstimatedWeight")
    protected int estimatedWeight;
    @XmlElement(name = "PackDateStart", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar packDateStart;
    @XmlElement(name = "PackDateEnd", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar packDateEnd;
    @XmlElement(name = "LoadDateStart", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar loadDateStart;
    @XmlElement(name = "LoadDateEnd", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar loadDateEnd;
    @XmlElement(name = "DeliveryDateStart", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar deliveryDateStart;
    @XmlElement(name = "DeliveryDateEnd", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar deliveryDateEnd;
    @XmlElement(name = "OriginRequestDateStart", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar originRequestDateStart;
    @XmlElement(name = "OriginRequestDateEnd", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar originRequestDateEnd;
    @XmlElement(name = "DestinationRequestDateStart", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar destinationRequestDateStart;
    @XmlElement(name = "DestinationRequestDateEnd", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar destinationRequestDateEnd;
    @XmlElement(name = "Urgent")
    protected boolean urgent;
    @XmlElement(name = "VIP")
    protected boolean vip;
    @XmlElement(name = "ShipperFirstName")
    protected String shipperFirstName;
    @XmlElement(name = "ShipperLastName")
    protected String shipperLastName;
    @XmlElement(name = "ShipperEmailAddress")
    protected String shipperEmailAddress;
    @XmlElement(name = "ShipperOriginAddress1")
    protected String shipperOriginAddress1;
    @XmlElement(name = "ShipperOriginAddress2")
    protected String shipperOriginAddress2;
    @XmlElement(name = "ShipperOriginCity")
    protected String shipperOriginCity;
    @XmlElement(name = "ShipperOriginState")
    protected String shipperOriginState;
    @XmlElement(name = "ShipperOriginZip")
    protected String shipperOriginZip;
    @XmlElement(name = "ShipperDestinationAddress1")
    protected String shipperDestinationAddress1;
    @XmlElement(name = "ShipperDestinationAddress2")
    protected String shipperDestinationAddress2;
    @XmlElement(name = "ShipperDestinationCity")
    protected String shipperDestinationCity;
    @XmlElement(name = "ShipperDestinationState")
    protected String shipperDestinationState;
    @XmlElement(name = "ShipperDestinationZip")
    protected String shipperDestinationZip;
    @XmlElement(name = "MSSOrderOriginServices")
    protected ArrayOfMSSOrderService mssOrderOriginServices;
    @XmlElement(name = "MSSOrderDestinationServices")
    protected ArrayOfMSSOrderService mssOrderDestinationServices;
    @XmlElement(name = "MSSOrderCrates")
    protected ArrayOfMSSOrderCrate mssOrderCrates;
    @XmlElement(name = "MSSOrderOriginPhoneNumbers")
    protected ArrayOfMSSOrderPhoneNumber mssOrderOriginPhoneNumbers;
    @XmlElement(name = "MSSOrderDestinationPhoneNumbers")
    protected ArrayOfMSSOrderPhoneNumber mssOrderDestinationPhoneNumbers;
    @XmlElement(name = "MSSOrderOriginNotes")
    protected ArrayOfString mssOrderOriginNotes;
    @XmlElement(name = "MSSOrderDestinationNotes")
    protected ArrayOfString mssOrderDestinationNotes;
    @XmlElement(name="MSSCustomerContact")
    protected ArrayOfMSSCustomerContact mssCustomerContact;
    

    /**
     * Gets the value of the customerContactID property.
     * 
     */
    public int getCustomerContactID() {
        return customerContactID;
    }

    /**
     * Sets the value of the customerContactID property.
     * 
     */
    public void setCustomerContactID(int value) {
        this.customerContactID = value;
    }

    /**
     * Gets the value of the customerContactEmailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerContactEmailAddress() {
        return customerContactEmailAddress;
    }

    /**
     * Sets the value of the customerContactEmailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerContactEmailAddress(String value) {
        this.customerContactEmailAddress = value;
    }

    /**
     * Gets the value of the affiliateID property.
     * 
     */
    public int getAffiliateID() {
        return affiliateID;
    }

    /**
     * Sets the value of the affiliateID property.
     * 
     */
    public void setAffiliateID(int value) {
        this.affiliateID = value;
    }

    /**
     * Gets the value of the billToID property.
     * 
     */
    public int getBillToID() {
        return billToID;
    }

    /**
     * Sets the value of the billToID property.
     * 
     */
    public void setBillToID(int value) {
        this.billToID = value;
    }

    /**
     * Gets the value of the purchaseOrderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    /**
     * Sets the value of the purchaseOrderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseOrderNumber(String value) {
        this.purchaseOrderNumber = value;
    }

    /**
     * Gets the value of the estimatedWeight property.
     * 
     */
    public int getEstimatedWeight() {
        return estimatedWeight;
    }

    /**
     * Sets the value of the estimatedWeight property.
     * 
     */
    public void setEstimatedWeight(int value) {
        this.estimatedWeight = value;
    }

    /**
     * Gets the value of the packDateStart property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPackDateStart() {
        return packDateStart;
    }

    /**
     * Sets the value of the packDateStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPackDateStart(XMLGregorianCalendar value) {
        this.packDateStart = value;
    }

    /**
     * Gets the value of the packDateEnd property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPackDateEnd() {
        return packDateEnd;
    }

    /**
     * Sets the value of the packDateEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPackDateEnd(XMLGregorianCalendar value) {
        this.packDateEnd = value;
    }

    /**
     * Gets the value of the loadDateStart property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLoadDateStart() {
        return loadDateStart;
    }

    /**
     * Sets the value of the loadDateStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLoadDateStart(XMLGregorianCalendar value) {
        this.loadDateStart = value;
    }

    /**
     * Gets the value of the loadDateEnd property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLoadDateEnd() {
        return loadDateEnd;
    }

    /**
     * Sets the value of the loadDateEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLoadDateEnd(XMLGregorianCalendar value) {
        this.loadDateEnd = value;
    }

    /**
     * Gets the value of the deliveryDateStart property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeliveryDateStart() {
        return deliveryDateStart;
    }

    /**
     * Sets the value of the deliveryDateStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeliveryDateStart(XMLGregorianCalendar value) {
        this.deliveryDateStart = value;
    }

    /**
     * Gets the value of the deliveryDateEnd property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeliveryDateEnd() {
        return deliveryDateEnd;
    }

    /**
     * Sets the value of the deliveryDateEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeliveryDateEnd(XMLGregorianCalendar value) {
        this.deliveryDateEnd = value;
    }

    /**
     * Gets the value of the originRequestDateStart property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOriginRequestDateStart() {
        return originRequestDateStart;
    }

    /**
     * Sets the value of the originRequestDateStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOriginRequestDateStart(XMLGregorianCalendar value) {
        this.originRequestDateStart = value;
    }

    /**
     * Gets the value of the originRequestDateEnd property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOriginRequestDateEnd() {
        return originRequestDateEnd;
    }

    /**
     * Sets the value of the originRequestDateEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOriginRequestDateEnd(XMLGregorianCalendar value) {
        this.originRequestDateEnd = value;
    }

    /**
     * Gets the value of the destinationRequestDateStart property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDestinationRequestDateStart() {
        return destinationRequestDateStart;
    }

    /**
     * Sets the value of the destinationRequestDateStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDestinationRequestDateStart(XMLGregorianCalendar value) {
        this.destinationRequestDateStart = value;
    }

    /**
     * Gets the value of the destinationRequestDateEnd property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDestinationRequestDateEnd() {
        return destinationRequestDateEnd;
    }

    /**
     * Sets the value of the destinationRequestDateEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDestinationRequestDateEnd(XMLGregorianCalendar value) {
        this.destinationRequestDateEnd = value;
    }

    /**
     * Gets the value of the urgent property.
     * 
     */
    public boolean isUrgent() {
        return urgent;
    }

    /**
     * Sets the value of the urgent property.
     * 
     */
    public void setUrgent(boolean value) {
        this.urgent = value;
    }

    /**
     * Gets the value of the vip property.
     * 
     */
    public boolean isVIP() {
        return vip;
    }

    /**
     * Sets the value of the vip property.
     * 
     */
    public void setVIP(boolean value) {
        this.vip = value;
    }

    /**
     * Gets the value of the shipperFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperFirstName() {
        return shipperFirstName;
    }

    /**
     * Sets the value of the shipperFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperFirstName(String value) {
        this.shipperFirstName = value;
    }

    /**
     * Gets the value of the shipperLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperLastName() {
        return shipperLastName;
    }

    /**
     * Sets the value of the shipperLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperLastName(String value) {
        this.shipperLastName = value;
    }

    /**
     * Gets the value of the shipperEmailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperEmailAddress() {
        return shipperEmailAddress;
    }

    /**
     * Sets the value of the shipperEmailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperEmailAddress(String value) {
        this.shipperEmailAddress = value;
    }

    /**
     * Gets the value of the shipperOriginAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperOriginAddress1() {
        return shipperOriginAddress1;
    }

    /**
     * Sets the value of the shipperOriginAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperOriginAddress1(String value) {
        this.shipperOriginAddress1 = value;
    }

    /**
     * Gets the value of the shipperOriginAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperOriginAddress2() {
        return shipperOriginAddress2;
    }

    /**
     * Sets the value of the shipperOriginAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperOriginAddress2(String value) {
        this.shipperOriginAddress2 = value;
    }

    /**
     * Gets the value of the shipperOriginCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperOriginCity() {
        return shipperOriginCity;
    }

    /**
     * Sets the value of the shipperOriginCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperOriginCity(String value) {
        this.shipperOriginCity = value;
    }

    /**
     * Gets the value of the shipperOriginState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperOriginState() {
        return shipperOriginState;
    }

    /**
     * Sets the value of the shipperOriginState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperOriginState(String value) {
        this.shipperOriginState = value;
    }

    /**
     * Gets the value of the shipperOriginZip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperOriginZip() {
        return shipperOriginZip;
    }

    /**
     * Sets the value of the shipperOriginZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperOriginZip(String value) {
        this.shipperOriginZip = value;
    }

    /**
     * Gets the value of the shipperDestinationAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperDestinationAddress1() {
        return shipperDestinationAddress1;
    }

    /**
     * Sets the value of the shipperDestinationAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperDestinationAddress1(String value) {
        this.shipperDestinationAddress1 = value;
    }

    /**
     * Gets the value of the shipperDestinationAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperDestinationAddress2() {
        return shipperDestinationAddress2;
    }

    /**
     * Sets the value of the shipperDestinationAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperDestinationAddress2(String value) {
        this.shipperDestinationAddress2 = value;
    }

    /**
     * Gets the value of the shipperDestinationCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperDestinationCity() {
        return shipperDestinationCity;
    }

    /**
     * Sets the value of the shipperDestinationCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperDestinationCity(String value) {
        this.shipperDestinationCity = value;
    }

    /**
     * Gets the value of the shipperDestinationState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperDestinationState() {
        return shipperDestinationState;
    }

    /**
     * Sets the value of the shipperDestinationState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperDestinationState(String value) {
        this.shipperDestinationState = value;
    }

    /**
     * Gets the value of the shipperDestinationZip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperDestinationZip() {
        return shipperDestinationZip;
    }

    /**
     * Sets the value of the shipperDestinationZip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperDestinationZip(String value) {
        this.shipperDestinationZip = value;
    }

    /**
     * Gets the value of the mssOrderOriginServices property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSOrderService }
     *     
     */
    public ArrayOfMSSOrderService getMSSOrderOriginServices() {
        return mssOrderOriginServices;
    }

    /**
     * Sets the value of the mssOrderOriginServices property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSOrderService }
     *     
     */
    public void setMSSOrderOriginServices(ArrayOfMSSOrderService value) {
        this.mssOrderOriginServices = value;
    }

    /**
     * Gets the value of the mssOrderDestinationServices property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSOrderService }
     *     
     */
    public ArrayOfMSSOrderService getMSSOrderDestinationServices() {
        return mssOrderDestinationServices;
    }

    /**
     * Sets the value of the mssOrderDestinationServices property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSOrderService }
     *     
     */
    public void setMSSOrderDestinationServices(ArrayOfMSSOrderService value) {
        this.mssOrderDestinationServices = value;
    }

    /**
     * Gets the value of the mssOrderCrates property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSOrderCrate }
     *     
     */
    public ArrayOfMSSOrderCrate getMSSOrderCrates() {
        return mssOrderCrates;
    }

    /**
     * Sets the value of the mssOrderCrates property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSOrderCrate }
     *     
     */
    public void setMSSOrderCrates(ArrayOfMSSOrderCrate value) {
        this.mssOrderCrates = value;
    }

    /**
     * Gets the value of the mssOrderOriginPhoneNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSOrderPhoneNumber }
     *     
     */
    public ArrayOfMSSOrderPhoneNumber getMSSOrderOriginPhoneNumbers() {
        return mssOrderOriginPhoneNumbers;
    }

    /**
     * Sets the value of the mssOrderOriginPhoneNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSOrderPhoneNumber }
     *     
     */
    public void setMSSOrderOriginPhoneNumbers(ArrayOfMSSOrderPhoneNumber value) {
        this.mssOrderOriginPhoneNumbers = value;
    }

    /**
     * Gets the value of the mssOrderDestinationPhoneNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSOrderPhoneNumber }
     *     
     */
    public ArrayOfMSSOrderPhoneNumber getMSSOrderDestinationPhoneNumbers() {
        return mssOrderDestinationPhoneNumbers;
    }

    /**
     * Sets the value of the mssOrderDestinationPhoneNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSOrderPhoneNumber }
     *     
     */
    public void setMSSOrderDestinationPhoneNumbers(ArrayOfMSSOrderPhoneNumber value) {
        this.mssOrderDestinationPhoneNumbers = value;
    }

    /**
     * Gets the value of the mssOrderOriginNotes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getMSSOrderOriginNotes() {
        return mssOrderOriginNotes;
    }

    /**
     * Sets the value of the mssOrderOriginNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setMSSOrderOriginNotes(ArrayOfString value) {
        this.mssOrderOriginNotes = value;
    }

    /**
     * Gets the value of the mssOrderDestinationNotes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getMSSOrderDestinationNotes() {
        return mssOrderDestinationNotes;
    }

    /**
     * Sets the value of the mssOrderDestinationNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setMSSOrderDestinationNotes(ArrayOfString value) {
        this.mssOrderDestinationNotes = value;
    }

	public ArrayOfMSSCustomerContact getMssCustomerContact() {
		return mssCustomerContact;
	}

	public void setMssCustomerContact(ArrayOfMSSCustomerContact mssCustomerContact) {
		this.mssCustomerContact = mssCustomerContact;
	}

}
