

delimiter $$
DROP trigger IF EXISTS `redsky`.`trigger_add_history_operationsintelligence` $$
CREATE trigger redsky.trigger_add_history_operationsintelligence 
BEFORE UPDATE on redsky.operationsintelligence
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT; 
  DECLARE countWO LONGTEXT;
  
  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
  SET countWO =" ";


   IF (OLD.type <> NEW.type ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.type~') into fieldNameList;
      select CONCAT(oldValueList,OLD.type,'~') into oldValueList;
      select CONCAT(newValueList,NEW.type,'~') into newValueList;
  END IF;

    
  IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;

   

   IF (OLD.quantitiy <> NEW.quantitiy ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.quantitiy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quantitiy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quantitiy,'~') into newValueList;
  END IF;

    

   IF (OLD.esthours <> NEW.esthours ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.esthours~') into fieldNameList;
      select CONCAT(oldValueList,OLD.esthours,'~') into oldValueList;
      select CONCAT(newValueList,NEW.esthours,'~') into newValueList;
  END IF;

    
   IF (OLD.comments <> NEW.comments ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.comments~') into fieldNameList;
      select CONCAT(oldValueList,OLD.comments,'~') into oldValueList;
      select CONCAT(newValueList,NEW.comments,'~') into newValueList;
  END IF;

    
   IF (OLD.workorder <> NEW.workorder ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.workorder~') into fieldNameList;
      select CONCAT(oldValueList,OLD.workorder,'~') into oldValueList;
      select CONCAT(newValueList,NEW.workorder,'~') into newValueList;
      IF(NEW.type <> 'T' and NEW.type <> 'Z' and NEW.type <> 'F') then
        select count(type) into countWO from operationsintelligence where shipNumber=NEW.shipNumber and workorder = OLD.workorder and type =New.type;
    if(countWO > 1) then
      SET NEW.accountLineId=null;
    END IF;   
      END IF;
  END IF;

    
   IF (OLD.estimatedbuyrate <> NEW.estimatedbuyrate ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.estimatedbuyrate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estimatedbuyrate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estimatedbuyrate,'~') into newValueList;
  END IF;

      IF (OLD.revisionbuyrate <> NEW.revisionbuyrate ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionbuyrate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionbuyrate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionbuyrate,'~') into newValueList;
  END IF;

    
   IF (OLD.estimatedsellrate <> NEW.estimatedsellrate ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.estimatedsellrate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estimatedsellrate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estimatedsellrate,'~') into newValueList;
  END IF;

    
 IF (OLD.revisionsellrate <> NEW.revisionsellrate ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionsellrate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionsellrate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionsellrate,'~') into newValueList;
  END IF;

   IF ((OLD.date <> NEW.date) or (OLD.date is null and NEW.date is not null)
    or (OLD.date is not null and NEW.date is null)) THEN
      select CONCAT(fieldNameList,'operationsintelligence.date~') into fieldNameList;

      IF(OLD.date is null) THEN
          select CONCAT(oldValueList," ",'~') into oldValueList;
        END IF;
        IF(NEW.date is null) THEN
          select CONCAT(newValueList," ",'~') into newValueList;
        END IF;

      IF(OLD.date is not null) then
          select CONCAT(oldValueList,OLD.date,'~') into oldValueList;
       END IF;
        IF(NEW.date is not null) then
          select CONCAT(newValueList,NEW.date,'~') into newValueList;
        END IF;

  END IF;
  
  
 
  
  
  
  
  
   
    IF (OLD.ticket <> NEW.ticket ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.ticket~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ticket,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ticket,'~') into newValueList;
  END IF;

 
   
   
    IF (OLD.estimatedquantity <> NEW.estimatedquantity ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.estimatedquantity~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estimatedquantity,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estimatedquantity,'~') into newValueList;
  END IF;
   
   
   
   IF (OLD.revisionquantity <> NEW.revisionquantity ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionquantity~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionquantity,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionquantity,'~') into newValueList;
  END IF;
   
   IF (OLD.estimatedexpense <> NEW.estimatedexpense ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.estimatedexpense~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estimatedexpense,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estimatedexpense,'~') into newValueList;
  END IF;

    IF (OLD.revisionexpense <> NEW.revisionexpense ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionexpense~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionexpense,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionexpense,'~') into newValueList;
  END IF;

    IF (OLD.revisionrevenue <> NEW.revisionrevenue ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionrevenue~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionrevenue,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionrevenue,'~') into newValueList;
  END IF;

   IF (OLD.shipNumber <> NEW.shipNumber ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.shipNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
  END IF;
  
    IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;
   
    IF (OLD.serviceOrderId <> NEW.serviceOrderId ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.serviceOrderId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.serviceOrderId,'~') into newValueList;
  END IF;
   
   
    IF (OLD.workTicketId <> NEW.workTicketId ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.workTicketId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.workTicketId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.workTicketId,'~') into newValueList;
  END IF;
   
   
   
    IF (OLD.scopeOfWorkOrder <> NEW.scopeOfWorkOrder ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.scopeOfWorkOrder~') into fieldNameList;
      select CONCAT(oldValueList,OLD.scopeOfWorkOrder,'~') into oldValueList;
      select CONCAT(newValueList,NEW.scopeOfWorkOrder,'~') into newValueList;
  END IF;
   
   IF (OLD.estimatedTotalExpenseForOI <> NEW.estimatedTotalExpenseForOI ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.estimatedTotalExpenseForOI~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estimatedTotalExpenseForOI,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estimatedTotalExpenseForOI,'~') into newValueList;
  END IF;
   
   IF (OLD.estimatedTotalRevenueForOI <> NEW.estimatedTotalRevenueForOI ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.estimatedTotalRevenueForOI~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estimatedTotalRevenueForOI,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estimatedTotalRevenueForOI,'~') into newValueList;
  END IF;
  
  IF (OLD.revisedTotalExpenseForOI <> NEW.revisedTotalExpenseForOI ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisedTotalExpenseForOI~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisedTotalExpenseForOI,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisedTotalExpenseForOI,'~') into newValueList;
  END IF;
  
  IF (OLD.revisedTotalRevenueForOI <> NEW.revisedTotalRevenueForOI ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisedTotalRevenueForOI~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisedTotalRevenueForOI,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisedTotalRevenueForOI,'~') into newValueList;
  END IF;
  
  IF (OLD.estimatedGrossMarginForOI <> NEW.estimatedGrossMarginForOI ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.estimatedGrossMarginForOI~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estimatedGrossMarginForOI,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estimatedGrossMarginForOI,'~') into newValueList;
  END IF;
  
  
   IF (OLD.revisedGrossMarginForOI <> NEW.revisedGrossMarginForOI ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisedGrossMarginForOI~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisedGrossMarginForOI,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisedGrossMarginForOI,'~') into newValueList;
  END IF;
   
    IF (OLD.estimatedGrossMarginPercentageForOI <> NEW.estimatedGrossMarginPercentageForOI ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.estimatedGrossMarginPercentageForOI~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estimatedGrossMarginPercentageForOI,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estimatedGrossMarginPercentageForOI,'~') into newValueList;
  END IF;
   
    IF (OLD.revisedGrossMarginPercentageForOI <> NEW.revisedGrossMarginPercentageForOI ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisedGrossMarginPercentageForOI~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisedGrossMarginPercentageForOI,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisedGrossMarginPercentageForOI,'~') into newValueList;
  END IF;
   
   
     
   
     IF (OLD.itemsJbkEquipId <> NEW.itemsJbkEquipId ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.itemsJbkEquipId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.itemsJbkEquipId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.itemsJbkEquipId,'~') into newValueList;
  END IF;
   
   
    IF (OLD.estmatedSalesTax <> NEW.estmatedSalesTax ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.estmatedSalesTax~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estmatedSalesTax,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estmatedSalesTax,'~') into newValueList;
  END IF;
  IF (OLD.revisionScopeOfConsumables <> NEW.revisionScopeOfConsumables ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionScopeOfConsumables~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionScopeOfConsumables,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionScopeOfConsumables,'~') into newValueList;
  END IF;
  
  
    IF (OLD.revisionSalesTax <> NEW.revisionSalesTax ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionSalesTax~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionSalesTax,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionSalesTax,'~') into newValueList;
  END IF;
  
  
     IF (OLD.displayPriority <> NEW.displayPriority ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.displayPriority~') into fieldNameList;
      select CONCAT(oldValueList,OLD.displayPriority,'~') into oldValueList;
      select CONCAT(newValueList,NEW.displayPriority,'~') into newValueList;
  END IF;
  
    IF (OLD.ticketStatus <> NEW.ticketStatus ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.ticketStatus~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ticketStatus,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ticketStatus,'~') into newValueList;
  END IF;
  IF (OLD.targetActual <> NEW.targetActual ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.targetActual~') into fieldNameList;
      select CONCAT(oldValueList,OLD.targetActual,'~') into oldValueList;
      select CONCAT(newValueList,NEW.targetActual,'~') into newValueList;
  END IF;
  IF (OLD.revisionScopeOfWorkOrder <> NEW.revisionScopeOfWorkOrder ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionScopeOfWorkOrder~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionScopeOfWorkOrder,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionScopeOfWorkOrder,'~') into newValueList;
  END IF;
  IF (OLD.revisionScopeOfNumberOfComputer <> NEW.revisionScopeOfNumberOfComputer ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionScopeOfNumberOfComputer~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionScopeOfNumberOfComputer,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionScopeOfNumberOfComputer,'~') into newValueList;
  END IF;
  IF (OLD.revisionScopeOfNumberOfEmployee <> NEW.revisionScopeOfNumberOfEmployee ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionScopeOfNumberOfEmployee~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionScopeOfNumberOfEmployee,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionScopeOfNumberOfEmployee,'~') into newValueList;
  END IF;
  IF (OLD.revisionScopeOfSalesTax <> NEW.revisionScopeOfSalesTax ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionScopeOfSalesTax~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionScopeOfSalesTax,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionScopeOfSalesTax,'~') into newValueList;
  END IF;
    IF (OLD.ticketStatus <> NEW.ticketStatus ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.ticketStatus~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ticketStatus,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ticketStatus,'~') into newValueList;
  END IF;
  
     IF (OLD.estmatedValuation <> NEW.estmatedValuation ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.estmatedValuation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estmatedValuation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estmatedValuation,'~') into newValueList;
  END IF;
     IF (OLD.revisionValuation <> NEW.revisionValuation ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionValuation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionValuation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionValuation,'~') into newValueList;
  END IF;
  
     IF (OLD.estmatedConsumables <> NEW.estmatedConsumables ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.estmatedConsumables~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estmatedConsumables,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estmatedConsumables,'~') into newValueList;
  END IF;
  
     IF (OLD.revisionConsumables <> NEW.revisionConsumables ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionConsumables~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionConsumables,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionConsumables,'~') into newValueList;
  END IF;
  
       IF (OLD.numberOfComputer <> NEW.numberOfComputer ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.numberOfComputer~') into fieldNameList;
      select CONCAT(oldValueList,OLD.numberOfComputer,'~') into oldValueList;
      select CONCAT(newValueList,NEW.numberOfComputer,'~') into newValueList;
  END IF;

      IF (OLD.numberOfEmployee <> NEW.numberOfEmployee ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.numberOfEmployee~') into fieldNameList;
      select CONCAT(oldValueList,OLD.numberOfEmployee,'~') into oldValueList;
      select CONCAT(newValueList,NEW.numberOfEmployee,'~') into newValueList;
  END IF;

      IF (OLD.salesTax <> NEW.salesTax ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.salesTax~') into fieldNameList;
      select CONCAT(oldValueList,OLD.salesTax,'~') into oldValueList;
      select CONCAT(newValueList,NEW.salesTax,'~') into newValueList;
  END IF;

 IF (OLD.consumables <> NEW.consumables ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.consumables~') into fieldNameList;
      select CONCAT(oldValueList,OLD.consumables,'~') into oldValueList;
      select CONCAT(newValueList,NEW.consumables,'~') into newValueList;
  END IF;
  
  IF (OLD.revisionComment <> NEW.revisionComment ) THEN
      select CONCAT(fieldNameList,'operationsintelligence.revisionComment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.revisionComment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.revisionComment,'~') into newValueList;
  END IF;
  IF (OLD.status <> NEW.status ) THEN
       select CONCAT(fieldNameList,'company.status~') into fieldNameList;
       IF(OLD.status = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.status = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.status = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.status = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
  
    
 CALL add_tblHistory (OLD.id,"operationsintelligence", fieldNameList, oldValueList, newValueList,NEW.updatedBy, OLD.corpID, now());

END 
$$