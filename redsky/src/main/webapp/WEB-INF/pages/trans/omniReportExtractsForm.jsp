<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title><fmt:message key="omniReportExtracts.title" /></title>
<meta name="heading" content="<fmt:message key='omniReportExtracts.heading'/>" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style> <%@ include file="/common/calenderStyle.css"%> </style>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="javascript" type="text/javascript">

 function findBookingAgentCode(){
	   companyCode=document.forms['omniReportExtractsForm'].elements['companyCode'].value ;
        var url="findBookingAgentCode.html?ajax=1&decorator=simple&popup=true&companyCode="+encodeURI(companyCode);
       	http4.open("GET", url, true);
    	http4.onreadystatechange = handleHttpResponseBookingAgentCode;
    	http4.send(null);
       }  
  function handleHttpResponseBookingAgentCode(){
             if (http4.readyState == 4){
              var results = http4.responseText
               results = results.trim();
              var res = results.replace("[",'');
                res = res.replace("]",'');
                res=res.split(",");
             var targetElement = document.forms['omniReportExtractsForm'].elements['bookingAgentCode'];
					targetElement.length = res.length;
              for(i=0;i<res.length;i++)	{
                document.forms['omniReportExtractsForm'].elements['bookingAgentCode'].options[i].text = res[i];
				document.forms['omniReportExtractsForm'].elements['bookingAgentCode'].options[i].value = res[i];
			  }
             }
             
}
function fieldValidates(){
        if(document.forms['omniReportExtractsForm'].elements['endDate'].value==''){
	       alert("Please enter the end date "); 
	       	return false;
	       
	       } else if(document.forms['omniReportExtractsForm'].elements['companyCode'].value=='')  {
	       alert("Please enter the Company Code"); 
	     	return false;
	     		} else if(document.forms['omniReportExtractsForm'].elements['bookingAgentCode'].value=='') {
	       alert("Please enter the Booking Agent Code"); 
	     	return false;
	      } 
	     else if(document.forms['omniReportExtractsForm'].elements['jobTypes'].value=='') {
	 		       alert("Please enter the Job Type"); 
	 		     	return false;
	 		      } 
	       else {
	       var agree= confirm("This process is not reversible, please confirm that you want to proceed? Hit OK to proceed and Cancel to cancel.");
            if(agree){
	       document.forms['omniReportExtractsForm'].submit();
	       return true;
	       } else {
            return false;
           }
	       }
       
       }
function fieldValidate(){
        if(document.forms['omniReportExtractsForm'].elements['endDate'].value==''){
	       alert("Please enter the end date "); 
	       	return false;
	       
	       } else if(document.forms['omniReportExtractsForm'].elements['companyCode'].value=='')  {
	       alert("Please enter the Company Code"); 
	     	return false;
	     		} else if(document.forms['omniReportExtractsForm'].elements['bookingAgentCode'].value=='') {
	       alert("Please enter the Booking Agent Code"); 
	     	return false;
	      } else if(document.forms['omniReportExtractsForm'].elements['jobTypes'].value=='') {
		       alert("Please enter the Job Type"); 
		     	return false;
		      } 
	       else {
	       document.forms['omniReportExtractsForm'].submit();
	       return true;
	       }
       
       } 
       
       function fieldValidateByOmni(){
        if(document.forms['omniReportExtractsForm'].elements['endDate'].value==''){
	       alert("Please enter the end date "); 
	       	return false;
	       	}else if(document.forms['omniReportExtractsForm'].elements['jobTypes'].value=='') {
	 		       alert("Please enter the Job Type"); 
	 		     	return false;
	 		      }  else {
	       document.forms['omniReportExtractsForm'].submit();
	       return true;
	       }
       
       }  
       
 var http4 = getHTTPObject();
	function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
</script>
</head>

<div id="Layer1" style="width:100%">
<s:form name="omniReportExtractsForm" id="omniReportExtractsForm" action="omniReportExtracts" method="post" validate="true"  cssClass="form_magn">
<s:hidden name="jobTypes" value="INT, GST, JVS, EUR"/>
	<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="dd-NNN-yy"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="dd-NNN-yy"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>


<div id="otabs">
	<ul>
	<li><a class="current"><span>OMNI Report Extract</span></a></li>
	</ul>
</div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:12px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table  border="0" >
	<tbody>	
	<tr><td height="5"></td></tr>
	<tr>
		<td width="5px"></td> 
	   	<td class="listwhitetext" align="right" width="90px">End&nbsp;Date<font color="red" size="2">*</font></td>
		<c:if test="${not empty endDate}">
			<s:text id="endDate" name="${FormDateValue}"> <s:param name="value" value="endDate" /></s:text>
			<td width="150px"><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" cssStyle="width:95px;" maxlength="11" readonly="true" /><img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<c:if test="${empty endDate}" >
			<td width="150px"><s:textfield cssClass="input-text" id="endDate" name="endDate" required="true" cssStyle="width:95px;" maxlength="11" readonly="true" /><img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>	
		<td class="listwhitetext" align="right">Company&nbsp;Division<font color="red" size="2">*</font></td>
		<td><s:select cssClass="list-menu" cssStyle="width:100%;"  name="companyCode" list="%{companyCodeList}"  onchange="return findBookingAgentCode();" headerKey="" headerValue="" /></td>
		<%-- <td  class="listwhitetext" align="right" >Job&nbsp;Type<font color="red" size="2">*</font></td>
		<td><s:select cssClass="list-menu" cssStyle="width:175px;height:50px"  name="jobTypes" list="%{jobtype}"   value="%{multiplejobType}" multiple="true"  /></td> --%>
	</tr>
	
	<tr>
		<td colspan="3"></td>		
		<td class="listwhitetext" align="right">Booking&nbsp;Agent&nbsp;Code<font color="red" size="2">*</font></td>
	  	<td><s:select cssClass="list-menu" cssStyle="width:100%;"  name="bookingAgentCode" list="%{bookingAgentList}"   headerKey="" headerValue="" /></td>
		</tr>		
		<tr><td height="10"></td></tr>
		<tr>	
	  		<td width="5px"></td>
	  		<td colspan="3"></td>		
			<td colspan=""><s:submit cssClass="cssbutton" cssStyle="width:100%;"  method="omniReport" value="Extract" onclick=" return fieldValidate();"/></td>
	   </tr>
	   <tr>	
	  		<td width="5px"></td>
	  		<td colspan="3"></td>	
	    		<td colspan="2"><s:submit cssClass="cssbutton" cssStyle="width:210px;"  method="omniReportUpdate" value="Extract and Update OMNI Date" onclick=" return fieldValidates();"/></td>
	   </tr>
	   <tr>	
	  		<td width="5px"></td>
	  		<td colspan="3"></td>					
			<td colspan=""><s:submit cssClass="cssbutton" cssStyle="width:210px;"  method="omniReportByOmni" value="Extract Data for a given OMNI Date" onclick=" return fieldValidateByOmni();"/></td>
	   </tr>	
        <tr><td height="20"></td></tr>
      
	</tbody></table>
 </div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>

</div>


<%-- <table class="mainDetailTable" cellspacing="1" cellpadding="3" border="0" style="width:610px">
	<div class="subcontent-tab" style="border:2px solid #74B3DC; border-bottom:none">OMNI Report Extract</div>
	  		<tr>
	  			<td width="50px"></td>
	  			<td width="70px"></td>
	  			<td width="30px"></td>
	  			<td width="70px"></td>
	  		</tr>
  			<tr> 
				<!--<td class="listwhitetext" align="right">Begin Date<font color="red" size="2">*</font></td>
					<c:if test="${not empty beginDate}">
						<s:text id="beginDate" name="${FormDateValue}"><s:param name="value" value="beginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" value="%{beginDate}" size="10" maxlength="11" readonly="true" /> <img id="calender3" align="top" src="${pageContext.request.contextPath}/images/calender.png"HEIGHT=20 WIDTH=20onclick=" onclick="cal.select(document.forms['omniReportExtractsForm'].beginDate,'calender3',document.forms['omniReportExtractsForm'].dateFormat.value); return false;" /></td>
					</c:if>
					<c:if test="${empty beginDate}">
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" required="true" size="10" maxlength="11" readonly="true" /> <img id="calender3" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['omniReportExtractsForm'].beginDate,'calender3',document.forms['omniReportExtractsForm'].dateFormat.value); return false;" /></td>
					</c:if>
				      	
				    --><td class="listwhitetext" align="right">End Date<font color="red" size="2">*</font></td>
		 			<c:if test="${not empty endDate}">
						<s:text id="endDate" name="${FormDateValue}"> <s:param name="value" value="endDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" size="10" maxlength="11" readonly="true" /> <img id="calender4" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['omniReportExtractsForm'].endDate,'calender4',document.forms['omniReportExtractsForm'].dateFormat.value); return false;" /></td>
					</c:if>
					<c:if test="${empty endDate}" >
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" required="true" size="10" maxlength="11" readonly="true" /> <img id="calender4" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['omniReportExtractsForm'].endDate,'calender4',document.forms['omniReportExtractsForm'].dateFormat.value); return false;" /></td>
					</c:if>		 	
			</tr> 
			<tr>
	  			<td width="50px"></td>
	  			<td width="70px"></td>
	  			<td width="30px"></td>
	  			<td width="70px"></td>
	  		</tr> 
	  		<tr>	
	  			<td width="50px"></td>			
				<td colspan="4"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="omniReport" value="Extract" onclick=" return fieldValidate();"/></td>
	    	</tr>	 				
	</table>       
--%>
<s:text id="customerFileMoveDateFormattedValue" name="FormDateValue"><s:param name="value" value="refMaster.stopDate"/></s:text>
<s:hidden name="refMaster.stopDate" />
</s:form>
<script type="text/javascript"> 
    Form.focusFirstElement($("refMasterForm")); 
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>