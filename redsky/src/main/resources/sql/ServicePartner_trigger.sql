
DELIMITER $$
DROP trigger IF EXISTS `redsky`.`trigger_add_history_servicepartner` $$
create trigger trigger_add_history_servicepartner before update on redsky.servicepartner
for each row

BEGIN

   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;
   DECLARE updatedon DATETIME;
  set NEW.updatedon=now();
   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";
      
     IF ((OLD.atArrival <> NEW.atArrival) or (OLD.atArrival is null and NEW.atArrival is not null)
	  or (OLD.atArrival is not null and NEW.atArrival is null)) THEN
	    select CONCAT(fieldNameList,'servicepartner.atArrival~') into fieldNameList;
	    
	    IF(OLD.atArrival is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.atArrival is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.atArrival is not null) then
        	select CONCAT(oldValueList,OLD.atArrival,'~') into oldValueList;
     	END IF;
      	IF(NEW.atArrival is not null) then
        	select CONCAT(newValueList,NEW.atArrival,'~') into newValueList;
      	END IF;
      	
	END IF;
	
	
	
	
	IF ((OLD.atDepart <> NEW.atDepart) or (OLD.atDepart is null and NEW.atDepart is not null)
	  or (OLD.atDepart is not null and NEW.atDepart is null)) THEN
	    select CONCAT(fieldNameList,'servicepartner.atDepart~') into fieldNameList;
	    
	    IF(OLD.atDepart is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.atDepart is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.atDepart is not null) then
        	select CONCAT(oldValueList,OLD.atDepart,'~') into oldValueList;
     	END IF;
      	IF(NEW.atDepart is not null) then
        	select CONCAT(newValueList,NEW.atDepart,'~') into newValueList;
      	END IF;
      	
	END IF;

	IF ((OLD.revisedETD <> NEW.revisedETD) or (OLD.revisedETD is null and NEW.revisedETD is not null)
	  or (OLD.revisedETD is not null and NEW.revisedETD is null)) THEN
	    select CONCAT(fieldNameList,'servicepartner.revisedETD~') into fieldNameList;
	    
	    IF(OLD.revisedETD is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.revisedETD is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.revisedETD is not null) then
        	select CONCAT(oldValueList,OLD.revisedETD,'~') into oldValueList;
     	END IF;
      	IF(NEW.revisedETD is not null) then
        	select CONCAT(newValueList,NEW.revisedETD,'~') into newValueList;
      	END IF;
      	
	END IF;
	
	IF ((OLD.revisedETA <> NEW.revisedETA) or (OLD.revisedETA is null and NEW.revisedETA is not null)
	  or (OLD.revisedETA is not null and NEW.revisedETA is null)) THEN
	    select CONCAT(fieldNameList,'servicepartner.revisedETA~') into fieldNameList;
	    
	    IF(OLD.revisedETA is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.revisedETA is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.revisedETA is not null) then
        	select CONCAT(oldValueList,OLD.revisedETA,'~') into oldValueList;
     	END IF;
      	IF(NEW.revisedETA is not null) then
        	select CONCAT(newValueList,NEW.revisedETA,'~') into newValueList;
      	END IF;
      	
	END IF;
	
	
	IF ((OLD.etArrival <> NEW.etArrival) or (OLD.etArrival is null and NEW.etArrival is not null)
	  or (OLD.etArrival is not null and NEW.etArrival is null)) THEN
	    select CONCAT(fieldNameList,'servicepartner.etArrival~') into fieldNameList;
	    
	    IF(OLD.etArrival is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.etArrival is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.etArrival is not null) then
        	select CONCAT(oldValueList,OLD.etArrival,'~') into oldValueList;
     	END IF;
      	IF(NEW.etArrival is not null) then
        	select CONCAT(newValueList,NEW.etArrival,'~') into newValueList;
      	END IF;
      	
	END IF;
	
	
	
	IF ((OLD.etDepart <> NEW.etDepart) or (OLD.etDepart is null and NEW.etDepart is not null)
	  or (OLD.etDepart is not null and NEW.etDepart is null)) THEN
	    select CONCAT(fieldNameList,'servicepartner.etDepart~') into fieldNameList;
	    
	    IF(OLD.etDepart is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.etDepart is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.etDepart is not null) then
        	select CONCAT(oldValueList,OLD.etDepart,'~') into oldValueList;
     	END IF;
      	IF(NEW.etDepart is not null) then
        	select CONCAT(newValueList,NEW.etDepart,'~') into newValueList;
      	END IF;
      	
	END IF;

 IF (OLD.email <> NEW.email ) THEN
       select CONCAT(fieldNameList,'servicepartner.email~') into fieldNameList;
       select CONCAT(oldValueList,OLD.email,'~') into oldValueList;
       select CONCAT(newValueList,NEW.email,'~') into newValueList;
   END IF;


 IF (OLD.corpID <> NEW.corpID ) THEN
       select CONCAT(fieldNameList,'servicepartner.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
       select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
   END IF;






 IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
       select CONCAT(fieldNameList,'servicepartner.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
   END IF;



 IF (OLD.shipNumber <> NEW.shipNumber ) THEN
       select CONCAT(fieldNameList,'servicepartner.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
   END IF;



 IF (OLD.city <> NEW.city ) THEN
       select CONCAT(fieldNameList,'servicepartner.city~') into fieldNameList;
       select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
       select CONCAT(newValueList,NEW.city,'~') into newValueList;
   END IF;



 IF (OLD.miles <> NEW.miles ) THEN
       select CONCAT(fieldNameList,'servicepartner.miles~') into fieldNameList;
       select CONCAT(oldValueList,OLD.miles,'~') into oldValueList;
       select CONCAT(newValueList,NEW.miles,'~') into newValueList;
   END IF;


 IF (OLD.bookNumber <> NEW.bookNumber ) THEN
       select CONCAT(fieldNameList,'servicepartner.bookNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.bookNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.bookNumber,'~') into newValueList;
   END IF;


IF (OLD.carrierClicd <> NEW.carrierClicd ) THEN
       select CONCAT(fieldNameList,'servicepartner.carrierClicd~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierClicd,'~') into oldValueList;
       select CONCAT(newValueList,NEW.carrierClicd,'~') into newValueList;
   END IF;


IF (OLD.carrierCode <> NEW.carrierCode ) THEN
       select CONCAT(fieldNameList,'servicepartner.carrierCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.carrierCode,'~') into newValueList;
   END IF;


IF (OLD.carrierName <> NEW.carrierName ) THEN
       select CONCAT(fieldNameList,'servicepartner.carrierName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.carrierName,'~') into newValueList;
   END IF;

IF (OLD.carrierNumber <> NEW.carrierNumber ) THEN
       select CONCAT(fieldNameList,'servicepartner.carrierNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.carrierNumber,'~') into newValueList;
   END IF;



IF (OLD.carrierPhone <> NEW.carrierPhone ) THEN
       select CONCAT(fieldNameList,'servicepartner.carrierPhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierPhone,'~') into oldValueList;
       select CONCAT(newValueList,NEW.carrierPhone,'~') into newValueList;
   END IF;


IF (OLD.carrierVessels <> NEW.carrierVessels ) THEN
       select CONCAT(fieldNameList,'servicepartner.carrierVessels~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierVessels,'~') into oldValueList;
       select CONCAT(newValueList,NEW.carrierVessels,'~') into newValueList;
   END IF;


IF (OLD.ETATA <> NEW.ETATA ) THEN
       select CONCAT(fieldNameList,'servicepartner.ETATA~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ETATA,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ETATA,'~') into newValueList;
   END IF;

IF (OLD.ETDTA <> NEW.ETDTA ) THEN
       select CONCAT(fieldNameList,'servicepartner.ETDTA~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ETDTA,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ETDTA,'~') into newValueList;
   END IF;


IF (OLD.omni <> NEW.omni ) THEN
       select CONCAT(fieldNameList,'servicepartner.omni~') into fieldNameList;
       select CONCAT(oldValueList,OLD.omni,'~') into oldValueList;
       select CONCAT(newValueList,NEW.omni,'~') into newValueList;
   END IF;




 IF ((OLD.pdCost1 <> NEW.pdCost1) or (OLD.pdCost1 is null and NEW.pdCost1 is not null) 
    or (OLD.pdCost1 is not null and NEW.pdCost1 is null)) THEN
    select CONCAT(fieldNameList,'servicepartner.pdCost1~') into fieldNameList;
    IF(OLD.pdCost1 is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.pdCost1 is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.pdCost1 is not null) then
      select CONCAT(oldValueList,OLD.pdCost1,'~') into oldValueList;
    END IF;
    IF(NEW.pdCost1 is not null) then
      select CONCAT(newValueList,NEW.pdCost1,'~') into newValueList;
    END IF; 
  END IF;


 IF ((OLD.pdCost2 <> NEW.pdCost2) or (OLD.pdCost2 is null and NEW.pdCost2 is not null)
    or (OLD.pdCost2 is not null and NEW.pdCost2 is null)) THEN
    select CONCAT(fieldNameList,'servicepartner.pdCost2~') into fieldNameList;
    IF(OLD.pdCost2 is null) THEN
      select CONCAT(oldValueList,"0",'~') into oldValueList;
    END IF;
    IF(NEW.pdCost2 is null) THEN
      select CONCAT(newValueList,"0",'~') into newValueList;
    END IF;

    IF(OLD.pdCost2 is not null) then
      select CONCAT(oldValueList,OLD.pdCost2,'~') into oldValueList;
    END IF;
    IF(NEW.pdCost2 is not null) then
      select CONCAT(newValueList,NEW.pdCost2,'~') into newValueList;
    END IF; 
  END IF;



IF (OLD.pdDays2 <> NEW.pdDays2 ) THEN
       select CONCAT(fieldNameList,'servicepartner.pdDays2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pdDays2,'~') into oldValueList;
       select CONCAT(newValueList,NEW.pdDays2,'~') into newValueList;
   END IF;



IF (OLD.pdFreedays <> NEW.pdFreedays ) THEN
       select CONCAT(fieldNameList,'servicepartner.pdFreedays~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pdFreedays,'~') into oldValueList;
       select CONCAT(newValueList,NEW.pdFreedays,'~') into newValueList;
   END IF;


IF (OLD.carrierArrival <> NEW.carrierArrival ) THEN
       select CONCAT(fieldNameList,'servicepartner.carrierArrival~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierArrival,'~') into oldValueList;
       select CONCAT(newValueList,NEW.carrierArrival,'~') into newValueList;
   END IF;



IF (OLD.carrierDeparture <> NEW.carrierDeparture ) THEN
       select CONCAT(fieldNameList,'servicepartner.carrierDeparture~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierDeparture,'~') into oldValueList;
       select CONCAT(newValueList,NEW.carrierDeparture,'~') into newValueList;
   END IF;

IF (OLD.coord <> NEW.coord ) THEN
       select CONCAT(fieldNameList,'servicepartner.coord~') into fieldNameList;
       select CONCAT(oldValueList,OLD.coord,'~') into oldValueList;
       select CONCAT(newValueList,NEW.coord,'~') into newValueList;
   END IF;



IF (OLD.partnerType <> NEW.partnerType ) THEN
       select CONCAT(fieldNameList,'servicepartner.partnerType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.partnerType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.partnerType,'~') into newValueList;
   END IF;


IF (OLD.dropPick <> NEW.dropPick ) THEN
	    select CONCAT(fieldNameList,'servicepartner.dropPick~') into fieldNameList;

	    IF(OLD.dropPick = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.dropPick = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.dropPick = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.dropPick = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
END IF;


IF (OLD.liveLoad <> NEW.liveLoad ) THEN
	    select CONCAT(fieldNameList,'servicepartner.liveLoad~') into fieldNameList;

	    IF(OLD.liveLoad = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.liveLoad = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.liveLoad = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.liveLoad = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
END IF;


IF (OLD.blNumber <> NEW.blNumber ) THEN
       select CONCAT(fieldNameList,'servicepartner.blNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.blNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.blNumber,'~') into newValueList;
   END IF;

 IF (OLD.transhipped <> NEW.transhipped ) THEN
	    select CONCAT(fieldNameList,'servicepartner.transhipped~') into fieldNameList;

	    IF(OLD.transhipped = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.transhipped = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.transhipped = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.transhipped = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
END IF;


IF (OLD.polCode <> NEW.polCode ) THEN
       select CONCAT(fieldNameList,'servicepartner.polCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.polCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.polCode,'~') into newValueList;
   END IF;


IF (OLD.poeCode <> NEW.poeCode ) THEN
       select CONCAT(fieldNameList,'servicepartner.poeCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.poeCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.poeCode,'~') into newValueList;
   END IF;

IF (OLD.actgCode <> NEW.actgCode ) THEN
       select CONCAT(fieldNameList,'servicepartner.actgCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.actgCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.actgCode,'~') into newValueList;
   END IF;




IF (OLD.serviceOrderId <> NEW.serviceOrderId ) THEN
       select CONCAT(fieldNameList,'servicepartner.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.serviceOrderId,'~') into newValueList;
   END IF;


IF (OLD.houseBillIssued <> NEW.houseBillIssued ) THEN
       select CONCAT(fieldNameList,'servicepartner.houseBillIssued~') into fieldNameList;
       select CONCAT(oldValueList,OLD.houseBillIssued,'~') into oldValueList;
       select CONCAT(newValueList,NEW.houseBillIssued,'~') into newValueList;
   END IF;


IF (OLD.cntnrNumber <> NEW.cntnrNumber ) THEN
       select CONCAT(fieldNameList,'servicepartner.cntnrNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cntnrNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.cntnrNumber,'~') into newValueList;
   END IF;


IF (OLD.aesNumber <> NEW.aesNumber ) THEN
       select CONCAT(fieldNameList,'servicepartner.aesNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.aesNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.aesNumber,'~') into newValueList;
   END IF;




IF (OLD.skids <> NEW.skids ) THEN
       select CONCAT(fieldNameList,'servicepartner.skids~') into fieldNameList;
       select CONCAT(oldValueList,OLD.skids,'~') into oldValueList;
       select CONCAT(newValueList,NEW.skids,'~') into newValueList;
   END IF;


IF (OLD.clone <> NEW.clone ) THEN
       select CONCAT(fieldNameList,'servicepartner.clone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.clone,'~') into oldValueList;
       select CONCAT(newValueList,NEW.clone,'~') into newValueList;
   END IF;


IF (OLD.servicePartnerId <> NEW.servicePartnerId ) THEN
       select CONCAT(fieldNameList,'servicepartner.servicePartnerId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.servicePartnerId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.servicePartnerId,'~') into newValueList;
   END IF;

   IF (OLD.status <> NEW.status ) THEN
      select CONCAT(fieldNameList,'servicepartner.status~') into fieldNameList;
      IF(OLD.status = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.status = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(OLD.status is null) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(NEW.status = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.status = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
       IF(NEW.status is null) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
END IF;

  IF (OLD.ugwIntId <> NEW.ugwIntId ) THEN
       select CONCAT(fieldNameList,'servicepartner.ugwIntId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ugwIntId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ugwIntId,'~') into newValueList;
   END IF;
   
   
   IF (OLD.houseBillNumber <> NEW.houseBillNumber ) THEN
       select CONCAT(fieldNameList,'servicepartner.houseBillNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.houseBillNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.houseBillNumber,'~') into newValueList;
   END IF;
   
   IF (OLD.caed <> NEW.caed ) THEN
       select CONCAT(fieldNameList,'servicepartner.caed~') into fieldNameList;
       select CONCAT(oldValueList,OLD.caed,'~') into oldValueList;
       select CONCAT(newValueList,NEW.caed,'~') into newValueList;
   END IF;
		call ServiceOrder_UpdateStatus(OLD.shipnumber,NEW.updatedBy);
		update serviceorder set isSOExtract=false,updatedBy = NEW.updatedBy,updatedOn = now() where id=NEW.serviceorderid;
   		CALL add_tblHistory (OLD.id,"servicepartner", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END
$$



