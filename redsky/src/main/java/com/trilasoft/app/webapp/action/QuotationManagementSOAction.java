package com.trilasoft.app.webapp.action;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.PricingDetailsDTO;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Agent;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.Contract;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DefaultAccountLine;
import com.trilasoft.app.model.DsAutomobileAssistance;
import com.trilasoft.app.model.DsColaService;
import com.trilasoft.app.model.DsCrossCulturalTraining;
import com.trilasoft.app.model.DsHomeFindingAssistancePurchase;
import com.trilasoft.app.model.DsHomeRental;
import com.trilasoft.app.model.DsLanguage;
import com.trilasoft.app.model.DsMoveMgmt;
import com.trilasoft.app.model.DsOngoingSupport;
import com.trilasoft.app.model.DsPreviewTrip;
import com.trilasoft.app.model.DsRelocationAreaInfoOrientation;
import com.trilasoft.app.model.DsRelocationExpenseManagement;
import com.trilasoft.app.model.DsRepatriation;
import com.trilasoft.app.model.DsSchoolEducationalCounseling;
import com.trilasoft.app.model.DsTaxServices;
import com.trilasoft.app.model.DsTemporaryAccommodation;
import com.trilasoft.app.model.DsTenancyManagement;
import com.trilasoft.app.model.DsVisaImmigration;
import com.trilasoft.app.model.DspDetails;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.OperationsIntelligence;
import com.trilasoft.app.model.PricingDetailsValue;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServiceOrderDashboard;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.Storage;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.AdAddressesDetailsManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ContractManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DefaultAccountLineManager;
import com.trilasoft.app.service.DsAutomobileAssistanceManager;
import com.trilasoft.app.service.DsColaServiceManager;
import com.trilasoft.app.service.DsCrossCulturalTrainingManager;
import com.trilasoft.app.service.DsHomeFindingAssistancePurchaseManager;
import com.trilasoft.app.service.DsHomeRentalManager;
import com.trilasoft.app.service.DsLanguageManager;
import com.trilasoft.app.service.DsMoveMgmtManager;
import com.trilasoft.app.service.DsOngoingSupportManager;
import com.trilasoft.app.service.DsPreviewTripManager;
import com.trilasoft.app.service.DsRelocationAreaInfoOrientationManager;
import com.trilasoft.app.service.DsRelocationExpenseManagementManager;
import com.trilasoft.app.service.DsRepatriationManager;
import com.trilasoft.app.service.DsSchoolEducationalCounselingManager;
import com.trilasoft.app.service.DsTaxServicesManager;
import com.trilasoft.app.service.DsTemporaryAccommodationManager;
import com.trilasoft.app.service.DsTenancyManagementManager;
import com.trilasoft.app.service.DsVisaImmigrationManager;
import com.trilasoft.app.service.DspDetailsManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.ExchangeRateManager;
import com.trilasoft.app.service.ExpressionManager;
import com.trilasoft.app.service.ItemsJbkEquipManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.OperationsIntelligenceManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.webapp.json.JSONArray;
import com.trilasoft.app.webapp.json.JSONException;
import com.trilasoft.app.webapp.json.JSONObject;
import com.trilasoft.mobilemover.service.SyncServiceStub.Decimal;
import com.trilasoft.pricepoint.wsclient.PricePointClientServiceCall;

public class QuotationManagementSOAction extends BaseAction implements Preparable{
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	
	StringBuilder systemDate = new StringBuilder(format.format(new Date()));
	
	/*private String vendorCode;
	private String vendorName;
	private String basis1;
	private String estQuantity;
	private String estRate;
	private String estSellRate;
	private String estExpense;
	private String estPassPercentage;
	private String estRevenueAmt;
	private String quoteDescription;
	
	private Boolean displayOnQuote;
	private Boolean additionalService;
	private Boolean statusCheck;*/
	
	
	private ServiceOrderManager serviceOrderManager;
	
	private ClaimManager claimManager;
	
	private ContractManager contractManager;
	
	private CustomerFileManager customerFileManager;

	private TrackingStatusManager trackingStatusManager;

	private ServicePartnerManager servicePartnerManager;
	
	private PartnerManager partnerManager;
	
	 private ChargesManager chargesManager;
	
	private NotesManager notesManager;	
	
	private RefMasterManager refMasterManager;
	 
	private DsPreviewTrip dsPreviewTrip;
	
	private DsPreviewTripManager dsPreviewTripManager;
	
	private DsTemporaryAccommodation dsTemporaryAccommodation;
		
	private DsTemporaryAccommodationManager dsTemporaryAccommodationManager ;
		
	private DsSchoolEducationalCounseling dsSchoolEducationalCounseling;
	private AdAddressesDetailsManager adAddressesDetailsManager;	
	private DsSchoolEducationalCounselingManager  dsSchoolEducationalCounselingManager ;
	private PartnerPrivateManager partnerPrivateManager;
	private DsColaService dsColaService;
	private DsColaServiceManager dsColaServiceManager;
	private Boolean costElementFlag;	
	private DsOngoingSupport dsOngoingSupport;
		
	private DsOngoingSupportManager  dsOngoingSupportManager ;
		
	private DsTenancyManagement dsTenancyManagement;
	
	private DsTenancyManagementManager  dsTenancyManagementManager ;

	private DsMoveMgmt dsMoveMgmt;
	
	private DsMoveMgmtManager  dsMoveMgmtManager ;

	private DsAutomobileAssistance dsAutomobileAssistance;
	
	private DsAutomobileAssistanceManager  dsAutomobileAssistanceManager ;
	
	// Modification done as per "QuotationManagementAction.java" by Kunal
	private static Map<String, String> countryCod;
	
	private DsRelocationExpenseManagement dsRelocationExpenseManagement;
		
	private DsRelocationExpenseManagementManager  dsRelocationExpenseManagementManager ; 
		
	private DsHomeFindingAssistancePurchase dsHomeFindingAssistancePurchase;
		
	private DsHomeFindingAssistancePurchaseManager  dsHomeFindingAssistancePurchaseManager; 
		
	private DsVisaImmigration dsVisaImmigration;
		
	private DsVisaImmigrationManager dsVisaImmigrationManager;
		
	private DsTaxServicesManager dsTaxServicesManager;
		
	private DsHomeRental dsHomeRental;
		
	private DsHomeRentalManager dsHomeRentalManager;
		
	private DsLanguage dsLanguage;
		
	private DsLanguageManager dsLanguageManager; 
		
	private DsRepatriation dsRepatriation;
		
	private DsRepatriationManager dsRepatriationManager; 
		
	private DsCrossCulturalTraining dsCrossCulturalTraining;
		
	private DsCrossCulturalTrainingManager dsCrossCulturalTrainingManager;
		
	private DsRelocationAreaInfoOrientation dsRelocationAreaInfoOrientation;
		
	private DsRelocationAreaInfoOrientationManager dsRelocationAreaInfoOrientationManager;
		
	private  DsTaxServices dsTaxServices;
	
	private CompanyManager companyManager;

	private Date sysdate = new Date();

	private CustomerFile customerFile;
	
	private CustomerFile customerFileTemp;
	
	private ServiceOrder serviceOrderTemp;
	
	private Set customerServiceOrders;

	private ServiceOrder serviceOrder;

	private ServicePartner servicePartner;

	private SystemDefault systemDefault;

	private WorkTicket workTicket;

	private AccountLine accountLine;
	
	private AccountLine synchedAccountLine;
	
	private boolean accNonEditFlag=false; 
	private boolean utsiRecAccDateFlag=false;
	private boolean utsiPayAccDateFlag=false;
	private Storage storage;
	
	private String companies;

	private Long id;

	private Long i;

	private Long autoShip;

	private Long prevShip;

	private String shipnumber;

	private Set serviceOrders;

	private List workTickets;

	private Set storages;

	private List maxShip;

	private String ship;
	private String weightType;
	private String shipPrev;
	
    private List companyDivis=new ArrayList();

	private List servicePartners;

	private List servicePartnerss;

	private List servicePartnersPopup;

	private List refMasters;

	private List checkShipNumberList;

	private Miscellaneous miscellaneous;

	private Container container;

	private Carton carton;

	private Vehicle vehicle;

	private Agent agent;
	private Long cid; 
	private Long customerFileId;
	private String countOriginDetailNotes;

	private String countDestinationDetailNotes;

	private String countWeightDetailNotes;

	private String contract;
	
	private String chargeCode;
	
	private List quotationDiscriptionList= new ArrayList();
	
	private String countVipDetailNotes;
	private String quotationContract;
	private String quotationBillToCode;
	private String countServiceOrderNotes;

	private static Map<String, String> state;

	private static Map<String, String> ocountry;

	private static Map<String, String> dcountry;

	private Map<String, String> job;

	private Map<String, String> routing;

	private Map<String, String> commodit;

	private Map<String, String> commodits;

	private List mode;

	private Map<String, String> service;
	private Map<String, String> reloService;
	private Map<String, String> pkmode;

	private Map<String, String> loadsite;

	private Map<String, String> specific;

	private Map<String, String> jobtype;

	private Map<String, String> military;

	private Map<String, String> tcktservc;

	private Map<String, String> dept;

	private Map<String, String> house;

	private Map<String, String> confirm;

	public Map<String, String> tcktactn;

	private Map<String, String> woinstr;

	private Map<String, String> paytype;

	private Map<String, String> yesno;

	private Map<String, String> glcodes;

	private Map<String, String> isdriver;

	private Map<String, String> partnerType;

	private Map<String, String> SITOUTTA;

	private Map<String, String> omni;

	private Map<String, String> QUOTESTATUS;

	private Map<String, String> Special;

	private Map<String, String> Peak;

	private Map<String, String> Howpack;

	private Map<String, String> AGTPER;

	private Map<String, String> EQUIP;

	private  Map<String, String> sale = new LinkedHashMap<String, String>();

	private  Map<String, String> qc = new LinkedHashMap<String, String>();


	private  Map<String, String> coord = new LinkedHashMap<String, String>();

	private List weightunits;

	private List volumeunits;

	private List lengthunits;

	private Map<String, String> JOB_STATUS;
	
	private Map<String, String> statusReason;

	private List claims;

	private TrackingStatus trackingStatus;

	private List containers;

	private List cartons;

	private List vehicles;

	private Long sid;
	private Long aid;
	private Long pid;
	private String buttonType;

	private List accountLineList;

	private List accountList;

	private ArrayList containerWeightList;

	private ArrayList containerVolumeWeightList;

	private ArrayList containerTareWeightList;

	private ArrayList containerNetWeightList;

	private Map<String, String> category;

	private Map<String, String> basis;

	private Map<String, String> country;

	private Map<String, String> payingStatus;
	
	private static Map<String, String> ostates;
	
    private static Map<String, String> dstates;
    
    private List contracts = new ArrayList();
    
    private DefaultAccountLineManager defaultAccountLineManager;
    
    private AccountLineManager accountLineManager;
    
    private String jobtypeSO;
    
    private String routingSO;
    
    private String modeSO;
    private String packingModeSO;
    private String commoditySO;
    private String serviceTypeSO;
    private String accountIdCheck;
    private String hitFlag;
    private List maxLineNumber;
    
	private String accountLineNumber;
	
	private Long autoLineNumber;
	
	private String accountLineStatus;
	
	private List vendorNameList=new ArrayList();
	 
	//private static String oldStatus;
	 
	private String shipSize;
	
	private Billing billing;
	private String multiCurrency;
	private String baseCurrencyCompanyDivision="";	
	private CompanyDivisionManager companyDivisionManager;
	private  ExchangeRateManager exchangeRateManager;
	private List accExchangeRateList;
	
	private ServiceOrder addCopyServiceOrder;
	private Miscellaneous addCopyMiscellaneous;
	private String aidListIds; 
	private List<SystemDefault> sysDefaultDetail;
	private List packingServiceList;
	private Map<String, String> originAddress;
	private Map<String, String> destinationAddress;
	private String salesCommisionRate;
	private String grossMarginThreshold;
	private String accountInterface;
	private boolean contractType=false;
	private String sessionCorpID;
	private String accCorpID;
	private String accountLineBasis;
	private String accountLineEstimateQuantity;
	private String estCurrency;
	private String estValueDate;
	private String estExchangeRate;
	private String estLocalAmount;
	private String estLocalRate;

	private String estimatePayableContractCurrency;
	private String estimatePayableContractValueDate;
	private String estimatePayableContractExchangeRate;
	private String estimatePayableContractRateAmmount;
	private String estimatePayableContractRate;
	
	
	private String estSellCurrency;
	private String estSellValueDate;
	private String estSellExchangeRate;
	private String estSellLocalAmount;
	private String estSellLocalRate;
	private String estimateContractCurrency;
	private String estimateContractValueDate;
	private String estimateContractExchangeRate;
	private String estimateContractRate;
	private String estimateContractRateAmmount;	
	
	private String usertype;
	private String roleHitFlag;
	private String soid;
	private String requestedAQP;
	private int quoteAcceptance;
	private  Map<String,String>estVatList;
	private  Map<String,String>payVatList;
	private  Map<String,String>estVatPersentList;
	private  Map<String,String>euVatPercentList;
	private  Map<String,String>payVatPercentList;
    private String estVatFlag;
    private String defaultTemplate;
    private String enbState;
    
//	voxmecodestart
	private Company company; 
	private String voxmeIntergartionFlag;
	
	private int dayCount;
	private String shipNumber;
	private Map<String, String> resourceCategory;
	private ItemsJbkEquipManager itemsJbkEquipManager;
	private Map<String, List> resourceMap;
	private Map<String, Map> resourceMap1;
	private String jobType;
	private String routing1;
	private String disableChk;
	private String companyDivision;
	private Map<String, String> currencyExchangeRate;
	// resource Template
	private List resourceList = new ArrayList();
	private boolean billingCMMContractType=false;
	private boolean billingDMMContractType=false;
	private boolean networkAgent=false;
	 private String returnAjaxStringValue;
	 private String minShip;
	 private String countShip;
	 private Boolean usaFlag;
	 private Map<String, String> flagCarrierList;
	private Map<String, String> languageList;
	 private  String companyDivisionAcctgCodeUnique;
	 private boolean pricePointFlag=false;
	 private String marketCountryCode;
	 private Map marketPlaceList=new HashMap();
	 private Map<String, String> marketAreaList= new HashMap<String, String>();
	 
	 private String pricingMode;
	 private String pricingWeight;
	 private String pricingVolume;
	 private String pricingStorageUnit;
	 private String marketArea;
	 private String packService;
	 private String packingModePricing;
	 private boolean pricePointexpired=false;
	 private boolean accountLineAccountPortalFlag=false;
	 private Date pricePointUserStartDate;
	 private String checkContractChargesMandatory= "0";

	 private String userQuoteServices;

	 private Map<String, String> quoteAcceptReason;
	 
	 private ErrorLogManager errorLogManager;
	 private Boolean ignoreForBilling=false;
	 private List chargeCodeList1;
	 private Map<String, String> countryDsc;
	 private String commodityForUser;
	 private List resourceListForOI;
	 private OperationsIntelligenceManager operationsIntelligenceManager;
	 private String compDivFlag;
     private String oiJobList;
	 Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(QuotationManagementSOAction.class);
	
    public QuotationManagementSOAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		usertype=user.getUserType();
		this.sessionCorpID = user.getCorpID();
		 discountUserFlag=user;
		 pricePointUserStartDate=user.getPricePointStartDate();
		 commodityForUser = user.getCommodity();
	        if(commodityForUser==null)
	        {
	        	commodityForUser="";
	        }
	}

	public String saveOnTabChange() throws Exception {
	       //if (option enabled for this company in the system parameters table then call save) {
		validateFormNav = "OK";
	    String s = save();    // else simply navigate to therequested page)
	    return gotoPageString;
	} 
	
	public String resouceTemplate(){
		
		return SUCCESS;
	}
	public void prepare() throws Exception {
		try {
			logger.warn(" AJAX Call : "+getRequest().getParameter("ajax"));  
			accCorpID=sessionCorpID;
			if(getRequest().getParameter("ajax") == null){  
			country = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			estimatorList = refMasterManager.findUser(sessionCorpID,"ROLE_SALE");
			coordinatorList = refMasterManager.findUser(sessionCorpID,"ROLE_COORD");
			statusReason = refMasterManager.findByParameter(sessionCorpID, "STATUSREASON");
			quoteAcceptReason=refMasterManager.findByParameter(sessionCorpID, "QuoteAcceptReason");
			companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,"");
			multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString(); 
			company= companyManager.findByCorpID(sessionCorpID).get(0);
            if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }
			if(company!=null){
				 if(company.getEstimateVATFlag()!=null){
						estVatFlag=company.getEstimateVATFlag();
					}
				if(company.getVoxmeIntegration()!=null){
					voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
				}
				 if(company.getUTSI()!=null){
					 networkAgent=company.getUTSI();
					}else{
						networkAgent=false;
					}
				 if(pricePointUserStartDate!=null){
				 pricePointFlag=company.getEnablePricePoint();
				 }	
				 if(company.getPriceEndDate()!=null && company.getPriceEndDate().compareTo(new Date())>0){				
						 pricePointexpired=true; 
									
				}
				 if((company.getQuoteServices()!=null)&&(!company.getQuoteServices().equalsIgnoreCase(""))){
					 	userQuoteServices=company.getQuoteServices();
					 }else{
						 userQuoteServices="N/A";
					 }
				 compDivFlag=company.getCompanyDivisionFlag();
			}
			//voxmecodeend
			sysDefaultDetail = billingManager.findDefaultBookingAgentDetail(sessionCorpID);
			systemDefaultVatCalculationNew="N";
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					if(systemDefault.getAccountLineAccountPortalFlag() !=null ){
						accountLineAccountPortalFlag = 	systemDefault.getAccountLineAccountPortalFlag();
					}
					if(systemDefault.getBaseCurrency() !=null ){
						baseCurrency=systemDefault.getBaseCurrency();
						}
					checkContractChargesMandatory=systemDefault.getContractChargesMandatory();
					if(systemDefault.getVatCalculation()!=null && systemDefault.getVatCalculation()){
						systemDefaultVatCalculationNew="Y";	
					}
				}
			}
			getComboList(sessionCorpID);
			 reloService = refMasterManager.findServiceByJob("RLO",sessionCorpID, "SERVICE");
			 service = refMasterManager.findServiceByJob("NotRLO",sessionCorpID, "SERVICE");
			 enbState = customerFileManager.enableStateList(sessionCorpID);
			 costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
			 currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
	}
	public String getComboList(String corpId) {

		try {
			state = refMasterManager.findByParameter(corpId, "STATE");
			ocountry = refMasterManager.findCountry(corpId, "COUNTRY");
			dcountry = refMasterManager.findCountry(corpId, "COUNTRY");
			countryCod = refMasterManager.findByParameter(corpId, "COUNTRY");
			job = refMasterManager.findByParameter(corpId, "JOB");
			routing = refMasterManager.findByParameter(corpId, "ROUTING");
			commodit = refMasterManager.findByParameter(corpId, "COMMODIT");
			commodits = refMasterManager.findByParameter(corpId, "COMMODITS");
			mode = refMasterManager.findByParameters(corpId, "MODE");
			packingServiceList=refMasterManager.findByParameters(corpId, "PackingService");
			//service = refMasterManager.findByParameter(corpId, "SERVICE");
			pkmode = refMasterManager.findByParameter(corpId, "PKMODE");
			loadsite = refMasterManager.findByParameter(corpId, "LOADSITE");
			specific = refMasterManager.findByParameter(corpId, "SPECIFIC");
			jobtype = refMasterManager.findByParameter(corpId, "JOBTYPE");
			military = refMasterManager.findByParameter(corpId, "MILITARY");
			JOB_STATUS = refMasterManager.findByParameter(corpId, "JOB_STATUS");
			Special = refMasterManager.findByParameter(corpId, "SPECIAL");
			Howpack = refMasterManager.findByParameter(corpId, "HOWPACK");
			AGTPER = refMasterManager.findByParameter(corpId, "AGTPER");
			EQUIP = refMasterManager.findByParameter(corpId, "EQUIP");
			Peak = refMasterManager.findByParameter(corpId, "PEAK");
			tcktservc = refMasterManager.findByParameter(corpId, "TCKTSERVC");
			dept = refMasterManager.findByParameter(corpId, "DEPT");
			house = refMasterManager.findByParameter(corpId, "HOUSE");
			confirm = refMasterManager.findByParameter(corpId, "CONFIRM");
			tcktactn = refMasterManager.findByParameter(corpId, "TCKTACTN");
			woinstr = refMasterManager.findByParameter(corpId, "WOINSTR");
			paytype = refMasterManager.findByParameter(corpId, "PAYTYPE");
			sale = refMasterManager.findUser(corpId, "ROLE_SALE");
			coord = refMasterManager.findUser(corpId, "ROLE_COORD");
			qc = refMasterManager.findUser(corpId, "ROLE_QC");
			statusReason = refMasterManager.findByParameter(corpId, "statusReason");
			glcodes = refMasterManager.findByParameter(corpId, "GLCODES");
			isdriver = refMasterManager.findByParameter(corpId, "ISDRIVER");

			yesno = refMasterManager.findByParameter(corpId, "YESNO");
			partnerType = refMasterManager.findByParameter(corpId, "partnerType");
			SITOUTTA = refMasterManager.findByParameter(corpId, "SITOUTTA");
			omni = refMasterManager.findByParameter(corpId, "omni");
			QUOTESTATUS = refMasterManager.findByParameter(corpId, "QUOTESTATUS");
			category = refMasterManager.findByParameter(corpId, "ACC_CATEGORY");
			category.put("", "");
			basis = refMasterManager.findByParameter(corpId, "ACC_BASIS");
			basis.put("", "");
			country = refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
			payingStatus = refMasterManager.findByParameter(corpId, "ACC_STATUS");
			estVatList=new LinkedHashMap<String, String>();
		    estVatList.put("", "");
		    estVatList.putAll(refMasterManager.findByParameterWithoutParent(corpId, "EUVAT"));
		    payVatList=new LinkedHashMap<String, String>();
		    payVatList.put("", "");
		    payVatList.putAll(refMasterManager.findByParameterWithoutParent(corpId, "PAYVATDESC"));
			euVatPercentList = refMasterManager.findVatPercentList(corpId, "EUVAT");
			payVatPercentList= refMasterManager.findVatPercentList(sessionCorpID, "PAYVATDESC");
			estVatPersentList = refMasterManager.findVatPercentList(corpId, "EUVAT");	 
			flagCarrierList=refMasterManager.findByParameter(corpId, "FLAGCARRIER");
			languageList = refMasterManager.findByflexValue(corpId, "LANGUAGE");
			Map<String, String> temp = new LinkedHashMap<String, String>();
			for(Map.Entry<String, String> rec:languageList.entrySet()){
				if((rec.getKey()!=null)&&(!rec.getKey().toString().trim().equalsIgnoreCase(""))){
					temp.put(rec.getKey(), rec.getValue());
				}
			}
			languageList.clear();
			languageList.putAll(temp);
			weightunits = new ArrayList();
			weightunits.add("Lbs");
			weightunits.add("Kgs");

			volumeunits = new ArrayList();
			volumeunits.add("Cft");
			volumeunits.add("Cbm");

			lengthunits = new ArrayList();
			lengthunits.add("Ft");
			lengthunits.add("Mtr");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}

		return SUCCESS;

	}
	@SkipValidation
	public Map<String, String> findByCountryParameterDesc(){
		Map newMap =new LinkedHashMap<String, String>();							
		Iterator mapIterator11 = countryCod.entrySet().iterator();
		 				while (mapIterator11.hasNext()) {
		 					Map.Entry entry = (Map.Entry) mapIterator11.next();
		 					String key1 = (String) entry.getKey();
		 					if (!(newMap.containsKey(key1))){ 
		 						newMap.put(countryCod.get(key1),key1);
		 					}
		 				}
		 				countryDsc = new LinkedHashMap<String, String>();
		 				countryDsc.putAll(newMap);
		 				return countryDsc;
		 			}
	@SkipValidation
    public String addCopyPricingServiceOrder(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			/*try{
				savePricingLine();
				} catch(Exception e){
					
				}*/
			//getComboList(sessionCorpID);
			if (cid != null) {   
				customerFile = customerFileManager.get(cid);
				coordinatorList=refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_COORD",sessionCorpID);
				if(customerFile.getCoordinator()!=null && !(customerFile.getCoordinator().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getCoordinator());
					if(!aliasNameList.isEmpty()){
						String aliasName=aliasNameList.get(0).toString();
						String coordCode=customerFile.getCoordinator().toUpperCase();
						String alias = aliasName.toUpperCase();
						if(!coordinatorList.containsValue(coordCode)){
							coordinatorList.put(coordCode,alias );
						}
					}
				}
				//service = refMasterManager.findServiceByJob(customerFile.getJob(),sessionCorpID, "SERVICE");
				estimatorList=refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_SALE",sessionCorpID);
				if(customerFile.getSalesMan()!=null && !(customerFile.getSalesMan().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getSalesMan());
					if(!aliasNameList.isEmpty()){
						String estimatorCode=customerFile.getSalesMan().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!estimatorList.containsValue(estimatorCode)){
							estimatorList.put(estimatorCode,alias );
						}
					}
						
				}
				companies =  companyManager.findByCompanyDivis(sessionCorpID);
			    maxShip = customerFileManager.findMaximumShipCF(customerFile.getSequenceNumber(),"",customerFile.getCorpID());
			    
			    if ( maxShip.get(0) == null ) {          
			    	ship = "01";
			    }else  {
			    	autoShip = Long.parseLong((maxShip).get(0).toString()) + 1;
			        if((autoShip.toString()).length() == 1) {
			        	ship = "0"+(autoShip.toString());
			        }else {
			        	ship=autoShip.toString();
			        }
			        prevShip = Long.parseLong((maxShip).get(0).toString());
			        if((prevShip.toString()).length() == 1) {
			        	shipPrev = "0"+(prevShip.toString());
			        }else {
			        	shipPrev=prevShip.toString();
			        }
			        if(!ship.equals("01"))
			        {
			        	shipnumber = customerFile.getSequenceNumber() + shipPrev;
			            tempID=Long.parseLong((serviceOrderManager.findIdByShipNumber(shipnumber)).get(0).toString());
			            serviceOrderPrev=serviceOrderManager.get(tempID);
			        }            
			    }
			    shipnumber = customerFile.getSequenceNumber() + ship;
			    addCopyServiceOrder = (ServiceOrder)serviceOrderManager.get(sid);
			    shipNumberOld = addCopyServiceOrder.getShipNumber();
			    addCopyMiscellaneous = (Miscellaneous)miscellaneousManager.get(sid);
			    serviceOrder = new ServiceOrder();
			    miscellaneous=new Miscellaneous();
			    //miscellaneous.setShipNumber(shipnumber);
			    miscellaneous.setEstimateTareWeight(addCopyMiscellaneous.getEstimateTareWeight());
			    miscellaneous.setActualTareWeight(addCopyMiscellaneous.getActualTareWeight());
			    miscellaneous.setRwghTare(addCopyMiscellaneous.getRwghTare());
			    miscellaneous.setChargeableTareWeight(addCopyMiscellaneous.getChargeableTareWeight());
			    miscellaneous.setEntitleTareWeight(addCopyMiscellaneous.getEntitleTareWeight());
			    miscellaneous.setUnit1(addCopyMiscellaneous.getUnit1());
				miscellaneous.setUnit2(addCopyMiscellaneous.getUnit2());
				miscellaneous.setEntitleGrossWeight(addCopyMiscellaneous.getEntitleGrossWeight());
				miscellaneous.setEstimateGrossWeight(addCopyMiscellaneous.getEstimateGrossWeight());
				miscellaneous.setActualGrossWeight(addCopyMiscellaneous.getActualGrossWeight());
				miscellaneous.setChargeableGrossWeight(addCopyMiscellaneous.getChargeableGrossWeight());
				miscellaneous.setRwghGross(addCopyMiscellaneous.getRwghGross());
				miscellaneous.setEntitleNetWeight(addCopyMiscellaneous.getEntitleNetWeight());
				miscellaneous.setEstimatedNetWeight(addCopyMiscellaneous.getEstimatedNetWeight());
				miscellaneous.setActualNetWeight(addCopyMiscellaneous.getActualNetWeight());
				miscellaneous.setChargeableNetWeight(addCopyMiscellaneous.getChargeableNetWeight());
				miscellaneous.setRwghNet(addCopyMiscellaneous.getRwghNet());
				miscellaneous.setEntitleCubicFeet(addCopyMiscellaneous.getEntitleCubicFeet());
				miscellaneous.setEstimateCubicFeet(addCopyMiscellaneous.getEstimateCubicFeet());
				miscellaneous.setActualCubicFeet(addCopyMiscellaneous.getActualCubicFeet());
				miscellaneous.setChargeableCubicFeet(addCopyMiscellaneous.getChargeableCubicFeet());
				miscellaneous.setRwghCubicFeet(addCopyMiscellaneous.getRwghCubicFeet());
				miscellaneous.setNetEntitleCubicFeet(addCopyMiscellaneous.getNetEntitleCubicFeet());
				miscellaneous.setNetEstimateCubicFeet(addCopyMiscellaneous.getNetEstimateCubicFeet());
				miscellaneous.setNetActualCubicFeet(addCopyMiscellaneous.getNetActualCubicFeet());
				miscellaneous.setChargeableNetCubicFeet(addCopyMiscellaneous.getChargeableNetCubicFeet());
				miscellaneous.setRwghNetCubicFeet(addCopyMiscellaneous.getRwghNetCubicFeet());
				miscellaneous.setEntitleNumberAuto(addCopyMiscellaneous.getEntitleNumberAuto());
				miscellaneous.setEstimateAuto(addCopyMiscellaneous.getEstimateAuto());
				miscellaneous.setActualAuto(addCopyMiscellaneous.getActualAuto());
				miscellaneous.setEntitleAutoWeight(addCopyMiscellaneous.getEntitleAutoWeight());
				miscellaneous.setEstimatedAutoWeight(addCopyMiscellaneous.getEstimatedAutoWeight());
				miscellaneous.setActualAutoWeight(addCopyMiscellaneous.getActualAutoWeight());
				miscellaneous.setMillitaryShipment(addCopyMiscellaneous.getMillitaryShipment()); 
				miscellaneous.setEntitleTareWeightKilo(addCopyMiscellaneous.getEntitleTareWeightKilo());
				miscellaneous.setEntitleGrossWeightKilo(addCopyMiscellaneous.getEntitleGrossWeightKilo());
				miscellaneous.setEntitleNetWeightKilo(addCopyMiscellaneous.getEntitleNetWeightKilo());
				miscellaneous.setEstimateGrossWeightKilo(addCopyMiscellaneous.getEstimateGrossWeightKilo());
				miscellaneous.setEstimateTareWeightKilo(addCopyMiscellaneous.getEstimateTareWeightKilo());
				miscellaneous.setEstimatedNetWeightKilo(addCopyMiscellaneous.getEstimatedNetWeightKilo());
				miscellaneous.setActualGrossWeightKilo(addCopyMiscellaneous.getActualGrossWeightKilo());
				miscellaneous.setActualTareWeightKilo(addCopyMiscellaneous.getActualTareWeightKilo());
				miscellaneous.setActualNetWeightKilo(addCopyMiscellaneous.getActualNetWeightKilo());
				miscellaneous.setChargeableGrossWeightKilo(addCopyMiscellaneous.getChargeableGrossWeightKilo());
				miscellaneous.setChargeableTareWeightKilo(addCopyMiscellaneous.getChargeableTareWeightKilo());
				miscellaneous.setChargeableNetWeightKilo(addCopyMiscellaneous.getChargeableNetWeightKilo());
				miscellaneous.setRwghGrossKilo(addCopyMiscellaneous.getRwghGrossKilo());
				miscellaneous.setRwghTareKilo(addCopyMiscellaneous.getRwghTareKilo());
				miscellaneous.setRwghNetKilo(addCopyMiscellaneous.getRwghNetKilo());
				miscellaneous.setEntitleCubicMtr(addCopyMiscellaneous.getEntitleCubicMtr());
				miscellaneous.setNetEntitleCubicMtr(addCopyMiscellaneous.getNetEntitleCubicMtr());
				miscellaneous.setEstimateCubicMtr(addCopyMiscellaneous.getEstimateCubicMtr());
				miscellaneous.setNetEstimateCubicMtr(addCopyMiscellaneous.getNetEstimateCubicMtr());
				miscellaneous.setActualCubicMtr(addCopyMiscellaneous.getActualCubicMtr());
				miscellaneous.setNetActualCubicMtr(addCopyMiscellaneous.getNetActualCubicMtr());
				miscellaneous.setChargeableCubicMtr(addCopyMiscellaneous.getChargeableCubicMtr());
				miscellaneous.setChargeableNetCubicMtr(addCopyMiscellaneous.getChargeableNetCubicMtr());
				miscellaneous.setRwghCubicMtr(addCopyMiscellaneous.getRwghCubicMtr());
				miscellaneous.setRwghNetCubicMtr(addCopyMiscellaneous.getRwghNetCubicMtr());  
				miscellaneous.setCreatedOn(new Date());
				miscellaneous.setUpdatedOn(new Date());
				miscellaneous.setUpdatedBy(getRequest().getRemoteUser());
				miscellaneous.setCreatedBy(getRequest().getRemoteUser());
				serviceOrder.setTransitDays(addCopyServiceOrder.getTransitDays());
				serviceOrder.setEquipment	(addCopyServiceOrder.getEquipment());
				serviceOrder.setLastName(addCopyServiceOrder.getLastName());
			    serviceOrder.setDefaultAccountLineStatus(addCopyServiceOrder.isDefaultAccountLineStatus());
			    serviceOrder.setCustomerFileId	(addCopyServiceOrder.getCustomerFileId());
				serviceOrder.setSequenceNumber	(addCopyServiceOrder.getSequenceNumber());
				serviceOrder.setShip(ship);
				serviceOrder.setShipNumber	(shipnumber);
				serviceOrder.setControlFlag	(addCopyServiceOrder.getControlFlag());
			    serviceOrder.setRegistrationNumber	(addCopyServiceOrder.getRegistrationNumber());
				serviceOrder.setStatusDate	(new Date());
			    serviceOrder.setJob	(addCopyServiceOrder.getJob());
				serviceOrder.setRouting	(addCopyServiceOrder.getRouting());
				serviceOrder.setServiceType	(addCopyServiceOrder.getServiceType());
				serviceOrder.setMode	(addCopyServiceOrder.getMode());
				serviceOrder.setPackingMode	(addCopyServiceOrder.getPackingMode());
				serviceOrder.setPackingService(addCopyServiceOrder.getPackingService());
				serviceOrder.setPayType	(addCopyServiceOrder.getPayType());
				serviceOrder.setCoordinator	(addCopyServiceOrder.getCoordinator()); 
			    serviceOrder.setSalesMan	(addCopyServiceOrder.getSalesMan()); 
			    serviceOrder.setPrefix	(addCopyServiceOrder.getPrefix()); 
			    serviceOrder.setFirstName	(addCopyServiceOrder.getFirstName()); 
			    serviceOrder.setLastName	(addCopyServiceOrder.getLastName()); 
			    serviceOrder.setMi	(addCopyServiceOrder.getMi()); 
			    serviceOrder.setOriginAddressLine1	(addCopyServiceOrder.getOriginAddressLine1()); 
			    serviceOrder.setSuffix	(addCopyServiceOrder.getSuffix()); 
			    serviceOrder.setOriginAddressLine2	(addCopyServiceOrder.getOriginAddressLine2()); 
			    serviceOrder.setOriginAddressLine3	(addCopyServiceOrder.getOriginAddressLine3()); 
			    serviceOrder.setOriginCity	(addCopyServiceOrder.getOriginCity()); 
			    serviceOrder.setOriginState	(addCopyServiceOrder.getOriginState()); 
			    serviceOrder.setOriginCountryCode	(addCopyServiceOrder.getOriginCountryCode()); 
			    serviceOrder.setOriginCountry	(addCopyServiceOrder.getOriginCountry()); 
			    serviceOrder.setOriginZip	(addCopyServiceOrder.getOriginZip()); 
			    
			    serviceOrder.setOriginPreferredContactTime(addCopyServiceOrder.getOriginPreferredContactTime());
			    
			    serviceOrder.setOriginDayPhone	(addCopyServiceOrder.getOriginDayPhone()); 
			    serviceOrder.setOriginHomePhone	(addCopyServiceOrder.getOriginHomePhone()); 
			    serviceOrder.setOriginFax	(addCopyServiceOrder.getOriginFax()); 
			    serviceOrder.setDestinationAddressLine1	(addCopyServiceOrder.getDestinationAddressLine1()); 
			    serviceOrder.setDestinationAddressLine2	(addCopyServiceOrder.getDestinationAddressLine2()); 
			    serviceOrder.setDestinationAddressLine3	(addCopyServiceOrder.getDestinationAddressLine3()); 
			    serviceOrder.setDestinationCity	(addCopyServiceOrder.getDestinationCity()); 
			    serviceOrder.setDestinationCityCode	(addCopyServiceOrder.getDestinationCityCode()); 
			    serviceOrder.setOriginCityCode	(addCopyServiceOrder.getOriginCityCode()); 
			    serviceOrder.setDestinationState	(addCopyServiceOrder.getDestinationState()); 
			    serviceOrder.setDestinationCountryCode	(addCopyServiceOrder.getDestinationCountryCode()); 
			    serviceOrder.setDestinationCountry	(addCopyServiceOrder.getDestinationCountry()); 
			    serviceOrder.setDestinationZip	(addCopyServiceOrder.getDestinationZip()); 
			    serviceOrder.setDestPreferredContactTime(addCopyServiceOrder.getDestPreferredContactTime());
			    serviceOrder.setDestinationDayPhone	(addCopyServiceOrder.getDestinationDayPhone()); 
			    serviceOrder.setDestinationHomePhone	(addCopyServiceOrder.getDestinationHomePhone()); 
			    serviceOrder.setDestinationFax	(addCopyServiceOrder.getDestinationFax()); 
			    serviceOrder.setContactName	(addCopyServiceOrder.getContactName());
			    serviceOrder.setContactNameExt	(addCopyServiceOrder.getContactNameExt());
			    serviceOrder.setOriginContactEmail	(addCopyServiceOrder.getOriginContactEmail());
			    serviceOrder.setContactPhone	(addCopyServiceOrder.getContactPhone()); 
			    serviceOrder.setContactFax	(addCopyServiceOrder.getContactFax()); 
			    serviceOrder.setClientCode	(addCopyServiceOrder.getClientCode()); 
			    serviceOrder.setClientName	(addCopyServiceOrder.getClientName()); 
			    serviceOrder.setClientReference	(addCopyServiceOrder.getClientReference()); 
			    serviceOrder.setCommodity	(addCopyServiceOrder.getCommodity()); 
			    serviceOrder.setContract	(addCopyServiceOrder.getContract()); 
			    serviceOrder.setSocialSecurityNumber	(addCopyServiceOrder.getSocialSecurityNumber()); 
			    serviceOrder.setConsignee	(addCopyServiceOrder.getConsignee()); 
			    serviceOrder.setDestinationLoadSite	(addCopyServiceOrder.getDestinationLoadSite()); 
			    serviceOrder.setEstimator	(addCopyServiceOrder.getEstimator()); 
			    serviceOrder.setLastExtra	(addCopyServiceOrder.getLastExtra()); 
			    serviceOrder.setLastItem	(addCopyServiceOrder.getLastItem());
			    serviceOrder.setNextCheckOn	(addCopyServiceOrder.getNextCheckOn()); 
			    serviceOrder.setOriginContactName	(addCopyServiceOrder.getOriginContactName());
			    serviceOrder.setOriginContactNameExt	(addCopyServiceOrder.getOriginContactNameExt());
			    serviceOrder.setDestinationContactEmail	(addCopyServiceOrder.getDestinationContactEmail());
			    serviceOrder.setOriginContactPhone	(addCopyServiceOrder.getOriginContactPhone()); 
			    serviceOrder.setOriginContactWork	(addCopyServiceOrder.getOriginContactWork()); 
			    serviceOrder.setOriginLoadSite	(addCopyServiceOrder.getOriginLoadSite()); 
			    serviceOrder.setOrderBy	(addCopyServiceOrder.getOrderBy()); 
			    serviceOrder.setOrderPhone	(addCopyServiceOrder.getOrderPhone()); 
			    serviceOrder.setSuffixNext	(addCopyServiceOrder.getSuffixNext()); 
			    serviceOrder.setEmail	(addCopyServiceOrder.getEmail()); 
			    serviceOrder.setOriginCompany	(addCopyServiceOrder.getOriginCompany()); 
			    serviceOrder.setDestinationCompany	(addCopyServiceOrder.getDestinationCompany()); 
			    serviceOrder.setOriginMilitary	(addCopyServiceOrder.getOriginMilitary()); 
			    serviceOrder.setDestinationMilitary	(addCopyServiceOrder.getDestinationMilitary()); 
			    serviceOrder.setLastName2	(addCopyServiceOrder.getLastName2()); 
			    serviceOrder.setFirstName2	(addCopyServiceOrder.getFirstName2()); 
			    serviceOrder.setPrefix2	(addCopyServiceOrder.getPrefix2()); 
			    serviceOrder.setInitial2	(addCopyServiceOrder.getInitial2()); 
			    serviceOrder.setAndor	(addCopyServiceOrder.getAndor()); 
			    serviceOrder.setBillToCode	(addCopyServiceOrder.getBillToCode()); 
			    serviceOrder.setBillToName	(addCopyServiceOrder.getBillToName()); 
			    serviceOrder.setOriginDayExtn	(addCopyServiceOrder.getOriginDayExtn()); 
			    serviceOrder.setDestinationDayExtn	(addCopyServiceOrder.getDestinationDayExtn()); 
			    serviceOrder.setVip	(addCopyServiceOrder.getVip());
			    serviceOrder.setRevised	(addCopyServiceOrder.getRevised()); 
			    serviceOrder.setCompanyDivision(addCopyServiceOrder.getCompanyDivision()); 
			    serviceOrder.setPortOfLading	(addCopyServiceOrder.getPortOfLading()); 
			    serviceOrder.setPortOfEntry 	(addCopyServiceOrder.getPortOfEntry());  
			    serviceOrder.setOriginArea	(addCopyServiceOrder.getOriginArea()); 
			    serviceOrder.setDestinationArea	(addCopyServiceOrder.getDestinationArea()); 
			    serviceOrder.setTripNumber	(addCopyServiceOrder.getTripNumber()); 
			    serviceOrder.setEmail2	(addCopyServiceOrder.getEmail2());
			    serviceOrder.setOriginContactExtn	(addCopyServiceOrder.getOriginContactExtn());
				serviceOrder.setRank(addCopyServiceOrder.getRank());
			    serviceOrder.setDestinationContactExtn	(addCopyServiceOrder.getDestinationContactExtn());
				serviceOrder.setEmailSent	(addCopyServiceOrder.getEmailSent());
			    serviceOrder.setQuoteStatus	(addCopyServiceOrder.getQuoteStatus());
			    serviceOrder.setQuoteAcceptReason(addCopyServiceOrder.getQuoteAcceptReason());
			    serviceOrder.setEstimateGrossWeight	(addCopyServiceOrder.getEstimateGrossWeight()); 
			    serviceOrder.setActualGrossWeight	(addCopyServiceOrder.getActualGrossWeight()); 
			    serviceOrder.setEstimatedNetWeight	(addCopyServiceOrder.getEstimatedNetWeight()); 
			    serviceOrder.setActualNetWeight	(addCopyServiceOrder.getActualNetWeight()); 
			    serviceOrder.setEntitleNumberAuto	(addCopyServiceOrder.getEntitleNumberAuto()); 
			    serviceOrder.setEntitleBoatYN	(addCopyServiceOrder.getEntitleBoatYN()); 
			    serviceOrder.setEstimateAuto	(addCopyServiceOrder.getEstimateAuto()); 
			    serviceOrder.setEstimateBoat	(addCopyServiceOrder.getEstimateBoat()); 
			    serviceOrder.setActualAuto	(addCopyServiceOrder.getActualAuto()); 
			    serviceOrder.setActualBoat	(addCopyServiceOrder.getActualBoat()); 
			    serviceOrder.setEstimateCubicFeet	(addCopyServiceOrder.getEstimateCubicFeet()); 
			    serviceOrder.setActualCubicFeet	(addCopyServiceOrder.getActualCubicFeet());  
			    serviceOrder.setOriginMobile	(addCopyServiceOrder.getOriginMobile()); 
			    serviceOrder.setDestinationMobile	(addCopyServiceOrder.getDestinationMobile()); 
			    miscellaneous.setEquipment	(addCopyMiscellaneous.getEquipment()); 
			    serviceOrder.setStatusReason	(addCopyServiceOrder.getStatusReason()); 
			    serviceOrder.setBookingAgentCode	(addCopyServiceOrder.getBookingAgentCode()); 
			    serviceOrder.setBookingAgentName	(addCopyServiceOrder.getBookingAgentName()); 
			    serviceOrder.setEntitledTotalAmount	(addCopyServiceOrder.getEntitledTotalAmount()); 
			    serviceOrder.setEstimatedTotalExpense	(addCopyServiceOrder.getEstimatedTotalExpense()); 
			    serviceOrder.setEstimatedTotalRevenue	(addCopyServiceOrder.getEstimatedTotalRevenue()); 
			    serviceOrder.setDistributedTotalAmount	(addCopyServiceOrder.getDistributedTotalAmount()); 
			    serviceOrder.setEstimatedGrossMargin	(addCopyServiceOrder.getEstimatedGrossMargin()); 
			    serviceOrder.setEstimatedGrossMarginPercentage	(addCopyServiceOrder.getEstimatedGrossMarginPercentage());
			    
				/*# 7784 - Always show actual gross margin in accountline overview Start*/
				try{
			    serviceOrder.setProjectedGrossMargin(addCopyServiceOrder.getProjectedGrossMargin()); 
			    serviceOrder.setProjectedGrossMarginPercentage(addCopyServiceOrder.getProjectedGrossMarginPercentage()); 
				}catch(Exception e){}
				/*# 7784 - Always show actual gross margin in accountline overview End*/
				
			    serviceOrder.setRevisedTotalExpense	(addCopyServiceOrder.getRevisedTotalExpense()); 
			    serviceOrder.setRevisedTotalRevenue	(addCopyServiceOrder.getRevisedTotalRevenue()); 
			    serviceOrder.setRevisedGrossMargin	(addCopyServiceOrder.getRevisedGrossMargin()); 
			    serviceOrder.setRevisedGrossMarginPercentage	(addCopyServiceOrder.getRevisedGrossMarginPercentage()); 
			    serviceOrder.setActualExpense	(addCopyServiceOrder.getActualExpense()); 
			    serviceOrder.setActualRevenue	(addCopyServiceOrder.getActualRevenue()); 
			    serviceOrder.setActualGrossMargin	(addCopyServiceOrder.getActualGrossMargin()); 
			    serviceOrder.setActualGrossMarginPercentage	(addCopyServiceOrder.getActualGrossMarginPercentage()); 
			    serviceOrder.setCustomerFeedback("Yes"); 
			    serviceOrder.setDestinationEmail	(addCopyServiceOrder.getDestinationEmail()); 
			    serviceOrder.setDestinationEmail2	(addCopyServiceOrder.getDestinationEmail2());
			    serviceOrder.setBookingAgentCheckedBy	(addCopyServiceOrder.getBookingAgentCheckedBy());
			    serviceOrder.setBookingAgentVanlineCode(addCopyServiceOrder.getBookingAgentVanlineCode());
			    serviceOrder.setQuoteAccept(addCopyServiceOrder.getQuoteAccept());
			  //Bug 11839 - Possibility to modify Current status to Prior status
			    serviceOrder.setStatus("NEW");
				serviceOrder.setStatusNumber(1);
				serviceOrder.setCorpID(sessionCorpID);
				serviceOrder.setUnit1(addCopyServiceOrder.getUnit1());
				serviceOrder.setUnit2(addCopyServiceOrder.getUnit2());
				serviceOrder.setCreatedOn(new Date());
				serviceOrder.setUpdatedOn(new Date());
				serviceOrder.setCreatedBy(getRequest().getRemoteUser());
				serviceOrder.setUpdatedBy(getRequest().getRemoteUser());
				serviceOrder.setIncExcServiceType(addCopyServiceOrder.getIncExcServiceType());
				serviceOrder.setIncExcServiceTypeLanguage(addCopyServiceOrder.getIncExcServiceTypeLanguage());
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
					weightType="lbscft";
				}
				else{
					weightType="kgscbm";
				}
				ostates = customerFileManager.findDefaultStateList(addCopyServiceOrder.getOriginCountryCode(), sessionCorpID);
			    dstates = customerFileManager.findDefaultStateList(addCopyServiceOrder.getDestinationCountryCode(), sessionCorpID);
			    originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
			  	destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());			
			  	companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,addCopyServiceOrder.getBookingAgentCode());
				companies =  companyManager.findByCompanyDivis(sessionCorpID);
				}  
			serviceOrder.setCustomerFile(customerFile);
			serviceOrder= serviceOrderManager.save(serviceOrder);
 
			List tripList1 = dspDetailsManager.getListById(serviceOrder.getId());
			if(tripList1.isEmpty()){
				dspDetails = new DspDetails();
				dspDetails.setId(serviceOrder.getId());
				dspDetails.setCorpID(serviceOrder.getCorpID());
				dspDetails.setCreatedOn(new Date());
				dspDetails.setUpdatedOn(new Date());
				dspDetails.setCreatedBy(getRequest().getRemoteUser());
				dspDetails.setUpdatedBy(getRequest().getRemoteUser());
				dspDetails.setServiceOrderId(serviceOrder.getId());
				dspDetails.setShipNumber(serviceOrder.getShipNumber());
				dspDetailsManager.save(dspDetails);
			}            


 
			i = Long.parseLong(serviceOrderManager.findMaximumId().get(0).toString());
			serviceOrder = serviceOrderManager.get(i); 
			miscellaneous.setId(serviceOrder.getId());
			miscellaneous.setShipNumber(serviceOrder.getShipNumber());
			miscellaneous.setShip(serviceOrder.getShip());
			miscellaneous.setSequenceNumber(serviceOrder.getSequenceNumber());
			miscellaneous.setCorpID(serviceOrder.getCorpID());
			miscellaneous.setCreatedOn(serviceOrder.getCreatedOn());
			miscellaneousManager.save(miscellaneous);  
			trackingStatus = new TrackingStatus();
			trackingStatus.setId(serviceOrder.getId());
			trackingStatus.setCorpID(serviceOrder.getCorpID());
			trackingStatus.setShipNumber(serviceOrder.getShipNumber());
			trackingStatus.setSitOriginDays(0);
			trackingStatus.setSitDestinationDays(0);
			trackingStatus.setFromWH(0);
			trackingStatus.setDemurrageCostPerDay2(0);
			trackingStatus.setUsdaCost(0);
			trackingStatus.setInspectorCost(0);
			trackingStatus.setXrayCost(0);
			trackingStatus.setPerdiumCostPerDay(0);
			trackingStatus.setDemurrageCostPerDay1(0);
			trackingStatus.setStatus(serviceOrder.getCustomerFile().getStatus());
			trackingStatus.setCreatedOn(serviceOrder.getCreatedOn());
			trackingStatus.setUpdatedOn(new Date());
			trackingStatus.setCreatedBy(serviceOrder.getCreatedBy());
			trackingStatus.setUpdatedBy(getRequest().getRemoteUser()); 
			trackingStatus.setSequenceNumber(serviceOrder.getSequenceNumber());
			trackingStatus.setSurveyTimeFrom(serviceOrder.getCustomerFile().getSurveyTime());
			trackingStatus.setSurveyTimeTo(serviceOrder.getCustomerFile().getSurveyTime2());
			trackingStatus.setStatusDate(serviceOrder.getCustomerFile().getStatusDate());
			trackingStatus.setSurveyDate(serviceOrder.getCustomerFile().getSurvey());
			trackingStatus.setSurvey(serviceOrder.getCustomerFile().getSurvey());
			trackingStatus.setInitialContact(serviceOrder.getCustomerFile().getInitialContactDate());
			trackingStatus.setSurveyTimeFrom(serviceOrder.getCustomerFile().getSurveyTime());
			trackingStatus.setSurveyTimeTo(serviceOrder.getCustomerFile().getSurveyTime2());
			trackingStatus.setServiceOrder(serviceOrder);
			trackingStatus.setMiscellaneous(miscellaneous);
			trackingStatusManager.save(trackingStatus); 
				billing = new Billing();
				billing.setShipNumber(serviceOrder.getShipNumber());
				billing.setSequenceNumber(serviceOrder.getSequenceNumber());
				billing.setCreatedOn(serviceOrder.getCreatedOn());
				billing.setUpdatedOn(new Date());
				billing.setCreatedBy(serviceOrder.getCreatedBy());
				billing.setUpdatedBy(getRequest().getRemoteUser());
				billing.setBillingInsurancePayableCurrency(baseCurrency);
				billing.setCorpID(sessionCorpID);
				billing.setBillToCode(serviceOrder.getCustomerFile().getBillToCode());
				billing.setBillToName(serviceOrder.getCustomerFile().getBillToName());
				billing.setNetworkBillToCode(serviceOrder.getCustomerFile().getAccountCode());
		    	billing.setNetworkBillToName(serviceOrder.getCustomerFile().getAccountName());
				billing.setBillToAuthority(serviceOrder.getCustomerFile().getBillToAuthorization());
				billing.setBillingId(serviceOrder.getCustomerFile().getBillToCode());
				billing.setBillName(serviceOrder.getCustomerFile().getBillToName());
				billing.setBillTo1Point(serviceOrder.getCustomerFile().getBillPayMethod());
				billing.setContract(serviceOrder.getContract());
				billing.setPersonBilling(serviceOrder.getCustomerFile().getPersonBilling());
				billing.setPersonPayable(serviceOrder.getCustomerFile().getPersonPayable());
				billing.setPersonPricing(serviceOrder.getCustomerFile().getPersonPricing());
				billing.setAuditor(serviceOrder.getCustomerFile().getAuditor());
				billing.setBillTo2Code("");
				billing.setPrivatePartyBillingCode("");
				billing.setNoCharge(false);
				sysDefaultDetail = billingManager.findDefaultBookingAgentDetail(sessionCorpID);
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						billing.setCurrency(systemDefault.getBaseCurrency());
						if(systemDefault!=null  ){
							try{
							if(systemDefault.getReceivableVat()!=null && (!(systemDefault.getReceivableVat().toString().equals("")))){
			                if((billing.getPrimaryVatCode()==null || billing.getPrimaryVatCode().equalsIgnoreCase(""))){
							billing.setPrimaryVatCode(systemDefault.getReceivableVat());
			                }
			                billing.setSecondaryVatCode(systemDefault.getReceivableVat());
			                billing.setPrivatePartyVatCode(systemDefault.getReceivableVat());
			                billing.setStorageVatDescr(systemDefault.getReceivableVat());
			                billing.setInsuranceVatDescr(systemDefault.getReceivableVat());
			                if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
			                	billing.setStorageVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()))	;
			                	billing.setInsuranceVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()))	;
			                }
			                
							}
							if(systemDefault.getPayableVat()!=null && (!(systemDefault.getPayableVat().toString().equals("")))){
			                billing.setBookingAgentVatCode(systemDefault.getPayableVat());
			                billing.setOriginAgentVatCode(systemDefault.getPayableVat());
			                billing.setOriginSubAgentVatCode(systemDefault.getPayableVat());
			                billing.setDestinationSubAgentVatCode(systemDefault.getPayableVat());
			                billing.setDestinationAgentVatCode(systemDefault.getPayableVat());
			                billing.setVendorCodeVatCode(systemDefault.getPayableVat());
			                billing.setVendorCodeVatCode1(systemDefault.getPayableVat());
			                billing.setVendorStorageVatDescr(systemDefault.getPayableVat());
			                if(payVatPercentList.containsKey(systemDefault.getPayableVat())){
			                	billing.setVendorStorageVatPercent(new BigDecimal(payVatPercentList.get(systemDefault.getPayableVat())))	; 
					         }
			                
							}
							}catch(Exception e){
								e.printStackTrace();
							}
			                }
					}
				}
				billing.setServiceOrder(serviceOrder); 
				billing.setId(serviceOrder.getId());
				String creditTerm="";
				try{
				if(serviceOrder.getBillToCode()!=null && (!(serviceOrder.getBillToCode().toString().equals("")))){	
				creditTerm=partnerPrivateManager.getTheCreditTerm(serviceOrder.getBillToCode(),sessionCorpID);
				}
				}catch(Exception e){
					creditTerm="";
				} 
				billing.setCreditTerms(creditTerm); 
				billingManager.save(billing);  
				ServiceOrder serviceOrderOld = serviceOrderManager.get(sid);
				if(accountLineStatus==null||accountLineStatus.equals(""))
				{
					accountLineStatus="true";
				}
				accountLineList = accountLineManager.getAccountLineList(serviceOrderOld.getShipNumber(),accountLineStatus);
				java.util.Iterator<AccountLine> idIterator =  accountLineList.iterator(); 
				String user = getRequest().getRemoteUser();
				while (idIterator.hasNext()) 
				     {
					AccountLine accountLineOld = (AccountLine) idIterator.next(); 
					accountLine = new AccountLine();
					try{
						BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
						beanUtils.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
						beanUtils.copyProperties(accountLine, accountLineOld);
					}catch(Exception ex){
						logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					}
					try {
						if(accountLineOld.getReference()!=null && !accountLineOld.getReference().equals("") && accountLineOld.getExternalIntegrationReference()!=null && accountLineOld.getExternalIntegrationReference().equalsIgnoreCase("PP")){
							String newTariffId=saveToCreateTariffId(accountLineOld.getReference(),accountLineOld.getCategory(),serviceOrder.getJob(),serviceOrder.getMode(),serviceOrder.getPackingMode());
						if(newTariffId!=null && !newTariffId.equals("")){
							//Long pricePointTariff=Long.parseLong(newTariffId);
							accountLine.setReference(newTariffId);	
							accountLine.setExternalIntegrationReference("PP");
						}
						}
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					accountLine.setId(null);
					accountLine.setBillToCode(accountLineOld.getBillToCode()); 
		    		accountLine.setBillToName(accountLineOld.getBillToName());
		    		accountLine.setNetworkBillToCode(accountLineOld.getNetworkBillToCode());
		    		accountLine.setNetworkBillToName(accountLineOld.getNetworkBillToName());
					accountLine.setCategory(accountLineOld.getCategory()); 
					accountLine.setChargeCode(accountLineOld.getChargeCode()); 
					accountLine.setVATExclude(accountLineOld.getVATExclude()); 
					accountLine.setContract(accountLineOld.getContract());
					accountLine.setVendorCode(accountLineOld.getVendorCode());
					accountLine.setEstimateVendorName(accountLineOld.getEstimateVendorName()); 
					accountLine.setBasis(accountLineOld.getBasis()); 
					accountLine.setEstimateQuantity(accountLineOld.getEstimateQuantity());
					accountLine.setEstimateSellQuantity(accountLineOld.getEstimateSellQuantity());
					accountLine.setEstimateRate(accountLineOld.getEstimateRate());
					accountLine.setEstimateSellRate(accountLineOld.getEstimateSellRate()); 
					accountLine.setEstimateExpense(accountLineOld.getEstimateExpense()); 
					accountLine.setEstimateRevenueAmount(accountLineOld.getEstimateRevenueAmount()); 
					accountLine.setEstimatePassPercentage(accountLineOld.getEstimatePassPercentage());
					accountLine.setQuoteDescription(accountLineOld.getQuoteDescription());
					accountLine.setDescription(accountLineOld.getDescription());
					accountLine.setNote(accountLineOld.getNote());
					accountLine.setDisplayOnQuote(accountLineOld.isDisplayOnQuote());
					accountLine.setAdditionalService(accountLineOld.getAdditionalService());
				    accountLine.setStatus(accountLineOld.isStatus()); 
				    accountLine.setEstCurrency(accountLineOld.getEstCurrency());
				    accountLine.setEstLocalAmount(accountLineOld.getEstLocalAmount());
				    accountLine.setEstExchangeRate(accountLineOld.getEstExchangeRate());
				    accountLine.setEstValueDate(accountLineOld.getEstValueDate());
				    accountLine.setRevisionCurrency(accountLineOld.getRevisionCurrency());
				    accountLine.setRevisionLocalAmount(accountLineOld.getRevisionLocalAmount());
				    accountLine.setRevisionExchangeRate(accountLineOld.getRevisionExchangeRate());
				    accountLine.setRevisionValueDate(accountLineOld.getRevisionValueDate());
				    accountLine.setCorpID(sessionCorpID);
					accountLine.setServiceOrderId(serviceOrder.getId()); 
					accountLine.setServiceOrder(serviceOrder);
					accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
					accountLine.setShipNumber(serviceOrder.getShipNumber());
					accountLine.setCreatedOn(new Date());
					accountLine.setCreatedBy(user);
					accountLine.setUpdatedOn(new Date());
					accountLine.setUpdatedBy(user);
					accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
					///Code for AccountLine division 
					if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
						String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
						try{  
							String roles[] = agentRoleValue.split("~");
						  if(roles[3].toString().equals("hauler")){
							   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
								   accountLine.setDivision("01");
							   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
								   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
									   accountLine.setDivision("01");
								   }else{ 
									   String divisionTemp="";
											if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
											divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
											}
											if(!divisionTemp.equalsIgnoreCase("")){
												accountLine.setDivision(divisionTemp);
											}else{
												accountLine.setDivision(null);
											}
								     
								   }
							   }else{
								   String divisionTemp="";
										if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
										divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
										}
										if(!divisionTemp.equalsIgnoreCase("")){
											accountLine.setDivision(divisionTemp);
										}else{
											accountLine.setDivision(null);
										}
							   }
						  }else{
							  String divisionTemp="";
									if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
									}
									if(!divisionTemp.equalsIgnoreCase("")){
										accountLine.setDivision(divisionTemp);
									}else{
										accountLine.setDivision(null);
									}
						  }
						}catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					///Code for AccountLine division 

					maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
			        if ( maxLineNumber.get(0) == null ) {          
			        	accountLineNumber = "001";
			        }else {
			        	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
			            if((autoLineNumber.toString()).length() == 2) {
			            	accountLineNumber = "0"+(autoLineNumber.toString());
			            }
			            else if((autoLineNumber.toString()).length() == 1) {
			            	accountLineNumber = "00"+(autoLineNumber.toString());
			            } 
			            else {
			            	accountLineNumber=autoLineNumber.toString();
			            }
			        }
			        accountLine.setAccountLineNumber(accountLineNumber); 
					accountLineManager.save(accountLine); 
					
				     }
				if(accountLineStatus==null||accountLineStatus.equals(""))
				{
					accountLineStatus="true";
				}
				accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
        return SUCCESS;   
    
	}
	    
	    @SkipValidation
	    public String addAndCopyServiceOrder() { 
	    	try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				//getComboList(sessionCorpID);
				/*try{
					savePricingLine();
				} catch(Exception e){
					
				}*/
				if (cid != null) {   
					customerFile = customerFileManager.get(cid);
					coordinatorList=refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_COORD",sessionCorpID);
					if(customerFile.getCoordinator()!=null && !(customerFile.getCoordinator().equalsIgnoreCase(""))){
						List aliasNameList=userManager.findUserByUsername(customerFile.getCoordinator());
						if(!aliasNameList.isEmpty()){
							String aliasName=aliasNameList.get(0).toString();
							String coordCode=customerFile.getCoordinator().toUpperCase();
							String alias = aliasName.toUpperCase();
							if(!coordinatorList.containsValue(coordCode)){
								coordinatorList.put(coordCode,alias );
							}
						}
					}
					//service = refMasterManager.findServiceByJob(customerFile.getJob(),sessionCorpID, "SERVICE");
					estimatorList=refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_SALE",sessionCorpID);
					if(customerFile.getSalesMan()!=null && !(customerFile.getSalesMan().equalsIgnoreCase(""))){
						List aliasNameList=userManager.findUserByUsername(customerFile.getSalesMan());
						if(!aliasNameList.isEmpty()){
							String estimatorCode=customerFile.getSalesMan().toUpperCase();
							String aliasName=aliasNameList.get(0).toString();
							String alias = aliasName.toUpperCase();
							if(!estimatorList.containsValue(estimatorCode)){
								estimatorList.put(estimatorCode,alias );
							}
						}
							
					}
					companies =  companyManager.findByCompanyDivis(sessionCorpID);
				    maxShip = customerFileManager.findMaximumShipCF(customerFile.getSequenceNumber(),"",customerFile.getCorpID());
				    
				    if ( maxShip.get(0) == null ) {          
				    	ship = "01";
				    }else  {
				    	autoShip = Long.parseLong((maxShip).get(0).toString()) + 1;
				        if((autoShip.toString()).length() == 1) {
				        	ship = "0"+(autoShip.toString());
				        }else {
				        	ship=autoShip.toString();
				        }
				        prevShip = Long.parseLong((maxShip).get(0).toString());
				        if((prevShip.toString()).length() == 1) {
				        	shipPrev = "0"+(prevShip.toString());
				        }else {
				        	shipPrev=prevShip.toString();
				        }
				        if(!ship.equals("01"))
				        {
				        	shipnumber = customerFile.getSequenceNumber() + shipPrev;
				            tempID=Long.parseLong((serviceOrderManager.findIdByShipNumber(shipnumber)).get(0).toString());
				            serviceOrderPrev=serviceOrderManager.get(tempID);
				        }            
				    }
				    shipnumber = customerFile.getSequenceNumber() + ship;
				    addCopyServiceOrder = (ServiceOrder)serviceOrderManager.get(sid);
				    addCopyMiscellaneous = (Miscellaneous)miscellaneousManager.get(sid);
				    serviceOrder = new ServiceOrder();
				    miscellaneous=new Miscellaneous();
				    miscellaneous.setShipNumber(shipnumber);
				    miscellaneous.setEstimateTareWeight(addCopyMiscellaneous.getEstimateTareWeight());
				    miscellaneous.setActualTareWeight(addCopyMiscellaneous.getActualTareWeight());
				    miscellaneous.setRwghTare(addCopyMiscellaneous.getRwghTare());
				    miscellaneous.setChargeableTareWeight(addCopyMiscellaneous.getChargeableTareWeight());
				    miscellaneous.setEntitleTareWeight(addCopyMiscellaneous.getEntitleTareWeight());
				    miscellaneous.setUnit1(addCopyMiscellaneous.getUnit1());
					miscellaneous.setUnit2(addCopyMiscellaneous.getUnit2());
					miscellaneous.setEntitleGrossWeight(addCopyMiscellaneous.getEntitleGrossWeight());
					miscellaneous.setEstimateGrossWeight(addCopyMiscellaneous.getEstimateGrossWeight());
					miscellaneous.setActualGrossWeight(addCopyMiscellaneous.getActualGrossWeight());
					miscellaneous.setChargeableGrossWeight(addCopyMiscellaneous.getChargeableGrossWeight());
					miscellaneous.setRwghGross(addCopyMiscellaneous.getRwghGross());
					miscellaneous.setEntitleNetWeight(addCopyMiscellaneous.getEntitleNetWeight());
					miscellaneous.setEstimatedNetWeight(addCopyMiscellaneous.getEstimatedNetWeight());
					miscellaneous.setActualNetWeight(addCopyMiscellaneous.getActualNetWeight());
					miscellaneous.setChargeableNetWeight(addCopyMiscellaneous.getChargeableNetWeight());
					miscellaneous.setRwghNet(addCopyMiscellaneous.getRwghNet());
					miscellaneous.setEntitleCubicFeet(addCopyMiscellaneous.getEntitleCubicFeet());
					miscellaneous.setEstimateCubicFeet(addCopyMiscellaneous.getEstimateCubicFeet());
					miscellaneous.setActualCubicFeet(addCopyMiscellaneous.getActualCubicFeet());
					miscellaneous.setChargeableCubicFeet(addCopyMiscellaneous.getChargeableCubicFeet());
					miscellaneous.setRwghCubicFeet(addCopyMiscellaneous.getRwghCubicFeet());
					miscellaneous.setNetEntitleCubicFeet(addCopyMiscellaneous.getNetEntitleCubicFeet());
					miscellaneous.setNetEstimateCubicFeet(addCopyMiscellaneous.getNetEstimateCubicFeet());
					miscellaneous.setNetActualCubicFeet(addCopyMiscellaneous.getNetActualCubicFeet());
					miscellaneous.setChargeableNetCubicFeet(addCopyMiscellaneous.getChargeableNetCubicFeet());
					miscellaneous.setRwghNetCubicFeet(addCopyMiscellaneous.getRwghNetCubicFeet());
					miscellaneous.setEntitleNumberAuto(addCopyMiscellaneous.getEntitleNumberAuto());
					miscellaneous.setEstimateAuto(addCopyMiscellaneous.getEstimateAuto());
					miscellaneous.setActualAuto(addCopyMiscellaneous.getActualAuto());
					miscellaneous.setEntitleAutoWeight(addCopyMiscellaneous.getEntitleAutoWeight());
					miscellaneous.setEstimatedAutoWeight(addCopyMiscellaneous.getEstimatedAutoWeight());
					miscellaneous.setActualAutoWeight(addCopyMiscellaneous.getActualAutoWeight());
					miscellaneous.setMillitaryShipment(addCopyMiscellaneous.getMillitaryShipment());
					
					miscellaneous.setEntitleTareWeightKilo(addCopyMiscellaneous.getEntitleTareWeightKilo());
					miscellaneous.setEntitleGrossWeightKilo(addCopyMiscellaneous.getEntitleGrossWeightKilo());
					miscellaneous.setEntitleNetWeightKilo(addCopyMiscellaneous.getEntitleNetWeightKilo());
					miscellaneous.setEstimateGrossWeightKilo(addCopyMiscellaneous.getEstimateGrossWeightKilo());
					miscellaneous.setEstimateTareWeightKilo(addCopyMiscellaneous.getEstimateTareWeightKilo());
					miscellaneous.setEstimatedNetWeightKilo(addCopyMiscellaneous.getEstimatedNetWeightKilo());
					miscellaneous.setActualGrossWeightKilo(addCopyMiscellaneous.getActualGrossWeightKilo());
					miscellaneous.setActualTareWeightKilo(addCopyMiscellaneous.getActualTareWeightKilo());
					miscellaneous.setActualNetWeightKilo(addCopyMiscellaneous.getActualNetWeightKilo());
					miscellaneous.setChargeableGrossWeightKilo(addCopyMiscellaneous.getChargeableGrossWeightKilo());
					miscellaneous.setChargeableTareWeightKilo(addCopyMiscellaneous.getChargeableTareWeightKilo());
					miscellaneous.setChargeableNetWeightKilo(addCopyMiscellaneous.getChargeableNetWeightKilo());
					miscellaneous.setRwghGrossKilo(addCopyMiscellaneous.getRwghGrossKilo());
					miscellaneous.setRwghTareKilo(addCopyMiscellaneous.getRwghTareKilo());
					miscellaneous.setRwghNetKilo(addCopyMiscellaneous.getRwghNetKilo());
					miscellaneous.setEntitleCubicMtr(addCopyMiscellaneous.getEntitleCubicMtr());
					miscellaneous.setNetEntitleCubicMtr(addCopyMiscellaneous.getNetEntitleCubicMtr());
					miscellaneous.setEstimateCubicMtr(addCopyMiscellaneous.getEstimateCubicMtr());
					miscellaneous.setNetEstimateCubicMtr(addCopyMiscellaneous.getNetEstimateCubicMtr());
					miscellaneous.setActualCubicMtr(addCopyMiscellaneous.getActualCubicMtr());
					miscellaneous.setNetActualCubicMtr(addCopyMiscellaneous.getNetActualCubicMtr());
					miscellaneous.setChargeableCubicMtr(addCopyMiscellaneous.getChargeableCubicMtr());
					miscellaneous.setChargeableNetCubicMtr(addCopyMiscellaneous.getChargeableNetCubicMtr());
					miscellaneous.setRwghCubicMtr(addCopyMiscellaneous.getRwghCubicMtr());
					miscellaneous.setRwghNetCubicMtr(addCopyMiscellaneous.getRwghNetCubicMtr());
					
					
					miscellaneous.setCreatedOn(new Date());
					miscellaneous.setUpdatedOn(new Date());
					miscellaneous.setUpdatedBy(getRequest().getRemoteUser());
					miscellaneous.setCreatedBy(getRequest().getRemoteUser());
					serviceOrder.setTransitDays(addCopyServiceOrder.getTransitDays());
					serviceOrder.setEquipment	(addCopyServiceOrder.getEquipment());
					serviceOrder.setLastName(addCopyServiceOrder.getLastName());
				    serviceOrder.setDefaultAccountLineStatus(false);
				    serviceOrder.setCustomerFileId	(addCopyServiceOrder.getCustomerFileId());
					serviceOrder.setSequenceNumber	(addCopyServiceOrder.getSequenceNumber());
					serviceOrder.setShip(ship);
					serviceOrder.setShipNumber	(shipnumber);
					serviceOrder.setControlFlag	(addCopyServiceOrder.getControlFlag());
				    serviceOrder.setRegistrationNumber	(addCopyServiceOrder.getRegistrationNumber());
				    //Bug 11839 - Possibility to modify Current status to Prior status
				    serviceOrder.setStatusDate	(new Date());
				    serviceOrder.setJob	(addCopyServiceOrder.getJob());
					serviceOrder.setRouting	(addCopyServiceOrder.getRouting());
					serviceOrder.setServiceType	(addCopyServiceOrder.getServiceType());
					serviceOrder.setMode	(addCopyServiceOrder.getMode());
					serviceOrder.setPackingMode	(addCopyServiceOrder.getPackingMode());
					serviceOrder.setPackingService(addCopyServiceOrder.getPackingService());
					serviceOrder.setPayType	(addCopyServiceOrder.getPayType());
					serviceOrder.setCoordinator	(addCopyServiceOrder.getCoordinator());
					
				    serviceOrder.setSalesMan	(addCopyServiceOrder.getSalesMan());
					
				    serviceOrder.setPrefix	(addCopyServiceOrder.getPrefix());
					
				    serviceOrder.setFirstName	(addCopyServiceOrder.getFirstName());
					
				    serviceOrder.setLastName	(addCopyServiceOrder.getLastName());
					
				    serviceOrder.setMi	(addCopyServiceOrder.getMi());
					
				    serviceOrder.setOriginAddressLine1	(addCopyServiceOrder.getOriginAddressLine1());
					
				    serviceOrder.setSuffix	(addCopyServiceOrder.getSuffix());
					
				    serviceOrder.setOriginAddressLine2	(addCopyServiceOrder.getOriginAddressLine2());
					
				    serviceOrder.setOriginAddressLine3	(addCopyServiceOrder.getOriginAddressLine3());
					
				    serviceOrder.setOriginCity	(addCopyServiceOrder.getOriginCity());
					
				    serviceOrder.setOriginState	(addCopyServiceOrder.getOriginState());
					
				    serviceOrder.setOriginCountryCode	(addCopyServiceOrder.getOriginCountryCode());
							   		
				    serviceOrder.setOriginCountry	(addCopyServiceOrder.getOriginCountry());
					
				    serviceOrder.setOriginZip	(addCopyServiceOrder.getOriginZip());
				    
				    serviceOrder.setOriginPreferredContactTime(addCopyServiceOrder.getOriginPreferredContactTime());
					
				    serviceOrder.setOriginDayPhone	(addCopyServiceOrder.getOriginDayPhone());
					
				    serviceOrder.setOriginHomePhone	(addCopyServiceOrder.getOriginHomePhone());
					
				    serviceOrder.setOriginFax	(addCopyServiceOrder.getOriginFax());
					
				    serviceOrder.setDestinationAddressLine1	(addCopyServiceOrder.getDestinationAddressLine1());
					
				    serviceOrder.setDestinationAddressLine2	(addCopyServiceOrder.getDestinationAddressLine2());
					
				    serviceOrder.setDestinationAddressLine3	(addCopyServiceOrder.getDestinationAddressLine3());
					
				    serviceOrder.setDestinationCity	(addCopyServiceOrder.getDestinationCity());
					
				    serviceOrder.setDestinationCityCode	(addCopyServiceOrder.getDestinationCityCode());
					
				    serviceOrder.setOriginCityCode	(addCopyServiceOrder.getOriginCityCode());
					
				    serviceOrder.setDestinationState	(addCopyServiceOrder.getDestinationState());
					
				    serviceOrder.setDestinationCountryCode	(addCopyServiceOrder.getDestinationCountryCode());
						
				    serviceOrder.setDestinationCountry	(addCopyServiceOrder.getDestinationCountry());
					
				    serviceOrder.setDestinationZip	(addCopyServiceOrder.getDestinationZip());
				    
				    serviceOrder.setDestPreferredContactTime(addCopyServiceOrder.getDestPreferredContactTime());
					
				    serviceOrder.setDestinationDayPhone	(addCopyServiceOrder.getDestinationDayPhone());
					
				    serviceOrder.setDestinationHomePhone	(addCopyServiceOrder.getDestinationHomePhone());
					
				    serviceOrder.setDestinationFax	(addCopyServiceOrder.getDestinationFax());
					
				    serviceOrder.setContactName	(addCopyServiceOrder.getContactName());
				    serviceOrder.setContactNameExt	(addCopyServiceOrder.getContactNameExt());
				    serviceOrder.setOriginContactEmail	(addCopyServiceOrder.getOriginContactEmail());
				    serviceOrder.setContactPhone	(addCopyServiceOrder.getContactPhone());
					
				    serviceOrder.setContactFax	(addCopyServiceOrder.getContactFax());
					
				    serviceOrder.setClientCode	(addCopyServiceOrder.getClientCode());
					
				    serviceOrder.setClientName	(addCopyServiceOrder.getClientName());
					
				    serviceOrder.setClientReference	(addCopyServiceOrder.getClientReference());
					
				    serviceOrder.setCommodity	(addCopyServiceOrder.getCommodity());
					
				    serviceOrder.setContract	(addCopyServiceOrder.getContract());
					
				    serviceOrder.setSocialSecurityNumber	(addCopyServiceOrder.getSocialSecurityNumber());
					
				    serviceOrder.setConsignee	(addCopyServiceOrder.getConsignee());
					
				    serviceOrder.setDestinationLoadSite	(addCopyServiceOrder.getDestinationLoadSite());
					
				    serviceOrder.setEstimator	(addCopyServiceOrder.getEstimator());
					
				    serviceOrder.setLastExtra	(addCopyServiceOrder.getLastExtra());
					
				    serviceOrder.setLastItem	(addCopyServiceOrder.getLastItem());
				    serviceOrder.setNextCheckOn	(addCopyServiceOrder.getNextCheckOn());
					
				    serviceOrder.setOriginContactName	(addCopyServiceOrder.getOriginContactName());
				    serviceOrder.setOriginContactNameExt	(addCopyServiceOrder.getOriginContactNameExt());
				    serviceOrder.setDestinationContactEmail	(addCopyServiceOrder.getDestinationContactEmail());
				    serviceOrder.setOriginContactPhone	(addCopyServiceOrder.getOriginContactPhone());
					
				    serviceOrder.setOriginContactWork	(addCopyServiceOrder.getOriginContactWork());
					
				    serviceOrder.setOriginLoadSite	(addCopyServiceOrder.getOriginLoadSite());
					
				    serviceOrder.setOrderBy	(addCopyServiceOrder.getOrderBy());
					
				    serviceOrder.setOrderPhone	(addCopyServiceOrder.getOrderPhone());
					
				    serviceOrder.setSuffixNext	(addCopyServiceOrder.getSuffixNext());
					
				    serviceOrder.setEmail	(addCopyServiceOrder.getEmail());
						
				    serviceOrder.setOriginCompany	(addCopyServiceOrder.getOriginCompany());
					
				    serviceOrder.setDestinationCompany	(addCopyServiceOrder.getDestinationCompany());
					
				    serviceOrder.setOriginMilitary	(addCopyServiceOrder.getOriginMilitary());
					
				    serviceOrder.setDestinationMilitary	(addCopyServiceOrder.getDestinationMilitary());
					
				    
						
				    serviceOrder.setLastName2	(addCopyServiceOrder.getLastName2());
					
				    serviceOrder.setFirstName2	(addCopyServiceOrder.getFirstName2());
					
				    serviceOrder.setPrefix2	(addCopyServiceOrder.getPrefix2());
					
				    serviceOrder.setInitial2	(addCopyServiceOrder.getInitial2());
					
				    serviceOrder.setAndor	(addCopyServiceOrder.getAndor());
					
				    serviceOrder.setBillToCode	(addCopyServiceOrder.getBillToCode());
					
				    serviceOrder.setBillToName	(addCopyServiceOrder.getBillToName());
					
				    serviceOrder.setOriginDayExtn	(addCopyServiceOrder.getOriginDayExtn());
					
				    serviceOrder.setDestinationDayExtn	(addCopyServiceOrder.getDestinationDayExtn());
					
				    serviceOrder.setVip	(addCopyServiceOrder.getVip());
				    serviceOrder.setRevised	(addCopyServiceOrder.getRevised());
				   
				    serviceOrder.setCompanyDivision(addCopyServiceOrder.getCompanyDivision());
				    	    
				    serviceOrder.setPortOfLading	(addCopyServiceOrder.getPortOfLading());
				    	    
				    serviceOrder.setPortOfEntry 	(addCopyServiceOrder.getPortOfEntry()); 
					
				    serviceOrder.setOriginArea	(addCopyServiceOrder.getOriginArea());
					
				    serviceOrder.setDestinationArea	(addCopyServiceOrder.getDestinationArea());
					
				    serviceOrder.setTripNumber	(addCopyServiceOrder.getTripNumber());
					
				    serviceOrder.setEmail2	(addCopyServiceOrder.getEmail2());
				    serviceOrder.setOriginContactExtn	(addCopyServiceOrder.getOriginContactExtn());
					serviceOrder.setRank(addCopyServiceOrder.getRank());
				    serviceOrder.setDestinationContactExtn	(addCopyServiceOrder.getDestinationContactExtn());
					serviceOrder.setEmailSent	(addCopyServiceOrder.getEmailSent());
				    serviceOrder.setQuoteStatus	(addCopyServiceOrder.getQuoteStatus());
				    serviceOrder.setQuoteAcceptReason(addCopyServiceOrder.getQuoteAcceptReason());
				    serviceOrder.setEstimateGrossWeight	(addCopyServiceOrder.getEstimateGrossWeight());
					
				    serviceOrder.setActualGrossWeight	(addCopyServiceOrder.getActualGrossWeight());
					
				    serviceOrder.setEstimatedNetWeight	(addCopyServiceOrder.getEstimatedNetWeight());
					
				    serviceOrder.setActualNetWeight	(addCopyServiceOrder.getActualNetWeight());
					
				    serviceOrder.setEntitleNumberAuto	(addCopyServiceOrder.getEntitleNumberAuto());
					
				    serviceOrder.setEntitleBoatYN	(addCopyServiceOrder.getEntitleBoatYN());
					
				    serviceOrder.setEstimateAuto	(addCopyServiceOrder.getEstimateAuto());
					
				    serviceOrder.setEstimateBoat	(addCopyServiceOrder.getEstimateBoat());
					
				    serviceOrder.setActualAuto	(addCopyServiceOrder.getActualAuto());
					
				    serviceOrder.setActualBoat	(addCopyServiceOrder.getActualBoat());
					
				    
				    serviceOrder.setEstimateCubicFeet	(addCopyServiceOrder.getEstimateCubicFeet());
					
				     serviceOrder.setActualCubicFeet	(addCopyServiceOrder.getActualCubicFeet());
					
				    
				    serviceOrder.setOriginMobile	(addCopyServiceOrder.getOriginMobile());
					
				    serviceOrder.setDestinationMobile	(addCopyServiceOrder.getDestinationMobile());
					
				   	
				    miscellaneous.setEquipment	(addCopyMiscellaneous.getEquipment());
				    	    
				    serviceOrder.setStatusReason	(addCopyServiceOrder.getStatusReason());
				    	    
				    serviceOrder.setBookingAgentCode	(addCopyServiceOrder.getBookingAgentCode());
					
				    serviceOrder.setBookingAgentName	(addCopyServiceOrder.getBookingAgentName());
					
				    serviceOrder.setEntitledTotalAmount	(addCopyServiceOrder.getEntitledTotalAmount());
					
				    serviceOrder.setEstimatedTotalExpense	(new BigDecimal(0));
					
				    serviceOrder.setEstimatedTotalRevenue	(new BigDecimal(0));
					
				    serviceOrder.setDistributedTotalAmount	(addCopyServiceOrder.getDistributedTotalAmount());
					
				    serviceOrder.setEstimatedGrossMargin	(new BigDecimal(0));
					
				    serviceOrder.setEstimatedGrossMarginPercentage	(new BigDecimal(0));
					
				    serviceOrder.setRevisedTotalExpense	(addCopyServiceOrder.getRevisedTotalExpense());
					
				    serviceOrder.setRevisedTotalRevenue	(addCopyServiceOrder.getRevisedTotalRevenue());
					
				    serviceOrder.setRevisedGrossMargin	(addCopyServiceOrder.getRevisedGrossMargin());
					
				    serviceOrder.setRevisedGrossMarginPercentage	(addCopyServiceOrder.getRevisedGrossMarginPercentage());
					
				    serviceOrder.setActualExpense	(addCopyServiceOrder.getActualExpense());
					
				    serviceOrder.setActualRevenue	(addCopyServiceOrder.getActualRevenue());
					
				    serviceOrder.setActualGrossMargin	(addCopyServiceOrder.getActualGrossMargin());
					
				    serviceOrder.setActualGrossMarginPercentage	(addCopyServiceOrder.getActualGrossMarginPercentage());
					
				    serviceOrder.setCustomerFeedback("Yes");
				    	    
				    serviceOrder.setDestinationEmail(addCopyServiceOrder.getDestinationEmail());
				    serviceOrder.setIncExcServiceType(addCopyServiceOrder.getIncExcServiceType());
				    serviceOrder.setIncExcServiceTypeLanguage(addCopyServiceOrder.getIncExcServiceTypeLanguage());
				    serviceOrder.setDestinationEmail2(addCopyServiceOrder.getDestinationEmail2());
				    serviceOrder.setBookingAgentCheckedBy(addCopyServiceOrder.getBookingAgentCheckedBy());
				    serviceOrder.setBookingAgentVanlineCode(addCopyServiceOrder.getBookingAgentVanlineCode());
				    serviceOrder.setGrpPref(addCopyServiceOrder.getGrpPref());
				    serviceOrder.setQuoteAccept(addCopyServiceOrder.getQuoteAccept());
				  //Bug 11839 - Possibility to modify Current status to Prior status
				    serviceOrder.setStatus("NEW");
					serviceOrder.setStatusNumber(1);
					serviceOrder.setCorpID(sessionCorpID);
					serviceOrder.setUnit1(addCopyServiceOrder.getUnit1());
					serviceOrder.setUnit2(addCopyServiceOrder.getUnit2());
					serviceOrder.setCreatedOn(new Date());
					serviceOrder.setUpdatedOn(new Date());
					serviceOrder.setCreatedBy(getRequest().getRemoteUser());
					serviceOrder.setUpdatedBy(getRequest().getRemoteUser());
					if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
						weightType="lbscft";
					}
					else{
						weightType="kgscbm";
					}
					ostates = customerFileManager.findDefaultStateList(addCopyServiceOrder.getOriginCountryCode(), sessionCorpID);
				    dstates = customerFileManager.findDefaultStateList(addCopyServiceOrder.getDestinationCountryCode(), sessionCorpID);
				    originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
				  	destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());			
				  	companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,addCopyServiceOrder.getBookingAgentCode());
					companies =  companyManager.findByCompanyDivis(sessionCorpID);
					}  
				serviceOrder.setCustomerFile(customerFile);
				serviceOrder= serviceOrderManager.save(serviceOrder);

				List tripList1 = dspDetailsManager.getListById(serviceOrder.getId());
				if(tripList1.isEmpty()){
					dspDetails = new DspDetails();
					dspDetails.setId(serviceOrder.getId());
					dspDetails.setCorpID(serviceOrder.getCorpID());
					dspDetails.setCreatedOn(new Date());
					dspDetails.setUpdatedOn(new Date());
					dspDetails.setCreatedBy(getRequest().getRemoteUser());
					dspDetails.setUpdatedBy(getRequest().getRemoteUser());
					dspDetails.setServiceOrderId(serviceOrder.getId());
					dspDetails.setShipNumber(serviceOrder.getShipNumber());
					dspDetailsManager.save(dspDetails);
				}            


				i = Long.parseLong(serviceOrderManager.findMaximumId().get(0).toString());
				serviceOrder = serviceOrderManager.get(i); 
				miscellaneous.setId(serviceOrder.getId());
				miscellaneous.setShipNumber(serviceOrder.getShipNumber());
				miscellaneous.setShip(serviceOrder.getShip());
				miscellaneous.setSequenceNumber(serviceOrder.getSequenceNumber());
				miscellaneous.setCorpID(serviceOrder.getCorpID());
				miscellaneous.setCreatedOn(serviceOrder.getCreatedOn());
				miscellaneousManager.save(miscellaneous);  
				trackingStatus = new TrackingStatus();
				trackingStatus.setId(serviceOrder.getId());
				trackingStatus.setCorpID(serviceOrder.getCorpID());
				trackingStatus.setShipNumber(serviceOrder.getShipNumber());
				trackingStatus.setSitOriginDays(0);
				trackingStatus.setSitDestinationDays(0);
				trackingStatus.setFromWH(0);
				trackingStatus.setDemurrageCostPerDay2(0);
				trackingStatus.setUsdaCost(0);
				trackingStatus.setInspectorCost(0);
				trackingStatus.setXrayCost(0);
				trackingStatus.setPerdiumCostPerDay(0);
				trackingStatus.setDemurrageCostPerDay1(0);
				trackingStatus.setStatus(serviceOrder.getCustomerFile().getStatus());
				trackingStatus.setCreatedOn(serviceOrder.getCreatedOn());
				trackingStatus.setUpdatedOn(new Date());
				trackingStatus.setCreatedBy(serviceOrder.getCreatedBy());
				trackingStatus.setUpdatedBy(getRequest().getRemoteUser()); 
				trackingStatus.setSequenceNumber(serviceOrder.getSequenceNumber());
				trackingStatus.setSurveyTimeFrom(serviceOrder.getCustomerFile().getSurveyTime());
				trackingStatus.setSurveyTimeTo(serviceOrder.getCustomerFile().getSurveyTime2());
				trackingStatus.setStatusDate(serviceOrder.getCustomerFile().getStatusDate());
				trackingStatus.setSurveyDate(serviceOrder.getCustomerFile().getSurvey());
				trackingStatus.setSurvey(serviceOrder.getCustomerFile().getSurvey());
				trackingStatus.setInitialContact(serviceOrder.getCustomerFile().getInitialContactDate());
				trackingStatus.setSurveyTimeFrom(serviceOrder.getCustomerFile().getSurveyTime());
				trackingStatus.setSurveyTimeTo(serviceOrder.getCustomerFile().getSurveyTime2());
				trackingStatus.setServiceOrder(serviceOrder);
				trackingStatus.setMiscellaneous(miscellaneous);
				trackingStatusManager.save(trackingStatus); 
					billing = new Billing();
					billing.setShipNumber(serviceOrder.getShipNumber());
					billing.setSequenceNumber(serviceOrder.getSequenceNumber());
					billing.setCreatedOn(serviceOrder.getCreatedOn());
					billing.setUpdatedOn(new Date());
					billing.setCreatedBy(serviceOrder.getCreatedBy());
					billing.setUpdatedBy(getRequest().getRemoteUser());
					billing.setBillingInsurancePayableCurrency(baseCurrency);
					billing.setCorpID(sessionCorpID);
					billing.setBillToCode(serviceOrder.getCustomerFile().getBillToCode());
					billing.setBillToName(serviceOrder.getCustomerFile().getBillToName());
					billing.setNetworkBillToCode(serviceOrder.getCustomerFile().getAccountCode());
			    	billing.setNetworkBillToName(serviceOrder.getCustomerFile().getAccountName());
					billing.setBillToAuthority(serviceOrder.getCustomerFile().getBillToAuthorization());
					billing.setBillingId(serviceOrder.getCustomerFile().getBillToCode());
					billing.setBillName(serviceOrder.getCustomerFile().getBillToName());
					billing.setBillTo1Point(serviceOrder.getCustomerFile().getBillPayMethod());
					billing.setContract(serviceOrder.getContract());
					billing.setPersonBilling(serviceOrder.getCustomerFile().getPersonBilling());
					billing.setPersonPayable(serviceOrder.getCustomerFile().getPersonPayable());
					billing.setPersonPricing(serviceOrder.getCustomerFile().getPersonPricing());
					billing.setAuditor(serviceOrder.getCustomerFile().getAuditor());
					billing.setBillTo2Code("");
					billing.setPrivatePartyBillingCode("");
					billing.setNoCharge(false);
					sysDefaultDetail = billingManager.findDefaultBookingAgentDetail(sessionCorpID);
					if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
						for (SystemDefault systemDefault : sysDefaultDetail) {
							billing.setCurrency(systemDefault.getBaseCurrency());
						}
					}
					billing.setServiceOrder(serviceOrder); 
					billing.setId(serviceOrder.getId());
					String creditTerm="";
					try{
					if(serviceOrder.getBillToCode()!=null && (!(serviceOrder.getBillToCode().toString().equals("")))){	
					creditTerm=partnerPrivateManager.getTheCreditTerm(serviceOrder.getBillToCode(),sessionCorpID);
					}
					}catch(Exception e){
						creditTerm="";
					} 
					billing.setCreditTerms(creditTerm); 
					billingManager.save(billing);
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	e.printStackTrace();
		    	return "errorlog";
			}
	    		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	    return SUCCESS;   
	    }
	  
	  
	  @SkipValidation 
	    public String quoteAcceptance(){
		  try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			  serviceOrderManager.quoteAcceptanceUpdate(soid,requestedAQP,sessionCorpID,getRequest().getRemoteUser());
			  if(requestedAQP.equals("A")){
				  serviceOrder=serviceOrderManager.get(new Long(soid)); 
				 
			  }
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		  return SUCCESS;
	  }
	  
	  @SkipValidation 
	    public String quoteRejectedStatus(){
		  try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			  serviceOrder = serviceOrderManager.get(Long.parseLong(soid));
			  String qStatus =serviceOrder.getQuoteStatus() ;
			  serviceOrderManager.quoteStatusReasonUpdate(soid,requestedAQP,sessionCorpID,getRequest().getRemoteUser(),qStatus);
		} catch (NumberFormatException e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		  return SUCCESS;
	  }
	  
		private boolean checkAccessQuotation=false;
		private String updateTypeFlag="";
		private String allIdOFSo;
	  @SkipValidation 
	    public String quoteAcceptanceForQuote(){
		  try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			  String comptetive="";
			  if(customerFileId!=null)
			  {
				  customerFile = customerFileManager.get(customerFileId);
				  comptetive=customerFile.getComptetive();
			  }
			  serviceOrder =serviceOrderManager.get(Long.parseLong(soid));
			  serviceOrderManager.quoteFileUpdate(soid,requestedAQP,sessionCorpID,getRequest().getRemoteUser(),customerFileId,comptetive,checkAccessQuotation);
			  if(requestedAQP.equals("A")){
				  serviceOrder=serviceOrderManager.get(new Long(soid));
				  
			  }
			  if(serviceOrder.getIsNetworkRecord()){
					List linkedShipNumber=findLinkerShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					Iterator  it=serviceOrderRecords.iterator();
			    	while(it.hasNext()){
			    		ServiceOrder serviceOrderToRecods=(ServiceOrder)it.next(); 
			    		if(!(serviceOrder.getShipNumber().equals(serviceOrderToRecods.getShipNumber()))){
			    		serviceOrderManager.linkedQuoteFileUpdate(serviceOrderToRecods.getId(),requestedAQP,serviceOrderToRecods.getCorpID(),getRequest().getRemoteUser(),serviceOrderToRecods.getCustomerFileId(),comptetive,checkAccessQuotation,sessionCorpID);	
			    		}
			    	}
					//synchornizeServiceOrder(serviceOrderRecords,serviceOrder,trackingStatus);
					//synchornizeMiscellaneous(miscellaneousRecords,miscellaneous,trackingStatus);
				}
			  if(requestedAQP.equals("A") ){ 
			  	customerFileTemp = customerFileManager.get(customerFileId);
				customerServiceOrders = customerFileTemp.getServiceOrders(); 
				if (customerServiceOrders != null) {

					Iterator it = customerServiceOrders.iterator();
					while (it.hasNext()) { 
						serviceOrderTemp = (ServiceOrder) it.next(); 
						
						if(serviceOrderTemp.getQuoteAccept().equals("AC") ){
							serviceOrderManager.updateQuoteStatus(serviceOrderTemp.getId(),"NEW","1","A",getRequest().getRemoteUser(),"AC");
						 }
						if(serviceOrderTemp.getQuoteAccept().equals("PE")|| serviceOrderTemp.getQuoteAccept().equals("")){
							serviceOrderManager.updateQuoteStatus(serviceOrderTemp.getId(),"HOLD","100","P",getRequest().getRemoteUser(),"PE");
						 }
						if(serviceOrderTemp.getQuoteAccept().equals("RE")){
							serviceOrderManager.updateQuoteStatus(serviceOrderTemp.getId(),"CNCL","400","R",getRequest().getRemoteUser(),"RE");
						 }
						if(serviceOrderTemp.getIsNetworkRecord()){
				  			List linkedShipNumber=findLinkerShipNumber(serviceOrderTemp);
				  			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrderTemp);
				  			Iterator  its=serviceOrderRecords.iterator();
				  	    	while(its.hasNext()){
				  	    		ServiceOrder serviceOrderToRecodsTemp=(ServiceOrder)its.next(); 
				  	    		if(!(serviceOrderTemp.getShipNumber().equals(serviceOrderToRecodsTemp.getShipNumber()))){
				  	    		if(serviceOrderTemp.getQuoteAccept().equals("AC") ){
				  					serviceOrderManager.linkedUpdateQuoteStatus(serviceOrderToRecodsTemp.getId(),"NEW","1","A",getRequest().getRemoteUser(),"AC",sessionCorpID);
				  				 }
				  				if(serviceOrderTemp.getQuoteAccept().equals("PE")|| serviceOrderTemp.getQuoteAccept().equals("")){
				  					serviceOrderManager.linkedUpdateQuoteStatus(serviceOrderToRecodsTemp.getId(),"HOLD","100","P",getRequest().getRemoteUser(),"PE",sessionCorpID);
				  				 }
				  				if(serviceOrderTemp.getQuoteAccept().equals("RE")){
				  					serviceOrderManager.linkedUpdateQuoteStatus(serviceOrderToRecodsTemp.getId(),"CNCL","400","R",getRequest().getRemoteUser(),"RE",sessionCorpID);
				  				 }
				  	    		}
				  	    	}
				  			//synchornizeServiceOrder(serviceOrderRecords,serviceOrder,trackingStatus);
				  			//synchornizeMiscellaneous(miscellaneousRecords,miscellaneous,trackingStatus);
				  		}
						}
				}
				if(updateTypeFlag.equalsIgnoreCase("")){
					serviceOrderManager.updateQuoteStatus(Long.parseLong(soid),"NEW","1","A",getRequest().getRemoteUser(),"AC");
				}else{
					serviceOrderManager.updateAllQuoteStatus(allIdOFSo,"NEW","1","A",getRequest().getRemoteUser(),"AC");
				}
			  if(serviceOrder.getIsNetworkRecord()){
					List linkedShipNumber=findLinkerShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					Iterator  it=serviceOrderRecords.iterator();
			    	while(it.hasNext()){
			    		ServiceOrder serviceOrderToRecods=(ServiceOrder)it.next(); 
			    		if(!(serviceOrder.getShipNumber().equals(serviceOrderToRecods.getShipNumber()))){
			    			serviceOrderManager.linkedUpdateQuoteStatus(serviceOrderToRecods.getId(),"NEW","1","A",getRequest().getRemoteUser(),"AC",sessionCorpID);	
			    		}
			    	}
					//synchornizeServiceOrder(serviceOrderRecords,serviceOrder,trackingStatus);
					//synchornizeMiscellaneous(miscellaneousRecords,miscellaneous,trackingStatus);
				}
				}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
	      logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		  return SUCCESS;
	  }
	  
	  @SkipValidation
	    private List findLinkerShipNumber(ServiceOrder serviceOrder) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	List linkedShipNumberList= new ArrayList();
	    	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
	    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
	    	}else{
	    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
	    	}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return linkedShipNumberList;
	    }
	  @SkipValidation
	    private List<Object> findServiceOrderRecords(List linkedShipNumber,	ServiceOrder serviceOrderLocal) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			List<Object> recordList= new ArrayList();
	    	Iterator it =linkedShipNumber.iterator();
	    	while(it.hasNext()){
	    		String shipNumber= it.next().toString();
	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
	    		recordList.add(serviceOrderRemote);
	    	}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return recordList;
	    	}  

	public List getWeightunits() {
		return weightunits;
	}

	public List getVolumeunits() {
		return volumeunits;
	}

	public List getLengthunits() {
		return lengthunits;
	}

	public Map<String, String> getState() {
		return state;
	}

	public Map<String, String> getOcountry() {
		return ocountry;
	}

	public Map<String, String> getDcountry() {
		return dcountry;
	}

	public Map<String, String> getJob() {
		return job;
	}

	public Map<String, String> getRouting() {
		return routing;
	}

	public Map<String, String> getCommodit() {
		return commodit;
	}

	public Map<String, String> getCommodits() {
		return commodits;
	}

	public Map<String, String> getSpecial() {
		return Special;
	}

	public Map<String, String> getHowpack() {
		return Howpack;
	}

	/*public  Map<String, String> getMode() {
	 return mode;
	 }
	 */
	public List getMode() {
		return mode;
	}

	public Map<String, String> getService() {
		return service;
	}

	public Map<String, String> getPkmode() {
		return pkmode;
	}

	public Map<String, String> getLoadsite() {
		return loadsite;
	}

	public Map<String, String> getYesno() {
		return yesno;
	}

	public Map<String, String> getSpecific() {
		return specific;
	}

	public Map<String, String> getjobtype() {
		return jobtype;
	}

	public Map<String, String> getMilitary() {
		return military;
	}

	public Map<String, String> getAGTPER() {
		return AGTPER;
	}

	public Map<String, String> getEQUIP() {
		return EQUIP;
	}

	public Map<String, String> getJOB_STATUS() {
		return JOB_STATUS;
	}
	
	public Map<String, String> getPaytype() {
		return paytype;
	}

	public Map<String, String> getTcktservc() {
		return tcktservc;
	}

	public Map<String, String> getDept() {
		return dept;
	}

	public Map<String, String> getHouse() {
		return house;
	}

	public Map<String, String> getConfirm() {
		return confirm;
	}

	public Map<String, String> getTcktactn() {
		return tcktactn;
	}

	public Map<String, String> getWoinstr() {
		return woinstr;
	}

	public Map<String, String> getGlcodes() {
		return glcodes;
	}

	public Map<String, String> getPeak() {
		return Peak;
	}

	public Map<String, String> getIsdriver() {
		return isdriver;
	}

	public Map<String, String> getPartnerType() {
		return partnerType;
	}

	public Map<String, String> getSITOUTTA() {
		return SITOUTTA;
	}

	public Map<String, String> getOmni() {
		return omni;
	}

	public Map<String, String> getQUOTESTATUS() {
		return QUOTESTATUS;
	}

	public Map<String, String> getCategory() {
		return category;
	}

	public Map<String, String> getBasis() {
		return basis;
	}

	public Map<String, String> getCountry() {
		return country;
	}

	public Map<String, String> getPayingStatus() {
		return payingStatus;
	}

	public List getClaims() {
		return claims;
	}

	public Map<String, String> getSale() {
		return sale;
	}

	public Map<String, String> getCoord() {
		return coord;
	}

	public Map<String, String> getQc() {
		return qc;
	}

	private BillingManager billingManager;

	private List payables;

	private List receivables;

	private List costings;

	public int billingFlag;

	private ServiceOrder serviceOrderPrev;

	private long tempID;

	private String partnerCode;

	public long getTempID() {
		return tempID;
	}

	public void setTempID(long tempID) {
		this.tempID = tempID;
	}

	public ServiceOrder getServiceOrderPrev() {
		return serviceOrderPrev;
	}

	public void setServiceOrderPrev(ServiceOrder serviceOrderPrev) {
		this.serviceOrderPrev = serviceOrderPrev;
	}

	public List getCostings() {
		return costings;
	}

	public List getPayables() {
		return payables;
	}

	public List getContainers() {
		return containers;
	}

	public List getCartons() {
		return cartons;
	}

	public List getVehicles() {
		return vehicles;
	}

	public List getServicePartners() {
		return servicePartners;
	}

	public List getServicePartnersPopup() {
		return servicePartnersPopup;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public String delete() {
		serviceOrderManager.remove(serviceOrder.getId());
		saveMessage(getText("serviceOrder.deleted"));

		return SUCCESS;
	}

	@SkipValidation
	public String getNotesForIconChange() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			List m = notesManager.countForVipDetailNotes(serviceOrder.getShipNumber());
			List mo = notesManager.countForOriginDetailNotes(serviceOrder.getShipNumber());
			List md = notesManager.countForDestinationDetailNotes(serviceOrder.getShipNumber());
			List ms = notesManager.countForWeightDetailNotes(serviceOrder.getShipNumber());
			List mso = notesManager.countForServiceOrderNotes(serviceOrder.getShipNumber());

			if (mo.isEmpty()) {
				countOriginDetailNotes = "0";
			} else {
				countOriginDetailNotes = ((mo).get(0)).toString();
			}

			if (md.isEmpty()) {
				countDestinationDetailNotes = "0";
			} else {
				countDestinationDetailNotes = ((md).get(0)).toString();
			}

			if (ms.isEmpty()) {
				countWeightDetailNotes = "0";
			} else {
				countWeightDetailNotes = ((ms).get(0)).toString();
			}
			if (m.isEmpty()) {
				countVipDetailNotes = "0";
			} else {
				countVipDetailNotes = ((m).get(0)).toString();
			}

			if (mso.isEmpty()) {
				countServiceOrderNotes = "0";
			} else {
				countServiceOrderNotes = ((mso).get(0)).toString();
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private DspDetails dspDetails;
    private DspDetailsManager dspDetailsManager;	
	private String reloServiceType;
	private String systemDefaultStorageValue; 
	private String checkCompanyBA;
	public String save() throws Exception { 
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			 
			companies =  companyManager.findByCompanyDivis(sessionCorpID);
			companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,serviceOrder.getBookingAgentCode());
			ostates = customerFileManager.findDefaultStateList(customerFile.getOriginCountry(), sessionCorpID);
		    dstates = customerFileManager.findDefaultStateList(customerFile.getDestinationCountry(), sessionCorpID);
		    originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
		  	destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());			
		  	salesCommisionRate=serviceOrderManager.findSalesCommisionRate(sessionCorpID).get(0).toString();
			grossMarginThreshold=serviceOrderManager.findGrossMarginThreshold(sessionCorpID).get(0).toString();
			accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString();			

			if (cancel != null) {   
			    return "cancel";   
			}   
     
			if (delete != null) {   
			    return delete();   
			}  
			if(((serviceOrder.getOriginCountry().equalsIgnoreCase("United States")) && (serviceOrder.getOriginState()==null || serviceOrder.getOriginState().equalsIgnoreCase("")))) {
				
				String key = "State is a required field in Origin.";
				errorMessage(getText(key));
				return INPUT;
			
			}
			
			if(((serviceOrder.getDestinationCountry().equalsIgnoreCase("United States") ) && (serviceOrder.getDestinationState() == null || serviceOrder.getDestinationState().equalsIgnoreCase("")))) {
				
				String key = "State is a required field in Destination.";
				errorMessage(getText(key));
				return INPUT;
			
			}
			roleHitFlag="OK";
			serviceOrder.setCustomerFile(customerFile);
			if(serviceOrder.getJob().trim().equalsIgnoreCase("RLO"))
			{
				String serviceDoc=reloServiceType.trim().toString();
				if(serviceDoc.equals("")||serviceDoc.equals("#"))
			  	  {
			  	  }
			  	 else
			  	  {
			  		serviceDoc=serviceDoc.trim();
			  	    if(serviceDoc.indexOf("#")==0)
			  		 {
			  	    	serviceDoc=serviceDoc.substring(1);
			  		 }
			  		if(serviceDoc.lastIndexOf("#")==serviceDoc.length()-1)
			  		 {
			  			serviceDoc=serviceDoc.substring(0, serviceDoc.length()-1);
			  		 } 
			  	  } 
				serviceDoc=serviceDoc.replaceAll("#", ","); 
				serviceOrder.setServiceType(serviceDoc);
			}          
			
			if(checkCompanyBA!=null && (!checkCompanyBA.equalsIgnoreCase(""))&&(checkCompanyBA.equalsIgnoreCase("YES"))){
			if((serviceOrder.getJob()!=null) &&(!serviceOrder.getJob().equalsIgnoreCase("")) ){
				sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						 systemDefaultStorageValue = systemDefault.getStorage();								
						}
					}
				   if(systemDefaultStorageValue.contains(serviceOrder.getJob())){
				    List companyDivisionBookAgentValue = serviceOrderManager.findCompanyDivisionBookAgent(sessionCorpID);
				    if(companyDivisionBookAgentValue!=null && (!companyDivisionBookAgentValue.isEmpty())&& (companyDivisionBookAgentValue.get(0)!=null) &&(!companyDivisionBookAgentValue.get(0).toString().equals("")) ){
					    if(!companyDivisionBookAgentValue.contains(serviceOrder.getBookingAgentCode())){
						     String defaultBookingAgentCode="";
						     String defaultBookingAgentName="";
						     List bookAgent = customerFileManager.findDefaultBookingAgentCode(serviceOrder.getCompanyDivision());
						     if((bookAgent!=null)&&(!bookAgent.isEmpty())&&(bookAgent.get(0)!=null)){
						    	 CompanyDivision companyDivision  = (CompanyDivision)bookAgent.get(0);
						    	 	defaultBookingAgentCode = companyDivision.getBookingAgentCode();
									defaultBookingAgentName = companyDivision.getDescription();
								}
						     serviceOrder.setBookingAgentCode(defaultBookingAgentCode);
						     serviceOrder.setBookingAgentName(defaultBookingAgentName);
						     String key = "Booking agent should be Company Division's Booking Agent";							     
						     String key1= "Booking agent code has been changed to:" + serviceOrder.getBookingAgentCode(); 
						     saveMessage(getText(key+"<BR>"+key1));
						     //return SUCCESS;
					    }else{ 
					    	
					    } 
				   }   
				  }
				}
			}
			
			if(accountLineStatus==null||accountLineStatus.equals(""))
			{
				accountLineStatus="true";
			}
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			boolean isNew = (serviceOrder.getId() == null); 
			checkShipNumberList = serviceOrderManager.findShipNumber(serviceOrder.getShipNumber());
			if(!(checkShipNumberList.isEmpty()) && isNew)
			{
				autoShip = Long.parseLong(serviceOrder.getShip().toString()) + 1;
			    if((autoShip.toString()).length() == 1) {
			    	ship = "0"+(autoShip.toString());
			    }else {
			    	ship=autoShip.toString();
			    }
			    serviceOrder.setShip(ship);
				serviceOrder.setShipNumber(serviceOrder.getSequenceNumber()+ship);
			 }
			if(isNew){
				serviceOrder.setCreatedOn(new Date());
				serviceOrder.setCreatedBy(getRequest().getRemoteUser());
				estimatorList = refMasterManager.findUser(sessionCorpID,"ROLE_SALE");
				coordinatorList = refMasterManager.findUser(sessionCorpID,"ROLE_COORD");
			} 
			serviceOrder.setUpdatedOn(new Date());
			serviceOrder.setUpdatedBy(getRequest().getRemoteUser());
			serviceOrder.setCorpID(sessionCorpID);
			if(serviceOrder.getOriginState() == null){
				serviceOrder.setOriginState("");
			}
			if(serviceOrder.getDestinationState() == null){
				serviceOrder.setDestinationState("");
			} 
			if(!isNew){
   
			   if(billingManager.getByShipNumber(serviceOrder.getShipNumber()).isEmpty()) 
			   {
				   billing = new Billing();
			   }
			   else
			   {
			    billing = billingManager.getByShipNumber(serviceOrder.getShipNumber()).get(0);
			    billing.setContract(serviceOrder.getContract());
			    Contract contract= contractManager.getContactListByContarct(sessionCorpID,serviceOrder.getContract());
				if(contract.getPayMethod()!=null){
					billing.setBillTo1Point(contract.getPayMethod());
				}
				try{
					if(contract.getfXRateOnActualizationDate()!=null && contract.getfXRateOnActualizationDate()){
						billing.setfXRateOnActualizationDate(contract.getfXRateOnActualizationDate());
					}else{
						billing.setfXRateOnActualizationDate(false);
					}	
				}catch(Exception e){e.printStackTrace();}
				if(contract.getBillingInstructionCode()!=null){
					billing.setBillingInstructionCodeWithDesc(contract.getBillingInstructionCode());
					String[] str2 = (contract.getBillingInstructionCode()).split(":");
					if(str2!=null && ((str2.length>=2)) && str2[1]!=null){
					String billingInstruction =str2[1];
					billing.setBillingInstruction(billingInstruction);
					}
				}
			    billing=billingManager.save(billing);
			   } 
			    if(trackingStatusManager.checkById(serviceOrder.getId()).isEmpty())
			    {
			    	trackingStatus=new TrackingStatus();
			    }
			    else
			    {
			    	
			    } 
			    coordinatorList=refMasterManager.findCordinaor(serviceOrder.getJob(),"ROLE_COORD",sessionCorpID);
				if(serviceOrder.getCoordinator()!=null && !(serviceOrder.getCoordinator().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(serviceOrder.getCoordinator());
					if(!aliasNameList.isEmpty()){
						String aliasName=aliasNameList.get(0).toString();
						String coordCode=serviceOrder.getCoordinator().toUpperCase();
						String alias = aliasName.toUpperCase();
						if(!coordinatorList.containsValue(coordCode)){
							coordinatorList.put(coordCode,alias );
						}
					}
				} 
				//service = refMasterManager.findServiceByJob(serviceOrder.getJob(),sessionCorpID, "SERVICE");
				estimatorList=refMasterManager.findCordinaor(serviceOrder.getJob(),"ROLE_SALE",sessionCorpID);
				if(serviceOrder.getSalesMan()!=null && !(serviceOrder.getSalesMan().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(serviceOrder.getSalesMan());
					if(!aliasNameList.isEmpty()){
						String estimatorCode=serviceOrder.getSalesMan().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!estimatorList.containsValue(estimatorCode)){
							estimatorList.put(estimatorCode,alias );
						}
					}
						
				}
			} 
			serviceOrder.setUnit1(miscellaneous.getUnit1());
			serviceOrder.setUnit2(miscellaneous.getUnit2());
			if(serviceOrder.getQuoteStatus().equals("AC") ){
				serviceOrder.setQuoteAccept("A");
				//Bug 11839 - Possibility to modify Current status to Prior status
				serviceOrder.setStatus("NEW");
				serviceOrder.setStatusDate(new Date());
				serviceOrder.setControlFlag("C");
				serviceOrder.setMoveType("BookedMove");
				serviceOrder.setStatusNumber(1);				 
			}
			if(serviceOrder.getQuoteStatus().equals("RE") ){
				serviceOrder.setQuoteAccept("R");
				serviceOrder.setStatus("CNCL");
				serviceOrder.setStatusDate(new Date());
				serviceOrder.setStatusNumber(400);
			}
			if(serviceOrder.getQuoteStatus().equals("PE") ){
				serviceOrder.setQuoteAccept("P");
				serviceOrder.setStatus("HOLD");
				serviceOrder.setStatusDate(new Date());
				serviceOrder.setStatusNumber(100);
			}
			if(serviceOrder.getQuoteStatus().equals("") ){
				serviceOrder.setQuoteAccept("P");
				serviceOrder.setStatus("HOLD");
				serviceOrder.setStatusDate(new Date());
				serviceOrder.setStatusNumber(100);
			}	
      // getComboList(sessionCorpID);
			if(serviceOrder.getBookingAgentCode() != null && (!serviceOrder.getBookingAgentCode().equals(""))){
				if(bookingAgentValid() == null){
					String key = "BookingAgentCode is incorrect";
					errorMessage(getText(key));
			        return INPUT;
				} 
			} 
			
			if(serviceOrder.getJob().trim().equalsIgnoreCase("RLO"))
			{
				String serviceDoc=reloServiceType.trim().toString();
				if(serviceDoc.equals("")||serviceDoc.equals("#"))
			  	  {
			  	  }
			  	 else
			  	  {
			  		serviceDoc=serviceDoc.trim();
			  	    if(serviceDoc.indexOf("#")==0)
			  		 {
			  	    	serviceDoc=serviceDoc.substring(1);
			  		 }
			  		if(serviceDoc.lastIndexOf("#")==serviceDoc.length()-1)
			  		 {
			  			serviceDoc=serviceDoc.substring(0, serviceDoc.length()-1);
			  		 } 
			  	  } 
				serviceDoc=serviceDoc.replaceAll("#", ","); 
				serviceOrder.setServiceType(serviceDoc);
			}          
			
			serviceOrder = savePricingFooter(serviceOrder);
			
			/*if(pricingFooterValue != null && pricingFooterValue.trim().length() > 0){
				String strfooter[] = pricingFooterValue.split("-,-");
				
				try {
					serviceOrder.setEstimatedTotalExpense((strfooter[0] == null || "".equals(strfooter[0].trim())) ? new BigDecimal(0.00) : new BigDecimal(strfooter[0].trim()));
					serviceOrder.setEstimatedTotalRevenue((strfooter[1] == null || "".equals(strfooter[1].trim())) ? new BigDecimal(0.00) : new BigDecimal(strfooter[1].trim()));
					serviceOrder.setEstimatedGrossMargin((strfooter[2] == null || "".equals(strfooter[2].trim())) ? new BigDecimal(0.00) : new BigDecimal(strfooter[2].trim()));
					serviceOrder.setEstimatedGrossMarginPercentage((strfooter[3] == null || "".equals(strfooter[3].trim())) ? new BigDecimal(0.00) : new BigDecimal(strfooter[3].trim()));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}*/
			
			if(defaultTemplate != null && "YES".equals(defaultTemplate)){
				//serviceOrder.setDefaultAccountLineStatus(true);
			}
			//serviceOrder.setServiceOrderFlagCarrier(serviceOrder.getServiceOrderFlagCarrier());
			findByCountryParameterDesc();
			if((serviceOrder.getOriginCountry()!=null && !serviceOrder.getOriginCountry().equalsIgnoreCase(""))||(serviceOrder.getOriginCountryCode()==null || serviceOrder.getOriginCountryCode().equalsIgnoreCase(""))){
				serviceOrder.setOriginCountryCode(countryDsc.get(serviceOrder.getOriginCountry()));
			 }else if((serviceOrder.getOriginCountryCode()!=null && !serviceOrder.getOriginCountryCode().equalsIgnoreCase(""))||(serviceOrder.getOriginCountry()==null || serviceOrder.getOriginCountry().equalsIgnoreCase(""))){
				 serviceOrder.setOriginCountry(countryCod.get(serviceOrder.getOriginCountryCode()));
			 }

			    if((serviceOrder.getDestinationCountry()!=null && !serviceOrder.getDestinationCountry().equalsIgnoreCase(""))||(serviceOrder.getDestinationCountryCode()==null || serviceOrder.getDestinationCountryCode().equalsIgnoreCase(""))){
			    	serviceOrder.setDestinationCountryCode(countryDsc.get(serviceOrder.getDestinationCountry()));
			    }else if((serviceOrder.getDestinationCountryCode()!=null && !serviceOrder.getDestinationCountryCode().equalsIgnoreCase(""))||(serviceOrder.getDestinationCountry()==null || serviceOrder.getDestinationCountry().equalsIgnoreCase(""))){
			    	serviceOrder.setDestinationCountry(countryCod.get(serviceOrder.getDestinationCountryCode()));
			    }
			serviceOrder=serviceOrderManager.save(serviceOrder);
			String quotationContract = serviceOrder.getContract();
			if(quotationContract!=null && (!(quotationContract.toString().equals("")))){
				String	contractTypeValue="";
				contractTypeValue=	accountLineManager.checkContractType(quotationContract,sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}
				}
			if(serviceOrder!=null){
				contracts = customerFileManager.findCustJobContract(serviceOrder.getBillToCode(),serviceOrder.getJob(),serviceOrder.getCreatedOn(),sessionCorpID,serviceOrder.getCompanyDivision(),serviceOrder.getBookingAgentCode());
			}
			  String comptetive="";
			  if(customerFileId!=null)
			  {
				  customerFile = customerFileManager.get(customerFileId);
				  comptetive=customerFile.getComptetive();
			  }
			if(serviceOrder.getQuoteStatus().equals("AC") ){
				serviceOrderManager.updateCustomerfileQuotationStatus(customerFileId,"Accepted",getRequest().getRemoteUser(),serviceOrder.getId(),comptetive,checkAccessQuotation);
			} 
			if(serviceOrder.getQuoteStatus().equals("RE") ){
				serviceOrderManager.updateCustomerfileQuotationStatus(customerFileId,"Rejected",getRequest().getRemoteUser(),serviceOrder.getId(),comptetive,checkAccessQuotation);
			}
			if(serviceOrder.getQuoteStatus().equals("PE")|| serviceOrder.getQuoteAccept().equals("")){
				serviceOrderManager.updateCustomerfileQuotationStatus(customerFileId,"Pending",getRequest().getRemoteUser(),serviceOrder.getId(),comptetive,checkAccessQuotation);
			}
			
			if(serviceOrder.getQuoteStatus().equals("AC") ){
			customerFileTemp = customerFileManager.get(customerFile.getId());
			customerServiceOrders = customerFileTemp.getServiceOrders(); 
			if (customerServiceOrders != null) {

				Iterator it = customerServiceOrders.iterator();
				while (it.hasNext()) { 
					serviceOrderTemp = (ServiceOrder) it.next();
					//serviceOrder.setControlFlag("C");
					//serviceOrder.setFirstName(customerFile.getFirstName());
					//serviceOrder.setLastName(customerFile.getLastName());
					if(serviceOrderTemp.getQuoteStatus().equals("AC") ){
						serviceOrderManager.updateQuoteStatus(serviceOrderTemp.getId(),"NEW","1","A",getRequest().getRemoteUser(),"AC");
						//serviceOrder.setStatus("NEW");	
						//serviceOrder.setStatusDate(new Date());
						//serviceOrder.setStatusNumber(1);
					}
					if(serviceOrderTemp.getQuoteStatus().equals("PE")|| serviceOrderTemp.getQuoteAccept().equals("")){
						serviceOrderManager.updateQuoteStatus(serviceOrderTemp.getId(),"HOLD","100","P",getRequest().getRemoteUser(),"PE");
						//serviceOrder.setStatus("HOLD");
			    		//serviceOrder.setStatusDate(new Date());
			    		//serviceOrder.setStatusNumber(100); 
					}
					if(serviceOrderTemp.getQuoteStatus().equals("RE")){
						serviceOrderManager.updateQuoteStatus(serviceOrderTemp.getId(),"CNCL","400","R",getRequest().getRemoteUser(),"RE");
						//serviceOrder.setStatus("CNCL");
			    		//serviceOrder.setStatusDate(new Date());
			    		//serviceOrder.setStatusNumber(400); 
					}
					}
			}
			}
     
			hitFlag = "1";
			shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		    minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		    countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();

			List tripList1 = dspDetailsManager.getListById(serviceOrder.getId());
			if(tripList1.isEmpty()){
				dspDetails = new DspDetails();
				dspDetails.setId(serviceOrder.getId());
				dspDetails.setCorpID(serviceOrder.getCorpID());
				dspDetails.setCreatedOn(new Date());
				dspDetails.setUpdatedOn(new Date());
				dspDetails.setCreatedBy(getRequest().getRemoteUser());
				dspDetails.setUpdatedBy(getRequest().getRemoteUser());
				dspDetails.setServiceOrderId(serviceOrder.getId());
				dspDetails.setShipNumber(serviceOrder.getShipNumber());
				dspDetailsManager.save(dspDetails);
			}        
  
			if(!isNew){
				 
				List quotesToValidateList=customerFileManager.getQuotesToValidate(sessionCorpID, serviceOrder.getJob(), serviceOrder.getCompanyDivision());
				if(quotesToValidateList!=null && !quotesToValidateList.isEmpty() && !quotesToValidateList.contains(null)){
				quotesToValidate=quotesToValidateList.get(0).toString();
			    }else{
					quotesToValidate="";
			   }
			  if(!(quotesToValidate.toString().equals("QTG"))){
				if(usertype.equalsIgnoreCase("USER")) 
			    {
				sid=serviceOrder.getId();
				//savePricingLineQuotesSave(sid);
			    }	 
			   }
			  
			}
			//oldStatus=serviceOrder.getStatus(); 
			JOB_STATUS = refMasterManager.findByParameterStatus("JOB_STATUS", sessionCorpID, serviceOrder.getStatusNumber().intValue());
			shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
			if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
			String key = (isNew) ? "Quote has been added successfully" : "Quote has been updated successfully";   
			saveMessage(getText(key));
			} 
			//ostates = customerFileManager.findDefaultStateList(serviceOrder.getOriginCountry(), sessionCorpID);
			//dstates = customerFileManager.findDefaultStateList(serviceOrder.getDestinationCountry(), sessionCorpID);
			///originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
			//destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());			
			//companies =  companyManager.findByCompanyDivis(sessionCorpID);
			//companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,serviceOrder.getBookingAgentCode());
   getNotesForIconChange();
			if (!isNew) {  
			    miscellaneous.setId(serviceOrder.getId());
			    miscellaneous.setShipNumber(serviceOrder.getShipNumber());
			    miscellaneous.setShip(serviceOrder.getShip());
			    miscellaneous.setSequenceNumber(serviceOrder.getSequenceNumber());
			    miscellaneous.setCorpID(serviceOrder.getCorpID());
			    miscellaneous.setCreatedOn(serviceOrder.getCreatedOn());
			    if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						if(miscellaneous.getUnit1()==null || miscellaneous.getUnit1().equalsIgnoreCase("")){
						miscellaneous.setUnit1(systemDefault.getWeightUnit());
						}
						if(miscellaneous.getUnit1()==null || miscellaneous.getUnit1().equalsIgnoreCase("")){
						miscellaneous.setUnit2(systemDefault.getVolumeUnit());
						}
					}
				} 
			    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
			            if(systemDefault.getUnitVariable() !=null && systemDefault.getUnitVariable().equalsIgnoreCase("Yes"))
			            	disableChk ="Y";
			            else
			            	disableChk ="N";
					}
				}
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
					weightType="lbscft";
				}
				else{
					weightType="kgscbm";
				}
			    miscellaneousManager.save(miscellaneous); 
			    return INPUT;   
			} else {   
				//System.out.println("Before getting Maximum Id of service Order");
				i = Long.parseLong(serviceOrderManager.findMaximumId().get(0).toString());
			    serviceOrder = serviceOrderManager.get(i); 
			    miscellaneous.setId(serviceOrder.getId());
			    miscellaneous.setShipNumber(serviceOrder.getShipNumber());
			    miscellaneous.setShip(serviceOrder.getShip());
			    miscellaneous.setSequenceNumber(serviceOrder.getSequenceNumber());
			    miscellaneous.setCorpID(serviceOrder.getCorpID());
			    miscellaneous.setCreatedOn(serviceOrder.getCreatedOn());
			    if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						if(miscellaneous.getUnit1()==null || miscellaneous.getUnit1().equalsIgnoreCase("")){
							miscellaneous.setUnit1(systemDefault.getWeightUnit());
							}
							if(miscellaneous.getUnit1()==null || miscellaneous.getUnit1().equalsIgnoreCase("")){
							miscellaneous.setUnit2(systemDefault.getVolumeUnit());
							}
					}
				} 
			    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
			            if(systemDefault.getUnitVariable() !=null && systemDefault.getUnitVariable().equalsIgnoreCase("Yes"))
			            	disableChk ="Y";
			            else
			            	disableChk ="N";
					}
				}
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
					weightType="lbscft";
				}
				else{
					weightType="kgscbm";
				}
			    miscellaneousManager.save(miscellaneous); 
			    miscellaneous = miscellaneousManager.get(i);
			    trackingStatus = new TrackingStatus();
			    trackingStatus.setId(serviceOrder.getId());
			    trackingStatus.setCorpID(serviceOrder.getCorpID());
			    trackingStatus.setShipNumber(serviceOrder.getShipNumber());
			    trackingStatus.setSitOriginDays(0);
			    trackingStatus.setSitDestinationDays(0);
			    trackingStatus.setFromWH(0);
			    trackingStatus.setDemurrageCostPerDay2(0);
			    trackingStatus.setUsdaCost(0);
			    trackingStatus.setInspectorCost(0);
			    trackingStatus.setXrayCost(0);
			    trackingStatus.setPerdiumCostPerDay(0);
			    trackingStatus.setDemurrageCostPerDay1(0);
			    trackingStatus.setStatus(serviceOrder.getCustomerFile().getStatus());
				trackingStatus.setCreatedOn(serviceOrder.getCreatedOn());
				trackingStatus.setUpdatedOn(new Date());
				trackingStatus.setCreatedBy(serviceOrder.getCreatedBy());
				trackingStatus.setUpdatedBy(getRequest().getRemoteUser()); 
				trackingStatus.setSequenceNumber(serviceOrder.getSequenceNumber());
				trackingStatus.setSurveyTimeFrom(serviceOrder.getCustomerFile().getSurveyTime());
			    trackingStatus.setSurveyTimeTo(serviceOrder.getCustomerFile().getSurveyTime2());
				trackingStatus.setStatusDate(serviceOrder.getCustomerFile().getStatusDate());
				trackingStatus.setSurveyDate(serviceOrder.getCustomerFile().getSurvey());
				trackingStatus.setSurvey(serviceOrder.getCustomerFile().getSurvey());
				trackingStatus.setInitialContact(serviceOrder.getCustomerFile().getInitialContactDate());
				trackingStatus.setSurveyTimeFrom(serviceOrder.getCustomerFile().getSurveyTime());
			    trackingStatus.setSurveyTimeTo(serviceOrder.getCustomerFile().getSurveyTime2());
			    //trackingStatus.setFlagCarrier(serviceOrder.getServiceOrderFlagCarrier());
				trackingStatus.setServiceOrder(serviceOrder);
			    trackingStatus.setMiscellaneous(miscellaneous);
			    
			    boolean contractType=trackingStatusManager.getContractType(sessionCorpID ,serviceOrder.getCustomerFile().getContract());
			    if(contractType){
			    String networkPartnerNameData=accountLineManager.getNetworkPartnerName(sessionCorpID);
			    if(!networkPartnerNameData.equals("")){
			    	String[] str1 = networkPartnerNameData.split("~");
			    	String  networkPartnerCode=""; 
			    	String  networkPartnerName="";
			    	String  networkEmail="";
			    	try{
			    		networkPartnerCode=str1[0];
			    		networkPartnerName=str1[1];
			    		if(str1[2].toString().equalsIgnoreCase("NO")){
			    			networkEmail=str1[3];
			    		}else{
			    			networkEmail=str1[2];
			    		}
			    	} 
			    	catch(Exception e){ 
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			    	} 
			    	trackingStatus.setNetworkPartnerCode(networkPartnerCode);
			    	trackingStatus.setNetworkPartnerName(networkPartnerName);
			    	trackingStatus.setNetworkEmail(networkEmail);
			    }}
			    trackingStatusManager.save(trackingStatus); 
			    	billing = new Billing();
			    	billing.setShipNumber(serviceOrder.getShipNumber());
					billing.setSequenceNumber(serviceOrder.getSequenceNumber());
					billing.setCreatedOn(serviceOrder.getCreatedOn());
			   		billing.setUpdatedOn(new Date());
			   		billing.setCreatedBy(serviceOrder.getCreatedBy());
			   		billing.setUpdatedBy(getRequest().getRemoteUser());
			   		billing.setBillingInsurancePayableCurrency(baseCurrency);
			 		billing.setCorpID(sessionCorpID);
					billing.setBillToCode(serviceOrder.getCustomerFile().getBillToCode());
					billing.setBillToName(serviceOrder.getCustomerFile().getBillToName());
					billing.setNetworkBillToCode(serviceOrder.getCustomerFile().getAccountCode());
			    	billing.setNetworkBillToName(serviceOrder.getCustomerFile().getAccountName());
					billing.setBillToAuthority(serviceOrder.getCustomerFile().getBillToAuthorization());
					billing.setBillingId(serviceOrder.getCustomerFile().getBillToCode());
					billing.setBillName(serviceOrder.getCustomerFile().getBillToName());
					billing.setBillTo1Point(serviceOrder.getCustomerFile().getBillPayMethod());
					billing.setContract(serviceOrder.getContract());
					Contract contract= contractManager.getContactListByContarct(sessionCorpID,serviceOrder.getContract());
					if(contract.getPayMethod()!=null){
						billing.setBillTo1Point(contract.getPayMethod());
					}
					try{
					if(contract.getfXRateOnActualizationDate()!=null && contract.getfXRateOnActualizationDate()){
						billing.setfXRateOnActualizationDate(contract.getfXRateOnActualizationDate());
					}else{
						billing.setfXRateOnActualizationDate(false);
					}	
					}catch(Exception e){e.printStackTrace();}
					if(contract.getBillingInstructionCode()!=null){
						billing.setBillingInstructionCodeWithDesc(contract.getBillingInstructionCode());
						String[] str2 = (contract.getBillingInstructionCode()).split(":");
						if(str2!=null && (str2.length>=2) && str2[1]!=null){
			    		String billingInstruction =str2[1];
			    		billing.setBillingInstruction(billingInstruction);
						}
					}
					
					billing.setPersonBilling(serviceOrder.getCustomerFile().getPersonBilling());
			    	billing.setPersonPayable(serviceOrder.getCustomerFile().getPersonPayable());
			    	billing.setPersonPricing(serviceOrder.getCustomerFile().getPersonPricing());
			    	billing.setAuditor(serviceOrder.getCustomerFile().getAuditor());
					billing.setBillTo2Code("");
					billing.setPrivatePartyBillingCode("");
					billing.setNoCharge(false);
					sysDefaultDetail = billingManager.findDefaultBookingAgentDetail(sessionCorpID);
					if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
						for (SystemDefault systemDefault : sysDefaultDetail) {
							billing.setCurrency(systemDefault.getBaseCurrency());
						}
					}
					List partnerEmail=serviceOrderManager.partnerBillingEmail(serviceOrder.getCustomerFile().getBillToCode());
			    	if(partnerEmail!=null && !partnerEmail.isEmpty() && partnerEmail.get(0)!=null){
			    		billing.setBillingEmail(partnerEmail.get(0).toString());
			    	}
					billing.setServiceOrder(serviceOrder);
					billing.setId(serviceOrder.getId());
			    	String creditTerm="";
			    	String mc="";
			    	try{
			    	if(serviceOrder.getBillToCode()!=null && (!(serviceOrder.getBillToCode().toString().equals("")))){	
			    	creditTerm=partnerPrivateManager.getTheCreditTerm(serviceOrder.getBillToCode(),sessionCorpID);
			    	mc = partnerPrivateManager.getBillMc(serviceOrder.getBillToCode(),sessionCorpID);
			    	}
			    	}catch(Exception e){
			    		creditTerm="";
			    		mc="";
			    	} 
			    	billing.setCreditTerms(creditTerm);
			    	if(serviceOrder.getCustomerFile().getBillToReference()!=null && !serviceOrder.getCustomerFile().getBillToReference().equals("")){
			    	billing.setBillToReference(serviceOrder.getCustomerFile().getBillToReference());
			    	}else{
			        billing.setBillToReference(mc);
			    	}
			    	if(systemDefault!=null  ){
						try{
						if(systemDefault.getReceivableVat()!=null && (!(systemDefault.getReceivableVat().toString().equals("")))){
		                if((billing.getPrimaryVatCode()==null || billing.getPrimaryVatCode().equalsIgnoreCase(""))){
						billing.setPrimaryVatCode(systemDefault.getReceivableVat());
		                }
		                billing.setSecondaryVatCode(systemDefault.getReceivableVat());
		                billing.setPrivatePartyVatCode(systemDefault.getReceivableVat());
		                billing.setStorageVatDescr(systemDefault.getReceivableVat());
		                billing.setInsuranceVatDescr(systemDefault.getReceivableVat());
		                if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
		                	billing.setStorageVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()))	;
		                	billing.setInsuranceVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()))	;
		                }
		                
						}
						if(systemDefault.getPayableVat()!=null && (!(systemDefault.getPayableVat().toString().equals("")))){
		                billing.setBookingAgentVatCode(systemDefault.getPayableVat());
		                billing.setOriginAgentVatCode(systemDefault.getPayableVat());
		                billing.setOriginSubAgentVatCode(systemDefault.getPayableVat());
		                billing.setDestinationSubAgentVatCode(systemDefault.getPayableVat());
		                billing.setDestinationAgentVatCode(systemDefault.getPayableVat());
		                billing.setVendorCodeVatCode(systemDefault.getPayableVat());
		                billing.setVendorCodeVatCode1(systemDefault.getPayableVat());
		                billing.setVendorStorageVatDescr(systemDefault.getPayableVat());
		                if(payVatPercentList.containsKey(systemDefault.getPayableVat())){
		                	billing.setVendorStorageVatPercent(new BigDecimal(payVatPercentList.get(systemDefault.getPayableVat())))	; 
				         }
		                
						}
						}catch(Exception e){
							e.printStackTrace();
						}
		                }
					billingManager.save(billing);  
					//getRequest().getSession().setAttribute("id",serviceOrder.getId());
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			    return SUCCESS;   
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		} 
    }
	
	@SkipValidation
	public String savePricingLineQuotes() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (cancel != null) {   
            return "cancel";   
        }   
      
        if (delete != null) {   
            return delete();   
        }   
        roleHitFlag="OK";
        serviceOrder.setCustomerFile(customerFile); 
        boolean isNew = (serviceOrder.getId() == null);
        if(serviceOrder.getJob().trim().equalsIgnoreCase("RLO"))
        {
    		String serviceDoc=reloServiceType.trim().toString();
    		if(serviceDoc.equals("")||serviceDoc.equals("#"))
    	  	  {
    	  	  }
    	  	 else
    	  	  {
    	  		serviceDoc=serviceDoc.trim();
    	  	    if(serviceDoc.indexOf("#")==0)
    	  		 {
    	  	    	serviceDoc=serviceDoc.substring(1);
    	  		 }
    	  		if(serviceDoc.lastIndexOf("#")==serviceDoc.length()-1)
    	  		 {
    	  			serviceDoc=serviceDoc.substring(0, serviceDoc.length()-1);
    	  		 } 
    	  	  } 
    		serviceDoc=serviceDoc.replaceAll("#", ","); 
        	serviceOrder.setServiceType(serviceDoc);
        }        
        
        checkShipNumberList = serviceOrderManager.findShipNumber(serviceOrder.getShipNumber());
        if(!(checkShipNumberList.isEmpty()) && isNew)
        {
        	autoShip = Long.parseLong(serviceOrder.getShip().toString()) + 1;
            if((autoShip.toString()).length() == 1) {
            	ship = "0"+(autoShip.toString());
            }else {
            	ship=autoShip.toString();
            }
            serviceOrder.setShip(ship);
        	serviceOrder.setShipNumber(serviceOrder.getSequenceNumber()+ship);
         }
        if(isNew){
        	serviceOrder.setCreatedOn(new Date());
        	serviceOrder.setCreatedBy(getRequest().getRemoteUser());
        	estimatorList = refMasterManager.findUser(sessionCorpID,"ROLE_SALE");
			coordinatorList = refMasterManager.findUser(sessionCorpID,"ROLE_COORD");
        } 
        serviceOrder.setUpdatedOn(new Date());
        serviceOrder.setUpdatedBy(getRequest().getRemoteUser());
        serviceOrder.setCorpID(sessionCorpID);
        
        if(serviceOrder.getOriginState() == null){
        	serviceOrder.setOriginState("");
        }
        if(serviceOrder.getDestinationState() == null){
        	serviceOrder.setDestinationState("");
        }
        if(!isNew){
    
           if(billingManager.getByShipNumber(serviceOrder.getShipNumber()).isEmpty()) 
           {
        	   billing = new Billing();
           }
           else
           {
            billing = billingManager.getByShipNumber(serviceOrder.getShipNumber()).get(0);
           } 
            if(trackingStatusManager.checkById(serviceOrder.getId()).isEmpty())
            {
            	trackingStatus=new TrackingStatus();
            } 
            coordinatorList=refMasterManager.findCordinaor(serviceOrder.getJob(),"ROLE_COORD",sessionCorpID);
			if(serviceOrder.getCoordinator()!=null && !(serviceOrder.getCoordinator().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(serviceOrder.getCoordinator());
				if(!aliasNameList.isEmpty()){
					String aliasName=aliasNameList.get(0).toString();
					String coordCode=serviceOrder.getCoordinator().toUpperCase();
					String alias = aliasName.toUpperCase();
					if(!coordinatorList.containsValue(coordCode)){
						coordinatorList.put(coordCode,alias );
					}
				}
			} 
			//service = refMasterManager.findServiceByJob(serviceOrder.getJob(),sessionCorpID, "SERVICE");
			estimatorList=refMasterManager.findCordinaor(serviceOrder.getJob(),"ROLE_SALE",sessionCorpID);
			if(serviceOrder.getSalesMan()!=null && !(serviceOrder.getSalesMan().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(serviceOrder.getSalesMan());
				if(!aliasNameList.isEmpty()){
					String estimatorCode=serviceOrder.getSalesMan().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!estimatorList.containsValue(estimatorCode)){
						estimatorList.put(estimatorCode,alias );
					}
				}
					
			}
        } 
        serviceOrder.setUnit1(miscellaneous.getUnit1());
        serviceOrder.setUnit2(miscellaneous.getUnit2());
        serviceOrder.setEstimateGrossWeight(miscellaneous.getEstimateGrossWeight());
        serviceOrder.setEstimateCubicFeet(miscellaneous.getEstimateCubicFeet());
        
        if(serviceOrder.getQuoteStatus().equals("AC") ){
        	serviceOrder.setQuoteAccept("A");
        	//Bug 11839 - Possibility to modify Current status to Prior status
        	serviceOrder.setStatus("NEW");
        	serviceOrder.setStatusNumber(1);
        }
        if(serviceOrder.getQuoteStatus().equals("RE") ){
        	serviceOrder.setQuoteAccept("R");
        	serviceOrder.setStatus("CNCL");
        	serviceOrder.setStatusNumber(400);
        }
        if(serviceOrder.getQuoteStatus().equals("PE") ){
        	serviceOrder.setQuoteAccept("P");
        	serviceOrder.setStatus("HOLD");
        	serviceOrder.setStatusNumber(100);
        }
        if(serviceOrder.getQuoteStatus().equals("") ){
        	serviceOrder.setQuoteAccept("P");
        	serviceOrder.setStatus("HOLD");
        	serviceOrder.setStatusNumber(100);
        }	
        //getComboList(sessionCorpID);
        if(serviceOrder.getBookingAgentCode() != null && (!serviceOrder.getBookingAgentCode().equals(""))){
    		if(bookingAgentValid() == null){
    			String key = "BookingAgentCode is incorrect";
    			errorMessage(getText(key));
                return INPUT;
    		}
    		
    	}
        
       
        serviceOrder=serviceOrderManager.save(serviceOrder);
		  String comptetive="";
		  if(customerFileId!=null)
		  {
			  customerFile = customerFileManager.get(customerFileId);
			  comptetive=customerFile.getComptetive();
		  }        
        if(serviceOrder.getQuoteStatus().equals("AC") ){
        	serviceOrderManager.updateCustomerfileQuotationStatus(customerFileId,"Accepted",getRequest().getRemoteUser(),serviceOrder.getId(),comptetive,checkAccessQuotation);
        } 
        if(serviceOrder.getQuoteStatus().equals("RE") ){
        	serviceOrderManager.updateCustomerfileQuotationStatus(customerFileId,"Rejected",getRequest().getRemoteUser(),serviceOrder.getId(),comptetive,checkAccessQuotation);
        }
        if(serviceOrder.getQuoteStatus().equals("PE")|| serviceOrder.getQuoteAccept().equals("")){
        	serviceOrderManager.updateCustomerfileQuotationStatus(customerFileId,"Pending",getRequest().getRemoteUser(),serviceOrder.getId(),comptetive,checkAccessQuotation);
        }
        
        if(serviceOrder.getQuoteStatus().equals("AC") ){
    	customerFileTemp = customerFileManager.get(customerFile.getId());
		customerServiceOrders = customerFileTemp.getServiceOrders(); 
		if (customerServiceOrders != null) {

			Iterator it = customerServiceOrders.iterator();
			while (it.hasNext()) { 
				serviceOrderTemp = (ServiceOrder) it.next();
				//serviceOrder.setControlFlag("C");
				//serviceOrder.setFirstName(customerFile.getFirstName());
				//serviceOrder.setLastName(customerFile.getLastName());
				if(serviceOrderTemp.getQuoteStatus().equals("AC") ){
					serviceOrderManager.updateQuoteStatus(serviceOrderTemp.getId(),"NEW","1","A",getRequest().getRemoteUser(),"AC");
					//serviceOrder.setStatus("NEW");	
					//serviceOrder.setStatusDate(new Date());
					//serviceOrder.setStatusNumber(1);
				}
				if(serviceOrderTemp.getQuoteStatus().equals("PE")|| serviceOrderTemp.getQuoteAccept().equals("")){
					serviceOrderManager.updateQuoteStatus(serviceOrderTemp.getId(),"HOLD","100","P",getRequest().getRemoteUser(),"PE");
					//serviceOrder.setStatus("HOLD");
	        		//serviceOrder.setStatusDate(new Date());
	        		//serviceOrder.setStatusNumber(100); 
				}
				if(serviceOrderTemp.getQuoteStatus().equals("RE")){
					serviceOrderManager.updateQuoteStatus(serviceOrderTemp.getId(),"CNCL","400","R",getRequest().getRemoteUser(),"RE");
					//serviceOrder.setStatus("CNCL");
	        		//serviceOrder.setStatusDate(new Date());
	        		//serviceOrder.setStatusNumber(400); 
				}
				}
		}
		}
        
    	List tripList1 = dspDetailsManager.getListById(serviceOrder.getId());
    	if(tripList1.isEmpty()){
    		dspDetails = new DspDetails();
    		dspDetails.setId(serviceOrder.getId());
    		dspDetails.setCorpID(serviceOrder.getCorpID());
    		dspDetails.setCreatedOn(new Date());
    		dspDetails.setUpdatedOn(new Date());
    		dspDetails.setCreatedBy(getRequest().getRemoteUser());
    		dspDetails.setUpdatedBy(getRequest().getRemoteUser());
    		dspDetails.setServiceOrderId(serviceOrder.getId());
    		dspDetails.setShipNumber(serviceOrder.getShipNumber());
    		dspDetailsManager.save(dspDetails);
    	}            

          JOB_STATUS = refMasterManager.findByParameterStatus("JOB_STATUS", sessionCorpID, serviceOrder.getStatusNumber().intValue());
        shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){ 
        } 
        ostates = customerFileManager.findDefaultStateList(serviceOrder.getOriginCountry(), sessionCorpID);
        dstates = customerFileManager.findDefaultStateList(serviceOrder.getDestinationCountry(), sessionCorpID);
        originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
	  	destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());			
	  	companies =  companyManager.findByCompanyDivis(sessionCorpID);
        companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,serviceOrder.getBookingAgentCode());
	    getNotesForIconChange();
        if (!isNew) {  
            miscellaneous.setId(serviceOrder.getId());
            miscellaneous.setShipNumber(serviceOrder.getShipNumber());
            miscellaneous.setShip(serviceOrder.getShip());
            miscellaneous.setSequenceNumber(serviceOrder.getSequenceNumber());
            miscellaneous.setCorpID(serviceOrder.getCorpID());
            miscellaneous.setCreatedOn(serviceOrder.getCreatedOn());
            miscellaneousManager.save(miscellaneous); 
            return INPUT;   
        } else {   
        	//System.out.println("Before getting Maximum Id of service Order");
        	i = Long.parseLong(serviceOrderManager.findMaximumId().get(0).toString());
            serviceOrder = serviceOrderManager.get(i); 
            miscellaneous.setId(serviceOrder.getId());
            miscellaneous.setShipNumber(serviceOrder.getShipNumber());
            miscellaneous.setShip(serviceOrder.getShip());
            miscellaneous.setSequenceNumber(serviceOrder.getSequenceNumber());
            miscellaneous.setCorpID(serviceOrder.getCorpID());
            miscellaneous.setCreatedOn(serviceOrder.getCreatedOn());
            miscellaneousManager.save(miscellaneous);  
            miscellaneous = miscellaneousManager.get(i);
            trackingStatus = new TrackingStatus();
            trackingStatus.setId(serviceOrder.getId());
            trackingStatus.setCorpID(serviceOrder.getCorpID());
            trackingStatus.setShipNumber(serviceOrder.getShipNumber());
            trackingStatus.setSitOriginDays(0);
            trackingStatus.setSitDestinationDays(0);
            trackingStatus.setFromWH(0);
            trackingStatus.setDemurrageCostPerDay2(0);
            trackingStatus.setUsdaCost(0);
            trackingStatus.setInspectorCost(0);
            trackingStatus.setXrayCost(0);
            trackingStatus.setPerdiumCostPerDay(0);
            trackingStatus.setDemurrageCostPerDay1(0);
            trackingStatus.setStatus(serviceOrder.getCustomerFile().getStatus());
        	trackingStatus.setCreatedOn(serviceOrder.getCreatedOn());
        	trackingStatus.setUpdatedOn(new Date());
        	trackingStatus.setCreatedBy(serviceOrder.getCreatedBy());
        	trackingStatus.setUpdatedBy(getRequest().getRemoteUser()); 
        	trackingStatus.setSequenceNumber(serviceOrder.getSequenceNumber());
        	trackingStatus.setSurveyTimeFrom(serviceOrder.getCustomerFile().getSurveyTime());
            trackingStatus.setSurveyTimeTo(serviceOrder.getCustomerFile().getSurveyTime2());
        	trackingStatus.setStatusDate(serviceOrder.getCustomerFile().getStatusDate());
        	trackingStatus.setSurvey(serviceOrder.getCustomerFile().getSurvey());
        	trackingStatus.setInitialContact(serviceOrder.getCustomerFile().getInitialContactDate());
        	trackingStatus.setSurveyTimeFrom(serviceOrder.getCustomerFile().getSurveyTime());
            trackingStatus.setSurveyTimeTo(serviceOrder.getCustomerFile().getSurveyTime2());
        	trackingStatus.setServiceOrder(serviceOrder);
            trackingStatus.setMiscellaneous(miscellaneous);
            trackingStatusManager.save(trackingStatus); 
            	billing = new Billing();
    	    	billing.setShipNumber(serviceOrder.getShipNumber());
    			billing.setSequenceNumber(serviceOrder.getSequenceNumber());
    			billing.setCreatedOn(serviceOrder.getCreatedOn());
    	   		billing.setUpdatedOn(new Date());
    	   		billing.setCreatedBy(serviceOrder.getCreatedBy());
    	   		billing.setUpdatedBy(getRequest().getRemoteUser());
    	   		billing.setBillingInsurancePayableCurrency(baseCurrency);
    	 		billing.setCorpID(sessionCorpID);
        		billing.setBillToCode(serviceOrder.getCustomerFile().getBillToCode());
        		billing.setBillToName(serviceOrder.getCustomerFile().getBillToName());
        		billing.setNetworkBillToCode(serviceOrder.getCustomerFile().getAccountCode());
		    	billing.setNetworkBillToName(serviceOrder.getCustomerFile().getAccountName());
        		billing.setBillToAuthority(serviceOrder.getCustomerFile().getBillToAuthorization());
        		billing.setBillingId(serviceOrder.getCustomerFile().getBillToCode());
        		billing.setBillName(serviceOrder.getCustomerFile().getBillToName());
        		billing.setBillTo1Point(serviceOrder.getCustomerFile().getBillPayMethod());
        		billing.setContract(serviceOrder.getContract());
        		billing.setPersonBilling(serviceOrder.getCustomerFile().getPersonBilling());
            	billing.setPersonPayable(serviceOrder.getCustomerFile().getPersonPayable());
            	billing.setPersonPricing(serviceOrder.getCustomerFile().getPersonPricing());
        		billing.setBillTo2Code("");
        		billing.setPrivatePartyBillingCode("");
        		billing.setNoCharge(false);
        		sysDefaultDetail = billingManager.findDefaultBookingAgentDetail(sessionCorpID);
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						billing.setCurrency(systemDefault.getBaseCurrency());
					}
				}
        		billing.setServiceOrder(serviceOrder); 
        		billing.setId(serviceOrder.getId());
            	String creditTerm="";
            	try{
            	if(serviceOrder.getBillToCode()!=null && (!(serviceOrder.getBillToCode().toString().equals("")))){	
            	creditTerm=partnerPrivateManager.getTheCreditTerm(serviceOrder.getBillToCode(),sessionCorpID);
            	}
            	}catch(Exception e){
            		creditTerm="";
            	} 
            	billing.setCreditTerms(creditTerm); 
        		
        		billingManager.save(billing); 
        		
        		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"End");
            return SUCCESS;   
        }  
	}

	 public String bookingAgentValid(){
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	String book=serviceOrder.getBookingAgentCode();
	    List list=	serviceOrderManager.findByBooking(book, serviceOrder.getCorpID());
	        if(! list.isEmpty()){
	        	return "valid" ;
	        }else{
	        	return null;
	        }
	    	
	    }
	    
	
	
	public AccountLine getAccountLine() {
		return accountLine;
	}

	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;
	}

	public String getButtonType() {
		return buttonType;
	}

	public void setButtonType(String buttonType) {
		this.buttonType = buttonType;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getCoordinatr() {
		return coordinatr;
	}

	public void setCoordinatr(String coordinatr) {
		this.coordinatr = coordinatr;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public List getServicePartnerss() {
		return servicePartnerss;
	}

	public List getAccountList() {
		return accountList;
	}

	public void setAccountList(List accountList) {
		this.accountList = accountList;
	}

	public ArrayList getContainerWeightList() {
		return containerWeightList;
	}

	public void setContainerWeightList(ArrayList containerWeightList) {
		this.containerWeightList = containerWeightList;
	}

	public ArrayList getContainerNetWeightList() {
		return containerNetWeightList;
	}

	public void setContainerNetWeightList(ArrayList containerNetWeightList) {
		this.containerNetWeightList = containerNetWeightList;
	}

	public ArrayList getContainerTareWeightList() {
		return containerTareWeightList;
	}

	public void setContainerTareWeightList(ArrayList containerTareWeightList) {
		this.containerTareWeightList = containerTareWeightList;
	}

	public ArrayList getContainerVolumeWeightList() {
		return containerVolumeWeightList;
	}

	public void setContainerVolumeWeightList(ArrayList containerVolumeWeightList) {
		this.containerVolumeWeightList = containerVolumeWeightList;
	}
	private  Map<String, String> coordinatorList = new LinkedHashMap<String, String>();
	private  Map<String, String> estimatorList = new LinkedHashMap<String, String>();
	private String quotesToValidate;
	private String template;
	private String dayFocus;
	private String day;
	private String dayRowFocus;
	private String categoryType;
	
	private String pricingValue;
	private String pricingFooterValue;
	private String pricingHiddenValue;
	private String delimeter1;
	private String delimeter2;
	private String delimeter3;
	
	private String addPriceLine;
	
	
	
    private Map<String,String> inclusionsList;
    private Map<String,String> exclusionsList;

    private String jobIncExc;
    private String modeIncExc;
    private String routingIncExc;
    private String languageIncExc;
    private String servIncExc;
    private String checkDefaultIncExc;
    private String servIncExcDefault;
    private Map<String,String> incexcServiceList;
    private String companyDivisionIncExc;
    private String commodityIncExc;
    private String packingModeIncExc;
    private String originCountryIncExc;
    private String destinationCountryIncExc;
    private String serviceIncExc;

    @SkipValidation
   	public  String getInclusionExlusionlist(){ 
    	try {
			String returnVal=SUCCESS;
			if((userQuoteServices!=null)&&(!userQuoteServices.equalsIgnoreCase(""))&&(userQuoteServices.equalsIgnoreCase("Separate section"))){
				return getSeprateSectionInclusionExlusionlist();
			}else if((userQuoteServices!=null)&&(!userQuoteServices.equalsIgnoreCase(""))&&(userQuoteServices.equalsIgnoreCase("Single section"))){
				return getSingleSectionInclusionExlusionlist();
			}else{
				return returnVal;
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
    }    
    @SkipValidation
   	public  String getSingleSectionInclusionExlusionlist(){
    	try {
			Map<String, String> temp;
				Map<String,String> mapList=null;
				ServiceOrder serviceOrder1=null;
				String incRestCheck=getRequest().getParameter("incExcResetCheck");
				try{
				if((incRestCheck==null)||(incRestCheck.trim().toString().equalsIgnoreCase(""))){
					incRestCheck="F";
				}
				 }catch(Exception e){
					 incRestCheck="F";
					 e.printStackTrace();}
				
				if(id==null){
					if(checkDefaultIncExc.equalsIgnoreCase("default")){
						mapList=serviceOrderManager.getSingleSectionInclusionExlusionlist(jobIncExc,modeIncExc,routingIncExc,sessionCorpID,"en",companyDivisionIncExc,commodityIncExc,packingModeIncExc,originCountryIncExc,destinationCountryIncExc,serviceIncExc,"");
					}else{
						mapList=serviceOrderManager.getSingleSectionInclusionExlusionlist(jobIncExc,modeIncExc,routingIncExc,sessionCorpID,languageIncExc,companyDivisionIncExc,commodityIncExc,packingModeIncExc,originCountryIncExc,destinationCountryIncExc,serviceIncExc,"");
					}
				}else{
					if(checkDefaultIncExc.equalsIgnoreCase("default")){
						serviceOrder1=serviceOrderManager.get(id);
						billing = billingManager.get(id);
						mapList=serviceOrderManager.getSingleSectionInclusionExlusionlist(serviceOrder1.getJob(),serviceOrder1.getMode(),serviceOrder1.getRouting(),sessionCorpID,serviceOrder1.getIncExcServiceTypeLanguage(),serviceOrder1.getCompanyDivision(),serviceOrder1.getCommodity(),serviceOrder1.getPackingMode(),serviceOrder1.getOriginCountry(),serviceOrder1.getDestinationCountry(),serviceOrder1.getServiceType(),billing.getContract());
					}else{
			    		mapList=serviceOrderManager.getSingleSectionInclusionExlusionlist(jobIncExc,modeIncExc,routingIncExc,sessionCorpID,languageIncExc,companyDivisionIncExc,commodityIncExc,packingModeIncExc,originCountryIncExc,destinationCountryIncExc,serviceIncExc,billing.getContract());	
					}
				}
				
				incexcServiceList=new LinkedHashMap<String, String>();
				incexcServiceList.putAll(mapList);
				servIncExc="";
				if((mapList!=null)&&(!mapList.isEmpty())){
					for(Map.Entry<String,String> rec:mapList.entrySet()){
						if(servIncExc.equalsIgnoreCase("")){
							servIncExc=rec.getKey();
						}else{
							servIncExc=servIncExc+"#"+rec.getKey();
						}
					}
				}
				
					if(id!=null){
			   			serviceOrder1=serviceOrderManager.get(id);
			   	    	servIncExcDefault=serviceOrder1.getIncExcServiceType();
			   			}
					   try{
						   if(incRestCheck.equalsIgnoreCase("T")){
							   servIncExcDefault="";
						   }
						   }catch(Exception e){e.printStackTrace();}
					
				languageList = refMasterManager.findByflexValue(sessionCorpID, "LANGUAGE");
				temp = new LinkedHashMap<String, String>();
				for(Map.Entry<String, String> rec:languageList.entrySet()){
					if((rec.getKey()!=null)&&(!rec.getKey().toString().trim().equalsIgnoreCase(""))){
						temp.put(rec.getKey(), rec.getValue());
					}
				}
				languageList.clear();
				languageList.putAll(temp);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		
	return SUCCESS;
   	}     
    @SkipValidation
   	public  String getSeprateSectionInclusionExlusionlist(){
    	try {
			Map<String,String> mapList=null;
			ServiceOrder serviceOrder1=null;
			String incRestCheck=getRequest().getParameter("incExcResetCheck");
			try{
			if((incRestCheck==null)||(incRestCheck.trim().toString().equalsIgnoreCase(""))){
				incRestCheck="F";
			}
			 }catch(Exception e){
				 incRestCheck="F";
				 e.printStackTrace();}
			
			if(id==null){
				if(checkDefaultIncExc.equalsIgnoreCase("default")){
					mapList=serviceOrderManager.getInclusionExlusionlist(jobIncExc,modeIncExc,routingIncExc,sessionCorpID,"en",companyDivisionIncExc,commodityIncExc,packingModeIncExc,originCountryIncExc,destinationCountryIncExc,serviceIncExc,"");
				}else{
					mapList=serviceOrderManager.getInclusionExlusionlist(jobIncExc,modeIncExc,routingIncExc,sessionCorpID,languageIncExc,companyDivisionIncExc,commodityIncExc,packingModeIncExc,originCountryIncExc,destinationCountryIncExc,serviceIncExc,"");
				}
			}else{
				if(checkDefaultIncExc.equalsIgnoreCase("default")){
					serviceOrder1=serviceOrderManager.get(id);
					billing = billingManager.get(id);
					mapList=serviceOrderManager.getInclusionExlusionlist(serviceOrder1.getJob(),serviceOrder1.getMode(),serviceOrder1.getRouting(),sessionCorpID,serviceOrder1.getIncExcServiceTypeLanguage(),serviceOrder1.getCompanyDivision(),serviceOrder1.getCommodity(),serviceOrder1.getPackingMode(),serviceOrder1.getOriginCountry(),serviceOrder1.getDestinationCountry(),serviceOrder1.getServiceType(),billing.getContract());
					}else{
					mapList=serviceOrderManager.getInclusionExlusionlist(jobIncExc,modeIncExc,routingIncExc,sessionCorpID,languageIncExc,companyDivisionIncExc,commodityIncExc,packingModeIncExc,originCountryIncExc,destinationCountryIncExc,serviceIncExc,billing.getContract());
				}
			}
			
			inclusionsList=new LinkedHashMap<String, String>();
			exclusionsList=new LinkedHashMap<String, String>();
			servIncExc="";
			if((mapList!=null)&&(!mapList.isEmpty())){
			for(Map.Entry<String,String> rec:mapList.entrySet()){
				if(rec.getValue().split("~")[1].equalsIgnoreCase("Y")){
					inclusionsList.put(rec.getKey(), rec.getValue().split("~")[0]+"~"+rec.getValue().split("~")[2]);
				}else{
					exclusionsList.put(rec.getKey(), rec.getValue().split("~")[0]+"~"+rec.getValue().split("~")[2]);
				}
				if(rec.getValue().split("~")[2].equalsIgnoreCase("Y")){
					if(servIncExc.equalsIgnoreCase("")){
						servIncExc=(rec.getValue().split("~")[1].equalsIgnoreCase("Y")?rec.getKey()+":INC":rec.getKey()+":EXC");
					}else{
						servIncExc=servIncExc+"#"+(rec.getValue().split("~")[1].equalsIgnoreCase("Y")?rec.getKey()+":INC":rec.getKey()+":EXC");
					}
				}
			}
			}
			
			servIncExcDefault=servIncExc;
			if((checkDefaultIncExc!=null)&&(checkDefaultIncExc.equalsIgnoreCase("default"))){
				if(id!=null){
				serviceOrder1=serviceOrderManager.get(id);
				servIncExcDefault=serviceOrder1.getIncExcServiceType();
				}
				}
			   try{
				   if(incRestCheck.equalsIgnoreCase("T")){
					   servIncExcDefault="";
				   }
				   }catch(Exception e){e.printStackTrace();}
			
			languageList = refMasterManager.findByflexValue(sessionCorpID, "LANGUAGE");
			Map<String, String> temp = new LinkedHashMap<String, String>();
			for(Map.Entry<String, String> rec:languageList.entrySet()){
				if((rec.getKey()!=null)&&(!rec.getKey().toString().trim().equalsIgnoreCase(""))){
					temp.put(rec.getKey(), rec.getValue());
				}
			}
			languageList.clear();
			languageList.putAll(temp);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
	return SUCCESS;
   	}	
	
	
	public Date getDateFormat(String dateTemp){
		if(dateTemp == null || "".equals(dateTemp.trim())){
			return new Date();
		}else{
			try {
				return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").parse(dateTemp);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		Date date = new Date();
				
		/*if(dateTemp.trim().length() > 19){
			dateTemp = dateTemp.substring(0, 19);
		}
			
		SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		if(dateTemp !=null && (!(dateTemp.equals("")))){
			 try{
			      date = sdfSource.parse(dateTemp);
			      String dt1=sdfDestination.format(date);
			      date = sdfDestination.parse(dt1);
		     }catch(Exception e){
		    	 e.printStackTrace();
			     try {
					date = sdfDestination.parse(dateTemp);
				} catch (ParseException e1) {
					e1.printStackTrace();
					date = new Date();
				}
		     }
		}*/
		return date;
	}
	private String systemDefaultVatCalculationNew;
	private boolean checkDescriptionFieldVisibility=false;
	@SkipValidation
 	public String savePricingAjax(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			Long startTime = System.currentTimeMillis();
			String user = getRequest().getRemoteUser();
			
			costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
			getPricingHelper();
			List modeValue=serviceOrderManager.findPriceCode(serviceOrder.getMode(), "MODE",sessionCorpID);
			String quotationContract = serviceOrder.getContract();
			if(quotationContract != null && !"".equals(quotationContract)){
				String contractTypeValue = accountLineManager.checkContractType(quotationContract,sessionCorpID);
				if(contractTypeValue != null && (!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}
			}
			if(accountLineStatus == null || accountLineStatus.equals("")){
				accountLineStatus="true";
			}
			try{
				String permKey = sessionCorpID +"-"+"component.tab.accountline.blankDescription";
				checkDescriptionFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
				}catch(Exception e){
					e.printStackTrace();
				}
			BigDecimal totalExp = new BigDecimal(0);
			Map<String, String> vendorCodeVatMap=accountLineManager.getVendorVatCodeMap(sessionCorpID, serviceOrder.getShipNumber());
			Map<String, String> payVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "PAYVATDESC");
			
			if(pricingValue != null && pricingValue.trim().length() > 0){
				String strAcc[] = pricingValue.split(delimeter1);
				for (String str : strAcc) {
					String str1[] = str.split(delimeter2);
					if(str1 != null && str1.length > 0){
						if(str1[0] != null && str1[0].trim().length() > 0){
							Long id = Long.valueOf(str1[0].trim());
							String str2[] = str1[1].split(delimeter3);
							
							String currencyValue=""  ;
							String contactCurrency = "";
							String chargeStr = "";
							String vatCodePersent="";
							Boolean bb=false;
							String tempActgCode="";
							String chargeDescription="";
							String costElement="";
							String costElementDescription="";
							if(str2.length>1 && quotationContract != null && !"".equals(quotationContract.trim())){
								  List<Charges> chargeList =chargesManager.findChargesList(str2[1], quotationContract, sessionCorpID);
								  for (Charges charge : chargeList) {
									  bb=charge.getVATExclude();
									  chargeDescription=charge.getDescription();
									  costElement=charge.getCostElement();
									  costElementDescription=charge.getScostElementDescription();
								  }
							  }
							try{
								  if(str2.length>2 && str2[2]!=null && (!(str2[2].toString().trim().equals("")))){
									    if(bb!=null && (!(bb))){ 
											String vatCode=vendorCodeVatMap.get(str2[2]);			
											if((vatCode!=null)&&(!vatCode.equalsIgnoreCase(""))){
												vatCodePersent=vatCode+"~"+payVatPercentList.get(vatCode);
											}
										}
								  }
							  }catch(Exception e){e.printStackTrace();}
							  if(str2.length>1){
								  if(!costElementFlag){
										quotationDiscriptionList=	accountLineManager.findQuotationDiscription(serviceOrder.getContract(),str2[1],sessionCorpID);
								  }else{
										quotationDiscriptionList = accountLineManager.findChargeDetailFromSO(serviceOrder.getContract(),str2[1],sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
								  }
							  }
							  if(quotationDiscriptionList!=null && !quotationDiscriptionList.isEmpty() && quotationDiscriptionList.get(0)!=null && !quotationDiscriptionList.get(0).toString().equalsIgnoreCase("NoDiscription")){
								  chargeStr= quotationDiscriptionList.get(0).toString();
							  }
							  List l1ExRate=null;
							  List l2ExRate=null;
							  
							  if(chargeStr.split("#").length>3){
								  l1ExRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,chargeStr.split("#")[3]);
							  }
							  if(chargeStr.split("#").length>4){
							  	  l2ExRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,chargeStr.split("#")[4]);
							  }
							  /* Start - 9569 - Issue in 'Ignore Invoicing' */
							  try{
							  if(accountLine.getCategory().equalsIgnoreCase("Internal Cost")){
								 if(!costElementFlag)	{    
									 chargeCodeList1=chargesManager.findInternalCostChargeCodeChargeCode(str2[1], serviceOrder.getCompanyDivision(),sessionCorpID);
								 }else{
									 chargeCodeList1=chargesManager.findInternalCostChargeCodeCostElement(str2[1], serviceOrder.getCompanyDivision(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision()); 	 
								 }
							  }else{
							     if(billing.getContract()!=null && !billing.getContract().equalsIgnoreCase("")){
								  if(!costElementFlag)	{
									  chargeCodeList1=chargesManager.findChargeCode(str2[1], serviceOrder.getContract());
								  }
								  else{
									  chargeCodeList1=chargesManager.findChargeCodeCostElement(str2[1], serviceOrder.getContract(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());  
								  }
							    }
							  }
							  }catch(Exception e){}
							  try{
								  if(str2[2]!=null && (!(str2[2].toString().trim().equals("")))){
									  String actCode="";							
										String companyDivisionAcctgCodeUnique="";
										 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
										 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
											 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
										 }
								         if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
								 			actCode=partnerManager.getAccountCrossReference(str2[2],serviceOrder.getCompanyDivision(),sessionCorpID);
								 		}else{
								 			actCode=partnerManager.getAccountCrossReferenceUnique(str2[2],sessionCorpID);
								 		}
								         tempActgCode=actCode;							    
								  }
							  }catch(Exception e){e.printStackTrace();}							  
							AccountLine accountLine = accountLineManager.get(id);
							try {
								AccountLine accTemp = new AccountLine();
								accountLine.setCategory(str2[0]);
								accTemp.setChargeCode(str2[1]);
								try{
									if(costElementFlag){
								accountLine.setAccountLineCostElement(((str2[66] == null || "".equals(str2[66].trim())) ? "" : str2[66].trim()));
								accountLine.setAccountLineScostElementDescription(((str2[67] == null || "".equals(str2[67].trim())) ? "" : str2[67].trim()));
									}
								}catch(Exception e){e.printStackTrace();}
								try{
									accountLine.setRecVatDescr(str2[69]);
									accountLine.setRecVatPercent(str2[68]);
									accountLine.setPayVatDescr(str2[71]);
									accountLine.setPayVatPercent(str2[70]);
								}catch(Exception e){e.printStackTrace();}
								try{
									accountLine.setEstExpVatAmt((str2[72] == null || "".equals(str2[72].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[72].trim()));
								}catch(Exception e){e.printStackTrace();}
								try{
									accountLine.setRollUpInInvoice((str2[73] == null || "".equals(str2[73])) ? false : Boolean.parseBoolean(str2[73]));
								}catch(Exception e){e.printStackTrace();}
								accountLine.setVendorCode(str2[2]);
								accountLine.setEstimateVendorName(str2[3]);
								
								if(str2[4] != null){
									str2[4] = str2[4].trim().replace("Percentage", "%");
								}
								accountLine.setBasis(str2[4]);
								
								accountLine.setEstimateQuantity((str2[5] == null || "".equals(str2[5].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[5].trim()));
								accountLine.setEstimateRate((str2[6] == null || "".equals(str2[6].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[6].trim()));
								accountLine.setEstimateSellRate((str2[7] == null || "".equals(str2[7].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[7].trim()));
								accountLine.setEstimateExpense((str2[8] == null || "".equals(str2[8].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[8].trim()));
								accountLine.setEstimatePassPercentage((str2[9] == null || "".equals(str2[9].trim())) ? 0 : new Integer(str2[9].trim()));
								accountLine.setEstimateRevenueAmount((str2[10] == null || "".equals(str2[10].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[10].trim()));
								accountLine.setQuoteDescription(str2[11]);
								
								accountLine.setDisplayOnQuote((str2[12] == null || "".equals(str2[12])) ? false : Boolean.parseBoolean(str2[12]));
								accountLine.setAdditionalService((str2[13] == null || "".equals(str2[13])) ? false : Boolean.parseBoolean(str2[13]));
								accountLine.setStatus((str2[14] == null || "".equals(str2[14])) ? false : Boolean.parseBoolean(str2[14]));
								
								accountLine.setDivision(str2[15]);
								accountLine.setEstVatDescr(str2[16]);
								accountLine.setEstVatPercent(str2[17]);
								accountLine.setEstVatAmt((str2[18] == null || "".equals(str2[18].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[18].trim()));
								accountLine.setDeviation(str2[19]);
								accountLine.setEstimateDeviation((str2[20] == null || "".equals(str2[20].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[20].trim()));
								accountLine.setEstimateSellDeviation((str2[21] == null || "".equals(str2[21].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[21].trim()));
								
								accountLine.setContract(serviceOrder.getContract());
								//accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
								
								try{
									if(vatCodePersent!=null && !vatCodePersent.trim().equalsIgnoreCase("") && !vatCodePersent.trim().equalsIgnoreCase("~")){
										accountLine.setPayVatDescr(vatCodePersent.split("~")[0]);
										accountLine.setPayVatPercent(vatCodePersent.split("~")[1]);											
									}
								  }catch(Exception e){e.printStackTrace();}
								 
						  String chargeCode = accTemp.getChargeCode();
						  /*if(!costElementFlag){
								quotationDiscriptionList=	accountLineManager.findQuotationDiscription(serviceOrder.getContract(),chargeCode,sessionCorpID);
						  }else{
								quotationDiscriptionList = accountLineManager.findChargeDetailFromSO(serviceOrder.getContract(),chargeCode,sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
						  }*/
						  if(quotationDiscriptionList!=null && !quotationDiscriptionList.isEmpty() && quotationDiscriptionList.get(0)!=null && !quotationDiscriptionList.get(0).toString().equalsIgnoreCase("NoDiscription")){
							  chargeStr= quotationDiscriptionList.get(0).toString();
						  }
						   if(!chargeStr.equalsIgnoreCase("")){
							  String [] chrageDetailArr = chargeStr.split("#");
							  accountLine.setRecGl(chrageDetailArr[1]);
							  accountLine.setPayGl(chrageDetailArr[2]);
							  contactCurrency = chrageDetailArr[3];
							  currencyValue = chrageDetailArr[4];
							 
						  }else{
							  accountLine.setRecGl("");
							  accountLine.setPayGl("");
						  }
						  if(contractType && !chargeCode.equalsIgnoreCase(accountLine.getChargeCode())){
							  String decimalValue="0.00";
							  String ExchangeRateDecimalValue="0.00";
							  
							   accountLine.setContractCurrency(contactCurrency);
							   accountLine.setPayableContractCurrency(currencyValue);
							   accountLine.setEstimatePayableContractCurrency(currencyValue); 
							   accountLine.setEstimateContractCurrency(contactCurrency); 
							   accountLine.setRevisionPayableContractCurrency(currencyValue); 
							   accountLine.setRevisionContractCurrency(contactCurrency); 
							//   List l1ExRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,contactCurrency);
							   if(l1ExRate!=null && !l1ExRate.isEmpty() && l1ExRate.get(0)!=null && !l1ExRate.get(0).toString().equals("") ){
								   decimalValue = l1ExRate.get(0).toString();
							   }
							 //  List l2ExRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,currencyValue);
							   if(l2ExRate!=null && !l2ExRate.isEmpty() && l2ExRate.get(0)!=null && !l2ExRate.get(0).toString().equals("") ){
								   ExchangeRateDecimalValue = l2ExRate.get(0).toString();
							   }
							   if(decimalValue!=null && (!(decimalValue.equals("")))){
									try{
										accountLine.setContractExchangeRate(new BigDecimal(decimalValue));
										accountLine.setPayableContractExchangeRate(new BigDecimal(ExchangeRateDecimalValue));
										accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(ExchangeRateDecimalValue)); 
										accountLine.setEstimateContractExchangeRate(new BigDecimal(decimalValue)); 
										accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(ExchangeRateDecimalValue)); 
										accountLine.setRevisionContractExchangeRate(new BigDecimal(decimalValue)); 
								    }catch(Exception e){
										e.printStackTrace();
									}
							   }else{
									accountLine.setContractExchangeRate(new BigDecimal(0.00)); 
									accountLine.setPayableContractExchangeRate(new BigDecimal(0.00));
									accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(0.00)); 
									accountLine.setEstimateContractExchangeRate(new BigDecimal(0.00)); 
									accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(0.00)); 
									accountLine.setRevisionContractExchangeRate(new BigDecimal(0.00)); 
							   }
							   accountLine.setContractValueDate(new Date());
							   accountLine.setPayableContractValueDate(new Date());
							   accountLine.setEstimatePayableContractValueDate(new Date());
							   accountLine.setEstimateContractValueDate(new Date());
							   accountLine.setRevisionPayableContractValueDate(new Date());
							   accountLine.setRevisionContractValueDate(new Date());
						  }
						  
						  accountLine.setEstCurrency(str2[22]);
							accountLine.setEstSellCurrency(str2[23]);
							
							accountLine.setEstValueDate(getDateFormat(str2[24])); //estValueDateNew
							
							accountLine.setEstSellValueDate(getDateFormat(str2[25])); //estSellValueDateNew
							
							accountLine.setEstExchangeRate((str2[26] == null || "".equals(str2[26].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[26].trim())); //estExchangeRateNew
							accountLine.setEstSellExchangeRate((str2[27] == null || "".equals(str2[27].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[27].trim())); //estSellExchangeRateNew
							
							accountLine.setEstLocalRate((str2[28] == null || "".equals(str2[28].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[28].trim())); //estLocalRateNew
							accountLine.setEstSellLocalRate((str2[29] == null || "".equals(str2[29].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[29].trim())); //estSellLocalRateNew
							accountLine.setEstLocalAmount((str2[30] == null || "".equals(str2[30].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[30].trim())); //estLocalAmountNew
							accountLine.setEstSellLocalAmount((str2[31] == null || "".equals(str2[31].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[31].trim())); //estSellLocalAmountNew

							accountLine.setRevisionCurrency(str2[32]); //revisionCurrencyNew
							accountLine.setCountry(str2[33]); //countryNew
							
							accountLine.setRevisionValueDate(getDateFormat(str2[34]));
							accountLine.setValueDate(getDateFormat(str2[35]));
							accountLine.setRevisionExchangeRate((str2[36] == null || "".equals(str2[36].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[36].trim())); //revisionExchangeRateNew
							accountLine.setExchangeRate((str2[37] == null || "".equals(str2[37].trim())) ? new BigDecimal(0.00).doubleValue() : new BigDecimal(str2[37].trim()).doubleValue()); //exchangeRateNew
						
							
						  if(contractType){
							  try{
								  accountLine.setEstimatePayableContractCurrency(str2[38]);  //estimatePayableContractCurrencyNew
								  accountLine.setEstimateContractCurrency(str2[39]);  //estimateContractCurrencyNew
								  
								  accountLine.setEstimatePayableContractValueDate(getDateFormat(str2[40]));  //estimatePayableContractValueDateNew
								  accountLine.setEstimateContractValueDate(getDateFormat(str2[41]));  //estimateContractValueDateNew
								  
								  accountLine.setEstimatePayableContractExchangeRate(((str2[42] == null || "".equals(str2[42].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[42].trim()))); //estimatePayableContractExchangeRateNew
								  accountLine.setEstimateContractExchangeRate(((str2[43] == null || "".equals(str2[43].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[43].trim()))); //estimateContractExchangeRateNew
								  accountLine.setEstimatePayableContractRate(((str2[44] == null || "".equals(str2[44].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[44].trim()))); //estimatePayableContractRateNew
								  accountLine.setEstimateContractRate(((str2[45] == null || "".equals(str2[45].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[45].trim()))); // estimateContractRateNew
								  accountLine.setEstimatePayableContractRateAmmount(((str2[46] == null || "".equals(str2[46].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[46].trim()))); // estimatePayableContractRateAmmountNew
								  accountLine.setEstimateContractRateAmmount(((str2[47] == null || "".equals(str2[47].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[47].trim()))); // estimateContractRateAmmountNew
								  accountLine.setRevisionContractCurrency(str2[48]); //revisionContractCurrencyNew
								  accountLine.setRevisionPayableContractCurrency(str2[49]); //revisionPayableContractCurrencyNew
								  accountLine.setContractCurrency(str2[50]);//contractCurrencyNew
								  accountLine.setPayableContractCurrency(str2[51]);//payableContractCurrencyNew
								  
								  accountLine.setRevisionContractValueDate(getDateFormat(str2[52]));  //revisionContractValueDateNew
								  accountLine.setRevisionPayableContractValueDate(getDateFormat(str2[53])); ////revisionPayableContractValueDateNew
								  accountLine.setContractValueDate(getDateFormat(str2[54])); // contractValueDateNew
								  accountLine.setPayableContractValueDate(getDateFormat(str2[55])); // payableContractValueDateNew
								  
								  accountLine.setRevisionContractExchangeRate(((str2[56] == null || "".equals(str2[56].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[56].trim()))); //revisionContractExchangeRateNew
								  accountLine.setRevisionPayableContractExchangeRate(((str2[57] == null || "".equals(str2[57].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[57].trim()))); //revisionPayableContractExchangeRateNew
								  accountLine.setContractExchangeRate(((str2[58] == null || "".equals(str2[58].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[58].trim()))); //contractExchangeRateNew
								  accountLine.setPayableContractExchangeRate(((str2[59] == null || "".equals(str2[59].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[59].trim()))); // payableContractExchangeRateNew
								  
							  }catch(Exception e){
								  e.printStackTrace();
							  }
						  }
						  
						  accountLine.setChargeCode(chargeCode);
						 /* if(serviceOrder.getContract() != null && !"".equals(serviceOrder.getContract().trim())){
							  List<Charges> chargeList =chargesManager.findChargesList(accountLine.getChargeCode(), serviceOrder.getContract(), sessionCorpID);
							  for (Charges charge : chargeList) {
								  accountLine.setVATExclude(charge.getVATExclude());
							  }
						  }*/
						  accountLine.setVATExclude(bb);
						  if(accountLine.getVATExclude() !=null && accountLine.getVATExclude()){
							   accountLine.setRecVatDescr("");
							   accountLine.setRecVatPercent("");
							   accountLine.setRecVatAmt(new BigDecimal(0));
						  }
						  if(str2.length > 60 && str2[60].trim().length() > 0 ){
							  accountLine.setAccountLineNumber(str2[60]);
						  }
						  accountLine.setEstimateSellQuantity((str2[65] == null || "".equals(str2[65].trim())) ? new BigDecimal(0.00) : new BigDecimal(str2[65].trim()));
						  String value=str2[11];
						  if(setDescriptionChargeCode!=null &&(!setDescriptionChargeCode.equalsIgnoreCase(""))){
							  accountLine.setQuoteDescription(value); 
							  accountLine.setDescription(value);
							  accountLine.setNote(value);
						  }else{
							  accountLine.setQuoteDescription(value); 
						  } 
						  if(setDefaultDescriptionChargeCode!=null && (!setDefaultDescriptionChargeCode.equalsIgnoreCase(""))){
						  accountLine.setQuoteDescription(value); 
						  if(accountLine.getDescription() ==null || accountLine.getDescription().trim().equals("")){
						  accountLine.setDescription(value);
						  }
						  if(accountLine.getNote()==null || accountLine.getNote().trim().equals("") ){
						  accountLine.setNote(value);
						  }
						  }else{
							  accountLine.setQuoteDescription(value); 
						  }
						  
						  
						  if(accountLine.getChargeCode()!=null && (!accountLine.getChargeCode().equalsIgnoreCase("")) && chargeDescription!=null && (!chargeDescription.equalsIgnoreCase(""))){
							  if(!checkDescriptionFieldVisibility){
							  if(accountLine.getDescription() ==null || accountLine.getDescription().trim().equalsIgnoreCase("")){
								  accountLine.setDescription(chargeDescription);
							  }
							  if(accountLine.getQuoteDescription()==null || accountLine.getQuoteDescription().trim().equalsIgnoreCase("")){
								  accountLine.setQuoteDescription(chargeDescription);
							  }
							  }
							  if(accountLine.getNote()==null || accountLine.getNote().trim().equalsIgnoreCase("")){
								  accountLine.setNote(chargeDescription);
							  }
							  
							  if(accountLine.getAccountLineCostElement()==null || (accountLine.getAccountLineCostElement().equals(""))){
									 if(costElement!=null && (!costElement.equals(""))){
									 accountLine.setAccountLineCostElement(costElement);
									 }
								  }
								 if(accountLine.getAccountLineScostElementDescription()==null || (accountLine.getAccountLineScostElementDescription().equals(""))){
									 if(costElementDescription!=null && (!costElementDescription.equals(""))){
									 accountLine.setAccountLineScostElementDescription(costElementDescription);
									 }
								  }
							  
						  }
						  /* Start - 9569 - Issue in 'Ignore Invoicing' */
						  /*if(accountLine.getCategory().equalsIgnoreCase("Internal Cost")){
							 if(!costElementFlag)	{    
								 chargeCodeList1=chargesManager.findInternalCostChargeCodeChargeCode(chargeCode, serviceOrder.getCompanyDivision(),sessionCorpID);
							 }else{
								 chargeCodeList1=chargesManager.findInternalCostChargeCodeCostElement(chargeCode, serviceOrder.getCompanyDivision(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision()); 	 
							 }
						  }else{
						     if(billing.getContract()!=null && !billing.getContract().equalsIgnoreCase("")){
							  if(!costElementFlag)	{
								  chargeCodeList1=chargesManager.findChargeCode(chargeCode, serviceOrder.getContract());
							  }
							  else{
								  chargeCodeList1=chargesManager.findChargeCodeCostElement(chargeCode, serviceOrder.getContract(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());  
							  }
						    }
						  }*/
					  if(chargeCodeList1!=null && !chargeCodeList1.isEmpty() && chargeCodeList1.get(0)!=null && !chargeCodeList1.get(0).toString().equalsIgnoreCase("")){
							  try{
										String[] printOnInvoiceArr=chargeCodeList1.get(0).toString().split("#");
										if(printOnInvoiceArr.length>6){
										String printOnInvoice=printOnInvoiceArr[6];
										if((printOnInvoice.equalsIgnoreCase("Y"))){
											printOnInvoice="true";
											ignoreForBilling=Boolean.parseBoolean(printOnInvoice);
											accountLine.setIgnoreForBilling(ignoreForBilling);
										}else{
											printOnInvoice="false";
											ignoreForBilling=Boolean.parseBoolean(printOnInvoice);
											accountLine.setIgnoreForBilling(ignoreForBilling);
										}
										}
									}catch(Exception e){
										e.printStackTrace();
						  }	
					  }
					  /* --------- End  -------- */
						  /*try{
							  if(accountLine.getVendorCode()!=null && (!(accountLine.getVendorCode().toString().trim().equals("")))){
								  String actCode="";							
									String companyDivisionAcctgCodeUnique="";
									 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
									 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
										 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
									 }
							         if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
							 			actCode=partnerManager.getAccountCrossReference(accountLine.getVendorCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
							 		}else{
							 			actCode=partnerManager.getAccountCrossReferenceUnique(accountLine.getVendorCode(),sessionCorpID);
							 		}
							         accountLine.setActgCode(actCode);						    
							  }
						  }catch(Exception e){e.printStackTrace();}*/
					  	  accountLine.setActgCode(tempActgCode);
						  accountLine.setCompanyDivision(str2[61]);					  
						  try {
							if(str2[0]!=null && (str2[0].toString().equalsIgnoreCase("Origin") || str2[0].toString().equalsIgnoreCase("Destin")) && str2[62]!=null && !str2[62].toString().equals("") && str2[63]!=null && !str2[63].toString().equals("") && str2[64]!=null && !str2[64].toString().equals("")){
								if(accountLine.getExternalIntegrationReference()==null || accountLine.getExternalIntegrationReference().equals("")){
									String pricePintId=saveRequestedQuote(str2[62],str2[63],str2[64],str2[0],serviceOrder.getId(),accountLine.getId(),serviceOrder,miscellaneous,modeValue,company);  
									if(pricePintId!=null && !pricePintId.equals("")){
										accountLine.setReference(pricePintId);
										accountLine.setEstimateDate(new Date());
										accountLine.setExternalIntegrationReference("PP");
									}
								}else if(accountLine.getExternalIntegrationReference().equalsIgnoreCase("PP")){
									pricPointUpdateDetails(serviceOrder.getId(),accountLine.getReference(),serviceOrder,miscellaneous,sysDefaultDetail,modeValue);
									//String pricePintId=saveRequestedQuote(str2[62],str2[63],str2[64],str2[0],serviceOrder.getId(),accountLine.getId());  
								}
							
							  }
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						  accountLine.setUpdatedOn(new Date());
					      accountLine.setUpdatedBy(getRequest().getRemoteUser());
					      if(accountLine.getVATExclude() !=null && accountLine.getVATExclude()){
					           accountLine.setRecVatDescr("");
					           accountLine.setRecVatPercent("");
					           accountLine.setRecVatAmt(new BigDecimal(0));
					           accountLine.setPayVatDescr("");
					           accountLine.setPayVatPercent("");
					           accountLine.setPayVatAmt(new BigDecimal(0));
					           accountLine.setEstVatAmt(new BigDecimal(0));
					           accountLine.setEstExpVatAmt(new BigDecimal(0));
					           accountLine.setRevisionVatAmt(new BigDecimal(0));
					           accountLine.setRevisionExpVatAmt(new BigDecimal(0));
					         }						      
						  accountLineManager.save(accountLine);
							} catch (NumberFormatException e) {
								e.printStackTrace();
							}catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
				updateAccountLineAjax(serviceOrder);
				//serviceOrderManager.save(serviceOrder);
				
				/*if(pricingFooterValue != null && pricingFooterValue.trim().length() > 0){
					try {
						String strfooter[] = pricingFooterValue.split(delimeter3);
						
						if(strfooter != null && strfooter.length > 0)
						serviceOrder.setEstimatedTotalExpense((strfooter[0] == null || "".equals(strfooter[0].trim())) ? new BigDecimal(0.00) : new BigDecimal(strfooter[0].trim()));
						if(strfooter != null && strfooter.length > 1)
						serviceOrder.setEstimatedTotalRevenue((strfooter[1] == null || "".equals(strfooter[1].trim())) ? new BigDecimal(0.00) : new BigDecimal(strfooter[1].trim()));
						if(strfooter != null && strfooter.length > 2)
						serviceOrder.setEstimatedGrossMargin((strfooter[2] == null || "".equals(strfooter[2].trim())) ? new BigDecimal(0.00) : new BigDecimal(strfooter[2].trim()));
						if(strfooter != null && strfooter.length > 3)
							serviceOrder.setEstimatedGrossMarginPercentage((strfooter[3] == null || "".equals(strfooter[3].trim())) ? new BigDecimal(0.00) : new BigDecimal(strfooter[3].trim()));
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					serviceOrderManager.save(serviceOrder);
				}*/
			}
			
			if(addPriceLine != null && "AddLine".equals(addPriceLine.trim())){
				addPricingLineAjax();
			}else if(addPriceLine != null && "AddTemplate".equals(addPriceLine.trim())){
				addPricingDefaultLineAjax();
			}else if(inActiveLine != null &&  "Inactive".equals(addPriceLine.trim()) && inActiveLine.trim().length() > 0){
				inActivePricingLineAjax();
			}
			
			Long timeTaken = System.currentTimeMillis()-startTime;
			logger.warn("\n\nTime taken to save Pricing Line Ajax  "+timeTaken+"\n\n");
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
 		return SUCCESS;
	}
	
	public ServiceOrder savePricingFooter(ServiceOrder serviceOrder){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		System.out.println("pricing footer start........");
		if(pricingFooterValue != null && pricingFooterValue.trim().length() > 0){
			try {
				String strfooter[] = pricingFooterValue.split("-,-"); //   delimeter3
				
				if(strfooter != null && strfooter.length > 0)
					serviceOrder.setEstimatedTotalExpense((strfooter[0] == null || "".equals(strfooter[0].trim())) ? new BigDecimal(0.00) : new BigDecimal(strfooter[0].trim()));
				if(strfooter != null && strfooter.length > 1)
					serviceOrder.setEstimatedTotalRevenue((strfooter[1] == null || "".equals(strfooter[1].trim())) ? new BigDecimal(0.00) : new BigDecimal(strfooter[1].trim()));
				if(strfooter != null && strfooter.length > 2)
					serviceOrder.setEstimatedGrossMargin((strfooter[2] == null || "".equals(strfooter[2].trim())) ? new BigDecimal(0.00) : new BigDecimal(strfooter[2].trim()));
				if(strfooter != null && strfooter.length > 3)
					serviceOrder.setEstimatedGrossMarginPercentage((strfooter[3] == null || "".equals(strfooter[3].trim())) ? new BigDecimal(0.00) : new BigDecimal(strfooter[3].trim()));
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	e.printStackTrace();
			}
			//serviceOrderManager.save(serviceOrder);
		}
		/*# 7784 - Always show actual gross margin in accountline overview Start*/
		try{
		serviceOrder.setProjectedGrossMargin(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"EXP")));
		if(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").doubleValue()!=0){
			serviceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((serviceOrder.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV")).doubleValue())*100));
		}else{
			serviceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
		}
		}catch(Exception e){
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		/*# 7784 - Always show actual gross margin in accountline overview End*/
		System.out.println("EstimatedTotalExpense "+serviceOrder.getEstimatedTotalExpense());
		System.out.println("EstimatedTotalRevenue "+serviceOrder.getEstimatedTotalRevenue());
		System.out.println("EstimatedGrossMargin "+serviceOrder.getEstimatedGrossMargin());
		System.out.println("EstimatedGrossMarginPercentage "+serviceOrder.getEstimatedGrossMarginPercentage());
		System.out.println("pricing footer end........");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return serviceOrder;
	}
	
	@SkipValidation
 	public String findAllPricingLineAjax(){ 
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			returnAjaxStringValue ="";
			if(accountLineStatus == null || "".equals(accountLineStatus)){
				accountLineStatus="true";
			}
			List<AccountLine> list = accountLineManager.getAccountLineList(shipNumber,accountLineStatus);
			for (AccountLine accountLine : list) {
				if (returnAjaxStringValue.length()==0) {
					returnAjaxStringValue = accountLine.getId().toString();
				}else{
					returnAjaxStringValue = returnAjaxStringValue+","+accountLine.getId().toString();
				}
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		return SUCCESS;
	}
	
	@SkipValidation
 	public String findAllPricingAjax(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			Long startTime = System.currentTimeMillis();
			if(pricePointUserStartDate!=null){
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			if(company!=null){		   
			   pricePointFlag=company.getEnablePricePoint();
			
			if(company.getPriceEndDate()!=null && company.getPriceEndDate().compareTo(new Date())>0){			
					 pricePointexpired=true;	 
				
			}
			}
			}
			getPricingHelper();
			
			category = refMasterManager.findByParameter(sessionCorpID, "ACC_CATEGORY");
			category.put("", "");
			
			quotationContract=serviceOrder.getContract();
			if(quotationContract!=null && (!(quotationContract.toString().equals("")))){
				String	contractTypeValue = accountLineManager.checkContractType(quotationContract,sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;
				}
			}
			
			basis = refMasterManager.findByParameter(sessionCorpID, "ACC_BASIS");
			basis.put("", "");
			companyDivis.add("");
			companyDivis.addAll(customerFileManager.findCompanyDivisionByBookAg(serviceOrder.getCorpID(),""));

			
			if(accountLineStatus == null || "".equals(accountLineStatus)){
				accountLineStatus="true";
			}
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			aidListIds="";
			Iterator<AccountLine> idIterator =  accountLineList.iterator(); 
			while (idIterator.hasNext()) 
			     {
				AccountLine accountLineOld = (AccountLine) idIterator.next(); 
				if(!aidListIds.equalsIgnoreCase("")){
					aidListIds=aidListIds+","+accountLineOld.getId()+"";
				}else{
					aidListIds=accountLineOld.getId()+"";
				}
			     }

	/*	if(pricePointexpired){
				List<AccountLine> aa=accountLineList;
				for(AccountLine accLine:aa){
				try {
					if(accLine.getPricePointTariffId()!=null && !accLine.getPricePointTariffId().equals("")){
						BigDecimal soWeight=new BigDecimal("0.0");
						BigDecimal soVolume=new BigDecimal("0.0");
						miscellaneous = miscellaneousManager.get(serviceOrder.getId());
						if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
			    			weightType="lbscft";
			    		}
			    		else{
			    			weightType="kgscbm";
			    		} 					
						if(weightType.equalsIgnoreCase("lbscft") && miscellaneous.getEstimatedNetWeight()!=null && !miscellaneous.getEstimatedNetWeight().toString().equals("") && miscellaneous.getNetEstimateCubicFeet()!=null && !miscellaneous.getNetEstimateCubicFeet().toString().equals("") ){
							 soWeight= miscellaneous.getEstimatedNetWeight();
							 soVolume= miscellaneous.getNetEstimateCubicFeet();
							}
					   if(weightType.equalsIgnoreCase("kgscbm") && miscellaneous.getEstimatedNetWeightKilo()!=null && !miscellaneous.getEstimatedNetWeightKilo().toString().equals("") && miscellaneous.getNetEstimateCubicMtr()!=null && !miscellaneous.getNetEstimateCubicMtr().toString().equals("")){
								 soWeight= miscellaneous.getEstimatedNetWeightKilo();
								 soVolume= miscellaneous.getNetEstimateCubicMtr();
						   }
					   String transferee="";
						if(serviceOrder.getFirstName()!=null && !serviceOrder.getFirstName().equals("") && serviceOrder.getLastName()!=null && !serviceOrder.getLastName().equals("") ){
							transferee=serviceOrder.getFirstName()+" "+serviceOrder.getLastName();
						}else{
							transferee=serviceOrder.getFirstName();
						}
						String readDetais=pricepointReadDetails(soWeight,soVolume,transferee,serviceOrder.getStatus(),accLine.getPricePointTariffId().toString());	
						if(readDetais!=null && !readDetais.equals("")){
							pricePointUpdateList.put(accLine.getId().toString(), readDetais);
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					
				}	
			}*/		
			Long timeTaken = System.currentTimeMillis()-startTime;
			logger.warn("\n\nTime taken to find All Pricing by serviceOrderId: "+timeTaken+"\n\n");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
 		return SUCCESS;
 	}
	
	 private Map<String, String> pricePointUpdateList= new HashMap<String, String>();
	@SkipValidation
	public String editQuotationServiceOrderUpdate() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			getSession().removeAttribute("accIdList");
			roleHitFlag="OK";
			//getComboList(sessionCorpID);
			if (id != null) {
				serviceOrder = serviceOrderManager.get(id);
				customerFile = serviceOrder.getCustomerFile();
				contracts = customerFileManager.findCustJobContract(customerFile.getBillToCode(),serviceOrder.getJob(),customerFile.getCreatedOn(),sessionCorpID,serviceOrder.getCompanyDivision(),serviceOrder.getBookingAgentCode());
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				companies =  companyManager.findByCompanyDivis(sessionCorpID);
				companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,serviceOrder.getBookingAgentCode());
				ostates = customerFileManager.findDefaultStateList(customerFile.getOriginCountry(), sessionCorpID);
			    dstates = customerFileManager.findDefaultStateList(customerFile.getDestinationCountry(), sessionCorpID);
			    originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
			  	destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());			
			  	salesCommisionRate=serviceOrderManager.findSalesCommisionRate(sessionCorpID).get(0).toString();
				grossMarginThreshold=serviceOrderManager.findGrossMarginThreshold(sessionCorpID).get(0).toString();
				accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString();
				   
				if (serviceOrder.getMode() !=null && (serviceOrder.getMode().equalsIgnoreCase("Air") ||serviceOrder.getMode().equalsIgnoreCase("Sea"))){
			   	   usaFlag=serviceOrderManager.getUsaFlagCarrier(serviceOrder.getBillToCode(),sessionCorpID);
			   	if(usaFlag && (serviceOrder.getServiceOrderFlagCarrier()==null || serviceOrder.getServiceOrderFlagCarrier().equals(""))){
			   		   serviceOrder.setServiceOrderFlagCarrier("USA");
			   	   }
			      } 
				if ((miscellaneousManager.checkById(id)).isEmpty()) {
					miscellaneous = new Miscellaneous();

				} else {
					miscellaneous = miscellaneousManager.get(id);
				} 
			   	if(serviceOrder.getStatus().equalsIgnoreCase("")){
			   	//Bug 11839 - Possibility to modify Current status to Prior status
			   		serviceOrder.setStatus("NEW");
			   		serviceOrder.setStatusNumber(1);
			   	}
			   	else{
			   		//serviceOrder.setStatus(serviceOrder.getStatus());
			   	}
				serviceOrder.setCorpID(sessionCorpID);
				serviceOrder.setLastName(customerFile.getLastName());
				serviceOrder.setOriginPreferredContactTime(customerFile.getOriginPreferredContactTime());
				serviceOrder.setDestPreferredContactTime(customerFile.getDestPreferredContactTime());
				sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						corpIdBaseCurrency=systemDefault.getBaseCurrency();
			            if(systemDefault.getUnitVariable() !=null && systemDefault.getUnitVariable().equalsIgnoreCase("Yes"))
			            	disableChk ="Y";
			            else
			            	disableChk ="N";
					}
				}
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
					weightType="lbscft";
				}
				else{
					weightType="kgscbm";
				} 
				coordinatorList=refMasterManager.findCordinaor(serviceOrder.getJob(),"ROLE_COORD",sessionCorpID);
				if(serviceOrder.getCoordinator()!=null && !(serviceOrder.getCoordinator().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(serviceOrder.getCoordinator());
					if(!aliasNameList.isEmpty()){
						String aliasName=aliasNameList.get(0).toString();
						String coordCode=serviceOrder.getCoordinator().toUpperCase();
						String alias = aliasName.toUpperCase();
						if(!coordinatorList.containsValue(coordCode)){
							coordinatorList.put(coordCode,alias );
						}
					}
				}
				//service = refMasterManager.findServiceByJob(serviceOrder.getJob(),sessionCorpID, "SERVICE");
				estimatorList=refMasterManager.findCordinaor(serviceOrder.getJob(),"ROLE_SALE",sessionCorpID);
				if(serviceOrder.getSalesMan()!=null && !(serviceOrder.getSalesMan().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(serviceOrder.getSalesMan());
					if(!aliasNameList.isEmpty()){
						String estimatorCode=serviceOrder.getSalesMan().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!estimatorList.containsValue(estimatorCode)){
							estimatorList.put(estimatorCode,alias );
						}
					}
						
				}
				commodit =refMasterManager.findByParameter(sessionCorpID,"COMMODIT",serviceOrder.getJob());
				if(!serviceOrder.getJob().equalsIgnoreCase("RLO")){
				service=refMasterManager.findByParameter(serviceOrder.getCorpID(), "SERVICE",serviceOrder.getJob());
				}
/*			if(serviceOrder != null && serviceOrder.getShipNumber() != null && serviceOrder.getShipNumber().length() > 0 && "OFF".equals(serviceOrder.getJob())){
//				 resourceList = itemsJbkEquipManager.findByShipNum(serviceOrder.getShipNumber(), sessionCorpID);
					 //resourceMap = itemsJbkEquipManager.findByShipNumAndDay(serviceOrder.getShipNumber(), sessionCorpID,dayFocus);
					 resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
					 resourceMap1 = itemsJbkEquipManager.findByShipNumAndDayMap(serviceOrder.getShipNumber(), sessionCorpID,dayFocus);
					 if(day != null && dayFocus != null){
						 dayRowFocus = itemsJbkEquipManager.findByShipNumAndCategoryNull(serviceOrder.getShipNumber(), sessionCorpID,day);
					 }
					 //dayCount = resourceMap.size();
					 dayCount = resourceMap1.size();
				 }*/
				shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
			    minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
			    countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
			    String jobCheck=serviceOrder.getJob();
				if(jobCheck!=null && jobCheck.equalsIgnoreCase("GRP")){
					serviceOrder.setGrpPref(true);
				}
			    /*if(pricePointFlag){	       
			    List pricePointTariffIdDetails=serviceOrderManager.findTariffIdDetails(serviceOrder.getShipNumber(),sessionCorpID);
			    if(pricePointTariffIdDetails!=null && !pricePointTariffIdDetails.isEmpty()){
			    	BigDecimal soWeight=new BigDecimal("0.0");
					BigDecimal soVolume=new BigDecimal("0.0");				
					if(weightType.equalsIgnoreCase("lbscft") && miscellaneous.getEstimatedNetWeight()!=null && !miscellaneous.getEstimatedNetWeight().toString().equals("") && miscellaneous.getNetEstimateCubicFeet()!=null && !miscellaneous.getNetEstimateCubicFeet().toString().equals("") ){
						 soWeight= miscellaneous.getEstimatedNetWeight();
						 soVolume= miscellaneous.getNetEstimateCubicFeet();
						}
				   if(weightType.equalsIgnoreCase("kgscbm") && miscellaneous.getEstimatedNetWeightKilo()!=null && !miscellaneous.getEstimatedNetWeightKilo().toString().equals("") && miscellaneous.getNetEstimateCubicMtr()!=null && !miscellaneous.getNetEstimateCubicMtr().toString().equals("")){
							 soWeight= miscellaneous.getEstimatedNetWeightKilo();
							 soVolume= miscellaneous.getNetEstimateCubicMtr();
					   }
				   String transferee="";
					if(serviceOrder.getFirstName()!=null && !serviceOrder.getFirstName().equals("") && serviceOrder.getLastName()!=null && !serviceOrder.getLastName().equals("") ){
						transferee=serviceOrder.getFirstName()+" "+serviceOrder.getLastName();
					}else{
						transferee=serviceOrder.getFirstName();
					}
			    	Iterator tariffIdIterator=pricePointTariffIdDetails.iterator();
					  while (tariffIdIterator.hasNext()) {
					Object [] row = (Object[])tariffIdIterator.next();
					String readDetais=pricepointReadDetails(soWeight,soVolume,transferee,serviceOrder.getStatus(),row[1].toString());
					if(readDetais!=null && !readDetais.equals("")){
						pricePointUpdateList.put(row[0].toString(), readDetais);
					}
					  }
			    }
			 	
			    }*/
			} else {
				serviceOrder = new ServiceOrder();
				contracts = customerFileManager.findCustJobContract(customerFile.getBillToCode(),customerFile.getJob(),customerFile.getCreatedOn(),sessionCorpID,customerFile.getCompanyDivision(),customerFile.getBookingAgentCode());
				companies =  companyManager.findByCompanyDivis(sessionCorpID);
				companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,customerFile.getBookingAgentCode());
				ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
			    dstates = customerFileManager.findDefaultStateList("", sessionCorpID);
			    originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
			  	destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());			
			  	serviceOrder.setQuoteAccept("P"); 
				serviceOrder.setCreatedOn(new Date());
				serviceOrder.setUpdatedOn(new Date());
				serviceOrder.setCreatedBy(getRequest().getRemoteUser());
				serviceOrder.setUpdatedBy(getRequest().getRemoteUser());
				serviceOrder.setCorpID(sessionCorpID);
				estimatorList = refMasterManager.findUser(sessionCorpID,"ROLE_SALE");
				//service = refMasterManager.findServiceByJob(customerFile.getJob(),sessionCorpID, "SERVICE");
				coordinatorList = refMasterManager.findUser(sessionCorpID,"ROLE_COORD");
				serviceOrder.setIncExcServiceTypeLanguage("en");
			}
			if((checkDefaultIncExc==null)||(checkDefaultIncExc.equalsIgnoreCase(""))){
				checkDefaultIncExc="default";
			}    	
			if(id==null){
				getInclusionExlusionlist(); 
			}else{
				jobIncExc=serviceOrder.getJob();
				modeIncExc=serviceOrder.getMode();
				routingIncExc=serviceOrder.getRouting();
				languageIncExc=serviceOrder.getIncExcServiceTypeLanguage();
				companyDivisionIncExc=serviceOrder.getCompanyDivision();
			    commodityIncExc=serviceOrder.getCommodity();
			    packingModeIncExc=serviceOrder.getPackingMode();
			    originCountryIncExc=serviceOrder.getOriginCountry();
			    destinationCountryIncExc=serviceOrder.getDestinationCountry();
				getInclusionExlusionlist();
			}
			if((checkDefaultIncExc!=null)&&(checkDefaultIncExc.equalsIgnoreCase("default"))){
			servIncExcDefault=serviceOrder.getIncExcServiceType();
			}		
			getNotesForIconChange(); 
			customerFile = serviceOrder.getCustomerFile();
			quotationContract=serviceOrder.getContract();
			quotationBillToCode=customerFile.getBillToCode();
			if(quotationContract!=null && (!(quotationContract.toString().equals("")))){
				String	contractTypeValue="";
				contractTypeValue=	accountLineManager.checkContractType(quotationContract,sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}
				}
			if(accountLineStatus==null||accountLineStatus.equals(""))
			{
				accountLineStatus="true";
			}
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			valuationList=serviceOrderManager.findValuationList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			packingList=serviceOrderManager.findPackingList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List quotesToValidateList=customerFileManager.getQuotesToValidate(sessionCorpID, serviceOrder.getJob(), serviceOrder.getCompanyDivision());
			if(quotesToValidateList!=null && !quotesToValidateList.isEmpty() && !quotesToValidateList.contains(null)){
			quotesToValidate=quotesToValidateList.get(0).toString();
			}else{
				quotesToValidate="";
			}
			underLyingHaulList=serviceOrderManager.findUnderLyingHaulList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			otherMiscellaneousList=serviceOrderManager.findOtherMiscellaneousList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list1=serviceOrderManager.findPackingSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list2=serviceOrderManager.findUnpackingSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list3=serviceOrderManager.findContainerSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			if(!list1.contains(null)){
				packingSum=list1.get(0).toString();
			}else{
				packingSum="None";
			}
			if(!list3.contains(null)){
				packContSum=list3.get(0).toString();
			}else{
				packContSum="None";
			}
			if(!list2.contains(null)){
				packUnpackSum=list2.get(0).toString();
			}else{
				packUnpackSum="None";
			}
			List list4=serviceOrderManager.findValuationSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list5=serviceOrderManager.findTransportationSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list6=serviceOrderManager.findOthersSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			if(!list4.contains(null))
				{
				valuationSum=list4.get(0).toString();
				}else{
					valuationSum="None";
				}
			if(!list5.contains(null)){
				transportSum=list5.get(0).toString();
			}else{
				transportSum="None";
			}
			if(!list6.contains(null)){
				othersSum=list6.get(0).toString();
			}else{
				othersSum="None";
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return "errorlog";
		}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	/*class checkScopeComparator implements Comparator
	{

		public int compare(Object o1, Object o2) {
			OperationsIntelligence OperationsIntelligence1 = (OperationsIntelligence)o1;
			OperationsIntelligence OperationsIntelligence2 = (OperationsIntelligence)o2;
			return OperationsIntelligence1.getWorkorder().compareTo(OperationsIntelligence2.getWorkorder());
			//  int nameComp = emp1.getLastName().compareTo(emp2.getLastName());
			int checkWo =OperationsIntelligence1.getWorkorder().compareTo(OperationsIntelligence2.getWorkorder());
			if(checkWo != 0)
			{
			return checkWo;
			}
			if(OperationsIntelligence1.getTicket() != null && !(OperationsIntelligence1.getTicket().toString().isEmpty()))
					{
			int checkTicket = OperationsIntelligence1.getTicket().compareTo(OperationsIntelligence2.getTicket());
			if(checkTicket != 0  )
			{
			return checkTicket;
			}
					}
			int checkType = OperationsIntelligence1.getType().compareTo(OperationsIntelligence2.getType());
			if(checkType != 0 )
			{
			return checkType;
			}
			return OperationsIntelligence1.getDescription().compareTo(OperationsIntelligence2.getDescription());
		}
		
	}*/
	
	
	
	
	private String resouceTemplate="";
	private List scopeForWorkOrder;
	private String allWorkOrder;
	private String resourceListForOIStatus;
	private String distinctWorkOrderList;
	@SkipValidation
	public String findAllResourcesAjax(){
		try {
			
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
			if(resourceListForOIStatus==null||resourceListForOIStatus.equals("")){
				resourceListForOIStatus="true";
			} 
			if(resouceTemplate.equalsIgnoreCase("flag")){
				resourceListForOI = operationsIntelligenceManager.findAllResourcesAjax( shipNumber, sessionCorpID,allWorkOrder,resourceListForOIStatus);
			}else{
				resourceListForOI = operationsIntelligenceManager.findAllResourcesAjax( shipNumber, sessionCorpID,resouceTemplate,resourceListForOIStatus);
				String invoicedWorkTicketNumber = operationsIntelligenceManager.findInvoicedWorkTicketNumber(shipNumber);
				if(invoicedWorkTicketNumber!=null && !invoicedWorkTicketNumber.equalsIgnoreCase("")){
					Iterator<OperationsIntelligence> oprIterator =  resourceListForOI.iterator(); 
					resourceListForOI=new ArrayList();
					while (oprIterator.hasNext()){
						OperationsIntelligence newOprIntel = (OperationsIntelligence) oprIterator.next(); 
						try{
							if(newOprIntel.getTicket()!=null){	
								String ticketNumberVal = String.valueOf(newOprIntel.getTicket());
								if(invoicedWorkTicketNumber.contains(ticketNumberVal)){
									newOprIntel.setInvoiceNumber(true);
								}else{
									newOprIntel.setInvoiceNumber(false);
								}
								resourceListForOI.add(newOprIntel);
							}else{
								resourceListForOI.add(newOprIntel);
							}
						}catch(Exception e){}
					}
				}
			}
			List<ServiceOrder> serviceOrderList = serviceOrderManager.findShipNumber(shipNumber);
		     if (!(serviceOrderList == null || serviceOrderList.isEmpty())) {
				for (ServiceOrder serviceOrderTemp : serviceOrderList) {
					serviceOrder=serviceOrderTemp;
				}
		     }
			resourceCategory = refMasterManager.findByParameter(sessionCorpID, "Resource_Category");
			distinctWorkOrderList = operationsIntelligenceManager.findDistinctWorkOrder(shipNumber,sessionCorpID);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		 return SUCCESS;
	}
    private String divisionFlag;
    
    @SkipValidation
	public String addPricingLineAjax(){
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String user = getRequest().getRemoteUser();
			getPricingHelper();
			
			/*serviceOrder = serviceOrderManager.get(id);
			billing=billingManager.get(id);
			trackingStatus = trackingStatusManager.get(id);
			
			String baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
			baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID);
			
			euVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "EUVAT");
			multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString(); */
			
			
			
			accountLine = new AccountLine(); 
			accountLine.setBasis("");
			accountLine.setCategory(""); 
			accountLine.setCorpID(sessionCorpID);
			accountLine.setStatus(true);
			accountLine.setServiceOrder(serviceOrder);
			accountLine.setServiceOrderId(serviceOrder.getId());
			accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
			accountLine.setShipNumber(serviceOrder.getShipNumber());
			accountLine.setCreatedOn(new Date());
			accountLine.setCreatedBy(user);
			accountLine.setUpdatedOn(new Date());
			accountLine.setUpdatedBy(user); 
			accountLine.setDisplayOnQuote(false);
			accountLine.setAdditionalService(false);
			accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
			accountLine.setContract(billing.getContract());
			accountLine.setEstVatDescr("4");
			accountLine.setEstVatPercent(euVatPercentList.get("4"));
			accountLine.setBillToCode(billing.getBillToCode());
			accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
			accountLine.setNetworkBillToName(billing.getNetworkBillToName());
			
			boolean activateAccPortal =true;
			try{
			if(accountLineAccountPortalFlag){	
			activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
			accountLine.setActivateAccPortal(activateAccPortal);
			}
			}catch(Exception e){
				
			}
			 
			if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
				accountLine.setRecVatDescr(billing.getPrimaryVatCode());
				if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
					String recVatPercent="0";
					if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
			    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
			    		}
					accountLine.setRecVatPercent(recVatPercent);
					}
			}
			if(systemDefault!=null){
		         try{
		        if(systemDefault.getPayableVat()!=null && !systemDefault.getPayableVat().equals("")){	 
		         accountLine.setPayVatDescr(systemDefault.getPayableVat());
		         if(payVatPercentList.containsKey(systemDefault.getPayableVat())){
		        	 accountLine.setPayVatPercent(payVatPercentList.get(systemDefault.getPayableVat()))	; 
		         }
		        }
		        if(systemDefault.getReceivableVat()!=null && !systemDefault.getReceivableVat().equals("")){
		        if(accountLine.getRecVatDescr() ==null || accountLine.getRecVatDescr().toString().trim().equals("")){	
		         accountLine.setRecVatDescr(systemDefault.getReceivableVat());
		       
		         accountLine.setRevisionVatDescr(systemDefault.getReceivableVat());
		         accountLine.setEstVatDescr(systemDefault.getReceivableVat()); 
					
		         if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
		        	 accountLine.setRecVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()))	;
		         }
		        }
		         }
		         }catch(Exception e){
		        	 e.printStackTrace();	 
		         }
		         }
			
			accountLine.setBillToName(billing.getBillToName());
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			}
			if((!(trackingStatus.getSoNetworkGroup())) && billingCMMContractType && networkAgent && (!(trackingStatus.getAccNetworkGroup()))){
				String actCode="";
				accountLine.setVendorCode(serviceOrder.getBookingAgentCode());  
				accountLine.setEstimateVendorName(serviceOrder.getBookingAgentName());
				String companyDivisionAcctgCodeUnique="";
				 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
				 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
					 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
				 }
			     if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
					actCode=partnerManager.getAccountCrossReference(serviceOrder.getBookingAgentCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
				}else{
					actCode=partnerManager.getAccountCrossReferenceUnique(serviceOrder.getBookingAgentCode(),sessionCorpID);
				}
			     accountLine.setActgCode(actCode);	
			}
			if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
				String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
				try{  
					String roles[] = agentRoleValue.split("~");
				  if(roles[3].toString().equals("hauler")){
					   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
						   accountLine.setDivision("01");
					   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
						   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
							   accountLine.setDivision("01");
						   }else{ 
							   String divisionTemp="";
									if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
									}
									if(!divisionTemp.equalsIgnoreCase("")){
										accountLine.setDivision(divisionTemp);
									}else{
										accountLine.setDivision(null);
									}
						     
						   }
					   }else{
						   String divisionTemp="";
								if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
								divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
								}
								if(!divisionTemp.equalsIgnoreCase("")){
									accountLine.setDivision(divisionTemp);
								}else{
									accountLine.setDivision(null);
								}
					   }
				  }else{
					  String divisionTemp="";
							if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
							divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
							}
							if(!divisionTemp.equalsIgnoreCase("")){
								accountLine.setDivision(divisionTemp);
							}else{
								accountLine.setDivision(null);
							}
				  }
				}catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			if(multiCurrency.equalsIgnoreCase("Y")){
				boolean contractType=false;
				if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
					String	contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
					if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
						contractType=true;	
					}
				}
				String billingCurrency="";
				if(contractType){
					billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getBillToCode(),sessionCorpID);
				}			
				if(!billingCurrency.equalsIgnoreCase("")){
					AccountLineUtil.setRecRateCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, billingCurrency);
					/*if(contractType){
					AccountLineUtil.setContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, billingCurrency);
					}*/
				}else{
					if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals("")){
						AccountLineUtil.setRecRateCurrencyAndExchangeRageForBaseCurrency(accountLine, baseCurrency);
						/*if(contractType){
						AccountLineUtil.setContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrency);
						}*/
					}else{
						AccountLineUtil.setRecRateCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
						/*if(contractType){
						AccountLineUtil.setContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
						}*/
					}
				}
				if(contractType){
					if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals("")){
						AccountLineUtil.setContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrency);
					}else{
						AccountLineUtil.setContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
					}
				}
			}
			if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals("")){
				AccountLineUtil.setCountryCurrencyAndExchangeRageForBaseCurrency(accountLine, baseCurrency);
				if(contractType){
				AccountLineUtil.setOnlyPayableContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrency);
				}
			}else{
				AccountLineUtil.setCountryCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
				if(contractType){
				AccountLineUtil.setOnlyPayableContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
				}
			}
			
			 accountLine.setAccountLineNumber(AccountLineUtil.getAccountLineNumber(serviceOrderManager, serviceOrder));
			accountLineManager.save(accountLine); 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		return SUCCESS;	
    }
    
	@SkipValidation
	public String addPricingLine(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			savePricingLine();
			String user = getRequest().getRemoteUser();
			String baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
			serviceOrder = serviceOrderManager.get(sid);
			billing=billingManager.get(sid);
			trackingStatus=trackingStatusManager.get(sid);
			accountLine = new AccountLine(); 
			accountLine.setBasis("");
			accountLine.setCategory(""); 
			accountLine.setCorpID(sessionCorpID);
			accountLine.setStatus(true);
			accountLine.setServiceOrder(serviceOrder);
			accountLine.setServiceOrderId(serviceOrder.getId());
			accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
			accountLine.setShipNumber(serviceOrder.getShipNumber());
			accountLine.setCreatedOn(new Date());
			accountLine.setCreatedBy(user);
			accountLine.setUpdatedOn(new Date());
			accountLine.setUpdatedBy(user); 
			accountLine.setDisplayOnQuote(false);
			accountLine.setAdditionalService(false);
			accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
			accountLine.setContract(billing.getContract());
			accountLine.setEstVatDescr("4"); 
			accountLine.setEstVatPercent(euVatPercentList.get("4"));
			accountLine.setBillToCode(billing.getBillToCode());
			accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
			accountLine.setNetworkBillToName(billing.getNetworkBillToName());
			if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
				accountLine.setRecVatDescr(billing.getPrimaryVatCode());
				if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
					String recVatPercent="0";
					if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
			    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
			    		}
					accountLine.setRecVatPercent(recVatPercent);
					}
			}
			boolean activateAccPortal =true;
			try{
			if(accountLineAccountPortalFlag){	
			activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
			accountLine.setActivateAccPortal(activateAccPortal);
			}
			}catch(Exception e){
				
			}
			accountLine.setBillToName(billing.getBillToName());
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			    }
			if((!(trackingStatus.getSoNetworkGroup())) && billingCMMContractType && networkAgent && (!(trackingStatus.getAccNetworkGroup()))){
				String actCode="";
				accountLine.setVendorCode(serviceOrder.getBookingAgentCode());  
				accountLine.setEstimateVendorName(serviceOrder.getBookingAgentName());
				String companyDivisionAcctgCodeUnique="";
				 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
				 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
					 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
				 }
			     if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
					actCode=partnerManager.getAccountCrossReference(serviceOrder.getBookingAgentCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
				}else{
					actCode=partnerManager.getAccountCrossReferenceUnique(serviceOrder.getBookingAgentCode(),sessionCorpID);
				}
			     accountLine.setActgCode(actCode);	
			}
			if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
				String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
				try{  
					String roles[] = agentRoleValue.split("~");
				  if(roles[3].toString().equals("hauler")){
					   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
						   accountLine.setDivision("01");
					   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
						   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
							   accountLine.setDivision("01");
						   }else{ 
							   String divisionTemp="";
									if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
									}
									if(!divisionTemp.equalsIgnoreCase("")){
										accountLine.setDivision(divisionTemp);
									}else{
										accountLine.setDivision(null);
									}
						     
						   }
					   }else{
						   String divisionTemp="";
								if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
								divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
								}
								if(!divisionTemp.equalsIgnoreCase("")){
									accountLine.setDivision(divisionTemp);
								}else{
									accountLine.setDivision(null);
								}
					   }
				  }else{
					  String divisionTemp="";
							if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
							divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
							}
							if(!divisionTemp.equalsIgnoreCase("")){
								accountLine.setDivision(divisionTemp);
							}else{
								accountLine.setDivision(null);
							}
				  }
				}catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}		
			//accountLine.setEstCurrency(baseCurrency);
			//accountLine.setEstValueDate(new Date());
			//accountLine.setEstExchangeRate(new BigDecimal(1.0000));
			baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID);
			if(multiCurrency.equalsIgnoreCase("Y")){
				boolean contractType=false;
				if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String	contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
					if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
						contractType=true;	
					}
					}
				String billingCurrency="";
				if(contractType){
				billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getBillToCode(),sessionCorpID);
				}			
				if(!billingCurrency.equalsIgnoreCase(""))
				{
					accountLine.setRecRateCurrency(billingCurrency);
					accountLine.setEstSellCurrency(billingCurrency);
					accountLine.setRevisionSellCurrency(billingCurrency);
					accountLine.setRacValueDate(new Date());
					accountLine.setEstSellValueDate(new Date());
					accountLine.setRevisionSellValueDate(new Date());

					accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,billingCurrency);
					if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
						try{
						accountLine.setRecRateExchange(new BigDecimal(accExchangeRateList.get(0).toString()));
						accountLine.setEstSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
						accountLine.setRevisionSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
						}catch(Exception e){
						accountLine.setRecRateExchange(new BigDecimal(1.0000));
						accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
						accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
						}
					}else{
						accountLine.setRecRateExchange(new BigDecimal(1.0000));
						accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
						accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
					}
				}else{
				if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals(""))
				{
				accountLine.setRecRateCurrency(baseCurrency);
				accountLine.setRacValueDate(new Date());
				accountLine.setRecRateExchange(new BigDecimal(1.0000));
				accountLine.setEstSellCurrency(baseCurrency);
				accountLine.setEstSellValueDate(new Date());
				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
				accountLine.setRevisionSellCurrency(baseCurrency);
				accountLine.setRevisionSellValueDate(new Date());
				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
				}
				else
				{
				accountLine.setRecRateCurrency(baseCurrencyCompanyDivision);
				accountLine.setRacValueDate(new Date());
				accountLine.setEstSellCurrency(baseCurrencyCompanyDivision);
				accountLine.setEstSellValueDate(new Date());
				accountLine.setRevisionSellCurrency(baseCurrencyCompanyDivision);
				accountLine.setRevisionSellValueDate(new Date());
				accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
				if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
					try{
					accountLine.setRecRateExchange(new BigDecimal(accExchangeRateList.get(0).toString()));	
					accountLine.setEstSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
					accountLine.setRevisionSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
					}catch(Exception e){
					accountLine.setRecRateExchange(new BigDecimal(1.0000));
					accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
					accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
					}
				}else{
					accountLine.setRecRateExchange(new BigDecimal(1.0000));
					accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
					accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
				}
				}
				}
				
			}
			if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
			{
			accountLine.setEstCurrency(baseCurrency);
			accountLine.setRevisionCurrency(baseCurrency);	
			accountLine.setCountry(baseCurrency);
			accountLine.setEstValueDate(new Date());
			accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
			accountLine.setRevisionValueDate(new Date());
			accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
			accountLine.setValueDate(new Date());
			accountLine.setExchangeRate(new Double(1.0));
			}
			else
			{
			accountLine.setEstCurrency(baseCurrencyCompanyDivision);
			accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
			accountLine.setCountry(baseCurrencyCompanyDivision);
			accountLine.setEstValueDate(new Date());
			accountLine.setRevisionValueDate(new Date());
			accountLine.setValueDate(new Date());
			//accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision); 
			accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
				try{
				accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
				}catch(Exception e){
					accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
					accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
					accountLine.setExchangeRate(new Double(1.0));	
				}
			}else{
				accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
				accountLine.setExchangeRate(new Double(1.0));
			}
			}
			maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
			 if ( maxLineNumber.get(0) == null ) {          
			 	accountLineNumber = "001";
			 }else {
			 	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
			     if((autoLineNumber.toString()).length() == 2) {
			     	accountLineNumber = "0"+(autoLineNumber.toString());
			     }
			     else if((autoLineNumber.toString()).length() == 1) {
			     	accountLineNumber = "00"+(autoLineNumber.toString());
			     } 
			     else {
			     	accountLineNumber=autoLineNumber.toString();
			     }
			 }
			 accountLine.setAccountLineNumber(accountLineNumber);
			accountLineManager.save(accountLine);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	private String soId;
	public String getSoId() {
		return soId;
	}

	public void setSoId(String soId) {
		this.soId = soId;
	}
	private ExpressionManager expressionManager;
	public void setExpressionManager(ExpressionManager expressionManager) {
		this.expressionManager = expressionManager;
	}

	@SkipValidation
	public String findQuotationDiscription(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			if((soId==null)||(soId.toString().trim().equalsIgnoreCase(""))){
				soId="";
			}
			costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
			if(!costElementFlag){
				quotationDiscriptionList=	accountLineManager.findQuotationDiscription(contract,chargeCode,sessionCorpID);
			}else{
				quotationDiscriptionList = accountLineManager.findChargeDetailFromSO(contract,chargeCode,sessionCorpID,jobType,routing1,companyDivision);
			}
			String wording="";
			  if(!soId.equalsIgnoreCase("")){
				  List al=chargesManager.findChargeId(chargeCode, contract);
				  if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().equalsIgnoreCase(""))){
					  Charges charge=chargesManager.get(Long.parseLong(al.get(0).toString()));
					  Billing billing=billingManager.get(Long.parseLong(soId));
					  Miscellaneous miscellaneous=miscellaneousManager.get(Long.parseLong(soId));
					  if((charge.getWording()!=null)&&(!charge.getWording().toString().trim().equalsIgnoreCase(""))){
						  try {

								Map wordingValues = new HashMap();
								
								if (billing.getOnHand() != null) {
									wordingValues.put("billing.onHand", new BigDecimal(billing.getOnHand().toString()));
								} else {
									wordingValues.put("billing.onHand", 0.00);
								}
								if (charge.getPerValue() != null) {
									wordingValues.put("charges.perValue", new BigDecimal(charge.getPerValue().toString()));
								} else {
									wordingValues.put("charges.perValue", 0.00);
								}
								if (billing.getCycle() != 0 ) {
									String cy=billing.getCycle()+"";
									wordingValues.put("billing.cycle", new BigDecimal(cy));
								} else {
									wordingValues.put("billing.cycle", 0.00);
								}
								if (billing.getInsuranceRate() != null) {
									wordingValues.put("billing.insuranceRate", (BigDecimal) billing.getInsuranceRate());
								} else {
									wordingValues.put("billing.insuranceRate", 0.00);
								}
								if (billing.getInsuranceValueActual() != null) {
									wordingValues.put("billing.insuranceValueActual", (BigDecimal) billing.getInsuranceValueActual());
								} else {
									wordingValues.put("billing.insuranceValueActual", 0.00);
								}
								if (billing.getBaseInsuranceValue() != null) {
									wordingValues.put("billing.baseInsuranceValue", (BigDecimal) billing.getBaseInsuranceValue());
								} else {
									wordingValues.put("billing.baseInsuranceValue", 0.00);
								}
								if (charge.getMinimum() != null) {
									wordingValues.put("charges.minimum", (BigDecimal) charge.getMinimum());
								} else {
									wordingValues.put("charges.minimum", 0.00);
								}
								if (billing.getPostGrate() != null) {
									wordingValues.put("billing.postGrate", (BigDecimal) billing.getPostGrate().setScale(2,BigDecimal.ROUND_HALF_UP));
								} else {
									wordingValues.put("billing.postGrate", 0.00000);
								}
								if (charge.getDescription() != null) {
									wordingValues.put("charges.description", charge.getDescription().toString());
								} else {
									wordingValues.put("charges.description", "");
								}
								if (charge.getUseDiscount() != null) {
									wordingValues.put("charges.useDiscount", charge.getUseDiscount().toString());
								} else {
									wordingValues.put("charges.useDiscount", "");
								}
								if (charge.getPricePre() != 0.00) {
									wordingValues.put("charges.pricepre", charge.getPricePre());
								} else {
									wordingValues.put("charges.pricepre", "");
								}
								if (charge.getPerItem() != null) {
									wordingValues.put("form.qty", charge.getPerItem().toString());
								} else {
									wordingValues.put("form.qty", 0.0);
								}
								if(miscellaneous.getActualNetWeight()!=null){
									wordingValues.put("miscellaneous.actualNetWeight", new BigDecimal(miscellaneous.getActualNetWeight().toString()));
								}else{
									wordingValues.put("miscellaneous.actualNetWeight", 0.00);								
								}
								if(miscellaneous.getActualGrossWeight()!=null){
									wordingValues.put("miscellaneous.actualGrossWeight", new BigDecimal(miscellaneous.getActualGrossWeight().toString()));								
								}else{
									wordingValues.put("miscellaneous.actualGrossWeight", 0.00);
								}
								if(miscellaneous.getEstimateGrossWeight()!=null){
									wordingValues.put("miscellaneous.estimateGrossWeight", new BigDecimal(miscellaneous.getEstimateGrossWeight().toString()));								
								}else{
									wordingValues.put("miscellaneous.estimateGrossWeight", 0.00);
								}
								if(miscellaneous.getEstimatedNetWeight()!=null){
									wordingValues.put("miscellaneous.estimatedNetWeight", new BigDecimal(miscellaneous.getEstimatedNetWeight().toString()));								
								}else{
									wordingValues.put("miscellaneous.estimatedNetWeight", 0.00);
								}
								if(miscellaneous.getEstimateGrossWeightKilo()!=null){
									wordingValues.put("miscellaneous.estimateGrossWeightKilo", new BigDecimal(miscellaneous.getEstimateGrossWeightKilo().toString()));								
								}else{
									wordingValues.put("miscellaneous.estimateGrossWeightKilo", 0.00);
								}
								if(miscellaneous.getEstimatedNetWeightKilo()!=null){
									wordingValues.put("miscellaneous.estimatedNetWeightKilo", new BigDecimal(miscellaneous.getEstimatedNetWeightKilo().toString()));								
								}else{
									wordingValues.put("miscellaneous.estimatedNetWeightKilo", 0.00);
								}
								if(miscellaneous.getChargeableGrossWeight()!=null){
									wordingValues.put("miscellaneous.chargeableGrossWeight", new BigDecimal(miscellaneous.getChargeableGrossWeight().toString()));								
								}else{
									wordingValues.put("miscellaneous.chargeableGrossWeight", 0.00);
								}
								if(miscellaneous.getChargeableNetWeight()!=null){
									wordingValues.put("miscellaneous.chargeableNetWeight", new BigDecimal(miscellaneous.getChargeableNetWeight().toString()));								
								}else{
									wordingValues.put("miscellaneous.chargeableNetWeight", 0.00);
								}
								if(miscellaneous.getActualCubicFeet()!=null){
									wordingValues.put("miscellaneous.actualCubicFeet", new BigDecimal(miscellaneous.getActualCubicFeet().toString()));								
								}else{
									wordingValues.put("miscellaneous.actualCubicFeet", 0.00);
								}
								  if(miscellaneous.getNetActualCubicFeet()!=null){
										wordingValues.put("miscellaneous.netActualCubicFeet", new BigDecimal(miscellaneous.getNetActualCubicFeet().toString()));								
									}else{
										wordingValues.put("miscellaneous.netActualCubicFeet", 0.00);
									}
								if(miscellaneous.getActualNetWeightKilo()!=null){
									wordingValues.put("miscellaneous.actualNetWeightKilo", new BigDecimal(miscellaneous.getActualNetWeightKilo().toString()));								
								}else{
									wordingValues.put("miscellaneous.actualNetWeightKilo", 0.00);
								}
								if(miscellaneous.getActualGrossWeightKilo()!=null){
									wordingValues.put("miscellaneous.actualGrossWeightKilo", new BigDecimal(miscellaneous.getActualGrossWeightKilo().toString()));								
								}else{
									wordingValues.put("miscellaneous.actualGrossWeightKilo", 0.00);
								}
								if(miscellaneous.getActualCubicMtr()!=null){
									wordingValues.put("miscellaneous.actualCubicMtr", new BigDecimal(miscellaneous.getActualCubicMtr().toString()));								
								}else{
									wordingValues.put("miscellaneous.actualCubicMtr", 0.00);
								}
								if(miscellaneous.getNetActualCubicMtr()!=null){
									wordingValues.put("miscellaneous.netActualCubicMtr", new BigDecimal(miscellaneous.getNetActualCubicMtr().toString()));								
								}else{
									wordingValues.put("miscellaneous.netActualCubicMtr", 0.00);
								}
								
								if(miscellaneous.getEstimateCubicFeet()!=null){
									wordingValues.put("miscellaneous.estimateCubicFeet", new BigDecimal(miscellaneous.getEstimateCubicFeet().toString()));								
								}else{
									wordingValues.put("miscellaneous.estimateCubicFeet", 0.00);
								}
								if(miscellaneous.getNetEstimateCubicFeet()!=null){
									wordingValues.put("miscellaneous.netEstimateCubicFeet", new BigDecimal(miscellaneous.getNetEstimateCubicFeet().toString()));								
								}else{
									wordingValues.put("miscellaneous.netEstimateCubicFeet", 0.00);
								}
								if(miscellaneous.getEstimateCubicMtr()!=null){
									wordingValues.put("miscellaneous.estimateCubicMtr", new BigDecimal(miscellaneous.getEstimateCubicMtr().toString()));								
								}else{
									wordingValues.put("miscellaneous.estimateCubicMtr", 0.00);
								}
								if(miscellaneous.getNetEstimateCubicMtr()!=null){
									wordingValues.put("miscellaneous.netEstimateCubicMtr", new BigDecimal(miscellaneous.getNetEstimateCubicMtr().toString()));								
								}else{
									wordingValues.put("miscellaneous.netEstimateCubicMtr", 0.00);
								}
								if(miscellaneous.getChargeableCubicFeet()!=null){
									wordingValues.put("miscellaneous.chargeableCubicFeet", new BigDecimal(miscellaneous.getChargeableCubicFeet().toString()));								
								}else{
									wordingValues.put("miscellaneous.chargeableCubicFeet", 0.00);
								}
								if(miscellaneous.getChargeableNetCubicFeet()!=null){
									wordingValues.put("miscellaneous.chargeableNetCubicFeet", new BigDecimal(miscellaneous.getChargeableNetCubicFeet().toString()));								
								}else{
									wordingValues.put("miscellaneous.chargeableNetCubicFeet", 0.00);
								}
								if(miscellaneous.getChargeableCubicMtr()!=null){
									wordingValues.put("miscellaneous.chargeableCubicMtr", new BigDecimal(miscellaneous.getChargeableCubicMtr().toString()));								
								}else{
									wordingValues.put("miscellaneous.chargeableCubicMtr", 0.00);
								}
								if(miscellaneous.getChargeableNetCubicMtr()!=null){
									wordingValues.put("miscellaneous.chargeableNetCubicMtr", new BigDecimal(miscellaneous.getChargeableNetCubicMtr().toString()));								
								}else{
									wordingValues.put("miscellaneous.chargeableNetCubicMtr", 0.00);
								}
								
	       						if(miscellaneous.getActualAutoWeight()!=null){
	       							wordingValues.put("miscellaneous.actualAutoWeight", new BigDecimal(miscellaneous.getActualAutoWeight().toString()));								
	       						}else{
	       							wordingValues.put("miscellaneous.actualAutoWeight", 0.00);
	       						}							
								
								if(billing.getPackDiscount()!=null){
									wordingValues.put("billing.packDiscount", new BigDecimal(billing.getPackDiscount().toString()));								
								}else{
									wordingValues.put("billing.packDiscount", 0.00);
								}	
								if(billing.getStorageMeasurement()!=null){
	       							wordingValues.put("billing.storageMeasurement", billing.getStorageMeasurement().toString());								
	       						}else{
	       							wordingValues.put("billing.storageMeasurement", "");
	       						}
								if(miscellaneous.getEntitleCubicFeet()!=null){
									wordingValues.put("miscellaneous.entitleCubicFeet", new BigDecimal(miscellaneous.getEntitleCubicFeet().toString()));
									}else{
									wordingValues.put("miscellaneous.entitleCubicFeet", 0.00);
									}
								if(miscellaneous.getChargeableNetWeightKilo()!=null){
									wordingValues.put("miscellaneous.chargeableNetWeightKilo", new BigDecimal(miscellaneous.getChargeableNetWeightKilo().toString()));
									}else{
									wordingValues.put("miscellaneous.chargeableNetWeightKilo", 0.00);
									}
								if(miscellaneous.getChargeableNetWeightKilo()!=null){
									wordingValues.put("miscellaneous.chargeableNetWeightKilo", new BigDecimal(miscellaneous.getChargeableNetWeightKilo().toString()));
									}else{
									wordingValues.put("miscellaneous.chargeableNetWeightKilo", 0.00);
									}
								if(miscellaneous.getEntitleNumberAuto()!=null){
	       							wordingValues.put("miscellaneous.entitleNumberAuto", new BigDecimal(miscellaneous.getEntitleNumberAuto().toString()));								
	       						}else{
	       							wordingValues.put("miscellaneous.entitleNumberAuto", 0.00);
	       						}
	       						if(miscellaneous.getEntitleAutoWeight()!=null){
	       							wordingValues.put("miscellaneous.entitleAutoWeight", new BigDecimal(miscellaneous.getEntitleAutoWeight().toString()));								
	       						}else{
	       							wordingValues.put("miscellaneous.entitleAutoWeight", 0.00);
	       						}
	       						if(miscellaneous.getEstimateAuto()!=null){
	       							wordingValues.put("miscellaneous.estimateAuto", new BigDecimal(miscellaneous.getEstimateAuto().toString()));								
	       						}else{
	       							wordingValues.put("miscellaneous.estimateAuto", 0.00);
	       						}
	       						if(miscellaneous.getEstimatedAutoWeight()!=null){
	       							wordingValues.put("miscellaneous.estimatedAutoWeight", new BigDecimal(miscellaneous.getEstimatedAutoWeight().toString()));								
	       						}else{
	       							wordingValues.put("miscellaneous.estimatedAutoWeight", 0.00);
	       						}
	       						if(miscellaneous.getActualAuto()!=null){
	       							wordingValues.put("miscellaneous.actualAuto", new BigDecimal(miscellaneous.getActualAuto().toString()));								
	       						}else{
	       							wordingValues.put("miscellaneous.actualAuto", 0.00);
	       						}
								wordingValues.put("form.price", 0);
								wordingValues.put("form.extra", 0);
								wordingValues.put("nprice", 0);
								wordingValues.put("form.perqty", 0);
								String chargeWordingTemp=charge.getWording().toString();
								while(chargeWordingTemp.indexOf('$')>-1)
									chargeWordingTemp=chargeWordingTemp.replace('$', '~');
								wording = expressionManager.executeExpression(chargeWordingTemp, wordingValues).toString();
								while(wording.indexOf('~')>-1)
									wording=wording.replace('~', '$'); 
							} catch (Exception ex) {
								logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
							}
					  }
				  }
			  }	
			  if(quotationDiscriptionList!=null && !quotationDiscriptionList.isEmpty() && quotationDiscriptionList.get(0)!=null && !quotationDiscriptionList.get(0).toString().equalsIgnoreCase("")){
				  String str=quotationDiscriptionList.get(0).toString();
				  str=str+"#"+wording;
				  quotationDiscriptionList= new ArrayList();
				  quotationDiscriptionList.add(str);
				  }
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}	
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;	
	}
	
	private String setDescriptionChargeCode;
    private String setDefaultDescriptionChargeCode;
	@SkipValidation
	public String savePricingLine(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			/*try{
			savePricingLineQuotes();
			}catch(Exception e){
				
			}*/
			try{
			      String permKey = sessionCorpID +"-"+"component.tab.accountline.blankDescription";
			      checkDescriptionFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			      }catch(Exception e){
			       e.printStackTrace();
			      }
			String user = getRequest().getRemoteUser();
			serviceOrder = serviceOrderManager.get(sid);
			trackingStatus = trackingStatusManager.get(sid);
			billing=billingManager.get(sid);
			customerFile = serviceOrder.getCustomerFile();
			quotationContract=serviceOrder.getContract();
			if(quotationContract!=null && (!(quotationContract.toString().equals("")))){
				String	contractTypeValue="";
				contractTypeValue=	accountLineManager.checkContractType(quotationContract,sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}
				}
			if(accountLineStatus==null||accountLineStatus.equals(""))
			{
				accountLineStatus="true";
			}
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			java.util.Iterator<AccountLine> idIterator =  accountLineList.iterator();
			HashMap <Long, Map> pricingLineMap = new LinkedHashMap<Long, Map>(); 
			
			String value=""  ;
			String decimalValue="0.00"  ;
			String currencyValue=""  ;
			String ExchangeRateDecimalValue="0.00"  ;
			String accountLineNumberPricing="";
			String contactCurrency = "";
			String chargeStr = "";
			while (idIterator.hasNext()) 
			     {
				  accountLine = (AccountLine) idIterator.next();
				  HashMap <String, String> pricingLineDataMap = new LinkedHashMap<String, String>(); 
				  Long accountLineId=accountLine.getId(); 
				  /*value=getRequest().getParameter("category"+accountLine.getId());
				  accountLine.setCategory(value); 
				  value=getRequest().getParameter("chargeCode"+accountLine.getId());
				  accountLine.setChargeCode(value);  
				  value=getRequest().getParameter("recGl"+accountLine.getId());
				  accountLine.setRecGl(value); 
				  value=getRequest().getParameter("payGl"+accountLine.getId());
				  accountLine.setPayGl(value); */
				  value=getRequest().getParameter("category"+accountLine.getId());
				  accountLine.setCategory(value);
				  String chargeCode = getRequest().getParameter("chargeCode"+accountLine.getId());
				  if(!costElementFlag){
						quotationDiscriptionList=	accountLineManager.findQuotationDiscription(quotationContract,chargeCode,sessionCorpID);
				  }else{
						quotationDiscriptionList = accountLineManager.findChargeDetailFromSO(quotationContract,chargeCode,sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
				  }
				  if(quotationDiscriptionList!=null && !quotationDiscriptionList.isEmpty() && quotationDiscriptionList.get(0)!=null && !quotationDiscriptionList.get(0).toString().equalsIgnoreCase("NoDiscription")){
					  chargeStr= quotationDiscriptionList.get(0).toString();
				  }
				   if(!chargeStr.equalsIgnoreCase("")){
					  String [] chrageDetailArr = chargeStr.split("#");
					  accountLine.setRecGl(chrageDetailArr[1]);
					  accountLine.setPayGl(chrageDetailArr[2]);
					  contactCurrency = chrageDetailArr[3];
					  currencyValue = chrageDetailArr[4];
					  
				  }else{
					  accountLine.setRecGl("");
					  accountLine.setPayGl("");
				  }
				  if(contractType && !chargeCode.equalsIgnoreCase(accountLine.getChargeCode())){//value = getRequest().getParameter("contractCurrency"+accountLine.getId());
					  //currencyValue= getRequest().getParameter("payableContractCurrency"+accountLine.getId());
					   accountLine.setContractCurrency(contactCurrency);
					   accountLine.setPayableContractCurrency(currencyValue);
					   accountLine.setEstimatePayableContractCurrency(currencyValue); 
					   accountLine.setEstimateContractCurrency(contactCurrency); 
					   accountLine.setRevisionPayableContractCurrency(currencyValue); 
					   accountLine.setRevisionContractCurrency(contactCurrency); 
					   List l1ExRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,contactCurrency);
					   if(l1ExRate!=null && !l1ExRate.isEmpty() && l1ExRate.get(0)!=null && !l1ExRate.get(0).toString().equals("") ){
						   decimalValue = l1ExRate.get(0).toString();
					   }
					   List l2ExRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,currencyValue);
					   if(l2ExRate!=null && !l2ExRate.isEmpty() && l2ExRate.get(0)!=null && !l2ExRate.get(0).toString().equals("") ){
						   ExchangeRateDecimalValue = l2ExRate.get(0).toString();
					   }
					  //decimalValue=getRequest().getParameter("contractExchangeRate"+accountLine.getId());
					 // ExchangeRateDecimalValue=getRequest().getParameter("payableContractExchangeRate"+accountLine.getId());
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setContractExchangeRate(new BigDecimal(decimalValue));
								accountLine.setPayableContractExchangeRate(new BigDecimal(ExchangeRateDecimalValue));
								accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(ExchangeRateDecimalValue)); 
								accountLine.setEstimateContractExchangeRate(new BigDecimal(decimalValue)); 
								accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(ExchangeRateDecimalValue)); 
								accountLine.setRevisionContractExchangeRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								  }
					 }else{
						accountLine.setContractExchangeRate(new BigDecimal(0.00)); 
						accountLine.setPayableContractExchangeRate(new BigDecimal(0.00));
						accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(0.00)); 
						accountLine.setEstimateContractExchangeRate(new BigDecimal(0.00)); 
						accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(0.00)); 
						accountLine.setRevisionContractExchangeRate(new BigDecimal(0.00)); 
					 }
					  /*value = contactCurrency;       //getRequest().getParameter("contractValueDate"+accountLine.getId());
					 // currencyValue = getRequest().getParameter("payableContractValueDate"+accountLine.getId());
					  try{  
						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				        Date du = new Date();
				        Date du1 = new Date();
				        du = df.parse(value);
				        du1 = df.parse(currencyValue);
				        accountLine.setContractValueDate(du);
				        accountLine.setPayableContractValueDate(du1);
						accountLine.setEstimatePayableContractValueDate(du1);
						accountLine.setEstimateContractValueDate(du);
						accountLine.setRevisionPayableContractValueDate(du1);
						accountLine.setRevisionContractValueDate(du);
				      }catch (ParseException e){*/
				    	  accountLine.setContractValueDate(new Date());
				    	  accountLine.setPayableContractValueDate(new Date());
							accountLine.setEstimatePayableContractValueDate(new Date());
							accountLine.setEstimateContractValueDate(new Date());
							accountLine.setRevisionPayableContractValueDate(new Date());
							accountLine.setRevisionContractValueDate(new Date());
				          // e.printStackTrace();
				     // }
					  }
				  
				  if(contractType){
					  try{

					  accountLine.setEstimatePayableContractCurrency(getRequest().getParameter("estimatePayableContractCurrencyNew"+accountLine.getId()));
					  accountLine.setEstimateContractCurrency(getRequest().getParameter("estimateContractCurrencyNew"+accountLine.getId()));
						 String dateTemp="";
						 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
						 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

						 dateTemp=getRequest().getParameter("estimatePayableContractValueDateNew"+accountLine.getId());					 
						  if(dateTemp!=null && (!(dateTemp.equals("")))){
								 try{
									   
								      Date date = sdfSource.parse(dateTemp);	
								      String dt1=sdfDestination.format(date);
								      date = sdfDestination.parse(dt1);
									  accountLine.setEstimatePayableContractValueDate(date); 
							     }catch(Exception e){
								      Date date = sdfDestination.parse(dateTemp);	
									  accountLine.setEstimatePayableContractValueDate(date); 
							     }
								  }else{
									  accountLine.setEstimatePayableContractValueDate(new Date());
								  }
							 dateTemp=getRequest().getParameter("estimateContractValueDateNew"+accountLine.getId());
							  if(dateTemp!=null && (!(dateTemp.equals("")))){
									 try{ 
									      Date date = sdfSource.parse(dateTemp);								 
									      String dt1=sdfDestination.format(date);
									      date = sdfDestination.parse(dt1);
									      accountLine.setEstimateContractValueDate(date); 

								     }catch(Exception e){
									      Date date = sdfDestination.parse(dateTemp);								 
										  accountLine.setEstimateContractValueDate(date); 
								    	 
								     }
									  }else{
										  accountLine.setEstimateContractValueDate(new Date());
									  }
						  
						 
							  decimalValue=getRequest().getParameter("estimatePayableContractExchangeRateNew"+accountLine.getId());
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(decimalValue)); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }
							  decimalValue=getRequest().getParameter("estimateContractExchangeRateNew"+accountLine.getId());
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstimateContractExchangeRate(new BigDecimal(decimalValue)); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }
								decimalValue=getRequest().getParameter("estimatePayableContractRateNew"+accountLine.getId());
								if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstimatePayableContractRate(new BigDecimal(decimalValue)); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
											  }
										  }
							  
								decimalValue=getRequest().getParameter("estimateContractRateNew"+accountLine.getId());
								if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstimateContractRate(new BigDecimal(decimalValue)); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
											  }
										  }
								decimalValue=getRequest().getParameter("estimatePayableContractRateAmmountNew"+accountLine.getId());
								if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstimatePayableContractRateAmmount(new BigDecimal(decimalValue)); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
											  }
										  }
								decimalValue=getRequest().getParameter("estimateContractRateAmmountNew"+accountLine.getId());
								if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstimateContractRateAmmount(new BigDecimal(decimalValue)); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
											  }
										  }
								

						 
					  }catch(Exception e){
							logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					  }
					  
					  try{
						  accountLine.setRevisionContractCurrency(getRequest().getParameter("revisionContractCurrencyNew"+accountLine.getId()));
						  accountLine.setRevisionPayableContractCurrency(getRequest().getParameter("revisionPayableContractCurrencyNew"+accountLine.getId()));
						  accountLine.setContractCurrency(getRequest().getParameter("contractCurrencyNew"+accountLine.getId()));
						  accountLine.setPayableContractCurrency(getRequest().getParameter("payableContractCurrencyNew"+accountLine.getId()));

						   					 String dateTemp="";
						  					 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
						  					 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  					 dateTemp=getRequest().getParameter("revisionContractValueDateNew"+accountLine.getId());					 
						  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
						  							 try{
						  								   
						  							      Date date = sdfSource.parse(dateTemp);	
						  							      String dt1=sdfDestination.format(date);
						  							      date = sdfDestination.parse(dt1);
						  								  accountLine.setRevisionContractValueDate(date); 
						  						     }catch(Exception e){
						  							      Date date = sdfDestination.parse(dateTemp);	
						  								  accountLine.setRevisionContractValueDate(date); 
						  						     }
						  							  }else{
						  								  accountLine.setRevisionContractValueDate(new Date());
						  							  }

						  					 dateTemp=getRequest().getParameter("revisionPayableContractValueDateNew"+accountLine.getId());					 
						  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
						  							 try{
						  								   
						  							      Date date = sdfSource.parse(dateTemp);	
						  							      String dt1=sdfDestination.format(date);
						  							      date = sdfDestination.parse(dt1);
						  								  accountLine.setRevisionPayableContractValueDate(date); 
						  						     }catch(Exception e){
						  							      Date date = sdfDestination.parse(dateTemp);	
						  								  accountLine.setRevisionPayableContractValueDate(date); 
						  						     }
						  							  }else{
						  								  accountLine.setRevisionPayableContractValueDate(new Date());
						  							  }

						  					 dateTemp=getRequest().getParameter("contractValueDateNew"+accountLine.getId());					 
						  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
						  							 try{
						  								   
						  							      Date date = sdfSource.parse(dateTemp);	
						  							      String dt1=sdfDestination.format(date);
						  							      date = sdfDestination.parse(dt1);
						  								  accountLine.setContractValueDate(date); 
						  						     }catch(Exception e){
						  							      Date date = sdfDestination.parse(dateTemp);	
						  								  accountLine.setContractValueDate(date); 
						  						     }
						  							  }else{
						  								  accountLine.setContractValueDate(new Date());
						  							  }


						  					 dateTemp=getRequest().getParameter("payableContractValueDateNew"+accountLine.getId());					 
						  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
						  							 try{
						  								   
						  							      Date date = sdfSource.parse(dateTemp);	
						  							      String dt1=sdfDestination.format(date);
						  							      date = sdfDestination.parse(dt1);
						  								  accountLine.setPayableContractValueDate(date); 
						  						     }catch(Exception e){
						  							      Date date = sdfDestination.parse(dateTemp);	
						  								  accountLine.setPayableContractValueDate(date); 
						  						     }
						  							  }else{
						  								  accountLine.setPayableContractValueDate(new Date());
						  							  }



						  						  decimalValue=getRequest().getParameter("revisionContractExchangeRateNew"+accountLine.getId());
						  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
						  								 try{
						  									accountLine.setRevisionContractExchangeRate(new BigDecimal(decimalValue)); 
						  							     }catch(Exception e){
						  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						  									  }
						  								  }
						  						  decimalValue=getRequest().getParameter("revisionPayableContractExchangeRateNew"+accountLine.getId());
						  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
						  								 try{
						  									accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(decimalValue)); 
						  							     }catch(Exception e){
						  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
						  									  }
						  								  }
						  						  decimalValue=getRequest().getParameter("contractExchangeRateNew"+accountLine.getId());
						  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
						  								 try{
						  									accountLine.setContractExchangeRate(new BigDecimal(decimalValue)); 
						  							     }catch(Exception e){
						  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
						  									  }
						  								  }
						  						  decimalValue=getRequest().getParameter("payableContractExchangeRateNew"+accountLine.getId());
						  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
						  								 try{
						  									accountLine.setPayableContractExchangeRate(new BigDecimal(decimalValue)); 
						  							     }catch(Exception e){
						  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
						  									  }
						  								  }

						  }	catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						  }			  
					  
					  try{
						  accountLine.setEstCurrency(getRequest().getParameter("estCurrencyNew"+accountLine.getId()));
						  accountLine.setEstSellCurrency(getRequest().getParameter("estSellCurrencyNew"+accountLine.getId()));
							 String dateTemp="";
							 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
							 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
							  dateTemp=getRequest().getParameter("estValueDateNew"+accountLine.getId());
							  if(dateTemp!=null && (!(dateTemp.equals("")))){
									 try{ 
									      Date date = sdfSource.parse(dateTemp);								 
									      String dt1=sdfDestination.format(date);
									      date = sdfDestination.parse(dt1);
									      accountLine.setEstValueDate(date); 
										 
								     }catch(Exception e){
									      Date date = sdfDestination.parse(dateTemp);								 
										  accountLine.setEstValueDate(date); 
								    	 
								     }
									  }else{
										  accountLine.setEstValueDate(new Date());
									  }

								  dateTemp=getRequest().getParameter("estSellValueDateNew"+accountLine.getId());
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
										 try{ 
										      Date date = sdfSource.parse(dateTemp);								 
										      String dt1=sdfDestination.format(date);
										      date = sdfDestination.parse(dt1);
										      accountLine.setEstSellValueDate(date); 

									     }catch(Exception e){
										      Date date = sdfDestination.parse(dateTemp);								 
											  accountLine.setEstSellValueDate(date); 
									     }
										  }else{
											  accountLine.setEstSellValueDate(new Date());
										  }
						  decimalValue=getRequest().getParameter("estExchangeRateNew"+accountLine.getId());
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstExchangeRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									  }
								  }
						  decimalValue=getRequest().getParameter("estSellExchangeRateNew"+accountLine.getId());
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellExchangeRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
									  }
								  }
						
						decimalValue=getRequest().getParameter("estLocalRateNew"+accountLine.getId());
						if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstLocalRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
									  }
								  }
						decimalValue=getRequest().getParameter("estSellLocalRateNew"+accountLine.getId());
						if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellLocalRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
									  }
								  }
						decimalValue=getRequest().getParameter("estLocalAmountNew"+accountLine.getId());
						if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstLocalAmount(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
									  }
								  }
						decimalValue=getRequest().getParameter("estSellLocalAmountNew"+accountLine.getId());
						if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellLocalAmount(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
									  }
								  }
					  }catch(Exception e){
							logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					  }		

					  try{
						  accountLine.setRevisionCurrency(getRequest().getParameter("revisionCurrencyNew"+accountLine.getId()));
						  accountLine.setCountry(getRequest().getParameter("countryNew"+accountLine.getId()));

						   					 String dateTemp="";
						  					 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
						  					 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  					 dateTemp=getRequest().getParameter("revisionValueDateNew"+accountLine.getId());					 
						  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
						  							 try{
						  								   
						  							      Date date = sdfSource.parse(dateTemp);	
						  							      String dt1=sdfDestination.format(date);
						  							      date = sdfDestination.parse(dt1);
						  								  accountLine.setRevisionValueDate(date); 
						  						     }catch(Exception e){
						  							      Date date = sdfDestination.parse(dateTemp);	
						  								  accountLine.setRevisionValueDate(date); 
						  						     }
						  							  }else{
						  								  accountLine.setRevisionValueDate(new Date());
						  							  }

						  					 dateTemp=getRequest().getParameter("valueDateNew"+accountLine.getId());					 
						  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
						  							 try{
						  								   
						  							      Date date = sdfSource.parse(dateTemp);	
						  							      String dt1=sdfDestination.format(date);
						  							      date = sdfDestination.parse(dt1);
						  								  accountLine.setValueDate(date); 
						  						     }catch(Exception e){
						  							      Date date = sdfDestination.parse(dateTemp);	
						  								  accountLine.setValueDate(date); 
						  						     }
						  							  }else{
						  								  accountLine.setValueDate(new Date());
						  							  }
						  						  decimalValue=getRequest().getParameter("revisionExchangeRateNew"+accountLine.getId());
						  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
						  								 try{
						  									accountLine.setRevisionExchangeRate(new BigDecimal(decimalValue)); 
						  							     }catch(Exception e){
						  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
						  									  }
						  								  }
						  						  decimalValue=getRequest().getParameter("exchangeRateNew"+accountLine.getId());
						  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
						  								 try{
						  									accountLine.setExchangeRate(Double.parseDouble(decimalValue)); 
						  							     }catch(Exception e){
						  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						  									  }
						  								  }

						  }	catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						  }					  
					  
				  }
				  
				  if(!contractType){
					  try{
					  accountLine.setEstCurrency(getRequest().getParameter("estCurrencyNew"+accountLine.getId()));
					  accountLine.setEstSellCurrency(getRequest().getParameter("estSellCurrencyNew"+accountLine.getId()));
						 String dateTemp="";
						 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
						 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  dateTemp=getRequest().getParameter("estValueDateNew"+accountLine.getId());
						  if(dateTemp!=null && (!(dateTemp.equals("")))){
								 try{ 
								      Date date = sdfSource.parse(dateTemp);								 
								      String dt1=sdfDestination.format(date);
								      date = sdfDestination.parse(dt1);
								      accountLine.setEstValueDate(date); 
									 
							     }catch(Exception e){
								      Date date = sdfDestination.parse(dateTemp);								 
									  accountLine.setEstValueDate(date); 
							    	 
							     }
								  }else{
									  accountLine.setEstValueDate(new Date());
								  }

							  dateTemp=getRequest().getParameter("estSellValueDateNew"+accountLine.getId());
							  if(dateTemp!=null && (!(dateTemp.equals("")))){
									 try{ 
									      Date date = sdfSource.parse(dateTemp);								 
									      String dt1=sdfDestination.format(date);
									      date = sdfDestination.parse(dt1);
									      accountLine.setEstSellValueDate(date); 

								     }catch(Exception e){
									      Date date = sdfDestination.parse(dateTemp);								 
										  accountLine.setEstSellValueDate(date); 
								     }
									  }else{
										  accountLine.setEstSellValueDate(new Date());
									  }
					  decimalValue=getRequest().getParameter("estExchangeRateNew"+accountLine.getId());
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstExchangeRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }
					  decimalValue=getRequest().getParameter("estSellExchangeRateNew"+accountLine.getId());
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstSellExchangeRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								  }
							  }
					
					decimalValue=getRequest().getParameter("estLocalRateNew"+accountLine.getId());
					if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstLocalRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								  }
							  }
					decimalValue=getRequest().getParameter("estSellLocalRateNew"+accountLine.getId());
					if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstSellLocalRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }
					decimalValue=getRequest().getParameter("estLocalAmountNew"+accountLine.getId());
					if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstLocalAmount(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }
					decimalValue=getRequest().getParameter("estSellLocalAmountNew"+accountLine.getId());
					if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstSellLocalAmount(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }
				  }catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				  }

				  try{
					  accountLine.setRevisionCurrency(getRequest().getParameter("revisionCurrencyNew"+accountLine.getId()));
					  accountLine.setCountry(getRequest().getParameter("countryNew"+accountLine.getId()));

					   					 String dateTemp="";
					  					 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
					  					 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					  					 dateTemp=getRequest().getParameter("revisionValueDateNew"+accountLine.getId());					 
					  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
					  							 try{
					  								   
					  							      Date date = sdfSource.parse(dateTemp);	
					  							      String dt1=sdfDestination.format(date);
					  							      date = sdfDestination.parse(dt1);
					  								  accountLine.setRevisionValueDate(date); 
					  						     }catch(Exception e){
					  							      Date date = sdfDestination.parse(dateTemp);	
					  								  accountLine.setRevisionValueDate(date); 
					  						     }
					  							  }else{
					  								  accountLine.setRevisionValueDate(new Date());
					  							  }

					  					 dateTemp=getRequest().getParameter("valueDateNew"+accountLine.getId());					 
					  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
					  							 try{
					  								   
					  							      Date date = sdfSource.parse(dateTemp);	
					  							      String dt1=sdfDestination.format(date);
					  							      date = sdfDestination.parse(dt1);
					  								  accountLine.setValueDate(date); 
					  						     }catch(Exception e){
					  							      Date date = sdfDestination.parse(dateTemp);	
					  								  accountLine.setValueDate(date); 
					  						     }
					  							  }else{
					  								  accountLine.setValueDate(new Date());
					  							  }
					  						  decimalValue=getRequest().getParameter("revisionExchangeRateNew"+accountLine.getId());
					  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
					  								 try{
					  									accountLine.setRevisionExchangeRate(new BigDecimal(decimalValue)); 
					  							     }catch(Exception e){
					  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
					  									  }
					  								  }
					  						  decimalValue=getRequest().getParameter("exchangeRateNew"+accountLine.getId());
					  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
					  								 try{
					  									accountLine.setExchangeRate(Double.parseDouble(decimalValue)); 
					  							     }catch(Exception e){
					  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
					  									  }
					  								  }

					  }	catch(Exception e){
							logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					  }					  
				  
				  }	  
				  		
				  try{
					  value=getRequest().getParameter("division"+accountLine.getId());
					  accountLine.setDivision(value);
					  }catch(Exception e){
							logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					  }
					  
				  accountLine.setChargeCode(chargeCode);
				  /*try{
					    List chargeid=chargesManager.findChargeId( accountLine.getChargeCode(),quotationContract);
						Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
						accountLine.setVATExclude(charge.getVATExclude());
					    }catch(Exception e){
					    	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					    }
					 if(accountLine.getVATExclude() !=null && (accountLine.getVATExclude())){
					   accountLine.setRecVatDescr("");
					   accountLine.setRecVatPercent("");
					   accountLine.setRecVatAmt(new BigDecimal(0));
				   }*/
					    
				  value=getRequest().getParameter("vendorCode"+accountLine.getId());
				  accountLine.setVendorCode(value); 
				  value=getRequest().getParameter("estimateVendorName"+accountLine.getId());
				  accountLine.setEstimateVendorName(value); 
				  value=getRequest().getParameter("basis"+accountLine.getId());
				  accountLine.setBasis(value); 
				  decimalValue=getRequest().getParameter("estimateQuantity"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							accountLine.setEstimateQuantity(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
						  }else{
							  accountLine.setEstimateQuantity(new BigDecimal(0.00)); 
						  }
				  try{
				  decimalValue=getRequest().getParameter("estimateSellQuantity"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							accountLine.setEstimateSellQuantity(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
						  }else{
							  accountLine.setEstimateSellQuantity(new BigDecimal(0.00)); 
						  }}catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
				  decimalValue=getRequest().getParameter("estimateRate"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							 accountLine.setEstimateRate(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
						  }else{
							  accountLine.setEstimateRate(new BigDecimal(0.00)); 
						  }
				  
				  decimalValue=getRequest().getParameter("estimateSellRate"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							 accountLine.setEstimateSellRate(new BigDecimal(decimalValue));  
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
						  }else{
							  accountLine.setEstimateSellRate(new BigDecimal(0.00)); 
						  }
				  
				  decimalValue=getRequest().getParameter("estimateExpense"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							 accountLine.setEstimateExpense(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
							  }
						  }else{
							  accountLine.setEstimateExpense(new BigDecimal(0.00)); 
						  }
				  
				  decimalValue=getRequest().getParameter("estimateRevenueAmount"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							 accountLine.setEstimateRevenueAmount(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
						  }else{
							  accountLine.setEstimateRevenueAmount(new BigDecimal(0.00)); 
						  }
				  try{
				  value=getRequest().getParameter("vatDecr"+accountLine.getId());
				  accountLine.setEstVatDescr(value); 
				  value=getRequest().getParameter("vatPers"+accountLine.getId());
				  accountLine.setEstVatPercent(value); 
				  decimalValue=getRequest().getParameter("vatAmt"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							 accountLine.setEstVatAmt(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
						  }else{
							  accountLine.setEstVatAmt(new BigDecimal(0.00)); 
						  }}catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						  }
				  decimalValue="0.00"  ;
				  decimalValue=getRequest().getParameter("estimatePassPercentage"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
				  try{
				  if(decimalValue.indexOf(".")>=0){
				  decimalValue=decimalValue.substring(0, decimalValue.indexOf("."));
				  }
				  accountLine.setEstimatePassPercentage(Integer.parseInt(decimalValue));
				   }catch(Exception e){
					   e.printStackTrace();
					  }
				  } else{
				  accountLine.setEstimatePassPercentage(0);  
				  }			  
				  if(setDescriptionChargeCode!=null &&(!setDescriptionChargeCode.equalsIgnoreCase(""))){
					  value=getRequest().getParameter("quoteDescription"+accountLine.getId());
					  accountLine.setQuoteDescription(value); 
					  accountLine.setDescription(value);
					  accountLine.setNote(value);
				  } 
				  if(setDefaultDescriptionChargeCode!=null && (!setDefaultDescriptionChargeCode.equalsIgnoreCase(""))){
				  value=getRequest().getParameter("quoteDescription"+accountLine.getId());
				  accountLine.setQuoteDescription(value);
				  if(accountLine.getDescription() ==null || accountLine.getDescription().trim().equals("")){
				  accountLine.setDescription(value);
				  }
				  if(accountLine.getNote()==null || accountLine.getNote().trim().equals("") ){
				  accountLine.setNote(value);
				  }}
				  
				  try{
					  if(accountLine.getChargeCode()!=null && (!accountLine.getChargeCode().equalsIgnoreCase(""))){
					    List chargeid=chargesManager.findChargeId( accountLine.getChargeCode(),quotationContract);
						Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
						accountLine.setVATExclude(charge.getVATExclude());
						if(!checkDescriptionFieldVisibility){
						
							  if(accountLine.getDescription()==null || accountLine.getDescription().trim().equalsIgnoreCase("")){
								  accountLine.setDescription(charge.getDescription());
							  }
							  if(accountLine.getQuoteDescription()==null || accountLine.getQuoteDescription().trim().equalsIgnoreCase("")){
								  accountLine.setQuoteDescription(charge.getDescription());
							  }
						}
							  if(accountLine.getNote()==null || accountLine.getNote().trim().equalsIgnoreCase("")){
								  accountLine.setNote(charge.getDescription());
							  }
							  if(accountLine.getAccountLineCostElement()==null || (accountLine.getAccountLineCostElement().equals(""))){
									 if(charge.getCostElement()!=null && (!charge.getCostElement().equals(""))){
									 accountLine.setAccountLineCostElement(charge.getCostElement());
									 }
								  }
								 if(accountLine.getAccountLineScostElementDescription()==null || (accountLine.getAccountLineScostElementDescription().equals(""))){
									 if(charge.getScostElementDescription()!=null && (!charge.getScostElementDescription().equals(""))){
									 accountLine.setAccountLineScostElementDescription(charge.getScostElementDescription());
									 }
								  }  
							  
							  
							  
						  }
						
						
					    }catch(Exception e){
					    	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					    }
					 if(accountLine.getVATExclude() !=null && (accountLine.getVATExclude())){
					   accountLine.setRecVatDescr("");
					   accountLine.setRecVatPercent("");
					   accountLine.setRecVatAmt(new BigDecimal(0));
				   }
				  
				  
				  
				  boolean pricingStatus=false;
				  if(getRequest().getParameter("displayOnQuote"+accountLine.getId())!=null && (getRequest().getParameter("displayOnQuote"+accountLine.getId())).equals("true"))
				  {
					  pricingStatus=true;  
				  }
				  accountLine.setDisplayOnQuote(pricingStatus);
				  boolean additionalServices=false;
				  if(getRequest().getParameter("additionalService"+accountLine.getId())!=null && (getRequest().getParameter("additionalService"+accountLine.getId())).equals("true"))
				  {
					  additionalServices=true;  
				  }
				  accountLine.setAdditionalService(additionalServices);			  
				  try{
					  accountLineNumberPricing=getRequest().getParameter("accountLineNumber"+accountLine.getId());
					  if(accountLineNumberPricing.equals("")){
					  maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
			            if ( maxLineNumber.get(0) == null ) {          
			            	accountLineNumber = "001";
			            }else {
			            	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
			                if((autoLineNumber.toString()).length() == 2) {
			                	accountLineNumber = "0"+(autoLineNumber.toString());
			                }
			                else if((autoLineNumber.toString()).length() == 1) {
			                	accountLineNumber = "00"+(autoLineNumber.toString());
			                } 
			                else {
			                	accountLineNumber=autoLineNumber.toString();
			                }
			            }
			            accountLine.setAccountLineNumber(accountLineNumber);
					  }
					  else{
						  accountLine.setAccountLineNumber(accountLineNumberPricing);
					  }
					  }catch(Exception e){
							logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					  }
				  try{
				  String deviation=getRequest().getParameter("deviation"+accountLine.getId());
				  accountLine.setDeviation(deviation);
				  String estimateDeviation=getRequest().getParameter("estimateDeviation"+accountLine.getId());
				  accountLine.setEstimateDeviation(new BigDecimal(estimateDeviation));
				  String estimateSellDeviation=getRequest().getParameter("estimateSellDeviation"+accountLine.getId());
				  accountLine.setEstimateSellDeviation(new BigDecimal(estimateSellDeviation));
				  if(accountLine.getVendorCode()!=null && (!(accountLine.getVendorCode().toString().trim().equals("")))){
					  
					  
					    if(accountLine.getVATExclude()!=null && (!(accountLine.getVATExclude()))){ 
					    	Map<String, String> vendorCodeVatMap=accountLineManager.getVendorVatCodeMap(sessionCorpID, serviceOrder.getShipNumber());
							String vatCode=vendorCodeVatMap.get(accountLine.getVendorCode());			
							Map<String, String> payVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "PAYVATDESC");
							if((vatCode!=null)&&(!vatCode.equalsIgnoreCase(""))){
								accountLine.setPayVatDescr(vatCode);
								accountLine.setPayVatPercent(payVatPercentList.get(vatCode));
							}
							/*
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode())) {
								accountLine.setPayVatDescr(billing.getOriginAgentVatCode());
							}
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode())){
								accountLine.setPayVatDescr(billing.getDestinationAgentVatCode());
								}
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())){
								accountLine.setPayVatDescr(billing.getOriginSubAgentVatCode());
								} 
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())){
								accountLine.setPayVatDescr(billing.getDestinationSubAgentVatCode());
								}
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getForwarderCode())){
								accountLine.setPayVatDescr(billing.getForwarderVatCode());
								}
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getBrokerCode())){
								accountLine.setPayVatDescr(billing.getBrokerVatCode());
								}
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode())){
								accountLine.setPayVatDescr(billing.getNetworkPartnerVatCode());
								}
							if(accountLine.getVendorCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode())){
								accountLine.setPayVatDescr(billing.getBookingAgentVatCode());
								} */
							}
					  
					  
					  
				  }
				  }catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				  }
				  accountLine.setStatus(true);
				  accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
				  accountLine.setContract(billing.getContract());
				  accountLineManager.save(accountLine);
				  //pricingLineDataMap.put("status", value);
				  //pricingLineMap.put(accountLineId, pricingLineDataMap);
			     }
			String estimatedTotalExpense="0.00";
			String estimatedTotalRevenue="0.00";
			String estimatedGrossMargin="0.00";
			String estimatedGrossMarginPercentage="0.00";
			estimatedTotalExpense=getRequest().getParameter("estimatedTotalExpense"+sid);
			if(estimatedTotalExpense==null ||(estimatedTotalExpense.equals(""))){
			estimatedTotalExpense="0.00";	
			}
			estimatedTotalRevenue=getRequest().getParameter("estimatedTotalRevenue"+sid);
			if(estimatedTotalRevenue==null || (estimatedTotalRevenue.equals(""))){
				estimatedTotalRevenue="0.00";	
				}
			estimatedGrossMargin=getRequest().getParameter("estimatedGrossMargin"+sid);
			if(estimatedGrossMargin==null || (estimatedGrossMargin.equals(""))){
				estimatedGrossMargin="0.00";	
				}
			estimatedGrossMarginPercentage=getRequest().getParameter("estimatedGrossMarginPercentage"+sid);
			if(estimatedGrossMarginPercentage==null || (estimatedGrossMarginPercentage.equals(""))){
				estimatedGrossMarginPercentage="0.00";	
				}
			//System.out.println("\n\n\n\nashish Test>>>>>>>>>>>>>     "+pricingLineMap);
			//accountLineManager.updatePricingLine(pricingLineMap);
			accountLineManager.updateSOTotalPricingLine(estimatedTotalExpense,estimatedTotalRevenue,sid,estimatedGrossMargin,estimatedGrossMarginPercentage);
			/*# 7784 - Always show actual gross margin in accountline overview Start*/
			try{
				ServiceOrder ss=serviceOrderManager.get(sid);
				ss.setProjectedGrossMargin(accountLineManager.getProjectedSum(ss.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(ss.getShipNumber(),"EXP")));
			if(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").doubleValue()!=0){
				ss.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((ss.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(ss.getShipNumber(),"REV")).doubleValue())*100));
			}else{
				ss.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
			}
			ss=serviceOrderManager.save(ss);
			}catch(Exception e){}
			/*# 7784 - Always show actual gross margin in accountline overview End*/
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;		
	}
	
	@SkipValidation
	public String savePricingLineQuotesSave(Long sid){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			try{
			      String permKey = sessionCorpID +"-"+"component.tab.accountline.blankDescription";
			      checkDescriptionFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			      }catch(Exception e){
			       e.printStackTrace();
			      }
			
			String user = getRequest().getRemoteUser();
			serviceOrder = serviceOrderManager.get(sid);
			trackingStatus = trackingStatusManager.get(sid);
			billing=billingManager.get(sid);
			customerFile = serviceOrder.getCustomerFile();
			quotationContract=serviceOrder.getContract();
			if(quotationContract!=null && (!(quotationContract.toString().equals("")))){
				String	contractTypeValue="";
				contractTypeValue=	accountLineManager.checkContractType(quotationContract,sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}
			}
			if(accountLineStatus==null||accountLineStatus.equals(""))
			{
				accountLineStatus="true";
			}
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			java.util.Iterator<AccountLine> idIterator =  accountLineList.iterator();
			HashMap <Long, Map> pricingLineMap = new LinkedHashMap<Long, Map>(); 
			
			String value=""  ;
			String decimalValue="0.00"  ;
			String currencyValue=""  ;
			String ExchangeRateDecimalValue="0.00"  ;
			String accountLineNumberPricing=""  ;
			String contactCurrency = "";
			String chargeStr = "";
			while (idIterator.hasNext()) 
			     {
				  accountLine = (AccountLine) idIterator.next();
				  HashMap <String, String> pricingLineDataMap = new LinkedHashMap<String, String>(); 
				  Long accountLineId=accountLine.getId(); 
				 /* value=getRequest().getParameter("category"+accountLine.getId());
				  accountLine.setCategory(value); 
				  value=getRequest().getParameter("chargeCode"+accountLine.getId());
				  accountLine.setChargeCode(value); 
				  value=getRequest().getParameter("recGl"+accountLine.getId());
				  accountLine.setRecGl(value); 
				  value=getRequest().getParameter("payGl"+accountLine.getId());
				  accountLine.setPayGl(value); */
				  value=getRequest().getParameter("category"+accountLine.getId());
				  accountLine.setCategory(value);
				  String chargeCode = getRequest().getParameter("chargeCode"+accountLine.getId());
				  if(!costElementFlag){
						quotationDiscriptionList=	accountLineManager.findQuotationDiscription(quotationContract,chargeCode,sessionCorpID);
				  }else{
						quotationDiscriptionList = accountLineManager.findChargeDetailFromSO(quotationContract,chargeCode,sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
				  }
				  if(quotationDiscriptionList!=null && !quotationDiscriptionList.isEmpty() && quotationDiscriptionList.get(0)!=null && !quotationDiscriptionList.get(0).toString().equalsIgnoreCase("NoDiscription")){
					  chargeStr= quotationDiscriptionList.get(0).toString();
				  }
				   if(!chargeStr.equalsIgnoreCase("")){
					  String [] chrageDetailArr = chargeStr.split("#");
					  accountLine.setRecGl(chrageDetailArr[1]);
					  accountLine.setPayGl(chrageDetailArr[2]);
					  contactCurrency = chrageDetailArr[3];
					  currencyValue = chrageDetailArr[4];
					  
				  }else{
					  accountLine.setRecGl("");
					  accountLine.setPayGl("");
				  }
				  if(contractType && !chargeCode.equalsIgnoreCase(accountLine.getChargeCode())){
					  //value = getRequest().getParameter("contractCurrency"+accountLine.getId());
					  //currencyValue= getRequest().getParameter("payableContractCurrency"+accountLine.getId());
					   accountLine.setContractCurrency(contactCurrency);
					   accountLine.setPayableContractCurrency(currencyValue);
					   accountLine.setEstimatePayableContractCurrency(currencyValue); 
					   accountLine.setEstimateContractCurrency(contactCurrency); 
					   accountLine.setRevisionPayableContractCurrency(currencyValue); 
					   accountLine.setRevisionContractCurrency(contactCurrency); 
					   List l1ExRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,contactCurrency);
					   if(l1ExRate!=null && !l1ExRate.isEmpty() && l1ExRate.get(0)!=null && !l1ExRate.get(0).toString().equals("") ){
						   decimalValue = l1ExRate.get(0).toString();
					   }
					   List l2ExRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,currencyValue);
					   if(l2ExRate!=null && !l2ExRate.isEmpty() && l2ExRate.get(0)!=null && !l2ExRate.get(0).toString().equals("") ){
						   ExchangeRateDecimalValue = l2ExRate.get(0).toString();
					   }
					  //decimalValue=getRequest().getParameter("contractExchangeRate"+accountLine.getId());
					 // ExchangeRateDecimalValue=getRequest().getParameter("payableContractExchangeRate"+accountLine.getId());
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setContractExchangeRate(new BigDecimal(decimalValue));
								accountLine.setPayableContractExchangeRate(new BigDecimal(ExchangeRateDecimalValue));
								accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(ExchangeRateDecimalValue)); 
								accountLine.setEstimateContractExchangeRate(new BigDecimal(decimalValue)); 
								accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(ExchangeRateDecimalValue)); 
								accountLine.setRevisionContractExchangeRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								  }
					 }else{
						accountLine.setContractExchangeRate(new BigDecimal(0.00)); 
						accountLine.setPayableContractExchangeRate(new BigDecimal(0.00));
						accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(0.00)); 
						accountLine.setEstimateContractExchangeRate(new BigDecimal(0.00)); 
						accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(0.00)); 
						accountLine.setRevisionContractExchangeRate(new BigDecimal(0.00)); 
					 }
					  /*value = contactCurrency;       //getRequest().getParameter("contractValueDate"+accountLine.getId());
					 // currencyValue = getRequest().getParameter("payableContractValueDate"+accountLine.getId());
					  try{  
						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				        Date du = new Date();
				        Date du1 = new Date();
				        du = df.parse(value);
				        du1 = df.parse(currencyValue);
				        accountLine.setContractValueDate(du);
				        accountLine.setPayableContractValueDate(du1);
						accountLine.setEstimatePayableContractValueDate(du1);
						accountLine.setEstimateContractValueDate(du);
						accountLine.setRevisionPayableContractValueDate(du1);
						accountLine.setRevisionContractValueDate(du);
				      }catch (ParseException e){*/
				    	  accountLine.setContractValueDate(new Date());
				    	  accountLine.setPayableContractValueDate(new Date());
							accountLine.setEstimatePayableContractValueDate(new Date());
							accountLine.setEstimateContractValueDate(new Date());
							accountLine.setRevisionPayableContractValueDate(new Date());
							accountLine.setRevisionContractValueDate(new Date());
				          // e.printStackTrace();
				     // }
					  }
				  
				  
				  if(contractType){
					  try{

					  accountLine.setEstimatePayableContractCurrency(getRequest().getParameter("estimatePayableContractCurrencyNew"+accountLine.getId()));
					  accountLine.setEstimateContractCurrency(getRequest().getParameter("estimateContractCurrencyNew"+accountLine.getId()));
						 String dateTemp="";
						 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
						 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

						 dateTemp=getRequest().getParameter("estimatePayableContractValueDateNew"+accountLine.getId());					 
						  if(dateTemp!=null && (!(dateTemp.equals("")))){
								 try{
									   
								      Date date = sdfSource.parse(dateTemp);	
								      String dt1=sdfDestination.format(date);
								      date = sdfDestination.parse(dt1);
									  accountLine.setEstimatePayableContractValueDate(date); 
							     }catch(Exception e){
								      Date date = sdfDestination.parse(dateTemp);	
									  accountLine.setEstimatePayableContractValueDate(date); 
							     }
								  }else{
									  accountLine.setEstimatePayableContractValueDate(new Date());
								  }
							 dateTemp=getRequest().getParameter("estimateContractValueDateNew"+accountLine.getId());
							  if(dateTemp!=null && (!(dateTemp.equals("")))){
									 try{ 
									      Date date = sdfSource.parse(dateTemp);								 
									      String dt1=sdfDestination.format(date);
									      date = sdfDestination.parse(dt1);
									      accountLine.setEstimateContractValueDate(date); 

								     }catch(Exception e){
									      Date date = sdfDestination.parse(dateTemp);								 
										  accountLine.setEstimateContractValueDate(date); 
								    	 
								     }
									  }else{
										  accountLine.setEstimateContractValueDate(new Date());
									  }
						  
						 
							  decimalValue=getRequest().getParameter("estimatePayableContractExchangeRateNew"+accountLine.getId());
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(decimalValue)); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }
							  decimalValue=getRequest().getParameter("estimateContractExchangeRateNew"+accountLine.getId());
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstimateContractExchangeRate(new BigDecimal(decimalValue)); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }
								decimalValue=getRequest().getParameter("estimatePayableContractRateNew"+accountLine.getId());
								if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstimatePayableContractRate(new BigDecimal(decimalValue)); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
											  }
										  }
							  
								decimalValue=getRequest().getParameter("estimateContractRateNew"+accountLine.getId());
								if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstimateContractRate(new BigDecimal(decimalValue)); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
											  }
										  }
								decimalValue=getRequest().getParameter("estimatePayableContractRateAmmountNew"+accountLine.getId());
								if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstimatePayableContractRateAmmount(new BigDecimal(decimalValue)); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
											  }
										  }
								decimalValue=getRequest().getParameter("estimateContractRateAmmountNew"+accountLine.getId());
								if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstimateContractRateAmmount(new BigDecimal(decimalValue)); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
											  }
										  }
								

						 
					  }catch(Exception e){
							logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					  }
					  
					  try{
						  accountLine.setRevisionContractCurrency(getRequest().getParameter("revisionContractCurrencyNew"+accountLine.getId()));
						  accountLine.setRevisionPayableContractCurrency(getRequest().getParameter("revisionPayableContractCurrencyNew"+accountLine.getId()));
						  accountLine.setContractCurrency(getRequest().getParameter("contractCurrencyNew"+accountLine.getId()));
						  accountLine.setPayableContractCurrency(getRequest().getParameter("payableContractCurrencyNew"+accountLine.getId()));

						   					 String dateTemp="";
						  					 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
						  					 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  					 dateTemp=getRequest().getParameter("revisionContractValueDateNew"+accountLine.getId());					 
						  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
						  							 try{
						  								   
						  							      Date date = sdfSource.parse(dateTemp);	
						  							      String dt1=sdfDestination.format(date);
						  							      date = sdfDestination.parse(dt1);
						  								  accountLine.setRevisionContractValueDate(date); 
						  						     }catch(Exception e){
						  							      Date date = sdfDestination.parse(dateTemp);	
						  								  accountLine.setRevisionContractValueDate(date); 
						  						     }
						  							  }else{
						  								  accountLine.setRevisionContractValueDate(new Date());
						  							  }

						  					 dateTemp=getRequest().getParameter("revisionPayableContractValueDateNew"+accountLine.getId());					 
						  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
						  							 try{
						  								   
						  							      Date date = sdfSource.parse(dateTemp);	
						  							      String dt1=sdfDestination.format(date);
						  							      date = sdfDestination.parse(dt1);
						  								  accountLine.setRevisionPayableContractValueDate(date); 
						  						     }catch(Exception e){
						  							      Date date = sdfDestination.parse(dateTemp);	
						  								  accountLine.setRevisionPayableContractValueDate(date); 
						  						     }
						  							  }else{
						  								  accountLine.setRevisionPayableContractValueDate(new Date());
						  							  }

						  					 dateTemp=getRequest().getParameter("contractValueDateNew"+accountLine.getId());					 
						  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
						  							 try{
						  								   
						  							      Date date = sdfSource.parse(dateTemp);	
						  							      String dt1=sdfDestination.format(date);
						  							      date = sdfDestination.parse(dt1);
						  								  accountLine.setContractValueDate(date); 
						  						     }catch(Exception e){
						  							      Date date = sdfDestination.parse(dateTemp);	
						  								  accountLine.setContractValueDate(date); 
						  						     }
						  							  }else{
						  								  accountLine.setContractValueDate(new Date());
						  							  }


						  					 dateTemp=getRequest().getParameter("payableContractValueDateNew"+accountLine.getId());					 
						  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
						  							 try{
						  								   
						  							      Date date = sdfSource.parse(dateTemp);	
						  							      String dt1=sdfDestination.format(date);
						  							      date = sdfDestination.parse(dt1);
						  								  accountLine.setPayableContractValueDate(date); 
						  						     }catch(Exception e){
						  							      Date date = sdfDestination.parse(dateTemp);	
						  								  accountLine.setPayableContractValueDate(date); 
						  						     }
						  							  }else{
						  								  accountLine.setPayableContractValueDate(new Date());
						  							  }



						  						  decimalValue=getRequest().getParameter("revisionContractExchangeRateNew"+accountLine.getId());
						  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
						  								 try{
						  									accountLine.setRevisionContractExchangeRate(new BigDecimal(decimalValue)); 
						  							     }catch(Exception e){
						  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);	  
						  									  }
						  								  }
						  						  decimalValue=getRequest().getParameter("revisionPayableContractExchangeRateNew"+accountLine.getId());
						  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
						  								 try{
						  									accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(decimalValue)); 
						  							     }catch(Exception e){
						  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						  									  }
						  								  }
						  						  decimalValue=getRequest().getParameter("contractExchangeRateNew"+accountLine.getId());
						  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
						  								 try{
						  									accountLine.setContractExchangeRate(new BigDecimal(decimalValue)); 
						  							     }catch(Exception e){
						  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						  									  }
						  								  }
						  						  decimalValue=getRequest().getParameter("payableContractExchangeRateNew"+accountLine.getId());
						  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
						  								 try{
						  									accountLine.setPayableContractExchangeRate(new BigDecimal(decimalValue)); 
						  							     }catch(Exception e){
						  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
						  									  }
						  								  }

						  }	catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						  }			  
					  
					  try{
						  accountLine.setEstCurrency(getRequest().getParameter("estCurrencyNew"+accountLine.getId()));
						  accountLine.setEstSellCurrency(getRequest().getParameter("estSellCurrencyNew"+accountLine.getId()));
							 String dateTemp="";
							 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
							 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
							  dateTemp=getRequest().getParameter("estValueDateNew"+accountLine.getId());
							  if(dateTemp!=null && (!(dateTemp.equals("")))){
									 try{ 
									      Date date = sdfSource.parse(dateTemp);								 
									      String dt1=sdfDestination.format(date);
									      date = sdfDestination.parse(dt1);
									      accountLine.setEstValueDate(date); 
										 
								     }catch(Exception e){
									      Date date = sdfDestination.parse(dateTemp);								 
										  accountLine.setEstValueDate(date); 
								    	 
								     }
									  }else{
										  accountLine.setEstValueDate(new Date());
									  }

								  dateTemp=getRequest().getParameter("estSellValueDateNew"+accountLine.getId());
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
										 try{ 
										      Date date = sdfSource.parse(dateTemp);								 
										      String dt1=sdfDestination.format(date);
										      date = sdfDestination.parse(dt1);
										      accountLine.setEstSellValueDate(date); 

									     }catch(Exception e){
										      Date date = sdfDestination.parse(dateTemp);								 
											  accountLine.setEstSellValueDate(date); 
									     }
										  }else{
											  accountLine.setEstSellValueDate(new Date());
										  }
						  decimalValue=getRequest().getParameter("estExchangeRateNew"+accountLine.getId());
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstExchangeRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									  }
								  }
						  decimalValue=getRequest().getParameter("estSellExchangeRateNew"+accountLine.getId());
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellExchangeRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
									  }
								  }
						
						decimalValue=getRequest().getParameter("estLocalRateNew"+accountLine.getId());
						if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstLocalRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
									  }
								  }
						decimalValue=getRequest().getParameter("estSellLocalRateNew"+accountLine.getId());
						if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellLocalRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
									  }
								  }
						decimalValue=getRequest().getParameter("estLocalAmountNew"+accountLine.getId());
						if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstLocalAmount(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
									  }
								  }
						decimalValue=getRequest().getParameter("estSellLocalAmountNew"+accountLine.getId());
						if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellLocalAmount(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
									  }
								  }
					  }catch(Exception e){
						  
							logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					  }		

					  try{
						  accountLine.setRevisionCurrency(getRequest().getParameter("revisionCurrencyNew"+accountLine.getId()));
						  accountLine.setCountry(getRequest().getParameter("countryNew"+accountLine.getId()));

						   					 String dateTemp="";
						  					 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
						  					 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  					 dateTemp=getRequest().getParameter("revisionValueDateNew"+accountLine.getId());					 
						  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
						  							 try{
						  								   
						  							      Date date = sdfSource.parse(dateTemp);	
						  							      String dt1=sdfDestination.format(date);
						  							      date = sdfDestination.parse(dt1);
						  								  accountLine.setRevisionValueDate(date); 
						  						     }catch(Exception e){
						  							      Date date = sdfDestination.parse(dateTemp);	
						  								  accountLine.setRevisionValueDate(date); 
						  						     }
						  							  }else{
						  								  accountLine.setRevisionValueDate(new Date());
						  							  }

						  					 dateTemp=getRequest().getParameter("valueDateNew"+accountLine.getId());					 
						  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
						  							 try{
						  								   
						  							      Date date = sdfSource.parse(dateTemp);	
						  							      String dt1=sdfDestination.format(date);
						  							      date = sdfDestination.parse(dt1);
						  								  accountLine.setValueDate(date); 
						  						     }catch(Exception e){
						  							      Date date = sdfDestination.parse(dateTemp);	
						  								  accountLine.setValueDate(date); 
						  						     }
						  							  }else{
						  								  accountLine.setValueDate(new Date());
						  							  }
						  						  decimalValue=getRequest().getParameter("revisionExchangeRateNew"+accountLine.getId());
						  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
						  								 try{
						  									accountLine.setRevisionExchangeRate(new BigDecimal(decimalValue)); 
						  							     }catch(Exception e){
						  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);	  
						  									  }
						  								  }
						  						  decimalValue=getRequest().getParameter("exchangeRateNew"+accountLine.getId());
						  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
						  								 try{
						  									accountLine.setExchangeRate(Double.parseDouble(decimalValue)); 
						  							     }catch(Exception e){
						  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
						  									  }
						  								  }

						  }	catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						  }					  
					  
				  }
				  
				  if(!contractType){
					  try{
					  accountLine.setEstCurrency(getRequest().getParameter("estCurrencyNew"+accountLine.getId()));
					  accountLine.setEstSellCurrency(getRequest().getParameter("estSellCurrencyNew"+accountLine.getId()));
						 String dateTemp="";
						 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
						 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  dateTemp=getRequest().getParameter("estValueDateNew"+accountLine.getId());
						  if(dateTemp!=null && (!(dateTemp.equals("")))){
								 try{ 
								      Date date = sdfSource.parse(dateTemp);								 
								      String dt1=sdfDestination.format(date);
								      date = sdfDestination.parse(dt1);
								      accountLine.setEstValueDate(date); 
									 
							     }catch(Exception e){
								      Date date = sdfDestination.parse(dateTemp);								 
									  accountLine.setEstValueDate(date); 
							    	 
							     }
								  }else{
									  accountLine.setEstValueDate(new Date());
								  }

							  dateTemp=getRequest().getParameter("estSellValueDateNew"+accountLine.getId());
							  if(dateTemp!=null && (!(dateTemp.equals("")))){
									 try{ 
									      Date date = sdfSource.parse(dateTemp);								 
									      String dt1=sdfDestination.format(date);
									      date = sdfDestination.parse(dt1);
									      accountLine.setEstSellValueDate(date); 

								     }catch(Exception e){
									      Date date = sdfDestination.parse(dateTemp);								 
										  accountLine.setEstSellValueDate(date); 
								     }
									  }else{
										  accountLine.setEstSellValueDate(new Date());
									  }
					  decimalValue=getRequest().getParameter("estExchangeRateNew"+accountLine.getId());
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstExchangeRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								  }
							  }
					  decimalValue=getRequest().getParameter("estSellExchangeRateNew"+accountLine.getId());
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstSellExchangeRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								  }
							  }
					
					decimalValue=getRequest().getParameter("estLocalRateNew"+accountLine.getId());
					if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstLocalRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }
					decimalValue=getRequest().getParameter("estSellLocalRateNew"+accountLine.getId());
					if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstSellLocalRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }
					decimalValue=getRequest().getParameter("estLocalAmountNew"+accountLine.getId());
					if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstLocalAmount(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }
					decimalValue=getRequest().getParameter("estSellLocalAmountNew"+accountLine.getId());
					if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstSellLocalAmount(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }
				  }catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				  }

				  try{
					  accountLine.setRevisionCurrency(getRequest().getParameter("revisionCurrencyNew"+accountLine.getId()));
					  accountLine.setCountry(getRequest().getParameter("countryNew"+accountLine.getId()));

					   					 String dateTemp="";
					  					 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
					  					 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					  					 dateTemp=getRequest().getParameter("revisionValueDateNew"+accountLine.getId());					 
					  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
					  							 try{
					  								   
					  							      Date date = sdfSource.parse(dateTemp);	
					  							      String dt1=sdfDestination.format(date);
					  							      date = sdfDestination.parse(dt1);
					  								  accountLine.setRevisionValueDate(date); 
					  						     }catch(Exception e){
					  							      Date date = sdfDestination.parse(dateTemp);	
					  								  accountLine.setRevisionValueDate(date); 
					  						     }
					  							  }else{
					  								  accountLine.setRevisionValueDate(new Date());
					  							  }

					  					 dateTemp=getRequest().getParameter("valueDateNew"+accountLine.getId());					 
					  					  if(dateTemp!=null && (!(dateTemp.equals("")))){
					  							 try{
					  								   
					  							      Date date = sdfSource.parse(dateTemp);	
					  							      String dt1=sdfDestination.format(date);
					  							      date = sdfDestination.parse(dt1);
					  								  accountLine.setValueDate(date); 
					  						     }catch(Exception e){
					  							      Date date = sdfDestination.parse(dateTemp);	
					  								  accountLine.setValueDate(date); 
					  						     }
					  							  }else{
					  								  accountLine.setValueDate(new Date());
					  							  }
					  						  decimalValue=getRequest().getParameter("revisionExchangeRateNew"+accountLine.getId());
					  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
					  								 try{
					  									accountLine.setRevisionExchangeRate(new BigDecimal(decimalValue)); 
					  							     }catch(Exception e){
					  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
					  									  }
					  								  }
					  						  decimalValue=getRequest().getParameter("exchangeRateNew"+accountLine.getId());
					  						  if(decimalValue!=null && (!(decimalValue.equals("")))){
					  								 try{
					  									accountLine.setExchangeRate(Double.parseDouble(decimalValue)); 
					  							     }catch(Exception e){
					  							   	logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
					  									  }
					  								  }

					  }	catch(Exception e){
							logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					  }					  
				  
				  }	  
				  			  
				  try{
					  value=getRequest().getParameter("division"+accountLine.getId());
					  accountLine.setDivision(value);
					  }catch(Exception e){
							logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					  }
					  
				  accountLine.setChargeCode(chargeCode); 
				  try{
					    List chargeid=chargesManager.findChargeId( accountLine.getChargeCode(),quotationContract);
						Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
						accountLine.setVATExclude(charge.getVATExclude());
					    }catch(Exception e){
					    	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					    }
					    if(accountLine.getVATExclude() !=null && (accountLine.getVATExclude())){
							   accountLine.setRecVatDescr("");
							   accountLine.setRecVatPercent("");
							   accountLine.setRecVatAmt(new BigDecimal(0));
						   }	    
				  value=getRequest().getParameter("vendorCode"+accountLine.getId());
				  accountLine.setVendorCode(value); 
				  value=getRequest().getParameter("estimateVendorName"+accountLine.getId());
				  accountLine.setEstimateVendorName(value); 
				  value=getRequest().getParameter("basis"+accountLine.getId());
				  accountLine.setBasis(value); 
				  decimalValue=getRequest().getParameter("estimateQuantity"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							accountLine.setEstimateQuantity(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
							  }
						  }else{
							  accountLine.setEstimateQuantity(new BigDecimal(0.00)); 
						  }
				  try{
				  decimalValue=getRequest().getParameter("estimateSellQuantity"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							accountLine.setEstimateSellQuantity(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
							  }
						  }else{
							  accountLine.setEstimateSellQuantity(new BigDecimal(0.00)); 
						  }
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					  }
				  decimalValue=getRequest().getParameter("estimateRate"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							 accountLine.setEstimateRate(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
							  }
						  }else{
							  accountLine.setEstimateRate(new BigDecimal(0.00)); 
						  }
				  
				  decimalValue=getRequest().getParameter("estimateSellRate"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							 accountLine.setEstimateSellRate(new BigDecimal(decimalValue));  
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
						  }else{
							  accountLine.setEstimateSellRate(new BigDecimal(0.00)); 
						  }
				  
				  decimalValue=getRequest().getParameter("estimateExpense"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							 accountLine.setEstimateExpense(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							  }
						  }else{
							  accountLine.setEstimateExpense(new BigDecimal(0.00)); 
						  }
				  
				  decimalValue=getRequest().getParameter("estimateRevenueAmount"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							 accountLine.setEstimateRevenueAmount(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
							  }
						  }else{
							  accountLine.setEstimateRevenueAmount(new BigDecimal(0.00)); 
						  }
				  try{
				  value=getRequest().getParameter("vatDecr"+accountLine.getId());
				  accountLine.setEstVatDescr(value); 
				  value=getRequest().getParameter("vatPers"+accountLine.getId());
				  accountLine.setEstVatPercent(value); 
				  decimalValue=getRequest().getParameter("vatAmt"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							 accountLine.setEstVatAmt(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
						  }else{
							  accountLine.setEstVatAmt(new BigDecimal(0.00)); 
						  }}catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
						  }
				  decimalValue="0.00"  ;
				  decimalValue=getRequest().getParameter("estimatePassPercentage"+accountLine.getId());
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
				 try{ 
				  if(decimalValue.indexOf(".")>=0){
						  decimalValue=decimalValue.substring(0, decimalValue.indexOf("."));
				    }
				  accountLine.setEstimatePassPercentage(Integer.parseInt(decimalValue)); 
			     }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);	  
					  }
				  }else{
					  accountLine.setEstimatePassPercentage(0);   
				  }
				  value=getRequest().getParameter("quoteDescription"+accountLine.getId());
				  accountLine.setQuoteDescription(value); 
				  if(accountLine.getDescription() ==null || accountLine.getDescription().trim().equals("")){
				  accountLine.setDescription(value);
				  }
				  if(accountLine.getNote()==null || accountLine.getNote().trim().equals("") ){
				  accountLine.setNote(value);
				  }
				  
				  try{
					  if(accountLine.getChargeCode()!=null && (!accountLine.getChargeCode().equalsIgnoreCase(""))){
					    List chargeid=chargesManager.findChargeId( accountLine.getChargeCode(),quotationContract);
						Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
						accountLine.setVATExclude(charge.getVATExclude());
						if(!checkDescriptionFieldVisibility){
							  if(accountLine.getDescription()==null || accountLine.getDescription().trim().equalsIgnoreCase("")){
								  accountLine.setDescription(charge.getDescription());
							  }
							  if(accountLine.getQuoteDescription()==null || accountLine.getQuoteDescription().trim().equalsIgnoreCase("")){
								  accountLine.setQuoteDescription(charge.getDescription());
							  }
						}
							  if(accountLine.getNote()==null || accountLine.getNote().trim().equalsIgnoreCase("")){
								  accountLine.setNote(charge.getDescription());
							  }
							  
							  if(accountLine.getAccountLineCostElement()==null || (accountLine.getAccountLineCostElement().equals(""))){
									 if(charge.getCostElement()!=null && (!charge.getCostElement().equals(""))){
									 accountLine.setAccountLineCostElement(charge.getCostElement());
									 }
								  }
								 if(accountLine.getAccountLineScostElementDescription()==null || (accountLine.getAccountLineScostElementDescription().equals(""))){
									 if(charge.getScostElementDescription()!=null && (!charge.getScostElementDescription().equals(""))){
									 accountLine.setAccountLineScostElementDescription(charge.getScostElementDescription());
									 }
								  }
							  
						  }
						
						
					    }catch(Exception e){
					    	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					    }
					    if(accountLine.getVATExclude() !=null && (accountLine.getVATExclude())){
							   accountLine.setRecVatDescr("");
							   accountLine.setRecVatPercent("");
							   accountLine.setRecVatAmt(new BigDecimal(0));
						   }
				  
				  boolean pricingStatus=false;
				  if(getRequest().getParameter("displayOnQuote"+accountLine.getId())!=null && (getRequest().getParameter("displayOnQuote"+accountLine.getId())).equals("true"))
				  {
					  pricingStatus=true;  
				  }
				  accountLine.setDisplayOnQuote(pricingStatus);
				  boolean additionalServices=false;
				  if(getRequest().getParameter("additionalService"+accountLine.getId())!=null && (getRequest().getParameter("additionalService"+accountLine.getId())).equals("true"))
				  {
					  additionalServices=true;  
				  }
				  accountLine.setAdditionalService(additionalServices);			  
				  accountLine.setStatus(true);
				  try{
				  accountLineNumberPricing=getRequest().getParameter("accountLineNumber"+accountLine.getId());
				  if(accountLineNumberPricing.equals("")){
				  maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
			        if ( maxLineNumber.get(0) == null ) {          
			        	accountLineNumber = "001";
			        }else {
			        	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
			            if((autoLineNumber.toString()).length() == 2) {
			            	accountLineNumber = "0"+(autoLineNumber.toString());
			            }
			            else if((autoLineNumber.toString()).length() == 1) {
			            	accountLineNumber = "00"+(autoLineNumber.toString());
			            } 
			            else {
			            	accountLineNumber=autoLineNumber.toString();
			            }
			        }
			        accountLine.setAccountLineNumber(accountLineNumber);
				  }
				  else{
					  accountLine.setAccountLineNumber(accountLineNumberPricing);
				  }
				  }catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
				  }
				  accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
				  accountLine.setContract(quotationContract);
				  try{
				  String deviation=getRequest().getParameter("deviation"+accountLine.getId());
				  accountLine.setDeviation(deviation);
				  String estimateDeviation=getRequest().getParameter("estimateDeviation"+accountLine.getId());
				  accountLine.setEstimateDeviation(new BigDecimal(estimateDeviation));
				  String estimateSellDeviation=getRequest().getParameter("estimateSellDeviation"+accountLine.getId());
				  accountLine.setEstimateSellDeviation(new BigDecimal(estimateSellDeviation));
				  if(accountLine.getVendorCode()!=null && (!(accountLine.getVendorCode().toString().trim().equals("")))){
					  
					  
					    if(accountLine.getVATExclude()!=null && (!(accountLine.getVATExclude()))){ 
					    	Map<String, String> vendorCodeVatMap=accountLineManager.getVendorVatCodeMap(sessionCorpID, serviceOrder.getShipNumber());
							String vatCode=vendorCodeVatMap.get(accountLine.getVendorCode());			
							Map<String, String> payVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "PAYVATDESC");
							if((vatCode!=null)&&(!vatCode.equalsIgnoreCase(""))){
								accountLine.setPayVatDescr(vatCode);
								accountLine.setPayVatPercent(payVatPercentList.get(vatCode));
							}
						/*
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode())) {
								accountLine.setPayVatDescr(billing.getOriginAgentVatCode());
							}
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode())){
								accountLine.setPayVatDescr(billing.getDestinationAgentVatCode());
								}
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())){
								accountLine.setPayVatDescr(billing.getOriginSubAgentVatCode());
								} 
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())){
								accountLine.setPayVatDescr(billing.getDestinationSubAgentVatCode());
								}
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getForwarderCode())){
								accountLine.setPayVatDescr(billing.getForwarderVatCode());
								}
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getBrokerCode())){
								accountLine.setPayVatDescr(billing.getBrokerVatCode());
								}
							if(accountLine.getVendorCode().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode())){
								accountLine.setPayVatDescr(billing.getNetworkPartnerVatCode());
								}
							if(accountLine.getVendorCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode())){
								accountLine.setPayVatDescr(billing.getBookingAgentVatCode());
								} 
						 */
							}
					  
					  
					  
				  }
				  }catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				  }
				  accountLineManager.save(accountLine);
				  //pricingLineDataMap.put("status", value);
				  //pricingLineMap.put(accountLineId, pricingLineDataMap);
			     }
			String estimatedTotalExpense="0.00";
			String estimatedTotalRevenue="0.00";
			String estimatedGrossMargin="0.00";
			String estimatedGrossMarginPercentage="0.00";
			estimatedTotalExpense=getRequest().getParameter("estimatedTotalExpense"+sid);
			if(estimatedTotalExpense==null ||(estimatedTotalExpense.equals(""))){
			estimatedTotalExpense="0.00";	
			}
			estimatedTotalRevenue=getRequest().getParameter("estimatedTotalRevenue"+sid);
			if(estimatedTotalRevenue==null ||(estimatedTotalRevenue.equals(""))){
				estimatedTotalRevenue="0.00";	
				}
			estimatedGrossMargin=getRequest().getParameter("estimatedGrossMargin"+sid);
			if(estimatedGrossMargin==null || (estimatedGrossMargin.equals(""))){
				estimatedGrossMargin="0.00";	
				}
			estimatedGrossMarginPercentage=getRequest().getParameter("estimatedGrossMarginPercentage"+sid);
			if(estimatedGrossMarginPercentage==null ||(estimatedGrossMarginPercentage.equals(""))){
				estimatedGrossMarginPercentage="0.00";	
				}
			//System.out.println("\n\n\n\nashish Test>>>>>>>>>>>>>     "+pricingLineMap);
			//accountLineManager.updatePricingLine(pricingLineMap);
			accountLineManager.updateSOTotalPricingLine(estimatedTotalExpense,estimatedTotalRevenue,sid,estimatedGrossMargin,estimatedGrossMarginPercentage);

			/*# 7784 - Always show actual gross margin in accountline overview Start*/
			try{
			ServiceOrder ss=serviceOrderManager.get(sid);
			ss.setProjectedGrossMargin(accountLineManager.getProjectedSum(ss.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(ss.getShipNumber(),"EXP")));
			if(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").doubleValue()!=0){
			ss.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((ss.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(ss.getShipNumber(),"REV")).doubleValue())*100));
			}else{
			ss.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
			}
			ss=serviceOrderManager.save(ss);
			}catch(Exception e){}
			/*# 7784 - Always show actual gross margin in accountline overview End*/		
			updateAccountLine(serviceOrder);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		return SUCCESS;		
		
	}
	private String inActiveLine;
	@SkipValidation
	public String inActivePricingLineAjax(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			//getPricingHelper();
			ServiceOrder serviceOrder = serviceOrderManager.get(id);
			if(inActiveLine != null && inActiveLine.trim().length() > 0){
				String arr[] = inActiveLine.split(",");
				for (String id : arr) {
					Long id1 = Long.parseLong(id);
					AccountLine accLine = accountLineManager.get(id1);
					accLine.setStatus(false);
					accLine.setUpdatedOn(new Date());
					accLine.setUpdatedBy(getRequest().getRemoteUser());					
					accountLineManager.save(accLine);
					
				/*	try {
						serviceOrder.setEstimatedTotalExpense(serviceOrder.getEstimatedTotalExpense().subtract(accLine.getEstimateExpense()));
						serviceOrder.setEstimatedTotalRevenue(serviceOrder.getEstimatedTotalRevenue().subtract(accLine.getEstimateRevenueAmount()));
						serviceOrder.setEstimatedGrossMargin(serviceOrder.getEstimatedTotalRevenue().subtract(serviceOrder.getEstimatedTotalExpense()));
						if(serviceOrder.getEstimatedTotalRevenue() == null || "".equals(serviceOrder.getEstimatedTotalRevenue().toString().trim()) 
								|| serviceOrder.getEstimatedTotalRevenue().intValue() == 0){
							serviceOrder.setEstimatedGrossMarginPercentage(new BigDecimal(0));
						}else{
							serviceOrder.setEstimatedGrossMarginPercentage((serviceOrder.getEstimatedGrossMargin().divide(serviceOrder.getEstimatedTotalRevenue(),2, BigDecimal.ROUND_HALF_UP)).multiply(new BigDecimal("100")));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					# 7784 - Always show actual gross margin in accountline overview Start
					try{
					serviceOrder.setProjectedGrossMargin(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"EXP")));
					if(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").doubleValue()!=0){
						serviceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((serviceOrder.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV")).doubleValue())*100));					
					}else{
						serviceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
					}
					}catch(Exception e){}*/
					/*# 7784 - Always show actual gross margin in accountline overview End*/
				}
				
				/*List<AccountLine> accList = accountLineManager.findAllAccountLineByID(inActiveLine, sessionCorpID);
				for (AccountLine accLine : accList) {
					accLine.setStatus(false);
					accountLineManager.save(accLine);
					try {
						serviceOrder.setEstimatedTotalExpense(serviceOrder.getEstimatedTotalExpense().subtract(accLine.getEstimateExpense()));
						serviceOrder.setEstimatedTotalRevenue(serviceOrder.getEstimatedTotalRevenue().subtract(accLine.getEstimateRevenueAmount()));
						serviceOrder.setEstimatedGrossMargin(serviceOrder.getEstimatedTotalRevenue().subtract(serviceOrder.getEstimatedTotalExpense()));
						serviceOrder.setEstimatedGrossMarginPercentage((serviceOrder.getEstimatedGrossMargin().divide(serviceOrder.getEstimatedTotalRevenue(),2, BigDecimal.ROUND_HALF_UP)).multiply(new BigDecimal("100")));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				serviceOrderManager.save(serviceOrder);*/
				updateAccountLineAjax(serviceOrder);
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String deletePricingLine(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			//String key = "Pricing one line has been deleted successfully.";   
      // saveMessage(getText(key));
			serviceOrder = serviceOrderManager.get(sid);
			String loginUser = getRequest().getRemoteUser();
			if(accountIdCheck.equals("")||accountIdCheck.equals(","))
			  {
			  }
			 else
			  {
				accountIdCheck=accountIdCheck.trim();
			    if(accountIdCheck.indexOf(",")==0)
				 {
			    	accountIdCheck=accountIdCheck.substring(1);
				 }
				if(accountIdCheck.lastIndexOf(",")==accountIdCheck.length()-1)
				 {
					accountIdCheck=accountIdCheck.substring(0, accountIdCheck.length()-1);
				 }
			    String[] arrayInvoice=accountIdCheck.split(",");
			    int arrayLength = arrayInvoice.length;
				for(int i=0;i<arrayLength;i++)
				 {	
				    id=Long.parseLong(arrayInvoice[i]);  
			          int payPost=accountLineManager.deletePricingLine(id,sid,loginUser);;
			     }
				
				accountIdCheck="";
			 } 
			savePricingLine();
			updateAccountLine(serviceOrder);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		return SUCCESS;		
	}
	private String vendorCodeNew;
    private User discountUserFlag;
	@SkipValidation
	public String getAccountlineField(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			accountLineBasis=accountLineBasis;
			accountLineEstimateQuantity=accountLineEstimateQuantity;
			accountLine=accountLineManager.get(aid);
			billing=billingManager.get(accountLine.getServiceOrderId());
			serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
			trackingStatus=trackingStatusManager.get(accountLine.getServiceOrderId());
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}
			String decimalValue="0.00";
			   decimalValue=getRequest().getParameter("estimateQuantityTemp");
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							accountLine.setEstimateQuantity(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
						  }	
				   decimalValue=getRequest().getParameter("estimateRateTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimateRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }
					   decimalValue=getRequest().getParameter("estimateExpenseTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstimateExpense(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
									  }
								  }
						   decimalValue=getRequest().getParameter("basisTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setBasis(decimalValue); 
										accountLineBasis=decimalValue;
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										  }
									  }
							String  dateValue="";					
																
								 try{
									 dateValue=getRequest().getParameter("estValueDateTemp");					 
									  if(dateValue!=null && (!(dateValue.equals("")))){
													 try{														
													  accountLine.setEstValueDate(CommonUtil.multiParse(dateValue));
												     }catch(Exception e){
												    	 e.printStackTrace();
												     }
											  } 
									 }catch(Exception e){
											logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									 }
						  
			if(contractType){
				
				   decimalValue=getRequest().getParameter("estimatePayableContractCurrencyTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimatePayableContractCurrency(decimalValue); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								  }
							  }
					   
  
  
						 String dateTemp="";
						 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
						 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						 try{
						 dateTemp=getRequest().getParameter("estimatePayableContractValueDateTemp");					 
						  if(dateTemp!=null && (!(dateTemp.equals("")))){
										 try{
										      Date date = sdfSource.parse(dateTemp);	
											  accountLine.setEstimatePayableContractValueDate(date); 
									     }catch(Exception e){
										      Date date = sdfDestination.parse(dateTemp);	
										      String dt1=sdfSource.format(date);
										      date = sdfSource.parse(dt1);
											  accountLine.setEstimatePayableContractValueDate(date); 
									     }
								  } 
						 }catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						 }
						  /*
 decimalValue=getRequest().getParameter("estimatePayableContractValueDateTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstimatePayableContractValueDate(decimalValue); 
							     }catch(Exception e){
										  
									  }
								  }*/
					   decimalValue=getRequest().getParameter("estimatePayableContractExchangeRateTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
									  }
								  }
					   decimalValue=getRequest().getParameter("estimatePayableContractRateTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstimatePayableContractRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);	  
									  }
								  }
					   decimalValue=getRequest().getParameter("estimatePayableContractRateAmmountTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstimatePayableContractRateAmmount(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);	  
									  }
								  }


						   decimalValue=getRequest().getParameter("estCurrencyTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstCurrency(decimalValue); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }
								 try{
								 dateTemp=getRequest().getParameter("estSellValueDateTemp");					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setEstSellValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setEstSellValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }						  
				/*			   decimalValue=getRequest().getParameter("estSellValueDateTemp");
								  if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstSellValueDate(decimalValue); 
									     }catch(Exception e){
												  
											  }
										  }*/
							   decimalValue=getRequest().getParameter("estExchangeRateTemp");
								  if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstExchangeRate(new BigDecimal(decimalValue)); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
											  }
										  }
							   decimalValue=getRequest().getParameter("estLocalRateTemp");
								  if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstLocalRate(new BigDecimal(decimalValue)); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
											  }
										  }
							   decimalValue=getRequest().getParameter("estLocalAmountTemp");
								  if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstLocalAmount(new BigDecimal(decimalValue));
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
											  }
										  }
						  
				}else{
					   decimalValue=getRequest().getParameter("estCurrencyTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstCurrency(decimalValue); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									  }
								  }
							 String dateTemp="";
							 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
							 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  
							 try{
								 dateTemp=getRequest().getParameter("estSellValueDateTemp");					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setEstSellValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setEstSellValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }						  
			/*			   decimalValue=getRequest().getParameter("estSellValueDateTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstSellValueDate(decimalValue); 
								     }catch(Exception e){
											  
										  }
									  }*/
						   decimalValue=getRequest().getParameter("estExchangeRateTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstExchangeRate(new BigDecimal(decimalValue)); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }
						   decimalValue=getRequest().getParameter("estLocalRateTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstLocalRate(new BigDecimal(decimalValue)); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
										  }
									  }
						   decimalValue=getRequest().getParameter("estLocalAmountTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstLocalAmount(new BigDecimal(decimalValue));
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }				
				}
  
  /*
			if((contractType)&&((accountLine.getEstLocalAmount()==null)||(accountLine.getEstLocalAmount().floatValue()==0))){
				if((chargeCode!=null)&&(!chargeCode.equalsIgnoreCase(""))&&(billing.getContract()!=null)&&(!billing.getContract().equalsIgnoreCase(""))){
					List al=chargesManager.findChargeCode(chargeCode,billing.getContract());
					if((al!=null)&&(al.get(0)!=null)&&(!al.isEmpty())){
						String str=al.get(0).toString();
						String strArr[]=str.split("#");
						if((strArr[10]!=null)&&(!strArr[10].trim().equalsIgnoreCase(""))){
							  BigDecimal exchangeRateBig=new BigDecimal("0");
						       double exchangeRate=0;
						       List eRate=new ArrayList();
						        Float f;
						        DecimalFormat df;
						        String revAmt="";
						       eRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,strArr[10]);
								 if((eRate!=null)&&(!eRate.isEmpty())&& eRate.get(0)!=null && (!(eRate.get(0).toString().equals(""))))
								 {
								 String exchangeRate1=eRate.get(0).toString();
								 exchangeRate=Double.parseDouble(exchangeRate1);
								 }else{
									 exchangeRate=1; 
								 }	
							   exchangeRateBig=new BigDecimal(exchangeRate);
							 	DecimalFormat df1 = new DecimalFormat("#.####");
							 	String revAmt1 = df1.format(exchangeRateBig.floatValue());
							   accountLine.setEstimatePayableContractValueDate(new Date());
					           accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(revAmt1));	
					           accountLine.setEstimatePayableContractCurrency(strArr[10]);
					  }
			      }
			  }
   }
			
			if((contractType)&&((accountLine.getEstLocalAmount()==null)||(accountLine.getEstLocalAmount().floatValue()==0))){
				String curr="";
				if((vendorCodeNew!=null)&&(!vendorCodeNew.equalsIgnoreCase(""))){
					curr=serviceOrderManager.getBillingCurrencyValue(vendorCodeNew, sessionCorpID);
					if(!curr.equalsIgnoreCase("")){
						  BigDecimal exchangeRateBig=new BigDecimal("0");
					       double exchangeRate=0;
					       List eRate=new ArrayList();
					        Float f;
					        DecimalFormat df;
					        String revAmt="";
					       eRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,curr);
							 if((eRate!=null)&&(!eRate.isEmpty())&& eRate.get(0)!=null && (!(eRate.get(0).toString().equals(""))))
							 {
							 String exchangeRate1=eRate.get(0).toString();
							 exchangeRate=Double.parseDouble(exchangeRate1);
							 }else{
								 exchangeRate=1; 
							 }	
						   exchangeRateBig=new BigDecimal(exchangeRate);
						 	DecimalFormat df1 = new DecimalFormat("#.####");
						 	String revAmt1 = df1.format(exchangeRateBig.floatValue());
						   accountLine.setEstValueDate(new Date());
				           accountLine.setEstExchangeRate(new BigDecimal(revAmt1));	
				           accountLine.setEstCurrency(curr);
					}else{
						curr=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
						  BigDecimal exchangeRateBig=new BigDecimal("0");
					       double exchangeRate=0;
					       List eRate=new ArrayList();
					        Float f;
					        DecimalFormat df;
					        String revAmt="";
					       eRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,curr);
							 if((eRate!=null)&&(!eRate.isEmpty())&& eRate.get(0)!=null && (!(eRate.get(0).toString().equals(""))))
							 {
							 String exchangeRate1=eRate.get(0).toString();
							 exchangeRate=Double.parseDouble(exchangeRate1);
							 }else{
								 exchangeRate=1; 
							 }	
						   exchangeRateBig=new BigDecimal(exchangeRate);
						 	DecimalFormat df1 = new DecimalFormat("#.####");
						 	String revAmt1 = df1.format(exchangeRateBig.floatValue());
						   accountLine.setEstValueDate(new Date());
				           accountLine.setEstExchangeRate(new BigDecimal(revAmt1));	
				           accountLine.setEstCurrency(curr);					
					}
				}else{
					curr=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
					  BigDecimal exchangeRateBig=new BigDecimal("0");
				       double exchangeRate=0;
				       List eRate=new ArrayList();
				        Float f;
				        DecimalFormat df;
				        String revAmt="";
				       eRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,curr);
						 if((eRate!=null)&&(!eRate.isEmpty())&& eRate.get(0)!=null && (!(eRate.get(0).toString().equals(""))))
						 {
						 String exchangeRate1=eRate.get(0).toString();
						 exchangeRate=Double.parseDouble(exchangeRate1);
						 }else{
							 exchangeRate=1; 
						 }	
					   exchangeRateBig=new BigDecimal(exchangeRate);
					 	DecimalFormat df1 = new DecimalFormat("#.####");
					 	String revAmt1 = df1.format(exchangeRateBig.floatValue());
					   accountLine.setEstValueDate(new Date());
			           accountLine.setEstExchangeRate(new BigDecimal(revAmt1));	
			           accountLine.setEstCurrency(curr);				
				}
   }		*/
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;			
	}
	@SkipValidation
	public String getAccountlineFieldPay(){
		try {
			accountLine=accountLineManager.get(aid);
			billing=billingManager.get(accountLine.getServiceOrderId());
			trackingStatus=trackingStatusManager.get(accountLine.getServiceOrderId());
			serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
			country = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
			List accountlineList1= accountLineManager.getAllAccountLineAgent(billing.getId()); 
			if(accountlineList1!=null && (!(accountlineList1.isEmpty()))){
				 Iterator accountlineIterator=accountlineList1.iterator();
				  while (accountlineIterator.hasNext()) {
					try{  
					Long  Accid = (Long) accountlineIterator.next();
							if(!Accid.toString().equalsIgnoreCase(aid.toString())){
								AccountLine accountLine1=accountLineManager.get(Accid);
								if(!((accountLine1.getRevisionExpense().floatValue()!=0)||(accountLine1.getRevisionRevenueAmount().floatValue()!=0)||(accountLine1.getActualRevenue().floatValue()!=0)||(accountLine1.getActualExpense().floatValue()!=0)||(accountLine1.getDistributionAmount().floatValue()!=0))){
										if(tempAccList.equalsIgnoreCase("")){
											tempAccList=Accid+"";
										}else{
											tempAccList=tempAccList+","+Accid+"";
										}
								}
							}
					}catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
				  }
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}
			String strValue="0.00";
			strValue=getRequest().getParameter("strValue");
				  if(strValue!=null && (!(strValue.equals("")))){
					  String str[]=strValue.split("~");
					  		try{
								accountLine.setBasis(str[0]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
					  		try{
								accountLine.setRecQuantity(new BigDecimal(str[1])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
					  		try{
								accountLine.setChargeCode(str[2]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setContract(str[3]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setVendorCode(str[4]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setCountry(str[5]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
						     
							 String dateTemp="";
							 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
							 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  
							 try{
								 dateTemp=str[6];					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }					     
							     
					  		try{
								accountLine.setExchangeRate(new Double(str[7])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }		
					  		try{
								accountLine.setLocalAmount(new BigDecimal(str[8])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setActualExpense(new BigDecimal(str[9])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setActualDiscount(new BigDecimal(str[10])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }					     
					     
						     
						     if(contractType){
								     try{
											accountLine.setPayableContractCurrency(str[11]); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }
									     
									  
										 try{
											 dateTemp=str[12];					 
											  if(dateTemp!=null && (!(dateTemp.equals("")))){
															 try{
															      Date date = sdfSource.parse(dateTemp);	
																  accountLine.setPayableContractValueDate(date); 
														     }catch(Exception e){
															      Date date = sdfDestination.parse(dateTemp);	
															      String dt1=sdfSource.format(date);
															      date = sdfSource.parse(dt1);
																  accountLine.setPayableContractValueDate(date); 
														     }
													  } 
											 }catch(Exception e){
													logger.error("Exception Occour: "+ e.getStackTrace()[0]);
											 }					     
						     
								  		try{
											accountLine.setPayableContractExchangeRate(new BigDecimal(str[13])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }		
								  		try{
											accountLine.setPayableContractRateAmmount(new BigDecimal(str[14])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }					     
						    
						       }
					     
					     
						  }
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}	
		
		
		return SUCCESS;			
	}
	
	@SkipValidation
	public String getAccountlineFieldRevNew(){
		try {
			accountLine=accountLineManager.get(aid);
			billing=billingManager.get(accountLine.getServiceOrderId());
			trackingStatus=trackingStatusManager.get(accountLine.getServiceOrderId());
			serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
			country = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
			List accountlineList1= accountLineManager.getAllAccountLineAgent(billing.getId()); 
			if(accountlineList1!=null && (!(accountlineList1.isEmpty()))){
				 Iterator accountlineIterator=accountlineList1.iterator();
				  while (accountlineIterator.hasNext()) {
					try{  
					Long  Accid = (Long) accountlineIterator.next();
							if(!Accid.toString().equalsIgnoreCase(aid.toString())){
								AccountLine accountLine1=accountLineManager.get(Accid);
								if(!((accountLine1.getRevisionExpense().floatValue()!=0)||(accountLine1.getRevisionRevenueAmount().floatValue()!=0)||(accountLine1.getActualRevenue().floatValue()!=0)||(accountLine1.getActualExpense().floatValue()!=0)||(accountLine1.getDistributionAmount().floatValue()!=0))){
										if(tempAccList.equalsIgnoreCase("")){
											tempAccList=Accid+"";
										}else{
											tempAccList=tempAccList+","+Accid+"";
										}
								}
							}
					}catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
				  }
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}
			String strValue="0.00";
			strValue=getRequest().getParameter("strValue");
				  if(strValue!=null && (!(strValue.equals("")))){
					  String str[]=strValue.split("~");
					  		try{
								accountLine.setBasis(str[0]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
						     if(contractType){
							  		try{
										accountLine.setRevisionSellQuantity(new BigDecimal(str[1])); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
									 }
						     }else{
							  		try{
										accountLine.setRevisionQuantity(new BigDecimal(str[1])); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
									 }					    	 
						     }
					  		try{
								accountLine.setChargeCode(str[2]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setContract(str[3]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setVendorCode(str[4]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setRevisionSellCurrency(str[5]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
						     
							 String dateTemp="";
							 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
							 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  
							 try{
								 dateTemp=str[6];					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setRevisionSellValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setRevisionSellValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }					     
						     
					  		try{
								accountLine.setRevisionSellLocalRate(new BigDecimal(str[7])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setRevisionSellExchangeRate(new BigDecimal(str[8])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }		
					  		try{
								accountLine.setRevisionSellLocalAmount(new BigDecimal(str[9])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setRevisionRevenueAmount(new BigDecimal(str[10])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setRevisionDiscount(new BigDecimal(str[11])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }					     
						  		try{
									accountLine.setRevisionSellRate(new BigDecimal(str[12])); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								 }					     
						     
						     if(contractType){
								     try{
											accountLine.setRevisionContractCurrency(str[13]); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }
									     
									  
										 try{
											 dateTemp=str[14];					 
											  if(dateTemp!=null && (!(dateTemp.equals("")))){
															 try{
															      Date date = sdfSource.parse(dateTemp);	
																  accountLine.setRevisionContractValueDate(date); 
														     }catch(Exception e){
															      Date date = sdfDestination.parse(dateTemp);	
															      String dt1=sdfSource.format(date);
															      date = sdfSource.parse(dt1);
																  accountLine.setRevisionContractValueDate(date); 
														     }
													  } 
											 }catch(Exception e){
													logger.error("Exception Occour: "+ e.getStackTrace()[0]);
											 }					     
									     
								  		try{
											accountLine.setRevisionContractRate(new BigDecimal(str[15])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }						     
								  		try{
											accountLine.setRevisionContractExchangeRate(new BigDecimal(str[16])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }		
								  		try{
											accountLine.setRevisionContractRateAmmount(new BigDecimal(str[17])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }					     
						    
						       }
					     
					     
						  }
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}	
		
		
		return SUCCESS;			
	}
	
	@SkipValidation
	public String getAccountlineFieldRev(){
		try {
			accountLine=accountLineManager.get(aid);
			billing=billingManager.get(accountLine.getServiceOrderId());
			trackingStatus=trackingStatusManager.get(accountLine.getServiceOrderId());
			serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
			country = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
			List accountlineList1= accountLineManager.getAllAccountLineAgent(billing.getId()); 
			if(accountlineList1!=null && (!(accountlineList1.isEmpty()))){
				 Iterator accountlineIterator=accountlineList1.iterator();
				  while (accountlineIterator.hasNext()) {
					try{  
					Long  Accid = (Long) accountlineIterator.next();
							if(!Accid.toString().equalsIgnoreCase(aid.toString())){
								AccountLine accountLine1=accountLineManager.get(Accid);
								if(!((accountLine1.getRevisionExpense().floatValue()!=0)||(accountLine1.getRevisionRevenueAmount().floatValue()!=0)||(accountLine1.getActualRevenue().floatValue()!=0)||(accountLine1.getActualExpense().floatValue()!=0)||(accountLine1.getDistributionAmount().floatValue()!=0))){
										if(tempAccList.equalsIgnoreCase("")){
											tempAccList=Accid+"";
										}else{
											tempAccList=tempAccList+","+Accid+"";
										}
								}
							}
					}catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
				  }
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}
			String strValue="0.00";
			strValue=getRequest().getParameter("strValue");
				  if(strValue!=null && (!(strValue.equals("")))){
					  String str[]=strValue.split("~");
					  		try{
								accountLine.setBasis(str[0]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
					  		try{
								accountLine.setRevisionQuantity(new BigDecimal(str[1])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
					  		try{
								accountLine.setChargeCode(str[2]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setContract(str[3]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setVendorCode(str[4]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setRevisionCurrency(str[5]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
						     
							 String dateTemp="";
							 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
							 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  
							 try{
								 dateTemp=str[6];					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setRevisionValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setRevisionValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }					     
						     
					  		try{
								accountLine.setRevisionLocalRate(new BigDecimal(str[7])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setRevisionExchangeRate(new BigDecimal(str[8])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }		
					  		try{
								accountLine.setRevisionLocalAmount(new BigDecimal(str[9])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setRevisionExpense(new BigDecimal(str[10])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setRevisionDiscount(new BigDecimal(str[11])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }					     
						  		try{
									accountLine.setRevisionRate(new BigDecimal(str[12])); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								 }					     
						     
						     if(contractType){
								     try{
											accountLine.setRevisionPayableContractCurrency(str[13]); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }
									     
									  
										 try{
											 dateTemp=str[14];					 
											  if(dateTemp!=null && (!(dateTemp.equals("")))){
															 try{
															      Date date = sdfSource.parse(dateTemp);	
																  accountLine.setRevisionPayableContractValueDate(date); 
														     }catch(Exception e){
															      Date date = sdfDestination.parse(dateTemp);	
															      String dt1=sdfSource.format(date);
															      date = sdfSource.parse(dt1);
																  accountLine.setRevisionPayableContractValueDate(date); 
														     }
													  } 
											 }catch(Exception e){
													logger.error("Exception Occour: "+ e.getStackTrace()[0]);
											 }					     
									     
								  		try{
											accountLine.setRevisionPayableContractRate(new BigDecimal(str[15])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }						     
								  		try{
											accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(str[16])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }		
								  		try{
											accountLine.setRevisionPayableContractRateAmmount(new BigDecimal(str[17])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }					     
						    
						       }
					     
					     
						  }
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}	
		
		
		return SUCCESS;			
	}
	@SkipValidation
	public String getAccountlineFieldRec(){
		try {
			accountLine=accountLineManager.get(aid);
			billing=billingManager.get(accountLine.getServiceOrderId());
			trackingStatus=trackingStatusManager.get(accountLine.getServiceOrderId());
			serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
			country = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
			multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString();
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			if(company!=null){
			if(company.getAccountLineNonEditable()!=null && company.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
				 accNonEditFlag =true;
			 }
			}
			List accountlineList1= accountLineManager.getAllAccountLineAgent(billing.getId()); 
			try{
				  if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup())	{
					   String networkSynchedId=	accountLineManager.getNetworkSynchedId(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
				      	if(!(networkSynchedId.equals(""))){
				      		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
				      		Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
				      		boolean accNonEditable=false;
							 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
								 accNonEditable =true;
							 }
							 if(accNonEditable){
								 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
					      			 utsiRecAccDateFlag=true; 
					      		}
								 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
					      			 utsiPayAccDateFlag=true;
					      		}
							 }else{
				      		if(synchedAccountLine.getRecAccDate()!=null && accountLine.getRecAccDate()== null){
				      			 utsiRecAccDateFlag=true; 
				      		}
				      		if(synchedAccountLine.getPayAccDate()!=null && accountLine.getRecAccDate()== null){
				      			 utsiPayAccDateFlag=true;
				      		}
							 }
				      		
				      	}  
				  }
					
				}catch(Exception e){
					
				}
			
			if(accountlineList1!=null && (!(accountlineList1.isEmpty()))){
				 Iterator accountlineIterator=accountlineList1.iterator();
				  while (accountlineIterator.hasNext()) {
					try{  
					Long  Accid = (Long) accountlineIterator.next();
							if(!Accid.toString().equalsIgnoreCase(aid.toString())){
								AccountLine accountLine1=accountLineManager.get(Accid);
								if(!((accountLine1.getRevisionExpense().floatValue()!=0)||(accountLine1.getRevisionRevenueAmount().floatValue()!=0)||(accountLine1.getActualRevenue().floatValue()!=0)||(accountLine1.getActualExpense().floatValue()!=0)||(accountLine1.getDistributionAmount().floatValue()!=0))){
										if(tempAccList.equalsIgnoreCase("")){
											tempAccList=Accid+"";
										}else{
											tempAccList=tempAccList+","+Accid+"";
										}
								}
							}
					}catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
				  }
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}
			String strValue="0.00";
			strValue=getRequest().getParameter("strValue");
				  if(strValue!=null && (!(strValue.equals("")))){
					  String str[]=strValue.split("~");
					  		try{
								accountLine.setBasis(str[0]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
					  		try{
								accountLine.setRecQuantity(new BigDecimal(str[1])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
					  		try{
								accountLine.setChargeCode(str[2]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setContract(str[3]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setVendorCode(str[4]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setRecRateCurrency(str[5]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
						     
							 String dateTemp="";
							 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
							 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  
							 try{
								 dateTemp=str[6];					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setRacValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setRacValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }					     
						     
					  		try{
								accountLine.setRecCurrencyRate(new BigDecimal(str[7])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setRecRateExchange(new BigDecimal(str[8])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }		
					  		try{
								accountLine.setActualRevenueForeign(new BigDecimal(str[9])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setActualRevenue(new BigDecimal(str[10])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setActualDiscount(new BigDecimal(str[11])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }					     
						  		try{
									accountLine.setRecRate(new BigDecimal(str[12])); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								 }					     
						     
						     if(contractType){
								     try{
											accountLine.setContractCurrency(str[13]); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }
									     
									  
										 try{
											 dateTemp=str[14];					 
											  if(dateTemp!=null && (!(dateTemp.equals("")))){
															 try{
															      Date date = sdfSource.parse(dateTemp);	
																  accountLine.setContractValueDate(date); 
														     }catch(Exception e){
															      Date date = sdfDestination.parse(dateTemp);	
															      String dt1=sdfSource.format(date);
															      date = sdfSource.parse(dt1);
																  accountLine.setContractValueDate(date); 
														     }
													  } 
											 }catch(Exception e){
													logger.error("Exception Occour: "+ e.getStackTrace()[0]);
											 }					     
									     
								  		try{
											accountLine.setContractRate(new BigDecimal(str[15])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }						     
								  		try{
											accountLine.setContractExchangeRate(new BigDecimal(str[16])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }		
								  		try{
											accountLine.setContractRateAmmount(new BigDecimal(str[17])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }					     
						    
						       }
					     
					     
						  }
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}	
		
		
		return SUCCESS;			
	}
	private String tempAccList;
	@SkipValidation
	public String getAccountlineFieldNew(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			accountLineBasis=accountLineBasis;
			accountLineEstimateQuantity=accountLineEstimateQuantity;
			accountLine=accountLineManager.get(aid);
			serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
			billing=billingManager.get(accountLine.getServiceOrderId());
			trackingStatus=trackingStatusManager.get(accountLine.getServiceOrderId());
			tempAccList="";
			List accountlineList1= accountLineManager.getAllAccountLineAgent(billing.getId()); 
			if(accountlineList1!=null && (!(accountlineList1.isEmpty()))){
				 Iterator accountlineIterator=accountlineList1.iterator();
				  while (accountlineIterator.hasNext()) {
					try{  
					Long  Accid = (Long) accountlineIterator.next();
							if(!Accid.toString().equalsIgnoreCase(aid.toString())){
								AccountLine accountLine1=accountLineManager.get(Accid);
								if(!((accountLine1.getRevisionExpense().floatValue()!=0)||(accountLine1.getRevisionRevenueAmount().floatValue()!=0)||(accountLine1.getActualRevenue().floatValue()!=0)||(accountLine1.getActualExpense().floatValue()!=0)||(accountLine1.getDistributionAmount().floatValue()!=0))){
										if(tempAccList.equalsIgnoreCase("")){
											tempAccList=Accid+"";
										}else{
											tempAccList=tempAccList+","+Accid+"";
										}
								}
							}
					}catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
				  }
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}  
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			    }
			    if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			      	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
			      }
			String decimalValue="0.00";
			
			   decimalValue=getRequest().getParameter("estimateQuantityTemp");
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							accountLine.setEstimateQuantity(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							  }
						  }
				   decimalValue=getRequest().getParameter("estimateSellQuantity");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimateSellQuantity(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								  }
							  }			  
				  
				   decimalValue=getRequest().getParameter("estimateSellRateTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimateSellRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								  }
							  }
					   decimalValue=getRequest().getParameter("estimateRevenueAmountTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstimateRevenueAmount(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
									  }
								  }		
						   decimalValue=getRequest().getParameter("basisTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setBasis(decimalValue); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }						  
			if(contractType){
			   decimalValue=getRequest().getParameter("estimateContractCurrencyTemp");
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							accountLine.setEstimateContractCurrency(decimalValue); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
						  }
					 String dateTemp="";
					 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
					 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				  
					 try{
						 dateTemp=getRequest().getParameter("estimateContractValueDateTemp");					 
						  if(dateTemp!=null && (!(dateTemp.equals("")))){
										 try{
										      Date date = sdfSource.parse(dateTemp);	
											  accountLine.setEstimateContractValueDate(date); 
									     }catch(Exception e){
										      Date date = sdfDestination.parse(dateTemp);	
										      String dt1=sdfSource.format(date);
										      date = sdfSource.parse(dt1);
											  accountLine.setEstimateContractValueDate(date); 
									     }
								  } 
						 }catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						 }				  
						/*			   decimalValue=getRequest().getParameter("estimateContractValueDateTemp");
										  if(decimalValue!=null && (!(decimalValue.equals("")))){
												 try{
													accountLine.setEstimateContractValueDate(decimalValue); 
											     }catch(Exception e){
														  
													  }
												  }*/
				   decimalValue=getRequest().getParameter("estimateContractExchangeRateTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimateContractExchangeRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }
				   decimalValue=getRequest().getParameter("estimateContractRateTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimateContractRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								  }
							  }
				   decimalValue=getRequest().getParameter("estimateContractRateAmmountTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimateContractRateAmmount(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }


					   decimalValue=getRequest().getParameter("estSellCurrencyTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellCurrency(decimalValue); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);	  
									  }
								  }
							 try{
								 dateTemp=getRequest().getParameter("estSellValueDateTemp");					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setEstSellValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setEstSellValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }						  
								/*			   decimalValue=getRequest().getParameter("estSellValueDateTemp");
												  if(decimalValue!=null && (!(decimalValue.equals("")))){
														 try{
															accountLine.setEstSellValueDate(decimalValue); 
													     }catch(Exception e){
																  
															  }
														  }*/
						   decimalValue=getRequest().getParameter("estSellExchangeRateTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstSellExchangeRate(new BigDecimal(decimalValue)); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										  }
									  }
						   decimalValue=getRequest().getParameter("estSellLocalRateTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstSellLocalRate(new BigDecimal(decimalValue)); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
										  }
									  }
						   decimalValue=getRequest().getParameter("estSellLocalAmountTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstSellLocalAmount(new BigDecimal(decimalValue));
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }
					  
			}else{
				   decimalValue=getRequest().getParameter("estSellCurrencyTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstSellCurrency(decimalValue); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								  }
							  }
						 String dateTemp="";
						 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
						 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						 try{
							 dateTemp=getRequest().getParameter("estSellValueDateTemp");					 
							  if(dateTemp!=null && (!(dateTemp.equals("")))){
											 try{
											      Date date = sdfSource.parse(dateTemp);	
												  accountLine.setEstSellValueDate(date); 
										     }catch(Exception e){
											      Date date = sdfDestination.parse(dateTemp);	
											      String dt1=sdfSource.format(date);
											      date = sdfSource.parse(dt1);
												  accountLine.setEstSellValueDate(date); 
										     }
									  } 
							 }catch(Exception e){
									logger.error("Exception Occour: "+ e.getStackTrace()[0]);
							 }					  
					/*			   decimalValue=getRequest().getParameter("estSellValueDateTemp");
									  if(decimalValue!=null && (!(decimalValue.equals("")))){
											 try{
												accountLine.setEstSellValueDate(decimalValue); 
										     }catch(Exception e){
													  
												  }
											  }*/
					   decimalValue=getRequest().getParameter("estSellExchangeRateTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellExchangeRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
										  
									  }
								  }
					   decimalValue=getRequest().getParameter("estSellLocalRateTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellLocalRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
									  }
								  }
					   decimalValue=getRequest().getParameter("estSellLocalAmountTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellLocalAmount(new BigDecimal(decimalValue));
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									  }
								  }
				
			}
					  
/*	if((contractType)&&((accountLine.getEstSellLocalAmount()==null)||(accountLine.getEstSellLocalAmount().floatValue()==0))){
				if((chargeCode!=null)&&(!chargeCode.equalsIgnoreCase(""))&&(billing.getContract()!=null)&&(!billing.getContract().equalsIgnoreCase(""))){
					List al=chargesManager.findChargeCode(chargeCode,billing.getContract());
					if((al!=null)&&(al.get(0)!=null)&&(!al.isEmpty())){
						String str=al.get(0).toString();
						String strArr[]=str.split("#");
						if((strArr[9]!=null)&&(!strArr[9].trim().equalsIgnoreCase(""))){
							  BigDecimal exchangeRateBig=new BigDecimal("0");
						       double exchangeRate=0;
						       List eRate=new ArrayList();
						        Float f;
						        DecimalFormat df;
						        String revAmt="";
						       eRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,strArr[9]);
								 if((eRate!=null)&&(!eRate.isEmpty())&& eRate.get(0)!=null && (!(eRate.get(0).toString().equals(""))))
								 {
								 String exchangeRate1=eRate.get(0).toString();
								 exchangeRate=Double.parseDouble(exchangeRate1);
								 }else{
									 exchangeRate=1; 
								 }	
							   exchangeRateBig=new BigDecimal(exchangeRate);
							 	DecimalFormat df1 = new DecimalFormat("#.####");
							 	String revAmt1 = df1.format(exchangeRateBig.floatValue());
							   accountLine.setEstimateContractValueDate(new Date());
					           accountLine.setEstimateContractExchangeRate(new BigDecimal(revAmt1));	
					           accountLine.setEstimateContractCurrency(strArr[9]);
					  }
			      }
			  }
   }		
			if((contractType)&&((accountLine.getEstSellLocalAmount()==null)||(accountLine.getEstSellLocalAmount().floatValue()==0))){
				String curr="";
				if((billing.getBillToCode()!=null)&&(!billing.getBillToCode().equalsIgnoreCase(""))){
					curr=serviceOrderManager.getBillingCurrencyValue(billing.getBillToCode(), sessionCorpID);
					if(!curr.equalsIgnoreCase("")){
						  BigDecimal exchangeRateBig=new BigDecimal("0");
					       double exchangeRate=0;
					       List eRate=new ArrayList();
					        Float f;
					        DecimalFormat df;
					        String revAmt="";
					       eRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,curr);
							 if((eRate!=null)&&(!eRate.isEmpty())&& eRate.get(0)!=null && (!(eRate.get(0).toString().equals(""))))
							 {
							 String exchangeRate1=eRate.get(0).toString();
							 exchangeRate=Double.parseDouble(exchangeRate1);
							 }else{
								 exchangeRate=1; 
							 }	
						   exchangeRateBig=new BigDecimal(exchangeRate);
						 	DecimalFormat df1 = new DecimalFormat("#.####");
						 	String revAmt1 = df1.format(exchangeRateBig.floatValue());
						   accountLine.setEstSellValueDate(new Date());
				           accountLine.setEstSellExchangeRate(new BigDecimal(revAmt1));	
				           accountLine.setEstSellCurrency(curr);
					}else{
						curr=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
						  BigDecimal exchangeRateBig=new BigDecimal("0");
					       double exchangeRate=0;
					       List eRate=new ArrayList();
					        Float f;
					        DecimalFormat df;
					        String revAmt="";
					       eRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,curr);
							 if((eRate!=null)&&(!eRate.isEmpty())&& eRate.get(0)!=null && (!(eRate.get(0).toString().equals(""))))
							 {
							 String exchangeRate1=eRate.get(0).toString();
							 exchangeRate=Double.parseDouble(exchangeRate1);
							 }else{
								 exchangeRate=1; 
							 }	
						   exchangeRateBig=new BigDecimal(exchangeRate);
						 	DecimalFormat df1 = new DecimalFormat("#.####");
						 	String revAmt1 = df1.format(exchangeRateBig.floatValue());
						   accountLine.setEstSellValueDate(new Date());
				           accountLine.setEstSellExchangeRate(new BigDecimal(revAmt1));	
				           accountLine.setEstSellCurrency(curr);					
					}
				}else{
					curr=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
					  BigDecimal exchangeRateBig=new BigDecimal("0");
				       double exchangeRate=0;
				       List eRate=new ArrayList();
				        Float f;
				        DecimalFormat df;
				        String revAmt="";
				       eRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,curr);
						 if((eRate!=null)&&(!eRate.isEmpty())&& eRate.get(0)!=null && (!(eRate.get(0).toString().equals(""))))
						 {
						 String exchangeRate1=eRate.get(0).toString();
						 exchangeRate=Double.parseDouble(exchangeRate1);
						 }else{
							 exchangeRate=1; 
						 }	
					   exchangeRateBig=new BigDecimal(exchangeRate);
					 	DecimalFormat df1 = new DecimalFormat("#.####");
					 	String revAmt1 = df1.format(exchangeRateBig.floatValue());
					   accountLine.setEstSellValueDate(new Date());
			           accountLine.setEstSellExchangeRate(new BigDecimal(revAmt1));	
			           accountLine.setEstSellCurrency(curr);				
				}
   }	*/
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;			
	}
	@SkipValidation
	public String updateAccEstCurrency(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			accountLine=accountLineManager.get(aid);
			billing=billingManager.get(accountLine.getServiceOrderId());
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}	
			try
			{
				if(contractType){
					
					 SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
			         Date du = new Date();
			         du = df.parse(estimatePayableContractValueDate);
			         df = new SimpleDateFormat("yyyy-MM-dd");
			         estimatePayableContractValueDate = df.format(du);
				}
			}catch (Exception e)
			{
			    e.printStackTrace();
			 } 		
			try
			{
				 SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
			     Date du = new Date();
			     du = df.parse(estValueDate);
			     df = new SimpleDateFormat("yyyy-MM-dd");
			     estValueDate = df.format(du);
				
			} 
 
			catch (ParseException e)
      {
			  e.printStackTrace();
      } 
			if(contractType){
				int i	= accountLineManager.updateAccEstContractCurrency(aid,estCurrency,estValueDate,estExchangeRate,estLocalAmount,estLocalRate,estimatePayableContractCurrency,estimatePayableContractValueDate,estimatePayableContractExchangeRate,estimatePayableContractRateAmmount,estimatePayableContractRate);																																									
			}else{
				int i	= accountLineManager.updateAccEstCurrency(aid,estCurrency,estValueDate,estExchangeRate,estLocalAmount,estLocalRate);
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;	
	}

	@SkipValidation
	public String updateAccEstSellCurrency(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			accountLine=accountLineManager.get(aid);
			billing=billingManager.get(accountLine.getServiceOrderId());
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}		
			try
			{
				if(contractType){
					
					 SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
			         Date du = new Date();
			         du = df.parse(estimateContractValueDate);
			         df = new SimpleDateFormat("yyyy-MM-dd");
			         estimateContractValueDate = df.format(du);
				}
			}catch (Exception e)
			{
			    e.printStackTrace();
			 } 

			try
			{
				 SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
			     Date du = new Date();
			     du = df.parse(estSellValueDate);
			     df = new SimpleDateFormat("yyyy-MM-dd");
			     estSellValueDate = df.format(du);
				
			} 
 
			catch (ParseException e)
      {
			  e.printStackTrace();
      } 
			if(contractType){
					
				    int i	= accountLineManager.updateAccEstContractSellCurrency(aid,estSellCurrency,estSellValueDate,estSellExchangeRate,estSellLocalAmount,estSellLocalRate,estimateContractCurrency,estimateContractValueDate,estimateContractExchangeRate,estimateContractRateAmmount,estimateContractRate);
			}else{
					int i	= accountLineManager.updateAccEstSellCurrency(aid,estSellCurrency,estSellValueDate,estSellExchangeRate,estSellLocalAmount,estSellLocalRate);
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}

		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;	
	}
	private String companyDivisionSO;
	private String baseCurrency;
	private String payVatDescrList;
	private String recVatDescrList;
	public void getPricingHelper(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(id);
			try {
				billing =billingManager.get(id);
				trackingStatus=trackingStatusManager.get(id);
				miscellaneous=miscellaneousManager.get(id);
			} catch (Exception e) {
				e.printStackTrace();
			}
			customerFile = serviceOrder.getCustomerFile();
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			if(company!=null){
				if(company.getEstimateVATFlag()!=null){
						estVatFlag=company.getEstimateVATFlag();
				}
				if(company.getUTSI()!=null){
					 networkAgent=company.getUTSI();
				}else{
					networkAgent=false;
				}
			}
			List <SystemDefault> systemDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			if (!(systemDefaultDetail == null || systemDefaultDetail.isEmpty())) {
				companyDivisionAcctgCodeUnique="";
				systemDefaultVatCalculationNew="N";
				for (SystemDefault systemDefault1 : systemDefaultDetail) {
					systemDefault=systemDefault1;
					if(systemDefault1.getCompanyDivisionAcctgCodeUnique()!=null){
						companyDivisionAcctgCodeUnique=systemDefault1.getCompanyDivisionAcctgCodeUnique();	
					}
					recVatDescrList=systemDefault1.getReceivableVat();
					payVatDescrList=systemDefault1.getPayableVat();
					if(systemDefault1.getVatCalculation()){
						systemDefaultVatCalculationNew="Y";	
					}

				}
			}
			baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
			baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID);
			multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString();
			euVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "EUVAT");
			payVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "PAYVATDESC");
			estVatList=new LinkedHashMap<String, String>();
		    estVatList.put("", "");
		    estVatList.putAll(refMasterManager.findByParameterWithoutParent(sessionCorpID, "EUVAT"));
		    payVatList=new LinkedHashMap<String, String>();
		    payVatList.put("", "");
		    payVatList.putAll(refMasterManager.findByParameterWithoutParent(sessionCorpID, "PAYVATDESC"));
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
	}
	
	@SkipValidation
	public String findDefaultPricingLineAjax(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(id);
			customerFile = serviceOrder.getCustomerFile();
			quotationContract=serviceOrder.getContract();
			quotationBillToCode=customerFile.getBillToCode(); 
			miscellaneous = miscellaneousManager.get(id);
			if(jobtypeSO.trim().equalsIgnoreCase("RLO"))
			{
				String serviceDoc=reloServiceType.trim().toString();
				if(serviceDoc.equals("")||serviceDoc.equals(","))
			  	  {
			  	  }
			  	 else
			  	  {
			  		serviceDoc=serviceDoc.trim();
			  	    if(serviceDoc.indexOf(",")==0)
			  		 {
			  	    	serviceDoc=serviceDoc.substring(1);
			  		 }
			  		if(serviceDoc.lastIndexOf(",")==serviceDoc.length()-1)
			  		 {
			  			serviceDoc=serviceDoc.substring(0, serviceDoc.length()-1);
			  		 } 
			  	  } 
				serviceDoc=serviceDoc.replaceAll("#", ","); 
				serviceTypeSO = reloServiceType;
			}
			List defaultList = defaultAccountLineManager.getDefaultAccountList(jobtypeSO, routingSO, modeSO,quotationContract,quotationBillToCode,packingModeSO,commoditySO,serviceTypeSO,companyDivisionSO,serviceOrder.getOriginCountryCode(),serviceOrder.getDestinationCountryCode(),miscellaneous.getEquipment(),serviceOrder.getOriginCity(),serviceOrder.getDestinationCity());
			if(defaultList != null && defaultList.size() > 0 && !defaultList.isEmpty() && defaultList.get(0) != null && !"".equals(defaultList.get(0).toString().trim())){
				returnAjaxStringValue = "YES";
			}else {
				returnAjaxStringValue = "NO";
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String rollUpInvoiceFlag;
	@SkipValidation
	public String addPricingDefaultLineAjax(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String user = getRequest().getRemoteUser();
			getPricingHelper();
			
			costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
			
			quotationContract=serviceOrder.getContract();
			quotationBillToCode=customerFile.getBillToCode();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			if(jobtypeSO.trim().equalsIgnoreCase("RLO"))
			{
				String serviceDoc=reloServiceType.trim().toString();
				if(serviceDoc.equals("")||serviceDoc.equals(","))
			  	  {
			  	  }
			  	 else
			  	  {
			  		serviceDoc=serviceDoc.trim();
			  	    if(serviceDoc.indexOf(",")==0)
			  		 {
			  	    	serviceDoc=serviceDoc.substring(1);
			  		 }
			  		if(serviceDoc.lastIndexOf(",")==serviceDoc.length()-1)
			  		 {
			  			serviceDoc=serviceDoc.substring(0, serviceDoc.length()-1);
			  		 } 
			  	  } 
				serviceDoc=serviceDoc.replaceAll("#", ","); 
				serviceTypeSO = reloServiceType;
			}
			List defaultList = defaultAccountLineManager.getDefaultAccountList(jobtypeSO, routingSO, modeSO,quotationContract,quotationBillToCode,packingModeSO,commoditySO,serviceTypeSO,companyDivisionSO,serviceOrder.getOriginCountryCode(),serviceOrder.getDestinationCountryCode(),miscellaneous.getEquipment(),serviceOrder.getOriginCity(),serviceOrder.getDestinationCity());
			
			Iterator it = defaultList.iterator();
			while(it.hasNext()){
				DefaultAccountLine defaultAccountLine = (DefaultAccountLine)it.next();
				AccountLine accountLine = new AccountLine();
				if(defaultAccountLine.getAmount()!=null && (!(defaultAccountLine.getAmount().toString().equals("")))){
					accountLine.setEstimateExpense(new BigDecimal(defaultAccountLine.getAmount()));
				}else{
					accountLine.setEstimateExpense(new BigDecimal(0.00));	
				}
				if(defaultAccountLine.getRate()!=null && (!(defaultAccountLine.getRate().toString().equals("")))){
					accountLine.setEstimateRate(new BigDecimal(defaultAccountLine.getRate()));
					accountLine.setEstLocalRate(new BigDecimal(defaultAccountLine.getRate()));
				}else{
					accountLine.setEstimateRate(new BigDecimal(0.00));
					accountLine.setEstLocalRate(new BigDecimal(0.0));
				}
				accountLine.setBasis(defaultAccountLine.getBasis());
				accountLine.setCategory(defaultAccountLine.getCategories());
				boolean contractType=false; 
				if(quotationContract!=null && (!(quotationContract.toString().equals("")))){
				String	contractTypeValue=	accountLineManager.checkContractType(quotationContract,sessionCorpID);
					if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
						contractType=true;	
					} 
				}
				if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals("")))){
					accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity()));
					if(contractType){
						accountLine.setEstimateSellQuantity(new BigDecimal(defaultAccountLine.getQuantity()));	
					}
				}else{
					accountLine.setEstimateQuantity(new BigDecimal(0.00));
					if(contractType){
						accountLine.setEstimateSellQuantity(new BigDecimal(0.00));	
					}
				}
				if(defaultAccountLine.getEstimatedRevenue()!=null && (!(defaultAccountLine.getEstimatedRevenue().toString().equals("")))){
					accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
				}else{
					accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));	
				}
				if(defaultAccountLine.getEstimatedRevenue()!=null && defaultAccountLine.getEstimatedRevenue().doubleValue()>0.00){
					accountLine.setDisplayOnQuote(true);
				}else{
					accountLine.setDisplayOnQuote(false);
				}
				if(defaultAccountLine.getMarkUp()!=null && (!( defaultAccountLine.getMarkUp().toString().equals("")))){
					accountLine.setEstimatePassPercentage(defaultAccountLine.getMarkUp());
				}else{
					accountLine.setEstimatePassPercentage(0);	
				}
				if(defaultAccountLine.getSellRate()!=null && (!(defaultAccountLine.getSellRate().toString().equals("")))){
					accountLine.setEstimateSellRate(defaultAccountLine.getSellRate());
					accountLine.setEstSellLocalRate(defaultAccountLine.getSellRate());
				}else {
					accountLine.setEstimateSellRate(new BigDecimal(0.00));
					accountLine.setEstSellLocalRate(new BigDecimal(0.00));
				}
				accountLine.setChargeCode(defaultAccountLine.getChargeCode());
				try {
					String ss=chargesManager.findPopulateCostElementData(defaultAccountLine.getChargeCode(), quotationContract,sessionCorpID);
					if(!ss.equalsIgnoreCase("")){
						accountLine.setAccountLineCostElement(ss.split("~")[0]);
						accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}

				///Code for AccountLine division 
				if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
					String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
					try{  
						String roles[] = agentRoleValue.split("~");
					  if(roles[3].toString().equals("hauler")){
						   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
							   accountLine.setDivision("01");
						   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
							   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
								   accountLine.setDivision("01");
							   }else{ 
								   String divisionTemp="";
										if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
										divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
										}
										if(!divisionTemp.equalsIgnoreCase("")){
											accountLine.setDivision(divisionTemp);
										}else{
											accountLine.setDivision(null);
										}
							     
							   }
						   }else{
							   String divisionTemp="";
									if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
									}
									if(!divisionTemp.equalsIgnoreCase("")){
										accountLine.setDivision(divisionTemp);
									}else{
										accountLine.setDivision(null);
									}
						   }
					  }else{
						  String divisionTemp="";
								if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
								divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
								}
								if(!divisionTemp.equalsIgnoreCase("")){
									accountLine.setDivision(divisionTemp);
								}else{
									accountLine.setDivision(null);
								}
					  }
					}catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				///Code for AccountLine division 			
				try{
					List<Charges> chargeList = chargesManager.findChargesList(defaultAccountLine.getChargeCode(),quotationContract,sessionCorpID);
					for (Charges charge : chargeList) {
						if(defaultAccountLine.getDescription()!=null && (!(defaultAccountLine.getDescription().toString().trim().equals("")))){
							accountLine.setDescription(defaultAccountLine.getDescription());
							accountLine.setNote(defaultAccountLine.getDescription());
							accountLine.setQuoteDescription(defaultAccountLine.getDescription());	
						}else{
						accountLine.setDescription(charge.getDescription());
						accountLine.setNote(charge.getDescription());
						accountLine.setQuoteDescription(charge.getDescription());
						}
						accountLine.setVATExclude(charge.getVATExclude());
						if(charge.getPrintOnInvoice()!=null && charge.getPrintOnInvoice()){
							accountLine.setIgnoreForBilling(true);
						}else{
							accountLine.setIgnoreForBilling(false);	
						}
						
					    if(!costElementFlag){
					    	accountLine.setRecGl(charge.getGl());
					    	accountLine.setPayGl(charge.getExpGl());
					    }else{
					    	accountLine = AccountLineUtil.getChargeGLCode(accountLineManager, serviceOrder, defaultAccountLine.getChargeCode(), quotationContract, sessionCorpID, accountLine);
					    }
						
						accountLine = AccountLineUtil.setContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, charge.getContractCurrency());
						accountLine.setEstimateContractRate(accountLine.getEstimateContractExchangeRate().multiply((accountLine.getEstimateSellRate())));
						accountLine.setEstimateContractRateAmmount(accountLine.getEstimateContractExchangeRate().multiply(accountLine.getEstimateRevenueAmount()));
						
						accountLine = AccountLineUtil.setOnlyPayableContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, charge.getPayableContractCurrency());
						accountLine.setEstimatePayableContractRate(accountLine.getEstimatePayableContractExchangeRate().multiply(accountLine.getEstimateRate()));
						accountLine.setEstimatePayableContractRateAmmount(accountLine.getEstimatePayableContractExchangeRate().multiply(accountLine.getEstimateExpense()));
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if((defaultAccountLine.getBillToCode()!=null)&&(!defaultAccountLine.getBillToCode().equalsIgnoreCase(""))){
					accountLine.setBillToCode(defaultAccountLine.getBillToCode());
					accountLine.setBillToName(defaultAccountLine.getBillToName());
				}else{
					accountLine.setBillToCode(billing.getBillToCode());
					accountLine.setBillToName(billing.getBillToName());
				}
				accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
				accountLine.setNetworkBillToName(billing.getNetworkBillToName());
				/*if(!accountLine.getVATExclude()){
					if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
			    		accountLine.setRecVatDescr(billing.getPrimaryVatCode());
			    		if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
				    		String recVatPercent="0";
				    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
					    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
					    		}
							accountLine.setRecVatPercent(recVatPercent);
				    		}
			    	}
				}*/
				if(defaultAccountLine.getVendorCode()!=null && (!(defaultAccountLine.getVendorCode().toString().equals("")))){
					accountLine.setVendorCode(defaultAccountLine.getVendorCode());
					accountLine.setEstimateVendorName(defaultAccountLine.getVendorName());
					String actCode="";				
					String companyDivisionAcctgCodeUnique="";
					 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
					 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
						 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
					 }
			         if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
			 			actCode=partnerManager.getAccountCrossReference(accountLine.getVendorCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
			 		}else{
			 			actCode=partnerManager.getAccountCrossReferenceUnique(accountLine.getVendorCode(),sessionCorpID);
			 		}
			         accountLine.setActgCode(actCode);
				}else{
					if(quotationContract!=null && (!(quotationContract.toString().equals("")))){
						  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,quotationContract);
					}
					if((!(trackingStatus.getSoNetworkGroup())) && billingCMMContractType && networkAgent && (!(trackingStatus.getAccNetworkGroup()))){
						String actCode="";
						accountLine.setVendorCode(serviceOrder.getBookingAgentCode());  
						accountLine.setEstimateVendorName(serviceOrder.getBookingAgentName());
						String companyDivisionAcctgCodeUnique="";
						 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
						 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
							 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
						 }
				         if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
				 			actCode=partnerManager.getAccountCrossReference(serviceOrder.getBookingAgentCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
				 		}else{
				 			actCode=partnerManager.getAccountCrossReferenceUnique(serviceOrder.getBookingAgentCode(),sessionCorpID);
				 		}
				         accountLine.setActgCode(actCode);	
					}else{
						accountLine.setVendorCode(defaultAccountLine.getVendorCode());
						accountLine.setEstimateVendorName(defaultAccountLine.getVendorName());
						String actCode="";				
						String companyDivisionAcctgCodeUnique="";
						 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
						 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
							 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
						 }
				         if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
				 			actCode=partnerManager.getAccountCrossReference(accountLine.getVendorCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
				 		}else{
				 			actCode=partnerManager.getAccountCrossReferenceUnique(accountLine.getVendorCode(),sessionCorpID);
				 		}
				         accountLine.setActgCode(actCode);
					}
				}

				if(multiCurrency.equalsIgnoreCase("Y")){
					
					String billingCurrency="";
					if(contractType){
						billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getBillToCode(),sessionCorpID);
					}				
					if(!billingCurrency.equalsIgnoreCase("")){
						accountLine = AccountLineUtil.setRecRateCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, billingCurrency);
						accountLine.setEstSellLocalRate(accountLine.getEstSellExchangeRate().multiply(accountLine.getEstimateSellRate()));
						accountLine.setEstSellLocalAmount(accountLine.getEstSellExchangeRate().multiply(accountLine.getEstimateRevenueAmount()));
					}else{
						if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals("")){
							accountLine = AccountLineUtil.setRecRateCurrencyAndExchangeRageForBaseCurrency( accountLine, baseCurrency);
							accountLine.setEstSellLocalRate(accountLine.getEstSellExchangeRate().multiply(accountLine.getEstimateSellRate()));
							accountLine.setEstSellLocalAmount(accountLine.getEstSellExchangeRate().multiply(accountLine.getEstimateRevenueAmount()));
						}else{
							accountLine = AccountLineUtil.setRecRateCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
							accountLine.setEstSellLocalRate(accountLine.getEstSellExchangeRate().multiply(accountLine.getEstimateSellRate()));
							accountLine.setEstSellLocalAmount(accountLine.getEstSellExchangeRate().multiply(accountLine.getEstimateRevenueAmount()));
						}
					}
				}
				
				String billingCurrency="";
				if(contractType){
				billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getVendorCode(),sessionCorpID);
				}
				if(!billingCurrency.equalsIgnoreCase("") && contractType){
					accountLine = AccountLineUtil.setCountryCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, billingCurrency);
					accountLine.setEstLocalRate(BigDecimal.valueOf(accountLine.getExchangeRate()).multiply(accountLine.getEstimateRate()));
					accountLine.setEstLocalAmount(BigDecimal.valueOf(accountLine.getExchangeRate()).multiply(accountLine.getEstimateExpense()));
				}else{
					if(baseCurrencyCompanyDivision == null ||  baseCurrencyCompanyDivision.equals("")){
						accountLine = AccountLineUtil.setCountryCurrencyAndExchangeRageForBaseCurrency(accountLine, baseCurrency);
						accountLine.setEstLocalRate(BigDecimal.valueOf(accountLine.getExchangeRate()).multiply(accountLine.getEstimateRate()));
						accountLine.setEstLocalAmount(BigDecimal.valueOf(accountLine.getExchangeRate()).multiply(accountLine.getEstimateExpense()));
					}else{
						accountLine = AccountLineUtil.setCountryCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
						accountLine.setEstLocalRate(BigDecimal.valueOf(accountLine.getExchangeRate()).multiply(accountLine.getEstimateRate()));
						accountLine.setEstLocalAmount(BigDecimal.valueOf(accountLine.getExchangeRate()).multiply(accountLine.getEstimateExpense()));
					}
				}
				//accountLine.setQuoteDescription(defaultAccountLine.getDescription());
				accountLine.setCorpID(sessionCorpID);
				accountLine.setServiceOrderId(serviceOrder.getId());
				accountLine.setStatus(true); 
				accountLine.setServiceOrder(serviceOrder);
				accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
				accountLine.setShipNumber(serviceOrder.getShipNumber());
				accountLine.setCreatedOn(new Date());
				accountLine.setCreatedBy(user);
				accountLine.setUpdatedOn(new Date());
				accountLine.setUpdatedBy(user); 
				accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
				accountLine.setContract(quotationContract);
				boolean activateAccPortal =true;
				try{
				if(accountLineAccountPortalFlag){	
				activateAccPortal =accountLineManager.findActivateAccPortal(jobtypeSO,companyDivisionSO,sessionCorpID);
				accountLine.setActivateAccPortal(activateAccPortal);
				}
				}catch(Exception e){
					
				}
				
			    //accountLine.setAccountLineNumber(AccountLineUtil.getAccountLineNumber(serviceOrderManager, serviceOrder));
				accountLineNumber="000";
				 try{
				 autoLineNumber = Long.valueOf(defaultAccountLine.getOrderNumber());
		             if((autoLineNumber.toString()).length() == 2) {
		             	accountLineNumber = "0"+(autoLineNumber.toString());
		             }else if((autoLineNumber.toString()).length() == 1) {
		             	accountLineNumber = "00"+(autoLineNumber.toString());
		             }else {
		             	accountLineNumber=autoLineNumber.toString();
		             }
				 }catch(Exception e){
					 e.printStackTrace();
				 }
				 accountLine.setAccountLineNumber(accountLineNumber);
			    if(systemDefault!=null && systemDefault.getVatCalculation() && !accountLine.getVATExclude()){
			    	   accountLine.setEstVatDescr(defaultAccountLine.getEstVatDescr());
			    	   accountLine.setEstVatPercent(defaultAccountLine.getEstVatPercent()); 
			    	   accountLine.setEstVatAmt(defaultAccountLine.getEstVatAmt()); 
			       }
			    if(systemDefault!=null && !accountLine.getVATExclude()){
			    	
						try{
							accountLine.setRecVatDescr(defaultAccountLine.getEstVatDescr());
				        	accountLine.setRecVatPercent(defaultAccountLine.getEstVatPercent()); 
				        	if(accountLine.getRecVatDescr()==null || (accountLine.getRecVatDescr().toString().trim().equals(""))){		
						if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
				    		accountLine.setRecVatDescr(billing.getPrimaryVatCode());
				    		if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
					    		String recVatPercent="0";
					    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
						    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
						    		}
								accountLine.setRecVatPercent(recVatPercent);
					    		}else{
						    		accountLine.setRecVatDescr(systemDefault.getReceivableVat());
							        if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
							        	 accountLine.setRecVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()));
									}
						    	}
				    	}else{
				    		accountLine.setRecVatDescr(systemDefault.getReceivableVat());
					        if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
					        	 accountLine.setRecVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()));
							}
				    	}
				        }
						}catch(Exception e){}
								    	
			         try{
			        	 accountLine.setPayVatDescr(defaultAccountLine.getEstExpVatDescr());
				         accountLine.setPayVatPercent(defaultAccountLine.getEstExpVatPercent()); 
				         if(accountLine.getPayVatDescr()==null || (accountLine.getPayVatDescr().toString().trim().equals(""))){	
			        	 if(accountLine.getVendorCode() !=null && trackingStatus.getOriginAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getOriginAgentCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getOriginAgentVatCode());
			        	 } 
			        	 else if(accountLine.getVendorCode() !=null && trackingStatus.getDestinationAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getDestinationAgentCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getDestinationAgentVatCode());
			        	 }
			        	 else if(accountLine.getVendorCode() !=null && trackingStatus.getOriginSubAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getOriginSubAgentVatCode());
			        	 }
			        	 else if(accountLine.getVendorCode() !=null && trackingStatus.getDestinationSubAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getDestinationSubAgentVatCode());
			        	 }		 
			        	 
			        	 else if(accountLine.getVendorCode() !=null && trackingStatus.getForwarderCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getForwarderCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getForwarderVatCode());
			        	 }
			        	 else if(accountLine.getVendorCode() !=null && trackingStatus.getBrokerCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getBrokerCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getBrokerVatCode());
			        	 }
			        	 else if(accountLine.getVendorCode() !=null && trackingStatus.getNetworkPartnerCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getNetworkPartnerVatCode());
			        	 }
			        	 
			        	 else if(accountLine.getVendorCode() !=null && serviceOrder.getBookingAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(serviceOrder.getBookingAgentCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getBookingAgentVatCode());
			        	 }
			        	 else if(accountLine.getVendorCode() !=null && billing.getVendorCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(billing.getVendorCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getVendorCodeVatCode());
			        	 }
			        	 else if(accountLine.getVendorCode() !=null && billing.getVendorCode1() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(billing.getVendorCode1().toString())){
			        		 accountLine.setPayVatDescr(billing.getVendorCodeVatCode1());
			        	 } 
			        	 if(accountLine.getPayVatDescr() ==null || accountLine.getPayVatDescr().toString().equals("") ){ 
			        	 if(systemDefault.getPayableVat()!=null && !systemDefault.getPayableVat().equals("")){
			               accountLine.setPayVatDescr(systemDefault.getPayableVat());
			        	 }
			        	 }
			        	 
			         if(payVatPercentList.containsKey(accountLine.getPayVatDescr())){
			        	 accountLine.setPayVatPercent(payVatPercentList.get(accountLine.getPayVatDescr()))	; 
			         }
				     }
			        if(systemDefault.getReceivableVat()!=null && (!(systemDefault.getReceivableVat().equals("")))){
			        if(accountLine.getRecVatDescr() ==null || accountLine.getRecVatDescr().toString().trim().equals("")){ 	
			         accountLine.setRecVatDescr(systemDefault.getReceivableVat()); 
			         accountLine.setRevisionVatDescr(systemDefault.getReceivableVat());
			         if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
			        	 accountLine.setRecVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()))	;
						}
			        }
			         }
			         }catch(Exception e){
			        	 e.printStackTrace();	 
			         }
			         try{
			        	 if(systemDefault!=null && systemDefault.getVatCalculation()){
			        		 
			        			Double estVatAmt=0.00;
			        			Double payVatPercent=0.00;
			        			payVatPercent=(accountLine.getRecVatPercent()==null?0.00:Double.parseDouble(accountLine.getRecVatPercent()));
			        			estVatAmt=(accountLine.getEstimateRevenueAmount()==null?0.00:Double.parseDouble(accountLine.getEstimateRevenueAmount()+""));
			        			if(contractType){
			        				estVatAmt=(accountLine.getEstSellLocalAmount()==null?0.00:Double.parseDouble(accountLine.getEstSellLocalAmount()+""));
			        			}
			        			estVatAmt=(estVatAmt*payVatPercent)/100;
			        			accountLine.setEstVatAmt(new BigDecimal(estVatAmt+""));
			        			estVatAmt=0.00;
			        			payVatPercent=0.00;
			        			payVatPercent=(accountLine.getPayVatPercent()==null?0.00:Double.parseDouble(accountLine.getPayVatPercent()+""));
			        			estVatAmt=(accountLine.getEstimateExpense()==null?0.00:Double.parseDouble(accountLine.getEstimateExpense()+""));
			        			if(contractType){
			        				estVatAmt=(accountLine.getEstLocalAmount()==null?0.00:Double.parseDouble(accountLine.getEstLocalAmount()+""));
			        			}
			        			estVatAmt=(estVatAmt*payVatPercent)/100;
			        			accountLine.setEstExpVatAmt(new BigDecimal(estVatAmt+""));
			        		 
			        	 }
			         }catch(Exception e){
			        	 e.printStackTrace();	 
			         }			         
			         }
			    
			    if((rollUpInvoiceFlag!=null && !rollUpInvoiceFlag.equalsIgnoreCase("")) && rollUpInvoiceFlag.equalsIgnoreCase("True")){
			    	if(quotationContract!=null && !quotationContract.equalsIgnoreCase("")){
						  if(!costElementFlag)	{
							  chargeCodeList1=chargesManager.findChargeCode(defaultAccountLine.getChargeCode(), quotationContract);
						  } else{
							  chargeCodeList1=chargesManager.findChargeCodeCostElement(defaultAccountLine.getChargeCode(), quotationContract,sessionCorpID,jobtypeSO, routingSO,companyDivisionSO);  
						  }
			    	}
			    	if(chargeCodeList1!=null && !chargeCodeList1.isEmpty() && chargeCodeList1.get(0)!=null && !chargeCodeList1.get(0).toString().equalsIgnoreCase("")){
					try{
						String[] rollUpInInvoiceArr = chargeCodeList1.get(0).toString().split("#");
						if(rollUpInInvoiceArr.length>13){
							String rollUpInInvoice = rollUpInInvoiceArr[13];
							if(rollUpInInvoice.equalsIgnoreCase("Y")){
								rollUpInInvoice="true";
								ignoreForBilling=Boolean.parseBoolean(rollUpInInvoice);
								accountLine.setRollUpInInvoice(ignoreForBilling);									
							}else{
								rollUpInInvoice="false";
								ignoreForBilling=Boolean.parseBoolean(rollUpInInvoice);
								accountLine.setRollUpInInvoice(ignoreForBilling);
							}
						}							
					}catch(Exception e){
						e.printStackTrace();
					}
			   	}
			  }
			      if(accountLine.getVATExclude() !=null && accountLine.getVATExclude()){
			           accountLine.setRecVatDescr("");
			           accountLine.setRecVatPercent("");
			           accountLine.setRecVatAmt(new BigDecimal(0));
			           accountLine.setPayVatDescr("");
			           accountLine.setPayVatPercent("");
			           accountLine.setPayVatAmt(new BigDecimal(0));
			           accountLine.setEstVatAmt(new BigDecimal(0));
			           accountLine.setEstExpVatAmt(new BigDecimal(0));
			           accountLine.setRevisionVatAmt(new BigDecimal(0));
			           accountLine.setRevisionExpVatAmt(new BigDecimal(0));
			         }						      

				accountLineManager.save(accountLine);
			}
			if(defaultList != null && !defaultList.isEmpty()){
				//serviceOrder.setDefaultAccountLineStatus(true);
				//serviceOrderManager.save(serviceOrder);
			}else{
				String key =  "defaultAccountLine.massage" ;
				saveMessage(getText(key));
			}
			updateAccountLineAjax(serviceOrder);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String addPricingDefaultLine(){ 
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Start");
			savePricingLine();
			String baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
			serviceOrder = serviceOrderManager.get(sid);
			billing =billingManager.get(sid);
			trackingStatus=trackingStatusManager.get(sid);
			customerFile = serviceOrder.getCustomerFile();
			quotationContract=serviceOrder.getContract();
			quotationBillToCode=customerFile.getBillToCode();
			miscellaneous = miscellaneousManager.get(sid);
			String user = getRequest().getRemoteUser();
			List defaultList = defaultAccountLineManager.getDefaultAccountList(jobtypeSO, routingSO, modeSO,quotationContract,quotationBillToCode,packingModeSO,commoditySO,serviceOrder.getServiceType(),companyDivisionSO,serviceOrder.getOriginCountryCode(),serviceOrder.getDestinationCountryCode(),miscellaneous.getEquipment(),serviceOrder.getOriginCity(),serviceOrder.getDestinationCity());
			
			Iterator it = defaultList.iterator();
			while(it.hasNext()){
				DefaultAccountLine defaultAccountLine = (DefaultAccountLine)it.next();
				accountLine = new AccountLine();
				if(defaultAccountLine.getAmount()!=null && (!(defaultAccountLine.getAmount().toString().equals("")))){
				accountLine.setEstimateExpense(new BigDecimal(defaultAccountLine.getAmount()));
				}else{
				accountLine.setEstimateExpense(new BigDecimal(0.00));	
				}
				if(defaultAccountLine.getRate()!=null && (!(defaultAccountLine.getRate().toString().equals("")))){
				accountLine.setEstimateRate(new BigDecimal(defaultAccountLine.getRate()));
				}else{
				accountLine.setEstimateRate(new BigDecimal(0.00));	
				}
				accountLine.setBasis(defaultAccountLine.getBasis());
				accountLine.setCategory(defaultAccountLine.getCategories());
				boolean contractType=false;
				if(quotationContract!=null && (!(quotationContract.toString().equals("")))){
				String	contractTypeValue=	accountLineManager.checkContractType(quotationContract,sessionCorpID);
					if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
						contractType=true;	
					}
					}
				if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals("")))){
				accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 
				if(contractType){
					accountLine.setEstimateSellQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 	
				}
				}else{
				accountLine.setEstimateQuantity(new BigDecimal(0.00)); 
				if(contractType){
					accountLine.setEstimateSellQuantity(new BigDecimal(0.00)); 	
				}
				}
				if(defaultAccountLine.getEstimatedRevenue()!=null && (!(defaultAccountLine.getEstimatedRevenue().toString().equals("")))){
				accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
				}else{
					accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));	
				}
				if(defaultAccountLine.getEstimatedRevenue()!=null && defaultAccountLine.getEstimatedRevenue().doubleValue()>0.00){
					accountLine.setDisplayOnQuote(true);
				}else{
					accountLine.setDisplayOnQuote(false);
				}
				if(defaultAccountLine.getMarkUp()!=null && (!( defaultAccountLine.getMarkUp().toString().equals("")))){
					accountLine.setEstimatePassPercentage(defaultAccountLine.getMarkUp());
					}else{
					accountLine.setEstimatePassPercentage(0);	
					}
				if(defaultAccountLine.getSellRate()!=null && (!(defaultAccountLine.getSellRate().toString().equals("")))){
					accountLine.setEstimateSellRate(defaultAccountLine.getSellRate());
					}else {
					accountLine.setEstimateSellRate(new BigDecimal(0.00));	
					}
				accountLine.setChargeCode(defaultAccountLine.getChargeCode());
				try {
					String ss=chargesManager.findPopulateCostElementData(defaultAccountLine.getChargeCode(), quotationContract,sessionCorpID);
					if(!ss.equalsIgnoreCase("")){
						accountLine.setAccountLineCostElement(ss.split("~")[0]);
						accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				///Code for AccountLine division 
				if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
					String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
					try{  
						String roles[] = agentRoleValue.split("~");
					  if(roles[3].toString().equals("hauler")){
						   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
							   accountLine.setDivision("01");
						   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
							   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
								   accountLine.setDivision("01");
							   }else{ 
								   String divisionTemp="";
										if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
										divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
										}
										if(!divisionTemp.equalsIgnoreCase("")){
											accountLine.setDivision(divisionTemp);
										}else{
											accountLine.setDivision(null);
										}
							     
							   }
						   }else{
							   String divisionTemp="";
									if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
									}
									if(!divisionTemp.equalsIgnoreCase("")){
										accountLine.setDivision(divisionTemp);
									}else{
										accountLine.setDivision(null);
									}
						   }
					  }else{
						  String divisionTemp="";
								if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
								divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
								}
								if(!divisionTemp.equalsIgnoreCase("")){
									accountLine.setDivision(divisionTemp);
								}else{
									accountLine.setDivision(null);
								}
					  }
					}catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				boolean activateAccPortal =true;
				try{
				if(accountLineAccountPortalFlag){	
				activateAccPortal =accountLineManager.findActivateAccPortal(jobtypeSO,companyDivisionSO,sessionCorpID);
				accountLine.setActivateAccPortal(activateAccPortal);
				}
				}catch(Exception e){
					
				}
				///Code for AccountLine division 			
				try{
					List chargeid=chargesManager.findChargeId(defaultAccountLine.getChargeCode(),quotationContract);
					Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
				    if(!costElementFlag){
				    	accountLine.setRecGl(charge.getGl());
				    	accountLine.setPayGl(charge.getExpGl());
				    }else{
				    	String chargeStr="";
						List glList = accountLineManager.findChargeDetailFromSO(quotationContract,defaultAccountLine.getChargeCode(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
						if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
							  chargeStr= glList.get(0).toString();
						  }
						   if(!chargeStr.equalsIgnoreCase("")){
							  String [] chrageDetailArr = chargeStr.split("#");
							  accountLine.setRecGl(chrageDetailArr[1]);
							  accountLine.setPayGl(chrageDetailArr[2]);
							}else{
							  accountLine.setRecGl("");
							  accountLine.setPayGl("");
						  }
				    }
				    if(defaultAccountLine.getDescription()!=null && (!(defaultAccountLine.getDescription().toString().trim().equals("")))){
				    	accountLine.setDescription(defaultAccountLine.getDescription());
						accountLine.setNote(defaultAccountLine.getDescription());
						accountLine.setQuoteDescription(defaultAccountLine.getDescription());
				    }else{
					accountLine.setDescription(charge.getDescription());
					accountLine.setNote(charge.getDescription());
					accountLine.setQuoteDescription(charge.getDescription());
				    }
					accountLine.setContractCurrency(charge.getContractCurrency());
					accountLine.setEstimateContractCurrency(charge.getContractCurrency());
					accountLine.setRevisionContractCurrency(charge.getContractCurrency());
					accountLine.setVATExclude(charge.getVATExclude());
					if(charge.getPrintOnInvoice()!=null && charge.getPrintOnInvoice()){
						accountLine.setIgnoreForBilling(true);
					}else{
						accountLine.setIgnoreForBilling(false);	
					}
					accountLine.setContractValueDate(new Date());
					accountLine.setEstimateContractValueDate(new Date());
					accountLine.setRevisionContractValueDate(new Date());
					List contractCurrencyExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,charge.getContractCurrency());
					if(contractCurrencyExchangeRateList != null && (!(contractCurrencyExchangeRateList.isEmpty())) && contractCurrencyExchangeRateList.get(0)!= null && (!(contractCurrencyExchangeRateList.get(0).toString().equals("")))){
						try{
						accountLine.setContractExchangeRate(new BigDecimal(contractCurrencyExchangeRateList.get(0).toString()));
						accountLine.setEstimateContractExchangeRate(new BigDecimal(contractCurrencyExchangeRateList.get(0).toString()));
						accountLine.setRevisionContractExchangeRate(new BigDecimal(contractCurrencyExchangeRateList.get(0).toString()));
						}catch(Exception e){
						accountLine.setContractExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setEstimateContractExchangeRate(new BigDecimal(1.0000));
						accountLine.setRevisionContractExchangeRate(new BigDecimal(1.0000));
						}
					}else{
						accountLine.setContractExchangeRate(new BigDecimal(1.0000));
						accountLine.setEstimateContractExchangeRate(new BigDecimal(1.0000));
						accountLine.setRevisionContractExchangeRate(new BigDecimal(1.0000));
					}
					accountLine.setPayableContractCurrency(charge.getPayableContractCurrency());
					accountLine.setEstimatePayableContractCurrency(charge.getPayableContractCurrency());
					accountLine.setRevisionPayableContractCurrency(charge.getPayableContractCurrency());
					accountLine.setPayableContractValueDate(new Date());
					accountLine.setEstimatePayableContractValueDate(new Date());
					accountLine.setRevisionPayableContractValueDate(new Date());
					List payableContractCurrencyExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,charge.getPayableContractCurrency());
					if(payableContractCurrencyExchangeRateList != null && (!(payableContractCurrencyExchangeRateList.isEmpty())) && payableContractCurrencyExchangeRateList.get(0)!= null && (!(payableContractCurrencyExchangeRateList.get(0).toString().equals("")))){
						try{
						accountLine.setPayableContractExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
						accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
						accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
						}catch(Exception e){
						accountLine.setPayableContractExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(1.0000));
						accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(1.0000));
						}
					}else{
						accountLine.setPayableContractExchangeRate(new BigDecimal(1.0000));
						accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(1.0000));
						accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(1.0000));
					}
				}catch(Exception e){
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				}
				if((defaultAccountLine.getBillToCode()!=null)&&(!defaultAccountLine.getBillToCode().equalsIgnoreCase(""))){
					accountLine.setBillToCode(defaultAccountLine.getBillToCode());
					accountLine.setBillToName(defaultAccountLine.getBillToName());
				}else{
					accountLine.setBillToCode(billing.getBillToCode());
					accountLine.setBillToName(billing.getBillToName());
				}
				accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
				accountLine.setNetworkBillToName(billing.getNetworkBillToName());
				/*if(!accountLine.getVATExclude()){
				if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
					accountLine.setRecVatDescr(billing.getPrimaryVatCode());
					if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
			    		String recVatPercent="0";
			    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
				    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
				    		}
						accountLine.setRecVatPercent(recVatPercent);
			    		}
				}
				}*/
				if(defaultAccountLine.getVendorCode()!=null && (!(defaultAccountLine.getVendorCode().toString().equals("")))){
				accountLine.setVendorCode(defaultAccountLine.getVendorCode());
				accountLine.setEstimateVendorName(defaultAccountLine.getVendorName());
				}else{
				if(quotationContract!=null && (!(quotationContract.toString().equals("")))){
					  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,quotationContract);
				    }
				if((!(trackingStatus.getSoNetworkGroup())) && billingCMMContractType && networkAgent && (!(trackingStatus.getAccNetworkGroup()))){
					String actCode="";
					accountLine.setVendorCode(serviceOrder.getBookingAgentCode());  
					accountLine.setEstimateVendorName(serviceOrder.getBookingAgentName());
					String companyDivisionAcctgCodeUnique="";
					 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
					 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
						 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
					 }
			         if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
			 			actCode=partnerManager.getAccountCrossReference(serviceOrder.getBookingAgentCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
			 		}else{
			 			actCode=partnerManager.getAccountCrossReferenceUnique(serviceOrder.getBookingAgentCode(),sessionCorpID);
			 		}
			         accountLine.setActgCode(actCode);	
				}else{
					accountLine.setVendorCode(defaultAccountLine.getVendorCode());
					accountLine.setEstimateVendorName(defaultAccountLine.getVendorName());
				}
				}
				
				/*accountLine.setRecGl(defaultAccountLine.getRecGl());
				accountLine.setPayGl(defaultAccountLine.getPayGl());
				accountLine.setDescription(defaultAccountLine.getDescription());*/
				baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID);
				if(multiCurrency.equalsIgnoreCase("Y")){
				//	String billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getBillToCode(),sessionCorpID);
					
					String billingCurrency="";
					if(contractType){
					billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getBillToCode(),sessionCorpID);
					}				
					if(!billingCurrency.equalsIgnoreCase(""))
					{
						accountLine.setRecRateCurrency(billingCurrency);
						accountLine.setEstSellCurrency(billingCurrency);
						accountLine.setRevisionSellCurrency(billingCurrency);
						accountLine.setRacValueDate(new Date());
						accountLine.setEstSellValueDate(new Date());
						accountLine.setRevisionSellValueDate(new Date());

						accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,billingCurrency);
						if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
							try{
							accountLine.setRecRateExchange(new BigDecimal(accExchangeRateList.get(0).toString()));
							accountLine.setEstSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
							accountLine.setRevisionSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
							}catch(Exception e){
							accountLine.setRecRateExchange(new BigDecimal(1.0000));
							accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
							accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
							}
						}else{
							accountLine.setRecRateExchange(new BigDecimal(1.0000));
							accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
							accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
						}
					}else{
					if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals(""))
					{
					accountLine.setRecRateCurrency(baseCurrency);
					accountLine.setRacValueDate(new Date());
					accountLine.setRecRateExchange(new BigDecimal(1.0000));
					accountLine.setEstSellCurrency(baseCurrency);
					accountLine.setEstSellValueDate(new Date());
					accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
					accountLine.setRevisionSellCurrency(baseCurrency);
					accountLine.setRevisionSellValueDate(new Date());
					accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
					}
					else
					{
					accountLine.setRecRateCurrency(baseCurrencyCompanyDivision);
					accountLine.setRacValueDate(new Date());
					accountLine.setEstSellCurrency(baseCurrencyCompanyDivision);
					accountLine.setEstSellValueDate(new Date());
					accountLine.setRevisionSellCurrency(baseCurrencyCompanyDivision);
					accountLine.setRevisionSellValueDate(new Date());
					accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
					if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
						try{
						accountLine.setRecRateExchange(new BigDecimal(accExchangeRateList.get(0).toString()));	
						accountLine.setEstSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
						accountLine.setRevisionSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
						}catch(Exception e){
						accountLine.setRecRateExchange(new BigDecimal(1.0000));
						accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
						accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
						}
					}else{
						accountLine.setRecRateExchange(new BigDecimal(1.0000));
						accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
						accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
					}
					}
					}
				}
				
				String billingCurrency="";
				if(contractType){
				billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getVendorCode(),sessionCorpID);
				}
				if(!billingCurrency.equalsIgnoreCase("") && contractType)
				{ 
					accountLine.setEstCurrency(billingCurrency);
					accountLine.setRevisionCurrency(billingCurrency);
					accountLine.setCountry(billingCurrency);
					accountLine.setEstValueDate(new Date());
					accountLine.setRevisionValueDate(new Date());
					accountLine.setValueDate(new Date());
					//accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision); 
					accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,billingCurrency);
					if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
						try{
						accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
						accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
						accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
						}catch(Exception e){
							accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
							accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
							accountLine.setExchangeRate(new Double(1.0));	
						}
					}else{
						accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
						accountLine.setExchangeRate(new Double(1.0));
					}
					
					
				}else{			
				if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
				{
				accountLine.setEstCurrency(baseCurrency);
				accountLine.setRevisionCurrency(baseCurrency);	
				accountLine.setCountry(baseCurrency);
				accountLine.setEstValueDate(new Date());
				accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
				accountLine.setRevisionValueDate(new Date());
				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
				accountLine.setValueDate(new Date());
				accountLine.setExchangeRate(new Double(1.0));
				}
				else
				{
				accountLine.setEstCurrency(baseCurrencyCompanyDivision);
				accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
				accountLine.setCountry(baseCurrencyCompanyDivision);
				accountLine.setEstValueDate(new Date());
				accountLine.setRevisionValueDate(new Date());
				accountLine.setValueDate(new Date());
				//accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision); 
				accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
				if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
					try{
					accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
					accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
					accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
					}catch(Exception e){
						accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
						accountLine.setExchangeRate(new Double(1.0));	
					}
				}else{
					accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
					accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
					accountLine.setExchangeRate(new Double(1.0));
				}
				}
				}
				//accountLine.setQuoteDescription(defaultAccountLine.getDescription());
				accountLine.setCorpID(sessionCorpID);
				accountLine.setServiceOrderId(serviceOrder.getId());
				accountLine.setStatus(true); 
				accountLine.setServiceOrder(serviceOrder);
				accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
				accountLine.setShipNumber(serviceOrder.getShipNumber());
				accountLine.setCreatedOn(new Date());
				accountLine.setCreatedBy(user);
				accountLine.setUpdatedOn(new Date());
				accountLine.setUpdatedBy(user); 
				accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
				accountLine.setContract(quotationContract);
				
				accountLineNumber="000";
				 try{
				 autoLineNumber = Long.valueOf(defaultAccountLine.getOrderNumber());
		             if((autoLineNumber.toString()).length() == 2) {
		             	accountLineNumber = "0"+(autoLineNumber.toString());
		             }else if((autoLineNumber.toString()).length() == 1) {
		             	accountLineNumber = "00"+(autoLineNumber.toString());
		             }else {
		             	accountLineNumber=autoLineNumber.toString();
		             }
				 }catch(Exception e){
					 e.printStackTrace();
				 }
			    accountLine.setAccountLineNumber(accountLineNumber);
			    if(estVatFlag.equalsIgnoreCase("Y")){
			    	   accountLine.setEstVatDescr(defaultAccountLine.getEstVatDescr());
			    	   accountLine.setEstVatPercent(defaultAccountLine.getEstVatPercent()); 
			    	   accountLine.setEstVatAmt(defaultAccountLine.getEstVatAmt()); 
			    	   
			       }
			    //if(systemDefault!=null && systemDefault.getReceivableVat()!=null && !systemDefault.getReceivableVat().equalsIgnoreCase("") && systemDefault.getPayableVat()!=null && !systemDefault.getPayableVat().equalsIgnoreCase("")){
			    	
					if(!accountLine.getVATExclude()){
						try{
							accountLine.setRecVatDescr(defaultAccountLine.getEstVatDescr());
				        	accountLine.setRecVatPercent(defaultAccountLine.getEstVatPercent()); 
				        	if(accountLine.getRecVatDescr()==null || (accountLine.getRecVatDescr().toString().trim().equals(""))){	
						if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
				    		accountLine.setRecVatDescr(billing.getPrimaryVatCode());
				    		if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
					    		String recVatPercent="0";
					    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
						    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
						    		}
								accountLine.setRecVatPercent(recVatPercent);
					    		}else{
						    		accountLine.setRecVatDescr(systemDefault.getReceivableVat());
							        if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
							        	 accountLine.setRecVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()));
									}
						    	}
				    	}else{
				    		accountLine.setRecVatDescr(systemDefault.getReceivableVat());
					        if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
					        	 accountLine.setRecVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()));
							}
				    	}
				        }
						}catch(Exception e){
				        	 e.printStackTrace();	 
				         }
					}
			    	
			    	
			    	
			         try{
			        	 accountLine.setPayVatDescr(defaultAccountLine.getEstExpVatDescr());
				         accountLine.setPayVatPercent(defaultAccountLine.getEstExpVatPercent()); 
				         if(accountLine.getPayVatDescr()==null || (accountLine.getPayVatDescr().toString().trim().equals(""))){
			        	 if(accountLine.getVendorCode() !=null && trackingStatus.getOriginAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getOriginAgentCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getOriginAgentVatCode());
			        	 } 
			        	 else if(accountLine.getVendorCode() !=null && trackingStatus.getDestinationAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getDestinationAgentCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getDestinationAgentVatCode());
			        	 }
			        	 else if(accountLine.getVendorCode() !=null && trackingStatus.getOriginSubAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getOriginSubAgentVatCode());
			        	 }
			        	 else if(accountLine.getVendorCode() !=null && trackingStatus.getDestinationSubAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getDestinationSubAgentVatCode());
			        	 }		 
			        	 
			        	 else if(accountLine.getVendorCode() !=null && trackingStatus.getForwarderCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getForwarderCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getForwarderVatCode());
			        	 }
			        	 else if(accountLine.getVendorCode() !=null && trackingStatus.getBrokerCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getBrokerCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getBrokerVatCode());
			        	 }
			        	 else if(accountLine.getVendorCode() !=null && trackingStatus.getNetworkPartnerCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getNetworkPartnerVatCode());
			        	 }
			        	 
			        	 else if(accountLine.getVendorCode() !=null && serviceOrder.getBookingAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(serviceOrder.getBookingAgentCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getBookingAgentVatCode());
			        	 }
			        	 else if(accountLine.getVendorCode() !=null && billing.getVendorCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(billing.getVendorCode().toString())){
			        		 accountLine.setPayVatDescr(billing.getVendorCodeVatCode());
			        	 }
			        	 else if(accountLine.getVendorCode() !=null && billing.getVendorCode1() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(billing.getVendorCode1().toString())){
			        		 accountLine.setPayVatDescr(billing.getVendorCodeVatCode1());
			        	 } 
			        	 if(accountLine.getPayVatDescr() ==null || accountLine.getPayVatDescr().toString().equals("") ){ 
			        	 if(systemDefault.getPayableVat()!=null && !systemDefault.getPayableVat().equals("")){
			               accountLine.setPayVatDescr(systemDefault.getPayableVat());
			        	 }
			        	 }
			         if(payVatPercentList.containsKey(accountLine.getPayVatDescr())){
			        	 accountLine.setPayVatPercent(payVatPercentList.get(accountLine.getPayVatDescr()))	; 
			         }
				     }
			         /*if(accountLine.getRecVatDescr() ==null || accountLine.getRecVatDescr().toString().trim().equals("")){
			         accountLine.setRecVatDescr(systemDefault.getReceivableVat());
			         }
			        // accountLine.setRevisionVatDescr(systemDefault.getReceivableVat());
			         if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
			        	 accountLine.setRecVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()));
						}*/
			         }catch(Exception e){
			        	 e.printStackTrace();	 
			         }
			         
			         Double estVatAmt=0.00;
	        			Double payVatPercent=0.00;
	        			payVatPercent=(accountLine.getRecVatPercent()==null?0.00:Double.parseDouble(accountLine.getRecVatPercent()));
	        			estVatAmt=(accountLine.getEstimateRevenueAmount()==null?0.00:Double.parseDouble(accountLine.getEstimateRevenueAmount()+""));
	        			if(contractType){
	        				estVatAmt=(accountLine.getEstSellLocalAmount()==null?0.00:Double.parseDouble(accountLine.getEstSellLocalAmount()+""));
	        			}
	        			estVatAmt=(estVatAmt*payVatPercent)/100;
	        			accountLine.setEstVatAmt(new BigDecimal(estVatAmt+""));
	        			estVatAmt=0.00;
	        			payVatPercent=0.00;
	        			payVatPercent=(accountLine.getPayVatPercent()==null?0.00:Double.parseDouble(accountLine.getPayVatPercent()+""));
	        			estVatAmt=(accountLine.getEstimateExpense()==null?0.00:Double.parseDouble(accountLine.getEstimateExpense()+""));
	        			if(contractType){
	        				estVatAmt=(accountLine.getEstLocalAmount()==null?0.00:Double.parseDouble(accountLine.getEstLocalAmount()+""));
	        			}
	        			estVatAmt=(estVatAmt*payVatPercent)/100;
	        			accountLine.setEstExpVatAmt(new BigDecimal(estVatAmt+""));
			         
			    //}
			    
				accountLineManager.save(accountLine);
			}
			if(!defaultList.isEmpty())
			{
			  //serviceOrder.setDefaultAccountLineStatus(true);
			  // serviceOrderManager.save(serviceOrder);
			}
			else
			{
				String key =  "defaultAccountLine.massage" ;
				saveMessage(getText(key));
			}
			updateAccountLine(serviceOrder);
			//quotationEstimateAccountList();
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	
	}
	private List valuationList;
	private List transportationList;
	private List packingList;
	private List packingContList;
	private List packingUnpackList;
	private List otherMiscellaneousList;
	private List underLyingHaulList;
	private String packingSum;
	private String packContSum;
	private String packUnpackSum;
	private String transportSum;
	private String othersSum;
	private String valuationSum;
    String grpPermission="";
	@SkipValidation
	public String editQuotationServiceOrder() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			roleHitFlag="OK";
			//getComboList(sessionCorpID);
			if (id != null) {
				
				/*companies =  companyManager.findByCompanyDivis(sessionCorpID);
				companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,serviceOrder.getBookingAgentCode());
				*/
				customerFile = customerFileManager.get(id);
				contracts = customerFileManager.findCustJobContract(customerFile.getBillToCode(),customerFile.getJob(),customerFile.getCreatedOn(),sessionCorpID,customerFile.getCompanyDivision(),customerFile.getBookingAgentCode());
				ostates = customerFileManager.findDefaultStateList(customerFile.getOriginCountry(), sessionCorpID);
			    dstates = customerFileManager.findDefaultStateList(customerFile.getDestinationCountry(), sessionCorpID);
			    companies =  companyManager.findByCompanyDivis(sessionCorpID);
			    companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,customerFile.getBookingAgentCode());
				maxShip = customerFileManager.findMaximumShipCF(customerFile.getSequenceNumber(),"",customerFile.getCorpID());
				if (maxShip.get(0) == null) {
					ship = "01";

				} else {
					autoShip = Long.parseLong((maxShip).get(0).toString()) + 1;
					//System.out.println(autoShip);
					if ((autoShip.toString()).length() == 1) {
						ship = "0" + (autoShip.toString());
					} else {
						ship = autoShip.toString();
					}

					prevShip = Long.parseLong((maxShip).get(0).toString());
					//System.out.println(prevShip);
					if ((prevShip.toString()).length() == 1) {
						shipPrev = "0" + (prevShip.toString());
					} else {
						shipPrev = prevShip.toString();
					}
					if (!ship.equals("01")) {
						shipnumber = customerFile.getSequenceNumber() + shipPrev;
						tempID = Long.parseLong((serviceOrderManager.findIdByShipNumber(shipnumber)).get(0).toString());
						serviceOrderPrev = serviceOrderManager.get(tempID);
					}

				}
				customerFile = customerFileManager.get(id);
				shipnumber = customerFile.getSequenceNumber() + ship;
				serviceOrder = new ServiceOrder();
				miscellaneous = new Miscellaneous();
				miscellaneous.setShipNumber(shipnumber);
				miscellaneous.setEstimateTareWeight(new BigDecimal("0.00"));
				miscellaneous.setActualTareWeight(new BigDecimal("0.00"));
				miscellaneous.setRwghTare(new BigDecimal("0.00"));
				miscellaneous.setChargeableTareWeight(new BigDecimal("0.00"));
				miscellaneous.setEntitleTareWeight(new BigDecimal("0.00"));
				serviceOrder.setShip(ship);
				serviceOrder.setShipNumber(shipnumber);			
				serviceOrder.setSequenceNumber(customerFile.getSequenceNumber());
				//Bug 11839 - Possibility to modify Current status to Prior status
				serviceOrder.setStatus("NEW");
				serviceOrder.setStatusNumber(new Integer(1));
				serviceOrder.setQuoteStatus("PE");
				serviceOrder.setStatusDate(new Date());
			    serviceOrder.setUpdatedOn(new Date());
				serviceOrder.setCreatedOn(new Date());
				serviceOrder.setCreatedBy(getRequest().getRemoteUser());
				serviceOrder.setUpdatedBy(getRequest().getRemoteUser());
				
				serviceOrder.setCompanyDivision(customerFile.getCompanyDivision());
				//sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						miscellaneous.setUnit1(systemDefault.getWeightUnit());
						miscellaneous.setUnit2(systemDefault.getVolumeUnit());
					}
				}  
				//sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
			            if(systemDefault.getUnitVariable() !=null && systemDefault.getUnitVariable().equalsIgnoreCase("Yes"))
			            	disableChk ="Y";
			            else
			            	disableChk ="N";
					}
				}
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
					weightType="lbscft";
				}
				else{
					weightType="kgscbm";
				}
				serviceOrder.setPrefix(customerFile.getPrefix());
				serviceOrder.setMi(customerFile.getMiddleInitial());
				serviceOrder.setFirstName(customerFile.getFirstName());
				serviceOrder.setLastName(customerFile.getLastName());
				serviceOrder.setSocialSecurityNumber(customerFile.getSocialSecurityNumber());
				serviceOrder.setJob(customerFile.getJob());
				if(!sessionCorpID.equalsIgnoreCase("CWMS"))
				{
				serviceOrder.setEstimator(customerFile.getEstimator());
				}
				serviceOrder.setSalesMan(customerFile.getSalesMan());
				serviceOrder.setCoordinator(customerFile.getCoordinator());
				serviceOrder.setOriginCompany(customerFile.getOriginCompany());
				serviceOrder.setOriginAddressLine1(customerFile.getOriginAddress1());
				serviceOrder.setOriginAddressLine2(customerFile.getOriginAddress2());
				serviceOrder.setOriginAddressLine3(customerFile.getOriginAddress3());
				serviceOrder.setOriginCity(customerFile.getOriginCity());
				serviceOrder.setOriginCityCode(customerFile.getOriginCityCode());
				serviceOrder.setOriginState(customerFile.getOriginState());
				serviceOrder.setOriginZip(customerFile.getOriginZip());
				serviceOrder.setOriginPreferredContactTime(customerFile.getOriginPreferredContactTime());
				serviceOrder.setOriginCountry(customerFile.getOriginCountry());
				serviceOrder.setOriginCountryCode(customerFile.getOriginCountryCode());
				serviceOrder.setOriginDayPhone(customerFile.getOriginDayPhone());
				serviceOrder.setOriginDayExtn(customerFile.getOriginDayPhoneExt());
				serviceOrder.setOriginHomePhone(customerFile.getOriginHomePhone());
				serviceOrder.setOriginFax(customerFile.getOriginFax());
				serviceOrder.setOriginContactName(customerFile.getContactName());
				serviceOrder.setOriginContactWork(customerFile.getContactPhone());
				serviceOrder.setEmail(customerFile.getEmail());
				serviceOrder.setEmail2(customerFile.getEmail2());
				serviceOrder.setDestinationCompany(customerFile.getDestinationCompany());
				serviceOrder.setDestinationAddressLine1(customerFile.getDestinationAddress1());
				serviceOrder.setDestinationAddressLine2(customerFile.getDestinationAddress2());
				serviceOrder.setDestinationAddressLine3(customerFile.getDestinationAddress3());
				serviceOrder.setDestinationCity(customerFile.getDestinationCity());
				serviceOrder.setDestinationCityCode(customerFile.getDestinationCityCode());
				serviceOrder.setDestinationZip(customerFile.getDestinationZip());
				serviceOrder.setDestPreferredContactTime(customerFile.getDestPreferredContactTime());
				serviceOrder.setDestinationCountryCode(customerFile.getDestinationCountryCode());
				serviceOrder.setDestinationCountry(customerFile.getDestinationCountry());
				serviceOrder.setDestinationDayPhone(customerFile.getDestinationDayPhone());
				serviceOrder.setDestinationDayExtn(customerFile.getDestinationDayPhoneExt());
				serviceOrder.setDestinationHomePhone(customerFile.getDestinationHomePhone());
				serviceOrder.setDestinationFax(customerFile.getDestinationFax());
				serviceOrder.setDestinationDayPhone(customerFile.getDestinationDayPhone());
				serviceOrder.setDestinationEmail(customerFile.getDestinationEmail());
				serviceOrder.setDestinationEmail2(customerFile.getDestinationEmail2());
				serviceOrder.setBillToName(customerFile.getBillToName());
				serviceOrder.setBillToCode(customerFile.getBillToCode());
				serviceOrder.setOriginMobile(customerFile.getOriginMobile());
				serviceOrder.setDestinationMobile(customerFile.getDestinationMobile());
				serviceOrder.setContactName(customerFile.getDestinationContactName());
				serviceOrder.setContactPhone(customerFile.getDestinationContactPhone());
				serviceOrder.setDestinationCountry(customerFile.getDestinationCountry());
				serviceOrder.setOriginCountry(customerFile.getOriginCountry());
				serviceOrder.setDestinationState(customerFile.getDestinationState());
				serviceOrder.setVip(customerFile.getVip());
				serviceOrder.setContract(customerFile.getContract());
				serviceOrder.setCoordinator(customerFile.getCoordinator());
				serviceOrder.setIncExcServiceTypeLanguage("en");
				if(!sessionCorpID.equalsIgnoreCase("CWMS"))
				{
				serviceOrder.setEstimator(customerFile.getEstimator());
				}
				serviceOrder.setSalesMan(customerFile.getSalesMan());
				serviceOrder.setCorpID(sessionCorpID);
				serviceOrder.setBookingAgentCode(customerFile.getBookingAgentCode());
				serviceOrder.setBookingAgentName(customerFile.getBookingAgentName());
				serviceOrder.setLastName(customerFile.getLastName());
				serviceOrder.setControlFlag(customerFile.getControlFlag());
				serviceOrder.setQuoteAccept("P"); 
				companies =  companyManager.findByCompanyDivis(sessionCorpID);
			    companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID,customerFile.getBookingAgentCode());

				//prev DATA
				if (!ship.equals("01")) {
					serviceOrder.setServiceType(serviceOrderPrev.getServiceType());
					serviceOrder.setRouting(serviceOrderPrev.getRouting());
				}
				//coordinatorList=refMasterManager.findCordinaor(serviceOrder.getJob(),"ROLE_COORD",sessionCorpID);
				//estimatorList=refMasterManager.findCordinaor(serviceOrder.getJob(),"ROLE_SALE",sessionCorpID);
				coordinatorList=refMasterManager.findCordinaor(serviceOrder.getJob(),"ROLE_COORD",sessionCorpID);
				if(serviceOrder.getCoordinator()!=null && !(serviceOrder.getCoordinator().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(serviceOrder.getCoordinator());
					if(!aliasNameList.isEmpty()){
						String aliasName=aliasNameList.get(0).toString();
						String coordCode=serviceOrder.getCoordinator().toUpperCase();
						String alias = aliasName.toUpperCase();
						if(!coordinatorList.containsValue(coordCode)){
							coordinatorList.put(coordCode,alias );
						}
					}
				}
				//service = refMasterManager.findServiceByJob(serviceOrder.getJob(),sessionCorpID, "SERVICE");
				estimatorList=refMasterManager.findCordinaor(serviceOrder.getJob(),"ROLE_SALE",sessionCorpID);
				if(serviceOrder.getSalesMan()!=null && !(serviceOrder.getSalesMan().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(serviceOrder.getSalesMan());
					if(!aliasNameList.isEmpty()){
						String estimatorCode=serviceOrder.getSalesMan().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!estimatorList.containsValue(estimatorCode)){
							estimatorList.put(estimatorCode,alias );
						}
					}
						
				}
				commodit =refMasterManager.findByParameter(sessionCorpID,"COMMODIT",serviceOrder.getJob());
			    company=companyManager.findByCorpID(sessionCorpID).get(0);
			 	String str=customerFile.getBillToCode();
			      List permission= partnerPrivateManager.getGroupAgePermission(str,sessionCorpID);
			 	
			      if(!(permission==null) && !(permission.isEmpty())&& permission.get(0)!=null){
			      	grpPermission=permission.get(0).toString();
			      	
			      }        
					String grpDefault=company.getGrpDefault();
					if(grpDefault.equalsIgnoreCase("Yes") && grpPermission.equalsIgnoreCase("true")){
						serviceOrder.setGrpPref(true);
					}
					else{
						serviceOrder.setGrpPref(false);
					}        
					String jobCheck=serviceOrder.getJob();
					if(jobCheck!=null && jobCheck.equalsIgnoreCase("GRP")){
						serviceOrder.setGrpPref(true);
					}
			}
			if(serviceOrder != null && serviceOrder.getShipNumber() != null && serviceOrder.getShipNumber().length() > 0){
				 resourceList = itemsJbkEquipManager.findByShipNum(serviceOrder.getShipNumber(), sessionCorpID);
			 }
			ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
			dstates = customerFileManager.findDefaultStateList("", sessionCorpID);
			valuationList=serviceOrderManager.findValuationList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			packingList=serviceOrderManager.findPackingList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
      // packingContList=serviceOrderManager.findPackingContList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			//packingUnpackList=serviceOrderManager.findPackingUnpackList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			underLyingHaulList=serviceOrderManager.findUnderLyingHaulList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			otherMiscellaneousList=serviceOrderManager.findOtherMiscellaneousList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list1=serviceOrderManager.findPackingSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list2=serviceOrderManager.findUnpackingSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list3=serviceOrderManager.findContainerSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
			destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());			
			
			if(!list1.contains(null)){
				packingSum=list1.get(0).toString();
			}else{
				packingSum="None";
			}
			if(!list3.contains(null)){
				packContSum=list3.get(0).toString();
			}else{
				packContSum="None";
			}
			if(!list2.contains(null)){
				packUnpackSum=list2.get(0).toString();
			}else{
				packUnpackSum="None";
			}
			List list4=serviceOrderManager.findValuationSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list5=serviceOrderManager.findTransportationSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list6=serviceOrderManager.findOthersSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			if(!list4.contains(null))
				{
				valuationSum=list4.get(0).toString();
				}else{
					valuationSum="None";
				}
			if(!list5.contains(null)){
				transportSum=list5.get(0).toString();
			}else{
				transportSum="None";
			}
			if(!list6.contains(null)){
				othersSum=list6.get(0).toString();
			}else{
				othersSum="None";
			}
			getNotesForIconChange();
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String editQuotationAfterSubmitCommodity() throws Exception {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
			if (save != null) {
				return save();
			}
			ostates = customerFileManager.findDefaultStateList(customerFile.getOriginCountry(), sessionCorpID);
			dstates = customerFileManager.findDefaultStateList(customerFile.getDestinationCountry(), sessionCorpID);
			originAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",serviceOrder.getJob());
			destinationAddress = adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",serviceOrder.getJob());			
			//getComboList(sessionCorpID);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
	  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String quotationEstimateAccountList() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Start");
			serviceOrder = serviceOrderManager.get(sid);
			customerFile = serviceOrder.getCustomerFile();
			quotationContract=serviceOrder.getContract();
			quotationBillToCode=customerFile.getBillToCode(); 
			//accountLineList = new ArrayList(serviceOrder.getAccountLines());
			if(accountLineStatus==null||accountLineStatus.equals(""))
			{
				accountLineStatus="true";
			}
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			getSession().setAttribute("accountLines", accountLineList);
			if ((billingManager.checkById(sid)).isEmpty()) {
				//System.out.println("\n\n Billing Record not found for id=\t" + sid + "\n\n");
				billingFlag = 0;
			} else {
				//System.out.println("\n\n Billing Record found for id=\t" + sid + "\n\n");
				billingFlag = 1;

			}
			//updateAccountLine( serviceOrder);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public  String addDefaultAccountLine(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid);
			billing=billingManager.get(sid);
			trackingStatus=trackingStatusManager.get(sid);
			customerFile = serviceOrder.getCustomerFile();
			quotationContract=serviceOrder.getContract();
			quotationBillToCode=customerFile.getBillToCode();
			miscellaneous = miscellaneousManager.get(sid);
			String user = getRequest().getRemoteUser();
			List defaultList = defaultAccountLineManager.getDefaultAccountList(jobtypeSO, routingSO, modeSO,quotationContract,quotationBillToCode,packingModeSO,commoditySO,serviceOrder.getServiceType(),companyDivisionSO,serviceOrder.getOriginCountryCode(),serviceOrder.getDestinationCountryCode(),miscellaneous.getEquipment(),serviceOrder.getOriginCity(),serviceOrder.getDestinationCity());
			
			Iterator it = defaultList.iterator();
			while(it.hasNext()){
				DefaultAccountLine defaultAccountLine = (DefaultAccountLine)it.next();
				accountLine = new AccountLine();
				if(defaultAccountLine.getAmount()!=null && (!(defaultAccountLine.getAmount().toString().equals("")))){
					accountLine.setEstimateExpense(new BigDecimal(defaultAccountLine.getAmount()));
					}else{
					accountLine.setEstimateExpense(new BigDecimal(0.00));	
					} 
				if(defaultAccountLine.getRate()!=null && (!(defaultAccountLine.getRate().toString().equals("")))){
					accountLine.setEstimateRate(new BigDecimal(defaultAccountLine.getRate()));
					}else{
					accountLine.setEstimateRate(new BigDecimal(0.00));	
					} 
				accountLine.setBasis(defaultAccountLine.getBasis());
				accountLine.setCategory(defaultAccountLine.getCategories());
				if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals("")))){
					accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 
					}else{
					accountLine.setEstimateQuantity(new BigDecimal(0.00)); 
					} 
				if(defaultAccountLine.getEstimatedRevenue()!=null && (!(defaultAccountLine.getEstimatedRevenue().toString().equals("")))){
					accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
					}else{
						accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));	
					}
				if(defaultAccountLine.getEstimatedRevenue()!=null && defaultAccountLine.getEstimatedRevenue().doubleValue()>0.00){
					accountLine.setDisplayOnQuote(true);
				}else{
					accountLine.setDisplayOnQuote(false);
				}
				if(defaultAccountLine.getMarkUp()!=null && (!( defaultAccountLine.getMarkUp().toString().equals("")))){
				accountLine.setEstimatePassPercentage(defaultAccountLine.getMarkUp());
				}else{
				accountLine.setEstimatePassPercentage(0);	
				}
				if(defaultAccountLine.getSellRate()!=null && (!(defaultAccountLine.getSellRate().toString().equals("")))){
				accountLine.setEstimateSellRate(defaultAccountLine.getSellRate());
				}else {
				accountLine.setEstimateSellRate(new BigDecimal(0.00));	
				}
				accountLine.setChargeCode(defaultAccountLine.getChargeCode());
				try {
					String ss=chargesManager.findPopulateCostElementData(defaultAccountLine.getChargeCode(), quotationContract,sessionCorpID);
					if(!ss.equalsIgnoreCase("")){
						accountLine.setAccountLineCostElement(ss.split("~")[0]);
						accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}

				if((defaultAccountLine.getBillToCode()!=null)&&(!defaultAccountLine.getBillToCode().equalsIgnoreCase(""))){
					accountLine.setBillToCode(defaultAccountLine.getBillToCode());
					accountLine.setBillToName(defaultAccountLine.getBillToName());
				}else{
					accountLine.setBillToCode(billing.getBillToCode());
					accountLine.setBillToName(billing.getBillToName());
				}
				accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
				accountLine.setNetworkBillToName(billing.getNetworkBillToName());
				if(!accountLine.getVATExclude()){
					accountLine.setRecVatDescr(defaultAccountLine.getEstVatDescr());
		        	accountLine.setRecVatPercent(defaultAccountLine.getEstVatPercent()); 
		        	if(accountLine.getRecVatDescr()==null || (accountLine.getRecVatDescr().toString().trim().equals(""))){	
				if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
					accountLine.setRecVatDescr(billing.getPrimaryVatCode());
					if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
			    		String recVatPercent="0";
			    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
				    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
				    		}
						accountLine.setRecVatPercent(recVatPercent);
			    		}
				}
				}
				}
				boolean activateAccPortal =true;
				try{
				if(accountLineAccountPortalFlag){	
				activateAccPortal =accountLineManager.findActivateAccPortal(jobtypeSO,companyDivisionSO,sessionCorpID);
				accountLine.setActivateAccPortal(activateAccPortal);
				}
				}catch(Exception e){
					
				}
				accountLine.setRecGl(defaultAccountLine.getRecGl());
				accountLine.setPayGl(defaultAccountLine.getPayGl());
				accountLine.setDescription(defaultAccountLine.getDescription());
				accountLine.setQuoteDescription(defaultAccountLine.getDescription());
				accountLine.setCorpID(sessionCorpID);
				accountLine.setServiceOrderId(serviceOrder.getId());
				accountLine.setStatus(true);
				accountLine.setServiceOrder(serviceOrder);
				accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
				accountLine.setShipNumber(serviceOrder.getShipNumber());
				accountLine.setCreatedOn(new Date());
				accountLine.setCreatedBy(user);
				accountLine.setUpdatedOn(new Date());
				accountLine.setUpdatedBy(user); 
				if(defaultAccountLine.getVendorCode()!=null && (!(defaultAccountLine.getVendorCode().toString().equals("")))){
				accountLine.setVendorCode(defaultAccountLine.getVendorCode());
				accountLine.setEstimateVendorName(defaultAccountLine.getVendorName());
			    }else{
				if(quotationContract!=null && (!(quotationContract.toString().equals("")))){
					  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,quotationContract);
				    }
				if((!(trackingStatus.getSoNetworkGroup())) && billingCMMContractType && networkAgent && (!(trackingStatus.getAccNetworkGroup()))){
					String actCode="";
					accountLine.setVendorCode(serviceOrder.getBookingAgentCode());  
					accountLine.setEstimateVendorName(serviceOrder.getBookingAgentName());
					String companyDivisionAcctgCodeUnique="";
					 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
					 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
						 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
					 }
			         if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
			 			actCode=partnerManager.getAccountCrossReference(serviceOrder.getBookingAgentCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
			 		}else{
			 			actCode=partnerManager.getAccountCrossReferenceUnique(serviceOrder.getBookingAgentCode(),sessionCorpID);
			 		}
			         accountLine.setActgCode(actCode);	
				}else{
					accountLine.setVendorCode(defaultAccountLine.getVendorCode());
					accountLine.setEstimateVendorName(defaultAccountLine.getVendorName());	
				}
				}
				accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
				accountLine.setContract(billing.getContract());
				 /* Start - 9569 - Issue in 'Ignore Invoicing' */
				  if(defaultAccountLine.getCategories()!=null && accountLine.getCategory().equalsIgnoreCase("Internal Cost")){
					 if(!costElementFlag)	{    
						 chargeCodeList1=chargesManager.findInternalCostChargeCodeChargeCode(defaultAccountLine.getChargeCode(), defaultAccountLine.getCompanyDivision(),sessionCorpID);
					 }else{
						 chargeCodeList1=chargesManager.findInternalCostChargeCodeCostElement(defaultAccountLine.getChargeCode(), defaultAccountLine.getCompanyDivision(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision()); 	 
					 }
				  }else{
				     if(billing.getContract()!=null && !billing.getContract().equalsIgnoreCase("")){
					  if(!costElementFlag)	{
						  chargeCodeList1=chargesManager.findChargeCode(defaultAccountLine.getChargeCode(), serviceOrder.getContract());
					  }
					  else{
						  chargeCodeList1=chargesManager.findChargeCodeCostElement(defaultAccountLine.getChargeCode(), serviceOrder.getContract(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());  
					  }
				    }
				  }
			  if(chargeCodeList1!=null && !chargeCodeList1.isEmpty() && chargeCodeList1.get(0)!=null && !chargeCodeList1.get(0).toString().equalsIgnoreCase("")){
					  try{
								String[] printOnInvoiceArr=chargeCodeList1.get(0).toString().split("#"); 
								if(printOnInvoiceArr.length>6){
								String printOnInvoice=printOnInvoiceArr[6];
								if((printOnInvoice.equalsIgnoreCase("Y"))){
									printOnInvoice="true";
									ignoreForBilling=Boolean.parseBoolean(printOnInvoice);
									accountLine.setIgnoreForBilling(ignoreForBilling);
								}else{
									printOnInvoice="false";
									ignoreForBilling=Boolean.parseBoolean(printOnInvoice);
									accountLine.setIgnoreForBilling(ignoreForBilling);
								}
								}
							}catch(Exception e){
								e.printStackTrace();
				  }
				  if((rollUpInvoiceFlag!=null && !rollUpInvoiceFlag.equalsIgnoreCase("")) && rollUpInvoiceFlag.equalsIgnoreCase("True")){
						try{
							String[] rollUpInInvoiceArr = chargeCodeList1.get(0).toString().split("#");
							if(rollUpInInvoiceArr.length>13){
								String rollUpInInvoice = rollUpInInvoiceArr[13];
								if(rollUpInInvoice.equalsIgnoreCase("Y")){
									rollUpInInvoice="true";
									ignoreForBilling=Boolean.parseBoolean(rollUpInInvoice);
									accountLine.setRollUpInInvoice(ignoreForBilling);									
								}else{
									rollUpInInvoice="false";
									ignoreForBilling=Boolean.parseBoolean(rollUpInInvoice);
									accountLine.setRollUpInInvoice(ignoreForBilling);
								}
							}							
						}catch(Exception e){
							e.printStackTrace();
						}
				   	}
			  }
			  /* --------- End  -------- */
			  accountLineNumber="000";
				 try{
				 autoLineNumber = Long.valueOf(defaultAccountLine.getOrderNumber());
		             if((autoLineNumber.toString()).length() == 2) {
		             	accountLineNumber = "0"+(autoLineNumber.toString());
		             }else if((autoLineNumber.toString()).length() == 1) {
		             	accountLineNumber = "00"+(autoLineNumber.toString());
		             }else {
		             	accountLineNumber=autoLineNumber.toString();
		             }
				 }catch(Exception e){
					 e.printStackTrace();
				 }
				 accountLine.setAccountLineNumber(accountLineNumber);
			    accountLine.setAccountLineNumber(accountLineNumber);
				
				accountLineManager.save(accountLine);
			}
			if(!defaultList.isEmpty())
			{
			  //serviceOrder.setDefaultAccountLineStatus(true);
			  // serviceOrderManager.save(serviceOrder);
			}
			else
			{
				String key =  "defaultAccountLine.massage" ;
				saveMessage(getText(key));
			}
			updateAccountLine(serviceOrder);
			quotationEstimateAccountList();
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String updateAccountLineAjax(ServiceOrder serviceOrder) {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Start");

			//System.out.println("I am in updateAccountLine top");
			BigDecimal cuDivisormar = new BigDecimal(100);
			billingFlag = 1;
			BigDecimal estimateRevSum=new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString());
			BigDecimal distributedAmountSum=new BigDecimal((accountLineManager.getdistributedAmountSum(serviceOrder.getShipNumber())).get(0).toString());
			BigDecimal projectedDistributedTotalAmountSum=new BigDecimal((accountLineManager.getProjectedDistributedTotalAmountSum(serviceOrder.getShipNumber())).get(0).toString());
			BigDecimal projectedActualExpenseSum=new BigDecimal((accountLineManager.getProjectedActualExpenseSum(serviceOrder.getShipNumber())).get(0).toString());
			BigDecimal projectedActualRevenueSum=new BigDecimal((accountLineManager.getProjectedActualRevenueSum(serviceOrder.getShipNumber())).get(0).toString());
			BigDecimal projectedActualRevenueSumHVY=new BigDecimal((accountLineManager.getProjectedActualRevenueSumHVY(serviceOrder.getShipNumber())).get(0).toString());
			BigDecimal estimateExpSum=new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString());
			BigDecimal revisionRevSum=new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString());
			BigDecimal revisionExpSum=new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString());
			BigDecimal entitleSum=new BigDecimal((accountLineManager.getEntitleSum(serviceOrder.getShipNumber())).get(0).toString());
			BigDecimal actualExpSum=new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString());
			BigDecimal actualRevSum=new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString());
			BigDecimal projectedRSum=accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV");
			BigDecimal projectedESum=accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"EXP");
			
			serviceOrder.setEstimatedTotalRevenue(estimateRevSum);
			BigDecimal divisormarEst = estimateRevSum;

			if (serviceOrder.getEstimatedTotalRevenue() == null || serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0"))
					|| serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.0000"))) {
				serviceOrder.setEstimatedGrossMarginPercentage(new BigDecimal("0"));
			} else {
				serviceOrder.setEstimatedGrossMarginPercentage((((estimateRevSum.subtract(estimateExpSum))).multiply(cuDivisormar)).divide(divisormarEst, 2));
			}

			serviceOrder.setEstimatedGrossMargin((estimateRevSum.subtract(estimateExpSum)));
			serviceOrder.setRevisedGrossMargin((revisionRevSum.subtract(revisionExpSum)));
			serviceOrder.setRevisedTotalRevenue(revisionRevSum);
			BigDecimal divisormarEstPer = revisionRevSum;

			if (serviceOrder.getRevisedTotalRevenue() == null || serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0"))
					|| serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.00"))|| serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.0000"))) {
				serviceOrder.setRevisedGrossMarginPercentage(new BigDecimal("0"));
			} else {
				serviceOrder.setRevisedGrossMarginPercentage((((revisionRevSum.subtract(revisionExpSum))).multiply(cuDivisormar)).divide(divisormarEstPer, 2));
			}

			serviceOrder.setRevisedTotalExpense(revisionExpSum);

			serviceOrder.setEstimatedTotalExpense(estimateExpSum);

			serviceOrder.setEntitledTotalAmount(entitleSum);

			serviceOrder.setActualExpense(actualExpSum);
			serviceOrder.setActualRevenue(actualRevSum);

			serviceOrder.setActualGrossMargin((actualRevSum.subtract(actualExpSum)));
			BigDecimal divisormar = actualRevSum;
			if (serviceOrder.getActualRevenue() == null || serviceOrder.getActualRevenue().equals(new BigDecimal("0"))
					|| serviceOrder.getActualRevenue().equals(new BigDecimal("0.00"))|| serviceOrder.getActualRevenue().equals(new BigDecimal("0.0000"))) {
				serviceOrder.setActualGrossMarginPercentage(new BigDecimal("0"));
			} else {
				serviceOrder.setActualGrossMarginPercentage((((actualRevSum.subtract(actualExpSum))).multiply(cuDivisormar)).divide(divisormar, 2));
			}
			/*# 7784 - Always show actual gross margin in accountline overview Start*/
			try{
			serviceOrder.setProjectedGrossMargin(projectedRSum.subtract (projectedESum));
			if(projectedRSum.doubleValue()!=0){
				serviceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((serviceOrder.getProjectedGrossMargin()).doubleValue()/(projectedRSum).doubleValue())*100));
			}else{
				serviceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
			}
			}catch(Exception e){}
			/*# 7784 - Always show actual gross margin in accountline overview End*/
			serviceOrder.setUpdatedOn(new Date());
			serviceOrder.setUpdatedBy(getRequest().getRemoteUser());
			
			serviceOrderManager.updateFromAccountLine(serviceOrder);
			serviceOrder = serviceOrderManager.get(sid);
			//accountLineList = new ArrayList( serviceOrder.getAccountLine() );

			//accountLineList = new ArrayList(serviceOrder.getAccountLines());
			if(accountLineStatus==null||accountLineStatus.equals(""))
			{
				accountLineStatus="true";
			}
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			getSession().setAttribute("accountLines", accountLineList);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}	
	@SkipValidation
	public String updateAccountLine(ServiceOrder serviceOrder) {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Start");

			//System.out.println("I am in updateAccountLine top");
			BigDecimal cuDivisormar = new BigDecimal(100);
			billingFlag = 1;
			serviceOrder.setEstimatedTotalRevenue(new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()));
			BigDecimal divisormarEst = new BigDecimal(((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()));

			if (serviceOrder.getEstimatedTotalRevenue() == null || serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0"))
					|| serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.0000"))) {
				serviceOrder.setEstimatedGrossMarginPercentage(new BigDecimal("0"));
			} else {
				serviceOrder.setEstimatedGrossMarginPercentage((((new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString())
				.subtract(new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(
						divisormarEst, 2));
			}

			serviceOrder.setEstimatedGrossMargin((new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract(new BigDecimal(
					(accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
			serviceOrder.setRevisedGrossMargin((new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract(new BigDecimal(
					(accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
			serviceOrder.setRevisedTotalRevenue(new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()));
			BigDecimal divisormarEstPer = new BigDecimal(((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()));

			if (serviceOrder.getRevisedTotalRevenue() == null || serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0"))
					|| serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.00"))|| serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.0000"))) {
				serviceOrder.setRevisedGrossMarginPercentage(new BigDecimal("0"));
			} else {
				serviceOrder.setRevisedGrossMarginPercentage((((new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString())
				.subtract(new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(
						divisormarEstPer, 2));
			}

			serviceOrder.setRevisedTotalExpense(new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString()));

			serviceOrder.setEstimatedTotalExpense(new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString()));

			serviceOrder.setEntitledTotalAmount(new BigDecimal((accountLineManager.getEntitleSum(serviceOrder.getShipNumber())).get(0).toString()));

			serviceOrder.setActualExpense(new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString()));
			serviceOrder.setActualRevenue(new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()));

			serviceOrder.setActualGrossMargin((new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract(new BigDecimal(
					(accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
			BigDecimal divisormar = new BigDecimal(((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()));
			if (serviceOrder.getActualRevenue() == null || serviceOrder.getActualRevenue().equals(new BigDecimal("0"))
					|| serviceOrder.getActualRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getActualRevenue().equals(new BigDecimal("0.0000"))) {
				serviceOrder.setActualGrossMarginPercentage(new BigDecimal("0"));
			} else {
				serviceOrder
				.setActualGrossMarginPercentage((((new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString())
				.subtract(new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(
						divisormar, 2));
			}
			/*# 7784 - Always show actual gross margin in accountline overview Start*/
			try{
			serviceOrder.setProjectedGrossMargin(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"EXP")));
			if(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").doubleValue()!=0){
				serviceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((serviceOrder.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV")).doubleValue())*100));
			}else{
				serviceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
			}
			}catch(Exception e){}
			/*# 7784 - Always show actual gross margin in accountline overview End*/
			serviceOrderManager.updateFromAccountLine(serviceOrder);
			serviceOrder = serviceOrderManager.get(sid);
			//accountLineList = new ArrayList( serviceOrder.getAccountLine() );

			//accountLineList = new ArrayList(serviceOrder.getAccountLines());
			if(accountLineStatus==null||accountLineStatus.equals(""))
			{
				accountLineStatus="true";
			}
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			getSession().setAttribute("accountLines", accountLineList);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
     public String getMarketArea(){
		
		try {
			if(marketCountryCode.equalsIgnoreCase("United States")){
				marketCountryCode="USA";
			}else if(marketCountryCode.equalsIgnoreCase("United Kingdom")){
				marketCountryCode="UK";
			}
			marketAreaList=serviceOrderManager.getMarketArea(marketCountryCode);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
	  return SUCCESS; 
     }
	@SkipValidation
	public String getMarketPlace(){
		try {
			if(marketCountryCode.equalsIgnoreCase("United States")){
				marketCountryCode="USA";
			}else if(marketCountryCode.equalsIgnoreCase("United Kingdom")){
				marketCountryCode="UK";
			}
			marketPlaceList = serviceOrderManager.findMarketPlace(marketCountryCode);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		return SUCCESS; 
	}
	private List pricingDetailsList= new ArrayList();
	private String corpIdBaseCurrency="";
	private String marketNameDetails="";
	private String marketNotesDetails="";
	private String pricePointMassageDetails;
	private String baseCurrencySysDefault;
	@SkipValidation
	public String findMarketplaceAjax() throws Exception {
		try {
			if(corpIdBaseCurrency.equals("")){
				sysDefaultDetail = billingManager.findDefaultBookingAgentDetail(sessionCorpID);
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						corpIdBaseCurrency=systemDefault.getBaseCurrency();
						baseCurrencySysDefault=systemDefault.getBaseCurrency();
					}
				}	
			}else{
				sysDefaultDetail = billingManager.findDefaultBookingAgentDetail(sessionCorpID);
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						//corpIdBaseCurrency=systemDefault.getBaseCurrency();
						baseCurrencySysDefault=systemDefault.getBaseCurrency();
					}
				}	
			}
			
			if(marketCountryCode.equalsIgnoreCase("United States")){
				marketCountryCode="USA";
			}else if(marketCountryCode.equalsIgnoreCase("United Kingdom")){
				marketCountryCode="UK";
			}
			country = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			marketAreaList=serviceOrderManager.getMarketArea(marketCountryCode);
			String pricingResponse=""; 
			String weight_unit="";
			String volume_unit="";
			BigDecimal soWeight=new BigDecimal("0.0");
			BigDecimal soVolume=new BigDecimal("0.0");
			String clientID="";
			Integer client_id=0;
			String pricingForMode="";
			if(jobType!=null && jobType.equalsIgnoreCase("STO")){
				pricingForMode="PS";
			}else{			
			if(pricingMode!=null && !pricingMode.equals("") && ! pricingMode.equalsIgnoreCase("Sea")){
				List modeValue = serviceOrderManager.findPriceCode(pricingMode, "MODE",sessionCorpID);
				 if (modeValue != null && !modeValue.isEmpty() && modeValue.get(0)!=null) {
					 pricingForMode=modeValue.get(0).toString();
				 }
			}		
			if(pricingMode!=null && !pricingMode.equals("") && pricingMode.equalsIgnoreCase("Sea")){
				if(packingModePricing!=null && !packingModePricing.equals("")){
					if(packingModePricing.equals("RORO") || packingModePricing.equals("GRP") || packingModePricing.equals("BKBLK") || packingModePricing.equals("LCL") || packingModePricing.equals("LVAN")  || packingModePricing.equals("T/W")){
					      pricingForMode="LCL";
					     }
					     if(packingModePricing.equals("LOOSE") || packingModePricing.equals("LVCO") || packingModePricing.equals("LVCNT") || packingModePricing.equals("LVCNTR") ){
					      pricingForMode="FCL_L";
					     } 
				}
			}
			}
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			if(company!=null){
				clientID=company.getClientID();
			}
			if(clientID!=null && !clientID.equalsIgnoreCase("")){
				client_id=Integer.parseInt(clientID);
			}
			Map<String, String> marketplaceMap = new TreeMap<String, String>();
			if(pricingStorageUnit!=null && !pricingStorageUnit.equalsIgnoreCase("")){
				weight_unit=pricingStorageUnit.substring(0, 3);
				if(weight_unit.equalsIgnoreCase("lbs")){
					weight_unit="lb";
				}else if(weight_unit.equalsIgnoreCase("kgs")){
					weight_unit="kg";
				}
				volume_unit=pricingStorageUnit.substring(3, 6);
				if(volume_unit.equalsIgnoreCase("cbm")){
					volume_unit="mt";
				}
					
			}
			if(packService.equalsIgnoreCase("Origin")){
				packService="origin";
			}else if(packService.equalsIgnoreCase("Destin")){
				packService="destination";
			}else{
				packService="";	
			}
			marketAreaCode=marketArea;
			Long marketId=Long.parseLong(marketArea);
			if(pricingWeight!=null && !pricingWeight.equals("")){
			 soWeight= new BigDecimal(pricingWeight);
			}
			if(pricingVolume!=null && !pricingVolume.equals("")){
			 soVolume= new BigDecimal(pricingVolume);
			}
      try {
				
				JSONObject obj = new JSONObject();
				obj.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
				obj.put("client", client_id);
				obj.put("market", marketId);
				JSONObject obj1 = new JSONObject();
				obj1.put("type", pricingForMode);
				obj1.put("weight", soWeight);
				obj1.put("volume", soVolume);
				
				/* Map map = new HashMap();
				 map.put("type", pricingMode);
				 map.put("weight", soWeight);
				 map.put("volume", soVolume);*/
				JSONArray list = new JSONArray();
				      // list.put(map);
				       list.put(obj1);
				
				 obj.accumulate("shipments", list);
				 obj.put("currency", corpIdBaseCurrency);
				 obj.put("service", packService);
				 obj.put("weight_unit", weight_unit);
				 obj.put("volume_unit", volume_unit);
				 System.out.println("********  REQUEST JSON - Quote / SO # ************"+obj.toString());
				 
				// String input="{\"client\":2,\"market\":"+marketId+",\"shipments\":[ \"type\":"+pricingMode+",\"weight\":"+pricingWeight+",\"volume\":"+pricingVolume+" ] ,\"currency\":USD,\"service\":"+packService+",\"weight_unit\":"+weight_unit+",\"volume_unit\":"+volume_unit+"}";
				 pricingResponse=PricePointClientServiceCall.putDataToServer(obj.toString());
					JSONObject jsr = new JSONObject(pricingResponse);
					JSONArray content = jsr.getJSONArray("quotes");
					for(int n = 0; n < content.length(); n++){
					 JSONObject record = content.getJSONObject(n);
					    String id=record.getString("id");
						String currency=record.getString("currency");
						String total=record.getString("total");
						String name=record.getString("name");
						String detailsQuotes="";
						Integer marketagent=0;
						String price="";
						String currencyde="";
						marketagent=Integer.parseInt(id);
						JSONObject objde = new JSONObject();
						objde.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
						objde.put("client", client_id);
						objde.put("market", marketId);
						objde.put("agent", marketagent);
						JSONObject objde1 = new JSONObject();
						objde1.put("type", pricingForMode);
						objde1.put("weight", soWeight);
						objde1.put("volume", soVolume);
						JSONArray listde = new JSONArray();
						       listde.put(objde1);		
						 objde.accumulate("shipments", listde);
						 objde.put("currency", corpIdBaseCurrency);
						 objde.put("service", packService);
						 objde.put("weight_unit", weight_unit);
						 objde.put("volume_unit", volume_unit);
						 System.out.println(objde.toString());
						 detailsQuotes=PricePointClientServiceCall.detailedQuotes(objde.toString());
						 JSONObject jsrde = new JSONObject(detailsQuotes);
							JSONArray contentde = jsrde.getJSONArray("quotes");
							for(int i = 0; i < contentde.length(); i++){
							JSONObject recordde = contentde.getJSONObject(i);
							   price=recordde.getString("price");						 
							   currencyde=recordde.getString("currency");						
							}                     
						String abc=currency+"~"+total+"~"+name+"~"+price+"~"+currencyde;
						marketplaceMap.put(id, abc);
					    // do some stuff....
					}
					JSONObject markets = jsr.getJSONObject("market");
					marketNameDetails=markets.getString("name");
					marketNotesDetails=markets.getString("notes");
				/*	Iterator keys = markets.keys();
					while (keys.hasNext()) {
						String key = (String) keys.next();
						JSONObject record = markets.getJSONObject(key);
					
						 //marketplaceMap.put(id, name);
					}*/
				 
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				pricePointMassageDetails="There was a error getting Pricing details from Price Point "+e;	
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				pricePointMassageDetails="There was a error getting Pricing details from Price Point "+e;
			}		
pricingDetailsList=serviceOrderManager.getPricingDetails(marketplaceMap,pricingWeight,pricingVolume,pricingMode,sessionCorpID,pricingStorageUnit,marketArea ,corpIdBaseCurrency,baseCurrencySysDefault);
			if(pricingDetailsList==null || pricingDetailsList.isEmpty()){
				pricePointMassageDetails="There were no results returned from Price Point";	
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
	return SUCCESS; 	
	}
	private String marketAreaCode;
	private String MarketagentId;
	private String marketAreaName;
	Map<String, String> detailsQuotesValue = new TreeMap<String, String>();
	private String weight_unit="";
	private String volume_unit="";
	private String SupplementRate="";
	private String SupplementType="";
	private String SupplementRate_basis="";
	
	private String pricePointShipNumber;
	private String pricePointAgent;
	private String pricePointWeightUnit;
	private String pricePointVolumeUnit;
	private String pricePointMinimum_density;
	private String pricePointTariff_published;
	private String pricePointRate;
	private String pricePoint_price;
	private String pricePoint_cctotal;
	private String pricePoint_thc;
	private String pricePoint_currency;
	private String pricePoint_rate_basis;
	private String pricePoint_WeightUnit;
	private String pricePoint_VolumeUnit;
	private String pricePointTariff;
	private String agentConsignment="";
	private String agentAdditional="";
	private String agentcustoms="";
	private String originResidence="";
	private String cctotalValue="";
	@SkipValidation
	public String findDetailsQuotesCharges() throws Exception{	
		try {
			if(corpIdBaseCurrency.equals("")){
				sysDefaultDetail = billingManager.findDefaultBookingAgentDetail(sessionCorpID);
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						corpIdBaseCurrency=systemDefault.getBaseCurrency();
					}
				}	
			}
			if(marketCountryCode.equalsIgnoreCase("United States")){
				marketCountryCode="USA";
			}else if(marketCountryCode.equalsIgnoreCase("United Kingdom")){
				marketCountryCode="UK";
			}
			country = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			marketAreaList=serviceOrderManager.getMarketArea(marketCountryCode);
			try {
			serviceOrder=serviceOrderManager.get(Long.parseLong(soId));
			pricePointShipNumber=serviceOrder.getShipNumber();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String detailsQuotes="";	
			BigDecimal soWeight=new BigDecimal("0.0");
			BigDecimal soVolume=new BigDecimal("0.0");
			String clientID="";
			Integer client_id=0;
			Integer marketagent=0;
			String pricingForMode="";
			if(jobType!=null && jobType.equalsIgnoreCase("STO")){
				pricingForMode="PS";
			}else{
			if(pricingMode!=null && !pricingMode.equals("") && ! pricingMode.equalsIgnoreCase("Sea")){
				List modeValue = serviceOrderManager.findPriceCode(pricingMode, "MODE",sessionCorpID);
				 if (modeValue != null && !modeValue.isEmpty() && modeValue.get(0)!=null) {
					 pricingForMode=modeValue.get(0).toString();
				 }
			}		
			if(pricingMode!=null && !pricingMode.equals("") && pricingMode.equalsIgnoreCase("Sea")){
				if(packingModePricing!=null && !packingModePricing.equals("")){
					if(packingModePricing.equals("RORO") || packingModePricing.equals("GRP") || packingModePricing.equals("BKBLK") || packingModePricing.equals("LCL") || packingModePricing.equals("LVAN")  || packingModePricing.equals("T/W")){
					      pricingForMode="LCL";
					     }
					     if(packingModePricing.equals("LOOSE") || packingModePricing.equals("LVCO") || packingModePricing.equals("LVCNT") || packingModePricing.equals("LVCNTR") ){
					      pricingForMode="FCL_L";
					     } 
				}
			}
			}
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			if(company!=null){
				clientID=company.getClientID();
			}
			if(clientID!=null && !clientID.equalsIgnoreCase("")){
				client_id=Integer.parseInt(clientID);
			}	
			if(pricingStorageUnit!=null && !pricingStorageUnit.equalsIgnoreCase("")){
				weight_unit=pricingStorageUnit.substring(0, 3);
				if(weight_unit.equalsIgnoreCase("lbs")){
					weight_unit="lb";
				}else if(weight_unit.equalsIgnoreCase("kgs")){
					weight_unit="kg";
				}
				volume_unit=pricingStorageUnit.substring(3, 6);
				if(volume_unit.equalsIgnoreCase("cbm")){
					volume_unit="mt";
				}
					
			}
			if(packService.equalsIgnoreCase("Origin")){
				packService="origin";
			}else if(packService.equalsIgnoreCase("Destin")){
				packService="destination";
			}else{
				packService="";	
			}
			Long marketId=Long.parseLong(marketAreaCode);
			if(pricingWeight!=null && !pricingWeight.equals("")){
			 soWeight= new BigDecimal(pricingWeight);
			}
			if(pricingVolume!=null && !pricingVolume.equals("")){
			 soVolume= new BigDecimal(pricingVolume);
			}
			if(MarketagentId !=null && ! MarketagentId.equals("")){
				marketagent=Integer.parseInt(MarketagentId);
			}
			JSONObject obj = new JSONObject();
			obj.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
			obj.put("client", client_id);
			obj.put("market", marketId);
			obj.put("agent", marketagent);
			JSONObject obj1 = new JSONObject();
			obj1.put("type", pricingForMode);
			obj1.put("weight", soWeight);
			obj1.put("volume", soVolume);
			JSONArray list = new JSONArray();
			     list.put(obj1);		
			 obj.accumulate("shipments", list);
			 obj.put("currency", corpIdBaseCurrency);
			 obj.put("service", packService);
			 obj.put("weight_unit", weight_unit);
			 obj.put("volume_unit", volume_unit);
			 System.out.println(obj.toString());
			 detailsQuotes=PricePointClientServiceCall.detailedQuotes(obj.toString());
			 JSONObject jsr = new JSONObject(detailsQuotes);
				JSONArray content = jsr.getJSONArray("quotes");
				for(int n = 0; n < content.length(); n++){
				JSONObject record = content.getJSONObject(n);			
				   String chargeable_volume=record.getString("chargeable_volume");
				   if(chargeable_volume!=null && !chargeable_volume.equals("")){
					   detailsQuotesValue.put("chargeable_volume", chargeable_volume);
					   pricePointVolumeUnit=chargeable_volume;
				   }
				   String chargeable_weight=record.getString("chargeable_weight");
				   if(chargeable_weight!=null && !chargeable_weight.equals("")){
					   detailsQuotesValue.put("chargeable_weight", chargeable_weight);  
					   pricePointWeightUnit=chargeable_weight;
				   }
				   String minimum_density=record.getString("minimum_density");
				   if(minimum_density!=null && !minimum_density.equals("")){
					   detailsQuotesValue.put("minimum_density", minimum_density); 
					   pricePointMinimum_density=minimum_density;
				   }
				   String tariff_published=record.getString("tariff_published");
				   if(tariff_published!=null && !tariff_published.equals("")){
					   detailsQuotesValue.put("tariff_published", tariff_published); 
					   pricePointTariff_published=tariff_published;
				   }
				   String tariff=record.getString("tariff");
				   if(tariff!=null && !tariff.equals("")){
					   //detailsQuotesValue.put("tariff", tariff_published); 
					   pricePointTariff=tariff;
				   }
				   String rate=record.getString("rate");
				   if(rate!=null && !rate.equals("")){
					   detailsQuotesValue.put("rate", rate);  
					   pricePointRate=rate;
				   }			
				   String thc=record.getString("thc");
				   if(thc!=null && !thc.equals("")){
					   detailsQuotesValue.put("thc", thc);  
					   pricePoint_thc=thc;
				   }
				   String rate_basis=record.getString("rate_basis");
				   if(rate_basis!=null && !rate_basis.equals("")){
					   detailsQuotesValue.put("rate_basis", rate_basis);
					   pricePoint_rate_basis=rate_basis;
				   }
				   String price=record.getString("price");
				   if(price!=null && !price.equals("")){
					   detailsQuotesValue.put("price", price); 
					   pricePoint_price=price;
				   }
				   String cctotal=record.getString("cctotal");
				   if(cctotal!=null && !cctotal.equals("")){
					   detailsQuotesValue.put("cctotal", cctotalValue); 
					   pricePoint_cctotal=cctotalValue;
				   }
				
				   if(pricingWeight!=null && !pricingWeight.equals("")){
					   detailsQuotesValue.put("pricingWeight", pricingWeight);   
				   }
				   if(pricingVolume!=null && !pricingVolume.equals("")){
					   detailsQuotesValue.put("pricingVolume", pricingVolume);   
				   }
				   if(packService!=null && !packService.equals("")){
					   if(packService.equalsIgnoreCase("origin")){
						   detailsQuotesValue.put("packService", "Origin");    
					   }else{
						   detailsQuotesValue.put("packService", "Destination"); 
					   }
					   
				   }
				   if(pricingForMode!=null && !pricingForMode.equals("")){
					   detailsQuotesValue.put("pricingForMode", pricingForMode);   
				   }
				   String currency=record.getString("currency");			  
				   if(currency!=null && !currency.equals("")){
					   detailsQuotesValue.put("currency", currency); 
					   pricePoint_currency=currency;
				   }
				   if(serviceOrder!=null && serviceOrder.getLastName()!=null && !serviceOrder.getLastName().equals("")){
					   if(serviceOrder.getFirstName()!=null && !serviceOrder.getFirstName().equals("")){
						   detailsQuotesValue.put("Transferee", serviceOrder.getFirstName()+" "+serviceOrder.getLastName());  
					   }else{
						   detailsQuotesValue.put("Transferee", serviceOrder.getLastName());
					   }
					  
				   }
				   if(serviceOrder!=null && serviceOrder.getStatus()!=null && !serviceOrder.getStatus().equals("")){
					   detailsQuotesValue.put("Status", serviceOrder.getStatus());
				   }			   
				   if(serviceOrder!=null && serviceOrder.getDestinationCountryCode()!=null && !serviceOrder.getDestinationCountryCode().equals("")){
					   if(serviceOrder.getDestinationCity()!=null && !serviceOrder.getDestinationCity().equals("")){
						   detailsQuotesValue.put("Residence", serviceOrder.getDestinationCountryCode()+"-"+serviceOrder.getDestinationCity()); 
					   }else{
						   detailsQuotesValue.put("Residence", serviceOrder.getDestinationCountryCode());
					   }				   
				   }			  
				   if(serviceOrder!=null && serviceOrder.getOriginCountryCode()!=null && !serviceOrder.getOriginCountryCode().equals("") ){
					   if(serviceOrder.getOriginCity()!=null && !serviceOrder.getOriginCity().equals("") ){
						   detailsQuotesValue.put("originResidence", serviceOrder.getOriginCountryCode()+"-"+serviceOrder.getOriginCity());   
					   }else{
						   detailsQuotesValue.put("originResidence", serviceOrder.getOriginCountryCode()); 
					   }
					   
				   }
				   if(serviceOrder!=null && serviceOrder.getDestinationCity()!=null && !serviceOrder.getDestinationCity().equals("")){
					   detailsQuotesValue.put("PortOfArrival", serviceOrder.getDestinationCity());
				   }
				   if(packService!=null && !packService.equals("")){
					   detailsQuotesValue.put("packService", packService);
				   }
				   if(serviceOrder!=null && !serviceOrder.getShipNumber().equals("")){
					   detailsQuotesValue.put("ShipNumber", serviceOrder.getShipNumber());				   
				   }
				   if(marketagent!=null && !marketagent.toString().equals("")){
					   detailsQuotesValue.put("marketagent", marketagent.toString());
				   }
				   if(corpIdBaseCurrency!=null && !corpIdBaseCurrency.toString().equals("")){
					   detailsQuotesValue.put("corpIdBaseCurrency", corpIdBaseCurrency);
				   }	
				   if(marketAreaName!=null && !marketAreaName.toString().equals("")){
					   detailsQuotesValue.put("marketAreaName", marketAreaName);
				   }
				   String weightUnit=record.getString("weight_unit");
				   if(weightUnit!=null && !weightUnit.equals("")){
					   detailsQuotesValue.put("weightUnit", weightUnit); 
					   pricePoint_WeightUnit=weightUnit;
				   }
				   String volumeUnit=record.getString("volume_unit");
				   if(volumeUnit!=null && !volumeUnit.equals("")){
					   detailsQuotesValue.put("volumeUnit", volumeUnit);  
					   pricePoint_VolumeUnit=volumeUnit;
				   }
				   String name=record.getString("name");
				   if(name!=null && !name.equals("")){
					   detailsQuotesValue.put("name", name);  
					   pricePointAgent=name;
				   }			  			
				}
				
				 try {
						String pricingSupplement="";
						JSONObject obj12 = new JSONObject();
						obj12.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
						obj12.put("agent", marketagent);
						obj12.put("market", marketId);
						System.out.println("********  REQUEST JSON - Quote / SO # ************"+obj12.toString());
						pricingSupplement=PricePointClientServiceCall.getSupplementCharges(obj.toString());
						PricingDetailsValue dto = null;
						JSONObject jsr12 = new JSONObject(pricingSupplement);
						JSONArray content12 = jsr12.getJSONArray("supplements");
						for(int n = 0; n < content12.length(); n++){
						JSONObject record12 = content12.getJSONObject(n);
						String rate=record12.getString("rate");
						if(!SupplementRate.equals("")){
							SupplementRate=SupplementRate+"~"+rate;
						}else{
							SupplementRate=rate;
						}
						String type=record12.getString("type");
						if(!SupplementType.equals("")){
							SupplementType=SupplementType+"~"+type;
						}else{
							SupplementType=type;
						}
						String basis=record12.getString("rate_basis");
						if(!SupplementRate_basis.equals("")){
							SupplementRate_basis=SupplementRate_basis+"~"+basis;
						}else{
							SupplementRate_basis=basis;
						}
						dto = new PricingDetailsValue();
						dto.setRate(rate);
						dto.setType(type);
						dto.setBasis(basis);
						supplementQuotesList.add(dto);
						}
						JSONObject agent = jsr12.getJSONObject("agent");
						JSONObject info = agent.getJSONObject("info");
						agentConsignment=info.getString("consignment");
						agentAdditional=info.getString("additional");
						agentcustoms=info.getString("customs");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		
		return SUCCESS;
	}
	@SkipValidation
   public String  findQuoteDetailsInvoice() throws Exception {
		try {
			if(corpIdBaseCurrency.equals("")){
				sysDefaultDetail = billingManager.findDefaultBookingAgentDetail(sessionCorpID);
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						corpIdBaseCurrency=systemDefault.getBaseCurrency();
					}
				}	
			}
			if(marketCountryCode.equalsIgnoreCase("United States")){
				marketCountryCode="USA";
			}else if(marketCountryCode.equalsIgnoreCase("United Kingdom")){
				marketCountryCode="UK";
			}
			//country = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			//marketAreaList=serviceOrderManager.getMarketArea(marketCountryCode);
			try {
			//serviceOrder=serviceOrderManager.get(Long.parseLong(soId));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String detailsQuotes="";	
			BigDecimal soWeight=new BigDecimal("0.0");
			BigDecimal soVolume=new BigDecimal("0.0");
			String clientID="";
			Integer client_id=0;
			Integer marketagent=0;
			String pricingForMode="";
			if(jobType!=null && jobType.equalsIgnoreCase("STO")){
				pricingForMode="PS";
			}else{
			if(pricingMode!=null && !pricingMode.equals("") && ! pricingMode.equalsIgnoreCase("Sea")){
				List modeValue = serviceOrderManager.findPriceCode(pricingMode, "MODE",sessionCorpID);
				 if (modeValue != null && !modeValue.isEmpty() && modeValue.get(0)!=null) {
					 pricingForMode=modeValue.get(0).toString();
				 }
			}		
			if(pricingMode!=null && !pricingMode.equals("") && pricingMode.equalsIgnoreCase("Sea")){
				if(packingModePricing!=null && !packingModePricing.equals("")){
					if(packingModePricing.equals("RORO") || packingModePricing.equals("GRP") || packingModePricing.equals("BKBLK") || packingModePricing.equals("LCL") || packingModePricing.equals("LVAN")  || packingModePricing.equals("T/W")){
					      pricingForMode="LCL";
					     }
					     if(packingModePricing.equals("LOOSE") || packingModePricing.equals("LVCO") || packingModePricing.equals("LVCNT") || packingModePricing.equals("LVCNTR") ){
					      pricingForMode="FCL_L";
					     } 
				}
			}
			}
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			if(company!=null){
				clientID=company.getClientID();
			}
			if(clientID!=null && !clientID.equalsIgnoreCase("")){
				client_id=Integer.parseInt(clientID);
			}	
			if(pricingStorageUnit!=null && !pricingStorageUnit.equalsIgnoreCase("")){
				weight_unit=pricingStorageUnit.substring(0, 3);
				if(weight_unit.equalsIgnoreCase("lbs")){
					weight_unit="lb";
				}else if(weight_unit.equalsIgnoreCase("kgs")){
					weight_unit="kg";
				}
				volume_unit=pricingStorageUnit.substring(3, 6);
				if(volume_unit.equalsIgnoreCase("cbm")){
					volume_unit="mt";
				}
					
			}
			if(packService.equalsIgnoreCase("Origin")){
				packService="origin";
			}else if(packService.equalsIgnoreCase("Destin")){
				packService="destination";
			}else{
				packService="";	
			}
			Long marketId=Long.parseLong(marketAreaCode);
			if(pricingWeight!=null && !pricingWeight.equals("")){
			 soWeight= new BigDecimal(pricingWeight);
			}
			if(pricingVolume!=null && !pricingVolume.equals("")){
			 soVolume= new BigDecimal(pricingVolume);
			}
			if(MarketagentId !=null && ! MarketagentId.equals("")){
				marketagent=Integer.parseInt(MarketagentId);
			}
			JSONObject obj = new JSONObject();
			obj.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
			obj.put("client", client_id);
			obj.put("market", marketId);
			obj.put("agent", marketagent);
			JSONObject obj1 = new JSONObject();
			obj1.put("type", pricingForMode);
			obj1.put("weight", soWeight);
			obj1.put("volume", soVolume);
			JSONArray list = new JSONArray();
			     list.put(obj1);		
			 obj.accumulate("shipments", list);
			 obj.put("currency", corpIdBaseCurrency);
			 obj.put("service", packService);
			 obj.put("weight_unit", weight_unit);
			 obj.put("volume_unit", volume_unit);
			 System.out.println("********  REQUEST JSON - Quote / SO # ************"+obj.toString());
			 detailsQuotes=PricePointClientServiceCall.detailedQuotes(obj.toString());
			 JSONObject jsr = new JSONObject(detailsQuotes);
				JSONArray content = jsr.getJSONArray("quotes");
				for(int n = 0; n < content.length(); n++){
				JSONObject record = content.getJSONObject(n);			
				   
				   String price=record.getString("price");
				   if(price!=null && !price.equals("")){
					   detailsQuotesValue.put("price", price); 
					   pricePoint_price=price;
				   }
				   String tariff=record.getString("tariff");
				   if(tariff!=null && !tariff.equals("")){
					   //detailsQuotesValue.put("tariff", tariff_published); 
					   pricePointTariff=tariff;
				   }
				  
				}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
			
			return SUCCESS;
	}
	private List supplementQuotesList= new ArrayList();
	@SkipValidation
	public String saveRequestedQuote(String pricePointAgentId,String pricePointMarket,String pricePointTariff,String category,Long id,Long AccId,ServiceOrder serviceOrder,Miscellaneous miscellaneous,List modeValue,Company company){
		    //serviceOrder=serviceOrderManager.get(id);
		    //miscellaneous = miscellaneousManager.get(id);
			if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
    			weightType="lbscft";
    		}
    		else{
    			weightType="kgscbm";
    		}
			String pricingForMode="";
			if(serviceOrder.getJob()!=null && serviceOrder.getJob().equalsIgnoreCase("STO")){
				pricingForMode="PS";
			}else{
			if(serviceOrder.getMode()!=null && !serviceOrder.getMode().equals("") && !serviceOrder.getMode().equalsIgnoreCase("Sea")){
				//List modeValue = serviceOrderManager.findPriceCode(serviceOrder.getMode(), "MODE",sessionCorpID);
				 if (modeValue != null && !modeValue.isEmpty() && modeValue.get(0)!=null) {
					 pricingForMode=modeValue.get(0).toString();
				 }
			}		
	        if(serviceOrder.getMode()!=null && !serviceOrder.getMode().equals("") && serviceOrder.getMode().equalsIgnoreCase("Sea")){
				if(serviceOrder.getPackingMode()!=null && !serviceOrder.getPackingMode().equals("")){
					if(serviceOrder.getPackingMode().equals("RORO") || serviceOrder.getPackingMode().equals("GRP") || serviceOrder.getPackingMode().equals("BKBLK") || serviceOrder.getPackingMode().equals("LCL") || serviceOrder.getPackingMode().equals("LVAN")  || serviceOrder.getPackingMode().equals("T/W")){
					      pricingForMode="LCL";
					     }
					     if(serviceOrder.getPackingMode().equals("LOOSE") || serviceOrder.getPackingMode().equals("LVCO") || serviceOrder.getPackingMode().equals("LVCNT") || serviceOrder.getPackingMode().equals("LVCNTR") ){
					      pricingForMode="FCL_L";
					     } 
				}
			}
			}
			String clientID="";
			Integer client_id=0;
			//company= companyManager.findByCorpID(sessionCorpID).get(0);
			if(company!=null){
				clientID=company.getClientID();
			}
			if(clientID!=null && !clientID.equalsIgnoreCase("")){
				client_id=Integer.parseInt(clientID);
			}
			if(weightType!=null && !weightType.equalsIgnoreCase("")){
				weight_unit=weightType.substring(0, 3);
				if(weight_unit.equalsIgnoreCase("lbs")){
					weight_unit="lb";
				}else if(weight_unit.equalsIgnoreCase("kgs")){
					weight_unit="kg";
				}
				volume_unit=weightType.substring(3, 6);
				if(volume_unit.equalsIgnoreCase("cbm")){
					volume_unit="mt";
				}
					
			}		
			if(category.equalsIgnoreCase("Origin")){
				packService="origin";
			}else if(category.equalsIgnoreCase("Destin")){
				packService="destination";
			}else{
				packService="";	
			}
			Long marketId=Long.parseLong(pricePointMarket);
			BigDecimal soWeight=new BigDecimal("0.0");
			BigDecimal soVolume=new BigDecimal("0.0");
			
			if(weightType.equalsIgnoreCase("lbscft") && miscellaneous.getEstimatedNetWeight()!=null && !miscellaneous.getEstimatedNetWeight().toString().equals("") && miscellaneous.getNetEstimateCubicFeet()!=null && !miscellaneous.getNetEstimateCubicFeet().toString().equals("") ){
				 soWeight= miscellaneous.getEstimatedNetWeight();
				 soVolume= miscellaneous.getNetEstimateCubicFeet();
				}
		   if(weightType.equalsIgnoreCase("kgscbm") && miscellaneous.getEstimatedNetWeightKilo()!=null && !miscellaneous.getEstimatedNetWeightKilo().toString().equals("") && miscellaneous.getNetEstimateCubicMtr()!=null && !miscellaneous.getNetEstimateCubicMtr().toString().equals("")){
					 soWeight= miscellaneous.getEstimatedNetWeightKilo();
					 soVolume= miscellaneous.getNetEstimateCubicMtr();
			   }
				Integer marketagent=0;
				if(pricePointAgentId!=null && !pricePointAgentId.equals("")){
					marketagent=Integer.parseInt(pricePointAgentId);
				}
				Integer Tariff=0;
				if(pricePointTariff !=null && ! pricePointTariff.equals("")){
					Tariff=Integer.parseInt(pricePointTariff.trim());
				}
			String transferee="";
			if(serviceOrder.getFirstName()!=null && !serviceOrder.getFirstName().equals("") && serviceOrder.getLastName()!=null && !serviceOrder.getLastName().equals("") ){
				transferee=serviceOrder.getFirstName()+" "+serviceOrder.getLastName();
			}else{
				transferee=serviceOrder.getFirstName();
			}
			String quoteStatus="";
/*			if(serviceOrder.getStatus().equalsIgnoreCase("Accept")){
				quoteStatus="Booked";
			}else if(serviceOrder.getStatus().equalsIgnoreCase("Merge")){
				quoteStatus="Inherit the status of the merged order";
			}else if(serviceOrder.getStatus().equalsIgnoreCase("Pending")){
				quoteStatus="Pending";
			}else if(serviceOrder.getStatus().equalsIgnoreCase("Reject")){
				quoteStatus="Cancelled";
			}else{
				quoteStatus=serviceOrder.getStatus();
			}*/
			if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("PE")){
				quoteStatus="Pending";
			}else if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("AC")){
				quoteStatus="Booked";
			}else if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("RE")){
				quoteStatus="Cancelled";
			}else if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("SU")){
				quoteStatus="Complete";
			}else{
				quoteStatus="Pending";
			}	
			String priceId="";
		     try {
				String pricingSupplement="";
				JSONObject obj = new JSONObject();
				obj.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
				obj.put("client", client_id);
				obj.put("market", marketId);
				obj.put("agent", marketagent);
				JSONObject obj1 = new JSONObject();
				obj1.put("type", pricingForMode);
				obj1.put("weight", soWeight);
				obj1.put("volume", soVolume);
				obj1.put("tariff", Tariff);
				JSONArray list = new JSONArray();
				     list.put(obj1);		
				 obj.accumulate("shipments", list);				
				 obj.put("service", packService);
				 obj.put("weight_unit", weight_unit);
				 obj.put("volume_unit", volume_unit);
				 obj.put("transferee", transferee);
				 obj.put("status", quoteStatus);
				System.out.println("********  REQUEST JSON - Quote / SO # ************"+obj.toString());
				pricingSupplement=PricePointClientServiceCall.saveQuotesDetails(obj.toString());
				PricingDetailsValue dto = null;
				JSONObject jsr = new JSONObject(pricingSupplement);				
				for(int n = 0; n < jsr.length(); n++){
				JSONObject record = jsr;
				String status=record.getString("status");
				priceId=record.getString("id");								
				}
				//serviceOrderManager.tariffIdUpdate(AccId,priceId,sessionCorpID);
			} catch (JSONException e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	e.printStackTrace();
			}
			return priceId;
	}
	@SkipValidation
	public String pricepointReadDetails(BigDecimal soWeight,BigDecimal soVolume,String transferee,String Status,String tariffId){	
		//corpIdBaseCurrency
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				corpIdBaseCurrency=systemDefault.getBaseCurrency();	            
			}
		}
		Integer accTariffId=0;
		if(tariffId!=null && !tariffId.equals("")){
			accTariffId=Integer.parseInt(tariffId);
		}
		String readInfo="";
		String readDetails="";
		String readTransferee="";
		String readStatus="";
		String readRefId="";
		BigDecimal readWeight=new BigDecimal("0.0");
		BigDecimal readVolume=new BigDecimal("0.0");
		try {
			JSONObject obj = new JSONObject();
			obj.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
			obj.put("id", accTariffId);
			obj.put("currency", corpIdBaseCurrency);
			System.out.println(obj.toString());
			readDetails=PricePointClientServiceCall.readQuotesDetails(obj.toString());
			JSONObject jsr = new JSONObject(readDetails);
			for(int n = 0; n < jsr.length(); n++){
				JSONObject record = jsr;
				readTransferee=record.getString("transferee");
				readStatus=record.getString("status");
				}
			JSONArray content = jsr.getJSONArray("quotes");
			for(int n = 0; n < content.length(); n++){
			JSONObject record = content.getJSONObject(n);
			   String pricingWeight=record.getString("declared_weight");
			   readWeight= new BigDecimal(pricingWeight);
			   String pricingVolume=record.getString("declared_volume");
			   readVolume= new BigDecimal(pricingVolume);	
			   readRefId=record.getString("id");	
			  
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		
		if(readStatus.equalsIgnoreCase("Booked")){
			readStatus="Accept";
		}else if(readStatus.equalsIgnoreCase("Inherit the status of the merged order")){
			readStatus="Merge";
		}else if(readStatus.equalsIgnoreCase("Pending")){
			readStatus="Pending";
		}else if(readStatus.equalsIgnoreCase("Cancelled")){
			readStatus="Reject";
		}
		if(soWeight.compareTo(readWeight)!=0){
			readInfo=readRefId;
		}else if(soVolume.compareTo(readVolume)!=0){
			readInfo=readRefId;
		}else if(!readTransferee.equalsIgnoreCase(transferee)){
			readInfo=readRefId;
		}else if(!Status.equalsIgnoreCase(readStatus)){
			readInfo=readRefId;
		}		
		return readInfo;
	}
	@SkipValidation
   public String saveToCreateTariffId(String TariffId,String category,String soJob,String Somode,String packMode){
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				corpIdBaseCurrency=systemDefault.getBaseCurrency();	            
			}
		}
		String pricingForMode="";
		if(soJob!=null && soJob.equalsIgnoreCase("STO")){
			pricingForMode="PS";
		}else{
		if(Somode!=null && !Somode.equals("") && !Somode.equalsIgnoreCase("Sea")){
			List modeValue = serviceOrderManager.findPriceCode(Somode, "MODE",sessionCorpID);
			 if (modeValue != null && !modeValue.isEmpty() && modeValue.get(0)!=null) {
				 pricingForMode=modeValue.get(0).toString();
			 }
		}		
        if(Somode!=null && !Somode.equals("") && Somode.equalsIgnoreCase("Sea")){
			if(packMode!=null && !packMode.equals("")){
				if(packMode.equals("RORO") || packMode.equals("GRP") || packMode.equals("BKBLK") || packMode.equals("LCL") || packMode.equals("LVAN")  || packMode.equals("T/W")){
				      pricingForMode="LCL";
				     }
				     if(packMode.equals("LOOSE") || packMode.equals("LVCO") || packMode.equals("LVCNT") || packMode.equals("LVCNTR") ){
				      pricingForMode="FCL_L";
				     } 
			}
		}
		}
		Integer refTariffId=0;
		if(TariffId!=null && !TariffId.equals("")){
		 refTariffId=Integer.parseInt(TariffId);
		}
	    String readDetails="";
	    String readMarketId="";
	    String readAgent="";
	    String readSoWeight="";
	    String readSoVolume="";
	    String readTariff="";
	    String readVolume_unit="";
	    String readWeight_unit="";
	    String readTransferee="";
	    String readStatus="";
	    String priceId="";
		JSONObject obj = new JSONObject();
		try {
			obj.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
			obj.put("id", refTariffId);
			obj.put("currency", corpIdBaseCurrency);
			System.out.println(obj.toString());
			readDetails = PricePointClientServiceCall.readQuotesDetails(obj.toString());
			JSONObject jsr = new JSONObject(readDetails);
			for(int n = 0; n < jsr.length(); n++){
				JSONObject record = jsr;
				readTransferee=record.getString("transferee");
				readStatus=record.getString("status");
				readVolume_unit=record.getString("volume_unit");
				readWeight_unit=record.getString("weight_unit");
				}
			JSONArray content = jsr.getJSONArray("quotes");
			for(int n = 0; n < content.length(); n++){
			JSONObject record = content.getJSONObject(n);
				readMarketId=record.getString("market");
				readAgent=record.getString("agent_id");
				readSoWeight=record.getString("declared_weight");
				readSoVolume=record.getString("declared_volume");
				readTariff=record.getString("tariff");
				//readStatus=record.getString("status");
				}
		} catch (Exception e) {
			// TODO: handle exception
		}
		   Long marketId=Long.parseLong(readMarketId);
		    Long readTariffId=Long.parseLong(readTariff);
			BigDecimal soWeight=new BigDecimal("0.0");
			BigDecimal soVolume=new BigDecimal("0.0");
			if(readSoWeight!=null && !readSoWeight.equals("")){
				soWeight=new BigDecimal(readSoWeight);
			}
			if(readSoVolume!=null && !readSoVolume.equals("")){
				soVolume=new BigDecimal(readSoVolume);
			}
		String clientID="";
		Integer client_id=0;
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		if(company!=null){
			clientID=company.getClientID();
		}
		if(clientID!=null && !clientID.equalsIgnoreCase("")){
			client_id=Integer.parseInt(clientID);
		}
		if(category.equalsIgnoreCase("Origin")){
			packService="origin";
		}else if(category.equalsIgnoreCase("Destin")){
			packService="destination";
		}else{
			packService="";	
		}
		try {
			String pricingSupplement="";
			JSONObject objar = new JSONObject();
			objar.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
			objar.put("client", client_id);
			objar.put("market", marketId);
			objar.put("agent", readAgent);
			JSONObject objar1 = new JSONObject();
			objar1.put("type", pricingForMode);
			objar1.put("weight", readSoWeight);
			objar1.put("volume", readSoVolume);
			objar1.put("tariff", readTariffId);
			JSONArray list = new JSONArray();
			     list.put(objar1);		
			     objar.accumulate("shipments", list);				
			     objar.put("service", packService);
			     objar.put("weight_unit", readWeight_unit);
			     objar.put("volume_unit", readVolume_unit);
			     objar.put("transferee", readTransferee);
			     objar.put("status", readStatus);
			System.out.println("********  REQUEST JSON - Quote / SO # ************"+objar.toString());
			pricingSupplement=PricePointClientServiceCall.saveQuotesDetails(objar.toString());
			PricingDetailsValue dto = null;
			JSONObject jsr = new JSONObject(pricingSupplement);				
			for(int n = 0; n < jsr.length(); n++){
			JSONObject record = jsr;
			String status=record.getString("status");
			priceId=record.getString("id");								
			}
			//serviceOrderManager.tariffIdUpdate(AccId,priceId,sessionCorpID);
		} catch (JSONException e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
	return priceId;
   }
	List pricPointReferenceList=new ArrayList();
	@SkipValidation
	public String updatePricPointCheckDetails(){
		try {
			pricPointReferenceList=serviceOrderManager.getPricPointDetails(shipNumber,sessionCorpID);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
	return SUCCESS;	
	}		
	//private String priceReadId;
	//private String priceTariffId;
	@SkipValidation
   public String pricPointUpdateDetails(Long psSoId,String tariffId,ServiceOrder serviceOrder,Miscellaneous miscellaneous,List<SystemDefault> sysDefaultDetail,List modeValue){
		try {
			//serviceOrder=serviceOrderManager.get(psSoId);
			//miscellaneous = miscellaneousManager.get(psSoId);
			//sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					corpIdBaseCurrency=systemDefault.getBaseCurrency();	            
				}
			}
			if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
				weightType="lbscft";
			}
			else{
				weightType="kgscbm";
			}
			String pricingForMode="";
			if(serviceOrder.getJob()!=null && serviceOrder.getJob().equalsIgnoreCase("STO")){
				pricingForMode="PS";
			}else{
			if(serviceOrder.getMode()!=null && !serviceOrder.getMode().equals("") && !serviceOrder.getMode().equalsIgnoreCase("Sea")){
				//List modeValue = serviceOrderManager.findPriceCode(serviceOrder.getMode(), "MODE",sessionCorpID);
				 if (modeValue != null && !modeValue.isEmpty() && modeValue.get(0)!=null) {
					 pricingForMode=modeValue.get(0).toString();
				 }
			}		
			if(serviceOrder.getMode()!=null && !serviceOrder.getMode().equals("") && serviceOrder.getMode().equalsIgnoreCase("Sea")){
				if(serviceOrder.getPackingMode()!=null && !serviceOrder.getPackingMode().equals("")){
					if(serviceOrder.getPackingMode().equals("RORO") || serviceOrder.getPackingMode().equals("GRP") || serviceOrder.getPackingMode().equals("BKBLK") || serviceOrder.getPackingMode().equals("LCL") || serviceOrder.getPackingMode().equals("LVAN")  || serviceOrder.getPackingMode().equals("T/W")){
					      pricingForMode="LCL";
					     }
					     if(serviceOrder.getPackingMode().equals("LOOSE") || serviceOrder.getPackingMode().equals("LVCO") || serviceOrder.getPackingMode().equals("LVCNT") || serviceOrder.getPackingMode().equals("LVCNTR") ){
					      pricingForMode="FCL_L";
					     } 
				}
			}
			}	
			if(weightType!=null && !weightType.equalsIgnoreCase("")){
				weight_unit=weightType.substring(0, 3);
				if(weight_unit.equalsIgnoreCase("lbs")){
					weight_unit="lb";
				}else if(weight_unit.equalsIgnoreCase("kgs")){
					weight_unit="kg";
				}
				volume_unit=weightType.substring(3, 6);
				if(volume_unit.equalsIgnoreCase("cbm")){
					volume_unit="mt";
				}
					
			}		

			BigDecimal soWeight=new BigDecimal("0.0");
			BigDecimal soVolume=new BigDecimal("0.0");
			
			if(weightType.equalsIgnoreCase("lbscft") && miscellaneous.getEstimatedNetWeight()!=null && !miscellaneous.getEstimatedNetWeight().toString().equals("") && miscellaneous.getNetEstimateCubicFeet()!=null && !miscellaneous.getNetEstimateCubicFeet().toString().equals("") ){
				 soWeight= miscellaneous.getEstimatedNetWeight();
				 soVolume= miscellaneous.getNetEstimateCubicFeet();
				}
   if(weightType.equalsIgnoreCase("kgscbm") && miscellaneous.getEstimatedNetWeightKilo()!=null && !miscellaneous.getEstimatedNetWeightKilo().toString().equals("") && miscellaneous.getNetEstimateCubicMtr()!=null && !miscellaneous.getNetEstimateCubicMtr().toString().equals("")){
					 soWeight= miscellaneous.getEstimatedNetWeightKilo();
					 soVolume= miscellaneous.getNetEstimateCubicMtr();
			   }		
				Integer Tariff=0;
				if(tariffId !=null && ! tariffId.equals("")){
					Tariff=Integer.parseInt(tariffId.trim());
				}		
				
			String transferee="";
			if(serviceOrder.getFirstName()!=null && !serviceOrder.getFirstName().equals("") && serviceOrder.getLastName()!=null && !serviceOrder.getLastName().equals("") ){
				transferee=serviceOrder.getFirstName()+" "+serviceOrder.getLastName();
			}else{
				transferee=serviceOrder.getFirstName();
			}

			String readDetails="";	
			String readRefId="";
			String quoteStatus="";
			if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("PE")){
				quoteStatus="Pending";
			}else if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("AC")){
				quoteStatus="Booked";
			}else if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("RE")){
				quoteStatus="Cancelled";
			}else if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("SU")){
				quoteStatus="Complete";
			}else{
				quoteStatus="Pending";
			}
			try {
				JSONObject obj = new JSONObject();
				obj.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
				obj.put("id", tariffId);
				obj.put("currency", corpIdBaseCurrency);
				System.out.println("********  REQUEST JSON - Quote / SO # ************"+obj.toString());
				readDetails=PricePointClientServiceCall.readQuotesDetails(obj.toString());
				JSONObject jsr = new JSONObject(readDetails);		
				JSONArray content = jsr.getJSONArray("quotes");
				for(int n = 0; n < content.length(); n++){
				JSONObject record = content.getJSONObject(n);
				   //String pricingWeight=record.getString("declared_weight");
				   //readWeight= new BigDecimal(pricingWeight);
				   //String pricingVolume=record.getString("declared_volume");
				   //readVolume= new BigDecimal(pricingVolume);	
				   readRefId=record.getString("id");	
				  
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			Integer readId=0;
			if(readRefId !=null && ! readRefId.equals("")){
				readId=Integer.parseInt(readRefId.trim());
			}
			try {				
				String updateSupplement="";
				JSONObject obup = new JSONObject();
				obup.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
				obup.put("id", Tariff);
				JSONObject obup1 = new JSONObject();
				obup1.put("id", readId);
				obup1.put("weight", soWeight);
				obup1.put("volume", soVolume);
				obup1.put("type", pricingForMode);
				JSONArray list = new JSONArray();
				list.put(obup1);
				obup.accumulate("shipments", list);
				obup.put("weight_unit", weight_unit);
				obup.put("volume_unit", volume_unit);
				obup.put("transferee", transferee);			
				obup.put("status", quoteStatus);
				System.out.println("********  REQUEST JSON - Quote / SO # ************"+obup.toString());
				updateSupplement=PricePointClientServiceCall.udateQuotesDetails(obup.toString());			
			} catch (Exception e) {
				// TODO: handle exception
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
	return SUCCESS;
   }
	@SkipValidation
	public String editBuyEstimateCurrency(){

		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			accountLineBasis=accountLineBasis;
			accountLineEstimateQuantity=accountLineEstimateQuantity;
			accountLine=accountLineManager.get(aid);
			billing=billingManager.get(accountLine.getServiceOrderId());
			serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
			trackingStatus=trackingStatusManager.get(accountLine.getServiceOrderId());
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}
			String decimalValue="0.00";
			   decimalValue=getRequest().getParameter("estimateQuantityTemp");
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							accountLine.setEstimateQuantity(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
						  }	
				   decimalValue=getRequest().getParameter("estimateRateTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimateRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }
					   decimalValue=getRequest().getParameter("estimateExpenseTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstimateExpense(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
									  }
								  }
						   decimalValue=getRequest().getParameter("basisTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setBasis(decimalValue); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										  }
									  }
							String  dateValue="";					
																
								 try{
									 dateValue=getRequest().getParameter("estValueDateTemp");					 
									  if(dateValue!=null && (!(dateValue.equals("")))){
													 try{														
													  accountLine.setEstValueDate(CommonUtil.multiParse(dateValue));
												     }catch(Exception e){
												    	 e.printStackTrace();
												     }
											  } 
									 }catch(Exception e){
											logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									 }
						  
			if(contractType){
				
				   decimalValue=getRequest().getParameter("estimatePayableContractCurrencyTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimatePayableContractCurrency(decimalValue); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								  }
							  }
					   
  
  
						 String dateTemp="";
						 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
						 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						 try{
						 dateTemp=getRequest().getParameter("estimatePayableContractValueDateTemp");					 
						  if(dateTemp!=null && (!(dateTemp.equals("")))){
										 try{
										      Date date = sdfSource.parse(dateTemp);	
											  accountLine.setEstimatePayableContractValueDate(date); 
									     }catch(Exception e){
										      Date date = sdfDestination.parse(dateTemp);	
										      String dt1=sdfSource.format(date);
										      date = sdfSource.parse(dt1);
											  accountLine.setEstimatePayableContractValueDate(date); 
									     }
								  } 
						 }catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						 }
						  
					   decimalValue=getRequest().getParameter("estimatePayableContractExchangeRateTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
									  }
								  }
					   decimalValue=getRequest().getParameter("estimatePayableContractRateTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstimatePayableContractRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);	  
									  }
								  }
					   decimalValue=getRequest().getParameter("estimatePayableContractRateAmmountTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstimatePayableContractRateAmmount(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);	  
									  }
								  }


						   decimalValue=getRequest().getParameter("estCurrencyTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstCurrency(decimalValue); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }
								 try{
								 dateTemp=getRequest().getParameter("estSellValueDateTemp");					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setEstSellValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setEstSellValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }						  
				
							   decimalValue=getRequest().getParameter("estExchangeRateTemp");
								  if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstExchangeRate(new BigDecimal(decimalValue)); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
											  }
										  }
							   decimalValue=getRequest().getParameter("estLocalRateTemp");
								  if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstLocalRate(new BigDecimal(decimalValue)); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
											  }
										  }
							   decimalValue=getRequest().getParameter("estLocalAmountTemp");
								  if(decimalValue!=null && (!(decimalValue.equals("")))){
										 try{
											accountLine.setEstLocalAmount(new BigDecimal(decimalValue));
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
											  }
										  }
						  
				}else{
					   decimalValue=getRequest().getParameter("estCurrencyTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstCurrency(decimalValue); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									  }
								  }
							 String dateTemp="";
							 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
							 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  
							 try{
								 dateTemp=getRequest().getParameter("estSellValueDateTemp");					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setEstSellValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setEstSellValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }						  
			
						   decimalValue=getRequest().getParameter("estExchangeRateTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstExchangeRate(new BigDecimal(decimalValue)); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }
						   decimalValue=getRequest().getParameter("estLocalRateTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstLocalRate(new BigDecimal(decimalValue)); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
										  }
									  }
						   decimalValue=getRequest().getParameter("estLocalAmountTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstLocalAmount(new BigDecimal(decimalValue));
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }				
				}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;			
	
	}

	@SkipValidation
	public String editSellEstimateCurrency(){

		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			accountLineBasis=accountLineBasis;
			accountLineEstimateQuantity=accountLineEstimateQuantity;
			accountLine=accountLineManager.get(aid);
			serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
			billing=billingManager.get(accountLine.getServiceOrderId());
			trackingStatus=trackingStatusManager.get(accountLine.getServiceOrderId());
			tempAccList="";
			List accountlineList1= accountLineManager.getAllAccountLineAgent(billing.getId()); 
			if(accountlineList1!=null && (!(accountlineList1.isEmpty()))){
				 Iterator accountlineIterator=accountlineList1.iterator();
				  while (accountlineIterator.hasNext()) {
					try{  
					Long  Accid = (Long) accountlineIterator.next();
							if(!Accid.toString().equalsIgnoreCase(aid.toString())){
								AccountLine accountLine1=accountLineManager.get(Accid);
								if(!((accountLine1.getRevisionExpense().floatValue()!=0)||(accountLine1.getRevisionRevenueAmount().floatValue()!=0)||(accountLine1.getActualRevenue().floatValue()!=0)||(accountLine1.getActualExpense().floatValue()!=0)||(accountLine1.getDistributionAmount().floatValue()!=0))){
										if(tempAccList.equalsIgnoreCase("")){
											tempAccList=Accid+"";
										}else{
											tempAccList=tempAccList+","+Accid+"";
										}
								}
							}
					}catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
				  }
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}  
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			    }
			    if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			      	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
			      }
			String decimalValue="0.00";
			
			   decimalValue=getRequest().getParameter("estimateQuantityTemp");
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							accountLine.setEstimateQuantity(new BigDecimal(decimalValue)); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							  }
						  }
				   decimalValue=getRequest().getParameter("estimateSellQuantity");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimateSellQuantity(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								  }
							  }			  
				  
				   decimalValue=getRequest().getParameter("estimateSellRateTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimateSellRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								  }
							  }
					   decimalValue=getRequest().getParameter("estimateRevenueAmountTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstimateRevenueAmount(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
									  }
								  }		
						   decimalValue=getRequest().getParameter("basisTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setBasis(decimalValue); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }						  
			if(contractType){
			   decimalValue=getRequest().getParameter("estimateContractCurrencyTemp");
				  if(decimalValue!=null && (!(decimalValue.equals("")))){
						 try{
							accountLine.setEstimateContractCurrency(decimalValue); 
					     }catch(Exception e){
					    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
							  }
						  }
					 String dateTemp="";
					 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
					 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				  
					 try{
						 dateTemp=getRequest().getParameter("estimateContractValueDateTemp");					 
						  if(dateTemp!=null && (!(dateTemp.equals("")))){
										 try{
										      Date date = sdfSource.parse(dateTemp);	
											  accountLine.setEstimateContractValueDate(date); 
									     }catch(Exception e){
										      Date date = sdfDestination.parse(dateTemp);	
										      String dt1=sdfSource.format(date);
										      date = sdfSource.parse(dt1);
											  accountLine.setEstimateContractValueDate(date); 
									     }
								  } 
						 }catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						 }				  
						
				   decimalValue=getRequest().getParameter("estimateContractExchangeRateTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimateContractExchangeRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }
				   decimalValue=getRequest().getParameter("estimateContractRateTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimateContractRate(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								  }
							  }
				   decimalValue=getRequest().getParameter("estimateContractRateAmmountTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstimateContractRateAmmount(new BigDecimal(decimalValue)); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
								  }
							  }


					   decimalValue=getRequest().getParameter("estSellCurrencyTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellCurrency(decimalValue); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);	  
									  }
								  }
							 try{
								 dateTemp=getRequest().getParameter("estSellValueDateTemp");					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setEstSellValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setEstSellValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }						  
								
						   decimalValue=getRequest().getParameter("estSellExchangeRateTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstSellExchangeRate(new BigDecimal(decimalValue)); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										  }
									  }
						   decimalValue=getRequest().getParameter("estSellLocalRateTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstSellLocalRate(new BigDecimal(decimalValue)); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
										  }
									  }
						   decimalValue=getRequest().getParameter("estSellLocalAmountTemp");
							  if(decimalValue!=null && (!(decimalValue.equals("")))){
									 try{
										accountLine.setEstSellLocalAmount(new BigDecimal(decimalValue));
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);  
										  }
									  }
					  
			}else{
				   decimalValue=getRequest().getParameter("estSellCurrencyTemp");
					  if(decimalValue!=null && (!(decimalValue.equals("")))){
							 try{
								accountLine.setEstSellCurrency(decimalValue); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								  }
							  }
						 String dateTemp="";
						 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
						 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						 try{
							 dateTemp=getRequest().getParameter("estSellValueDateTemp");					 
							  if(dateTemp!=null && (!(dateTemp.equals("")))){
											 try{
											      Date date = sdfSource.parse(dateTemp);	
												  accountLine.setEstSellValueDate(date); 
										     }catch(Exception e){
											      Date date = sdfDestination.parse(dateTemp);	
											      String dt1=sdfSource.format(date);
											      date = sdfSource.parse(dt1);
												  accountLine.setEstSellValueDate(date); 
										     }
									  } 
							 }catch(Exception e){
									logger.error("Exception Occour: "+ e.getStackTrace()[0]);
							 }					  
					
					   decimalValue=getRequest().getParameter("estSellExchangeRateTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellExchangeRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
										  
									  }
								  }
					   decimalValue=getRequest().getParameter("estSellLocalRateTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellLocalRate(new BigDecimal(decimalValue)); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
									  }
								  }
					   decimalValue=getRequest().getParameter("estSellLocalAmountTemp");
						  if(decimalValue!=null && (!(decimalValue.equals("")))){
								 try{
									accountLine.setEstSellLocalAmount(new BigDecimal(decimalValue));
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									  }
								  }
				
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;			
	
	}
	@SkipValidation
	public String editBuyRevisionCurrency(){

		try {
			accountLine=accountLineManager.get(aid);
			billing=billingManager.get(accountLine.getServiceOrderId());
			trackingStatus=trackingStatusManager.get(accountLine.getServiceOrderId());
			serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
			country = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
			List accountlineList1= accountLineManager.getAllAccountLineAgent(billing.getId()); 
			if(accountlineList1!=null && (!(accountlineList1.isEmpty()))){
				 Iterator accountlineIterator=accountlineList1.iterator();
				  while (accountlineIterator.hasNext()) {
					try{  
					Long  Accid = (Long) accountlineIterator.next();
							if(!Accid.toString().equalsIgnoreCase(aid.toString())){
								AccountLine accountLine1=accountLineManager.get(Accid);
								if(!((accountLine1.getRevisionExpense().floatValue()!=0)||(accountLine1.getRevisionRevenueAmount().floatValue()!=0)||(accountLine1.getActualRevenue().floatValue()!=0)||(accountLine1.getActualExpense().floatValue()!=0)||(accountLine1.getDistributionAmount().floatValue()!=0))){
										if(tempAccList.equalsIgnoreCase("")){
											tempAccList=Accid+"";
										}else{
											tempAccList=tempAccList+","+Accid+"";
										}
								}
							}
					}catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
				  }
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}
			String strValue="0.00";
			strValue=getRequest().getParameter("strValue");
				  if(strValue!=null && (!(strValue.equals("")))){
					  String str[]=strValue.split("~");
					  		try{
								accountLine.setBasis(str[0]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
					  		try{
								accountLine.setRevisionQuantity(new BigDecimal(str[1])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
					  		try{
								accountLine.setChargeCode(str[2]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setContract(str[3]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setVendorCode(str[4]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setRevisionCurrency(str[5]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
						     
							 String dateTemp="";
							 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
							 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  
							 try{
								 dateTemp=str[6];					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setRevisionValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setRevisionValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }					     
						     
					  		try{
								accountLine.setRevisionLocalRate(new BigDecimal(str[7])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setRevisionExchangeRate(new BigDecimal(str[8])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }		
					  		try{
								accountLine.setRevisionLocalAmount(new BigDecimal(str[9])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setRevisionExpense(new BigDecimal(str[10])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setRevisionDiscount(new BigDecimal(str[11])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }					     
						  		try{
									accountLine.setRevisionRate(new BigDecimal(str[12])); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								 }					     
						     
						     if(contractType){
								     try{
											accountLine.setRevisionPayableContractCurrency(str[13]); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }
									     
									  
										 try{
											 dateTemp=str[14];					 
											  if(dateTemp!=null && (!(dateTemp.equals("")))){
															 try{
															      Date date = sdfSource.parse(dateTemp);	
																  accountLine.setRevisionPayableContractValueDate(date); 
														     }catch(Exception e){
															      Date date = sdfDestination.parse(dateTemp);	
															      String dt1=sdfSource.format(date);
															      date = sdfSource.parse(dt1);
																  accountLine.setRevisionPayableContractValueDate(date); 
														     }
													  } 
											 }catch(Exception e){
													logger.error("Exception Occour: "+ e.getStackTrace()[0]);
											 }					     
									     
								  		try{
											accountLine.setRevisionPayableContractRate(new BigDecimal(str[15])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }						     
								  		try{
											accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(str[16])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }		
								  		try{
											accountLine.setRevisionPayableContractRateAmmount(new BigDecimal(str[17])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }					     
						    
						       }
					     
					     
						  }
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}	
		
		
		return SUCCESS;			
	
	}
	@SkipValidation
	public String editSellRevisionCurrency(){

		try {
			accountLine=accountLineManager.get(aid);
			billing=billingManager.get(accountLine.getServiceOrderId());
			trackingStatus=trackingStatusManager.get(accountLine.getServiceOrderId());
			serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
			country = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
			List accountlineList1= accountLineManager.getAllAccountLineAgent(billing.getId()); 
			if(accountlineList1!=null && (!(accountlineList1.isEmpty()))){
				 Iterator accountlineIterator=accountlineList1.iterator();
				  while (accountlineIterator.hasNext()) {
					try{  
					Long  Accid = (Long) accountlineIterator.next();
							if(!Accid.toString().equalsIgnoreCase(aid.toString())){
								AccountLine accountLine1=accountLineManager.get(Accid);
								if(!((accountLine1.getRevisionExpense().floatValue()!=0)||(accountLine1.getRevisionRevenueAmount().floatValue()!=0)||(accountLine1.getActualRevenue().floatValue()!=0)||(accountLine1.getActualExpense().floatValue()!=0)||(accountLine1.getDistributionAmount().floatValue()!=0))){
										if(tempAccList.equalsIgnoreCase("")){
											tempAccList=Accid+"";
										}else{
											tempAccList=tempAccList+","+Accid+"";
										}
								}
							}
					}catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
				  }
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}
			String strValue="0.00";
			strValue=getRequest().getParameter("strValue");
				  if(strValue!=null && (!(strValue.equals("")))){
					  String str[]=strValue.split("~");
					  		try{
								accountLine.setBasis(str[0]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
						     if(contractType){
							  		try{
										accountLine.setRevisionSellQuantity(new BigDecimal(str[1])); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
									 }
						     }else{
							  		try{
										accountLine.setRevisionQuantity(new BigDecimal(str[1])); 
								     }catch(Exception e){
								    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
									 }					    	 
						     }
					  		try{
								accountLine.setChargeCode(str[2]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setContract(str[3]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setVendorCode(str[4]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setRevisionSellCurrency(str[5]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
						     
							 String dateTemp="";
							 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
							 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  
							 try{
								 dateTemp=str[6];					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setRevisionSellValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setRevisionSellValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }					     
						     
					  		try{
								accountLine.setRevisionSellLocalRate(new BigDecimal(str[7])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setRevisionSellExchangeRate(new BigDecimal(str[8])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }		
					  		try{
								accountLine.setRevisionSellLocalAmount(new BigDecimal(str[9])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setRevisionRevenueAmount(new BigDecimal(str[10])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setRevisionDiscount(new BigDecimal(str[11])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }					     
						  		try{
									accountLine.setRevisionSellRate(new BigDecimal(str[12])); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								 }					     
						     
						     if(contractType){
								     try{
											accountLine.setRevisionContractCurrency(str[13]); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }
									     
									  
										 try{
											 dateTemp=str[14];					 
											  if(dateTemp!=null && (!(dateTemp.equals("")))){
															 try{
															      Date date = sdfSource.parse(dateTemp);	
																  accountLine.setRevisionContractValueDate(date); 
														     }catch(Exception e){
															      Date date = sdfDestination.parse(dateTemp);	
															      String dt1=sdfSource.format(date);
															      date = sdfSource.parse(dt1);
																  accountLine.setRevisionContractValueDate(date); 
														     }
													  } 
											 }catch(Exception e){
													logger.error("Exception Occour: "+ e.getStackTrace()[0]);
											 }					     
									     
								  		try{
											accountLine.setRevisionContractRate(new BigDecimal(str[15])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }						     
								  		try{
											accountLine.setRevisionContractExchangeRate(new BigDecimal(str[16])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }		
								  		try{
											accountLine.setRevisionContractRateAmmount(new BigDecimal(str[17])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }					     
						    
						       }
					     
					     
						  }
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}	
		
		
		return SUCCESS;			
	
	}

	@SkipValidation
	public String editBuyActualCurrency(){

		try {
			accountLine=accountLineManager.get(aid);
			billing=billingManager.get(accountLine.getServiceOrderId());
			trackingStatus=trackingStatusManager.get(accountLine.getServiceOrderId());
			serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
			country = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
			List accountlineList1= accountLineManager.getAllAccountLineAgent(billing.getId()); 
			if(accountlineList1!=null && (!(accountlineList1.isEmpty()))){
				 Iterator accountlineIterator=accountlineList1.iterator();
				  while (accountlineIterator.hasNext()) {
					try{  
					Long  Accid = (Long) accountlineIterator.next();
							if(!Accid.toString().equalsIgnoreCase(aid.toString())){
								AccountLine accountLine1=accountLineManager.get(Accid);
								if(!((accountLine1.getRevisionExpense().floatValue()!=0)||(accountLine1.getRevisionRevenueAmount().floatValue()!=0)||(accountLine1.getActualRevenue().floatValue()!=0)||(accountLine1.getActualExpense().floatValue()!=0)||(accountLine1.getDistributionAmount().floatValue()!=0))){
										if(tempAccList.equalsIgnoreCase("")){
											tempAccList=Accid+"";
										}else{
											tempAccList=tempAccList+","+Accid+"";
										}
								}
							}
					}catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
				  }
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}
			String strValue="0.00";
			strValue=getRequest().getParameter("strValue");
				  if(strValue!=null && (!(strValue.equals("")))){
					  String str[]=strValue.split("~");
					  		try{
								accountLine.setBasis(str[0]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
					  		try{
								accountLine.setRecQuantity(new BigDecimal(str[1])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
					  		try{
								accountLine.setChargeCode(str[2]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setContract(str[3]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setVendorCode(str[4]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setCountry(str[5]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
						     
							 String dateTemp="";
							 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
							 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  
							 try{
								 dateTemp=str[6];					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }					     
							     
					  		try{
								accountLine.setExchangeRate(new Double(str[7])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }		
					  		try{
								accountLine.setLocalAmount(new BigDecimal(str[8])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setActualExpense(new BigDecimal(str[9])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setActualDiscount(new BigDecimal(str[10])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }					     
					     
						     
						     if(contractType){
								     try{
											accountLine.setPayableContractCurrency(str[11]); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }
									     
									  
										 try{
											 dateTemp=str[12];					 
											  if(dateTemp!=null && (!(dateTemp.equals("")))){
															 try{
															      Date date = sdfSource.parse(dateTemp);	
																  accountLine.setPayableContractValueDate(date); 
														     }catch(Exception e){
															      Date date = sdfDestination.parse(dateTemp);	
															      String dt1=sdfSource.format(date);
															      date = sdfSource.parse(dt1);
																  accountLine.setPayableContractValueDate(date); 
														     }
													  } 
											 }catch(Exception e){
													logger.error("Exception Occour: "+ e.getStackTrace()[0]);
											 }					     
						     
								  		try{
											accountLine.setPayableContractExchangeRate(new BigDecimal(str[13])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }		
								  		try{
											accountLine.setPayableContractRateAmmount(new BigDecimal(str[14])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }					     
						    
						       }
					     
					     
						  }
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}	
		
		
		return SUCCESS;			
	
	}
	@SkipValidation
	public String editSellActualCurrency(){

		try {
			accountLine=accountLineManager.get(aid);
			billing=billingManager.get(accountLine.getServiceOrderId());
			trackingStatus=trackingStatusManager.get(accountLine.getServiceOrderId());
			serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
			country = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
			multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString();
			List accountlineList1= accountLineManager.getAllAccountLineAgent(billing.getId()); 
			if(accountlineList1!=null && (!(accountlineList1.isEmpty()))){
				 Iterator accountlineIterator=accountlineList1.iterator();
				  while (accountlineIterator.hasNext()) {
					try{  
					Long  Accid = (Long) accountlineIterator.next();
							if(!Accid.toString().equalsIgnoreCase(aid.toString())){
								AccountLine accountLine1=accountLineManager.get(Accid);
								if(!((accountLine1.getRevisionExpense().floatValue()!=0)||(accountLine1.getRevisionRevenueAmount().floatValue()!=0)||(accountLine1.getActualRevenue().floatValue()!=0)||(accountLine1.getActualExpense().floatValue()!=0)||(accountLine1.getDistributionAmount().floatValue()!=0))){
										if(tempAccList.equalsIgnoreCase("")){
											tempAccList=Accid+"";
										}else{
											tempAccList=tempAccList+","+Accid+"";
										}
								}
							}
					}catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
				  }
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String contractTypeValue = accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}else{
					contractType=false;
				}
			}else{
				contractType=false;
			}
			String strValue="0.00";
			strValue=getRequest().getParameter("strValue");
				  if(strValue!=null && (!(strValue.equals("")))){
					  String str[]=strValue.split("~");
					  		try{
								accountLine.setBasis(str[0]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
					  		try{
								accountLine.setRecQuantity(new BigDecimal(str[1])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
					  		try{
								accountLine.setChargeCode(str[2]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setContract(str[3]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setVendorCode(str[4]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setRecRateCurrency(str[5]); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }
						     
							 String dateTemp="";
							 SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MMM-yy");
							 SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						  
							 try{
								 dateTemp=str[6];					 
								  if(dateTemp!=null && (!(dateTemp.equals("")))){
												 try{
												      Date date = sdfSource.parse(dateTemp);	
													  accountLine.setRacValueDate(date); 
											     }catch(Exception e){
												      Date date = sdfDestination.parse(dateTemp);	
												      String dt1=sdfSource.format(date);
												      date = sdfSource.parse(dt1);
													  accountLine.setRacValueDate(date); 
											     }
										  } 
								 }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								 }					     
						     
					  		try{
								accountLine.setRecCurrencyRate(new BigDecimal(str[7])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }						     
					  		try{
								accountLine.setRecRateExchange(new BigDecimal(str[8])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }		
					  		try{
								accountLine.setActualRevenueForeign(new BigDecimal(str[9])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setActualRevenue(new BigDecimal(str[10])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }	
					  		try{
								accountLine.setActualDiscount(new BigDecimal(str[11])); 
						     }catch(Exception e){
						    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
							 }					     
						  		try{
									accountLine.setRecRate(new BigDecimal(str[12])); 
							     }catch(Exception e){
							    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
								 }					     
						     
						     if(contractType){
								     try{
											accountLine.setContractCurrency(str[13]); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }
									     
									  
										 try{
											 dateTemp=str[14];					 
											  if(dateTemp!=null && (!(dateTemp.equals("")))){
															 try{
															      Date date = sdfSource.parse(dateTemp);	
																  accountLine.setContractValueDate(date); 
														     }catch(Exception e){
															      Date date = sdfDestination.parse(dateTemp);	
															      String dt1=sdfSource.format(date);
															      date = sdfSource.parse(dt1);
																  accountLine.setContractValueDate(date); 
														     }
													  } 
											 }catch(Exception e){
													logger.error("Exception Occour: "+ e.getStackTrace()[0]);
											 }					     
									     
								  		try{
											accountLine.setContractRate(new BigDecimal(str[15])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }						     
								  		try{
											accountLine.setContractExchangeRate(new BigDecimal(str[16])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }		
								  		try{
											accountLine.setContractRateAmmount(new BigDecimal(str[17])); 
									     }catch(Exception e){
									    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
										 }					     
						    
						       }
					     
					     
						  }
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
	    	return "errorlog";
		}	
		
		
		return SUCCESS;			
	
	}	
	
	private  String shipNumberOld;
	  @SkipValidation 
    public String addWithResourcesQuotes() {
		  addCopyPricingServiceOrder();
		 try{
		  Class c=Class.forName("com.trilasoft.app.webapp.action.OperationsIntelligenceAction");  
		  OperationsIntelligenceAction oi=(OperationsIntelligenceAction)c.newInstance();  
		  oi.addWorkOrderToResources(shipNumberOld,shipnumber,serviceOrder.getId(),operationsIntelligenceManager);
		 }catch(Exception e){}
		  return SUCCESS;
	  }
	
	
	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public static Map<String, String> getDstates() {
		return dstates;
	}

	public static void setDstates(Map<String, String> dstates) {
		QuotationManagementSOAction.dstates = dstates;
	}

	public static Map<String, String> getOstates() {
		return ostates;
	}

	public static void setOstates(Map<String, String> ostates) {
		QuotationManagementSOAction.ostates = ostates;
	}
	
	public Map<String, String> getStatusReason() {
		return statusReason;
	}	

	public String getJobtypeSO() {
		return jobtypeSO;
	}

	public void setJobtypeSO(String jobtypeSO) {
		this.jobtypeSO = jobtypeSO;
	}

	public String getModeSO() {
		return modeSO;
	}

	public void setModeSO(String modeSO) {
		this.modeSO = modeSO;
	}

	public String getRoutingSO() {
		return routingSO;
	}

	public void setRoutingSO(String routingSO) {
		this.routingSO = routingSO;
	}

	public void setDefaultAccountLineManager(
			DefaultAccountLineManager defaultAccountLineManager) {
		this.defaultAccountLineManager = defaultAccountLineManager;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public String getAccountLineNumber() {
		return accountLineNumber;
	}

	public void setAccountLineNumber(String accountLineNumber) {
		this.accountLineNumber = accountLineNumber;
	}

	public Long getAutoLineNumber() {
		return autoLineNumber;
	}

	public void setAutoLineNumber(Long autoLineNumber) {
		this.autoLineNumber = autoLineNumber;
	}

	public List getMaxLineNumber() {
		return maxLineNumber;
	}

	public void setMaxLineNumber(List maxLineNumber) {
		this.maxLineNumber = maxLineNumber;
	}

	public String getAccountLineStatus() {
		return accountLineStatus;
	}

	public void setAccountLineStatus(String accountLineStatus) {
		this.accountLineStatus = accountLineStatus;
	}

	public String getQuotationContract() {
		return quotationContract;
	}

	public void setQuotationContract(String quotationContract) {
		this.quotationContract = quotationContract;
	}

	public List getVendorNameList() {
		return vendorNameList;
	}

	public void setVendorNameList(List vendorNameList) {
		this.vendorNameList = vendorNameList;
	} 
	public String getShipSize() {
		return shipSize;
	}

	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public String getCompanies() {
		return companies;
	}
	public void setCompanies(String companies) {
		this.companies = companies;
	}

	public String getQuotationBillToCode() {
		return quotationBillToCode;
	}

	public void setQuotationBillToCode(String quotationBillToCode) {
		this.quotationBillToCode = quotationBillToCode;
	}

	/**
	 * @return the weightType
	 */
	public String getWeightType() {
		return weightType;
	}

	/**
	 * @param weightType the weightType to set
	 */
	public void setWeightType(String weightType) {
		this.weightType = weightType;
	}

	public Map<String, String> getCoordinatorList() {
		return coordinatorList;
	}

	public Map<String, String> getEstimatorList() {
		return estimatorList;
	}

	public String getRoleHitFlag() {
		return roleHitFlag;
	}

	public void setRoleHitFlag(String roleHitFlag) {
		this.roleHitFlag = roleHitFlag;
	}

	public ServiceOrder getAddCopyServiceOrder() {
		return addCopyServiceOrder;
	}

	public void setAddCopyServiceOrder(ServiceOrder addCopyServiceOrder) {
		this.addCopyServiceOrder = addCopyServiceOrder;
	}

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}
	public String getRequestedAQP() {
		return requestedAQP;
	}

	public void setRequestedAQP(String requestedAQP) {
		this.requestedAQP = requestedAQP;
	}

	public String getSoid() {
		return soid;
	}

	public void setSoid(String soid) {
		this.soid = soid;
	}

	public int getQuoteAcceptance() {
		return quoteAcceptance;
	}

	public void setQuoteAcceptance(int quoteAcceptance) {
		this.quoteAcceptance = quoteAcceptance;
	}

	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public List getQuotationDiscriptionList() {
		return quotationDiscriptionList;
	}

	public void setQuotationDiscriptionList(List quotationDiscriptionList) {
		this.quotationDiscriptionList = quotationDiscriptionList;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public DsColaService getDsColaService() {
		return dsColaService;
	}

	public void setDsColaService(DsColaService dsColaService) {
		this.dsColaService = dsColaService;
	}

	public DsCrossCulturalTraining getDsCrossCulturalTraining() {
		return dsCrossCulturalTraining;
	}

	public void setDsCrossCulturalTraining(
			DsCrossCulturalTraining dsCrossCulturalTraining) {
		this.dsCrossCulturalTraining = dsCrossCulturalTraining;
	}

	public DsHomeFindingAssistancePurchase getDsHomeFindingAssistancePurchase() {
		return dsHomeFindingAssistancePurchase;
	}

	public void setDsHomeFindingAssistancePurchase(
			DsHomeFindingAssistancePurchase dsHomeFindingAssistancePurchase) {
		this.dsHomeFindingAssistancePurchase = dsHomeFindingAssistancePurchase;
	}

	public DsHomeRental getDsHomeRental() {
		return dsHomeRental;
	}

	public void setDsHomeRental(DsHomeRental dsHomeRental) {
		this.dsHomeRental = dsHomeRental;
	}

	public DsLanguage getDsLanguage() {
		return dsLanguage;
	}

	public void setDsLanguage(DsLanguage dsLanguage) {
		this.dsLanguage = dsLanguage;
	}

	public DsPreviewTrip getDsPreviewTrip() {
		return dsPreviewTrip;
	}

	public void setDsPreviewTrip(DsPreviewTrip dsPreviewTrip) {
		this.dsPreviewTrip = dsPreviewTrip;
	}

	public DsRelocationAreaInfoOrientation getDsRelocationAreaInfoOrientation() {
		return dsRelocationAreaInfoOrientation;
	}

	public void setDsRelocationAreaInfoOrientation(
			DsRelocationAreaInfoOrientation dsRelocationAreaInfoOrientation) {
		this.dsRelocationAreaInfoOrientation = dsRelocationAreaInfoOrientation;
	}

	public DsRelocationExpenseManagement getDsRelocationExpenseManagement() {
		return dsRelocationExpenseManagement;
	}

	public void setDsRelocationExpenseManagement(
			DsRelocationExpenseManagement dsRelocationExpenseManagement) {
		this.dsRelocationExpenseManagement = dsRelocationExpenseManagement;
	}

	public DsRepatriation getDsRepatriation() {
		return dsRepatriation;
	}

	public void setDsRepatriation(DsRepatriation dsRepatriation) {
		this.dsRepatriation = dsRepatriation;
	}

	public DsSchoolEducationalCounseling getDsSchoolEducationalCounseling() {
		return dsSchoolEducationalCounseling;
	}

	public void setDsSchoolEducationalCounseling(
			DsSchoolEducationalCounseling dsSchoolEducationalCounseling) {
		this.dsSchoolEducationalCounseling = dsSchoolEducationalCounseling;
	}

	public DsTaxServices getDsTaxServices() {
		return dsTaxServices;
	}

	public void setDsTaxServices(DsTaxServices dsTaxServices) {
		this.dsTaxServices = dsTaxServices;
	}

	public DsTemporaryAccommodation getDsTemporaryAccommodation() {
		return dsTemporaryAccommodation;
	}

	public void setDsTemporaryAccommodation(
			DsTemporaryAccommodation dsTemporaryAccommodation) {
		this.dsTemporaryAccommodation = dsTemporaryAccommodation;
	}

	public DsVisaImmigration getDsVisaImmigration() {
		return dsVisaImmigration;
	}

	public void setDsVisaImmigration(DsVisaImmigration dsVisaImmigration) {
		this.dsVisaImmigration = dsVisaImmigration;
	}

	public void setDsColaServiceManager(DsColaServiceManager dsColaServiceManager) {
		this.dsColaServiceManager = dsColaServiceManager;
	}

	public void setDsCrossCulturalTrainingManager(
			DsCrossCulturalTrainingManager dsCrossCulturalTrainingManager) {
		this.dsCrossCulturalTrainingManager = dsCrossCulturalTrainingManager;
	}

	public void setDsHomeFindingAssistancePurchaseManager(
			DsHomeFindingAssistancePurchaseManager dsHomeFindingAssistancePurchaseManager) {
		this.dsHomeFindingAssistancePurchaseManager = dsHomeFindingAssistancePurchaseManager;
	}

	public void setDsHomeRentalManager(DsHomeRentalManager dsHomeRentalManager) {
		this.dsHomeRentalManager = dsHomeRentalManager;
	}

	public void setDsLanguageManager(DsLanguageManager dsLanguageManager) {
		this.dsLanguageManager = dsLanguageManager;
	}

	public void setDsPreviewTripManager(DsPreviewTripManager dsPreviewTripManager) {
		this.dsPreviewTripManager = dsPreviewTripManager;
	}

	public void setDsRelocationAreaInfoOrientationManager(
			DsRelocationAreaInfoOrientationManager dsRelocationAreaInfoOrientationManager) {
		this.dsRelocationAreaInfoOrientationManager = dsRelocationAreaInfoOrientationManager;
	}

	public void setDsRelocationExpenseManagementManager(
			DsRelocationExpenseManagementManager dsRelocationExpenseManagementManager) {
		this.dsRelocationExpenseManagementManager = dsRelocationExpenseManagementManager;
	}

	public void setDsRepatriationManager(DsRepatriationManager dsRepatriationManager) {
		this.dsRepatriationManager = dsRepatriationManager;
	}

	public void setDsSchoolEducationalCounselingManager(
			DsSchoolEducationalCounselingManager dsSchoolEducationalCounselingManager) {
		this.dsSchoolEducationalCounselingManager = dsSchoolEducationalCounselingManager;
	}

	public void setDsTaxServicesManager(DsTaxServicesManager dsTaxServicesManager) {
		this.dsTaxServicesManager = dsTaxServicesManager;
	}

	public void setDsTemporaryAccommodationManager(
			DsTemporaryAccommodationManager dsTemporaryAccommodationManager) {
		this.dsTemporaryAccommodationManager = dsTemporaryAccommodationManager;
	}

	public void setDsVisaImmigrationManager(
			DsVisaImmigrationManager dsVisaImmigrationManager) {
		this.dsVisaImmigrationManager = dsVisaImmigrationManager;
	}

	/**
	 * @return the dsAutomobileAssistance
	 */
	public DsAutomobileAssistance getDsAutomobileAssistance() {
		return dsAutomobileAssistance;
	}

	/**
	 * @param dsAutomobileAssistance the dsAutomobileAssistance to set
	 */
	public void setDsAutomobileAssistance(
			DsAutomobileAssistance dsAutomobileAssistance) {
		this.dsAutomobileAssistance = dsAutomobileAssistance;
	}

	/**
	 * @return the dsMoveMgmt
	 */
	public DsMoveMgmt getDsMoveMgmt() {
		return dsMoveMgmt;
	}

	/**
	 * @param dsMoveMgmt the dsMoveMgmt to set
	 */
	public void setDsMoveMgmt(DsMoveMgmt dsMoveMgmt) {
		this.dsMoveMgmt = dsMoveMgmt;
	}

	/**
	 * @return the dsOngoingSupport
	 */
	public DsOngoingSupport getDsOngoingSupport() {
		return dsOngoingSupport;
	}

	/**
	 * @param dsOngoingSupport the dsOngoingSupport to set
	 */
	public void setDsOngoingSupport(DsOngoingSupport dsOngoingSupport) {
		this.dsOngoingSupport = dsOngoingSupport;
	}

	/**
	 * @return the dsTenancyManagement
	 */
	public DsTenancyManagement getDsTenancyManagement() {
		return dsTenancyManagement;
	}

	/**
	 * @param dsTenancyManagement the dsTenancyManagement to set
	 */
	public void setDsTenancyManagement(DsTenancyManagement dsTenancyManagement) {
		this.dsTenancyManagement = dsTenancyManagement;
	}

	/**
	 * @param dsAutomobileAssistanceManager the dsAutomobileAssistanceManager to set
	 */
	public void setDsAutomobileAssistanceManager(
			DsAutomobileAssistanceManager dsAutomobileAssistanceManager) {
		this.dsAutomobileAssistanceManager = dsAutomobileAssistanceManager;
	}

	/**
	 * @param dsMoveMgmtManager the dsMoveMgmtManager to set
	 */
	public void setDsMoveMgmtManager(DsMoveMgmtManager dsMoveMgmtManager) {
		this.dsMoveMgmtManager = dsMoveMgmtManager;
	}

	/**
	 * @param dsOngoingSupportManager the dsOngoingSupportManager to set
	 */
	public void setDsOngoingSupportManager(
			DsOngoingSupportManager dsOngoingSupportManager) {
		this.dsOngoingSupportManager = dsOngoingSupportManager;
	}

	/**
	 * @param dsTenancyManagementManager the dsTenancyManagementManager to set
	 */
	public void setDsTenancyManagementManager(
			DsTenancyManagementManager dsTenancyManagementManager) {
		this.dsTenancyManagementManager = dsTenancyManagementManager;
	}

	public String getPackingModeSO() {
		return packingModeSO;
	}

	public void setPackingModeSO(String packingModeSO) {
		this.packingModeSO = packingModeSO;
	}

	public String getCommoditySO() {
		return commoditySO;
	}

	public void setCommoditySO(String commoditySO) {
		this.commoditySO = commoditySO;
	}

	public String getServiceTypeSO() {
		return serviceTypeSO;
	}

	public void setServiceTypeSO(String serviceTypeSO) {
		this.serviceTypeSO = serviceTypeSO;
	}

	public String getAccountIdCheck() {
		return accountIdCheck;
	}

	public void setAccountIdCheck(String accountIdCheck) {
		this.accountIdCheck = accountIdCheck;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public Long getAid() {
		return aid;
	}

	public void setAid(Long aid) {
		this.aid = aid;
	}

	public String getAccCorpID() {
		return accCorpID;
	}

	public void setAccCorpID(String accCorpID) {
		this.accCorpID = accCorpID;
	}

	public String getAccountLineBasis() {
		return accountLineBasis;
	}

	public void setAccountLineBasis(String accountLineBasis) {
		this.accountLineBasis = accountLineBasis;
	}

	public String getAccountLineEstimateQuantity() {
		return accountLineEstimateQuantity;
	}

	public void setAccountLineEstimateQuantity(String accountLineEstimateQuantity) {
		this.accountLineEstimateQuantity = accountLineEstimateQuantity;
	}

	public String getEstCurrency() {
		return estCurrency;
	}

	public void setEstCurrency(String estCurrency) {
		this.estCurrency = estCurrency;
	}

	public String getEstExchangeRate() {
		return estExchangeRate;
	}

	public void setEstExchangeRate(String estExchangeRate) {
		this.estExchangeRate = estExchangeRate;
	}

	public String getEstLocalAmount() {
		return estLocalAmount;
	}

	public void setEstLocalAmount(String estLocalAmount) {
		this.estLocalAmount = estLocalAmount;
	}

	public String getEstValueDate() {
		return estValueDate;
	}

	public void setEstValueDate(String estValueDate) {
		this.estValueDate = estValueDate;
	}

	public Long getCustomerFileId() {
		return customerFileId;
	}

	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}

	public CustomerFile getCustomerFileTemp() {
		return customerFileTemp;
	}

	public void setCustomerFileTemp(CustomerFile customerFileTemp) {
		this.customerFileTemp = customerFileTemp;
	}

	public Set getCustomerServiceOrders() {
		return customerServiceOrders;
	}

	public void setCustomerServiceOrders(Set customerServiceOrders) {
		this.customerServiceOrders = customerServiceOrders;
	}

	public ServiceOrder getServiceOrderTemp() {
		return serviceOrderTemp;
	}

	public void setServiceOrderTemp(ServiceOrder serviceOrderTemp) {
		this.serviceOrderTemp = serviceOrderTemp;
	}

	public String getMultiCurrency() {
		return multiCurrency;
	}

	public void setMultiCurrency(String multiCurrency) {
		this.multiCurrency = multiCurrency;
	}

	public String getBaseCurrencyCompanyDivision() {
		return baseCurrencyCompanyDivision;
	}

	public void setBaseCurrencyCompanyDivision(String baseCurrencyCompanyDivision) {
		this.baseCurrencyCompanyDivision = baseCurrencyCompanyDivision;
	}

	public void setCompanyDivisionManager(
			CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}

	public void setExchangeRateManager(ExchangeRateManager exchangeRateManager) {
		this.exchangeRateManager = exchangeRateManager;
	}

	public List getAccExchangeRateList() {
		return accExchangeRateList;
	}

	public void setAccExchangeRateList(List accExchangeRateList) {
		this.accExchangeRateList = accExchangeRateList;
	}

    public String getEstLocalRate() {
		return estLocalRate;
	}

	public void setEstLocalRate(String estLocalRate) {
		this.estLocalRate = estLocalRate;
	}
	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public String getCompanyDivisionSO() {
		return companyDivisionSO;
	}

	public void setCompanyDivisionSO(String companyDivisionSO) {
		this.companyDivisionSO = companyDivisionSO;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public List getPackingServiceList() {
		return packingServiceList;
	}

	public void setPackingServiceList(List packingServiceList) {
		this.packingServiceList = packingServiceList;
	}

	public List getValuationList() {
		return valuationList;
	}

	public void setValuationList(List valuationList) {
		this.valuationList = valuationList;
	}

	public List getTransportationList() {
		return transportationList;
	}

	public void setTransportationList(List transportationList) {
		this.transportationList = transportationList;
	}

	public List getPackingList() {
		return packingList;
	}

	public void setPackingList(List packingList) {
		this.packingList = packingList;
	}

	public List getOtherMiscellaneousList() {
		return otherMiscellaneousList;
	}

	public void setOtherMiscellaneousList(List otherMiscellaneousList) {
		this.otherMiscellaneousList = otherMiscellaneousList;
	}

	public List getPackingContList() {
		return packingContList;
	}

	public void setPackingContList(List packingContList) {
		this.packingContList = packingContList;
	}

	public List getPackingUnpackList() {
		return packingUnpackList;
	}

	public void setPackingUnpackList(List packingUnpackList) {
		this.packingUnpackList = packingUnpackList;
	}

	public List getUnderLyingHaulList() {
		return underLyingHaulList;
	}

	public void setUnderLyingHaulList(List underLyingHaulList) {
		this.underLyingHaulList = underLyingHaulList;
	}

	public String getPackingSum() {
		return packingSum;
	}

	public void setPackingSum(String packingSum) {
		this.packingSum = packingSum;
	}

	public String getPackContSum() {
		return packContSum;
	}

	public void setPackContSum(String packContSum) {
		this.packContSum = packContSum;
	}

	public String getPackUnpackSum() {
		return packUnpackSum;
	}

	public void setPackUnpackSum(String packUnpackSum) {
		this.packUnpackSum = packUnpackSum;
	}

	public String getTransportSum() {
		return transportSum;
	}

	public void setTransportSum(String transportSum) {
		this.transportSum = transportSum;
	}

	public String getValuationSum() {
		return valuationSum;
	}

	public void setValuationSum(String valuationSum) {
		this.valuationSum = valuationSum;
	}

	public String getOthersSum() {
		return othersSum;
	}

	public void setOthersSum(String othersSum) {
		this.othersSum = othersSum;
	}

	public String getQuotesToValidate() {
		if((quotesToValidate==null || quotesToValidate.equals(""))){
			List quotesToValidateList=customerFileManager.getQuotesToValidate(sessionCorpID, serviceOrder.getJob(), serviceOrder.getCompanyDivision());
			if(quotesToValidateList!=null && !quotesToValidateList.isEmpty() && !quotesToValidateList.contains(null)){
			quotesToValidate=quotesToValidateList.get(0).toString();
			}else{
				quotesToValidate="";
			}
			}
			return quotesToValidate;
		}

	public void setQuotesToValidate(String quotesToValidate) {
		this.quotesToValidate = quotesToValidate;
	}

	public String getSalesCommisionRate() {
		return salesCommisionRate;
	}

	public void setSalesCommisionRate(String salesCommisionRate) {
		this.salesCommisionRate = salesCommisionRate;
	}

	public String getGrossMarginThreshold() {
		return grossMarginThreshold;
	}

	public void setGrossMarginThreshold(String grossMarginThreshold) {
		this.grossMarginThreshold = grossMarginThreshold;
	}

	public String getAccountInterface() {
		return accountInterface;
	}

	public void setAccountInterface(String accountInterface) {
		this.accountInterface = accountInterface;
	}

	public boolean isContractType() {
		return contractType;
	}

	public void setContractType(boolean contractType) {
		this.contractType = contractType;
	}

	public Map<String, String> getReloService() {
		return reloService;
	}

	public void setReloService(Map<String, String> reloService) {
		this.reloService = reloService;
	}

	public String getReloServiceType() {
		return reloServiceType;
	}

	public void setReloServiceType(String reloServiceType) {
		this.reloServiceType = reloServiceType;
	}

	public DspDetails getDspDetails() {
		return dspDetails;
	}

	public void setDspDetails(DspDetails dspDetails) {
		this.dspDetails = dspDetails;
	}

	public void setDspDetailsManager(DspDetailsManager dspDetailsManager) {
		this.dspDetailsManager = dspDetailsManager;
	}
	@SkipValidation		
	public Map<String, String> getOriginAddress() {
		return( originAddress!=null && !originAddress.isEmpty())?originAddress:adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Origin",customerFile.getJob()); 
	}

	public void setOriginAddress(Map<String, String> originAddress) {
		this.originAddress = originAddress;
	}
	@SkipValidation	
	public Map<String, String> getDestinationAddress() {
		return( destinationAddress!=null && !destinationAddress.isEmpty())?destinationAddress:adAddressesDetailsManager.getAdAddressesDropDown(customerFile.getId(),sessionCorpID,"Destination",customerFile.getJob()); 
	}
	public void setDestinationAddress(Map<String, String> destinationAddress) {
		this.destinationAddress = destinationAddress;
	}
	public void setAdAddressesDetailsManager(
			AdAddressesDetailsManager adAddressesDetailsManager) {
		this.adAddressesDetailsManager = adAddressesDetailsManager;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public void setChargesManager(ChargesManager chargesManager) {
		this.chargesManager = chargesManager;
	}

	public ItemsJbkEquipManager getItemsJbkEquipManager() {
		return itemsJbkEquipManager;
	}

	public void setItemsJbkEquipManager(ItemsJbkEquipManager itemsJbkEquipManager) {
		this.itemsJbkEquipManager = itemsJbkEquipManager;
	}

	public List getResourceList() {
		return resourceList;
	}

	public void setResourceList(List resourceList) {
		this.resourceList = resourceList;
	}
	public static Map<String, String> getCountryCod() {
		return countryCod;
	}

	public static void setCountryCod(Map<String, String> countryCod) {
		QuotationManagementSOAction.countryCod = countryCod;
	}
	
	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public List getAccountLineList() {
		return accountLineList;
	}

	public void setAccountLineList(ArrayList accountLineList) {
		this.accountLineList = accountLineList;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}


	public WorkTicket getWorkTicket() {
		return workTicket;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public Carton getCarton() {
		return carton;
	}

	public void setCarton(Carton carton) {
		this.carton = carton;
	}

	public String getCountDestinationDetailNotes() {
		return countDestinationDetailNotes;
	}

	public String getCountOriginDetailNotes() {
		return countOriginDetailNotes;
	}

	public String getCountWeightDetailNotes() {
		return countWeightDetailNotes;
	}

	public String getCountVipDetailNotes() {
		return countVipDetailNotes;
	}

	public String getCountServiceOrderNotes() {
		return countServiceOrderNotes;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public ServicePartner getServicePartner() {
		return servicePartner;
	}

	public void setServicePartner(ServicePartner servicePartner) {
		this.servicePartner = servicePartner;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}

	private MiscellaneousManager miscellaneousManager;

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public List getRefMasters() {
		return refMasters;
	}

	public Set getServiceOrders() {
		return serviceOrders;
	}

	public List getWorkTickets() {
		return workTickets;
	}

	public Set getStorages() {
		return storages;
	}

	private User user;

	private Set<Role> roles;

	private String coordinatr;

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	private String gotoPageString;

	public String getGotoPageString() {

	   return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

	   this.gotoPageString = gotoPageString;

	}
	private String validateFormNav;
    
    public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	
	public String getEstVatFlag() {
		return estVatFlag;
	}
	public void setEstVatFlag(String estVatFlag) {
		this.estVatFlag = estVatFlag;
	}
    public Map<String, String> getEstVatList() {
		return estVatList;
	}

	public void setEstVatList(Map<String, String> estVatList) {
		this.estVatList = estVatList;
	}

	public Map<String, String> getEstVatPersentList() {
		return estVatPersentList;
	}

	public void setEstVatPersentList(Map<String, String> estVatPersentList) {
		this.estVatPersentList = estVatPersentList;
	}
	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}
//	voxmecodeend

	public Map<String, String> getResourceCategory() {
		return resourceCategory;
	}

	public void setResourceCategory(Map<String, String> resourceCategory) {
		this.resourceCategory = resourceCategory;
	}

	public Map<String, List> getResourceMap() {
		return resourceMap;
	}

	public void setResourceMap(Map<String, List> resourceMap) {
		this.resourceMap = resourceMap;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getDayFocus() {
		return dayFocus;
	}

	public void setDayFocus(String dayFocus) {
		this.dayFocus = dayFocus;
	}

	public String getEnbState() {
		return enbState;
	}

	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getDayRowFocus() {
		return dayRowFocus;
	}

	public void setDayRowFocus(String dayRowFocus) {
		this.dayRowFocus = dayRowFocus;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getRouting1() {
		return routing1;
	}

	public void setRouting1(String routing1) {
		this.routing1 = routing1;
	}

	public Boolean getCostElementFlag() {
		return costElementFlag;
	}

	public void setCostElementFlag(Boolean costElementFlag) {
		this.costElementFlag = costElementFlag;
	}

	public String getDisableChk() {
		return disableChk;
	}

	public void setDisableChk(String disableChk) {
		this.disableChk = disableChk;
	}


	public String getEstSellCurrency() {
		return estSellCurrency;
	}

	public void setEstSellCurrency(String estSellCurrency) {
		this.estSellCurrency = estSellCurrency;
	}

	public String getEstSellValueDate() {
		return estSellValueDate;
	}

	public void setEstSellValueDate(String estSellValueDate) {
		this.estSellValueDate = estSellValueDate;
	}

	public String getEstSellExchangeRate() {
		return estSellExchangeRate;
	}

	public void setEstSellExchangeRate(String estSellExchangeRate) {
		this.estSellExchangeRate = estSellExchangeRate;
	}

	public String getEstSellLocalAmount() {
		return estSellLocalAmount;
	}

	public void setEstSellLocalAmount(String estSellLocalAmount) {
		this.estSellLocalAmount = estSellLocalAmount;
	}

	public String getEstSellLocalRate() {
		return estSellLocalRate;
	}

	public void setEstSellLocalRate(String estSellLocalRate) {
		this.estSellLocalRate = estSellLocalRate;
	}

	public String getEstimateContractCurrency() {
		return estimateContractCurrency;
	}

	public void setEstimateContractCurrency(String estimateContractCurrency) {
		this.estimateContractCurrency = estimateContractCurrency;
	}

	public String getEstimateContractValueDate() {
		return estimateContractValueDate;
	}

	public void setEstimateContractValueDate(String estimateContractValueDate) {
		this.estimateContractValueDate = estimateContractValueDate;
	}

	public String getEstimateContractExchangeRate() {
		return estimateContractExchangeRate;
	}

	public void setEstimateContractExchangeRate(String estimateContractExchangeRate) {
		this.estimateContractExchangeRate = estimateContractExchangeRate;
	}

	public String getEstimateContractRate() {
		return estimateContractRate;
	}

	public void setEstimateContractRate(String estimateContractRate) {
		this.estimateContractRate = estimateContractRate;
	}

	public String getEstimateContractRateAmmount() {
		return estimateContractRateAmmount;
	}

	public void setEstimateContractRateAmmount(String estimateContractRateAmmount) {
		this.estimateContractRateAmmount = estimateContractRateAmmount;
	}

	public String getEstimatePayableContractCurrency() {
		return estimatePayableContractCurrency;
	}

	public void setEstimatePayableContractCurrency(
			String estimatePayableContractCurrency) {
		this.estimatePayableContractCurrency = estimatePayableContractCurrency;
	}

	public String getEstimatePayableContractValueDate() {
		return estimatePayableContractValueDate;
	}

	public void setEstimatePayableContractValueDate(
			String estimatePayableContractValueDate) {
		this.estimatePayableContractValueDate = estimatePayableContractValueDate;
	}

	public String getEstimatePayableContractExchangeRate() {
		return estimatePayableContractExchangeRate;
	}

	public void setEstimatePayableContractExchangeRate(
			String estimatePayableContractExchangeRate) {
		this.estimatePayableContractExchangeRate = estimatePayableContractExchangeRate;
	}

	public String getEstimatePayableContractRateAmmount() {
		return estimatePayableContractRateAmmount;
	}

	public void setEstimatePayableContractRateAmmount(
			String estimatePayableContractRateAmmount) {
		this.estimatePayableContractRateAmmount = estimatePayableContractRateAmmount;
	}

	public String getEstimatePayableContractRate() {
		return estimatePayableContractRate;
	}

	public void setEstimatePayableContractRate(String estimatePayableContractRate) {
		this.estimatePayableContractRate = estimatePayableContractRate;
	}

	public String getVendorCodeNew() {
		return vendorCodeNew;
	}

	public void setVendorCodeNew(String vendorCodeNew) {
		this.vendorCodeNew = vendorCodeNew;
	}

	public String getTempAccList() {
		return tempAccList;
	}

	public void setTempAccList(String tempAccList) {
		this.tempAccList = tempAccList;
	}
	public Map<String, String> getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}
	public void setCurrencyExchangeRate(Map<String, String> currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}


	public Map<String, Map> getResourceMap1() {
		return resourceMap1;
	}

	public void setResourceMap1(Map<String, Map> resourceMap1) {
		this.resourceMap1 = resourceMap1;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	public void setDivisionFlag(String divisionFlag) {
		this.divisionFlag = divisionFlag;
	}
	public String getDivisionFlag() {
		return divisionFlag;
	}

	public Map<String, String> getEuVatPercentList() {
		return euVatPercentList;
	}

	public void setEuVatPercentList(Map<String, String> euVatPercentList) {
		this.euVatPercentList = euVatPercentList;
	}

	public boolean isBillingCMMContractType() {
		return billingCMMContractType;
	}

	public void setBillingCMMContractType(boolean billingCMMContractType) {
		this.billingCMMContractType = billingCMMContractType;
	}

	public boolean isNetworkAgent() {
		return networkAgent;
	}

	public void setNetworkAgent(boolean networkAgent) {
		this.networkAgent = networkAgent;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public String getSetDescriptionChargeCode() {
		return setDescriptionChargeCode;
	}

	public void setSetDescriptionChargeCode(String setDescriptionChargeCode) {
		this.setDescriptionChargeCode = setDescriptionChargeCode;
	}

	public String getSetDefaultDescriptionChargeCode() {
		return setDefaultDescriptionChargeCode;
	}

	public void setSetDefaultDescriptionChargeCode(
			String setDefaultDescriptionChargeCode) {
		this.setDefaultDescriptionChargeCode = setDefaultDescriptionChargeCode;
	}

	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}

	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}	
	
	public String getPricingValue() {
		return pricingValue;
	}

	public void setPricingValue(String pricingValue) {
		this.pricingValue = pricingValue;
	}

	public String getDelimeter1() {
		return delimeter1;
	}

	public void setDelimeter1(String delimeter1) {
		this.delimeter1 = delimeter1;
	}

	public String getDelimeter2() {
		return delimeter2;
	}

	public void setDelimeter2(String delimeter2) {
		this.delimeter2 = delimeter2;
	}

	public String getDelimeter3() {
		return delimeter3;
	}

	public void setDelimeter3(String delimeter3) {
		this.delimeter3 = delimeter3;
	}

	public String getAddPriceLine() {
		return addPriceLine;
	}

	public void setAddPriceLine(String addPriceLine) {
		this.addPriceLine = addPriceLine;
	}

	public String getPricingFooterValue() {
		return pricingFooterValue;
	}

	public void setPricingFooterValue(String pricingFooterValue) {
		this.pricingFooterValue = pricingFooterValue;
	}

	public String getPricingHiddenValue() {
		return pricingHiddenValue;
	}

	public void setPricingHiddenValue(String pricingHiddenValue) {
		this.pricingHiddenValue = pricingHiddenValue;
	}

	public String getMinShip() {
		return minShip;
	}

	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}

	public String getCountShip() {
		return countShip;
	}

	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}

	public String getDefaultTemplate() {
		return defaultTemplate;
	}

	public void setDefaultTemplate(String defaultTemplate) {
		this.defaultTemplate = defaultTemplate;
	}

	public String getInActiveLine() {
		return inActiveLine;
	}

	public void setInActiveLine(String inActiveLine) {
		this.inActiveLine = inActiveLine;
	}
	public List getContracts() {
		return contracts;
	}

	public void setContracts(List contracts) {
		this.contracts = contracts;
	}


	public void setContractManager(ContractManager contractManager) {
		this.contractManager = contractManager;
	}

	public boolean isBillingDMMContractType() {
		return billingDMMContractType;
	}

	public void setBillingDMMContractType(boolean billingDMMContractType) {
		this.billingDMMContractType = billingDMMContractType;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public String getCheckCompanyBA() {
		return checkCompanyBA;
	}

	public void setCheckCompanyBA(String checkCompanyBA) {
		this.checkCompanyBA = checkCompanyBA;
	}

	public Map<String, String> getFlagCarrierList() {
		return flagCarrierList;
	}

	public void setFlagCarrierList(Map<String, String> flagCarrierList) {
		this.flagCarrierList = flagCarrierList;
	}

	public Boolean getUsaFlag() {
		return usaFlag;
	}

	public void setUsaFlag(Boolean usaFlag) {
		this.usaFlag = usaFlag;
	}

	public String getCompanyDivisionAcctgCodeUnique() {
		return companyDivisionAcctgCodeUnique;
	}

	public void setCompanyDivisionAcctgCodeUnique(
			String companyDivisionAcctgCodeUnique) {
		this.companyDivisionAcctgCodeUnique = companyDivisionAcctgCodeUnique;
	}

	public boolean isPricePointFlag() {
		return pricePointFlag;
	}

	public void setPricePointFlag(boolean pricePointFlag) {
		this.pricePointFlag = pricePointFlag;
	}

	public String getMarketCountryCode() {
		return marketCountryCode;
	}

	public void setMarketCountryCode(String marketCountryCode) {
		this.marketCountryCode = marketCountryCode;
	}

	public Map<String, String> getMarketAreaList() {
		return marketAreaList;
	}

	public void setMarketAreaList(Map<String, String> marketAreaList) {
		this.marketAreaList = marketAreaList;
	}


	public String getPricingMode() {
		return pricingMode;
	}

	public void setPricingMode(String pricingMode) {
		this.pricingMode = pricingMode;
	}

	public String getPricingWeight() {
		return pricingWeight;
	}

	public void setPricingWeight(String pricingWeight) {
		this.pricingWeight = pricingWeight;
	}

	public String getPricingVolume() {
		return pricingVolume;
	}

	public void setPricingVolume(String pricingVolume) {
		this.pricingVolume = pricingVolume;
	}

	public String getPricingStorageUnit() {
		return pricingStorageUnit;
	}

	public void setPricingStorageUnit(String pricingStorageUnit) {
		this.pricingStorageUnit = pricingStorageUnit;
	}

	public void setMarketArea(String marketArea) {
		this.marketArea = marketArea;
	}

	public String getPackService() {
		return packService;
	}

	public void setPackService(String packService) {
		this.packService = packService;
	}

	public List getPricingDetailsList() {
		return pricingDetailsList;
	}

	public void setPricingDetailsList(List pricingDetailsList) {
		this.pricingDetailsList = pricingDetailsList;
	}

	public Map getMarketPlaceList() {
		return marketPlaceList;
	}

	public void setMarketPlaceList(Map marketPlaceList) {
		this.marketPlaceList = marketPlaceList;
	}

	public String getPackingModePricing() {
		return packingModePricing;
	}

	public void setPackingModePricing(String packingModePricing) {
		this.packingModePricing = packingModePricing;
	}

	public String getMarketAreaCode() {
		return marketAreaCode;
	}

	public void setMarketAreaCode(String marketAreaCode) {
		this.marketAreaCode = marketAreaCode;
	}

	public String getMarketagentId() {
		return MarketagentId;
	}

	public void setMarketagentId(String marketagentId) {
		MarketagentId = marketagentId;
	}

	public Map<String, String> getDetailsQuotesValue() {
		return detailsQuotesValue;
	}

	public void setDetailsQuotesValue(Map<String, String> detailsQuotesValue) {
		this.detailsQuotesValue = detailsQuotesValue;
	}

	public List getSupplementQuotesList() {
		return supplementQuotesList;
	}

	public void setSupplementQuotesList(List supplementQuotesList) {
		this.supplementQuotesList = supplementQuotesList;
	}

	public Map<String, String> getLanguageList() {
		return languageList;
	}
	public void setLanguageList(Map<String, String> languageList) {
		this.languageList = languageList;
	}
	public Map<String, String> getInclusionsList() {
		return inclusionsList;
	}
	public void setInclusionsList(Map<String, String> inclusionsList) {
		this.inclusionsList = inclusionsList;
	}
	public Map<String, String> getExclusionsList() {
		return exclusionsList;
	}
	public void setExclusionsList(Map<String, String> exclusionsList) {
		this.exclusionsList = exclusionsList;
	}
	public String getJobIncExc() {
		return jobIncExc;
	}
	public void setJobIncExc(String jobIncExc) {
		this.jobIncExc = jobIncExc;
	}
	public String getModeIncExc() {
		return modeIncExc;
	}
	public void setModeIncExc(String modeIncExc) {
		this.modeIncExc = modeIncExc;
	}
	public String getRoutingIncExc() {
		return routingIncExc;
	}
	public void setRoutingIncExc(String routingIncExc) {
		this.routingIncExc = routingIncExc;
	}
	public String getServIncExc() {
		return servIncExc;
	}
	public void setServIncExc(String servIncExc) {
		this.servIncExc = servIncExc;
	}
	public String getLanguageIncExc() {
		return languageIncExc;
	}
	public void setLanguageIncExc(String languageIncExc) {
		this.languageIncExc = languageIncExc;
	}
	public String getCheckDefaultIncExc() {
		return checkDefaultIncExc;
	}
	public void setCheckDefaultIncExc(String checkDefaultIncExc) {
		this.checkDefaultIncExc = checkDefaultIncExc;
	}
	public String getServIncExcDefault() {
		return servIncExcDefault;
	}
	public void setServIncExcDefault(String servIncExcDefault) {
		this.servIncExcDefault = servIncExcDefault;
	}

	public boolean isPricePointexpired() {
		return pricePointexpired;
	}

	public void setPricePointexpired(boolean pricePointexpired) {
		this.pricePointexpired = pricePointexpired;
	}

	public boolean isCheckAccessQuotation() {
		return checkAccessQuotation;
	}

	public void setCheckAccessQuotation(boolean checkAccessQuotation) {
		this.checkAccessQuotation = checkAccessQuotation;
	}

	public String getMarketAreaName() {
		return marketAreaName;
	}

	public void setMarketAreaName(String marketAreaName) {
		this.marketAreaName = marketAreaName;
	}

	public String getCorpIdBaseCurrency() {
		return corpIdBaseCurrency;
	}

	public void setCorpIdBaseCurrency(String corpIdBaseCurrency) {
		this.corpIdBaseCurrency = corpIdBaseCurrency;
	}

	public String getSupplementRate() {
		return SupplementRate;
	}

	public void setSupplementRate(String supplementRate) {
		SupplementRate = supplementRate;
	}

	public String getSupplementType() {
		return SupplementType;
	}

	public void setSupplementType(String supplementType) {
		SupplementType = supplementType;
	}

	public String getSupplementRate_basis() {
		return SupplementRate_basis;
	}

	public void setSupplementRate_basis(String supplementRate_basis) {
		SupplementRate_basis = supplementRate_basis;
	}

	public String getWeight_unit() {
		return weight_unit;
	}

	public void setWeight_unit(String weight_unit) {
		this.weight_unit = weight_unit;
	}

	public String getVolume_unit() {
		return volume_unit;
	}

	public void setVolume_unit(String volume_unit) {
		this.volume_unit = volume_unit;
	}

	public String getPricePointShipNumber() {
		return pricePointShipNumber;
	}

	public void setPricePointShipNumber(String pricePointShipNumber) {
		this.pricePointShipNumber = pricePointShipNumber;
	}

	public String getPricePointAgent() {
		return pricePointAgent;
	}

	public void setPricePointAgent(String pricePointAgent) {
		this.pricePointAgent = pricePointAgent;
	}

	public String getPricePointWeightUnit() {
		return pricePointWeightUnit;
	}

	public void setPricePointWeightUnit(String pricePointWeightUnit) {
		this.pricePointWeightUnit = pricePointWeightUnit;
	}

	public String getPricePointVolumeUnit() {
		return pricePointVolumeUnit;
	}

	public void setPricePointVolumeUnit(String pricePointVolumeUnit) {
		this.pricePointVolumeUnit = pricePointVolumeUnit;
	}

	public String getPricePointMinimum_density() {
		return pricePointMinimum_density;
	}

	public void setPricePointMinimum_density(String pricePointMinimum_density) {
		this.pricePointMinimum_density = pricePointMinimum_density;
	}

	public String getPricePointTariff_published() {
		return pricePointTariff_published;
	}

	public void setPricePointTariff_published(String pricePointTariff_published) {
		this.pricePointTariff_published = pricePointTariff_published;
	}

	public String getPricePointRate() {
		return pricePointRate;
	}

	public void setPricePointRate(String pricePointRate) {
		this.pricePointRate = pricePointRate;
	}

	public String getPricePoint_price() {
		return pricePoint_price;
	}

	public void setPricePoint_price(String pricePoint_price) {
		this.pricePoint_price = pricePoint_price;
	}

	public String getPricePoint_cctotal() {
		return pricePoint_cctotal;
	}

	public void setPricePoint_cctotal(String pricePoint_cctotal) {
		this.pricePoint_cctotal = pricePoint_cctotal;
	}

	public String getPricePoint_thc() {
		return pricePoint_thc;
	}

	public void setPricePoint_thc(String pricePoint_thc) {
		this.pricePoint_thc = pricePoint_thc;
	}

	public String getPricePoint_currency() {
		return pricePoint_currency;
	}

	public void setPricePoint_currency(String pricePoint_currency) {
		this.pricePoint_currency = pricePoint_currency;
	}

	public String getPricePoint_rate_basis() {
		return pricePoint_rate_basis;
	}

	public void setPricePoint_rate_basis(String pricePoint_rate_basis) {
		this.pricePoint_rate_basis = pricePoint_rate_basis;
	}

	public String getPricePoint_WeightUnit() {
		return pricePoint_WeightUnit;
	}

	public void setPricePoint_WeightUnit(String pricePoint_WeightUnit) {
		this.pricePoint_WeightUnit = pricePoint_WeightUnit;
	}

	public String getPricePoint_VolumeUnit() {
		return pricePoint_VolumeUnit;
	}

	public void setPricePoint_VolumeUnit(String pricePoint_VolumeUnit) {
		this.pricePoint_VolumeUnit = pricePoint_VolumeUnit;
	}

	public boolean isAccountLineAccountPortalFlag() {
		return accountLineAccountPortalFlag;
	}

	public void setAccountLineAccountPortalFlag(boolean accountLineAccountPortalFlag) {
		this.accountLineAccountPortalFlag = accountLineAccountPortalFlag;
	}
	public String getPricePointTariff() {
		return pricePointTariff;
	}

	public void setPricePointTariff(String pricePointTariff) {
		this.pricePointTariff = pricePointTariff;
	}

	public Map<String, String> getPricePointUpdateList() {
		return pricePointUpdateList;
	}

	public void setPricePointUpdateList(Map<String, String> pricePointUpdateList) {
		this.pricePointUpdateList = pricePointUpdateList;
	}

	public String getMarketNameDetails() {
		return marketNameDetails;
	}

	public void setMarketNameDetails(String marketNameDetails) {
		this.marketNameDetails = marketNameDetails;
	}

	public String getMarketNotesDetails() {
		return marketNotesDetails;
	}

	public void setMarketNotesDetails(String marketNotesDetails) {
		this.marketNotesDetails = marketNotesDetails;
	}

	public String getAgentConsignment() {
		return agentConsignment;
	}

	public void setAgentConsignment(String agentConsignment) {
		this.agentConsignment = agentConsignment;
	}

	public String getAgentAdditional() {
		return agentAdditional;
	}

	public void setAgentAdditional(String agentAdditional) {
		this.agentAdditional = agentAdditional;
	}

	public String getAgentcustoms() {
		return agentcustoms;
	}

	public void setAgentcustoms(String agentcustoms) {
		this.agentcustoms = agentcustoms;
	}

	public String getOriginResidence() {
		return originResidence;
	}

	public void setOriginResidence(String originResidence) {
		this.originResidence = originResidence;
	}

	public List getPricPointReferenceList() {
		return pricPointReferenceList;
	}

	public void setPricPointReferenceList(List pricPointReferenceList) {
		this.pricPointReferenceList = pricPointReferenceList;
	}

	public String getUpdateTypeFlag() {
		return updateTypeFlag;
	}

	public void setUpdateTypeFlag(String updateTypeFlag) {
		this.updateTypeFlag = updateTypeFlag;
	}

	public String getAllIdOFSo() {
		return allIdOFSo;
	}

	public void setAllIdOFSo(String allIdOFSo) {
		this.allIdOFSo = allIdOFSo;
	}

	public User getDiscountUserFlag() {
		return discountUserFlag;
	}

	public void setDiscountUserFlag(User discountUserFlag) {
		this.discountUserFlag = discountUserFlag;
	}

	public String getPricePointMassageDetails() {
		return pricePointMassageDetails;
	}

	public void setPricePointMassageDetails(String pricePointMassageDetails) {
		this.pricePointMassageDetails = pricePointMassageDetails;
	}

	public Date getPricePointUserStartDate() {
		return pricePointUserStartDate;
	}

	public void setPricePointUserStartDate(Date pricePointUserStartDate) {
		this.pricePointUserStartDate = pricePointUserStartDate;
	}

	

	public String getUserQuoteServices() {
		return userQuoteServices;
	}
	public void setUserQuoteServices(String userQuoteServices) {
		this.userQuoteServices = userQuoteServices;
	}
	public Map<String, String> getIncexcServiceList() {
		return incexcServiceList;
	}
	public void setIncexcServiceList(Map<String, String> incexcServiceList) {
		this.incexcServiceList = incexcServiceList;
	}

	public Map<String, String> getQuoteAcceptReason() {
		return quoteAcceptReason;
	}

	public void setQuoteAcceptReason(Map<String, String> quoteAcceptReason) {
		this.quoteAcceptReason = quoteAcceptReason;
	}

	public String getBaseCurrencySysDefault() {
		return baseCurrencySysDefault;
	}

	public void setBaseCurrencySysDefault(String baseCurrencySysDefault) {
		this.baseCurrencySysDefault = baseCurrencySysDefault;
	}

	public String getCctotalValue() {
		return cctotalValue;
	}

	public void setCctotalValue(String cctotalValue) {
		this.cctotalValue = cctotalValue;
	}

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}

	public String getCompanyDivisionIncExc() {
		return companyDivisionIncExc;
	}

	public void setCompanyDivisionIncExc(String companyDivisionIncExc) {
		this.companyDivisionIncExc = companyDivisionIncExc;
	}

	public String getCommodityIncExc() {
		return commodityIncExc;
	}

	public void setCommodityIncExc(String commodityIncExc) {
		this.commodityIncExc = commodityIncExc;
	}

	public String getPackingModeIncExc() {
		return packingModeIncExc;
	}

	public void setPackingModeIncExc(String packingModeIncExc) {
		this.packingModeIncExc = packingModeIncExc;
	}

	public String getOriginCountryIncExc() {
		return originCountryIncExc;
	}

	public void setOriginCountryIncExc(String originCountryIncExc) {
		this.originCountryIncExc = originCountryIncExc;
	}

	public String getDestinationCountryIncExc() {
		return destinationCountryIncExc;
	}

	public void setDestinationCountryIncExc(String destinationCountryIncExc) {
		this.destinationCountryIncExc = destinationCountryIncExc;
	}

	public Boolean getIgnoreForBilling() {
		return ignoreForBilling;
	}

	public void setIgnoreForBilling(Boolean ignoreForBilling) {
		this.ignoreForBilling = ignoreForBilling;
	}

	public String getSystemDefaultVatCalculationNew() {
		return systemDefaultVatCalculationNew;
	}

	public void setSystemDefaultVatCalculationNew(
			String systemDefaultVatCalculationNew) {
		this.systemDefaultVatCalculationNew = systemDefaultVatCalculationNew;
	}

	public Map<String, String> getPayVatList() {
		return payVatList;
	}

	public void setPayVatList(Map<String, String> payVatList) {
		this.payVatList = payVatList;
	}

	public Map<String, String> getPayVatPercentList() {
		return payVatPercentList;
	}

	public void setPayVatPercentList(Map<String, String> payVatPercentList) {
		this.payVatPercentList = payVatPercentList;
	}

	public String getRollUpInvoiceFlag() {
		return rollUpInvoiceFlag;
	}

	public void setRollUpInvoiceFlag(String rollUpInvoiceFlag) {
		this.rollUpInvoiceFlag = rollUpInvoiceFlag;
	}

	public String getAidListIds() {
		return aidListIds;
	}

	public void setAidListIds(String aidListIds) {
		this.aidListIds = aidListIds;
	}

	public Map<String, String> getCountryDsc() {
		return countryDsc;
	}

	public void setCountryDsc(Map<String, String> countryDsc) {
		this.countryDsc = countryDsc;
	}

	public String getCommodityForUser() {
		return commodityForUser;
	}

	public void setCommodityForUser(String commodityForUser) {
		this.commodityForUser = commodityForUser;
	}

	public List getResourceListForOI() {
		return resourceListForOI;
	}

	public void setResourceListForOI(List resourceListForOI) {
		this.resourceListForOI = resourceListForOI;
	}

	public void setOperationsIntelligenceManager(
			OperationsIntelligenceManager operationsIntelligenceManager) {
		this.operationsIntelligenceManager = operationsIntelligenceManager;
	}

	public String getResouceTemplate() {
		return resouceTemplate;
	}

	public void setResouceTemplate(String resouceTemplate) {
		this.resouceTemplate = resouceTemplate;
	}

	public List getScopeForWorkOrder() {
		return scopeForWorkOrder;
	}

	public void setScopeForWorkOrder(List scopeForWorkOrder) {
		this.scopeForWorkOrder = scopeForWorkOrder;
	}

	public String getAllWorkOrder() {
		return allWorkOrder;
	}

	public void setAllWorkOrder(String allWorkOrder) {
		this.allWorkOrder = allWorkOrder;
	}

	public String getCompDivFlag() {
		return compDivFlag;
	}

	public void setCompDivFlag(String compDivFlag) {
		this.compDivFlag = compDivFlag;
	}

	public String getCheckContractChargesMandatory() {
		return checkContractChargesMandatory;
	}

	public void setCheckContractChargesMandatory(
			String checkContractChargesMandatory) {
		this.checkContractChargesMandatory = checkContractChargesMandatory;
	}

	public AccountLine getSynchedAccountLine() {
		return synchedAccountLine;
	}

	public void setSynchedAccountLine(AccountLine synchedAccountLine) {
		this.synchedAccountLine = synchedAccountLine;
	}

	public boolean isAccNonEditFlag() {
		return accNonEditFlag;
	}

	public void setAccNonEditFlag(boolean accNonEditFlag) {
		this.accNonEditFlag = accNonEditFlag;
	}

	public boolean isUtsiRecAccDateFlag() {
		return utsiRecAccDateFlag;
	}

	public void setUtsiRecAccDateFlag(boolean utsiRecAccDateFlag) {
		this.utsiRecAccDateFlag = utsiRecAccDateFlag;
	}

	public boolean isUtsiPayAccDateFlag() {
		return utsiPayAccDateFlag;
	}

	public void setUtsiPayAccDateFlag(boolean utsiPayAccDateFlag) {
		this.utsiPayAccDateFlag = utsiPayAccDateFlag;
	}

	public boolean isCheckDescriptionFieldVisibility() {
		return checkDescriptionFieldVisibility;
	}

	public void setCheckDescriptionFieldVisibility(
			boolean checkDescriptionFieldVisibility) {
		this.checkDescriptionFieldVisibility = checkDescriptionFieldVisibility;
	}

	public String getResourceListForOIStatus() {
		return resourceListForOIStatus;
	}

	public void setResourceListForOIStatus(String resourceListForOIStatus) {
		this.resourceListForOIStatus = resourceListForOIStatus;
	}

	public String getDistinctWorkOrderList() {
		return distinctWorkOrderList;
	}

	public void setDistinctWorkOrderList(String distinctWorkOrderList) {
		this.distinctWorkOrderList = distinctWorkOrderList;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	public String getServiceIncExc() {
		return serviceIncExc;
	}

	public void setServiceIncExc(String serviceIncExc) {
		this.serviceIncExc = serviceIncExc;
	}



}

