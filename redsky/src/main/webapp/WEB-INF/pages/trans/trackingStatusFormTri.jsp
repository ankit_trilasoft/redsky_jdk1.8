
<script type="text/javascript"> 
<c:if test="${hitFlag=='1'}">
 <c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}"/>
</c:if>
	try{
	var f = document.getElementById('trackingStatusForm'); 
	f.setAttribute("autocomplete", "off");
    ////Form.focusFirstElement($("trackingStatusForm"));   
    }
    catch(e){}
   
    function resetImage(){
    try{
		   	setTimeout(function(){showAddressImage();}, 20);  		
	       } 
	catch(e){}
	}
	function resetImage1(){
	try{
		   	setTimeout(function(){showAddressImage1();}, 20);  		
	       }
	catch(e){}
	}
	function resetImage3(){
	try{
		   	setTimeout(function(){showAddressImageSub1();}, 20);  		
	       }
    catch(e){}
    }
    function resetImage4(){
    try{
		   	setTimeout(function(){showAddressImageSub2();}, 20);  		
	 }
	 catch(e){}
	 }
	    try{
	    checkMultiAuthorization();
	    }
	    catch(e){}
	    try{
	    showEmailImage();        
	    }
	    catch(e){}
	    try{
	    showOriginAgentEmailImage();
	    }
	    catch(e){}
	    try{
	    showDestinationAgentEmailImage();
	    }
	    catch(e){}
	    try{
	    showSubOriginAgentEmailImage();
	    }
	    catch(e){}
	   try{
	    showSubDestinationAgentEmailImage();
	    }
	    catch(e){}
	    try{
	    showBrokerEmailImage();
	    }
	    catch(e){}
	    try{
	    showForwarderEmailImage();
	    }
	    catch(e){}
	    	    <sec-auth:authComponent componentId="module.script.form.agentScript">
window.onload = function() { 
		trap();
		try{ 
		document.getElementById("linkup_Network").style.display="none";
        document.getElementById("linkup_originAgentExSO").style.display="none";
        document.getElementById("linkup_destinationAgentExSO").style.display="none";
        document.getElementById("linkup_originSubAgentExSO").style.display="none";
        document.getElementById("linkup_destinationSubAgentExSO").style.display="none";
        }catch(e){}
		var elementsLen=document.forms['trackingStatusForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['trackingStatusForm'].elements[i].type=='text')
					{
						document.forms['trackingStatusForm'].elements[i].readOnly =true;
						document.forms['trackingStatusForm'].elements[i].className = 'input-textUpper';						
					}
					else
					{						document.forms['trackingStatusForm'].elements[i].disabled=true;
					}						
			}
			if(document.forms['trackingStatusForm'].elements['saveButton'])
			{
				document.forms['trackingStatusForm'].elements['saveButton'].disabled=false;
			}
			if(document.forms['trackingStatusForm'].elements['Reset'])
			{
				document.forms['trackingStatusForm'].elements['Reset'].disabled=false;
			}
			<sec-auth:authScript tableList="trackingStatus,miscellaneous" formNameList="trackingStatusForm,trackingStatusForm,trackingStatusForm" transIdList='${serviceOrder.shipNumber}'>
	
			</sec-auth:authScript>
				}		
 </sec-auth:authComponent> 
<sec-auth:authComponent componentId="module.script.form.corpAccountScript">
	window.onload = function() { 
		//trap();
		try{
		document.getElementById("linkup_Network").style.display="none";
        document.getElementById("linkup_originAgentExSO").style.display="none";
        document.getElementById("linkup_destinationAgentExSO").style.display="none";
        document.getElementById("linkup_originSubAgentExSO").style.display="none";
        document.getElementById("linkup_destinationSubAgentExSO").style.display="none";
        }catch(e){}
		var elementsLen=document.forms['trackingStatusForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['trackingStatusForm'].elements[i].type=='text')
					{
						document.forms['trackingStatusForm'].elements[i].readOnly =true;
						document.forms['trackingStatusForm'].elements[i].className = 'input-textUpper';
								}
					else
					{
						document.forms['trackingStatusForm'].elements[i].disabled=true;
					}						
			}			
	}
</sec-auth:authComponent>
<sec-auth:authComponent componentId="module.script.form.vendettiScript">
window.onload = function() { 
		trap();
		try{
		document.getElementById("linkup_Network").style.display="none";
        document.getElementById("linkup_originAgentExSO").style.display="none";
        document.getElementById("linkup_destinationAgentExSO").style.display="none";
        document.getElementById("linkup_originSubAgentExSO").style.display="none";
        document.getElementById("linkup_destinationSubAgentExSO").style.display="none";
        }catch(e){}
		var elementsLen=document.forms['trackingStatusForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['trackingStatusForm'].elements[i].type=='text')
					{
						document.forms['trackingStatusForm'].elements[i].readOnly =true;
						document.forms['trackingStatusForm'].elements[i].className = 'input-textUpper';						
					}
					else
					{
						document.forms['trackingStatusForm'].elements[i].disabled=true;
					}
			if(document.forms['trackingStatusForm'].elements[i].type=='radio')
			{
				var n=document.forms['trackingStatusForm'].elements[i].id;
				document.forms['trackingStatusForm'].elements[i].disabled=false;
			}						
			}		
			if(document.forms['trackingStatusForm'].elements['saveButton'])
			{
				document.forms['trackingStatusForm'].elements['saveButton'].disabled=false;
			}
			if(document.forms['trackingStatusForm'].elements['Reset'])
			{
					document.forms['trackingStatusForm'].elements['Reset'].disabled=false;
			}
					<sec-auth:enableScript tableList="trackingStatus,miscellaneous" formNameList="trackingStatusForm,trackingStatusForm,trackingStatusForm" >
				</sec-auth:enableScript>
				}		
 </sec-auth:authComponent>
 <sec-auth:authComponent componentId="module.script.form.partnerScript">
window.onload = function() { 
		trap();
		try{
		document.getElementById("linkup_Network").style.display="none";
        document.getElementById("linkup_originAgentExSO").style.display="none";
        document.getElementById("linkup_destinationAgentExSO").style.display="none";
        document.getElementById("linkup_originSubAgentExSO").style.display="none";
        document.getElementById("linkup_destinationSubAgentExSO").style.display="none";
        }catch(e){}
		var elementsLen=document.forms['trackingStatusForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['trackingStatusForm'].elements[i].type=='text')
					{
						document.forms['trackingStatusForm'].elements[i].readOnly =true;
						document.forms['trackingStatusForm'].elements[i].className = 'input-textUpper';
						}
					else
					{
						document.forms['trackingStatusForm'].elements[i].disabled=true;
					}
			if(document.forms['trackingStatusForm'].elements[i].type=='radio')
			{
				var n=document.forms['trackingStatusForm'].elements[i].id;
				document.forms['trackingStatusForm'].elements[i].disabled=false;
			}
			}
		
			if(document.forms['trackingStatusForm'].elements['saveButton'])
			{
				document.forms['trackingStatusForm'].elements['saveButton'].disabled=false;
			}
			if(document.forms['trackingStatusForm'].elements['Reset'])
			{
				
				document.forms['trackingStatusForm'].elements['Reset'].disabled=false;
			}
			<sec-auth:enableScript tableList="trackingStatus,miscellaneous" formNameList="trackingStatusForm,trackingStatusForm,trackingStatusForm" >
			</sec-auth:enableScript>			
	}		
 </sec-auth:authComponent>
 </script>
 <script type="text/javascript">
 var fieldName = document.forms['trackingStatusForm'].elements['field'].value;
 if(fieldName=='trackingStatus.loadA'){
  	fieldName=document.forms['trackingStatusForm'].elements['loadA'].name;
 }else if(fieldName=='trackingStatus.packA'){
 	fieldName = document.forms['trackingStatusForm'].elements['packA'].name;
  }else if(fieldName=='trackingStatus.deliveryA'){
	fieldName = document.forms['trackingStatusForm'].elements['deliveryA'].name;
	
 }else{
	fieldName = document.forms['trackingStatusForm'].elements['field'].value;
 }
 var fieldName1 = document.forms['trackingStatusForm'].elements['field1'].value;
 if(fieldName1=='trackingStatus.loadA'){
 	fieldName1=document.forms['trackingStatusForm'].elements['loadA'].name;
 }else if(fieldName1=='trackingStatus.packA'){
 	fieldName1=document.forms['trackingStatusForm'].elements['packA'].name;
 }else if(fieldName1=='trackingStatus.deliveryA'){
	 fieldName1=document.forms['trackingStatusForm'].elements['deliveryA'].name;
 }else{
	fieldName1 = document.forms['trackingStatusForm'].elements['field1'].value;
 }
 if(fieldName!=''){
	 document.forms['trackingStatusForm'].elements[fieldName].className = 'rules-textUpper';
	 animatedcollapse.addDiv('agentroles', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('weights', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('initiation', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('orgpack', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('sitorigin', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('interstate', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('orgfwd', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('military','fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('gstfwd', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('transport', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('destination', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('sitdestination', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('storage', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('customs', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('weightnvolumetracsub', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('weightnvolumetracsub2', 'fade=0,persist=0,show=1')
	 animatedcollapse.init()
 }
 if(fieldName1!=''){
	 document.forms['trackingStatusForm'].elements[fieldName1].className = 'rules-textUpper';
	 animatedcollapse.addDiv('agentroles', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('weights', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('initiation', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('orgpack', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('sitorigin', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('interstate', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('orgfwd', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('military','fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('gstfwd', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('transport', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('destination', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('sitdestination', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('storage', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('customs', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('weightnvolumetracsub', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('weightnvolumetracsub2', 'fade=0,persist=0,show=1')
	 animatedcollapse.init()
 }	    
</script>
