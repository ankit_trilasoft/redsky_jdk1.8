DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`copyAccountingTemplate`$$
CREATE DEFINER=`root`@`%` PROCEDURE  `redsky`.`copyAccountingTemplate`(IN l_updatedby varchar(100),IN FromContract varchar(100),IN l_corpid varchar(5),
IN ToContract varchar(100), OUT l_total INT)
begin

set l_total=0;

DROP TABLE IF EXISTS defaultaccountlinetemp;
create table defaultaccountlinetemp
(SELECT * FROM defaultaccountline WHERE corpid=l_corpid and contract=FromContract);
ALTER TABLE `redsky`.`defaultaccountlinetemp` CHANGE COLUMN `id` `id` BIGINT(20) NULL DEFAULT '0'  ;
update defaultaccountlinetemp set id=NULL ,contract=ToContract,createdon=now(),createdby=l_updatedby,updatedby=l_updatedby,updatedon=now();

DROP TABLE IF EXISTS defaultaccountlinetemp_1;
create table defaultaccountlinetemp_1
(SELECT distinct a.* FROM defaultaccountlinetemp a, charges b WHERE a.corpid=l_corpid and a.contract=ToContract and if(a.chargeCode='',1=1,a.chargeCode=b.charge) and a.contract=b.contract);

select count(*) into l_total from defaultaccountlinetemp_1;
set l_total=l_total;
insert into defaultaccountline
select * from defaultaccountlinetemp_1;

END $$

DELIMITER ;