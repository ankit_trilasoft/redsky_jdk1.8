package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;


import com.trilasoft.app.dao.NetworkPartnersDao; 
import com.trilasoft.app.model.NetworkPartners; 
import com.trilasoft.app.service.NetworkPartnersManager;

public class NetworkPartnersManagerImpl extends GenericManagerImpl<NetworkPartners, Long> implements NetworkPartnersManager {
	
	NetworkPartnersDao networkPartnersDao;
	
	public NetworkPartnersManagerImpl(NetworkPartnersDao networkPartnersDao) {
		  super(networkPartnersDao); 
		  this.networkPartnersDao = networkPartnersDao; 
	}

	public List getCorpidList(String sessionCorpID) { 
		return networkPartnersDao.getCorpidList(sessionCorpID);
	}

	public List findNetworkPartnerList(String sessionCorpID,String selectedCorpId) {
		return networkPartnersDao.findNetworkPartnerList(sessionCorpID, selectedCorpId);
	}
	public List findByCorpId(String sessionCorpID, String selectedCorpId){
		return networkPartnersDao.findByCorpId(sessionCorpID, selectedCorpId);
	}

	public Integer countNetworkPartnerByCorpId(String sessionCorpID,String selectedCorpId) {
		return networkPartnersDao.countNetworkPartnerByCorpId(sessionCorpID, selectedCorpId);
	}

	public void removeByCorpId(String sessionCorpID, String selectedCorpId) {
		networkPartnersDao.removeByCorpId(sessionCorpID, selectedCorpId);
	}

	public Integer countCompanyDivisionByCorpId(String sessionCorpID, String selectedCorpId) {
		return networkPartnersDao.countCompanyDivisionByCorpId(sessionCorpID,selectedCorpId);
	}
}
