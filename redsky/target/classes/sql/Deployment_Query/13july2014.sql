// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`company` ADD COLUMN `singleCompanyDivisionInvoicing` BIt(1) DEFAULT b'0'

ALTER TABLE `redsky`.`customersurveyfeedback` ADD COLUMN `serviceDetails` VARCHAR(25) AFTER `feedBack`;

ALTER TABLE `redsky`.`customersurveyfeedback` ADD COLUMN `serviceForDestEmail` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`defaultaccountline` ADD COLUMN `equipment` VARCHAR(20) NULL  AFTER `destinationCountry` ;

ALTER TABLE `redsky`.`refmaster` ADD COLUMN `status` VARCHAR(10) AFTER `label6`;

update refmaster set status='Active' ;

ALTER TABLE `redsky`.`refquoteservices` ADD COLUMN `displayOrder` INTEGER DEFAULT null AFTER `updatedBy`;

insert  into networkdatafields(modelName, fieldName, transactionType,createdBy, createdOn, updatedBy, updatedOn,useSysDefault,toModelName,toFieldName,type)
values('Billing','wareHousereceiptDate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
('Billing','wareHousereceiptDate','Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');

insert  into networkdatafields(modelName, fieldName, transactionType,createdBy, createdOn, updatedBy, updatedOn,useSysDefault,toModelName,toFieldName,type)
values('Billing','locationAgentCode','Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','locationAgentCode','Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','locationAgentName','Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','locationAgentName','Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `contract` VARCHAR(100) AFTER `emailPrintOption`,
 ADD COLUMN `networkPartnerCode` VARCHAR(10) AFTER `contract`,
 ADD COLUMN `source` VARCHAR(40) AFTER `networkPartnerCode`,
 ADD COLUMN `vendorCode` VARCHAR(8) AFTER `source`,
 ADD COLUMN `insuranceHas` VARCHAR(1) AFTER `vendorCode`,
 ADD COLUMN `insuranceOption` VARCHAR(150) AFTER `insuranceHas`,
 ADD COLUMN `defaultVat` VARCHAR(30) AFTER `insuranceOption`,
 ADD COLUMN `billToAuthorization` VARCHAR(50) AFTER `defaultVat`;


ALTER TABLE `redsky`.`dspdetails` 
ADD COLUMN `HOM_estimatedHSRReferral` DECIMAL(19,2) NULL DEFAULT NULL  AFTER `DSS_comment` , 
ADD COLUMN `HOM_actualHSRReferral` DECIMAL(19,2) NULL DEFAULT NULL  AFTER `HOM_estimatedHSRReferral` , 
ADD COLUMN `HSM_offerDate` DATETIME NULL DEFAULT NULL  AFTER `HOM_actualHSRReferral` , 
ADD COLUMN `HSM_closingDate` DATETIME NULL DEFAULT NULL  AFTER `HSM_offerDate` , 
ADD COLUMN `TAC_timeAuthorized` VARCHAR(65) NULL DEFAULT NULL  AFTER `HSM_closingDate` ;

Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Days', 'Days', 40, 'Time_Authorized','en',now(),'kkumar',now(),'kkumar'),
('HSRG', 'Weeks', 'Weeks', 40, 'Time_Authorized','en',now(),'kkumar',now(),'kkumar'),
('HSRG', 'Months', 'Months', 40, 'Time_Authorized','en',now(),'kkumar',now(),'kkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('HSRG','Time_Authorized',20,true,now(),'kkumar',now(),'kkumar',1,1,'');


ALTER TABLE `redsky`.`accountline` ADD COLUMN `dynamicNavisionflag` BIT(1) DEFAULT b'0' AFTER `accountLineScostElementDescription`,
 ADD COLUMN `deleteDynamicNavisionflag` BIT(1) DEFAULT b'0' AFTER `dynamicNavisionflag`,
 ADD COLUMN `oldCompanyDivision` VARCHAR(10) AFTER `deleteDynamicNavisionflag`,
 ADD COLUMN `sendDynamicNavisionDate` DATETIME AFTER `oldCompanyDivision`,
 ADD COLUMN `dynamicNavXfer` VARCHAR(100) AFTER `sendDynamicNavisionDate`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `networkPartnerName` VARCHAR(255) NULL  AFTER `billToAuthorization` , 
ADD COLUMN `vendorName` VARCHAR(255) NULL  AFTER `networkPartnerName` ;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `soListView` VARCHAR(25) DEFAULT '' AFTER `moveType`;

ALTER TABLE `redsky`.`sodashboard` ADD COLUMN `customFlag` VARCHAR(45) DEFAULT '' AFTER `loadingDt`,
ADD COLUMN `customDate` DATETIME DEFAULT null  AFTER `customFlag`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `localJobs` VARCHAR(500) DEFAULT '' AFTER `hubWarehouseOperationalLimit`;

update accountline set dynamicNavisionflag=false,deleteDynamicNavisionflag=false,oldCompanyDivision=null,dynamicNavXfer=null,sendDynamicNavisionDate=null
where corpId in ('VOER','VOCZ','VOAO','VORU');

select corpid,rulenumber,expression from todorule where expression like '%serviceorder.mode%' and corpid in ('STAR','ICMG');

select distinct corpid from serviceorder where mode='overland';

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'TRU', 'Truck', 3, 'MODE','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', 'RAI', 'Rail', 3, 'MODE','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP');

SELECT * FROM parametercontrol where parameter='MODE';

update parametercontrol set hybridflag=0 where parameter='MODE';

select corpid,code,description from refmaster where parameter='MODE' and code not in
(select code from refmaster where parameter='MODE' and corpid='TSFT');

select code,group_concat(corpid) from refmaster
where parameter='MODE' group by code having count(code)>1;

delete from refmaster where parameter='MODE' and corpid<>'TSFT' and code='TRU';

delete from refmaster where parameter='MODE' and corpid not in ('TSFT')
and code<>'STO';

update serviceorder set mode='Rail' where  mode='Overland' and corpid not in ('ICMG','SSCW','CWMS');
update serviceorder set mode='Truck' where  mode='Overland' and corpid in ('ICMG','SSCW','CWMS');
update serviceorder set mode='' where  mode='Storage' and corpid ='SSCW';

