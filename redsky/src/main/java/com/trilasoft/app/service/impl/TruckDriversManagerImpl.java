package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.TruckDriversDao;
import com.trilasoft.app.model.TruckDrivers;
import com.trilasoft.app.service.TruckDriversManager;

public class TruckDriversManagerImpl extends GenericManagerImpl<TruckDrivers, Long> implements TruckDriversManager{
	TruckDriversDao truckDriversDao;
	public TruckDriversManagerImpl(TruckDriversDao truckDriversDao){
		super(truckDriversDao);
		this.truckDriversDao=truckDriversDao;
	}
	public List getDriverValidation(String driverCode,String truckNumber,String partnerCode, String corpId){
		return truckDriversDao.getDriverValidation(driverCode, truckNumber,partnerCode, corpId);
	}
	public List findDriverList(String truckNumber, String corpId){
		return truckDriversDao.findDriverList(truckNumber, corpId);
	}
	public List getPrimaryFlagStatus(String truckNumber, String corpId){
		return truckDriversDao.getPrimaryFlagStatus(truckNumber, corpId);
	}
}
