<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Wt/So Details</title>   
<meta name="heading" content="Tool Tip"/> 
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b></b></td>
	<td align="right"  style="width:30px;">
		<img align="right" class="openpopup" onclick="hideTooltip();" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>
<s:set name="wtSoDetails" value="wtSoDetails" scope="request"/>   
<display:table name="wtSoDetails" class="table" requestURI="" id="wtSoDetails"  style="width:100%;" partialList="true" size="1">
<display:column title="SO#" style="width:15%">
<c:if test="${wtSoDetails.what=='L' || wtSoDetails.what=='A' }">
<c:out value="${wtSoDetails.shipnumber}"/>
</c:if>
</display:column>
  <display:column   title="Ticket#" style="width:15%">
  <c:if test="${wtSoDetails.what=='L' || wtSoDetails.what=='A' }">
<c:out value="${wtSoDetails.ticket}"/>
</c:if>
  </display:column>
</display:table>
  

