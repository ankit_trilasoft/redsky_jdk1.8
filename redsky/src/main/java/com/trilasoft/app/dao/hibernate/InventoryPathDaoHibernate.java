package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.InventoryPathDao;
import com.trilasoft.app.model.InventoryPath;

public class InventoryPathDaoHibernate extends GenericDaoHibernate<InventoryPath,Long> implements InventoryPathDao{

	public InventoryPathDaoHibernate() {
		super(InventoryPath.class);
	}

	public List getListByInventoryId(Long id) {
	    return getHibernateTemplate().find("from InventoryPath where inventoryDataId='"+id+"'");
	}

	public  List getListByAccessInfoId(Long id,String type){
      return getHibernateTemplate().find("from InventoryPath where accessInfoId='"+id+"' and accessInfoType='"+type+"'");

	}
	public List getInventoryPackingImg(Long id){
		 return getHibernateTemplate().find("from InventoryPath where inventoryPackingId='"+id+"'");
	}

	public void deleteAllWorkTicketInventory(String field, String corpID,Long fieldVal) {
		getHibernateTemplate().bulkUpdate("delete from InventoryPath where "+field+" = '"+fieldVal+"' and corpID='"+corpID+"'");
	}

	public List getListByLocId(Long id, String fieldName) {
		return getHibernateTemplate().find("from InventoryPath where "+fieldName+"='"+id+"'");
	}
	public void deleteLInkedImgs(Long id){
		this.getSession().createSQLQuery("delete from inventorypath where networkSynchedId='"+id+"' and id!='"+id+"'").executeUpdate();	
	}
	
}
