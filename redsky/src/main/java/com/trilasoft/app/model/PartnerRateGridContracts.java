package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="partnerrategridcontracts")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class PartnerRateGridContracts  extends BaseObject{
	private Long id;
	private Long partnerRateGridID;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private String discountCompany;
	private Date effectiveDate;
	private String scope;
	private String contract;
	private String discountBaseRate=new String("0");
	private String discountAddlCharges=new String("0");
	private String discountHaulingCharges=new String("0");
	private String discountAutoHandling=new String("0");
	private String discountSITCharges=new String("0");
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append(
				"partnerRateGridID", partnerRateGridID)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("updatedOn", updatedOn)
				.append("createdOn", createdOn).append("discountCompany",
						discountCompany).append("effectiveDate", effectiveDate)
				.append("scope", scope).append("contract", contract).append(
						"discountBaseRate", discountBaseRate).append(
						"discountAddlCharges", discountAddlCharges).append(
						"discountHaulingCharges", discountHaulingCharges)
				.append("discountAutoHandling", discountAutoHandling).append(
						"discountSITCharges", discountSITCharges).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerRateGridContracts))
			return false;
		PartnerRateGridContracts castOther = (PartnerRateGridContracts) other;
		return new EqualsBuilder().append(id, castOther.id).append(
				partnerRateGridID, castOther.partnerRateGridID).append(corpID,
				castOther.corpID).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(createdOn,
						castOther.createdOn).append(discountCompany,
						castOther.discountCompany).append(effectiveDate,
						castOther.effectiveDate).append(scope, castOther.scope)
				.append(contract, castOther.contract).append(discountBaseRate,
						castOther.discountBaseRate).append(discountAddlCharges,
						castOther.discountAddlCharges).append(
						discountHaulingCharges,
						castOther.discountHaulingCharges).append(
						discountAutoHandling, castOther.discountAutoHandling)
				.append(discountSITCharges, castOther.discountSITCharges)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(partnerRateGridID)
				.append(corpID).append(createdBy).append(updatedBy).append(
						updatedOn).append(createdOn).append(discountCompany)
				.append(effectiveDate).append(scope).append(contract).append(
						discountBaseRate).append(discountAddlCharges).append(
						discountHaulingCharges).append(discountAutoHandling)
				.append(discountSITCharges).toHashCode();
	}
	
	@Column(length=100)
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	
	@Column(length=10)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length=7)
	public String getDiscountAddlCharges() {
		return discountAddlCharges;
	}
	public void setDiscountAddlCharges(String discountAddlCharges) {
		this.discountAddlCharges = discountAddlCharges;
	}
	
	@Column(length=7)
	public String getDiscountAutoHandling() {
		return discountAutoHandling;
	}
	public void setDiscountAutoHandling(String discountAutoHandling) {
		this.discountAutoHandling = discountAutoHandling;
	}
	
	@Column(length=7)
	public String getDiscountBaseRate() {
		return discountBaseRate;
	}
	public void setDiscountBaseRate(String discountBaseRate) {
		this.discountBaseRate = discountBaseRate;
	}
	
	@Column(length=10)
	public String getDiscountCompany() {
		return discountCompany;
	}
	public void setDiscountCompany(String discountCompany) {
		this.discountCompany = discountCompany;
	}
	
	@Column(length=7)
	public String getDiscountHaulingCharges() {
		return discountHaulingCharges;
	}
	public void setDiscountHaulingCharges(String discountHaulingCharges) {
		this.discountHaulingCharges = discountHaulingCharges;
	}
	
	@Column(length=7)
	public String getDiscountSITCharges() {
		return discountSITCharges;
	}
	public void setDiscountSITCharges(String discountSITCharges) {
		this.discountSITCharges = discountSITCharges;
	}
	
	@Column
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(length=20)
	public Long getPartnerRateGridID() {
		return partnerRateGridID;
	}
	public void setPartnerRateGridID(Long partnerRateGridID) {
		this.partnerRateGridID = partnerRateGridID;
	}
	
	@Column(length=225)
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
}

