package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.SurveyResponseDtlDao;
import com.trilasoft.app.model.SurveyResponseDtl;
import com.trilasoft.app.service.SurveyResponseDtlManager;

public class SurveyResponseDtlManagerImpl extends GenericManagerImpl<SurveyResponseDtl,Long> implements SurveyResponseDtlManager{
	SurveyResponseDtlDao surveyResponseDtlDao;
	public SurveyResponseDtlManagerImpl(SurveyResponseDtlDao surveyResponseDtlDao) {
		super(surveyResponseDtlDao);
		this.surveyResponseDtlDao=surveyResponseDtlDao;
	}

}
