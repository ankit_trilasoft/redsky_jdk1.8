-- ALTER TABLE `redsky`.`carton` ADD COLUMN `networkId` BIGINT(20) DEFAULT NULL AFTER `description`;

-- ALTER TABLE `redsky`.`vehicle` ADD COLUMN `networkId` BIGINT(20) DEFAULT NULL AFTER `classEPA`;

-- ALTER TABLE `redsky`.`servicepartner` ADD COLUMN `networkId` BIGINT(20) DEFAULT NULL AFTER `servicePartnerId`;

-- ALTER TABLE `redsky`.`consigneeInstruction` ADD COLUMN `networkId` BIGINT(20) DEFAULT NULL AFTER `collect`;

ALTER TABLE `redsky`.`custom` ADD COLUMN `networkId` BIGINT(20) DEFAULT NULL AFTER `serviceOrderId`;

ALTER TABLE `redsky`.`dsfamilydetails` ADD COLUMN `networkId` BIGINT(20) DEFAULT NULL AFTER `age`;

ALTER TABLE `redsky`.`address` ADD COLUMN `networkId` BIGINT(20) DEFAULT NULL AFTER `phone`;

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `contractType` VARCHAR(3),ADD COLUMN `isNetworkGroup` BIT(1) DEFAULT b'0' AFTER `contractType`;

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `contractType` VARCHAR(3) ;

-- ALTER TABLE `redsky`.`container` ADD COLUMN `networkId` BIGINT(20) DEFAULT NULL  ;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `securityChecked` BIT(1) DEFAULT b'0' AFTER `commissionable`;

ALTER TABLE `redsky`.`company` ADD COLUMN `postingDateStop` bit(1) DEFAULT b'0' AFTER `creditInvoiceSequence`;
