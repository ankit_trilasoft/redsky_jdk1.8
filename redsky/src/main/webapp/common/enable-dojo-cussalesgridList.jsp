<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head> 
  
     <c:if test="${not empty ship && ship=='CS' }">
    <title>Material And Equipment Customer Sales</title> 
	 <meta name="heading" content="Material And Equipment Customer Sales"/> 
	 </c:if>
	  <c:if test="${not empty ship && ship=='OS' }">
    <title>Material And Equipment OO Sales</title> 
	 <meta name="heading" content="Material And Equipment OO Sales"/> 
	 </c:if>
	 <c:if test="${ empty ship  }">
    <title>Overall Customer And OO Sales </title> 
	 <meta name="heading" content="Overall Customer And OO Sales "/> 
	 </c:if>
	 </head>

<!-- load the dojo toolkit base -->
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>"
    djConfig="parseOnLoad:true, isDebug:false"></script>
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/redskydojo.js'/>"></script>
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojox/form/DropDownSelect.js'/>"></script>



<style type="text/css">
  @import "<c:url value='/scripts/dojo/dijit/themes/nihilo/nihilo.css'/>";
  @import "<c:url value='/scripts/dojo/dojox/grid/_grid/Grid.css'/>";
  @import "<c:url value='/scripts/dojo/dojox/grid/_grid/nihiloGrid.css'/>";
  @import "<c:url value='/scripts/dojo/dojox/form/resources/DropDownSelect.css'/>";
  @import "<c:url value='/styles/${appConfig["csstheme"]}/dojo-layout.css'/>";

 


</style>
<script type="text/javascript">

function setCbHiddenId(temp){
	//alert("editttttttttttttttttttttt onchange vvvvvvvvvvvvvvvvvvvvvvvv");
}
function search(){
	
	//  var id = document.forms['gridForm'].elements['id'].value;
	//var discript = document.forms['gridForm'].elements['itemsJbkEquip.descript'].value;
	//alert("hiiiiiii");
	var wh=document.getElementById('whouse').value;
	var url = "itemsJbkEquipCustomerSales.html?whouse="+wh;
	document.forms['gridForm'].action = url;
	document.forms['gridForm'].submit();
	}

</script>
 <script type="text/javascript">
  dojo.require("dojox.grid.Grid");
  dojo.require("dojox.grid._data.model");
  dojo.require("dojox.grid._data.dijitEditors");
  dojo.require("dojo.parser");
  dojo.require("dojox.grid._grid.publicEvents");  
  dojo.require("dojox.form.DropDownSelect"); 
		    
  

  function getColumnListAry(){
	  var columnListAsStr = '';
	  <c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
	  	if (columnListAsStr != '') columnListAsStr += ',';
	  	columnListAsStr += '<c:out value="${fieldName}"/>';	
	  </c:forEach> 	
	  return columnListAsStr.split(',');	
  }
  	  
  function getColumnName(inputColumnIndex){
	  return getColumnListAry()[inputColumnIndex];
  }

  function getColumnNameFromObj(inputObj){
	  return getColumnName(getParentTd(inputObj).getAttribute('idx'));
  }

  function getColumnValue(columnName){
	    var selectedRow = grid2.selection.getSelected()[0];
	    var rowData = grid2.model.data[selectedRow];
	    return rowData[getColumnIndex(columnName)];
  }

  function setColumnValue(columnName, newValue){
	    var selectedRow = grid2.selection.getSelected()[0];
	    var rowData = grid2.model.data[selectedRow];
	    rowData[getColumnIndex(columnName)] = newValue;
  }
  
  function getColumnIndex(inputColumnName){
	  var i=0;
	  var columnListAry = getColumnListAry();
	  for (i=0;i<=columnListAry.length;i++) {
	  	if (columnListAry[i] == inputColumnName) {
	  		return i;
	  	}
	  }
  }
  

  //coupled to html used for rendering the grid 
  function getParentTd(inputObj){
	  var loopfor = 5;
	  var loopCnt = 0;
	  var parentNodeObj = inputObj.parentNode; 
	  while (true) {
		  if (parentNodeObj == null || parentNodeObj.tagName == 'TD' || loopCnt >= loopfor) break;
		  parentNodeObj = parentNodeObj.parentNode;
		  loopCnt++;
		  
  		}
		return parentNodeObj;
  }
  

	
	button = dojo.byId('dojoxGrid-input');
	dojo.connect(button, 'onchange', 'processOnChange');
	
	  
  
</script>
<script type="text/javascript">
var showDetail = function(e){	
    var value = e.cellNode.firstChild.data;
    var rowData = grid2.model.data[e.rowIndex];
   var id=rowData[0];
  var chn=id+":"+e.cellIndex
  var pp=document.forms['gridForm'].elements['chngMap'];
if(pp.value==null || pp.value==""){
	pp.value=chn;
}else if(pp.value!=null && pp.value!=""){
	pp.value=pp.value+","+chn;
}else{
	
}
//alert(document.forms['gridForm'].elements['chngMap'].value);

}
var showIdForInvoice = function(e){
	//alert("sinle click...........")
    var value = e.cellNode.firstChild.data;
    var rowData = grid2.model.data[e.rowIndex];
    var id=rowData[0];
    var seqNumber=rowData[9];
    document.forms['operationsResourceLimits'].elements['id'].value=id;
    //alert(id);
    //alert(id1);

}

dojo.addOnLoad(function(){
   dojo.connect(grid2, "onCellDblClick", showDetail);
   dojo.connect(grid2,"onApplyCellEdit", function( val, row, col )
	        {	     
       //alert(col)     
	           if(col==7){		          	      		 
	      		    var rowData = grid2.model.data[row];
	      		   var soldqty=rowData[2];	      		 	
	      		   var retqty=rowData[7];
	      		   if(retqty.toString().indexOf(".")>-1){
	      			 document.forms['gridForm'].elements['negValMsg'].value="1";
		      		   //alert("Decimal return values are not allowed");
	      		   }else{
	      			 document.forms['gridForm'].elements['negValMsg'].value="0";
	      		   }
	      		 var jbkItmId=rowData[0];  
	      		 var matchQty=0;
	      		//alert(jbkItmId);
	      		//alert('${materialsList}');
	      		 <c:forEach var="itm" items="${materialsList}" >
	      		 if(jbkItmId=='${itm.id}'){
	      		matchQty='${itm.returned}'
	      		 }
	      		
	      		 </c:forEach>
	      		// alert(matchQty); 
	      		 var diff=0;
	      		
	      		if(parseInt(retqty)>parseInt(matchQty)){
	      			diff=retqty-matchQty;
	      		 }	else{
	      			diff=matchQty-retqty;
	      		 }	
	      		     		  
	      			   if(parseInt(soldqty)<parseInt(retqty)){
	      				 //rowData[7]=0;
	      				   //alert("match") 
	      				  var pp=document.forms['gridForm'].elements['lineFail'].value;
	      				  if(pp!=null && pp!=''){
	      					//alert("match1") 
		      				  pp=pp+","+row;
	      				  }else{
	      					//alert("match2") 
	      					pp=row;
	      				  }
	      				document.forms['gridForm'].elements['lineFail'].value=pp;
	      				//alert(document.forms['gridForm'].elements['lineFail'].value);
	      				//alert('edit');
	      				   //alert("Return Quantity should not exceed quantity sold for theline number");
	      			   }
	      		   
	      	
	        }});
   dojo.connect(grid2, "onCellClick", showIdForInvoice);
});	 
</script>
<script>
function elmLoop(){

	var theForm = document.forms[0]

	   for(i=0; i<theForm.elements.length; i++){
	   var alertText = ""
	   alertText += "Element Type: " + theForm.elements[i].type + "\n"

	      if(theForm.elements[i].type == "text" || theForm.elements[i].type == "textarea" || theForm.elements[i].type == "button"){
	      alertText += "Element Value: " + theForm.elements[i].value + "\n"
	      }
	      else if(theForm.elements[i].type == "checkbox"){
	      alertText += "Element Checked? " + theForm.elements[i].checked + "\n"
	      }
	      else if(theForm.elements[i].type == "select-one"){
	      alertText += "Selected Option's Text: " + theForm.elements[i].options[theForm.elements[i].selectedIndex].text + "\n"
	      }
	   alert(alertText)
	   }

	} 

</script>
<script type="text/javascript">
function sortArticles(){
	 // alert("hiiiiiiiiiiii");
}
<!--

//-->
</script>

<%@page import="com.trilasoft.app.webapp.util.GridDataProcessor"%>


<!-- ***********************move code from below once ready******* -->

<script type="text/javascript">

    // ==========================================================================
    // Create a data model
    // ==========================================================================
    <%
	String htmlCode = ((String[][])request.getAttribute("tableMetadata"))[0][2];				
	String pattern = "listRow\\[[0-9a-zA-Z_]+\\]"; 
    %>
    data = [
			<c:forEach var="listRow" items="${request[tableMetadata[0][0]]}" varStatus="status">
			[
			<c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
				<c:if test="${tableMetadata[2][fieldNameIteratorStatus.index] == 'html'}">
					"<%=GridDataProcessor.processHtml(htmlCode, pageContext.getAttribute("listRow"), pattern)%>"
				 </c:if>			
				<c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int' || tableMetadata[2][fieldNameIteratorStatus.index] == 'long' || tableMetadata[2][fieldNameIteratorStatus.index] == 'float'|| tableMetadata[2][fieldNameIteratorStatus.index] == 'double'|| tableMetadata[2][fieldNameIteratorStatus.index] == 'BigDecimal')}">

	 				<c:out value="${listRow[fieldName]}"/>
	 			</c:if>
				<c:if test="${tableMetadata[2][fieldNameIteratorStatus.index] == 'text'}">
				 	'<c:out value="${listRow[fieldName]}"/>'		
				 </c:if>
				  //   For Accountline.......//
				 <c:if test="${tableMetadata[2][fieldNameIteratorStatus.index] == 'string'}">
				 	'<c:out value="${listRow[fieldName]}"/>'		
				 </c:if>
				//// 
				 
				 <c:if test="${!fieldNameIteratorStatus.last}">,</c:if>
			</c:forEach> 
			] <c:if test="${!status.last}">,</c:if>			
			</c:forEach>
		];
		 material=[<c:forEach var="listRow" items="${invDocMap}" varStatus="status">
           <c:set var="list" value="${listRow}"/>
	     '<c:out value="${listRow.value}"/>-id-<c:out value="${listRow.key}"/>',
     </c:forEach>
      ];
      basis=[<c:forEach var="listRow" items="${bilingbasis}" varStatus="status">
           <c:set var="list" value="${listRow}"/>
	     '<c:out value="${listRow}"/>',
     </c:forEach>
      ];
      checkNew=[<c:forEach var="listRow" items="${billingcheckNew}" varStatus="status">
           <c:set var="list" value="${listRow}"/>
	     '<c:out value="${listRow}"/>',
     </c:forEach>
      ]; 
      
      
      job=[<c:forEach var="listRow" items="${job}" varStatus="status">
           <c:set var="list" value="${listRow}"/>'<c:out value="${fn:substring(list,0,fn:indexOf(list, '='))}"/>',
     </c:forEach>
      ];
        model = new dojox.grid.data.Table(null, data);
        
        
      var formatQuantity1 = 0;
      var formatRate1 = 0;
      var formatAmount1 = 0;
      var formatGrossQuantity1 =0;
      var formatReturnQuantity1 = 0;
       
    formatRate =   function calcRate(inDatum){
    	if(isNaN(inDatum)){
			inDatum = 0;
		}
    	formatRate1 = inDatum;
    	return  (inDatum);
    }
      
    formatGrossQuantity =   function calcGross(inDatum){
    	if(isNaN(inDatum)){
			inDatum = 0;
		} 
    	formatGrossQuantity1 = inDatum;
		return (inDatum);
	}
	
	formatReturnQuantity =   function calcReturn(inDatum){
		if(isNaN(inDatum)){
			inDatum = 0;
		}
    	formatReturnQuantity1 = inDatum;
    	return  (inDatum);
	}
   
   	formatQuantity =   function calcQuan(inDatum){
   		if(isNaN(inDatum)){
			inDatum = 0;
		}
   		if(inDatum==null){
			inDatum = 0;
		}
		if(inDatum == 0 || inDatum == ''){
			if((formatGrossQuantity1 !='Null' || formatReturnQuantity1 !='Null')){
	        	formatQuantity1 =((Math.round(formatGrossQuantity1*100)/100).toFixed(0)-(Math.round(formatReturnQuantity1*100)/100).toFixed(0))
	    		////formatQuantity1 = (formatGrossQuantity1-formatReturnQuantity1);
	    		return  (formatQuantity1);
	    		/// } else {
	    		/// return (inDatum);
     		}
		}else{
			formatQuantity1 = inDatum;
     		return (formatQuantity1);
     	}
	}
	
  	formatAmount =   function calcAmount(inDatum){
  		if(isNaN(inDatum)){
			inDatum = 0;
		}
  		if(inDatum == 0 || inDatum == ''){
    	if(formatRate1 !='Null' || formatQuantity1 !='Null'){
        		formatAmount1=(formatRate1*formatQuantity1);
       	return  ((Math.round(formatAmount1*100)/100).toFixed(2));  
    	}
  	    }else{
  	    	formatAmount1 = inDatum;
 		return (formatAmount1);
 	    }	
    }
      
    
    // ==========================================================================
    // Tie UI to the data model
    // ==========================================================================
    model.observer(this);
    modelChange = function(){
      dojo.byId("rowCount").innerHTML = 'Row count: ' + model.count;
    }
    
    // ==========================================================================
    // Custom formatters 
    // ==========================================================================
    formatCurrency = function(inDatum){
      return isNaN(inDatum) ? '...' : dojo.currency.format(inDatum, this.constraint);
    }
    formatNumber = function(inDatum){
      return isNaN(inDatum) ? '' : inDatum;
    }    
    formatDate = function(inDatum){
      return dojo.date.locale.format(new Date(inDatum), this.constraint);
    }
    
    // ==========================================================================
    // Grid structure
    // ==========================================================================
    
    
    
    
    
    
    
    gridLayout = [{
     type: 'dojox.GridRowView', width: '0px'
   },{
     defaultCell: { width: 4, styles: 'text-align: right;'  },
     rows: [[
           <c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
           <c:if test="${not empty ship}">
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'long') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'id'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input,formatter: function(id) {return '<a href="#" onClick="getEdit('+id+')">'+id+'</a>';}, width: 5}
                 </c:if>
                 </c:if>
                 <c:if test="${empty ship}">
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'long') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'id'}">
                     { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;',formatter: function(id) {return '<a href="#" onClick="getEdit('+id+')">'+id+'</a>';}, width: 5}
                   </c:if>
                   </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'long') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'id'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, formatter: formatNumber, width: 5}
               </c:if>
                <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'descript'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.ComboBox,options: material,width: 30,onClick:setCbHiddenId(this)}
                 </c:if>
                 <c:if test="${not empty ship}">
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'descript'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.ComboBox,options: material,width: 18,onClick:setCbHiddenId(this)}
               </c:if>
               </c:if>
               <c:if test="${empty ship}">
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                 tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'descript'}">
                 { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',options: material,width: 18,onClick:setCbHiddenId(this)}
             </c:if>
             </c:if>
               <c:if test="${empty ship}">
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'customerName'}">
               { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;',width: 10}
             </c:if>
             <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'payMethod'}">
             { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;',  width: 10}
           </c:if>
           </c:if>
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'string') && 	tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly'}"> 
							{ name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>',  width: '70px'}
				</c:if>
				<c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') && 	tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'basis'}"> 
							{ name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.ComboBox, options: basis, width: '200px'}
				</c:if>
				<c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') && 	tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'checkNew'}"> 
							{ name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.ComboBox, options: checkNew, width: '200px'}
				</c:if>
				<c:if test="${empty ship}">
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'totalCost'}">
                         { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;',  constraint: {min:0,max:999999 }, formatter: formatRate, width: '100px',editable: false}
                     /// { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.Dijit,editorClass: dijit.form.NumberTextBox, editorProps: {min:5.00, max:999.99, places:2}, formatter: formatRate, width: 5}
                 </c:if>
                 </c:if>
                 /* <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'cost'}">
                  		 { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999 }, formatter: formatRate, width: 5}
              		////  { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.Dijit,editorClass: dijit.form.NumberTextBox, editorProps: {min:5.00, max:999.99, places:2}, formatter: formatRate, width: 5}
               </c:if> */
               /*  <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'qty'}">
                  /// { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: { places: 0 }, formatter: formatGrossQuantity, width: 10, maxlength: 12, id="qty" }
                     { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999, places: 0 }, formatter: formatGrossQuantity, width: 10 }
              
                 </c:if> */
                 <c:if test="${not empty ship}">
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'soldQty'}">
                 /// { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: { places: 0 }, formatter: formatGrossQuantity, width: 10, maxlength: 12, id="qty" }
                    { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999, places: 0 }, formatter: formatGrossQuantity, width: 10 }
             
                </c:if>
                </c:if>
                <c:if test="${empty ship}">
                <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'soldQty'}">
                /// { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: { places: 0 }, formatter: formatGrossQuantity, width: 10, maxlength: 12, id="qty" }
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999, places: 0 }, formatter: formatGrossQuantity, width: 10 }
            
               </c:if>
               </c:if>
                
                <c:if test="${empty ship}">
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'returned'}">
                  /// { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: { places: 0 }, formatter: formatGrossQuantity, width: 10, maxlength: 12, id="qty" }
               	 { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>',styles: 'text-align: right;',editor: dojox.grid.editors.Input,constraint: {min:0,max:999999, places: 0 }, formatter: formatGrossQuantity, width: 10}
               </c:if>
               </c:if>
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'ooprice'}">
               /// { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: { places: 0 }, formatter: formatGrossQuantity, width: 10, maxlength: 12, id="qty" }
            	 { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', constraint: {min:0,max:999999, places: 0 }, formatter: formatGrossQuantity, width: 10,editable: false }
            </c:if>
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'freeQty'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999, places: 0 }, formatter: formatReturnQuantity, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'minLevel'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999, places: 0 }, formatter: formatReturnQuantity, width: 10}
               </c:if>
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'maxResourceLimit'}">
               { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999, places: 0 }, formatter: formatReturnQuantity, width: 10}
           </c:if>
                /* <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'actualQty'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, formatter: formatQuantity, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'actualQty'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, formatter: formatQuantity,  width: 10}
               </c:if> */
               
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'actual'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;',editor: dojox.grid.editors.Input, formatter: formatAmount, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'actual'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, formatter: formatAmount, width: 10}
               </c:if>
                <c:if test="${tableMetadata[2][fieldNameIteratorStatus.index] == 'html'}">
                           { name: '<fmt:message key="${fieldName}"/>'}
                </c:if> 
                <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'unitCost'}">
                { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;',  width: 10}
              </c:if>
                <c:if test="${empty ship}">
                <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'createdOn'}">
                { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editorClass: "dijit.form.NumberTextBox", width: 10}
              </c:if>
              </c:if>
              <c:if test="${empty ship}">
              <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'invoiceSeqNumber'}">
                  { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;',formatter: function(invoiceSeqNumber) {return invoiceSeqNumber+'<img id='+invoiceSeqNumber+' src="${pageContext.request.contextPath}/images/invoice.png" onclick="showReport(this);"/>';}, width: 8}
                </c:if>
                </c:if>
               <c:if test="${!fieldNameIteratorStatus.last}">,</c:if>
              
                
      </c:forEach>
     ]]
   }];
    
    function showReport(obj){ 
        var tmp=obj.id;       
        var tmp1=tmp.toString();         
        var val2 = "00000";
 		val2 = val2.substring(tmp1.length-1); 
 		tmp1=val2+tmp1; 		   
    	var url = 'viewReceipt.html?decorator=popup&popup=true&fileType=PDF&jasperName=CustomerSalePackRequisition.jrxml&reportParameter_Corporate ID=${sessionCorpID}&&jobNumber=&reportParameter_Receipt Number='+tmp1;
    	document.forms['gridForm'].action =url;
    	document.forms['gridForm'].submit();	
 				
    }
    function getEdit(tmp){
    	//alert("editttttttttttttttttttttt"+tmp);
    	//window.open('editEquipMaterialLimit.html?id='+tmp);
    	var id=123;
    	var eqmateid=123;
    	var category="";
    	var branch="";
    	var div="";
    	var act="";
    	var ret="";
    	var des="";
    	var des="";
    	var comm="";
    	var rid="";
    	var actReturn="";
    	<c:forEach var="list" items="${materialsList}">
    	<c:set var="ttid" value="${list.id}"></c:set>
    	ttttt='${ttid}';
    	if(tmp==ttttt){
    		//alert("items id true..."+tmp);
    		eqmateid='${list.equipMaterialsId}';
    		//act='${list.actualQty}';
    		ret='${list.soldQty}';
    		comm='${list.comments}';
    		actReturn='${list.returned}';
    		<c:forEach var="eqps" items="${equipMaterialsLimitsList}">
    		<c:set var="emid" value="${eqps.id}"></c:set>
        	var ttttt1='${emid}';
        	//alert(ttttt1);        	
        	if(ttttt1==eqmateid){
        		//alert("mat found..."+ttttt1);
        		 category='${eqps.category}';
            	 branch='${eqps.branch}';
            	 div='${eqps.division}';
            	 des='${eqps.resource}';
            	 rid='${emid}';
        	}
    		</c:forEach>
    	}
    	</c:forEach> 
    	document.forms['operationsResourceLimits'].elements['id'].value=tmp;
    	document.forms['operationsResourceLimits'].elements['category'].value=category;
    	document.forms['operationsResourceLimits'].elements['branch'].value=branch;
    	document.forms['operationsResourceLimits'].elements['division'].value=div;    	
    	document.forms['operationsResourceLimits'].elements['actQty'].value=ret;    	
    	document.forms['operationsResourceLimits'].elements['resource'].value=des;
    	document.forms['operationsResourceLimits'].elements['resourceId'].value=rid;
    	document.forms['operationsResourceLimits'].elements['retQty'].value=actReturn;
    	document.forms['operationsResourceLimits'].elements['masterRetQty'].value=actReturn;
    	document.forms['operationsResourceLimits'].elements['resetTemp'].value=category+"~"+branch+"~"+div+"~"+ret+"~"+des+"~"+actReturn;
    	
    	//alert(document.forms['gridForm'].elements['id'].value);
    	document.getElementById('saveBox').style.display="block";
      	document.getElementById('searchBox111').style.display="none";
      	document.forms['operationsResourceLimits'].elements['cngres'].value='false';
    	document.forms['operationsResourceLimits'].elements['cngwhouse'].value='false'; 
      	
    }
   
  
    // ==========================================================================
    // UI Action
    // ==========================================================================
    addRow = function(){
    	grid2.addRow([
           0, "", 0,"", ""
         ]); 
        
    	 document.forms['gridForm'].action="multipleCustomerSalesAssignment.html"
       }
    addRow1 = function(){
    	grid2.addRow([
    	              0, "", 0,"", ""
    	            ]); 
    	grid2.addRow([
    	              0, "", 0,"", ""
    	            ]); 
    	grid2.addRow([
    	              0, "", 0,"", ""
    	            ]); 
    	grid2.addRow([
    	              0, "", 0,"", ""
    	            ]); 
    	grid2.addRow([
    	              0, "", 0,"", ""
    	            ]);
    	 
    	 document.forms['gridForm'].action="multipleCustomerSalesAssignment.html"
         
       }
    /* saveRows = function(){      	
      	 
      	 document.forms['gridForm'].action="multipleAssignment.html"
           
         } */
 
         function val(){	
        		grid2.edit.apply();
        		var fields = "";
        		var fieldTypes = "";
        		var editability = "";
        		<c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
        			if (fields != "") fields += ',';
        			fields += '<c:out value="${fieldName}"/>';
        			if (editability != "") editability += ',';
        			editability += '<c:out value="${tableMetadata[3][fieldNameIteratorStatus.index]}"/>';
        			if (fieldTypes != "") fieldTypes += ',';
        			fieldTypes += '<c:out value="${tableMetadata[2][fieldNameIteratorStatus.index]}"/>';		
        		</c:forEach> 
        	  document.forms['gridForm'].elements['listData'].value = dojo.toJson(grid2.model.data);
        	  document.forms['gridForm'].elements['listFieldNames'].value = fields;
        	  document.forms['gridForm'].elements['listFieldTypes'].value = fieldTypes;      
        	  document.forms['gridForm'].elements['listFieldEditability'].value = editability;
        	  //
        	  var pp=document.forms['gridForm'].elements['chngMap'];
        	 // alert(pp.value);
        	   var url="chkForSalesItemsListAjax.html?ajax=1&decorator=simple&popup=true&listData="+dojo.toJson(grid2.model.data)+"&listFieldNames="+fields+"&listFieldTypes="+fieldTypes+"&listFieldEditability="+editability+"&listIdField=id&chngMap="+pp.value;
        		http34.open("GET", url, true);
        		http34.onreadystatechange = handleHttpResponse34;
        		http34.send(null);
        	}

         function handleHttpResponse34(){
         	if (http34.readyState == 4){
        	 var results = http34.responseText
            results = results.trim();
          //alert(results);
          if(results==''){
         	 saveListData();
          }else{
         	 alert('Quantity Assigned for following line numbers (' +results+') exceeds/decreases set Maximum Levels/Minimum Levels, please input valid amount.');
          }
        /**	var res = results.split(",");      
        		 for(i=0; i<res.length; i++){
        	     		if(res[i] == ''){   	   	     		
        	     	saveListData();
        	     		}else{
        	     		alert('Quantity Assigned for following rows exceeds/decreases set Maximum Levels/Minimum Levels, please input valid amount : '+results);
        	     	
        		         }
            }*/
         	}
        }
         var http34=getHTTPObject77();
         function getHTTPObject77() {
             var xmlhttp;
             if(window.XMLHttpRequest)  {
                 xmlhttp = new XMLHttpRequest();
             }
             else if (window.ActiveXObject)  {
                 xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                 if (!xmlhttp)  {
                     xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
                 } }
             return xmlhttp;
         }
    saveListData = function(){
        	 var wh=document.getElementById('whouse').value;
        	 var payMt=document.getElementById('payMt').value;
        	 var cName=document.getElementById('cName').value;
        	 if(cName==null || cName==''){
     	    	alert("Customer Name is Required");
     	    	 return false;
     	    }
        	 if(payMt==null || payMt==''){
     	    	alert("Payment Method is Required");
     	    	 return false;
     	    }
        	    if(wh==null || wh==''){
            	    alert("Warehose is Required");
            	    return false;
        	    }
        	   
        	    
    	grid2.edit.apply();
    	var fields = "";
    	var fieldTypes = "";
    	var editability = "";
		<c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
			if (fields != "") fields += ',';
			fields += '<c:out value="${fieldName}"/>';
			if (editability != "") editability += ',';
			editability += '<c:out value="${tableMetadata[3][fieldNameIteratorStatus.index]}"/>';
			if (fieldTypes != "") fieldTypes += ',';
			fieldTypes += '<c:out value="${tableMetadata[2][fieldNameIteratorStatus.index]}"/>';
			
		</c:forEach> 
		 document.forms['gridForm'].action="multipleAssignmentCsOs.html"
      document.forms['gridForm'].elements['listData'].value = dojo.toJson(grid2.model.data);
      document.forms['gridForm'].elements['listFieldNames'].value = fields;
      document.forms['gridForm'].elements['listFieldTypes'].value = fieldTypes;      
      document.forms['gridForm'].elements['listFieldEditability'].value = editability;
      document.forms['gridForm'].submit();
    } 
         updateListData = function(){
        	 grid2.edit.apply();
        	 var pp=document.forms['gridForm'].elements['lineFail'].value; 
        	 var pp1=document.forms['gridForm'].elements['negValMsg'].value;   	 	
        	    
			   if(pp!=null && pp.length>0){
				   alert("Return Quantity should not exceed quantity sold for the following line number "+pp);
				   document.forms['gridForm'].elements['lineFail'].value="";
			   }else if(pp1=='1'){
				  // document.forms['gridForm'].elements['negValMsg'].value="0";
				   alert("Decimal return values are not allowed");
			   }
				   else{
         	
         	var fields = "";
         	var fieldTypes = "";
         	var editability = "";
     		<c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
     			if (fields != "") fields += ',';
     			fields += '<c:out value="${fieldName}"/>';
     			if (editability != "") editability += ',';
     			editability += '<c:out value="${tableMetadata[3][fieldNameIteratorStatus.index]}"/>';
     			if (fieldTypes != "") fieldTypes += ',';
     			fieldTypes += '<c:out value="${tableMetadata[2][fieldNameIteratorStatus.index]}"/>';
     			
     		</c:forEach> 
     		 document.forms['gridForm'].action="updateMultipleAssignmentCsOs.html"
           document.forms['gridForm'].elements['listData'].value = dojo.toJson(grid2.model.data);
           document.forms['gridForm'].elements['listFieldNames'].value = fields;
           document.forms['gridForm'].elements['listFieldTypes'].value = fieldTypes;      
           document.forms['gridForm'].elements['listFieldEditability'].value = editability;
           document.forms['gridForm'].submit();
			   }
         } 
</script>

  
	<div id="newmnav">
         <ul>
          <c:choose>
          <c:when test="${not empty ship && ship=='CS' }">
           <li id="newmnav1">
          <a class="current"><span>Material And Equipment Customer Sales</span></a></li>
          <%-- <a href="itemsJbkEquipCustomerSales.html?ship=CS&itemType=${itemType}"><span>Material And Equipment Customer Sales</span></a> --%>
         <li>
       <a href="itemsJbkEquipCustomerSales.html?ship=OS&itemType=${itemType}"><span>Material And Equipment OO Sales</span></a> </li> 
          
          <li>
       <a href="overallItemsJbkEquipCustomerSales.html?ship=&itemType=${itemType}"><span>Overall Customer And OO Sales</span></a> </li> 
        </c:when>
         <c:when test="${not empty ship && ship=='OS' }">
           <li>
       <a href="itemsJbkEquipCustomerSales.html?ship=CS&itemType=${itemType}"><span>Material And Equipment Customer Sales</span></a> </li> 
          
         
          <li id="newmnav1">
          <a class="current"><span>Material And Equipment OO Sales</span></a></li>
           <li>
       <a href="overallItemsJbkEquipCustomerSales.html?ship=&itemType=${itemType}"><span>Overall Customer And OO Sales</span></a> </li> 
       <%-- <a href="itemsJbkEquipCustomerSales.html?ship=OS&itemType=${itemType}"><span>Material And Equipment OO Sales</span></a> --%>
      </c:when>
         
          <c:otherwise>
           <li>
       <a href="itemsJbkEquipCustomerSales.html?ship=CS&itemType=${itemType}"><span>Material And Equipment Customer Sales</span></a> </li>       
         
          
         
            <li>
          <a href="itemsJbkEquipCustomerSales.html?ship=OS&itemType=${itemType}"><span>Material And Equipment OO Sales</span></a></li>
          <li id="newmnav1">
       <a class="current"><span>Overall Customer And OO Sales</span></a> </li> 
          </c:otherwise>
          </c:choose>
         
          <%-- <c:if test="${itemType=='E'}"> <li><a class="current"><span>Equipment List</span></a></li></c:if> --%><!--
       
        <c:choose>
          <c:when test="${ship=='OS' }">
          <li id="newmnav1">
          <a class="current"><span>Material And Equipment OO Sales</span></a></li>
       <%-- <a href="itemsJbkEquipCustomerSales.html?ship=OS&itemType=${itemType}"><span>Material And Equipment OO Sales</span></a> --%>
      </c:when>
      <c:otherwise>
      <li>
       <a href="itemsJbkEquipCustomerSales.html?ship=OS&itemType=${itemType}"><span>Material And Equipment OO Sales</span></a> </li> 
      </c:otherwise>
      </c:choose>
      
         --></ul>
       </div>
 
       <div class="spnblk" style="margin-top:12px;"></div>
       <c:if test="${not empty ship }">  
</br>
<div style="clear:both;">
        <table class="table" style="margin-bottom:5px;">	
        	<thead>	
			<tr>
				
				<th>Customer Name<font color="red" size="2">*</font></th>
				<th>Payment Method<font color="red" size="2">*</font></th>	
				<th>Warehouse<font color="red" size="2">*</font></th>				
				 
			</tr>	
			</thead>		
				<tr>
				<td><s:textfield  id="cName" cssClass="input-text" name="customerName" size="39" cssStyle="width:150px" tabindex="15" onchange="setcust(this);"/></td></td>
				<td><s:select id="payMt" cssClass="list-menu" name="payMethod" list="%{paytype}" cssStyle="width:180px" headerKey="" headerValue=" " onchange="changePay(this);" tabindex="30" /></td>
				<td><s:select id="whouse" cssClass="list-menu" name="whouse" list="%{house}"  cssStyle="width:150px" headerKey="" headerValue="" onchange="search();"/></td>
				</tr>
					
       
       </table>
       </div>

</br>
 </c:if>   
 <c:choose>
  <c:when test='${itemType == "M"|| itemType=="E"}'>
 <table style="width:100%; border:1px solid #c1c1c1; border-top:none;padding-top:0px;" cellpading="0" cellspacing="0"">
 <tr>
<td>	      
<div id="grid2" jsId="grid2" dojoType="dojox.Grid" model="model" structure="gridLayout"
        style="width:100%;height:350px;border-width:1px 1px 1px 1px; ">
 </div>
 </td>
 </tr>
 </table></c:when>
 <c:otherwise>
 <table style="width:100%; border:1px solid #c1c1c1; border-top:none;padding-top:0px;" cellpading="0" cellspacing="0"">
 <tr>
<td>	      
<div id="grid2" jsId="grid2" dojoType="dojox.Grid" model="model" structure="gridLayout"
        style="width:100%;height:250px;border-width:1px 1px 1px 1px;">
 </div>
 </td>
 </tr>
 </table>
    </c:otherwise></c:choose>

<div id="rowCount" style="font-size:12px;font-family:arial;font-weight: bold;background-color: #d3d2d2;padding:5px;margin-bottom:4px; "></div>
<table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;width:100%;">
<%-- <tr>
<td align="left" width="649px"  style=" font-size:12px;font-family:arial;font-weight: bold;background-color: #d3d2d2;padding:5px;  " class="">Total</td>
<td align="right" width="241px" class="" style="font-size:12px;font-family:arial;font-weight: bold;background-color: #d3d2d2;padding:5px; "><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${actualcost}" /></div></td>

<td align="left" width=""  style=" font-size:12px;font-family:arial;font-weight: bold;background-color: #d3d2d2;padding:5px;  " class=""></td>
</tr> --%>
</table>
<c:if test="${not empty ship}">
<div id="controls">
<table cellpadding="4" cellspacing="2" style="margin:0px;padding:0px;"><tr><td>
 <%-- <s:submit cssClass="cssbutton1" key="button.save" method="delete" onclick="saveListData()" /></td> --%>
 <td>
 <div id="adddiv">
 <s:submit cssClass="cssbutton1" value="Single Item Sales" method="delete" onclick="addRow()" cssStyle="width:130px"/>
 </div></td>
 <td>
 <s:submit cssClass="cssbutton1" value="Multiple Item Sales" method="delete" onclick="addRow1()" cssStyle="width:130px"/>
 </td>
  <td>
 <%-- <s:submit cssClass="cssbutton1" key="button.reset" method="delete" onclick="grid2.refresh()" /> --%>
 <s:submit cssClass="cssbutton1" value="FINISH" method="delete" onclick="return val()" cssStyle="width:70px"/></td>
 </td>
  <td>
 <s:submit cssClass="cssbutton1" value="CANCEL" method="" onclick="window.location.reload();" cssStyle="width:70px"/>
 </td>
 </tr></table>
</div>
</c:if>
<c:if test="${ empty ship}">
<div id="controls">
<table cellpadding="4" cellspacing="2" style="margin:0px;padding:0px;"><tr><td>
 <%-- <s:submit cssClass="cssbutton1" key="button.save" method="delete" onclick="saveListData()" /></td> --%><!--
 <td>
 <div id="adddiv">
 <s:submit cssClass="cssbutton1" value="Single Item Sales" method="delete" onclick="addRow()" cssStyle="width:150px"/>
 </div></td>
 <td>
 <s:submit cssClass="cssbutton1" value="Multiple Item Sales" method="delete" onclick="addRow1()" cssStyle="width:150px"/>
 </td>
  --><td>
 <%-- <s:submit cssClass="cssbutton1" key="button.reset" method="delete" onclick="grid2.refresh()" /> --%>
 <s:submit cssClass="cssbutton1" value="SAVE" method="delete" onclick="updateListData()" cssStyle="width:70px"/></td>
 </td>
 </tr></table>
</div>
</c:if>


<br/>
<script type="text/javascript">
if(document.forms['gridForm'].elements['showAdd'].value=='Yes')
{
document.getElementById("adddiv").style.display="block";
}
 if(document.forms['gridForm'].elements['showAdd'].value=='No')
{
document.getElementById("adddiv").style.display="none";
}
 function setcust(cus){
	 document.forms['gridForm'].elements['customerName'].value=cus.value
 }
 function changePay(pay){
	 document.forms['gridForm'].elements['payMethod'].value=pay.value
 }
 try{
	 document.getElementById('whouse').value='${whouse}';
 }catch(e){
	// alert("error"+e);
 }
</script>



