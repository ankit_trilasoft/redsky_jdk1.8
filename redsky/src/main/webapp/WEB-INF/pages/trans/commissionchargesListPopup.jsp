<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Agent Directory</title>   
    <meta name="heading" content="Agent Directory"/>  
    <style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-left:347px;
margin-bottom:13px;
!margin-bottom:2px;
margin-top:-38px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:60%;
!width:98%;
}
</style>
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
     
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top" onclick="getCharges();" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set>
<s:form id="chargesListForm" method="post" >
<table class="table" style="width:100%"  >
<thead>
<tr>
<th><fmt:message key="charges.charge"/></th>
<th><fmt:message key="charges.description"/></th>
<th></th>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="chargeCde" size="25" cssClass="input-text" onfocus="onFormLoad();" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/></td>
			    <td><s:textfield name="description" size="25" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			    <s:hidden name="contract" value="${contract}"/>
			</td>
			<td width="130px" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
			</tr>
		</tbody>
	</table>

</div><div class="spn" style="width:100%">&nbsp;</div><br><br><br>
<s:set name="userContactList" value="userContactList" scope="request"/>  
<display:table name="chargeCdeList" class="table" requestURI="" id="chargeCdeList" pagesize="10" style="margin-top: -10px; margin-left: 20px; width: 96%;" >   
	    <display:column title="Charge Code" style="width:65px"> 
	    	<a href="javascript: void(0)" onclick= "getValue('${chargeCdeList.charge}');">
	    	 <c:out value="${chargeCdeList.charge}" /></a>
	    </display:column>
        <display:column property="description" title="Charge Description" style="width:65px"/>
</display:table> 
</div>
</s:form>

<script type="text/javascript">
		function getValue(chargeCode){
				window.opener.document.forms['saleCommissionStructurePage'].elements['commissionChargeCode'].value = chargeCode;
			window.close();
		}
	</script>
	<script language="javascript" type="text/javascript">
    function clear_fields(){
	document.forms['chargesListForm'].elements['chargeCde'].value = "";
	document.forms['chargesListForm'].elements['description'].value = "";
	}

	function getCharges(){
		url = "searchChargeForSaleCommision.html?decorator=popup&popup=true";
		document.forms['chargesListForm'].action = url;
		document.forms['chargesListForm'].submit();
	}
</script> 