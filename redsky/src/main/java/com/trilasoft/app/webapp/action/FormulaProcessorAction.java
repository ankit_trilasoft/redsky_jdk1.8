package com.trilasoft.app.webapp.action;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;

import org.appfuse.service.LookupManager;
import org.appfuse.webapp.action.BaseAction;
import org.codehaus.groovy.control.CompilationFailedException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.context.support.XmlWebApplicationContext;

import com.trilasoft.app.service.ExpressionManager;
import com.trilasoft.app.service.impl.ExpressionManagerImpl;

import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;
import groovy.lang.GroovyShell;

public class FormulaProcessorAction extends BaseAction {

	private static String[] testFormulae = {
			"(iif((dow(@jdayrev.dated@)=7 .or. DOW(@jdayrev.dated@)=1),.035,.0336)*@jbook.aweight@)+(.45*@jbook.revenue@)",
			"(iif(dow(@timeSheet.workDate@)=7 .or. DOW(@timeSheet.workDate@)=1,.06,.0545)*@workTicket.actualWeight@)+(.3*@workTicket.revenue@)",
			"iif(@imisc.actgross@<378,764.78,iif(@imisc.actgross@<500,500*578.21/@imisc.actgross@,iif(@imisc.actgross@<724,578.21,iif(@imisc.actgross@<750,750*558.49/@imisc.actgross@,iif(@imisc.actgross@<992,558.49,iif(@imisc.actgross@<1000,1000*554.35/@imisc.actgross@,iif(@imisc.actgross@<1410,554.35,iif(@imisc.actgross@<1500,1500*521.41/@imisc.actgross@,521.41))))))))",
			"iif(@IMISC.ACTNET@<4224,159.69,iif(@IMISC.ACTNET@<6000,6000*112.43/@imisc.actnet@,iif(@IMISC.ACTNET@<8074,112.43,iif(@IMISC.ACTNET@<9000,9000*100.88/@imisc.actnet@,iif(@IMISC.ACTNET@<10074,100.88,iif(@IMISC.ACTNET@<12000,12000*84.69/@imisc.actnet@,iif(@IMISC.ACTNET@<14999,84.69,iif(@IMISC.ACTNET@<15000,15000*87.25/@imisc.actnet@,87.25))))))))",
			"iif(FORM.QTY<343,450,iif(FORM.QTY<440,440*350/form.qty,iif(FORM.QTY<566,350,iif(FORM.QTY<660,660*300/form.qty,FORM.QTY*300/form.qty))))",
			"@Ibilling.insval_ac@*@ibilling.ins_rate@*@IBILLING.CYCLE@",
			"@Ibilling.insval_ac@*@ibilling.ins_rate@*@IBILLING.CYCLE@+\" text val\""};

	private ExpressionManager expressionManager;
	
	public void setExpressionManager(ExpressionManager expressionManager ){
		this.expressionManager = expressionManager;
	}
	
	public void executeExpression() {
		//ExpressionManager expressionManager = new ExpressionManagerImpl();
		Map values = new HashMap();
		values.put("timeSheet.workDate", new Date());
		values.put("workTicket.actualWeight", 1000);
		values.put("workTicket.revenue", 2000.00);
		values.put("imisc.actgross", 3000.00);
		values.put("IMISC.ACTNET", 3000.00);
		values.put("imisc.actnet", 3000.00);
		values.put("FORM.QTY", 3000);
		values.put("form.qty", 3000);
		values.put("Ibilling.insval_ac", 3000.00);
		values.put("ibilling.ins_rate", 3000.00);
		values.put("IBILLING.CYCLE", 3000.00);
		Object value = expressionManager.executeExpression(testFormulae[1], values);
		//System.out.println(value);
	}

	
}
