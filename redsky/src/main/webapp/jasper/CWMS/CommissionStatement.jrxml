<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="CommissionStatement"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="842"
		 pageHeight="595"
		 columnWidth="782"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Begin Date" isForPrompting="true" class="java.util.Date"/>
	<parameter name="End Date" isForPrompting="true" class="java.util.Date"/>
	<parameter name="Sales Person / Consultant" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[select
"CommissionStatement-CWMS.jrxml","Sales" AS TYPE,
accountline.id as iii,
accountline.`companyDivision` AS "Company Division",
right(serviceorder.`shipNumber`,9) AS "S/O Number",
if(serviceorder.`salesman` is null,"",serviceorder.`salesman`) AS "Sales Person",
if(serviceorder.`estimator` is null,"",serviceorder.`estimator`) AS "Consultant",
concat(app_userS.first_name," ",app_userS.last_name) AS NameUser,
if(app_userS.`parentAgent` is null,"",app_userS.`parentAgent`) AS "Sales Parent Agent",
if(serviceorder.`prefix`!='',concat(serviceorder.`prefix`," ",IF(serviceorder.`firstName`<>"",CONCAT(serviceorder.`firstName`," ",serviceorder.`lastName`),serviceorder.`lastName`)),IF(serviceorder.`firstName`<>"",CONCAT(serviceorder.`firstName`," ",serviceorder.`lastName`),serviceorder.`lastName`))AS serviceorder_first_lastName,
serviceorder.`sequenceNumber` AS "C/F Number",
trim(charges.commission) AS commission,
(trim(charges.commission)/100) AS commission1,
charges.commission AS Comm_Amount,
charges.`description` AS "Description",
charges.`charge` AS "Charge Code",
charges.`costElement` AS "Cost Element",
rategrid.`quantity1` AS Quantity,
(rategrid.`rate1`/100) AS Rate, 
rategrid.`rate1` AS Rate1,
accountline.`actualRevenue` AS "Actual Receivable",
accountline.`recInvoiceNumber` AS "Sales invoice number",
DATE_FORMAT(accountline.`receivedInvoiceDate`,'%d %b %y') AS "Sales invoice date",
accountline.`actualExpense` AS "Actual Payable",
if(accountline.`vendorCode`="","",concat("'",accountline.`vendorCode`)) AS "Bill to code Payable",
accountline.`vendorCode` AS Vendor,
billing.billtoname AS billing_billtoname,
accountline.`estimateVendorName` AS "Billing Party Payable",
accountline.`invoiceNumber` AS "Purchase invoice number",
DATE_FORMAT(accountline.`invoiceDate`,'%d %b %y') AS "Purchase invoice date",
(select count(*) from accountline a where a.`serviceOrderId`=accountline.`serviceOrderId` AND a.status=true and  (a.`chargecode`=concat('SACOM',serviceorder.job))) AS COUN
FROM
`serviceorder` serviceorder 
INNER JOIN `billing` billing ON serviceorder.`id` = billing.`id` AND billing.`corpID`=$P{Corporate ID}
INNER JOIN `trackingstatus` trackingstatus ON serviceorder.`id` = trackingstatus.`id` AND trackingstatus.`corpID`=$P{Corporate ID}
LEFT OUTER JOIN `app_user` app_userS ON app_userS.username=serviceorder.`salesman` AND app_userS.`corpID` = $P{Corporate ID}
INNER JOIN `accountline` accountline ON serviceorder.`id` = accountline.`serviceOrderId` and (if(trim(accountline.`chargecode`)=concat('SACOM',serviceorder.job),(app_userS.`parentAgent`=trim(accountline.`vendorCode`) AND accountline.`corpID`=$P{Corporate ID}),app_userS.`corpID`=$P{Corporate ID})) AND accountline.status=true
LEFT OUTER JOIN `charges` charges ON charges.`charge` = accountline.`chargecode` AND charges.`contract`=billing.`contract`  AND charges.`corpID`=$P{Corporate ID} AND charges.`commissionable` is true
LEFT OUTER JOIN `rategrid` rategrid ON rategrid.id=(SELECT min(r.id) FROM rategrid r where r.contractname=concat('Internal Costs ',serviceorder.companydivision)
and r.`charge` = (select c.id from charges c where c.contract=concat('Internal Costs ',serviceorder.companydivision) and c.charge=concat('SACOM',serviceorder.job) and c.`corpID`=$P{Corporate ID})
and r.quantity1>=(select (((sum(aa.`actualRevenue`)- sum(aa.`actualExpense`))/sum(aa.`actualRevenue`))*100) from accountline aa where aa.`chargecode`=charges.`charge` AND aa.status=true AND charges.`commissionable` is true AND (charges.`commission` ="" || charges.`commission` is null || charges.`commission` ='' || charges.`commission` =0) and  aa.corpid='CWMS' and aa.`serviceOrderId`=accountline.`serviceOrderId` group by r.charge))
WHERE serviceorder.`corpID`=$P{Corporate ID}
AND serviceorder.`salesman` like $P{Sales Person / Consultant}
AND DATE_FORMAT(accountline.`receivedInvoiceDate`,'%Y-%m-%d') between $P{Begin Date} AND $P{End Date}
AND accountline.status=true
AND charges.`commissionable` is true
AND trim(accountline.`chargecode`) not in ('SACOM2',concat('SACOM',serviceorder.job))
AND app_userS.`parentAgent` is not null
HAVING COUN>0

Union

select
"CommissionStatement-CWMS.jrxml","Consultant" AS TYPE,
accountline.id as iii,
accountline.`companyDivision` AS "Company Division",
right(serviceorder.`shipNumber`,9) AS "S/O Number",
if(serviceorder.`salesman` is null,"",serviceorder.`salesman`) AS "Sales Person",
if(serviceorder.`estimator` is null,"",serviceorder.`estimator`) AS "Consultant",
concat(app_userS.first_name," ",app_userS.last_name) AS NameUser,
if(app_userS.`parentAgent` is null,"",app_userS.`parentAgent`) AS "Sales Parent Agent",
if(serviceorder.`prefix`!='',concat(serviceorder.`prefix`," ",IF(serviceorder.`firstName`<>"",CONCAT(serviceorder.`firstName`," ",serviceorder.`lastName`),serviceorder.`lastName`)),IF(serviceorder.`firstName`<>"",CONCAT(serviceorder.`firstName`," ",serviceorder.`lastName`),serviceorder.`lastName`))AS serviceorder_first_lastName,
serviceorder.`sequenceNumber` AS "C/F Number",
trim(partner.vendorCommission) AS commission,
(trim(partner.vendorCommission)/100) AS commission1,
partner.vendorCommission AS Comm_Amount,
accountline.`description` AS "Description",
charges.`charge` AS "Charge Code",
charges.`costElement` AS "Cost Element",
rategrid.`quantity1` AS Quantity,
(rategrid.`rate1`/100) AS Rate, 
rategrid.`rate1` AS Rate1,
sum(accountline.`actualRevenue`) AS "Actual Receivable",
accountline.`recInvoiceNumber` AS "Sales invoice number",
DATE_FORMAT(accountline.`receivedInvoiceDate`,'%d %b %y') AS "Sales invoice date",
sum(accountline.`actualExpense`) AS "Actual Payable",
if(accountline.`vendorCode`="","",concat("'",accountline.`vendorCode`)) AS "Bill to code Payable",
accountline.`vendorCode` AS Vendor,
billing.billtoname AS billing_billtoname,
accountline.`estimateVendorName` AS "Billing Party Payable",
accountline.`invoiceNumber` AS "Purchase invoice number",
DATE_FORMAT(accountline.`invoiceDate`,'%d %b %y') AS "Purchase invoice date",
(select count(*) from accountline a where a.`serviceOrderId`=accountline.`serviceOrderId` AND a.status=true and  (a.`chargecode` = 'SACOM2')) AS COUN
FROM
`serviceorder` serviceorder 
INNER JOIN `billing` billing ON serviceorder.`id` = billing.`id` AND billing.`corpID`=$P{Corporate ID}
INNER JOIN `trackingstatus` trackingstatus ON serviceorder.`id` = trackingstatus.`id` AND trackingstatus.`corpID`=$P{Corporate ID}
INNER JOIN `accountline` accountline ON serviceorder.`id` = accountline.`serviceOrderId` AND accountline.`corpID`=$P{Corporate ID} AND accountline.status=true
LEFT OUTER JOIN `app_user` app_userS ON app_userS.`corpID` = $P{Corporate ID} and (app_userS.username=serviceorder.`estimator` and (if(trim(accountline.`chargecode`) ='SACOM2',app_userS.`parentAgent`=trim(accountline.`vendorCode`),app_userS.`corpID`=$P{Corporate ID})))
LEFT OUTER JOIN `partnerpublic` partner ON partner.`partnercode` = app_userS.`parentAgent` AND partner.`corpID` in ('TSFT',$P{Corporate ID})
LEFT OUTER JOIN `charges` charges ON charges.`charge` = accountline.`chargecode` AND charges.`contract`=billing.`contract`  AND charges.`corpID`=$P{Corporate ID} AND charges.`commissionable` is true AND charges.`commission` =''
LEFT OUTER JOIN `rategrid` rategrid ON rategrid.`contractname`=concat('Internal Costs ',serviceorder.companydivision) AND rategrid.`charge` = (select c.id from charges c where c.contract=concat('Internal Costs ',serviceorder.companydivision) and c.`corpID`=$P{Corporate ID} and c.charge=concat('SACOM',serviceorder.job) AND rategrid.id=(SELECT min(r.id) FROM rategrid r where r.contractname=concat('Internal Costs ',serviceorder.companydivision) and r.`charge` = (select c.id from charges c where c.contract=concat('Internal Costs ',serviceorder.companydivision) and c.charge=concat('SACOM',serviceorder.job) and c.`corpID`=$P{Corporate ID}) 
and r.quantity1>=((select (((sum(aa.`actualRevenue`)- sum(aa.`actualExpense`))/sum(aa.`actualRevenue`))*100) from accountline aa, charges cc where cc.`charge` = aa.`chargecode` AND aa.status=true AND cc.`contract`=aa.`contract`  AND cc.`corpID`=$P{Corporate ID} AND cc.`commissionable` is true AND (cc.`commission` ='' || cc.`commission` is null) and aa.corpid=$P{Corporate ID} and aa.`serviceOrderId`=accountline.`serviceOrderId`)))) AND rategrid.`corpID`=$P{Corporate ID} 
WHERE serviceorder.`corpID`=$P{Corporate ID}
AND serviceorder.`estimator` like $P{Sales Person / Consultant}
AND DATE_FORMAT(accountline.`receivedInvoiceDate`,'%Y-%m-%d') between $P{Begin Date} AND $P{End Date}
AND accountline.status=true
AND charges.`commissionable` is true
AND charges.`commission` =''
AND trim(accountline.`chargecode`) <>'SACOM2'
AND app_userS.`parentAgent` is not null
GROUP BY accountline.`shipNumber`
HAVING COUN>0]]></queryString>

	<field name="CommissionStatement-CWMS.jrxml" class="java.lang.String"/>
	<field name="TYPE" class="java.lang.String"/>
	<field name="iii" class="java.lang.Long"/>
	<field name="Company Division" class="java.lang.String"/>
	<field name="S/O Number" class="java.lang.String"/>
	<field name="Sales Person" class="java.lang.String"/>
	<field name="Consultant" class="java.lang.String"/>
	<field name="NameUser" class="java.lang.String"/>
	<field name="Sales Parent Agent" class="java.lang.String"/>
	<field name="serviceorder_first_lastName" class="java.lang.String"/>
	<field name="C/F Number" class="java.lang.String"/>
	<field name="commission" class="java.math.BigDecimal"/>
	<field name="commission1" class="java.math.BigDecimal"/>
	<field name="Comm_Amount" class="java.lang.String"/>
	<field name="Description" class="java.lang.String"/>
	<field name="Charge Code" class="java.lang.String"/>
	<field name="Cost Element" class="java.lang.String"/>
	<field name="Quantity" class="java.math.BigDecimal"/>
	<field name="Rate" class="java.math.BigDecimal"/>
	<field name="Rate1" class="java.math.BigDecimal"/>
	<field name="Actual Receivable" class="java.math.BigDecimal"/>
	<field name="Sales invoice number" class="java.lang.String"/>
	<field name="Sales invoice date" class="java.lang.String"/>
	<field name="Actual Payable" class="java.math.BigDecimal"/>
	<field name="Bill to code Payable" class="java.lang.String"/>
	<field name="Vendor" class="java.lang.String"/>
	<field name="Billing Party Payable" class="java.lang.String"/>
	<field name="Purchase invoice number" class="java.lang.String"/>
	<field name="Purchase invoice date" class="java.lang.String"/>
	<field name="COUN" class="java.lang.Long"/>
	<field name="billing_billtoname" class="java.lang.String"/>

	<sortField name="TYPE" order="Descending" />
	<sortField name="NameUser" />
	<sortField name="S/O Number" />
	<sortField name="commission" />

	<variable name="Rev" class="java.math.BigDecimal" resetType="Group" resetGroup="Commission" calculation="Sum">
		<variableExpression><![CDATA[$F{Actual Receivable}]]></variableExpression>
	</variable>
	<variable name="Exp" class="java.math.BigDecimal" resetType="Group" resetGroup="Commission" calculation="Sum">
		<variableExpression><![CDATA[$F{Actual Payable}]]></variableExpression>
	</variable>
	<variable name="RevSP" class="java.math.BigDecimal" resetType="Group" resetGroup="Vendor" calculation="Sum">
		<variableExpression><![CDATA[$F{Actual Receivable}]]></variableExpression>
	</variable>
	<variable name="ExpSP" class="java.math.BigDecimal" resetType="Group" resetGroup="Vendor" calculation="Sum">
		<variableExpression><![CDATA[$F{Actual Payable}]]></variableExpression>
	</variable>
	<variable name="CountVen" class="java.lang.Integer" resetType="Group" incrementType="Group" incrementGroup="Ship" resetGroup="Vendor" calculation="Count">
		<variableExpression><![CDATA[$V{Ship_COUNT}]]></variableExpression>
	</variable>
	<variable name="Commi" class="java.math.BigDecimal" resetType="Group" resetGroup="Vendor" calculation="Sum">
		<variableExpression><![CDATA[$F{TYPE}.equals("Sales") ? (($F{commission}.equals(new BigDecimal(0.00)) || $F{commission}.equals("") || $F{commission}.equals(new BigDecimal(0)) || $F{commission}.equals(null)) ? ($F{Rate}.multiply($F{Actual Receivable})) : ($F{commission1}.multiply(($F{Actual Receivable}.subtract($F{Actual Payable}))))) : (($F{commission}.equals(new BigDecimal(0.00)) || $F{commission}.equals("") || $F{commission}.equals(new BigDecimal(0)) || $F{commission}.equals(null)) ? new BigDecimal(0.00) : ($F{commission1}.multiply(($F{Actual Receivable}))))]]></variableExpression>
	</variable>
	<variable name="Com" class="java.math.BigDecimal" resetType="Group" resetGroup="Vendor" calculation="Sum">
		<variableExpression><![CDATA[$F{Rate}.multiply($F{Actual Receivable})]]></variableExpression>
	</variable>

		<group  name="Vendor" >
			<groupExpression><![CDATA[$F{NameUser}]]></groupExpression>
			<groupHeader>
			<band height="36"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="19"
						width="782"
						height="17"
						forecolor="#000000"
						backcolor="#CCCCCC"
						key="staticText-1"
						printWhenGroupChanges="Vendor"
						isPrintWhenDetailOverflows="true"/>
					<box>					<pen lineWidth="0.75"/>
					<topPen lineWidth="0.75"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[Vendor Summary]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="1"
						width="484"
						height="18"
						key="textField"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{NameUser}+" - "+$F{TYPE}+" - ("+$F{Sales Parent Agent}+")"]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="21"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="446"
						y="3"
						width="100"
						height="18"
						forecolor="#3300CC"
						key="textField-12"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{RevSP}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="3"
						width="222"
						height="18"
						key="staticText-16"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Totals:          Orders]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Group" evaluationGroup="Ship"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="40"
						y="3"
						width="32"
						height="18"
						key="textField-14"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$V{CountVen}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="782"
						height="0"
						forecolor="#3300CC"
						key="line-3"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="3"
						width="90"
						height="18"
						forecolor="#330099"
						key="textField-18"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{Commi}]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<group  name="Ship" >
			<groupExpression><![CDATA[$F{S/O Number}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="Commission" >
			<groupExpression><![CDATA[$F{commission}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="30"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="545"
						y="0"
						width="33"
						height="30"
						forecolor="#660066"
						key="textField-3"
						stretchType="RelativeToTallestObject"
						printWhenGroupChanges="Commission"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{TYPE}.equals("Sales") ? (($F{commission}.equals(new BigDecimal(0.00)) || $F{commission}.equals("") ||  $F{commission}.equals(new BigDecimal(0)) || $F{commission}.equals(null)) ? $F{Rate1} : $F{commission}) : (($F{commission}.equals(new BigDecimal(0.00)) || $F{commission}.equals("") || $F{commission}.equals(new BigDecimal(0)) || $F{commission}.equals(null)) ? new BigDecimal(0.00) : $F{commission})]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="471"
						y="0"
						width="74"
						height="30"
						forecolor="#660066"
						key="textField-15"
						stretchType="RelativeToTallestObject"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="Commission"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{Rev}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="0"
						width="90"
						height="30"
						forecolor="#660066"
						key="textField-17"
						stretchType="RelativeToTallestObject"
						printWhenGroupChanges="Commission"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{TYPE}.equals("Sales") ? (($F{commission}.equals(new BigDecimal(0.00)) || $F{commission}.equals("") || $F{commission}.equals(new BigDecimal(0)) || $F{commission}.equals(null)) ? ($F{Rate}.multiply($V{Rev})) : ($F{commission1}.multiply(($V{Rev}.subtract($V{Exp}))))) : (($F{commission}.equals(new BigDecimal(0.00)) || $F{commission}.equals("") || $F{commission}.equals(new BigDecimal(0)) || $F{commission}.equals(null)) ? new BigDecimal(0.00) : ($F{commission1}.multiply(($V{Rev}))))]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<group  name="Invoice" >
			<groupExpression><![CDATA[$F{Sales invoice number}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="Charge" >
			<groupExpression><![CDATA[$F{Charge Code}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="46"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="502"
						y="12"
						width="280"
						height="18"
						key="textField"/>
					<box rightPadding="10"></box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["From   "+(new java.text.SimpleDateFormat("MM/dd/yy")).format($P{Begin Date})+ "  To   "+(new java.text.SimpleDateFormat("MM/dd/yy")).format($P{End Date})]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="1"
						y="1"
						width="349"
						height="45"
						key="staticText-10"/>
					<box leftPadding="3"></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="18" isBold="true"/>
					</textElement>
				<text><![CDATA[Commission Statement]]></text>
				</staticText>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="18"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Opaque"
						x="93"
						y="0"
						width="105"
						height="18"
						forecolor="#000000"
						backcolor="#CCCCCC"
						key="staticText-2"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="2"></box>
					<textElement verticalAlignment="Middle" isStyledText="true">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Shipper Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="471"
						y="0"
						width="72"
						height="18"
						forecolor="#000000"
						backcolor="#CCCCCC"
						key="staticText-3"
						isPrintWhenDetailOverflows="true"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right" verticalAlignment="Middle" isStyledText="true">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Revenue Amt]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="543"
						y="0"
						width="35"
						height="18"
						forecolor="#000000"
						backcolor="#CCCCCC"
						key="staticText-4"
						isPrintWhenDetailOverflows="true"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Middle" isStyledText="true">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Com%]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="0"
						width="93"
						height="18"
						forecolor="#000000"
						backcolor="#CCCCCC"
						key="staticText-6"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="2"></box>
					<textElement verticalAlignment="Middle" isStyledText="true">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Order Number]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="578"
						y="0"
						width="90"
						height="18"
						forecolor="#000000"
						backcolor="#CCCCCC"
						key="staticText-8"
						isPrintWhenDetailOverflows="true"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right" verticalAlignment="Middle" isStyledText="true">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Comm Amt]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="668"
						y="0"
						width="114"
						height="18"
						forecolor="#000000"
						backcolor="#CCCCCC"
						key="staticText-9"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left" verticalAlignment="Middle" isStyledText="true">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Item Description ]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="417"
						y="0"
						width="54"
						height="18"
						forecolor="#000000"
						backcolor="#CCCCCC"
						key="staticText-11"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left" verticalAlignment="Middle" isStyledText="true">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Doc Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="326"
						y="0"
						width="91"
						height="18"
						forecolor="#000000"
						backcolor="#CCCCCC"
						key="staticText-12"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left" verticalAlignment="Middle" isStyledText="true">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Document Number]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="198"
						y="0"
						width="128"
						height="18"
						forecolor="#000000"
						backcolor="#CCCCCC"
						key="staticText-17"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="2"></box>
					<textElement verticalAlignment="Middle" isStyledText="true">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Account Name]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="30"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="93"
						height="30"
						key="textField-20"
						stretchType="RelativeToTallestObject"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="Ship"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="2"></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{S/O Number}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="93"
						y="0"
						width="105"
						height="30"
						key="textField-21"
						stretchType="RelativeToTallestObject"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="Ship"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="2"></box>
					<textElement verticalAlignment="Top" isStyledText="true">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_first_lastName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="326"
						y="0"
						width="91"
						height="30"
						key="textField-22"
						stretchType="RelativeToTallestObject"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="Invoice"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Sales invoice number}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="417"
						y="0"
						width="54"
						height="30"
						key="textField-23"
						stretchType="RelativeToTallestObject"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="Invoice"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Sales invoice date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="198"
						y="0"
						width="128"
						height="30"
						key="textField-24"
						stretchType="RelativeToTallestObject"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="Ship"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="2"></box>
					<textElement verticalAlignment="Top" isStyledText="true">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_billtoname}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="668"
						y="0"
						width="114"
						height="30"
						key="textField-25"
						stretchType="RelativeToTallestObject"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="2"></box>
					<textElement verticalAlignment="Top" isStyledText="true">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Description}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="545"
						y="0"
						width="33"
						height="30"
						key="textField-26"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{TYPE}.equals("Sales") ? (($F{commission}.equals(new BigDecimal(0.00)) || $F{commission}.equals("") ||  $F{commission}.equals(new BigDecimal(0)) || $F{commission}.equals(null)) ? $F{Rate1} : $F{commission}) : (($F{commission}.equals(new BigDecimal(0.00)) || $F{commission}.equals("") || $F{commission}.equals(new BigDecimal(0)) || $F{commission}.equals(null)) ? new BigDecimal(0.00) : $F{commission})]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="0"
						width="90"
						height="30"
						key="textField-27"
						stretchType="RelativeToTallestObject"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{TYPE}.equals("Sales") ? (($F{commission}.equals(new BigDecimal(0.00)) || $F{commission}.equals("") || $F{commission}.equals(new BigDecimal(0)) || $F{commission}.equals(null)) ? ($F{Rate}.multiply($F{Actual Receivable})) : ($F{commission1}.multiply(($F{Actual Receivable}.subtract($F{Actual Payable}))))) : (($F{commission}.equals(new BigDecimal(0.00)) || $F{commission}.equals("") || $F{commission}.equals(new BigDecimal(0)) || $F{commission}.equals(null)) ? new BigDecimal(0.00) : ($F{commission1}.multiply(($F{Actual Receivable}))))]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="471"
						y="0"
						width="74"
						height="30"
						forecolor="#000000"
						key="textField-28"
						stretchType="RelativeToTallestObject"
						isPrintRepeatedValues="false"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{Actual Receivable}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="22"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="573"
						y="3"
						width="170"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-8"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="745"
						y="3"
						width="36"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-9"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="495"
						y="3"
						width="173"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-10"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="10"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="3"
						width="222"
						height="19"
						key="staticText-14"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial"/>
					</textElement>
				<text><![CDATA[Commission Statement]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="782"
						height="0"
						forecolor="#000000"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
