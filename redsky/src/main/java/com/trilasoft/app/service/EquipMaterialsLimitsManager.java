package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.EquipMaterialsLimits;


public interface EquipMaterialsLimitsManager extends GenericManager<EquipMaterialsLimits, Long>{
	public List getEquipMaterialLimitList(String branch,String div,String cat);
	public EquipMaterialsLimits getByDesc(String cat,String corpid);
	public void saveEquipMaterialObject(EquipMaterialsLimits obj);
	public void updateEquipMaterialObject(EquipMaterialsLimits equipMaterialsLimits,String cngres,String cngwhouse);
	public Map<String,String> getCompanyDivisionMap(String corpid);

}
