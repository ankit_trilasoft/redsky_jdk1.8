package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.DsHomeRental;

public interface DsHomeRentalManager extends GenericManager<DsHomeRental, Long> {
	List getListById(Long id);
}
