/*

Created date    -06-april-2016
Createdby       -shivom agarwal
Used database   -redsky
Used tables     -storage ,location
Summary        -this trigger is used  to  create book storage and release in particular location.


*/
delimiter $$

DROP trigger IF EXISTS `redsky`.`Trigger_after_update_Storage` $$

CREATE  TRIGGER redsky.Trigger_after_update_Storage after update ON redsky.storage
FOR EACH ROW
BEGIN
declare Fvolume decimal(19,2);
set Fvolume = case when (NEW.volume>OLD.volume) then NEW.volume-OLD.volume else case when (NEW.volume=OLD.volume) then 0 end end ;

call storageBookAndRelease(NEW.locationid,'L',NEW.releaseDate,NEW.updatedOn,NEW.updatedBy,Fvolume,OLD.corpid,NEW.volUnit,NEW.storageid,'abc',NEW.idNum);
call ServiceOrder_UpdateStatus(OLD.shipnumber,NEW.updatedBy);

end;

$$


delimiter;