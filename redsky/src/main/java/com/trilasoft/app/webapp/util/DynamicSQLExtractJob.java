/**
 * Implementation of <strong>ActionSupport</strong> that contains convenience methods.
 * This class represents the basic actions on "DataExtraction"  in Redsky its use "QUARTZ".
 * @Class Name	DataExtractJob
 * @Author      Ravi Mishra
 * @Version     V01.0
 * @Since       1.0
 * @Date        29-Dec-2008
 */
package com.trilasoft.app.webapp.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.trilasoft.app.dao.CompanyDao;
import com.trilasoft.app.dao.ImfEntitlementsDao;
import com.trilasoft.app.dao.SystemDefaultDao;
import com.trilasoft.app.dao.ToDoRuleDao;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.EmailSetup;
import com.trilasoft.app.model.ExtractQueryFile;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.SQLExtract;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ExtractQueryFileManager;
import com.trilasoft.app.service.SQLExtractManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ToDoRuleManager;

import java.util.Calendar;

public class DynamicSQLExtractJob extends QuartzJobBean {
	
	private static final String APPLICATION_CONTEXT_KEY = "applicationContext";
	private ApplicationContext appCtx;
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd";
	private Company company;
	private CompanyManager companymanager;
	private List<Company> companyList;
	private ExtractQueryFileManager extractManager;
	private Long id;
	private ExtractQueryFile extractQueryFile;
	private ClaimManager claimManager;
	private User user;
	private ServiceOrderManager serviceOrderManager;
	private EmailSetupManager emailSetupManager;
	private SQLExtractManager sqlExtractManager;
	private SQLExtract sqlExtract;
	private String defaultCondition1;
	private String defaultCondition2;
	private String defaultCondition3;
	private String defaultCondition4;
	private String defaultCondition5;
	private String queryString = "";
	private String where ="where";
	private  String and   ="and";
	private String like=  "like";
	private  String orderby="order By";
	private String groupby ="group by";
	private String whereClause1Value;
	private String whereClause2Value;
	private String whereClause3Value;
	private String whereClause4Value;
	private String whereClause5Value;
	private String orderByValue;
	private String groupByValue;
	private String orderBy;
	private String groupBy;
	private String emailTo;
	private UserManager userManager;
	private String emailOut=(String)AppInitServlet.redskyConfigMap.get("email.out");
	private String emailDomain=(String)AppInitServlet.redskyConfigMap.get("email.domainfilter");
	static final Logger logger = Logger.getLogger(DynamicSQLExtractJob.class);   
	private ApplicationContext getApplicationContext(JobExecutionContext context)
			throws Exception {
		ApplicationContext appCtx = null;
		appCtx = (ApplicationContext) context.getScheduler().getContext().get(
				APPLICATION_CONTEXT_KEY);
		if (appCtx == null) {
			throw new JobExecutionException(
					"No application context available in scheduler context for key \""
							+ APPLICATION_CONTEXT_KEY + "\"");
		}
		return appCtx;
	}
	
	//This method automatically called when you run jetty see applicationContext.xml 
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException { 
		String ipAddress = "";
		try {
			ipAddress = "" + InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			logger.error("Error executing query " + e.getStackTrace()[0]);
		}
		try {
			logger.warn(emailOut+"\n\n\n\n\n\n\n\n\n\n\n\n\n sql extract");
			String executionTime="";
			appCtx = getApplicationContext(context);  
			ExtractQueryFileManager extractManager=(ExtractQueryFileManager)appCtx.getBean("extractManager");
			ClaimManager claimManager=(ClaimManager)appCtx.getBean("claimManager");
			sqlExtractManager=(SQLExtractManager)appCtx.getBean("sqlExtractManager");  
			emailSetupManager=(EmailSetupManager)appCtx.getBean("emailSetupManager");
			UserManager userManager=(UserManager)appCtx.getBean("userManager");  
			List schedulerQueryList =sqlExtractManager.getSchedulerQueryList();
			Iterator schedulerIterator = schedulerQueryList.iterator(); 
			SimpleDateFormat format = new SimpleDateFormat("MM-dd");
		    StringBuilder systemDate = new StringBuilder(format.format(new Date())); 
			String queryScheduler=new String(""); 
			List calenderSchedulerQueryList=new ArrayList(); 
		    Date todayDate=format.parse(systemDate.toString());
			while(schedulerIterator.hasNext()){
				try{
					//id=(Long)schedulerIterator.next(); 
					String idData=schedulerIterator.next().toString();
					id = Long.parseLong(idData)  ;
					sqlExtract = sqlExtractManager.get(id);
					if(sqlExtract.getSqlQueryScheduler().equalsIgnoreCase("D")){
						calenderSchedulerQueryList.add(id);
					}
					if(sqlExtract.getSqlQueryScheduler().equalsIgnoreCase("Q")){
						Date firstQuarterDate=format.parse("03-31");
						Date secondQuarterDate=format.parse("06-30");
						Date thirdQuarterDate=format.parse("09-30");
						Date fourthQuarterDate=format.parse("12-31");
						if(todayDate.equals(firstQuarterDate)||todayDate.equals(secondQuarterDate)||todayDate.equals(thirdQuarterDate)||todayDate.equals(fourthQuarterDate)){
							calenderSchedulerQueryList.add(id);
						}
					}
					if(sqlExtract.getSqlQueryScheduler().equalsIgnoreCase("W")){
						GregorianCalendar newCal = new GregorianCalendar( );
						int day = newCal.get( Calendar.DAY_OF_WEEK );
						String perWeekScheduler =new String("0");
						perWeekScheduler=sqlExtract.getPerWeekScheduler();
						if((Integer.parseInt(perWeekScheduler))==day){
							calenderSchedulerQueryList.add(id);	
						}

					}
					if(sqlExtract.getSqlQueryScheduler().equalsIgnoreCase("M")){
						GregorianCalendar newCal = new GregorianCalendar( );
						int day = newCal.get( Calendar.DAY_OF_MONTH ); 
						String perMonthScheduler =new String("0");
						perMonthScheduler=sqlExtract.getPerMonthScheduler();
						if((Integer.parseInt(perMonthScheduler))==day){
							calenderSchedulerQueryList.add(id);	
						}

					}
				}catch (Exception e) { 
					e.printStackTrace();
					String emailmessage=e.toString();
				 	 if(emailmessage.length()>200){
				 		emailmessage=emailmessage.substring(200);	 
				 	 }
				 	 
			    }	
				}
			
			Iterator itscheduler = calenderSchedulerQueryList.iterator(); 
			
			
			while(itscheduler.hasNext()){
				try{
				id=Long.parseLong(itscheduler.next().toString()); 
				String header = new String();
				sqlExtract = sqlExtractManager.get(id);
				String tempCorpId=sqlExtract.getCorpID();
				defaultCondition1 = sqlExtract.getDefaultCondition1();
				defaultCondition2 = sqlExtract.getDefaultCondition2();
				defaultCondition3 = sqlExtract.getDefaultCondition3();
				defaultCondition4 = sqlExtract.getDefaultCondition4();
				defaultCondition5 = sqlExtract.getDefaultCondition5(); 
				
			if(sqlExtract.getWhereClause1() == null || sqlExtract.getWhereClause1().trim().equalsIgnoreCase("")   )
					{
						queryString = sqlExtract.getSqlText();
					}
				
				else if(sqlExtract.getWhereClause2()== null||  sqlExtract.getWhereClause2().trim().equalsIgnoreCase("") )
				{ 
					if(sqlExtract.getSqlText().contains("where"))
						{ 
						     queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'");	
						 }
						else
						{
							queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'");	
						} 	
				} 
				else if(sqlExtract.getWhereClause3()== null || sqlExtract.getWhereClause3().trim().equalsIgnoreCase(""))
				{ 
					if(sqlExtract.getSqlText().contains("where"))
					{
						queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'");
					}
					else
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'");
					}
				} 
				else if( sqlExtract.getWhereClause4() == null || sqlExtract.getWhereClause4().trim().equalsIgnoreCase(""))
				{ 	
					if(sqlExtract.getSqlText().contains("where"))
					{				
						queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");
					}
					else
					{
						queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");
						
					}
				}  
				else if( sqlExtract.getWhereClause5() == null || sqlExtract.getWhereClause5().trim().equalsIgnoreCase(""))
				{ 	
					if(sqlExtract.getSqlText().contains("where"))
					{	
						queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
					 }
					else
					{
						queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
					 }
				}
				else{ 
					if(sqlExtract.getSqlText().contains("where"))
						{	
							queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
						}
						else
						{
							queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
						}
					} 
						if(sqlExtract.getOrderBy() == null || sqlExtract.getOrderBy().trim().equalsIgnoreCase("") )
							{
								if( sqlExtract.getGroupBy()== null || sqlExtract.getGroupBy().trim().equalsIgnoreCase(""))
									{
										queryString=queryString;
									}
									else
									{
									queryString=queryString.concat(" ").concat(groupby).concat(" ").concat(sqlExtract.getGroupBy());
			
									}
							}
							else if (  sqlExtract.getGroupBy()== null || sqlExtract.getGroupBy().trim().equalsIgnoreCase(""))
							{
							
									if(!orderByValue.equalsIgnoreCase("") && !orderByValue.equalsIgnoreCase("Ascending")){
										orderByValue="desc";
									}
									else{
										orderByValue="asc";
									}
								queryString=queryString.concat(" ").concat(orderby).concat(" ").concat(sqlExtract.getOrderBy().concat(" ").concat(orderByValue));
			
							} 
						logger.warn("\n\n\n\n\n\n\n\n\n sql query           "+queryString);
				try{
					String executeTest = sqlExtractManager.testSQLQuery(queryString);
				}catch(Exception ex){ 
				} 
				String sqlError = ""; 
				String subject="";
				List extractList=new ArrayList();
						try{
						
							extractList = sqlExtractManager.sqlDataExtract(queryString);
						}catch(Exception ex)
						{
							extractList.add("Error");
						}
						if(sqlExtract.getFileType().toLowerCase().equals("xls"))
						{
						String uploadDir =("/resources");
						uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local"+ "/" + "redskydoc" + "/" + "sqlExtract" + "/" + tempCorpId + "/");
						try{
						 File dirPath = new File(uploadDir);
					        if (!dirPath.exists()) {
					            dirPath.mkdirs();
					        }
						}catch(Exception e){}
						File file = new File(sqlExtract.getFileName());
						String file_name = sqlExtract.getFileName();
						subject=file_name;
						String fileName=sqlExtract.getFileName()+"."+sqlExtract.getFileType().toLowerCase();
						file_name=uploadDir+file_name+"."+sqlExtract.getFileType().toLowerCase();
				        FileWriter file1 = new FileWriter(file_name);
				        BufferedWriter out = new BufferedWriter (file1); 
				        boolean dataReturn=false;
						if(!extractList.contains("Error"))
						{
							
						if (!extractList.isEmpty()) 
									{ 	
										String query = queryString.replaceAll("\r\n", " ");
										int starValue = query.indexOf("as")+3;
										int endValue = query.indexOf(" from ");
										String subQuery = query.substring(starValue,endValue);
										subQuery = subQuery+",";
										String [] temp = null;
								       
										temp = subQuery.split(" as '");
										int totalLength = temp.length;
										String finalOP ="";
										for (int i = 0 ; i < totalLength ; i++) {
								     	   String tmp = temp[i];
								     	   finalOP += tmp.substring(0,tmp.indexOf(",")-1)+"\t";
										}
										header = finalOP.substring(0,finalOP.length()-1)+"\n";
										 out.write(header);
						
										Iterator it = extractList.iterator();
										while (it.hasNext()) 
											{
												try{
												Object[] row = (Object[]) it.next();
												
				
												for(int i=0; i<totalLength; i++)
													{
														if (row[i] != null) 
															{
																String data = row[i].toString();
																out.write((data + "\t"));
																dataReturn=true;
															} 
														else 
															{
															out.write("\t");
															}
													}
												}catch(Exception ex)
												{
													for(int i=0; i<totalLength; i++)
													{
														if ( it.next() != null) 
															{
																String data =  it.next().toString();
																out.write((data + "\t"));
															} 
														else 
															{
															out.write("\t");
															}
													}
												}
												out.write("\n");
											}
									
										
									}
						else{  
							header = "NO Record Found , thanks";
							out.write(header); 
						}
						}
								else{ 
									header = "NO Record Found , thanks";
									out.write(header);
										
									}
						out.close(); 
						try {
						String host = "localhost";
						   String from = "support@redskymobility.com"; 
						   String UserEmailId=sqlExtract.getEmail();
						   logger.warn("sqlExtract.getEmail()="+sqlExtract.getEmail()+"\n");
							String tempRecipient="";
							String tempRecipientArr[]=UserEmailId.split(",");
							for(String str1:tempRecipientArr){
								if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
									if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
									tempRecipient += str1;
								}
							}
							UserEmailId=tempRecipient;
							 logger.warn("After Filter from doNotEmailFlag ="+UserEmailId+"\n");
							 logger.warn("emailDomain ="+emailDomain+"\n");
							 logger.warn("emailOut ="+emailOut+"\n");
							if((emailOut!=null)&&(!emailOut.equalsIgnoreCase(""))&&(emailOut.equalsIgnoreCase("YES")) && dataReturn){						
								//Run for Dev1 & dev2
								if((emailDomain!=null)&&(!emailDomain.equalsIgnoreCase(""))){
									 String chk[]= UserEmailId.split(",");
									 String strtemp="";
									 for(String str:chk){
										 if((emailDomain.indexOf(str.substring(str.indexOf("@")+1, str.length()))>-1)){
												if (!strtemp.equalsIgnoreCase("")) strtemp += ",";
												strtemp += str;
										 }
									 }
									 UserEmailId=strtemp;
									}
								 emailTo=UserEmailId;
								 logger.warn("Final TO address is ="+UserEmailId+"\n");
								 if(!UserEmailId.equalsIgnoreCase("")){
								   String to[] = UserEmailId.split(","); 
								   logger.warn("Final TO address is ="+UserEmailId+"\n");
								   //String filename = file.getName() + ".xls";
								   // Get system properties
								   Properties props = System.getProperties();
								   props.put("mail.transport.protocol", "smtp");
								   props.put("mail.smtp.host", host); 
								   Session session = Session.getInstance(props, null);
								   session.setDebug(true);
								   logger.warn(session.getProperties());
								   
								 /*  Message message = new MimeMessage(session);
								   message.setFrom(new InternetAddress(from)); 
								   InternetAddress[] toAddress = new InternetAddress[to.length];
								   for (int i = 0; i < to.length; i++)
								     toAddress[i] = new InternetAddress(to[i]);
								   message.setRecipients(Message.RecipientType.TO, toAddress);    
								   message.setSubject(subject);
								   BodyPart messageBodyPart = new MimeBodyPart();
								   String msgText1 = "Hi "+",\n\nPlease check the attached auto generated extract file.\n\nSchedule Details\nFile Name:"+fileName;
								   msgText1 = msgText1 	+ "\n\nSource # " + ipAddress.substring(ipAddress.lastIndexOf(".")+1, ipAddress.length()) + "\n";
								   messageBodyPart.setText(msgText1);
								   Multipart multipart = new MimeMultipart();
								   multipart.addBodyPart(messageBodyPart);
								   messageBodyPart = new MimeBodyPart();
								   DataSource source = new FileDataSource(file_name);
								   messageBodyPart.setDataHandler(new DataHandler(source));
								   messageBodyPart.setFileName(fileName); 
								   multipart.addBodyPart(messageBodyPart);
								   message.setContent(multipart);  
								  try{
								       Transport.send(message);
								       logger.warn("\n\n\n\n\n\n\n Email11 ex    ");
								  }
								  catch(SendFailedException sfe)
								   {  
								 	 message.setRecipients(Message.RecipientType.TO,  sfe.getValidUnsentAddresses());
								 	 Transport.send(message); 
								   } */
								    String msgText1 = "Hi "+",\n\nPlease check the attached auto generated extract file.\n\nSchedule Details\nFile Name:"+fileName;
								    msgText1 = msgText1 	+ "\n\nSource # " + ipAddress.substring(ipAddress.lastIndexOf(".")+1, ipAddress.length()) + "\n";
								    EmailSetup emailSetup = new EmailSetup();
									emailSetup.setRetryCount(0);
									emailSetup.setRecipientTo(emailTo);
									emailSetup.setRecipientCc("");
									emailSetup.setRecipientBcc("");
									emailSetup.setSubject(subject);
									emailSetup.setBody(msgText1);
									emailSetup.setSignature(from);
									emailSetup.setAttchedFileLocation(file_name);
									emailSetup.setDateSent(new Date());
									emailSetup.setEmailStatus("SaveForEmail");
									emailSetup.setCorpId(sqlExtract.getCorpID());
									emailSetup.setCreatedOn(new Date());
									emailSetup.setCreatedBy("EMAILSETUP");
									emailSetup.setUpdatedOn(new Date());
									emailSetup.setUpdatedBy("EMAILSETUP");
									emailSetup.setSignaturePart("");
									emailSetup.setModule("SQL Extract");
									emailSetup.setFileNumber("");		
									emailSetupManager.save(emailSetup);
							}
							}
						  }catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace(); 
						 }
						}if(sqlExtract.getFileType().toLowerCase().equals("csv"))
						{
							String uploadDir =("/resources");
							uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local"+ "/" + "redskydoc" + "/" + "sqlExtract" + "/" + tempCorpId + "/");
							try{
							 File dirPath = new File(uploadDir);
						        if (!dirPath.exists()) {
						            dirPath.mkdirs();
						        }
							}catch(Exception e){}
							File file = new File(sqlExtract.getFileName());
							String file_name = sqlExtract.getFileName();
							subject=file_name;
							String fileName=sqlExtract.getFileName()+"."+sqlExtract.getFileType().toLowerCase();
							file_name=uploadDir+file_name+"."+sqlExtract.getFileType().toLowerCase();
					        FileWriter file1 = new FileWriter(file_name);
					        //BufferedWriter out = new BufferedWriter (file1); 
					        PrintWriter out = new PrintWriter(file1);
					        boolean dataReturn=false;
							if(!extractList.contains("Error"))
							{
								
							if (!extractList.isEmpty()) 
										{ 	
											String query = queryString.replaceAll("\r\n", " ");
											int starValue = query.indexOf("as")+3;
											int endValue = query.indexOf(" from ");
											String subQuery = query.substring(starValue,endValue);
											subQuery = subQuery+",";
											String [] temp = null;
									       
											temp = subQuery.split(" as '");
											int totalLength = temp.length;
											String finalOP ="";
											for (int i = 0 ; i < totalLength ; i++) {
										     	   String tmp = temp[i];
										     	   String tmp1=tmp.substring(0,tmp.indexOf(",")-1);
										     	   String tmp2=tmp1.replace("''", "");
										     	  String tmp5=tmp2.replace("'", "");
										     	  String tmp3=tmp5.replace("?","");
										     	  finalOP += tmp3+",";
												}
												//header = finalOP.substring(0,finalOP.length()-1)+"\n";
												//outputStream.write(new String("\"header"+"\",").getBytes());
												header = finalOP.substring(0,finalOP.length()-1);
												out.print(header);
												out.print("\n");
							
											Iterator it = extractList.iterator();
											while (it.hasNext()) 
												{
													try{
													Object[] row = (Object[]) it.next();
													
					
													for(int i=0; i<totalLength; i++)
														{
															if (row[i] != null) 
																{
																	String data = row[i].toString();
																	data=data.replaceAll(",", "");
																	if(i==totalLength-1){
																		out.print((data + ""));	
																	}else{
																	out.print((data + ","));
																	}
																	dataReturn=true;
																} 
															else 
																{
																if(i==totalLength-1){
																	
																}else{
																out.print(",");
																}
																}
														}
													}catch(Exception ex)
													{
														for(int i=0; i<totalLength; i++)
														{
															if ( it.next() != null) 
																{
																	String data =  it.next().toString();
																	data=data.replaceAll(",", "");
																	if(i==totalLength-1){
																		out.print((data + ""));
																	}else{
																	out.print((data + ","));
																	}
																} 
															else 
																{
																if(i==totalLength-1){
																	
																}else{
																out.print(",");
																}
																}
														}
													}
													out.print("\n");
												}
										
											
										}
							else{  
								header = "NO Record Found , thanks";
								out.print(header); 
							}
							}
									else{ 
										header = "NO Record Found , thanks";
										out.print(header);
											
										}
							out.close(); 
							try {
							String host = "localhost";
							   String from = "support@redskymobility.com"; 
							   String UserEmailId=sqlExtract.getEmail();
							   logger.warn("sqlExtract.getEmail()="+sqlExtract.getEmail()+"\n");
								String tempRecipient="";
								String tempRecipientArr[]=UserEmailId.split(",");
								for(String str1:tempRecipientArr){
									if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
										if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
										tempRecipient += str1;
									}
								}
								UserEmailId=tempRecipient;
								 logger.warn("After Filter from doNotEmailFlag ="+UserEmailId+"\n");
								 logger.warn("emailDomain ="+emailDomain+"\n");
								 logger.warn("emailOut ="+emailOut+"\n");
								if((emailOut!=null)&&(!emailOut.equalsIgnoreCase(""))&&(emailOut.equalsIgnoreCase("YES")) && dataReturn){						
									//Run for Dev1 & dev2
									if((emailDomain!=null)&&(!emailDomain.equalsIgnoreCase(""))){
										 String chk[]= UserEmailId.split(",");
										 String strtemp="";
										 for(String str:chk){
											 if((emailDomain.indexOf(str.substring(str.indexOf("@")+1, str.length()))>-1)){
													if (!strtemp.equalsIgnoreCase("")) strtemp += ",";
													strtemp += str;
											 }
										 }
										 UserEmailId=strtemp;
										}
									 emailTo=UserEmailId;
									 logger.warn("Final TO address is ="+UserEmailId+"\n");
									 if(!UserEmailId.equalsIgnoreCase("")){
									   String to[] = UserEmailId.split(","); 
									   logger.warn("Final TO address is ="+UserEmailId+"\n");
									   //String filename = file.getName() + ".xls";
									   // Get system properties
									   Properties props = System.getProperties();
									   props.put("mail.transport.protocol", "smtp");
									   props.put("mail.smtp.host", host); 
									   Session session = Session.getInstance(props, null);
									   session.setDebug(true);
									   logger.warn(session.getProperties());
									   
									   /*Message message = new MimeMessage(session);
									   message.setFrom(new InternetAddress(from)); 
									   InternetAddress[] toAddress = new InternetAddress[to.length];
									   for (int i = 0; i < to.length; i++)
									     toAddress[i] = new InternetAddress(to[i]);
									   message.setRecipients(Message.RecipientType.TO, toAddress);    
									   message.setSubject(subject);
									   BodyPart messageBodyPart = new MimeBodyPart();
									   String msgText1 = "Hi "+",\n\nPlease check the attached auto generated extract file.\n\nSchedule Details\nFile Name:"+fileName;
									   msgText1 = msgText1 	+ "\n\nSource # " + ipAddress.substring(ipAddress.lastIndexOf(".")+1, ipAddress.length()) + "\n";
									   messageBodyPart.setText(msgText1);
									   Multipart multipart = new MimeMultipart();
									   multipart.addBodyPart(messageBodyPart);
									   messageBodyPart = new MimeBodyPart();
									   DataSource source = new FileDataSource(file_name);
									   messageBodyPart.setDataHandler(new DataHandler(source));
									   messageBodyPart.setFileName(fileName); 
									   multipart.addBodyPart(messageBodyPart);
									   message.setContent(multipart);  
									  try{
									       Transport.send(message);
									       logger.warn("\n\n\n\n\n\n\n Email11 ex    ");
									  }
									  catch(SendFailedException sfe)
									   {  
									 	 message.setRecipients(Message.RecipientType.TO,  sfe.getValidUnsentAddresses());
									 	 Transport.send(message); 
									   } */
									    String msgText1 = "Hi "+",\n\nPlease check the attached auto generated extract file.\n\nSchedule Details\nFile Name:"+fileName;
									    msgText1 = msgText1 	+ "\n\nSource # " + ipAddress.substring(ipAddress.lastIndexOf(".")+1, ipAddress.length()) + "\n";
									    EmailSetup emailSetup = new EmailSetup();
										emailSetup.setRetryCount(0);
										emailSetup.setRecipientTo(emailTo);
										emailSetup.setRecipientCc("");
										emailSetup.setRecipientBcc("");
										emailSetup.setSubject(subject);
										emailSetup.setBody(msgText1);
										emailSetup.setSignature(from);
										emailSetup.setAttchedFileLocation(file_name);
										emailSetup.setDateSent(new Date());
										emailSetup.setEmailStatus("SaveForEmail");
										emailSetup.setCorpId(sqlExtract.getCorpID());
										emailSetup.setCreatedOn(new Date());
										emailSetup.setCreatedBy("EMAILSETUP");
										emailSetup.setUpdatedOn(new Date());
										emailSetup.setUpdatedBy("EMAILSETUP");
										emailSetup.setSignaturePart("");
										emailSetup.setModule("SQL Extract");
										emailSetup.setFileNumber("");		
										emailSetupManager.save(emailSetup);
								}
								}
							  }catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace(); 
							 }
							}
						 
			}catch(Exception e4){e4.printStackTrace();}
			}
			
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}	
		
	}

	public void setCompanymanager(CompanyManager companymanager) {
		this.companymanager = companymanager;
	}

	public List<Company> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public ExtractQueryFileManager getExtractManager() {
		return extractManager;
	}

	public void setExtractManager(ExtractQueryFileManager extractManager) {
		this.extractManager = extractManager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ExtractQueryFile getExtractQueryFile() {
		return extractQueryFile;
	}

	public void setExtractQueryFile(ExtractQueryFile extractQueryFile) {
		this.extractQueryFile = extractQueryFile;
	}

	public ClaimManager getClaimManager() {
		return claimManager;
	}

	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setSqlExtractManager(SQLExtractManager sqlExtractManager) {
		this.sqlExtractManager = sqlExtractManager;
	}

	public SQLExtract getSqlExtract() {
		return sqlExtract;
	}

	public void setSqlExtract(SQLExtract sqlExtract) {
		this.sqlExtract = sqlExtract;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

}

