<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>


<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
<style>
span.pagelinks 
{display:block;
font-size:0.85em;
margin-bottom:5px;
!margin-bottom:-1px;
margin-top:-32px;
!margin-top:-52px;
padding:2px 0px;
padding-left: 52%;
!padding-left: 63%;
text-align:left;
width:51%;
!width:48%;
}
</style>
</head>
<!-- 
<c:set var="forms"> 
<s:form id="partnerListDetailForm" action='${empty param.popup?"editPartner.html":"editPartner.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="id"/>
<s:submit cssClass="button" method="edit" key="button.viewDetail"/>
</s:form>
</c:set>
 -->
  
<c:set var="buttons">   
	<c:if test="${not empty param.popup && partnerType == 'PP'}">  
		<input type="button" class="cssbutton" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerAddFormPopup.html?partnerType=${partnerType}&decorator=popup&popup=true"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	</c:if>
	<c:if test="${empty param.popup}">  
    <input type="button" class="cssbutton" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerAddForm.html?partnerType=${partnerType}"/>'"  
        value="<fmt:message key="button.add"/>"/>
    </c:if>       
     <%--   
    <input type="button" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/> 
      --%>    
</c:set> 
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerListForm" action='${empty param.popup?"searchPartnerAdmin.html":"searchPartner.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<s:hidden name="popupFrom" value="<%= request.getParameter("popupFrom")%>" />
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />


<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden  name="fld_seventhDescription" value="${param.fld_seventhDescription}" />
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" /> 
    <c:set var="fld_seventhDescription" value="${param.fld_seventhDescription}" />	
</c:if>
<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 

<table class="table" border="0" style="width:888px;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th><fmt:message key="partner.name"/></th>
<th>Country Code</th>
<th>Country Name</th>
<th><fmt:message key="partner.billingState"/></th>

<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partner.partnerCode" size="20" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="partner.lastName" size="24" cssClass="text medium" />
			</td>
			<c:if test="${partnerType == 'AG' || partnerType == 'VN'  || partnerType == 'CR'}">  
			 			<td>
						    <s:textfield name="partner.terminalCountryCode" size="15" cssClass="text medium"/>
						</td>
						<td>
						    <s:textfield name="partner.terminalCountry" size="24" cssClass="text medium"/>
						</td>
						<td>
						    <s:textfield name="partner.terminalState" size="24" cssClass="text medium"/>
						</td>
			</c:if>
			<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
						<td>
						    <s:textfield name="partner.billingCountryCode" size="15" cssClass="text medium"/>
						</td>
						<td>
						    <s:textfield name="partner.billingCountry" size="24" cssClass="text medium"/>
						</td>
						<td>
						    <s:textfield name="partner.billingState" size="24" cssClass="text medium"/>
						</td>
			</c:if>
		</tr>
		<tr>
			<td colspan="4"></td>
			<td width="130px" style="border-left: hidden;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
	</s:form>

<div id="newmnav">   
 <ul>
 <c:choose>
 	<c:when test="${partnerType == 'PP'}">  
 		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setPrivate();"><span>Private Party List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<li><a onclick="setAccount();"><span>Accounts List</span></a></li>
		<li><a onclick="setAgent();"><span>Agents List</span></a></li>
		<li><a onclick="setVendor();"><span>Vendors List</span></a></li>
		<li><a onclick="setCarrier();"><span>Carriers List</span></a></li>
		<li><a onclick="setOwner();"><span>Owner Ops</span></a></li>
	</c:when>
	<c:when test="${partnerType == 'AG'}"> 
		<li><a onclick="setPrivate();"><span>Private Party List</span></a></li>
		<li><a onclick="setAccount();"><span>Accounts List</span></a></li>
		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setAgent();"><span>Agents List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<li><a onclick="setVendor();"><span>Vendors List</span></a></li>
		<li><a onclick="setCarrier();"><span>Carriers List</span></a></li>
		<li><a onclick="setOwner();"><span>Owner Ops</span></a></li>
	</c:when>
	<c:when test="${partnerType == 'VN'}"> 
		<li><a onclick="setPrivate();"><span>Private Party List</span></a></li>
		<li><a onclick="setAccount();"><span>Accounts List</span></a></li>
		<li><a onclick="setAgent();"><span>Agents List</span></a></li>
		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setVendor();"><span>Vendors List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<li><a onclick="setCarrier();"><span>Carriers List</span></a></li>
		<li><a onclick="setOwner();"><span>Owner Ops</span></a></li>
	</c:when>
	<c:when test="${partnerType == 'CR'}"> 
		<li><a onclick="setPrivate();"><span>Private Party List</span></a></li>
		<li><a onclick="setAccount();"><span>Accounts List</span></a></li>
		<li><a onclick="setAgent();"><span>Agents List</span></a></li>
		<li><a onclick="setVendor();"><span>Vendors List</span></a></li>
		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setCarrier();"><span>Carriers List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<li><a onclick="setOwner();"><span>Owner Ops</span></a></li>
	</c:when>
	<c:when test="${partnerType == 'OO'}"> 
		<li><a onclick="setPrivate();"><span>Private Party List</span></a></li>
		<li><a onclick="setAccount();"><span>Accounts List</span></a></li>
		<li><a onclick="setAgent();"><span>Agents List</span></a></li>
		<li><a onclick="setVendor();"><span>Vendors List</span></a></li>
		<li><a onclick="setCarrier();"><span>Carriers List</span></a></li>
		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setOwner();"><span>Owner Ops<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</c:when>
	<c:otherwise>
		<c:set var="partnerType" value="AC" />
		<li><a onclick="setPrivate();"><span>Private Party List</span></a></li>
		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setAccount();"><span>Accounts List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<li><a onclick="setAgent();"><span>Agents List</span></a></li>
		<li><a onclick="setVendor();"><span>Vendors List</span></a></li>
		<li><a onclick="setCarrier();"><span>Carriers List</span></a></li>
		<li><a onclick="setOwner();"><span>Owner Ops</span></a></li>
	</c:otherwise>
</c:choose>
</ul>
</div><div class="spn" style="width:888px">&nbsp;</div><br>

<s:set name="partners" value="partners" scope="request"/>  
<display:table name="partners" class="table" requestURI="" id="partnerList" export="${empty param.popup}" defaultsort="2" pagesize="10" 
		style="width:888px" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode"
		href="editPartnerAddForm.html?partnerType=${partnerType}" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>    
    <display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
    <c:if test="${partnerType == 'AG'}"> 
	    <display:column property="rank" sortable="true" titleKey="partner.rank" style="width:15px"/>
	    <display:column property="terminalCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
        <display:column property="terminalState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
        <display:column property="terminalCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    </c:if>
    <c:if test="${partnerType == 'VN'  || partnerType == 'CR' }"> 
	    <display:column property="terminalCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
        <display:column property="terminalState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
        <display:column property="terminalCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    </c:if>
    <c:if test="${partnerType == 'PP' || partnerType == 'OO' || partnerType == 'AC'}"> 
    <display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    </c:if>
    <c:if test="${partnerType == 'AG'}">  
    <display:column titleKey="partner.fidi" style="width:65px"><c:if test="${partnerList.fidi}"><c:out value="Yes" /></c:if><c:if test="${!partnerList.fidi}"><c:out value="No" /></c:if></display:column>
    </c:if> 
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:140px"/>
 	
    <c:if test="${param.popup}">
    	<display:column style="width:105px;cursor:pointer;"><A onclick="location.href='<c:url value="/viewPartner.html?id=${partnerList.id}&partnerType=${partnerType}&type=ZZ&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"/>'">View Detail</A></display:column>
    </c:if>
    
    
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>   
  
    <display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>  
</div>
<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:if test="${not empty param.popup && partnerType == 'PP' && (flag==0 || flag==1 || flag==2 || flag==3 || flag==4)}">  
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td><c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:set var="isTrue" value="false" scope="session"/>
<!--

			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" align="center" class="content-tab"><a href="editPartner.html?id=${myMessageList.id} " >Addresses</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" align="center" class="content-tab"><a href="editPartnerDetail.html?id=${myMessageList.id} " >Details</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" align="center" class="content-tab"><a href="editPartnerRemarks.html?id=${myMessageList.id} " >Remarks</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
 -->

<script language="javascript" type="text/javascript">
function clear_fields(){
		document.forms['partnerListForm'].elements['partner.lastName'].value = "";
		document.forms['partnerListForm'].elements['partner.partnerCode'].value = "";
		<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'CR'}"> 
			document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value = "";
			document.forms['partnerListForm'].elements['partner.terminalState'].value = "";
			document.forms['partnerListForm'].elements['partner.terminalCountry'].value = "";
		</c:if>  
  			<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
			document.forms['partnerListForm'].elements['partner.billingCountryCode'].value = "";
			document.forms['partnerListForm'].elements['partner.billingState'].value = "";
			document.forms['partnerListForm'].elements['partner.billingCountry'].value = "";
		</c:if> 
}

function setAccount(){
	<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'CR'}"> 
		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value =document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.terminalCountry'].value =document.forms['partnerListForm'].elements['partner.terminalCountry'].value;
		document.forms['partnerListForm'].elements['partner.terminalState'].value =document.forms['partnerListForm'].elements['partner.terminalState'].value;
	</c:if>  
   	<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value =document.forms['partnerListForm'].elements['partner.billingCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.billingCountry'].value =document.forms['partnerListForm'].elements['partner.billingCountry'].value;
		document.forms['partnerListForm'].elements['partner.billingState'].value =document.forms['partnerListForm'].elements['partner.billingState'].value;
	</c:if>
	document.forms['partnerListForm'].elements['partnerType'].value = "AC";
	document.forms['partnerListForm'].elements['findFor'].value = "account";
	document.forms['partnerListForm'].elements['partner.lastName'].value = document.forms['partnerListForm'].elements['partner.lastName'].value;
	document.forms['partnerListForm'].elements['partner.partnerCode'].value = document.forms['partnerListForm'].elements['partner.partnerCode'].value;
	document.forms['partnerListForm'].submit();
}

function setPrivate(){
	<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'CR'}"> 
		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value =document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.terminalCountry'].value =document.forms['partnerListForm'].elements['partner.terminalCountry'].value;
		document.forms['partnerListForm'].elements['partner.terminalState'].value =document.forms['partnerListForm'].elements['partner.terminalState'].value;
	</c:if>  
   	<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value =document.forms['partnerListForm'].elements['partner.billingCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.billingCountry'].value =document.forms['partnerListForm'].elements['partner.billingCountry'].value;
		document.forms['partnerListForm'].elements['partner.billingState'].value =document.forms['partnerListForm'].elements['partner.billingState'].value;
	</c:if>
	document.forms['partnerListForm'].elements['partnerType'].value = "PP";
	document.forms['partnerListForm'].elements['findFor'].value = "account";
	document.forms['partnerListForm'].elements['partner.lastName'].value = document.forms['partnerListForm'].elements['partner.lastName'].value;
	document.forms['partnerListForm'].elements['partner.partnerCode'].value = document.forms['partnerListForm'].elements['partner.partnerCode'].value;
	document.forms['partnerListForm'].submit();
}

function setAgent(){
	<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'CR'}"> 
		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value =document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.terminalCountry'].value =document.forms['partnerListForm'].elements['partner.terminalCountry'].value;
		document.forms['partnerListForm'].elements['partner.terminalState'].value =document.forms['partnerListForm'].elements['partner.terminalState'].value;
	</c:if>  
   	<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value =document.forms['partnerListForm'].elements['partner.billingCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.billingCountry'].value =document.forms['partnerListForm'].elements['partner.billingCountry'].value;
		document.forms['partnerListForm'].elements['partner.billingState'].value =document.forms['partnerListForm'].elements['partner.billingState'].value;
	</c:if>
	document.forms['partnerListForm'].elements['partnerType'].value = "AG";
	document.forms['partnerListForm'].elements['findFor'].value = "account";
	document.forms['partnerListForm'].elements['partner.lastName'].value =document.forms['partnerListForm'].elements['partner.lastName'].value;
	document.forms['partnerListForm'].elements['partner.partnerCode'].value =document.forms['partnerListForm'].elements['partner.partnerCode'].value;
	document.forms['partnerListForm'].submit();
}

function setVendor(){
	<c:if test="${partnerType == 'AG' || partnerType == 'VN'  || partnerType == 'CR'}"> 
		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value =document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.terminalCountry'].value =document.forms['partnerListForm'].elements['partner.terminalCountry'].value;
		document.forms['partnerListForm'].elements['partner.terminalState'].value =document.forms['partnerListForm'].elements['partner.terminalState'].value;
	</c:if>  
   	<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value =document.forms['partnerListForm'].elements['partner.billingCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.billingCountry'].value =document.forms['partnerListForm'].elements['partner.billingCountry'].value;
		document.forms['partnerListForm'].elements['partner.billingState'].value =document.forms['partnerListForm'].elements['partner.billingState'].value;
	</c:if>
	document.forms['partnerListForm'].elements['partnerType'].value = "VN";
	document.forms['partnerListForm'].elements['findFor'].value = "vendor";
	document.forms['partnerListForm'].elements['partner.lastName'].value =document.forms['partnerListForm'].elements['partner.lastName'].value;
	document.forms['partnerListForm'].elements['partner.partnerCode'].value = document.forms['partnerListForm'].elements['partner.partnerCode'].value;
	document.forms['partnerListForm'].submit();
}

function setCarrier(){
	
	<c:if test="${partnerType == 'AG' || partnerType == 'VN'  || partnerType == 'CR'}"> 
		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value =document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.terminalCountry'].value =document.forms['partnerListForm'].elements['partner.terminalCountry'].value;
		document.forms['partnerListForm'].elements['partner.terminalState'].value =document.forms['partnerListForm'].elements['partner.terminalState'].value;
	</c:if>  
   	<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value =document.forms['partnerListForm'].elements['partner.billingCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.billingCountry'].value =document.forms['partnerListForm'].elements['partner.billingCountry'].value;
		document.forms['partnerListForm'].elements['partner.billingState'].value =document.forms['partnerListForm'].elements['partner.billingState'].value;
	</c:if>
	document.forms['partnerListForm'].elements['partnerType'].value = "CR";
	document.forms['partnerListForm'].elements['findFor'].value = "carrier";
	document.forms['partnerListForm'].elements['partner.lastName'].value =document.forms['partnerListForm'].elements['partner.lastName'].value;
	document.forms['partnerListForm'].elements['partner.partnerCode'].value = document.forms['partnerListForm'].elements['partner.partnerCode'].value;
	document.forms['partnerListForm'].submit();
}

function setOwner(){
	<c:if test="${partnerType == 'AG' || partnerType == 'VN'  || partnerType == 'CR'}"> 
		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value =document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.terminalCountry'].value =document.forms['partnerListForm'].elements['partner.terminalCountry'].value;
		document.forms['partnerListForm'].elements['partner.terminalState'].value =document.forms['partnerListForm'].elements['partner.terminalState'].value;
	</c:if>  
   	<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value =document.forms['partnerListForm'].elements['partner.billingCountryCode'].value;
		document.forms['partnerListForm'].elements['partner.billingCountry'].value =document.forms['partnerListForm'].elements['partner.billingCountry'].value;
		document.forms['partnerListForm'].elements['partner.billingState'].value =document.forms['partnerListForm'].elements['partner.billingState'].value;
	</c:if>
	document.forms['partnerListForm'].elements['partnerType'].value = "OO";
	document.forms['partnerListForm'].elements['findFor'].value = "owner";
	document.forms['partnerListForm'].elements['partner.lastName'].value = document.forms['partnerListForm'].elements['partner.lastName'].value;
	document.forms['partnerListForm'].elements['partner.partnerCode'].value =document.forms['partnerListForm'].elements['partner.partnerCode'].value;
	document.forms['partnerListForm'].submit();
}
</script>
<script type="text/javascript">   
   //highlightTableRows("partnerList"); 
   Form.focusFirstElement($("partnerListForm")); 
  
   	try{
   	if('<%=session.getAttribute("lastName")%>'=='null'){
   		document.forms['partnerListForm'].elements['partner.lastName'].value='';
   	}
   	}
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("lastName")%>'!='null'){
   		document.forms['partnerListForm'].elements['partner.lastName'].value='<%=session.getAttribute("lastName")%>';
   	}
   	}
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("partnerCode")%>'=='null'){	
   		document.forms['partnerListForm'].elements['partner.partnerCode'].value='';
   	}
   	}
   	catch(e){}
   	if('<%=session.getAttribute("partnerCode")%>'!='null'){	
   		document.forms['partnerListForm'].elements['partner.partnerCode'].value='<%=session.getAttribute("partnerCode")%>';
   	}
   	
   	try{
   	<c:if test="${partnerType == 'AG' || partnerType == 'VN'  || partnerType == 'CR'}"> 
   		if('<%=session.getAttribute("billingCountryCode")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value='';
	   	}
		if('<%=session.getAttribute("billingCountryCode")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value='<%=session.getAttribute("billingCountryCode")%>';
	   	}
	   	
	   	if('<%=session.getAttribute("billingCountry")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.terminalCountry'].value='';
	   	}
		if('<%=session.getAttribute("billingCountry")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.terminalCountry'].value='<%=session.getAttribute("billingCountry")%>';
	   	}
	   	
	   	if('<%=session.getAttribute("billingStateCode")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.terminalState'].value='';
	   	}
	   	if('<%=session.getAttribute("billingStateCode")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.terminalState'].value='<%=session.getAttribute("billingStateCode")%>';
	   	}
   	</c:if>  
   	}
   	catch(e){}
   try{
   	<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''}">  
	   	if('<%=session.getAttribute("billingCountryCode")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value='';
	   	}
		if('<%=session.getAttribute("billingCountryCode")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value='<%=session.getAttribute("billingCountryCode")%>';
	   	}
	   	
	   	if('<%=session.getAttribute("billingCountry")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.billingCountry'].value='';
	   	}
		if('<%=session.getAttribute("billingCountry")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.billingCountry'].value='<%=session.getAttribute("billingCountry")%>';
	   	}
	   	
	   	if('<%=session.getAttribute("billingStateCode")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.billingState'].value='';
	   	}
	   	if('<%=session.getAttribute("billingStateCode")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.billingState'].value='<%=session.getAttribute("billingStateCode")%>';
	   	}
	</c:if>
   }
   catch(e){}
</script>