package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "partnerrefhaulinggrid")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class PartnerRefHaulingGrid extends BaseObject{
	
	private Long id;
	private String corpID;
	private BigDecimal lowDistance;
	private BigDecimal highDistance;
	private String grid;
	private String countryCode;
	private String unit;
	private BigDecimal rateMile;
	private BigDecimal rateFlat;
	
	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("lowDistance", lowDistance).append(
				"highDistance", highDistance).append("grid", grid).append(
				"countryCode", countryCode).append("unit", unit).append(
				"rateMile", rateMile).append("rateFlat", rateFlat).append(
				"createdBy", createdBy).append("updatedBy", updatedBy).append(
				"createdOn", createdOn).append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerRefHaulingGrid))
			return false;
		PartnerRefHaulingGrid castOther = (PartnerRefHaulingGrid) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(lowDistance, castOther.lowDistance)
				.append(highDistance, castOther.highDistance).append(grid,
						castOther.grid).append(countryCode,
						castOther.countryCode).append(unit, castOther.unit)
				.append(rateMile, castOther.rateMile).append(rateFlat,
						castOther.rateFlat).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				lowDistance).append(highDistance).append(grid).append(
				countryCode).append(unit).append(rateMile).append(rateFlat)
				.append(createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).toHashCode();
	}
	
	@Column(length = 10)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length = 5)
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	@Column(length = 50)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length = 5)
	public String getGrid() {
		return grid;
	}
	public void setGrid(String grid) {
		this.grid = grid;
	}
	@Column(length = 10)
	public BigDecimal getHighDistance() {
		return highDistance;
	}
	public void setHighDistance(BigDecimal highDistance) {
		this.highDistance = highDistance;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length = 10)
	public BigDecimal getLowDistance() {
		return lowDistance;
	}
	public void setLowDistance(BigDecimal lowDistance) {
		this.lowDistance = lowDistance;
	}
	@Column(length = 10)
	public BigDecimal getRateFlat() {
		return rateFlat;
	}
	public void setRateFlat(BigDecimal rateFlat) {
		this.rateFlat = rateFlat;
	}
	@Column(length = 10)
	public BigDecimal getRateMile() {
		return rateMile;
	}
	public void setRateMile(BigDecimal rateMile) {
		this.rateMile = rateMile;
	}
	@Column(length = 10)
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	@Column(length = 10)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}
