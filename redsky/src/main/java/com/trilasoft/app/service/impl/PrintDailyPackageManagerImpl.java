package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PrintDailyPackageDao;
import com.trilasoft.app.model.PrintDailyPackage;
import com.trilasoft.app.service.PrintDailyPackageManager;

public class PrintDailyPackageManagerImpl extends GenericManagerImpl<PrintDailyPackage, Long> implements PrintDailyPackageManager {   
	
	PrintDailyPackageDao printDailyPackageDao;   
	  
	public PrintDailyPackageManagerImpl(PrintDailyPackageDao printDailyPackageDao) {   
        super(printDailyPackageDao);   
        this.printDailyPackageDao = printDailyPackageDao;   
    }

	public List<PrintDailyPackage> getByCorpId(String corpId,String tabId) {
		return printDailyPackageDao.getByCorpId(corpId, tabId);
	}
	
	public List<PrintDailyPackage> findChildByParentId(long id) {
		return printDailyPackageDao.findChildByParentId(id);
	}

	public void deleteByParentId(long id) {
		printDailyPackageDao.deleteByParentId(id);
	}

	public List findAutoCompleterList(String jrxmlName, String corpId) {
		return printDailyPackageDao.findAutoCompleterList(jrxmlName,corpId);
	}

	public List<PrintDailyPackage> searchPrintPackage(String formName,String corpId, String serviceType, String serviceMode,String military) {
		return printDailyPackageDao.searchPrintPackage( formName, corpId, serviceType, serviceMode, military);
	}

	public void deleteChild(String corpId) {
		printDailyPackageDao.deleteChild(corpId);
		
	}

	
}
