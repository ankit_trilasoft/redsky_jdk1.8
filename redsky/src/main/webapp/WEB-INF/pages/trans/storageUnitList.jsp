<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";
while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_OPS") || userRole.equalsIgnoreCase("ROLE_OPS_MGMT")){
		userRole=role.getName();
		break;
	}	
}%>
<head>
<title><fmt:message key="storageList.title" /></title>
<meta name="heading" content="<fmt:message key='storageList.heading'/>" />
<script type="text/javascript">
		function check(targetElement) {
		// for Access START
			if(targetElement.checked){
			      var userCheckStatus = document.forms['assignItemsForm'].elements['idCheck'].value;
			      if(userCheckStatus == ''){
				  	document.forms['assignItemsForm'].elements['idCheck'].value =targetElement.value ;
			      }else{
			       	var userCheckStatus=document.forms['assignItemsForm'].elements['idCheck'].value = userCheckStatus+','+targetElement.value;
			      	document.forms['assignItemsForm'].elements['idCheck'].value = userCheckStatus.replace( ',,' , ',' );
			      }
			   }
			   if(targetElement.checked==false){ 
			     var userCheckStatus = document.forms['assignItemsForm'].elements['idCheck'].value;
			     var userCheckStatus=document.forms['assignItemsForm'].elements['idCheck'].value = userCheckStatus.replace( targetElement.value , '' );
			     document.forms['assignItemsForm'].elements['idCheck'].value = userCheckStatus.replace( ',,' , ',' );
			  }
			if(document.forms['assignItemsForm'].elements['idCheck'].value==',' || document.forms['assignItemsForm'].elements['idCheck'].value==''){
				document.forms['assignItemsForm'].elements['forwardBtn'].disabled = true ;
			  	document.forms['assignItemsreForm'].elements['forwardBtn1'].disabled = true ;
			}else{
				document.forms['assignItemsForm'].elements['forwardBtn'].disabled = false ;
			  	document.forms['assignItemsreForm'].elements['forwardBtn1'].disabled = false ;
			}	
		// for Access END
		
		//for Release START
			if(targetElement.checked){
			      var userCheckStatus = document.forms['assignItemsreForm'].elements['idCheck'].value;
			      if(userCheckStatus == ''){
				  	document.forms['assignItemsreForm'].elements['idCheck'].value =targetElement.value ;
			      }else{
			       	var userCheckStatus=document.forms['assignItemsreForm'].elements['idCheck'].value = userCheckStatus+','+targetElement.value;
			      	document.forms['assignItemsreForm'].elements['idCheck'].value = userCheckStatus.replace( ',,' , ',' );
			      }
			   }
			   if(targetElement.checked==false){ 
			     var userCheckStatus = document.forms['assignItemsreForm'].elements['idCheck'].value;
			     var userCheckStatus=document.forms['assignItemsreForm'].elements['idCheck'].value = userCheckStatus.replace( targetElement.value , '' );
			     document.forms['assignItemsreForm'].elements['idCheck'].value = userCheckStatus.replace( ',,' , ',' );
			  }
			if(document.forms['assignItemsreForm'].elements['idCheck'].value==',' || document.forms['assignItemsreForm'].elements['idCheck'].value==''){
				document.forms['assignItemsForm'].elements['forwardBtn'].disabled = true ;
			  	document.forms['assignItemsreForm'].elements['forwardBtn1'].disabled = true ;
			}else{
				document.forms['assignItemsForm'].elements['forwardBtn'].disabled = false ;
			  	document.forms['assignItemsreForm'].elements['forwardBtn1'].disabled = false ;
			}
		//for Release END	
			  //document.forms['assignItemsForm'].elements['id'].value=targetElement.value;
			  //document.forms['assignItemsreForm'].elements['id'].value=targetElement.value;
			  //document.forms['assignItemsmoForm'].elements['id1'].value=targetElement.value;
			  //document.forms['assignItemsForm'].elements['locate'].value=document.forms['assignItemsForm'].elements['id'].value;
			  
			  //document.forms['assignItemsmoForm'].elements['forwardBtn2'].disabled = false ;
			  //document.forms['assignItemsForm'].elements['id1'].value=targetElement.value;
		  }
		  
		  function setFlagValue(){
		  	document.forms['assignItemsForm'].elements['hitFlag'].value = '1';
		  }
		  function setFlagValueRe(){
		  	document.forms['assignItemsreForm'].elements['hitFlag'].value = '1';
		  }
		  
		  function checkAll()
			{
			document.forms['assignItemsForm'].elements['idCheck'].value = "";
			document.forms['assignItemsreForm'].elements['idCheck'].value = "";
			var len = document.forms['storageListForm'].elements['dd'].length;
				for (i = 0; i < len; i++)
				{
				if(document.forms['storageListForm'].elements['dd'][i].disabled == false)
					{
					document.forms['storageListForm'].elements['dd'][i].checked = true ;
					check(document.forms['storageListForm'].elements['dd'][i]);
					}
				}
			}
			
			function uncheckAll()
			{
			var len = document.forms['storageListForm'].elements['dd'].length;
			for (i = 0; i < len; i++)
				{
				if(document.forms['storageListForm'].elements['dd'][i].disabled == false)
					{
					document.forms['storageListForm'].elements['dd'][i].checked = false ;
					check(document.forms['storageListForm'].elements['dd'][i]);
					}
				}
				
			}

function show(theTable)
{
     if (document.getElementById(theTable).style.display == 'none')
     {
          document.getElementById(theTable).style.display = 'block';
     }
}
function hide(theTable)
{
     if (document.getElementById(theTable).style.display == 'none')
     {
          document.getElementById(theTable).style.display = 'none';
     }
     else
     {
          document.getElementById(theTable).style.display = 'none';
     }
}

</script>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="storageListForm" action="searchStorages" method="post">
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<div id="Layer1" style="width:100%">
    <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="serviceOrder.id" />
			<div id="newmnav">
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <c:choose>
			     <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose> 
		      </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		      <c:choose>
			     
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose> 
		      </sec-auth:authComponent> 
			  <c:if test="${forwardingTabVal!='Y'}"> 
					<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
				</c:if>
				<c:if test="${forwardingTabVal=='Y'}">
					<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
				</c:if>
			  
			  <c:if test="${serviceOrder.job !='INT'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
			  <c:if test="${serviceOrder.job =='INT'}">
			    <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  </sec-auth:authComponent>
			  
			  			 			  		   <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>	
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket</span></a></li>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			    <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div><div style="padding-bottom:3px;"></div>
	
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
		<div id="newmnav">
		   			 <ul>
					  	<li><a href="editWorkTicketUpdate.html?id=${workTicket.id}"><span>Work Ticket</span></a></li>
					  	<li><a href="bookStorageLibraries.html?id=${workTicket.id} "><span>Storage List</span></a></li>
						<li><a href="searchUnitLocates.html?locationId=&type=&occupied=&warehouse=${workTicket.warehouse}&id=${workTicket.id}"><span>Add Storage</span></a></li>
                        <li id="newmnav1" style="background:#FFF"><a class="current"><span>Access/Release Storage</span></a></li>
					  	<li><a href="storageUnitMove.html?id=${workTicket.id}"><span>Move Storage Location</span></a></li>
					  	<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
					</ul>
		</div><div class="spn">&nbsp;</div>


	
	
	<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%">
	<tbody>
		<tr>
			<td>
				<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
				<table>
					<tbody>
						<tr>
							<td width="10px"></td>
							<td class="listwhite">Ticket Number</td>
							<td><s:textfield name="workTicket.ticket" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
							<td width="10px"></td>
							<td class="listwhite">Warehouse</td>
							<td><s:textfield name="workTicket.warehouse" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
						</tr>
					</tbody>
				</table>
				</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	<s:set name="storages" value="storages" scope="request" />
	<c:set var="checkAll">
		<input type="checkbox" name="allbox" onclick="checkAll(this.form)" style="margin: 0 0 0 4px" />
	</c:set>
	<div id="otabs" style="margin-bottom:2px;float:left;">
		<ul>
			<li><a class="current"><span>List</span></a></li>
		</ul>
	</div>
	<div id="chkAllButton"  class="listwhitetext" style="display:none;float:left;" >
			<input type="radio"  name="chk" onClick="checkAll()" /><strong>Check All</strong>
			<input type="radio"  name="chk" onClick="uncheckAll()"  /><strong>Uncheck All</strong>	
			</div>
	<div class="spnblk">&nbsp;</div> 
			<display:table name="storages" class="table" requestURI="" id="storageList" export="true" pagesize="50">
				<%if((userRole.equalsIgnoreCase("ROLE_OPS")) || (userRole.equalsIgnoreCase("ROLE_OPS_MGMT"))){ %>
				<c:if test="${storageList.releaseDate==null}">
					<display:column>
					<input type="checkbox"  name="dd" onclick="check(this)" value="${storageList.id}">
					</display:column>
				</c:if>
				<c:if test="${storageList.releaseDate!=null}">
					<display:column>
					<input type="checkbox" disabled="disabled" name="dd" onclick="check(this)" value="${storageList.id}">
					</display:column>
				</c:if>
				<%} %>
				<display:column property="ticket" sortable="true" titleKey="storage.ticket" />
				<display:column property="description" sortable="true" titleKey="storage.description" maxLength="15"/>
		 		<display:column property="locationId" sortable="true" titleKey="storage.locationId" />
		 		<display:column property="storageId" sortable="true" title="Storage" />
				<display:column property="releaseDate" sortable="true" titleKey="storage.releaseDate" style="width:100px"  format="{0,date,dd-MMM-yyyy}"/>
				<display:column property="containerId" sortable="true" titleKey="storage.containerId" />
				<display:column property="itemTag" sortable="true" titleKey="storage.itemTag"  style="width:120px"/>
				<display:column property="model" sortable="true" titleKey="storage.model" />
				<display:column property="serial" sortable="true" titleKey="storage.serial" />
				<display:column property="pieces" sortable="true" titleKey="storage.pieces" />				
				<display:column sortable="true" title="Volume">
				<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="0"
                  groupingUsed="true" value="${storageList.volume}" /></div>
                  </display:column>
				<display:column property="volUnit" sortable="true" title="Unit" />
				<display:setProperty name="paging.banner.item_name" value="storage" />
				<display:setProperty name="paging.banner.items_name" value="storage" />
				
				<display:setProperty name="export.excel.filename" value="Storage List.xls" />
				<display:setProperty name="export.csv.filename" value="Storage List.csv" />
				<display:setProperty name="export.pdf.filename" value="Storage List.pdf" />
			</display:table>

			</td>
		</tr>
	</tbody>
</table>
</div>   
	
	<c:set var="buttons">
	

	
	
	</c:set>
	<c:out value="${buttons}" escapeXml="false" />
	<s:hidden name="locate"  />
	
	
</s:form>
<table>
<tr>
	<td>
		<s:form id="assignItemsForm" action="editStorageUnitAc.html?id=${workTicket.id}" method="post">
			<s:hidden name="idCheck" /> 
			<s:hidden name="id1"  value="%{workTicket.id}" />
			<s:hidden name="ticket" value="%{workTicket.ticket}" />
			
			<input type="submit" class="cssbutton1" name="forwardBtn"  disabled value="Access" style="width:55px; height:25px" />
			<c:if test="${hitFlag == 1}" >
				<c:redirect url="/bookStorageLibraries.html?id=${workTicket.id}"  />
			</c:if>
			<c:if test="${hitFlag == 0}" >
				<c:redirect url="/storageUnit.html?id=${workTicket.id}"  />
			</c:if>
		</s:form>
	</td>
	<td>
		<s:form id="assignItemsreForm" action="editStorageUnitRe.html?id=${workTicket.id}" method="post">
			<s:hidden name="idCheck" /> 
			<s:hidden name="id1"  value="%{workTicket.id}" />
			<s:hidden name="ticket" value="%{workTicket.ticket}" />
			
			<input type="submit" class="cssbutton1" name="forwardBtn1"  disabled value="Release" style="width:55px; height:25px"  />
			<c:if test="${hitFlag == 1}" >
				<c:redirect url="/bookStorageLibraries.html?id=${workTicket.id}"  />
			</c:if>
			<c:if test="${hitFlag == 0}" >
				<c:redirect url="/storageUnit.html?id=${workTicket.id}"  />
			</c:if>
			
		</s:form>
	</td>
</tr>
</table>
<script type="text/javascript"> 
   try{
    var len = document.forms['storageListForm'].elements['dd'].length;
    }
    catch(e){}
    try{
    if(len>1)
    {
    	show('chkAllButton');
    }
    }
    catch(e){}
</script>