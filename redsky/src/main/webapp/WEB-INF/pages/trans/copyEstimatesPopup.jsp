<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="accountLineList.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountLineList.heading'/>"/> 
<style>
 span.pagelinks {display:block;font-size:0.85em;margin-bottom:5px;margin-top:-18px;padding:2px 0;text-align:right;width:106%;}
.tdheight-footer {border: 1px solid #e0e0e0;padding: 0;}
form .table{font-size:1em;}
#mainPopup {padding:10px;}
.s-wid-copy{width:22.5%;}
.copy-1 {width:0%;}
.copy-2 {width:0%;}
.copy-3 {width:0%;}
.copy-0 {width:12.5%;}
@media (min-width: 768px) and (max-width: 979px) {
.copy-0 {width:11.5%;}	
}
.copy-01 {width:12.7%;}
@media (min-width: 768px) and (max-width: 979px) {
.copy-01 {width:11.7%;}	
}
 </style>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
</head>
<s:set name="checkLHF" id="checkLHF"  value="false" scope="request" />
<s:set name="idForCheckLHF" id="idForCheckLHF" value="0" scope="request" />
<s:set name="copyEstimateAccountLineList" value="copyEstimateAccountLineList" scope="request"/>
<s:form name="saveCopyEstimation" method="post" action="saveCopyEstimation">
<s:hidden name="acctLineNumber" value=""/>
<s:hidden name="estimateExpenseLineId" value=""/>
<s:hidden name="estimateRevenueLineId" value=""/>
<s:hidden name="revisionExpenseLineId" value=""/>
<s:hidden name="revisionRevenueLineId" value=""/>
<s:hidden name="billing.billToCode" value="${billing.billToCode}"/>
<s:hidden name="billing.billToName" value="${billing.billToName}"/>
<s:hidden name="contractType"/>
<s:hidden name="distinctAccLineId"/>
<s:hidden name="pricingType"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.SetRegistrationNumber">
	<s:hidden name="setRegistrationNumber" value="YES"/>
	</configByCorp:fieldVisibility>
<c:set var="grossMarginActualization" value="N" />
<configByCorp:fieldVisibility componentId="component.field.accountLine.grossMarginActualization">
<c:set var="grossMarginActualization" value="Y"/>
</configByCorp:fieldVisibility>
<s:hidden name="grossMarginActualization" value="${grossMarginActualization}"/>	
</s:form>
<table cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
	<tr>
	<td class="s-wid-copy"></td>
	<td width="30">
		<input type="checkbox" id="selectAllEstimateExpense" onclick="selectAllEstimateExpense(this);checkEstimateExpenseId(this);"/>
	</td>
	<td align="left" class="listwhitetext copy-0"><b>All Est Exp</b></td>
	
	<td class="copy-1"></td>
	<td style="width: 25px;">
		<input type="checkbox" id="selectAllEstimateRevenue" onclick="selectAllEstimateRevenue(this);checkEstimateReveuneId(this);"/>
	</td>
	<td align="left" style="width:7%;" class="listwhitetext"><b>All Est Rev</b></td>
	
	<td class="copy-2"></td>
	<td style="width: 3%;">
		<input type="checkbox" id="selectAllRevisionExpense" onclick="selectAllRevisionExpense(this);checkRevisionExpenseId(this);"/>
	</td>
	<td align="left" class="listwhitetext copy-01"><b>All Rev Exp</b></td>
	
	<td class="copy-3"></td>
	<td style="width: 3%;">
		<input type="checkbox" id="selectAllRevisionRevenue" onclick="selectAllRevisionRevenue(this);checkRevisionRevenueId(this);"/>
	</td>
	<td align="left" class="listwhitetext"><b>All Rev Rev</b></td>
	
	</tr>
</table>
<s:form name="copyEstimation" method="post" action="">
<display:table name="copyEstimateAccountLineList" class="table" requestURI="" id="accountLineList" style="width:100%;!margin-top:1px;" defaultsort="1" pagesize="100" >   
 		<c:choose> 
     <c:when test="${serviceOrder.job !='' && serviceOrder.job!=null}">  
 		<c:if test="${billingFlag ==1 && billingContractFlag=='Y'}">                                                                 
			<display:column sortable="true" sortProperty="accountLineNumber" titleKey="accountLine.accountLineNumber"  style="width:40px; padding:0px;" >
			<a href="#" onclick="goToEditAccountLinePage(${serviceOrder.id},${accountLineList.id})"/>
			<c:out value="${accountLineList.accountLineNumber}" /></a>
			</display:column>
		</c:if>
		<c:if test="${billingFlag ==0 || billingContractFlag=='N'}">
			<display:column sortable="true" sortProperty="accountLineNumber" titleKey="accountLine.accountLineNumber"  style="width:50px;padding:0px;"><a href="#" onclick="showMessage();"/><c:out value="${accountLineList.accountLineNumber}" /></a></display:column>
		</c:if>
	</c:when>
	<c:otherwise>
           <display:column sortable="true" sortProperty="accountLineNumber" titleKey="accountLine.accountLineNumber"  style="width:50px;padding:0px;"><a href="#" onclick="showJobMessage();"/><c:out value="${accountLineList.accountLineNumber}" /></a></display:column>
    </c:otherwise>
</c:choose>				
		<%-- <display:column  sortable="true" title="Act"   style="width:60px"> 
		 <c:if test="${accountLineList.status}">
		  <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		 </c:if>
		 <c:if test="${accountLineList.status==false}">
		<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		 </c:if>
		 </display:column>--%>
		
		 <display:column  sortable="true" title="Charge Code" style="width:40px;padding:0px;">
		 <c:if test="${(checkLHF==false)}">
		 <c:if test="${(accountLineList.chargeCode=='LHF'|| accountLineList.chargeCode=='lhf') && (accountLineList.recInvoiceNumber =='') && (accountLineList.status)}">
		 <s:set name="checkLHF"  value="true"/>
		 <s:set name="idForCheckLHF" value="${accountLineList.id}"/>
		 </c:if>
		 </c:if>
		  <c:out value="${accountLineList.chargeCode}" />
		 </display:column>
		 <display:column headerClass="containeralign" sortable="true"  title="Entitle. Amount" style="width:60px; border-right:medium solid #d1d1d1; text-align:right;padding-right:0em;!important ">
	       <div align="right" style="margin:2px 0 0;float:right; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${accountLineList.entitlementAmount}" /></div>
        </display:column>
	    <display:column headerClass="containeralign"  sortable="true"  title="Estimate Expense" style="width:60px;padding-right:0em; !important">
	        	      
	        	      <input type="hidden" id="vendorCode${accountLineList.id}" name="vendorCode${accountLineList.id}" value="${accountLineList.vendorCode}"/>
	        	      <input type="hidden" id="chargeCode${accountLineList.id}" name="chargeCode${accountLineList.id}" value="${accountLineList.chargeCode}"/>
	        	      <input type="hidden" id="accountLineNumber${accountLineList.id}" name="accountLineNumber${accountLineList.id}" value="${accountLineList.accountLineNumber}"/>	        	      
	        	      <c:if test="${(accountLineList.estimateExpense!='0.00' && accountLineList.estimateExpense!='0.0000') && ((accountLineList.revisionExpense=='0.00' || accountLineList.revisionExpense=='0.0000') ||  (accountLineList.actualExpense=='0.00' || accountLineList.actualExpense=='0.0000') ) && (accountLineList.actualExpense=='0.00' || accountLineList.actualExpense=='0.0000')}">
	        	       <input type="checkbox" id="${accountLineList.id}" name="estimateList" class="estimateList"  onclick="pickDistinctId();chkForExpense('${accountLineList.accountLineNumber}:EstExp',this,'${accountLineList.payVatDescr}','${accountLineList.contract}','${accountLineList.chargeCode}')"/>
	                 </c:if>
	                 <c:if test="${(accountLineList.actualExpense!='0.00' && accountLineList.actualExpense!='0.0000') || ((accountLineList.estimateExpense=='0.00' || accountLineList.estimateExpense=='0.0000') && (accountLineList.revisionExpense!='0.00' && accountLineList.revisionExpense!='0.0000') && (accountLineList.actualExpense!='0.00' && accountLineList.actualExpense!='0.0000')) || ((accountLineList.estimateExpense!='0.00' && accountLineList.estimateExpense!='0.0000') && (accountLineList.revisionExpense!='0.00' && accountLineList.revisionExpense!='0.0000') && (accountLineList.actualExpense!='0.00' && accountLineList.actualExpense!='0.0000')) ||(accountLineList.estimateExpense=='0.00' || accountLineList.estimateExpense=='0.0000') }">
	        	       <input type="checkbox"    disabled="disabled"  />
	                 </c:if>
	        
	        <div align="right" style="margin:2px 0 0;float:right; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${accountLineList.estimateExpense}" /></div>
        </display:column>
        <display:column  headerClass="containeralign" sortable="true"  title="Mk %" style="width:40px;border-right:medium solid #d1d1d1;padding-right:0em; !important">
         <c:if test="${accountLineList.estimatePassPercentage==0}">
	      <c:out value=""></c:out>
	      </c:if>
	      <c:if test="${accountLineList.estimatePassPercentage!=0}">
	      <div align="right" style="margin:0px;float:right;color:#003366; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${accountLineList.estimatePassPercentage}" /></div></c:if>
          </display:column>
	    <display:column  headerClass="containeralign" sortable="true"  title="Estimate Revenue" style="width:60px;border-right:medium solid #d1d1d1;padding-right:0em; !important">
		  
		   
		   <c:if test="${(accountLineList.estimateRevenueAmount!='0.00' && accountLineList.estimateRevenueAmount!='0.0000') && ((accountLineList.revisionRevenueAmount=='0.00' || accountLineList.revisionRevenueAmount=='0.0000') || (accountLineList.actualRevenue=='0.00' || accountLineList.actualRevenue=='0.0000')) && (accountLineList.actualRevenue=='0.00' || accountLineList.actualRevenue=='0.0000')}">
		    <input type="checkbox" id="${accountLineList.id}" name="estimateRevenueList" class="estimateRevenueList"  onclick="pickDistinctId();chkForRevenue('${accountLineList.accountLineNumber}:EstRev',this,'${accountLineList.recVatDescr}','${accountLineList.contract}','${accountLineList.chargeCode}')"/>
		  </c:if>
		  <c:if test="${(accountLineList.actualRevenue!='0.00' && accountLineList.actualRevenue!='0.0000') || ((accountLineList.estimateRevenueAmount=='0.00' || accountLineList.estimateRevenueAmount=='0.0000') && (accountLineList.revisionRevenueAmount!='0.00' && accountLineList.revisionRevenueAmount!='0.0000') && (accountLineList.actualRevenue!='0.00' && accountLineList.actualRevenue!='0.0000')) || ((accountLineList.estimateRevenueAmount!='0.00' && accountLineList.estimateRevenueAmount!='0.0000') && (accountLineList.revisionRevenueAmount!='0.00' && accountLineList.revisionRevenueAmount!='0.0000') && (accountLineList.actualRevenue!='0.00' && accountLineList.actualRevenue!='0.0000'))|| (accountLineList.estimateRevenueAmount=='0.00' || accountLineList.estimateRevenueAmount=='0.0000')}">
		    <input type="checkbox"    disabled="disabled"/>
		  </c:if>		  
		  
		  <div align="right" style="margin:2px 0 0;float:right; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="0" 
                  groupingUsed="true" value="${accountLineList.estimateRevenueAmount}" /></div>
                  
                  
        </display:column>
		<display:column headerClass="containeralign"  sortable="true"  title="Revision Expense" style="width:60px;padding-right:0em;!important">
		   <input type="hidden" id="revisionExpense${accountLineList.id}" name="revisionExpense${accountLineList.id}" value="${accountLineList.revisionExpense}"/>
		  <c:if test="${((accountLineList.revisionExpense!='0.00' && accountLineList.revisionExpense!='0.0000') && (accountLineList.actualExpense=='0.00' || accountLineList.actualExpense=='0.0000'))}">
	        	       <input type="checkbox" id="${accountLineList.id}" name="revisionList" class="revisionList" onclick="pickDistinctId();chkForExpense('${accountLineList.accountLineNumber}:RevExp',this,'${accountLineList.payVatDescr}','${accountLineList.contract}','${accountLineList.chargeCode}')"/>
	                 </c:if>
	                 <c:if test="${(accountLineList.actualExpense!='0.00' && accountLineList.actualExpense!='0.0000') || ((accountLineList.revisionExpense!='0.00' && accountLineList.revisionExpense!='0.0000') && (accountLineList.actualExpense!='0.00' && accountLineList.actualExpense!='0.0000') ) ||((accountLineList.revisionExpense=='0.00' || accountLineList.revisionExpense=='0.0000') && (accountLineList.actualExpense!='0.00' && accountLineList.actualExpense!='0.0000')) || (accountLineList.revisionExpense=='0.00' || accountLineList.revisionExpense=='0.0000')}">
	        	       <input type="checkbox"    disabled="disabled"  />
	                 </c:if>
		  
		  <div align="right" style="margin:0px;float:right; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${accountLineList.revisionExpense}" /></div>
        </display:column>
        <display:column   sortable="true"  headerClass="containeralign" title="Mk %" style="width:40px;border-right:medium solid #d1d1d1;padding-right:0em; !important">
	      <c:if test="${accountLineList.revisionPassPercentage==0}">
	      <c:out value=""></c:out>
	      </c:if>
	       <c:if test="${accountLineList.revisionPassPercentage!=0}">
	       <div align="right" style="margin:0px;color:#003366;float:right; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${accountLineList.revisionPassPercentage}" /></div></c:if>
        </display:column>
		<display:column headerClass="containeralign"  sortable="true"  title="Revision Revenue" style="width:60px;border-right:medium solid #d1d1d1;padding-right:0em;!important ">
		   
		   <c:if test="${((accountLineList.revisionRevenueAmount!='0.00' && accountLineList.revisionRevenueAmount!='0.0000') && (accountLineList.actualRevenue=='0.00' || accountLineList.actualRevenue=='0.0000'))}">
		    <input type="checkbox" id="${accountLineList.id}" name="revisionRevenueList" class="revisionRevenueList"  onclick="pickDistinctId();chkForRevenue('${accountLineList.accountLineNumber}:RevRev',this,'${accountLineList.recVatDescr}','${accountLineList.contract}','${accountLineList.chargeCode}')"/>
		  </c:if>
		  <c:if test="${(accountLineList.actualRevenue!='0.00' && accountLineList.actualRevenue!='0.0000') || ((accountLineList.revisionRevenueAmount!='0.00' && accountLineList.revisionRevenueAmount!='0.0000') && (accountLineList.actualRevenue!='0.00' && accountLineList.actualRevenue!='0.0000'))|| ((accountLineList.revisionRevenueAmount=='0.00' ||accountLineList.revisionRevenueAmount=='0.0000' ) && (accountLineList.actualRevenue!='0.00' && accountLineList.actualRevenue!='0.0000')) || (accountLineList.revisionRevenueAmount=='0.00' || accountLineList.revisionRevenueAmount=='0.0000') }">
		    <input type="checkbox"    disabled="disabled"/>
		  </c:if>		
		  
		   <div align="right" style="margin:0px;float:right; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${accountLineList.revisionRevenueAmount}" /></div>
        </display:column>
        <display:column headerClass="containeralign"  sortable="true"  title="Actual Expense" style="width:60px;border-right:medium solid #d1d1d1; text-align:right;padding-right:0em;!important">
		  <div align="right" style="margin:0px;float:right; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${accountLineList.actualExpense}" /></div>
        </display:column>
        
		<display:column headerClass="containeralign"  sortable="true"  title="Actual Revenue" style="width:60px; border-right:medium solid #d1d1d1;text-align:right;padding-right:0em;!important ">
		   <div align="right" style="margin:0px;float:right; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${accountLineList.actualRevenue}" /></div>
        </display:column>        
        <display:column property="estimateVendorName" sortable="true" title="Vendor Name" maxLength="9"  style="width:80px; padding:0px;"/>
        </display:table> 
     </s:form>
     
     <c:if test="${!serviceOrder.revised}">
     <input type="button" class="cssbuttonA" style="width:120px;"  name="Copy Revision"  value="Copy To Revision" onclick="checkEstimatehecBox();" />
     </c:if>
     <c:if test="${serviceOrder.revised}">
     <input type="button" class="cssbuttonA" style="width:120px;"  name="Copy Revision"  value="Copy To Revision" onclick=" " disabled="disabled"/>
     </c:if>
     <c:if test="${grossMarginActualization =='Y' }">
     <c:if test="${(serviceOrder.estimatedGrossMarginPercentage>=10 && serviceOrder.revisedGrossMarginPercentage>=10) || (billing.approvedBy !=null && billing.approvedBy !='' && not empty billing.dateApproved)}">
     <input type="button" class="cssbuttonA" style="width:100px;"  name="Copy Actual"  value="Copy To Actual" onclick="checkActualBox();" />
     </c:if>
     </c:if>
     <c:if test="${grossMarginActualization !='Y' }">
     <input type="button" class="cssbuttonA" style="width:100px;"  name="Copy Actual"  value="Copy To Actual" onclick="checkActualBox();" />
     </c:if>
     <input type="button" class="cssbuttonA" style="width:120px;"    value="Clear check boxes" onclick="clearcheckboxes()" />
     <input type="button" class="cssbuttonA" style="width:100px;"    value="Close Screen" onclick="javascript:window.close()" />    
     
<script type="text/javascript">

function chkVat(){
	  var vatFlag='${systemDefaultVatCalculation}';
	  var acctlineNum=document.forms['saveCopyEstimation'].elements['acctLineNumber'].value;
	  var accNum="";
	  var item=acctlineNum.split("~");
	  for(var t=0;t<item.length;t++){
		  if(accNum!=null && accNum!=''){
			  if(accNum.indexOf(item[t].split(":")[0])<0){
				  accNum=accNum+","+item[t].split(":")[0];
			  }
		  }else{
			  accNum=item[t].split(":")[0];
		  }
	  }
	   return accNum;
}


  <c:if test="${hitFlag=='1'}">
    pick();
  </c:if>
  function pick() {	
	   var pricingType=document.forms['saveCopyEstimation'].elements['pricingType'].value;
	   if(pricingType!=null && pricingType !=undefined && pricingType !='' && pricingType=='Y'){
		   parent.window.opener.findSoAllPricing();
	   }else{
		   parent.window.opener.document.location.reload();
	   }
  		window.close();
	   } 
	
 	function clearcheckboxes(){
	if(document.forms['copyEstimation'].estimateList!=undefined){
	if(document.forms['copyEstimation'].estimateList.length!=undefined){
	      for (i=0; i<document.forms['copyEstimation'].estimateList.length; i++){
	          document.forms['copyEstimation'].estimateList[i].checked='';
	       }	
	    }else{
	    document.forms['copyEstimation'].estimateList.checked=''
	    }
	  }
	    if(document.forms['copyEstimation'].estimateRevenueList!=undefined){
	    if(document.forms['copyEstimation'].estimateRevenueList.length!=undefined){
	      for (i=0; i<document.forms['copyEstimation'].estimateRevenueList.length; i++){
	          document.forms['copyEstimation'].estimateRevenueList[i].checked='';	            
	      }
	     }else{
	      document.forms['copyEstimation'].estimateRevenueList.checked='';
	    }
	    }
	    if(document.forms['copyEstimation'].revisionList!=undefined){
		    if(document.forms['copyEstimation'].revisionList.length!=undefined){
		      for (i=0; i<document.forms['copyEstimation'].revisionList.length; i++){
		          document.forms['copyEstimation'].revisionList[i].checked='';	            
		      }
		     }else{
		      document.forms['copyEstimation'].revisionList.checked='';
		    }
		    } 
	    if(document.forms['copyEstimation'].revisionRevenueList!=undefined){
		    if(document.forms['copyEstimation'].revisionRevenueList.length!=undefined){
		      for (i=0; i<document.forms['copyEstimation'].revisionRevenueList.length; i++){
		          document.forms['copyEstimation'].revisionRevenueList[i].checked='';	            
		      }
		     }else{
		      document.forms['copyEstimation'].revisionRevenueList.checked='';
		    }
		    }
	    document.forms['saveCopyEstimation'].elements['acctLineNumber'].value="";
	    document.forms['saveCopyEstimation'].elements['distinctAccLineId'].value="";
	    document.getElementById('selectAllEstimateExpense').checked=false;
	    document.getElementById('selectAllEstimateRevenue').checked=false;
	    document.getElementById('selectAllRevisionExpense').checked=false;
	    document.getElementById('selectAllRevisionRevenue').checked=false;
	}
	
  function checkEstimatehecBox(){
      var estimateId="";
      var estimateRevenueListId="";
      var estimateListhitflag='0';
       var estimateRevenueListhitflag='0';
      if(document.forms['copyEstimation'].estimateList!=undefined){
       if(document.forms['copyEstimation'].estimateList.length!=undefined){
	      for (i=0; i<document.forms['copyEstimation'].estimateList.length; i++){	        
	          if(document.forms['copyEstimation'].estimateList[i].checked==true){  
	             if(estimateId==''){
	                   estimateId=document.forms['copyEstimation'].estimateList[i].id;
	                 }else{
	                  estimateId= estimateId+"#"+document.forms['copyEstimation'].estimateList[i].id;
	                   }
	            }
	       }	
	     }else{
	         if(document.forms['copyEstimation'].estimateList.checked==true){
	           estimateId=document.forms['copyEstimation'].estimateList.id;
	            }
	         }	        
	     }else{
	      estimateListhitflag='1';
	     }
	     if(document.forms['copyEstimation'].estimateRevenueList!=undefined){
	      if(document.forms['copyEstimation'].estimateRevenueList.length!=undefined){
	          for (i=0; i<document.forms['copyEstimation'].estimateRevenueList.length; i++){
	          if (document.forms['copyEstimation'].estimateRevenueList[i].checked==true){  
	             if(estimateRevenueListId==''){
	             estimateRevenueListId=document.forms['copyEstimation'].estimateRevenueList[i].id;
	                 }else{
	                  estimateRevenueListId= estimateRevenueListId+"#"+document.forms['copyEstimation'].estimateRevenueList[i].id;
	                   }
	            }
	     }
	     }else{
	         if(document.forms['copyEstimation'].estimateRevenueList.checked==true){
	         estimateRevenueListId=document.forms['copyEstimation'].estimateRevenueList.id;
	         }
	       }
	       }else{
	         estimateRevenueListhitflag='1';
	       }
	     document.forms['saveCopyEstimation'].elements['estimateExpenseLineId'].value=estimateId;
	     document.forms['saveCopyEstimation'].elements['estimateRevenueLineId'].value=estimateRevenueListId;    
	     if(estimateListhitflag!='1' || estimateRevenueListhitflag!='1'){
	       if(estimateId!="" || estimateRevenueListId!=""){
	    	   var checkVendorCodeForRevisionExpense=checkVendorCodeForCopytoRevision();
	    	   if(checkVendorCodeForRevisionExpense.trim()!="")
	    	   {
	    	  	alert(checkVendorCodeForRevisionExpense);
	    	   }
	    	   else{
	          document.forms['saveCopyEstimation'].submit();
	    	   }
	          }else{alert("Nothing is checked for copy");} 
	     }else{
	        alert("Nothing is checked for copy");
	     }	       	     
	 }
  function checkActualBox(){	  
	  var revisionId="";
      var revisionRevenueListId="";
      var estimateExpenseLineId = ""
      var estimateRevenueLineId = "";
      var revisionListhitflag='0';
      var revisionListhitflag1='0';
      var revisionRevenueListhitflag='0';
      var revisionRevenueListhitflag1='0';
      var contractType="${contractType}";
      if(document.forms['copyEstimation'].estimateList!=undefined){    	   
    	    if(document.forms['copyEstimation'].estimateList.length!=undefined){
    	   	   
	      for (i=0; i<document.forms['copyEstimation'].estimateList.length; i++){
	    	    	        
	          if(document.forms['copyEstimation'].estimateList[i].checked==true){
	      	        	  
	             if(estimateExpenseLineId==''){
	    
	            	 estimateExpenseLineId=document.forms['copyEstimation'].estimateList[i].id;
	            	  }else{
	    
	            		  estimateExpenseLineId= estimateExpenseLineId+"#"+document.forms['copyEstimation'].estimateList[i].id;
	                	 }
	            }
	       }	
	     }else{
	         if(document.forms['copyEstimation'].estimateList.checked==true){
	    
	        	 estimateExpenseLineId=document.forms['copyEstimation'].estimateList.id;
	            }
	         }	        
	     }else{
	    	 revisionListhitflag='1';
	     }
	     if(document.forms['copyEstimation'].estimateRevenueList!=undefined){		    	   	 
	      if(document.forms['copyEstimation'].estimateRevenueList.length!=undefined){	    	  
	          for (i=0; i<document.forms['copyEstimation'].estimateRevenueList.length; i++){	        	  
	          if (document.forms['copyEstimation'].estimateRevenueList[i].checked==true){	        	  
	             if(estimateRevenueLineId==''){
	            	 estimateRevenueLineId=document.forms['copyEstimation'].estimateRevenueList[i].id;	            	 
	                 }else{
	                	 estimateRevenueLineId= estimateRevenueLineId+"#"+document.forms['copyEstimation'].estimateRevenueList[i].id;
	                   }
	            }
	     }
	     }else{
	         if(document.forms['copyEstimation'].estimateRevenueList.checked==true){
	        	 estimateRevenueLineId=document.forms['copyEstimation'].estimateRevenueList.id;
	         }
	       }
	       }else{
	    	   revisionRevenueListhitflag='1';
	       } 
      if(document.forms['copyEstimation'].revisionList!=undefined){
    	
       if(document.forms['copyEstimation'].revisionList.length!=undefined){
    	   
	      for (i=0; i<document.forms['copyEstimation'].revisionList.length; i++){
	    		        
	          if(document.forms['copyEstimation'].revisionList[i].checked==true){  
	             if(revisionId==''){
	        
	                   revisionId=document.forms['copyEstimation'].revisionList[i].id;
	                 }else{
	        
	                 revisionId= revisionId+"#"+document.forms['copyEstimation'].revisionList[i].id;
	                   }
	            }
	       }	
	     }else{
	         if(document.forms['copyEstimation'].revisionList.checked==true){
	        
	           revisionId=document.forms['copyEstimation'].revisionList.id;
	            }
	         }	        
	     }else{
	      revisionListhitflag1='1';
	     }
	     if(document.forms['copyEstimation'].revisionRevenueList!=undefined){
	    	
	      if(document.forms['copyEstimation'].revisionRevenueList.length!=undefined){
	    	
	          for (i=0; i<document.forms['copyEstimation'].revisionRevenueList.length; i++){
	        
	          if (document.forms['copyEstimation'].revisionRevenueList[i].checked==true){
	          
	             if(revisionRevenueListId==''){
	             revisionRevenueListId=document.forms['copyEstimation'].revisionRevenueList[i].id;
	                 }else{
	                  revisionRevenueListId= revisionRevenueListId+"#"+document.forms['copyEstimation'].revisionRevenueList[i].id;
	                   }
	            }
	     }
	     }else{
	         if(document.forms['copyEstimation'].revisionRevenueList.checked==true){
	         revisionRevenueListId=document.forms['copyEstimation'].revisionRevenueList.id;
	         }
	       }
	       }else{
	    	   revisionRevenueListhitflag1='1';
	       }
	     
	     document.forms['saveCopyEstimation'].elements['estimateExpenseLineId'].value=estimateExpenseLineId;
	     document.forms['saveCopyEstimation'].elements['estimateRevenueLineId'].value=estimateRevenueLineId; 
	     document.forms['saveCopyEstimation'].elements['revisionExpenseLineId'].value=revisionId;
	     document.forms['saveCopyEstimation'].elements['revisionRevenueLineId'].value=revisionRevenueListId;   
      	 if(revisionListhitflag1!='1' || revisionRevenueListhitflag1!='1' || revisionListhitflag!='1' || revisionRevenueListhitflag!='1'){
         
	       if(revisionId!="" || revisionRevenueListId!="" || estimateExpenseLineId!="" || estimateRevenueLineId!=""){
	    	   if((estimateRevenueLineId!="")||(revisionRevenueListId!=""))
	    	   {	    		       		       
				  <c:if test="${billing.billToCode!='' && billing.billToCode!=null}">
			       var accNum=chkVat();
			       var checkVendor=checkVendorCodeForExpence();
			       var checkChargeCode=checkChargeCodeForRevenue();
		    	   if(accNum!=null && accNum!='' && contractType!=null && contractType!=undefined && (contractType==true || contractType=='true')){    		   
		    		   var agree = confirm("Vat is missing in ("+accNum+") account lines for copy actual. Click ok to proceed ");
		               if(agree)
		                {
		            	   if(checkVendor.trim()!=""){
					    	   alert(checkVendor);
				    	   }else if (checkChargeCode.trim()!=""){
				    		   alert(checkChargeCode);
				    	   }
				    	   else{
				    	  	document.forms['saveCopyEstimation'].action ='saveCopyRevision.html';
				          	document.forms['saveCopyEstimation'].submit();
					       }
		                }
		    	   }else if(checkVendor.trim()!=""){
			    	   alert(checkVendor);
		    	   }else if (checkChargeCode.trim()!=""){
		    		   alert(checkChargeCode);
		    	   }else{
		    	  	document.forms['saveCopyEstimation'].action ='saveCopyRevision.html';
		          	document.forms['saveCopyEstimation'].submit();
			       }
	        	  </c:if>
	    	   	  <c:if test="${billing.billToCode=='' || billing.billToCode==null}">
	    	   			alert("No Primary Bill-to-Code has been selected in the Billing section so you cannot copy to Actuals."); 	    		       
	          	  </c:if>

	    	   }else{
	    		   var accNum=chkVat();
	    		   var checkVendor=checkVendorCodeForExpence();	
	    		   var checkChargeCode=checkChargeCodeForRevenue();
		    	   if(accNum!=null && accNum!='' && contractType!=null && contractType!=undefined && (contractType==true || contractType=='true')){    		   
		    		   var agree = confirm("Vat is missing in ("+accNum+") account lines for copy actual. Click ok to proceed ");
		    		   if(agree)
		                {
		    			   if(checkVendor.trim()!=""){
					    	   alert(checkVendor);
				    	   }else if (checkChargeCode.trim()!=""){
				    		   alert(checkChargeCode);
				    	   }else{
		 	    		   document.forms['saveCopyEstimation'].action ='saveCopyRevision.html';
			          	   document.forms['saveCopyEstimation'].submit();
				    	   }  
		                }
		    	   }else if(checkVendor.trim()!=""){
			    	   alert(checkVendor);
		    	   }else if (checkChargeCode.trim()!=""){
		    		   alert(checkChargeCode);
		    	   }else{
 	    		   document.forms['saveCopyEstimation'].action ='saveCopyRevision.html';
	          	   document.forms['saveCopyEstimation'].submit();
		    	   }
	    	   }
	          }else{alert("Nothing is checked for copy");} 
	     }else{
	        alert("Nothing is checked for copy");
	     }
  } 
  
  function pickDistinctId()
  {
   var newIds='';
   if(document.forms['copyEstimation'].estimateList!=undefined){	   
	   if(document.forms['copyEstimation'].estimateList.length!=undefined){
	      for (i=0; i<document.forms['copyEstimation'].estimateList.length; i++){
		 	    var targetId=document.forms['copyEstimation'].estimateList[i].id;
		 	   if(document.forms['copyEstimation'].estimateList[i].checked==true){			 	    
		 	    if(newIds==''){
		 		    newIds=targetId;
		 		    }else{
		 		    	if(newIds.indexOf(targetId)<0){
		 			    	newIds=newIds + ',' + targetId;
		 		    	}
		 		    }
		 	   }
	       }
	   }else{
	 	    var targetId=document.forms['copyEstimation'].estimateList.id;
		 	   if(document.forms['copyEstimation'].estimateList.checked==true){			 	    
		 	    if(newIds==''){
		 		    newIds=targetId;
		 		    }else{
		 		    	if(newIds.indexOf(targetId)<0){
		 			    	newIds=newIds + ',' + targetId;
		 		    	}
		 		    }
		 	   }		   
	   }
	}
	if(document.forms['copyEstimation'].estimateRevenueList!=undefined){
		if(document.forms['copyEstimation'].estimateRevenueList.length!=undefined){
	      for (i=0; i<document.forms['copyEstimation'].estimateRevenueList.length; i++){
		 	    var targetId=document.forms['copyEstimation'].estimateRevenueList[i].id;
		 	   if(document.forms['copyEstimation'].estimateRevenueList[i].checked==true){
		 	    if(newIds==''){
		 		    newIds=targetId;
		 		    }else{
		 		    	if(newIds.indexOf(targetId)<0){
		 			    	newIds=newIds + ',' + targetId;
		 		    	}
		 		    }
	 		    }
	       }
		}else{
	 	    var targetId=document.forms['copyEstimation'].estimateRevenueList.id;
		 	   if(document.forms['copyEstimation'].estimateRevenueList.checked==true){			 	    
		 	    if(newIds==''){
		 		    newIds=targetId;
		 		    }else{
		 		    	if(newIds.indexOf(targetId)<0){
		 			    	newIds=newIds + ',' + targetId;
		 		    	}
		 		    }
		 	   }		   
			
		}
	}	  
   
	if(document.forms['copyEstimation'].revisionList!=undefined){
		if(document.forms['copyEstimation'].revisionList.length!=undefined){
			for (i=0; i<document.forms['copyEstimation'].revisionList.length; i++){
			 	   var targetId=document.forms['copyEstimation'].revisionList[i].id;
			 	   if(document.forms['copyEstimation'].revisionList[i].checked==true){			 	    
			 	    if(newIds==''){
			 		    newIds=targetId;
			 		    }else{
			 		    	if(newIds.indexOf(targetId)<0){
			 			    	newIds=newIds + ',' + targetId;
			 		    	}
			 		    }
			 	   }
		       }
		}else{
	 	    var targetId=document.forms['copyEstimation'].revisionList.id;
		 	   if(document.forms['copyEstimation'].revisionList.checked==true){			 	    
		 	    if(newIds==''){
		 		    newIds=targetId;
		 		    }else{
		 		    	if(newIds.indexOf(targetId)<0){
		 			    	newIds=newIds + ',' + targetId;
		 		    	}
		 		    }
		 	   }		   
		}	
	  }
	if(document.forms['copyEstimation'].revisionRevenueList!=undefined){
		if(document.forms['copyEstimation'].revisionRevenueList.length!=undefined){
		      for (i=0; i<document.forms['copyEstimation'].revisionRevenueList.length; i++){
			 	    var targetId=document.forms['copyEstimation'].revisionRevenueList[i].id;
			 	   if(document.forms['copyEstimation'].revisionRevenueList[i].checked==true){	
			 	    if(newIds==''){
			 		    newIds=targetId;
			 		    }else{
			 		    	if(newIds.indexOf(targetId)<0){
			 			    	newIds=newIds + ',' + targetId;
			 		    	}
			 		    }
			 	   }
		       }	
		}else{
	 	    var targetId=document.forms['copyEstimation'].revisionRevenueList.id;
		 	   if(document.forms['copyEstimation'].revisionRevenueList.checked==true){			 	    
		 	    if(newIds==''){
		 		    newIds=targetId;
		 		    }else{
		 		    	if(newIds.indexOf(targetId)<0){
		 			    	newIds=newIds + ',' + targetId;
		 		    	}
		 		    }
		 	   }		   
		}
	  }
	 document.forms['saveCopyEstimation'].elements['distinctAccLineId'].value=newIds;
  }
  function checkVatExclude(contract,chargeCode){
	  var temp=contract+"~"+chargeCode;
	  var rec='';
		<c:forEach var="entry" items="${chargeValExcludeMap}">
		if('${entry.key}'==temp.trim()){
			rec='${entry.value}';
		}
	</c:forEach>
	return rec;
  }
  function chkForExpense(lineNum,ischk,vatDesc,contract,chargeCode){
	  var vatFlag='${systemDefaultVatCalculation}';
	 // alert(vatFlag);
	  var acctlineNum=document.forms['saveCopyEstimation'].elements['acctLineNumber'].value;
	  var vatExclude=checkVatExclude(contract,chargeCode);
	  var tmp="";
	  if(vatFlag=='true' && vatExclude!='T'){
		  if(ischk.checked){
			  if(vatDesc==null || vatDesc==''){
			 if(acctlineNum!=null && acctlineNum!='') {
				 tmp=acctlineNum+"~"+lineNum;
				 document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=tmp;
			 }else{
				 tmp=lineNum;
				 document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=tmp;
			 }

			  }
			  }else{
				  var acctlineNums=acctlineNum.split("~");
				  var newVal="";
				  for(var i=0;i<acctlineNums.length;i++){
					  if(acctlineNums[i]!=lineNum){
						  if(newVal!=''){
							  newVal=newVal+"~"+acctlineNums[i];
						  }else{
							  newVal=acctlineNums[i];
						  }
					  }
				  }

				  document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=newVal;	  
	  }
  }
  }
  function chkForRevenue(lineNum,ischk,vatDesc,contract,chargeCode){
	  var vatFlag='${systemDefaultVatCalculation}';
	  var acctlineNum=document.forms['saveCopyEstimation'].elements['acctLineNumber'].value;
	  var vatExclude=checkVatExclude(contract,chargeCode);
	  var tmp="";
	  if(vatFlag=='true' && vatExclude!='T'){
		  if(ischk.checked){
			  if(vatDesc==null || vatDesc==''){
			 if(acctlineNum!=null && acctlineNum!='') {
				 tmp=acctlineNum+"~"+lineNum;
				 document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=tmp;
			 }else{
				 tmp=lineNum;
				 document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=tmp;
			 }

			  }
			  }else{
				  var acctlineNums=acctlineNum.split("~");
				  var newVal="";
				  for(var i=0;i<acctlineNums.length;i++){
					  if(acctlineNums[i]!=lineNum){
						  if(newVal!=''){
							  newVal=newVal+"~"+acctlineNums[i];
						  }else{
							  newVal=acctlineNums[i];
						  }
					  }
				  }

				  document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=newVal;	  
	  }
  }
  }
  function checkVendorCodeForExpence(){

		try{  
			var newIds='';
		   if(document.forms['copyEstimation'].estimateList!=undefined){	   
			   if(document.forms['copyEstimation'].estimateList.length!=undefined){
			      for (i=0; i<document.forms['copyEstimation'].estimateList.length; i++){
				 	    var targetId=document.forms['copyEstimation'].estimateList[i].id;
				 	    var vendorCode=document.getElementById('vendorCode'+targetId).value;
				 	    var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
				 	   if((document.forms['copyEstimation'].estimateList[i].checked==true)&&(vendorCode==null || vendorCode==undefined || vendorCode.trim()=='')){			 	    
				 	    if(newIds==''){
				 		    newIds=accountLineNumber;
				 		    }else{
				 		    	if(newIds.indexOf(accountLineNumber)<0){
				 			    	newIds=newIds + ',' + accountLineNumber;
				 		    	}
				 		    }
				 	   }
			       }
			   }else{
			 	    var targetId=document.forms['copyEstimation'].estimateList.id;
			 	    var vendorCode=document.getElementById('vendorCode'+targetId).value;
			 	   var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
				 	   if((document.forms['copyEstimation'].estimateList.checked==true)&&(vendorCode==null || vendorCode==undefined || vendorCode.trim()=='')){			 	    
				 	    if(newIds==''){
				 		    newIds=accountLineNumber;
				 		    }else{
				 		    	if(newIds.indexOf(accountLineNumber)<0){
				 			    	newIds=newIds + ',' + accountLineNumber;
				 		    	}
				 		    }
				 	   }		   
			   }
			}
			if(document.forms['copyEstimation'].revisionList!=undefined){
				if(document.forms['copyEstimation'].revisionList.length!=undefined){
					for (i=0; i<document.forms['copyEstimation'].revisionList.length; i++){
					 	   var targetId=document.forms['copyEstimation'].revisionList[i].id;
					 	   var vendorCode=document.getElementById('vendorCode'+targetId).value;
					 	  var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
					 	   if((document.forms['copyEstimation'].revisionList[i].checked==true)&&(vendorCode==null || vendorCode==undefined || vendorCode.trim()=='')){			 	    
					 	    if(newIds==''){
					 		    newIds=accountLineNumber;
					 		    }else{
					 		    	if(newIds.indexOf(accountLineNumber)<0){
					 			    	newIds=newIds + ',' + accountLineNumber;
					 		    	}
					 		    }
					 	   }
				       }
				}else{
			 	    var targetId=document.forms['copyEstimation'].revisionList.id;
			 	    var vendorCode=document.getElementById('vendorCode'+targetId).value;
			 	   var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
				 	   if((document.forms['copyEstimation'].revisionList.checked==true)&&(vendorCode==null || vendorCode==undefined || vendorCode.trim()=='')){			 	    
				 	    if(newIds==''){
				 		    newIds=accountLineNumber;
				 		    }else{
				 		    	if(newIds.indexOf(accountLineNumber)<0){
				 			    	newIds=newIds + ',' + accountLineNumber;
				 		    	}
				 		    }
				 	   }		   
				}	
			  }
			  if(newIds!=''){
				  var temp=newIds.split(",");
				  newIds="";
				  for(var u=0;u<temp.length;u++){
					  newIds += "Vendor Code Is Missing for Line #"+temp[u]+"\n";
				  }
			  }
		}catch(e){}
			  return newIds;
		}
  
  
  function checkChargeCodeForRevenue(){

		try{  
			
			var newIdsChargeCode='';
			
			   if(document.forms['copyEstimation'].estimateList!=undefined){	   
				   if(document.forms['copyEstimation'].estimateList.length!=undefined){
				      for (i=0; i<document.forms['copyEstimation'].estimateList.length; i++){
					 	    var targetId=document.forms['copyEstimation'].estimateList[i].id;
					 	    var chargeCode=document.getElementById('chargeCode'+targetId).value;
					 	    var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
					 	   if((document.forms['copyEstimation'].estimateList[i].checked==true)&&(chargeCode==null || chargeCode==undefined || chargeCode.trim()=='')){			 	    
					 	    if(newIdsChargeCode==''){
					 		    newIdsChargeCode=accountLineNumber;
					 		    }else{
					 		    	if(newIdsChargeCode.indexOf(accountLineNumber)<0){
					 			    	newIdsChargeCode=newIdsChargeCode + ',' + accountLineNumber;
					 		    	}
					 		    }
					 	   }
				       }
				   }else{
				 	    var targetId=document.forms['copyEstimation'].estimateList.id;
				 	    var chargeCode=document.getElementById('chargeCode'+targetId).value;
				 	   var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
					 	   if((document.forms['copyEstimation'].estimateList.checked==true)&&(chargeCode==null || chargeCode==undefined || chargeCode.trim()=='')){			 	    
					 	    if(newIdsChargeCode==''){
					 		    newIdsChargeCode=accountLineNumber;
					 		    }else{
					 		    	if(newIdsChargeCode.indexOf(accountLineNumber)<0){
					 			    	newIdsChargeCode=newIdsChargeCode + ',' + accountLineNumber;
					 		    	}
					 		    }
					 	   }		   
				   }
				}
			    if(document.forms['copyEstimation'].revisionList!=undefined){	   
				   if(document.forms['copyEstimation'].revisionList.length!=undefined){
				      for (i=0; i<document.forms['copyEstimation'].revisionList.length; i++){
					 	    var targetId=document.forms['copyEstimation'].revisionList[i].id;
					 	    var chargeCode=document.getElementById('chargeCode'+targetId).value;
					 	    var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
					 	   if((document.forms['copyEstimation'].revisionList[i].checked==true)&&(chargeCode==null || chargeCode==undefined || chargeCode.trim()=='')){			 	    
					 	    if(newIdsChargeCode==''){
					 		    newIdsChargeCode=accountLineNumber;
					 		    }else{
					 		    	if(newIdsChargeCode.indexOf(accountLineNumber)<0){
					 			    	newIdsChargeCode=newIdsChargeCode + ',' + accountLineNumber;
					 		    	}
					 		    }
					 	   }
				       }
				   }else{
				 	    var targetId=document.forms['copyEstimation'].revisionList.id;
				 	    var chargeCode=document.getElementById('chargeCode'+targetId).value;
				 	   var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
					 	   if((document.forms['copyEstimation'].revisionList.checked==true)&&(chargeCode==null || chargeCode==undefined || chargeCode.trim()=='')){			 	    
					 	    if(newIdsChargeCode==''){
					 		    newIdsChargeCode=accountLineNumber;
					 		    }else{
					 		    	if(newIdsChargeCode.indexOf(accountLineNumber)<0){
					 			    	newIdsChargeCode=newIdsChargeCode + ',' + accountLineNumber;
					 		    	}
					 		    }
					 	   }		   
				   }
				}
			
		   if(document.forms['copyEstimation'].estimateRevenueList!=undefined){	   
			   if(document.forms['copyEstimation'].estimateRevenueList.length!=undefined){
			      for (i=0; i<document.forms['copyEstimation'].estimateRevenueList.length; i++){
				 	    var targetId=document.forms['copyEstimation'].estimateRevenueList[i].id;
				 	    var chargeCode=document.getElementById('chargeCode'+targetId).value;
				 	    var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
				 	   if((document.forms['copyEstimation'].estimateRevenueList[i].checked==true)&&(chargeCode==null || chargeCode==undefined || chargeCode.trim()=='')){			 	    
				 	    if(newIdsChargeCode==''){
				 		    newIdsChargeCode=accountLineNumber;
				 		    }else{
				 		    	if(newIdsChargeCode.indexOf(accountLineNumber)<0){
				 			    	newIdsChargeCode=newIdsChargeCode + ',' + accountLineNumber;
				 		    	}
				 		    }
				 	   }
			       }
			   }else{
			 	    var targetId=document.forms['copyEstimation'].estimateRevenueList.id;
			 	    var chargeCode=document.getElementById('chargeCode'+targetId).value;
			 	   var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
				 	   if((document.forms['copyEstimation'].estimateRevenueList.checked==true)&&(chargeCode==null || chargeCode==undefined || chargeCode.trim()=='')){			 	    
				 	    if(newIdsChargeCode==''){
				 		    newIdsChargeCode=accountLineNumber;
				 		    }else{
				 		    	if(newIdsChargeCode.indexOf(accountLineNumber)<0){
				 			    	newIdsChargeCode=newIdsChargeCode + ',' + accountLineNumber;
				 		    	}
				 		    }
				 	   }		   
			   }
			}
			if(document.forms['copyEstimation'].revisionRevenueList!=undefined){
				if(document.forms['copyEstimation'].revisionRevenueList.length!=undefined){
					for (i=0; i<document.forms['copyEstimation'].revisionRevenueList.length; i++){
					 	   var targetId=document.forms['copyEstimation'].revisionRevenueList[i].id;
					 	   var chargeCode=document.getElementById('chargeCode'+targetId).value;
					 	  var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
					 	   if((document.forms['copyEstimation'].revisionRevenueList[i].checked==true)&&(chargeCode==null || chargeCode==undefined || chargeCode.trim()=='')){			 	    
					 	    if(newIdsChargeCode==''){
					 		    newIdsChargeCode=accountLineNumber;
					 		    }else{
					 		    	if(newIdsChargeCode.indexOf(accountLineNumber)<0){
					 			    	newIdsChargeCode=newIdsChargeCode + ',' + accountLineNumber;
					 		    	}
					 		    }
					 	   }
				       }
				}else{
			 	    var targetId=document.forms['copyEstimation'].revisionRevenueList.id;
			 	    var chargeCode=document.getElementById('chargeCode'+targetId).value;
			 	   var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
				 	   if((document.forms['copyEstimation'].revisionRevenueList.checked==true)&&(chargeCode==null || chargeCode==undefined || chargeCode.trim()=='')){			 	    
				 	    if(newIdsChargeCode==''){
				 		    newIdsChargeCode=accountLineNumber;
				 		    }else{
				 		    	if(newIdsChargeCode.indexOf(accountLineNumber)<0){
				 			    	newIdsChargeCode=newIdsChargeCode + ',' + accountLineNumber;
				 		    	}
				 		    }
				 	   }		   
				}	
			  }
			  if(newIdsChargeCode!=''){
				  var temp=newIdsChargeCode.split(",");
				  newIdsChargeCode="";
				  for(var u=0;u<temp.length;u++){
					  newIdsChargeCode += "Charge Code Is Missing for Line #"+temp[u]+"\n";
				  }
			  }
		}catch(e){}
			  return newIdsChargeCode;
		}
  
  function checkVendorCodeForCopytoRevision(){

		try{  
			var newIdsRevisionExpense='';
		   if(document.forms['copyEstimation'].estimateList!=undefined){	   
			   if(document.forms['copyEstimation'].estimateList.length!=undefined){
			      for (i=0; i<document.forms['copyEstimation'].estimateList.length; i++){
				 	    var targetId=document.forms['copyEstimation'].estimateList[i].id;
				 	    var vendorCode=document.getElementById('vendorCode'+targetId).value;
				 	   var revisionExpense=document.getElementById('revisionExpense'+targetId).value;
				 	    var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
				 	   if((document.forms['copyEstimation'].estimateList[i].checked==true)&&(vendorCode==null || vendorCode==undefined || vendorCode.trim()=='')&&(revisionExpense==0.00 || revisionExpense==0)){			 	    
				 	    if(newIdsRevisionExpense==''){
				 		    newIdsRevisionExpense=accountLineNumber;
				 		    }else{
				 		    	if(newIdsRevisionExpense.indexOf(accountLineNumber)<0){
				 			    	newIdsRevisionExpense=newIdsRevisionExpense + ',' + accountLineNumber;
				 		    	}
				 		    }
				 	   }
			       }
			   }else{
			 	    var targetId=document.forms['copyEstimation'].estimateList.id;
			 	    var vendorCode=document.getElementById('vendorCode'+targetId).value;
			 	   var revisionExpense=document.getElementById('revisionExpense'+targetId).value;
			 	   var accountLineNumber=document.getElementById('accountLineNumber'+targetId).value;
				 	   if((document.forms['copyEstimation'].estimateList.checked==true)&&(vendorCode==null || vendorCode==undefined || vendorCode.trim()=='')&&(revisionExpense==0.00 || revisionExpense==0)){			 	    
				 	    if(newIdsRevisionExpense==''){
				 		    newIdsRevisionExpense=accountLineNumber;
				 		    }else{
				 		    	if(newIdsRevisionExpense.indexOf(accountLineNumber)<0){
				 			    	newIdsRevisionExpense=newIdsRevisionExpense + ',' + accountLineNumber;
				 		    	}
				 		    }
				 	   }		   
			   }
			}
			  if(newIdsRevisionExpense!=''){
				  var temp=newIdsRevisionExpense.split(",");
				  newIdsRevisionExpense="";
				  for(var u=0;u<temp.length;u++){
					  newIdsRevisionExpense += "Revision Expense cannot be entered as no Partner has been selected. Please select a partner before entering revision expense for Line #"+temp[u]+"\n";
				  }
			  }
		}catch(e){}
			  return newIdsRevisionExpense;
		}
function selectAllEstimateExpense(target){
	 <c:forEach items="${copyEstimateAccountLineList}" var="estList">
		var estimateExpense="${estList.estimateExpense}";
		var revisionExpense="${estList.revisionExpense}";
		var actualExpense="${estList.actualExpense}";
		
      if((estimateExpense!='0.00' && estimateExpense!='0.0000') && ((revisionExpense=='0.00' || revisionExpense=='0.0000') ||  (actualExpense=='0.00' || actualExpense=='0.0000') ) && (actualExpense=='0.00' || actualExpense=='0.0000')){
    	  chkAllForEstimateExpense('${estList.accountLineNumber}:EstExp','${estList.payVatDescr}','${estList.contract}','${estList.chargeCode}',target);
    	  pickAllDistinctId(target,'EstExp');
      }
	</c:forEach>
}

function selectAllEstimateRevenue(target){
	<c:forEach items="${copyEstimateAccountLineList}" var="estRevList">
		var estimateRevenueExpense="${estRevList.estimateRevenueAmount}";
		var revisionRevenue="${estRevList.revisionRevenueAmount}";
		var actualRevenue="${estRevList.actualRevenue}";
		
	if(((estimateRevenueExpense!='0.00' && estimateRevenueExpense!='0.0000') && ((revisionRevenue=='0.00' || revisionRevenue=='0.0000') || actualRevenue=='0.00' || actualRevenue=='0.0000')) && (actualRevenue=='0.00' || actualRevenue=='0.0000')){
		chkAllForEstimateRevenue('${estRevList.accountLineNumber}:EstRev','${estRevList.recVatDescr}','${estRevList.contract}','${estRevList.chargeCode}',target);
		pickAllDistinctId(target,'EstRev');
	}
	</c:forEach>
}

function selectAllRevisionExpense(target){
	<c:forEach items="${copyEstimateAccountLineList}" var="revExpList">
		var revisionExpense="${revExpList.revisionExpense}";
		var actualExpense="${revExpList.actualExpense}";
		
		if(((revisionExpense!='0.00' && revisionExpense!='0.0000') && (actualExpense=='0.00' || actualExpense=='0.0000'))){
			chkAllForRevisionExpense('${revExpList.accountLineNumber}:RevExp','${revExpList.payVatDescr}','${revExpList.contract}','${revExpList.chargeCode}',target);
			pickAllDistinctId(target,'RevExp');
		}
	</c:forEach>
}
function selectAllRevisionRevenue(target){
	<c:forEach items="${copyEstimateAccountLineList}" var="revRevList">
	var revisionRevenue="${revRevList.revisionRevenueAmount}";
	var actualRevenue="${revRevList.actualRevenue}";
	
	if(((revisionRevenue!='0.00' && revisionRevenue!='0.0000') && (actualRevenue=='0.00' || actualRevenue=='0.0000'))){
		chkAllForRevisionRevenue('${revRevList.accountLineNumber}:RevRev','${revRevList.recVatDescr}','${revRevList.contract}','${revRevList.chargeCode}',target);
		pickAllDistinctId(target,'RevRev');
	}
</c:forEach>
}

function chkAllForEstimateExpense(lineNum,vatDesc,contract,chargeCode,target){
	  var vatFlag='${systemDefaultVatCalculation}';
	  var acctlineNum=document.forms['saveCopyEstimation'].elements['acctLineNumber'].value;
	  var vatExclude=checkVatExclude(contract,chargeCode);
	  var tmp="";
	  if(vatFlag=='true' && vatExclude!='T'){
		  
		  	if(target.checked==true){
	 	    	$('input:checkbox[name=estimateList]').attr('checked',true);
	 	    }else if(target.checked==false){
	 	    	$('input:checkbox[name=estimateList]').attr('checked',false);
	 	    }
			$(".estimateList:checked").each(function() {
				if(vatDesc==null || vatDesc==''){
					 if(acctlineNum!=null && acctlineNum!='') {
						 tmp=acctlineNum+"~"+lineNum;
						 document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=tmp;
					 }else{
						 tmp=lineNum;
						 document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=tmp;
					 }
	
				  }
			});
			$(".estimateList:not(:checked").each(function() {
				var acctlineNums=acctlineNum.split("~");
				  var newVal="";
				  for(var i=0;i<acctlineNums.length;i++){
					  if(acctlineNums[i]!=lineNum){
						  if(newVal!=''){
							  newVal=newVal+"~"+acctlineNums[i];
						  }else{
							  newVal=acctlineNums[i];
						  }
					  }
				  }
				document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=newVal;
			});
	}
}

function chkAllForEstimateRevenue(lineNum,vatDesc,contract,chargeCode,target){
	  var vatFlag='${systemDefaultVatCalculation}';
	  var acctlineNum=document.forms['saveCopyEstimation'].elements['acctLineNumber'].value;
	  var vatExclude=checkVatExclude(contract,chargeCode);
	  var tmp="";
	  if(vatFlag=='true' && vatExclude!='T'){
		  
	 	    if(target.checked==true){
	 	    	$('input:checkbox[name=estimateRevenueList]').attr('checked',true);
	 	    }else if(target.checked==false){
	 	    	$('input:checkbox[name=estimateRevenueList]').attr('checked',false);
	 	    }
			$(".estimateRevenueList:checked").each(function() {
				if(vatDesc==null || vatDesc==''){
					 if(acctlineNum!=null && acctlineNum!='') {
						 tmp=acctlineNum+"~"+lineNum;
						 document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=tmp;
					 }else{
						 tmp=lineNum;
						 document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=tmp;
					 }
	
				  }
			});
			$(".estimateRevenueList:not(:checked").each(function() {
				var acctlineNums=acctlineNum.split("~");
				  var newVal="";
				  for(var i=0;i<acctlineNums.length;i++){
					  if(acctlineNums[i]!=lineNum){
						  if(newVal!=''){
							  newVal=newVal+"~"+acctlineNums[i];
						  }else{
							  newVal=acctlineNums[i];
						  }
					  }
				  }
				document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=newVal;
			});
		
	}
}

function chkAllForRevisionExpense(lineNum,vatDesc,contract,chargeCode,target){
	  var vatFlag='${systemDefaultVatCalculation}';
	  var acctlineNum=document.forms['saveCopyEstimation'].elements['acctLineNumber'].value;
	  var vatExclude=checkVatExclude(contract,chargeCode);
	  var tmp="";
	  if(vatFlag=='true' && vatExclude!='T'){
		  
	 	    if(target.checked==true){
	 	    	$('input:checkbox[name=revisionList]').attr('checked',true);
	 	    }else if(target.checked==false){
	 	    	$('input:checkbox[name=revisionList]').attr('checked',false);
	 	    }
			$(".revisionList:checked").each(function() {
				if(vatDesc==null || vatDesc==''){
					 if(acctlineNum!=null && acctlineNum!='') {
						 tmp=acctlineNum+"~"+lineNum;
						 document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=tmp;
					 }else{
						 tmp=lineNum;
						 document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=tmp;
					 }
	
				  }
			});
			$(".revisionList:not(:checked").each(function() {
				var acctlineNums=acctlineNum.split("~");
				  var newVal="";
				  for(var i=0;i<acctlineNums.length;i++){
					  if(acctlineNums[i]!=lineNum){
						  if(newVal!=''){
							  newVal=newVal+"~"+acctlineNums[i];
						  }else{
							  newVal=acctlineNums[i];
						  }
					  }
				  }
				document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=newVal;
			});
	}
}

function chkAllForRevisionRevenue(lineNum,vatDesc,contract,chargeCode,target){
	  var vatFlag='${systemDefaultVatCalculation}';
	  var acctlineNum=document.forms['saveCopyEstimation'].elements['acctLineNumber'].value;
	  var vatExclude=checkVatExclude(contract,chargeCode);
	  var tmp="";
	  if(vatFlag=='true' && vatExclude!='T'){
		  
	 	    if(target.checked==true){
	 	    	$('input:checkbox[name=revisionRevenueList]').attr('checked',true);
	 	    }else if(target.checked==false){
	 	    	$('input:checkbox[name=revisionRevenueList]').attr('checked',false);
	 	    }
			$(".revisionRevenueList:checked").each(function() {
				if(vatDesc==null || vatDesc==''){
					 if(acctlineNum!=null && acctlineNum!='') {
						 tmp=acctlineNum+"~"+lineNum;
						 document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=tmp;
					 }else{
						 tmp=lineNum;
						 document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=tmp;
					 }
	
				  }
			});
			$(".revisionRevenueList:not(:checked").each(function() {
				var acctlineNums=acctlineNum.split("~");
				  var newVal="";
				  for(var i=0;i<acctlineNums.length;i++){
					  if(acctlineNums[i]!=lineNum){
						  if(newVal!=''){
							  newVal=newVal+"~"+acctlineNums[i];
						  }else{
							  newVal=acctlineNums[i];
						  }
					  }
				  }
				document.forms['saveCopyEstimation'].elements['acctLineNumber'].value=newVal;
			});
	}
}
function pickAllDistinctId(target,type){
 var newIds='';
 	if(type=='EstExp'){
		if(target.checked==true){
	    	$('input:checkbox[name=estimateList]').attr('checked',true);
	    }else if(target.checked==false){
	    	$('input:checkbox[name=estimateList]').attr('checked',false);
	    }
	}
 	if(document.forms['copyEstimation'].estimateList!=undefined){	   
	   if(document.forms['copyEstimation'].estimateList.length!=undefined){
	      for (i=0; i<document.forms['copyEstimation'].estimateList.length; i++){
		 	    var targetId=document.forms['copyEstimation'].estimateList[i].id;
		 	   if(document.forms['copyEstimation'].estimateList[i].checked==true){			 	    
		 	    if(newIds==''){
		 		    newIds=targetId;
		 		    }else{
		 		    	if(newIds.indexOf(targetId)<0){
		 			    	newIds=newIds + ',' + targetId;
		 		    	}
		 		    }
		 	   }
	       }
	   }else{
	 	    var targetId=document.forms['copyEstimation'].estimateList.id;
		 	   if(document.forms['copyEstimation'].estimateList.checked==true){			 	    
		 	    if(newIds==''){
		 		    newIds=targetId;
		 		    }else{
		 		    	if(newIds.indexOf(targetId)<0){
		 			    	newIds=newIds + ',' + targetId;
		 		    	}
		 		    }
		 	   }		   
	   }
	}
 	if(type=='EstRev'){
		if(target.checked==true){
	    	$('input:checkbox[name=estimateRevenueList]').attr('checked',true);
	    }else if(target.checked==false){
	    	$('input:checkbox[name=estimateRevenueList]').attr('checked',false);
	    }
	}
	if(document.forms['copyEstimation'].estimateRevenueList!=undefined){
		if(document.forms['copyEstimation'].estimateRevenueList.length!=undefined){
	      for (i=0; i<document.forms['copyEstimation'].estimateRevenueList.length; i++){
		 	    var targetId=document.forms['copyEstimation'].estimateRevenueList[i].id;
		 	   if(document.forms['copyEstimation'].estimateRevenueList[i].checked==true){
		 	    if(newIds==''){
		 		    newIds=targetId;
		 		    }else{
		 		    	if(newIds.indexOf(targetId)<0){
		 			    	newIds=newIds + ',' + targetId;
		 		    	}
		 		    }
	 		    }
	       }
		}else{
	 	    var targetId=document.forms['copyEstimation'].estimateRevenueList.id;
		 	   if(document.forms['copyEstimation'].estimateRevenueList.checked==true){			 	    
		 	    if(newIds==''){
		 		    newIds=targetId;
		 		    }else{
		 		    	if(newIds.indexOf(targetId)<0){
		 			    	newIds=newIds + ',' + targetId;
		 		    	}
		 		    }
		 	   }		   
			
		}
	}	  
	if(type=='RevExp'){
		if(target.checked==true){
	    	$('input:checkbox[name=revisionList]').attr('checked',true);
	    }else if(target.checked==false){
	    	$('input:checkbox[name=revisionList]').attr('checked',false);
	    }
	}
	if(document.forms['copyEstimation'].revisionList!=undefined){
		if(document.forms['copyEstimation'].revisionList.length!=undefined){
			for (i=0; i<document.forms['copyEstimation'].revisionList.length; i++){
			 	   var targetId=document.forms['copyEstimation'].revisionList[i].id;
			 	   if(document.forms['copyEstimation'].revisionList[i].checked==true){			 	    
			 	    if(newIds==''){
			 		    newIds=targetId;
			 		    }else{
			 		    	if(newIds.indexOf(targetId)<0){
			 			    	newIds=newIds + ',' + targetId;
			 		    	}
			 		    }
			 	   }
		       }
		}else{
	 	    var targetId=document.forms['copyEstimation'].revisionList.id;
		 	   if(document.forms['copyEstimation'].revisionList.checked==true){			 	    
		 	    if(newIds==''){
		 		    newIds=targetId;
		 		    }else{
		 		    	if(newIds.indexOf(targetId)<0){
		 			    	newIds=newIds + ',' + targetId;
		 		    	}
		 		    }
		 	   }		   
		}	
	  }
	if(type=='RevRev'){
		if(target.checked==true){
	    	$('input:checkbox[name=revisionRevenueList]').attr('checked',true);
	    }else if(target.checked==false){
	    	$('input:checkbox[name=revisionRevenueList]').attr('checked',false);
	    }
	}
	if(document.forms['copyEstimation'].revisionRevenueList!=undefined){
		if(document.forms['copyEstimation'].revisionRevenueList.length!=undefined){
		      for (i=0; i<document.forms['copyEstimation'].revisionRevenueList.length; i++){
			 	    var targetId=document.forms['copyEstimation'].revisionRevenueList[i].id;
			 	   if(document.forms['copyEstimation'].revisionRevenueList[i].checked==true){	
			 	    if(newIds==''){
			 		    newIds=targetId;
			 		    }else{
			 		    	if(newIds.indexOf(targetId)<0){
			 			    	newIds=newIds + ',' + targetId;
			 		    	}
			 		    }
			 	   }
		       }	
		}else{
	 	    var targetId=document.forms['copyEstimation'].revisionRevenueList.id;
		 	   if(document.forms['copyEstimation'].revisionRevenueList.checked==true){			 	    
		 	    if(newIds==''){
		 		    newIds=targetId;
		 		    }else{
		 		    	if(newIds.indexOf(targetId)<0){
		 			    	newIds=newIds + ',' + targetId;
		 		    	}
		 		    }
		 	   }		   
		}
	  }
	 document.forms['saveCopyEstimation'].elements['distinctAccLineId'].value=newIds;
}
function checkEstimateExpenseId(target){
	var newIds='';
	if(target.checked==true){
		$(".estimateList:checked").each(function() {
			var targetId = $(this).attr('id');
			if(newIds==''){
	 		    newIds=targetId;
	 		    }else{
	 		    	if(newIds.indexOf(targetId)<0){
	 			    	newIds=newIds + ',' + targetId;
	 		    	}
	 		    }
	 	   });
		if(newIds==''){
			alert('Nothing found for check.');
			document.getElementById('selectAllEstimateExpense').checked=false;
		}
	}
}
function checkEstimateReveuneId(target){
	var newIds='';
	if(target.checked==true){
		$(".estimateRevenueList:checked").each(function() {
			var targetId = $(this).attr('id');
			if(newIds==''){
	 		    newIds=targetId;
	 		    }else{
	 		    	if(newIds.indexOf(targetId)<0){
	 			    	newIds=newIds + ',' + targetId;
	 		    	}
	 		    }
	 	   });
		if(newIds==''){
			alert('Nothing found for check.');
			document.getElementById('selectAllEstimateRevenue').checked=false;
		}
	}
}
function checkRevisionExpenseId(target){
	var newIds='';
	if(target.checked==true){
		$(".revisionList:checked").each(function() {
			var targetId = $(this).attr('id');
			if(newIds==''){
	 		    newIds=targetId;
	 		    }else{
	 		    	if(newIds.indexOf(targetId)<0){
	 			    	newIds=newIds + ',' + targetId;
	 		    	}
	 		    }
	 	   });
		if(newIds==''){
			alert('Nothing found for check.');
			document.getElementById('selectAllRevisionExpense').checked=false;
		}
	}
}
function checkRevisionRevenueId(target){
	var newIds='';
	if(target.checked==true){
		$(".revisionRevenueList:checked").each(function() {
			var targetId = $(this).attr('id');
			if(newIds==''){
	 		    newIds=targetId;
	 		    }else{
	 		    	if(newIds.indexOf(targetId)<0){
	 			    	newIds=newIds + ',' + targetId;
	 		    	}
	 		    }
	 	   });
		if(newIds==''){
			alert('Nothing found for check.');
			document.getElementById('selectAllRevisionRevenue').checked=false;
		}
	}
}
</script>       