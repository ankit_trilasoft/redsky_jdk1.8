package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.OperationsIntelligenceDao;
import com.trilasoft.app.model.OperationsIntelligence;

public class OperationsIntelligenceDaoHibernate extends GenericDaoHibernate<OperationsIntelligence, Long> implements OperationsIntelligenceDao{
	
	public OperationsIntelligenceDaoHibernate()
	{
		super(OperationsIntelligence.class);
	}
	protected HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();  
    } 
	public void updateOperationsIntelligence(Long id,String fieldName,String fieldValue,String tableName , String shipNumber,String updatedBy)
	{
	if(!fieldName.equalsIgnoreCase(""))
	{
		getHibernateTemplate().bulkUpdate("update "+tableName+" set "+fieldName+"='"+fieldValue+"',updatedOn=now(),updatedBy='"+updatedBy+"' where id ='"+id+"' and shipNumber='"+ shipNumber +"' ");
	}else{
		getSession().createSQLQuery("update serviceorder set "+fieldValue+" ,updatedOn=now(),updatedBy='"+updatedBy+"' where id ='"+id+"' and shipNumber='"+ shipNumber +"' ").executeUpdate();
	}}
	
	 public List findAllResourcesAjax(String shipNumber,String sessionCorpID,String resouceTemplate, String resourceListForOIStatus){
		 getHibernateTemplate().clear();
		 String ticketNumber = findInvoicedWorkTicketNumber(shipNumber);
		 if(resourceListForOIStatus!=null && (!(resourceListForOIStatus.trim().equals(""))) && resourceListForOIStatus.trim().equalsIgnoreCase("true")){
			 if(resouceTemplate!=null && !resouceTemplate.equalsIgnoreCase("") && !resouceTemplate.equalsIgnoreCase("A")){
				 String workorderForName="";
				 String []workorderNumber = resouceTemplate.split(",");
				int workorderLength = workorderNumber.length;
				for(String workOrderValue :workorderNumber ){
					if(workorderForName.equalsIgnoreCase("")){
						workorderForName="'"+workOrderValue.trim()+"'";
					}else{
						workorderForName = workorderForName +",'"+workOrderValue.trim()+"'";
					}
				}
				if(ticketNumber!=null && !ticketNumber.equalsIgnoreCase("")){
					return getHibernateTemplate().find("from OperationsIntelligence where corpID='"+sessionCorpID +"' and  status is true and shipNumber='" + shipNumber + "' and ( quantitiy <> 0 or quantitiy is null) and (ticketStatus is null or ticketStatus='') and workorder in (" + workorderForName + ") and (ticket not in ("+ ticketNumber+") or ticket is null) order by workorder,displayPriority desc,ticket,type,description ");
				}else{
					return getHibernateTemplate().find("from OperationsIntelligence where corpID='"+sessionCorpID +"' and  status is true and shipNumber='" + shipNumber + "' and ( quantitiy <> 0 or quantitiy is null) and (ticketStatus is null or ticketStatus='') and workorder in (" + workorderForName + ") order by workorder,displayPriority desc,ticket,type,description ");
				}
			   }else  if(resouceTemplate.equalsIgnoreCase("A")){
				   if(ticketNumber!=null && !ticketNumber.equalsIgnoreCase("")){
					   return getHibernateTemplate().find("from OperationsIntelligence where corpID='"+sessionCorpID +"' and  status is true and shipNumber='" + shipNumber + "' and (ticket not in ("+ ticketNumber+") or ticket is null) and (ticketStatus is null or ticketStatus='') order by workorder,displayPriority desc,ticket,type,description ");
				   }else{
					   return getHibernateTemplate().find("from OperationsIntelligence where corpID='"+sessionCorpID +"' and  status is true and shipNumber='" + shipNumber + "' and (ticketStatus is null or ticketStatus='') order by workorder,displayPriority desc,ticket,type,description ");
				   }
			   }else{
				   if(ticketNumber!=null && !ticketNumber.equalsIgnoreCase("")){
					   return getHibernateTemplate().find("from OperationsIntelligence where corpID='"+sessionCorpID +"' and  status is true and shipNumber='" + shipNumber + "' and (ticket not in ("+ ticketNumber+") or ticket is null) and ( quantitiy <> 0 or quantitiy is null) and (ticketStatus is null or ticketStatus='') order by workorder,displayPriority desc,ticket,type,description ");
				   }else{
					   return getHibernateTemplate().find("from OperationsIntelligence where corpID='"+sessionCorpID +"' and  status is true and shipNumber='" + shipNumber + "' and ( quantitiy <> 0 or quantitiy is null) and (ticketStatus is null or ticketStatus='') order by workorder,displayPriority desc,ticket,type,description ");
				   }
			   }
		 }else{
			 if(resouceTemplate!=null && !resouceTemplate.equalsIgnoreCase("") && !resouceTemplate.equalsIgnoreCase("A")){
				 String workorderForName="";
				 String []workorderNumber = resouceTemplate.split(",");
				int workorderLength = workorderNumber.length;
				for(String workOrderValue :workorderNumber ){
					if(workorderForName.equalsIgnoreCase("")){
						workorderForName="'"+workOrderValue.trim()+"'";
					}else{
						workorderForName = workorderForName +",'"+workOrderValue.trim()+"'";
					}
				}
				if(ticketNumber!=null && !ticketNumber.equalsIgnoreCase("")){
					return getHibernateTemplate().find("from OperationsIntelligence where corpID='"+sessionCorpID +"' and  shipNumber='" + shipNumber + "' and ( quantitiy <> 0 or quantitiy is null) and (ticketStatus is null or ticketStatus='') and workorder in (" + workorderForName + ") and (ticket not in ("+ ticketNumber+") or ticket is null) order by workorder,displayPriority desc,ticket,type,description ");
				}else{
					return getHibernateTemplate().find("from OperationsIntelligence where corpID='"+sessionCorpID +"' and  shipNumber='" + shipNumber + "' and ( quantitiy <> 0 or quantitiy is null) and (ticketStatus is null or ticketStatus='') and workorder in (" + workorderForName + ") order by workorder,displayPriority desc,ticket,type,description ");
				}
			   }else if(resouceTemplate.equalsIgnoreCase("A")){
				   if(ticketNumber!=null && !ticketNumber.equalsIgnoreCase("")){
					   return getHibernateTemplate().find("from OperationsIntelligence where  corpID='"+sessionCorpID +"' and shipNumber='" + shipNumber + "' and (ticket not in ("+ ticketNumber+") or ticket is null) and (ticketStatus is null or ticketStatus='') order by workorder,displayPriority desc,ticket,type,description ");
				   }else{
					   return getHibernateTemplate().find("from OperationsIntelligence where corpID='"+sessionCorpID +"' and  shipNumber='" + shipNumber + "' and (ticketStatus is null or ticketStatus='') order by workorder,displayPriority desc,ticket,type,description ");
				   }
			   }else{
					 return getHibernateTemplate().find("from OperationsIntelligence where corpID='"+sessionCorpID +"' and  shipNumber='" + shipNumber + "' and ( quantitiy <> 0 or quantitiy is null) and (ticketStatus is null or ticketStatus='') order by workorder,displayPriority desc,ticket,type,description ");
			   }
		 }
	}
	 public void getDeleteWorkOrder(String shipNumber,String sessionCorpID){		 
		 getSession().createSQLQuery("delete from operationsintelligence where corpID='"+sessionCorpID +"' and  shipNumber='" + shipNumber + "' and ( quantitiy =0 or quantitiy is null or quantitiy=0.00 ) ").executeUpdate();
		 
	 }
	 
	 public String findInvoicedWorkTicketNumber(String shipNumber){
		 String ticketNumber="";
		 String query = "select group_concat(ticket) from workticket where shipnumber='" + shipNumber + "' and (invoicenumber!=null || invoicenumber!='')";
		 List ticketNumberList=getSession().createSQLQuery(query).list();
		 ticketNumberList=getSession().createSQLQuery(query).list();
		 if(ticketNumberList!=null && !ticketNumberList.isEmpty() && ticketNumberList.get(0)!=null){
			 ticketNumber = ticketNumberList.get(0).toString();
		 }
		 return ticketNumber;
	 }
	 
	 public String getSellRateBuyRate(String type,String shipNumber,String descript,String sessionCorpID){
		 String value="";
		 String query="";
		 query="select i.cost,c.quantity2preset , c.payablePreset from charges c "+
		 "inner join resourcecontractcharges r on c.contract = r.contract "+
		 "and  c.charge = r.charge "+
		 "inner join billing b on b.contract=r.contract "+
		 "inner join itemsjequip i on i.id = r.parentid "+
		 "where c.corpid='"+sessionCorpID+"' "+
		 "and r.parentid= (select id from itemsjequip where descript='"+descript+"' and type='"+type+"' and corpid='"+sessionCorpID+"') "+
		 "and b.shipnumber='"+shipNumber+"'";
		 
		 List al=getSession().createSQLQuery(query).list();
		 if(al!=null && !al.isEmpty() && al.get(0)!=null){
			 Iterator itr=al.iterator();
			 while(itr.hasNext()){
				 Object obj[]=(Object[])itr.next();
				 value=obj[0]+"~"+obj[1]+"~"+obj[2];
			 }
		 }	
		 return value;
	 } 
	 public void deletefromOIResorcesList(long id,String shipNumber, String usertype) 
	 {
		 getHibernateTemplate().bulkUpdate("update OperationsIntelligence set ticketStatus='Cancel', updatedBy='"+usertype+"' where shipNumber='" + shipNumber + "' and id ='"+id+"'"); 
		 getSession().createSQLQuery("UPDATE operationsintelligence, (select sum(salesTax) salesTax from (select distinct salesTax,shipnumber,workorder from operationsintelligence o where o.status is true and shipnumber='"+shipNumber+"' and ticketStatus is null and ( quantitiy <> 0 and quantitiy is not null))a) src SET estmatedSalesTax = src.salesTax  where shipnumber='"+ shipNumber +"' and ticketStatus is null and ( quantitiy <> 0 and quantitiy is not null)").executeUpdate();
	 }
	 public void deletefromWorkTicketResorcesList(long id,String shipNumber,String ticket, String sessionCorpID)
	 {
		 getSession().createSQLQuery("delete from itemsjbkequip where ticket='" + ticket + "' and id ='"+id+"' and shipNum='" + shipNumber + "' ").executeUpdate(); 
	 }
	 public List distinctWorkOrderForWT(String shipNumber,String sessionCorpID)
	 {
		 return getHibernateTemplate().find("from OperationsIntelligence where status is true and shipNumber='" + shipNumber + "' and corpID='"+sessionCorpID +"' and type <> 'T' and quantitiy <> 0 and (ticket is  null ) and  (ticketStatus is null) group by workorder order by workorder"); 
	 }
	 public List distinctWorkOrderForThirdParty(String shipNumber,String sessionCorpID)
	 {
		 return getHibernateTemplate().find("from OperationsIntelligence where status is true and shipNumber='" + shipNumber + "' and corpID='"+sessionCorpID +"' and quantitiy <> 0 and type = 'T' and ticketStatus is null and (ticket is  null ) order by workorder"); 
	 }
	 
	 public List getResourceFromOI(String sessionCorpID ,String shipNumber, String workorder)
	 {
		 return getHibernateTemplate().find("from OperationsIntelligence where status is true and  shipNumber='" + shipNumber + "' and corpID='"+sessionCorpID +"' and workorder='"+workorder +"' and type <> 'T' and quantitiy <> 0 and ticketStatus is null ");  
	 }
	 public List getResourceFromOIForTP(String sessionCorpID ,String shipNumber, String workorder,String tpId)
	 {
		 return getHibernateTemplate().find("from OperationsIntelligence where status is true and  shipNumber='" + shipNumber + "' and corpID='"+sessionCorpID +"' and workorder='"+workorder +"' and type = 'T' and quantitiy <> 0 and ticketStatus is null and id='"+tpId+"' ");	 
	 }
	 
	 public void updateOperationsIntelligenceTicketNo(Long ticket,String workorder,String shipNumber,String sessionCorpID,String tpId,String date1 , Long ticketId,String updatedBy,String targetActual)
	 {
		// ticketId = ticketId.replaceAll("\\[", "").replaceAll("\\]","");
		 SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
		 if(tpId.equalsIgnoreCase("OI"))
		{
			 Date startDate;
			    try {
			        startDate = df.parse(date1);
			        date1 = df.format(startDate);
			        System.out.println(date1);
			    } catch (ParseException e) {
			        e.printStackTrace();
			    }
		}else {
		 Date du = new Date();
         try {
			du = df.parse(date1);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         df = new SimpleDateFormat("yyyy-MM-dd");
         date1 = df.format(du);
		 }
		 if(tpId.equalsIgnoreCase("OI") ){
				getHibernateTemplate().bulkUpdate("update OperationsIntelligence set ticket='"+ticket+"', date='"+date1+"' ,workTicketId='"+ticketId+"',updatedBy='"+updatedBy+"',targetActual='"+targetActual+"' where workorder ='"+workorder+"' and shipNumber='"+ shipNumber +"' and corpID='"+sessionCorpID +"' and type <> 'T' and quantitiy <> 0 and ticketStatus is null and status is true");	 
		 }else{
			 if(tpId != null && !(tpId.equalsIgnoreCase(""))){
				 getHibernateTemplate().bulkUpdate("update OperationsIntelligence set ticket='"+ticket+"' , date='"+date1+"' ,workTicketId='"+ticketId+"' ,updatedBy='"+updatedBy+"',targetActual='"+targetActual+"' where workorder ='"+workorder+"' and shipNumber='"+ shipNumber +"' and corpID='"+sessionCorpID +"' and id='"+tpId+"' and quantitiy <> 0 and ticketStatus is null and status is true"); 
			 }else{
				 getHibernateTemplate().bulkUpdate("update OperationsIntelligence set ticket='"+ticket+"', date='"+date1+"' ,workTicketId='"+ticketId+"' ,updatedBy='"+updatedBy+"',targetActual='"+targetActual+"' where workorder ='"+workorder+"' and shipNumber='"+ shipNumber +"' and corpID='"+sessionCorpID +"' and type <> 'T' and quantitiy <> 0 and ticketStatus is null and status is true");  
			}
		 }
	 }
	 public void updateOperationsIntelligenceForScope(String workorder,String tempScopeOfWo,String shipNumber,Long ticket)
	 {
		 getHibernateTemplate().bulkUpdate("update OperationsIntelligence set scopeOfWorkOrder='"+StringEscapeUtils.escapeSql(tempScopeOfWo)+"' where shipNumber='"+ shipNumber +"' and workorder='"+workorder+"' ");
		 /*if(ticket != null){
		 getHibernateTemplate().bulkUpdate("update WorkTicket set instructions='"+StringEscapeUtils.escapeSql(tempScopeOfWo)+"' where shipNumber='"+ shipNumber +"' and ticket='"+ticket+"'  and (instructions is null or instructions ='') "); 
		 }*/
		 }
	 
	 public void saveScopeInOperationsIntelligence(String workorder,String tempScopeOfWo,String shipNumber,Long ticket,String numberOfComputer,String numberOfEmployee,Double salesTax,String consumables,String revisionScopeValue,boolean consumablePercentageCheck,String aB5Surcharge,boolean editAB5Surcharge ,BigDecimal consumableFromPP) {
		 if((revisionScopeValue!=null && !revisionScopeValue.equalsIgnoreCase("")) && revisionScopeValue.equalsIgnoreCase("revisionFalse")){
			 	getHibernateTemplate().bulkUpdate("update OperationsIntelligence set scopeOfWorkOrder='"+StringEscapeUtils.escapeSql(tempScopeOfWo)+"', numberOfComputer='"+numberOfComputer+"', numberOfEmployee='"+numberOfEmployee+"', salesTax='"+salesTax+"', consumables='"+consumables+"',estimateConsumablePercentage="+consumablePercentageCheck+",aB5Surcharge='"+aB5Surcharge+"',editAB5Surcharge="+editAB5Surcharge+" where shipNumber='"+shipNumber+"' and workorder='"+workorder+"' ");
			 try {
				getSession().createSQLQuery("UPDATE operationsintelligence, (select sum(salesTax) salesTax from (select distinct salesTax,shipnumber,workorder from operationsintelligence o where o.status is true and shipnumber='"+shipNumber+"' and ticketStatus is null and ( quantitiy <> 0 and quantitiy is not null))a) src SET estmatedSalesTax = src.salesTax  where shipnumber='"+ shipNumber +"' and ticketStatus is null and ( quantitiy <> 0 and quantitiy is not null)").executeUpdate();
			 }catch (Exception e) {
				e.printStackTrace();
			 }
			 /*if(ticket != null){
				 getHibernateTemplate().bulkUpdate("update WorkTicket set instructions='"+StringEscapeUtils.escapeSql(tempScopeOfWo)+"' where shipNumber='"+ shipNumber +"' and ticket='"+ticket+"' and (instructions is null or instructions ='') "); 
			 }*/
			 if(editAB5Surcharge){
				 updateExpenseRevenueAB5Surcharge(shipNumber); 
			 }else{
				 updateEstimateAndRevisionAB5SurchargeWithPercentage(shipNumber, workorder, "Estimate", consumableFromPP);  
			 }
			 if(consumablePercentageCheck){
				 //updateEstimateAndRevisionConsumablesWithParnerPercentage(shipNumber, workorder, "Estimate", consumableFromPP);
				 updateExpenseRevenueConsumables(shipNumber);
			 }else if(consumableFromPP.compareTo(BigDecimal.ZERO) != 0){
				 updateEstimateAndRevisionConsumablesWithParnerPercentage(shipNumber, workorder, "Estimate", consumableFromPP); 
			 }
			 else{
				 updateEstimateAndRevisionConsumablesWithOutParnerPercentage(shipNumber, workorder, "Estimate");
			 }
		}else{
			getHibernateTemplate().bulkUpdate("update OperationsIntelligence set revisionScopeOfWorkOrder='"+StringEscapeUtils.escapeSql(tempScopeOfWo)+"', revisionScopeOfNumberOfComputer='"+numberOfComputer+"', revisionScopeOfNumberOfEmployee='"+numberOfEmployee+"', revisionScopeOfSalesTax='"+salesTax+"', revisionScopeOfConsumables='"+consumables+"',revisionConsumablePercentage="+consumablePercentageCheck+", revisionScopeOfAB5Surcharge ='"+aB5Surcharge+"',revisionaeditAB5Surcharge ="+editAB5Surcharge+" where shipNumber='"+shipNumber+"' and workorder='"+workorder+"' ");
			try {
				getSession().createSQLQuery("UPDATE operationsintelligence, (select sum(revisionScopeOfSalesTax) salesTax from (select distinct revisionScopeOfSalesTax,shipnumber,workorder from operationsintelligence o where o.status is true and shipnumber='"+shipNumber+"' and ticketStatus is null and ( quantitiy <> 0 and quantitiy is not null))a) src SET revisionSalesTax = src.salesTax  where shipnumber='"+ shipNumber +"' and ticketStatus is null and ( quantitiy <> 0 and quantitiy is not null)").executeUpdate();
			 }catch (Exception e) {
				e.printStackTrace();
			 }
			
			 if(editAB5Surcharge){
				 updateExpenseRevenueAB5Surcharge(shipNumber); 
			 }else{
				 updateEstimateAndRevisionAB5SurchargeWithPercentage(shipNumber, workorder, "Revision", consumableFromPP); 
			 }
			if(consumablePercentageCheck){
				 //updateEstimateAndRevisionConsumablesWithParnerPercentage(shipNumber, workorder, "Revision", consumableFromPP);
				updateExpenseRevenueConsumables(shipNumber);
			 }else if(consumableFromPP.compareTo(BigDecimal.ZERO) != 0){
				 updateEstimateAndRevisionConsumablesWithParnerPercentage(shipNumber, workorder, "Revision", consumableFromPP); 
			 }
			else{
				 updateEstimateAndRevisionConsumablesWithOutParnerPercentage(shipNumber, workorder, "Revision");
			 }
		}
	}
	 
	 
	public void updateOperationIntelligenceRevisionSalesTax(String shipNumber){
		 
		 try {
				getSession().createSQLQuery("UPDATE operationsintelligence, (select sum(revisionScopeOfSalesTax) salesTax from (select distinct revisionScopeOfSalesTax,shipnumber,workorder from operationsintelligence o where o.status is true and shipnumber='"+shipNumber+"' and ticketStatus is null and ( quantitiy <> 0 and quantitiy is not null))a) src SET revisionSalesTax = src.salesTax  where shipnumber='"+ shipNumber +"' and ticketStatus is null and ( quantitiy <> 0 and quantitiy is not null)").executeUpdate();
			 }catch (Exception e) {
				e.printStackTrace();
			 }
	 }
	 
	 public String findScopeOfWorkOrder(Long id,String shipNumber)
	 {
		 /*return getHibernateTemplate().find("select scopeOfWorkOrder from OperationsIntelligence where shipNumber='" + shipNumber + "' and id='"+id +"'").toString(); */
		 String query ="select if(scopeOfWorkOrder is null,'aa',scopeOfWorkOrder ) as scopeOfWorkOrder, if(numberOfComputer is null,'aa',numberOfComputer) as numberOfComputer, if(numberOfEmployee is null,'aa',numberOfEmployee) as numberOfEmployee, if(salesTax is null,'aa',salesTax ) as salesTax, if(consumables is null,'aa',consumables) as consumables,if(targetactual is null or targetactual='','aa',targetactual) as targetactual from operationsintelligence where   shipNumber='" + shipNumber + "' and id='"+id +"'";
				String scopeOfWO = "";
				String numberOfComputer= "";
				String numberOfEmployee = "";
				String salesTax = "";
				String consumables = "";
				String targetActualVal = "";
				List list = getSession().createSQLQuery(query).list();
				if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase(""))
				{
					 Iterator itr=list.iterator();
					 while(itr.hasNext()){
						 	Object obj[]=(Object[])itr.next();
						 	scopeOfWO = obj[0].toString();
							numberOfComputer = obj[1].toString();
							numberOfEmployee = obj[2].toString();
							salesTax = obj[3].toString();
							consumables = obj[4].toString();
							targetActualVal = obj[5].toString();
					 }
					
				}
				return scopeOfWO+"~"+numberOfComputer+"~"+numberOfEmployee+"~"+salesTax+"~"+consumables+"~"+targetActualVal;
	 }
	 
	 public String scopeByWorkOrderInUserPortal(String workorder, String shipNumber){
		 
		 String query ="select if(scopeOfWorkOrder is null,'aa',scopeOfWorkOrder ) as scopeOfWorkOrder, if(numberOfComputer is null,'aa',numberOfComputer) as numberOfComputer, if(numberOfEmployee is null,'aa',numberOfEmployee) as numberOfEmployee, if(salesTax is null,'aa',salesTax ) as salesTax, if(consumables is null,'aa',consumables) as consumables from operationsintelligence where status is true and  workorder='"+workorder+"' and shipNumber='"+shipNumber+"' and scopeOfWorkOrder is not null Limit 1";
			String scopeOfWO = "";
			String numberOfComputer= "";
			String numberOfEmployee = "";
			String salesTax = "";
			String consumables = "";
			List list = getSession().createSQLQuery(query).list();
			if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase(""))
			{
				 Iterator itr=list.iterator();
				 while(itr.hasNext()){
					 	Object obj[]=(Object[])itr.next();
					 	if(obj[0] !=null && obj[0] != ""){
					 		scopeOfWO = obj[0].toString();
					 	}else{
					 		scopeOfWO="aa";
					 	}
					 	if(obj[1] !=null && obj[1] != ""){
					 		numberOfComputer = obj[1].toString();
					 	}else{
					 		numberOfComputer="aa";
					 	}
					 	
					 	if(obj[2] !=null && obj[2] != ""){
					 		numberOfEmployee = obj[2].toString();
					 	}else{
					 		numberOfEmployee="aa";
					 	}
					 	
					 	if(obj[3] !=null && obj[3] != ""){
					 		salesTax = obj[3].toString();
					 	}else{
					 		salesTax="aa";
					 	}
					 	
					 	if(obj[4] !=null && obj[4] != ""){
					 		consumables = obj[4].toString();
					 	}else{
					 		consumables="aa";
					 	}
						
				 }
				return scopeOfWO+"~"+numberOfComputer+"~"+numberOfEmployee+"~"+salesTax+"~"+consumables;
			}else{
				return null;
			}
	 }
	 
	 public String checkForResourceTransFerWorkTicket(String sessionCorpID ,String shipNumber, String workorder)
	 {
		 String query ="select distinct ticket ,workTicketId ,date,targetActual from operationsintelligence where status is true and shipNumber='" + shipNumber + "' and workorder='" + workorder + "' and corpID='"+sessionCorpID +"' and type <> 'T' and quantitiy <> 0 and ticketStatus is null and (ticket <> null || ticket <> '')";	 
	     String ticketForOi="";
	     List al=getSession().createSQLQuery(query).list();
		 if(al!=null && !al.isEmpty() && al.get(0)!=null){
			 Iterator itr=al.iterator();
			 while(itr.hasNext()){
				 Object obj[]=(Object[])itr.next();
				 if(obj[0] != null && obj[1] != null)
				 {
				 ticketForOi=obj[0]+"~"+obj[1]+"~"+obj[2]+"~"+obj[3];
				 }
	 }}
			 return ticketForOi;
		 }
	 
	public void updateOperationsIntelligenceItemsJbkEquipId(Long id ,Long itemsJbkEquipId,String shipNumber,String sessionCorpID)
	{
		 getHibernateTemplate().bulkUpdate("update OperationsIntelligence set itemsJbkEquipId='"+itemsJbkEquipId+"' where id ='"+id+"' and corpID='"+sessionCorpID +"' and shipNumber='"+ shipNumber +"' "); 
	}
	public List scopeOfWorkOrder(String shipNumber,String sessionCorpID ,String resouceTemplate)
	 {
		String query = null;
		if(resouceTemplate==null){
			resouceTemplate="";
		}
		 String workorderForName="";
		 String []workorderNumber = resouceTemplate.split(",");
		int workorderLength = workorderNumber.length;
		for(String workOrderValue :workorderNumber ){
			if(workorderForName.equalsIgnoreCase("")){
				workorderForName="'"+workOrderValue.trim()+"'";
			}else{
				workorderForName = workorderForName +",'"+workOrderValue.trim()+"'";
			}
		}
		if(resouceTemplate.equalsIgnoreCase("A") || resouceTemplate.equalsIgnoreCase("")){
		query="from OperationsIntelligence where status is true and  shipNumber='" + shipNumber + "' and corpID='"+sessionCorpID +"' and quantitiy <> 0 and ticketStatus is null";
		}else{
			query="from OperationsIntelligence where status is true and  shipNumber='" + shipNumber + "' and corpID='"+sessionCorpID +"' and quantitiy <> 0 and ticketStatus is null AND workorder in ("+workorderForName+") ";
		}
		 return getHibernateTemplate().find(query);	 
	 }
	
	 public String checkForTicketBasedOnWorkOrder(String sessionCorpID ,String shipNumber, String workorder)
	 {
		 String query ="select distinct ticket from operationsintelligence where status is true and shipnumber='"+shipNumber+"' and quantitiy <> 0 and ticketStatus is null and type <> 'T' and workorder='"+workorder+"' and ticket is not null";	 
	     String ticketForOi="";
	     List al=getSession().createSQLQuery(query).list();
		 if(al!=null && !al.isEmpty() && al.get(0)!=null){
			 ticketForOi =al.toString().replaceAll("[\\[\\](){}]","");
	 }
			 return ticketForOi;
		 }
	 public List distinctWorkOrderForOI(String shipNumber,String sessionCorpID,String flag )
	 {
		 List distinctWorkOrderList = new ArrayList();
		 String query = "";
		 if(flag.equalsIgnoreCase("forTaskDetails")){
			 query ="select o.id,o.workorder,t.ticket,t.targetActual,t.reviewStatus,t.invoiceNumber,t.service,o.scopeOfWorkOrder,o.date,o.workTicketId,o.corpID,o.shipNumber from  operationsintelligence o left join workticket t ON  t.ticket=o.ticket and t.corpID =o.corpID where o.corpID='"+sessionCorpID+"' and t.corpID ='"+sessionCorpID+"' and o.status is true and o.shipnumber='" + shipNumber + "' and quantitiy <> 0 and type <>'T' and  ticketStatus is null and o.ticket is not null and workOrder is not null GROUP BY workorder,ticket";
		 }else{
          query ="select o.id,o.workorder,t.ticket,t.targetActual,t.reviewStatus,t.invoiceNumber,t.service,o.scopeOfWorkOrder,o.date,o.workTicketId,o.corpID,o.shipNumber from  operationsintelligence o left join workticket t ON  t.ticket=o.ticket and t.corpID =o.corpID where o.corpID='"+sessionCorpID+"' and t.corpID ='"+sessionCorpID+"' and  o.status is true and o.shipnumber='" + shipNumber + "' and quantitiy <> 0 and ticketStatus is null and workOrder is not null GROUP BY workorder,ticket";	 
		 }
         List listMasterList =   this.getSession().createSQLQuery(query).list();
         Iterator it = listMasterList.iterator();
         while(it.hasNext())
         {
        	 String targetActual = "";
        	Object[] obj = (Object[])it.next(); 
        	DistinctWorkOrderDTO distinctWorkOrderDTO = new DistinctWorkOrderDTO();
        	if (obj[0] == null) {
        		distinctWorkOrderDTO.setId("");
			} else {
				distinctWorkOrderDTO.setId(obj[0].toString());
			}
        	if (obj[1] == null) {
        		distinctWorkOrderDTO.setWorkorder("");
        	}else{
        		distinctWorkOrderDTO.setWorkorder(obj[1].toString());
        	}
        	
        	if (obj[2] == null) {
        		distinctWorkOrderDTO.setTicket("");
        	}else{
        		distinctWorkOrderDTO.setTicket(obj[2].toString());
        	}
        	
        	if (obj[3] == null) {
        		distinctWorkOrderDTO.setTargetActual("");
        	}else{
        		if(obj[3].toString().equalsIgnoreCase("T")){targetActual = "Target";}
        		if(obj[3].toString().equalsIgnoreCase("A")){targetActual = "Actual";}
        		if(obj[3].toString().equalsIgnoreCase("C")){targetActual = "Cancelled";}
        		if(obj[3].toString().equalsIgnoreCase("D")){targetActual = "Dispatching";}
        		if(obj[3].toString().equalsIgnoreCase("F")){targetActual = "Forecasting";}
        		if(obj[3].toString().equalsIgnoreCase("R")){targetActual = "Rejected";}
        		if(obj[3].toString().equalsIgnoreCase("P")){targetActual = "Pending";}
        		distinctWorkOrderDTO.setTargetActual(targetActual);
        	}
        	
        	if (obj[4] == null) {
        		distinctWorkOrderDTO.setReviewStatus("");
        	}else{
        		distinctWorkOrderDTO.setReviewStatus(obj[4].toString());
        	}
        	if (obj[5] == null) {
        		distinctWorkOrderDTO.setInvoiceNumber("");
        	}else{
        		distinctWorkOrderDTO.setInvoiceNumber(obj[5].toString());
        	}
        	
        	if (obj[6] == null) {
        		distinctWorkOrderDTO.setService("");
        	}else{
        		distinctWorkOrderDTO.setService(obj[6].toString());
        	}
        	
        	if (obj[7] == null) {
        		distinctWorkOrderDTO.setScopeOfWorkOrder("");
        	}else{
        		distinctWorkOrderDTO.setScopeOfWorkOrder(obj[7].toString());
        	}
        	
        	if (obj[8] == null) {
        		distinctWorkOrderDTO.setDate("");
        	}else{
        		distinctWorkOrderDTO.setDate(obj[8]);
        	}
        	
        	if (obj[9] == null) {
        		distinctWorkOrderDTO.setWorkTicketId("");
        	}else{
        		distinctWorkOrderDTO.setWorkTicketId(obj[9]);
        	}
        	
        	if (obj[10] == null) {
        		distinctWorkOrderDTO.setCorpId("");
        	}else{
        		distinctWorkOrderDTO.setCorpId(obj[10]);
        	}
        	
        	if (obj[11] == null) {
        		distinctWorkOrderDTO.setShipNumber("");
        	}else{
        		distinctWorkOrderDTO.setShipNumber(obj[11]);
        	}
        	
        	distinctWorkOrderList.add(distinctWorkOrderDTO);
         }
         
	     return distinctWorkOrderList;
	 }
	 
	 public List distinctWOForOI(String shipNumber,String sessionCorpID,String flag,String wo)
	 {     
		// List distinctWorkOrderList = new ArrayList();
		 String query = "";
			 query ="select workorder from  operationsintelligence where status is true and shipnumber='" + shipNumber + "' and workorder not in ('"+wo+"') and quantitiy <> 0 and type <>'T' and  ticketStatus is null and ticket is not null and workOrder is not null GROUP BY workorder,ticket";
         List listMasterList =   this.getSession().createSQLQuery(query).list();
         return listMasterList;
         
	 }
	 
	 public List findResourcesByWorkOrder(String workorder,String shipNumber)
	 {
		String workorderForName="";
		 String []workorderNumber = workorder.split(",");
			int workorderLength = workorderNumber.length;
			for(String workOrderValue :workorderNumber )
			{
				workOrderValue=workOrderValue.trim();
				if(workorderForName == "")
				{
					workorderForName="workOrder = '" + workOrderValue + "'";
				}else{
					workorderForName = workorderForName +" or "+"workOrder = '" + workOrderValue + "'";
				}
			}
		 
		 return getHibernateTemplate().find("from OperationsIntelligence where status is true and  shipNumber='" + shipNumber + "' and quantitiy <> 0 and ticketStatus is null and workOrder is not null and (" + workorderForName + ") ");	
	 }
	 public String scopeByWorkOrder(String workorder ,String shipNumber)
	 {
		 String query ="select scopeOfWorkOrder from operationsintelligence where status is true and  workorder='"+workorder+"' and shipNumber='"+shipNumber+"' and scopeOfWorkOrder is not null Limit 1";	 
	     String ticketForOi="";
	     List al=getSession().createSQLQuery(query).list();
		 if(al!=null && !al.isEmpty() && al.get(0)!=null){
			 ticketForOi =al.toString().replaceAll("[\\[\\](){}]","");
	 }
			 return ticketForOi;
		 }
	 public void updateCalculationOfOI(String calculateOI,String shipNumber){
		 if(calculateOI!=null && !calculateOI.equalsIgnoreCase("")){
			 String updateOI ="estimatedTotalExpenseForOI = '" + calculateOI.split(",")[0] + "'"+", estimatedTotalRevenueForOI = '" + calculateOI.split(",")[1] + "'"+
			 ", estimatedGrossMarginForOI = '" + calculateOI.split(",")[2] + "'"+", estimatedGrossMarginPercentageForOI = '" + calculateOI.split(",")[3] + "'"+
			 ", revisedTotalExpenseForOI = '" + calculateOI.split(",")[4] + "'"+", revisedTotalRevenueForOI = '" + calculateOI.split(",")[5] + "'" +
			 ", revisedGrossMarginForOI = '" + calculateOI.split(",")[6] + "'"+", revisedGrossMarginPercentageForOI = '" + calculateOI.split(",")[7] + "'";
			 
			 getHibernateTemplate().bulkUpdate("update OperationsIntelligence set "+ updateOI +" where shipNumber='"+ shipNumber +"' ");
		 }
	 }
	 
		public void updateSalesTax(Double salesTaxForOI, String shipNumber,String salesTaxFieldName) {
			 getHibernateTemplate().bulkUpdate("update OperationsIntelligence set "+salesTaxFieldName+"='"+salesTaxForOI+"' where shipNumber='"+ shipNumber +"' "); 
			 /*if(salesTaxFieldName.equalsIgnoreCase("revisionValuation") && salesTaxForOI.doubleValue()>0){
				 getHibernateTemplate().bulkUpdate("update Billing set totalInsrVal='"+salesTaxForOI+"' where shipNumber='"+ shipNumber +"' ");
			 }else{
				 getHibernateTemplate().bulkUpdate("update Billing set totalInsrVal='"+salesTaxForOI+"' where shipNumber='"+ shipNumber +"' and (totalInsrVal is null or totalInsrVal='')  "); 
			 }*/
		}
		
		public List showDistinctWorkOrderForCopy(String shipNumber,String sessionCorpID )
		{
			 return getHibernateTemplate().find("from OperationsIntelligence where corpID='"+sessionCorpID +"' and  status is true and  shipNumber='" + shipNumber + "' and quantitiy <> 0 and ticketStatus is null and workOrder is not null group by workorder order by workorder");	
		}
	 
		public List copyWorkOrderToResource(String distinctWorkOrder,String shipNumber)
		{
			 String workorderForName="";
			 String []workorderNumber = distinctWorkOrder.split(",");
			for(String workOrderValue :workorderNumber ){
				if(workorderForName.equalsIgnoreCase("")){
					workorderForName="'"+workOrderValue.trim()+"'";
				}else{
					workorderForName = workorderForName +",'"+workOrderValue.trim()+"'";
				}
			}
			 return getHibernateTemplate().find("from OperationsIntelligence where  status is true and shipNumber='" + shipNumber + "' and quantitiy <> 0 and ticketStatus is null and workorder in (" + workorderForName + ") order by workorder,displayPriority desc,ticket,type,description");	
		}
		
		public String FindMaxWorkOrder(String shipNumber)
		{
			 String query ="select workorder from operationsintelligence where shipNumber='" + shipNumber + "' and quantitiy <> 0 and ticketStatus is null order by workorder desc limit 1";
			String[] scopeOfWO;
			String maxWO = "";
			List list = getSession().createSQLQuery(query).list();
			if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase(""))
			{
				scopeOfWO = list.get(0).toString().split("_");
				Integer workOrderNumber = Integer.parseInt(scopeOfWO[1].toString())+1;
				if(workOrderNumber < 10)
				{
				maxWO = "WO_0"+String.valueOf(workOrderNumber);
				}else{
					maxWO = "WO_"+String.valueOf(workOrderNumber);
				}
			}
			 return maxWO;
		}
		
		public List FindValueFromOI(String shipNumber){
		String query ="select concat(ifNULL(estmatedSalesTax,0.00),'~',ifNULL(estmatedConsumables,0.00),'~',ifNULL(estmatedValuation,0.00),'~',ifNULL(revisionSalesTax,0.00),'~',ifNULL(revisionConsumables,0.00),'~',ifNULL(revisionValuation,0.00)"
				+ " ,'~',ifNULL(estimatedTotalExpenseForOI,0.00),'~',ifNULL(estimatedTotalRevenueForOI,0.00),'~',ifNULL(estimatedGrossMarginForOI,0.00),'~',ifNULL(estimatedGrossMarginPercentageForOI,0.00)"
				+ " ,'~',ifNULL(revisedTotalExpenseForOI,0.00),'~',ifNULL(revisedTotalRevenueForOI,0.00),'~',ifNULL(revisedGrossMarginForOI,0.00),'~',ifNULL(revisedGrossMarginPercentageForOI,0.00),'~',ifNULL(estmatedAB5Surcharge,0.00),'~',ifNULL(revisionaB5Surcharge,0.00)) "
				+ " from operationsintelligence where status is true and shipNumber='" + shipNumber + "' and quantitiy <> 0 and ticketStatus is null order by workorder desc limit 1";
		List list = getSession().createSQLQuery(query).list();
		if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase(""))
		{
			return list;
		}else{
			return null;
		}
		}
		
		public void changeStatusOfWorkOrder(String workOrderGroup, String shipNumber,String updatedBy,String itemsJbkEquipId){
			String query = "";
			String ticketNumber = "";
			String rowCountVal = "";
			List workOrderList = new ArrayList();
			if(workOrderGroup !=null && !workOrderGroup.equalsIgnoreCase("")){
				String workOrderQuery = "select count(*),ticket from operationsintelligence where shipNumber='"+shipNumber+"' and workorder= "+workOrderGroup+" and type <> 'T' and ticket is not null and (quantitiy <> 0 or quantitiy is null) and (ticketStatus is null or ticketStatus='')";
				workOrderList = getSession().createSQLQuery(workOrderQuery).list();
				if(workOrderList!=null && !workOrderList.isEmpty()){
				//query ="select ticket from operationsintelligence where status is true and shipNumber='"+shipNumber+"'  and workorder="+workOrderGroup+" and type <> 'T' and ticket is not null ";	
					Iterator itr = workOrderList.iterator();
					while(itr.hasNext()){
						Object obj[]=(Object[])itr.next();
						if(obj[0]!=null){
							rowCountVal = obj[0].toString(); 
						 }
						 if(obj[1]!=null){
							 ticketNumber = obj[1].toString(); 
						 }
					}
				if(rowCountVal.equalsIgnoreCase("1") && !ticketNumber.equalsIgnoreCase("")){
					getHibernateTemplate().bulkUpdate("update WorkTicket set targetActual='C',updatedBy='System :"+updatedBy+"', updatedOn=now() where shipNumber='"+shipNumber+"' and ticket = '"+ticketNumber+"' and (invoiceNumber is null or invoiceNumber='') "); 	
				}
				if(!ticketNumber.equalsIgnoreCase("")){
					getHibernateTemplate().bulkUpdate("delete from ItemsJbkEquip where id= "+itemsJbkEquipId+" and ticket = '"+ticketNumber+"' and shipNum='"+shipNumber+"' ");
				}
			}
			}
		}
		
		public void changeStatusOfWorkTicketFromDel(Long id , String shipNumber,String updatedBy,Long itemsJbkEquipId)
		{
			 String type= "";
			 String workOrderForTP="";
			String idOfOI = "select type,workorder from operationsintelligence where status is true and shipnumber='"+shipNumber+"' and id='"+id+"' and ticket is not null";
			List TypeById = getSession().createSQLQuery(idOfOI).list();
			 if(TypeById!=null && !TypeById.isEmpty() && TypeById.get(0)!=null && !TypeById.get(0).toString().equalsIgnoreCase(""))
			 {
				/* type =TypeById.toString().replaceAll("[\\[\\](){}]","");*/
				 Iterator itr=TypeById.iterator();
				 while(itr.hasNext()){
					 Object obj[]=(Object[])itr.next();
					 if(obj[0] != null && obj[1] != null) 
					 {
						 type=obj[0].toString();
						 workOrderForTP = obj[1].toString();
					 }
			 }}
			 if(type.equalsIgnoreCase("T"))
			 {
				 String queryForWO ="select ticket from operationsintelligence where status is true and shipnumber='"+shipNumber+"' and workorder='"+workOrderForTP+"' and type='T' and id='"+id+"' and quantitiy <> 0 and ticket is not null";
					List list = getSession().createSQLQuery(queryForWO).list();
					String ticket = "";	
					 if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase(""))
						{
							Iterator it = list.iterator();
							while(it.hasNext())
							{
							Object obj = (Object)it.next();
								 ticket = obj.toString();
								
							}	}
					 getHibernateTemplate().bulkUpdate("update WorkTicket set targetActual='C',updatedBy='System :"+updatedBy+"', updatedOn=now() where shipNumber='"+ shipNumber +"' and ticket='"+ticket+"' and (invoiceNumber is null or invoiceNumber='') "); 
			 }else{
			 String queryForWO ="select workorder from operationsintelligence where status is true and shipNumber='" + shipNumber + "' and quantitiy <> 0 and ticketStatus is null and type <> 'T' and workOrder is not null group by workorder order by workorder";
			 List list = getSession().createSQLQuery(queryForWO).list();
			String distinctWo = "";	
			 if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase(""))
				{
					Iterator it = list.iterator();
					while(it.hasNext())
					{
					Object obj = (Object)it.next();
						String workOrder = obj.toString();
						if(distinctWo.isEmpty())
						{
							distinctWo="'"+workOrder+"'";
						}else{
							distinctWo = distinctWo+",'"+workOrder+"'";;
						}
					}	
				}
			 String itemsJbkEquipIdVal = String.valueOf(itemsJbkEquipId);
			 changeStatusOfWorkOrder(distinctWo , shipNumber, updatedBy,itemsJbkEquipIdVal);
		}}
		
		public void updateWorkOrder(String workorder,String shipNumber,String usertype){
			Integer temp = Integer.parseInt(workorder.split("_")[1])+1;
			String updateWO;
			if(temp <10){
			updateWO = "WO_0"+temp.intValue();
			}else{
		    updateWO = "WO_"+temp.intValue();	
			}
			getSession().createSQLQuery("update operationsintelligence set workorder='"+updateWO+"', updatedBy='"+usertype+"' where workorder='"+workorder +"'  and  shipNumber='"+ shipNumber +"' ").executeUpdate();	
			}
		
		public void changeStatusOfWorkOrderForThirdParty(String workorder,String shipNumber,String updatedBy, Long ticket)
		{
			/*String queryForWO ="select ticket from operationsintelligence where status is true and shipnumber='"+shipNumber+"' and workorder='"+workorder+"' and type='T' and quantitiy = 0 and ticket is not null";
			List list = getSession().createSQLQuery(queryForWO).list();
			String ticket = "";	
			 if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase(""))
				{
					Iterator it = list.iterator();
					while(it.hasNext())
					{
					Object obj = (Object)it.next();
						 ticket = obj.toString();
						
					}	}*/
			 getHibernateTemplate().bulkUpdate("update WorkTicket set targetActual='C',updatedBy='System :"+updatedBy+"', updatedOn=now() where shipNumber='"+ shipNumber +"' and ticket='"+ticket+"' and (invoiceNumber is null or invoiceNumber='') and targetActual != 'C' "); 	
		}
		
		public List distinctWOForInsert(String action,String shipNumber,String workorder){
			if(action.equalsIgnoreCase("Before"))
			{
			Integer temp = Integer.parseInt(workorder.split("_")[1])-1;
			String updateWO;
			if(temp==0)
			{
			temp=1;
			}
			if(temp <10){
			updateWO = "WO_0"+temp.intValue();
			}else{
		    updateWO = "WO_"+temp.intValue();	
			}
			System.out.println("from OperationsIntelligence where status is true and  shipNumber='" + shipNumber + "'  and quantitiy <> 0 and workorder > '"+updateWO+"' group by workorder order by workorder  desc");
			 return getHibernateTemplate().find("from OperationsIntelligence where status is true and  shipNumber='" + shipNumber + "'  and quantitiy <> 0 and workorder >= '"+updateWO+"' group by workorder order by workorder  desc"); 
			}
			else
			{
				System.out.println("from OperationsIntelligence where status is true and  shipNumber='" + shipNumber + "'  and quantitiy <> 0 and workorder > '"+workorder+"' group by workorder order by workorder  desc"); 
				 return getHibernateTemplate().find("from OperationsIntelligence where status is true and  shipNumber='" + shipNumber + "'  and quantitiy <> 0 and workorder > '"+workorder+"' group by workorder order by workorder  desc"); 
			}
			 }
		
		public void updateOIDate(Long ticket,String shipNumber,String date1,String updatedBy,String targetActual){
			getSession().createSQLQuery("update operationsintelligence set date='"+date1+"',updatedBy='"+updatedBy+"',targetActual='"+targetActual+"' where ticket='"+ticket +"' and shipNumber='"+ shipNumber +"' ").executeUpdate();	
			}
		
		public void updateOIResourceTicketAndDate(Long ticket,String shipNumber,String updatedBy){
			getSession().createSQLQuery("update operationsintelligence set ticket=null,date =null,updatedBy='"+updatedBy+"' where ticket='"+ticket +"' and shipNumber='"+ shipNumber +"' ").executeUpdate();
		}
		
		
		public List OIListForAccount(Long shipNumber){
			 return getHibernateTemplate().find("from OperationsIntelligence where status is true and  shipNumber='" + shipNumber + "' and quantitiy <> 0  group by workorder order by workorder  desc"); 
		 }	
		
		public List taskDetailsListForOI(String sessionCorpID, Long ticket){
			 return getHibernateTemplate().find("from TaskDetail where ticket='"+ ticket +"' and corpId='"+ sessionCorpID +"' ");
		}
		
		public String getScopeWithValues(String shipNumber, String workorder,String type){
			String value="";
			String scopeValue="";
		String query = "select concat(FLOOR(quantitiy),' ',description,'~',if(scopeofworkorder is null ,'',scopeofworkorder)) FROM operationsintelligence where status is true and  shipnumber = '"+shipNumber+"' and workorder='"+workorder+"' and ( quantitiy <> 0 or quantitiy is null) and type <> 'T'  and type not in ('M','E') and ticketStatus is null order by type desc";
		List list = this.getSession().createSQLQuery(query).list();
		 if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase("")){
		int len = list.size();
			 Iterator it = list.iterator();
		while(it.hasNext()){
			len = len-1;
			Object obj = (Object)it.next();
			String temp = obj.toString().split("~")[0];
			try{
			scopeValue =  obj.toString().split("~")[1];
			}catch(Exception e){
				scopeValue="";	
			}
			if(value.isEmpty()){
				value= obj.toString().split("~")[0];
			}else{
				if(len!=0){
				value = value+", " +obj.toString().split("~")[0];
				}else{
					value = value+" and " +obj.toString().split("~")[0];
				}
			}
		}
		
		 }
			value = value+"~"+scopeValue;
			
			return value;
		}
		
		public void getScopeWithValues(String shipNumber, String workorder, String type, Long ticket){
			try{
			String value="";
			String scopeValue="";
		String query = "select concat(FLOOR(quantitiy),' ',description,'~',if(scopeofworkorder is null ,'',scopeofworkorder)) FROM operationsintelligence where  status is true and shipnumber = '"+shipNumber+"' and workorder='"+workorder+"' and ( quantitiy <> 0 or quantitiy is null) and type <> 'T' and type not in ('M','E') and ticketStatus is null order by type desc";
		List list = this.getSession().createSQLQuery(query).list();
		 if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase("")){
		int len = list.size();
			 Iterator it = list.iterator();
		while(it.hasNext()){
			len = len-1;
			Object obj = (Object)it.next();
			String temp = obj.toString().split("~")[0];
			try{
			scopeValue =  obj.toString().split("~")[1];
			}catch(Exception e){
				scopeValue="";	
			}
			if(value.isEmpty()){
				value= obj.toString().split("~")[0];
			}else{
				if(len!=0){
				value = value+", " +obj.toString().split("~")[0];
				}else{
					value = value+" and " +obj.toString().split("~")[0];
				}
			}
		}
		
		 }
			//value = value+" "+scopeValue;
			getHibernateTemplate().bulkUpdate("update WorkTicket set crewVehicleSummary='"+StringEscapeUtils.escapeSql(value)+"' where shipNumber='"+ shipNumber +"' and ticket='"+ticket+"' ");
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		
		 public String valueOfCommentForRevised(Long id,String shipNumber, String commentField) {
			 ArrayList<String> list = new ArrayList<String>();
			 list =  (ArrayList<String>) getHibernateTemplate().find("SELECT "+commentField+" from OperationsIntelligence where status is true and  id = "+id+" and shipNumber='"+ shipNumber +"' ");
			String listString = "";
			 if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase("")){
			for (String s : list)
			{
			    listString += s + "\t";
			}}
			return listString.trim();
		 }
		 
		 public void saveCmtForRevised(Long id, String shipNumber,String value, String commentField){
			 getHibernateTemplate().bulkUpdate("UPDATE OperationsIntelligence set "+commentField+"='"+StringEscapeUtils.escapeSql(value)+"' where id = "+id+" and shipNumber='"+ shipNumber +"' ");
		 }
		 
		 public List crewListBYTicket(Long ticket, String sessionCorpID){
			String username="";		
			String query =  "select userName from timesheet where corpid='"+sessionCorpID+"' and ticket= "+ticket+" ";
			List list = this.getSession().createSQLQuery(query).list();
					 if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase("")){
							for (Object value : list)
							{
							 if(value != null && !(username.toString().equalsIgnoreCase(""))){
								 username = username +",'"+value.toString().trim()+"'";
							}else{
								username = "'"+value.toString().trim()+"'";
							}
							}
		 
					 }
				return getHibernateTemplate().find(" From Payroll where username in ("+username+") and  corpid='"+sessionCorpID+"' ");			 
					 }
		 
		 public List addWorkOrderToResource(String shipNumber){
			 return getHibernateTemplate().find("from OperationsIntelligence where  status is true and  shipNumber='" + shipNumber + "' and ( quantitiy <> 0 or quantitiy is null) and ticketStatus is null order by workorder,displayPriority desc,ticket,type,description ");
		 }

	 public class DistinctWorkOrderDTO{
		 private Object id;
		 private Object workorder;
		 private Object ticket;
		 private Object targetActual;
		 private Object reviewStatus;
		 private Object invoiceNumber;
		 private Object service;
		 private Object scopeOfWorkOrder;
		 private Object date;
		 private Object workTicketId;
		 private Object corpId;
		 private Object shipNumber;
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getWorkorder() {
			return workorder;
		}
		public void setWorkorder(Object workorder) {
			this.workorder = workorder;
		}
		public Object getTicket() {
			return ticket;
		}
		public void setTicket(Object ticket) {
			this.ticket = ticket;
		}
		public Object getTargetActual() {
			return targetActual;
		}
		public void setTargetActual(Object targetActual) {
			this.targetActual = targetActual;
		}
		public Object getReviewStatus() {
			return reviewStatus;
		}
		public void setReviewStatus(Object reviewStatus) {
			this.reviewStatus = reviewStatus;
		}
		public Object getInvoiceNumber() {
			return invoiceNumber;
		}
		public void setInvoiceNumber(Object invoiceNumber) {
			this.invoiceNumber = invoiceNumber;
		}
		public Object getService() {
			return service;
		}
		public void setService(Object service) {
			this.service = service;
		}
		public Object getScopeOfWorkOrder() {
			return scopeOfWorkOrder;
		}
		public void setScopeOfWorkOrder(Object scopeOfWorkOrder) {
			this.scopeOfWorkOrder = scopeOfWorkOrder;
		}
		public Object getDate() {
			return date;
		}
		public void setDate(Object date) {
			this.date = date;
		}
		public Object getWorkTicketId() {
			return workTicketId;
		}
		public void setWorkTicketId(Object workTicketId) {
			this.workTicketId = workTicketId;
		}
		public Object getCorpId() {
			return corpId;
		}
		public void setCorpId(Object corpId) {
			this.corpId = corpId;
		}
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		
	 }
	 
	 public List findDataForCopyEstimate(String shipNumber,String sessionCorpID){
		 String ticketNumber = findInvoicedWorkTicketNumber(shipNumber);
		 List copyEstimateList = new ArrayList();
		 String query = "";
		 if(ticketNumber!=null && !ticketNumber.equalsIgnoreCase("")){
			 query="select group_concat(id),workorder,ticket,date_format(date,'%d-%b-%Y') from operationsintelligence where  status is true and shipNumber='"+shipNumber+"' and  corpid='"+sessionCorpID+"' and (ticket not in ("+ ticketNumber+") or ticket is null) and estimatedquantity >0.00 and (quantitiy<>revisedquantity or estimatedexpense<>revisionexpense or estimatedrevenue<>revisionrevenue or esthours<>revisionquantity or numberOfComputer<>revisionScopeOfNumberOfComputer or numberOfEmployee<>revisionScopeOfNumberOfEmployee or salesTax<>revisionScopeOfSalesTax or consumables<>revisionScopeOfConsumables) and ticketStatus is null group by workorder order by workorder";
		 }else{
			 query="select group_concat(id),workorder,ticket,date_format(date,'%d-%b-%Y') from operationsintelligence where  status is true and shipNumber='"+shipNumber+"' and  corpid='"+sessionCorpID+"' and estimatedquantity >0.00 and (quantitiy<>revisedquantity or estimatedexpense<>revisionexpense or estimatedrevenue<>revisionrevenue or esthours<>revisionquantity or numberOfComputer<>revisionScopeOfNumberOfComputer or numberOfEmployee<>revisionScopeOfNumberOfEmployee or salesTax<>revisionScopeOfSalesTax or consumables<>revisionScopeOfConsumables) and ticketStatus is null group by workorder order by workorder";
		 }
		 List list = this.getSession().createSQLQuery(query).list();
		 Iterator it = list.iterator();
         while(it.hasNext()){
        	 String targetActual = "";
        	Object[] obj = (Object[])it.next(); 
        	DistinctWorkOrderDTO dto = new DistinctWorkOrderDTO();
        	if (obj[0] == null) {
        		dto.setId("");
			}else{
				dto.setId(obj[0].toString());
			}
        	if(obj[1] == null){
        		dto.setWorkorder("");
        	}else{
        		dto.setWorkorder(obj[1].toString());
        	}
        	if (obj[2] == null) {
        		dto.setTicket("");
        	}else{
        		dto.setTicket(obj[2].toString());
        	}
        	if (obj[3] == null) {
        		dto.setDate("");
        	}else{
        		dto.setDate(obj[3].toString());
        	}
        	copyEstimateList.add(dto);
         }
         return copyEstimateList;
	 }

		public List updateSelectiveInvoice(Long id, boolean oIStatus, String sessionCorpID,Long itemsJbkEquipId,String shipNumber,String ticket,String workorder,boolean estConsumCheck,BigDecimal estConsumableFromPP,boolean revConsumCheck,BigDecimal revConsumableFromPP,BigDecimal aB5Surcharge,boolean editAB5Surcharge,BigDecimal revisionScopeOfAB5Surcharge,boolean revisionaeditAB5Surcharge) {
		try {
			if(oIStatus==false){
				int i=getHibernateTemplate().bulkUpdate("update OperationsIntelligence set status=" +oIStatus+ ",consumables=0.00,revisionScopeOfConsumables=0.00,estimateConsumablePercentage=false,revisionConsumablePercentage=false,aB5Surcharge=0.00,editAB5Surcharge=false,revisionScopeOfAB5Surcharge=0.00,revisionaeditAB5Surcharge=false, updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now()  where id='"+id+"' and corpID='"+sessionCorpID+"' "); 
			}else{
				int i=getHibernateTemplate().bulkUpdate("update OperationsIntelligence set status=" +oIStatus+ ", updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now()  where id='"+id+"' and corpID='"+sessionCorpID+"' "); 
			}
			if(estConsumCheck){
				
			}
			else if(estConsumableFromPP.compareTo(BigDecimal.ZERO) != 0){
				updateEstimateAndRevisionConsumablesWithParnerPercentage(shipNumber,workorder,"Estimate", estConsumableFromPP);
			}else{
				updateEstimateAndRevisionConsumablesWithOutParnerPercentage(shipNumber,workorder,"Estimate");
			}
			
			 if(!editAB5Surcharge){
				 updateEstimateAndRevisionAB5SurchargeWithPercentage(shipNumber, workorder, "Estimate", aB5Surcharge);  
			 }
			if(revConsumCheck){
				
			}
			else if(revConsumableFromPP.compareTo(BigDecimal.ZERO) != 0){
				updateEstimateAndRevisionConsumablesWithParnerPercentage(shipNumber,workorder,"Revision", estConsumableFromPP);
			}else{
				updateEstimateAndRevisionConsumablesWithOutParnerPercentage(shipNumber,workorder,"Revision");
			}
			
			 if(!revisionaeditAB5Surcharge){
				 updateEstimateAndRevisionAB5SurchargeWithPercentage(shipNumber, workorder, "Revision", revisionScopeOfAB5Surcharge);  
			 }
			//updateEstimateAndRevisionConsumables(shipNumber, workorder);
			if(oIStatus==false && (ticket!=null && !ticket.equalsIgnoreCase(""))){
				getSession().createSQLQuery("delete from itemsjbkequip where ticket='"+ticket+"' and id ='"+itemsJbkEquipId+"' and shipNum='"+shipNumber+"' ").executeUpdate();
			}
			return null;
		} catch (Exception e) {
	    	 e.printStackTrace();
		}
		return null;
	}
	
	public String findRevisionScopeOfWorkOrder(Long id,String shipNumber){
		 String query ="select if(revisionScopeOfWorkOrder is null,'aa',revisionScopeOfWorkOrder ) as scopeOfWorkOrder, if(revisionScopeOfNumberOfComputer is null,'aa',revisionScopeOfNumberOfComputer) as numberOfComputer, if(revisionScopeOfNumberOfEmployee is null,'aa',revisionScopeOfNumberOfEmployee) as numberOfEmployee, if(revisionScopeOfSalesTax is null,'aa',revisionScopeOfSalesTax ) as salesTax, if(revisionScopeOfConsumables is null,'aa',revisionScopeOfConsumables) as consumables from operationsintelligence where shipNumber='" + shipNumber + "' and id='"+id +"'";
				String scopeOfWO = "";
				String numberOfComputer= "";
				String numberOfEmployee = "";
				String salesTax = "";
				String consumables = "";
				List list = getSession().createSQLQuery(query).list();
				if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase("")){
					 Iterator itr=list.iterator();
					 while(itr.hasNext()){
						 	Object obj[]=(Object[])itr.next();
						 	scopeOfWO = obj[0].toString();
							numberOfComputer = obj[1].toString();
							numberOfEmployee = obj[2].toString();
							salesTax = obj[3].toString();
							consumables = obj[4].toString();
					 }
					
				}
				return scopeOfWO+"~"+numberOfComputer+"~"+numberOfEmployee+"~"+salesTax+"~"+consumables;
	 }
	public void updateExpenseRevenueConsumables(String shipNumber){
		String updateFields="";
		try {
		//String query ="select sum(consumables),sum(revisionScopeOfConsumables) from operationsintelligence where shipnumber='"+shipNumber+"' and type in('C','V') and status is true";
		String query=" select sum(consumables),sum(revisionScopeOfConsumables) from operationsintelligence "
				+ " inner join ( select shipnumber,id,(sum(consumables)/count(consumables)),(sum(revisionScopeOfConsumables)/count(revisionScopeOfConsumables)) "
				+ " from operationsintelligence opr where opr.shipnumber='"+shipNumber+"' and opr.type in('C','V') and opr.status is true and ( quantitiy <> 0) and (ticketStatus is null or ticketStatus='') group by workorder ) as opr "
				+ " on opr.id=operationsintelligence.id where operationsintelligence.shipnumber='"+shipNumber+"' and type in('C','V') and status is true "
				+ " and ( quantitiy <> 0) and (ticketStatus is null or ticketStatus='') group by operationsintelligence.shipnumber";
		List list = getSession().createSQLQuery(query).list();
			 Iterator itr=list.iterator();
			 while(itr.hasNext()){
				 Object obj[]=(Object[])itr.next();
				 if(obj[0]!=null){
					 updateFields = " set estmatedConsumables= '"+obj[0].toString()+"'"; 
				 }
				 if(obj[1]!=null){
					if(!updateFields.equalsIgnoreCase("")){
						updateFields = updateFields+","+" revisionConsumables = '"+obj[1].toString()+"'";
					}else{
						updateFields = " set revisionConsumables = '"+obj[1].toString()+"'";
					}
				 }
				 if(!updateFields.equalsIgnoreCase("")){
					 getSession().createSQLQuery("update operationsintelligence "+updateFields+"  where shipnumber='"+shipNumber+"' ").executeUpdate();
				 }
			}
		}catch (Exception e) {
	    	 e.printStackTrace();
		}
	}
	
	/*
	 * Bug 14390 - O&I and Ops work flow -- Stage One
	 */
	public void updateExpenseRevenueAB5Surcharge(String shipNumber){
		String updateFields="";
		try {
		//String query ="select sum(consumables),sum(revisionScopeOfConsumables) from operationsintelligence where shipnumber='"+shipNumber+"' and type in('C','V') and status is true";
		String query=" select sum(aB5Surcharge),sum(revisionScopeOfAB5Surcharge) from operationsintelligence "
				+ " inner join ( select shipnumber,id,(sum(aB5Surcharge)/count(aB5Surcharge)),(sum(revisionScopeOfAB5Surcharge)/count(revisionScopeOfAB5Surcharge)) "
				+ " from operationsintelligence opr where opr.shipnumber='"+shipNumber+"' and opr.type in('C','V') and opr.status is true and ( quantitiy <> 0) and (ticketStatus is null or ticketStatus='') group by workorder ) as opr "
				+ " on opr.id=operationsintelligence.id where operationsintelligence.shipnumber='"+shipNumber+"' and type in('C','V') and status is true "
				+ " and ( quantitiy <> 0) and (ticketStatus is null or ticketStatus='') group by operationsintelligence.shipnumber";
		List list = getSession().createSQLQuery(query).list();
		 Iterator itr=list.iterator();
		 while(itr.hasNext()){
			 Object obj[]=(Object[])itr.next();
			 if(obj[0]!=null){
				 updateFields = " set estmatedAB5Surcharge= '"+obj[0].toString()+"'"; 
			 }
			 if(obj[1]!=null){
				if(!updateFields.equalsIgnoreCase("")){
					updateFields = updateFields+","+" revisionaB5Surcharge = '"+obj[1].toString()+"'";
				}else{
					updateFields = " set revisionaB5Surcharge = '"+obj[1].toString()+"'";
				}
			 }
			 if(!updateFields.equalsIgnoreCase("")){
				 getSession().createSQLQuery("update operationsintelligence "+updateFields+"  where shipnumber='"+shipNumber+"' ").executeUpdate();
			 }
		} 

		}catch (Exception e) {
	    	 e.printStackTrace();
		}
	}
	
	/**
	 * End
	 */
	
	public List findEstimateRevisionExpenseRevenueSum(String shipNumber){
		List list = new ArrayList();
		try {
			String query ="select sum(estimatedexpense), sum(estimatedrevenue), sum(revisionexpense), sum(revisionrevenue) from operationsintelligence"
					+ " where shipNumber='"+shipNumber+"' and ( quantitiy <> 0) and (ticketStatus is null or ticketStatus='') and status is true";
			list = getSession().createSQLQuery(query).list();
		}catch (Exception e) {
	    	 e.printStackTrace();
		}
		return list;
	}
	public void updateEstimateAndRevisionConsumables(String shipNumber,String workOrder){
		String updateFields="";
		try {
		String query ="select sum(estimatedrevenue *.05),sum(revisionrevenue *.05) from operationsintelligence where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and ( quantitiy <> 0) and (ticketStatus is null or ticketStatus='')";
		List list = getSession().createSQLQuery(query).list();
			 Iterator itr=list.iterator();
			 while(itr.hasNext()){
				 Object obj[]=(Object[])itr.next();
				 if(obj[0]!=null){
					 updateFields = " set consumables= '"+obj[0].toString()+"'"; 
				 }
				 if(obj[1]!=null){
					if(!updateFields.equalsIgnoreCase("")){
						updateFields = updateFields+","+" revisionScopeOfConsumables = '"+obj[1].toString()+"'";
					}else{
						updateFields = " set revisionScopeOfConsumables = '"+obj[1].toString()+"'";
					}
				 }
				 if(!updateFields.equalsIgnoreCase("")){
					 getSession().createSQLQuery("update operationsintelligence "+updateFields+"  where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and estimateConsumablePercentage is false ").executeUpdate();
				 }
				 updateExpenseRevenueConsumables(shipNumber);
			}
		}catch (Exception e) {
	    	 e.printStackTrace();
		}
	}
	public String findDistinctWorkOrder(String shipNumber,String sessionCorpID){
		String workOrderList = "";
		String query ="select distinct(workorder) from operationsintelligence where corpID='"+sessionCorpID +"' and  shipNumber='"+shipNumber+"' ";
		List list = getSession().createSQLQuery(query).list();
		if(list!=null && !list.isEmpty() && list.get(0)!=null){
			workOrderList = list.toString().replace("[", "").replace("]", "");
		}
		return workOrderList;
	}
	
	public void updateEstimateAndRevisionConsumablesWithParnerPercentage(String shipNumber,String workOrder,String fieldName,BigDecimal consumableFromPP){
		String updateFields="";
		String query = "";
		MathContext mc = new MathContext(4);
		BigDecimal divideVal = new BigDecimal(100);
		BigDecimal consumableVal = new BigDecimal(0.00);
		try {
		if(fieldName.equalsIgnoreCase("Estimate")){
			query ="select sum(estimatedrevenue) from operationsintelligence where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and (quantitiy <> 0) and (ticketStatus is null or ticketStatus='')";
		}else{
			query ="select sum(revisionrevenue) from operationsintelligence where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and (quantitiy <> 0) and (ticketStatus is null or ticketStatus='')";
		}
		List list = getSession().createSQLQuery(query).list();
			if(list!=null && !list.isEmpty() && list.get(0)!=null){
				if(fieldName.equalsIgnoreCase("Estimate")){
					String estRevenue = list.get(0).toString();
					consumableVal = (new BigDecimal(estRevenue).multiply(consumableFromPP)).divide(divideVal, mc);
					updateFields = " set consumables= '"+consumableVal+"'";
				 }else{
					 String revRevenue = list.get(0).toString();
					 consumableVal = (new BigDecimal(revRevenue).multiply(consumableFromPP)).divide(divideVal, mc);
					 updateFields = " set revisionScopeOfConsumables= '"+consumableVal+"'";
				 }
				if(!updateFields.equalsIgnoreCase("")){
					if(fieldName.equalsIgnoreCase("Estimate")){
						getSession().createSQLQuery("update operationsintelligence "+updateFields+"  where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and (quantitiy <> 0) and estimateConsumablePercentage is false and (ticketStatus is null or ticketStatus='') ").executeUpdate();
					}else{
						getSession().createSQLQuery("update operationsintelligence "+updateFields+"  where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and (quantitiy <> 0) and revisionConsumablePercentage is false and (ticketStatus is null or ticketStatus='') ").executeUpdate();
					}
					 updateExpenseRevenueConsumables(shipNumber);
				}
			}
		}catch (Exception e) {
	    	 e.printStackTrace();
		}
	}
	
	/*
	 * Bug 14390 - O&I and Ops work flow -- Stage One
	 */
	public void updateEstimateAndRevisionAB5SurchargeWithPercentage(String shipNumber,String workOrder,String fieldName,BigDecimal consumableFromPP){
		String updateFields="";
		String query = "";
		MathContext mc = new MathContext(4);
		BigDecimal divideVal = new BigDecimal(100);
		BigDecimal surchargePercentage = new BigDecimal(0.00);
		BigDecimal surchargeVal = new BigDecimal(0.00);
		try {
		if(fieldName.equalsIgnoreCase("Estimate")){
			query ="select sum(estimatedrevenue) from operationsintelligence where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and (quantitiy <> 0) and (ticketStatus is null or ticketStatus='')";
		}else{
			query ="select sum(revisionrevenue) from operationsintelligence where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and (quantitiy <> 0) and (ticketStatus is null or ticketStatus='')";
		}
		List list = getSession().createSQLQuery(query).list();
			if(list!=null && !list.isEmpty() && list.get(0)!=null){
				if(fieldName.equalsIgnoreCase("Estimate")){
					String estRevenue = list.get(0).toString();
					surchargeVal = (new BigDecimal(estRevenue).multiply(surchargePercentage)).divide(divideVal, mc);
					updateFields = " set aB5Surcharge='"+surchargeVal+"' ";
				 }else{
					 String revRevenue = list.get(0).toString();
					 surchargeVal = (new BigDecimal(revRevenue).multiply(surchargePercentage)).divide(divideVal, mc);
					 updateFields = " set revisionScopeOfAB5Surcharge= '"+surchargeVal+"'";
				 }
				if(!updateFields.equalsIgnoreCase("")){
					if(fieldName.equalsIgnoreCase("Estimate")){
						getSession().createSQLQuery("update operationsintelligence "+updateFields+"  where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and (quantitiy <> 0) and editAB5Surcharge is false and (ticketStatus is null or ticketStatus='') ").executeUpdate();
					}else{
						getSession().createSQLQuery("update operationsintelligence "+updateFields+"  where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and (quantitiy <> 0) and revisionaeditAB5Surcharge is false and (ticketStatus is null or ticketStatus='') ").executeUpdate();
					}
					updateExpenseRevenueAB5Surcharge(shipNumber);
				}
			}
			else{
				updateExpenseRevenueAB5Surcharge(shipNumber);
			}
		}catch (Exception e) {
	    	 e.printStackTrace();
		}
	}
	
	/**
	 * End
	 */
	
	public void updateEstimateAndRevisionConsumablesWithOutParnerPercentage(String shipNumber,String workOrder,String fieldName){
		String updateFields="";
		String query = "";
		MathContext mc = new MathContext(4);
		BigDecimal divideVal = new BigDecimal(100);
		BigDecimal consumableVal = new BigDecimal(0.00);
		try {
		if(fieldName.equalsIgnoreCase("Estimate")){
			query ="select sum(estimatedrevenue *.055) from operationsintelligence where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and (quantitiy <> 0 or quantitiy is null) and (ticketStatus is null or ticketStatus='')";
		}else{
			query ="select sum(revisionrevenue *.055) from operationsintelligence where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and (quantitiy <> 0 or quantitiy is null) and (ticketStatus is null or ticketStatus='')";
		}
		List list = getSession().createSQLQuery(query).list();
			if(list!=null && !list.isEmpty() && list.get(0)!=null){
				if(fieldName.equalsIgnoreCase("Estimate")){
					String estRevenue = list.get(0).toString();
					consumableVal = new BigDecimal(estRevenue);
					updateFields = " set consumables= '"+consumableVal+"'";
				 }else{
					 String revRevenue = list.get(0).toString();
					 consumableVal = new BigDecimal(revRevenue);
					 updateFields = " set revisionScopeOfConsumables= '"+consumableVal+"'";
				 }
				if(!updateFields.equalsIgnoreCase("")){
					if(fieldName.equalsIgnoreCase("Estimate")){
						getSession().createSQLQuery("update operationsintelligence "+updateFields+"  where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and estimateConsumablePercentage is false and (quantitiy <> 0) and (ticketStatus is null or ticketStatus='')").executeUpdate();
					}else{
						getSession().createSQLQuery("update operationsintelligence "+updateFields+"  where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and revisionConsumablePercentage is false and (quantitiy <> 0) and (ticketStatus is null or ticketStatus='')").executeUpdate();	
					}					 
				}
			}
			updateExpenseRevenueConsumables(shipNumber);
		}catch (Exception e) {
	    	 e.printStackTrace();
		}
	}
	
	public Boolean findConsumableChecked(String shipNumber,String workOrder,String fieldName){
		Boolean check = false;
		List consumableCheck = getSession().createSQLQuery("select "+fieldName+" from operationsintelligence where shipnumber='"+shipNumber+"' and workorder='"+workOrder+"' and type in('C','V') and status is true and "+fieldName+" is true and (quantitiy <> 0 or quantitiy is null) and (ticketStatus is null or ticketStatus='')").list();
		if(consumableCheck!=null && !consumableCheck.isEmpty()){
			check = Boolean.parseBoolean(consumableCheck.get(0).toString());
		}
		return check;
	}
	
	/*
	 * Bug 14390 - O&I and Ops work flow -- Stage One
	 */
	public String getPresetValue(String contract, String charge, String sessionCorpID,String descript){
		String presetValue="0.00";
		
		String query="select c.quantity2preset from charges c "+
				 "inner join resourcecontractcharges r on c.contract = r.contract "+
				 "and  c.charge = r.charge "+
				 "inner join itemsjequip i on i.id = r.parentid "+
				 "where c.corpid='"+sessionCorpID+"' and c.contract='"+contract+"' "+
				 "and r.parentid= (select id from itemsjequip where descript='"+descript+"' and type='"+charge+"' and corpid='"+sessionCorpID+"') ";
				
		//List presetValueList = getSession().createSQLQuery("select quantity2preset from charges where contract='"+contract+"' and charge='"+charge+"' and corpID='"+sessionCorpID+"' ").list();
		List presetValueList = getSession().createSQLQuery(query).list();
		if(presetValueList!=null && !presetValueList.isEmpty()){
			presetValue=presetValueList.get(0).toString();
		}
		return presetValue;
	}
	/**
	 * End
	 */
	public void updateAccountLineId(String shipNumber, Long serviceOrderId, String operationsIntelligenceIds,Long accountLineId) {
		getSession().createSQLQuery("update operationsintelligence set accountLineId='"+accountLineId+"'  where shipnumber='"+shipNumber+"' and id in("+ operationsIntelligenceIds+ ")").executeUpdate();
				
	}
	public double getHoursWithValues(String shipNumber, String workorder, String type) {
		double esthoursValue=0.0;
		 
	String query = "select esthours FROM operationsintelligence where status is true and  shipnumber = '"+shipNumber+"' and workorder='"+workorder+"' and ( quantitiy <> 0 or quantitiy is null) and type = 'C'  and type not in ('M','E') and ticketStatus is null";
	List list = this.getSession().createSQLQuery(query).list();
	 if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase("")){
		 esthoursValue =Double.parseDouble(list.get(0).toString());
	 }
		 
		
		return esthoursValue;
	}
}