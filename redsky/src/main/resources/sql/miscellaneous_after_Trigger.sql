delimiter $$

CREATE trigger redsky.trigger_add_history_miscellaneous_after

AFTER INSERT on redsky.miscellaneous

for each row BEGIN

DECLARE unit LONGTEXT;
DECLARE pestimatedNetWeightKilo LONGTEXT;
DECLARE pactualNetWeightKilo LONGTEXT;
DECLARE pnetEstimateCubicMtr LONGTEXT;
DECLARE pnetActualCubicMtr LONGTEXT;
DECLARE pestimatedNetWeight LONGTEXT;
DECLARE pactualNetWeight LONGTEXT;
DECLARE pnetEstimateCubicFeet LONGTEXT;
DECLARE pnetActualCubicFeet LONGTEXT;


SET unit = " ";
SET pestimatedNetWeightKilo = "0.00 ";
SET pactualNetWeightKilo = "0.00 ";
SET pnetEstimateCubicMtr = " 0.00";
SET pnetActualCubicMtr = "0.00 ";
SET pestimatedNetWeight = " 0.00";
SET pactualNetWeight = "0.00 ";
SET pnetEstimateCubicFeet = "0.00 ";
SET pnetActualCubicFeet = "0.00 ";


select weightunit into unit from systemdefault sd where sd.corpid=NEW.corpid;

select estimatedNetWeightKilo,actualNetWeightKilo,
netEstimateCubicMtr,netActualCubicMtr
into
pestimatedNetWeightKilo,pactualNetWeightKilo,
pnetEstimateCubicMtr,pnetActualCubicMtr from
miscellaneous where id=NEW.id;


select estimatedNetWeight,actualNetWeight,
netEstimateCubicFeet,netActualCubicFeet
into
pestimatedNetWeight,pactualNetWeight,
pnetEstimateCubicFeet,pnetActualCubicFeet
 from
miscellaneous   where id=NEW.id;


if(unit='Kgs') then


update sodashboard set estweight=pestimatedNetWeightKilo,
actualweight=pactualNetWeightKilo,
estvolume=pnetEstimateCubicMtr,
actVolume=pnetActualCubicMtr where serviceorderid=NEW.id;
end if;


if(unit='Lbs') then

update sodashboard set estweight=pestimatedNetWeight,
actualweight=pactualNetWeight,
estvolume=pnetEstimateCubicFeet,
actVolume=pnetActualCubicFeet where serviceorderid=NEW.id;

end if;

END $$

delimiter;
