<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title>Quotes List</title>   
    <meta name="heading" content="Quotes List"/>   
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
   <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery.min.js"></script>
<script>
function showOrHideRejectReason(){
	var reject="F";
	<c:forEach items="${serviceOrders}" var="list1">
	var id='${list1.id}';
	var rejectTemp=document.getElementById('quoteAccept'+id).value;
	if(rejectTemp=='R'){
			reject="T";
	}
	<configByCorp:fieldVisibility componentId="component.field.quoteAcceptReason.showUTSI">
	if(rejectTemp=='A' || rejectTemp=='R'){
		reject="T";
	}
	</configByCorp:fieldVisibility>
	</c:forEach>
	if(reject=="F"){
		$("#serviceOrderList tr").find('th:last, td:last').hide();
	}else{
		$("#serviceOrderList tr").find('th:last, td:last').show();
	}
	<c:forEach items="${serviceOrders}" var="list1">
	if(reject!='F'){
		var id='${list1.id}';
		//var e2=document.getElementById('tempQuoteStatusReason'+id);
		//alert(e1);
		var e1=document.getElementById('tempStatusReason'+id);
		if(document.getElementById('quoteAccept'+id).value=='R'){
			e1.style.display = 'block';
			//e2.style.display = 'block';
		}else{
			e1.style.display = 'none';
			//e2.style.display = 'none';
		}
		<configByCorp:fieldVisibility componentId="component.field.quoteAcceptReason.showUTSI">
		if(document.getElementById('quoteAccept'+id).value=='R'||document.getElementById('quoteAccept'+id).value=='A'){
			e1.style.display = 'block';
		}
		else{
			e1.style.display = 'none';
			//e2.style.display = 'none';
		}
		</configByCorp:fieldVisibility>
	}
	</c:forEach>
}

  </script>
   
   <script language="javascript" type="text/javascript">

function clear_fields(){
	document.forms['serviceOrderListForm'].elements['serviceOrder.shipNumber'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.firstName'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.lastName'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.status'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.statusDate'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.job'].value = "";
}
</script>
<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:95%;
}
</style>
</head>   

  <div id="Layer5" style="width:100%">
<c:set var="buttons">   
    <table><tr><td><input type="button" class="cssbutton" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/editQuotationServiceOrder.html?id=${customerFile.id}"/>'"
         value="<fmt:message key="button.add"/>"/>  </td> 
  </tr></table>
         
</c:set>   
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set>  
<c:if test="${not empty serviceOrder.id}">
  <div id="newmnav">
	  <ul>
	  	  <li><a href="QuotationFileForm.html?id=${customerFile.id}" ><span>Quotation File</span></a></li>
	  	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Quotes<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		  
		  <li><a onclick="openWindow('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=quotation&reportSubModule=quote&decorator=popup&popup=true',750,400)"><span>Forms</span></a></li>
		   <li><a href="showAccountPolicyQuotation.html?id=${customerFile.id}&code=${customerFile.billToCode}&jobNumber=${customerFile.sequenceNumber}" ><span>Account Policy</span></a></li>	
	 </ul>
</div><div class="spn" style="margin-top:-15px;">&nbsp;</div>

</c:if>
<c:if test="${empty serviceOrder.id}">
  <div id="newmnav">
	  <ul>
	  	  <li><a href="QuotationFileForm.html?id=${customerFile.id}" ><span>Quotation File</span></a></li>
	  	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Quotes<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
	     <%--  <c:if test="${usertype!='ACCOUNT' && usertype!='AGENT'}">
		    <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		  </c:if> --%>
		  <li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&jobNumber=${customerFile.sequenceNumber}&companyDivision=${customerFile.companyDivision}&jobType=${customerFile.job}&billToCode=${customerFile.billToCode}&reportModule=quotation&reportSubModule=quote&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
		  <li><a href="showAccountPolicyQuotation.html?id=${customerFile.id}&code=${customerFile.billToCode}&jobNumber=${customerFile.sequenceNumber}" ><span>Account Policy</span></a></li>		  
	  </ul>
</div><div class="spn" style="margin-top:-15px;">&nbsp;</div>


</c:if>   
<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>	 
<s:hidden name="fileID"  id= "fileID" value="%{customerFile.id}"/> 
<s:hidden id="forQuotation" name="forQuotation" value="QC"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" value="agentQuotes" />
<s:form id="serviceOrderListForm" action="searchServiceOrders" method="post" > 
<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>  

<script language="javascript" type="text/javascript">  
function requestedQuotes(targetElement,bookingAgentCode,billToCode,evt){
	   var bookingAgent=bookingAgentCode;
	   var billToCode=billToCode;
	   if(bookingAgent==null||bookingAgent==undefined){  bookingAgent="";	  }
	   if(billToCode==null||billToCode==undefined){	  billToCode="";  }
	   var requestedAQP = evt.value;
	   var id = evt.id;
	   id=id.replace("quoteAccept",""); 
	   id=id=id.trim();
	   var targetElement=targetElement;
	   if(requestedAQP=='A'){
		  if((bookingAgent=='')&&(billToCode=='' || billToCode!='')){
			  alert("Booking Agent code not valid");
			  document.forms['serviceOrderListForm'].elements['quoteAccept'+targetElement].value=document.getElementById('tempQuoteAccept'+id).value;
		  }else{
			  var billToCode1=billToCode;
		  		if(billToCode1==''){
		  			billToCode1="Invalid";
		  		}
			  	var bookCode=bookingAgent+"~"+billToCode1;
		    	var url="findBookCodeStatus.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
				http50.open("GET", url, true);
				http50.onreadystatechange = function(){handleHttpResponse50(targetElement,bookingAgent,billToCode,requestedAQP,id)};
				http50.send(null);
		  }
	    }else{
	        requestedQuotesValidation(targetElement,bookingAgent,billToCode,requestedAQP,id); 
	    }  
}
function handleHttpResponse50(targetElement,bookingAgent,billToCode,requestedAQP,id){
	if (http50.readyState == 4) {
        var results = http50.responseText
        results = results.trim();
        if(results.length > 2) {
            var rec=results.split(",");
            var bookingAgent1=rec[0].split(":");
            var billToCode1=rec[1].split(":");
            if(bookingAgent1[1]=="Y"){
				if((billToCode=='')||(billToCode1[1]=="Y")){
	       		     requestedQuotesValidation(targetElement,bookingAgent,billToCode,requestedAQP,id);       		        
				}else{
          			alert("Quote cannot be accepted as selected partners have not been approved yet");
             		document.forms['serviceOrderListForm'].elements['quoteAccept'+targetElement].value=document.getElementById('tempQuoteAccept'+id).value;
				}     		    	
            }else{
       			alert("Quote cannot be accepted as selected partners have not been approved yet");
       			document.forms['serviceOrderListForm'].elements['quoteAccept'+targetElement].value=document.getElementById('tempQuoteAccept'+id).value;        
            }
        }
	}
}
function requestedQuotesValidation(targetElement,bookingAgent,billToCode,requestedAQP,id){
    var targetElement=targetElement;
    var requestedAQP =requestedAQP;
  		 if(requestedAQP=='M' ){
  		 	mergeScript(targetElement);
  		 }else{	
  			if(requestedAQP=='A'){
  				type='Accept';
 			}else if(requestedAQP=='R'){
  				type='Reject';
  			}else if(requestedAQP=='P' ){
    			 type='Pending';
    	    }else if(requestedAQP=='M' ){
      			 mergeScript(targetElement);
      			 var type='Merge';
      		}else if( requestedAQP==''){
       		     alert("You can not select blank value.");
                 document.forms['serviceOrderListForm'].elements['quoteAccept'+targetElement].value=document.getElementById('tempQuoteAccept'+id).value;
      		}
  			if(requestedAQP!=''){
  				var agree = confirm("Are you sure, You want to change status to "+type+"\n"+"Click ok to continue or cancel to reset.");
 				 if(agree){
 					document.getElementById('tempQuoteAccept'+id).value=requestedAQP;
 					showOrHideRejectReason();
   					  soid=targetElement;
    				  var url='';
    				 <c:if test="${customerFile.controlFlag =='C'}">
	 					url = "quoteAcceptance.html?ajax=1&decorator=simple&popup=true&requestedAQP="+encodeURI(requestedAQP) +"&soid="+encodeURI(soid);
					 </c:if>
  				     <c:if test="${customerFile.controlFlag !='C'}">
     					url = "quoteAcceptanceForQuote.html?ajax=1&decorator=simple&popup=true&requestedAQP="+encodeURI(requestedAQP) +"&soid="+encodeURI(soid)+"&customerFileId="+${customerFile.id};
	 				 </c:if>
  				  	 http52.open("GET", url, true);
   					 http52.onreadystatechange = handleHttpResponse52;
   				 	 http52.send(null);
  					}else{
  						 document.forms['serviceOrderListForm'].elements['quoteAccept'+targetElement].value=document.getElementById('tempQuoteAccept'+id).value;
  						showOrHideRejectReason();
  						location.href=window.location;
      				}
 			}
   	   }	
}
function handleHttpResponse52(){
	if (http52.readyState == 4){
			var results = http52.responseText
			location.href=window.location;
            }
      }
function rejectedQuotesCust(targetElement, evt){
    var requestedAQP = evt.value;
    //alert(targetElement);
   soid=targetElement;
   	 var url = "rejectedQuotesCust.html?ajax=1&decorator=simple&popup=true&requestedAQP="+encodeURI(requestedAQP) +"&soid="+encodeURI(soid)+"&customerFileId="+${customerFile.id};
	  	http555.open("GET", url, true);
    	http555.onreadystatechange = handleHttpResponseRej;
    	http555.send(null);
   
   }
function handleHttpResponseRej(){
		if (http555.readyState == 4){
 			var results = http555.responseText
 		     }
}

var http50 = getHTTPObject();
var http52 = getHTTPObject();
var http555 = getHTTPObject();
function getHTTPObject(){
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}	
function mergeScript(targetElement){
	javascript:openWindow("mergeServiceOrdersForm.html?decorator=popup&popup=true&sequenceNumber=${customerFile.bookingAgentName}&bookingAgentName=${customerFile.bookingAgentName}&bookingAgentCode=${customerFile.bookingAgentCode}&quotesId="+targetElement,"height=200,width=500");
}
</script>

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:100%">
		<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>
					<tr>
						<td align="right" class="listwhitebox">Cust#</td>
						<td><s:textfield name="customerFile.sequenceNumber" size="21" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" size="10" readonly="true" cssClass="input-textUpper"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" size="17" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right"><s:textfield name="customerFile.originCountryCode" required="true" size="8" readonly="true" cssClass="input-textUpper"/></td>
					</tr>
					<tr>
						
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" size="21" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" size="10" readonly="true" cssClass="input-textUpper" /></td>
						<td align="left" class="listwhitebox"><fmt:message key='quotationFile.billToCode'/></td>
						<td colspan="2"><s:textfield name="customerFile.billToName" required="true" size="35" readonly="true" cssClass="input-textUpper" /></td>
						</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
<div id="layer2" style="width:100%">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Quotes List</span></a></li> 
		  </ul>
		</div>		
		<div class="spnblk" style="line-height:7px;">&nbsp;</div>
		<c:if test="${serviceOrders!=null && serviceOrders!='[]'}">
		<div align="left" style="margin:0px;margin-right:8px;font-size:11px;padding-left:200px;"><b>*Note:</b> Quote status is saved automatically when changed.</div>
		</c:if>
<display:table name="serviceOrders" class="table" requestURI="" id="serviceOrderList" export="true" defaultsort="1" pagesize="10" style="width:99%;!margin-top:-2px;margin-left:5px;">   
<display:column property="shipNumber" sortable="true" title="Quotes List" href="editQuotationServiceOrderUpdate.html" paramId="id" paramProperty="id" style="width:75px"/>
<display:column property="routing" sortable="true" titleKey="serviceOrder.routing" style="width:20px"/>  
<display:column property="commodity" sortable="true" titleKey="serviceOrder.commodity" style="width:20px"/>  
<display:column property="job" sortable="true" titleKey="serviceOrder.job" style="width:50px"/>
<display:column property="mode" sortable="true" titleKey="serviceOrder.mode" style="width:20px"/> 
<display:column property="packingMode" sortable="true" titleKey="serviceOrder.packingMode" style="width:50px"/> 
<c:choose>
  <c:when test="${weightUnit == 'Lbs'}">
    <display:column property="estimateGrossWeight" sortable="true" title="Est.Weight" style="width:20px"/>
  </c:when>
  <c:otherwise>
	 <display:column property="estimateGrossWeightKilo" sortable="true" title="Est.Weight" style="width:20px"/>
  </c:otherwise>
 </c:choose>
<c:choose>
  <c:when test="${volumeUnit == 'Cft'}">
    <display:column property="estimateCubicFeet" sortable="true" title="Est.Vol" style="width:20px"/>  
  </c:when>
  <c:otherwise>
	 <display:column property="estimateCubicMtr" sortable="true" title="Est.Vol" style="width:20px"/>  
  </c:otherwise>
 </c:choose>

<display:column property="estimatedTotalExpense" sortable="true" title="Expense" style="width:50px"/>
<display:column property="estimatedTotalRevenue" sortable="true" title="Revenue" style="width:50px"/>
<display:column property="estimatedGrossMargin" sortable="true" title="Margin" style="width:20px"/> 

<display:column title="Quote Status*" style="width:50px"> 
<select name="quoteAccept${serviceOrderList.id}" id="quoteAccept${serviceOrderList.id}" onchange="requestedQuotes('${serviceOrderList.id}','${serviceOrderList.bookingAgentCode}','${serviceOrderList.billToCode}',this);" class="list-menu"> 
<c:forEach var="chrms" items="${quoteAccept}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == serviceOrderList.quoteAccept}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
</c:forEach> 
</select> 
<s:hidden name="tempQuoteAccept${serviceOrderList.id}" id="tempQuoteAccept${serviceOrderList.id}" value="${serviceOrderList.quoteAccept}"/>
</display:column>
<div id="rejectDiv">
<display:column title="Status&nbsp;Reason" style="width:50px">
<div id="tempStatusReason${serviceOrderList.id}">
<c:if test="${serviceOrderList.quoteAccept == 'R'}">
<select name="statusReason${serviceOrderList.id}" id="statusReason${serviceOrderList.id}"  class="list-menu" onchange="rejectedQuotesCust('${serviceOrderList.id}',this); ">
<c:forEach var="statusRes" items="${statusReason}" varStatus="reasonStatus">
                                  <c:choose>
	                                  <c:when test="${statusRes.key == serviceOrderList.statusReason}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${statusRes.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${statusRes.value}"></c:out>
                                </option>
</c:forEach> 
</select>
</c:if>
<configByCorp:fieldVisibility componentId="component.field.quoteAcceptReason.showUTSI">
<c:if test="${serviceOrderList.quoteAccept == 'A'}">
<select name="quoteAcceptReason${serviceOrderList.id}" id="quoteAcceptReason${serviceOrderList.id}"  class="list-menu" onchange="rejectedQuotesCust('${serviceOrderList.id}',this); ">
<c:forEach var="quoteAcceptRes" items="${quoteAcceptReason}" varStatus="reasonQuoteAccept">
                                  <c:choose>
	                                  <c:when test="${quoteAcceptRes.key == serviceOrderList.quoteAcceptReason}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${quoteAcceptRes.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${quoteAcceptRes.value}"></c:out>
                                </option>
</c:forEach> 
</select>
</c:if>
</configByCorp:fieldVisibility>
</div>
</display:column>
</div>
 <display:setProperty name="paging.banner.items_name" value="serviceOrder"/>
 <display:setProperty name="paging.banner.item_name" value="serviceorder"/>   
    <display:setProperty name="paging.banner.items_name" value="orders"/>   
    <display:setProperty name="export.excel.filename" value="ServiceOrder List.xls"/>   
    <display:setProperty name="export.csv.filename" value="ServiceOrder List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="ServiceOrder List.pdf"/>  
</display:table>
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="customerFile.sequenceNumber" />
<s:hidden name="customerFile.id" />
<s:hidden name="requestedAQP" />
<c:if test="${not empty customerFile.sequenceNumber}">
<c:out value="${buttons}" escapeXml="false" />
</c:if>
<c:if test="${empty customerFile.sequenceNumber}">
<c:set var="isTrue" value="false" scope="application"/>
</c:if>
</div> 
   <c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="agentQuotes" scope="session"/>
<c:if test="${empty customerFile.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty customerFile.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
 </s:form>
<script type="text/javascript">
<c:if test="${serviceOrders!=null && serviceOrders!='[]'}">
showOrHideRejectReason();
</c:if>
</script>  
