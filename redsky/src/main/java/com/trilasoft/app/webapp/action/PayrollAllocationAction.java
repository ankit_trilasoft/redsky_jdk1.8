package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.PayrollAllocation;
import com.trilasoft.app.model.Sharing;
import com.trilasoft.app.service.PayrollAllocationManager;
import com.trilasoft.app.service.PayrollManager;
import com.trilasoft.app.service.RefMasterManager;



public class PayrollAllocationAction extends BaseAction{
	
	
	private Long id;
	private Long maxId;
	private String sessionCorpID;
	private Set payrollAllocationSet;

	private List payrollAllocationList;
	private PayrollAllocationManager payrollAllocationManager;
	private PayrollAllocation payrollAllocation;
	private List refMasters;
	private RefMasterManager refMasterManager;
	private PayrollManager payrollManager;
	private Map<String, String> compDevision;
	private Map<String, String> payroll;
	private Map<String, String> job;
	private Map<String, String> tcktservc;
	private Map<String, String> revcalc;
	private Map<String, String> revcalculation;
	private List searchList;
	private Map<String, String> prate;
	private Map<String, String> service;
	private List mode;
	
	public String getComboList(String corpId){
		payroll= refMasterManager.findByParameter(corpId, "PAYROLL");
		job= refMasterManager.findByParameter(corpId, "JOB");
		tcktservc= refMasterManager.findByParameter(corpId, "TCKTSERVC");
		revcalc= refMasterManager.findByParameter(corpId, "REVCALC");
		prate=refMasterManager.findByParameter(corpId, "P_RATE");
		compDevision=payrollManager.getOpenCompnayDevisionCode(corpId);
		service = refMasterManager.findByParameter(corpId, "TCKTSERVC");
		mode = refMasterManager.findByParameters(corpId, "MODE");
		
		return SUCCESS;
	}
	
public List getRefMasters() {
		return refMasters;
	}



	public void setRefMasters(List refMasters) {
		this.refMasters = refMasters;
	}



	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}



public void setPayrollAllocationManager(
			PayrollAllocationManager payrollAllocationManager) {
		this.payrollAllocationManager = payrollAllocationManager;
	}



public void setPayrollAllocation(PayrollAllocation payrollAllocation) {
		this.payrollAllocation = payrollAllocation;
	}


public String save() throws Exception {
	
	boolean isNew = (payrollAllocation.getId() == null); 
	
	if(isNew)
    {
		payrollAllocation.setCreatedOn(new Date());
		
    }
      
	getComboList(sessionCorpID);
	revcalculation = payrollAllocationManager.findRecalc(sessionCorpID);
	payrollAllocation.setCorpID(sessionCorpID);
	payrollAllocation.setUpdatedBy(getRequest().getRemoteUser());
	payrollAllocation.setUpdatedOn(new Date());
	payrollAllocation= payrollAllocationManager.save(payrollAllocation);
	if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
	String key = (isNew) ? "PayrollAllocation has been added successfully" : "PayrollAllocation has been updated successfully";
	saveMessage(getText(key));
	}
	if (!isNew) {   
    	return INPUT;   
    } else {   
    	maxId = Long.parseLong(payrollAllocationManager.findMaximumId().get(0).toString());
    	payrollAllocation = payrollAllocationManager.get(maxId);
     return SUCCESS;
    } 
		
	
}


@SkipValidation
  public String search(){        
    boolean code = (payrollAllocation.getCode() == null);
    boolean job = (payrollAllocation.getJob() == null);
    boolean service = (payrollAllocation.getService() == null);
    boolean calculation = (payrollAllocation.getCalculation() == null);
    getComboList(sessionCorpID);
    if(!code || !job || !service || !calculation){
    	 payrollAllocationList = payrollAllocationManager.searchpayrollAllocation(payrollAllocation.getCode(),payrollAllocation.getJob(),payrollAllocation.getService(),payrollAllocation.getCalculation());
 	    //payrollAllocationSet =new HashSet(searchList);
 	  }	    	   
    return SUCCESS;     
} 

    private String gotoPageString;
	public String saveOnTabChange() throws Exception {
		String s = save();
		return gotoPageString;
	}



	public List getPayrollAllocationList() {
		return payrollAllocationList;
	}

	public void setPayrollAllocationList(List payrollAllocationList) {
		this.payrollAllocationList = payrollAllocationList;
	}

	public Set getPayrollAllocationSet() {
		return payrollAllocationSet;
	}

	public void setPayrollAllocationSet(Set payrollAllocationSet) {
		this.payrollAllocationSet = payrollAllocationSet;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public void setId(Long id) {
		this.id = id;
	}



public String payrollAllocationList(){
	getComboList(sessionCorpID);	
	payrollAllocationList=payrollAllocationManager.getAll();
	// payrollAllocationSet=new HashSet(payrollAllocationList);		
	return SUCCESS;

	}
	
	public PayrollAllocationAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	
	public String edit(){
		getComboList(sessionCorpID);
		if (id != null){
			payrollAllocation=payrollAllocationManager.get(id);
			try{
				if(payrollAllocation !=null && payrollAllocation.getCompanyDivision()!=null && (!(payrollAllocation.getCompanyDivision().toString().trim().equals("")))){
					
					if(compDevision.containsKey(payrollAllocation.getCompanyDivision())){
						
					}else{
						compDevision=payrollManager.getOpenCompnayDevisionCode(sessionCorpID);
					}
					}
				}
			catch (Exception exception) {
				exception.printStackTrace();
			}

		}else{
			payrollAllocation=new PayrollAllocation();
			payrollAllocation.setCreatedOn(new Date());
			payrollAllocation.setValueDate(new Date());
			payrollAllocation.setUpdatedOn(new Date());
		}
		payrollAllocation.setCorpID(sessionCorpID);
		revcalculation = payrollAllocationManager.findRecalc(sessionCorpID);
		
		return SUCCESS;
	}



	public PayrollAllocation getPayrollAllocation() {
		return payrollAllocation;
	}

	public Map<String, String> getRevcalc() {
		return revcalc;
	}

	public void setRevcalc(Map<String, String> revcalc) {
		this.revcalc = revcalc;
	}

	
	public Map<String, String> getJob() {
		return job;
	}

	public void setJob(Map<String, String> job) {
		this.job = job;
	}

	public List getSearchList() {
		return searchList;
	}

	public void setSearchList(List searchList) {
		this.searchList = searchList;
	}

	public Map<String, String> getPayroll() {
		return payroll;
	}

	public void setPayroll(Map<String, String> payroll) {
		this.payroll = payroll;
	}

	public Map<String, String> getTcktservc() {
		return tcktservc;
	}

	public void setTcktservc(Map<String, String> tcktservc) {
		this.tcktservc = tcktservc;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public Map<String, String> getPrate() {
		return prate;
	}

	public void setPrate(Map<String, String> prate) {
		this.prate = prate;
	}

	public void setPayrollManager(PayrollManager payrollManager) {
		this.payrollManager = payrollManager;
	}

	public Map<String, String> getCompDevision() {
		return compDevision;
	}

	public void setCompDevision(Map<String, String> compDevision) {
		this.compDevision = compDevision;
	}

	/**
	 * @return the revcalculation
	 */
	public Map<String, String> getRevcalculation() {
		return revcalculation;
	}

	/**
	 * @param revcalculation the revcalculation to set
	 */
	public void setRevcalculation(Map<String, String> revcalculation) {
		this.revcalculation = revcalculation;
	}

	public Map<String, String> getService() {
		return service;
	}

	public List getMode() {
		return mode;
	}

	public void setMode(List mode) {
		this.mode = mode;
	}

}
