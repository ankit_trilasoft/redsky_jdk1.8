<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  

<head>   
    <title><fmt:message key="chargesList.title"/></title>   
    <meta name="heading" content="<fmt:message key='chargesList.heading'/>"/> 
<style>

span.pagelinks {
	display:block;
	font-size:0.95em;
	margin-bottom:3px;
	!margin-bottom:1px;
	margin-top:-18px;
	!margin-top:-18px;
	padding:2px 0px;
	text-align:right;
	width:99%;
}
</style>
<script language="javascript" type="text/javascript">
    function clear_fields(){
	document.forms['chargesListForm'].elements['driverCommissionPlan.charge'].value = "";
	document.forms['chargesListForm'].elements['driverCommissionPlan.description'].value = "";
	}
</script>   
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top" method="searchChargesList" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set>

<s:form id="chargesListForm" action='' method="post" >
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:12px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%"  >
<thead>
<tr>

<th><fmt:message key="charges.charge"/></th>

<th><fmt:message key="charges.description"/></th>
<th></th>
</tr></thead>	
		<tbody>
		<tr>
			
			<td>
			    <s:textfield name="driverCommissionPlan.charge" size="9" required="true" cssClass="text medium" onfocus="onFormLoad();" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/></td>
			    <td><s:textfield name="driverCommissionPlan.description" size="25" required="true" cssClass="text medium" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			    <s:hidden name="contract" value="${contract}"/>
			</td>
			<td width="130px" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
			</tr>
		</tbody>
	</table>

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Charges List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<s:set name="chargess" value="chargess" scope="request"/>
<display:table name="chargess" class="table" requestURI="" id="chargesList"  defaultsort="2" pagesize="20"  decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
		
    <display:column  sortable="true" titleKey="charges.charge">
    <c:set var="des" value="${chargesList.description}"/>
     <a onclick='setParentValue("${chargesList.charge}","${fn:escapeXml(des)}");'><c:out value="${chargesList.charge}"/></a>
    </display:column>    
    <display:column property="description" sortable="true" titleKey="charges.description"/>
    <display:column property="contract" sortable="true" title="Contract"/>
    <display:setProperty name="paging.banner.item_name" value="charges"/>   
    <display:setProperty name="paging.banner.items_name" value="chargess"/>   
  
    <display:setProperty name="export.excel.filename" value="Charges List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Charges List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Charges List.pdf"/>   
</display:table> 
</s:form>
<script type="text/javascript">
function setParentValue(charge,description){
	window.opener.document.getElementById("newcharge").value=charge; 
	window.opener.document.getElementById("newDescription").value=description;
	window.opener.setContractValue(); 
	 self.close();
}
////window.close();
</script>