<%@ include file="/common/taglibs.jsp"%>  

 
<head>   
    <title><fmt:message key="forwardingList.title"/></title>   
    <meta name="heading" content="<fmt:message key='forwardingList.heading'/>"/> 
    
<script language="javascript" type="text/javascript">

function clear_fields(){
	document.forms['searchForm'].elements['containerNo'].value = "";
	document.forms['searchForm'].elements['bookingNo'].value = "";
	document.forms['searchForm'].elements['blNumber'].value = "";
	document.forms['searchForm'].elements['sailDate'].value = "";
	document.forms['searchForm'].elements['lastName'].value = "";
	document.forms['searchForm'].elements['vesselFilight'].value = ""; 
	
}

function goToForwardingDetail(targetValue){
alert(targetValue);
		//document.forms['searchForm'].elements['id'].value = targetValue;
		//document.forms['searchForm'].action = 'editContainer.html?from=list';
		document.forms['searchForm'].action = 'containers.html?id='+targetValue;
		alert(document.forms['searchForm'].action);
		document.forms['searchForm'].submit();
}
function goToSearchDetail(){
		return selectSearchField();
		document.forms['searchForm'].action = 'searchForwarding.html';
		document.forms['searchForm'].submit();
}

function selectSearchField(){
	
		var containerNumber = document.forms['searchForm'].elements['containerNo'].value; 
		var bookNumber = document.forms['searchForm'].elements['bookingNo'].value;
		var blNumber = document.forms['searchForm'].elements['blNumber'].value;
		if(containerNumber=='' && bookNumber=='' &&  blNumber==''){
			alert('Please select any one of the search criteria!');	
			return false;	
		}
		
}
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}

div.error, span.error, li.error, div.message {

width:450px;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>
</head>   
  
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editContainer.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px; !margin-bottom:10px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:45px; !margin-bottom:10px;" onclick="clear_fields();"/> 
</c:set>   
 <configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>  
<s:form id="searchForm" name="searchForm" action="searchForwarding" method="post" validate="true">   
<s:hidden name="id" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top" >
    <div class="top" style="margin-top:10px;!margin-top:-5px; "><span></span></div>
    <div class="center-content">
<table class="table" style="width:100%"  >
<thead>
<tr>
<th>Last Name</th>
<th>Container#</th>
<th>Booking#</th>
<th>BL#/AWB#</th>
<th>Sail Date</th>
<th>Vessel/Flight #</th>
<th>CAED&nbsp;#</th>
<th></th>
</tr></thead>	
		<tbody>
			<tr>
			    <td width="" align="left">
				    <s:textfield name="lastName" required="true" cssClass="input-text" cssStyle="width:120px"/>
				</td>
				<td width="" align="left">
				    <s:textfield name="containerNo" required="true" cssClass="input-text" cssStyle="width:110px"/>
				</td>
				<td width="" align="left">
				    <s:textfield name="bookingNo" required="true" cssClass="input-text" cssStyle="width:110px"/>
				</td>
				<td width="" align="left">
				    <s:textfield name="blNumber" required="true" cssClass="input-text" maxlength="30" cssStyle="width:150px"/>
				</td>
				<c:if test="${not empty sailDate}">
					<s:text id="dateFormattedValue" name="${FormDateValue}"><s:param name="value" value="sailDate" /></s:text>
					<td align="left" style="width:95px; !width:100px;"><s:textfield cssClass="input-text" id="sailDate" name="sailDate" value="%{dateFormattedValue}" cssStyle="width:60px" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/><img id="sailDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
					<c:if test="${empty sailDate}">
					<td align="left" style="width:95px; !width:100px;"><s:textfield cssClass="input-text" id="sailDate" name="sailDate" required="true" cssStyle="width:60px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this);"/><img id="sailDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<td width="" align="left">
				    <s:textfield id="vesselFilight" name="vesselFilight" required="true" cssClass="input-text" maxlength="20" cssStyle="width:142px"/>
				</td>
				<td width="" align="left">
				    <s:textfield id="caed" name="caed" required="true" cssClass="input-text" maxlength="20" cssStyle="width:142px"/>
				</td>
				<td  align="center" style="border-left: hidden;">
				    <c:out value="${searchbuttons}" escapeXml="false" />   
				</td>
			</tr>			
		</tbody>
	</table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<c:out value="${searchresults}" escapeXml="false" />  
		<div id="otabs" style="margin-top: -10px;">
		  <ul>
		    <li><a class="current"><span>Forwarding Look Up</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<s:set name="containers" value="containers" scope="request"/>   
<display:table name="containers" class="table" requestURI="" id="containersList" export="true" defaultsort="2" pagesize="25" style="width:100%; " >   
    <display:column sortable="true" title="Job #">

   <c:if test="${forwardingTabVal!='Y'}">  
	<a href="containers.html?id=${containersList.id}">
       <c:out value="${containersList.shipNumber}" />
    </a>
	</c:if>
	<c:if test="${forwardingTabVal=='Y'}">
		<a href="containersAjaxList.html?id=${containersList.id}">
		<c:out value="${containersList.shipNumber}" />
		</a>
	</c:if>   
    </display:column>
   	<display:column property="lastName" sortable="true" title="Last Name"/>
   	<display:column property="billToName" sortable="true" title="Bill To Name"/>
   	<display:column property="containerNumber" sortable="true" title="Container#"/>
   	<display:column property="bookNumber" sortable="true" title="Booking#"/>
   	<display:column property="blNumber" sortable="true" title="BL#/AWB#"/>
   	<display:column property="coordinator" sortable="true" title="Coordinator"/>
   	<display:column property="etDepart" sortable="true" title="Sail Date" style="width:100px"  format="{0,date,dd-MMM-yyyy}"/>
   	<display:column property="beginLoad" sortable="true" title="Loading Date" style="width:100px"  format="{0,date,dd-MMM-yyyy}"/>
   	<display:column property="carrierVessels" sortable="true" title="Vessel / Flight #"/>
   	<display:column property="caed" sortable="true" title="CAED&nbsp;#"/>
   	
    <display:setProperty name="paging.banner.item_name" value="container"/>   
    <display:setProperty name="paging.banner.items_name" value="container"/>   
  
    <display:setProperty name="export.excel.filename" value="Container List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Container List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Container List.pdf"/>   
</display:table>   
<c:set var="isTrue" value="false" scope="session"/>
</s:form>  
 
<script type="text/javascript">   
try{
document.forms['searchForm'].elements['lastName'].focus();
}
catch(e){}

</script> 

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>