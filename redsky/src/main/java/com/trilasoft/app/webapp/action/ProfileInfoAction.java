package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.ProfileInfo;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.ProfileInfoManager;
import com.trilasoft.app.service.RefMasterManager;

public class ProfileInfoAction extends BaseAction implements Preparable {
	private String sessionCorpID;
	private Long id;
	private ProfileInfo profileInfo;
	private RefMasterManager refMasterManager;
	private ProfileInfoManager profileInfoManager;
	private String partnerType;
	private String partnerCode;
	private String partnerId;
	Date currentdate = new Date();
	private String popupval;
	private String gotoPageString;
	private Map<String, String> agent;
	private PartnerPublic partnerPublic;
	private PartnerPublicManager partnerPublicManager;
	static final Logger logger = Logger.getLogger(ProfileInfoAction.class);
	
	public ProfileInfoAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	@SkipValidation
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		agent = refMasterManager.findByParameter(corpId, "AGENTPROFILEINFO");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partnerPublic = partnerPublicManager.get(new Long(partnerId));
		List profileInfoList=profileInfoManager.getProfileInfoListByPartnerCode(sessionCorpID, partnerCode);
		if(profileInfoList != null && (!(profileInfoList.isEmpty())) && profileInfoList.get(0)!= null && (!(profileInfoList.get(0).toString().equals("")))){
			profileInfo = (ProfileInfo)profileInfoManager.getProfileInfoListByPartnerCode(sessionCorpID, partnerCode).get(0);
		}else {
			profileInfo = new ProfileInfo();
			profileInfo.setPartnerCode(partnerCode);
			profileInfo.setPartnerPrivateId(new Long(partnerId));
			profileInfo.setCorpId(sessionCorpID);
			profileInfo.setCreatedOn(new Date());
			profileInfo.setUpdatedOn(new Date());
			
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
		boolean isNew = (profileInfo.getId() == null);
		profileInfo.setUpdatedBy(getRequest().getRemoteUser());
		if (isNew) {
			profileInfo.setCreatedOn(new Date());
		}
		profileInfo.setUpdatedOn(new Date());
		profileInfo=profileInfoManager.save(profileInfo); 
		if (!popupval.equalsIgnoreCase("true")) {
			if (gotoPageString == null || gotoPageString.equalsIgnoreCase("")) {
				String key = (isNew) ? "Profile Info added successfully." : "Profile Info updated successfully.";
				saveMessage(getText(key));
			}
		}
		partnerPublic = partnerPublicManager.get(new Long(partnerId));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProfileInfo getProfileInfo() {
		return profileInfo;
	}

	public void setProfileInfo(ProfileInfo profileInfo) {
		this.profileInfo = profileInfo;
	}

	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public ProfileInfoManager getProfileInfoManager() {
		return profileInfoManager;
	}

	public void setProfileInfoManager(ProfileInfoManager profileInfoManager) {
		this.profileInfoManager = profileInfoManager;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public Date getCurrentdate() {
		return currentdate;
	}

	public void setCurrentdate(Date currentdate) {
		this.currentdate = currentdate;
	}

	public static Logger getLogger() {
		return logger;
	}

	public String getPopupval() {
		return popupval;
	}

	public void setPopupval(String popupval) {
		this.popupval = popupval;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public Map<String, String> getAgent() {
		return agent;
	}

	public void setAgent(Map<String, String> agent) {
		this.agent = agent;
	}

	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}

	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}

	public PartnerPublicManager getPartnerPublicManager() {
		return partnerPublicManager;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

}
