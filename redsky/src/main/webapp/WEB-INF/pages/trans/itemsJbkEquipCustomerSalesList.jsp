<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<head> 
  
     <c:if test="${not empty ship && ship=='CS' }">
    <title>Material And Equipment Customer Sales</title> 
	 <meta name="heading" content="Material And Equipment Customer Sales"/> 
	 </c:if>
	  <c:if test="${not empty ship && ship=='OS' }">
    <title>Material And Equipment OO Sales</title> 
	 <meta name="heading" content="Material And Equipment OO Sales"/> 
	 </c:if>
	 <c:if test="${ empty ship  }">
    <title>Overall Customer And OO Sales </title> 
	 <meta name="heading" content="Overall Customer And OO Sales "/> 
	 </c:if>
	<style> <%@ include file="/common/calenderStyle.css"%> </style>
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
	<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    <script type="text/javascript">
		function mailtovendor1(targetElement) {
	var mailToVendorvalue = document.forms['assignItemsForm'].elements['mailtovendor'].value;
		if(document.forms['assignItemsForm'].elements['mailtovendor'].value == '' ){
			mailToVendorvalue=','+targetElement.value;
		}else{
			if(mailToVendorvalue.indexOf(targetElement.value)>=0){
				if(targetElement.value == '' ){
					mailToVendorvalue = mailToVendorvalue.replace(targetElement.value,"");
				}else{
				mailToVendorvalue = mailToVendorvalue.replace(','+targetElement.value,"");
			}
			}else{
				mailToVendorvalue = document.forms['assignItemsForm'].elements['mailtovendor'].value +','+ targetElement.value;
			}
		}
	document.forms['assignItemsForm'].elements['mailtovendor'].value = 	mailToVendorvalue;
	
	} 

	function sendEmailtoVendor() {
		//document.forms['customerFileForm'].elements['customerFile.customerPortalId'].value = document.forms['customerFileForm'].elements['customerFile.corpID'].value + document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
		var daReferrer = document.referrer; 
		var email = document.forms['assignItemsForm'].elements['mailtovendor'].value; 
		
		
	}
		</script> 
		<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-33px;
padding:2px 0px;
text-align:right;
width:80%;
}
</style>		
</head>


<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
 <s:form id="itemsJbkEquipListForm" action="searchItemsJbkEquips" method="post" >
 <div id="Layer8" style="width:100%">
 <s:hidden name="itemType" value="<%=request.getParameter("itemType") %>" />
 <c:set var="itemType" value="<%=request.getParameter("itemType") %>" />
 <s:hidden name="wcontract" value="<%=request.getParameter("wcontract") %>" />
 <c:set var="wcontract" value="<%=request.getParameter("wcontract") %>" />
 <s:set name="materialsList" value="materialsList" scope="request"/>
  
 
<s:form id="assignItemsForm" action="updatestatusitem" method="post">
	
	<s:hidden name="mailtovendor" />
	<s:hidden name="ticketW" value="%{workTicket.ticket}" />
  	<s:hidden name="itemType" value="<%=request.getParameter("itemType") %>" />
 	<c:set var="itemType" value="<%=request.getParameter("itemType") %>" />
 	<s:hidden name="wcontract" value="<%=request.getParameter("wcontract") %>" />
 	<c:set var="ticket" value="%{workTicket.ticket}" />
 	<c:set var="workTicketID" value="%{workTicket.id}" />
  
<%--<input type="submit" name="forwardBtn3" value="Unassign"
			class="cssbutton" style="width:70px; height:25px" onclick="sendEmailtoVendor()" />
<s:hidden name="itemsJbkEquip.id" value="%{workTicket.id}"/>
--%><c:if test="${assignItem == 'assignItems' }" >
		<c:redirect url="/itemsJbkEquips.html?id=${id}&itemType=${itemType}"/>
</c:if>
</s:form>
<script type="text/javascript">
    highlightTableRows("itemsJbkEquipList"); 
    Form.focusFirstElement($("itemsJbkEquipListForm")); 
</script> 
<script type="text/javascript">
		function rsearch(){
		
		//  var id = document.forms['gridForm'].elements['id'].value;
		//var discript = document.forms['gridForm'].elements['itemsJbkEquip.descript'].value;
			if(document.forms['gridForm'].elements['beginDate'].value == ''){
	       		alert("Please enter the begin date"); 
	       		return false;
	       }
	       if(document.forms['gridForm'].elements['endDate'].value == ''){
	       		alert("Please enter the end date "); 
	       		return false;
	       }else{
	    	   var url = "overallItemsJbkEquipCustomerSalesSearch.html";
	       	   document.forms['gridForm'].action = url;
	   		    document.forms['gridForm'].submit();
	       }
		
		}
function clear_fields(){
			
			    document.forms['gridForm'].elements['branch'].value = "";
			    document.forms['gridForm'].elements['beginDate'].value = "";
			    document.forms['gridForm'].elements['endDate'].value = "";
	
}
</script> 
<%
	//String editUrl = "<a onclick=javascript:openWindow('editItemsJbkEquip.html?id=listRow[id]&decorator=popup&popup=true');>Edit</a>";
	String editUrl = "<a onclick=javascript:void('editItemsJbkEquip.html?id=listRow[id]&decorator=popup&popup=true');>Edit</a>";
    /* String[][] tableMetadata = {
		{"materialsList", "itemsJbkEquip",""}, 	
      { "id", "descript","freeQty","cost","salesPrice","ooprice","minLevel"},
      { "long", "text", "int", "float","int","int" ,"float"},
      { "readonly", "editable", "editable", "editable","editable","editable","editable"},
      
    }; */
   /*  String[][] tableMetadata = {
    		{"materialsList", "itemsJbkEquip",""}, 	
          { "id", "descript","actualQty","returned","custName","emailId","custCell","refferedBy"},
          {  "long","text", "int", "int","text","text","text","text"},
          {  "readonly","editable", "editable", "editable","editable","editable","editable","editable"}
          
        }; */

        String[][] tableMetadata = {
        		{"materialsList", "itemsJbkEquip",""}, 	
              { "id", "descript","soldQty","customerName","payMethod","totalCost","unitCost","returned","createdOn","invoiceSeqNumber"},
              {  "long","text", "int","text", "text","float","text","int","text","text"},
              {  "readonly","editable", "editable","editable", "editable","editable","readonly","editable","readonly","readonly"}
              
            };

    request.setAttribute("tableMetadata", tableMetadata);
    
    
%>

<s:form id="gridForm" name="gridForm" action="saveCustomerSalesList" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="listData" />
	<s:hidden name="ship"  />
	<s:hidden name="listFieldNames" />	
	<s:hidden name="listFieldEditability" />	
	<s:hidden name="listFieldTypes" />		
	<s:hidden name="listIdField" value="id"/>	
	<s:hidden name="listIdFieldType" value="long"/>	
	<s:hidden name="showAdd" value="Yes" />			
	<s:hidden name="ticket" value="${ticket}" />
	<s:hidden name="workTicketID" value="${workTicketID}" />
	<%-- <s:hidden name="itemType" value="<%=request.getParameter("itemType") %>"/> --%>
	<s:hidden name="descript" value="${materialsList}" />
	<s:hidden name="chngMap"/>
	<s:hidden name="customerName"  />
	<s:hidden name="payMethod"  />
	<s:hidden name="lineFail"  />
	<s:hidden name="negValMsg" value="0" />
	<%-- <s:hidden name="itemsJbkEquip.id" value="%{workTicket.id}"/>
    <s:hidden name="wcontract" value="<%=request.getParameter("wcontract") %>" />
	<c:set var="sid" value="<%=request.getParameter("sid") %>" />
	<s:hidden name="sid" value="${sid}" />
	<c:set var="id" value="<%=request.getParameter("id") %>" scope ="session"/> --%>
	


<%-- <div id="newmnav">
		  
		  
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			  
			  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  

			   <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>				  
			  <li id="newmnav1" style="background:#FFF"><a class="current""><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</ul>
		</div> --%><div class="spn">&nbsp;</div>
	
	
<%-- <table width="100%" style="margin:0px;"><tr><td style="margin:0px;"><%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%></td></tr></table> --%>
</td>
<c:if test="${empty ship}">
<div id="searchBox111" >		
<div id="content" align="center" >
<div id="liquid-round-top">
    <div class="top" style="margin-top:0px;!margin-top: 0px; "><span></span></div>
    <div class="center-content">
 <table class="table" width="97%" >
		<thead>
			<tr>
				<!-- <th>Branch</th>
				<th>Division</th> -->
				<th>Warehouse</th>
				<th>From Date<font color="red" size="2">*</font></th>
				<th>To Date<font color="red" size="2">*</font></th>
				 <th></th> 
			</tr>
		</thead>	
			<tbody>
				<tr>			
							    	
						    <%-- <td ><s:select cssClass="list-menu" name="branch" list="{'Edmonton','Calgary'}" cssStyle="width:150px" headerKey="" headerValue="" /></td>						   
					  		<td ><s:select cssClass="list-menu" name="division" list="{'Domestic','Starline','OMD'}" cssStyle="width:150px" headerKey="" headerValue="" /></td> --%>
																 
					  		<td ><s:select id="branch" cssClass="list-menu" name="branch" list="%{house}"  cssStyle="width:150px" headerKey="" headerValue="" /></td>
								
					   
				    
				
							
				
  			
				<!--<td class="listwhitetext" align="right">Begin Date<font color="red" size="2">*</font></td>
					--><c:if test="${not empty beginDate}">
						<s:text id="beginDate" name="${FormDateValue}"><s:param name="value" value="beginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" value="%{beginDate}" size="8" maxlength="11" readonly="true" /> <img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty beginDate}">
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" required="true" size="8" maxlength="11" readonly="true" /> <img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				      	
				<!--<td class="listwhitetext" align="right">End Date<font color="red" size="2">*</font></td>
		 			--><c:if test="${not empty endDate}">
						<s:text id="endDate" name="${FormDateValue}"> <s:param name="value" value="endDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" size="8" maxlength="11" readonly="true" /><img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty endDate}" >
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" required="true" size="8" maxlength="11" readonly="true" /> <img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>	
				<td>
					    <input type="button"  class="cssbutton1" value="Search" style="!margin-bottom: 15px;"  onclick="rsearch()"/>   
					    <input type="button"  class="cssbutton1" value="Clear" style="!margin-bottom: 15px;"  onclick="clear_fields();"/>  
					</td> 
			</tr> 				
								
		</tbody>
</table>
</div>

<div class="bottom-header"><span></span></div>
</div>
</div> 		
</div>
</c:if>
<%-- <table width="100%" style="margin:0px;"><tr><td style="margin:0px;">		
<div id="newmnav">
		  <ul>
		    <li><a href="#"><span>Customer Information</span></a></li>
		    <c:if test="${itemType =='M'}">
		    	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Material</span></a></li>
		    	<li><a href="itemsJbkResourceList.html?id=${id}&itemType=E&sid=${sid}"><span>Resource</span></a></li>
			</c:if>
			<c:if test="${itemType =='E'}">
				<li id="newmnav1" style="background:#FFF"><a class="current"><span>Equipment</span></a></li>
				<li ><a href="itemsJbkResourceList.html?id=${id}&itemType=M&sid=${sid}"><span>Resource</span></a></li>
			</c:if>
		    <li><a href="workTicketCrews.html?id=${id}&sid=${sid}"><span>Crew</span></a></li>
		    <li><a href="truckingOperationsList.html?ticket=${workTicket.ticket}&sid=${serviceOrder.id}&tid=${workTicket.id}"><span>Truck</span></a></li>
		    <configByCorp:fieldVisibility componentId="component.tab.workTicket.whseMgmtTab">
		    <li><a href="bookStorages.html?id=${id} "><span>Whse/Mgmt</span></a></li>
		    </configByCorp:fieldVisibility>
  		</ul><div class="spn">&nbsp;</div>
</div>
</td></tr></table> --%>
		
		
		
		
<%-- <div id="content" align="center" >
<div id="liquid-round-top">
    <div class="top" style="margin-top:0px;!margin-top: 0px; "><span></span></div>
    <div class="center-content">
<table><tr>
		<td colspan="6">
		<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0">
			<tbody>
				<tr>
					<td align="right" height="30" class="listwhitetext"><fmt:message key='workTicket.ticket' /></td>
					<td align="left" ><s:textfield name="workTicket.ticket"  cssClass="input-textUpper" size="10" readonly="true" /></td>
					<td align="right" class="listwhitetext"><fmt:message key="workTicket.service" /></td>
					<td colspan="3"><s:textfield name="workTicket.service" cssClass="input-textUpper" size="10" readonly="true"/></td>
					<td align="right" class="listwhitetext">Warehouse</td> 
					<td colspan="3"><s:textfield name="workTicket.warehouse1"  value="${wrehouseDesc}" cssClass="input-textUpper" size="50" readonly="true"/></td>
				</tr>
				</tbody>
				</table>
				</td>
				</tr></table></div>
<div class="bottom-header"><span></span></div>
</div>
</div> 	 --%>							
		
		
		

	<%-- <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div> --%>
		<div class="spnblk">&nbsp;</div>
</table>
</s:form>
</div>
<div style="display:none" id="saveBox">
<jsp:include flush="true" page="equipMaterialCustomerSalesDetails.jsp"></jsp:include>
</div> 
</s:form>
<script type="text/javascript">
<c:if test="${valFlag=='openSaveBlock'}">
document.getElementById('saveBox').style.display="block";
document.getElementById('searchBox111').style.display="none";
</c:if>
</script>
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
<%@ include file="/common/enable-dojo-cussalesgridList.jsp" %>
