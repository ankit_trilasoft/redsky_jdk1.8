package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DsFamilyDetails; 

public interface DsFamilyDetailsDao extends GenericDao<DsFamilyDetails, Long>{
	List getDsFamilyDetailsList(Long id, String sessionCorpID);
	public void setCFileFamilySize(Long id, String sessionCorpID);
	Long findRemoteDsFamilyDetails(Long networkId, Long id);
	void deleteNetworkDsFamilyDetails(Long customerFileId, Long networkId);
	int updateFamilyDetailsNetworkId(Long id);
	public String getDsFamilyDetailsBlankFieldId(String ikeaflag,Long cid, String sessionCorpID);
	public Long getDsFamilyIdByRelationship(Long cid,String sessionCorpID,String relationshipVal);
	public List findRelationshipList(Long cid,String sessionCorpID);
	public List findSpouseDetails(Long id, String sessionCorpID);
	public int findRelationshipList1(Long cid,String sessionCorpID,Long id);
	public int findRelationshipList2(Long cid,String sessionCorpID);
}
