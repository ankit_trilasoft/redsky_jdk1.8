package com.trilasoft.app.service;


import org.appfuse.service.GenericManager; 
import com.trilasoft.app.model.ConsigneeInstruction; 
import java.util.List; 
 
public interface ConsigneeInstructionManager extends GenericManager<ConsigneeInstruction, Long>{ 
    public List<ConsigneeInstruction> findBySequenceNumber(String sequenceNumber);
    public List findMaximumId();
    public List checkById(Long id);
	public List getByShipNumber(String shipNumber);
	public List findConsigneeCode(String shipNumber);
	public List findConsigneeAddress(String DaCode);
	public List findConsigneeJobAddress(String DaCode,String shipNum);
	public List findShipmentLocation(String consigneeInstructionCode,String corpID);
	public List findConsigneeSubAgentAddress(String sACode, String shipNum);
	public List consigneeInstructionList(Long serviceOrderId);
	public Long findRemoteConsigneeInstruction(Long serviceOrderId);
	public List findConsigneeNCAddress(String shipNum);
	public List getShiperAddress(String corpID,String shipNum);
	public List consigneeInstructionListOtherCorpId(Long sid);
} 