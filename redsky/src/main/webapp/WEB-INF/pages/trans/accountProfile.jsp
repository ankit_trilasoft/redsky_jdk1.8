<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title><fmt:message key="accountProfileDetail.title" /></title>
<meta name="heading" content="<fmt:message key='accountProfileDetail.heading'/>" />
<style>
	.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;
	color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;
	padding:2px 3px 3px 5px; height:17px; border:1px solid #99BBE8; 
	border-right:none; border-left:none}
	
	div.wrapper-img{ width:570px;}
div.thumbline
  {
  margin:2px;
  border:none;
  float:left;
  text-align:center;
 
  background:transparent url(images/agent_prf_bg.png) no-repeat scroll 0 0;
  height:152px;
  padding:20px 20px 15px;
  width:238px;
  }
div.thumbline img
  {
  display:inline;
  
  margin:3px 40px 0px 0px;
  border:1px solid #ffffff;
  }

.table td, .table th, .tableHeaderTable td {
	border:1px;
	padding:0.5em;
}

.table tr.odd {
	background:#FFFFFF ;
	border-top:0px ;
	height:0px;
	padding:0px;
}

.table thead th, .tableHeaderTable td {
	border-style:1px;
	border-width:0;
}

.table caption {
	padding:0px;
}

.table tbody tr:hover, .table tr.over, .contribTable tr:hover {
   border-bottom: none;
   border-top: none;
   cursor:default;
   background-color:#fff !important; /* important needed for Tapestry, as is .table tr:hover */
}

.contribTable tr.even {
   border-top:none;
   background-color:#fff !important;
}

.contribTable tr.odd {
 	border-top: none;
}

.profile_black{color:#000000;}

div#main p {
margin-top:0;

}
.new {display:none;}

.myClass{ height:150px; width:auto}

.urClass{ height:auto; width:150px; }
	
input[type="radio"] {
    vertical-align: bottom;
}	
	
</style>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script language="JavaScript" type="text/JavaScript">
var namesVec = new Array("130.png", "129.png");
var root='images/';
function swapImg(ima){
// divides the path
nr = ima.getAttribute('src').split('/');
// gets the last part of path, ie name
nr = nr[nr.length-1]
// former was .split('.')[0];
 
if(nr==namesVec[0]){ima.setAttribute('src',root+namesVec[1]);}
else{ima.setAttribute('src',root+namesVec[0]);}
 
}
</script>
<script language="javascript">
function secImage1(){


if(document.getElementById('userImage1').height > 150)
{
 document.getElementById('userImage1').style.height = "120px";
 document.getElementById('userImage1').style.width = "auto";
}
}
function secImage2(){

if(document.getElementById('userImage2').height > 150 )
{
 document.getElementById('userImage2').style.height = "120px";
 document.getElementById('userImage2').style.width = "auto";
}        

}
function secImage3(){
if(document.getElementById('userImage3').height > 150)
{
 document.getElementById('userImage3').style.height = "120px";
 document.getElementById('userImage3').style.width = "auto";
} 
}
function secImage4(){
if(document.getElementById('userImage4').height > 150)
{
 document.getElementById('userImage4').style.height = "120px";
 document.getElementById('userImage4').style.width = "auto";
} 
}

</script>
<script>
animatedcollapse.addDiv('rating', 'fade=1,persist=0,hide=1')
animatedcollapse.addDiv('activity', 'fade=1,persist=0,hide=1')
animatedcollapse.addDiv('profile_photo', 'fade=1,persist=0,hide=1')
animatedcollapse.init()

function notExists(){
	alert("The Account information has not been saved yet, please save account information to continue");
	
}

function openNotesPopup(targetElement){
	var id = document.forms['accountProfileForm'].elements['accountProfile.id'].value;
	var notesId = document.forms['accountProfileForm'].elements['accountProfile.partnerCode'].value;
	var noteSubType = document.forms['accountProfileForm'].elements['accountProfile.type'].value;
	var imgID = targetElement.id;
	var pId = document.forms['accountProfileForm'].elements['partner.id'].value;
	openWindow('accountNotes.html?id='+id+'&notesId='+notesId+'&accountNotesFor=AP&noteFor=AccountProfile&subType='+noteSubType+'&imageId='+imgID+'&pId='+pId+'&for=List&fieldId=countAccountProfileNotes&decorator=popup&popup=true',800,600);
	document.forms['accountProfileForm'].elements['formStatus'].value = '';
}

function openNotesPopupTab(targetElement){
	var id = document.forms['accountProfileForm'].elements['accountProfile.id'].value;
	var notesId = document.forms['accountProfileForm'].elements['accountProfile.partnerCode'].value;
	var noteSubType = document.forms['accountProfileForm'].elements['accountProfile.type'].value;
	var imgID = targetElement.id;
	var pId = document.forms['accountProfileForm'].elements['partner.id'].value;
	openWindow('accountNotes.html?id='+id+'&notesId='+notesId+'&accountNotesFor=AP&noteFor=AccountProfile&subType='+noteSubType+'&imageId='+imgID+'&pId='+pId+'&for=List&fromTab=yes&fieldId=countAccountProfileNotes&decorator=popup&popup=true',800,600);
	document.forms['accountProfileForm'].elements['formStatus'].value = '';
}
</script>
<script>
function checkdate(clickType){
progressBarAutoSave('1');
var id =document.forms['accountProfileForm'].elements['accountProfile.id'].value;
      		var id1 = document.forms['accountProfileForm'].elements['partner.id'].value;
	if ('${autoSavePrompt}' == 'No'){
	if (document.forms['accountProfileForm'].elements['formStatus'].value == '1')
     {
	var noSaveAction = '<c:out value="${'accountProfile.id'}"/>';
      		
			var partnerType = document.forms['accountProfileForm'].elements['partnerType'].value;
		      	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
				      noSaveAction = 'editPartnerPublic.html?id='+id1+'&partnerType=AC';
		          		}
		      	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
						noSaveAction = 'accountContactList.html?id='+id1+'&partnerType= AC';
						}
				 if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
						noSaveAction = 'editContractPolicy.html?id='+id1+'&partnerType= AC';
						}
				 if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
				 		if('<%=session.getAttribute("paramView")%>' == 'View'){
							noSaveAction = 'searchPartnerView.html';
						}else{
							noSaveAction = 'partnerPublics.html?partnerType=AC';
						}
						
		        }
		         if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.partnerReloSvcs'){
						noSaveAction = 'partnerReloSvcs.html?id='+id1+'&partnerType= AC';
						}           
    	processAutoSave(document.forms['accountProfileForm'], 'saveAccountProfile!saveOnTabChange.html', noSaveAction);
		document.forms['accountProfileForm'].action = 'saveAccountProfile!saveOnTabChange.html';
	}else{
	if(id1 != '')  {
	        if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
			      location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AC';
	           		}
	        if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
					location.href = 'accountContactList.html?id='+id1+'&partnerType=AC';
					}
		 	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
					location.href= 'editContractPolicy.html?id='+id1+'&partnerType=AC';
					}
		 	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
					if('<%=session.getAttribute("paramView")%>' == 'View'){
						location.href = 'searchPartnerView.html';
					}else{
						location.href = 'partnerPublics.html?partnerType=AC';
					}
	        } 
	        if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.partnerReloSvcs'){
						location.href= 'partnerReloSvcs.html?id='+id1+'&partnerType=AC'; 
						}  
      } 
	}
	}
	else {
	
   if(!(clickType == 'save'))   {
     var id1 =document.forms['accountProfileForm'].elements['partner.id'].value;
     if (document.forms['accountProfileForm'].elements['formStatus'].value == '1')
     {
       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='billingDetail.heading'/>");
       if(agree)
        {
           document.forms['accountProfileForm'].action ='saveAccountProfile!saveOnTabChange.html';
           document.forms['accountProfileForm'].submit();
        }  else {
         if(id1 != '')  {
             if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
		      			location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AC';
           				}
		     if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
						location.href = 'accountContactList.html?id='+id1+'&partnerType=AC';
						}
			 if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
						location.href = 'editContractPolicy.html?id='+id1+'&partnerType=AC';
						}
			 if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
						if('<%=session.getAttribute("paramView")%>' == 'View'){
						location.href = 'searchPartnerView.html';
						}else{
							location.href = 'partnerPublics.html?partnerType=AC';
						}
		     } 
		     if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.partnerReloSvcs'){
						location.href= 'partnerReloSvcs.html?id='+id1+'&partnerType=AC'; 
						}  
         }
       }
    }   else   {
     if(id1 != '')  {
	        if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
			      location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AC';
	           		}
	        if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
					location.href = 'accountContactList.html?id='+id1+'&partnerType=AC';
					}
		 	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
					location.href= 'editContractPolicy.html?id='+id1+'&partnerType=AC';
					}
		 	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
					if('<%=session.getAttribute("paramView")%>' == 'View'){
						location.href = 'searchPartnerView.html';
					}else{
						location.href = 'partnerPublics.html?partnerType=AC';
					}
	        } 
	        if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.partnerReloSvcs'){
						location.href= 'partnerReloSvcs.html?id='+id1+'&partnerType=AC'; 
						}  
      }
  }
  }
  }
 }

function changeStatus(){
	document.forms['accountProfileForm'].elements['formStatus'].value = '1';
}
</script>
</head>
<s:hidden name="fileNameFor" id="fileNameFor" value="PO" />
<s:hidden name="fileID" id="fileID" value="%{partner.id}" />
<c:set var="PPID" value="%{partner.id}"/>
<s:hidden name="PPID" id="PPID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}" />
<s:hidden name="ppType" id="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>" />
<s:form id="accountProfileForm" action="saveAccountProfile" method="post" validate="true">
	<s:hidden id="countAccountProfileNotes" name="countAccountProfileNotes" value="<%=request.getParameter("countAccountProfileNotes") %>" />
	<c:set var="countAccountProfileNotes" value="<%=request.getParameter("countAccountProfileNotes") %>" />
	<c:set var="agentRatingVal" value="N" />
	<configByCorp:fieldVisibility componentId="component.partnerProfile.agentRatingCalculation">
		<c:set var="agentRatingVal" value="Y" />
	</configByCorp:fieldVisibility>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy" />
	<c:if test="${param.popup}">
		<s:hidden name="fld_code" value="${param.fld_code}" />
		<s:hidden name="fld_description" value="${param.fld_description}" />
		<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />
		<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />
		<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />
		<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />
		<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />
		<c:set var="fld_code" value="${param.fld_code}" />
		<c:set var="fld_description" value="${param.fld_description}" />
		<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
		<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
		<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
		<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
		<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
	</c:if>
	<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
	<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
	<s:hidden name="accountProfile.id" />
	<s:hidden name="accountProfile.corpID" />
	<s:hidden name="accountProfile.partnerCode" />
	<s:hidden name="partner.id" />
	<s:hidden name="partner.corpID" />
	<s:hidden name="id" value="<%=request.getParameter("id") %>" />
	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
	<s:hidden name="formStatus" value="" />
	<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
	<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
	<c:if test="${validateFormNav == 'OK'}">
		<c:choose>
			<c:when test="${gotoPageString == 'gototab.accountlist' }">
				<c:if test='${paramValue == "View"}'>
					<c:redirect url="/searchPartnerView.html" />
				</c:if>
				<c:if test='${paramValue != "View"}'>
					<c:redirect url="/partnerPublics.html?partnerType=AC" />
				</c:if>
				<c:redirect url="/partnerPublics.html?partnerType=AC" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.accountdetail' }">
				<c:redirect url="/editPartnerPublic.html?id=${partner.id}&partnerType=AC" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.accountcontact' }">
				<c:redirect url="/accountContactList.html?id=${partner.id}&partnerType=${partnerType}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.contractpolicy' }">
				<c:redirect url="/editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.partnerReloSvcs' }">
				<c:redirect url="/partnerReloSvcs.html?id=${partner.id}&partnerType=${partnerType}" />
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
	</c:if>
	<div id="layer4" style="width:100%;">
	<div id="newmnav">
	<ul>
		<c:if test="${param.popup}">
			<%-- <li><a href='${empty param.popup?"partners.html":"javascript:history.go(-4)"}' ><span>List</span></a></li>  --%>
			<li><a
				href="searchPartner.html?partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account
			List</span></a></li>
			<li><a
				href="editPartnerAddFormPopup.html?id=<%=request.getParameter("id") %>&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account
			Detail</span></a></li>
			<li><a
				href="accountInfoPage.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account
			Info</span></a></li>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Profile<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<li><a
				href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account
			Contact</span></a></li>
			<li><a
				href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Contract
			Policy</span></a></li>			
		</c:if>
		<c:if test="${empty param.popup}">
			<li><a onclick="setReturnString('gototab.accountdetail');return checkdate('none');"><span>Account Detail</span></a></li>
			<!--<li><a href="editPartnerAddForm.html?id=${partner.id}&partnerType=AC"><span>Account Detail</span></a></li>
			  		-->
			  		<%-- Modified for Bug ID: 6176 --%>
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Profile<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<c:if test="${sessionCorpID!='TSFT' }">
			<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
			</c:if>
			<configByCorp:fieldVisibility componentId="component.section.partner.SuddathInfo">
			<c:if test="${partnerType == 'AC'}">
			<li><a href="editProfileInfo.html?partnerId=${partner.id}&partnerCode=${partner.partnerCode}&partnerType=${partnerType}"><span>Profile Information</span></a></li>
			</c:if>
			</configByCorp:fieldVisibility>
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<configByCorp:fieldVisibility componentId="component.standard.accountContactTab">
			<li><a onclick="setReturnString('gototab.accountcontact');return checkdate('none');"><span>Account Contact</span></a></li>
			</configByCorp:fieldVisibility>
			<!--<li><a href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}"><span>Account Contact</span></a></li>
			  		-->
			<c:if test="${checkTransfereeInfopackage==true}">
				<li><a onclick="setReturnString('gototab.contractpolicy');return checkdate('none');"><span>Policy</span></a></li>
			</c:if>
			<%-- Changes Done By Kunal for ticket number: 6176 --%>
			<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
				<c:if test="${usertype!='ACCOUNT'}">
					<c:if test="${partner.partnerPortalActive == true && partnerType == 'AG'}">
						<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
					</c:if>
					<c:if test="${partner.partnerPortalActive == true && partnerType == 'AC'}">
					<configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">	<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users</span></a></li></configByCorp:fieldVisibility>
					</c:if>
				</c:if>
			</sec-auth:authComponent>
			<!--<li><a href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}"><span>Policy</span></a></li>
			  		-->
			<c:if test='${paramValue == "View"}'>
				<li><a onclick="setReturnString('gototab.accountlist');return checkdate('none');"><span>Partner List</span></a></li>
			</c:if>
			<c:if test='${paramValue != "View"}'>
				<li><a onclick="setReturnString('gototab.accountlist');return checkdate('none');"><span>Partner List</span></a></li>
			</c:if>
			<!--<li><a href="searchPartner.html?partnerType=AC"><span>Account List</span></a></li>
			  	  --> 
		  <li><a onclick="setReturnString('gototab.partnerReloSvcs');return checkdate('none');"><span>Services</span></a></li> 
				 
				<c:url value="frequentlyAskedQuestionsList.html" var="url">
				<c:param name="partnerCode" value="${partner.partnerCode}"/>
				<c:param name="partnerType" value="AC"/>
				<c:param name="partnerId" value="${partner.id}"/>
				<c:param name="lastName" value="${partner.lastName}"/>
				<c:param name="status" value="${partner.status}"/>
				</c:url>			
				<c:if test="${partnerType == 'AC'}">
					 <c:if test="${not empty partner.id}">
					<li><a href="${url}"><span>FAQ</span></a></li>
					<c:if test="${partner.partnerPortalActive == true}">
						<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
					</c:if>
					  </c:if>
				</c:if>	
				<%-- Modification as per BugID: 6176 FAQ and Notes Position Changed Before Up now down --%>
				<c:if test="${not empty accountProfile.id}">
				<li><a onclick="openNotesPopupTab(this);"><span>Notes</span></a></li>
				<li><a onclick="window.open('auditList.html?id=${accountProfile.id}&tableName=accountprofile&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
			</c:if>					
			</c:if>
	</ul>
	</div>
	<div class="spn">&nbsp;</div>
	</div>
	<div id="Layer1" onkeydown="changeStatus();" style="width: 100%">
	<div id="content" align="center">
	<div id="liquid-round-top">
	<div class="top"><span></span></div>
	<div class="center-content">
	<table class="" cellspacing="0" cellpadding="0" border="0" width="100%">
		<tbody>
			<tr>
				<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>
						<tr>
							<td colspan="8">
							<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" width="75%">
								<tr>
									<td align="right" class="listwhitetext" width="60px">Name</td>
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.lastName" required="true" cssClass="input-textUpper" size="50" maxlength="80" readonly="true" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.partnerCode' /></td>
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="8" readonly="true" /></td>
									<td align="right" class="listwhitetext">Status</td>
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.status" required="true" cssClass="input-textUpper" size="15" readonly="true" /></td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width=""><fmt:message key='accountProfile.stage' />:</td>
							<td class="listwhitetext" width="400px"><s:radio id="stage" name="accountProfile.stage" list="%{stageunits}" onclick="changeStatus();" tabindex="1" /></td>
							
						    <c:set var="ischeckedStatus" value="false"/>
	 			            <c:if test="${accountProfile.vip}">
						    <c:set var="ischeckedStatus" value="true"/>
				            </c:if>	
						
						<td align="right" class="listwhitetext" width="60px">VIP</td><td  class="listwhitetext" ><s:checkbox key="accountProfile.vip" value="${ischeckedStatus}" fieldValue="true" onclick="changeStatus();" /></td>
						
						</tr>
						<tr>
							<td height="8px"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext"><fmt:message key='accountProfile.type' /></td>
							<td><s:select cssClass="list-menu" name="accountProfile.type" cssStyle="width:160px" list="{'Private','Public','RMC'}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="2" /></td>
							<td align="right" class="listwhitetext"><fmt:message key='accountProfile.industry' /></td>
							<td><s:select cssClass="list-menu" name="accountProfile.industry" cssStyle="width:170px" list="%{industry}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="3" /></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext"><fmt:message key='accountProfile.employees' /></td>
							<td class="listwhitetext"><s:textfield cssClass="input-text" name="accountProfile.employees" cssStyle="width:160px" maxlength="15" tabindex="4" /></td>
							<td align="right" class="listwhitetext"><fmt:message key='accountProfile.currency' /></td>
							<td align="left" class="listwhitetext"><s:select cssClass="list-menu" key="accountProfile.currency" cssStyle="width:170px" list="%{currency}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="5" /></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext"><fmt:message key='accountProfile.source' /></td>
							<td class="listwhitetext"><s:select cssClass="list-menu" name="accountProfile.source" list="%{lead}" cssStyle="width:245px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="6" /></td>
							<td align="right" class="listwhitetext"><fmt:message key='accountProfile.revenue' /></td>
							<td><s:select cssClass="list-menu" name="accountProfile.revenue" cssStyle="width:170px" list="%{revenue}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="7" /></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext"><fmt:message key='accountProfile.country' /></td>
							<td colspan=""><s:select cssClass="list-menu" name="accountProfile.country" cssStyle="width:245px" list="%{ocountry}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="8" /></td>
							<td align="right" class="listwhitetext"><fmt:message key='accountProfile.owner' /></td>
							<td><s:select cssClass="list-menu" name="accountProfile.owner" cssStyle="width:170px" list="%{sale}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="9" /></td>
							<c:if test="${empty accountProfile.id}">
								<td colspan="3" align="right" style="width:605px" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
							</c:if>
							<c:if test="${not empty accountProfile.id}">
								<c:choose>
									<c:when test="${countAccountProfileNotes == '0' || countAccountProfileNotes == '' || countAccountProfileNotes == null}">
										<td colspan="3" align="right" style="width:605px" valign="bottom"><img id="countAccountProfileNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="openNotesPopup(this);" /><a onclick="openNotesPopup(this);"></a></td>
									</c:when>
									<c:otherwise>
										<td colspan="3" align="right" style="width:605px" valign="bottom"><img id="countAccountProfileNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="openNotesPopup(this);" /><a onclick="openNotesPopup(this);"></a></td>
									</c:otherwise>
								</c:choose>
							</c:if>
						</tr>
						<tr>
							<td height="15px;"></td>
						</tr>
					</tbody>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan="5" width="100%" style="margin: 0px;">
				<span onclick="swapImg(img1);">
					<div onClick="javascript:animatedcollapse.toggle('rating'); " style="margin:0px;">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
						<tr>
							<td class="probg_left"></td>
							<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img1" src="${pageContext.request.contextPath}/images/129.png" /></a>&nbsp;Rating</td>
							<td class="probg_right"></td>
						</tr>
					</table>
					</div>
				</span>
				<c:if test="${agentRatingVal=='N'}">
					<div id="rating" style="margin:0px; padding:0px; margin-left:10px; margin-bottom:0.3em;">
						<iframe src="partnerAccountRating.html?ajax=1&decorator=simple&popup=true&partnerCode=${partner.partnerCode}&id=${partner.id}" WIDTH="100%" FRAMEBORDER=0>
							<p>Your browser does not support iframes.</p>
						</iframe>
					</div>
				</c:if>
				<c:if test="${agentRatingVal=='Y'}">
					<div id="rating" style="margin:0px; padding:0px; margin-left:10px; margin-bottom:0.3em;">
						<iframe src ="agentRatingCalculation.html?ajax=1&decorator=simple&popup=true&partnerCode=${partner.partnerCode}"  WIDTH="100%" FRAMEBORDER=0>
							<p>Your browser does not support iframes.</p>
						</iframe>
					</div>
				</c:if>
				</td>
			</tr>
			<tr>
				<td colspan="8" width="100%" style="margin: 0px;">
				<span onclick="swapImg(img2);">
					<div onClick="javascript:animatedcollapse.toggle('activity');" style="margin:0px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
							<tr>
								<td class="probg_left"></td>
								<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img2" src="${pageContext.request.contextPath}/images/129.png" /></a>&nbsp;Activity Analysis</td>
								<td class="probg_right"></td>
							</tr>
						</table>
					</div>
				</span>
				<div id="activity" style="margin:0px;padding:0px; margin-left:10px; margin-bottom:0.3em;">
					<iframe src="partnerAccountActivity.html?ajax=1&decorator=simple&popup=true&partnerCode=${partner.partnerCode}&id=${partner.id}" WIDTH="100%" FRAMEBORDER=0>
						<p>Your browser does not support iframes.</p>
					</iframe>
				</div>
				</td>
			</tr>
						
		<tr>
							<td  colspan="5" width="100%">
								<span onclick="swapImg(img8);"><div  onClick="javascript:animatedcollapse.toggle('profile_photo')" style="margin: 0px;">
       								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;">
										<tr>
											<td class="probg_left"></td>
											<td NOWRAP class="probg_center"><a href="javascript:;"><img id="img8" src="${pageContext.request.contextPath}/images/129.png" /></a>&nbsp;Profile Photos</td>
											<td class="probg_right"></td>
										</tr>
									</table>
								</div>
								</span>	
								<div id="profile_photo" >
    								<table width="100%" style="padding-left:10px;">    
								      <tr>
								      	<td colspan="5">
								      		<div class="wrapper-img">
											    <c:if test="${(partner.location1 == null || partner.location1 == '')
											    && (partner.location2 == null || partner.location2 == '')
											    && (partner.location3 == null || partner.location3 == '')
											    && (partner.location4 == null || partner.location4 == '')
											    }">
												    <br>
												    <b>No photo available.</b>
											    </c:if>  
      											<c:if test="${!(partner.location1 == null || partner.location1 == '')}">      
													<div class="thumbline">
												 		<img id="userImage1" class="urClass"  src="UserImage?location=${partner.location1}" alt=""  onload="secImage1()" style="border:thin solid #219DD1; vertical-align: middle; "/>
												 	</div>
												</c:if>
												<c:if test="${!(partner.location2 == null || partner.location2 == '')}">
													<div class="thumbline">
														<img id="userImage2" class="urClass" src="UserImage?location=${partner.location2}" alt=""  onload="secImage2()"    style="border:thin solid #219DD1; vertical-align: middle; "/>
													</div>
												</c:if>
												<c:if test="${!(partner.location3 == null || partner.location3 == '')}">
													<div class="thumbline">
														<img id="userImage3" class="urClass" src="UserImage?location=${partner.location3}" alt="" onload="secImage3()"  style="border:thin solid #219DD1; vertical-align: middle; "/>
													</div>
												</c:if>
												<c:if test="${!(partner.location4 == null || partner.location4 == '')}">
													<div class="thumbline">
														<img id="userImage4" class="urClass" src="UserImage?location=${partner.location4}" alt="" onload="secImage4()"   style="border:thin solid #219DD1; vertical-align: middle;"/>
													</div>
												</c:if>
											</div>
      									</td>
      								</tr>     
								</table>
							</div>
						</td>
					</tr>
			
						
		</tbody>
	</table>
	</div>
	<div class="bottom-header" style="!margin-top:45px;"><span></span></div>
	</div>
	</div>
	</div>
	<table border="0">
		<tr>
		<tr>
			<fmt:formatDate var="cartonCreatedOnFormattedValue" value="${accountProfile.createdOn}" pattern="${displayDateTimeEditFormat}" />
			<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='accountProfile.createdOn' /></td>
			<s:hidden name="accountProfile.createdOn" value="${cartonCreatedOnFormattedValue}" />
			<td><fmt:formatDate value="${accountProfile.createdOn}" pattern="${displayDateTimeFormat}" /></td>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='accountProfile.createdBy' /></td>
			<%-- 
					
						<td align="right" class="listwhitetext" style="width:70px "><b><fmt:message key='accountProfile.createdOn'/></b></td>
						<c:if test="${not empty accountProfile.createdOn}">
							<s:text id="createdOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountProfile.createdOn"/></s:text>
							<td><s:hidden  id="createdOn" name="accountProfile.createdOn" value="%{createdOnFormattedValue}" /></td>
						</c:if>
						<c:if test="${empty accountProfile.createdOn}">
							<td><s:hidden cssClass="input-text" id="createdOn" name="accountProfile.createdOn" /></td>
						</c:if>
						<td style="width:130px"><fmt:formatDate value="${accountProfile.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='accountProfile.createdBy' /></b></td>
						--%>
			<c:if test="${not empty accountProfile.id}">
				<s:hidden name="accountProfile.createdBy" />
				<td style="width:85px"><s:label name="createdBy" value="%{accountProfile.createdBy}" /></td>
			</c:if>
			<c:if test="${empty accountProfile.id}">
				<s:hidden name="accountProfile.createdBy" value="${pageContext.request.remoteUser}" />
				<td><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
			</c:if>
			<td align="right" class="listwhitetext" style="width:110px"><b><fmt:message key='accountProfile.updatedOn' /></b></td>
			<s:text id="updatedOnFormattedValue" name="${FormDateValue}">
				<s:param name="value" value="accountProfile.updatedOn" />
			</s:text>
			<td><s:hidden cssClass="input-text" id="updatedOn" name="accountProfile.updatedOn" value="%{updatedOnFormattedValue}" /></td>
			<td style="width:130px"><fmt:formatDate value="${accountProfile.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='accountProfile.updatedBy' /></b></td>
			<c:if test="${not empty accountProfile.id}">
				<s:hidden name="accountProfile.updatedBy" />
				<td><s:label name="updatedBy" value="%{accountProfile.updatedBy}" /></td>
			</c:if>
			<c:if test="${empty accountProfile.id}">
				<s:hidden name="accountProfile.updatedBy" value="${pageContext.request.remoteUser}" />
				<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}" /></td>
			</c:if>
		</tr>
	</table>
	<c:if test="${empty param.popup}">
		<s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:50px; height:25px " tabindex="10" />
		<s:reset cssClass="cssbutton" key="Reset" cssStyle="width:50px; height:25px " tabindex="11" />
	</c:if>
	<c:if test="${hitflag == 1}">
		<c:redirect url="/editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}" />
	</c:if>
</s:form>
<script type="text/javascript">  

var param = '<%=session.getAttribute("paramView")%>';
try{
if(param == 'View'){
	document.forms['accountProfileForm'].elements['stageProspect'].checked="checked";
	document.forms['accountProfileForm'].elements['stageQuote'].disabled="true";
	document.forms['accountProfileForm'].elements['stagePanel'].disabled="true";
	document.forms['accountProfileForm'].elements['stageContract'].disabled="true";
	document.forms['accountProfileForm'].elements['stageMajor'].disabled="true";
}

} catch(e){}
try{
<c:if test="${not empty param.popup}" > 
	for(i=0;i<100;i++){
		document.forms['accountProfileForm'].elements[i].disabled = true;
	}	 
</c:if>
}
catch(e){} 
try{
Form.focusFirstElement($("accountProfileForm")); 
}
catch(e){}
</script>
