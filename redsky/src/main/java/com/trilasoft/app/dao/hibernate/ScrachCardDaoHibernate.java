package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ScrachCardDao;
import com.trilasoft.app.model.ScrachCard;

public class ScrachCardDaoHibernate  extends GenericDaoHibernate<ScrachCard, Long> implements ScrachCardDao {

	public ScrachCardDaoHibernate() {   
        super(ScrachCard.class);   
    } 
	
	public List getScrachCardByUser(String userName){
		return getHibernateTemplate().find("from ScrachCard where createdBy=?", userName);
	}
	public List getSchedulerScrachCardByUser(String userName ,Date workDate){
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( workDate ) );
		return getHibernateTemplate().find("from ScrachCard where workDate='"+formatedDate1+"' and createdBy='"+userName+"'");
	}
	public List getSchedulerScrachCardByDate(Date workDate){
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( workDate ) );
		return getHibernateTemplate().find("from ScrachCard where workDate='"+formatedDate1+"'");
	}

}
