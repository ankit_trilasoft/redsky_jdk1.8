package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.PortDao;
import com.trilasoft.app.dao.hibernate.InventoryArticleDaoHibernate.DTO;
import com.trilasoft.app.model.Port;

public class PortDaoHibernate extends GenericDaoHibernate<Port,Long> implements PortDao{
	public PortDaoHibernate() 
    {   
        super(Port.class);   
    
    }
	public List findByPort(String pName) {
		return getHibernateTemplate().find("from Port where pName=?",pName);
	}
	public List searchByPort(String portCode, String portName, String country,String modeType, String corpId,String active) {
		if(active.equalsIgnoreCase("true")){
			return getHibernateTemplate().find("from Port where active=true and portCode like '%"+ portCode.replaceAll("'", "''").replaceAll(":", "''") + "%' AND portName like '%"+ portName.replaceAll("'", "''").replaceAll(":", "''") +"%' AND country like '"+country.replaceAll("'", "''").replaceAll(":", "''") +"%' AND mode like '"+modeType.replaceAll("'", "''").replaceAll(":", "''") +"%' and corpID in ('" + corpId + "','XXXX','TSFT','NULL')");
		}else{
		return getHibernateTemplate().find("from Port where active=false and portCode like '%"+ portCode.replaceAll("'", "''").replaceAll(":", "''") + "%' AND portName like '%"+ portName.replaceAll("'", "''").replaceAll(":", "''") +"%' AND country like '"+country.replaceAll("'", "''").replaceAll(":", "''") +"%' AND mode like '"+modeType.replaceAll("'", "''").replaceAll(":", "''") +"%' and corpID in ('" + corpId + "','XXXX','TSFT','NULL')");
		}
	}
	public List findMaximumId(){
		return getHibernateTemplate().find("select max(id) from Port");
	}
	public List portExisted(String portCode, String corpId) {
		return getHibernateTemplate().find("from Port where portCode = '"+portCode+"'  and corpID in ('" + corpId + "','TSFT')");
	}
	public List portListByCorpId(String corpId){
		return getHibernateTemplate().find("from Port where active=true and corpID in ('" + corpId + "','XXXX','TSFT','NULL')");
	}
	public List searchByPortCountry(String portCode, String portName, String country, String modeType, String sessionCorpID) {
		return getHibernateTemplate().find("from Port where active=true and portCode like '"+ portCode.replaceAll("'", "''").replaceAll(":", "''") + "%' AND portName like '"+ portName.replaceAll("'", "''").replaceAll(":", "''") +"%' AND countryCode like '"+country.replaceAll("'", "''").replaceAll(":", "''") +"%' AND mode ='"+modeType+"' and corpID in ('" + sessionCorpID + "','XXXX','TSFT','NULL')");
	}
    public int updatePortActiveStatus(String Status,Long id, String updatedBy) {  
		if(Status.equals("true"))
		{	
		return getHibernateTemplate().bulkUpdate("update Port set active=true,updatedOn = now(),updatedBy='"+updatedBy+"' where id='"+id+"'");		
		}
		else
		{			
			return getHibernateTemplate().bulkUpdate("update Port set active=false,updatedOn = now(),updatedBy='"+updatedBy+"' where id='"+id+"'");
		}
	}
    public List getPortName(String country, String sessionCorpID, String portCode) { 
		return getHibernateTemplate().find("from Port where portCode = '"+ portCode +"' AND countryCode = '"+country+"'  and corpID in ('" + sessionCorpID + "','XXXX','TSFT','NULL')");
	}
    
    class DTO{
		private Object polCode;
		private  Object carrierDeparture;
		public Object getPolCode() {
			return polCode;
		}
		public void setPolCode(Object polCode) {
			this.polCode = polCode;
		}
		public Object getCarrierDeparture() {
			return carrierDeparture;
		}
		public void setCarrierDeparture(Object carrierDeparture) {
			this.carrierDeparture = carrierDeparture;
		}

}
	 public List getPOLAutoComplete(String sessionCorpID,String polCode,String mode){
		List dtoList=new ArrayList();		
		List list = this.getSession().createSQLQuery("select  portCode,portName from port where corpID in ('" + sessionCorpID + "','TSFT') and (portCode like '%"+polCode+"%' or portName like '%"+polCode+"%') and active=true and mode='"+mode+"' ").list();
	    Iterator itr=list.iterator();
	    DTOPOL dto=null;
	    while(itr.hasNext()){
	     dto=new DTOPOL();
	    Object[] obj=(Object[])itr.next();
	    dto.setPolCode(obj[0].toString());
	    dto.setCarrierDeparture(obj[1].toString());
	    dtoList.add(dto);
	    }
	    return dtoList;
	 }
	 class DTOPOE{
			private Object poeCode;
			private  Object carrierArrival;
			public Object getPoeCode() {
				return poeCode;
			}
			public Object getCarrierArrival() {
				return carrierArrival;
			}
			public void setPoeCode(Object poeCode) {
				this.poeCode = poeCode;
			}
			public void setCarrierArrival(Object carrierArrival) {
				this.carrierArrival = carrierArrival;
			}
			
	}
	 public class DTOPOL{
			private Object polCode;
			private  Object carrierDeparture;
			public Object getPolCode() {
				return polCode;
			}
			public void setPolCode(Object polCode) {
				this.polCode = polCode;
			}
			public Object getCarrierDeparture() {
				return carrierDeparture;
			}
			public void setCarrierDeparture(Object carrierDeparture) {
				this.carrierDeparture = carrierDeparture;
			}
	}
		 public List getPOEAutoComplete(String sessionCorpID,String poeCode,String mode){
			List dtoList=new ArrayList();		
			List list = this.getSession().createSQLQuery("select  portCode,portName from port where corpID in ('" + sessionCorpID + "','TSFT') and portCode like '%"+poeCode+"%' and active=true and mode='" + mode + "' ").list();
		    Iterator itr=list.iterator();
		    while(itr.hasNext()){
		    DTOPOE dto=new DTOPOE();
		    Object[] obj=(Object[])itr.next();
		    dto.setPoeCode(obj[0].toString());
		    dto.setCarrierArrival(obj[1].toString());
		    dtoList.add(obj);
		    }
		    return dtoList;
		 }
}
