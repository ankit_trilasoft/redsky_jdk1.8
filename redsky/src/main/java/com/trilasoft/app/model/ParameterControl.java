package com.trilasoft.app.model;
import java.util.Date;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity
@Table(name="parametercontrol")
@FilterDefs({
	@FilterDef(name = "refMasterCorpID", parameters = { @ParamDef(name = "partnerCorpID", type = "string") })
})

@Filters( {
	@Filter(name = "refMasterCorpID", condition = "corpID in (:partnerCorpID) ")
} )
public class ParameterControl extends BaseObject {
	private Long id;
	private String corpID;
	private String parameter;
	private String comments;
	private Integer fieldLength;
	private Boolean tsftFlag;
	private Boolean hybridFlag;
	private String customType;
	
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String createdBy;
	private Boolean active;
	private String childParameter;
	private String description;
	private String nameonform;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("parameter", parameter)
				.append("comments", comments)
				.append("fieldLength", fieldLength)
				.append("tsftFlag", tsftFlag).append("hybridFlag", hybridFlag)
				.append("customType", customType)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("createdBy", createdBy)
				.append("active", active).append("childParameter", childParameter)
				.append("description", description)
				.append("nameonform", nameonform)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ParameterControl))
			return false;
		ParameterControl castOther = (ParameterControl) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(parameter, castOther.parameter)
				.append(comments, castOther.comments)
				.append(fieldLength, castOther.fieldLength)
				.append(tsftFlag, castOther.tsftFlag)
				.append(hybridFlag, castOther.hybridFlag)
				.append(customType, castOther.customType)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(createdBy, castOther.createdBy)
				.append(active, castOther.active)
				.append(childParameter, castOther.childParameter)
				.append(description, castOther.description)
				.append(nameonform, castOther.nameonform)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(parameter).append(comments).append(fieldLength)
				.append(tsftFlag).append(hybridFlag).append(customType)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(createdBy).append(active)
				.append(childParameter)
				.append(description)
				.append(nameonform)
				.toHashCode();
	}
	@Column(length=255)
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	@Column(length=100)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=2)
	public Integer getFieldLength() {
		return fieldLength;
	}
	public void setFieldLength(Integer fieldLength) {
		this.fieldLength = fieldLength;
	}
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=50)
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	@Column(length=100)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Boolean getTsftFlag() {
		return tsftFlag;
	}
	public void setTsftFlag(Boolean tsftFlag) {
		this.tsftFlag = tsftFlag;
	}
	public Boolean getHybridFlag() {
		return hybridFlag;
	}
	public void setHybridFlag(Boolean hybridFlag) {
		this.hybridFlag = hybridFlag;
	}
	public String getCustomType() {
		return customType;
	}
	public void setCustomType(String customType) {
		this.customType = customType;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getChildParameter() {
		return childParameter;
	}
	public void setChildParameter(String childParameter) {
		this.childParameter = childParameter;
	}
	@Column
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column
	public String getNameonform() {
		return nameonform;
	}
	public void setNameonform(String nameonform) {
		this.nameonform = nameonform;
	}
}
