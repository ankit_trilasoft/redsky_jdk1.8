//----Created By Bibhash---

package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.model.AuditSetup;
import com.trilasoft.app.service.AuditSetupManager;
import com.trilasoft.app.service.RefMasterManager;

public class AuditSetupAction extends BaseAction {

	private String sessionCorpID;
    private Long maxId;
	private AuditSetupManager auditSetupManager;

	private Long id;
	private List searchList;

	private Set AuditSetupSet;

	private AuditSetup auditSetup;

	private static List<AuditSetup> auditSetupList;

	private String tableNames;

	private static List fieldList;
	private static List tableList;
	private RefMasterManager refMasterManager;
	private static Map<String, String> prulerole;
	private List multiRoles;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(AuditSetupAction.class);

	
	public AuditSetupAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	 @SkipValidation
	public String auditSetupList() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 auditSetupList = auditSetupManager.getList(sessionCorpID);
		AuditSetupSet = new HashSet(auditSetupList);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	 @SkipValidation
	public String edit() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 getComboList(sessionCorpID);
		 prulerole = refMasterManager.findByParameter(sessionCorpID, "PAUDITROLE");
		 systemAuditable();
		if (id != null) {
			auditSetup = auditSetupManager.get(id);
			fieldList=auditSetupManager.auditSetupField(auditSetup.getTableName(),"true");
			if((auditSetup.getAlertRole()!=null)&&(!auditSetup.getAlertRole().equalsIgnoreCase("")))
	    	 {
	    					multiRoles = new ArrayList();
				String[] ac = auditSetup.getAlertRole().split(",");
				int arrayLength2 = ac.length;
				for (int k = 0; k < arrayLength2; k++) {
					String accConJobType = ac[k];
					multiRoles.add(accConJobType.trim());
					}
			} 
		
		} else {
			auditSetup = new AuditSetup();
			auditSetup.setAuditable("true");
			auditSetup.setCreatedOn(new Date());
			auditSetup.setValueDate(new Date());		
			auditSetup.setUpdatedOn(new Date());
		}
		auditSetup.setCorpID(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		if(auditSetup.getAlertRole() == null || auditSetup.getAlertRole().isEmpty() )
		{
			auditSetup.setAlertRole("");
		}
		boolean isNew = (auditSetup.getId() == null);
		systemAuditable();
		List isexisted=auditSetupManager.checkData(auditSetup.getTableName(),auditSetup.getFieldName(),sessionCorpID);
		if(!isexisted.isEmpty()  && isNew){
			errorMessage("Same combination "+auditSetup.getTableName()+" and "+ auditSetup.getFieldName()+" already exists, Please select another combination");
			//auditSetupList();
			fieldList=auditSetupManager.auditSetupField(auditSetup.getTableName(),"true");
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return ERROR; 
		}
	  else{
		 
		  if(isNew)
		    {
			  auditSetup.setCreatedOn(new Date());
		    }
		      else
		        {
		    	  auditSetup.setCreatedOn(auditSetup.getCreatedOn());
					if((auditSetup.getAlertRole()!=null)&&(!auditSetup.getAlertRole().equalsIgnoreCase("")))
			    	 {
			    					multiRoles = new ArrayList();
						String[] ac = auditSetup.getAlertRole().split(",");
						int arrayLength2 = ac.length;
						for (int k = 0; k < arrayLength2; k++) {
							String accConJobType = ac[k];
							multiRoles.add(accConJobType.trim());
							}
					} 
		        }
		  
		  fieldList=auditSetupManager.auditSetupField(auditSetup.getTableName(),"true");
		  auditSetup.setCorpID(sessionCorpID);	
		  auditSetup.setUpdatedBy(getRequest().getRemoteUser());
		  auditSetup.setUpdatedOn(new Date());
		  if((auditSetup.getAlertRole()!=null)&&(!auditSetup.getAlertRole().equalsIgnoreCase(""))){
		  auditSetup.setAlertRole(auditSetup.getAlertRole().replaceAll(", ", ","));
		  }
		  auditSetup= auditSetupManager.save(auditSetup);
		  auditSetupList();
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "AuditSetup has been added successfully" : "AuditSetup has been updated successfully";
		saveMessage(getText(key));
		}
		if (!isNew) { 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return INPUT;   
        } else {   
        	maxId = Long.parseLong(auditSetupManager.findMaximumId().get(0).toString());
        	auditSetup = auditSetupManager.get(maxId);
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
          return SUCCESS;
      } 
	  }
	}

	private String auditFieldName;
	private String auditTableName;
	private String auditAuditable;
	private String isAlertValue;
	
	@SkipValidation
	  public String search()
	    { 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		/*boolean corpID = (auditSetup.getCorpID() == null);
	    boolean tableName = (auditSetup.getTableName() == null);
	    boolean fieldName = (auditSetup.getFieldName() == null);
	    boolean auditable = (auditSetup.getAuditable() == null);
	    
	    System.out.println("\n\n\n\n auditSetup.getAuditable()-->"+auditSetup.getAuditable());
	    
	       if(!tableName || !fieldName || !corpID || !auditable)
	       {*/
	    	   searchList = auditSetupManager.searchAuditSetup(sessionCorpID,auditTableName,auditFieldName,auditAuditable,isAlertValue);
	    	   AuditSetupSet =new HashSet(searchList);
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    return SUCCESS;     
	    } 
	
	
	private String gotoPageString;
	private String validateFormNav;
	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		validateFormNav = "OK";
    	String s = save();
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return gotoPageString;
    }

	 @SkipValidation
	public String systemAuditableField()
	{
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 //System.out.println("\n\ntableName-->>"+tableNames);
		fieldList=auditSetupManager.auditSetupField(this.getTableNames(),"true");
		//System.out.println("\n\nfieldList-->>"+fieldList);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");		
		return SUCCESS;
	}
	 @SkipValidation
	public String systemAuditable()
	{
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 systemAuditableField();
		tableList=auditSetupManager.auditSetupTable("true");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	 private List alertValue;
	 public String getComboList(String corpId){
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 
		 alertValue =new ArrayList();
		 alertValue.add("All Updates");
		 alertValue.add("Network Update");
		 
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
	 }
	
	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public AuditSetup getAuditSetup() {
		return auditSetup;
	}

	public void setAuditSetup(AuditSetup auditSetup) {
		this.auditSetup = auditSetup;
	}

	public void setAuditSetupManager(AuditSetupManager auditSetupManager) {
		this.auditSetupManager = auditSetupManager;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public List getSearchList() {
		return searchList;
	}

	public void setSearchList(List searchList) {
		this.searchList = searchList;
	}

	public List<AuditSetup> getAuditSetupList() {
		return auditSetupList;
	}

	public void setAuditSetupList(List<AuditSetup> auditSetupList) {
		this.auditSetupList = auditSetupList;
	}

	public Set getAuditSetupSet() {
		return AuditSetupSet;
	}

	public void setAuditSetupSet(Set auditSetupSet) {
		AuditSetupSet = auditSetupSet;
	}

	public List getFieldList() {
		return fieldList;
	}

	public void setFieldList(List fieldList) {
		this.fieldList = fieldList;
	}

	public List getTableList() {
		return tableList;
	}

	public void setTableList(List tableList) {
		this.tableList = tableList;
	}

	public String getTableNames() {
		return tableNames;
	}

	public void setTableNames(String tableNames) {
		this.tableNames = tableNames;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	public String getAuditAuditable() {
		return auditAuditable;
	}
	public void setAuditAuditable(String auditAuditable) {
		this.auditAuditable = auditAuditable;
	}
	public String getAuditFieldName() {
		return auditFieldName;
	}
	public void setAuditFieldName(String auditFieldName) {
		this.auditFieldName = auditFieldName;
	}
	public String getAuditTableName() {
		return auditTableName;
	}
	public void setAuditTableName(String auditTableName) {
		this.auditTableName = auditTableName;
	}
	public static Map<String, String> getPrulerole() {
		return prulerole;
	}
	public static void setPrulerole(Map<String, String> prulerole) {
		AuditSetupAction.prulerole = prulerole;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}	
	public List getAlertValue() {
		return alertValue;
	}
	public void setAlertValue(List alertValue) {
		this.alertValue = alertValue;
	}
	public String getIsAlertValue() {
		return isAlertValue;
	}
	public void setIsAlertValue(String isAlertValue) {
		this.isAlertValue = isAlertValue;
	}
	public List getMultiRoles() {
		return multiRoles;
	}
	public void setMultiRoles(List multiRoles) {
		this.multiRoles = multiRoles;
	}
	
}
