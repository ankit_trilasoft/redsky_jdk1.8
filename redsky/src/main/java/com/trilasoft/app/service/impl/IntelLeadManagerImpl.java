package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.IntelLeadDao;
import com.trilasoft.app.model.IntelLead;
import com.trilasoft.app.service.IntelLeadManager;

public class IntelLeadManagerImpl extends GenericManagerImpl<IntelLead, Long> implements IntelLeadManager{

	IntelLeadDao intelLeadDao;
	public IntelLeadManagerImpl(IntelLeadDao intelLeadDao) {
		super(intelLeadDao);
		this.intelLeadDao = intelLeadDao;
	}
	
	public List<IntelLead> getIntelLeadLists(String corpId)
	{
		return intelLeadDao.getIntelLeadLists(corpId);
	}
	
	public void updateActionValue(Long id, String sessionCorpID, String shipnumber, String seqNum){
		 intelLeadDao.updateActionValue(id, sessionCorpID, shipnumber, seqNum);
	}

}
