package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="todorule")

/*@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")*/
@FilterDef(name = "todoRuleCorpID", parameters = { @ParamDef(name = "todoRuleCorpIDValue", type = "string") })
@Filter(name = "todoRuleCorpID", condition = "corpID in (:todoRuleCorpIDValue) ")
public class ToDoRule extends BaseObject{
	
	private Long id;
	private String entitytablerequired;
	private String fieldToValidate1;
	private String fieldToValidate2;
	private String fieldToValidate3;
	private String expression;
	private String testdate;
	private Long durationAddSub;
	private String messagedisplayed;
	private String rolelist;
	private String url;	
	private String corpID;	
	private String status;
	private Boolean checkEnable;
	private String fieldDisplay;
	private Long ruleNumber;
	
	
//	New Field 
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Date valueDate;
	private Date systemDate;
	
	private String ruleType;
	private Date emailSentDateUpdate;
	private String emailAddress;
	
	private String noteType;
	private String noteSubType;
	private String remarks;
	private String emailNotification;
	private Date emailOn;
	private String manualEmail;
	private Boolean publishRule = new Boolean(false);
// Document checklist 
	
	private String docType;
	private String service;
	private String mode;
	private String routing;
	private String jobType;
	private String billToCode;
	private String bookingAgent;
	private String serviceCondition;
	private String modeCondition;
	private String routingCondition;
	private String jobTypeCondition;
	private String billToCodeCondition;
	private String bookingAgentCondition;
	private Boolean isAccount = new Boolean(false);

	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("entitytablerequired", entitytablerequired)
				.append("fieldToValidate1", fieldToValidate1)
				.append("fieldToValidate2", fieldToValidate2)
				.append("fieldToValidate3", fieldToValidate3)
				.append("expression", expression).append("testdate", testdate)
				.append("durationAddSub", durationAddSub)
				.append("messagedisplayed", messagedisplayed)
				.append("rolelist", rolelist).append("url", url)
				.append("corpID", corpID).append("status", status)
				.append("checkEnable", checkEnable)
				.append("fieldDisplay", fieldDisplay)
				.append("ruleNumber", ruleNumber)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("valueDate", valueDate)
				.append("systemDate", systemDate).append("ruleType", ruleType)
				.append("emailSentDateUpdate", emailSentDateUpdate)
				.append("emailAddress", emailAddress)
				.append("noteType", noteType)
				.append("noteSubType", noteSubType).append("remarks", remarks)
				.append("emailNotification", emailNotification)
				.append("emailOn", emailOn).append("manualEmail", manualEmail)
				.append("publishRule", publishRule).append("docType", docType)
				.append("service", service)
				.append("mode", mode)
				.append("routing", routing)
				.append("jobType", jobType)
				.append("billToCode", billToCode)
				.append("bookingAgent", bookingAgent)
				.append("serviceCondition", serviceCondition)
				.append("modeCondition", modeCondition)
				.append("routingCondition", routingCondition)
				.append("jobTypeCondition", jobTypeCondition)
				.append("billToCodeCondition", billToCodeCondition)
				.append("bookingAgentCondition", bookingAgentCondition)
				.append("isAccount", isAccount)
				.toString();
	}



	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ToDoRule))
			return false;
		ToDoRule castOther = (ToDoRule) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(entitytablerequired, castOther.entitytablerequired)
				.append(fieldToValidate1, castOther.fieldToValidate1)
				.append(fieldToValidate2, castOther.fieldToValidate2)
				.append(fieldToValidate3, castOther.fieldToValidate3)
				.append(expression, castOther.expression)
				.append(testdate, castOther.testdate)
				.append(durationAddSub, castOther.durationAddSub)
				.append(messagedisplayed, castOther.messagedisplayed)
				.append(rolelist, castOther.rolelist)
				.append(url, castOther.url).append(corpID, castOther.corpID)
				.append(status, castOther.status)
				.append(checkEnable, castOther.checkEnable)
				.append(fieldDisplay, castOther.fieldDisplay)
				.append(ruleNumber, castOther.ruleNumber)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(valueDate, castOther.valueDate)
				.append(systemDate, castOther.systemDate)
				.append(ruleType, castOther.ruleType)
				.append(emailSentDateUpdate, castOther.emailSentDateUpdate)
				.append(emailAddress, castOther.emailAddress)
				.append(noteType, castOther.noteType)
				.append(noteSubType, castOther.noteSubType)
				.append(remarks, castOther.remarks)
				.append(emailNotification, castOther.emailNotification)
				.append(emailOn, castOther.emailOn)
				.append(manualEmail, castOther.manualEmail)
				.append(publishRule, castOther.publishRule)
				.append(docType, castOther.docType)
				.append(service, castOther.service)
				.append(mode, castOther.mode)
				.append(routing, castOther.routing)
				.append(jobType, castOther.jobType)
				.append(billToCode, castOther.billToCode)
				.append(bookingAgent, castOther.bookingAgent)
				.append(serviceCondition, castOther.serviceCondition)
				.append(modeCondition, castOther.modeCondition)
				.append(routingCondition, castOther.routingCondition)
				.append(jobTypeCondition, castOther.jobTypeCondition)
				.append(billToCodeCondition, castOther.billToCodeCondition)
				.append(bookingAgentCondition, castOther.bookingAgentCondition)
				.append("isAccount", castOther.isAccount)
				.isEquals();
	}



	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(entitytablerequired)
				.append(fieldToValidate1).append(fieldToValidate2)
				.append(fieldToValidate3).append(expression).append(testdate)
				.append(durationAddSub).append(messagedisplayed)
				.append(rolelist).append(url).append(corpID).append(status)
				.append(checkEnable).append(fieldDisplay).append(ruleNumber)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(valueDate).append(systemDate)
				.append(ruleType).append(emailSentDateUpdate)
				.append(emailAddress).append(noteType).append(noteSubType)
				.append(remarks).append(emailNotification).append(emailOn)
				.append(manualEmail).append(publishRule).append(docType)
				.append(service)
				.append(mode)
				.append(routing)
				.append(jobType)
				.append(billToCode)
				.append(bookingAgent)
				.append(serviceCondition)
				.append(modeCondition)
				.append(routingCondition)
				.append(jobTypeCondition)
				.append(billToCodeCondition)
				.append(bookingAgentCondition)
				.append(isAccount)
				.toHashCode();
	}



	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length=50)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column(length=50)
	public Date getUpdatedOn() {
		return updatedOn;
	}
	
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=10)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=50)
	public String getEntitytablerequired() {
		return entitytablerequired;
	}

	public void setEntitytablerequired(String entitytablerequired) {
		this.entitytablerequired = entitytablerequired;
	}
	@Column(length=5000)
	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}
	@Column(length=75)
	public String getFieldToValidate1() {
		return fieldToValidate1;
	}

	public void setFieldToValidate1(String fieldToValidate1) {
		this.fieldToValidate1 = fieldToValidate1;
	}
	@Column(length=75)
	public String getFieldToValidate2() {
		return fieldToValidate2;
	}

	public void setFieldToValidate2(String fieldToValidate2) {
		this.fieldToValidate2 = fieldToValidate2;
	}
	@Column(length=75)
	public String getFieldToValidate3() {
		return fieldToValidate3;
	}

	public void setFieldToValidate3(String fieldToValidate3) {
		this.fieldToValidate3 = fieldToValidate3;
	}
	@Column(length=100)
	public String getMessagedisplayed() {
		return messagedisplayed;
	}

	public void setMessagedisplayed(String messagedisplayed) {
		this.messagedisplayed = messagedisplayed;
	}
	@Column(length=500)
	public String getRolelist() {
		return rolelist;
	}

	public void setRolelist(String rolelist) {
		this.rolelist = rolelist;
	}
	@Column(length=30)
	public Date getSystemDate() {
		return systemDate;
	}

	public void setSystemDate(Date systemDate) {
		this.systemDate = systemDate;
	}
	@Column(length=50)
	public String getTestdate() {
		return testdate;
	}

	public void setTestdate(String testdate) {
		this.testdate = testdate;
	}
	@Column(length=30)
	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	@Column(length=75)
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	@Column(length=75)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getCheckEnable() {
		return checkEnable;
	}

	public void setCheckEnable(Boolean checkEnable) {
		this.checkEnable = checkEnable;
	}
	@Column
	public Long getDurationAddSub() {
		return durationAddSub;
	}

	public void setDurationAddSub(Long durationAddSub) {
		this.durationAddSub = durationAddSub;
	}

	@Column(length=75)
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Column
	public Date getEmailSentDateUpdate() {
		return emailSentDateUpdate;
	}

	public void setEmailSentDateUpdate(Date emailSentDateUpdate) {
		this.emailSentDateUpdate = emailSentDateUpdate;
	}

	@Column(length=45)
	public String getRuleType() {
		return ruleType;
	}

	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}


	@Column(length=75)
	public String getFieldDisplay() {
		return fieldDisplay;
	}
	public void setFieldDisplay(String fieldDisplay) {
		this.fieldDisplay = fieldDisplay;
	}
	@Column( length=30 )
	public String getNoteSubType() {
		return noteSubType;
	}
	public void setNoteSubType(String noteSubType) {
		this.noteSubType = noteSubType;
	}
	@Column( length=30 )
	public String getNoteType() {
		return noteType;
	}
	public void setNoteType(String noteType) {
		this.noteType = noteType;
	}
	@Column( length=500 )
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Long getRuleNumber() {
		return ruleNumber;
	}
	public void setRuleNumber(Long ruleNumber) {
		this.ruleNumber = ruleNumber;
	}
	@Column( length=55 )
	public String getEmailNotification() {
		return emailNotification;
	}
	public void setEmailNotification(String emailNotification) {
		this.emailNotification = emailNotification;
	}
	@Column(length=30)
	public Date getEmailOn() {
		return emailOn;
	}
	public void setEmailOn(Date emailOn) {
		this.emailOn = emailOn;
	}


	@Column
	public String getManualEmail() {
		return manualEmail;
	}


	public void setManualEmail(String manualEmail) {
		this.manualEmail = manualEmail;
	}

	@Column
	public Boolean getPublishRule() {
		return publishRule;
	}

	public void setPublishRule(Boolean publishRule) {
		this.publishRule = publishRule;
	}


	@Column(length=25)
	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}


	@Column
	public String getBookingAgent() {
		return bookingAgent;
	}



	public void setBookingAgent(String bookingAgent) {
		this.bookingAgent = bookingAgent;
	}


	@Column
	public String getService() {
		return service;
	}



	public void setService(String service) {
		this.service = service;
	}


	@Column
	public String getMode() {
		return mode;
	}



	public void setMode(String mode) {
		this.mode = mode;
	}


	@Column
	public String getRouting() {
		return routing;
	}



	public void setRouting(String routing) {
		this.routing = routing;
	}


	@Column
	public String getJobType() {
		return jobType;
	}



	public void setJobType(String jobType) {
		this.jobType = jobType;
	}


	@Column
	public String getBillToCode() {
		return billToCode;
	}



	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}


	@Column
	public String getServiceCondition() {
		return serviceCondition;
	}

	public void setServiceCondition(String serviceCondition) {
		this.serviceCondition = serviceCondition;
	}

	@Column
	public String getModeCondition() {
		return modeCondition;
	}

	public void setModeCondition(String modeCondition) {
		this.modeCondition = modeCondition;
	}

	@Column
	public String getRoutingCondition() {
		return routingCondition;
	}

	public void setRoutingCondition(String routingCondition) {
		this.routingCondition = routingCondition;
	}

	@Column
	public String getJobTypeCondition() {
		return jobTypeCondition;
	}

	public void setJobTypeCondition(String jobTypeCondition) {
		this.jobTypeCondition = jobTypeCondition;
	}

	@Column
	public String getBillToCodeCondition() {
		return billToCodeCondition;
	}

	public void setBillToCodeCondition(String billToCodeCondition) {
		this.billToCodeCondition = billToCodeCondition;
	}

	@Column
	public String getBookingAgentCondition() {
		return bookingAgentCondition;
	}

	public void setBookingAgentCondition(String bookingAgentCondition) {
		this.bookingAgentCondition = bookingAgentCondition;
	}



	public Boolean getIsAccount() {
		return isAccount;
	}


	public void setIsAccount(Boolean isAccount) {
		this.isAccount = isAccount;
	}
	
	
}
