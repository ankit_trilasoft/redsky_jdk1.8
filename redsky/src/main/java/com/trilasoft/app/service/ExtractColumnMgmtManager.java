package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ExtractColumnMgmt;
import com.trilasoft.app.model.Userlogfile;

public interface ExtractColumnMgmtManager extends GenericManager<ExtractColumnMgmt, Long>{

	public List getExtractColum(String reportName, String sessionCorpID);

	public List getSelectCondition(String selectCondition, String reportName,String sessionCorpID);
	
	public List getReportName(String corpid);
	
	public List getTableName(String corpid);
	
	public List getFieldNameList(String corpid);
	
	public List getDisplayNameList(String corpid);
	
	public List getTableNameByReportName(String reportName, String corpid);
	
	public boolean checkExtractColumnMgmtRecord(String reportName, String tableName, String fieldName, String corpid);
	
	public String checkExtractColumnMgmtRecordId(String reportName, String tableName, String fieldName, String corpid);
	
	public Long getExtractColMgmtSeqNo(String corpId);
	
	public List searchFilterList(String reportName, String tableName, String fieldNameSearch, String displayName, String corpId, boolean tsftflag);
	
	public List searchDefaultList(String corpId);
	
	public List getCorpIdList();
	
}
