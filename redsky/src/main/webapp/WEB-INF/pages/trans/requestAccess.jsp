<%@ include file="/common/taglibs.jsp"%>
<head>
	<title>Partner Request Access</title>
	<meta name="heading" content="Partner Request Access"/>
    <meta name="menu" content="UserIDHelp"/>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
</head>
<script language="javascript" type="text/javascript">

function checkFields(){
	selectAll('userJobType');
	var countryCode= document.forms['partnerRequestAccess'].elements['userCountry'].value;
	var ListBoxOptions = document.getElementsByName('userJobType')[0].options;
	
	if(document.forms['partnerRequestAccess'].elements['userCountry'].value==''){
  			alert("Please select the country");
  			document.forms['partnerRequestAccess'].elements['userCountry'].focus();
  			return false;
  	}
  	if(countryCode=='USA')
  	{
  		if(document.forms['partnerRequestAccess'].elements['userState'].value==''){
  			alert("Please select the state");
  			document.forms['partnerRequestAccess'].elements['userState'].focus();
  			return false;
  	}
  	}
  	if(document.forms['partnerRequestAccess'].elements['userBranch'].value==''){
  			//alert("Please select the company name");
  			//document.forms['partnerRequestAccess'].elements['userBranch'].focus();
  			//return false;
  	}
  	if(document.forms['partnerRequestAccess'].elements['userLastName'].value==''){
  			alert("Please enter the last name");
  			document.forms['partnerRequestAccess'].elements['userLastName'].focus();
  			return false;
  	}
  	if(document.forms['partnerRequestAccess'].elements['userFirstName'].value==''){
  			alert("Please enter the first name");
  			document.forms['partnerRequestAccess'].elements['userFirstName'].focus();
  			return false;
  	}
  	
  	
  	if(document.forms['partnerRequestAccess'].elements['userPhoneNumber'].value==''){
  			alert("Please enter the phone1");
  			document.forms['partnerRequestAccess'].elements['userPhoneNumber'].focus();
  			return false;
  	}
  	if(document.forms['partnerRequestAccess'].elements['phoneType1'].value==''){
  			alert("Please select the type of phone1");
  			document.forms['partnerRequestAccess'].elements['phoneType1'].focus();
  			return false;
  	}
  	if(document.forms['partnerRequestAccess'].elements['userFaxNumber'].value==''){
  			alert("Please enter the phone2");
  			document.forms['partnerRequestAccess'].elements['userFaxNumber'].focus();
  			return false;
  	}
  	if(document.forms['partnerRequestAccess'].elements['phoneType2'].value==''){
  			alert("Please select the  type of phone2");
  			document.forms['partnerRequestAccess'].elements['phoneType2'].focus();
  			return false;
  	}
  	if(document.forms['partnerRequestAccess'].elements['userEmail'].value==''){
  			alert("Please enter the emailID");
  			document.forms['partnerRequestAccess'].elements['userEmail'].focus();
  			return false;
  	}
  	var total = '0';
         for(var i = 0; i < ListBoxOptions.length; i++) {
                
                	total = total+1;
            
        }
        if(total == 0)
        {
        	alert("Please select job function.");
        	return false;
        }
  
	return echeck();
}


    
 function getCompanyName(){
 		var countryCode= document.forms['partnerRequestAccess'].elements['userCountry'].value;
 		var stateCode= document.forms['partnerRequestAccess'].elements['userState'].value;
 		var sessionCorpID= document.forms['partnerRequestAccess'].elements['sessionCorpID'].value;
 		if(countryCode=='')
 		{
 			alert('Please select the country');
 			return false;
 		}else{
 		if(countryCode=='USA')
 		{
 			if(stateCode=='')
 			{
 				alert('Please select the state');
 			}else{
 			javascript:openWindow("agentPopUp.html?partnerType=AG&userCountry="+countryCode+"&sessionCorpID="+sessionCorpID+"&stateCode="+stateCode+"&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=userBranch&fld_code=partnerCode");
 			}
 		}else{
 		
 		javascript:openWindow("agentPopUp.html?partnerType=AG&userCountry="+countryCode+"&sessionCorpID="+sessionCorpID+"&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=userBranch&fld_code=partnerCode");
 		}
		}
	
	}

function copyToAllias(){
	var firstName= document.forms['partnerRequestAccess'].elements['userFirstName'].value;
	document.forms['partnerRequestAccess'].elements['userAlias'].value=firstName;
}
function onlyPhoneNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
	}
	

function echeck() {
var str=document.forms['partnerRequestAccess'].elements['userEmail'].value;
		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   alert("Invalid E-mail ID");
		   document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		   return false;
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert("Invalid E-mail ID");
		    document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		   return false;
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("Invalid E-mail ID");
		     document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		    return false;
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    alert("Invalid E-mail ID");
		    document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		    return false;
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Invalid E-mail ID");
		     document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		    return false;
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    alert("Invalid E-mail ID");
		    document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		    return false;
		 }
		
		 if (str.indexOf(" ")!=-1){
		    alert("Invalid E-mail ID");
		    document.forms['partnerRequestAccess'].elements['userEmail'].focus();
		    return false;
		 }
			
 		 return true;					
	}
	
function displayState()
{
		var country=document.forms['partnerRequestAccess'].elements['userCountry'].value;
	  	var e2 = document.getElementById('hid1');
		if(country == 'USA'){
		e2.style.visibility = 'visible';
		}else{
		e2.style.visibility = 'hidden';
		}
		

}

function copyJobFunction(target){
	document.forms['partnerRequestAccess'].elements['selectedJobFunction'].value= document.forms['partnerRequestAccess'].elements['selectedJobFunction'].value +"," +target.value;
}

function openEmailForm(){
	window.open('agentEmailForm.html?id=${pricingControl.id}&sessionCorpID=${sessionCorpID}&decorator=popup&popup=true','surveysList','height=520,width=800,top=200, left=100, scrollbars=yes,resizable=yes');
}


</script>
<style type="text/css">


div.error, span.error, li.error, div.message {
width:450px;
}

table.pickList11 td select {
    width: 150px;
}
form .info {
text-align:left;
padding-left:15px;
}

input.button, button {
padding:2px 2px 0 0;
width:4em;
}

form {
margin-top:40px;
!margin-top:-5px;
}

div#main {
margin:-5px 0 0;

}
div#branding {

height:10px;

}
</style>



 <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>
<s:form id="partnerRequestAccess" name="partnerRequestAccess" action="saveRequestAccess" method="post" validate="true">
<s:hidden key="user.id"/>
<s:hidden name="partnerCode"/>
<c:set var="sessionCorpID" value="<%=request.getParameter("sessionCorpID") %>" />
<s:hidden name="sessionCorpID" value="<%=request.getParameter("sessionCorpID") %>" />
<s:hidden key="user.version"/>
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<div id="Layer1" style="width:60%;margin-left: 50px;margin-top: -10px;"> 

		<div id="newmnav">
		  <ul>
		  	<li id="newmnav1" ><a class="current"><span>Partner Portal Id Request Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">

<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
   <tr>
 	 	<td align="left" class="listwhitetext" height="30px" colspan="4" style="padding-top:8px;"><font color="red">Please enter data in all the entry fields and then submit your request for access to redsky.</font></td>
 		<td align="left" height="15px"></td>
   </tr>
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center">My Company Details</td>
												<td class="probg_right"></td>
											</tr>
										</table>
										
  	<table border="0" cellpadding="2" cellspacing="1" width="100%">
		  <tbody>  	
		 
		  	<tr>
		  		<td align="right" width="120px" class="listwhitetext"><fmt:message key="user.address.country"/><font color="red" size="2">*</font></td>
		  	  	<td align="left"><s:select cssClass="list-menu" id="userCountry" name="userCountry" list="%{ocountry}" headerKey="" headerValue="" cssStyle="width:170px"  onchange="displayState();"/>
		  	  	<td colspan="2" align="left" style="margin: 0px;padding:0px; ">
				<div id="hid1" style="margin: 0px;padding:0px;">
				 	<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0"  style="margin: 0px;padding:0px;">
					<tbody>
					<tr>
					<td align="right" class="listwhitetext">State<font color="red" size="2">*</font></td>
		  	  		<td align="left"><s:select cssClass="list-menu" id="userState" name="userState" list="%{ostates}" headerKey="" headerValue="" cssStyle="width:170px"/>
					</tr>
					</tbody>
					</table>
				</div>																													
				</td>	
		  	  	
		  	
		  	</tr>
		  	<tr>
		  		<td align="right" class="listwhitetext">Company Name</td>
		  		<td align="left"><s:textfield name="userBranch"  maxlength="100" size="28" cssClass="input-textUpper" readonly="true" /></td>
		  		 		
		  		
		  		<td align="left" class="listwhitetext" colspan="3"><img align="top" class="openpopup" width="17" height="20" onclick="return getCompanyName();" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" />
				<i>(Click to select your company)</i></td>
				
			</tr>
			<tr>
			<td align="right" class="listwhitetext" colspan="2"><font color="red" size="2">*</font>If your company is not listed, click <a  onClick="openEmailForm()" style="text-decoration: underline; cursor: pointer;">here</a> </td>
			</tr>
			<tr><td colspan="7">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
											<tr>
												<td class="probg_left"></td>
												<td NOWRAP class="probg_center">My Details</td>
												<td class="probg_right"></td>
											</tr>
										</table>
										</td>
										</tr>
			
		  	<tr>
		  		<td align="right" class="listwhitetext">First Name<font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield name="userFirstName"  maxlength="100" size="28" cssClass="input-text" readonly="false" onchange="copyToAllias();"/></td>
		  	
		  		<td align="right" class="listwhitetext">Last Name<font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield name="userLastName"  maxlength="100" size="28" cssClass="input-text" readonly="false"/></td>
		  		
		    </tr>
		  	<tr>
		  		<td align="right" class="listwhitetext">Preferred Salutation<font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield name="userAlias"  maxlength="100" size="8" cssClass="input-text" readonly="false"/></td>
		  	</tr>
		  
		  	
		  	<tr>
		  		<td align="right" class="listwhitetext">Phone1:<font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield name="userPhoneNumber"  maxlength="100" size="28" cssClass="input-text" readonly="false" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
		  		<td align="right" class="listwhitetext">Type:<font color="red" size="2">*</font></td>
		  		<td align="left"><s:select cssClass="list-menu"  name="phoneType1" list="{'Work','Cell','Home','Fax'}" headerKey="" headerValue="" cssStyle="width:100px" />
		  	</tr>
		  	<tr>
		  		<td align="right" class="listwhitetext">Phone2:<font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield name="userFaxNumber"  maxlength="100" size="28" cssClass="input-text" readonly="false" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
		  		<td align="right" width="" class="listwhitetext">Type:<font color="red" size="2">*</font></td>
		  		<td align="left"><s:select cssClass="list-menu"  name="phoneType2" list="{'Work','Cell','Home','Fax'}" headerKey="" headerValue="" cssStyle="width:100px" />
		  	</tr>
		  	<tr>
		  		<td align="right" class="listwhitetext">Email<font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield id="userEmail" name="userEmail"  maxlength="100" size="28" cssClass="input-text" readonly="false" onchange="return echeck()"/></td>
		  	</tr>
		  <tr>
		  	
		  	<td align="left" class="colored_bg"  colspan="7">
		  	 
       	 
			        <fieldset >
			            <legend>Job Functions<font color="red" size="2">*</font></legend>
			            <table class="pickList" >
			                <tr>
			                    <th class="pickLabel">
			                        <label class="required">Function List</label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required">Selected Functions</label>
			                    </th>
			                </tr>
			                <c:set var="leftList" value="${availableJobTypes}" scope="request"/>
			                <s:set name="rightList" value="userJobType1" scope="request"/>
			                <c:import url="/WEB-INF/pages/pickList1.jsp">
			                    <c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="availableJobTypes && !userJobType"/>
			                    <c:param name="rightId" value="userJobType"/>
			                </c:import>
			            </table>
			        </fieldset>
			  
			   
			    	
			    </td>
		  	
		  	
		  
		  	</tbody>
	</table>
  
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

		   
		  <table class="detailTabLabel" border="0" style="margin: 0px;padding: 0px;" width="100%">
		  <tbody> 	
		   	
		   <tr>
		  		<td align="right">
        		<s:submit cssClass="cssbutton1" type="button"   value="Submit" onclick="return checkFields();"/>  
        		</td>
       
        		<td align="left">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        	
       	  	</tr>		  	
		  </tbody>
		  </table>
		  
		  </div>

</s:form>

<script type="text/javascript">
<c:if test="${accountMessage=='User already exist with this emailID. Please change the emailID.'}">
document.forms['partnerRequestAccess'].elements['userEmail'].select();
</c:if>
	try{
		var country=document.forms['partnerRequestAccess'].elements['userCountry'].value;
		}
		catch(e){}
		try{
		var e2 = document.getElementById('hid1');
		}
		catch(e){}
		try{
		if(country == 'USA'){
		e2.style.visibility = 'visible';
		}else{
		e2.style.visibility = 'hidden';
		}
		}
		catch(e){}
</script>