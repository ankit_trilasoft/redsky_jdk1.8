package com.trilasoft.app.dao.hibernate;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.NetworkControlDao;
import com.trilasoft.app.model.NetworkControl;
import com.trilasoft.app.service.NetworkControlManager;

public class NetworkControlDaoHibernate extends GenericDaoHibernate<NetworkControl, Long> implements NetworkControlDao {

	public NetworkControlDaoHibernate() {
		super(NetworkControl.class);
		// TODO Auto-generated constructor stub
	}

}
