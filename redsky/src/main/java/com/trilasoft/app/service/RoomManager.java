/**
 * 
 */
package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.Room;

/**
 * @author GVerma
 *
 */
public interface RoomManager extends GenericManager<Room, Long> {

	public List getAllRoom(String sessionCorpID, String shipno);

	public List checkRoom(String corpID, int roomCode, String shipNumber);

}
