package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.UserDevice;

import com.trilasoft.app.dao.UserDeviceDao;

public class UserDeviceDaoHibernate extends GenericDaoHibernate<UserDevice, Long> implements UserDeviceDao{

	public UserDeviceDaoHibernate() {   
        super(UserDevice.class);   
    }
	
	public List findRecordByLoginUser(String loginUser, String deviceIdVal){
		try {
			return getHibernateTemplate().find(" from UserDevice where userName = '"+loginUser+"' and deviceId = '"+deviceIdVal+"' ");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
