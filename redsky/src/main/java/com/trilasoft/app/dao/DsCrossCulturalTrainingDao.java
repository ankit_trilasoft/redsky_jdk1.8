package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.DsCrossCulturalTraining;

public interface DsCrossCulturalTrainingDao extends GenericDao<DsCrossCulturalTraining, Long>{
	List getListById(Long id);
}
