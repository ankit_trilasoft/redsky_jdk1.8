package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.RefCrate;

public interface RefCrateDao extends GenericDao<RefCrate, Long> {

	public List<RefCrate> findByLastName(String lastName);

	public List<RefCrate> findForRefCrate(String lastName, String partnerCode, String billingCountryCode);

	public List<RefCrate> findByBillingCountryCode(String billingCountryCode);
	public List refCrateList(String corpID);
}
