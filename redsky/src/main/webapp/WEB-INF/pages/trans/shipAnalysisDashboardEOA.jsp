<%@ include file="/common/taglibs.jsp"%> 
<head> 
<title>Shipment Analysis Dashboard Embassy of Australia</title> 
<meta name="heading" content="Shipment Analysis Dashboard Embassy of Australia"/>
<style>
body {padding:0px !important;}
div#inner-content {
margin-left:5px;
}
div#main {width:99% !important;}
.export{padding: 4px; width: 100px; background: none repeat scroll 0px 0px #F4F4F4; border: 1px solid rgb(227, 227, 227);}
</style>
<script language="javascript" type="text/javascript">
function validateSO(){
	var url='viewReportWithParam.html?id=2018&reportName=Shipment Analysis By SO&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=355320&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	document.forms['shipAnalysisDashboardEOA'].action =url;
	document.forms['shipAnalysisDashboardEOA'].submit();
	return true;
	}
function validateWEIGHT(){
	var url='viewReportWithParam.html?id=2019&reportName=Shipment Analysis By Weight&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=355320&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	document.forms['shipAnalysisDashboardEOA'].action =url;
	document.forms['shipAnalysisDashboardEOA'].submit();
	return true;
	}
function validateDPORT(){
	var url='viewReportWithParam.html?id=2020&reportName=Shipment Analysis By Destination Port&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=355320&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	document.forms['shipAnalysisDashboardEOA'].action =url;
	document.forms['shipAnalysisDashboardEOA'].submit();
	return true;
	}
</script>
</head> 
<s:form id="shipAnalysisDashboardEOA" name="shipAnalysisDashboardEOA" action="viewReportWithParam" method="post" validate="true">   
 <div id="Layer1" align="center" style="width:100%; margin:0px; padding: 0px;  ">
 <div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top:10px;"><span></span></div>
<div class="center-content" style="padding-right:0px;padding-left:0px;">
<table cellspacing="2" cellpadding="2" border="0" width="95%" style="margin:0px auto;padding:0px;">
<tbody>	
	 <tr>
     <td align="center" style="margin: 0px; padding: 0px">
     <iframe  style="margin: 0px; padding: 0px;" src ="${pageContext.request.contextPath}/images/ShipmentAnalysisSO.jpg"  WIDTH=940 HEIGHT=493 FRAMEBORDER=0 scrolling="no">     
	 </iframe>
	 </td>
	 </tr>
	 <tr>
	 <td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateSO();"/><a href="javascript:void(0)" onClick="return validateSO();"> XLS </a></div></td>
     </tr>
	 
    <tr>
    <td align="center" style="margin: 0px; padding: 0px">
    <iframe  style="margin: 0px; padding: 0px; " src ="${pageContext.request.contextPath}/images/ShipmentAnalysisWeight.jpg"  WIDTH=940 HEIGHT=493 FRAMEBORDER=0 scrolling="no"></iframe>
	</td>
	</tr>
	<tr>
	<td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateWEIGHT();"/><a href="javascript:void(0)" onClick="return validateWEIGHT();"> XLS </a></div></td>
  	</tr>
	
	<tr>
    <td align="center" style="margin: 0px; padding: 0px">
    <iframe  style="margin: 0px; padding: 0px;" src ="${pageContext.request.contextPath}/images/ShipmentAnalysisDPort.jpg"  WIDTH=940 HEIGHT=493 FRAMEBORDER=0 scrolling="no"></iframe>
	</td>
	</tr>
	<tr>
	<td align="center"><div class="export">Export: <img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateDPORT();"/><a href="javascript:void(0)" onClick="return validateDPORT();"> XLS </a></div></td>
    </tr>
  
  
 <tr>
 </tr>
 <tr>
 </tr>

</tbody></table>
</div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div> 
</div>
</div>
</div>      
</s:form>
</html>