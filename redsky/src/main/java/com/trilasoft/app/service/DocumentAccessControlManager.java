package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.DocumentAccessControl;

public interface DocumentAccessControlManager extends GenericManager<DocumentAccessControl, Long> {

	List isExisted(String fileType, String sessionCorpID);

}
