delimiter $$

CREATE trigger redsky.trigger_add_history_datacatalog
BEFORE UPDATE on redsky.datacatalog
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";


  




   IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'datacatalog.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;




   IF (OLD.fieldName <> NEW.fieldName ) THEN
      select CONCAT(fieldNameList,'datacatalog.fieldName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fieldName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fieldName,'~') into newValueList;
  END IF;





 IF ((OLD.valueDate <> NEW.valueDate) or (OLD.valueDate is null and NEW.valueDate is not null)
      or (OLD.valueDate is not null and NEW.valueDate is null)) THEN

       select CONCAT(fieldNameList,'charges.valueDate~') into fieldNameList;
       IF(OLD.valueDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.valueDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.valueDate is not null) THEN
        	select CONCAT(oldValueList,OLD.valueDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.valueDate is not null) THEN
        	select CONCAT(newValueList,NEW.valueDate,'~') into newValueList;
      	END IF;

   END IF;



   IF (OLD.tableName <> NEW.tableName ) THEN
      select CONCAT(fieldNameList,'datacatalog.tableName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tableName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tableName,'~') into newValueList;
  END IF;





   IF (OLD.rulesFiledName <> NEW.rulesFiledName ) THEN
      select CONCAT(fieldNameList,'datacatalog.rulesFiledName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.rulesFiledName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.rulesFiledName,'~') into newValueList;
  END IF;





    IF (OLD.flag <> NEW.flag ) THEN
       select CONCAT(fieldNameList,'datacatalog.flag~') into fieldNameList;
       IF(OLD.flag = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.flag = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.flag = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.flag = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




    IF (OLD.editable <> NEW.editable ) THEN
       select CONCAT(fieldNameList,'datacatalog.editable~') into fieldNameList;
       IF(OLD.editable = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.editable = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.editable = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.editable = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'datacatalog.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;




   IF (OLD.auditable <> NEW.auditable ) THEN
      select CONCAT(fieldNameList,'datacatalog.auditable~') into fieldNameList;
      select CONCAT(oldValueList,OLD.auditable,'~') into oldValueList;
      select CONCAT(newValueList,NEW.auditable,'~') into newValueList;
  END IF;



   IF (OLD.isdateField <> NEW.isdateField ) THEN
      select CONCAT(fieldNameList,'datacatalog.isdateField~') into fieldNameList;
      select CONCAT(oldValueList,OLD.isdateField,'~') into oldValueList;
      select CONCAT(newValueList,NEW.isdateField,'~') into newValueList;
  END IF;



   IF (OLD.defineByToDoRule <> NEW.defineByToDoRule ) THEN
      select CONCAT(fieldNameList,'datacatalog.defineByToDoRule~') into fieldNameList;
      select CONCAT(oldValueList,OLD.defineByToDoRule,'~') into oldValueList;
      select CONCAT(newValueList,NEW.defineByToDoRule,'~') into newValueList;
  END IF;




   IF (OLD.visible <> NEW.visible ) THEN
      select CONCAT(fieldNameList,'datacatalog.visible~') into fieldNameList;
      select CONCAT(oldValueList,OLD.visible,'~') into oldValueList;
      select CONCAT(newValueList,NEW.visible,'~') into newValueList;
  END IF;



   IF (OLD.charge <> NEW.charge ) THEN
      select CONCAT(fieldNameList,'datacatalog.charge~') into fieldNameList;
      select CONCAT(oldValueList,OLD.charge,'~') into oldValueList;
      select CONCAT(newValueList,NEW.charge,'~') into newValueList;
  END IF;



   IF (OLD.usDomestic <> NEW.usDomestic ) THEN
      select CONCAT(fieldNameList,'datacatalog.usDomestic~') into fieldNameList;
      select CONCAT(oldValueList,OLD.usDomestic,'~') into oldValueList;
      select CONCAT(newValueList,NEW.usDomestic,'~') into newValueList;
  END IF;



   IF (OLD.referenceTable <> NEW.referenceTable ) THEN
      select CONCAT(fieldNameList,'datacatalog.referenceTable~') into fieldNameList;
      select CONCAT(oldValueList,OLD.referenceTable,'~') into oldValueList;
      select CONCAT(newValueList,NEW.referenceTable,'~') into newValueList;
  END IF;



 IF (OLD.parameterLinkingCode <> NEW.parameterLinkingCode ) THEN
      select CONCAT(fieldNameList,'datacatalog.parameterLinkingCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.parameterLinkingCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.parameterLinkingCode,'~') into newValueList;
  END IF;



  CALL add_tblHistory (OLD.id,"datacatalog", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;