<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="truckDetails.title"/></title>   
    <meta name="heading" content="<fmt:message key='truckDetails.heading'/>"/>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<style>
.wd-10 {width:70px;}
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->
 <script language="javascript" type="text/javascript">
 <c:set value="false" var="dueInspFlag"/>
<configByCorp:fieldVisibility componentId="component.truckingOps.OnBasisOfDotInspDue">
<c:set value="true" var="dueInspFlag"/>
 </configByCorp:fieldVisibility>
</script>
<script language="JavaScript">
	function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39)||(keyCode==110) || (keyCode==109) || ( keyCode==190); 
	}
	function onlyRateAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
	  
	}	
	
function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="0";
	        
	        return false;
	        }
	        var f = document.getElementById('truckForm'); 
		f.setAttribute("autocomplete", "off");
	    }
	 
	      return true;
	}
function onlyFloat(targetElement)
{   var i;
	var s = targetElement.value;
	var count = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if (((c < '0') || (c > '9')) && (c != '.') || (count>'1')) 
        {
        	alert("Enter valid Number");
	        document.getElementById(targetElement.id).value=0;
            document.getElementById(targetElement.id).select();
	        return false;
        }
    }
    return true;
}	
	
</script>
<script language="JavaScript">
function DOTMand()
{
	var DueDateMand = document.forms['truckForm'].elements['truck.docVehicle'].value;
	var showMand1=document.getElementById('dotInspDueId');
	var showMand2=document.getElementById('dotInspDueId1');
	if(DueDateMand=='Y')
		{
		showMand1.style.display = 'none';
		showMand2.style.display = 'block';	
		}
	else{
		showMand1.style.display = 'block';
		showMand2.style.display = 'none';
	}
	
	}
</script>

<script>
function checkdate(clickType){
	if ('${autoSavePrompt}' == 'No'){
			var noSaveAction = '<c:out value="${truck.id}"/>';
      		var id = document.forms['truckForm'].elements['truck.id'].value;	
      		
			if(document.forms['truckForm'].elements['gotoPageString'].value == 'gototab.turckList'){
				noSaveAction = 'truckList.html';
			}
			
			processAutoSave(document.forms['truckForm'], 'saveTruck!saveOnTabChange.html', noSaveAction);
	}
	else{
	if(!(clickType == 'save')){
	var id = document.forms['truckForm'].elements['truck.id'].value;
	if(document.forms['truckForm'].elements['gotoPageString'].value == 'gototab.turckList'){
				noSaveAction = 'truckList.html';
			}
			
			processAutoSave(document.forms['truckForm'], 'saveTruck!saveOnTabChange.html', noSaveAction);
	}	
}
}
function check(){
	<c:if test="${dueInspFlag eq true }">
	var temp = document.forms['truckForm'].elements['truck.docVehicle'].value;
	var dueDate = document.forms['truckForm'].elements['truck.dotInspDue'].value;
	if(temp=='Y' && dueDate=='')
		{
		alert('DOT Inspection Due is required field.');
		return false;
		}
	</c:if>
	if(document.forms['truckForm'].elements['truck.description'].value == ''){
		alert('Please fill  the description');
		return false;
	}
	if(document.forms['truckForm'].elements['truck.type'].value == ''){
		alert('Please select the type');
		return false;
	}
	if(document.forms['truckForm'].elements['truck.localNumber'].value == ''){
		alert('Please fill the Local Truck #');
		return false;
	}
	if(document.forms['truckForm'].elements['truck.truckStatus'].value == 'I' && document.forms['truckForm'].elements['truck.inactiveDate'].value=='')
		{
		    alert('Please Enter the Inactive Date.');
		    return false;
		}	
	//setFlagValue();
}
<%--
function setFlagValue(){
	document.forms['truckForm'].elements['hitFlag'].value = '1';
}

//Function for parent id of operator owner
--%>
function getTruckTypeDescList() {
	var truckType = document.forms['truckForm'].elements['truck.type'].value;
	var url="truckDescription.html?ajax=1&decorator=simple&popup=true&truckType=" + encodeURI(truckType);
     httpParentId.open("GET", url, true);
     httpParentId.onreadystatechange = handleHttpResponse555;
     httpParentId.send(null);
}
function handleHttpResponse555(){
             if (httpParentId.readyState >= 4){
                var results = httpParentId.responseText
                results = results.trim();
                res=results.replace("[" ,"");
                res=res.replace("]" ,"");
					document.forms['truckForm'].elements['truckTypeDesc'].value = res;
             }
        
}

function getDriverList(targetElement) {
	var warehouse = targetElement.value;
	var url="findDriverList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(warehouse);
     httpDriver.open("GET", url, true);
     httpDriver.onreadystatechange = handleHttpResponse55;
     httpDriver.send(null);
}
function handleHttpResponse55(){
             if (httpDriver.readyState >= 1){
                var results = httpDriver.responseText
                results = results.trim();
                var res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split(",");
                targetElement = document.forms['truckForm'].elements['truck.driver'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['truckForm'].elements['truck.driver'].options[i].text = '';
					document.forms['truckForm'].elements['truck.driver'].options[i].value = '';
					}else{
					document.forms['truckForm'].elements['truck.driver'].options[i].text = res[i];
					document.forms['truckForm'].elements['truck.driver'].options[i].value = res[i];
					
					if (document.getElementById("driver").options[i].value == '${truck.driver}'){
					   document.getElementById("driver").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("driver").value = '${truck.driver}';
             }
        
}

function getState(targetElement) {
	var country = targetElement.value;
 	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse5;
     httpState.send(null);
}
function handleHttpResponse5(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
                
                res = results.split("@");
                targetElement = document.forms['truckForm'].elements['truck.state'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['truckForm'].elements['truck.state'].options[i].text = '';
					document.forms['truckForm'].elements['truck.state'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['truckForm'].elements['truck.state'].options[i].text = stateVal[1];
					document.forms['truckForm'].elements['truck.state'].options[i].value = stateVal[0];
					
					if (document.getElementById("state").options[i].value == '${truck.state}'){
					   document.getElementById("state").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("state").value = '${truck.state}';
             }
        }  
      function autoPopulate_customerFile_originCountry(targetElement) {		
		var oriCountry = targetElement.value;
		var oriCountryCode = '';
		if(oriCountry == 'United States')
		{
			oriCountryCode = "USA";
		}
		if(oriCountry == 'India')
		{
			oriCountryCode = "IND";
		}
		if(oriCountry == 'Canada')
		{
			oriCountryCode = "CAN";
		}
		if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
			document.forms['truckForm'].elements['truck.state'].disabled = false;
		}else{
			document.forms['truckForm'].elements['truck.state'].disabled = true;
			document.forms['truckForm'].elements['truck.state'].value = '';
		}
}  
 String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
        
 var httpState = getHTTPObjectState()
 function getHTTPObjectState()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 

var httpDriver = getHTTPObjectDriver()
 function getHTTPObjectDriver()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 

var httpParentId = getHTTPObjectParentId()
 function getHTTPObjectParentId()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
        
function addOperatorOwner(){
	var partentId = document.forms['truckForm'].elements['truck.localNumber'].value;
	if(partentId!=''){
	window.open('editOwnerOperatorDetails.html?decorator=popup&popup=true&bucket=Truck&type=Tractor&parentId='+partentId,'sqlExtractInputForm','height=300,width=750, top=100,left=100, scrollbars=yes,resizable=yes').focus();
	}else{
	alert("Please Select the Owner/Pay To");
	}
} 

function truckDriverList(){
	var localNumber = document.forms['truckForm'].elements['truck.localNumber'].value;
	var ownerPayTo = document.forms['truckForm'].elements['truck.ownerPayTo'].value;
	var truckId = document.forms['truckForm'].elements['truck.id'].value;
	var truckDriverType = document.forms['truckForm'].elements['truckTypeDesc'].value;
	var truckDescription = document.forms['truckForm'].elements['truck.description'].value;
	
	var url="driverList.html?truckNumber=" + encodeURI(localNumber) + "&partnerCode="+ encodeURI(ownerPayTo)+ "&truckId="+ encodeURI(truckId)+ "&truckDriverType="+ encodeURI(truckDriverType)+ "&truckDescription="+ encodeURI(truckDescription);
	location.href = url;	
}    
function truckTrailersList(){
	var localNumber = document.forms['truckForm'].elements['truck.localNumber'].value;
	var ownerPayTo = document.forms['truckForm'].elements['truck.ownerPayTo'].value;
	var truckId = document.forms['truckForm'].elements['truck.id'].value;
	var truckDriverType = document.forms['truckForm'].elements['truckTypeDesc'].value;
	var truckDescription = document.forms['truckForm'].elements['truck.description'].value;
	
	var url="trailerList.html?truckNumber=" + encodeURI(localNumber) + "&truckId="+ encodeURI(truckId)+ "&partnerCode="+ encodeURI(ownerPayTo)+"&truckDriverType="+ encodeURI(truckDriverType)+ "&truckDescription="+ encodeURI(truckDescription);
	location.href = url;	
} 
var count=0;
function numbersonly(myfield, e)
{
	var key;
	var keychar;

	var val = myfield.value;
			if(val.indexOf(".")<0)
			{
			count=0;
			}
    
	if (window.event)
	   key = window.event.keyCode;
	else if (e)
	   key = e.which;
	else
	   return true;
		keychar = String.fromCharCode(key);
	if ((key==null) || (key==0) || (key==8) ||     (key==9) || (key==13) || (key==27) )
	   return true;
	else if ((("0123456789").indexOf(keychar) > -1))
	   return true;
	else if ((count<1)&&(keychar == ".")){		
	   count++;
	   return true;	   
	}
	else if ((count>0)&&(keychar == "."))
	{
	   return false;
	}	
	else
	   return false;
}
function isFloat(targetElement)
{   var i;
var s = targetElement.value;
for (i = 0; i < s.length; i++)
{   
    var c = s.charAt(i);
    if (((c < "0") || (c > "9"))) {
    if (c == ".") {
    
    }else{
    alert("Only numbers are allowed here");
    //alert(targetElement.id);
    document.getElementById(targetElement.id).value='';
    document.getElementById(targetElement.id).select();
    return false;
    }
}
}
return true;
}
function dotNotAllowed(target,fieldName){
	var dotValue=target.value;
	if(dotValue=='.' && dotValue.length==1 ){
		alert("Please enter valid Capacity.");
		document.forms['truckForm'].elements[fieldName].value="";
		document.forms['truckForm'].elements[fieldName].value.focus();
		return false;
	    }
     }
function enableStateList(){ 
	var oriCon=document.forms['truckForm'].elements['truck.country'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['truckForm'].elements['truck.state'].disabled = false;
	  }else{
		  document.forms['truckForm'].elements['truck.state'].disabled = true;
	  }	        
}

</script>

</head> 
<s:hidden name="fileNameFor"  id= "fileNameFor" value="Truck"/>
<s:hidden name="fileID" id ="fileID" value="%{truck.id}" />
<c:set var="fileID" value="%{truck.id}"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form cssClass="form_magn" id="truckForm" name="truckForm" action="saveTruck" method="post" validate="true">   
<s:hidden name="truckNo" value="${truck.localNumber}" />
<s:hidden name="truck.id" />
<s:hidden name="truck.corpID" />
<s:hidden name="parentId" value="%{truck.localNumber}"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="truckTypeDesc" />   
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.turckList' }">
	<c:redirect url="/truckList.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>

<div id="newmnav" class="nav_tabs"><!-- sandeep -->
	<ul>
		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Truck Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<li><a href="truckList.html"><span>Truck List</span></a></li>
		<c:if test="${not empty truck.id}">
		<li><a href="#" onclick="truckDriverList();"><span>Driver</span></a></li>
		<li><a href="#" onclick="truckTrailersList();"><span>Trailers</span></a></li>
		</c:if>
	</ul>
</div>
<div style="width:780px">
	<div class="spn">&nbsp;</div>
	<div style="padding-bottom:0px;"></div>
</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
<div id="Layer1" style="width:100%;">
<table cellspacing="0" cellpadding="0" border="0" style="width:100%;">
	<tbody>
		<tr>
			<td>
				<table cellspacing="0" cellpadding="2" border="0" class="detailTabLabel" style="width:100%;">
					<tbody>
					
						<tr>
						<td colspan="13">
						
						<table cellspacing="0" cellpadding="2" border="0" class="detailTabLabel">			
						
						<tr>
							<td align="left" class="listwhite" colspan="13">
							<table cellspacing="0" cellpadding="2" border="0" class="detailTabLabel" style="">
							
							<tr>
							<td align="right" class="listwhitetext" width="21"></td>
							<td align="right" width="78px" class="listwhitetext">Local&nbsp;Truck&nbsp;#<font color="red" size="2">*</font></td>
							<td colspan="3"><s:textfield cssClass="input-text" cssStyle="width:197px"  name='truck.localNumber' size="20" maxlength="20" tabindex="1"/></td>
							<td align="right" class="listwhitetext" width="10"></td>
							<td width="6%" align="right" class="listwhitetext"><fmt:message key='truck.description'/><font color="red" size="2">*</font></td>
							<td  width="20%"><s:textfield cssClass="input-text" name='truck.description' cssStyle="width:234px" maxlength="28" tabindex="2"/></td>
							<td align="right" class="listwhitetext" width=""></td>
						
							<td align="right" class="listwhitetext"><fmt:message key='truck.type'/><font color="red" size="2">*</font></td>
							<td colspan="1"><s:select cssClass="list-menu" name="truck.type" list="%{type}" headerKey="" headerValue="" cssStyle="width:224px" onchange="getTruckTypeDescList();changeStatus();" tabindex="3" /></td>
							
						</tr>
						
						<tr>
						<td align="right" class="listwhitetext" width="18"></td>
						<td align="right" class="listwhitetext">Status</td>
						<td><select name="truck.truckStatus" style="width:88px;" class="list-menu" tabindex="4"><c:if test="${truck.truckStatus=='A'}"><option value="A">Active</option><option value="I">Inacitve</option>
						</c:if><c:if test="${truck.truckStatus=='I'}"><option value="I">Inacitve</option><option value="A">Acitve</option></c:if>
						</select></td>
						<td align="right" class="listwhitetext" width="10"></td>						
							<td align="right" class="listwhitetext" width="18"></td>
							
							
							<td align="left" class="listwhitetext" colspan="4">
							<table border="0" class="detailTabLabel" cellspacing="1" cellpadding="2">	
							<tr>
							<td align="right" class="listwhitetext" width="2"></td>
							<td align="right" class="listwhitetext">Inactive Date</td>
							<c:if test="${not empty truck.inactiveDate}">
								<s:text id="inactiveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="truck.inactiveDate"/></s:text>
								<td><s:textfield cssClass="input-text" id="inactiveDate" name="truck.inactiveDate" value="%{inactiveDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td width="38"><img id="inactiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
								<c:if test="${empty truck.inactiveDate}">
								<td><s:textfield cssClass="input-text" id="inactiveDate" name="truck.inactiveDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td width="41"><img id="inactiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							</tr>
							</table>
							</td>
							</tr>
						
							<tr>
							<td align="right" class="listwhitetext" width="23"></td>							
							<td align="right" class="listwhitetext"><fmt:message key='truck.make'/></td>
							<td width="10%"><s:textfield cssClass="input-text" cssStyle="width:84px" name='truck.make' size="15" maxlength="15" tabindex="5"/></td>
							<td align="right" width="4%"class="listwhitetext"><fmt:message key='truck.year'/></td>
							<td><s:textfield cssClass="input-text" name='truck.year' cssStyle="width:68px" maxlength="4" tabindex="6"/></td>
							<td align="right" class="listwhitetext" width="10"></td>
							<td align="right" width="4%"class="listwhitetext"><fmt:message key='vehicle.model'/></td>
							<td colspan="1"><s:textfield cssClass="input-text" name='truck.model' cssStyle="width:235px" maxlength="20" tabindex="7"/></td>
							<td align="right" class="listwhitetext" width=""></td>
							<td align="right" width="48px" class="listwhitetext"><fmt:message key='truck.vin'/></td>
							<td colspan="2"><s:textfield cssClass="input-text" name='truck.vin' cssStyle="width:221px" maxlength="25" tabindex="8"/></td>
						
							</tr>		
							</table>
							</td>
						</tr>	
						
						
						<tr>
						<td colspan="18">
						<table style="padding: 0px; margin: 0px 0px 0px -2px;" border="0">
						<tr>
							<td align="right" class="listwhitetext" width="99"><fmt:message key='truck.dotInsp'/></td>
							<c:if test="${not empty truck.dotInsp}">
								<s:text id="dotInspFormattedValue" name="${FormDateValue}"><s:param name="value" value="truck.dotInsp"/></s:text>
								<td width=""><s:textfield cssClass="input-text" id="dotInsp" name="truck.dotInsp" value="%{dotInspFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td width="20"><img id="dotInsp_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
								<c:if test="${empty truck.dotInsp}">
								<td width=""><s:textfield cssClass="input-text" id="dotInsp" name="truck.dotInsp" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td width="38"><img id="dotInsp_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" width="10"></td>
							 <c:if test="${dueInspFlag eq false }">
							<td width="100" align="right" class="listwhitetext"><fmt:message key='truck.dotInspDue'/></td>
							</c:if>
							 <c:if test="${dueInspFlag eq true }">
							<td width="100" align="right" id="dotInspDueId" class="listwhitetext"><fmt:message key='truck.dotInspDue'/></td>
							<td width="100" align="right" id="dotInspDueId1" class="listwhitetext"><fmt:message key='truck.dotInspDue'/><font color="red" size="2">*</font></td>
							</c:if>
							<c:if test="${not empty truck.dotInspDue}">
								<s:text id="dotInspDueFormattedValue" name="${FormDateValue}"><s:param name="value" value="truck.dotInspDue"/></s:text>
								<td width=""><s:textfield cssClass="input-text" id="dotInspDue" name="truck.dotInspDue" value="%{dotInspDueFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td width="38"><img id="dotInspDue_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
								<c:if test="${empty truck.dotInspDue}">
								<td width=""><s:textfield cssClass="input-text" id="dotInspDue" name="truck.dotInspDue" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td width="31"><img id="dotInspDue_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td align="right" class="listwhitetext" width="118"><fmt:message key='truck.lastStInsp'/></td>
							<c:if test="${not empty truck.lastStInsp}">
								<s:text id="lastStInspFormattedValue" name="${FormDateValue}"><s:param name="value" value="truck.lastStInsp"/></s:text>
								<td width="50"><s:textfield cssClass="input-text" id="lastStInsp" name="truck.lastStInsp" value="%{lastStInspFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="lastStInsp_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
								<c:if test="${empty truck.lastStInsp}">
								<td width="50"><s:textfield cssClass="input-text" id="lastStInsp" name="truck.lastStInsp" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="lastStInsp_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
							<td align="right" class="listwhitetext">TRL Type</td>
						    <td colspan="2"><s:select cssClass="list-menu" id="trailertype" name="truck.trailertype" list="%{trailertype}" headerKey="" headerValue=""  cssStyle="width:140px" onchange="changeStatus();" tabindex="9"/></td>
						
							
							
						</tr>
						
						<tr>
							<td align="right" class="listwhitetext"><fmt:message key='truck.lastVlInsp'/></td>
							<c:if test="${not empty truck.lastVlInsp}">
								<s:text id="lastVlInspFormattedValue" name="${FormDateValue}"><s:param name="value" value="truck.lastVlInsp"/></s:text>
								<td><s:textfield cssClass="input-text" id="lastVlInsp" name="truck.lastVlInsp" value="%{lastVlInspFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="lastVlInsp_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
								<c:if test="${empty truck.lastVlInsp}">
								<td><s:textfield cssClass="input-text" id="lastVlInsp" name="truck.lastVlInsp" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="lastVlInsp_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" width="10"></td>
							<td align="right" class="listwhitetext"><fmt:message key='truck.vlInspDue'/></td>
							<c:if test="${not empty truck.vlInspDue}">
								<s:text id="vlInspDueFormattedValue" name="${FormDateValue}"><s:param name="value" value="truck.vlInspDue"/></s:text>
								<td><s:textfield cssClass="input-text" id="vlInspDue" name="truck.vlInspDue" value="%{vlInspDueFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="vlInspDue_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
								<c:if test="${empty truck.vlInspDue}">
								<td><s:textfield cssClass="input-text" id="vlInspDue" name="truck.vlInspDue" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="vlInspDue_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
							<td align="right" class="listwhitetext">In&nbsp;Service</td>
							<c:if test="${not empty truck.inService}">
								<s:text id="inServiceFormattedValue" name="${FormDateValue}"><s:param name="value" value="truck.inService"/></s:text>
								<td><s:textfield cssClass="input-text" id="inService" name="truck.inService" value="%{inServiceFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="inService_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
								<c:if test="${empty truck.inService}">
								<td><s:textfield cssClass="input-text" id="inService" name="truck.inService" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="inService_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
							
							<td align="right" class="listwhitetext" width="120px"><fmt:message key='truck.pmDate'/></td>
							<td colspan="2">
							<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="1">
							<tr>
							<c:if test="${not empty truck.pmDate}">
								<s:text id="pmDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="truck.pmDate"/></s:text>
								<td width="50px"><s:textfield cssClass="input-text" id="pmDate" name="truck.pmDate" value="%{pmDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
								<td align="left"><img id="pmDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
								<c:if test="${empty truck.pmDate}">
								<td width="50px"><s:textfield cssClass="input-text" id="pmDate" name="truck.pmDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
								<td align="left"><img id="pmDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							</tr>
							</table>
							
							</td>
						</tr>
						</table>
						</td>
						
								
						</tr>						
						<tr>
						</tr>
						</table>						
						</td>
						</tr>
						<tr>
							<td colspan="12">
								<table style="margin:0px;padding:0px;" border="0">
									<tr>
										<td align="right" class="listwhitetext" width="26"></td>
										<td align="right" class="listwhitetext">Owner/Pay&nbsp;To</td>

										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="truck.ownerPayTo" required="true"  readonly="true" cssStyle="width:65px" size="8" maxlength="8" tabindex="10" /></td>

						                <td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('vendorListInTruck.html?partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=truck.ownerPayToName&fld_code=truck.ownerPayTo');" src="<c:url value='/images/open-popup.gif'/>" /></td>
										
										<td align="left"  class="listwhitetext"><s:textfield cssClass="input-textUpper" key="truck.ownerPayToName"  required="true" readonly="true" cssStyle="width:250px" size="46" tabindex="11"/></td>
										
										<td align="right" class="listwhitetext" width="96">Accounting&nbsp;Type</td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="truck.accountingType" required="true" cssStyle="width:206px" size="20" maxlength="20" tabindex="12"/></td>
						              	
									<tr>	
								</table>
							</td>
						</tr>
						
						<tr><td align="center" colspan="13" class="vertlinedata"></td></tr>
						
						<tr>
						<td colspan="13">
						<table style="margin-left:20px;padding:0px;">
						<tr>
				
						<td align="right" class="listwhitetext"><fmt:message key='truck.country'/></td>						
						<td colspan="3" width="32%"><s:select cssClass="list-menu" name="truck.country" list="%{ocountry}" headerKey="" headerValue=""  cssStyle="width:245px" onchange="getState(this);autoPopulate_customerFile_originCountry(this);enableStateList();" tabindex="13" /></td>
						<td align="right" class="listwhitetext" width=""></td>
						<td align="right" class="listwhitetext"><fmt:message key='truck.state'/></td>
						<td colspan="3"><s:select cssClass="list-menu" id="state" name="truck.state" list="%{states}" headerKey="" headerValue=""  cssStyle="width:105px" onchange="changeStatus();" tabindex="14" /></td>
						<td align="right" class="listwhitetext">Must&nbsp;Dispatch&nbsp;With</td>
						<td colspan="3"><s:textfield cssClass="input-text" name="truck.mustdispatch" cssStyle="width:81px" size="11" maxlength="10" tabindex="15"/></td>
							
						</tr>
						<tr>							
							<td align="right" class="listwhitetext">Fleet</td>
							<td align="left" colspan="3">
							<table border="0" cellpadding="1" cellspacing="0" class="detailTabLabel">
							<tr>
								
							<td>
							<s:select name="truck.fleet" cssClass="list-menu" list="%{fleet}" cssStyle="width:105px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="16"/>
							<!--<s:textfield cssClass="input-text" name='truck.fleet' size="12" maxlength="20" />--></td>
							
							<td align="right" class="listwhitetext" width="65">Acquired&nbsp;</td>
							<c:if test="${not empty truck.acquired}">
								<s:text id="acquiredFormattedValue" name="${FormDateValue}"><s:param name="value" value="truck.acquired"/></s:text>
								<td><s:textfield cssClass="input-text" id="acquired" name="truck.acquired" value="%{acquiredFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="acquired_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
								<c:if test="${empty truck.acquired}">
								<td><s:textfield cssClass="input-text" id="acquired" name="truck.acquired" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="acquired_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
							</tr>
							</table>
							<td>
							<td align="right" class="listwhitetext">Original&nbsp;Cost</td>
							<td colspan="3"><s:textfield cssClass="input-text" name='truck.originalCost' cssStyle="width:101px;" size="15" maxlength="8" onkeydown="return onlyRateAllowed(event)" tabindex="17"/></td>
							<td align="right" class="listwhitetext">Truck&nbsp;Pay</td>
							<td colspan="2"><s:textfield cssClass="input-text" name='truck.payToTruck' cssStyle="width:81px;" size="11" maxlength="8" onkeydown="return onlyRateAllowed(event)" tabindex="18"/></td>
						</tr>
						<tr>
						
							<td align="right" class="listwhitetext">License&nbsp;#&nbsp;/Tag&nbsp;#</td>
							<td colspan="3"><s:textfield cssClass="input-text" name='truck.tagNumber' cssStyle="width:240px" maxlength="15" tabindex="19"/></td>
							<td align="right" class="listwhitetext" width=""></td>
							
							
							<td align="right" colspan="1" class="listwhitetext">Wheel&nbsp;Base</td>
							<td colspan="3"><s:textfield cssClass="input-text" name='truck.wheelBase' cssStyle="width:101px;" size="15" maxlength="20" tabindex="20"/></td>
							<td align="right" colspan="1" class="listwhitetext">Agency#</td>
							<td colspan="3"><s:select cssClass="list-menu" name="truck.agency" list="%{driverAgencyList}" cssStyle="width:85px;height:20px" headerKey="" headerValue="" tabindex="21" /></td>
							<td align="right" colspan="1" class="listwhitetext">Agency&nbsp;Truck&nbsp;Id</td>
							<td ><s:textfield cssClass="input-text" name='truck.agencyTruckId' cssStyle="width:60px;" size="11" maxlength="20" tabindex="22"/></td>
						</tr>
						<tr>
							
							<td align="right" class="listwhitetext">VL Truck #</td>
							<td colspan="3"><s:textfield cssClass="input-text" name='truck.vlTruckNumber' cssStyle="width:240px" size="15" maxlength="10" tabindex="23"/></td>
							<td align="right" class="listwhitetext"></td>
							<td align="right" class="listwhitetext">Condition</td>
						    <td colspan="1"><s:select cssClass="list-menu" id="condition" name="truck.condition" list="%{condition}" headerKey="" headerValue=""  cssStyle="width:105px" onchange="changeStatus();" tabindex="24" /></td>
						
							<td colspan="2"></td>
							
							<td align="right" class="listwhitetext"><fmt:message key='truck.docVehicle'/></td>
							<td><s:select cssClass="list-menu" name="truck.docVehicle" list="%{yesno}" cssStyle="width:85px" onchange="DOTMand();changeStatus();" tabindex="25" /></td>
						</tr>
						
						<tr>
							
							<td align="right" class="listwhitetext"><fmt:message key='truck.warehouse'/></td>
							<td colspan="3"><s:select cssClass="list-menu" name="truck.warehouse" list="%{house}" headerKey="" headerValue="" cssStyle="width:244px" tabindex="26" /></td>
							
							<td align="right" class="listwhitetext" width=""></td>
							<c:set var="ischecked" value="false"/>
							<c:if test="${truck.isGaraged}">
								<c:set var="ischecked" value="true"/>
							</c:if>		
							<td align="right" class="listwhitetext"><fmt:message key='truck.isGaraged'/></td>	
							<td align="left" class="listwhitetext" colspan="3"><s:checkbox key="truck.isGaraged" cssStyle="margin:0px;" value="${ischecked}" fieldValue="true" tabindex="27"/></td>
							
							
							<td align="right" class="listwhitetext"><fmt:message key='truck.cdlVehicle'/></td>
							<td><s:select cssClass="list-menu" name="truck.cdlVehicle" list="%{yesno}" cssStyle="width:85px" onchange="changeStatus();" tabindex="28" /></td>
						</tr>
						<tr>
							
							<td align="right" class="listwhitetext"><fmt:message key='truck.miles'/></td>
							<td align="left" colspan="3">
							<table border="0" cellpadding="0" cellspacing="0" class="detailTabLabel">
							<tr>
								
							<td><s:textfield cssClass="input-text" name='truck.miles' cssStyle="width:100px" size="5" maxlength="7" tabindex="29" onchange="isNumeric(this);"/></td>
							
							<td align="right" class="listwhitetext" width="70"><fmt:message key='truck.milesdate'/>&nbsp;</td>
							<c:if test="${not empty truck.milesdate}">
								<s:text id="milesdateFormattedValue" name="${FormDateValue}"><s:param name="value" value="truck.milesdate"/></s:text>
								<td width="30"><s:textfield cssClass="input-text" id="milesdate" name="truck.milesdate" value="%{milesdateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td align="left" width="27">&nbsp;<img id="milesdate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
								<c:if test="${empty truck.milesdate}">
								<td width="30"><s:textfield cssClass="input-text" id="milesdate" name="truck.milesdate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td align="left" width="27">&nbsp;<img id="milesdate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
							</tr>
							</table>
							<td>
							
							<td align="right" class="listwhitetext">Assigned&nbsp;to&nbsp;Staff</td>
							<td colspan="5"><s:textfield cssClass="input-text" name='truck.driver' id="driver" maxlength="80" cssStyle="width:290px;" tabindex="30"/></td>
						</tr>
						
						<tr>
						
							<td align="right" class="listwhitetext"><fmt:message key='truck.gl'/></td>
							<td colspan="3"><s:select cssClass="list-menu" name="truck.gl" list="%{glCode}" headerKey="" headerValue="" cssStyle="width:244px" onchange="changeStatus();" tabindex="31" /></td>
							<td align="right" class="listwhitetext" width=""></td>
							
							<td align="right" class="listwhitetext"><fmt:message key='truck.glType'/></td>
							<td colspan="3"><s:select cssClass="list-menu" name="truck.glType" list="%{glType}" headerKey="" headerValue="" cssStyle="width:105px" onchange="changeStatus();" tabindex="32" /></td>
							
							<td align="right" class="listwhitetext"><fmt:message key='truck.truckOrder'/></td>
							<td colspan="2"><s:textfield cssClass="input-text" name='truck.truckOrder' cssStyle="width:81px;" size="11" maxlength="7" onchange="isNumeric(this);" tabindex="33"/></td>
						</tr>
						</table>
						</td>
						</tr>
						
						<tr><td align="center" colspan="13" class="vertlinedata"></td></tr>
						
						<tr>
							
							<td colspan="13">
								<table cellspacing="0" cellpadding="2" border="0" class="detailTabLabel" width="100%">
									<tr>
										<td colspan="15">
										<table cellspacing="0" cellpadding="2" border="0" class="detailTabLabel">
										<tr>
										<td align="right" class="listwhitetext" style="width:22px;"></td>										
										<td align="right" class="listwhitetext" width="65">Cost&nbsp;per&nbsp;use&nbsp;${currencySign}</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.cost' size="10" maxlength="7" onchange="onlyFloat(this);" tabindex="34"/></td>
										<td align="right" class="listwhitetext" width="30px"></td>										
										<td align="right" class="listwhitetext" width="54px"><fmt:message key='truck.charge'/>${currencySign}</td>
									   	<td><s:textfield cssClass="input-text wd-10" name='truck.charge' size="10" maxlength="7" onchange="onlyFloat(this);" tabindex="35"/></td>
										<td align="right" class="listwhitetext" width="30px"></td>
										<td align="right" class="listwhitetext" style="width:94px"><fmt:message key='truck.otCharge'/>${currencySign}</td>					
										<td><s:textfield cssClass="input-text wd-10" name='truck.otCharge' size="10" maxlength="7" onchange="onlyFloat(this);" tabindex="36"/></td>
										<td align="right" class="listwhitetext" width="20px" ></td>
										<td align="right" class="listwhitetext" style="width: 104px;" ><fmt:message key='truck.dtCharge'/>${currencySign}</td>									
										<td><s:textfield cssClass="input-text wd-10" name='truck.dtCharge' size="10" maxlength="7" onchange="onlyFloat(this);" tabindex="37"/></td>
									</tr>
									</table>
									</td>
									</tr>
									<tr><td align="center" colspan="16" height="2px"></td></tr>	
									<tr><td align="center" colspan="16" class="vertlinedata"></td></tr>	
							<tr>
							<td colspan="15">
							<table cellspacing="0" cellpadding="2" border="0" class="detailTabLabel">							
							<tr>
							<td align="right" class="listwhitetext" width="27px"></td>
							<td align="right" class="listwhitetext">Axles</td>
							<td><s:textfield cssClass="input-text wd-10" name='truck.axle' size="10" maxlength="2" onchange="isNumeric(this);" tabindex="38"/></td>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext">KPin/Axle&nbsp;1</td>
							<td><s:textfield cssClass="input-text wd-10" name='truck.kpinToAxle1' size="10" maxlength="10" tabindex="39"/></td>
							<td align="right" class="listwhitetext" width="48px"></td>
							<td align="right" class="listwhitetext">Axle&nbsp;1/Axle&nbsp;2</td>
							<td><s:textfield cssClass="input-text wd-10" name='truck.axle1ToAxle2' size="10" maxlength="10" tabindex="40"/></td>
							<td align="right" class="listwhitetext" width="51px"></td>
							<td align="right" class="listwhitetext">Axle&nbsp;2/Axle&nbsp;3</td>
							<td><s:textfield cssClass="input-text wd-10" name='truck.axle2ToAxle3' size="10" maxlength="10" tabindex="41"/></td>
							<td align="right" class="listwhitetext" style="padding-left:15px;">Axle&nbsp;3/Axle&nbsp;4</td>
							<td><s:textfield cssClass="input-text wd-10" name='truck.axle3ToAxle4' size="10" maxlength="10" tabindex="42"/></td>
                             	
							</tr>	
						
								<tr>
										<td align="right" class="listwhitetext"></td>
										<td align="right" class="listwhitetext">Axle&nbsp;4/Axle&nbsp;5</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.axle4ToAxle5' size="10" maxlength="10" tabindex="43"/></td>
										<td align="right" class="listwhitetext" width="10px"></td>
										<td align="right" class="listwhitetext">Axle&nbsp;5/Axle&nbsp;6</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.axle5ToAxle6' size="10" maxlength="10" tabindex="44"/></td>
										<td align="right" class="listwhitetext" width="10px"></td>
										<td align="right" class="listwhitetext">Axle&nbsp;6/Axle&nbsp;7</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.axle6ToAxle7' size="10" maxlength="10" tabindex="45"/></td>
										<td align="right" class="listwhitetext" width="10px"></td>
										<td align="right" class="listwhitetext">Axle&nbsp;7/Axle&nbsp;8</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.axle7ToAxle8' size="10" maxlength="10" tabindex="46"/></td>
                                        <td align="right" class="listwhitetext">Trl&nbsp;Kind</td>							
										<td><s:textfield cssClass="input-text wd-10" name='truck.trailerKind' size="10" maxlength="20" tabindex="47"/></td>
									</tr>	
									
									<tr>
										<td align="right" class="listwhitetext"></td>
										<td align="right" class="listwhitetext">Axle&nbsp;8/Axle&nbsp;9</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.axle8ToAxle9' size="10" maxlength="10" tabindex="48"/></td>
										<td align="right" class="listwhitetext" width="10px"></td>
										<td align="right" class="listwhitetext">Axle&nbsp;9/Axle&nbsp;10</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.axle9ToAxle10' size="10" maxlength="10" tabindex="49"/></td>
										<td align="right" class="listwhitetext" width="10px"></td>
										<td align="right" class="listwhitetext">Axle&nbsp;10/Axle&nbsp;11</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.axle10ToAxle11' size="10" maxlength="10" tabindex="50"/></td>
										<td align="right" class="listwhitetext" width="10px"></td>
										<td align="right" class="listwhitetext">Last&nbsp;Axle/Rear</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.lastAxleRear' size="10" maxlength="10" tabindex="51"/></td>
                                        
										<td align="right" class="listwhitetext">GP&nbsp;Class</td>
										<td ><s:textfield cssClass="input-text wd-10" name='truck.gpClass' size="10" maxlength="15" tabindex="52"/></td>
									</tr>	
									<tr>
										<td align="right" class="listwhitetext"></td>
										<td align="right" class="listwhitetext">Gross</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.gross' size="10" maxlength="7" onkeydown="return onlyRateAllowed(event)" tabindex="53"/></td>
										<td align="right" class="listwhitetext" width="10px"></td>
										<td align="right" class="listwhitetext">Height</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.height' size="10" maxlength="7" onkeydown="return onlyRateAllowed(event)" tabindex="54"/></td>
										<td align="right" class="listwhitetext" width="10px"></td>
										<td align="right" class="listwhitetext">Weight</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.weight' size="10" maxlength="7" onkeydown="return onlyRateAllowed(event)" tabindex="55"/></td>
										<td align="right" class="listwhitetext" width="10px"></td>
										<td align="right" class="listwhitetext"><fmt:message key='truck.pound'/></td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.pound' size="10" maxlength="7" onchange="onlyFloat(this);" tabindex="56"/></td>
                                       
										<td align="right" class="listwhitetext"></td>
										<td></td>
									</tr>	
									<tr>
									<td align="right" class="listwhitetext"></td>
						<td align="right" class="listwhitetext">Width</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.width' size="10" maxlength="7" onkeydown="return onlyRateAllowed(event)" tabindex="57"/></td>
										<td align="right" class="listwhitetext" width="10px"></td>
										<td align="right" class="listwhitetext">Length</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.length' size="10" maxlength="7" onkeydown="return onlyRateAllowed(event)" tabindex="58"/></td>
						
						<td align="right" class="listwhitetext" width="10px"></td>
										<td align="right" class="listwhitetext">Capacity</td>
										<td><s:textfield cssClass="input-text wd-10" name='truck.capacity' size="10" maxlength="7" tabindex="59" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event)" onblur="return isFloat(this)" onchange="return dotNotAllowed(this,'truck.capacity')"/></td>
						</tr>
						</table>
						</td>
						</tr>
								</table>
							</td>
						</tr>
						</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>

<c:if test="${not empty truck.id}">						
<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <tr>
 		  <td>
 		  <div id="newmnav" style="!margin-bottom:3px;">
	<ul>
		<li id="newmnav1" style="background:#FFF "><a class="current" style="margin-bottom:-3px;"><span>Expiry&nbsp;List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</ul>
</div>
<div class="spn">&nbsp;</div>
  </td>
 </tr> 		  
<tr><td align="left" colspan="7">
<div style=" width:100%; overflow-x:auto; overflow-y:auto;">
	<s:set name="ownerOperatorList" value="ownerOperatorList" scope="request"/>
	<display:table name="ownerOperatorList" class="table" requestURI="" id="ownerOperatorList"  defaultsort="2" pagesize="10" style="width:100%">
    <display:column title="Expiry&nbsp;For">
    <a href="javascript:openWindow('editOwnerOperatorDetails.html?id=${ownerOperatorList.id}&parentId=${parentId}&bucket=Truck&type=Tractor&decorator=popup&popup=true',750,300)">
    <c:out value="${ownerOperatorList.fieldName}"></c:out></a>
    </display:column>
    <display:column property="expiryDate"  sortable="true" title="Expiry&nbsp;Date"  format="{0,date,dd-MMM-yyyy}"/>
    <display:column sortProperty="required" sortable="true" title="Required" style="width: 16%; padding-left: 15px" media="html">
    <input type="checkbox" disabled="disabled" <c:if test="${ownerOperatorList.required}">checked="checked"</c:if>/>
    </display:column>
	</display:table>
	<input type="button" class="cssbutton1" onclick="addOperatorOwner();" value="Add&nbsp;Expirations" style="width:120px; height:25px" tabindex="60"/>
</div>
</td></tr>
<tr><td align="left" colspan="7" height="15"></td></tr>
</tbody>
</table>
</c:if>			
</div>	
  </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<table>
	<tbody>
		<tr>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='truck.createdOn'/></b></td>
			<td valign="top"></td>
			<td style="width:120px">
				<fmt:formatDate var="agentBaseCreatedOnFormattedValue" value="${truck.createdOn}" pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="truck.createdOn" value="${agentBaseCreatedOnFormattedValue}"/>
				<fmt:formatDate value="${truck.createdOn}" pattern="${displayDateTimeFormat}"/>
			</td>		
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='truck.createdBy' /></b></td>
			<c:if test="${not empty truck.id}">
				<s:hidden name="truck.createdBy"/>
				<td style="width:85px"><s:label name="createdBy" value="%{truck.createdBy}"/></td>
			</c:if>
			<c:if test="${empty truck.id}">
				<s:hidden name="truck.createdBy" value="${pageContext.request.remoteUser}"/>
				<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='truck.updatedOn'/></b></td>
			<fmt:formatDate var="agentBaseupdatedOnFormattedValue" value="${truck.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
			<s:hidden name="truck.updatedOn" value="${agentBaseupdatedOnFormattedValue}"/>
			<td style="width:120px"><fmt:formatDate value="${truck.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='truck.updatedBy' /></b></td>
			<c:if test="${not empty truck.id}">
				<s:hidden name="truck.updatedBy"/>
				<td style="width:85px"><s:label name="updatedBy" value="%{truck.updatedBy}"/></td>
			</c:if>
			<c:if test="${empty truck.id}">
				<s:hidden name="truck.updatedBy" value="${pageContext.request.remoteUser}"/>
				<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
		</tr>
	</tbody>
</table>               
      <s:submit cssClass="cssbutton1" key="button.save" cssStyle="width:55px; height:26px" onclick="return check();"/>   
      <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:26px" key="Reset" />
      <input type="button" class="cssbutton1" style="width:55px; height:26px" tabindex="61" onclick="location.href='<c:url value="/truckList.html"/>'"  value="<fmt:message key="button.cancel"/>"/>   
      <s:hidden name="hitFlag" />
	  <c:if test="${hitFlag == 1}" >
      <c:redirect url="/editTruck.html?id=${truck.id}"  />
      </c:if>  
  
<s:hidden name="secondDescription" />    
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="seventhDescription" />
</s:form>
<script type="text/javascript">
getTruckTypeDescList();
   try{
    autoPopulate_customerFile_originCountry(document.forms['truckForm'].elements['truck.country']);
 	}
 	catch(e){}
 	try{
 	getState(document.forms['truckForm'].elements['truck.country']);
  	}
  	catch(e){} 
  	try{
  		var enbState = '${enbState}';
  		 var oriCon=document.forms['truckForm'].elements['truck.country'].value;
  		 if(oriCon=="")	 {
  				document.getElementById('state').disabled = true;
  				document.getElementById('state').value ="";
  		 }else{
  		  		if(enbState.indexOf(oriCon)> -1){
  					document.getElementById('state').disabled = false; 
  					document.getElementById('state').value='${truck.country}';
  				}else{
  					document.getElementById('state').disabled = true;
  					document.getElementById('state').value ="";
  				} 
  			}
  		 }catch(e){}  	 
   document.forms['truckForm'].elements['truck.localNumber'].focus();
</script>

<script type="text/javascript">
	setOnSelectBasedMethods(["calcDays(),calculateDeliveryDate()"]);
	setCalendarFunctionality();
	DOTMand();
</script>