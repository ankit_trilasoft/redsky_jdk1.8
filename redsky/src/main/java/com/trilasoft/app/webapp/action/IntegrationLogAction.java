package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.dao.hibernate.IntegrationLogDaoHibernate.MemoUploadDTO;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.IntegrationLog;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.IntegrationLogManager;
import com.trilasoft.app.service.RefMasterManager;

public class IntegrationLogAction extends BaseAction {
    private IntegrationLogManager integrationLogManager;
    private List integrationLogs;
    private List memoUploadLogs;
    private String sessionCorpID;
    private List ls;
	private IntegrationLog integrationLog;
	private MemoUploadDTO memoUplod;
	private ServiceOrder serviceOrder;
	private RefMasterManager refMasterManager;
	private String shipNum;
	private String intType;
	private  Map<String, String> integrationType;
	private Company company;
	private CompanyManager companyManager;
	private Boolean enableMoveForYou;
	private Boolean enableMSS;
	private Boolean vanlineEnabled;
	private Boolean voxmeIntegration;
	private Boolean enablePricePoint;
	public IntegrationLogAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
    
    
    public List getIntegrationLogs() {
        return integrationLogs;
    }
    
    @SkipValidation
    public void getCompanyList()
 {
	 company=companyManager.findByCorpID(sessionCorpID).get(0);
	 	enableMoveForYou = company.getEnableMoveForYou();
		enableMSS=company.getEnableMSS();
		if(enableMSS==null)
		{
			enableMSS = false;
		}
		vanlineEnabled=company.getVanlineEnabled();
		voxmeIntegration = company.getVoxmeIntegration();
		enablePricePoint = company.getEnablePricePoint();
 }

    public String list() {
        
    	String key = "Please enter your search criteria below";
    	getCompanyList();
		saveMessage(getText(key));
		integrationType = refMasterManager.findByParameter(sessionCorpID, "INTEGRATIONTYPE");
        return SUCCESS;
    }
    public String memoList() {
        
    	//String key = "Please enter your search criteria below";
		//saveMessage(getText(key));
		memoUploadLogs = integrationLogManager.findMemoUpload("", "", "", "", sessionCorpID);
		//integrationLogs = integrationLogManager.getAllWithServiceOrderId(sessionCorpID);
        return SUCCESS;
    }
    @SkipValidation
	public String edit() {
    	serviceOrder = (ServiceOrder)integrationLogManager.findServiceOrder(shipNum, sessionCorpID).get(0);
		return SUCCESS;
	}
	
	@SkipValidation  
	   public String searchCustomCenterIntegrationLog() {
			getCompanyList();
		   integrationType = refMasterManager.findByParameter(sessionCorpID, "INTEGRATIONTYPE");
		    boolean fileName = (integrationLog.getFileName() == null);
		   boolean processedOn = (integrationLog.getProcessedOn() == null);
	    	boolean recordID = (integrationLog.getRecordID() == null);
	    	boolean message = (integrationLog.getMessage() == null);
	    	boolean transId = (integrationLog.getTransactionID() == null);  
	    	/*if(intType==null || (intType.equals(""))){
	    		intType="rg";
	    	}*/
	    	if(!fileName || !recordID || !message || !transId || !processedOn) {
	    		integrationLogs = integrationLogManager.findIntegrationCenterLogsWithServiceOrderId(integrationLog.getFileName(), integrationLog.getProcessedOn(), integrationLog.getRecordID(), integrationLog.getMessage(), integrationLog.getTransactionID(),intType, sessionCorpID);
	      	}
	    	 
	    	return SUCCESS;   
	    }
    
   @SkipValidation  
   public String search() {
	   integrationType = refMasterManager.findByParameter(sessionCorpID, "INTEGRATIONTYPE");
	    boolean fileName = (integrationLog.getFileName() == null);
	   boolean processedOn = (integrationLog.getProcessedOn() == null);
    	boolean recordID = (integrationLog.getRecordID() == null);
    	boolean message = (integrationLog.getMessage() == null);
    	boolean transId = (integrationLog.getTransactionID() == null);  
    	if(intType==null){
    		intType="";
    	}
    	if(!fileName || !recordID || !message || !transId || !processedOn) {
    		integrationLogs = integrationLogManager.findIntegrationLogsWithServiceOrderId(integrationLog.getFileName(), integrationLog.getProcessedOn(), integrationLog.getRecordID(), integrationLog.getMessage(), integrationLog.getTransactionID(),intType, sessionCorpID);
      	}
    	 
    	return SUCCESS;   
    }
   @SkipValidation  
   public String searchMemoUploadList() {
	  
	    boolean so = (memoUplod.getSo() == null);
	    if(so){
	    	memoUplod.setSo("");
	    }
	   boolean noteid = (memoUplod.getNote() == null);
	   if(noteid){
	    	memoUplod.setNote("");
	    }
    	boolean upstatus = (memoUplod.getUploadStatus() == null);
    	if(upstatus){
	    	memoUplod.setUploadStatus("");
	    }
    	boolean update = (memoUplod.getUploadDate() == null);
    	if(update){
	    	memoUplod.setUploadStatus(new Date().toString());
	    }    	
    		memoUploadLogs = integrationLogManager.findMemoUpload(memoUplod.getSo(), memoUplod.getNote(), memoUplod.getUploadStatus(), memoUplod.getUploadDate(), sessionCorpID);
      	
    	 
    	return SUCCESS;   
    }
   
   	public String weeklyXML() {
        integrationLogs = integrationLogManager.getWeeklyXML(sessionCorpID);
        return SUCCESS;
   	}
   	
   	public String editAgentIntegration(){
   		return SUCCESS;
   	}

	public IntegrationLog getIntegrationLog() {
		return integrationLog;
	}


	public void setIntegrationLogManager(IntegrationLogManager integrationLogManager) {
		this.integrationLogManager = integrationLogManager;
	}


	public void setIntegrationLog(IntegrationLog integrationLog) {
		this.integrationLog = integrationLog;
	}


	

	public String getShipNum() {
		return shipNum;
	}


	public void setShipNum(String shipNum) {
		this.shipNum = shipNum;
	}


	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}


	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}


	public List getMemoUploadLogs() {
		return memoUploadLogs;
	}


	public void setMemoUploadLogs(List memoUploadLogs) {
		this.memoUploadLogs = memoUploadLogs;
	}


	public MemoUploadDTO getMemoUplod() {
		return memoUplod;
	}


	public void setMemoUplod(MemoUploadDTO memoUplod) {
		this.memoUplod = memoUplod;
	}


	public Map<String, String> getIntegrationType() {
		return integrationType;
	}


	public void setIntegrationType(Map<String, String> integrationType) {
		this.integrationType = integrationType;
	}


	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}


	public String getIntType() {
		return intType;
	}


	public void setIntType(String intType) {
		this.intType = intType;
	}


	public Company getCompany() {
		return company;
	}


	public void setCompany(Company company) {
		this.company = company;
	}


	public Boolean getEnableMoveForYou() {
		return enableMoveForYou;
	}


	public void setEnableMoveForYou(Boolean enableMoveForYou) {
		this.enableMoveForYou = enableMoveForYou;
	}


	public Boolean getEnableMSS() {
		return enableMSS;
	}


	public void setEnableMSS(Boolean enableMSS) {
		this.enableMSS = enableMSS;
	}


	public Boolean getVanlineEnabled() {
		return vanlineEnabled;
	}


	public void setVanlineEnabled(Boolean vanlineEnabled) {
		this.vanlineEnabled = vanlineEnabled;
	}


	public Boolean getVoxmeIntegration() {
		return voxmeIntegration;
	}


	public void setVoxmeIntegration(Boolean voxmeIntegration) {
		this.voxmeIntegration = voxmeIntegration;
	}


	public Boolean getEnablePricePoint() {
		return enablePricePoint;
	}


	public void setEnablePricePoint(Boolean enablePricePoint) {
		this.enablePricePoint = enablePricePoint;
	}



	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	
}