<%@ include file="/common/taglibs.jsp"%>  

 
<head>   
    <title><fmt:message key="quotationFileList.title"/></title>   
    <meta name="heading" content="<fmt:message key='quotationFileList.heading'/>"/>   
    
<script language="javascript" type="text/javascript">
// # 10219 start
<c:if test="${checkAccessQuotation== true}">
{
<c:redirect url="/customerFiles.html">
</c:redirect>
}
</c:if>
// #10219 end          


function clear_fields(){
	    document.forms['searchForm'].elements['activeStatus'].checked = false;
		document.forms['searchForm'].elements['customerFile.job'].value  = "";
		document.forms['searchForm'].elements['customerFile.coordinator'].value  = "";
		document.forms['searchForm'].elements['customerFile.quotationStatus'].value  = "";
		document.forms['searchForm'].elements['customerFile.sequenceNumber'].value  = ""; 
		document.forms['searchForm'].elements['customerFile.lastName'].value  = "";
		document.forms['searchForm'].elements['customerFile.firstName'].value  = "";
		document.forms['searchForm'].elements['customerFile.billToName'].value  = "";
		document.forms['searchForm'].elements['customerFile.estimator'].value ="";
		document.forms['searchForm'].elements['customerFile.accountName'].value ="";
}

function selectSearchField(){
	var activeStatus=document.forms['searchForm'].elements['activeStatus'].checked;
		var sequenceNumber=document.forms['searchForm'].elements['customerFile.sequenceNumber'].value; 
		var lastName=document.forms['searchForm'].elements['customerFile.lastName'].value;
		var firstName=document.forms['searchForm'].elements['customerFile.firstName'].value;
		var billToName=document.forms['searchForm'].elements['customerFile.billToName'].value;
		var job=document.forms['searchForm'].elements['customerFile.job'].value;
		var coordinator=document.forms['searchForm'].elements['customerFile.coordinator'].value;
		var estimator=document.forms['searchForm'].elements['customerFile.estimator'].value;
		var status=document.forms['searchForm'].elements['customerFile.quotationStatus'].value;
		var accountName=document.forms['searchForm'].elements['customerFile.accountName'].value;
		if(activeStatus==false)
		if(sequenceNumber=='' && lastName=='' &&  firstName=='' && billToName=='' && job=='' && coordinator=='' && estimator=='' && status=='' && accountName=='')
		{
			alert('Please select any one of the search criteria!');	
			return false;	
		}
		
}
</script>

<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-8px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
div.error, span.error, li.error, div.message {
background:#BCD2EF none repeat scroll 0%;
border:1px solid #000000;
color:#000000;
font-family:Arial,Helvetica,sans-serif;
font-weight:bold;
margin:10px auto;
padding:3px;
text-align:center;
vertical-align:bottom;
width:450px;
}
.table td, .table th, .tableHeaderTable td {   
    padding: 0.4em;
}

form {
margin-top:-40px;
!margin-top:-5px;
}
div#content {margin-bottom:2px;}
</style>

</head>   
  
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:60px; height:25px"  
        onclick="location.href='<c:url value="/QuotationFileForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:60px; height:25px;margin-right:2px;" align="top" method="searchQuotation" key="button.search" onclick="return selectSearchField();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:60px; height:25px;" onclick="clear_fields();"/> 
</c:set>   

<s:hidden name="fileID"  id= "fileID" value=""/>
     
<s:form id="searchForm" action="searchQuotationFiles" method="post" validate="true">  
<s:hidden name="csoSortOrder"   value="${csoSortOrder}"/>
<c:set var="csoSortOrder"  value="${csoSortOrder}"/>
<s:hidden name="orderForCso"   value="${orderForCso}"/>
<c:set var="orderForCso"  value="${orderForCso}"/>
<div id="layer1" style="width:100%"> 
<c:set var="coordinatr" value="<%=request.getParameter("coordinatr") %>" />
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top" >
    <div class="top" style="margin-top:10px;!margin-top:-5px; "><span></span></div>
    <div class="center-content">
<table class="table" style="width:99%;" border="0">
<thead>
<tr>
<th>Quotation File#</th>
<th><fmt:message key="customerFile.lastName"/></th>
<th><fmt:message key="customerFile.firstName"/></th>
<th>Billing&nbsp;Party</th>
<th>Account&nbsp;Name</th>
<th><fmt:message key="customerFile.job"/></th>
<th><fmt:message key="customerFile.coordinator"/></th>
<th><fmt:message key="customerFile.quotationStatus"/></th>
<th>Consultant</th>
</tr></thead>	
		<tbody>
		<tr>
			<td width="" align="left">
			    <s:textfield name="customerFile.sequenceNumber" required="true" cssClass="input-text" size="10"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="customerFile.lastName" required="true" cssClass="input-text" size="12"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="customerFile.firstName" required="true" cssClass="input-text" size="9"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="customerFile.billToName" required="true" cssClass="input-text" size="9"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="customerFile.accountName" required="true" cssClass="input-text" cssStyle="width:80px" size="20"/>
			</td>
			<td width="">
			    <s:select cssClass="list-menu" name="customerFile.job" list="%{job}" cssStyle="width:160px" headerKey="" headerValue="" />
			</td>
			<td width="11%" align="left">
				<c:if test="${coordinatr == null || coordinatr == ''}" >
			    	<s:select cssClass="list-menu" name="customerFile.coordinator" list="%{coord}" cssStyle="width:110px" headerKey="" headerValue=""/>
			    </c:if>
			    <c:if test="${coordinatr != '' && coordinatr != null}" >
			    	<s:select cssClass="list-menu" name="customerFile.coordinator" list="%{coord}" cssStyle="width:110px" headerKey="" headerValue="" value="${coordinatr}"/>
			    </c:if>
			</td>
			
			<td width="" align="left">
			   <s:select cssClass="list-menu"   name="customerFile.quotationStatus" list="%{orderStatus1}" cssStyle="width:87px" headerKey="" headerValue=""/>
			</td>
			  <td align="left"><s:select cssClass="list-menu" name="customerFile.estimator" list="%{sale}"  cssStyle="width:92px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="10"/></td>
			</tr>
			<tr>
				<td width="" align="left" colspan="3"></td>	
							
				<td style="border-left:hidden;text-align:right;padding:0px;" colspan="6" class="listwhitetext">
				<table style="margin:0px;padding:0px;border:none; float:right;">
				<tr>
				<c:if  test="${compDivFlag == 'Yes'}">
					<td style="border:none;" class="listwhitetext">
			Company Division<br>
			<s:select name="customerFile.companyDivision" list="%{companyDivis}" cssClass="list-menu" cssStyle="width:85px;margin-right:18px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
			</c:if>
			<c:if  test="${compDivFlag != 'Yes'}">
			<s:hidden name="customerFile.companyDivision"/>
			</c:if>
				<td style="border:none;vertical-align:bottom;" class="listwhitetext">
			Active&nbsp;Quotes<br><s:checkbox key="activeStatus" cssStyle="vertical-align:middle; margin:5px 33px 3px 25px;"/>
			</td>
				<td style="border:none;" class="listwhitetext">
					Search Options<br>
			<s:select name="serviceOrderSearchVal" list="%{serviceOrderSearchType}" cssClass="list-menu" cssStyle="margin-top:2px;"/>
				</td>
				<td style="border:none;vertical-align:bottom;">
				    <c:out value="${searchbuttons}" escapeXml="false" />   
				    </td>
				    </tr>
				    </table>
				</td>
			</tr>	
			
		</tbody>
	</table>
</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>

<c:out value="${searchresults}" escapeXml="false" />  
		<div id="otabs" style="margin-bottom:0px;!margin-bottom:-13px;">
		  <ul>
		    <li><a class="current"><span>Quotation File List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<s:set name="quotationManagementListExt" value="quotationManagementListExt" scope="request"/>   
<display:table name="quotationManagementListExt" class="table" requestURI="" id="customerFileList" export="true" defaultsort="2" pagesize="10" style="width:99%;margin-top:1px;margin-left: 5px; " >   
    
    <display:column property="sequenceNumber" sortable="true" title="Quotation File#" url="/QuotationFileForm.html?from=list" paramId="id" paramProperty="id"/>
    <display:column property="lastName" sortable="true" titleKey="customerFile.lastName" maxLength="20"/>
    <display:column property="firstName" sortable="true" titleKey="customerFile.firstName" maxLength="20"/>
    <display:column property="billToName" sortable="true" titleKey="quotationFile.billToCode" maxLength="19"/> 
    <display:column property="accountName" sortable="true" title="Account&nbsp;Name" style="width:150px" maxLength="19"/> 
    <display:column property="job" sortable="true" titleKey="customerFile.job" style="width:70px"/>
    <display:column property="coordinator" sortable="true" titleKey="customerFile.coordinator" />
    <display:column property="statusDate" sortable="true" titleKey="customerFile.statusDate" style="width:100px"  format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="quotationStatus" sortable="true" titleKey="customerFile.quotationStatus" style="width:100px"/>
    <display:column property="createdOn" sortable="true" titleKey="customerFile.createdOn" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>
    <display:setProperty name="paging.banner.item_name" value="customerfile"/>   
    <display:setProperty name="paging.banner.items_name" value="customers"/>   
  
    <display:setProperty name="export.excel.filename" value="CustomerFile List.xls"/>   
    <display:setProperty name="export.csv.filename" value="CustomerFile List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="CustomerFile List.pdf"/>   
</display:table>   
  
<c:out value="${buttons}" escapeXml="false" />   
<c:set var="isTrue" value="false" scope="session"/>
</div>

</s:form>  
 
<script type="text/javascript">   
	try{
	document.forms['searchForm'].elements['customerFile.sequenceNumber'].focus();
   }
   catch(e){}
    highlightTableRows("customerFileList");  
    Form.focusFirstElement($("searchForm")); 
         try{
    <c:if test="${detailPage == true}" >
    	<c:redirect url="/QuotationFileForm.html?from=list&id=${customerFileList.id}" />
	</c:if>
	}
	catch(e){}  
	try{
   	if('${activeStatus}'=='true')
   	{
   		document.forms['searchForm'].elements['activeStatus'].checked = true;
   	}
   	}
   	catch(e){}
   try{
   if('${activeStatus}'=='false')
   	{
   		document.forms['searchForm'].elements['activeStatus'].checked = false;
   	}
   	}
   	catch(e){}   
</script> 
<script type="text/javascript">  
try{
	var companyDivision='${defaultCompanyCode}';
	if(companyDivision!=''){
	 document.forms['searchForm'].elements['customerFile.companyDivision'].value='${companyCode}';
	}
}catch(e){}
</script>