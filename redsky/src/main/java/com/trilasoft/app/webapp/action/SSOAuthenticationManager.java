package com.trilasoft.app.webapp.action;

import org.acegisecurity.AbstractAuthenticationManager;
import org.acegisecurity.AccountExpiredException;
import org.acegisecurity.AcegiMessageSource;
import org.acegisecurity.Authentication;
import org.acegisecurity.AuthenticationException;
import org.acegisecurity.AuthenticationServiceException;
import org.acegisecurity.BadCredentialsException;
import org.acegisecurity.CredentialsExpiredException;
import org.acegisecurity.DisabledException;
import org.acegisecurity.LockedException;
import org.acegisecurity.concurrent.ConcurrentLoginException;
import org.acegisecurity.concurrent.ConcurrentSessionController;
import org.acegisecurity.concurrent.NullConcurrentSessionController;
import org.acegisecurity.event.authentication.AbstractAuthenticationEvent;
import org.acegisecurity.event.authentication.AuthenticationFailureBadCredentialsEvent;
import org.acegisecurity.event.authentication.AuthenticationFailureConcurrentLoginEvent;
import org.acegisecurity.event.authentication.AuthenticationFailureCredentialsExpiredEvent;
import org.acegisecurity.event.authentication.AuthenticationFailureDisabledEvent;
import org.acegisecurity.event.authentication.AuthenticationFailureExpiredEvent;
import org.acegisecurity.event.authentication.AuthenticationFailureLockedEvent;
import org.acegisecurity.event.authentication.AuthenticationFailureProviderNotFoundEvent;
import org.acegisecurity.event.authentication.AuthenticationFailureProxyUntrustedEvent;
import org.acegisecurity.event.authentication.AuthenticationFailureServiceExceptionEvent;
import org.acegisecurity.event.authentication.AuthenticationSuccessEvent;
import org.acegisecurity.providers.AuthenticationProvider;
import org.acegisecurity.providers.ProviderManager;
import org.acegisecurity.providers.ProviderNotFoundException;
import org.acegisecurity.providers.cas.ProxyUntrustedException;
import org.acegisecurity.providers.dao.DaoAuthenticationProvider;
import org.acegisecurity.userdetails.UserDetailsService;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.dao.UserDao;
import org.appfuse.model.User;
import org.appfuse.util.StringUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.MessageSource;
 import org.springframework.context.MessageSourceAware;
 import org.springframework.context.support.MessageSourceAccessor;
 import org.springframework.util.Assert;
 import java.lang.reflect.Constructor;
 import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Date;
 import java.util.Iterator;
 import java.util.List;
import java.util.Properties; 
import org.acegisecurity.userdetails.UserDetails;

 public class  SSOAuthenticationManager   extends ProviderManager implements InitializingBean,



     ApplicationEventPublisherAware, MessageSourceAware {



     //~ Static fields/initializers =====================================================================================



 



     private static final Log logger = LogFactory.getLog(ProviderManager.class);



     private static final Properties DEFAULT_EXCEPTION_MAPPINGS = new Properties();





    //~ Instance fields ================================================================================================





    private ApplicationEventPublisher applicationEventPublisher;



    private ConcurrentSessionController sessionController = new NullConcurrentSessionController();



    private List providers;



    protected MessageSourceAccessor messages = AcegiMessageSource.getAccessor();



    private Properties exceptionMappings = new Properties();





    static {



        DEFAULT_EXCEPTION_MAPPINGS.put(AccountExpiredException.class.getName(),



            AuthenticationFailureExpiredEvent.class.getName());



        DEFAULT_EXCEPTION_MAPPINGS.put(AuthenticationServiceException.class.getName(),



            AuthenticationFailureServiceExceptionEvent.class.getName());



        DEFAULT_EXCEPTION_MAPPINGS.put(LockedException.class.getName(), AuthenticationFailureLockedEvent.class.getName());



        DEFAULT_EXCEPTION_MAPPINGS.put(CredentialsExpiredException.class.getName(),



            AuthenticationFailureCredentialsExpiredEvent.class.getName());



        DEFAULT_EXCEPTION_MAPPINGS.put(DisabledException.class.getName(), AuthenticationFailureDisabledEvent.class.getName());



        DEFAULT_EXCEPTION_MAPPINGS.put(BadCredentialsException.class.getName(),



            AuthenticationFailureBadCredentialsEvent.class.getName());



        DEFAULT_EXCEPTION_MAPPINGS.put(UsernameNotFoundException.class.getName(),



            AuthenticationFailureBadCredentialsEvent.class.getName());



        DEFAULT_EXCEPTION_MAPPINGS.put(ConcurrentLoginException.class.getName(),



            AuthenticationFailureConcurrentLoginEvent.class.getName());



        DEFAULT_EXCEPTION_MAPPINGS.put(ProviderNotFoundException.class.getName(),



            AuthenticationFailureProviderNotFoundEvent.class.getName());



        DEFAULT_EXCEPTION_MAPPINGS.put(ProxyUntrustedException.class.getName(),



            AuthenticationFailureProxyUntrustedEvent.class.getName());



    }





    public   SSOAuthenticationManager() {



        exceptionMappings.putAll(DEFAULT_EXCEPTION_MAPPINGS);



    }





    //~ Methods ========================================================================================================





    public void   afterPropertiesSet() throws Exception {



        checkIfValidList(this.providers);



        Assert.notNull(this.messages, "A message source must be set");



        doAddExtraDefaultExceptionMappings(DEFAULT_EXCEPTION_MAPPINGS);



    }





    private void   checkIfValidList(List listToCheck) {



        if ((listToCheck == null) || (listToCheck.size() == 0)) {



            throw new IllegalArgumentException("A list of AuthenticationManagers is required");



        }



    }

    



    protected void   doAddExtraDefaultExceptionMappings(Properties exceptionMappings) {}

    



    public Authentication   doAuthentication(Authentication authentication)
        throws AuthenticationException {
        Iterator iter = providers.iterator();
        Class toTest = authentication.getClass();
        AuthenticationException lastException = null;
        while (iter.hasNext()) {
            AuthenticationProvider provider = (AuthenticationProvider) iter.next();
            if(provider instanceof DaoAuthenticationProvider){
            	UserDetailsService userDetailsService=((DaoAuthenticationProvider) provider).getUserDetailsService();
            	System.out.println("user nameeeeeeeeeeeeeeee in mngr"+authentication.getName());
            	org.appfuse.dao.hibernate.UserDaoHibernate us1=(org.appfuse.dao.hibernate.UserDaoHibernate)userDetailsService;
            	//UserDetails userDetails=(UserDetails) userDetailsService.loadUserByUsername(authentication.getName());
            	//UserDetails us=(UserDetails) userDetailsService.loadUserByUsername(authentication.getName());
            	org.acegisecurity.userdetails.UserDetails us=(org.acegisecurity.userdetails.UserDetails) userDetailsService.loadUserByUsername(authentication.getName());
               if(us!=null){
            	   String algorithm="SHA";
            	   //
            	  /* String algorithm = (String) getConfiguration().get(Constants.ENC_ALGORITHM);

                   if (algorithm == null) { // should only happen for test case
                       log.debug("assuming testcase, setting algorithm to 'SHA'");
                       algorithm = "SHA";
                   }
                   String pass=user.getPassword();
                   user.setPassword(StringUtil.encodePassword(pass, algorithm));*/
            	   //
            	   String pass1=us.getPassword();
            	   String pass2=StringUtil.encodePassword(authentication.getCredentials().toString(), algorithm);
            	   if(!pass1.equals(pass2)){
            		   Date curr=new Date();
            			SimpleDateFormat fm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
            			String dtt=fm.format(curr);
            			SimpleDateFormat fm1=new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss") ;
            			String dtt11=fm1.format(curr);
            			String dtt1=fm.format(curr);
               			dtt1=dtt1+"  "+"FAILURE";
               			dtt11=dtt11+"  "+"Failed";
               			dtt11=dtt11+"      "+"Incorrect Password";
            			//System.out.println(dtt);
            		   ((UserDao) userDetailsService).updateUserLoginFailureReason(authentication.getName(),dtt11,dtt1);
            	   }else if(pass1.equals(pass2)){            		   
            		   Date curr=new Date();
           			SimpleDateFormat fm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
           			String dtt=fm.format(curr);
           			List usrs=((UserDao) userDetailsService).findUserDetails(authentication.getName());
           			org.appfuse.model.User tmpuser=null;
           			if(usrs!=null && !(usrs.isEmpty()) && usrs.get(0)!=null){
           				tmpuser=(User) usrs.get(0);
           				Date expdt=tmpuser.getPwdexpiryDate();
           				SimpleDateFormat fm1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
               			String dtt1=fm1.format(expdt);
               			Date currr=new Date();
               			SimpleDateFormat fm11=new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss") ;
               			String dttright1=fm11.format(currr);
               			//Date curr1=new Date();               			
               			String dtt2=fm1.format(currr);
               			if((tmpuser.isCredentialsExpired())){
               				String dtt3=dttright1;
               				String dtt4=dtt2;
               				dtt4=dtt4+"  "+"FAILURE";
               				dtt3=dtt3+"  "+"Failed";
                			dtt3=dtt3+"      "+"Password Expired";
                 		   ((UserDao) userDetailsService).updateUserLoginFailureReason(authentication.getName(),dtt3,dtt4);
               				
               			}else if((tmpuser.isAccountExpired())){
               				String dtt3=dttright1;
               				String dtt4=dtt2;
               				dtt4=dtt4+"  "+"FAILURE";
               				dtt3=dtt3+"  "+"Failed";
                			dtt3=dtt3+"      "+"Account Expired";
                 		   ((UserDao) userDetailsService).updateUserLoginFailureReason(authentication.getName(),dtt3,dtt4);
               				
               			}else if((tmpuser.isAccountLocked())){
               				String dtt3=dttright1;
               				String dtt4=dtt2;
               				dtt4=dtt4+"  "+"FAILURE";
               				dtt3=dtt3+"  "+"Failed";
                			dtt3=dtt3+"      "+"Account Locked";
                 		   ((UserDao) userDetailsService).updateUserLoginFailureReason(authentication.getName(),dtt3,dtt4);
               				
               			}else{
               				dtt=dtt+"  "+"SUCCESS";
                 		   ((UserDao) userDetailsService).updateUserLoginFailureReason(authentication.getName(),"",dtt);
               				
               			}
               				
           			}
           			
            		   
            	   }
            	System.out.println(pass1);
            	System.out.println(pass2);
               }
            }
            if (provider.supports(toTest)) {
               logger.debug("Authentication attempt using " + provider.getClass().getName());
                Authentication result = null;
                try {
                    result = provider.authenticate(authentication);
                    sessionController.checkAuthenticationAllowed(result);
                } catch (AuthenticationException ae) {
                    lastException = ae;
                    result = null;
                }

                if (result != null) {
                    sessionController.registerSuccessfulAuthentication(result);
                    publishEvent(new AuthenticationSuccessEvent(result));
                    return result;

                }
            }
        }
        if (lastException == null) {
            lastException = new ProviderNotFoundException(messages.getMessage("ProviderManager.providerNotFound",
                        new Object[] {toTest.getName()}, "No AuthenticationProvider found for {0}"));
        }
        // Publish the event
        String className = exceptionMappings.getProperty(lastException.getClass().getName());
        AbstractAuthenticationEvent event = null;
        if (className != null) {
            try {
                Class clazz = getClass().getClassLoader().loadClass(className);
                Constructor constructor = clazz.getConstructor(new Class[] {
                        Authentication.class, AuthenticationException.class
                        });
                Object obj = constructor.newInstance(new Object[] {authentication, lastException});
                Assert.isInstanceOf(AbstractAuthenticationEvent.class, obj, "Must be an AbstractAuthenticationEvent");
                event = (AbstractAuthenticationEvent) obj;
            } catch (ClassNotFoundException ignored) {}
            catch (NoSuchMethodException ignored) {}
            catch (IllegalAccessException ignored) {}
            catch (InstantiationException ignored) {}
            catch (InvocationTargetException ignored) {}
        }
        if (event != null) {
            publishEvent(event);
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("No event was found for the exception " + lastException.getClass().getName());
            }
        }
        throw lastException;
    }





    public List   getProviders() {
        return this.providers;
    }

    



    public ConcurrentSessionController  getSessionController() {



        return sessionController;



    }





    public void   setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {



        this.applicationEventPublisher = applicationEventPublisher;



    }




    public void   setMessageSource(MessageSource messageSource) {


        this.messages = new MessageSourceAccessor(messageSource);



    }

    



    public void   setProviders(List newList) {



        checkIfValidList(newList);





        Iterator iter = newList.iterator();





        while (iter.hasNext()) {



            Object currentObject = null;





            try {



                currentObject = iter.next();





                //TODO bad idea, should use assignable from or instance of 



                AuthenticationProvider attemptToCast = (AuthenticationProvider) currentObject;



            } catch (ClassCastException cce) {



                throw new IllegalArgumentException("AuthenticationProvider " + currentObject.getClass().getName()



                    + " must implement AuthenticationProvider");



            }



        }





        this.providers = newList;



    }

    



    public void   setSessionController(ConcurrentSessionController sessionController) {



        this.sessionController = sessionController;



    }





    private void   publishEvent( ApplicationEvent event ) {



        if ( applicationEventPublisher != null ) {



            applicationEventPublisher.publishEvent( event );



        }



    }



}
