package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DsRelocationAreaInfoOrientationDao;
import com.trilasoft.app.model.DsRelocationAreaInfoOrientation;



public class DsRelocationAreaInfoOrientationDaoHibernate extends GenericDaoHibernate<DsRelocationAreaInfoOrientation, Long> implements DsRelocationAreaInfoOrientationDao{
 public DsRelocationAreaInfoOrientationDaoHibernate(){
	 super(DsRelocationAreaInfoOrientation.class);
 }
	public List getListById(Long id) {		
		return getHibernateTemplate().find("from DsRelocationAreaInfoOrientation where id = "+id+" ");
	}

}
