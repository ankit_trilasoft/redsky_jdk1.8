/**
 * @Class Name  ServicePartner Dao Hibernate
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */ 

package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ServicePartnerDao;
import com.trilasoft.app.dao.hibernate.PartnerDaoHibernate.HollPartnerDTO;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.ServicePartner;

public class ServicePartnerDaoHibernate extends GenericDaoHibernate<ServicePartner, Long> implements ServicePartnerDao {
	public ServicePartnerDaoHibernate() {
		super(ServicePartner.class);
	}
	public class SDTO{
		private Object id;
		private  Object carrierName;
		private  Object carrierDeparture;
		private Object carrierArrival;
		private Object atDepart;
		private Object atArrival;
		private Object pieces;
		public Object getCarrierName() {
			return carrierName;
		}
		public void setCarrierName(Object carrierName) {
			this.carrierName = carrierName;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getCarrierDeparture() {
			return carrierDeparture;
		}
		public void setCarrierDeparture(Object carrierDeparture) {
			this.carrierDeparture = carrierDeparture;
		}
		public Object getCarrierArrival() {
			return carrierArrival;
		}
		public void setCarrierArrival(Object carrierArrival) {
			this.carrierArrival = carrierArrival;
		}
		public Object getAtDepart() {
			return atDepart;
		}
		public void setAtDepart(Object atDepart) {
			this.atDepart = atDepart;
		}
		public Object getAtArrival() {
			return atArrival;
		}
		public void setAtArrival(Object atArrival) {
			this.atArrival = atArrival;
		}
		public Object getPieces() {
			return pieces;
		}
		public void setPieces(Object pieces) {
			this.pieces = pieces;
		}
	}

	public List<ServicePartner> findByLastName(String lastName) {
		return getHibernateTemplate().find("from ServicePartner where  lastName=?", lastName);
	}

	public List checkById(Long id) {
		return getHibernateTemplate().find("select id from ServicePartner where id=?", id);
	}

	public List checkByShipNumber(String ship) {
		return getHibernateTemplate().find("select shipNumber from ServicePartner where  status=true and  shipNumber=?", ship);
	}

	public List findMaxId() {
		return getHibernateTemplate().find("select max(id) from ServicePartner");
	}

	public List<ServicePartner> findByBroker(String shipNumber) {
		return getHibernateTemplate().find("from ServicePartner where (partnerType='BR' OR partnerType='FW') AND shipNumber=?", shipNumber);
	}

	public List<ServicePartner> findByCarriers(String shipNumber) {
		return getHibernateTemplate().find("from ServicePartner where  partnerType='CR' AND shipNumber=?", shipNumber);
	}

	public List<ServicePartner> findByCarrier(String shipNumber) {
		getHibernateTemplate().setMaxResults(500);
		return getHibernateTemplate().find("from Partner where isCarrier is true");
	}

	 public List routingListOtherCorpId(Long sid){
	    	return getSession().createSQLQuery("select id from servicepartner where serviceOrderId='"+sid+"'  ").list();
	  }
	 
	public List<ServicePartner> findByAgent(String shipNumber) {
		return getHibernateTemplate().find("from ServicePartner where  status=true and  (partnerType='BK' OR partnerType='DA' OR partnerType='OA' OR partnerType='DL' OR partnerType='PK' OR partnerType='ST') AND shipNumber=?",shipNumber);
	}

	public List<ServicePartner> findByPartner(String shipNumber) {
		return getHibernateTemplate().find("from ServicePartner where  status=true and  shipNumber=?", shipNumber);
	}

	public List<ServicePartner> findForPartner(String corpId, String shipNumber) {
		return getHibernateTemplate().find("select partnerType from ServicePartner where  status=true and  corpID='" + corpId + "' and shipNumber=?", shipNumber);
	}

	public List<ServicePartner> findByCarrierFromAccountLine(String shipNumber) {
		return getHibernateTemplate().find("from AccountLine where category='Freight/Carrier' AND shipNumber=?", shipNumber);
	}

	public int updateTrack(String shipNumber, String billLading,String hbillLading,String userName) {
		System.out.println("hbillLading>>> "+hbillLading+"billLading>>> "+billLading);
		List newList = new ArrayList();
		String blNum = "";
		String hblNum = "";
		// List servicePartnerlist = getHibernateTemplate().find("from ServicePartner where shipNumber=?", shipNumber);
		List servicePartnerlist = this.getSession().createSQLQuery("select * from servicepartner where shipNumber='"+shipNumber+"'  and status=true  ").list();
		if (servicePartnerlist == null || servicePartnerlist.isEmpty()) {
			//return getHibernateTemplate().bulkUpdate("update TrackingStatus set billLading='" + billLading + "' where shipNumber=?", shipNumber);
			return this.getSession().createSQLQuery("update trackingstatus set billLading='" + billLading + "',hbillLading='" + hbillLading + "',updatedBy='"+userName+"', updatedOn=now() where shipNumber='"+shipNumber+"'").executeUpdate();
		} else {
			newList = this.getSession().createSQLQuery("select concat(if(blNumber<>'',blNumber,'No'),'~',if(hblNumber<>'',hblNumber,'No')) from servicepartner where id in (select min(id) from servicepartner where shipNumber='" + shipNumber + "'  and status=true  and partnerType='CR')").list();
	
			if (newList.get(0) != null && !newList.isEmpty() && (!(newList.get(0).toString().trim().equals("~")))) {
				String a[] = newList.get(0).toString().split("~");
				blNum = a[0].toString();
				hblNum = a[1].toString();
			}
			if (blNum.equalsIgnoreCase(null) || blNum.equalsIgnoreCase("null") || blNum.equalsIgnoreCase("No")) {
				blNum = "";
			}
			if (hblNum.equalsIgnoreCase(null) || hblNum.equalsIgnoreCase("null") || hblNum.equalsIgnoreCase("NO")) {
				hblNum = "";
			}
			//return getHibernateTemplate().bulkUpdate("update TrackingStatus set billLading='" + blNum + "' where shipNumber=?", shipNumber);
			return this.getSession().createSQLQuery("update trackingstatus set billLading='" + blNum + "', hbillLading='" + hblNum + "',updatedBy='"+userName+"', updatedOn=now() where shipNumber='"+shipNumber+"'").executeUpdate();
		}
	}

	public List<ServicePartner> portName(String portCode, String sessionCorpID){
		return getHibernateTemplate().find("select distinct portName from Port where portCode='"+ portCode+"' and corpid in ('TSFT','"+sessionCorpID+"')");
	}

	public List findCarrierFromAccLine(String carr, String vendorCode, String shipNumber) {
		return getHibernateTemplate().find("from AccountLine where vendorCode='" + vendorCode.replaceAll("'", "''").replaceAll(":", "''") + "' AND category='" + carr.replaceAll("'", "''") + "' AND shipNumber='" + shipNumber.replaceAll("'", "''") + "' ");
	}

	public List findTranshipped(String shipNumber) {
		return getHibernateTemplate().find("select carrierNumber,transhipped,polCode,poeCode,carrierCode, carrierName,carrierArrival,cntnrNumber from ServicePartner  where  status=true and   shipNumber='" + shipNumber + "'");
	}

	public int updateDeleteStatus(Long ids) {
		return getHibernateTemplate().bulkUpdate("update ServicePartner set status=false where id='" + ids + "' or servicePartnerId='" + ids + "'");
	}

	public int updateServiceOrder(String carrierDeparture, String carrierArrival, String shipNumber,String userName) {
		List newList = new ArrayList();
		List newArrList = new ArrayList();
		String carrierDep = "";
		String carrierArr = "";
		newList = this.getSession().createSQLQuery("select carrierDeparture from servicepartner where   status=true and id in (select min(id) from servicepartner where shipNumber='" + shipNumber + "' and status=true  and partnerType='CR')").list();
		newArrList = this.getSession().createSQLQuery("select carrierArrival from servicepartner where status=true and  id in (select max(id) from servicepartner where shipNumber='" + shipNumber + "' and status=true and partnerType='CR')").list();
		if (!newList.isEmpty() && newList.get(0) != null) {
			carrierDep = newList.get(0).toString();
		}

		if (carrierDep.equalsIgnoreCase(null)) {
			carrierDep = "";
		}
		if (!newArrList.isEmpty() && newList.get(0) != null) {
			carrierArr = newArrList.get(0).toString();
		}

		if (carrierArr.equalsIgnoreCase(null)) {
			carrierArr = "";
		}
		this.getSession().createSQLQuery("update serviceorder set carrierDeparture ='"+carrierDep.replaceAll("'", "\\\\'")+"',updatedBy='"+userName+"', updatedOn=now() where shipNumber='" + shipNumber + "'").executeUpdate();
		return this.getSession().createSQLQuery("update serviceorder set carrierArrival='"+carrierArr.replaceAll("'", "\\\\'")+"',updatedBy='"+userName+"' , updatedOn=now() where shipNumber='" + shipNumber + "'").executeUpdate();
	}

	public int updateServiceOrderDate(Long id, String corpId,String userName) {
		List newETDList = new ArrayList();
		List newATDList = new ArrayList();
		String carrierETDNew = null;
		String carrierATDNew = null;
		Date carrierETD = null;
		Date carrierATD = null;
		newETDList = this.getSession().createSQLQuery(
				"select p.etDepart from servicepartner p where p.serviceOrderId='" + id
				+ "' and  p.status=true  and  p.carriernumber = (select min(k.carriernumber) from servicepartner k where p.serviceorderid = k.serviceorderid and k.status=true) and p.corpid = '" + corpId
				+ "'  ").list();
		newATDList = this.getSession().createSQLQuery(
				"select p.atDepart from servicepartner p where p.serviceOrderId='" + id
				+ "' and p.status=true and p.carriernumber = (select min(k.carriernumber) from servicepartner k where p.serviceorderid = k.serviceorderid and k.status=true) and p.corpid = '" + corpId
				+ "'  ").list();
		if (!newETDList.isEmpty() && newETDList.get(0) != null) {
			try {
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				carrierETD = df.parse(newETDList.get(0).toString());
				StringBuilder nowYYYYMMDD = new StringBuilder(df.format(carrierETD));
				carrierETDNew = nowYYYYMMDD.toString();

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		if (!newATDList.isEmpty() && newATDList.get(0) != null) {
			try {
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				carrierATD = df.parse(newATDList.get(0).toString());
				StringBuilder nowYYYYMMDD = new StringBuilder(df.format(carrierATD));
				carrierATDNew = nowYYYYMMDD.toString();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		if (carrierETDNew == null && carrierATDNew != null)
			return this.getSession().createSQLQuery("update serviceorder set ETD =" + carrierETDNew + " , ATD ='" + carrierATDNew + "' , updatedBy='"+userName+"' , updatedOn=now() where id="+id+"").executeUpdate();
		if (carrierATDNew == null && carrierETDNew != null)
			return this.getSession().createSQLQuery("update serviceorder set ETD ='" + carrierETDNew + "' , ATD =" + carrierATDNew + " , updatedBy='"+userName+"' , updatedOn=now() where id="+id+"").executeUpdate();
		if (carrierETDNew == null && carrierATDNew == null)
			return this.getSession().createSQLQuery("update serviceorder set ETD =" + carrierETDNew + " , ATD =" + carrierATDNew + " , updatedBy='"+userName+"' , updatedOn=now() where id="+id+"").executeUpdate();
		if (carrierETDNew != null && carrierATDNew != null)
			return this.getSession().createSQLQuery("update serviceorder set ETD ='" + carrierETDNew + "' , ATD ='" + carrierATDNew + "' , updatedBy='"+userName+"' , updatedOn=now() where id="+id+"").executeUpdate();
		else
			return this.getSession().createSQLQuery("update serviceorder set ETD ='" + carrierETDNew + "' , ATD ='" + carrierATDNew + "' , updatedBy='"+userName+"' , updatedOn=now()  where id="+id+"").executeUpdate();

	}

	public int updateServiceOrderActualDate(Long id, String corpId, Long customerFileId,String userName) {
		List newETAList = new ArrayList();
		List newATAList = new ArrayList();
		String carrierETANew = null;
		String carrierATANew = null;
		Date carrierETA = null;
		Date carrierATA = null;
		newETAList = this.getSession().createSQLQuery(
				"select p.etArrival from servicepartner p where p.serviceOrderId='" + id
				+ "' and p.status=true and p.carriernumber = (select max(k.carriernumber) from servicepartner k where p.serviceorderid = k.serviceorderid and k.status=true) and p.corpid = '" + corpId
				+ "'  ").list();
		newATAList = this.getSession().createSQLQuery(
				"select p.atArrival from servicepartner p where p.serviceOrderId='" + id
				+ "' and p.status = true and p.carriernumber = (select max(k.carriernumber) from servicepartner k where p.serviceorderid = k.serviceorderid and k.status= true ) and p.corpid = '" + corpId
				+ "'  ").list();
		if (!newETAList.isEmpty() && newETAList.get(0) != null) {
			try {
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				carrierETA = df.parse(newETAList.get(0).toString());
				StringBuilder nowYYYYMMDD = new StringBuilder(df.format(carrierETA));
				carrierETANew = nowYYYYMMDD.toString();

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		if (!newATAList.isEmpty() && newATAList.get(0) != null) {
			try {
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				carrierATA = df.parse(newATAList.get(0).toString());
				StringBuilder nowYYYYMMDD = new StringBuilder(df.format(carrierATA));
				carrierATANew = nowYYYYMMDD.toString();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		if(carrierETANew != null){
			this.getSession().createSQLQuery("update customerfile set salesStatus='Booked',updatedBy='"+userName+"',updatedOn=now() where id = '"+customerFileId+"' and salesStatus!='Booked' ").executeUpdate();
		}
		if (carrierETANew == null && carrierATANew != null)
			return this.getSession().createSQLQuery("update serviceorder set ETA =" + carrierETANew + " , ATA ='" + carrierATANew + "' ,updatedBy='"+userName+"' , updatedOn=now() where id="+id+"").executeUpdate();
		if (carrierETANew != null && carrierATANew == null)
			return this.getSession().createSQLQuery("update serviceorder set ETA ='" + carrierETANew + "' , ATA =" + carrierATANew + " ,updatedBy='"+userName+"' , updatedOn=now() where id="+id+"").executeUpdate();
		if (carrierETANew == null && carrierATANew == null)
			return this.getSession().createSQLQuery("update serviceorder set ETA =" + carrierETANew + " , ATA =" + carrierATANew + " ,updatedBy='"+userName+"' , updatedOn=now() where id="+id+"").executeUpdate();
		if (carrierETANew != null && carrierATANew != null)
			return this.getSession().createSQLQuery("update serviceorder set ETA ='" + carrierETANew + "' , ATA ='" + carrierATANew + "' ,updatedBy='"+userName+"' , updatedOn=now() where id="+id+"").executeUpdate();
		else
			return this.getSession().createSQLQuery("update serviceorder set ETA ='" + carrierETANew + "' , ATA ='" + carrierATANew + "' ,updatedBy='"+userName+"' , updatedOn=now() where id="+id+"").executeUpdate();
	}

	public List findByCarrierCode(String carrierCode) {
		return getHibernateTemplate().find("select id from PartnerPublic where partnerCode=?", carrierCode);
	}

	public List getContainerNumber(String shipNumber) {
		return getHibernateTemplate().find("select containerNumber from Container where status = true and  shipNumber=? and containerNumber is not null ", shipNumber);
	}

	public List goSOChild(Long serviceOrderId, String corpID, Long id) {
		return getHibernateTemplate().find("from  ServicePartner where  status=true and  serviceOrderId='" + serviceOrderId + "' and id !='" + id + "' and corpID='" + corpID + "'");
	}

	public List goNextSOChild(Long serviceOrderId, String corpID, Long id) {
		return getHibernateTemplate().find("select id from  ServicePartner where  status=true and  serviceOrderId='" + serviceOrderId + "' and id >'" + id + "' and corpID='" + corpID + "' order by 1 ");
	}

	public List goPrevSOChild(Long serviceOrderId, String corpID, Long id) {
		return getHibernateTemplate().find("select id from  ServicePartner where  status=true and  serviceOrderId='" + serviceOrderId + "' and id <'" + id + "' and corpID='" + corpID + "' order by 1 desc");
	}

	public List findMaximumChild(String shipNm) {
		return getHibernateTemplate().find("select max(id) from ServicePartner where   shipNumber=?", shipNm);
	}

	public List findMinimumChild(String shipNm) {
		return getHibernateTemplate().find("select min(id) from ServicePartner where  shipNumber=?", shipNm);
	}
	public List findTranshipChild(String shipNm){
		return getHibernateTemplate().find("select min(id) from ServicePartner where  status=true and  transhipped is not true and atArrival is not null and shipNumber=?", shipNm);
	}
	public List findCountChild(String shipNm) {
		return getHibernateTemplate().find("select count(*) from ServicePartner where   shipNumber=?", shipNm);
	}

	public List findClone(String shipNumber) {
		return getHibernateTemplate().find("select clone, etDepart, etArrival, atDepart, atArrival, carrierCode, carrierName, aesNumber,revisedETD,revisedETA from ServicePartner  where  status=true and   shipNumber='" + shipNumber + "'");
	}
	
     public int copyAtd(Date atd, String shipNumber)
     {
    	 String atdDate=null;
    	 if (!(atd == null)) {
    			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
    			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(atd));
    			atdDate = nowYYYYMMDD2.toString();
    			}
    	 return getHibernateTemplate().bulkUpdate("update ServicePartner set atDepart='"+atdDate+"' where shipNumber='"+shipNumber+"'");
     }
	
	 public int copyAta(Date ata, String shipNumber){
		 String ataDate=null;
		 if (!(ata == null)) {
 			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
 			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(ata));
 			ataDate = nowYYYYMMDD2.toString();
 			}
		 return getHibernateTemplate().bulkUpdate("update ServicePartner set atArrival='"+ataDate+"' where shipNumber='"+shipNumber+"'");
	 }

	public List routingList(Long serviceOrderId) {
		return getHibernateTemplate().find("from ServicePartner where  serviceOrderId='" + serviceOrderId + "'");
	}

	public Long findRemoteServicePartner(String carrierNumber,Long serviceOrderId) {
		return Long.parseLong(this.getSession().createSQLQuery("select id from servicepartner where carrierNumber='"+carrierNumber+"' and serviceOrderId="+serviceOrderId+"").list().get(0).toString());
	}

	public void deleteNetworkServicePartner(Long serviceOrderId,String carrierNumber, String routingStatus) {
		this.getSession().createSQLQuery("update  servicepartner set status="+routingStatus+" where carrierNumber='"+carrierNumber+"' and serviceOrderId="+serviceOrderId+"").executeUpdate();		
	}
	public List findServicePartnerBySid(Long sofid, String sessionCorpID){
		return this.getSession().createSQLQuery("select id from servicepartner where serviceOrderId='"+sofid+"'and corpID='"+sessionCorpID+"' ").list();
	}
	
	public List findRoutingLines(Long grpId, String sessionCorpID){
		
		return this.getSession().createSQLQuery("select sp.id  from servicepartner sp  inner join serviceorder s  on s.shipNumber=sp.shipNumber where s.grpID='"+grpId+"' and sp.status = true ").list();
	}
	
	public List routingListByGrpId(Long servicePartnerId){
	// return this.getSession().createSQLQuery("select sp.id from servicepartner sp  inner join serviceorder s  on s.shipNumber=sp.shipNumber where s.grpID='"+grpId+"'").list();
		return getHibernateTemplate().find("from ServicePartner where  servicePartnerId='"+servicePartnerId+"'");
		//return this.getSession().createSQLQuery("select  s.id  from  serviceorder s left outer join container c on s.id=c.serviceorderId where s.corpId='' and c.serviceOrderId is null");
	}
	
    public List routingListByIdAndgrpId(Long grpId){
		
		return this.getSession().createSQLQuery("select sp.id from servicepartner sp  inner join serviceorder s  on s.shipNumber=sp.shipNumber where s.grpID='"+grpId+"' or s.id='"+grpId+"' sp.status = true ").list();
	}
    
    public List routingWithNoLine(String sessionCorpId,Long grpId,Long masterServicePartnerId){
    	//return this.getSession().createSQLQuery("select  s.id  from  serviceorder s left outer join servicepartner sp on s.id=sp.serviceorderId where s.corpId='"+sessionCorpId+"' and sp.serviceorderId is null  and s.grpId ='"+grpId+"'").list();
    	
    	String temShips="";
    	List listOfChildOrders=new ArrayList();
    	List<ServicePartner> listOfPartners=new ArrayList();
    	StringBuilder formatShipNos=new StringBuilder("");
    	List listOfIdWithNoLine=new ArrayList();
    	listOfChildOrders=getHibernateTemplate().find("select shipNumber from ServiceOrder where grpId=?",grpId);
    	if(listOfChildOrders!=null && !listOfChildOrders.isEmpty() ){
    		Iterator itt=listOfChildOrders.iterator();
    		while(itt.hasNext()){
    			formatShipNos=formatShipNos.append(",").append("'").append(itt.next()).append("'");
    			
    			
    		}
    		 temShips=formatShipNos.substring(1,formatShipNos.length());
    		listOfPartners=getHibernateTemplate().find("select shipNumber from ServicePartner where  servicePartnerId =?",masterServicePartnerId);
    	}
    	if(listOfPartners!=null && !(listOfPartners.isEmpty())){
    		Iterator listIter=listOfPartners.iterator();
    		while(listIter.hasNext()){
    			String temp=listIter.next().toString();
    		//	if(temp.getServicePartnerId()!=)
    			
    			 if(temShips.indexOf(temp)>-1){
    				 temShips=temShips.replace(temp,"");
    				 temShips=temShips.replace("''","");
    				 temShips=temShips.replaceAll(",,",",");
    				    int len=temShips.length()-1;
    				   
    				    if(len==temShips.lastIndexOf(",")){
    				    	temShips=temShips.substring(0,temShips.length());
    				    
    				    }
    				    if(temShips.indexOf(",")==0){
    				    	temShips=temShips.substring(1,temShips.length());
    				    
    				    }
    			
    		}
    		
    	}
    	
    	
    }
    	if(temShips!=null && !temShips.equalsIgnoreCase("")){
    		listOfIdWithNoLine=getHibernateTemplate().find("select id from ServiceOrder where shipNumber in ("+temShips+")");
    	}
    	
    	return listOfIdWithNoLine;
}
	public List findRemoteRoutingList(String shipNumber) {
		return this.getSession().createSQLQuery("select id from servicepartner where shipNumber='"+shipNumber+"'  ").list();
	}

	public List routingListByGrpId(Long grpId, Long contId){
		return this.getSession().createSQLQuery("select c.id from servicepartner c  inner join serviceorder s  on s.shipNumber=c.shipNumber where s.grpID='"+grpId+"' and c.servicePartnerId='"+contId+"' c.status = true ").list();
	}
    public void updateRouting(String oldIdList){
    	getHibernateTemplate().bulkUpdate("update ServicePartner set servicePartnerId=null where servicePartnerId!=null and serviceOrderId in("+oldIdList+")");
    }

	public void deleteNetworkServicePartnerByParentId(Long parentId, String shipNumber) {
		getHibernateTemplate().bulkUpdate("update ServicePartner set status=false where servicePartnerId='" + parentId + "' and shipNumber='"+shipNumber+"'");
	}
	public List getMaxAtArrivalDate(Long soId, String sessionCorpID){
		return this.getSession().createSQLQuery("select id from servicepartner where serviceOrderId='"+soId+"'and corpID='"+sessionCorpID+"' and status = true  group by AtArrival desc limit 1 ").list();
	}
	public class PartnerDTO{
		 private String partnerCode;
		 private String firstName;
		 private String lastName;
		 private String billingCity;
		 private String billingCountryCode;
		 private String billingState;
		 private String aliasName;
		public String getPartnerCode() {
			return partnerCode;
		}
		public void setPartnerCode(String partnerCode) {
			this.partnerCode = partnerCode;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getBillingCity() {
			return billingCity;
		}
		public void setBillingCity(String billingCity) {
			this.billingCity = billingCity;
		}
		public String getBillingCountryCode() {
			return billingCountryCode;
		}
		public void setBillingCountryCode(String billingCountryCode) {
			this.billingCountryCode = billingCountryCode;
		}
		public String getBillingState() {
			return billingState;
		}
		public void setBillingState(String billingState) {
			this.billingState = billingState;
		}
		public String getAliasName() {
			return aliasName;
		}
		public void setAliasName(String aliasName) {
			this.aliasName = aliasName;
		}
	}
	public List getCarrierListvalue(String partnerCode,String partnerlastName,String partneraliasName,String partnerbillingCountryCode,String partnerbillingState,String corpID){
		List  listvalue=new ArrayList();
		/*String ASDF="select * from partnerpublic where  corpID='"+corpID+"' and isCarrier is true and status='Approved'" +
				"and (lastName like '%" + partnerlastName.replaceAll("'", "''") + "%' OR  firstName like '%"
							+ partnerlastName.replaceAll("'", "''") + "%' or aliasName like '%" + partnerlastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%'  AND ( aliasName like '%" + partneraliasName.replaceAll("'", "''") + "%' or lastName like '%" + partneraliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
							+ partnerbillingCountryCode.replaceAll("'", "''") + "%' AND terminalCountry like '%" + partnerbillingCountryCode.replaceAll("'", "''")
							+ "%' AND terminalState like '%" + partnerbillingState.replaceAll("'", "''") + "%'";*/
		
	
	List listvalue11=this.getSession().createSQLQuery("select partnerCode,firstName,lastName,aliasName,terminalCountryCode,terminalState,billingCity from partner where  corpID='"+corpID+"' and isCarrier is true and status='Approved'" +
				"and (lastName like '%" + partnerlastName.replaceAll("'", "''") + "%' OR  firstName like '%"
							+ partnerlastName.replaceAll("'", "''") + "%' or aliasName like '%" + partnerlastName.replaceAll("'", "''") + "%') AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%'  AND ( aliasName like '%" + partneraliasName.replaceAll("'", "''") + "%' or lastName like '%" + partneraliasName.replaceAll("'", "''") + "%') AND terminalCountryCode like '%"
							+ partnerbillingCountryCode.replaceAll("'", "''") + "%' AND terminalState like '%" + partnerbillingState.replaceAll("'", "''") + "%' ").list();
	if((listvalue11!=null)&&(!listvalue11.isEmpty())&&(listvalue11.get(0)!=null)){
		Iterator it=listvalue11.iterator();
		PartnerDTO PartnerPubDto=null;
		 while(it.hasNext())
		  {
			 Object [] row=(Object[])it.next();
			 PartnerPubDto=new PartnerDTO(); 
			 PartnerPubDto.setPartnerCode((String)(row[0]==null?"":row[0]));
			 PartnerPubDto.setFirstName((String)(row[1]==null?"":row[1]));
			 PartnerPubDto.setLastName((String)(row[2]==null?"":row[2]));
			 PartnerPubDto.setAliasName((String)(row[2]==null?"":row[3]));
			 PartnerPubDto.setBillingCountryCode((String)(row[2]==null?"":row[4]));
			 PartnerPubDto.setBillingState((String)(row[2]==null?"":row[5]));
			 PartnerPubDto.setBillingCity((String)(row[2]==null?"":row[6]));
			 listvalue.add(PartnerPubDto);
		  }
	}
	
	return listvalue;
	}
	public List getCarrierNameForAgent(String partnerCode,String corpID){
		return this.getSession().createSQLQuery("select lastName, status, companyDivision, if(terminalEmail is null , '' ,terminalEmail), if(trackingUrl is null , '' ,trackingUrl), paymentMethod,if(billingCurrency is null,' ',billingCurrency),if(isAgent is true,'T','F'),if(isAccount is true,'T','F'),if(typeOfVendor='Insurance','Y','N')  from partner where partnerCode='"+partnerCode+"' and corpID ='"+corpID+"' and status='Approved' and isCarrier = true").list();
	}
	public List<ServicePartner> findByCarriersID(Long id) {
		return getHibernateTemplate().find("from ServicePartner where  status=true and  partnerType='CR' AND serviceOrderId=?", id);
	}
	
	public void updateServicePartnerDetails(Long id,String fieldName,String fieldValue,String fieldName1,String fieldValue1,String tableName, String sessionCorpID, String userName){
		String query="";
		if((fieldValue==null || fieldValue.equals("null"))&&(fieldValue1==null || fieldValue1.equals("null"))){
			query="update "+tableName+" set "+fieldName+"=null,"+fieldName1+"=null,updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
		}else if((fieldValue!=null && !fieldValue.equals("null"))&&(fieldValue1==null || fieldValue1.equals("null"))){
			query="update "+tableName+" set "+fieldName+"='"+fieldValue.replaceAll("'", "\\\\\'")+"',"+fieldName1+"=null,updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
		}else if((fieldValue==null || fieldValue.equals("null"))&&(fieldValue1!=null && !fieldValue1.equals("null"))){
			query="update "+tableName+" set "+fieldName+"=null,"+fieldName1+"='"+fieldValue1.replaceAll("'", "\\\\\'")+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
		}else{
			query="update "+tableName+" set "+fieldName+"='"+fieldValue.replaceAll("'", "\\\\\'")+"',"+fieldName1+"='"+fieldValue1.replaceAll("'", "\\\\\'")+"',updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' ";
		}		
		getSession().createSQLQuery(query).executeUpdate();
	}

	public void updateStatus(Long id, String routingStatus, String updateRecords) {
		getHibernateTemplate().bulkUpdate("update ServicePartner set status="+routingStatus+" where id= "+id+"");
		if(updateRecords!=null && updateRecords.equalsIgnoreCase("updateAll")){
			getHibernateTemplate().bulkUpdate("update ServicePartner set status="+routingStatus+" where servicePartnerId= "+id+" ");	
		}
		
	}
	
	public List getCarrierName(String partnerCode,String corpID){
		return this.getSession().createSQLQuery("select lastName  from partner where partnerCode='"+partnerCode+"' and corpID ='"+corpID+"' and status='Approved' and isCarrier = true").list();
	}
	//13674 - Create dashboard
	public List<ServicePartner> findTransptDetailList(String shipNumber,String corpID) { 
		List list1 = new ArrayList();
		 List transptDetail = this.getSession().createSQLQuery("SELECT servicepartnerMin.carrierName AS servicepartner_carrierName,servicepartnerMin.carrierDeparture AS servicepartnerMin_carrierDeparture,servicepartnerMax.carrierArrival AS servicepartnerMax_carrierArrival,DATE_FORMAT(servicepartnerMin.atDepart,'%d-%b-%y') AS servicepartnerMin_atDepart,DATE_FORMAT(servicepartnerMax.atArrival,'%d-%b-%y') AS servicepartnerMax_atArrival FROM serviceorder serviceorder LEFT OUTER JOIN servicepartner servicepartnerMin ON serviceorder.id = servicepartnerMin.serviceOrderId and servicepartnerMin.status is true and servicepartnerMin.corpID ='"+corpID+"' AND servicepartnerMin.id = (select min(bb.id) from servicepartner bb where bb.serviceOrderId = servicepartnerMin.serviceOrderId and bb.status is true group by servicepartnerMin.serviceOrderId) LEFT OUTER JOIN servicepartner servicepartnerMax ON serviceorder.id = servicepartnerMax.serviceOrderId and servicepartnerMax.status is true AND servicepartnerMax.corpID ='"+corpID+"' AND servicepartnerMax.id = (select max(bb.id) from servicepartner bb where bb.serviceOrderId = servicepartnerMax.serviceOrderId and bb.status is true group by servicepartnerMax.serviceOrderId) WHERE serviceorder.corpID ='"+corpID+"' AND serviceorder.shipNumber='"+shipNumber+"'").list();
		 Iterator it=transptDetail.iterator();
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();
				SDTO dTO=new SDTO();
                dTO.setCarrierName(row[0]);
                dTO.setCarrierDeparture(row[1]);
				dTO.setCarrierArrival(row[2]);
				dTO.setAtDepart(row[3]);
				dTO.setAtArrival(row[4]);
				List pieceCount = this.getSession().createSQLQuery("select sum(pieces) from container where corpId='"+corpID+"' and shipNumber='"+shipNumber+"' and status is true").list();
				if(pieceCount!=null && !pieceCount.isEmpty() && pieceCount.get(0)!=null){
					dTO.setPieces(pieceCount.get(0).toString());
				}else{
					dTO.setPieces("");	
				}
				list1.add(dTO);
		       }
	

		return list1;
	} 
	
}
