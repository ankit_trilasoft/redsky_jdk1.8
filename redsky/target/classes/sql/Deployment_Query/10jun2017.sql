// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;


ALTER TABLE `redsky`.`accountprofile` ADD COLUMN vip bit(1) DEFAULT b'0' ;

ALTER TABLE `redsky`.`contract` ADD COLUMN `clientPostingGroup` VARCHAR(255) DEFAULT '' ;


ALTER TABLE `redsky`.`contract` ADD COLUMN `accountCode` VARCHAR(8) DEFAULT '' ,
ADD COLUMN `accountName` VARCHAR(255) DEFAULT '' AFTER `accountCode`;

ALTER TABLE `redsky`.`refquoteservices` ADD COLUMN `englishId` BIGINT(20) NULL  AFTER `destinationCountry` , ADD COLUMN `germanId` BIGINT(20) NULL  AFTER `englishId` , ADD COLUMN `frenchId` BIGINT(20) NULL  AFTER `germanId` , ADD COLUMN `dutchId` BIGINT(20) NULL  AFTER `frenchId` ;


ALTER TABLE `redsky`.`bookstorage` ADD COLUMN `oldStorage` VARCHAR(65) NULL  AFTER `warehouse` ;