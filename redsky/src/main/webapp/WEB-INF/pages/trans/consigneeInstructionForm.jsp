<%--
/**
 * Implementation of View that contains add and edit details.
 * This file represents the basic view on "Consignee Instruction" in Redsky.
 * @File Name	consigneeInstructionForm
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        22-Dec-2008
 * --%>

<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %> 
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
 <head>   
    <title><fmt:message key="consigneeInstructionDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='consigneeInstructionDetail.heading'/>"/>   

<script language="JavaScript">
 <sec-auth:authComponent componentId="module.script.form.agentScript">
 window.onload = function() { 
	trap();
	<sec-auth:authScript tableList="consigneeInstruction" formNameList="consigneeInstructionForm" transIdList='${serviceOrder.shipNumber}'>
	
	</sec-auth:authScript>
	
	}
	</sec-auth:authComponent>
	
	
<sec-auth:authComponent componentId="module.script.form.corpAccountScript">

	window.onload = function() { 
		trap();
	var elementsLen=document.forms['consigneeInstruction'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['consigneeInstruction'].elements[i].type=='text')
					{
						document.forms['consigneeInstruction'].elements[i].readOnly =true;
						document.forms['consigneeInstruction'].elements[i].className = 'input-textUpper';
						
					}
					else
					{
						document.forms['consigneeInstruction'].elements[i].disabled=true;
					}
					
						
			}
			
	}
</sec-auth:authComponent>

function trap() 
		  {
		  
		  if(document.images)
		    {
		    
		    	for(i=0;i<document.images.length;i++)
		      {
		      	
		        	if(document.images[i].src.indexOf('nav')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}
		      }
		    }
		  }
		  
		  function right(e) {
		//var msg = "Sorry, you don't have permission.";
		if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
		}
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
		}
		else return true;
		}
function consigneeInstructionFormValue(){
     var shipNum = document.forms['consigneeInstructionForm'].elements['shipNum'].value;
     var url="consigneeInstructionCode.html?ajax=1&decorator=simple&popup=true&shipNum =" + encodeURI(shipNum);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse5;
     http2.send(null);  
}   

function handleHttpResponse5()
     {
             if (http2.readyState == 4)
             {
                 var results = http2.responseText
                 results = results.trim();  
                 results = results.replace('[','');
                 results=results.replace(']',''); 
                 document.forms['consigneeInstructionForm'].elements['DaCode'].value = results;
              }
}

function consigneeInstructionFormAddValue(){    
     var shipNum = document.forms['consigneeInstructionForm'].elements['shipNum'].value;
     var val = document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consignmentInstructions'].value;
     var DaCode = document.forms['consigneeInstructionForm'].elements['DaCode'].value;
     var sACode = document.forms['consigneeInstructionForm'].elements['sACode'].value;
     
     if(val== 'NC'){
    	 var url="consigneeInstructionNCAddress.html?ajax=1&decorator=simple&popup=true&shipNum="+encodeURIComponent(shipNum);
	     http9.open("GET", url, true);
	     http9.onreadystatechange = handleHttpResponse555555;
	     http9.send(null);
	     }
	     else{
	     document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value = '';
	     document.forms['consigneeInstructionForm'].elements['consigneeInstruction.notification'].value = '';
	     }   
     
     if(DaCode!='' && DaCode!="null")
     {
     	if(val=='D' || val== 'S' || val== 'N' )
     	{
	     var url="consigneeInstructionJobAddress.html?ajax=1&decorator=simple&popup=true&DaCode =" + encodeURI(DaCode)+ "&shipNum=" + encodeURI(shipNum);
	     http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponse55;
	     http2.send(null);
	     }
	     else{
	     document.forms['consigneeInstructionForm'].elements['consigneeInstruction.notification'].value = '';
	     }
	    if(val== 'S')
	     {
	     var url="consigneeInstructionJobAddress.html?ajax=1&decorator=simple&popup=true&DaCode =" + encodeURI(DaCode)+ "&shipNum=" + encodeURI(shipNum);
	     http3.open("GET", url, true);
	     http3.onreadystatechange = handleHttpResponse555;
	     http3.send(null);
	     }
	     else{
	     document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value = '';
	     }
	     if(val== 'D')
	     {
	     var url="consigneeInstructionJobAddress.html?ajax=1&decorator=simple&popup=true&DaCode =" + encodeURI(DaCode)+ "&shipNum=" + encodeURI(shipNum);
	     http4.open("GET", url, true);
	     http4.onreadystatechange = handleHttpResponse5555;
	     http4.send(null);
	     }
	     else{
	     document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value = '';
	     }
	     if(val== 'N')
	     {
	     var url="consigneeInstructionJobAddress.html?ajax=1&decorator=simple&popup=true&DaCode =" + encodeURI(DaCode)+ "&shipNum=" + encodeURI(shipNum);
	     http5.open("GET", url, true);
	     http5.onreadystatechange = handleHttpResponse55555;
	     http5.send(null);
	     }
	     else{
	     document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value = '';
	     }
	}
	if(sACode!='' && sACode!="null")
     {
	if(val== 'SA'){
	     var url="consigneeDestinationSubAgentAddress.html?ajax=1&decorator=simple&popup=true&sACode =" + encodeURI(sACode)+ "&shipNum=" + encodeURI(shipNum);
	     http6.open("GET", url, true);
	     http6.onreadystatechange = handleHttpResponse007;
	     http6.send(null);
	     }
	     else{
	     document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value = '';
	     document.forms['consigneeInstructionForm'].elements['consigneeInstruction.notification'].value = '';
	     }
	     if(val== 'CD'){
	     var url="consigneeDestinationSubAgentAddress.html?ajax=1&decorator=simple&popup=true&sACode =" + encodeURI(sACode)+ "&shipNum=" + encodeURI(shipNum);
	     http8.open("GET", url, true);
	     http8.onreadystatechange = handleHttpResponse00007;
	     http8.send(null);
	     }
	     else{
	     document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value = '';
	     document.forms['consigneeInstructionForm'].elements['consigneeInstruction.notification'].value = '';
	     }
	     }
}

function handleHttpResponse55()
     {
             if (http2.readyState == 4)
             {
                 var results = http2.responseText
                 results = results.trim(); 
                 var res = results.split("~"); 
                 if(res[0] == '1')
                 {
                 res[0] ='';
                 }
                 else{
                 res[0]=res[0]+"\n";
                 }
                 if(res[1] == '1')
                 {
                 res[1] = '';
                 }
                 else{
                 res[1]=res[1]+"\n";
                 }
                 if(res[2] == '1')
                 {
                 res[2] = '';
                 }
                 else{
                 res[2]=res[2]+"\n";
                 }
                 if(res[3] == '1')
                 {
                 res[3] = '';
                 }
                 else{
                 res[3]=res[3]+"\n";
                 }
                 if(res[4] == '1')
                 {
                 res[4] = '';
                 }
                 else{
                 res[4]=res[4]+",";
                 }
                 if(res[5] == '1')
                 {
                 res[5] = '';
                 }
                 else{
                 res[5]="Phn : "+res[5];
                 }
                 if(res[6] == '1')
                 {
                 res[6] = '';
                 }
                 else{
                 res[6]=res[6]+",";
                 }
                 if(res[7] == '1')
                 {
                 res[7] = '';
                 }
                 else{
                 res[7]=res[7]+"\n";
                 }
                 if(res[8] == '1')
                 {
                 res[8] = '';
                 }
                 else{
                 res[8]=res[8]+"\t";
                 }
                 if(res[16] == '1')
                 {
                 res[16] = '';
                 }
                 else{
                 res[16]=res[16]+"\n";
                 }
                 if(res[17] == '1')
                 {
                 res[17] = '';
                 }
                 else{
                 res[17]=res[17]+",";
                 }
                 if(res[18] == '1')
                 {
                 res[18] = '';
                 }
                 else{
                 res[18]=res[18]+",";
                 }
                 if(res[20] == '1')
                 {
                 res[20] = '';
                 }
                 else{
                 res[20]=res[20]+",";
                 }
                 document.forms['consigneeInstructionForm'].elements['consigneeInstruction.notification'].value = res[0]+res[1]+res[2]+res[3]+res[17]+res[18]+res[20]+res[16]+res[5];
              }
}

function handleHttpResponse555()
     {
             if (http3.readyState == 4)
             {
                 var results = http3.responseText
                 results = results.trim();  
                 var res = results.split("~"); 
                 if(res[0] == '1')
                 {
                 res[0] ='';
                 }
                 else{
                 res[0]=res[0]+"\n";
                 }
                 if(res[1] == '1')
                 {
                 res[1] = '';
                 }
                 else{
                 res[1]=res[1]+"\n";
                 }
                 if(res[2] == '1')
                 {
                 res[2] = '';
                 }
                 else{
                 res[2]=res[2]+"\n";
                 }
                 if(res[3] == '1')
                 {
                 res[3] = '';
                 }
                 else{
                 res[3]=res[3]+"\n";
                 }
                 if(res[4] == '1')
                 {
                 res[4] = '';
                 }
                 else{
                 res[4]=res[4]+",";
                 }
                 if(res[5] == '1')
                 {
                 res[5] = '';
                 }
                 else{
                 res[5]="Phn : "+res[5];
                 }
                 if(res[6] == '1')
                 {
                 res[6] = '';
                 }
                 else{
                 res[6]=res[6]+"\n"+"c/o ";
                 }
                 if(res[7] == '1')
                 {
                 res[7] = '';
                 }
                 else{
                 res[7]=res[7]+' ';
                 }
                 if(res[8] == '1')
                 {
                 res[8] = '';
                 }
                 else{
                 res[8]=res[8]+"\t";
                 }
                 if(res[16] == '1')
                 {
                 res[16] = '';
                 }
                 else{
                 res[16]=res[16]+"\n";
                 }
                 if(res[17] == '1')
                 {
                 res[17] = '';
                 }
                 else{
                 res[17]=res[17]+",";
                 }
                 if(res[18] == '1')
                 {
                 res[18] = '';
                 }
                 else{
                 res[18]=res[18]+",";
                 }
                 if(res[20] == '1')
                 {
                 res[20] = '';
                 }
                 else{
                 res[20]=res[20]+",";
                 }
                 document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value=res[7]+res[6]+res[0]+res[1]+res[2]+res[3]+res[17]+res[18]+res[20]+res[16]+res[5];
              }
}

function handleHttpResponse5555()
     {
             if (http4.readyState == 4)
             {
                 var results = http4.responseText
                 results = results.trim();
                 var res = results.split("~"); 
                 if(res[0] == '1')
                 {
                 res[0] ='';
                 }
                 else{
                 res[0]=res[0]+"\n";
                 }
                 if(res[1] == '1')
                 {
                 res[1] = '';
                 }
                 else{
                 res[1]=res[1]+"\n";
                 }
                 if(res[2] == '1')
                 {
                 res[2] = '';
                 }
                 else{
                 res[2]=res[2]+"\n";
                 }
                 if(res[3] == '1')
                 {
                 res[3] = '';
                 }
                 else{
                 res[3]=res[3]+"\n";
                 }
                 if(res[4] == '1')
                 {
                 res[4] = '';
                 }
                 else{
                 res[4]=res[4]+",";
                 }
                 if(res[5] == '1')
                 {
                 res[5] = '';
                 }
                 else{
                 res[5]="Phn : "+res[5];
                 }
                 if(res[6] == '1')
                 {
                 res[6] = '';
                 }
                 else{
                 res[6]=res[6]+",";
                 }
                 if(res[7] == '1')
                 {
                 res[7] = '';
                 }
                 else{
                 res[7]=res[7]+",";
                 }
                 if(res[8] == '1')
                 {
                 res[8] = '';
                 }
                 else{
                 res[8]=res[8]+"\t";
                 }
                 if(res[16] == '1')
                 {
                 res[16] = '';
                 }
                 else{
                 res[16]=res[16]+"\n";
                 }
                 if(res[17] == '1')
                 {
                 res[17] = '';
                 }
                 else{
                 res[17]=res[17]+",";
                 }
                 if(res[18] == '1')
                 {
                 res[18] = '';
                 }
                 else{
                 res[18]=res[18]+",";
                 }
                 if(res[20] == '1')
                 {
                 res[20] = '';
                 }
                 else{
                 res[20]=res[20]+",";
                 }
                 document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value=res[0]+res[1]+res[2]+res[3]+res[17]+res[18]+res[20]+res[16]+res[5];
              }
}

function handleHttpResponse55555()
     {
             if (http5.readyState == 4)
             {
                 var results = http5.responseText
                 results = results.trim();  
                 var res = results.split("~"); 
                 if(res[0] == '1')
                 {
                 res[0] ='';
                 }
                 else{
                 res[0]=res[0]+"\n";
                 }
                 if(res[1] == '1')
                 {
                 res[1] = '';
                 }
                 else{
                 res[1]=res[1]+"\n";
                 }
                 if(res[2] == '1')
                 {
                 res[2] = '';
                 }
                 else{
                 res[2]=res[2]+"\n";
                 }
                 if(res[3] == '1')
                 {
                 res[3] = '';
                 }
                 else{
                 res[3]=res[3]+"\n";
                 }
                 if(res[4] == '1')
                 {
                 res[4] = '';
                 }
                 else{
                 res[4]=res[4]+",";
                 }
                 if(res[5] == '1')
                 {
                 res[5] = '';
                 }
                 else{
                 res[5]=res[5];
                 }
                 if(res[6] == '1')
                 {
                 res[6] = '';
                 }
                 else{
                 res[6]=res[6]+' ,';
                 }
                 if(res[7] == '1')
                 {
                 res[7] = '';
                 }
                 else{
                 res[7]=res[7]+' ';
                 }
                 if(res[8] == '1')
                 {
                 res[8] = '';
                 }
                 else{
                 res[8]=res[8]+" ";
                 }
                 if(res[9] == '1')
                 {
                 res[9] = '';
                 }
                 else{
                 res[9]=res[9]+" ";
                 }
                 if(res[10] == '1')
                 {
                 res[10] = '';
                 }
                 else{
                 res[10]=res[10]+",";
                 }
                 if(res[11] == '1')
                 {
                 res[11] = '';
                 }
                 else{
                 res[11]="Phn :"+res[11];
                 }
                 if(res[12] == '1')
                 {
                 res[12] = '';
                 }
                 else{
                 if(res[11] != ''){
                 res[12]=","+res[12]+"\n";
                 }
                 else{
                 res[12]="Phn :"+res[12];
                 }
                 }
                 if(res[13] == '1')
                 {
                 res[13] = '';
                 }
                 else{
                 if(res[11] == '' && res[12] == ''){
                 res[13]="Phn :" +res[13];
                 }
                 else{
                 res[13]="," +res[13];
                 }
                 }
                 if(res[14] == '1')
                 {
                 res[14] = '';
                 }
                 else{
                 res[14]=res[14]+"\n";
                 }
                 if(res[15] == '1')
                 {
                 res[15] = '';
                 }
                 else{
                 res[15]=res[15]+",";
                 }
                 if(res[19] == '1')
                 {
                 res[19] = '';
                 }
                 else{
                 res[19]=res[19]+",";
                 }
                 if(res[21] == '1')
                 {
                 res[21] = '';
                 }
                 else{
                 res[21]=res[21]+",";
                 }
                 document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value=res[7]+res[6]+res[8]+res[9]+res[10]+res[15]+res[19]+res[21]+res[14]+res[11]+res[12]+res[13];
              }
}
function handleHttpResponse555555()
{
        if (http9.readyState == 4)
        {
            var results = http9.responseText
            results = results.trim();  
            var res = results.split("~"); 
            if(res[0] == '1')
            {
            res[0] ='';
            }
            else{
            res[0]=res[0]+"\n";
            }
            if(res[1] == '1')
            {
            res[1] = '';
            }
            else{
            res[1]=res[1]+" ";
            }
            if(res[2] == '1')
            {
            res[2] = '';
            }
            else{
            res[2]=res[2]+"\n";
            }
            if(res[3] == '1')
            {
            res[3] = '';
            }
            else{
            res[3]=res[3]+"\n";
            }
            if(res[4] == '1')
            {
            res[4] = '';
            }
            else{
            res[4]=res[4]+",";
            }
            if(res[5] == '1')
            {
            res[5] = '';
            }
            else{
            	if(res[6]=='1' && res[7]=='1'){
            		res[5]="\nPhn: "+res[5];
            	}else{
            		res[5]="\nPhn: "+res[5]+',';
            	}
            }
            if(res[6] == '1')
            {
            res[6] = '';
            }
            else{
            	if(res[7]=='1' && res[5]==''){
            		res[6]="\nPhn: "+res[6]
            	}else{
            		res[6]=res[6]+',';
            	}
           }
            if(res[7] == '1')
            {
            res[7] = '';
            }
            else{
            	if(res[5]=='' && res[6]==''){
            		res[7]="\nPhn: "+res[7];
            	}else{
            		res[7]= res[7];
            	}
           }
            if(res[8] == '1')
            {
            res[8] = '';
            }
            else{
            res[8]=res[8]+" ";
            }
            if(res[9] == '1')
            {
            res[9] = '';
            }
            else{
            res[9]=res[9]+" ";
            }
            if(res[10] == '1')
            {
            res[10] = '';
            }
            else{
            res[10]=res[10]+",";
            }
            if(res[11] == '1')
            {
            res[11] = '';
            }
            else{
            res[11]=+res[11]+',';
            }
            document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value=res[1]+res[0]+res[2]+res[3]+res[4]+res[9]+res[10]+res[11]+res[8]+res[5]+res[6]+res[7];
            document.forms['consigneeInstructionForm'].elements['consigneeInstruction.notification'].value=res[1]+res[0]+res[2]+res[3]+res[4]+res[9]+res[10]+res[11]+res[8]+res[5]+res[6]+res[7];
        }
}
function handleHttpResponse007()
     {
             if (http6.readyState == 4)
             {
                 var results = http6.responseText
                 results = results.trim();  
                 var res = results.split("~"); 
                 if(res[0] == '1')
                 {
                 res[0] ='';
                 }
                 else{
                 res[0]=res[0]+"\n";
                 }
                 if(res[1] == '1')
                 {
                 res[1] = '';
                 }
                 else{
                 res[1]=res[1]+"\n";
                 }
                 if(res[2] == '1')
                 {
                 res[2] = '';
                 }
                 else{
                 res[2]=res[2]+"\n";
                 }
                 if(res[3] == '1')
                 {
                 res[3] = '';
                 }
                 else{
                 res[3]=res[3]+"\n";
                 }
                 if(res[4] == '1')
                 {
                 res[4] = '';
                 }
                 else{
                 res[4]=res[4]+"\n";
                 }
                 if(res[5] == '1')
                 {
                 res[5] = '';
                 }
                 else{
                 res[5]=res[5]+",";
                 }
                 if(res[6] == '1')
                 {
                 res[6] = '';
                 }
                 else{
                 res[6]=res[6]+",";
                 }
                 if(res[7] == '1')
                 {
                 res[7] = '';
                 }
                 else{
                 res[7]=res[7]+"\n";
                 }
                 if(res[8] == '1')
                 {
                 res[8] = '';
                 }
                 else{
                 res[8]="\n Phn : "+res[8];
                 }
                 if(res[11] == '1')
                 {
                 res[11] = '';
                 }
                 else{
                 res[11]=res[11]+",";
                 }
                 document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value = res[0]+res[1]+res[2]+res[3]+res[4]+res[5]+res[6]+res[11]+res[7]+res[8];
                 document.forms['consigneeInstructionForm'].elements['consigneeInstruction.notification'].value = res[0]+res[1]+res[2]+res[3]+res[4]+res[5]+res[6]+res[11]+res[7]+res[8];
              }
}
function handleHttpResponse00007()
     {
             if (http8.readyState == 4)
             {
                 var results = http8.responseText
                 results = results.trim();  
                 var res = results.split("~"); 
                 if(res[10] == '1')
                 {
                 res[10] = '';
                 }
                 else{
                 res[10]=res[10]+" ";
                 }
                 if(res[9] == '1')
                 {
                 res[9] = '';
                 }
                 else{
                 res[9]=res[9]+"\n"+"c/o ";
                 }
                 if(res[0] == '1')
                 {
                 res[0] ='';
                 }
                 else{
                 res[0]=res[0]+"\n";
                 }
                 if(res[1] == '1')
                 {
                 res[1] = '';
                 }
                 else{
                 res[1]=res[1]+"\n";
                 }
                 if(res[2] == '1')
                 {
                 res[2] = '';
                 }
                 else{
                 res[2]=res[2]+"\n";
                 }
                 if(res[3] == '1')
                 {
                 res[3] = '';
                 }
                 else{
                 res[3]=res[3]+"\n";
                 }
                 if(res[4] == '1')
                 {
                 res[4] = '';
                 }
                 else{
                 res[4]=res[4]+"\n";
                 }
                 if(res[5] == '1')
                 {
                 res[5] = '';
                 }
                 else{
                 res[5]=res[5]+",";
                 }
                 if(res[6] == '1')
                 {
                 res[6] = '';
                 }
                 else{
                 res[6]=res[6]+",";
                 }
                 if(res[7] == '1')
                 {
                 res[7] = '';
                 }
                 else{
                 res[7]=res[7]+"\n";
                 }
                 if(res[8] == '1')
                 {
                 res[8] = '';
                 }
                 else{
                 res[8]="Phn : "+res[8];
                 }
                 if(res[11] == '1')
                 {
                 res[11] = '';
                 }
                 else{
                 res[11]=res[11]+",";
                 }
                 document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value = res[10]+res[9]+res[0]+res[1]+res[2]+res[3]+res[4]+res[5]+res[6]+res[11]+res[7]+res[8];
                 document.forms['consigneeInstructionForm'].elements['consigneeInstruction.notification'].value = res[0]+res[1]+res[2]+res[3]+res[4]+res[5]+res[6]+res[11]+res[7]+res[8];
              }
}


function shipmentLocationAddValue(){    
       var osACode = document.forms['consigneeInstructionForm'].elements['osACode'].value;
       if(osACode!='' && osACode!="null")
        {
 	     var url="shipmentLocationAddress.html?ajax=1&decorator=simple&popup=true&osACode =" + encodeURI(osACode);
	     http7.open("GET", url, true);
	     http7.onreadystatechange = handleHttpResponse7777;
	     http7.send(null);
	     }
}

function handleHttpResponse7777()
     {
             if (http7.readyState == 4)
             {
                 var results = http7.responseText
                 results = results.trim();  
                 document.forms['consigneeInstructionForm'].elements['consigneeInstruction.shipmentLocation'].value = results;
              }
}





function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http555 = getHTTPObject();
  
 function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1();
    
    
    function getHTTPObject2()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject2();
    
    function getHTTPObject3()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http5 = getHTTPObject3();
  
  
  function getHTTPObject6()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http6 = getHTTPObject6();
  
   function getHTTPObject7()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http7 = getHTTPObject7();  
    
  function getHTTPObject8()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http8 = getHTTPObject8(); 
    
    function getHTTPObject9()
    {
        var xmlhttp;
        if(window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else if (window.ActiveXObject)
        {
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            if (!xmlhttp)
            {
                xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
            }
        }
        return xmlhttp;
    }
        var http9 = getHTTPObject9();
    
// function for validations.
var form_submitted = false;
function submit_form()
{
  if (form_submitted)
  {
    alert ("Your form has already been submitted. Please wait...");
    return false;
  }
  else
  {
    form_submitted = true;
    return true;
  }
}

  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['consigneeInstructionForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['consigneeInstructionForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['consigneeInstructionForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['consigneeInstructionForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'editConsignee.html?sid='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['consigneeInstructionForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['consigneeInstructionForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
function goToUrl(id)
	{
		location.href = "editConsignee.html?sid="+id;
	}
	
function checkLengthLocation(){
	var txt1 = document.forms['consigneeInstructionForm'].elements['consigneeInstruction.shipmentLocation'].value;
	if(txt1.length >1000){
		alert("Cannot enter more than 1000 characters in Shipment Location.");
		document.forms['consigneeInstructionForm'].elements['consigneeInstruction.shipmentLocation'].value = txt1.substring(0,1000);
		return false;
	}else{
		return true;
	}
}   	

function checkLengthAddress(){
	var txt1 = document.forms['consigneeInstructionForm'].elements['consigneeInstruction.shipperAddress'].value;
	if(txt1.length >1000){
		alert("Cannot enter more than 1000 characters in Shipper.");
		document.forms['consigneeInstructionForm'].elements['consigneeInstruction.shipperAddress'].value = txt1.substring(0,1000);
		return false;
	}else{
		return true;
	}
} 

function checkLengthConsignee(){
	var txt1 = document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value;	
	if(txt1.length >1000){
		alert("Cannot enter more than 1000 characters in Consignee.");
		document.forms['consigneeInstructionForm'].elements['consigneeInstruction.consigneeAddress'].value = txt1.substring(0,1000);
		return false;
	}else{
		return true;
	}	
} 

function checkLengthNotify(){
	var txt1 = document.forms['consigneeInstructionForm'].elements['consigneeInstruction.notification'].value;	
	if(txt1.length >1000){
		alert("Cannot enter more than 1000 characters in Notify.");
		document.forms['consigneeInstructionForm'].elements['consigneeInstruction.notification'].value = txt1.substring(0,1000);
		return false;
	}else{
		return true;
	}	
} 

function checkLengthSpecialInstBol(){
	var txt1 = document.forms['consigneeInstructionForm'].elements['consigneeInstruction.specialInstructions'].value;	
	if(txt1.length >1000){
		alert("Cannot enter more than 1000 characters in Special Instructions for OBL.");
		document.forms['consigneeInstructionForm'].elements['consigneeInstruction.specialInstructions'].value = txt1.substring(0,1000);
		return false;
	}else{
		return true;
	}	
} 


</script>
<style type="text/css">

/* collapse */
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>

</head>   
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="consigneeInstructionForm" action="saveConsignee" method="post" validate="true">   
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="id" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="consigneeInstruction.id"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="serviceOrder.sid" value="%{serviceOrder.sid}"/>
<s:hidden name="soId" value="%{serviceOrder.sid}"/>
<c:set var="shipNum" value="${serviceOrder.shipNumber}"/>
<s:hidden name="shipNum" value="${serviceOrder.shipNumber}"/>
<s:hidden name="consigneeInstruction.serviceOrderId" value="%{serviceOrder.id}"/>
<s:hidden name="consigneeInstruction.corpID" />
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.bookingAgentCode"/>
<s:hidden name="oaCode"/>
<s:hidden name="DaCode"/>
<s:hidden name="sACode"/>
<s:hidden name="osACode"/>
<s:hidden name="serviceOrder.ship"/> 
<s:hidden name="customerFile.id" />
<s:hidden name="serviceOrder.id" /> 
<div id="Layer1" style="width:100%">
<div id="newmnav" style="float:left">
  <ul>
  <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
   <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a></li>
   </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.billingTab">
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
  	<li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
  </sec-auth:authComponent>
  </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.accountingTab">
  <c:choose>
	<%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
	   <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
	</c:when> --%>
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
		<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	</c:otherwise>
  </c:choose> 
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
  <c:choose> 
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
		<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	</c:otherwise>
  </c:choose>
  </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>  
  <sec-auth:authComponent componentId="module.tab.container.forwardingTab">
  <li id="newmnav1" style="background:#FFF "><a href="containers.html?id=${serviceOrder.id}" class="current" ><span>Forwarding</span></a></li>
   </sec-auth:authComponent>
    <sec-auth:authComponent componentId="module.tab.serviceorder.accountingPortalTab">	
	   <li><a href="accountLineSalesPortalList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	</sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.domesticTab">
  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
        <c:if test="${serviceOrder.job =='INT'}">
         <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
        </c:if>
   </sec-auth:authComponent>     
  <sec-auth:authComponent componentId="module.tab.container.statusTab">
     <c:if test="${serviceOrder.job =='RLO'}"> 
	 <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
</c:if>
<c:if test="${serviceOrder.job !='RLO'}"> 
		<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
</c:if>
  </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.ticketTab">
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
  </sec-auth:authComponent>
   <configByCorp:fieldVisibility componentId="component.standard.claimTab">
  <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
   <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
   </sec-auth:authComponent>
   </configByCorp:fieldVisibility>
      <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
			      <c:if test="${ usertype=='AGENT' && surveyTab}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			</c:if>
			</sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.customerFileTab">
  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
  </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.reportTab">
  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Container&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
     <li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
</sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
    <li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
</sec-auth:authComponent>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float:left;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if></tr></table>
<div class="spn">&nbsp;</div>



    <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
 <div id="Layer4" style="width:100%;">
 <div id="newmnav">   
 <ul>
  <li><a  href="containers.html?id=${serviceOrder.id}"><span>SS Container</span></a></li>
  <li><a  href="cartons.html?id=${serviceOrder.id}" ><span>Piece Count</span></a></li>
  <li><a  href="vehicles.html?id=${serviceOrder.id}" ><span>Vehicle</span></a></li>
  <li><a href="servicePartnerss.html?id=${serviceOrder.id}"><span>Routing</span></a></li>
  <li id="newmnav1" style="background:#FFF "><a href="editConsignee.html?sid=${serviceOrder.id}"  class="current"><span>Consignee Instructions</span></a></li>
    <sec-auth:authComponent componentId="module.tab.container.auditTab">
    <li>
    <a onclick="window.open('auditList.html?id=${consigneeInstruction.id}&tableName=consigneeInstruction&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')">
    <span>Audit</span></a></li>
    </sec-auth:authComponent>
  <c:if test="${countBondedGoods >= 0}" >
  <li><a href="customs.html?id=${serviceOrder.id}"><span>Custom</span></a></li>
  </c:if>
  
  </ul>
</div><div class="spn">&nbsp;</div>

</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
				<tbody>
					<tr>
                     <tr>
                     <td align="right" class="listwhitetext">Consignment&nbsp;Instructions</td>
                    
									<td align="left" style="width:100px"><s:select
										cssClass="list-menu" name="consigneeInstruction.consignmentInstructions"
										list="%{specific}" cssStyle="width:307px" onchange="consigneeInstructionFormAddValue();shipmentLocationAddValue()" tabindex="1"/></td>
                       <td colspan="5">
                       <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
				        <tr> 
				        <td  class="listwhitetext" align="right"  style="width:20px;"></td>	
				         <td  class="listwhitetext" align="right" >Express&nbsp;Release</td>	  	   		
	  			         <td align="left"  width="50px"><s:checkbox name="consigneeInstruction.expressRelease" value="${consigneeInstruction.expressRelease}" fieldValue="true" tabindex="2"/></td>
				         <td  class="listwhitetext" align="right" >Collect</td>	  	   		
	  			         <td ><s:checkbox name="consigneeInstruction.collect" value="${consigneeInstruction.collect}" fieldValue="true" tabindex="3"/></td>
				        </tr>
				        </table>
				      </td>
                     </tr>
                     
                     <tr>
                     <td align="right"  class="listwhitetext" >Shipper</td>
                     <td align="left" colspan="1" class="listwhitetext"><s:textarea name="consigneeInstruction.shipperAddress"  rows="4" cols="52" cssClass="textarea" tabindex="4" onkeypress="return checkLengthAddress();"/></td>
                     <td align="right"  class="listwhitetext" style="width:115px;">Shipment Location</td>
                     <td align="left" colspan="4" class="listwhitetext"><s:textarea name="consigneeInstruction.shipmentLocation"  rows="4" cols="41" tabindex="5" cssClass="textarea" onkeypress="return checkLengthLocation();"/></td>
                     
                     </tr>
                     
                     <tr>
                     <td align="right"  class="listwhitetext">Consignee</td>
                     <td align="left" colspan="1" class="listwhitetext"><s:textarea name="consigneeInstruction.consigneeAddress"  rows="6" cols="52" tabindex="6" cssClass="textarea" onkeypress="return checkLengthConsignee();"/></td>
                     <td align="right"  class="listwhitetext">Notify</td>
                     <td align="left" colspan="4" class="listwhitetext"><s:textarea name="consigneeInstruction.notification"  rows="6" cols="41" tabindex="7" cssClass="textarea" onkeypress="return checkLengthNotify();"/></td>
                 
                     </tr>
                     <tr>
                     <td align="right"  class="listwhitetext">Special&nbsp;Instructions&nbsp;for&nbsp;OBL</td>
                     <td align="left" colspan="1" class="listwhitetext"><s:textarea name="consigneeInstruction.specialInstructions"  rows="2" cols="52" tabindex="8"  cssClass="textarea" onkeypress="return checkLengthSpecialInstBol();" /></td>
                     </tr>		
										</tbody>
									</table>
									</td>									
								</tr>
</tbody>
</table>						
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>	
			<table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="containerCreatedOnFormattedValue" value="${consigneeInstruction.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="consigneeInstruction.createdOn" value="${containerCreatedOnFormattedValue}" />
						<td><fmt:formatDate value="${consigneeInstruction.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></td>
						
						<c:if test="${not empty consigneeInstruction.id}">
								<s:hidden name="consigneeInstruction.createdBy"/>
								<td><s:label name="createdBy" value="%{consigneeInstruction.createdBy}"/></td>
							</c:if>
							<c:if test="${empty consigneeInstruction.id}">
								<s:hidden name="consigneeInstruction.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn'/></td>
						<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${consigneeInstruction.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="consigneeInstruction.updatedOn" value="${containerUpdatedOnFormattedValue}" />
						<td><fmt:formatDate value="${consigneeInstruction.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
						<c:if test="${not empty consigneeInstruction.id}">
							<s:hidden name="consigneeInstruction.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{consigneeInstruction.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty consigneeInstruction.id}">
							<s:hidden name="consigneeInstruction.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
				
<sec-auth:authComponent componentId="module.button.consignee.saveButton">														              
<s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" tabindex="9" />   
<s:reset cssClass="cssbutton1" key="Reset" cssStyle="width:55px; height:25px" tabindex="10"/>
</sec-auth:authComponent>
</div>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
 </s:form>   
  
<script type="text/javascript">
	function navigationVal(){
		if (window.addEventListener)
			 window.addEventListener("load", function(){
				 var imgList = document.getElementsByTagName('img');
				 for(var i = 0 ; i < imgList.length ; i++){
					if(imgList[i].src.indexOf('images/nav') > 1){
						imgList[i].style.display = 'none';
					}
				 }
			 }, false)
	}
    try{
    consigneeInstructionFormValue();
   }
   catch(e){}
   <sec-auth:authComponent componentId="module.script.form.corpSalesScript">
   navigationVal();
 	window.onload = function() { 	 		
 			var elementsLen=document.forms['consigneeInstructionForm'].elements.length;
 			for(i=0;i<=elementsLen-1;i++){
 				if(document.forms['consigneeInstructionForm'].elements[i].type=='text'){
 						document.forms['consigneeInstructionForm'].elements[i].readOnly =true;
 						document.forms['consigneeInstructionForm'].elements[i].className = 'input-textUpper';
 					}else{
 						document.forms['consigneeInstructionForm'].elements[i].disabled=true;
 					}
 			}
 	}
 </sec-auth:authComponent>
</script>  
