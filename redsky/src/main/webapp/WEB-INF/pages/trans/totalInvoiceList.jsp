<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="reverseInvoiceList.title"/></title>   
    <meta name="heading" content="<fmt:message key='reverseInvoiceList.heading'/>"/> 
    <style type="text/css"> 

.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot td {
border:1px solid #E0E0E0;
}
.table2 thead th, table2HeaderTable td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
border-style:solid;
border-right:medium;
color:#99BBE8;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

</style> 
<style type="text/css">h2 {background-color: #FBBFFF}</style>  
</head>
<c:if test="${totalInvoiceList!='[]'}"> 
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left"><b>Invoice List</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
<s:form id="totalInvoiceList" action="" method="post"> 
<s:hidden name="systemDefaultmiscVl" value="%{systemDefaultmiscVl}" /> 
<s:set name="totalInvoiceList" value="totalInvoiceList" scope="request"/> 
<display:table name="totalInvoiceList" class="table" requestURI="" id="totalInvoiceList" style="width:200px" defaultsort="1" pagesize="100" >   
 		 <display:column property="recInvoiceNumber" headerClass="headerColorInv" title="Invoice#" style="width:40px" />
 		 <display:column property="baseCurrency"  title="Curr." style="width:40px" /> 
 		 <display:column property="billToName"  title="BillToName" style="width:40px" maxLength="7"/> 
 		 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}">
		 <display:column   title="Total" headerClass="containeralign"   style="width:60px">  
		      <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
             groupingUsed="true" value="${totalInvoiceList.actualRevenueSum}" /></div>
         </display:column>
         </c:if>
         <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
         <display:column   title="Total" headerClass="containeralign"   style="width:60px">  
		      <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
             groupingUsed="true" value="${totalInvoiceList.distributionAmountSumInv}" /></div>
         </display:column>
         </c:if>
          <display:column title="Amount&nbsp;Paid" style="width:40px">
	          <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
		             groupingUsed="true" value="${totalInvoiceList.amountPaid}" /></div>
		  </display:column> 
          <display:column title="Amount&nbsp;Pending" style="width:40px">
	          <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
	             groupingUsed="true" value="${totalInvoiceList.amountPending}" /></div>
          </display:column>   
</display:table>  
</s:form>
</c:if>
<c:if test="${totalInvoiceList=='[]'}">
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left"><b>No Invoice List found</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
</c:if>
		  	