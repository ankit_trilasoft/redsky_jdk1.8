// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`billing` ADD COLUMN contractSystem varchar(30) DEFAULT '';

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN packingDays varchar(25) DEFAULT '';

ALTER TABLE `redsky`.`company` ADD COLUMN `restrictAccess` BIT(1) NULL DEFAULT b'0'  AFTER `status` ;

ALTER TABLE `redsky`.`company` ADD COLUMN `extractedFileAudit` bit(1) DEFAULT b'0';
update company set extractedFileAudit=false;

CREATE  TABLE `redsky`.`extractedfilelog` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `corpID` VARCHAR(10) NULL ,
  `fileName` VARCHAR(375) NULL ,
  `location` varchar(375) NULL,
 `createdBy` VARCHAR(80) NULL ,
  `createdOn` DATETIME NULL ,
 PRIMARY KEY (`id`) );
 
 ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `rutTaxNumber` VARCHAR(25) NULL  AFTER `compensationYear` ;
 
 ALTER TABLE `redsky`.`extractedfilelog` ADD COLUMN `module` VARCHAR(30) NULL ;
 
 
 ALTER TABLE `redsky`.`company` ADD COLUMN `restrictedMassage` VARCHAR(255) NULL  AFTER `extractedFileAudit` ;
 
 ALTER TABLE `redsky`.`partnerprivate` CHANGE COLUMN `rutTaxNumber` `rutTaxNumber` VARCHAR(30) NULL DEFAULT NULL  ;
 
 ALTER TABLE `redsky`.`inventorydata` CHANGE COLUMN `mode` `mode` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL  ;
