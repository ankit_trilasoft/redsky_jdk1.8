package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.SQLExtract;

public interface SQLExtractManager extends GenericManager<SQLExtract, Long> {

	public String testSQLQuery(String sqlQuery);

	public List getAllList(String sessionCorpID);
	public List getAllUserList(String sessionCorpID);
	public List sqlDataExtract(String sqlText);

	public List getAllCorpId();

	public List  getUniquename(String fileName);

	public List getSearch(String fileName, String corpID);

	public List getSchedulerQueryList();

	public List getSqlExtraxtSearch(String fileName, String description, String sessionCorpID);

	public List getAllSqlExtractFiles();

	public List getAllListForTSFT();
	
	public List getSqlExtractByID(Long id,String sessionCorpID );
	
	public int deleteSqlExtract(Long id);
}
