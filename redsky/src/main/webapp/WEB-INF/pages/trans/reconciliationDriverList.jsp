<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>


<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>  
    <script type="text/javascript">
    function setParentValue(charge,description){
    	//alert("hiii");
    	//alert(charge);
    	//alert(description);
    	window.opener.document.forms['vanLineSettForm'].elements['OwnerCode'].value=charge;
   
    	 self.close();
    }

    function clear_fields(){
		document.forms['partnerListForm'].elements['partner.lastName'].value = "";
		document.forms['partnerListForm'].elements['partner.partnerCode'].value = "";
		document.forms['partnerListForm'].elements['partner.aliasName'].value = "";	 
			document.forms['partnerListForm'].elements['partner.billingCountryCode'].value = "";
			document.forms['partnerListForm'].elements['partner.billingState'].value = "";
			document.forms['partnerListForm'].elements['partner.billingCountry'].value = "";
		
		
}
    </script>
    
    </head>
    <c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" onclick="" cssStyle="width:55px; height:25px" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
   </c:set> 
    <s:form id="partnerListForm" onsubmit="return resetOriginCode();" action='reconciliationDriverSearch.html?partnerType=OO&decorator=popup&popup=true&search=YES' method="post" >  
    <div id="layer1" style="width:100%;">
<div id="otabs">
  <ul>
    <li><a class="current"><span>Search</span></a></li>
  </ul>
</div>
<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" border="0" style="width:100%;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th><fmt:message key="partner.name"/></th>
<th>Alias Name</th>
<th>Country Code</th>
<th>Country Name</th>
<th><fmt:message key="partner.billingState"/></th>

</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partner.partnerCode" size="15" cssClass="input-text"/>
			</td>
			<td>
			    <s:textfield name="partner.lastName" size="15" cssClass="input-text" />
			</td>
			<td>
			    <s:textfield name="partner.aliasName" size="15" cssClass="input-text" />
			</td>	
		
				<td>
				    <s:textfield name="partner.billingCountryCode" size="15" cssClass="input-text"/>
				</td>
				<td>
				    <s:textfield name="partner.billingCountry" size="22" cssClass="input-text"/>
				</td>
				<td>
				    <s:textfield name="partner.billingState" size="22" cssClass="input-text"/>
				</td>
			
		</tr>
		<tr>
			<td colspan="5"></td>
			<td width="130px" style="border-left: hidden;text-align:right;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	</div>
    
   <display:table name="partners" class="table" requestURI="" id="partnerList" export="${empty param.popup}" defaultsort="2" pagesize="10" style="width:100%;margin-top:11px;" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' > 
     
       	<display:column  sortable="true" titleKey="partner.partnerCode">
    	<a href="" onclick="setParentValue('${partnerList.partnerCode}','${partnerList.lastName}');"><c:out value="${partnerList.partnerCode}"/></a>
    	 </display:column>  
     
    <display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
     <display:column  title="Alias Name" sortable="true" style="width:390px"><c:out value="${partnerList.aliasName}" /></display:column>
     
     <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.billingAddress1}','${partnerList.billingAddress2}','${partnerList.billingCity}','${partnerList.billingZip}','${partnerList.billingState}','${partnerList.billingCountry}');"/></a></display:column>  
    <display:column title="Acct Ref #" sortable="true" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerList.partnerCode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    	</display:column>
    	<display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    </display:table>
    
    </s:form>