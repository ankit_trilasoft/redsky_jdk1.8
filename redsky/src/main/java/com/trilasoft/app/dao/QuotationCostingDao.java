package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.QuotationCosting;

public interface QuotationCostingDao extends GenericDao<QuotationCosting, Long> {   
      public List checkById(Long id);
      public List<QuotationCosting> findBySequenceNumber(String sequenceNumber);
   }