package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="accessinfo")
public class AccessInfo {
	private String originResidenceType;
	private String originResidenceSize;
	private Boolean originLongCarry;
	private String originCarryDistance;
	private String originCarrydetails;
	private Boolean originStairCarry;
	private String originStairDistance;
	private String originStairdetails;
	private String originfloor;
	private Boolean originNeedCrane;
	private Boolean originElevator;
	private String originElevatorDetails;
	private Boolean originExternalElevator;   
	private String originElevatorType;
	private String originParkingType;
	private String originParkinglotsize;
	private String originParkingSpots;
	private String originDistanceToParking; 
	private String originShuttleDistance;
	private Boolean originReserveParking;
	private Boolean originAdditionalStopRequired;
	private Boolean originShuttleRequired;
	private Boolean destinationLongCarry; 
	private Boolean destinationStairCarry; 
	private Boolean destinationNeedCrane; 
	private Boolean destinationElevator; 
	private Boolean destinationExternalElevator; 
	private Boolean destinationReserveParking; 
	private Boolean destinationShuttleRequired; 
	private Boolean destinationAdditionalStopRequired; 
	private String originAdditionalStopDetails;
	private String destinationResidenceType;
	private String destinationResidenceSize;
	private String destinationCarryDistance;
	private String destinationCarrydetails;
	private String destinationStairDistance;
	private String destinationStairdetails;
	private String destinationfloor;
	private String destinationElevatorDetails;
	private String destinationElevatorType;
	private String destinationParkingType;
	private String destinationParkinglotsize;
	private String destinationParkingSpots;
	private String destinationDistanceToParking;
	private String destinationShuttleDistance;
	private String destinationAdditionalStopDetails;
	private Long id;
	private String corpID;
	private String originComment;
	private String destinationComment;
	private Long serviceOrderId;
	private Long customerFileId;
	private Long networkSynchedId;
	private Boolean inventory;
	private Boolean labels;
	private Boolean typeofPacking;
	private Boolean packMaterials;
	private Boolean tools;
	private Boolean piano;
	private Boolean carMotorbike;
	private Boolean crating;
	private String remarks;
	/*
	 * Bug 13896 - Add created on, created by, updated on and updated by in access info
	 */
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("originResidenceType", originResidenceType)
				.append("originResidenceSize", originResidenceSize)
				.append("originLongCarry", originLongCarry)
				.append("originCarryDistance", originCarryDistance)
				.append("originCarrydetails", originCarrydetails)
				.append("originStairCarry", originStairCarry)
				.append("originStairDistance", originStairDistance)
				.append("originStairdetails", originStairdetails)
				.append("originfloor", originfloor)
				.append("originNeedCrane", originNeedCrane)
				.append("originElevator", originElevator)
				.append("originElevatorDetails", originElevatorDetails)
				.append("originExternalElevator", originExternalElevator)
				.append("originElevatorType", originElevatorType)
				.append("originParkingType", originParkingType)
				.append("originParkinglotsize", originParkinglotsize)
				.append("originParkingSpots", originParkingSpots)
				.append("originDistanceToParking", originDistanceToParking)
				.append("originShuttleDistance", originShuttleDistance)
				.append("originReserveParking", originReserveParking)
				.append("originAdditionalStopRequired",
						originAdditionalStopRequired)
				.append("originShuttleRequired", originShuttleRequired)
				.append("destinationLongCarry", destinationLongCarry)
				.append("destinationStairCarry", destinationStairCarry)
				.append("destinationNeedCrane", destinationNeedCrane)
				.append("destinationElevator", destinationElevator)
				.append("destinationExternalElevator",
						destinationExternalElevator)
				.append("destinationReserveParking", destinationReserveParking)
				.append("destinationShuttleRequired",
						destinationShuttleRequired)
				.append("destinationAdditionalStopRequired",
						destinationAdditionalStopRequired)
				.append("originAdditionalStopDetails",
						originAdditionalStopDetails)
				.append("destinationResidenceType", destinationResidenceType)
				.append("destinationResidenceSize", destinationResidenceSize)
				.append("destinationCarryDistance", destinationCarryDistance)
				.append("destinationCarrydetails", destinationCarrydetails)
				.append("destinationStairDistance", destinationStairDistance)
				.append("destinationStairdetails", destinationStairdetails)
				.append("destinationfloor", destinationfloor)
				.append("destinationElevatorDetails",
						destinationElevatorDetails)
				.append("destinationElevatorType", destinationElevatorType)
				.append("destinationParkingType", destinationParkingType)
				.append("destinationParkinglotsize", destinationParkinglotsize)
				.append("destinationParkingSpots", destinationParkingSpots)
				.append("destinationDistanceToParking",
						destinationDistanceToParking)
				.append("destinationShuttleDistance",
						destinationShuttleDistance)
				.append("destinationAdditionalStopDetails",
						destinationAdditionalStopDetails).append("id", id)
				.append("corpID", corpID)
				.append("originComment", originComment)
				.append("destinationComment", destinationComment)
				.append("serviceOrderId", serviceOrderId)
				.append("customerFileId", customerFileId)
				.append("networkSynchedId", networkSynchedId)
				.append("inventory",inventory)
				.append("labels",labels)
				.append("typeofPacking",typeofPacking)
				.append("packMaterials",packMaterials)
				.append("tools",tools)
				.append("piano",piano)
				.append("carMotorbike",carMotorbike)
				.append("crating",crating)
				.append("remarks",remarks)
				.append("createdOn", createdOn)
				.append("createdBy", createdBy)
				.append("updatedOn", updatedOn)
				.append("updatedBy", updatedBy).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AccessInfo))
			return false;
		AccessInfo castOther = (AccessInfo) other;
		return new EqualsBuilder()
				.append(originResidenceType, castOther.originResidenceType)
				.append(originResidenceSize, castOther.originResidenceSize)
				.append(originLongCarry, castOther.originLongCarry)
				.append(originCarryDistance, castOther.originCarryDistance)
				.append(originCarrydetails, castOther.originCarrydetails)
				.append(originStairCarry, castOther.originStairCarry)
				.append(originStairDistance, castOther.originStairDistance)
				.append(originStairdetails, castOther.originStairdetails)
				.append(originfloor, castOther.originfloor)
				.append(originNeedCrane, castOther.originNeedCrane)
				.append(originElevator, castOther.originElevator)
				.append(originElevatorDetails, castOther.originElevatorDetails)
				.append(originExternalElevator,
						castOther.originExternalElevator)
				.append(originElevatorType, castOther.originElevatorType)
				.append(originParkingType, castOther.originParkingType)
				.append(originParkinglotsize, castOther.originParkinglotsize)
				.append(originParkingSpots, castOther.originParkingSpots)
				.append(originDistanceToParking,
						castOther.originDistanceToParking)
				.append(originShuttleDistance, castOther.originShuttleDistance)
				.append(originReserveParking, castOther.originReserveParking)
				.append(originAdditionalStopRequired,
						castOther.originAdditionalStopRequired)
				.append(originShuttleRequired, castOther.originShuttleRequired)
				.append(destinationLongCarry, castOther.destinationLongCarry)
				.append(destinationStairCarry, castOther.destinationStairCarry)
				.append(destinationNeedCrane, castOther.destinationNeedCrane)
				.append(destinationElevator, castOther.destinationElevator)
				.append(destinationExternalElevator,
						castOther.destinationExternalElevator)
				.append(destinationReserveParking,
						castOther.destinationReserveParking)
				.append(destinationShuttleRequired,
						castOther.destinationShuttleRequired)
				.append(destinationAdditionalStopRequired,
						castOther.destinationAdditionalStopRequired)
				.append(originAdditionalStopDetails,
						castOther.originAdditionalStopDetails)
				.append(destinationResidenceType,
						castOther.destinationResidenceType)
				.append(destinationResidenceSize,
						castOther.destinationResidenceSize)
				.append(destinationCarryDistance,
						castOther.destinationCarryDistance)
				.append(destinationCarrydetails,
						castOther.destinationCarrydetails)
				.append(destinationStairDistance,
						castOther.destinationStairDistance)
				.append(destinationStairdetails,
						castOther.destinationStairdetails)
				.append(destinationfloor, castOther.destinationfloor)
				.append(destinationElevatorDetails,
						castOther.destinationElevatorDetails)
				.append(destinationElevatorType,
						castOther.destinationElevatorType)
				.append(destinationParkingType,
						castOther.destinationParkingType)
				.append(destinationParkinglotsize,
						castOther.destinationParkinglotsize)
				.append(destinationParkingSpots,
						castOther.destinationParkingSpots)
				.append(destinationDistanceToParking,
						castOther.destinationDistanceToParking)
				.append(destinationShuttleDistance,
						castOther.destinationShuttleDistance)
				.append(destinationAdditionalStopDetails,
						castOther.destinationAdditionalStopDetails)
				.append(id, castOther.id).append(corpID, castOther.corpID)
				.append(originComment, castOther.originComment)
				.append(destinationComment, castOther.destinationComment)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(customerFileId, castOther.customerFileId)
				.append(networkSynchedId, castOther.networkSynchedId)
				.append(inventory,castOther.inventory)
				.append(labels,castOther.labels)
				.append(typeofPacking,castOther.typeofPacking)
				.append(packMaterials,castOther.packMaterials)
				.append(tools,castOther.tools)
				.append(piano,castOther.piano)
				.append(carMotorbike,castOther.carMotorbike)
				.append(crating,castOther.crating)
				.append(remarks,castOther.remarks)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(originResidenceType)
				.append(originResidenceSize).append(originLongCarry)
				.append(originCarryDistance).append(originCarrydetails)
				.append(originStairCarry).append(originStairDistance)
				.append(originStairdetails).append(originfloor)
				.append(originNeedCrane).append(originElevator)
				.append(originElevatorDetails).append(originExternalElevator)
				.append(originElevatorType).append(originParkingType)
				.append(originParkinglotsize).append(originParkingSpots)
				.append(originDistanceToParking).append(originShuttleDistance)
				.append(originReserveParking)
				.append(originAdditionalStopRequired)
				.append(originShuttleRequired).append(destinationLongCarry)
				.append(destinationStairCarry).append(destinationNeedCrane)
				.append(destinationElevator)
				.append(destinationExternalElevator)
				.append(destinationReserveParking)
				.append(destinationShuttleRequired)
				.append(destinationAdditionalStopRequired)
				.append(originAdditionalStopDetails)
				.append(destinationResidenceType)
				.append(destinationResidenceSize)
				.append(destinationCarryDistance)
				.append(destinationCarrydetails)
				.append(destinationStairDistance)
				.append(destinationStairdetails).append(destinationfloor)
				.append(destinationElevatorDetails)
				.append(destinationElevatorType).append(destinationParkingType)
				.append(destinationParkinglotsize)
				.append(destinationParkingSpots)
				.append(destinationDistanceToParking)
				.append(destinationShuttleDistance)
				.append(destinationAdditionalStopDetails).append(id)
				.append(corpID).append(originComment)
				.append(destinationComment).append(serviceOrderId)
				.append(customerFileId).append(networkSynchedId)
				.append(inventory).append(labels).append(typeofPacking)
				.append(packMaterials).append(tools).append(piano)
				.append(carMotorbike).append(crating).append(remarks)
				.append(createdOn)
				.append(createdBy)
				.append(updatedOn)
				.append(updatedBy).toHashCode();
	}
	@Column
	public String getDestinationAdditionalStopDetails() {
		return destinationAdditionalStopDetails;
	}
	public void setDestinationAdditionalStopDetails(
			String destinationAdditionalStopDetails) {
		this.destinationAdditionalStopDetails = destinationAdditionalStopDetails;
	}
	@Column
	public Boolean getDestinationAdditionalStopRequired() {
		return destinationAdditionalStopRequired;
	}
	public void setDestinationAdditionalStopRequired(
			Boolean destinationAdditionalStopRequired) {
		this.destinationAdditionalStopRequired = destinationAdditionalStopRequired;
	}
	@Column
	public String getDestinationCarrydetails() {
		return destinationCarrydetails;
	}
	public void setDestinationCarrydetails(String destinationCarrydetails) {
		this.destinationCarrydetails = destinationCarrydetails;
	}
	@Column
	public String getDestinationCarryDistance() {
		return destinationCarryDistance;
	}
	public void setDestinationCarryDistance(String destinationCarryDistance) {
		this.destinationCarryDistance = destinationCarryDistance;
	}
	@Column
	public String getDestinationDistanceToParking() {
		return destinationDistanceToParking;
	}
	public void setDestinationDistanceToParking(String destinationDistanceToParking) {
		this.destinationDistanceToParking = destinationDistanceToParking;
	}
	@Column
	public Boolean getDestinationElevator() {
		return destinationElevator;
	}
	public void setDestinationElevator(Boolean destinationElevator) {
		this.destinationElevator = destinationElevator;
	}
	@Column
	public String getDestinationElevatorDetails() {
		return destinationElevatorDetails;
	}
	public void setDestinationElevatorDetails(String destinationElevatorDetails) {
		this.destinationElevatorDetails = destinationElevatorDetails;
	}
	@Column
	public String getDestinationElevatorType() {
		return destinationElevatorType;
	}
	public void setDestinationElevatorType(String destinationElevatorType) {
		this.destinationElevatorType = destinationElevatorType;
	}
	@Column
	public Boolean getDestinationExternalElevator() {
		return destinationExternalElevator;
	}
	public void setDestinationExternalElevator(Boolean destinationExternalElevator) {
		this.destinationExternalElevator = destinationExternalElevator;
	}
	@Column
	public String getDestinationfloor() {
		return destinationfloor;
	}
	public void setDestinationfloor(String destinationfloor) {
		this.destinationfloor = destinationfloor;
	}
	@Column
	public Boolean getDestinationLongCarry() {
		return destinationLongCarry;
	}
	public void setDestinationLongCarry(Boolean destinationLongCarry) {
		this.destinationLongCarry = destinationLongCarry;
	}
	@Column
	public Boolean getDestinationNeedCrane() {
		return destinationNeedCrane;
	}
	public void setDestinationNeedCrane(Boolean destinationNeedCrane) {
		this.destinationNeedCrane = destinationNeedCrane;
	}
	@Column
	public String getDestinationParkinglotsize() {
		return destinationParkinglotsize;
	}
	public void setDestinationParkinglotsize(String destinationParkinglotsize) {
		this.destinationParkinglotsize = destinationParkinglotsize;
	}
	@Column
	public String getDestinationParkingType() {
		return destinationParkingType;
	}
	public void setDestinationParkingType(String destinationParkingType) {
		this.destinationParkingType = destinationParkingType;
	}
	@Column
	public Boolean getDestinationReserveParking() {
		return destinationReserveParking;
	}
	public void setDestinationReserveParking(Boolean destinationReserveParking) {
		this.destinationReserveParking = destinationReserveParking;
	}
	@Column
	public String getDestinationResidenceSize() {
		return destinationResidenceSize;
	}
	public void setDestinationResidenceSize(String destinationResidenceSize) {
		this.destinationResidenceSize = destinationResidenceSize;
	}
	@Column
	public String getDestinationResidenceType() {
		return destinationResidenceType;
	}
	public void setDestinationResidenceType(String destinationResidenceType) {
		this.destinationResidenceType = destinationResidenceType;
	}
	@Column
	public String getDestinationShuttleDistance() {
		return destinationShuttleDistance;
	}
	public void setDestinationShuttleDistance(String destinationShuttleDistance) {
		this.destinationShuttleDistance = destinationShuttleDistance;
	}
	@Column
	public Boolean getDestinationShuttleRequired() {
		return destinationShuttleRequired;
	}
	public void setDestinationShuttleRequired(Boolean destinationShuttleRequired) {
		this.destinationShuttleRequired = destinationShuttleRequired;
	}
	@Column
	public Boolean getDestinationStairCarry() {
		return destinationStairCarry;
	}
	public void setDestinationStairCarry(Boolean destinationStairCarry) {
		this.destinationStairCarry = destinationStairCarry;
	}
	@Column
	public String getDestinationStairdetails() {
		return destinationStairdetails;
	}
	public void setDestinationStairdetails(String destinationStairdetails) {
		this.destinationStairdetails = destinationStairdetails;
	}
	@Column
	public String getDestinationStairDistance() {
		return destinationStairDistance;
	}
	public void setDestinationStairDistance(String destinationStairDistance) {
		this.destinationStairDistance = destinationStairDistance;
	}
	@Column
	public String getOriginAdditionalStopDetails() {
		return originAdditionalStopDetails;
	}
	public void setOriginAdditionalStopDetails(String originAdditionalStopDetails) {
		this.originAdditionalStopDetails = originAdditionalStopDetails;
	}
	@Column
	public Boolean getOriginAdditionalStopRequired() {
		return originAdditionalStopRequired;
	}
	public void setOriginAdditionalStopRequired(Boolean originAdditionalStopRequired) {
		this.originAdditionalStopRequired = originAdditionalStopRequired;
	}
	@Column
	public String getOriginCarrydetails() {
		return originCarrydetails;
	}
	public void setOriginCarrydetails(String originCarrydetails) {
		this.originCarrydetails = originCarrydetails;
	}
	@Column
	public String getOriginCarryDistance() {
		return originCarryDistance;
	}
	public void setOriginCarryDistance(String originCarryDistance) {
		this.originCarryDistance = originCarryDistance;
	}
	@Column
	public String getOriginDistanceToParking() {
		return originDistanceToParking;
	}
	public void setOriginDistanceToParking(String originDistanceToParking) {
		this.originDistanceToParking = originDistanceToParking;
	}
	@Column
	public Boolean getOriginElevator() {
		return originElevator;
	}
	public void setOriginElevator(Boolean originElevator) {
		this.originElevator = originElevator;
	}
	@Column
	public String getOriginElevatorDetails() {
		return originElevatorDetails;
	}
	public void setOriginElevatorDetails(String originElevatorDetails) {
		this.originElevatorDetails = originElevatorDetails;
	}
	@Column
	public String getOriginElevatorType() {
		return originElevatorType;
	}
	public void setOriginElevatorType(String originElevatorType) {
		this.originElevatorType = originElevatorType;
	}
	@Column
	public Boolean getOriginExternalElevator() {
		return originExternalElevator;
	}
	public void setOriginExternalElevator(Boolean originExternalElevator) {
		this.originExternalElevator = originExternalElevator;
	}
	@Column
	public String getOriginfloor() {
		return originfloor;
	}
	public void setOriginfloor(String originfloor) {
		this.originfloor = originfloor;
	}
	@Column
	public Boolean getOriginLongCarry() {
		return originLongCarry;
	}
	public void setOriginLongCarry(Boolean originLongCarry) {
		this.originLongCarry = originLongCarry;
	}
	@Column
	public Boolean getOriginNeedCrane() {
		return originNeedCrane;
	}
	public void setOriginNeedCrane(Boolean originNeedCrane) {
		this.originNeedCrane = originNeedCrane;
	}
	@Column
	public String getOriginParkinglotsize() {
		return originParkinglotsize;
	}
	public void setOriginParkinglotsize(String originParkinglotsize) {
		this.originParkinglotsize = originParkinglotsize;
	}
	@Column
	public String getOriginParkingType() {
		return originParkingType;
	}
	public void setOriginParkingType(String originParkingType) {
		this.originParkingType = originParkingType;
	}
	@Column
	public Boolean getOriginReserveParking() {
		return originReserveParking;
	}
	public void setOriginReserveParking(Boolean originReserveParking) {
		this.originReserveParking = originReserveParking;
	}
	@Column
	public String getOriginResidenceSize() {
		return originResidenceSize;
	}
	public void setOriginResidenceSize(String originResidenceSize) {
		this.originResidenceSize = originResidenceSize;
	}
	@Column
	public String getOriginResidenceType() {
		return originResidenceType;
	}
	public void setOriginResidenceType(String originResidenceType) {
		this.originResidenceType = originResidenceType;
	}
	@Column
	public String getOriginShuttleDistance() {
		return originShuttleDistance;
	}
	public void setOriginShuttleDistance(String originShuttleDistance) {
		this.originShuttleDistance = originShuttleDistance;
	}
	@Column
	public Boolean getOriginStairCarry() {
		return originStairCarry;
	}
	public void setOriginStairCarry(Boolean originStairCarry) {
		this.originStairCarry = originStairCarry;
	}
	@Column
	public String getOriginStairdetails() {
		return originStairdetails;
	}
	public void setOriginStairdetails(String originStairdetails) {
		this.originStairdetails = originStairdetails;
	}
	@Column
	public String getOriginStairDistance() {
		return originStairDistance;
	}
	public void setOriginStairDistance(String originStairDistance) {
		this.originStairDistance = originStairDistance;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getOriginParkingSpots() {
		return originParkingSpots;
	}
	public void setOriginParkingSpots(String originParkingSpots) {
		this.originParkingSpots = originParkingSpots;
	}
	@Column
	public Boolean getOriginShuttleRequired() {
		return originShuttleRequired;
	}
	public void setOriginShuttleRequired(Boolean originShuttleRequired) {
		this.originShuttleRequired = originShuttleRequired;
	}
	@Column
	public String getDestinationParkingSpots() {
		return destinationParkingSpots;
	}
	public void setDestinationParkingSpots(String destinationParkingSpots) {
		this.destinationParkingSpots = destinationParkingSpots;
	}
	public String getOriginComment() {
		return originComment;
	}
	public void setOriginComment(String originComment) {
		this.originComment = originComment;
	}
	public String getDestinationComment() {
		return destinationComment;
	}
	public void setDestinationComment(String destinationComment) {
		this.destinationComment = destinationComment;
	}
	@Column
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Column
	public Long getCustomerFileId() {
		return customerFileId;
	}
	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}
	@Column
	public Long getNetworkSynchedId() {
		return networkSynchedId;
	}
	public void setNetworkSynchedId(Long networkSynchedId) {
		this.networkSynchedId = networkSynchedId;
	}
	@Column
	public Boolean getInventory() {
		return inventory;
	}
	public void setInventory(Boolean inventory) {
		this.inventory = inventory;
	}
	@Column
	public Boolean getLabels() {
		return labels;
	}
	public void setLabels(Boolean labels) {
		this.labels = labels;
	}
	@Column
	public Boolean getTypeofPacking() {
		return typeofPacking;
	}
	public void setTypeofPacking(Boolean typeofPacking) {
		this.typeofPacking = typeofPacking;
	}
	@Column
	public Boolean getPackMaterials() {
		return packMaterials;
	}
	public void setPackMaterials(Boolean packMaterials) {
		this.packMaterials = packMaterials;
	}
	@Column
	public Boolean getTools() {
		return tools;
	}
	public void setTools(Boolean tools) {
		this.tools = tools;
	}
	@Column
	public Boolean getPiano() {
		return piano;
	}
	public void setPiano(Boolean piano) {
		this.piano = piano;
	}
	@Column
	public Boolean getCarMotorbike() {
		return carMotorbike;
	}
	public void setCarMotorbike(Boolean carMotorbike) {
		this.carMotorbike = carMotorbike;
	}
	@Column
	public Boolean getCrating() {
		return crating;
	}
	public void setCrating(Boolean crating) {
		this.crating = crating;
	}
	@Column
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
}