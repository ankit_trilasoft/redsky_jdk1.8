package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.SystemConfigurationDao;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.SystemConfiguration;
import com.trilasoft.app.service.SystemConfigurationManager;





public class SystemConfigurationManagerImpl  extends GenericManagerImpl<SystemConfiguration, Long> implements SystemConfigurationManager {

	 SystemConfigurationDao systemConfigurationDao; 
    
    
    public SystemConfigurationManagerImpl(SystemConfigurationDao systemConfigurationDao) { 
        super(systemConfigurationDao); 
        this.systemConfigurationDao = systemConfigurationDao; 
    } 

    public List checkById(Long id) { 
        return systemConfigurationDao.checkById(id); 
    }
    public List<SystemConfiguration>getList()
    {
    	return systemConfigurationDao.getList();
    }
    public List<DataCatalog>searchByTableAndField(String tableName,String fieldName,String corpID,String description,String auditable,String defineByToDoRule,String isdateField,String visible,String charge) 
    {
    	return systemConfigurationDao.searchByTableAndField(tableName, fieldName, corpID, description, auditable, defineByToDoRule, isdateField, visible, charge);
    }
    public List configurableTable(String b)
	{
		return systemConfigurationDao.configurableTable(b);
	}
    public List configurableField(String tableName,String b)
    {
    	return systemConfigurationDao.configurableField(tableName, b);
    }

	public List<DataCatalog> getMasterList(String corpID) {
		return systemConfigurationDao.getMasterList(corpID);	
	}
}  