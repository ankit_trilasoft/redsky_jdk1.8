<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="AccountLineStatusReport"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="955"
		 pageHeight="595"
		 columnWidth="895"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Begin Date" isForPrompting="true" class="java.util.Date"/>
	<parameter name="End Date" isForPrompting="true" class="java.util.Date"/>
	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[select
"AccruedRevenueReport-R-UGRN.jrxml",
accountline.`companyDivision` AS "Company Division",
serviceorder.`id` AS "S/O ID",
serviceorder.`shipNumber` AS "S/O Number",
serviceorder.`sequenceNumber` AS "C/F Number",
DATE_FORMAT(billing.`revenueRecognition`,'%d-%m-%Y') AS "Receivable Recognition Date",
accountline.`id` AS "Accounting Line ID",
charges.`charge` AS "Charge Code",
charges.`costElement` AS "Cost Element",
accountline.`revisionSellLocalAmount` AS "Revised Receivable Amount excluding VAT (Invoice Currency)",
accountline.`revisionSellCurrency` AS "Revised Receivable Billing Currency",
accountline.`actualRevenueForeign` AS "Actual Receivable Amount excluding VAT (Invoice Currency)",
accountline.`recRateCurrency` AS "Actual Receivable Billing Currency",
round(if(accountline.`recVatPercent` is null ||  accountline.`recVatPercent`=0,0.00,((accountline.`actualRevenueForeign`*accountline.`recVatPercent`)/100)),2) AS "Actual Receivable VAT Amount (Invoice Currency)",
accountline.`revisionRevenueAmount` AS "Revised Receivable Amount excluding VAT (Home Currency)",
accountline.`actualRevenue` AS "Actual Receivable Amount excluding VAT (Home Currency)",
round(if(accountline.`recVatPercent` is null ||  accountline.`recVatPercent`=0,0.00,((accountline.`actualRevenue`*accountline.`recVatPercent`)/100)),2) AS "Actual Receivable VAT Amount (Home Currency)",
refmaster_R.`description` AS "VAT Code Receivable",
accountline.`recGl` AS "GL account Receivable",
if(accountline.`billToCode`="","",concat("'",accountline.`billToCode`)) AS "Bill to Code Receivable",
accountline.`billToName` AS "Bill to Party Receivable",
partnerpublicParent.`aliasName` AS "Account Name",
partneraccountref_RR.accountCrossReference AS "Accounting Cross Reference Receivable",
partneraccountref_RS.accountCrossReference AS "Accounting Cross Reference Suspense Account",
accountline.`recInvoiceNumber` AS "Sales invoice number",
DATE_FORMAT(accountline.`receivedInvoiceDate`,'%d-%m-%Y') AS "Sales invoice document date",
DATE_FORMAT(accountline.`recPostDate`,'%d-%m-%Y') AS "Sales invoice POSTING date",
accountline.`revisionLocalAmount` AS "Revised Payable Amount excluding VAT (Invoice Currency)",
accountline.`revisionCurrency` AS "Revised Payable Billing Currency",
accountline.`localAmount` AS "Actual Payable Amount excluding VAT (Invoice Currency)",
accountline.`country` AS "Actual Payable Billing Currency",
round(if(accountline.`payVatPercent` is null ||  accountline.`payVatPercent`=0,0.00,((accountline.`localAmount`*accountline.`payVatPercent`)/100)),2) AS "Actual Payable VAT Amount (Invoice Currency)",
accountline.`revisionExpense` AS "Revised Payable Amount excluding VAT (Home Currency)",
accountline.`actualExpense` AS "Actual Payable Amount excluding VAT (Home Currency)",
round(if(accountline.`payVatPercent` is null ||  accountline.`payVatPercent`=0,0.00,((accountline.`actualExpense`*accountline.`payVatPercent`)/100)),2) AS "Actual Payable VAT Amount (Home Currency)",
refmaster_P.`description` AS "VAT Code Payable",
accountline.`payGl` AS "GL account Payable",
if(accountline.`vendorCode`="","",concat("'",accountline.`vendorCode`)) AS "Bill to code Payable",
accountline.`estimateVendorName` AS "Billing Party Payable",
partneraccountref_PP.accountCrossReference AS "Accounting Cross Reference Payable",
partneraccountref_PS.accountCrossReference AS "Accounting Cross Reference Suspense Account",
accountline.`invoiceNumber` AS "Purchase invoice number",
DATE_FORMAT(accountline.`invoiceDate`,'%d-%m-%Y') AS "Purchase invoice document date",
DATE_FORMAT(accountline.`payPostDate`,'%d-%m-%Y') AS "Purchase invoice POSTING date",
if(accountline.`status` is true,"True","False") AS "Accountline Status",
serviceorder.`status`  AS "SO Status",
if(accountline.`payAccDate` is not null,"True","False") AS "Payable Transferred to Accounting",
if(accountline.`recAccDate` is not null,"True","False") AS "Receivable Transferred to Accounting",
DATE_FORMAT(trackingstatus.deliveryA,'%d-%m-%Y') AS   "Actual Delivery Date",
DATE_FORMAT(trackingstatus.serviceCompleteDate,'%d-%m-%Y') AS "Service Complete Date",
serviceorder.`job` AS "Job Type" 
FROM
`serviceorder` serviceorder 
INNER JOIN `accountline` accountline ON serviceorder.`id` = accountline.`serviceOrderId` AND accountline.`corpID`=$P{Corporate ID}
INNER JOIN `billing` billing ON serviceorder.`id` = billing.`id` AND billing.`corpID`=$P{Corporate ID}
INNER JOIN `trackingstatus` trackingstatus ON serviceorder.`id` = trackingstatus.`id` AND trackingstatus.`corpID`=$P{Corporate ID}
LEFT OUTER JOIN `charges` charges ON charges.`charge` = accountline.`chargecode` AND charges.`contract`=billing.`contract` AND charges.`corpID`=$P{Corporate ID}
LEFT OUTER JOIN `partnerpublic` partnerpublic ON partnerpublic.`partnerCode`= accountline.`billToCode` AND partnerpublic.corpID in ('TSFT',$P{Corporate ID})
LEFT OUTER JOIN `partnerpublic` partnerpublicParent ON partnerpublicParent.`partnerCode`= partnerpublic.`agentparent` AND partnerpublicParent.corpID in ('TSFT',$P{Corporate ID})
LEFT OUTER JOIN `partneraccountref` partneraccountref_RR ON partneraccountref_RR.`partnerCode`= accountline.`billToCode` AND partneraccountref_RR.corpID in ('TSFT',$P{Corporate ID}) AND partneraccountref_RR.refType='R'   
LEFT OUTER JOIN `partneraccountref` partneraccountref_RS ON partneraccountref_RS.`partnerCode`= accountline.`billToCode` AND partneraccountref_RS.corpID in ('TSFT',$P{Corporate ID}) AND partneraccountref_RS.refType='S'   
LEFT OUTER JOIN `partneraccountref` partneraccountref_PP ON partneraccountref_PP.`partnerCode`= accountline.`vendorCode` AND partneraccountref_PP.corpID in ('TSFT',$P{Corporate ID}) AND partneraccountref_PP.refType='P'   
LEFT OUTER JOIN `partneraccountref` partneraccountref_PS ON partneraccountref_PS.`partnerCode`= accountline.`vendorCode` AND partneraccountref_PS.corpID in ('TSFT',$P{Corporate ID}) AND partneraccountref_PS.refType='S'  
left outer join `refmaster` refmaster_R on accountline.`recVatDescr`=refmaster_R.`code` and refmaster_R.`parameter` = 'EUVAT' and refmaster_R.`corpID` in ($P{Corporate ID},'TSFT')
left outer join `refmaster` refmaster_P on accountline.`payVatDescr` =refmaster_P.`code` and refmaster_P.`parameter` = 'PAYVATDESC' and refmaster_P.`corpId` in ($P{Corporate ID},'TSFT')
WHERE serviceorder.`corpID`=$P{Corporate ID}
AND DATE_FORMAT(accountline.`createdOn`,'%Y-%m-%d') between $P{Begin Date} AND $P{End Date}
ORDER BY accountline.`shipNumber`]]></queryString>

	<field name="AccruedRevenueReport-R-UGRN.jrxml" class="java.lang.String"/>
	<field name="Company Division" class="java.lang.String"/>
	<field name="S/O ID" class="java.lang.Long"/>
	<field name="S/O Number" class="java.lang.String"/>
	<field name="C/F Number" class="java.lang.String"/>
	<field name="Receivable Recognition Date" class="java.lang.String"/>
	<field name="Accounting Line ID" class="java.lang.Long"/>
	<field name="Charge Code" class="java.lang.String"/>
	<field name="Cost Element" class="java.lang.String"/>
	<field name="Revised Receivable Amount excluding VAT (Invoice Currency)" class="java.math.BigDecimal"/>
	<field name="Revised Receivable Billing Currency" class="java.lang.String"/>
	<field name="Actual Receivable Amount excluding VAT (Invoice Currency)" class="java.math.BigDecimal"/>
	<field name="Actual Receivable Billing Currency" class="java.lang.String"/>
	<field name="Actual Receivable VAT Amount (Invoice Currency)" class="java.lang.Double"/>
	<field name="Revised Receivable Amount excluding VAT (Home Currency)" class="java.math.BigDecimal"/>
	<field name="Actual Receivable Amount excluding VAT (Home Currency)" class="java.math.BigDecimal"/>
	<field name="Actual Receivable VAT Amount (Home Currency)" class="java.lang.Double"/>
	<field name="VAT Code Receivable" class="java.lang.String"/>
	<field name="GL account Receivable" class="java.lang.String"/>
	<field name="Bill to Code Receivable" class="java.lang.String"/>
	<field name="Bill to Party Receivable" class="java.lang.String"/>
	<field name="Account Name" class="java.lang.String"/>
	<field name="Accounting Cross Reference Receivable" class="java.lang.String"/>
	<field name="Accounting Cross Reference Suspense Account" class="java.lang.String"/>
	<field name="Sales invoice number" class="java.lang.String"/>
	<field name="Sales invoice document date" class="java.lang.String"/>
	<field name="Sales invoice POSTING date" class="java.lang.String"/>
	<field name="Revised Payable Amount excluding VAT (Invoice Currency)" class="java.math.BigDecimal"/>
	<field name="Revised Payable Billing Currency" class="java.lang.String"/>
	<field name="Actual Payable Amount excluding VAT (Invoice Currency)" class="java.math.BigDecimal"/>
	<field name="Actual Payable Billing Currency" class="java.lang.String"/>
	<field name="Actual Payable VAT Amount (Invoice Currency)" class="java.lang.Double"/>
	<field name="Revised Payable Amount excluding VAT (Home Currency)" class="java.math.BigDecimal"/>
	<field name="Actual Payable Amount excluding VAT (Home Currency)" class="java.math.BigDecimal"/>
	<field name="Actual Payable VAT Amount (Home Currency)" class="java.lang.Double"/>
	<field name="VAT Code Payable" class="java.lang.String"/>
	<field name="GL account Payable" class="java.lang.String"/>
	<field name="Bill to code Payable" class="java.lang.String"/>
	<field name="Billing Party Payable" class="java.lang.String"/>
	<field name="Accounting Cross Reference Payable" class="java.lang.String"/>
	<field name="Purchase invoice number" class="java.lang.String"/>
	<field name="Purchase invoice document date" class="java.lang.String"/>
	<field name="Purchase invoice POSTING date" class="java.lang.String"/>
	<field name="Accountline Status" class="java.lang.String"/>
	<field name="SO Status" class="java.lang.String"/>
	<field name="Payable Transferred to Accounting" class="java.lang.String"/>
	<field name="Receivable Transferred to Accounting" class="java.lang.String"/>
	<field name="Actual Delivery Date" class="java.lang.String"/>
	<field name="Service Complete Date" class="java.lang.String"/>
	<field name="Job Type" class="java.lang.String"/>

		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="48"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="20"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="18"  isSplitAllowed="true" >
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="15"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
