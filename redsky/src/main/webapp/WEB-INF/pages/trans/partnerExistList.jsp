<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title>Partner List</title>
    <meta name="heading" content="Partner List"/> 
<style type="text/css">
/* collapse */

span.pagelinks {
display:block;
font-size:0.85em;
margin-top:-11px;
padding:2px 0px;
text-align:right;
width:99%;
}

div.error, span.error, li.error, div.message {
background:#B3DAF4 none repeat scroll 0 0;
border:1px solid #000000;
color:#000000;
font-family:Arial,Helvetica,sans-serif;
font-weight:normal;
margin:10px auto;
padding:3px;
text-align:center;
vertical-align:bottom;
width:600px;
}
</style>
<script type="text/javascript">
function partnerList(){
	location.href ='partnerPublics.html';
}
</script> 

</head>
<s:form id="partnerForm" name="partnerForm" enctype="multipart/form-data" action="savePartnerPublic.html" method="post" validate="true">
<s:hidden name="exList" value="true"/>
<s:hidden name="popupval" value="false"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
	
<div id="layer1" style="width:100%">
<div id="otabs">
	<ul>
		<li><a class="current"><span>Partner List</span></a></li>
	</ul>
</div><div class="spnblk">&nbsp;</div> 
<s:set name="partnerListNew" value="partnerListNew" scope="request"/>  
<display:table name="partnerListNew" class="table" requestURI="" id="partnerListNewList" export="false" pagesize="10" style="width:100%">   
	<display:column property="partnerCode" sortable="true" title="Partner Code" style="width:40px"/>
	<display:column property="firstName" sortable="true" title="First Name" style="width:25px"/>
	<display:column property="lastName" sortable="true" title="Last Name" style="width:25px"/>
    <display:column property="terminalAddress1" sortable="true" title="Terminal Address" style="width:12%"/>
    <display:column property="terminalCountryCode" sortable="true" title="Country" style="width:9%"/>
    <display:column property="terminalCity" sortable="true" title="City" style="width:20px"/>

    <display:column title="PParty" style="width:15px;font-size: 9px;" >
		<c:if test="${partnerListNewList.isPrivateParty == true}">  
    		<a href="editPartnerPublic.html?id=${partnerListNewList.id}&partnerType=PP"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Account" style="width:15px;font-size: 9px; width:6%;" >
		<c:if test="${partnerListNewList.isAccount == true}">  
    		<a href="editPartnerPublic.html?id=${partnerListNewList.id}&partnerType=AC"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Agent" style="width:15px;font-size: 9px; width:6%;" >
		<c:if test="${partnerListNewList.isAgent == true}">  
    		<a href="editPartnerPublic.html?id=${partnerListNewList.id}&partnerType=AG"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Carrier" style="width:15px;font-size: 9px;width:6%;" >
		<c:if test="${partnerListNewList.isCarrier == true}">  
    		<a href="editPartnerPublic.html?id=${partnerListNewList.id}&partnerType=CR"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Vendor" style="width:15px;font-size: 9px;width:6%;" >
		<c:if test="${partnerListNewList.isVendor == true}">  
    		<a href="editPartnerPublic.html?id=${partnerListNewList.id}&partnerType=VN"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Owner Ops" style="width:15px;font-size: 9px;width:6%;" >
		<c:if test="${partnerListNewList.isOwnerOp == true}">  
    		<a href="editPartnerPublic.html?id=${partnerListNewList.id}&partnerType=OO"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
    
    
    <display:setProperty name="paging.banner.item_name" value="partnerListNew"/>   
    <display:setProperty name="paging.banner.items_name" value="partnerListNew"/>   
   
</display:table> 

<input type="button"" name="addBtn" class="cssbutton" style="width:115px; height:25px; margin-left:15px;" onclick="partnerList();" align="top" value="Record Found"/>      
<s:submit cssClass="cssbutton" cssStyle="width:115px; height:25px; margin-left:15px;" align="top" value="Record Not Found"/> 

</div>
</s:form>

<script type="text/javascript">   
    Form.focusFirstElement($("partnerForm")); 
</script>  
