package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DsTaxServicesDao; 
import com.trilasoft.app.model.DsTaxServices;

public class DsTaxServicesDaoHibernate extends GenericDaoHibernate<DsTaxServices, Long> implements DsTaxServicesDao {

	public DsTaxServicesDaoHibernate() {
		super(DsTaxServices.class); 
	}

	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsTaxServices where id = "+id+" ");
	}

}
