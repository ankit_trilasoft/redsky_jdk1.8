package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DsFamilyDetailsDao;
import com.trilasoft.app.model.DsFamilyDetails;

public class DsFamilyDetailsDaoHibernate extends GenericDaoHibernate<DsFamilyDetails, Long>implements DsFamilyDetailsDao {

	public DsFamilyDetailsDaoHibernate() {   
        super(DsFamilyDetails.class);   
    }

	public List getDsFamilyDetailsList(Long id, String sessionCorpID) {
		return getHibernateTemplate().find("from DsFamilyDetails where corpID='"+sessionCorpID+"' and customerFileId=?", id); 
	}
	
	public void setCFileFamilySize(Long id, String sessionCorpID){
		List list = getSession().createSQLQuery("select count(*) from dsfamilydetails where customerfileId="+id).list();
		
		if(list == null || list.isEmpty() || list.get(0)== null || "".equals(list.get(0).toString().trim())){
			getSession().createSQLQuery("update customerfile set familysize=0 where id="+id).executeUpdate();
		}else{
			int i = Integer.parseInt(list.get(0).toString());
			getSession().createSQLQuery("update customerfile set familysize="+i+" where id="+id).executeUpdate();
		}
	}

	public Long findRemoteDsFamilyDetails(Long networkId, Long id) {
		return Long.parseLong(this.getSession().createSQLQuery("select id from dsfamilydetails where networkId="+networkId+" and customerFileId="+id+"").list().get(0).toString());
	}

	public void deleteNetworkDsFamilyDetails(Long customerFileId, Long networkId) {
		this.getSession().createSQLQuery("delete from dsfamilydetails where networkId="+networkId+" and customerFileId="+customerFileId+"").executeUpdate();		
	}

	public int updateFamilyDetailsNetworkId(Long id) {
		int i=getSession().createSQLQuery("update dsfamilydetails set networkId="+id+" where id="+id).executeUpdate();
		return i;
	}
	
	public String getDsFamilyDetailsBlankFieldId(String ikeaflag,Long cid, String sessionCorpID){
		String query;
		
		if(ikeaflag==null ||ikeaflag.equalsIgnoreCase("")||!ikeaflag.equals("1"))
		{
			
		query="select id from dsfamilydetails where corpid = '"+sessionCorpID+"' and customerfileid='"+cid+"' "
					+" and ((firstname is null or firstname='') or (lastname is null or lastname='') or "
					+" (relationship is null or relationship='') or (gender is null or gender='') )";
		}
		else
		{
			query="select id from dsfamilydetails where corpid = '"+sessionCorpID+"' and customerfileid='"+cid+"' "
			+" and ((firstname is null or firstname='') or (lastname is null or lastname='') or "
			+" (relationship is null or relationship='') or (gender is null or gender='') or (dateOfBirth is null or dateOfBirth='') or (nationality is null or nationality='') )";
		}
		String idFound="";
		try{
			List idList= this.getSession().createSQLQuery(query).list();
				if(idList !=null && (!(idList.isEmpty())) && idList.get(0)!=null){
					idFound	= "Y";
				}else{
					idFound	= "N";
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			return idFound;
	}
	
	public Long getDsFamilyIdByRelationship(Long cid,String sessionCorpID,String relationshipVal){
		Long familyId = null;
		String query = "select id from dsfamilydetails where customerFileId='"+cid+"' and corpid = '"+sessionCorpID+"' and relationship='"+relationshipVal+"'";
		List familyIdList = this.getSession().createSQLQuery(query).list();
		if(familyIdList!=null && !familyIdList.isEmpty()){
			familyId = Long.parseLong(familyIdList.get(0).toString());
		}
		return familyId;
	}
	
	public List findRelationshipList(Long cid,String sessionCorpID){
		List familyRelationshipValList = new ArrayList();
		try {
			String query = "select relationship from dsfamilydetails where customerFileId='"+cid+"' and corpid = '"+sessionCorpID+"' ";
			List familyList = this.getSession().createSQLQuery(query).list();
			if (!familyList.isEmpty()) {
				familyRelationshipValList = familyList;
			}			
		}catch (Exception e) {
	    	 e.printStackTrace();
		}
		return familyRelationshipValList;
	}

	public List findSpouseDetails(Long id, String sessionCorpID) {
		try {
			return getSession().createSQLQuery(
					"select concat(if(FirstName is null or FirstName ='','NOData',FirstName),'~',if(LastName is null or LastName ='','NOData',LastName),'~',if(CellNumber is null or CellNumber ='','NOData',CellNumber),'~',if(email is null or email ='','NOData',email),'~',if(prefix is null or prefix ='','NOData',prefix)) from dsfamilydetails where corpid = '"+sessionCorpID+"' and customerfileid='"+id+"' and Relationship ='Spouse' limit 1 ").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e); 
		}
		return null;
	}

	
	public int findRelationshipList1(Long cid,String sessionCorpID,Long id){
		
		return (int)Long.parseLong(this.getSession().createSQLQuery("select count(relationship)  from dsfamilydetails where customerFileId='"+cid+"' and   relationship='Self' and corpid = '"+sessionCorpID+"' and id <>'"+id+"' ").list().get(0).toString());
	}
	
	public int findRelationshipList2(Long cid,String sessionCorpID){
		
		return (int)Long.parseLong(this.getSession().createSQLQuery("select count(relationship)  from dsfamilydetails where customerFileId='"+cid+"' and   relationship='Self' and corpid = '"+sessionCorpID+"' ").list().get(0).toString());
	}
}
