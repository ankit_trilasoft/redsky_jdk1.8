package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;
import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.PayrollAllocation;

public interface PayrollAllocationManager  extends GenericManager<PayrollAllocation, Long>{

	public List findMaximumId();
	
	public List findById(Long id);
	
	
	public List<PayrollAllocation> searchpayrollAllocation(String code,String job,String service,String calculation);

	public Map<String, String> findRecalc(String corpId);
}
