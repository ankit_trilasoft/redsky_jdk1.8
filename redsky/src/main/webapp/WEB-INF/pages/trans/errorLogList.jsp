<%@ include file="/common/taglibs.jsp" %> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<head> 
<title>Error Log Info</title> 
<meta name="heading" content="Error Log Info"/> 
 <style><%@ include file="/common/calenderStyle.css"%></style>
 <script src="${pageContext.request.contextPath}/scripts/jquery.min.js"></script>
   <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>    
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-15px;
!margin-top:-10px;
}
div#main {
margin:-5px 0 0;
}
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
<script type="text/javascript">
var r1={
		 'special1':/['\#'&'\$'&'\~'&'\!'&''\+'&'\\'&'\/'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\|'&'\['&'\]'&'\,'&'\`'&'\='&'('&'\)']/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		
function validCheck(targetElement,w){
	 targetElement.value = targetElement.value.replace(r1[w],'');
	}
</script>
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:52px; height:25px;"  method="searchErrorLogList"  key="button.search" />   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px; height:25px;" onclick="clear_fields();"/> 
</c:set>   
<body>
 
<script language="javascript" type="text/javascript">
function clear_fields(){
		document.forms['errorLogInfo'].elements['errorLogCorpId'].value  = "";
		document.forms['errorLogInfo'].elements['errorLogModule'].value  = "";
		document.forms['errorLogInfo'].elements['errorLogCreatedBy'].value  = "";
		document.forms['errorLogInfo'].elements['errorLogCreatedOn'].value  = "";
}

</script>
 <s:form id="errorLogInfo" name="errorLogInfo" action="errorLogList" method="post" validate="true"> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:set name="errorLogList" value="%{errorLogList}" scope="session"/>  
<s:hidden name="id" />
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content" style="padding-left:15px;">
<table class="table" style="width:100%"  >
<thead>
<tr>
<th>Corp&nbsp;Id</th>
<th>Module</th>
<th>Created&nbsp;By</th>
<th>Created&nbsp;On</th>
</tr></thead>	
		<tbody>
		<tr>
			<td width="20" align="left">
				<s:select cssClass="list-menu"   name="errorLogCorpId"  id="errorLogCorpId" list="%{distinctCorpId}" headerKey="" headerValue="" cssStyle="width:150px"/>
				</td>
			<td width="20" align="left">
			    <s:textfield name="errorLogModule" id= "errorLogModule" required="true" cssClass="input-text" size="16" onchange="validCheck(this,'special')"/>
			</td>
			<td width="20" align="left">
			    <s:textfield name="errorLogCreatedBy" id ="errorLogCreatedBy" required="true" cssClass="input-text" size="35" onchange="validCheck(this,'special')"  onblur="validCheck(this,'special')"/>
			</td>
					
			<td width="20" align="left">
			         <c:if test="${not empty errorLogCreatedOn}">
			         <s:text id="createdOnDateFormate" name="${FormDateValue}"><s:param name="value" value="errorLogCreatedOn"/></s:text>
					<s:textfield cssClass="input-text" id="errorLogCreatedOn" value="%{createdOnDateFormate}" name="errorLogCreatedOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" />
					<img id="errorLogCreatedOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick=""/>
					</c:if>
	
					<c:if test="${empty errorLogCreatedOn}"> 
					   <s:textfield cssClass="input-text" id="errorLogCreatedOn" name="errorLogCreatedOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" />
					   <img id="errorLogCreatedOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick=""/>
		            </c:if>	
			</td>
			</tr>
			<tr>
			<td colspan="6" style="border:none;vertical-align:bottom; text-align:right;" >
			    <c:out value="${searchbuttons}" escapeXml="false" />
			</td>
			</tr>
			</table>  
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>
</div>
</div> 
<c:out value="${searchresults}" escapeXml="false" />  
		<div id="otabs" style="margin-top: -10px;">
		  <ul>
		    <li><a class="current"><span>Error Log List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<display:table name="errorLogList" class="table" pagesize="25"requestURI="" id="customerFileList" export="true" defaultsort="6"  defaultorder="descending" style="width:100%;">
<display:column title="CorpID" property="corpId" sortable="true" />
<display:column title="Module" property="module" sortable="true" />
<display:column title="Methods" property="methods" sortable="true" />
<display:column title="Message" property="message" sortable="true" />
<display:column title="Created By" property="createdBy" sortable="true" />
<display:column title="Created On" property="createdOn" format="{0,date,dd-MMM-yyyy}" sortable="true" />
<display:setProperty name="export.excel.filename" value="ErrorLog List.xls"/>   
<display:setProperty name="export.csv.filename" value="ErrorLog List.csv"/>   
<display:setProperty name="export.pdf.filename" value="ErrorLog List.pdf"/>  
</display:table>
</s:form>
 <script type="text/javascript">	
	setCalendarFunctionality();
</script>
</body> 
