package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;
import java.util.List;
import com.trilasoft.app.model.SurveyEmailAudit;

public interface SurveyEmailAuditDao extends GenericDao<SurveyEmailAudit, Long>{
	public Long getSurveyEmailAuditSession(String shipNumber,String accountId,String corpId);
}
