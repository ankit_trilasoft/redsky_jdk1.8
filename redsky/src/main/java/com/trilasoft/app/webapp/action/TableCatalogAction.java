package com.trilasoft.app.webapp.action;

import static java.lang.System.out;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.TableCatalog;
import com.trilasoft.app.service.TableCatalogManager;




public class TableCatalogAction extends BaseAction implements Preparable {
	
	private Long id;	
	private TableCatalog tableCatalog;
	private TableCatalogManager tableCatalogManager;
	private String sessionCorpID;
	private List exportTableList;
	private String tableName;
	
	public void prepare() throws Exception { 
		
	}
	
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(TableCatalogAction.class);
	
    public TableCatalogAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}    
    
   	public String edit() { 
		if (id != null) {
	    	tableCatalog = tableCatalogManager.get(id);
		} else {
			tableCatalog = new TableCatalog(); 
			tableCatalog.setCreatedOn(new Date());
			tableCatalog.setUpdatedOn(new Date());
		}
		return SUCCESS;
	}
   	
   	private String tableN;
   	private String tableD;
   	public String searchTableCatalogRecord()
   	{
   		tableCatalogList=tableCatalogManager.getTableListValue(tableN,tableD);
   		return SUCCESS;
   	}
	public String save() throws Exception {
		
		boolean isNew = (tableCatalog.getId() == null);  
				if (isNew) { 
					tableCatalog.setCreatedOn(new Date());
					tableCatalog.setCreatedBy(getRequest().getRemoteUser());
		        }
		    	tableCatalog.setUpdatedOn(new Date());
		    	tableCatalog.setUpdatedBy(getRequest().getRemoteUser()); 
		    	tableCatalog=tableCatalogManager.save(tableCatalog);  
		    	String key = (isNew) ? "tableCatalog.added" : "tableCatalog.updated";
			    saveMessage(getText(key));
				return SUCCESS;
	     }
	private List tableCatalogList;
	public String list()
	{ 
		tableCatalogList=tableCatalogManager.getAll();
		return SUCCESS;
	}
	public String delete()
	{		
		tableCatalogManager.remove(id);    
	      return SUCCESS;		    
	}	
	
	public void exportTableCatalog()throws Exception{
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	exportTableList=tableCatalogManager.getData(tableName,sessionCorpID);
    	if(!exportTableList.isEmpty()){
    	HttpServletResponse response = getResponse();
    	response.setContentType("application/vnd.ms-excel");
    	ServletOutputStream outputStream = response.getOutputStream();
    	File file = new File("TableCatalogExtract");
    	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
    	response.setHeader("Pragma", "public");
    	response.setHeader("Cache-Control", "max-age=0");
    	out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + exportTableList.size() + "\t is extracted");   
    	outputStream.write("Description \t".getBytes());
    	outputStream.write("FieldName \t".getBytes());  
    	outputStream.write("createdBy \t".getBytes());
    	outputStream.write("createdOn \t".getBytes());
    	outputStream.write("updatedBy \t".getBytes());    	
    	outputStream.write("updatedOn \t".getBytes());
    	outputStream.write("ValueDate \t".getBytes());    	
    	outputStream.write("TableName \t".getBytes());
    	outputStream.write("RulesFiledName \t".getBytes());
    	outputStream.write("Editable \t".getBytes());
    	outputStream.write("CorpID \t".getBytes());
    	outputStream.write("Auditable \t".getBytes());
    	outputStream.write("IsdateField \t".getBytes());
    	outputStream.write("DefineByToDoRule \t".getBytes());
    	outputStream.write("Visible \t".getBytes());
    	outputStream.write("Charge \t".getBytes());
    	outputStream.write("UsDomestic \t".getBytes());
    	outputStream.write("ReferenceTable \t".getBytes());
    	outputStream.write("ParameterLinkingCode \t".getBytes());
    	outputStream.write("\r".getBytes());
    	
    	Iterator it = exportTableList.iterator();
    	while(it.hasNext()){
    		Object []data = (Object[]) it.next();
    		if(data[1]!=null){
    			outputStream.write((""+(data[1].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[2]!=null){
    			outputStream.write((""+(data[2].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[3]!=null){
    			outputStream.write((""+(data[3].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[4]!=null){
    			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder date = new StringBuilder(format.format(data[4]));
    			outputStream.write((""+(date.toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[5]!=null){
    			outputStream.write((""+(data[5].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[6]!=null){
    			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder date = new StringBuilder(format.format(data[6]));
    			outputStream.write((""+(date.toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[7]!=null){
    			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder date = new StringBuilder(format.format(data[7]));
    			outputStream.write((""+(date.toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[8]!=null){
    			outputStream.write((""+(data[8].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[9]!=null){
    			outputStream.write((""+(data[9].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		} 
    		if(data[11]!=null){
    			outputStream.write((""+(data[11].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[12]!=null){
    			outputStream.write((""+(data[12].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[13]!=null){
    			outputStream.write((""+(data[13].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[14]!=null){
    			outputStream.write((""+(data[14].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[15]!=null){
    			outputStream.write((""+(data[15].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[16]!=null){
    			outputStream.write((""+(data[16].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[17]!=null){
    			outputStream.write((""+(data[17].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[18]!=null){
    			outputStream.write((""+(data[18].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[19]!=null){
    			outputStream.write((""+(data[19].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(data[20]!=null){
    			outputStream.write((""+(data[20].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		
    		outputStream.write("\r\n".getBytes());
    	}
    	}
    	else{
    		HttpServletResponse response = getResponse();
			ServletOutputStream outputStream = response.getOutputStream();
			File file=new File("TableCatalogExtract");
			response.setContentType("application/vnd.ms-excel");
			//response.setHeader("Cache-Control", "no-cache");			
			response.setHeader("Content-Disposition", "attachment; filename="+file.getName()+".xls");			 
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			String header="NO Record Found , thanks";
			outputStream.write(header.getBytes());
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
       }

	
	public List getExportTableList() {
		return exportTableList;
	}

	public void setExportTableList(List exportTableList) {
		this.exportTableList = exportTableList;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public TableCatalog getTableCatalog() {
		return tableCatalog;
	}


	public void setTableCatalog(TableCatalog tableCatalog) {
		this.tableCatalog = tableCatalog;
	}


	public String getSessionCorpID() {
		return sessionCorpID;
	}


	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}


	public void setTableCatalogManager(TableCatalogManager tableCatalogManager) {
		this.tableCatalogManager = tableCatalogManager;
	}


	public List getTableCatalogList() {
		return tableCatalogList;
	}


	public void setTableCatalogList(List tableCatalogList) {
		this.tableCatalogList = tableCatalogList;
	}


	public String getTableN() {
		return tableN;
	}


	public void setTableN(String tableN) {
		this.tableN = tableN;
	}


	public String getTableD() {
		return tableD;
	}


	public void setTableD(String tableD) {
		this.tableD = tableD;
	}
	
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	}
