package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "vanlinegltype")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class VanLineGLType extends BaseObject implements ListLinkData{

	private String corpID;

	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;

	private Long id;

	private String glType;

	private String description;

	private String glCode;

	private Boolean doNotAuto;

	private Boolean calcByDrvr;

	private Boolean putGL;

	private Boolean useDrvrGL;

	private BigDecimal pc;

	private String bucket;

	private String invItem;

	private String calc;

	private String realCalc;

	private BigDecimal discrepancy;

	private BigDecimal shortHaul;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("createdBy", createdBy).append("updatedBy", updatedBy).append("createdOn", createdOn).append("updatedOn",
				updatedOn).append("id", id).append("glType", glType).append("description", description).append("glCode", glCode).append("doNotAuto", doNotAuto).append(
				"calcByDrvr", calcByDrvr).append("putGL", putGL).append("useDrvrGL", useDrvrGL).append("pc", pc).append("bucket", bucket).append("invItem", invItem).append("calc",
				calc).append("realCalc", realCalc).append("discrepancy", discrepancy).append("shortHaul", shortHaul).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof VanLineGLType))
			return false;
		VanLineGLType castOther = (VanLineGLType) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(createdBy, castOther.createdBy).append(updatedBy, castOther.updatedBy).append(createdOn,
				castOther.createdOn).append(updatedOn, castOther.updatedOn).append(id, castOther.id).append(glType, castOther.glType).append(description, castOther.description)
				.append(glCode, castOther.glCode).append(doNotAuto, castOther.doNotAuto).append(calcByDrvr, castOther.calcByDrvr).append(putGL, castOther.putGL).append(useDrvrGL,
						castOther.useDrvrGL).append(pc, castOther.pc).append(bucket, castOther.bucket).append(invItem, castOther.invItem).append(calc, castOther.calc).append(
						realCalc, castOther.realCalc).append(discrepancy, castOther.discrepancy).append(shortHaul, castOther.shortHaul).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(createdBy).append(updatedBy).append(createdOn).append(updatedOn).append(id).append(glType).append(description).append(
				glCode).append(doNotAuto).append(calcByDrvr).append(putGL).append(useDrvrGL).append(pc).append(bucket).append(invItem).append(calc).append(realCalc).append(
				discrepancy).append(shortHaul).toHashCode();
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 255)
	public String getGlType() {
		return glType;
	}

	public void setGlType(String glType) {
		this.glType = glType;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length = 100)
	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	@Column(length = 1000)
	public String getCalc() {
		return calc;
	}

	public void setCalc(String calc) {
		this.calc = calc;
	}

	@Column
	public Boolean getCalcByDrvr() {
		return calcByDrvr;
	}

	public void setCalcByDrvr(Boolean calcByDrvr) {
		this.calcByDrvr = calcByDrvr;
	}

	@Column(length = 255)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column
	public BigDecimal getDiscrepancy() {
		return discrepancy;
	}

	public void setDiscrepancy(BigDecimal discrepancy) {
		this.discrepancy = discrepancy;
	}
	
	@Column
	public Boolean getDoNotAuto() {
		return doNotAuto;
	}

	public void setDoNotAuto(Boolean doNotAuto) {
		this.doNotAuto = doNotAuto;
	}

	@Column(length = 20)
	public String getGlCode() {
		return glCode;
	}

	public void setGlCode(String glCode) {
		this.glCode = glCode;
	}
	
	@Column(length = 255)
	public String getInvItem() {
		return invItem;
	}

	public void setInvItem(String invItem) {
		this.invItem = invItem;
	}

	@Column(precision = 5, scale = 2)
	public BigDecimal getPc() {
		return pc;
	}

	public void setPc(BigDecimal pc) {
		this.pc = pc;
	}
	
	@Column
	public Boolean getPutGL() {
		return putGL;
	}

	public void setPutGL(Boolean putGL) {
		this.putGL = putGL;
	}

	@Column(length = 1000)
	public String getRealCalc() {
		return realCalc;
	}

	public void setRealCalc(String realCalc) {
		this.realCalc = realCalc;
	}

	@Column
	public BigDecimal getShortHaul() {
		return shortHaul;
	}

	public void setShortHaul(BigDecimal shortHaul) {
		this.shortHaul = shortHaul;
	}

	@Column
	public Boolean getUseDrvrGL() {
		return useDrvrGL;
	}

	public void setUseDrvrGL(Boolean useDrvrGL) {
		this.useDrvrGL = useDrvrGL;
	}
@Transient
	public String getListCode() {
		
		return glType;
	}
@Transient
	public String getListDescription() {
		
		return "";
	}
@Transient
	public String getListEigthDescription() {
		
		return "";
	}
@Transient
	public String getListFifthDescription() {
		
		return "";
	}
@Transient
	public String getListFourthDescription() {
		
		return "";
	}
@Transient
	public String getListNinthDescription() {
		
		return "";
	}
@Transient
	public String getListSecondDescription() {
		
		return "";
	}
@Transient
	public String getListSeventhDescription() {
		
		return "";
	}
@Transient
	public String getListSixthDescription() {
		
		return "";
	}
@Transient
	public String getListTenthDescription() {
		
		return "";
	}
@Transient
	public String getListThirdDescription() {
		
		return "";
	}

}
