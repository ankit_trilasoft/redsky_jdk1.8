package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;   
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.dao.hibernate.GenericDaoHibernate;   
import org.appfuse.model.User;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.MiscellaneousDao;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTOForToolTipRevenue;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.Miscellaneous;   
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.webapp.action.CommonUtil;
    
	public class MiscellaneousDaoHibernate extends GenericDaoHibernate<Miscellaneous, Long> implements MiscellaneousDao { 
		private Company company;
		private String sessionCorpID;
      public MiscellaneousDaoHibernate()
      {   
        super(Miscellaneous.class);   
    } 
      private HibernateUtil hibernateUtil;
  	
  	public void setHibernateUtil(HibernateUtil hibernateUtil){
          this.hibernateUtil = hibernateUtil;
  	}
  	public void getSessionCorpID()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();	
	}
  	
      public List<Miscellaneous> findBySequenceNumber(String sequenceNumber) {   
        return getHibernateTemplate().find("from Miscellaneous where sequenceNumber=?", sequenceNumber);   
    } 
      public List findMaximumId() {
      	return getHibernateTemplate().find("select max(id) from Miscellaneous");
      }
      public List checkById(Long id) {
      	return getHibernateTemplate().find("select id from Miscellaneous where id=?",id);
      }
	public void updateTrackingStatus(Date beginPacking, Date endPacking, Date packA, Date beginLoad, Date endLoad, Date loadA, Date deliveryShipper, Date deliveryLastDay, Date deliveryTA, Date deliveryA, Long id,String packATime,String deliveryATime,String deliveryTATime,String loadATime) {
		getSessionCorpID();
		String queryString = "update TrackingStatus set ";
		 String queryString1="";
		 String queryString2="";
		 String queryString3="";
		 String queryString4="";
		 String queryString5="";
		 String queryString6="";
		 String queryString7="";
		 String queryString8=""; 
		 String queryString9="";
		 String queryString0="";
		 String whereString="";
		 
		 SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
	      
		 if(beginPacking != null){
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(beginPacking) );
		      String beginPackingD = nowYYYYMMDD2.toString();
			 queryString1 = "beginPacking='"+beginPackingD+"', ";
		 }else{
			 queryString1 = "beginPacking= null, ";
		 }
		 
		 if(endPacking != null){
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(endPacking) );
		      String endPackingD = nowYYYYMMDD2.toString();
			 queryString2 = "endPacking='"+endPackingD+"', ";
		 }else{
			 queryString2 = "endPacking =  null, ";
		 }
		 List list=getSession().createQuery("from Company where corpID='"+sessionCorpID+"'").list();
		 if(list!=null && !list.isEmpty() && list.get(0)!=null){
			 company=(Company)list.get(0);
		 }
		 if(packA != null){
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(packA)+" "+packATime);
			 String packAD = nowYYYYMMDD2.toString();
			 if(company.getAllowFutureActualDateEntry()!=null && !company.getAllowFutureActualDateEntry()){
		      SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd"); 
				try {
					Date currentTimeZoneWiseDate =  dateformatYYYYMMDD.parse(packAD);
				} catch (Exception e) {
					e.printStackTrace();
				}
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				 String currentDate = dateformatYYYYMMDD1.format(date);
				 try {
					Date currentTimeZoneWiseCurrentDate =  dateformatYYYYMMDD1.parse(currentDate);
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					if(dateformatYYYYMMDD.parse(packAD).after(dateformatYYYYMMDD1.parse(currentDate))){
					
					// queryString3 = "packA = null, ";
               }
					else{
						 queryString3 = "packA='"+packAD+"', ";
					}
				} catch (Exception e) {

				} } }else{
					 queryString3 = "packA = null, ";
		 }
		 if(beginLoad != null){
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(beginLoad) );
		      String beginLoadD = nowYYYYMMDD2.toString();
			 queryString4 = "beginLoad='"+beginLoadD+"', ";
		 }else{
			 queryString4 = "beginLoad = null, ";
		 }
		 
		 if(endLoad != null){
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(endLoad) );
		      String endLoadD = nowYYYYMMDD2.toString();
			 queryString5 = "endLoad='"+endLoadD+"', ";
		 }else{
			 queryString5 = "endLoad = null, ";
		 }
		 if(loadA != null){
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(loadA)+" "+loadATime );
		      String loadAD = nowYYYYMMDD2.toString();
			 if(company.getAllowFutureActualDateEntry()!=null && !company.getAllowFutureActualDateEntry()){
		      SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd"); 
				try {
					Date currentTimeZoneWiseDate =  dateformatYYYYMMDD.parse(loadAD);
				} catch (Exception e) {
					e.printStackTrace();
				}
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				 String currentDate = dateformatYYYYMMDD1.format(date);
				 try {
					Date currentTimeZoneWiseCurrentDate =  dateformatYYYYMMDD1.parse(currentDate);
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					if(dateformatYYYYMMDD.parse(loadAD).after(dateformatYYYYMMDD1.parse(currentDate))){
					
						// queryString6 = "loadA =null, ";
               }
					else{
						queryString6 = "loadA='"+loadAD+"', ";
					}
				} catch (Exception e) {

				} } }else{
					 queryString6 = "loadA =null, ";
		 }
			 
		 if(deliveryShipper != null){
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(deliveryShipper) );
		      String deliveryShipperD = nowYYYYMMDD2.toString();
			 queryString7 = "deliveryShipper='"+deliveryShipperD+"', ";
		 }else{
			 queryString7 = "deliveryShipper = null, ";
		 }
		 if(deliveryLastDay != null){
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(deliveryLastDay) );
		      String deliveryLastDayD = nowYYYYMMDD2.toString();
			 queryString8 = "deliveryLastDay='"+deliveryLastDayD+"', ";
		 }else{
			 queryString8 = "deliveryLastDay = null, ";
		 }
		 if(deliveryTA != null){
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(deliveryTA)+" "+deliveryTATime);
		      String deliveryTAD = nowYYYYMMDD2.toString();
			 queryString9 = "deliveryTA='"+deliveryTAD+"', ";
		 }else{
			 queryString9 = "deliveryTA = null, ";
		 }
			 if(deliveryA != null){
				 if(company.getAllowFutureActualDateEntry()!=null && !company.getAllowFutureActualDateEntry()){
				 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(deliveryA)+" "+deliveryATime);
			      String deliveryAD = nowYYYYMMDD2.toString();
			      SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd"); 
					try {
						Date currentTimeZoneWiseDate =  dateformatYYYYMMDD.parse(deliveryAD);
						//System.out.println(currentTimeZoneWiseDate);
					} catch (Exception e) {
						e.printStackTrace();
					}
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					Date date = new Date();
					 String currentDate = dateformatYYYYMMDD1.format(date);
					 try {
						Date currentTimeZoneWiseCurrentDate =  dateformatYYYYMMDD1.parse(currentDate);
						//System.out.println(currentTimeZoneWiseCurrentDate);
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						if(dateformatYYYYMMDD.parse(deliveryAD).after(dateformatYYYYMMDD1.parse(currentDate))){
							 //queryString0 = "deliveryA = null, ";
						}
						else
						{
							queryString0 = "deliveryA='"+deliveryAD+"', ";
	                }
					} catch (Exception e) {

					} } }else{
				 queryString0 = "deliveryA = null, ";
			 }
		 
		/* if(deliveryA != null){
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(deliveryA)+" "+deliveryATime);
		      String deliveryAD = nowYYYYMMDD2.toString();
			 queryString0 = "deliveryA='"+deliveryAD+"', ";
		 }else{
			 queryString0 = "deliveryA = null, ";
		 }*/
		 
		 whereString = " where id='"+id+"'";
		 
		 String forwardingQuery = new StringBuilder(queryString).append(queryString1).append(queryString2).append(queryString3).append(queryString4).append(queryString5).append(queryString6).append(queryString7).append(queryString8).append(queryString9).append(queryString0).toString();
		 System.err.println("/n/n1--->>"+forwardingQuery);
		 forwardingQuery = forwardingQuery.substring(0, forwardingQuery.length()-2);
		 System.err.println("/n/n2--->>"+forwardingQuery);
		 forwardingQuery = new StringBuilder(forwardingQuery).append(whereString).toString();
		 System.err.println("/n/n3--->>"+forwardingQuery);
		 getHibernateTemplate().bulkUpdate(forwardingQuery);
		// beginPacking='"+beginPacking+"' , endPacking='"+endPacking+"', packA='"+packA+"', beginLoad='"+beginLoad+"', endLoad='"+endLoad+"', loadA='"+loadA+"' , deliveryShipper='"+deliveryShipper+"' , deliveryLastDay='"+deliveryLastDay+"' , deliveryTA='"+deliveryTA+"' , deliveryA='"+deliveryA+"' where id='"+id+"'");
	}
	public void updateBilling(Date date, Long id , String loginUser) {
		if(date != null){
			 SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(date) );
		      String fromatedDate = nowYYYYMMDD2.toString();
		      getHibernateTemplate().bulkUpdate("update Billing set revenueRecognition='"+fromatedDate+"' ,updatedOn=now(), updatedBy='"+loginUser+"' where id='"+id+"' and revenueRecognition is null ");
		     
		}
	}
	public void updateTrackingStatusDestination(Date sitDestinationIn,Long id){
		if(sitDestinationIn != null){
			 SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(sitDestinationIn) );
		      String fromatedDate = nowYYYYMMDD2.toString();
			getHibernateTemplate().bulkUpdate("update TrackingStatus set sitDestinationIn='"+fromatedDate+"' where id='"+id+"'");
		}else{
			getHibernateTemplate().bulkUpdate("update TrackingStatus set sitDestinationIn = null where id ='"+id+"'");
		}
	}
	public void updateTrackingStatussitDestinationA(Date sitDestinationA,Long id){
		if(sitDestinationA != null){
			 SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(sitDestinationA) );
		      String fromatedDate = nowYYYYMMDD2.toString();
			getHibernateTemplate().bulkUpdate("update TrackingStatus set sitDestinationA='"+fromatedDate+"' where id='"+id+"'");
		}else{
			getHibernateTemplate().bulkUpdate("update TrackingStatus set sitDestinationA = null where id ='"+id+"'");
		}
	}
	public void updateTrackingStatusDestinationTA(String sitDestinationTA,Long id){
			
		getHibernateTemplate().bulkUpdate("update TrackingStatus set sitDestinationTA='"+sitDestinationTA+"' where id='"+id+"'");
	}
	public void updateTrackingStatusDestinationDays(int sitDestinationDays,Long id){
		getHibernateTemplate().bulkUpdate("update TrackingStatus set sitDestinationDays='"+sitDestinationDays+"' where id='"+id+"'");
	}
	public class DTO{
		private Object id;
		private Object driver;
		private Object shiper;
		private Object job;
		private Object orderNo;
		private Object origin;
		private Object destination;
		private Object loadBeg;
		private Object loadEnd;
		private Object deliveryBeg;
		private Object deliveryEnd;
		private Object ETA;
		private Object driverPaper;
		private Object estWgt;
		private Object estimatedRevenue;
		private Object haulingAgentVanlineCode;
		private Object selfHaul;
		private Object fromArea;
		private Object toArea;
		private Object packAuthorize;
		private Object tarif;
		private Object shipmentRegistrationDate;
		private Object survey;
		private Object discount;
		private Object transportationDiscount;
		private Object tripNumber;
		private Object originAgentCode;
		private Object destinationAgentCode;
		private Object originAgentAddress;
		private Object destinationAgentAddress;
		private Object beginPacking;
		private Object endPacking;
		private Object loadCount;
		private Object deliveryA;
		private Object unPack;
		private Object vanID;
		private Object sitDestinationYN;
		private Object g11;
		private Object shipmentType;
		private Object originVanlineCode;
		private Object destinationVanlineCode;
		private Object jobStatus;
		
		public Object getFromArea() {
			return fromArea;
		}
		public void setFromArea(Object fromArea) {
			this.fromArea = fromArea;
		}
		public Object getToArea() {
			return toArea;
		}
		public void setToArea(Object toArea) {
			this.toArea = toArea;
		}
				
		public Object getHaulingAgentVanlineCode() {
			return haulingAgentVanlineCode;
		}
		public void setHaulingAgentVanlineCode(Object haulingAgentVanlineCode) {
			this.haulingAgentVanlineCode = haulingAgentVanlineCode;
		}
		public Object getSelfHaul() {
			return selfHaul;
		}
		public void setSelfHaul(Object selfHaul) {
			this.selfHaul = selfHaul;
		}
		public Object getDeliveryBeg() {
			return deliveryBeg;
		}
		public void setDeliveryBeg(Object deliveryBeg) {
			this.deliveryBeg = deliveryBeg;
		}
		public Object getDeliveryEnd() {
			return deliveryEnd;
		}
		public void setDeliveryEnd(Object deliveryEnd) {
			this.deliveryEnd = deliveryEnd;
		}
		public Object getDestination() {
			return destination;
		}
		public void setDestination(Object destination) {
			this.destination = destination;
		}
		public Object getDriver() {
			return driver;
		}
		public void setDriver(Object driver) {
			this.driver = driver;
		}
		public Object getDriverPaper() {
			return driverPaper;
		}
		public void setDriverPaper(Object driverPaper) {
			this.driverPaper = driverPaper;
		}
		public Object getEstWgt() {
			return estWgt;
		}
		public void setEstWgt(Object estWgt) {
			this.estWgt = estWgt;
		}
		public Object getETA() {
			return ETA;
		}
		public void setETA(Object eta) {
			ETA = eta;
		}
		public Object getJob() {
			return job;
		}
		public void setJob(Object job) {
			this.job = job;
		}
		public Object getLoadBeg() {
			return loadBeg;
		}
		public void setLoadBeg(Object loadBeg) {
			this.loadBeg = loadBeg;
		}
		public Object getLoadEnd() {
			return loadEnd;
		}
		public void setLoadEnd(Object loadEnd) {
			this.loadEnd = loadEnd;
		}
		public Object getOrderNo() {
			return orderNo;
		}
		public void setOrderNo(Object orderNo) {
			this.orderNo = orderNo;
		}
		public Object getOrigin() {
			return origin;
		}
		public void setOrigin(Object origin) {
			this.origin = origin;
		}
		public Object getShiper() {
			return shiper;
		}
		public void setShiper(Object shiper) {
			this.shiper = shiper;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getEstimatedRevenue() {
			return estimatedRevenue;
		}
		public void setEstimatedRevenue(Object estimatedRevenue) {
			this.estimatedRevenue = estimatedRevenue;
		}
		public Object getPackAuthorize() {
			return packAuthorize;
		}
		public void setPackAuthorize(Object packAuthorize) {
			this.packAuthorize = packAuthorize;
		}
		public Object getTarif() {
			return tarif;
		}
		public void setTarif(Object tarif) {
			this.tarif = tarif;
		}
		public Object getShipmentRegistrationDate() {
			return shipmentRegistrationDate;
		}
		public void setShipmentRegistrationDate(Object shipmentRegistrationDate) {
			this.shipmentRegistrationDate = shipmentRegistrationDate;
		}
		public Object getSurvey() {
			return survey;
		}
		public void setSurvey(Object survey) {
			this.survey = survey;
		}
		public Object getDiscount() {
			return discount;
		}
		public void setDiscount(Object discount) {
			this.discount = discount;
		}
		public Object getTransportationDiscount() {
			return transportationDiscount;
		}
		public void setTransportationDiscount(Object transportationDiscount) {
			this.transportationDiscount = transportationDiscount;
		}
		public Object getTripNumber() {
			return tripNumber;
		}
		public void setTripNumber(Object tripNumber) {
			this.tripNumber = tripNumber;
		}
		public Object getOriginAgentCode() {
			return originAgentCode;
		}
		public void setOriginAgentCode(Object originAgentCode) {
			this.originAgentCode = originAgentCode;
		}
		public Object getDestinationAgentCode() {
			return destinationAgentCode;
		}
		public void setDestinationAgentCode(Object destinationAgentCode) {
			this.destinationAgentCode = destinationAgentCode;
		}
	
		public Object getBeginPacking() {
			return beginPacking;
		}
		public void setBeginPacking(Object beginPacking) {
			this.beginPacking = beginPacking;
		}
		public Object getEndPacking() {
			return endPacking;
		}
		public void setEndPacking(Object endPacking) {
			this.endPacking = endPacking;
		}
		public Object getLoadCount() {
			return loadCount;
		}
		public void setLoadCount(Object loadCount) {
			this.loadCount = loadCount;
		}
		public Object getDeliveryA() {
			return deliveryA;
		}
		public void setDeliveryA(Object deliveryA) {
			this.deliveryA = deliveryA;
		}
		public Object getUnPack() {
			return unPack;
		}
		public void setUnPack(Object unPack) {
			this.unPack = unPack;
		}
		public Object getVanID() {
			return vanID;
		}
		public void setVanID(Object vanID) {
			this.vanID = vanID;
		}
		public Object getSitDestinationYN() {
			return sitDestinationYN;
		}
		public void setSitDestinationYN(Object sitDestinationYN) {
			this.sitDestinationYN = sitDestinationYN;
		}
		public Object getOriginAgentAddress() {
			return originAgentAddress;
		}
		public void setOriginAgentAddress(Object originAgentAddress) {
			this.originAgentAddress = originAgentAddress;
		}
		public Object getDestinationAgentAddress() {
			return destinationAgentAddress;
		}
		public void setDestinationAgentAddress(Object destinationAgentAddress) {
			this.destinationAgentAddress = destinationAgentAddress;
		}
		public Object getG11() {
			return g11;
		}
		public void setG11(Object g11) {
			this.g11 = g11;
		}
		public Object getShipmentType() {
			return shipmentType;
		}
		public void setShipmentType(Object shipmentType) {
			this.shipmentType = shipmentType;
		}
		public Object getOriginVanlineCode() {
			return originVanlineCode;
		}
		public void setOriginVanlineCode(Object originVanlineCode) {
			this.originVanlineCode = originVanlineCode;
		}
		public Object getDestinationVanlineCode() {
			return destinationVanlineCode;
		}
		public void setDestinationVanlineCode(Object destinationVanlineCode) {
			this.destinationVanlineCode = destinationVanlineCode;
		}
		public Object getJobStatus() {
			return jobStatus;
		}
		public void setJobStatus(Object jobStatus) {
			this.jobStatus = jobStatus;
		}
		@Override
		public String toString() {
			return  id + ", " + driver.toString().replace(","," ") + ","
			+ shiper.toString().replace(",", " ") + ", " + job + "," + orderNo
			+ ", " + origin.toString().replace(",", " ") + "," + destination.toString().replace(","," ")
			+ ", " + loadBeg + ", " + loadEnd
			+ ", " + deliveryBeg + ", "
			+ deliveryEnd + "," + ETA + ", "
			+ driverPaper + ", " + estWgt
			+ ", " + estimatedRevenue
			+ ", " + haulingAgentVanlineCode
			+ ", " + selfHaul + ", " + fromArea
			+ ", " + toArea + ", " + packAuthorize
			+ ", " + tarif + ", "
			+ shipmentRegistrationDate + ", " + survey
			+ ", " + discount + ", "
			+ transportationDiscount + ", " + tripNumber
			+ ", " + originAgentCode
			+ ", " + destinationAgentCode
			+ ", " + originAgentAddress.toString().replace(","," ")
			+ ", " + destinationAgentAddress.toString().replace(","," ")
			+ "," + beginPacking + ", "
			+ endPacking + ", " + loadCount + ", "
			+ deliveryA + ", " + unPack + ", " + vanID
			+ ", " + sitDestinationYN + ", " + g11
			+ ", " + shipmentType + ", "
			+ originVanlineCode + ", "
			+ destinationVanlineCode+ ", "
			+ jobStatus ;
		}
		
	}
	
	String dateTO;
	String dateFROM;
	public List findHaulingMgtList(Date fromdate, Date todate,String corpid,String filterdDrivers,String opsHubList,String driverAgency,String selfHaul,String fromArea, String toArea, String fromZipCodes, String toZipCodes, String fromRadius, String toRadius, String job,String filterdTripNumber,String JobStatus,String filterdRegumber) {
		List <Object> haulingMgtList = new ArrayList<Object>();
		if (!(fromdate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(fromdate));
			dateFROM = nowYYYYMMDD1.toString();
		}

		if (!(todate == null)) {
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(todate));
			dateTO = nowYYYYMMDD2.toString();
		}
		if(driverAgency.equalsIgnoreCase("")){
			driverAgency = "''";
		}
		String qString="";
		String qString1 = "";
		if(filterdDrivers.equalsIgnoreCase("Unassigned drivers")){
	    	qString=" and m.driverName='' ";
	    }
		if(filterdDrivers.equalsIgnoreCase("Our drivers")){
	    	qString1 = " inner JOIN  partnerpublic pp  ON pp.partnercode = m.driverid and pp.isOwnerOp is true and pp.corpID IN ('TSFT','"+corpid+"','"+hibernateUtil.getParentCorpID(corpid)+"')";
	    }
		if(filterdDrivers.equalsIgnoreCase("All")){
			qString = " and m.driverName !='' ";
		}
		if(fromArea==null || fromArea.equalsIgnoreCase("")){
			fromArea="";
		}
		if(toArea==null || toArea.equalsIgnoreCase("")){
			toArea="";
		}
		
		String query = "SELECT distinct if(m.driverName='' or m.driverName is null ,'',concat(UPPER(m.driverName),' # ',m.driverId)) ,CONCAT(s.lastName,',',s.firstName) ," +
	    "s.job , if(s.registrationNumber is null or s.registrationNumber='' ,s.shipNumber,s.registrationNumber) as registrationNumber ,CONCAT(s.originCity,',',s.originState) , CONCAT(s.destinationCity,',',s.destinationState) , " +
		"date(t.beginLoad),date(t.loadA),date(t.deliveryShipper),date(t.deliveryLastDay) ,date(t.deliveryTA), m.paperwork , " +
		"If(s.mode='AIR',m.estimateGrossWeight,m.estimatedNetWeight),s.id,m.estimatedRevenue,if(m.haulingAgentVanlineCode is null,''," +
		"m.haulingAgentVanlineCode),m.selfHaul,if(m.g11 is true,'Y','N'),s.shipmentType";   	
        query = query + ",r.HHDGArea as fromArea, r1.HHDGArea as toArea, m.packAuthorize, m.tarif, m.shipmentRegistrationDate, c.survey, b.discount, b.transportationDiscount,if(m.tripNumber is null,'',m.tripNumber),if(t.originAgentCode is null,'',"+
        "t.originAgentCode),if(t.destinationAgentCode is null,'',t.destinationAgentCode),CONCAT(p.billingcity,',',p.billingstate) as originAgentAddress,CONCAT(dp.billingcity,',',dp.billingstate) as destinationAgentAddress "+
        ",date(t.beginPacking),date(t.endPacking),m.loadCount,date(t.deliveryA),m.unPack,m.vanID,t.sitDestinationYN,if(t.originAgentVanlineCode is null,'',t.originAgentVanlineCode) as originVanlineCode," +
        "if(t.destinationAgentVanlineCode is null,'',t.destinationAgentVanlineCode) as destinationVanlineCode,s.status as JobStatus";
		query = query + " FROM ";
		query=query+" serviceorder s INNER JOIN  trackingstatus t ON s.id = t.id and t.corpID = '"+corpid+"' " +
		 " INNER JOIN miscellaneous m ON s.id = m.id and m.corpID = '"+corpid+"' " ;
		 if(!driverAgency.equals("''")){
		    	query=query+" and (upper(m.haulingAgentCode) in ("+driverAgency+") or upper(t.originAgentCode) in ("+driverAgency+") or upper( s.bookingAgentCode) in ("+driverAgency+"))  "; 
		   }
		 query=query+ " left outer join partnerpublic p on p.partnerCode=t.originAgentCode and p.corpID IN ('TSFT','"+corpid+"','"+hibernateUtil.getParentCorpID(corpid)+"') " +
		 " left outer join partnerpublic dp on dp.partnerCode=t.destinationAgentCode and dp.corpID IN ('TSFT','"+corpid+"','"+hibernateUtil.getParentCorpID(corpid)+"') " ;
	    
	    query=query + qString1+
	    " INNER JOIN customerfile c ON s.customerFileId = c.id and c.corpID = '"+corpid+"' INNER JOIN billing b ON s.id = b.id and b.corpID = '"+corpid+"' " ;
	    if((!selfHaul.equalsIgnoreCase("")) && (selfHaul.equalsIgnoreCase("S"))){
	    	query=query + " inner join companydivision hd on (hd.vanlinecode = (upper(m.haulingAgentCode)) or hd.bookingAgentCode = (upper(m.haulingAgentCode))) and hd.corpID = '"+corpid+"' and (hd.operationshub like '%"+opsHubList+"%')"+
			" inner join companydivision bd on (bd.vanlinecode = (upper(t.originAgentCode)) or bd.bookingAgentCode=(upper(t.originAgentCode))) and bd.corpID = '"+corpid+"' and (bd.operationshub like '%"+opsHubList+"%' )"+
		    " inner join companydivision od on (od.vanlinecode = (upper(s.bookingAgentCode)) or od.bookingAgentCode=(upper(s.bookingAgentCode))) and od.corpID = '"+corpid+"' and (od.operationshub like '%"+opsHubList+"%' ) ";
	    }else{
	    	query=query +  " left outer join companydivision hd on hd.vanlinecode = (upper(m.haulingAgentCode)) and hd.corpID = '"+corpid+"' and (hd.operationshub = '"+opsHubList+"')"+
		" left outer join companydivision bd on bd.vanlinecode = (upper(t.originAgentCode)) and bd.corpID = '"+corpid+"' and (bd.operationshub = '"+opsHubList+"' )"+
	    " left outer join companydivision od on od.vanlinecode = (upper(s.bookingAgentCode)) and od.corpID = '"+corpid+"' and (od.operationshub = '"+opsHubList+"' ) ";
	    }
		query=query+"left outer join  refzipcountyarea r ON r.zipCode=left(s.originZip,5)" +
	    " left outer join refzipcountyarea r1 ON r1.zipCode=left(s.destinationZip,5) ";
		query=query+" WHERE s.corpID = '"+corpid+"' " +
		//" AND s.status not in ('CLSD','CNCL')" + 			
		/*" AND s.commodity in ('HHG','HHG/A','AUTO') " +*/
		" AND t.beginLoad >= '"+dateFROM+"' " +
		" AND t.beginLoad <= '"+dateTO+"' " ;
		if((JobStatus.equals("ACTIVE")) && (!JobStatus.equals(""))){
	    	query=query+" AND  s.status not in ('DWNLD','HOLD','CLSD','CNCL')"; 
	    }else if((JobStatus.equals("HOLD")) && (!JobStatus.equals(""))){
		   query=query+ " AND s.status='HOLD'  ";
	    }else if((JobStatus.equals("DWLD")) && (!JobStatus.equals(""))){
		   query=query+ " AND s.status='DWNLD' ";
	    }else{
	    	query=query+"AND s.status not in ('CLSD','CNCL')";
	    }
		if(fromArea!=null && !fromArea.equalsIgnoreCase("")){query=query+" AND r.HHDGArea='"+fromArea+"' AND r.HHDGArea is not null";}
		if(toArea!=null && !toArea.equalsIgnoreCase("")){query=query+" AND r1.HHDGArea='"+toArea+"' AND r1.HHDGArea is not null";}
	/*	if((fromRadius==null || fromRadius.equalsIgnoreCase("")) && (toRadius==null || toRadius.equalsIgnoreCase(""))){
			query=query+" AND (s.originZip in ("+fromZipCodes+") AND s.destinationZip in ("+toZipCodes+")) ";	
		}else{*/
			if((fromZipCodes!=null) && (!(fromZipCodes.equalsIgnoreCase(""))) && ( (toZipCodes==null) || (toZipCodes.equalsIgnoreCase(""))))
			{query=query+" AND s.originZip in ("+fromZipCodes+")";}
			if(((fromZipCodes==null) || fromZipCodes.equalsIgnoreCase("")) && ((toZipCodes!=null) && (!(toZipCodes.equalsIgnoreCase("")))))
			{query=query+" AND s.destinationZip in ("+toZipCodes+")";}
			if((fromZipCodes!=null) && (!(fromZipCodes.equalsIgnoreCase(""))) && (toZipCodes!=null) && (!(toZipCodes.equalsIgnoreCase(""))))
			{query=query+" AND (s.originZip in ("+fromZipCodes+") AND s.destinationZip in ("+toZipCodes+"))";}		
		/*}*/
		if((!selfHaul.equalsIgnoreCase("")) && (!selfHaul.equalsIgnoreCase("All")) ){
		query=query+ " AND selfHaul='"+selfHaul+"' ";
		}
		if(selfHaul.equalsIgnoreCase("") || selfHaul==null && (!selfHaul.equalsIgnoreCase("All"))){
		query=query + " AND (selfHaul='' OR selfhaul is null) ";
		}
		
		if((!job.equals("''")) && (!job.equals(""))){
	    	query=query+" and s.job in ("+job+")  "; 
	   }else{
		   query=query+ " AND s.job IN ('MVL','UVL','VAI','VAF') ";
	   }
		if(filterdTripNumber!=null && !filterdTripNumber.equals("")){
			query=query+ " AND m.tripNumber like '"+filterdTripNumber+"%'";	
		}
		if(filterdRegumber!=null && !filterdRegumber.equals("")){
			filterdRegumber = CommonUtil.getRegistrationNum(filterdRegumber,"");
			query=query+ " AND s.registrationNumber like '"+filterdRegumber+"%'";	
		}
		query=query+ qString ;
	    query=query+ " order by UPPER(m.driverName) desc";
		System.out.println("\n\n\n\n\n\n\n\n\n\n query"+query);	    
		List haulingMgt = this.getSession().createSQLQuery(query).list();		
		DTO dTO;
		Iterator it=haulingMgt.iterator();
		while(it.hasNext()){
			Object []obj=(Object[]) it.next();
			dTO=new DTO();
			if(obj[0]!=null){
				dTO.setDriver(obj[0]);
			}else{
				dTO.setDriver(" ");
			}
			if(obj[1]!=null){
				dTO.setShiper(obj[1]);
			}else{
				dTO.setShiper(" ");
			}
			if(obj[2]!=null){
				dTO.setJob(obj[2]);
			}else{
				dTO.setJob(" ");
			}
			if(obj[3]!=null){
				dTO.setOrderNo(obj[3]);
			}else{
				dTO.setOrderNo(" ");
			}
			if(obj[4]!=null){
				dTO.setOrigin(obj[4]);
			}else{
				dTO.setOrigin(" ");
			}
			if(obj[5]!=null){
				dTO.setDestination(obj[5]);
			}else{
				dTO.setDestination(" ");
			}
			if(obj[6]!=null){
				dTO.setLoadBeg(obj[6]);
			}else{
				dTO.setLoadBeg(" ");
			}
			if(obj[7]!=null){
				dTO.setLoadEnd(obj[7]);
			}else{
				dTO.setLoadEnd(" ");
			}
			if(obj[8]!=null){
				dTO.setDeliveryBeg(obj[8]);
			}else{
				dTO.setDeliveryBeg(" ");
			}
			if(obj[9]!=null){
				dTO.setDeliveryEnd(obj[9]);
			}else{
				dTO.setDeliveryEnd(" ");
			}
			if(obj[10]!=null){
				dTO.setETA(obj[10]);
			}else{
				dTO.setETA(" ");
			}
			if(obj[11]!=null){
				dTO.setDriverPaper(obj[11]);
			}else{
				dTO.setDriverPaper(" ");
			}
			if(obj[12]!=null){
				dTO.setEstWgt(obj[12]);
			}else{
				dTO.setEstWgt("0.0");
			}
			if(obj[13]!=null){
				dTO.setId(obj[13]);
			}else{
				dTO.setId(" ");
			}
			if(obj[14]!=null){
				dTO.setEstimatedRevenue(obj[14]);
			}else{
				dTO.setEstimatedRevenue("0.0");
			}
			if(obj[15]!=null){
				dTO.setHaulingAgentVanlineCode(obj[15]);
			}else{
				dTO.setHaulingAgentVanlineCode("");
			}
			if(obj[16]!=null){
				dTO.setSelfHaul(obj[16]);
			}else{
				dTO.setSelfHaul("");
			}
			if(obj[19]!=null){
				dTO.setFromArea(obj[19]);
			}else{
				dTO.setFromArea("");
			}
			if(obj[20]!=null){
				dTO.setToArea(obj[20]);
			}else{
				dTO.setToArea("");
			}
			List packName=this.getSession().createSQLQuery("select r.description from refmaster r where r.code='"+obj[21]+"' and parameter='HOWPACK' ").addScalar("r.description", Hibernate.STRING).list();
			if(packName!=null && !packName.isEmpty()){
				dTO.setPackAuthorize(packName.get(0).toString());	
			}else{
				dTO.setPackAuthorize(obj[21]);	
			}
			if(obj[22]!=null){
				dTO.setTarif(obj[22]);
			}else{
				dTO.setTarif(" ");
			}
			if(obj[23]!=null){
				dTO.setShipmentRegistrationDate(obj[23]);
			}else{
				dTO.setShipmentRegistrationDate(" ");
			}
			if(obj[24]!=null){
				dTO.setSurvey(obj[24]);
			}else{
				dTO.setSurvey("");
			}
			
			if(obj[25]!=null){
				dTO.setDiscount(obj[25]);
			}else{
				dTO.setDiscount("0.00");
			}
			if(obj[26]!=null){
				dTO.setTransportationDiscount(obj[26]);
			}else{
				dTO.setTransportationDiscount("0.00");
			}
			if(obj[27]!=null){
				dTO.setTripNumber(obj[27]);
			}else{
				dTO.setTripNumber(" ");
			}
			if(obj[28]!=null){
				dTO.setOriginAgentCode(obj[28]);
			}else{
				dTO.setOriginAgentCode(" ");
			}
			if(obj[29]!=null){
				dTO.setDestinationAgentCode(obj[29]);
			}else{
				dTO.setDestinationAgentCode(" ");
			}
			if(obj[30]!=null){
				dTO.setOriginAgentAddress(obj[30]);	
			}else{
				dTO.setOriginAgentAddress(" ");
			}
			if(obj[31]!=null){
				dTO.setDestinationAgentAddress(obj[31]);
			}else{
				dTO.setDestinationAgentAddress(" ");	
			}
			if(obj[32]!=null){
				dTO.setBeginPacking(obj[32]);
			}else{
				dTO.setBeginPacking(" ");
			}
			if(obj[33]!=null){
				dTO.setEndPacking(obj[33]);
			}else{
				dTO.setEndPacking(" ");
			}
			if(obj[34]!=null){
				dTO.setLoadCount(obj[34]);
			}else{
				dTO.setLoadCount(" ");
			}
			if(obj[35]!=null){
				dTO.setDeliveryA(obj[35]);
			}else{
				dTO.setDeliveryA(" ");
			}
			List unpackName=this.getSession().createSQLQuery("select r.description from refmaster r where r.code='"+obj[36]+"' and parameter='HOWPACK' ").addScalar("r.description", Hibernate.STRING).list();
			if(unpackName!=null && !unpackName.isEmpty()){
				dTO.setUnPack(unpackName.get(0).toString());
			}else{
				dTO.setUnPack(obj[36]);	
			}
			if(obj[37]!=null){
				dTO.setVanID(obj[37]);
			}else{
				dTO.setVanID(" ");
			}
			if(obj[38]!=null){
				dTO.setSitDestinationYN(obj[38]);
			}else{
				dTO.setSitDestinationYN(" ");
			}
			if(obj[17]!=null){
				dTO.setG11(obj[17]);
			}else{
				dTO.setG11(" ");
			}
			if(obj[18]!=null){
				dTO.setShipmentType(obj[18]);
			}else{
				dTO.setShipmentType(" ");
			}
			if(obj[39]!=null){
				dTO.setOriginVanlineCode(obj[39]);
			}else{
				dTO.setOriginVanlineCode(" ");
			}
			if(obj[40]!=null){
				dTO.setDestinationVanlineCode(obj[40]);
			}else{
				dTO.setDestinationVanlineCode(" ");
			}
			if(obj[41]!=null){
				dTO.setJobStatus(obj[41]);
			}else{
				dTO.setJobStatus(" ");
			}
			haulingMgtList.add(dTO);
		}
	return  haulingMgtList;
	}
	public List findByOwner(String partnerCode,String corpId) { 
		return getHibernateTemplate().find("select lastName from Partner where isOwnerOp is true and status='Approved' and partnerCode='"+partnerCode+"'");
	}
	public List findByOwnerAndContact(String partnerCode,String corpId) { 
		return getHibernateTemplate().find("select concat(firstName,'#',lastName,'#',billingPhone) from Partner where isOwnerOp is true and status='Approved' and partnerCode='"+partnerCode+"'");
	}
	public List findByOwnerFromTruck(String partnerCode,String truckNumber,String corpId){
		return this.getSession().createSQLQuery("select concat(if(p.firstName is null,'',p.firstName),'#',if(p.lastName is null,'',p.lastName),'#',if(p.billingPhone is null,'',p.billingPhone)) from truckdrivers t,partner p where t.driverCode= p.partnerCode and t.driverCode='"+partnerCode+"' and t.truckNumber like  '%"+truckNumber+"%'  and t.corpID='"+corpId+"' and  p.partnerCode='"+partnerCode+"' and p.corpID='"+corpId+"' union select concat(if(p.firstName is null,'',p.firstName),'#',if(p.lastName is null,'',p.lastName),'#',if(p.billingPhone is null,'',p.billingPhone)) from truckdrivers t,partner p where  p.partnerCode='"+partnerCode+"' and p.corpID='"+corpId+"' group by p.partnerCode;").list();
	}
	public List findByOwnerPerticularTruck(String truckNumber,String corpId){
		return this.getSession().createSQLQuery("select concat(if(t.driverCode is null,'',t.driverCode),'#',if(p.firstName is null,'',p.firstName),'#',if(p.lastName is null,'',p.lastName),'#',if(p.billingPhone is null,'',p.billingPhone)) from truckdrivers t,partner p where t.driverCode= p.partnerCode and t.truckNumber ='"+truckNumber+"' and t.primaryDriver=true and t.corpID='"+corpId+"' and p.corpID='"+corpId+"';").list();
	}
	public List findByTrailerPerticularTruck(String truckNumber,String corpId){
		return this.getSession().createSQLQuery("select trailerCode from trucktrailers where truckNumber='"+truckNumber+"' and corpID='"+corpId+"' and primaryTrailer=true ").list();
	}
	public void updateMiscellaneous(Long id,String status){
		getHibernateTemplate().bulkUpdate("update Miscellaneous set status='"+status+"' where id='"+id+"' ");
	}
	public Map<String, String> getDriverAgency(String corpid){
		List<CompanyDivision> list;
		list= getHibernateTemplate().find("from CompanyDivision where corpID='"+corpid+"' and vanLineCode is not null and vanLineCode!=''");
		Map<String, String> driverAgencyMap = new LinkedHashMap<String, String>();
		for (CompanyDivision companyDivision : list) {
			driverAgencyMap.put(companyDivision.getBookingAgentCode(),companyDivision.getVanLineCode());
		}
		return driverAgencyMap;
	}
	public void updateTrackingStatusLateReason(String partyResponsible,String lateReason, Long id){
		getHibernateTemplate().bulkUpdate("update TrackingStatus set partyResponsible='"+partyResponsible+"',lateReason='"+lateReason+"' where id='"+id+"' ");	
	}
	public void updatePackingDriverId(String partnerCode, String service,String shipNumber, String sessionCorpID) {
		String packingDriverId = this.getSession().createSQLQuery("select if(packingDriverId is null or packingDriverId='','1',packingDriverId) from miscellaneous where shipNumber = '"+shipNumber+"' ").list().get(0).toString();
		String loadingDriverId = this.getSession().createSQLQuery("select if(loadingDriverId is null or loadingDriverId='','1',loadingDriverId) from miscellaneous where shipNumber = '"+shipNumber+"' ").list().get(0).toString();
		String deliveryDriverId = this.getSession().createSQLQuery("select if(deliveryDriverId is null or deliveryDriverId='','1',deliveryDriverId) from miscellaneous where shipNumber = '"+shipNumber+"' ").list().get(0).toString();
		String queryString="";
		if(service.equalsIgnoreCase("PK") && (packingDriverId.equalsIgnoreCase("1"))){
			queryString = "update Miscellaneous set packingDriverId = '"+partnerCode+"' where shipNumber = '"+shipNumber+"' ";
			getHibernateTemplate().bulkUpdate(queryString);
		}
		if(service.equalsIgnoreCase("LD")  && (loadingDriverId.equalsIgnoreCase("1"))){
			queryString = "update Miscellaneous set loadingDriverId = '"+partnerCode+"' where shipNumber = '"+shipNumber+"' ";
			getHibernateTemplate().bulkUpdate(queryString);
		}
		if(service.equalsIgnoreCase("DL")  && (deliveryDriverId.equalsIgnoreCase("1"))){
			queryString = "update Miscellaneous set deliveryDriverId = '"+partnerCode+"' where shipNumber = '"+shipNumber+"' ";
			getHibernateTemplate().bulkUpdate(queryString);
		}
		if(service.equalsIgnoreCase("PL")  && (packingDriverId.equalsIgnoreCase("1"))){
			queryString = "update Miscellaneous set packingDriverId = '"+partnerCode+"' where shipNumber = '"+shipNumber+"' ";
			getHibernateTemplate().bulkUpdate(queryString);
		}
		if(service.equalsIgnoreCase("PL")  && (loadingDriverId.equalsIgnoreCase("1"))){
			queryString = "update Miscellaneous set loadingDriverId = '"+partnerCode+"' where shipNumber = '"+shipNumber+"' ";
			getHibernateTemplate().bulkUpdate(queryString);
		}
	}
	public List partyResponsible(String corpId, String spNumber){
		List tempList=new ArrayList();
		List list=this.getSession().createSQLQuery("select distinct concat(if(t.originAgentCode is null or t.originAgentCode='','1',t.originAgentCode),'~', " +
				"if(t.destinationAgentcode is null or t.destinationAgentcode='','1',t.destinationAgentcode),'~', " +
				"if(m.haulingAgentCode is null or m.haulingAgentCode='','1',m.haulingAgentCode),'~', " +
				"if(t.brokerCode is null or t.brokerCode='','1',t.brokerCode),'~', " +
				"if(t.forwarderCode is null or t.forwarderCode='','1',t.forwarderCode),'~', " +
				"if(s.bookingAgentCode is null or s.bookingAgentCode='','1',s.bookingAgentCode)) " +
				"from trackingstatus t,miscellaneous m,serviceorder s where t.id=m.id and t.corpid='"+corpId+"' " +
				"and m.corpid='"+corpId+"' and s.corpid='"+corpId+"'  and t.shipnumber='"+spNumber+"' and s.id=t.id;").list();
		if((list!=null)&&(!(list.isEmpty()) )&& (list.get(0)!=null)){
		String data =list.get(0).toString();	
		String dataArray[]=data.split("~");
		for(int i=0;i<dataArray.length;i++){
			String value=dataArray[i];
			if(!(value.equalsIgnoreCase("1"))){
				if(tempList.contains(value)){}else{tempList.add(value);}
			}
		}
		}
		return tempList;
	}
	
	public String weightForAmountDistribution(Long miscId,String grpId, String sessionCorpId){
		String query1="";
		String query2="";
		List childWeights=new ArrayList();
		List masterWeights=new ArrayList();
		String result="";
		List defaultUnit=getHibernateTemplate().find("select volumeUnit from SystemDefault where corpId='"+sessionCorpId+"'");
		 if(defaultUnit!=null && !(defaultUnit.isEmpty())){
	        	if(defaultUnit.get(0).toString().equalsIgnoreCase("Cft")){
		 query1="select if(actualCubicFeet is null or actualCubicFeet='' or actualCubicFeet='0.00',if(netActualCubicFeet is null or netActualCubicFeet='' or netActualCubicFeet='0.00',0.00,netActualCubicFeet),actualCubicFeet)" +
				" from miscellaneous where id="+miscId+"";
		 query2="select if(actualCubicFeet is null or actualCubicFeet='' or actualCubicFeet='0.00',0.00,actualCubicFeet)" +
		" from miscellaneous where id="+grpId+"";
	        	}
	        	else{
	        		
	        		query1="select if(actualCubicMtr is null or actualCubicMtr='' or actualCubicMtr='0.00',if(netActualCubicMtr is null or netActualCubicMtr='' or netActualCubicMtr='0.00',0.00,netActualCubicMtr),actualCubicMtr)" +
					" from miscellaneous where id="+miscId+"";
			 query2="select if(actualCubicMtr is null or actualCubicMtr='' or actualCubicMtr='0.00',0.00,actualCubicMtr)" +
			" from miscellaneous where id="+grpId+"";
	        		}
	        	childWeights=this.getSession().createSQLQuery(query1).list();
	        	masterWeights=this.getSession().createSQLQuery(query2).list();
	        	if(childWeights!=null && !(childWeights.isEmpty())){
	        		result=result+childWeights.get(0).toString();
	        	}
	        	if(masterWeights!=null && !(masterWeights.isEmpty())){
	        		result=result+"#"+masterWeights.get(0).toString();
	        	}
	        	
		 }
		 else{
			 result="";
		 }
	  return result;      	
	}
	
	public String findMasterOrderWeight(String id,String corpId){
		String query1="";
		List volumeList=new ArrayList();
		String result="";
		List defaultUnit=getHibernateTemplate().find("select volumeUnit from SystemDefault where corpId='"+corpId+"'");
		 if(defaultUnit!=null && !(defaultUnit.isEmpty())){
	        	if(defaultUnit.get(0).toString().equalsIgnoreCase("Cft")){
	        		 query1="select if(actualCubicFeet is null or actualCubicFeet='' or actualCubicFeet='0.00',estimateCubicFeet,actualCubicFeet)" +
	 				" from miscellaneous where id="+id+"";
	        		
	        	}
                else{
	        		
	        		query1="select if(actualCubicMtr is null or actualCubicMtr='' or actualCubicMtr='0.00',estimateCubicMtr,actualCubicMtr)" +
					" from miscellaneous where id="+id+"";
               }
	        	volumeList=this.getSession().createSQLQuery(query1).list();
	        	
		 }
		 if(volumeList!=null && !(volumeList.isEmpty()) && (volumeList.get(0)!=null)){
			 result=volumeList.get(0).toString();
		 }
		 else{
			 result=""; 
		 }
		 return result;
	}
	public List findZipCodeList(String sessionCorpID, String lattitude1,String lattitude2, String longitude1, String longitude2){
		String query="";
		if(lattitude2.equalsIgnoreCase("") && longitude2.equalsIgnoreCase("")){
			query = "select distinct(zipcode) from RefZipGeoCodeMap where latitude = "+lattitude1+" and longitude = "+longitude1+" and country='USA' and corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"')  ";
		}else{
			query = "select distinct(zipcode) from RefZipGeoCodeMap where latitude >= "+lattitude2+" and latitude <= "+lattitude1+" and longitude >= "+longitude2+" and longitude <= "+longitude1+" and country='USA' and corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"')  ";
		}
		return getHibernateTemplate().find(query);
	}
	public List findLattLongList(String sessionCorpID, String zipCode) {
		String query = "select distinct(concat(if(latitude is null or latitude='','',latitude),'#',if(longitude is null or longitude='','',longitude))) from refzipgeocodemap where zipcode = '"+zipCode+"' and country='USA' and corpID in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') ";
		return this.getSession().createSQLQuery(query).list();
	}
	public void updateTripNumber(String tripNumber,String tripid){
		tripid = tripid.trim();
		if (tripid.indexOf(",") == 0) {
			tripid = tripid.substring(1);
		}
		if (tripid.lastIndexOf(",") == tripid.length() - 1) {
			tripid = tripid.substring(0, tripid.length() - 1);
		}
		String[] arrayid = tripid.split(",");
		int arrayLength = arrayid.length;
		for (int i = 0; i < arrayLength; i++) {
			Long id = Long.parseLong(arrayid[i]);
		getHibernateTemplate().bulkUpdate("update Miscellaneous set tripNumber='"+tripNumber+"' where id ="+id+" ");
		}
	}
	public void removeTripNumber(String tripid){
		getHibernateTemplate().bulkUpdate("update Miscellaneous set tripNumber='',driverId='',driverName='' where id ="+tripid+" ");
	}
	public List getTripOrder(String tripNumber, String sessionCorpID){
		return getHibernateTemplate().find("from Miscellaneous where tripNumber='"+tripNumber+"' and corpID='"+sessionCorpID+"' ");
	}
	public List findmaxTripNumber(String sessionCorpID){
		//return this.getSession().createSQLQuery("select max(tripNumber) from miscellaneous where corpID='"+sessionCorpID+"'").list();
		return getHibernateTemplate().find("select max(tripNumber) from Miscellaneous where corpID='"+sessionCorpID+"' ");
	}
	public class TripNumberDTO{
		private Object tripEstimatedRevenue;
		private Object tripEstimateGrossWeight;
		public Object getTripEstimatedRevenue() {
			return tripEstimatedRevenue;
		}
		public void setTripEstimatedRevenue(Object tripEstimatedRevenue) {
			this.tripEstimatedRevenue = tripEstimatedRevenue;
		}
		public Object getTripEstimateGrossWeight() {
			return tripEstimateGrossWeight;
		}
		public void setTripEstimateGrossWeight(Object tripEstimateGrossWeight) {
			this.tripEstimateGrossWeight = tripEstimateGrossWeight;
		}
	}
	
	public List getTripNumber(String tripNumber, String sessionCorpID){
		List temp=new ArrayList();
		List tripList=new ArrayList();
		temp=this.getSession().createSQLQuery("SELECT sum(estimatedRevenue) as tripEstimatedRevenue  ,sum(if(estimatedNetWeight is null,'0',estimatedNetWeight)) as tripEstimateGrossWeight  FROM miscellaneous  where  tripNumber='"+tripNumber+"' and corpID='"+sessionCorpID+"' ")
		            .addScalar("tripEstimatedRevenue",Hibernate.STRING)
		            .addScalar("tripEstimateGrossWeight", Hibernate.STRING).list();
		Iterator trip=temp.iterator();
		TripNumberDTO tripDto=null;
		while(trip.hasNext()){
			Object [] row=(Object[])trip.next();
			tripDto = new TripNumberDTO();	
			tripDto.setTripEstimatedRevenue(row[0]);
			tripDto.setTripEstimateGrossWeight(row[1]);
			tripList.add(tripDto);
			
		}
		return tripList;
	}
	public List findTripName(String sessionCorpID){
		
		return getHibernateTemplate().find("select lastTripNumber from Company where corpID='"+sessionCorpID+"' ");
		
	}
	/*public List getOpenTripList(String tripNumber, String sessionCorpID){
		
		return getHibernateTemplate().find("select lastTripNumber from Company where corpID='"+sessionCorpID+"' ");	
	}*/
	public void updateJobStatus(String sessionCorpID,String soStatus,String orderNo){
		String suborderNo=orderNo.substring(0, 5);
		if(suborderNo.equalsIgnoreCase(sessionCorpID)){
			//getHibernateTemplate().bulkUpdate("update ServiceOrder set status='"+soStatus+"' where shipNumber='"+orderNo+"'");
		}else{
			//getHibernateTemplate().bulkUpdate("update ServiceOrder set status='"+soStatus+"' where registrationNumber='"+orderNo+"'");
		}		
	}
	public  class DTOForToolTipVehicle{
		private Object vehicleList;

		public Object getVehicleList() {
			return vehicleList;
		}

		public void setVehicleList(Object vehicleList) {
			this.vehicleList = vehicleList;
		}
		
	 }
	public List findToolTipForVehicle(String orderNo, String sessionCorpID, String job){
		String query="";
		query="select year, make, model from vehicle v, serviceorder s where v.status = true and  s.id=v.serviceorderid and s.id='"+orderNo+"' and s.corpId='"+sessionCorpID+"' and s.job='"+job+"'";
		 List list= this.getSession().createSQLQuery(query).list();
	  		List <Object> vehicleList = new ArrayList<Object>();
				 Iterator it=list.iterator();
				 DTOForToolTipVehicle dTOForToolTip=null;
				  while(it.hasNext()) 
				  {
					  Object [] row=(Object[])it.next();					 
					 
						  dTOForToolTip=new DTOForToolTipVehicle();						 
						  dTOForToolTip.setVehicleList("Year : " +row[0]);
						  vehicleList.add(dTOForToolTip);
						  
						  dTOForToolTip=new DTOForToolTipVehicle(); 
						  dTOForToolTip.setVehicleList("Make : " +row[1]);
						  vehicleList.add(dTOForToolTip);
						  
						  dTOForToolTip=new DTOForToolTipVehicle(); 
						  dTOForToolTip.setVehicleList("Model : " +row[2]);
						  vehicleList.add(dTOForToolTip);
				  }
		 return vehicleList;
	 }
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void updateTargetRevenueRecognition(Date deliveryA, Long id, String loginUser) {
		if(deliveryA != null){/*
			 SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(deliveryA) );
		      String fromatedDate = nowYYYYMMDD2.toString();
		      getHibernateTemplate().bulkUpdate("update ServiceOrder set targetRevenueRecognition='"+fromatedDate+"' ,updatedOn=now(), updatedBy='"+loginUser+"' where id='"+id+"'");
		     
		*/}
	}
}
