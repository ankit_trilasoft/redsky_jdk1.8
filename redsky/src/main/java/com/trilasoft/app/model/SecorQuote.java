package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="secorquote")
public class SecorQuote extends BaseObject{
	private Long id;
	private String corpID;
	private String customerName;
	private String originCountry;
	private String originCity;
	private String originState;
	private String originZip;
	private String destinationCountry;
	private String destinationCity;
	private String destinationState;
	private String destinationZip;
	private String mode;
	private Date moveDate;
	private String pickUpPoint;
	private String facilityPoint;
	private String deliveryPoint;
	private Double quoteAmount;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("customerName", customerName).append(
				"originCountry", originCountry)
				.append("originCity", originCity).append("originState",
						originState).append("originZip", originZip).append(
						"destinationCountry", destinationCountry).append(
						"destinationCity", destinationCity).append(
						"destinationState", destinationState).append(
						"destinationZip", destinationZip).append("mode", mode)
				.append("moveDate", moveDate)
				.append("pickUpPoint", pickUpPoint).append("facilityPoint",
						facilityPoint).append("deliveryPoint", deliveryPoint)
				.append("quoteAmount", quoteAmount).append("createdBy",
						createdBy).append("updatedBy", updatedBy).append(
						"createdOn", createdOn).append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof SecorQuote))
			return false;
		SecorQuote castOther = (SecorQuote) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(customerName, castOther.customerName)
				.append(originCountry, castOther.originCountry).append(
						originCity, castOther.originCity).append(originState,
						castOther.originState).append(originZip,
						castOther.originZip).append(destinationCountry,
						castOther.destinationCountry).append(destinationCity,
						castOther.destinationCity).append(destinationState,
						castOther.destinationState).append(destinationZip,
						castOther.destinationZip).append(mode, castOther.mode)
				.append(moveDate, castOther.moveDate).append(pickUpPoint,
						castOther.pickUpPoint).append(facilityPoint,
						castOther.facilityPoint).append(deliveryPoint,
						castOther.deliveryPoint).append(quoteAmount,
						castOther.quoteAmount).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				customerName).append(originCountry).append(originCity).append(
				originState).append(originZip).append(destinationCountry)
				.append(destinationCity).append(destinationState).append(
						destinationZip).append(mode).append(moveDate).append(
						pickUpPoint).append(facilityPoint)
				.append(deliveryPoint).append(quoteAmount).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.toHashCode();
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	@Column
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	@Column
	public String getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	@Column
	public String getDestinationState() {
		return destinationState;
	}
	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}
	@Column
	public String getDestinationZip() {
		return destinationZip;
	}
	public void setDestinationZip(String destinationZip) {
		this.destinationZip = destinationZip;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	@Column
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	@Column
	public String getOriginState() {
		return originState;
	}
	public void setOriginState(String originState) {
		this.originState = originState;
	}
	@Column
	public String getOriginZip() {
		return originZip;
	}
	public void setOriginZip(String originZip) {
		this.originZip = originZip;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getDeliveryPoint() {
		return deliveryPoint;
	}
	public void setDeliveryPoint(String deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}
	@Column
	public String getFacilityPoint() {
		return facilityPoint;
	}
	public void setFacilityPoint(String facilityPoint) {
		this.facilityPoint = facilityPoint;
	}
	@Column
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	@Column
	public Date getMoveDate() {
		return moveDate;
	}
	public void setMoveDate(Date moveDate) {
		this.moveDate = moveDate;
	}
	@Column
	public String getPickUpPoint() {
		return pickUpPoint;
	}
	public void setPickUpPoint(String pickUpPoint) {
		this.pickUpPoint = pickUpPoint;
	}
	public Double getQuoteAmount() {
		return quoteAmount;
	}
	public void setQuoteAmount(Double quoteAmount) {
		this.quoteAmount = quoteAmount;
	}	
	
}
