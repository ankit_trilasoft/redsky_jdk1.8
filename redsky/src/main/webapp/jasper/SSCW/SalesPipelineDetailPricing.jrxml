<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="SalesPipelineDetailPricing"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="1133"
		 pageHeight="612"
		 columnWidth="1113"
		 columnSpacing="0"
		 leftMargin="2"
		 rightMargin="18"
		 topMargin="18"
		 bottomMargin="18"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="begindate" isForPrompting="true" class="java.util.Date"/>
	<parameter name="enddate" isForPrompting="true" class="java.util.Date"/>
	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT "SalesPipeLineDetailPricing-R-EXTRACT-SSCW.jrxml",
	CONCAT(app_user1.`first_name`," ",app_user1.`last_name`) AS 'Person Pricing',
	customerfile.`comptetive` AS 'Competitive',
	customerfile.`job` AS 'Job',
	Date(customerfile.createdon) AS 'Created ON',
	customerfile.`salesStatus` AS 'Sales Status',
	Date(customerfile.`priceSubmissionToTranfDate`) AS 'Price Submission Transferee',
	Date(customerfile.`quoteAcceptenceDate`) AS 'Quote Acceptance',
	Date(customerfile.`moveDate`) AS 'Move Date',
	concat(if(customerfile.lastName is null || trim(customerfile.lastName)='','',trim(customerfile.lastName)),"",if(customerfile.firstName is null || trim(customerfile.firstName)='','',if(customerfile.lastName is null || trim(customerfile.lastName)='',trim(customerfile.firstName),CONCAT(", ",trim(customerfile.firstName)))))AS 'Customer Name',
	trim(customerfile.`origincity`) AS 'Origin City',
	customerfile.`originstate` AS 'Origin State',
	customerfile.`origincountry` AS 'Origin Country',
	trim(customerfile.`destinationcity`) AS 'Destination City',
	customerfile.`destinationstate` AS 'Destination State',
	customerfile.`destinationCountry` AS 'Destination Country',
	customerfile.`sequenceNumber` AS 'Order Number'
FROM `customerfile` customerfile 
	LEFT OUTER JOIN `app_user` app_user1 ON customerfile.`personPricing` = app_user1.`username` AND app_user1.`corpID`= $P{Corporate ID}
WHERE customerfile.`corpID` =$P{Corporate ID}
	AND customerfile.`status` <>'DWNLD'
	AND (customerfile.status <> 'CNCL' OR customerfile.statusReason <> 'CANCELLED')
	AND (date(customerfile.`createdon`) between $P{begindate}  and $P{enddate})
ORDER BY 'Person Pricing', 'Competitive', 'Sales Status', 'Survey Date', 'Move Date']]></queryString>

	<field name="SalesPipeLineDetailPricing-R-EXTRACT-SSCW.jrxml" class="java.lang.String"/>
	<field name="Person Pricing" class="java.lang.String"/>
	<field name="Competitive" class="java.lang.String"/>
	<field name="Job" class="java.lang.String"/>
	<field name="Created ON" class="java.sql.Date"/>
	<field name="Sales Status" class="java.lang.String"/>
	<field name="Price Submission Transferee" class="java.sql.Date"/>
	<field name="Quote Acceptance" class="java.sql.Date"/>
	<field name="Move Date" class="java.sql.Date"/>
	<field name="Customer Name" class="java.lang.String"/>
	<field name="Origin City" class="java.lang.String"/>
	<field name="Origin State" class="java.lang.String"/>
	<field name="Origin Country" class="java.lang.String"/>
	<field name="Destination City" class="java.lang.String"/>
	<field name="Destination State" class="java.lang.String"/>
	<field name="Destination Country" class="java.lang.String"/>
	<field name="Order Number" class="java.lang.String"/>

	<sortField name="Person Pricing" />
	<sortField name="Competitive" />
	<sortField name="Sales Status" />
	<sortField name="Created ON" />
	<sortField name="Move Date" />


		<group  name="estimator" >
			<groupExpression><![CDATA[$F{Person Pricing}]]></groupExpression>
			<groupHeader>
			<band height="20"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="59"
						y="7"
						width="0"
						height="0"
						key="staticText-110"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[Static Text]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="1"
						width="95"
						height="18"
						key="textField-9"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Pricing person : "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="97"
						y="1"
						width="260"
						height="18"
						forecolor="#990033"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Person Pricing}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="58"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="0"
						width="1112"
						height="19"
						key="staticText-118"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Sales Pipeline Detail Pricing]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="75"
						y="40"
						width="65"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-10"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Created On"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="48"
						y="40"
						width="27"
						height="18"
						key="staticText-127"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Job]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="608"
						y="40"
						width="78"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-128"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Origin City]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="438"
						y="40"
						width="65"
						height="18"
						key="staticText-130"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Move Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="394"
						y="19"
						width="97"
						height="18"
						key="staticText-131"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Survey between]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="541"
						y="19"
						width="30"
						height="18"
						key="staticText-132"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[  and  ]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="491"
						y="19"
						width="50"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{begindate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="571"
						y="19"
						width="50"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{enddate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1039"
						y="40"
						width="74"
						height="18"
						key="textField-11"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Order Number"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Transparent"
						x="140"
						y="40"
						width="70"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-134"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Sales Status]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="40"
						width="48"
						height="18"
						key="staticText-135"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Competitive]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="210"
						y="40"
						width="130"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-136"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Price Submission Transferee]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="340"
						y="40"
						width="98"
						height="18"
						key="staticText-137"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Quote Acceptance]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="503"
						y="40"
						width="105"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-138"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Customer Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="686"
						y="40"
						width="52"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-139"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Origin State]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="816"
						y="40"
						width="68"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-140"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Destination City]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="884"
						y="40"
						width="73"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-141"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Destination State]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="957"
						y="40"
						width="82"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-15"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Destination Country"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="738"
						y="40"
						width="78"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-14"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Origin Country"]]></textFieldExpression>
				</textField>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" pattern="MM/dd/yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="75"
						y="0"
						width="65"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="2">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{Created ON}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="48"
						y="0"
						width="27"
						height="18"
						key="textField"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Job}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="MM/dd/yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="438"
						y="0"
						width="65"
						height="18"
						key="textField"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{Move Date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="608"
						y="0"
						width="78"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box leftPadding="1"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Origin City}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1039"
						y="0"
						width="74"
						height="18"
						key="textField"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Order Number}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="48"
						height="18"
						key="textField-1"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Competitive}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="140"
						y="0"
						width="70"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Sales Status}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="MM/dd/yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="210"
						y="0"
						width="130"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="2">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{Price Submission Transferee}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="MM/dd/yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="340"
						y="0"
						width="98"
						height="18"
						key="textField-4"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{Quote Acceptance}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="503"
						y="0"
						width="105"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-5"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Customer Name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="686"
						y="0"
						width="52"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-6"/>
					<box leftPadding="1"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Origin State}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="816"
						y="0"
						width="78"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-7"/>
					<box leftPadding="1"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Destination City}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="894"
						y="0"
						width="63"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-8"/>
					<box leftPadding="1"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Destination State}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="957"
						y="0"
						width="82"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-12"/>
					<box leftPadding="1"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Destination Country}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="738"
						y="0"
						width="78"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-13"/>
					<box leftPadding="1"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Origin Country}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="23"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="900"
						y="8"
						width="170"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1075"
						y="8"
						width="36"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="8"
						width="119"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" size="9"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="4"
						width="1110"
						height="2"
						forecolor="#00FFCC"
						key="line-1"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="2.0" lineStyle="Solid"/>
</graphicElement>
				</line>
				<staticText>
					<reportElement
						x="243"
						y="8"
						width="626"
						height="15"
						key="staticText-82"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font/>
					</textElement>
				<text><![CDATA[SalesPipelineDetailPricing.jrxml]]></text>
				</staticText>
			</band>
		</pageFooter>
		<summary>
			<band height="1"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
