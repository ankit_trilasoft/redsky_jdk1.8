package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import org.appfuse.model.BaseObject; 
import javax.persistence.Column;
import javax.persistence.Entity; 
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder; 
@Entity
@Table(name="miscellaneous")
public class Miscellaneous extends BaseObject implements ToDoRuleRecord
{	
	private String corpID;
	private Long id;
	private String sequenceNumber;
	private String ship;
	private String shipNumber;
	private BigDecimal estimateGrossWeight;
	private BigDecimal estimateGrossWeightKilo;
	private BigDecimal actualGrossWeight;
	private BigDecimal actualGrossWeightKilo;
	private BigDecimal entitleTareWeight;
	private BigDecimal entitleTareWeightKilo;
	private BigDecimal estimateTareWeight;
	private BigDecimal estimateTareWeightKilo;
	private BigDecimal actualTareWeight;
	private BigDecimal actualTareWeightKilo;
	private BigDecimal entitleNetWeight;
	private BigDecimal entitleNetWeightKilo;
	private BigDecimal entitleGrossWeight;
	private BigDecimal entitleGrossWeightKilo;
	private BigDecimal estimatedNetWeight;
	private BigDecimal estimatedNetWeightKilo;
	private BigDecimal actualNetWeight;
	private BigDecimal actualNetWeightKilo;
	private Integer entitleNumberAuto;
	private Integer estimateAuto;
	private Integer actualAuto;
	private BigDecimal entitleCubicFeet;
	private BigDecimal estimateCubicFeet;
	private BigDecimal actualCubicFeet;
	private BigDecimal netEntitleCubicFeet;
	private BigDecimal netEstimateCubicFeet;
	private BigDecimal netActualCubicFeet;
	
	private BigDecimal entitleCubicMtr;
	private BigDecimal estimateCubicMtr;
	private BigDecimal actualCubicMtr;
	private BigDecimal netEntitleCubicMtr;
	private BigDecimal netEstimateCubicMtr;
	private BigDecimal netActualCubicMtr;

	
	private String bookerSelfPacking;
	private String bookerOwnAuthority;
	private String packAuthorize;
	private String unPack;
	private String packingBulky;
	private String codeHauling;
	private String tarif;
	private String section;
	private String applicationNumber3rdParty;
	private String needWaiveEstimate;
    private String status;
	private String needWaiveSurvey;
	private String needWaivePeakRate;
	private String needWaiveSRA;
	private String vanLineOrderNumber;
	private String vanLineNationalCode;
	private String originCountyCode;
	private String originStateCode;
	private String originCounty;
	private String originState;
	private String destinationCountyCode;
	private String destinationStateCode;
	private String destinationCounty;
	private String destinationState;
	private String extraStopYN;
	private BigDecimal mile;
	private BigDecimal ratePSTG;
	private BigDecimal entitleAutoWeight;
	private BigDecimal actualAutoWeight;
	private BigDecimal estimatedAutoWeight;
	private BigDecimal estimatedRevenue;
	private BigDecimal estimatedExpense;
	private String warehouse;
	private String tag;
	private BigDecimal actualHouseHoldGood;
	private BigDecimal actualRevenueDollor;
	private String agentPerformNonperform;
	private String destination24HR;
	private String origin24Hr;
	private String vanLinePickupNumber;
	private Date shipmentRegistrationDate;
	private Date atlasDown;
	private BigDecimal rwghGross;
	private BigDecimal rwghTare;
	private BigDecimal rwghNet;
	private BigDecimal rwghGrossKilo;
	private BigDecimal rwghTareKilo;
	private BigDecimal rwghNetKilo;
	private Integer actualBoat;
	private BigDecimal trailerVolumeWeight;
	private String gate;
	private String trailer;
	private String pendingNumber;
	private BigDecimal vanLineCODAmount;
	private String weightTicket;
	private String domesticInstruction;
	private String peakRate;
	private String paperWork;
	private Date recivedPO;
	private Date recivedHVI;
	private Date confirmOriginAgent;
	private Date dd1840;
	private Date quoteOn;
	private Date recivedPayment;
	private Date settledDate;
	private BigDecimal chargeableGrossWeight;
	private BigDecimal chargeableNetWeight;
	private BigDecimal chargeableTareWeight;
	private BigDecimal chargeableGrossWeightKilo;
	private BigDecimal chargeableNetWeightKilo;
	private BigDecimal chargeableTareWeightKilo;
	
	private BigDecimal rwghCubicFeet;
	private BigDecimal rwghNetCubicFeet;
	private BigDecimal chargeableCubicFeet;
	private BigDecimal chargeableNetCubicFeet;
	private BigDecimal rwghCubicMtr;
	private BigDecimal rwghNetCubicMtr;
	private BigDecimal chargeableCubicMtr;
	private BigDecimal chargeableNetCubicMtr;
	
	private String unit1;
	private String unit2;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	
	
	private String haulingAgentCode;
	private String haulerName;
	private String driverId;
	private String driverName;
	private String driverCell;
	private String carrier;
	private String carrierName;
	private String fromNonTempStorage;
	
	//Enhancement Military Shipments
	
	private String millitaryShipment;
	private Boolean dp3;
	private Boolean shortFuse;
	private Date originDocsToBase;
	private Date printGbl;
	private Date actualPickUp;
	private Date millitarySurveyDate;
	private Date millitarySurveyResults;
	private Date millitaryDeliveryDate;
	private Date millitarySitDestinationDate;
	private Date millitaryActualWeightDate;
	private Date preApprovalDate1;
	private Date preApprovalDate2;
	private Date preApprovalDate3;
	private String equipment;
	private String preApprovalRequest1;
	private String preApprovalRequest2;
	private String preApprovalRequest3;
	//private String originBase;
	private double originScore; 
	//
	private String weaponsIncluded;
	private Date transDocument;
	
	private Date assignDate;
	
	private String selfHaul;
	
	private String haulingAgentVanlineCode;
	
	
	private String packingDriverId;
    private String loadingDriverId;
	private String deliveryDriverId;
	private String sitDestinationDriverId;
	private String loadSignature; 
	private String deliverySignature;
	private Integer loadCount; 
	private Integer deliveryCount; 
	private String tractorAgentVanLineCode;
	private String tractorAgentName;
	private String vanAgentVanLineCode;
	private String vanAgentName;
	private String vanID;
	private String vanName;
	private String tripNumber;
	private Boolean g11;
	private String 	ugwIntId;
	private BigDecimal totalActualRevenue;
	private BigDecimal discount = new BigDecimal(0.00);
	private BigDecimal totalDiscountPercentage;
	private BigDecimal actualLineHaul;
	private String excessWeightBilling;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("corpID", corpID)
				.append("id", id)
				.append("sequenceNumber", sequenceNumber)
				.append("ship", ship)
				.append("shipNumber", shipNumber)
				.append("estimateGrossWeight", estimateGrossWeight)
				.append("estimateGrossWeightKilo", estimateGrossWeightKilo)
				.append("actualGrossWeight", actualGrossWeight)
				.append("actualGrossWeightKilo", actualGrossWeightKilo)
				.append("entitleTareWeight", entitleTareWeight)
				.append("entitleTareWeightKilo", entitleTareWeightKilo)
				.append("estimateTareWeight", estimateTareWeight)
				.append("estimateTareWeightKilo", estimateTareWeightKilo)
				.append("actualTareWeight", actualTareWeight)
				.append("actualTareWeightKilo", actualTareWeightKilo)
				.append("entitleNetWeight", entitleNetWeight)
				.append("entitleNetWeightKilo", entitleNetWeightKilo)
				.append("entitleGrossWeight", entitleGrossWeight)
				.append("entitleGrossWeightKilo", entitleGrossWeightKilo)
				.append("estimatedNetWeight", estimatedNetWeight)
				.append("estimatedNetWeightKilo", estimatedNetWeightKilo)
				.append("actualNetWeight", actualNetWeight)
				.append("actualNetWeightKilo", actualNetWeightKilo)
				.append("entitleNumberAuto", entitleNumberAuto)
				.append("estimateAuto", estimateAuto)
				.append("actualAuto", actualAuto)
				.append("entitleCubicFeet", entitleCubicFeet)
				.append("estimateCubicFeet", estimateCubicFeet)
				.append("actualCubicFeet", actualCubicFeet)
				.append("netEntitleCubicFeet", netEntitleCubicFeet)
				.append("netEstimateCubicFeet", netEstimateCubicFeet)
				.append("netActualCubicFeet", netActualCubicFeet)
				.append("entitleCubicMtr", entitleCubicMtr)
				.append("estimateCubicMtr", estimateCubicMtr)
				.append("actualCubicMtr", actualCubicMtr)
				.append("netEntitleCubicMtr", netEntitleCubicMtr)
				.append("netEstimateCubicMtr", netEstimateCubicMtr)
				.append("netActualCubicMtr", netActualCubicMtr)
				.append("bookerSelfPacking", bookerSelfPacking)
				.append("bookerOwnAuthority", bookerOwnAuthority)
				.append("packAuthorize", packAuthorize)
				.append("unPack", unPack)
				.append("packingBulky", packingBulky)
				.append("codeHauling", codeHauling)
				.append("tarif", tarif)
				.append("section", section)
				.append("applicationNumber3rdParty", applicationNumber3rdParty)
				.append("needWaiveEstimate", needWaiveEstimate)
				.append("status", status)
				.append("needWaiveSurvey", needWaiveSurvey)
				.append("needWaivePeakRate", needWaivePeakRate)
				.append("needWaiveSRA", needWaiveSRA)
				.append("vanLineOrderNumber", vanLineOrderNumber)
				.append("vanLineNationalCode", vanLineNationalCode)
				.append("originCountyCode", originCountyCode)
				.append("originStateCode", originStateCode)
				.append("originCounty", originCounty)
				.append("originState", originState)
				.append("destinationCountyCode", destinationCountyCode)
				.append("destinationStateCode", destinationStateCode)
				.append("destinationCounty", destinationCounty)
				.append("destinationState", destinationState)
				.append("extraStopYN", extraStopYN)
				.append("mile", mile)
				.append("ratePSTG", ratePSTG)
				.append("entitleAutoWeight", entitleAutoWeight)
				.append("actualAutoWeight", actualAutoWeight)
				.append("estimatedAutoWeight", estimatedAutoWeight)
				.append("estimatedRevenue", estimatedRevenue)
				.append("estimatedExpense", estimatedExpense)
				.append("warehouse", warehouse)
				.append("tag", tag)
				.append("actualHouseHoldGood", actualHouseHoldGood)
				.append("actualRevenueDollor", actualRevenueDollor)
				.append("agentPerformNonperform", agentPerformNonperform)
				.append("destination24HR", destination24HR)
				.append("origin24Hr", origin24Hr)
				.append("vanLinePickupNumber", vanLinePickupNumber)
				.append("shipmentRegistrationDate", shipmentRegistrationDate)
				.append("atlasDown", atlasDown)
				.append("rwghGross", rwghGross)
				.append("rwghTare", rwghTare)
				.append("rwghNet", rwghNet)
				.append("rwghGrossKilo", rwghGrossKilo)
				.append("rwghTareKilo", rwghTareKilo)
				.append("rwghNetKilo", rwghNetKilo)
				.append("actualBoat", actualBoat)
				.append("trailerVolumeWeight", trailerVolumeWeight)
				.append("gate", gate)
				.append("trailer", trailer)
				.append("pendingNumber", pendingNumber)
				.append("vanLineCODAmount", vanLineCODAmount)
				.append("weightTicket", weightTicket)
				.append("domesticInstruction", domesticInstruction)
				.append("peakRate", peakRate)
				.append("paperWork", paperWork)
				.append("recivedPO", recivedPO)
				.append("recivedHVI", recivedHVI)
				.append("confirmOriginAgent", confirmOriginAgent)
				.append("dd1840", dd1840)
				.append("quoteOn", quoteOn)
				.append("recivedPayment", recivedPayment)
				.append("settledDate", settledDate)
				.append("chargeableGrossWeight", chargeableGrossWeight)
				.append("chargeableNetWeight", chargeableNetWeight)
				.append("chargeableTareWeight", chargeableTareWeight)
				.append("chargeableGrossWeightKilo", chargeableGrossWeightKilo)
				.append("chargeableNetWeightKilo", chargeableNetWeightKilo)
				.append("chargeableTareWeightKilo", chargeableTareWeightKilo)
				.append("rwghCubicFeet", rwghCubicFeet)
				.append("rwghNetCubicFeet", rwghNetCubicFeet)
				.append("chargeableCubicFeet", chargeableCubicFeet)
				.append("chargeableNetCubicFeet", chargeableNetCubicFeet)
				.append("rwghCubicMtr", rwghCubicMtr)
				.append("rwghNetCubicMtr", rwghNetCubicMtr)
				.append("chargeableCubicMtr", chargeableCubicMtr)
				.append("chargeableNetCubicMtr", chargeableNetCubicMtr)
				.append("unit1", unit1)
				.append("unit2", unit2)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("haulingAgentCode", haulingAgentCode)
				.append("haulerName", haulerName)
				.append("driverId", driverId)
				.append("driverName", driverName)
				.append("driverCell", driverCell)
				.append("carrier", carrier)
				.append("carrierName", carrierName)
				.append("fromNonTempStorage", fromNonTempStorage)
				.append("millitaryShipment", millitaryShipment)
				.append("dp3", dp3)
				.append("shortFuse", shortFuse)
				.append("originDocsToBase", originDocsToBase)
				.append("printGbl", printGbl)
				.append("actualPickUp", actualPickUp)
				.append("millitarySurveyDate", millitarySurveyDate)
				.append("millitarySurveyResults", millitarySurveyResults)
				.append("millitaryDeliveryDate", millitaryDeliveryDate)
				.append("millitarySitDestinationDate",
						millitarySitDestinationDate)
				.append("millitaryActualWeightDate", millitaryActualWeightDate)
				.append("preApprovalDate1", preApprovalDate1)
				.append("preApprovalDate2", preApprovalDate2)
				.append("preApprovalDate3", preApprovalDate3)
				.append("equipment", equipment)
				.append("preApprovalRequest1", preApprovalRequest1)
				.append("preApprovalRequest2", preApprovalRequest2)
				.append("preApprovalRequest3", preApprovalRequest3)
				.append("originScore", originScore)
				.append("weaponsIncluded", weaponsIncluded)
				.append("transDocument", transDocument)
				.append("assignDate", assignDate).append("selfHaul", selfHaul)
				.append("haulingAgentVanlineCode", haulingAgentVanlineCode)
				.append("packingDriverId", packingDriverId)
				.append("loadingDriverId", loadingDriverId)
				.append("deliveryDriverId", deliveryDriverId)
				.append("sitDestinationDriverId", sitDestinationDriverId)
				.append("loadSignature", loadSignature)
				.append("deliverySignature", deliverySignature)
				.append("loadCount", loadCount)
				.append("deliveryCount", deliveryCount)
				.append("tractorAgentVanLineCode", tractorAgentVanLineCode)
				.append("tractorAgentName", tractorAgentName)
				.append("vanAgentVanLineCode", vanAgentVanLineCode)
				.append("vanAgentName", vanAgentName).append("vanID", vanID)
				.append("vanName", vanName).append("tripNumber", tripNumber)
				.append("g11", g11).append("ugwIntId", ugwIntId)
				.append("totalActualRevenue", totalActualRevenue)
				.append("discount", discount)
				.append("totalDiscountPercentage", totalDiscountPercentage)
				.append("actualLineHaul", actualLineHaul).append("excessWeightBilling", excessWeightBilling).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Miscellaneous))
			return false;
		Miscellaneous castOther = (Miscellaneous) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(ship, castOther.ship)
				.append(shipNumber, castOther.shipNumber)
				.append(estimateGrossWeight, castOther.estimateGrossWeight)
				.append(estimateGrossWeightKilo,
						castOther.estimateGrossWeightKilo)
				.append(actualGrossWeight, castOther.actualGrossWeight)
				.append(actualGrossWeightKilo, castOther.actualGrossWeightKilo)
				.append(entitleTareWeight, castOther.entitleTareWeight)
				.append(entitleTareWeightKilo, castOther.entitleTareWeightKilo)
				.append(estimateTareWeight, castOther.estimateTareWeight)
				.append(estimateTareWeightKilo,
						castOther.estimateTareWeightKilo)
				.append(actualTareWeight, castOther.actualTareWeight)
				.append(actualTareWeightKilo, castOther.actualTareWeightKilo)
				.append(entitleNetWeight, castOther.entitleNetWeight)
				.append(entitleNetWeightKilo, castOther.entitleNetWeightKilo)
				.append(entitleGrossWeight, castOther.entitleGrossWeight)
				.append(entitleGrossWeightKilo,
						castOther.entitleGrossWeightKilo)
				.append(estimatedNetWeight, castOther.estimatedNetWeight)
				.append(estimatedNetWeightKilo,
						castOther.estimatedNetWeightKilo)
				.append(actualNetWeight, castOther.actualNetWeight)
				.append(actualNetWeightKilo, castOther.actualNetWeightKilo)
				.append(entitleNumberAuto, castOther.entitleNumberAuto)
				.append(estimateAuto, castOther.estimateAuto)
				.append(actualAuto, castOther.actualAuto)
				.append(entitleCubicFeet, castOther.entitleCubicFeet)
				.append(estimateCubicFeet, castOther.estimateCubicFeet)
				.append(actualCubicFeet, castOther.actualCubicFeet)
				.append(netEntitleCubicFeet, castOther.netEntitleCubicFeet)
				.append(netEstimateCubicFeet, castOther.netEstimateCubicFeet)
				.append(netActualCubicFeet, castOther.netActualCubicFeet)
				.append(entitleCubicMtr, castOther.entitleCubicMtr)
				.append(estimateCubicMtr, castOther.estimateCubicMtr)
				.append(actualCubicMtr, castOther.actualCubicMtr)
				.append(netEntitleCubicMtr, castOther.netEntitleCubicMtr)
				.append(netEstimateCubicMtr, castOther.netEstimateCubicMtr)
				.append(netActualCubicMtr, castOther.netActualCubicMtr)
				.append(bookerSelfPacking, castOther.bookerSelfPacking)
				.append(bookerOwnAuthority, castOther.bookerOwnAuthority)
				.append(packAuthorize, castOther.packAuthorize)
				.append(unPack, castOther.unPack)
				.append(packingBulky, castOther.packingBulky)
				.append(codeHauling, castOther.codeHauling)
				.append(tarif, castOther.tarif)
				.append(section, castOther.section)
				.append(applicationNumber3rdParty,
						castOther.applicationNumber3rdParty)
				.append(needWaiveEstimate, castOther.needWaiveEstimate)
				.append(status, castOther.status)
				.append(needWaiveSurvey, castOther.needWaiveSurvey)
				.append(needWaivePeakRate, castOther.needWaivePeakRate)
				.append(needWaiveSRA, castOther.needWaiveSRA)
				.append(vanLineOrderNumber, castOther.vanLineOrderNumber)
				.append(vanLineNationalCode, castOther.vanLineNationalCode)
				.append(originCountyCode, castOther.originCountyCode)
				.append(originStateCode, castOther.originStateCode)
				.append(originCounty, castOther.originCounty)
				.append(originState, castOther.originState)
				.append(destinationCountyCode, castOther.destinationCountyCode)
				.append(destinationStateCode, castOther.destinationStateCode)
				.append(destinationCounty, castOther.destinationCounty)
				.append(destinationState, castOther.destinationState)
				.append(extraStopYN, castOther.extraStopYN)
				.append(mile, castOther.mile)
				.append(ratePSTG, castOther.ratePSTG)
				.append(entitleAutoWeight, castOther.entitleAutoWeight)
				.append(actualAutoWeight, castOther.actualAutoWeight)
				.append(estimatedAutoWeight, castOther.estimatedAutoWeight)
				.append(estimatedRevenue, castOther.estimatedRevenue)
				.append(estimatedExpense, castOther.estimatedExpense)
				.append(warehouse, castOther.warehouse)
				.append(tag, castOther.tag)
				.append(actualHouseHoldGood, castOther.actualHouseHoldGood)
				.append(actualRevenueDollor, castOther.actualRevenueDollor)
				.append(agentPerformNonperform,
						castOther.agentPerformNonperform)
				.append(destination24HR, castOther.destination24HR)
				.append(origin24Hr, castOther.origin24Hr)
				.append(vanLinePickupNumber, castOther.vanLinePickupNumber)
				.append(shipmentRegistrationDate,
						castOther.shipmentRegistrationDate)
				.append(atlasDown, castOther.atlasDown)
				.append(rwghGross, castOther.rwghGross)
				.append(rwghTare, castOther.rwghTare)
				.append(rwghNet, castOther.rwghNet)
				.append(rwghGrossKilo, castOther.rwghGrossKilo)
				.append(rwghTareKilo, castOther.rwghTareKilo)
				.append(rwghNetKilo, castOther.rwghNetKilo)
				.append(actualBoat, castOther.actualBoat)
				.append(trailerVolumeWeight, castOther.trailerVolumeWeight)
				.append(gate, castOther.gate)
				.append(trailer, castOther.trailer)
				.append(pendingNumber, castOther.pendingNumber)
				.append(vanLineCODAmount, castOther.vanLineCODAmount)
				.append(weightTicket, castOther.weightTicket)
				.append(domesticInstruction, castOther.domesticInstruction)
				.append(peakRate, castOther.peakRate)
				.append(paperWork, castOther.paperWork)
				.append(recivedPO, castOther.recivedPO)
				.append(recivedHVI, castOther.recivedHVI)
				.append(confirmOriginAgent, castOther.confirmOriginAgent)
				.append(dd1840, castOther.dd1840)
				.append(quoteOn, castOther.quoteOn)
				.append(recivedPayment, castOther.recivedPayment)
				.append(settledDate, castOther.settledDate)
				.append(chargeableGrossWeight, castOther.chargeableGrossWeight)
				.append(chargeableNetWeight, castOther.chargeableNetWeight)
				.append(chargeableTareWeight, castOther.chargeableTareWeight)
				.append(chargeableGrossWeightKilo,
						castOther.chargeableGrossWeightKilo)
				.append(chargeableNetWeightKilo,
						castOther.chargeableNetWeightKilo)
				.append(chargeableTareWeightKilo,
						castOther.chargeableTareWeightKilo)
				.append(rwghCubicFeet, castOther.rwghCubicFeet)
				.append(rwghNetCubicFeet, castOther.rwghNetCubicFeet)
				.append(chargeableCubicFeet, castOther.chargeableCubicFeet)
				.append(chargeableNetCubicFeet,
						castOther.chargeableNetCubicFeet)
				.append(rwghCubicMtr, castOther.rwghCubicMtr)
				.append(rwghNetCubicMtr, castOther.rwghNetCubicMtr)
				.append(chargeableCubicMtr, castOther.chargeableCubicMtr)
				.append(chargeableNetCubicMtr, castOther.chargeableNetCubicMtr)
				.append(unit1, castOther.unit1)
				.append(unit2, castOther.unit2)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(haulingAgentCode, castOther.haulingAgentCode)
				.append(haulerName, castOther.haulerName)
				.append(driverId, castOther.driverId)
				.append(driverName, castOther.driverName)
				.append(driverCell, castOther.driverCell)
				.append(carrier, castOther.carrier)
				.append(carrierName, castOther.carrierName)
				.append(fromNonTempStorage, castOther.fromNonTempStorage)
				.append(millitaryShipment, castOther.millitaryShipment)
				.append(dp3, castOther.dp3)
				.append(shortFuse, castOther.shortFuse)
				.append(originDocsToBase, castOther.originDocsToBase)
				.append(printGbl, castOther.printGbl)
				.append(actualPickUp, castOther.actualPickUp)
				.append(millitarySurveyDate, castOther.millitarySurveyDate)
				.append(millitarySurveyResults,
						castOther.millitarySurveyResults)
				.append(millitaryDeliveryDate, castOther.millitaryDeliveryDate)
				.append(millitarySitDestinationDate,
						castOther.millitarySitDestinationDate)
				.append(millitaryActualWeightDate,
						castOther.millitaryActualWeightDate)
				.append(preApprovalDate1, castOther.preApprovalDate1)
				.append(preApprovalDate2, castOther.preApprovalDate2)
				.append(preApprovalDate3, castOther.preApprovalDate3)
				.append(equipment, castOther.equipment)
				.append(preApprovalRequest1, castOther.preApprovalRequest1)
				.append(preApprovalRequest2, castOther.preApprovalRequest2)
				.append(preApprovalRequest3, castOther.preApprovalRequest3)
				.append(originScore, castOther.originScore)
				.append(weaponsIncluded, castOther.weaponsIncluded)
				.append(transDocument, castOther.transDocument)
				.append(assignDate, castOther.assignDate)
				.append(selfHaul, castOther.selfHaul)
				.append(haulingAgentVanlineCode,
						castOther.haulingAgentVanlineCode)
				.append(packingDriverId, castOther.packingDriverId)
				.append(loadingDriverId, castOther.loadingDriverId)
				.append(deliveryDriverId, castOther.deliveryDriverId)
				.append(sitDestinationDriverId,
						castOther.sitDestinationDriverId)
				.append(loadSignature, castOther.loadSignature)
				.append(deliverySignature, castOther.deliverySignature)
				.append(loadCount, castOther.loadCount)
				.append(deliveryCount, castOther.deliveryCount)
				.append(tractorAgentVanLineCode,
						castOther.tractorAgentVanLineCode)
				.append(tractorAgentName, castOther.tractorAgentName)
				.append(vanAgentVanLineCode, castOther.vanAgentVanLineCode)
				.append(vanAgentName, castOther.vanAgentName)
				.append(vanID, castOther.vanID)
				.append(vanName, castOther.vanName)
				.append(tripNumber, castOther.tripNumber)
				.append(g11, castOther.g11)
				.append(ugwIntId, castOther.ugwIntId)
				.append(totalActualRevenue, castOther.totalActualRevenue)
				.append(discount, castOther.discount)
				.append(totalDiscountPercentage,
						castOther.totalDiscountPercentage)
				.append(actualLineHaul, castOther.actualLineHaul).append(excessWeightBilling, castOther.excessWeightBilling).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(sequenceNumber).append(ship).append(shipNumber)
				.append(estimateGrossWeight).append(estimateGrossWeightKilo)
				.append(actualGrossWeight).append(actualGrossWeightKilo)
				.append(entitleTareWeight).append(entitleTareWeightKilo)
				.append(estimateTareWeight).append(estimateTareWeightKilo)
				.append(actualTareWeight).append(actualTareWeightKilo)
				.append(entitleNetWeight).append(entitleNetWeightKilo)
				.append(entitleGrossWeight).append(entitleGrossWeightKilo)
				.append(estimatedNetWeight).append(estimatedNetWeightKilo)
				.append(actualNetWeight).append(actualNetWeightKilo)
				.append(entitleNumberAuto).append(estimateAuto)
				.append(actualAuto).append(entitleCubicFeet)
				.append(estimateCubicFeet).append(actualCubicFeet)
				.append(netEntitleCubicFeet).append(netEstimateCubicFeet)
				.append(netActualCubicFeet).append(entitleCubicMtr)
				.append(estimateCubicMtr).append(actualCubicMtr)
				.append(netEntitleCubicMtr).append(netEstimateCubicMtr)
				.append(netActualCubicMtr).append(bookerSelfPacking)
				.append(bookerOwnAuthority).append(packAuthorize)
				.append(unPack).append(packingBulky).append(codeHauling)
				.append(tarif).append(section)
				.append(applicationNumber3rdParty).append(needWaiveEstimate)
				.append(status).append(needWaiveSurvey)
				.append(needWaivePeakRate).append(needWaiveSRA)
				.append(vanLineOrderNumber).append(vanLineNationalCode)
				.append(originCountyCode).append(originStateCode)
				.append(originCounty).append(originState)
				.append(destinationCountyCode).append(destinationStateCode)
				.append(destinationCounty).append(destinationState)
				.append(extraStopYN).append(mile).append(ratePSTG)
				.append(entitleAutoWeight).append(actualAutoWeight)
				.append(estimatedAutoWeight).append(estimatedRevenue)
				.append(estimatedExpense).append(warehouse).append(tag)
				.append(actualHouseHoldGood).append(actualRevenueDollor)
				.append(agentPerformNonperform).append(destination24HR)
				.append(origin24Hr).append(vanLinePickupNumber)
				.append(shipmentRegistrationDate).append(atlasDown)
				.append(rwghGross).append(rwghTare).append(rwghNet)
				.append(rwghGrossKilo).append(rwghTareKilo).append(rwghNetKilo)
				.append(actualBoat).append(trailerVolumeWeight).append(gate)
				.append(trailer).append(pendingNumber).append(vanLineCODAmount)
				.append(weightTicket).append(domesticInstruction)
				.append(peakRate).append(paperWork).append(recivedPO)
				.append(recivedHVI).append(confirmOriginAgent).append(dd1840)
				.append(quoteOn).append(recivedPayment).append(settledDate)
				.append(chargeableGrossWeight).append(chargeableNetWeight)
				.append(chargeableTareWeight).append(chargeableGrossWeightKilo)
				.append(chargeableNetWeightKilo)
				.append(chargeableTareWeightKilo).append(rwghCubicFeet)
				.append(rwghNetCubicFeet).append(chargeableCubicFeet)
				.append(chargeableNetCubicFeet).append(rwghCubicMtr)
				.append(rwghNetCubicMtr).append(chargeableCubicMtr)
				.append(chargeableNetCubicMtr).append(unit1).append(unit2)
				.append(createdBy).append(createdOn).append(updatedBy)
				.append(updatedOn).append(haulingAgentCode).append(haulerName)
				.append(driverId).append(driverName).append(driverCell)
				.append(carrier).append(carrierName).append(fromNonTempStorage)
				.append(millitaryShipment).append(dp3).append(shortFuse)
				.append(originDocsToBase).append(printGbl).append(actualPickUp)
				.append(millitarySurveyDate).append(millitarySurveyResults)
				.append(millitaryDeliveryDate)
				.append(millitarySitDestinationDate)
				.append(millitaryActualWeightDate).append(preApprovalDate1)
				.append(preApprovalDate2).append(preApprovalDate3)
				.append(equipment).append(preApprovalRequest1)
				.append(preApprovalRequest2).append(preApprovalRequest3)
				.append(originScore).append(weaponsIncluded)
				.append(transDocument).append(assignDate).append(selfHaul)
				.append(haulingAgentVanlineCode).append(packingDriverId)
				.append(loadingDriverId).append(deliveryDriverId)
				.append(sitDestinationDriverId).append(loadSignature)
				.append(deliverySignature).append(loadCount)
				.append(deliveryCount).append(tractorAgentVanLineCode)
				.append(tractorAgentName).append(vanAgentVanLineCode)
				.append(vanAgentName).append(vanID).append(vanName)
				.append(tripNumber).append(g11).append(ugwIntId)
				.append(totalActualRevenue).append(discount)
				.append(totalDiscountPercentage).append(actualLineHaul)
				.append(excessWeightBilling)
				.toHashCode();
	}
	@Column(length=5)
	public Integer getActualAuto() {
		return actualAuto;
	}
	public void setActualAuto(Integer actualAuto) {
		this.actualAuto = actualAuto;
	}
	@Column(length=20)
	public BigDecimal getActualAutoWeight() {
		return actualAutoWeight;
	}
	public void setActualAutoWeight(BigDecimal actualAutoWeight) {
		this.actualAutoWeight = actualAutoWeight;
	}
	@Column(length=5)
	public Integer getActualBoat() {
		return actualBoat;
	}
	public void setActualBoat(Integer actualBoat) {
		this.actualBoat = actualBoat;
	}
	/**
	 * @return the actualGrossWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getActualGrossWeightKilo() {
		return actualGrossWeightKilo;
	}
	/**
	 * @param actualGrossWeightKilo the actualGrossWeightKilo to set
	 */
	public void setActualGrossWeightKilo(BigDecimal actualGrossWeightKilo) {
		this.actualGrossWeightKilo = actualGrossWeightKilo;
	}
	@Column(length=20)
	public BigDecimal getActualHouseHoldGood() {
		return actualHouseHoldGood;
	}
	public void setActualHouseHoldGood(BigDecimal actualHouseHoldGood) {
		this.actualHouseHoldGood = actualHouseHoldGood;
	}
	/**
	 * @return the actualNetWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getActualNetWeightKilo() {
		return actualNetWeightKilo;
	}
	/**
	 * @param actualNetWeightKilo the actualNetWeightKilo to set
	 */
	public void setActualNetWeightKilo(BigDecimal actualNetWeightKilo) {
		this.actualNetWeightKilo = actualNetWeightKilo;
	}
	@Column(length=20)
	public BigDecimal getActualRevenueDollor() {
		return actualRevenueDollor;
	}
	public void setActualRevenueDollor(BigDecimal actualRevenueDollor) {
		this.actualRevenueDollor = actualRevenueDollor;
	}
	/**
	 * @return the actualTareWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getActualTareWeightKilo() {
		return actualTareWeightKilo;
	}
	/**
	 * @param actualTareWeightKilo the actualTareWeightKilo to set
	 */
	public void setActualTareWeightKilo(BigDecimal actualTareWeightKilo) {
		this.actualTareWeightKilo = actualTareWeightKilo;
	}
	@Column(length=5)
	public String getAgentPerformNonperform() {
		return agentPerformNonperform;
	}
	public void setAgentPerformNonperform(String agentPerformNonperform) {
		this.agentPerformNonperform = agentPerformNonperform;
	}
	@Column(length=1)
	public String getApplicationNumber3rdParty() {
		return applicationNumber3rdParty;
	}
	public void setApplicationNumber3rdParty(String applicationNumber3rdParty) {
		this.applicationNumber3rdParty = applicationNumber3rdParty;
	}
	@Column
	public Date getAtlasDown() {
		return atlasDown;
	}
	public void setAtlasDown(Date atlasDown) {
		this.atlasDown = atlasDown;
	}
	@Column(length=1)
	public String getBookerOwnAuthority() {
		return bookerOwnAuthority;
	}
	public void setBookerOwnAuthority(String bookerOwnAuthority) {
		this.bookerOwnAuthority = bookerOwnAuthority;
	}
	@Column(length=8)
	public String getBookerSelfPacking() {
		return bookerSelfPacking;
	}
	public void setBookerSelfPacking(String bookerSelfPacking) {
		this.bookerSelfPacking = bookerSelfPacking;
	}
	@Column(length=8)
	public String getCodeHauling() {
		return codeHauling;
	}
	public void setCodeHauling(String codeHauling) {
		this.codeHauling = codeHauling;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=2)
	public String getDestinationState() {
		return destinationState;
	}
	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}
	@Column(length=5)
	public String getDestinationCountyCode() {
		return destinationCountyCode;
	}
	public void setDestinationCountyCode(String destinationCountyCode) {
		this.destinationCountyCode = destinationCountyCode;
	}
	@Column(length=5)
	public String getDestinationStateCode() {
		return destinationStateCode;
	}
	public void setDestinationStateCode(String destinationStateCode) {
		this.destinationStateCode = destinationStateCode;
	}
	@Column(length=5)
	public String getOriginCountyCode() {
		return originCountyCode;
	}
	public void setOriginCountyCode(String originCountyCode) {
		this.originCountyCode = originCountyCode;
	}
	@Column(length=5)
	public String getOriginStateCode() {
		return originStateCode;
	}
	public void setOriginStateCode(String originStateCode) {
		this.originStateCode = originStateCode;
	}
	@Column(length=20)
	public BigDecimal getEntitleAutoWeight() {
		return entitleAutoWeight;
	}
	public void setEntitleAutoWeight(BigDecimal entitleAutoWeight) {
		this.entitleAutoWeight = entitleAutoWeight;
	}
	/**
	 * @return the entitleGrossWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getEntitleGrossWeightKilo() {
		return entitleGrossWeightKilo;
	}
	/**
	 * @param entitleGrossWeightKilo the entitleGrossWeightKilo to set
	 */
	public void setEntitleGrossWeightKilo(BigDecimal entitleGrossWeightKilo) {
		this.entitleGrossWeightKilo = entitleGrossWeightKilo;
	}
	/**
	 * @return the entitleNetWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getEntitleNetWeightKilo() {
		return entitleNetWeightKilo;
	}
	/**
	 * @param entitleNetWeightKilo the entitleNetWeightKilo to set
	 */
	public void setEntitleNetWeightKilo(BigDecimal entitleNetWeightKilo) {
		this.entitleNetWeightKilo = entitleNetWeightKilo;
	}
	@Column(length=5)
	public Integer getEntitleNumberAuto() {
		return entitleNumberAuto;
	}
	public void setEntitleNumberAuto(Integer entitleNumberAuto) {
		this.entitleNumberAuto = entitleNumberAuto;
	}
	@Column(length=5)
	public Integer getEstimateAuto() {
		return estimateAuto;
	}
	public void setEstimateAuto(Integer estimateAuto) {
		this.estimateAuto = estimateAuto;
	}
	@Column(length=20)
	public BigDecimal getEstimatedAutoWeight() {
		return estimatedAutoWeight;
	}
	public void setEstimatedAutoWeight(BigDecimal estimatedAutoWeight) {
		this.estimatedAutoWeight = estimatedAutoWeight;
	}
	
	/**
	 * @return the estimatedNetWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getEstimatedNetWeightKilo() {
		return estimatedNetWeightKilo;
	}
	/**
	 * @param estimatedNetWeightKilo the estimatedNetWeightKilo to set
	 */
	public void setEstimatedNetWeightKilo(BigDecimal estimatedNetWeightKilo) {
		this.estimatedNetWeightKilo = estimatedNetWeightKilo;
	}
	@Column(length=20)
	public BigDecimal getEstimatedRevenue() {
		return estimatedRevenue;
	}
	public void setEstimatedRevenue(BigDecimal estimatedRevenue) {
		this.estimatedRevenue = estimatedRevenue;
	}
	@Column(length=20)
	public BigDecimal getEstimatedExpense() {
		return estimatedExpense;
	}
	public void setEstimatedExpense(BigDecimal estimatedExpense) {
		this.estimatedExpense = estimatedExpense;
	}
	/**
	 * @return the estimateGrossWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getEstimateGrossWeightKilo() {
		return estimateGrossWeightKilo;
	}
	/**
	 * @param estimateGrossWeightKilo the estimateGrossWeightKilo to set
	 */
	public void setEstimateGrossWeightKilo(BigDecimal estimateGrossWeightKilo) {
		this.estimateGrossWeightKilo = estimateGrossWeightKilo;
	}
	/**
	 * @return the estimateTareWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getEstimateTareWeightKilo() {
		return estimateTareWeightKilo;
	}
	/**
	 * @param estimateTareWeightKilo the estimateTareWeightKilo to set
	 */
	public void setEstimateTareWeightKilo(BigDecimal estimateTareWeightKilo) {
		this.estimateTareWeightKilo = estimateTareWeightKilo;
	}
	@Column(length=1)
	public String getExtraStopYN() {
		return extraStopYN;
	}
	public void setExtraStopYN(String extraStopYN) {
		this.extraStopYN = extraStopYN;
	}
	@Column(length=3)
	public String getGate() {
		return gate;
	}
	public void setGate(String gate) {
		this.gate = gate;
	}
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=20)
	public BigDecimal getMile() {
		return mile;
	}
	public void setMile(BigDecimal mile) {
		this.mile = mile;
	}
	@Column(length=1)
	public String getNeedWaiveEstimate() {
		return needWaiveEstimate;
	}
	public void setNeedWaiveEstimate(String needWaiveEstimate) {
		this.needWaiveEstimate = needWaiveEstimate;
	}
	@Column(length=1)
	public String getNeedWaivePeakRate() {
		return needWaivePeakRate;
	}
	public void setNeedWaivePeakRate(String needWaivePeakRate) {
		this.needWaivePeakRate = needWaivePeakRate;
	}
	@Column(length=1)
	public String getNeedWaiveSRA() {
		return needWaiveSRA;
	}
	public void setNeedWaiveSRA(String needWaiveSRA) {
		this.needWaiveSRA = needWaiveSRA;
	}
	@Column(length=1)
	public String getNeedWaiveSurvey() {
		return needWaiveSurvey;
	}
	public void setNeedWaiveSurvey(String needWaiveSurvey) {
		this.needWaiveSurvey = needWaiveSurvey;
	}
	@Column(length=1)
	public String getOrigin24Hr() {
		return origin24Hr;
	}
	public void setOrigin24Hr(String origin24Hr) {
		this.origin24Hr = origin24Hr;
	}
	@Column(length=30)
	public String getOriginCounty() {
		return originCounty;
	}
	public void setOriginCounty(String originCounty) {
		this.originCounty = originCounty;
	}
	@Column(length=2)
	public String getOriginState() {
		return originState;
	}
	public void setOriginState(String originState) {
		this.originState = originState;
	}
	@Column(length=10)
	public String getPackAuthorize() {
		return packAuthorize;
	}
	public void setPackAuthorize(String packAuthorize) {
		this.packAuthorize = packAuthorize;
	}
	@Column(length=1)
	public String getPackingBulky() {
		return packingBulky;
	}
	public void setPackingBulky(String packingBulky) {
		this.packingBulky = packingBulky;
	}
	@Column(length=1)
	public String getPeakRate() {
		return peakRate;
	}
	public void setPeakRate(String peakRate) {
		this.peakRate = peakRate;
	}
	@Column(length=11)
	public String getPendingNumber() {
		return pendingNumber;
	}
	public void setPendingNumber(String pendingNumber) {
		this.pendingNumber = pendingNumber;
	}
	@Column(length=20)
	public BigDecimal getRatePSTG() {
		return ratePSTG;
	}
	public void setRatePSTG(BigDecimal ratePSTG) {
		this.ratePSTG = ratePSTG;
	}
	@Column(length=2)
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	@Column(length=20)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column(length=2)
	public String getShip() {
		return ship;
	}
	public void setShip(String ship) {
		this.ship = ship;
	}
	@Column
	public Date getShipmentRegistrationDate() {
		return shipmentRegistrationDate;
	}
	public void setShipmentRegistrationDate(Date shipmentRegistrationDate) {
		this.shipmentRegistrationDate = shipmentRegistrationDate;
	}
	@Column(length=20)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column(length=2)
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	@Column(length=4)
	public String getTarif() {
		return tarif;
	}
	public void setTarif(String tarif) {
		this.tarif = tarif;
	}
	@Column(length=15)
	public String getTrailer() {
		return trailer;
	}
	public void setTrailer(String trailer) {
		this.trailer = trailer;
	}
	@Column(length=20)
	public BigDecimal getTrailerVolumeWeight() {
		return trailerVolumeWeight;
	}
	public void setTrailerVolumeWeight(BigDecimal trailerVolumeWeight) {
		this.trailerVolumeWeight = trailerVolumeWeight;
	}
	@Column(length=1)
	public String getUnPack() {
		return unPack;
	}
	public void setUnPack(String unPack) {
		this.unPack = unPack;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=20)
	public BigDecimal getVanLineCODAmount() {
		return vanLineCODAmount;
	}
	public void setVanLineCODAmount(BigDecimal vanLineCODAmount) {
		this.vanLineCODAmount = vanLineCODAmount;
	}
	@Column(length=15)
	public String getVanLineNationalCode() {
		return vanLineNationalCode;
	}
	public void setVanLineNationalCode(String vanLineNationalCode) {
		this.vanLineNationalCode = vanLineNationalCode;
	}
	@Column(length=25)
	public String getVanLineOrderNumber() {
		return vanLineOrderNumber;
	}
	public void setVanLineOrderNumber(String vanLineOrderNumber) {
		this.vanLineOrderNumber = vanLineOrderNumber;
	}
	@Column(length=8)
	public String getVanLinePickupNumber() {
		return vanLinePickupNumber;
	}
	public void setVanLinePickupNumber(String vanLinePickupNumber) {
		this.vanLinePickupNumber = vanLinePickupNumber;
	}
	@Column(length=1)
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	@Column(length=1)
	public String getWeightTicket() {
		return weightTicket;
	}
	public void setWeightTicket(String weightTicket) {
		this.weightTicket = weightTicket;
	}
	@Column(length=1)
	public String getDomesticInstruction() {
		return domesticInstruction;
	}
	public void setDomesticInstruction(String domesticInstruction) {
		this.domesticInstruction = domesticInstruction;
	}
	@Column(length=20)
	public BigDecimal getActualCubicFeet() {
		return actualCubicFeet;
	}
	public void setActualCubicFeet(BigDecimal actualCubicFeet) {
		this.actualCubicFeet = actualCubicFeet;
	}
	@Column(length=20)
	public BigDecimal getActualGrossWeight() {
		return actualGrossWeight;
	}
	public void setActualGrossWeight(BigDecimal actualGrossWeight) {
		this.actualGrossWeight = actualGrossWeight;
	}
	@Column(length=20)
	public BigDecimal getActualNetWeight() {
		return actualNetWeight;
	}
	public void setActualNetWeight(BigDecimal actualNetWeight) {
		this.actualNetWeight = actualNetWeight;
	}
	@Column(length=20)
	public BigDecimal getActualTareWeight() {
		return actualTareWeight;
	}
	public void setActualTareWeight(BigDecimal actualTareWeight) {
		this.actualTareWeight = actualTareWeight;
	}
	@Column(length=20)
	public BigDecimal getChargeableGrossWeight() {
		return chargeableGrossWeight;
	}
	public void setChargeableGrossWeight(BigDecimal chargeableGrossWeight) {
		this.chargeableGrossWeight = chargeableGrossWeight;
	}
	@Column(length=20)
	public BigDecimal getChargeableNetWeight() {
		return chargeableNetWeight;
	}
	public void setChargeableNetWeight(BigDecimal chargeableNetWeight) {
		this.chargeableNetWeight = chargeableNetWeight;
	}
	@Column(length=20)
	public BigDecimal getChargeableTareWeight() {
		return chargeableTareWeight;
	}
	public void setChargeableTareWeight(BigDecimal chargeableTareWeight) {
		this.chargeableTareWeight = chargeableTareWeight;
	}
	@Column(length=20)
	public BigDecimal getEntitleGrossWeight() {
		return entitleGrossWeight;
	}
	public void setEntitleGrossWeight(BigDecimal entitleGrossWeight) {
		this.entitleGrossWeight = entitleGrossWeight;
	}
	@Column(length=20)
	public BigDecimal getEntitleNetWeight() {
		return entitleNetWeight;
	}
	public void setEntitleNetWeight(BigDecimal entitleNetWeight) {
		this.entitleNetWeight = entitleNetWeight;
	}
	@Column(length=20)
	public BigDecimal getEstimateCubicFeet() {
		return estimateCubicFeet;
	}
	public void setEstimateCubicFeet(BigDecimal estimateCubicFeet) {
		this.estimateCubicFeet = estimateCubicFeet;
	}
	@Column(length=20)
	public BigDecimal getEstimatedNetWeight() {
		return estimatedNetWeight;
	}
	public void setEstimatedNetWeight(BigDecimal estimatedNetWeight) {
		this.estimatedNetWeight = estimatedNetWeight;
	}
	@Column(length=20)
	public BigDecimal getEstimateGrossWeight() {
		return estimateGrossWeight;
	}
	public void setEstimateGrossWeight(BigDecimal estimateGrossWeight) {
		this.estimateGrossWeight = estimateGrossWeight;
	}
	@Column(length=20)
	public BigDecimal getEstimateTareWeight() {
		return estimateTareWeight;
	}
	public void setEstimateTareWeight(BigDecimal estimateTareWeight) {
		this.estimateTareWeight = estimateTareWeight;
	}
	
	@Column(length=20)
	public BigDecimal getNetActualCubicFeet() {
		return netActualCubicFeet;
	}
	public void setNetActualCubicFeet(BigDecimal netActualCubicFeet) {
		this.netActualCubicFeet = netActualCubicFeet;
	}
	@Column(length=20)
	public BigDecimal getNetEstimateCubicFeet() {
		return netEstimateCubicFeet;
	}
	public void setNetEstimateCubicFeet(BigDecimal netEstimateCubicFeet) {
		this.netEstimateCubicFeet = netEstimateCubicFeet;
	}
	@Column(length=20)
	public BigDecimal getRwghGross() {
		return rwghGross;
	}
	public void setRwghGross(BigDecimal rwghGross) {
		this.rwghGross = rwghGross;
	}
	@Column(length=20)
	public BigDecimal getRwghNet() {
		return rwghNet;
	}
	public void setRwghNet(BigDecimal rwghNet) {
		this.rwghNet = rwghNet;
	}
	@Column(length=20)
	public BigDecimal getRwghTare() {
		return rwghTare;
	}
	public void setRwghTare(BigDecimal rwghTare) {
		this.rwghTare = rwghTare;
	}
	@Column(length=20)
	public BigDecimal getChargeableCubicFeet() {
		return chargeableCubicFeet;
	}
	public void setChargeableCubicFeet(BigDecimal chargeableCubicFeet) {
		this.chargeableCubicFeet = chargeableCubicFeet;
	}
	@Column(length=20)
	public BigDecimal getChargeableNetCubicFeet() {
		return chargeableNetCubicFeet;
	}
	public void setChargeableNetCubicFeet(BigDecimal chargeableNetCubicFeet) {
		this.chargeableNetCubicFeet = chargeableNetCubicFeet;
	}
	@Column(length=20)
	public BigDecimal getEntitleCubicFeet() {
		return entitleCubicFeet;
	}
	public void setEntitleCubicFeet(BigDecimal entitleCubicFeet) {
		this.entitleCubicFeet = entitleCubicFeet;
	}
	@Column(length=20)
	public BigDecimal getNetEntitleCubicFeet() {
		return netEntitleCubicFeet;
	}
	public void setNetEntitleCubicFeet(BigDecimal netEntitleCubicFeet) {
		this.netEntitleCubicFeet = netEntitleCubicFeet;
	}
	@Column(length=20)
	public BigDecimal getRwghCubicFeet() {
		return rwghCubicFeet;
	}
	public void setRwghCubicFeet(BigDecimal rwghCubicFeet) {
		this.rwghCubicFeet = rwghCubicFeet;
	}
	@Column(length=20)
	public BigDecimal getRwghNetCubicFeet() {
		return rwghNetCubicFeet;
	}
	public void setRwghNetCubicFeet(BigDecimal rwghNetCubicFeet) {
		this.rwghNetCubicFeet = rwghNetCubicFeet;
	}
	@Column(length=10)
	public String getUnit1() {
		return unit1;
	}
	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}
	@Column(length=10)
	public String getUnit2() {
		return unit2;
	}
	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}
	@Column(length=20)
	public BigDecimal getEntitleTareWeight() {
		return entitleTareWeight;
	}
	public void setEntitleTareWeight(BigDecimal entitleTareWeight) {
		this.entitleTareWeight = entitleTareWeight;
	}
	@Column(length=30)
	public String getDestinationCounty() {
		return destinationCounty;
	}
	public void setDestinationCounty(String destinationCounty) {
		this.destinationCounty = destinationCounty;
	}
	@Column(length=1)
	public String getDestination24HR() {
		return destination24HR;
	}
	public void setDestination24HR(String destination24HR) {
		this.destination24HR = destination24HR;
	}
	@Column( length=45 )
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	@Column( length=20 )
	public String getDriverCell() {
		return driverCell;
	}
	public void setDriverCell(String driverCell) {
		this.driverCell = driverCell;
	}
	@Column( length=8 )
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	@Column( length=255 )
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	@Column( length=1 )
	public String getFromNonTempStorage() {
		return fromNonTempStorage;
	}
	public void setFromNonTempStorage(String fromNonTempStorage) {
		this.fromNonTempStorage = fromNonTempStorage;
	}
	@Column( length=255 )
	public String getHaulerName() {
		return haulerName;
	}
	public void setHaulerName(String haulerName) {
		this.haulerName = haulerName;
	}
	@Column( length=8 )
	public String getHaulingAgentCode() {
		return haulingAgentCode;
	}
	public void setHaulingAgentCode(String haulingAgentCode) {
		this.haulingAgentCode = haulingAgentCode;
	}
	@Column( length=1 )
	public String getWeaponsIncluded() {
		return weaponsIncluded;
	}
	public void setWeaponsIncluded(String weaponsIncluded) {
		this.weaponsIncluded = weaponsIncluded;
	}
	public Date getTransDocument() {
		return transDocument;
	}
	public void setTransDocument(Date transDocument) {
		this.transDocument = transDocument;
	}
	@Column( length=50 )
	public String getPaperWork() {
		return paperWork;
	}
	public void setPaperWork(String paperWork) {
		this.paperWork = paperWork;
	}
	@Column
	public Date getConfirmOriginAgent() {
		return confirmOriginAgent;
	}
	public void setConfirmOriginAgent(Date confirmOriginAgent) {
		this.confirmOriginAgent = confirmOriginAgent;
	}
	@Column
	public Date getDd1840() {
		return dd1840;
	}
	public void setDd1840(Date dd1840) {
		this.dd1840 = dd1840;
	}
	@Column
	public Date getQuoteOn() {
		return quoteOn;
	}
	public void setQuoteOn(Date quoteOn) {
		this.quoteOn = quoteOn;
	}
	@Column
	public Date getRecivedHVI() {
		return recivedHVI;
	}
	public void setRecivedHVI(Date recivedHVI) {
		this.recivedHVI = recivedHVI;
	}
	@Column
	public Date getRecivedPayment() {
		return recivedPayment;
	}
	public void setRecivedPayment(Date recivedPayment) {
		this.recivedPayment = recivedPayment;
	}
	@Column
	public Date getRecivedPO() {
		return recivedPO;
	}
	public void setRecivedPO(Date recivedPO) {
		this.recivedPO = recivedPO;
	}
	/**
	 * @return the millitaryShipment
	 */
	@Column(length=1)
	public String getMillitaryShipment() {
		return millitaryShipment;
	}
	/**
	 * @param millitaryShipment the millitaryShipment to set
	 */
	public void setMillitaryShipment(String millitaryShipment) {
		this.millitaryShipment = millitaryShipment;
	}
	/**
	 * @return the dp3
	 */
	@Column
	public Boolean getDp3() {
		return dp3;
	}
	/**
	 * @param dp3 the dp3 to set
	 */
	public void setDp3(Boolean dp3) {
		this.dp3 = dp3;
	}
	/**
	 * @return the originDocsToBase
	 */
	@Column
	public Date getOriginDocsToBase() {
		return originDocsToBase;
	}
	/**
	 * @param originDocsToBase the originDocsToBase to set
	 */
	public void setOriginDocsToBase(Date originDocsToBase) {
		this.originDocsToBase = originDocsToBase;
	}
	/**
	 * @return the millitaryActualWeightDate
	 */
	@Column
	public Date getMillitaryActualWeightDate() {
		return millitaryActualWeightDate;
	}
	/**
	 * @param millitaryActualWeightDate the millitaryActualWeightDate to set
	 */
	public void setMillitaryActualWeightDate(Date millitaryActualWeightDate) {
		this.millitaryActualWeightDate = millitaryActualWeightDate;
	}
	/**
	 * @return the millitaryDeliveryDate
	 */
	@Column
	public Date getMillitaryDeliveryDate() {
		return millitaryDeliveryDate;
	}
	/**
	 * @param millitaryDeliveryDate the millitaryDeliveryDate to set
	 */
	public void setMillitaryDeliveryDate(Date millitaryDeliveryDate) {
		this.millitaryDeliveryDate = millitaryDeliveryDate;
	}
	/**
	 * @return the millitarySitDestinationDate
	 */
	@Column
	public Date getMillitarySitDestinationDate() {
		return millitarySitDestinationDate;
	}
	/**
	 * @param millitarySitDestinationDate the millitarySitDestinationDate to set
	 */
	public void setMillitarySitDestinationDate(Date millitarySitDestinationDate) {
		this.millitarySitDestinationDate = millitarySitDestinationDate;
	}
	/**
	 * @return the millitarySurveyDate
	 */
	@Column
	public Date getMillitarySurveyDate() {
		return millitarySurveyDate;
	}
	/**
	 * @param millitarySurveyDate the millitarySurveyDate to set
	 */
	public void setMillitarySurveyDate(Date millitarySurveyDate) {
		this.millitarySurveyDate = millitarySurveyDate;
	}
	/**
	 * @return the millitarySurveyResults
	 */
	@Column
	public Date getMillitarySurveyResults() {
		return millitarySurveyResults;
	}
	/**
	 * @param millitarySurveyResults the millitarySurveyResults to set
	 */
	public void setMillitarySurveyResults(Date millitarySurveyResults) {
		this.millitarySurveyResults = millitarySurveyResults;
	}
	/**
	 * @return the preApprovalDate1
	 */
	@Column
	public Date getPreApprovalDate1() {
		return preApprovalDate1;
	}
	/**
	 * @param preApprovalDate1 the preApprovalDate1 to set
	 */
	public void setPreApprovalDate1(Date preApprovalDate1) {
		this.preApprovalDate1 = preApprovalDate1;
	}
	/**
	 * @return the preApprovalDate2
	 */
	@Column
	public Date getPreApprovalDate2() {
		return preApprovalDate2;
	}
	/**
	 * @param preApprovalDate2 the preApprovalDate2 to set
	 */
	public void setPreApprovalDate2(Date preApprovalDate2) {
		this.preApprovalDate2 = preApprovalDate2;
	}
	/**
	 * @return the preApprovalDate3
	 */
	@Column
	public Date getPreApprovalDate3() {
		return preApprovalDate3;
	}
	/**
	 * @param preApprovalDate3 the preApprovalDate3 to set
	 */
	public void setPreApprovalDate3(Date preApprovalDate3) {
		this.preApprovalDate3 = preApprovalDate3;
	}
	/**
	 * @return the preApprovalRequest1
	 */
	@Column(length=200)
	public String getPreApprovalRequest1() {
		return preApprovalRequest1;
	}
	/**
	 * @param preApprovalRequest1 the preApprovalRequest1 to set
	 */
	public void setPreApprovalRequest1(String preApprovalRequest1) {
		this.preApprovalRequest1 = preApprovalRequest1;
	}
	/**
	 * @return the preApprovalRequest2
	 */
	@Column(length=200)
	public String getPreApprovalRequest2() {
		return preApprovalRequest2;
	}
	/**
	 * @param preApprovalRequest2 the preApprovalRequest2 to set
	 */
	public void setPreApprovalRequest2(String preApprovalRequest2) {
		this.preApprovalRequest2 = preApprovalRequest2;
	}
	/**
	 * @return the preApprovalRequest3
	 */
	@Column(length=200)
	public String getPreApprovalRequest3() {
		return preApprovalRequest3;
	}
	/**
	 * @param preApprovalRequest3 the preApprovalRequest3 to set
	 */
	public void setPreApprovalRequest3(String preApprovalRequest3) {
		this.preApprovalRequest3 = preApprovalRequest3;
	}
	/**
	 * @return the equipment
	 */
	@Column(length=20)
	public String getEquipment() {
		return equipment;
	}
	/**
	 * @param equipment the equipment to set
	 */
	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}
	/**
	 * @return the shortFuse
	 */
	public Boolean getShortFuse() {
		return shortFuse;
	}
	/**
	 * @param shortFuse the shortFuse to set
	 */
	public void setShortFuse(Boolean shortFuse) {
		this.shortFuse = shortFuse;
	}
	/**
	 * @return the actualPickUp
	 */
	public Date getActualPickUp() {
		return actualPickUp;
	}
	/**
	 * @param actualPickUp the actualPickUp to set
	 */
	public void setActualPickUp(Date actualPickUp) {
		this.actualPickUp = actualPickUp;
	}
	/**
	 * @return the printGbl
	 */
	public Date getPrintGbl() {
		return printGbl;
	}
	/**
	 * @param printGbl the printGbl to set
	 */
	public void setPrintGbl(Date printGbl) {
		this.printGbl = printGbl;
	}
	@Column(length=5)
	public String getSelfHaul() {
		return selfHaul;
	}
	public void setSelfHaul(String selfHaul) {
		this.selfHaul = selfHaul;
	}
	/**
	 * @return the entitleTareWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getEntitleTareWeightKilo() {
		return entitleTareWeightKilo;
	}
	/**
	 * @param entitleTareWeightKilo the entitleTareWeightKilo to set
	 */
	public void setEntitleTareWeightKilo(BigDecimal entitleTareWeightKilo) {
		this.entitleTareWeightKilo = entitleTareWeightKilo;
	}
	/**
	 * @return the chargeableGrossWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getChargeableGrossWeightKilo() {
		return chargeableGrossWeightKilo;
	}
	/**
	 * @param chargeableGrossWeightKilo the chargeableGrossWeightKilo to set
	 */
	public void setChargeableGrossWeightKilo(BigDecimal chargeableGrossWeightKilo) {
		this.chargeableGrossWeightKilo = chargeableGrossWeightKilo;
	}
	/**
	 * @return the chargeableNetWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getChargeableNetWeightKilo() {
		return chargeableNetWeightKilo;
	}
	/**
	 * @param chargeableNetWeightKilo the chargeableNetWeightKilo to set
	 */
	public void setChargeableNetWeightKilo(BigDecimal chargeableNetWeightKilo) {
		this.chargeableNetWeightKilo = chargeableNetWeightKilo;
	}
	/**
	 * @return the chargeableTareWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getChargeableTareWeightKilo() {
		return chargeableTareWeightKilo;
	}
	/**
	 * @param chargeableTareWeightKilo the chargeableTareWeightKilo to set
	 */
	public void setChargeableTareWeightKilo(BigDecimal chargeableTareWeightKilo) {
		this.chargeableTareWeightKilo = chargeableTareWeightKilo;
	}
	/**
	 * @return the rwghGrossKilo
	 */
	@Column(length=20)
	public BigDecimal getRwghGrossKilo() {
		return rwghGrossKilo;
	}
	/**
	 * @param rwghGrossKilo the rwghGrossKilo to set
	 */
	public void setRwghGrossKilo(BigDecimal rwghGrossKilo) {
		this.rwghGrossKilo = rwghGrossKilo;
	}
	/**
	 * @return the rwghNetKilo
	 */
	@Column(length=20)
	public BigDecimal getRwghNetKilo() {
		return rwghNetKilo;
	}
	/**
	 * @param rwghNetKilo the rwghNetKilo to set
	 */
	public void setRwghNetKilo(BigDecimal rwghNetKilo) {
		this.rwghNetKilo = rwghNetKilo;
	}
	/**
	 * @return the rwghTareKilo
	 */
	@Column(length=20)
	public BigDecimal getRwghTareKilo() {
		return rwghTareKilo;
	}
	/**
	 * @param rwghTareKilo the rwghTareKilo to set
	 */
	public void setRwghTareKilo(BigDecimal rwghTareKilo) {
		this.rwghTareKilo = rwghTareKilo;
	}
	/**
	 * @return the actualCubicMtr
	 */
	@Column(length=20)
	public BigDecimal getActualCubicMtr() {
		return actualCubicMtr;
	}
	/**
	 * @param actualCubicMtr the actualCubicMtr to set
	 */
	public void setActualCubicMtr(BigDecimal actualCubicMtr) {
		this.actualCubicMtr = actualCubicMtr;
	}
	/**
	 * @return the entitleCubicMtr
	 */
	@Column(length=20)
	public BigDecimal getEntitleCubicMtr() {
		return entitleCubicMtr;
	}
	/**
	 * @param entitleCubicMtr the entitleCubicMtr to set
	 */
	public void setEntitleCubicMtr(BigDecimal entitleCubicMtr) {
		this.entitleCubicMtr = entitleCubicMtr;
	}
	/**
	 * @return the estimateCubicMtr
	 */
	@Column(length=20)
	public BigDecimal getEstimateCubicMtr() {
		return estimateCubicMtr;
	}
	/**
	 * @param estimateCubicMtr the estimateCubicMtr to set
	 */
	public void setEstimateCubicMtr(BigDecimal estimateCubicMtr) {
		this.estimateCubicMtr = estimateCubicMtr;
	}
	/**
	 * @return the netActualCubicMtr
	 */
	@Column(length=20)
	public BigDecimal getNetActualCubicMtr() {
		return netActualCubicMtr;
	}
	/**
	 * @param netActualCubicMtr the netActualCubicMtr to set
	 */
	public void setNetActualCubicMtr(BigDecimal netActualCubicMtr) {
		this.netActualCubicMtr = netActualCubicMtr;
	}
	/**
	 * @return the netEntitleCubicMtr
	 */
	@Column(length=20)
	public BigDecimal getNetEntitleCubicMtr() {
		return netEntitleCubicMtr;
	}
	/**
	 * @param netEntitleCubicMtr the netEntitleCubicMtr to set
	 */
	public void setNetEntitleCubicMtr(BigDecimal netEntitleCubicMtr) {
		this.netEntitleCubicMtr = netEntitleCubicMtr;
	}
	/**
	 * @return the netEstimateCubicMtr
	 */
	@Column(length=20)
	public BigDecimal getNetEstimateCubicMtr() {
		return netEstimateCubicMtr;
	}
	/**
	 * @param netEstimateCubicMtr the netEstimateCubicMtr to set
	 */
	public void setNetEstimateCubicMtr(BigDecimal netEstimateCubicMtr) {
		this.netEstimateCubicMtr = netEstimateCubicMtr;
	}
	/**
	 * @return the chargeableCubicMtr
	 */
	@Column(length=20)
	public BigDecimal getChargeableCubicMtr() {
		return chargeableCubicMtr;
	}
	/**
	 * @param chargeableCubicMtr the chargeableCubicMtr to set
	 */
	public void setChargeableCubicMtr(BigDecimal chargeableCubicMtr) {
		this.chargeableCubicMtr = chargeableCubicMtr;
	}
	/**
	 * @return the chargeableNetCubicMtr
	 */
	@Column(length=20)
	public BigDecimal getChargeableNetCubicMtr() {
		return chargeableNetCubicMtr;
	}
	/**
	 * @param chargeableNetCubicMtr the chargeableNetCubicMtr to set
	 */
	public void setChargeableNetCubicMtr(BigDecimal chargeableNetCubicMtr) {
		this.chargeableNetCubicMtr = chargeableNetCubicMtr;
	}
	/**
	 * @return the rwghCubicMtr
	 */
	@Column(length=20)
	public BigDecimal getRwghCubicMtr() {
		return rwghCubicMtr;
	}
	/**
	 * @param rwghCubicMtr the rwghCubicMtr to set
	 */
	public void setRwghCubicMtr(BigDecimal rwghCubicMtr) {
		this.rwghCubicMtr = rwghCubicMtr;
	}
	/**
	 * @return the rwghNetCubicMtr
	 */
	@Column(length=20)
	public BigDecimal getRwghNetCubicMtr() {
		return rwghNetCubicMtr;
	}
	/**
	 * @param rwghNetCubicMtr the rwghNetCubicMtr to set
	 */
	public void setRwghNetCubicMtr(BigDecimal rwghNetCubicMtr) {
		this.rwghNetCubicMtr = rwghNetCubicMtr;
	}
	@Column( length=8 )
	public String getHaulingAgentVanlineCode() {
		return haulingAgentVanlineCode;
	}
	public void setHaulingAgentVanlineCode(String haulingAgentVanlineCode) {
		this.haulingAgentVanlineCode = haulingAgentVanlineCode;
	}
	/**
	 * @return the carrierName
	 */
	@Column(length=50)
	public String getCarrierName() {
		return carrierName;
	}
	/**
	 * @param carrierName the carrierName to set
	 */
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	
	@Column(length=20)
	public double getOriginScore() {
		return originScore;
	}
	/**
	 * @param originScore the originScore to set
	 */
	public void setOriginScore(double originScore) {
		this.originScore = originScore;
	}
	@Column(length=50)
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column
	public Date getSettledDate() {
		return settledDate;
	}
	public void setSettledDate(Date settledDate) {
		this.settledDate = settledDate;
	}
	public String getPackingDriverId() {
		return packingDriverId;
	}
	public void setPackingDriverId(String packingDriverId) {
		this.packingDriverId = packingDriverId;
	}
	public String getLoadingDriverId() {
		return loadingDriverId;
	}
	public void setLoadingDriverId(String loadingDriverId) {
		this.loadingDriverId = loadingDriverId;
	}
	public String getDeliveryDriverId() {
		return deliveryDriverId;
	}
	public void setDeliveryDriverId(String deliveryDriverId) {
		this.deliveryDriverId = deliveryDriverId;
	}
	public String getSitDestinationDriverId() {
		return sitDestinationDriverId;
	}
	public void setSitDestinationDriverId(String sitDestinationDriverId) {
		this.sitDestinationDriverId = sitDestinationDriverId;
	}
		
	public String getLoadSignature() {
		return loadSignature;
	}
	public void setLoadSignature(String loadSignature) {
		this.loadSignature = loadSignature;
	}
	public String getDeliverySignature() {
		return deliverySignature;
	}
	public void setDeliverySignature(String deliverySignature) {
		this.deliverySignature = deliverySignature;
	}
	public Integer getLoadCount() {
		return loadCount;
	}
	public void setLoadCount(Integer loadCount) {
		this.loadCount = loadCount;
	}
	public Integer getDeliveryCount() {
		return deliveryCount;
	}
	public void setDeliveryCount(Integer deliveryCount) {
		this.deliveryCount = deliveryCount;
	}
	public String getTractorAgentVanLineCode() {
		return tractorAgentVanLineCode;
	}
	public void setTractorAgentVanLineCode(String tractorAgentVanLineCode) {
		this.tractorAgentVanLineCode = tractorAgentVanLineCode;
	}
	public String getTractorAgentName() {
		return tractorAgentName;
	}
	public void setTractorAgentName(String tractorAgentName) {
		this.tractorAgentName = tractorAgentName;
	}
	public String getVanAgentVanLineCode() {
		return vanAgentVanLineCode;
	}
	public void setVanAgentVanLineCode(String vanAgentVanLineCode) {
		this.vanAgentVanLineCode = vanAgentVanLineCode;
	}
	public String getVanAgentName() {
		return vanAgentName;
	}
	public void setVanAgentName(String vanAgentName) {
		this.vanAgentName = vanAgentName;
	}
	public String getVanID() {
		return vanID;
	}
	public void setVanID(String vanID) {
		this.vanID = vanID;
	}
	public String getVanName() {
		return vanName;
	}
	public void setVanName(String vanName) {
		this.vanName = vanName;
	}
	@Column
	public String getTripNumber() {
		return tripNumber;
	}
	public void setTripNumber(String tripNumber) {
		this.tripNumber = tripNumber;
	}
	@Column
	public Boolean getG11() {
		return g11;
	}
	public void setG11(Boolean g11) {
		this.g11 = g11;
	}
	@Column
	public String getUgwIntId() {
		return ugwIntId;
	}
	public void setUgwIntId(String ugwIntId) {
		this.ugwIntId = ugwIntId;
	}
	@Column
	public BigDecimal getTotalActualRevenue() {
		return totalActualRevenue;
	}
	public void setTotalActualRevenue(BigDecimal totalActualRevenue) {
		this.totalActualRevenue = totalActualRevenue;
	}
	@Column
	public Date getAssignDate() {
		return assignDate;
	}
	public void setAssignDate(Date assignDate) {
		this.assignDate = assignDate;
	}
	@Column
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public BigDecimal getTotalDiscountPercentage() {
		return totalDiscountPercentage;
	}
	public void setTotalDiscountPercentage(BigDecimal totalDiscountPercentage) {
		this.totalDiscountPercentage = totalDiscountPercentage;
	}
	public BigDecimal getActualLineHaul() {
		return actualLineHaul;
	}
	public void setActualLineHaul(BigDecimal actualLineHaul) {
		this.actualLineHaul = actualLineHaul;
	}
	public String getExcessWeightBilling() {
		return excessWeightBilling;
	}
	public void setExcessWeightBilling(String excessWeightBilling) {
		this.excessWeightBilling = excessWeightBilling;
	}
}

