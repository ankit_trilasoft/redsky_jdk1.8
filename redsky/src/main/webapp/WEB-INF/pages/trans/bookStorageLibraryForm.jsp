<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";
while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_OPS") || userRole.equalsIgnoreCase("ROLE_OPS_MGMT")){
		userRole=role.getName();
		break;
	}	
}%>
<head>
<title>Storage Detail</title>
<meta name="heading" content="Storage Detail" />
<link href='<s:url value="/css/main.css"/>' rel="stylesheet"
	type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>

	
</head>
<s:form id="bookStorageLibraryForm" action="saveBookStorageLibraries" method="post" validate="true">
<div id="Layer1" style="width:100%">	
	<s:hidden name="countLocationDetailNotes" value="${countTicketLocationNotes}"/>
	<s:hidden name="id1" value="<%=request.getParameter("id1") %>" />
	<s:hidden name="wtId" value="<%=request.getParameter("id1") %>" />
	<s:hidden name="id" value="<%=request.getParameter("id") %>" />
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="bookStorage.serviceOrderId" value="%{workTicket.serviceOrderId}" />
	<s:hidden name="soId" value="%{workTicket.serviceOrderId}" />
	<c:set var="countLocationDetailNotes" value="${countTicketLocationNotes}"/>
	<c:set var="id" value="<%=request.getParameter("id") %>" />
	<c:set var="id1" value="<%=request.getParameter("id1") %>" />

    <s:hidden name="storageLibrary.usedVolumeCft" />
    <s:hidden name="storageLibrary.usedVolumeCbm" />
    <s:hidden name="storageLibrary.availVolumeCft" />
    <s:hidden name="storageLibrary.availVolumeCbm" />
    <s:hidden name="addedUtilizedVolCft" />
    <s:hidden name="addedUtilizedVolCbm" />
    <s:hidden name="subAvailVolCft" />
    <s:hidden name="subAvailVolCbm" />    
    <s:hidden name="oldVolume"/> 
    <s:hidden name="oldVolume1"/>
    <s:hidden name="oldUnit"/>
    <s:hidden name="oldPersistedVolume"/>        
    
	<s:hidden name="serviceOrder.id" />
	<div id="Layer5" style="width:100%;">
		<div id="newmnav">
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			  <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			  
			  <c:if test="${serviceOrder.job !='INT'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
			  <c:if test="${serviceOrder.job =='RLO'}">
			  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			  </c:if>
			  <c:if test="${serviceOrder.job !='RLO'}">
			  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			  </c:if>
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			   <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div>
		
		</div>
		<table width="100%"><tr><td>
		<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%></td></tr></table>
		
		<div id="newmnav">
		   			 <ul>
					  	<li><a href="editWorkTicketUpdate.html?id=<%=request.getParameter("id1") %>"><span>Work Ticket</span></a></li>
						<li><a href="bookStorageLibraries.html?id=<%=request.getParameter("id1") %>"><span>Storage List</span></a></li>
						<li id="newmnav1" style="background:#FFF"><a class="current"><span>Add Storage<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						<li><a href="storageUnit.html?id=<%=request.getParameter("id1") %>"><span>Access/Release Storage</span></a></li>
					  	<li><a href="storageUnitMove.html?id=<%=request.getParameter("id1") %>"><span>Move Storage Location</span></a></li>					  	
					</ul>
		</div><div class="spn">&nbsp;</div>
		
	
	<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
	<table class="" cellspacing="1" cellpadding="0"border="0">
		<tbody>
			<tr>
				<td>
				<table cellspacing="0" cellpadding="3" border="0">
					<tbody>
						<tr>
							<td class="listwhite" align="right" width="20px"></td>
							<td class="listwhitetext" align="right" width="150px">Storage Id</td>
							<td><s:textfield name="storage.storageId" readonly="true" cssClass="input-textUpper"/></td>
							<td class="listwhite" align="right" width="20px"></td>
							<td class="listwhitetext" align="right" width="250px">Location</td>
							<td><s:textfield name="bookStorage.locationId" readonly="true" cssStyle="width:175px;" cssClass="input-textUpper"/></td>
							<td></td>
							<td class="listwhitetext" align="right" width="300px">ID</td>
							<td><s:textfield name="bookStorage.id" size="10" maxlength="10" readonly="true" cssClass="input-textUpper"/></td>
							<c:if test="${empty bookStorage.id}">
								<td align="right" width="400px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty bookStorage.id}">
								<c:choose>
									<c:when test="${countTicketLocationNotes == '0' || countTicketLocationNotes == '' || countTicketLocationNotes == null}">
								
								<td align="right" width="300px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${id1}&ticket=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${id1}&notesId=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);" ></a></td>
								</c:when>
								<c:otherwise>
								
								<td align="right" width="300px"><img src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${id1}&ticket=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${id1}&notesId=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);" ></a></td>
								</c:otherwise>
								</c:choose> 
							</c:if>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right" valign="top" style="padding-top:5px">Description</td>
							<td colspan="2">
								<c:if test="${bookStorage.what=='R'}">
									<s:textarea name="bookStorage.description" cols="30" rows="4" cssStyle="border:1px solid #219DD1;" disabled="true"/>
								</c:if>
								<c:if test="${bookStorage.what!='R'}">
									<s:textarea name="bookStorage.description" cols="30" rows="4" cssStyle="border:1px solid #219DD1;"/>
								</c:if>
							</td>
							<td class="listwhitetext" align="right" valign="top" style="padding-top:5px" width="250px">Storage Action</td>
							<td align="left" valign="top"><s:textfield name="bookStorage.what" size="10" maxlength="10" cssClass="input-textUpper" tabindex="5" readonly="true"/></td>
						</tr>
						<tr>
							<td ></td>
							<td class="listwhitetext" align="right">Container ID</td>
							<td>
								<c:if test="${bookStorage.what!='R'}">
									<s:textfield name="bookStorage.containerId" size="20" maxlength="10" cssClass="input-text" />
								</c:if>	
								<c:if test="${bookStorage.what=='R'}">
									<s:textfield name="bookStorage.containerId" size="20" maxlength="10" cssClass="input-textUpper" readonly="true"/>
								</c:if>
							</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Model</td>
							<td>
								<c:if test="${bookStorage.what=='R'}">
									<s:textfield name="storage.model" size="20" maxlength="10" cssClass="input-textUpper" readonly="true"/>
								</c:if>
								<c:if test="${bookStorage.what!='R'}">
									<s:textfield name="storage.model" size="20" maxlength="10" cssClass="input-text" />
								</c:if>
							</td>
							<td></td>
						</tr>
						<tr>
			
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Serial</td>
							<td>
								<c:if test="${bookStorage.what=='R'}">
									<s:textfield name="storage.serial" size="20" maxlength="10" cssClass="input-textUpper" readonly="true" />
								</c:if>
								<c:if test="${bookStorage.what!='R'}">
									<s:textfield name="storage.serial" size="20" maxlength="10" cssClass="input-text" />
								</c:if>
							</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Weight</td>
							<td>
							<c:if test="${bookStorage.what=='R'}">
								<s:textfield name="storage.measQuantity" size="20" maxlength="10" cssStyle="text-align:right" cssClass="input-textUpper" readonly="true"/>
							</c:if>
							<c:if test="${bookStorage.what!='R'}">
								<s:textfield name="storage.measQuantity" size="20" maxlength="10" cssStyle="text-align:right" cssClass="input-text"/>
							</c:if>	
							</td>
							
							<td align="left" colspan="" valign="middle" class="listwhitetext" width="150px"><fmt:message key='labels.units'/>
								<c:if test="${bookStorage.what=='R'}">
									<s:radio name="storage.unit" list="%{weightunits}" disabled="true"  onchange="changeStatus();"/>
								</c:if>
								<c:if test="${bookStorage.what!='R'}">	
									<s:radio name="storage.unit" list="%{weightunits}" onchange="changeStatus();"/>
								</c:if>
							</td>
							<td></td>
						</tr>
						<tr>
						<td></td>
						<td class="listwhitetext" align="right">Volume</td>
						<td>
							<c:if test="${bookStorage.what=='R'}">
								<s:textfield name="storage.volume" size="20" maxlength="10" cssStyle="text-align:right" cssClass="input-textUpper" readonly="true"/>
							</c:if>
							<c:if test="${bookStorage.what!='R'}">
								<s:textfield name="storage.volume" size="20" maxlength="10" cssStyle="text-align:right" cssClass="input-text" onchange="availableVolume();"/>
							</c:if>	
							</td>
							
							<td align="left" colspan="" valign="middle" class="listwhitetext" width="150px"><fmt:message key='labels.units'/>
								<c:if test="${bookStorage.what=='R'}">
									<s:radio name="storage.volUnit" list="%{volumeunits}" disabled="true"  onchange="changeStatus();"/>
								</c:if>
								<c:if test="${bookStorage.what!='R'}">	
									<s:radio name="storage.volUnit" list="%{volumeunits}" onchange="changeStatus();" onclick="availableVolume();"/>
								</c:if>
							</td>
							<td></td>
						</tr>
						
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Item Tag</td>
							<td>
							<c:if test="${bookStorage.what=='R'}">
								<s:textfield name="bookStorage.itemTag" size="20" maxlength="10" cssClass="input-textUpper" readonly="true" />
							</c:if>
							<c:if test="${bookStorage.what!='R'}">
								<s:textfield name="bookStorage.itemTag" size="20" maxlength="10" cssClass="input-text" />
							</c:if>
							</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Pieces</td>
							<td>
								<c:if test="${bookStorage.what=='R'}">
									<s:textfield name="bookStorage.pieces" size="20" maxlength="7" cssStyle="text-align:right" cssClass="input-textUpper" readonly="true"/>
								</c:if>	
								<c:if test="${bookStorage.what!='R'}">
									<s:textfield name="bookStorage.pieces" size="20" maxlength="7" cssStyle="text-align:right" cssClass="input-text" />
								</c:if>	
							</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Price</td>
							<td>
								<c:if test="${bookStorage.what=='R'}">
									<s:textfield name="bookStorage.price" size="20" maxlength="7" cssStyle="text-align:right" cssClass="input-textUpper" readonly="true" />
								</c:if>
								<c:if test="${bookStorage.what!='R'}">
									<s:textfield name="bookStorage.price" size="20" maxlength="7" cssStyle="text-align:right" cssClass="input-text" />
								</c:if>
							</td>
							<td></td>
						</tr>
					  	<!-- 
					  		<s:hidden name="bookStorage.pieces" required="true" cssClass="text medium" />
					  	 -->
						<tr>
							<s:hidden name="bookStorage.idNum" required="true"  />
							<s:hidden name="bookStorage.model" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.oldLocation" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.ticket" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.jobNumber" required="true"  />
							<s:hidden name="bookStorage.measQuantity" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.itemNumber" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.storageId"/>
							<s:hidden name="bookStorage.shipNumber" required="true"  />
							<s:hidden name="bookStorage.corpID"/>
							<s:hidden name="bookStorage.type"/>
							<s:hidden name="bookStorage.dated"/>
							
							
							<s:hidden name="storage.id" required="true" cssClass="text medium" />
							<s:hidden name="storage.idNum" required="true"  />
							<s:hidden name="storage.description" required="true" cssClass="text medium" />
							<s:hidden name="storage.jobNumber" required="true"  />
							<s:hidden name="storage.itemNumber" required="true" cssClass="text medium" />
							<s:hidden name="storage.shipNumber" required="true"  />
							<s:hidden name="storage.corpID" required="true"/>
							<s:hidden name="storage.locationId" required="true" cssClass="text medium" />
							<s:hidden name="storage.price" required="true" cssClass="text medium" />
							<s:hidden name="storage.containerId" required="true"/>
							<s:hidden name="storage.pieces"/>
							<s:hidden name="storage.itemTag" required="true" cssClass="text medium" />
							<s:hidden name="storage.createdOn" required="true" cssClass="text medium" />
							<s:hidden name="storage.createdBy" required="true" cssClass="text medium" />
							<s:hidden name="storage.updatedOn" required="true" cssClass="text medium" />
							<s:hidden name="storage.updatedBy" required="true" cssClass="text medium" />
							<c:if test="${not empty storage.releaseDate}">
		 <s:text id="storageFormattedReleaseDate" name="${FormDateValue}"> <s:param name="value" value="storage.releaseDate" /></s:text>
			 <s:hidden  name="storage.releaseDate" value="%{storageFormattedReleaseDate}" /> 
	 </c:if>
	 <c:if test="${empty storage.releaseDate}">
		 <s:hidden   name="storage.releaseDate"/> 
	 </c:if>
							<s:hidden name="ticket" value="<%=request.getParameter("ticket") %>"/> 
						</tr>
						
						
						<tr>
							<td></td>
							<td> </td>
							<td></td>
							<td> </td>
						</tr>

					</tbody>
				</table>
				
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

<table>
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='bookStorage.createdOn' /></td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${bookStorage.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="bookStorage.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${bookStorage.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>
							<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='bookStorage.createdBy' /></td>
								<c:if test="${not empty bookStorage.id}">
									<s:hidden name="bookStorage.createdBy"/>
									<td><s:label name="createdBy" value="%{bookStorage.createdBy}"/></td>
								</c:if>
								<c:if test="${empty bookStorage.id}">
									<s:hidden name="bookStorage.createdBy" value="${pageContext.request.remoteUser}"/>
									<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
								</c:if>
							<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='bookStorage.updatedOn' /></td>
							
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${bookStorage.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="bookStorage.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${bookStorage.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							
							<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='bookStorage.updatedBy' /></td>
								<c:if test="${not empty bookStorage.id}">
									<s:hidden name="bookStorage.updatedBy"/>
									<td><s:label name="updatedBy" value="%{bookStorage.updatedBy}"/></td>
								</c:if>
								<c:if test="${empty bookStorage.id}">
									<s:hidden name="bookStorage.updatedBy" value="${pageContext.request.remoteUser}"/>
									<td><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
								</c:if>
						</tr>
					</tbody>
				</table>
<%if((userRole.equalsIgnoreCase("ROLE_OPS")) || (userRole.equalsIgnoreCase("ROLE_OPS_MGMT"))){ %>
<div align="left">
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="saveUnit" key="button.save" onmouseover="return chkSelect();" onclick="forwardToMyMessage();" /> 
		<s:reset type="button" key="Reset" cssClass="cssbutton" cssStyle="width:55px; height:25px"/>	
</div>
<%} %>
</div>
</s:form>

<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script type="text/javascript"> 
	function forwardToMyMessage(){ 
		document.forms['bookStorageLibraryForm'].elements['storage.price'].value=document.forms['bookStorageLibraryForm'].elements['bookStorage.price'].value;		
		document.forms['bookStorageLibraryForm'].elements['storage.description'].value=document.forms['bookStorageLibraryForm'].elements['bookStorage.description'].value;		
		document.forms['bookStorageLibraryForm'].elements['storage.containerId'].value=document.forms['bookStorageLibraryForm'].elements['bookStorage.containerId'].value;		
		document.forms['bookStorageLibraryForm'].elements['storage.pieces'].value=document.forms['bookStorageLibraryForm'].elements['bookStorage.pieces'].value;		
		document.forms['bookStorageLibraryForm'].elements['storage.locationId'].value=document.forms['bookStorageLibraryForm'].elements['bookStorage.locationId'].value;
		document.forms['bookStorageLibraryForm'].elements['storage.itemNumber'].value=document.forms['bookStorageLibraryForm'].elements['bookStorage.itemNumber'].value;		
		//document.forms['bookStorageLibraryForm'].elements['storage.corpID'].value=document.forms['bookStorageLibraryForm'].elements['bookStorage.corpID'].value;	
		document.forms['bookStorageLibraryForm'].elements['storage.shipNumber'].value=document.forms['bookStorageLibraryForm'].elements['bookStorage.shipNumber'].value;	
		document.forms['bookStorageLibraryForm'].elements['storage.itemTag'].value=document.forms['bookStorageLibraryForm'].elements['bookStorage.itemTag'].value;			
		//document.forms['bookStorageLibraryForm'].elements['storage.unit'].value=document.forms['bookStorageLibraryForm'].elements['storage.price'].value;		
	}
</script>
<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || ( keyCode==110); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
	
	function notExists(){
			alert("The Ticket information is not saved yet");
		}
		
	function chkSelect()
	{
		if (checkFloat('bookStorageLibraryForm','storage.measQuantity','Invalid data in Weight') == false)
           {
              document.forms['bookStorageLibraryForm'].elements['storage.measQuantity'].focus();
              return false
           }
           if (checkFloat('bookStorageLibraryForm','storage.volume','Invalid data in Volume') == false)
           {
              document.forms['bookStorageLibraryForm'].elements['storage.volume'].focus();
              return false
           }
           if (checkFloat('bookStorageLibraryForm','bookStorage.pieces','Invalid data in Pieces') == false)
           {
              document.forms['bookStorageLibraryForm'].elements['bookStorage.pieces'].focus();
              return false
           }
           if (checkFloat('bookStorageLibraryForm','bookStorage.price','Invalid data in Price') == false)
           {
              document.forms['bookStorageLibraryForm'].elements['bookStorage.price'].focus();
              return false
           }	
	}
	
		// fuction for updating the volume in storage lib
function availableVolume(){
		var n=0;
		var unit = "";
		var rads =document.forms['bookStorageLibraryForm'].elements['storage.volUnit'];
		for (var i=0; i < rads.length; i++)
		    {
		    if (rads[i].checked)
		       {
		           unit = rads[i].value;
		       }
		    }
		var volume = document.forms['bookStorageLibraryForm'].elements['storage.volume'].value;
		var oldVolume = document.forms['bookStorageLibraryForm'].elements['oldVolume'].value;
		var oldVolume1 = document.forms['bookStorageLibraryForm'].elements['oldVolume1'].value;
		var availableVolCft=document.forms['bookStorageLibraryForm'].elements['storageLibrary.availVolumeCft'].value;
		var availableVolCbm=document.forms['bookStorageLibraryForm'].elements['storageLibrary.availVolumeCbm'].value;
		if(unit=='Cft')
		{
			var vol1=volume-oldVolume;	
			var availeLimit = eval(availableVolCft)-eval(vol1);		
			if(availeLimit<0)
			{
				var agree='This storage library has only '+availableVolCft+' available capacity.';
				alert(agree);
				document.forms['bookStorageLibraryForm'].elements['subAvailVolCbm'].value = eval(availableVolCbm);
				document.forms['bookStorageLibraryForm'].elements['subAvailVolCft'].value = eval(availableVolCft);
				document.forms['bookStorageLibraryForm'].elements['addedUtilizedVolCft'].value = document.forms['bookStorageLibraryForm'].elements['storageLibrary.usedVolumeCft'].value;
				document.forms['bookStorageLibraryForm'].elements['addedUtilizedVolCbm'].value = document.forms['bookStorageLibraryForm'].elements['storageLibrary.usedVolumeCbm'].value;
				
				setVolUnit(document.forms['bookStorageLibraryForm'].elements['oldUnit'].value);
				document.forms['bookStorageLibraryForm'].elements['storage.volume'].value=document.forms['bookStorageLibraryForm'].elements['oldVolume'].value;
			}else{			
				 var cubicMeter =(0.0283168466) * vol1;
				 var cubicMeterValue=cubicMeter.toFixed(2);
				 var utilizedVolCft1=eval(document.forms['bookStorageLibraryForm'].elements['storageLibrary.usedVolumeCft'].value);
				 var utilizedVolCbm1=eval(document.forms['bookStorageLibraryForm'].elements['storageLibrary.usedVolumeCbm'].value);
				 var addedUtilizedCft = utilizedVolCft1 + eval(vol1);
				 var addedUtilizedCbm = utilizedVolCbm1 + eval(cubicMeterValue);
				 var availeLimitCbm = eval(availableVolCbm)-eval(cubicMeterValue);		 
				 document.forms['bookStorageLibraryForm'].elements['subAvailVolCbm'].value=availeLimitCbm;
				 document.forms['bookStorageLibraryForm'].elements['subAvailVolCft'].value=availeLimit;
				 document.forms['bookStorageLibraryForm'].elements['addedUtilizedVolCft'].value = addedUtilizedCft;
				 document.forms['bookStorageLibraryForm'].elements['addedUtilizedVolCbm'].value = addedUtilizedCbm;
			 }
		}
		else{
		var vol1=volume-oldVolume1;	
		var availeLimit =availableVolCbm-vol1;		
		if(availeLimit<0)
		{
		    var agree='This storage library has only '+availableVolCbm+' available capacity.';
		    alert(agree);
			document.forms['bookStorageLibraryForm'].elements['subAvailVolCbm'].value = eval(availableVolCbm);
			document.forms['bookStorageLibraryForm'].elements['subAvailVolCft'].value = eval(availableVolCft);
			document.forms['bookStorageLibraryForm'].elements['addedUtilizedVolCft'].value = document.forms['bookStorageLibraryForm'].elements['storageLibrary.usedVolumeCft'].value;
			document.forms['bookStorageLibraryForm'].elements['addedUtilizedVolCbm'].value = document.forms['bookStorageLibraryForm'].elements['storageLibrary.usedVolumeCbm'].value;			
			//document.forms['bookStorageLibraryForm'].elements['storage.volUnit'].checked = document.forms['bookStorageLibraryForm'].elements['oldUnit'].value;
			setVolUnit(document.forms['bookStorageLibraryForm'].elements['oldUnit'].value);
			document.forms['bookStorageLibraryForm'].elements['storage.volume'].value=document.forms['bookStorageLibraryForm'].elements['oldVolume1'].value;
		}else{
			var cubicFeet =(35.3146) * vol1;
			var cubicFeetValue=cubicFeet.toFixed(2);
			var availeLimitCft = eval(availableVolCft)-eval(cubicFeetValue);
			document.forms['bookStorageLibraryForm'].elements['subAvailVolCbm'].value=availeLimit;
			document.forms['bookStorageLibraryForm'].elements['subAvailVolCft'].value=availeLimitCft;
			var utilizedVolCbm2=eval(document.forms['bookStorageLibraryForm'].elements['storageLibrary.usedVolumeCbm'].value);
			var utilizedVolCft2=eval(document.forms['bookStorageLibraryForm'].elements['storageLibrary.usedVolumeCft'].value);
			var addedUtilizedCbm = utilizedVolCbm2 + eval(vol1);
			var addedUtilizedCft = utilizedVolCft2 + eval(cubicFeetValue);
			document.forms['bookStorageLibraryForm'].elements['addedUtilizedVolCbm'].value = addedUtilizedCbm;
			document.forms['bookStorageLibraryForm'].elements['addedUtilizedVolCft'].value = addedUtilizedCft;	
	      }
	 }
}
function setVolUnit(oldvalue)
{
	var rads = document.forms['bookStorageLibraryForm'].elements['storage.volUnit'];
	for (var i=0; i < rads.length; i++)
	    {
	    rads[i].checked = false;
		if (rads[i].value==oldvalue)
	       {
	           rads[i].checked=true;
	       }
	    }	
}
		
function onloadConversion()
{
	var oldUnitValuation = document.forms['bookStorageLibraryForm'].elements['oldUnit'].value;
	var oldQuntity = document.forms['bookStorageLibraryForm'].elements['storage.volume'].value;
	if(oldUnitValuation=='Cft'){
		document.forms['bookStorageLibraryForm'].elements['oldVolume'].value = document.forms['bookStorageLibraryForm'].elements['storage.volume'].value;
		var cubicMeter =(0.0283168466) * oldQuntity;
		document.forms['bookStorageLibraryForm'].elements['oldVolume1'].value = cubicMeter;
	}else{
		document.forms['bookStorageLibraryForm'].elements['oldVolume1'].value = document.forms['bookStorageLibraryForm'].elements['storage.volume'].value;
		var cubicFeet =(35.3146) * oldQuntity;
		document.forms['bookStorageLibraryForm'].elements['oldVolume'].value = cubicFeet;
	}
}		
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript"> 
onloadConversion();
  try{  
<c:if test="${bookStorage.what=='R'}">
document.forms['bookStorageLibraryForm'].elements['method:saveUnit'].disabled=true;
document.forms['bookStorageLibraryForm'].elements['Reset'].disabled=true;
</c:if>
}
catch(e){}

    document.forms['bookStorageLibraryForm'].elements['bookStorage.price'].value = '${storage.price}';
	document.forms['bookStorageLibraryForm'].elements['bookStorage.description'].value = '${storage.description}';	
	document.forms['bookStorageLibraryForm'].elements['bookStorage.containerId'].value = '${storage.containerId}';
	document.forms['bookStorageLibraryForm'].elements['bookStorage.pieces'].value = '${storage.pieces}';		
	document.forms['bookStorageLibraryForm'].elements['bookStorage.locationId'].value = '${storage.locationId}';
	document.forms['bookStorageLibraryForm'].elements['bookStorage.itemNumber'].value = '${storage.itemNumber}';		
	document.forms['bookStorageLibraryForm'].elements['bookStorage.shipNumber'].value = '${storage.shipNumber}';	
	document.forms['bookStorageLibraryForm'].elements['bookStorage.itemTag'].value = '${storage.itemTag}';	
		
</script>

