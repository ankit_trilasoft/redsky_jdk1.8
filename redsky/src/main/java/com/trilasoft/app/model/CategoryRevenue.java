package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="categoryrevenue")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class CategoryRevenue extends BaseObject{
	
	private Long id;
	private String corpID;
	private String totalCrew;
	private String numberOfCrew1;
	private String percentPerPerson1;
	private String numberOfCrew2;
	private String percentPerPerson2;
	private String numberOfCrew3;
	private String percentPerPerson3;
	private String numberOfCrew4;
	private String percentPerPerson4;
	private String numberOfCrew5;
	private String percentPerPerson5;
	private String numberOfCrew6;
	private String percentPerPerson6;
	private String numberOfCrew7;
	private String percentPerPerson7;
	private String numberOfCrew8;
	private String percentPerPerson8;
	private String numberOfCrew9;
	private String percentPerPerson9;
	private String numberOfCrew10;
	private String percentPerPerson10;
	private String totalpercentage;
	private String secondSet;
	
	private String companyDivision;
    
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Date valueDate;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("totalCrew", totalCrew).append("numberOfCrew1",
				numberOfCrew1).append("percentPerPerson1", percentPerPerson1)
				.append("numberOfCrew2", numberOfCrew2).append(
						"percentPerPerson2", percentPerPerson2).append(
						"numberOfCrew3", numberOfCrew3).append(
						"percentPerPerson3", percentPerPerson3).append(
						"numberOfCrew4", numberOfCrew4).append(
						"percentPerPerson4", percentPerPerson4).append(
						"numberOfCrew5", numberOfCrew5).append(
						"percentPerPerson5", percentPerPerson5).append(
						"numberOfCrew6", numberOfCrew6).append(
						"percentPerPerson6", percentPerPerson6).append(
						"numberOfCrew7", numberOfCrew7).append(
						"percentPerPerson7", percentPerPerson7).append(
						"numberOfCrew8", numberOfCrew8).append(
						"percentPerPerson8", percentPerPerson8).append(
						"numberOfCrew9", numberOfCrew9).append(
						"percentPerPerson9", percentPerPerson9).append(
						"numberOfCrew10", numberOfCrew10).append(
						"percentPerPerson10", percentPerPerson10).append(
						"totalpercentage", totalpercentage).append("secondSet",
						secondSet).append("companyDivision", companyDivision)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("valueDate", valueDate).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CategoryRevenue))
			return false;
		CategoryRevenue castOther = (CategoryRevenue) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(totalCrew, castOther.totalCrew)
				.append(numberOfCrew1, castOther.numberOfCrew1).append(
						percentPerPerson1, castOther.percentPerPerson1).append(
						numberOfCrew2, castOther.numberOfCrew2).append(
						percentPerPerson2, castOther.percentPerPerson2).append(
						numberOfCrew3, castOther.numberOfCrew3).append(
						percentPerPerson3, castOther.percentPerPerson3).append(
						numberOfCrew4, castOther.numberOfCrew4).append(
						percentPerPerson4, castOther.percentPerPerson4).append(
						numberOfCrew5, castOther.numberOfCrew5).append(
						percentPerPerson5, castOther.percentPerPerson5).append(
						numberOfCrew6, castOther.numberOfCrew6).append(
						percentPerPerson6, castOther.percentPerPerson6).append(
						numberOfCrew7, castOther.numberOfCrew7).append(
						percentPerPerson7, castOther.percentPerPerson7).append(
						numberOfCrew8, castOther.numberOfCrew8).append(
						percentPerPerson8, castOther.percentPerPerson8).append(
						numberOfCrew9, castOther.numberOfCrew9).append(
						percentPerPerson9, castOther.percentPerPerson9).append(
						numberOfCrew10, castOther.numberOfCrew10).append(
						percentPerPerson10, castOther.percentPerPerson10)
				.append(totalpercentage, castOther.totalpercentage).append(
						secondSet, castOther.secondSet).append(companyDivision,
						castOther.companyDivision).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(valueDate,
						castOther.valueDate).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(totalCrew).append(numberOfCrew1).append(
						percentPerPerson1).append(numberOfCrew2).append(
						percentPerPerson2).append(numberOfCrew3).append(
						percentPerPerson3).append(numberOfCrew4).append(
						percentPerPerson4).append(numberOfCrew5).append(
						percentPerPerson5).append(numberOfCrew6).append(
						percentPerPerson6).append(numberOfCrew7).append(
						percentPerPerson7).append(numberOfCrew8).append(
						percentPerPerson8).append(numberOfCrew9).append(
						percentPerPerson9).append(numberOfCrew10).append(
						percentPerPerson10).append(totalpercentage).append(
						secondSet).append(companyDivision).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn).append(
						valueDate).toHashCode();
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length=25)
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=5)
	public String getNumberOfCrew1() {
		return numberOfCrew1;
	}
	public void setNumberOfCrew1(String numberOfCrew1) {
		this.numberOfCrew1 = numberOfCrew1;
	}
	@Column(length=5)
	public String getNumberOfCrew10() {
		return numberOfCrew10;
	}
	public void setNumberOfCrew10(String numberOfCrew10) {
		this.numberOfCrew10 = numberOfCrew10;
	}
	@Column(length=5)
	public String getNumberOfCrew2() {
		return numberOfCrew2;
	}
	public void setNumberOfCrew2(String numberOfCrew2) {
		this.numberOfCrew2 = numberOfCrew2;
	}
	@Column(length=5)
	public String getNumberOfCrew3() {
		return numberOfCrew3;
	}
	public void setNumberOfCrew3(String numberOfCrew3) {
		this.numberOfCrew3 = numberOfCrew3;
	}
	@Column(length=5)
	public String getNumberOfCrew4() {
		return numberOfCrew4;
	}
	public void setNumberOfCrew4(String numberOfCrew4) {
		this.numberOfCrew4 = numberOfCrew4;
	}
	@Column(length=5)
	public String getNumberOfCrew5() {
		return numberOfCrew5;
	}
	public void setNumberOfCrew5(String numberOfCrew5) {
		this.numberOfCrew5 = numberOfCrew5;
	}
	@Column(length=5)
	public String getNumberOfCrew6() {
		return numberOfCrew6;
	}
	public void setNumberOfCrew6(String numberOfCrew6) {
		this.numberOfCrew6 = numberOfCrew6;
	}
	@Column(length=5)
	public String getNumberOfCrew7() {
		return numberOfCrew7;
	}
	public void setNumberOfCrew7(String numberOfCrew7) {
		this.numberOfCrew7 = numberOfCrew7;
	}
	@Column(length=5)
	public String getNumberOfCrew8() {
		return numberOfCrew8;
	}
	public void setNumberOfCrew8(String numberOfCrew8) {
		this.numberOfCrew8 = numberOfCrew8;
	}
	@Column(length=5)
	public String getNumberOfCrew9() {
		return numberOfCrew9;
	}
	public void setNumberOfCrew9(String numberOfCrew9) {
		this.numberOfCrew9 = numberOfCrew9;
	}
	@Column(length=10)
	public String getPercentPerPerson1() {
		return percentPerPerson1;
	}
	public void setPercentPerPerson1(String percentPerPerson1) {
		this.percentPerPerson1 = percentPerPerson1;
	}
	@Column(length=10)
	public String getPercentPerPerson10() {
		return percentPerPerson10;
	}
	public void setPercentPerPerson10(String percentPerPerson10) {
		this.percentPerPerson10 = percentPerPerson10;
	}
	@Column(length=10)
	public String getPercentPerPerson2() {
		return percentPerPerson2;
	}
	public void setPercentPerPerson2(String percentPerPerson2) {
		this.percentPerPerson2 = percentPerPerson2;
	}
	@Column(length=10)
	public String getPercentPerPerson3() {
		return percentPerPerson3;
	}
	public void setPercentPerPerson3(String percentPerPerson3) {
		this.percentPerPerson3 = percentPerPerson3;
	}
	@Column(length=10)
	public String getPercentPerPerson4() {
		return percentPerPerson4;
	}
	public void setPercentPerPerson4(String percentPerPerson4) {
		this.percentPerPerson4 = percentPerPerson4;
	}
	@Column(length=10)
	public String getPercentPerPerson5() {
		return percentPerPerson5;
	}
	public void setPercentPerPerson5(String percentPerPerson5) {
		this.percentPerPerson5 = percentPerPerson5;
	}
	@Column(length=10)
	public String getPercentPerPerson6() {
		return percentPerPerson6;
	}
	public void setPercentPerPerson6(String percentPerPerson6) {
		this.percentPerPerson6 = percentPerPerson6;
	}
	@Column(length=10)
	public String getPercentPerPerson7() {
		return percentPerPerson7;
	}
	public void setPercentPerPerson7(String percentPerPerson7) {
		this.percentPerPerson7 = percentPerPerson7;
	}
	@Column(length=10)
	public String getPercentPerPerson8() {
		return percentPerPerson8;
	}
	public void setPercentPerPerson8(String percentPerPerson8) {
		this.percentPerPerson8 = percentPerPerson8;
	}
	@Column(length=10)
	public String getPercentPerPerson9() {
		return percentPerPerson9;
	}
	public void setPercentPerPerson9(String percentPerPerson9) {
		this.percentPerPerson9 = percentPerPerson9;
	}
	@Column(length=50)
	public String getSecondSet() {
		return secondSet;
	}
	public void setSecondSet(String secondSet) {
		this.secondSet = secondSet;
	}
	@Column(length=10)
	public String getTotalCrew() {
		return totalCrew;
	}
	public void setTotalCrew(String totalCrew) {
		this.totalCrew = totalCrew;
	}
	@Column(length=10)
	public String getTotalpercentage() {
		return totalpercentage;
	}
	public void setTotalpercentage(String totalpercentage) {
		this.totalpercentage = totalpercentage;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column(length=25)
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=25)
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	@Column(length=25)
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

}
