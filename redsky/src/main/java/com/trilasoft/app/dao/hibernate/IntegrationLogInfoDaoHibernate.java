package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.IntegrationLogInfoDao;
import com.trilasoft.app.model.IntegrationLogInfo;

public class IntegrationLogInfoDaoHibernate extends GenericDaoHibernate<IntegrationLogInfo, Long> implements IntegrationLogInfoDao {
	public IntegrationLogInfoDaoHibernate() {   
        super(IntegrationLogInfo.class);   
    } 
	public String findSendMoveCloudStatus(String sessionCorpID ,String fileNumber){
		String emailStatus1="";
		List l1 =  this.getSession().createSQLQuery("select detailedStatus from integrationloginfo where corpId='"+ sessionCorpID + "' and sourceOrderNum='" + fileNumber+ "' ").list();
		if((l1!=null) && (!l1.isEmpty()) && (l1.get(0)!=null)){
		
			emailStatus1= l1.get(l1.size() - 1).toString();
		}
		emailStatus1 =emailStatus1.trim();
		return emailStatus1;
		}
	
	public String findSendMoveCloudStatusCrew(String sessionCorpID ,String fileNumber){
		String emailStatus1="";
		List l1 =  this.getSession().createSQLQuery("select detailedStatus from integrationloginfo where corpId='"+ sessionCorpID + "' and sourceOrderNum='" + fileNumber+ "' and destOrderNum<>'' ").list();
		if((l1!=null) && (!l1.isEmpty()) && (l1.get(0)!=null)){
		
			emailStatus1= l1.get(l1.size() - 1).toString();
		}
		emailStatus1 =emailStatus1.trim();
		return emailStatus1;
		}
	public String findSendMoveCloudStatusClaim(String sessionCorpID ,String fileNumber){
		String emailStatus1="";
		List l1 =  this.getSession().createSQLQuery("select detailedStatus from integrationloginfo where corpId='"+ sessionCorpID + "' and sourceOrderNum='" + fileNumber+ "' and destOrderNum<>'' and destOrderNum='Claim' ").list();
		if((l1!=null) && (!l1.isEmpty()) && (l1.get(0)!=null)){
		
			emailStatus1= l1.get(l1.size() - 1).toString();
		}
		emailStatus1 =emailStatus1.trim();
		return emailStatus1;
		}
	public String findSendMoveCloudGlobalStatus(String sessionCorpID ,String fileNumber,String taskType){
		String emailStatus1="";
		List l1 =  this.getSession().createSQLQuery("select detailedStatus from integrationloginfo where corpId='"+ sessionCorpID + "' and sourceOrderNum='" + fileNumber+ "' and destOrderNum<>'' and ugwwOA='"+taskType+"' ").list();
		if((l1!=null) && (!l1.isEmpty()) && (l1.get(0)!=null)){
		
			emailStatus1= l1.get(l1.size() - 1).toString();
		}
		emailStatus1 =emailStatus1.trim();
		return emailStatus1;
		}
}
