package com.trilasoft.app.webapp.convertor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Locale;
import java.util.TimeZone;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.XWorkException;

import ognl.DefaultTypeConverter;

public class DateConvertor extends DefaultTypeConverter {
	private static String MILLISECOND_FORMAT = ".SSS";
    final private static SimpleDateFormat RFC3399_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public Object convertValue(Map context, Object o, Member member, String s, Object value, Class toType) {
        Object result = null;

        if (value == null || toType.isAssignableFrom(value.getClass())) {
            // no need to convert at all, right?
            return value;
        }
        
        if (Date.class.isAssignableFrom(toType)) {
            result = doConvertToDate(context, value, toType);
        }    
        return result;
    }

    private Locale getLocale(Map context) {
        if (context == null) {
            return Locale.getDefault();
        }

        Locale locale = (Locale) context.get(ActionContext.LOCALE);

        if (locale == null) {
            locale = Locale.getDefault();
        }

        return locale;
    }
    
	
	
    private Object doConvertToDate(Map context, Object value, Class toType) {
        Date result = null;

        if ((value instanceof String && value != null && ((String)value).length() > 0) || 
        		(value instanceof String[] && value != null )) {
            String sa = null;
            if (value instanceof String)
            	sa  = (String) value;
            else if (value instanceof String[]){
            	sa  = ((String[]) value)[0];
            }
            Locale locale = getLocale(context);

            DateFormat df = null;
            if (java.sql.Time.class == toType) {
                df = DateFormat.getTimeInstance(DateFormat.MEDIUM, locale);
            } else if (java.sql.Timestamp.class == toType) {
                Date check = null;
                SimpleDateFormat dtfmt = (SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.SHORT,
                        DateFormat.MEDIUM,
                        locale);
                SimpleDateFormat fullfmt = new SimpleDateFormat(dtfmt.toPattern() + MILLISECOND_FORMAT,
                        locale);

                SimpleDateFormat dfmt = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT,
                        locale);

                SimpleDateFormat[] fmts = {fullfmt, dtfmt, dfmt};
                for (int i = 0; i < fmts.length; i++) {
                    try {
                        check = fmts[i].parse(sa);
                        df = fmts[i];
                        if (check != null) {
                            break;
                        }
                    } catch (ParseException ignore) {
                    }
                }
             } else if(java.util.Date.class == toType) {
            	Date check = null;
                SimpleDateFormat d1 = (SimpleDateFormat)DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG, locale);
                SimpleDateFormat d2 = (SimpleDateFormat)DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM, locale);
                SimpleDateFormat d3 = (SimpleDateFormat)DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, locale);
                SimpleDateFormat d4  = new SimpleDateFormat("dd-MMM-yy");
                SimpleDateFormat d5  = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
                SimpleDateFormat[] dfs = {d5, d4, d1, d2, d3, RFC3399_FORMAT}; //added RFC 3339 date format (XW-473)
                String corpTimeZone = (String)ActionContext.getContext().getSession().get("corpTimeZone");
                TimeZone serverZone=  d1.getTimeZone();
                if (corpTimeZone != null ){
                	TimeZone zone = TimeZone.getTimeZone(corpTimeZone);
                	d1.setTimeZone(zone);
                	d2.setTimeZone(zone);
                	d3.setTimeZone(zone);
                	//d4.setTimeZone(zone);
                	d5.setTimeZone(zone);
                	RFC3399_FORMAT.setTimeZone(zone);
                }
                
                for (int i = 0; i < dfs.length; i++) {
                	try {
                		check = dfs[i].parse(sa);
                		df = dfs[i];
                		if (check != null) {
                			break;
                		}
                	}
                	catch (ParseException ignore) {
                	}
                }
            }
            //final fallback for dates without time
            if (df == null){
            	df = DateFormat.getDateInstance(DateFormat.SHORT, locale);
            }
            try {
            	df.setLenient(false); // let's use strict parsing (XW-341)
                result = df.parse(sa);
                if (! (Date.class == toType)) {
                    try {
                        Constructor constructor = toType.getConstructor(new Class[]{long.class});
                        return constructor.newInstance(new Object[]{new Long(result.getTime())});
                    } catch (Exception e) {
                        throw new XWorkException("Couldn't create class " + toType + " using default (long) constructor", e);
                    }
                }
            } catch (ParseException e) {
                throw new XWorkException("Could not parse date", e);
            }
        } else if (Date.class.isAssignableFrom(value.getClass())) {
            result = (Date) value;
        } 
        return result;
    }

	

}
