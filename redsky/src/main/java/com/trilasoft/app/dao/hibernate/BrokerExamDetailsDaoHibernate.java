package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.BrokerExamDetailsDao;
import com.trilasoft.app.model.BrokerExamDetails;

public class BrokerExamDetailsDaoHibernate extends GenericDaoHibernate<BrokerExamDetails, Long> implements BrokerExamDetailsDao {

	public BrokerExamDetailsDaoHibernate()
			 {
				super(BrokerExamDetails.class);
				// TODO Auto-generated constructor stub
			 }
	
	public List findByBrokerExamDetailsList(String shipNumber) {
		return getHibernateTemplate().find("from BrokerExamDetails where shipNumber=? ",shipNumber);
	}
	public void updateBrokerDetailsAjaxFromList(Long id,String fieldName,String fieldValue,String sessionCorpID,String shipNumber, String userName,String fieldType){
		String query="";
		String actualDate="";
		try{
			if (fieldValue!=null && fieldType.equals("D")) {
				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
	              Date du = new Date();
	              du = df.parse(fieldValue);
	              df = new SimpleDateFormat("yyyy-MM-dd");
	              actualDate = df.format(du);
	 			fieldValue=actualDate;
	 			query="update brokerexamdetails set "+fieldName+"='"+fieldValue+"', updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' and shipNumber='"+shipNumber+"' and corpId='"+sessionCorpID+"' ";
	 			}
			else if (fieldValue!=null && fieldType.equals("B")) {
	 			query="update brokerexamdetails set "+fieldName+"="+fieldValue+", updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' and shipNumber='"+shipNumber+"' and corpId='"+sessionCorpID+"' ";
	 			}
			else{
	 				query="update brokerexamdetails set "+fieldName+"='"+fieldValue+"', updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' and shipNumber='"+shipNumber+"' and corpId='"+sessionCorpID+"' ";
	 			}
		this.getSession().createSQLQuery(query).executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
}

}
