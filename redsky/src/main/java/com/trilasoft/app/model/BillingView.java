package com.trilasoft.app.model;

public class BillingView {
	private Long id;
	private String shipNumber;
	private String billingCurrency;
	private String corpID;
	private String insuranceVatDescr;
	 private String storageVatDescr;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	public String getBillingCurrency() {
		return billingCurrency;
	}
	public void setBillingCurrency(String billingCurrency) {
		this.billingCurrency = billingCurrency;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	public String getInsuranceVatDescr() {
		return insuranceVatDescr;
	}
	public void setInsuranceVatDescr(String insuranceVatDescr) {
		this.insuranceVatDescr = insuranceVatDescr;
	}
	public String getStorageVatDescr() {
		return storageVatDescr;
	}
	public void setStorageVatDescr(String storageVatDescr) {
		this.storageVatDescr = storageVatDescr;
	}
}
