<%@ include file="/common/taglibs.jsp"%>  

 
<head>   
    <title><fmt:message key="sharingList.title"/></title>   
    <meta name="heading" content="<fmt:message key='sharingList.title'/>"/>   
    
<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
			for(i=0;i<=1;i++)
			{
					document.forms['sharingList'].elements[i].value = "";
			}
}
</script>
<style>
span.pagelinks {
	display:block;
	font-size:0.95em;
	margin-bottom:3px;
	!margin-bottom:2px;
	margin-top:-18px;
	margin-top:-19px;
	padding:2px 0px;
	text-align:right;
	width:99%;
}

form {
margin-top:-40px;
!margin-top:-15px;
}

div#main {
margin:-5px 0 0;

}
</style>

</head>   


<s:form id="sharingList" name="sharingList" action="searchSharing"  validate="true">   
<div id="Layer1" style="width:100%;">  

<div id="otabs">
  <ul>
    <li><a class="current"><span>Search</span></a></li>
  </ul>
</div>
<div class="spnblk">&nbsp;</div>

<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/sharingForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px;" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
    
</c:set> 
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:99%;"  >
<thead>
<tr>
<th><fmt:message key="sharing.code"/></th>
<th><fmt:message key="sharing.discription"/></th>
<th>&nbsp;</th>
</tr></thead>	
		<tbody>
		<tr>
		    <td width="300" align="left"><s:textfield name="sharing.code" required="true" onkeyup="valid(this,'special')" onblur="valid(this,'special')" cssClass="input-text" size="17"/></td>
			<td width="300" align="left"><s:textfield name="sharing.description" required="true" onkeyup="valid(this,'special')"  onblur="valid(this,'special')" cssClass="input-text" size="40"/></td>
			<td  align="left"><c:out value="${searchbuttons}" escapeXml="false"/></td>
			</tr>
			
		</tbody>
	</table>
</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>

<c:out value="${searchresults}" escapeXml="false" />  

		<div id="otabs" style="margin-top:-15px;">
		  <ul>
		    <li><a class="current"><span> Payroll Revenue Sharing List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

 
	<display:table name="sharingSet" class="table" requestURI="" id="SharingSetId" export="true" defaultsort="1" pagesize="10" style="width:100%;margin-top: 1px;" >   
    <display:column property="code" href="sharingForm.html" paramId="id" paramProperty="id" sortable="true" titleKey="sharing.code"/>
   	<display:column property="description"  sortable="true" titleKey="sharing.discription"/>
    <display:column property="usuallyUsedFor" sortable="true" titleKey="sharing.usuallyUsedFor"/>
    
  
    <display:setProperty name="paging.banner.item_name" value="sharing"/>   
    <display:setProperty name="paging.banner.items_name" value="customers"/>   
  
    <display:setProperty name="export.excel.filename" value="Payroll List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Payroll List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Payroll List.pdf"/>   
</display:table>   
  
<c:out value="${buttons}" escapeXml="false" />   
<c:set var="isTrue" value="false" scope="session"/>
</div>
</s:form>  
<script type="text/javascript"> 

highlightTableRows("SharingSetId"); 

</script>
