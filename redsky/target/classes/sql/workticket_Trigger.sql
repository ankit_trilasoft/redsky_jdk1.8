
DELIMITER $$
create trigger redsky.trigger_add_history_workticket BEFORE UPDATE on redsky.workticket
for each row BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

      IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
       select CONCAT(fieldNameList,'workticket.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
   END IF;



   IF (OLD.shipNumber <> NEW.shipNumber ) THEN
       select CONCAT(fieldNameList,'workticket.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
   END IF;

   IF (OLD.registrationNumber <> NEW.registrationNumber ) THEN
       select CONCAT(fieldNameList,'workticket.registrationNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.registrationNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.registrationNumber,'~') into newValueList;
   END IF;

   IF (OLD.targetActual <> NEW.targetActual ) THEN
       select CONCAT(fieldNameList,'workticket.targetActual~') into fieldNameList;
       select CONCAT(oldValueList,OLD.targetActual,'~') into oldValueList;
       select CONCAT(newValueList,NEW.targetActual,'~') into newValueList;
   END IF;

   IF (OLD.originMobile <> NEW.originMobile ) THEN
       select CONCAT(fieldNameList,'workticket.originMobile~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originMobile,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originMobile,'~') into newValueList;
   END IF;

   IF (OLD.destinationMobile <> NEW.destinationMobile ) THEN
       select CONCAT(fieldNameList,'workticket.destinationMobile~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationMobile,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationMobile,'~') into newValueList;
   END IF;

   IF (OLD.storageMeasurement <> NEW.storageMeasurement ) THEN
       select CONCAT(fieldNameList,'workticket.storageMeasurement~') into fieldNameList;
       select CONCAT(oldValueList,OLD.storageMeasurement,'~') into oldValueList;
       select CONCAT(newValueList,NEW.storageMeasurement,'~') into newValueList;
   END IF;

   IF (OLD.ticket <> NEW.ticket ) THEN
       select CONCAT(fieldNameList,'workticket.ticket~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ticket,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ticket,'~') into newValueList;
   END IF;

   IF (OLD.jobMode <> NEW.jobMode ) THEN
       select CONCAT(fieldNameList,'workticket.jobMode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.jobMode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.jobMode,'~') into newValueList;
   END IF;

   IF (OLD.jobType <> NEW.jobType ) THEN
       select CONCAT(fieldNameList,'workticket.jobType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.jobType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.jobType,'~') into newValueList;
   END IF;

   IF (OLD.service <> NEW.service ) THEN
       select CONCAT(fieldNameList,'workticket.service~') into fieldNameList;
       select CONCAT(oldValueList,OLD.service,'~') into oldValueList;
       select CONCAT(newValueList,NEW.service,'~') into newValueList;
   END IF;

   IF (OLD.warehouse <> NEW.warehouse ) THEN
       select CONCAT(fieldNameList,'workticket.warehouse~') into fieldNameList;
       select CONCAT(oldValueList,OLD.warehouse,'~') into oldValueList;
       select CONCAT(newValueList,NEW.warehouse,'~') into newValueList;
   END IF;

   IF ((OLD.estimatedWeight <> NEW.estimatedWeight) or (OLD.estimatedWeight is null and NEW.estimatedWeight is not null) or (OLD.estimatedWeight is not null and NEW.estimatedWeight is null)) THEN
		select CONCAT(fieldNameList,'workticket.estimatedWeight~') into fieldNameList;
		IF(OLD.estimatedWeight is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimatedWeight is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimatedWeight is not null) then
			select CONCAT(oldValueList,OLD.estimatedWeight,'~') into oldValueList;
		END IF;
		IF(NEW.estimatedWeight is not null) then
			select CONCAT(newValueList,NEW.estimatedWeight,'~') into newValueList;
		END IF; 
	END IF;

   IF (OLD.confirm <> NEW.confirm ) THEN
       select CONCAT(fieldNameList,'workticket.confirm~') into fieldNameList;
       select CONCAT(oldValueList,OLD.confirm,'~') into oldValueList;
       select CONCAT(newValueList,NEW.confirm,'~') into newValueList;
   END IF;
   
  IF ((date_format(OLD.confirmDate,'%Y-%m-%d') <> date_format(NEW.confirmDate,'%Y-%m-%d')) or (date_format(OLD.confirmDate,'%Y-%m-%d') is null and date_format(NEW.confirmDate,'%Y-%m-%d') is not null)or (date_format(OLD.confirmDate,'%Y-%m-%d') is not null and date_format(NEW.confirmDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'workticket.confirmDate~') into fieldNameList;
       IF(OLD.confirmDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.confirmDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.confirmDate is not null) THEN
        	select CONCAT(oldValueList,OLD.confirmDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.confirmDate is not null) THEN
        	select CONCAT(newValueList,NEW.confirmDate,'~') into newValueList;
      	END IF;
   END IF;

   IF (OLD.firstName <> NEW.firstName ) THEN
       select CONCAT(fieldNameList,'workticket.firstName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.firstName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.firstName,'~') into newValueList;
   END IF;

   IF (OLD.lastName <> NEW.lastName ) THEN
       select CONCAT(fieldNameList,'workticket.lastName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.lastName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.lastName,'~') into newValueList;
   END IF;

   IF (OLD.phone <> NEW.phone ) THEN
       select CONCAT(fieldNameList,'workticket.phone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.phone,'~') into oldValueList;
       select CONCAT(newValueList,NEW.phone,'~') into newValueList;
   END IF;

   IF (OLD.address1 <> NEW.address1 ) THEN
       select CONCAT(fieldNameList,'workticket.address1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.address1,'~') into oldValueList;
       select CONCAT(newValueList,NEW.address1,'~') into newValueList;
   END IF;
   
   IF (OLD.address2 <> NEW.address2 ) THEN
       select CONCAT(fieldNameList,'workticket.address2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.address2,'~') into oldValueList;
       select CONCAT(newValueList,NEW.address2,'~') into newValueList;
   END IF;
   
   IF (OLD.address3 <> NEW.address3 ) THEN
       select CONCAT(fieldNameList,'workticket.address3~') into fieldNameList;
       select CONCAT(oldValueList,OLD.address3,'~') into oldValueList;
       select CONCAT(newValueList,NEW.address3,'~') into newValueList;
   END IF;
   
   IF (OLD.address4 <> NEW.address4 ) THEN
       select CONCAT(fieldNameList,'workticket.address4~') into fieldNameList;
       select CONCAT(oldValueList,OLD.address4,'~') into oldValueList;
       select CONCAT(newValueList,NEW.address4,'~') into newValueList;
   END IF;

   IF (OLD.city <> NEW.city ) THEN
       select CONCAT(fieldNameList,'workticket.city~') into fieldNameList;
       select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
       select CONCAT(newValueList,NEW.city,'~') into newValueList;
   END IF;
   
   IF (OLD.state <> NEW.state ) THEN
       select CONCAT(fieldNameList,'workticket.state~') into fieldNameList;
       select CONCAT(oldValueList,OLD.state,'~') into oldValueList;
       select CONCAT(newValueList,NEW.state,'~') into newValueList;
   END IF;
   
   IF (OLD.zip <> NEW.zip ) THEN
       select CONCAT(fieldNameList,'workticket.zip~') into fieldNameList;
       select CONCAT(oldValueList,OLD.zip,'~') into oldValueList;
       select CONCAT(newValueList,NEW.zip,'~') into newValueList;
   END IF;
   
   IF (OLD.instructions <> NEW.instructions ) THEN
       select CONCAT(fieldNameList,'workticket.instructions~') into fieldNameList;
       select CONCAT(oldValueList,OLD.instructions,'~') into oldValueList;
       select CONCAT(newValueList,NEW.instructions,'~') into newValueList;
   END IF;
   
   IF (OLD.instructionCode <> NEW.instructionCode ) THEN
       select CONCAT(fieldNameList,'workticket.instructionCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.instructionCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.instructionCode,'~') into newValueList;
   END IF;
   
   IF (OLD.account <> NEW.account ) THEN
       select CONCAT(fieldNameList,'workticket.account~') into fieldNameList;
       select CONCAT(oldValueList,OLD.account,'~') into oldValueList;
       select CONCAT(newValueList,NEW.account,'~') into newValueList;
   END IF;
   
   IF ((OLD.crews <> NEW.crews) or (OLD.crews is null and NEW.crews is not null) or (OLD.crews is not null and NEW.crews is null)) THEN
		select CONCAT(fieldNameList,'workticket.crews~') into fieldNameList;
		IF(OLD.crews is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.crews is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.crews is not null) then
			select CONCAT(oldValueList,OLD.crews,'~') into oldValueList;
		END IF;
		IF(NEW.crews is not null) then
			select CONCAT(newValueList,NEW.crews,'~') into newValueList;
		END IF; 
	END IF;
   
   IF ((OLD.estimatedHours <> NEW.estimatedHours) or (OLD.estimatedHours is null and NEW.estimatedHours is not null) 
	  or (OLD.estimatedHours is not null and NEW.estimatedHours is null)) THEN
		select CONCAT(fieldNameList,'workticket.estimatedHours~') into fieldNameList;
		IF(OLD.estimatedHours is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimatedHours is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimatedHours is not null) then
			select CONCAT(oldValueList,OLD.estimatedHours,'~') into oldValueList;
		END IF;
		IF(NEW.estimatedHours is not null) then
			select CONCAT(newValueList,NEW.estimatedHours,'~') into newValueList;
		END IF; 
	END IF;

   IF ((date_format(OLD.openDate,'%Y-%m-%d') <> date_format(NEW.openDate,'%Y-%m-%d')) or (date_format(OLD.openDate,'%Y-%m-%d') is null and date_format(NEW.openDate,'%Y-%m-%d') is not null)or (date_format(OLD.openDate,'%Y-%m-%d') is not null and date_format(NEW.openDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'workticket.openDate~') into fieldNameList;
       IF(OLD.openDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.openDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.openDate is not null) THEN
        	select CONCAT(oldValueList,OLD.openDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.openDate is not null) THEN
        	select CONCAT(newValueList,NEW.openDate,'~') into newValueList;
      	END IF;
   END IF;
         
   IF (OLD.whoCancelled <> NEW.whoCancelled ) THEN
       select CONCAT(fieldNameList,'workticket.whoCancelled~') into fieldNameList;
       select CONCAT(oldValueList,OLD.whoCancelled,'~') into oldValueList;
       select CONCAT(newValueList,NEW.whoCancelled,'~') into newValueList;
   END IF;

   IF ((OLD.estimatedCartoons <> NEW.estimatedCartoons) or (OLD.estimatedCartoons is null and NEW.estimatedCartoons is not null) 
	  or (OLD.estimatedCartoons is not null and NEW.estimatedCartoons is null)) THEN
		select CONCAT(fieldNameList,'workticket.estimatedCartoons~') into fieldNameList;
		IF(OLD.estimatedCartoons is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimatedCartoons is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimatedCartoons is not null) then
			select CONCAT(oldValueList,OLD.estimatedCartoons,'~') into oldValueList;
		END IF;
		IF(NEW.estimatedCartoons is not null) then
			select CONCAT(newValueList,NEW.estimatedCartoons,'~') into newValueList;
		END IF; 
	END IF;
   
   IF ((OLD.estimatedCubicFeet <> NEW.estimatedCubicFeet) or (OLD.estimatedCubicFeet is null and NEW.estimatedCubicFeet is not null)
	  or (OLD.estimatedCubicFeet is not null and NEW.estimatedCubicFeet is null)) THEN
		select CONCAT(fieldNameList,'workticket.estimatedCubicFeet~') into fieldNameList;
		IF(OLD.estimatedCubicFeet is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.estimatedCubicFeet is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.estimatedCubicFeet is not null) then
			select CONCAT(oldValueList,OLD.estimatedCubicFeet,'~') into oldValueList;
		END IF;
		IF(NEW.estimatedCubicFeet is not null) then
			select CONCAT(newValueList,NEW.estimatedCubicFeet,'~') into newValueList;
		END IF; 
	END IF;
   
   IF (OLD.scheduledArrive <> NEW.scheduledArrive ) THEN
       select CONCAT(fieldNameList,'workticket.scheduledArrive~') into fieldNameList;
       select CONCAT(oldValueList,OLD.scheduledArrive,'~') into oldValueList;
       select CONCAT(newValueList,NEW.scheduledArrive,'~') into newValueList;
   END IF;

   IF (OLD.leaveWareHouse <> NEW.leaveWareHouse ) THEN
       select CONCAT(fieldNameList,'workticket.leaveWareHouse~') into fieldNameList;
       select CONCAT(oldValueList,OLD.leaveWareHouse,'~') into oldValueList;
       select CONCAT(newValueList,NEW.leaveWareHouse,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationAddress1 <> NEW.destinationAddress1 ) THEN
       select CONCAT(fieldNameList,'workticket.destinationAddress1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationAddress1,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationAddress1,'~') into newValueList;
   END IF;
   
   IF (OLD.crewName <> NEW.crewName ) THEN
       select CONCAT(fieldNameList,'workticket.crewName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.crewName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.crewName,'~') into newValueList;
   END IF;
   
   IF (OLD.projectLeadName <> NEW.projectLeadName ) THEN
       select CONCAT(fieldNameList,'workticket.projectLeadName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.projectLeadName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.projectLeadName,'~') into newValueList;
   END IF;
   
   IF (OLD.projectLeadNumber <> NEW.projectLeadNumber ) THEN
       select CONCAT(fieldNameList,'workticket.projectLeadNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.projectLeadNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.projectLeadNumber,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationAddress2 <> NEW.destinationAddress2 ) THEN
       select CONCAT(fieldNameList,'workticket.destinationAddress2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationAddress2,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationAddress2,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationAddress3 <> NEW.destinationAddress3 ) THEN
       select CONCAT(fieldNameList,'workticket.destinationAddress3~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationAddress3,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationAddress3,'~') into newValueList;
   END IF;

   IF (OLD.destinationAddress4 <> NEW.destinationAddress4 ) THEN
       select CONCAT(fieldNameList,'workticket.destinationAddress4~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationAddress4,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationAddress4,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationCity <> NEW.destinationCity ) THEN
       select CONCAT(fieldNameList,'workticket.destinationCity~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationCity,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationCity,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationState <> NEW.destinationState ) THEN
       select CONCAT(fieldNameList,'workticket.destinationState~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationState,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationState,'~') into newValueList;
   END IF;

   IF (OLD.destinationPhone <> NEW.destinationPhone ) THEN
       select CONCAT(fieldNameList,'workticket.destinationPhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationPhone,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationPhone,'~') into newValueList;
   END IF;

   IF (OLD.destinationZip <> NEW.destinationZip ) THEN
       select CONCAT(fieldNameList,'workticket.destinationZip~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationZip,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationZip,'~') into newValueList;
   END IF;
   
   IF (OLD.mi <> NEW.mi ) THEN
       select CONCAT(fieldNameList,'workticket.mi~') into fieldNameList;
       select CONCAT(oldValueList,OLD.mi,'~') into oldValueList;
       select CONCAT(newValueList,NEW.mi,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationCountryCode <> NEW.destinationCountryCode ) THEN
       select CONCAT(fieldNameList,'workticket.destinationCountryCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationCountryCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationCountryCode,'~') into newValueList;
   END IF;

   IF (OLD.destinationCountry <> NEW.destinationCountry ) THEN
       select CONCAT(fieldNameList,'workticket.destinationCountry~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationCountry,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationCountry,'~') into newValueList;
   END IF;
   
   IF (OLD.prefix <> NEW.prefix ) THEN
       select CONCAT(fieldNameList,'workticket.prefix~') into fieldNameList;
       select CONCAT(oldValueList,OLD.prefix,'~') into oldValueList;
       select CONCAT(newValueList,NEW.prefix,'~') into newValueList;
   END IF;
   
   IF (OLD.suffix <> NEW.suffix ) THEN
       select CONCAT(fieldNameList,'workticket.suffix~') into fieldNameList;
       select CONCAT(oldValueList,OLD.suffix,'~') into oldValueList;
       select CONCAT(newValueList,NEW.suffix,'~') into newValueList;
   END IF;

   IF (OLD.homePhone <> NEW.homePhone ) THEN
       select CONCAT(fieldNameList,'workticket.homePhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.homePhone,'~') into oldValueList;
       select CONCAT(newValueList,NEW.homePhone,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationHomePhone <> NEW.destinationHomePhone ) THEN
       select CONCAT(fieldNameList,'workticket.destinationHomePhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationHomePhone,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationHomePhone,'~') into newValueList;
   END IF;
     
   IF ((OLD.actualWeight <> NEW.actualWeight) or (OLD.actualWeight is null and NEW.actualWeight is not null) 
	  or (OLD.actualWeight is not null and NEW.actualWeight is null)) THEN
		select CONCAT(fieldNameList,'workticket.actualWeight~') into fieldNameList;
		IF(OLD.actualWeight is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.actualWeight is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.actualWeight is not null) then
			select CONCAT(oldValueList,OLD.actualWeight,'~') into oldValueList;
		END IF;
		IF(NEW.actualWeight is not null) then
			select CONCAT(newValueList,NEW.actualWeight,'~') into newValueList;
		END IF; 
	END IF;
   
   IF ((OLD.crewRevenue <> NEW.crewRevenue) or (OLD.crewRevenue is null and NEW.crewRevenue is not null) 
	  or (OLD.crewRevenue is not null and NEW.crewRevenue is null)) THEN
		select CONCAT(fieldNameList,'workticket.crewRevenue~') into fieldNameList;
		IF(OLD.crewRevenue is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.crewRevenue is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.crewRevenue is not null) then
			select CONCAT(oldValueList,OLD.crewRevenue,'~') into oldValueList;
		END IF;
		IF(NEW.crewRevenue is not null) then
			select CONCAT(newValueList,NEW.crewRevenue,'~') into newValueList;
		END IF; 
	END IF;
   
   IF (OLD.beginningTime <> NEW.beginningTime ) THEN
       select CONCAT(fieldNameList,'workticket.beginningTime~') into fieldNameList;
       select CONCAT(oldValueList,OLD.beginningTime,'~') into oldValueList;
       select CONCAT(newValueList,NEW.beginningTime,'~') into newValueList;
   END IF;
   
   IF (OLD.contactFax <> NEW.contactFax ) THEN
       select CONCAT(fieldNameList,'workticket.contactFax~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contactFax,'~') into oldValueList;
       select CONCAT(newValueList,NEW.contactFax,'~') into newValueList;
   END IF;
   
   IF (OLD.contactName <> NEW.contactName ) THEN
       select CONCAT(fieldNameList,'workticket.contactName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contactName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.contactName,'~') into newValueList;
   END IF;

   IF (OLD.contactPhone <> NEW.contactPhone ) THEN
       select CONCAT(fieldNameList,'workticket.contactPhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contactPhone,'~') into oldValueList;
       select CONCAT(newValueList,NEW.contactPhone,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationDock <> NEW.destinationDock ) THEN
       select CONCAT(fieldNameList,'workticket.destinationDock~') into fieldNameList;
       IF(OLD.destinationDock = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.destinationDock = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.destinationDock = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.destinationDock = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.acceptByDriver <> NEW.acceptByDriver ) THEN
       select CONCAT(fieldNameList,'workticket.acceptByDriver~') into fieldNameList;
       IF(OLD.acceptByDriver = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.destinationDock = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.acceptByDriver = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.acceptByDriver = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.destinationElevator <> NEW.destinationElevator ) THEN
       select CONCAT(fieldNameList,'workticket.destinationElevator~') into fieldNameList;
       IF(OLD.destinationElevator = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.destinationElevator = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.destinationElevator = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.destinationElevator = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.endTime <> NEW.endTime ) THEN
       select CONCAT(fieldNameList,'workticket.endTime~') into fieldNameList;
       select CONCAT(oldValueList,OLD.endTime,'~') into oldValueList;
       select CONCAT(newValueList,NEW.endTime,'~') into newValueList;
   END IF;
   
   IF (OLD.originContactName <> NEW.originContactName ) THEN
       select CONCAT(fieldNameList,'workticket.originContactName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originContactName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originContactName,'~') into newValueList;
   END IF;
   
   IF (OLD.originContactPhone <> NEW.originContactPhone ) THEN
       select CONCAT(fieldNameList,'workticket.originContactPhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originContactPhone,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originContactPhone,'~') into newValueList;
   END IF;
   
   IF (OLD.originContactWeekend <> NEW.originContactWeekend ) THEN
       select CONCAT(fieldNameList,'workticket.originContactWeekend~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originContactWeekend,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originContactWeekend,'~') into newValueList;
   END IF;
   
   IF (OLD.originDock <> NEW.originDock ) THEN
       select CONCAT(fieldNameList,'workticket.originDock~') into fieldNameList;
       IF(OLD.originDock = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.originDock = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.originDock = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.originDock = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
    IF (OLD.thirdPartyRequired <> NEW.thirdPartyRequired ) THEN
       select CONCAT(fieldNameList,'workticket.thirdPartyRequired~') into fieldNameList;
       IF(OLD.thirdPartyRequired = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.thirdPartyRequired = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.thirdPartyRequired = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.thirdPartyRequired = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.originElevator <> NEW.originElevator ) THEN
       select CONCAT(fieldNameList,'workticket.originElevator~') into fieldNameList;
       IF(OLD.originElevator = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.originElevator = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.originElevator = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.originElevator = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.orderBy <> NEW.orderBy ) THEN
       select CONCAT(fieldNameList,'workticket.orderBy~') into fieldNameList;
       select CONCAT(oldValueList,OLD.orderBy,'~') into oldValueList;
       select CONCAT(newValueList,NEW.orderBy,'~') into newValueList;
   END IF;

   IF ((OLD.stgAmount <> NEW.stgAmount) or (OLD.stgAmount is null and NEW.stgAmount is not null) 
	  or (OLD.stgAmount is not null and NEW.stgAmount is null)) THEN
		select CONCAT(fieldNameList,'workticket.stgAmount~') into fieldNameList;
		IF(OLD.stgAmount is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.stgAmount is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.stgAmount is not null) then
			select CONCAT(oldValueList,OLD.stgAmount,'~') into oldValueList;
		END IF;
		IF(NEW.stgAmount is not null) then
			select CONCAT(newValueList,NEW.stgAmount,'~') into newValueList;
		END IF; 
	END IF;
   
   IF (OLD.stgHow <> NEW.stgHow ) THEN
       select CONCAT(fieldNameList,'workticket.stgHow~') into fieldNameList;
       select CONCAT(oldValueList,OLD.stgHow,'~') into oldValueList;
       select CONCAT(newValueList,NEW.stgHow,'~') into newValueList;
   END IF;
   
   IF ((OLD.trucks <> NEW.trucks) or (OLD.trucks is null and NEW.trucks is not null) 
	  or (OLD.trucks is not null and NEW.trucks is null)) THEN
		select CONCAT(fieldNameList,'workticket.trucks~') into fieldNameList;
		IF(OLD.trucks is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.trucks is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.trucks is not null) then
			select CONCAT(oldValueList,OLD.trucks,'~') into oldValueList;
		END IF;
		IF(NEW.trucks is not null) then
			select CONCAT(newValueList,NEW.trucks,'~') into newValueList;
		END IF; 
	END IF;

   IF ((OLD.revenueCalculation <> NEW.revenueCalculation) or (OLD.revenueCalculation is null and NEW.revenueCalculation is not null) 
	  or (OLD.revenueCalculation is not null and NEW.revenueCalculation is null)) THEN
		select CONCAT(fieldNameList,'workticket.revenueCalculation~') into fieldNameList;
		IF(OLD.revenueCalculation is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.revenueCalculation is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.revenueCalculation is not null) then
			select CONCAT(oldValueList,OLD.revenueCalculation,'~') into oldValueList;
		END IF;
		IF(NEW.revenueCalculation is not null) then
			select CONCAT(newValueList,NEW.revenueCalculation,'~') into newValueList;
		END IF; 
	END IF;
      
   IF ((OLD.revenue <> NEW.revenue) or (OLD.revenue is null and NEW.revenue is not null) 
	  or (OLD.revenue is not null and NEW.revenue is null)) THEN
		select CONCAT(fieldNameList,'workticket.revenue~') into fieldNameList;
		IF(OLD.revenue is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.revenue is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.revenue is not null) then
			select CONCAT(oldValueList,OLD.revenue,'~') into oldValueList;
		END IF;
		IF(NEW.revenue is not null) then
			select CONCAT(newValueList,NEW.revenue,'~') into newValueList;
		END IF; 
	END IF;
   
   IF (OLD.timeFrom <> NEW.timeFrom ) THEN
       select CONCAT(fieldNameList,'workticket.timeFrom~') into fieldNameList;
       select CONCAT(oldValueList,OLD.timeFrom,'~') into oldValueList;
       select CONCAT(newValueList,NEW.timeFrom,'~') into newValueList;
   END IF;

   IF (OLD.timeTo <> NEW.timeTo ) THEN
       select CONCAT(fieldNameList,'workticket.timeTo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.timeTo,'~') into oldValueList;
       select CONCAT(newValueList,NEW.timeTo,'~') into newValueList;
   END IF;
   
   IF ((OLD.endMiles <> NEW.endMiles) or (OLD.endMiles is null and NEW.endMiles is not null) 
	  or (OLD.endMiles is not null and NEW.endMiles is null)) THEN
		select CONCAT(fieldNameList,'workticket.endMiles~') into fieldNameList;
		IF(OLD.endMiles is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.endMiles is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.endMiles is not null) then
			select CONCAT(oldValueList,OLD.endMiles,'~') into oldValueList;
		END IF;
		IF(NEW.endMiles is not null) then
			select CONCAT(newValueList,NEW.endMiles,'~') into newValueList;
		END IF; 
	END IF;
   
   IF ((OLD.beginningMiles <> NEW.beginningMiles) or (OLD.beginningMiles is null and NEW.beginningMiles is not null) 
	  or (OLD.beginningMiles is not null and NEW.beginningMiles is null)) THEN
		select CONCAT(fieldNameList,'workticket.beginningMiles~') into fieldNameList;
		IF(OLD.beginningMiles is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.beginningMiles is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.beginningMiles is not null) then
			select CONCAT(oldValueList,OLD.beginningMiles,'~') into oldValueList;
		END IF;
		IF(NEW.beginningMiles is not null) then
			select CONCAT(newValueList,NEW.beginningMiles,'~') into newValueList;
		END IF; 
	END IF;

   IF (OLD.contract <> NEW.contract ) THEN
       select CONCAT(fieldNameList,'workticket.contract~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
       select CONCAT(newValueList,NEW.contract,'~') into newValueList;
   END IF;
   
   IF (OLD.qc <> NEW.qc ) THEN
       select CONCAT(fieldNameList,'workticket.qc~') into fieldNameList;
       IF(OLD.qc = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.qc = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.qc = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.qc = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.noCharge <> NEW.noCharge ) THEN
       select CONCAT(fieldNameList,'workticket.noCharge~') into fieldNameList;
       IF(OLD.noCharge = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.noCharge = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.noCharge = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.noCharge = true) THEN
	    select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   

  IF ((date_format(OLD.approvedDate,'%Y-%m-%d') <> date_format(NEW.approvedDate,'%Y-%m-%d')) or (date_format(OLD.approvedDate,'%Y-%m-%d') is null and date_format(NEW.approvedDate,'%Y-%m-%d') is not null)or (date_format(OLD.approvedDate,'%Y-%m-%d') is not null and date_format(NEW.approvedDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'workticket.approvedDate~') into fieldNameList;
       IF(OLD.approvedDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.approvedDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.approvedDate is not null) THEN
        	select CONCAT(oldValueList,OLD.approvedDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.approvedDate is not null) THEN
        	select CONCAT(newValueList,NEW.approvedDate,'~') into newValueList;
      	END IF;
   END IF;
   
   
   IF (OLD.approvedBy <> NEW.approvedBy ) THEN
       select CONCAT(fieldNameList,'workticket.approvedBy~') into fieldNameList;
       select CONCAT(oldValueList,OLD.approvedBy,'~') into oldValueList;
       select CONCAT(newValueList,NEW.approvedBy,'~') into newValueList;
   END IF;
   
   IF (OLD.qcId <> NEW.qcId ) THEN
       select CONCAT(fieldNameList,'workticket.qcId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.qcId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.qcId,'~') into newValueList;
   END IF;
      
  IF ((date_format(OLD.date1,'%Y-%m-%d') <> date_format(NEW.date1,'%Y-%m-%d')) or (date_format(OLD.date1,'%Y-%m-%d') is null and date_format(NEW.date1,'%Y-%m-%d') is not null)or (date_format(OLD.date1,'%Y-%m-%d') is not null and date_format(NEW.date1,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'workticket.date1~') into fieldNameList;
       IF(OLD.date1 is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.date1 is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.date1 is not null) THEN
        	select CONCAT(oldValueList,OLD.date1,'~') into oldValueList;
     	END IF;
      	IF(NEW.date1 is not null) THEN
        	select CONCAT(newValueList,NEW.date1,'~') into newValueList;
      	END IF;
   END IF;
   
   IF ((date_format(OLD.date2,'%Y-%m-%d') <> date_format(NEW.date2,'%Y-%m-%d')) or (date_format(OLD.date2,'%Y-%m-%d') is null and date_format(NEW.date2,'%Y-%m-%d') is not null)or (date_format(OLD.date2,'%Y-%m-%d') is not null and date_format(NEW.date2,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'workticket.date2~') into fieldNameList;
       IF(OLD.date2 is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.date2 is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.date2 is not null) THEN
        	select CONCAT(oldValueList,OLD.date2,'~') into oldValueList;
     	END IF;
      	IF(NEW.date2 is not null) THEN
        	select CONCAT(newValueList,NEW.date2,'~') into newValueList;
      	END IF;
   END IF;
   
  IF ((date_format(OLD.scheduled1,'%Y-%m-%d') <> date_format(NEW.scheduled1,'%Y-%m-%d')) or (date_format(OLD.scheduled1,'%Y-%m-%d') is null and date_format(NEW.scheduled1,'%Y-%m-%d') is not null)or (date_format(OLD.scheduled1,'%Y-%m-%d') is not null and date_format(NEW.scheduled1,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'workticket.scheduled1~') into fieldNameList;
       IF(OLD.scheduled1 is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.scheduled1 is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.scheduled1 is not null) THEN
        	select CONCAT(oldValueList,OLD.scheduled1,'~') into oldValueList;
     	END IF;
      	IF(NEW.scheduled1 is not null) THEN
        	select CONCAT(newValueList,NEW.scheduled1,'~') into newValueList;
      	END IF;
   END IF;
   
   
  IF ((date_format(OLD.scheduled2,'%Y-%m-%d') <> date_format(NEW.scheduled2,'%Y-%m-%d')) or (date_format(OLD.scheduled2,'%Y-%m-%d') is null and date_format(NEW.scheduled2,'%Y-%m-%d') is not null)or (date_format(OLD.scheduled2,'%Y-%m-%d') is not null and date_format(NEW.scheduled2,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'workticket.scheduled2~') into fieldNameList;
       IF(OLD.scheduled2 is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.scheduled2 is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.scheduled2 is not null) THEN
        	select CONCAT(oldValueList,OLD.scheduled2,'~') into oldValueList;
     	END IF;
      	IF(NEW.scheduled2 is not null) THEN
        	select CONCAT(newValueList,NEW.scheduled2,'~') into newValueList;
      	END IF;
   END IF;
   
   
   
   IF ((date_format(OLD.scanned,'%Y-%m-%d') <> date_format(NEW.scanned,'%Y-%m-%d')) or (date_format(OLD.scanned,'%Y-%m-%d') is null and date_format(NEW.scanned,'%Y-%m-%d') is not null)or (date_format(OLD.scanned,'%Y-%m-%d') is not null and date_format(NEW.scanned,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'workticket.scanned~') into fieldNameList;
       IF(OLD.scanned is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.scanned is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.scanned is not null) THEN
        	select CONCAT(oldValueList,OLD.scanned,'~') into oldValueList;
     	END IF;
      	IF(NEW.scanned is not null) THEN
        	select CONCAT(newValueList,NEW.scanned,'~') into newValueList;
      	END IF;
   END IF;
   
   
   IF (OLD.originCompany <> NEW.originCompany ) THEN
       select CONCAT(fieldNameList,'workticket.originCompany~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originCompany,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originCompany,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationCompany <> NEW.destinationCompany ) THEN
       select CONCAT(fieldNameList,'workticket.destinationCompany~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationCompany,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationCompany,'~') into newValueList;
   END IF;
   
   IF (OLD.originFax <> NEW.originFax ) THEN
       select CONCAT(fieldNameList,'workticket.originFax~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originFax,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originFax,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationFax <> NEW.destinationFax ) THEN
       select CONCAT(fieldNameList,'workticket.destinationFax~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationFax,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationFax,'~') into newValueList;
   END IF;
   
   IF (OLD.salesMan <> NEW.salesMan ) THEN
       select CONCAT(fieldNameList,'workticket.salesMan~') into fieldNameList;
       select CONCAT(oldValueList,OLD.salesMan,'~') into oldValueList;
       select CONCAT(newValueList,NEW.salesMan,'~') into newValueList;
   END IF;
   
   IF (OLD.estimator <> NEW.estimator ) THEN
       select CONCAT(fieldNameList,'workticket.estimator~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimator,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimator,'~') into newValueList;
   END IF;
   
   IF (OLD.coordinator <> NEW.coordinator ) THEN
       select CONCAT(fieldNameList,'workticket.coordinator~') into fieldNameList;
       select CONCAT(oldValueList,OLD.coordinator,'~') into oldValueList;
       select CONCAT(newValueList,NEW.coordinator,'~') into newValueList;
   END IF;
   
   IF (OLD.originCountryCode <> NEW.originCountryCode ) THEN
       select CONCAT(fieldNameList,'workticket.originCountryCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originCountryCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originCountryCode,'~') into newValueList;
   END IF;
   
   IF (OLD.originCountry <> NEW.originCountry ) THEN
       select CONCAT(fieldNameList,'workticket.originCountry~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originCountry,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originCountry,'~') into newValueList;
   END IF;
   
   IF (OLD.carrier <> NEW.carrier ) THEN
       select CONCAT(fieldNameList,'workticket.carrier~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrier,'~') into oldValueList;
       select CONCAT(newValueList,NEW.carrier,'~') into newValueList;
   END IF;
   
     
      IF (OLD.phoneExt <> NEW.phoneExt ) THEN
       select CONCAT(fieldNameList,'workticket.phoneExt~') into fieldNameList;
       select CONCAT(oldValueList,OLD.phoneExt,'~') into oldValueList;
       select CONCAT(newValueList,NEW.phoneExt,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationPhoneExt <> NEW.destinationPhoneExt ) THEN
       select CONCAT(fieldNameList,'workticket.destinationPhoneExt~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationPhoneExt,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationPhoneExt,'~') into newValueList;
   END IF;
   
   IF (OLD.containerNumber <> NEW.containerNumber ) THEN
       select CONCAT(fieldNameList,'workticket.containerNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.containerNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.containerNumber,'~') into newValueList;
   END IF;

   IF (OLD.unit1 <> NEW.unit1 ) THEN
       select CONCAT(fieldNameList,'workticket.unit1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit1,'~') into oldValueList;
       select CONCAT(newValueList,NEW.unit1,'~') into newValueList;
   END IF;
   
   IF (OLD.unit2 <> NEW.unit2 ) THEN
       select CONCAT(fieldNameList,'workticket.unit2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit2,'~') into oldValueList;
       select CONCAT(newValueList,NEW.unit2,'~') into newValueList;
   END IF;
   
   IF (OLD.reviewStatus <> NEW.reviewStatus ) THEN
       select CONCAT(fieldNameList,'workticket.reviewStatus~') into fieldNameList;
       select CONCAT(oldValueList,OLD.reviewStatus,'~') into oldValueList;
       select CONCAT(newValueList,NEW.reviewStatus,'~') into newValueList;
   END IF;

   IF ((date_format(OLD.reviewStatusDate,'%Y-%m-%d') <> date_format(NEW.reviewStatusDate,'%Y-%m-%d')) or (date_format(OLD.reviewStatusDate,'%Y-%m-%d') is null and date_format(NEW.reviewStatusDate,'%Y-%m-%d') is not null)or (date_format(OLD.reviewStatusDate,'%Y-%m-%d') is not null and date_format(NEW.reviewStatusDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'workticket.reviewStatusDate~') into fieldNameList;
       IF(OLD.reviewStatusDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.reviewStatusDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.reviewStatusDate is not null) THEN
        	select CONCAT(oldValueList,OLD.reviewStatusDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.reviewStatusDate is not null) THEN
        	select CONCAT(newValueList,NEW.reviewStatusDate,'~') into newValueList;
      	END IF;
   END IF;

    IF ((date_format(OLD.statusDate,'%Y-%m-%d') <> date_format(NEW.statusDate,'%Y-%m-%d')) or (date_format(OLD.statusDate,'%Y-%m-%d') is null and date_format(NEW.statusDate,'%Y-%m-%d') is not null)or (date_format(OLD.statusDate,'%Y-%m-%d') is not null and date_format(NEW.statusDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'workticket.statusDate~') into fieldNameList;
       IF(OLD.statusDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.statusDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.statusDate is not null) THEN
        	select CONCAT(oldValueList,OLD.statusDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.statusDate is not null) THEN
        	select CONCAT(newValueList,NEW.statusDate,'~') into newValueList;
      	END IF;
   END IF;
   
   IF (OLD.storageOut <> NEW.storageOut) THEN
       select CONCAT(fieldNameList,'workticket.storageOut~') into fieldNameList;
       select CONCAT(oldValueList,OLD.storageOut,'~') into oldValueList;
       select CONCAT(newValueList,NEW.storageOut,'~') into newValueList;
   END IF;
    IF (OLD.vendorName <> NEW.vendorName) THEN
       select CONCAT(fieldNameList,'workticket.vendorName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.vendorName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.vendorName,'~') into newValueList;
   END IF;
   
   IF (OLD.vendorCode <> NEW.vendorCode) THEN
       select CONCAT(fieldNameList,'workticket.vendorCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.vendorCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.vendorCode,'~') into newValueList;
   END IF;
   
   IF (OLD.vendorCode <> NEW.companyDivision) THEN
       select CONCAT(fieldNameList,'workticket.companyDivision~') into fieldNameList;
       select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
       select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
   END IF;
   
    IF (OLD.vendorCode <> NEW.payMethod) THEN
       select CONCAT(fieldNameList,'workticket.payMethod~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payMethod,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payMethod,'~') into newValueList;
   END IF;
   
    IF (OLD.vendorCode <> NEW.accountName) THEN
       select CONCAT(fieldNameList,'workticket.accountName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.accountName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.accountName,'~') into newValueList;
   END IF;
   IF ((OLD.stgAmount <> NEW.extraPayroll) or (OLD.extraPayroll is null and NEW.extraPayroll is not null) 
	  or (OLD.extraPayroll is not null and NEW.extraPayroll is null)) THEN
		select CONCAT(fieldNameList,'workticket.extraPayroll~') into fieldNameList;
		IF(OLD.extraPayroll is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.extraPayroll is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.extraPayroll is not null) then
			select CONCAT(oldValueList,OLD.extraPayroll,'~') into oldValueList;
		END IF;
		IF(NEW.extraPayroll is not null) then
			select CONCAT(newValueList,NEW.extraPayroll,'~') into newValueList;
		END IF; 
	END IF;
	IF (OLD.collectCOD <> NEW.collectCOD ) THEN
       select CONCAT(fieldNameList,'workticket.collectCOD~') into fieldNameList;
       IF(OLD.collectCOD = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.collectCOD = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.collectCOD = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.collectCOD = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   IF (OLD.serviceOrderId <> NEW.serviceOrderId ) THEN
       select CONCAT(fieldNameList,'workticket.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.serviceOrderId,'~') into newValueList;
   END IF;
 IF (OLD.bondedGoods <> NEW.bondedGoods ) THEN
       select CONCAT(fieldNameList,'workticket.bondedGoods~') into fieldNameList;
       select CONCAT(oldValueList,OLD.bondedGoods,'~') into oldValueList;
       select CONCAT(newValueList,NEW.bondedGoods,'~') into newValueList;
   END IF;
   
   
   
   
   
   
   
   
     IF (OLD. distanceInKmMiles <> NEW.distanceInKmMiles ) THEN
       select CONCAT(fieldNameList,'workticket.distanceInKmMiles~') into fieldNameList;
       select CONCAT(oldValueList,OLD.distanceInKmMiles,'~') into oldValueList;
       select CONCAT(newValueList,NEW.distanceInKmMiles,'~') into newValueList;
   END IF;
   
   
   IF ((OLD.distance <> NEW.distance) or (OLD.distance is null and NEW.distance is not null)
      or (OLD.distance is not null and NEW.distance is null)) THEN
        select CONCAT(fieldNameList,'workticket.distance~') into fieldNameList;
        IF(OLD.distance is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.distance is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.distance is not null) then
            select CONCAT(oldValueList,OLD.distance,'~') into oldValueList;
        END IF;
        IF(NEW.distance is not null) then
            select CONCAT(newValueList,NEW.distance,'~') into newValueList;
        END IF;
    END IF;
   
     IF (OLD.originShuttle <> NEW.originShuttle ) THEN
       select CONCAT(fieldNameList,'workticket.originShuttle~') into fieldNameList;
       IF(OLD.originShuttle = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.originShuttle = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.originShuttle = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.originShuttle = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
     IF (OLD.destinationShuttle <> NEW.destinationShuttle ) THEN
       select CONCAT(fieldNameList,'workticket.destinationShuttle~') into fieldNameList;
       IF(OLD.destinationShuttle = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.destinationShuttle = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.destinationShuttle = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.destinationShuttle = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
     IF (OLD.originParkingPermit <> NEW.originParkingPermit ) THEN
       select CONCAT(fieldNameList,'workticket.originParkingPermit~') into fieldNameList;
       IF(OLD.originParkingPermit = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.originParkingPermit = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.originParkingPermit = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.originParkingPermit = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   
     IF (OLD.destinationParkingPermit <> NEW.destinationParkingPermit ) THEN
       select CONCAT(fieldNameList,'workticket.destinationParkingPermit~') into fieldNameList;
       IF(OLD.destinationParkingPermit = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.destinationParkingPermit = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.destinationParkingPermit = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.destinationParkingPermit = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
 IF (OLD.whichFloor <> NEW.whichFloor ) THEN
       select CONCAT(fieldNameList,'workticket.whichFloor~') into fieldNameList;
       select CONCAT(oldValueList,OLD.whichFloor,'~') into oldValueList;
       select CONCAT(newValueList,NEW.whichFloor,'~') into newValueList;
   END IF;
   
   IF (OLD.farInMeters <> NEW.farInMeters ) THEN
       select CONCAT(fieldNameList,'workticket.farInMeters~') into fieldNameList;
       select CONCAT(oldValueList,OLD.farInMeters,'~') into oldValueList;
       select CONCAT(newValueList,NEW.farInMeters,'~') into newValueList;
   END IF;
   
   IF (OLD.parking <> NEW.parking ) THEN
       select CONCAT(fieldNameList,'workticket.parking~') into fieldNameList;
       select CONCAT(oldValueList,OLD.parking,'~') into oldValueList;
       select CONCAT(newValueList,NEW.parking,'~') into newValueList;
   END IF;
   
   IF (OLD.dest_whichFloor <> NEW.dest_whichFloor ) THEN
       select CONCAT(fieldNameList,'workticket.dest_whichFloor~') into fieldNameList;
       select CONCAT(oldValueList,OLD.dest_whichFloor,'~') into oldValueList;
       select CONCAT(newValueList,NEW.dest_whichFloor,'~') into newValueList;
   END IF;
   
   IF (OLD.dest_farInMeters <> NEW.dest_farInMeters ) THEN
       select CONCAT(fieldNameList,'workticket.dest_farInMeters~') into fieldNameList;
       select CONCAT(oldValueList,OLD.dest_farInMeters,'~') into oldValueList;
       select CONCAT(newValueList,NEW.dest_farInMeters,'~') into newValueList;
   END IF;
   
   IF (OLD.dest_park <> NEW.dest_park ) THEN
       select CONCAT(fieldNameList,'workticket.dest_park~') into fieldNameList;
       select CONCAT(oldValueList,OLD.dest_park,'~') into oldValueList;
       select CONCAT(newValueList,NEW.dest_park,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationElevator <> NEW.destinationElevator ) THEN
       select CONCAT(fieldNameList,'workticket.destinationElevator~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationElevator,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationElevator,'~') into newValueList;
   END IF;
   
   IF (OLD.originElevator <> NEW.originElevator ) THEN
       select CONCAT(fieldNameList,'workticket.originElevator~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originElevator,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originElevator,'~') into newValueList;
   END IF;
   
   IF (OLD.parkingD <> NEW.parkingD ) THEN
       select CONCAT(fieldNameList,'workticket.parkingD~') into fieldNameList;
       select CONCAT(oldValueList,OLD.parkingD,'~') into oldValueList;
       select CONCAT(newValueList,NEW.parkingD,'~') into newValueList;
   END IF;
   
   IF (OLD.parkingS <> NEW.parkingS ) THEN
       select CONCAT(fieldNameList,'workticket.parkingS~') into fieldNameList;
       select CONCAT(oldValueList,OLD.parkingS,'~') into oldValueList;
       select CONCAT(newValueList,NEW.parkingS,'~') into newValueList;
   END IF;
   
   IF (OLD.siteD <> NEW.siteD ) THEN
       select CONCAT(fieldNameList,'workticket.siteD~') into fieldNameList;
       select CONCAT(oldValueList,OLD.siteD,'~') into oldValueList;
       select CONCAT(newValueList,NEW.siteD,'~') into newValueList;
   END IF;
   
   IF (OLD.siteS <> NEW.siteS ) THEN
       select CONCAT(fieldNameList,'workticket.siteS~') into fieldNameList;
       select CONCAT(oldValueList,OLD.siteS,'~') into oldValueList;
       select CONCAT(newValueList,NEW.siteS,'~') into newValueList;
   END IF;
   
   IF (OLD.destMeters <> NEW.destMeters ) THEN
       select CONCAT(fieldNameList,'workticket.destMeters~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destMeters,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destMeters,'~') into newValueList;
   END IF;
   
    IF (OLD.origMeters <> NEW.origMeters ) THEN
       select CONCAT(fieldNameList,'workticket.origMeters~') into fieldNameList;
       select CONCAT(oldValueList,OLD.origMeters,'~') into oldValueList;
       select CONCAT(newValueList,NEW.origMeters,'~') into newValueList;
   END IF;
   
   
   IF (OLD.weekendFlag <> NEW.weekendFlag ) THEN
       select CONCAT(fieldNameList,'workticket.weekendFlag~') into fieldNameList;
       IF(OLD.weekendFlag = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.weekendFlag = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.weekendFlag = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.weekendFlag = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   
   IF (OLD.resourceReqdAm <> NEW.resourceReqdAm ) THEN
       select CONCAT(fieldNameList,'workticket.resourceReqdAm~') into fieldNameList;
       IF(OLD.resourceReqdAm = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.resourceReqdAm = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.resourceReqdAm = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.resourceReqdAm = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   
   IF (OLD.estimatedPieces <> NEW.estimatedPieces ) THEN
       select CONCAT(fieldNameList,'workticket.estimatedPieces~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimatedPieces,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimatedPieces,'~') into newValueList;
   END IF;
   
   
   IF (OLD.crates <> NEW.crates ) THEN
       select CONCAT(fieldNameList,'workticket.crates~') into fieldNameList;
       select CONCAT(oldValueList,OLD.crates,'~') into oldValueList;
       select CONCAT(newValueList,NEW.crates,'~') into newValueList;
   END IF;
   
    IF ((OLD.actualVolume <> NEW.actualVolume) or (OLD.actualVolume is null and NEW.actualVolume is not null) 
	  or (OLD.actualVolume is not null and NEW.actualVolume is null)) THEN
		select CONCAT(fieldNameList,'workticket.actualVolume~') into fieldNameList;
		IF(OLD.actualVolume is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.actualVolume is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.actualVolume is not null) then
			select CONCAT(oldValueList,OLD.actualVolume,'~') into oldValueList;
		END IF;
		IF(NEW.actualVolume is not null) then
			select CONCAT(newValueList,NEW.actualVolume,'~') into newValueList;
		END IF; 
	END IF;
   
   IF (OLD.changes <> NEW.changes ) THEN
       select CONCAT(fieldNameList,'workticket.changes~') into fieldNameList;
       select CONCAT(oldValueList,OLD.changes,'~') into oldValueList;
       select CONCAT(newValueList,NEW.changes,'~') into newValueList;
   END IF;
   
   IF (OLD.resourceReqdPm <> NEW.resourceReqdPm ) THEN
       select CONCAT(fieldNameList,'workticket.resourceReqdPm~') into fieldNameList;
       IF(OLD.resourceReqdPm = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.resourceReqdPm = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.resourceReqdPm = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.resourceReqdPm = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
     IF (OLD.originPreferredContactTime <> NEW.originPreferredContactTime ) THEN
       select CONCAT(fieldNameList,'workticket.originPreferredContactTime~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originPreferredContactTime,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originPreferredContactTime,'~') into newValueList;
   END IF;
   
     IF (OLD.destPreferredContactTime <> NEW.destPreferredContactTime ) THEN
       select CONCAT(fieldNameList,'workticket.destPreferredContactTime~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destPreferredContactTime,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destPreferredContactTime,'~') into newValueList;
   END IF;
   
     IF (OLD.description <> NEW.description ) THEN
       select CONCAT(fieldNameList,'workticket.description~') into fieldNameList;
       select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
       select CONCAT(newValueList,NEW.description,'~') into newValueList;
   END IF;
   
     IF (OLD.originLat <> NEW.originLat ) THEN
       select CONCAT(fieldNameList,'workticket.originLat~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originLat,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originLat,'~') into newValueList;
   END IF;
   
     IF (OLD.originLon <> NEW.originLon ) THEN
       select CONCAT(fieldNameList,'workticket.originLon~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originLon,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originLon,'~') into newValueList;
   END IF;
   
     IF (OLD.destLat <> NEW.destLat ) THEN
       select CONCAT(fieldNameList,'workticket.destLat~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destLat,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destLat,'~') into newValueList;
   END IF;
   
     IF (OLD.destLon <> NEW.destLon ) THEN
       select CONCAT(fieldNameList,'workticket.destLon~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destLon,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destLon,'~') into newValueList;
   END IF;
   
     IF (OLD.tag <> NEW.tag ) THEN
       select CONCAT(fieldNameList,'workticket.tag~') into fieldNameList;
       select CONCAT(oldValueList,OLD.tag,'~') into oldValueList;
       select CONCAT(newValueList,NEW.tag,'~') into newValueList;
   END IF;
   
     IF (OLD.orderId <> NEW.orderId ) THEN
       select CONCAT(fieldNameList,'workticket.orderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.orderId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.orderId,'~') into newValueList;
   END IF;
   
     IF (OLD.invoiceNumber <> NEW.invoiceNumber ) THEN
       select CONCAT(fieldNameList,'workticket.invoiceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.invoiceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.invoiceNumber,'~') into newValueList;
   END IF;
   
   
    IF ((OLD.priority <> NEW.priority) or (OLD.priority is null and NEW.priority is not null) 
	  or (OLD.priority is not null and NEW.priority is null)) THEN
		select CONCAT(fieldNameList,'workticket.priority~') into fieldNameList;
		IF(OLD.priority is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.priority is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.priority is not null) then
			select CONCAT(oldValueList,OLD.priority,'~') into oldValueList;
		END IF;
		IF(NEW.priority is not null) then
			select CONCAT(newValueList,NEW.priority,'~') into newValueList;
		END IF; 
	END IF;
	
   IF (OLD.originLongCarry <> NEW.originLongCarry ) THEN
       select CONCAT(fieldNameList,'workticket.originLongCarry~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originLongCarry,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originLongCarry,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationLongCarry <> NEW.destinationLongCarry ) THEN
       select CONCAT(fieldNameList,'workticket.destinationLongCarry~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationLongCarry,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationLongCarry,'~') into newValueList;
   END IF;
   
   IF (OLD.attachedFile <> NEW.attachedFile ) THEN
       select CONCAT(fieldNameList,'workticket.attachedFile~') into fieldNameList;
       select CONCAT(oldValueList,OLD.attachedFile,'~') into oldValueList;
       select CONCAT(newValueList,NEW.attachedFile,'~') into newValueList;
   END IF;
   
   IF (OLD.hse <> NEW.hse ) THEN
       select CONCAT(fieldNameList,'workticket.hse~') into fieldNameList;
       IF(OLD.hse = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.hse = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.hse = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.hse = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF ((OLD.requiredCrew <> NEW.requiredCrew) or (OLD.requiredCrew is null and NEW.requiredCrew is not null) 
	  or (OLD.requiredCrew is not null and NEW.requiredCrew is null)) THEN
		select CONCAT(fieldNameList,'workticket.requiredCrew~') into fieldNameList;
		IF(OLD.requiredCrew is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.requiredCrew is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.requiredCrew is not null) then
			select CONCAT(oldValueList,OLD.requiredCrew,'~') into oldValueList;
		END IF;
		IF(NEW.requiredCrew is not null) then
			select CONCAT(newValueList,NEW.requiredCrew,'~') into newValueList;
		END IF; 
	END IF;
	
   IF (OLD.ticketAssignedStatus <> NEW.ticketAssignedStatus ) THEN
       select CONCAT(fieldNameList,'workticket.ticketAssignedStatus~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ticketAssignedStatus,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ticketAssignedStatus,'~') into newValueList;
   END IF;
   
   IF (OLD.originAddressBook <> NEW.originAddressBook ) THEN
       select CONCAT(fieldNameList,'workticket.originAddressBook~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originAddressBook,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originAddressBook,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationAddressBook <> NEW.destinationAddressBook ) THEN
       select CONCAT(fieldNameList,'workticket.destinationAddressBook~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationAddressBook,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationAddressBook,'~') into newValueList;
   END IF;
   
    
   
if(date_format(OLD.updatedOn,'%Y-%m-%d %H:%i:%s') <> date_format(NEW.updatedOn,'%Y-%m-%d %H:%i:%s'))then
		CALL add_tblHistory (OLD.id,"workticket", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
	end if;
	if(date_format(OLD.updatedOn,'%Y-%m-%d %H:%i:%s') = date_format(NEW.updatedOn,'%Y-%m-%d %H:%i:%s'))then
		CALL add_tblHistory (OLD.id,"workticket", fieldNameList, oldValueList, newValueList, 'System', OLD.corpID, now());
	end if;

END
$$
