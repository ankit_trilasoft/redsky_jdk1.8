update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`accessinfo` MODIFY COLUMN `originComment` VARCHAR(2000) DEFAULT NULL,MODIFY COLUMN `destinationComment` VARCHAR(2000) DEFAULT NULL;

ALTER TABLE `redsky`.`drivercommissionplan` MODIFY COLUMN `contract` VARCHAR(250) DEFAULT NULL;

ALTER TABLE partnerpublic DROP storageemail;

ALTER TABLE `redsky`.`printdailypackage` ADD COLUMN `status`  bit(1) DEFAULT b'1';

ALTER TABLE `redsky`.`cportalresourcemgmt` ADD COLUMN `jobType` VARCHAR(5) DEFAULT NULL AFTER `fileSize`;

ALTER TABLE `redsky`.`app_user` MODIFY COLUMN `contract` VARCHAR(100) DEFAULT NULL;

ALTER TABLE `redsky`.`billing` MODIFY COLUMN `contract` VARCHAR(100) DEFAULT NULL;

ALTER TABLE `redsky`.`contract` MODIFY COLUMN `contract` VARCHAR(100) DEFAULT NULL;

ALTER TABLE `redsky`.`costing` MODIFY COLUMN `contract` VARCHAR(100) DEFAULT NULL;

ALTER TABLE `redsky`.`customerfile` MODIFY COLUMN `contract` VARCHAR(100) DEFAULT NULL;

ALTER TABLE `redsky`.`defaultaccount` MODIFY COLUMN `contract` VARCHAR(100) DEFAULT NULL;

ALTER TABLE `redsky`.`defaultaccountline` MODIFY COLUMN `contract` VARCHAR(100) DEFAULT NULL;

ALTER TABLE `redsky`.`resourcecontractcharges` MODIFY COLUMN `contract` VARCHAR(100) DEFAULT NULL;

ALTER TABLE `redsky`.`serviceorder` MODIFY COLUMN `contract` VARCHAR(100) DEFAULT NULL;

ALTER TABLE `redsky`.`timesheet` MODIFY COLUMN `contract` VARCHAR(100) DEFAULT NULL;

ALTER TABLE `redsky`.`track` MODIFY COLUMN `contract` VARCHAR(100) DEFAULT NULL;

ALTER TABLE `redsky`.`workticket` MODIFY COLUMN `contract` VARCHAR(100) DEFAULT NULL;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `driverCompanyDivision` BIT(1) DEFAULT b'0' ;

update accountline set driverCompanyDivision=false;

update partnerpublic set lastname=replace(lastname,'#','') where lastname like '%#%';

ALTER TABLE `redsky`.`emailsetup` MODIFY COLUMN `body` VARCHAR(5000) DEFAULT NULL;

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `mmCounselor` VARCHAR(82) NULL  AFTER `contactLastName` ;

ALTER TABLE `redsky`.`claim` ADD COLUMN `claimBy` VARCHAR(25) NULL  AFTER `idNumber` , ADD COLUMN `noOfDaysForSettelment` INT(11) NULL  AFTER `claimBy` ;

ALTER TABLE `redsky`.`claim` CHANGE COLUMN `claimBy` `claimBy` INT(10) NULL DEFAULT NULL  ;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `claimBy` INT(10) NULL  AFTER `storageEmail` ;

ALTER TABLE `redsky`.`customerfile` MODIFY COLUMN `billToAuthorization` VARCHAR(50) ;

ALTER TABLE `redsky`.`companydivision` ADD COLUMN `mssCustomerID` VARCHAR(45) DEFAULT NULL AFTER `bankAddress`, ADD COLUMN `mssPassword` VARCHAR(225) DEFAULT NULL AFTER `mssCustomerID`;

CREATE TABLE `redsky`.`lumpsumInsurance` (
   `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
   `corpID` VARCHAR(30) NOT NULL,
   `atricle` VARCHAR(60) DEFAULT NULL,
   `valuation` VARCHAR(45) DEFAULT NULL,
   `serviceOrderID` BIGINT(20) DEFAULT NULL,
   `createdBy` VARCHAR(82) DEFAULT NULL,
   `updatedBy` VARCHAR(82) DEFAULT NULL,
   `updatedOn` DATETIME DEFAULT NULL,
   `createdOn` DATETIME DEFAULT NULL,
   PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `redsky`.`servicepartner` ADD COLUMN `houseBillNumber` VARCHAR(45) NULL  AFTER `ugwIntId` ;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `lumpSum` BIT(1) DEFAULT b'0';

/*Add_tblhistory
 Copy from Dev1 and move to produciton*/


