<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="companyDivisionList.title"/></title>
    <meta name="heading" content="<fmt:message key='companyDivisionList.heading'/>"/>

<style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:1px;
!margin-bottom:2px;
margin-top:-13px;
padding:2px 0px;
text-align:right;
width:100%;
</style>
</head>
  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton1" cssStyle="width:58px;!margin-bottom: 10px;" align="top" method="search" key="button.search" onclick="goToSearch()"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:58px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
</c:set>
<c:set var="buttons">
<input type="button" class="cssbuttonA" style="margin-right:5px; height:27px;"
        onclick="location.href='<c:url value="/editCompanyDivision.html"/>'"
        value="<fmt:message key="button.add"/>"/>
</c:set>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton1" cssStyle="width:58px;!margin-bottom: 10px;" align="top" method="search" key="button.search" onclick="goToSearch()"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:58px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
</c:set>

<s:form cssClass="form_magn" id="companyDivisionListForm" action="searchCompanyDivisions" method="post" > 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%" border="0" cellpadding="0" cellspacing="0">
<thead>
<tr>
<th><fmt:message key="companyDivision.companyCode"/></th>
<th><fmt:message key="companyDivision.description"/></th>
<th><fmt:message key="companyDivision.accountingCode"/></th>
<th><fmt:message key="companyDivision.bookingAgentCode"/></th>
<th><fmt:message key="companyDivision.vanLineCode"/></th>
<th><fmt:message key="companyDivision.vanlinedefaultJobtype"/></th>

</tr></thead>	
		<tbody>
		<tr>			
			<td align="left">
			    <s:textfield name="companyDivision.companyCode" size="16" required="true" cssClass="input-text" cssStyle="width:200px;"/>
			</td>
			<td align="left" >
			    <s:textfield name="companyDivision.description" size="16" required="true" cssClass="input-text" cssStyle="width:200px;" />
			</td>
			<td align="left">
			    <s:textfield name="companyDivision.accountingCode" size="16" required="true" cssClass="input-text" cssStyle="width:200px;" />
			</td>
			<td align="left" >
			    <s:textfield name="companyDivision.bookingAgentCode" size="16" required="true" cssClass="input-text" cssStyle="width:200px;"/>
			</td>
				<td align="left" >
			    <s:textfield name="companyDivision.vanLineCode" size="16" required="true" cssClass="input-text" cssStyle="width:200px;"/>
			</td>
			<td align="left" >
			    <s:select cssClass="list-menu" name="companyDivision.vanlinedefaultJobtype" list="%{job}" cssStyle="width:200px" headerKey="" headerValue="" />
			</td>	
						
		</tr>
		<tr>
		<td colspan="5"></td>		
			
			<td align="center" style="border-left: hidden;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		
		</tbody>
	</table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<c:out value="${searchresults}" escapeXml="false" /> 


<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Company Division List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		</tbody>
</table>

<s:set name="companyDivisionsExt" value="companyDivisions" scope="request"/>
<display:table name="companyDivisionsExt" class="table" requestURI="" id="companyDivisionList" export="true" pagesize="10">
    <display:column property="corpID" sortable="true" paramId="id" paramProperty="id" titleKey="companyDivision.corpID" href="editCompanyDivision.html"/>
    <display:column property="companyCode" sortable="true" titleKey="companyDivision.companyCode"/>
    <display:column property="description" sortable="true" titleKey="companyDivision.description"/>
 	<display:column property="accountingCode" sortable="true" titleKey="companyDivision.accountingCode"/>
 	<display:column property="bookingAgentCode" sortable="true" titleKey="companyDivision.bookingAgentCode"/>
    <display:column property="vanLineCode" sortable="true" titleKey="companyDivision.vanLineCode" />
    <display:column property="vanLineCodeGen" sortable="true" titleKey="companyDivision.vanLineCodeGen" />
    <display:column property="vanlinedefaultJobtype" sortable="true" titleKey="companyDivision.vanlinedefaultJobtype"/>
    <display:column property="vanlineCoordinator" sortable="true" titleKey="companyDivision.vanlineCoordinator"/>
    <display:column property="SCAC" sortable="true" titleKey="companyDivision.SCAC"/>
    
    <display:setProperty name="paging.banner.item_name" value="companyDivision"/>
    <display:setProperty name="export.excel.filename" value="CompanyDivision List.xls"/>
    <display:setProperty name="export.csv.filename" value="CompanyDivision List.csv"/>
    <display:setProperty name="export.pdf.filename" value="CompanyDivision List.pdf"/>
</display:table>

<c:out value="${buttons}" escapeXml="false" />
</s:form>

<script language="javascript" type="text/javascript">

function clear_fields(){
	document.forms['companyDivisionListForm'].elements['companyDivision.companyCode'].value = "";
	document.forms['companyDivisionListForm'].elements['companyDivision.description'].value = "";
	document.forms['companyDivisionListForm'].elements['companyDivision.accountingCode'].value = "";
	document.forms['companyDivisionListForm'].elements['companyDivision.bookingAgentCode'].value = "";
	document.forms['companyDivisionListForm'].elements['companyDivision.vanLineCode'].value = "";
	document.forms['companyDivisionListForm'].elements['companyDivision.vanlinedefaultJobtype'].value = "";
	
}
</script>

<script type="text/javascript">
    highlightTableRows("companyDivisionList");
    Form.focusFirstElement($("companyDivisionListForm")); 
</script>