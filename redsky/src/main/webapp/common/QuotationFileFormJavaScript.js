<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1"%>

<script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/common/formCalender.js"></script> 
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
<script language="javascript" type="text/javascript">
function myDate() {var HH2,HH1,MM2,MM1,tim1=document.forms.customerFileForm.elements["customerFile.surveyTime"].value,tim2=document.forms.customerFileForm.elements["customerFile.surveyTime2"].value;tim1=tim1.replace(":",""),tim2=tim2.replace(":",""),HH1=1==tim1.substring(0,tim1.length-2).length?"0"+tim1.substring(0,tim1.length-2):tim1.substring(0,tim1.length-2),MM1=tim1.substring(tim1.length-2,tim1.length),HH2=1==tim2.substring(0,tim1.length-2).length?"0"+tim2.substring(0,tim2.length-2):tim2.substring(0,tim2.length-2),MM2=tim2.substring(tim2.length-2,tim2.length),document.forms.customerFileForm.elements["customerFile.surveyTime"].value=HH1+":"+MM1,document.forms.customerFileForm.elements["customerFile.surveyTime2"].value=HH2+":"+MM2;var f=document.getElementById("customerFileForm");f.setAttribute("autocomplete","off");}
</script>


<script type="text/javascript">

function autoPopulate_customerFile_approvedDate(targetElement) {var mydate=new Date,daym,year=mydate.getYear(),y=""+year;1e3>year&&(year+=1900);var day=mydate.getDay(),month=mydate.getMonth()+1;1==month&&(month="Jan"),2==month&&(month="Feb"),3==month&&(month="Mar"),4==month&&(month="Apr"),5==month&&(month="May"),6==month&&(month="Jun"),7==month&&(month="Jul"),8==month&&(month="Aug"),9==month&&(month="Sep"),10==month&&(month="Oct"),11==month&&(month="Nov"),12==month&&(month="Dec");var daym=mydate.getDate();10>daym&&(daym="0"+daym);var datam=daym+"-"+month+"-"+y.substring(1,3);targetElement.form.elements["customerFile.billApprovedDate"].value=datam;}	
</script>


<script type="text/javascript">
function validate_email(field)
{
with (field)
{
apos=value.indexOf("@")
dotpos=value.lastIndexOf(".")
if (apos<1||dotpos-apos<2) 
  {alert("Not a valid e-mail address!");
  field.value = "";
  return false}
else {return true}
}
}
</script>


<script language="JavaScript">
function isNumeric(targetElement)
{   var i;
    var s = targetElement.value;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        alert("Enter valid number");
        targetElement.value="";
        
        return false;
        }
    }
    //alert("Enter valid Number");
    // All characters are numbers.
    return true;
}
function onlyTimeFormatAllowed(evt)
{
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
}
function onlyPhoneNumsAllowed(evt)
{
	
  var keyCode = evt.which ? evt.which : evt.keyCode;
  //alert(keyCode);
  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
}
</script>


<script type="text/javascript">

//Declaring required variables
var digits = "0123456789";
//non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
//characters which are allowed in international phone numbers
//(a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
//Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
 for (i = 0; i < s.length; i++)
 {   
     // Check that current character is number.
     var c = s.charAt(i);
     if (((c < "0") || (c > "9"))) return false;
 }
 // All characters are numbers.
 return true;
}

function stripCharsInBag(s, bag)
{   var i;
 var returnString = "";
 // Search through string's characters one by one.
 // If character is not in bag, append to returnString.
 for (i = 0; i < s.length; i++)
 {   
     // Check that current character isn't whitespace.
     var c = s.charAt(i);
     if (bag.indexOf(c) == -1) returnString += c;
 }
 return returnString;
}

function checkInternationalPhone(strPhone){
var s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;
	
	if (checkInternationalPhone(Phone)==false){
		alert("Please enter a valid phone number")
		targetElelemnt.value="";
		return false;
	}
	return true;
}
</script>


<script type="text/javascript">
function dateValidation(targetElement) {
alert(targetElement.value);
	var openDate = document.forms['customerFileForm'].elements['customerFile.createdOn'].value;
	var survey = targetElement.value;
	var openDateArray = openDate.split("/");
	var surveyArray = survey.split("/");
	var openDate_Date = openDateArray[1];
	var openDate_Month = openDateArray[0];
	var openDate_Year = openDateArray[2];
	var survey_Date = surveyArray[1];
	var survey_Month = surveyArray[0];
	var survey_Year = surveyArray[2];
	
	alert(targetElement.value);
} 
</script>
<script type="text/javascript">
function checkStatusReason(){
	var quotationStatus=document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value;
	<configByCorp:fieldVisibility componentId="component.field.qfStatusReason.showVOER">
	var statusReason=document.forms['customerFileForm'].elements['customerFile.qfStatusReason'].value;
	if((quotationStatus=='Rejected' || quotationStatus=='Cancelled') && statusReason==''){
		alert("Please fill QFStatusReason");
 	 	return false;	
	}else{
		return true;	
	}
	</configByCorp:fieldVisibility>
}
</script>
<SCRIPT LANGUAGE="JavaScript">
function IsValidTime(temp) {
//checkStatusReason()
	<c:if test="${checkContractChargesMandatory=='1'}">
	var contractStr = document.forms['customerFileForm'].elements['customerFile.contract'].value;
	if(contractStr==''){
		alert("Contract is a required field");
		return false;
	}
	</c:if>
	<c:if test="${checkContractChargesMandatory=='2'}">
	var contractStr = document.forms['customerFileForm'].elements['customerFile.contract'].value;
	if(contractStr==''){
		var agree= confirm("Contract has not been selected. would you like to select the contract Yes/NO.");
		 if(agree){
			 document.forms['customerFileForm'].elements['customerFile.contract'].focus();
			 return false;
		 }
	}
	</c:if>
enableCheckBox();
var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format. Please use HH:MM format");
//document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}

hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("'Pre Move Survey' time must between 0 to 23:59 (Hrs)");
//document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
//document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
//document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}

// **************Check for Survey Time2*************************

var time2Str = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. please Use HH:MM format");
//document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];

if (hourTime2 < 0  || hourTime2 > 23) {
alert("'Pre Move Survey' time must between 0 to 23:59 (Hrs)");
//document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
//document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
//document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}

//**************Check for Preffered Time1*************************
<configByCorp:fieldVisibility componentId="component.field.prefSurveyDate.showCorpId">
var prefTime1Str = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value;
var prefMatchTime1Array = prefTime1Str.match(timePat);
if (prefMatchTime1Array == null) {
alert("Preffered time is not in a valid format. please Use HH:MM format");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].focus();
return false;
}
hourTime2 = prefMatchTime1Array[1];
minuteTime2 = prefMatchTime1Array[2];
secondTime2 = prefMatchTime1Array[4];
ampmTime2 = prefMatchTime1Array[6];
if (hourTime2 < 0  || hourTime2 > 23) {
alert("'Preffered Survey' time must between 0 to 23:59 (Hrs)");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'Preffered Survey' time must between 0 to 59 (Min)");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'Preffered Survey' time must between 0 to 59 (Sec)");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].focus();
return false;
}

//**************Check for Preffered Time2*************************
var prefTime2Str = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value;
var prefTime3Str = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value;
var prefTime4Str = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value;
var prefMatchTime2Array = prefTime2Str.match(timePat);
var prefMatchTime3Array = prefTime3Str.match(timePat);
var prefMatchTime4Array = prefTime4Str.match(timePat);
if ((prefMatchTime2Array == null) ||(prefMatchTime3Array == null) || (prefMatchTime4Array == null))  {
alert("Preffered time is not in a valid format. please Use HH:MM format");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].focus();
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].focus();
(prefMatchTime2Array == null)
return false;
}
hourTime2 = prefMatchTime2Array[1];
minuteTime2 = prefMatchTime2Array[2];
secondTime2 = prefMatchTime2Array[4];
ampmTime2 = prefMatchTime2Array[6];

if (hourTime2 < 0  || hourTime2 > 23) {
alert("'Preffered Survey' time must between 0 to 23:59 (Hrs)");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'Preffered Survey' time must between 0 to 59 (Min)");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'Preffered Survey' time must between 0 to 59 (Sec)");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].focus();
return false;
}
//**************Check for Preffered Time3*************************
hourTime2 = prefMatchTime3Array[1];
minuteTime2 = prefMatchTime3Array[2];
secondTime2 = prefMatchTime3Array[4];
ampmTime2 = prefMatchTime3Array[6];
if (hourTime2 < 0  || hourTime2 > 23) {
	alert("'Preffered Survey' time must between 0 to 23:59 (Hrs)");
	document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].focus();
	return false;
	}
	if (minuteTime2<0 || minuteTime2 > 59) {
	alert ("'Preffered Survey' time must between 0 to 59 (Min)");
	document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].focus();
	return false;
	}
	if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
	alert ("'Preffered Survey' time must between 0 to 59 (Sec)");
	document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].focus();
	return false;
	}
	//**************Check for Preffered Time4*************************
	hourTime2 = prefMatchTime4Array[1];
	minuteTime2 = prefMatchTime4Array[2];
	secondTime2 = prefMatchTime4Array[4];
	ampmTime2 = prefMatchTime4Array[6];
	if (hourTime2 < 0  || hourTime2 > 23) {
		alert("'Preffered Survey' time must between 0 to 23:59 (Hrs)");
		document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].focus();
		return false;
		}
		if (minuteTime2<0 || minuteTime2 > 59) {
		alert ("'Preffered Survey' time must between 0 to 59 (Min)");
		document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].focus();
		return false;
		}
		if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
		alert ("'Preffered Survey' time must between 0 to 59 (Sec)");
		document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].focus();
		return false;
		}
		
		</configByCorp:fieldVisibility>
		var ocountry = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
		if(ocountry==""){
			alert("Country is a required field in Origin.");
			return false;
		}
		var city = document.forms['customerFileForm'].elements['customerFile.originCity'].value;
		if(city==""){
			alert("City is a required field in Origin.");
			return false;
		}
		var dcountry = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
		if(dcountry==""){
			alert("Country is a required field in Destination.");
			return false;
		}
		var dcountry = document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
		if(dcountry==""){
			alert("City is a required field in Destination.");
			return false;
		}
		
PricingContract();
if(temp=='saveAuto'){

saveAuto('none');
}

}
function checkDate(){
	return lastName();
		var str =  document.forms['customerFileForm'].elements['customerFile.survey'].value;
		var str_array = str.split("/");
		surveydate = new Date(str_array[2],str_array[0]*1-1,str_array[1]);
		var str1 =  document.forms['customerFileForm'].elements['customerFile.moveDate'].value;
		var str1_array = str1.split("/");
		movedate = new Date(str1_array[2],str1_array[0]*1-1,str1_array[1]);
		var str2 =  document.forms['customerFileForm'].elements['customerFile.createdOn'].value;
		var str2_array = str2.split("/");
		var stre = str2_array[2].split(" ");
		createdate = new Date("20"+stre[0],str2_array[0]*1-1,str2_array[1]);
		if(!(str == '')	|| !(str1 == '')){
		if(!(str == '') && surveydate < createdate){
			agree = confirm("Survey date is less than create date, do you want to continue ?");
			return agree;
		}
		if(!(str1 == '') && movedate < createdate){
			agree = confirm("Move date is less than create date, do you want to continue ?");
			return agree;
		}
		}
		return true;
	
}
var httpCheckPriceContract = getHTTPObjectPriceContract();
function getHTTPObjectPriceContract()
{
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function checkPriceContract() {
    var contracts = document.forms['customerFileForm'].elements['customerFile.contract'].value;
   //alert(contracts);
    var url="priceContract.html?ajax=1&decorator=simple&popup=true&contracts=" + encodeURI(contracts);
     httpCheckPriceContract.open("GET", url, true);
     httpCheckPriceContract.onreadystatechange = handleHttpResponse222333;
     httpCheckPriceContract.send(null);
} 
function handleHttpResponse222333() {if(4==httpCheckPriceContract.readyState){var results=httpCheckPriceContract.responseText;results=results.trim();var res=results.split("#");if(res.length>=1){var billPayMethod=document.getElementById("billPayMethod").selectedIndex,partnerCode=document.forms.customerFileForm.elements["customerFile.billToCode"].value;0==billPayMethod?document.forms.customerFileForm.elements["customerFile.billPayMethod"].value=res[5]:0==billPayMethod||null!=partnerCode&&""!=partnerCode||(document.forms.customerFileForm.elements["customerFile.billPayMethod"].value=res[5])}}} 



function checkTime()
{
	var tim1=document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
	var tim2=document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
	time1=tim1.replace(":","");
	time2=tim2.replace(":","");
	if(time1 > time2){
		alert("'Pre Move Survey' To time cannot be less than from time");
		//document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = '00:00';
		document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
		return false;
	}else{
		if(document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked == false){
			/*
			var sendmail = confirm("Do you want to activate the customer ? press OK to activate or CANCEL to proceed without activation");
			if(sendmail){
				//alert("yes");
				document.forms['customerFileForm'].elements['sendEmail'].value="yes";
				generatePortalId();
			}else{
				//alert("No");			
				document.forms['customerFileForm'].elements['sendEmail'].value="no";
			}
			*/
			document.forms['customerFileForm'].elements['submitCnt'].value = 0;
			document.forms['customerFileForm'].elements['submitType'].value = '';
		
			document.forms['customerFileForm'].submit();
		}
	}
	
}
function checkBillToCode(){"500270"==document.forms.customerFileForm.elements["customerFile.billToCode"].value?(document.forms.customerFileForm.elements["customerFile.contactName"].disabled=!1,document.forms.customerFileForm.elements["customerFile.contactPhone"].disabled=!1,document.forms.customerFileForm.elements["customerFile.contactFax"].disabled=!1,document.forms.customerFileForm.elements["customerFile.contactEmail"].disabled=!1,document.forms.customerFileForm.elements["customerFile.organization"].disabled=!1,document.forms.customerFileForm.elements["customerFile.destinationContactName"].disabled=!1,document.forms.customerFileForm.elements["customerFile.destinationContactPhone"].disabled=!1,document.forms.customerFileForm.elements["customerFile.destinationContactFax"].disabled=!1,document.forms.customerFileForm.elements["customerFile.destinationContactEmail"].disabled=!1,document.forms.customerFileForm.elements["customerFile.destinationOrganization"].disabled=!1):(document.forms.customerFileForm.elements["customerFile.contactName"].disabled=!0,document.forms.customerFileForm.elements["customerFile.contactPhone"].disabled=!0,document.forms.customerFileForm.elements["customerFile.contactFax"].disabled=!0,document.forms.customerFileForm.elements["customerFile.contactEmail"].disabled=!0,document.forms.customerFileForm.elements["customerFile.organization"].disabled=!0,document.forms.customerFileForm.elements["customerFile.destinationContactName"].disabled=!0,document.forms.customerFileForm.elements["customerFile.destinationContactPhone"].disabled=!0,document.forms.customerFileForm.elements["customerFile.destinationContactFax"].disabled=!0,document.forms.customerFileForm.elements["customerFile.destinationContactEmail"].disabled=!0,document.forms.customerFileForm.elements["customerFile.destinationOrganization"].disabled=!0);}
function notExists(){
	alert("The customer information is not been saved yet");
}
function copyCompanyToDestination(targetElement){
		document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value = targetElement.value;
	}
function resetAfterSubmit(){
	var goCnt = '-'+document.forms['customerFileForm'].elements['submitCnt'].value * 1;
	history.go(goCnt);
}

function openPopWindow(target){
	var targetName=target.name,first=document.forms.customerFileForm.elements["customerFile.firstName"].value,last=document.forms.customerFileForm.elements["customerFile.lastName"].value,orgAdd1=document.forms.customerFileForm.elements["customerFile.originAddress1"].value,orgAdd2=document.forms.customerFileForm.elements["customerFile.originAddress2"].value,orgAdd3=document.forms.customerFileForm.elements["customerFile.originAddress3"].value,orgCountry=document.forms.customerFileForm.elements["customerFile.originCountry"].value,orgState=document.forms.customerFileForm.elements["customerFile.originState"].value,orgCity=document.forms.customerFileForm.elements["customerFile.originCity"].value,orgZip=document.forms.customerFileForm.elements["customerFile.originZip"].value,phone=document.forms.customerFileForm.elements["customerFile.originHomePhone"].value,email=document.forms.customerFileForm.elements["customerFile.email"].value,orgphone=document.forms.customerFileForm.elements["customerFile.originHomePhone"].value,orgemail=document.forms.customerFileForm.elements["customerFile.email"].value,orgeFax=document.forms.customerFileForm.elements["customerFile.originFax"].value,prefix=document.forms.customerFileForm.elements["customerFile.prefix"].value,companyDiv=document.forms.customerFileForm.elements["customerFile.companyDivision"].value,status=document.forms.customerFileForm.elements["customerFile.status"].value,checkAddress=document.forms.customerFileForm.elements.destinationcopyaddress.checked,orgAddress=orgAdd1+"~"+orgAdd2+"~"+orgAdd3+"~"+orgCountry+"~"+orgState+"~"+orgCity+"~"+orgZip+"~"+orgphone+"~"+orgemail+"~"+orgeFax+"~"+first+"~"+last+"~"+prefix+"~"+companyDiv+"~"+status,destAdd1=document.forms.customerFileForm.elements["customerFile.destinationAddress1"].value,destAdd2=document.forms.customerFileForm.elements["customerFile.destinationAddress2"].value,destAdd3=document.forms.customerFileForm.elements["customerFile.destinationAddress3"].value,destCountry=document.forms.customerFileForm.elements["customerFile.destinationCountry"].value,destState=document.forms.customerFileForm.elements["customerFile.destinationState"].value,destCity=document.forms.customerFileForm.elements["customerFile.destinationCity"].value,destZip=document.forms.customerFileForm.elements["customerFile.destinationZip"].value,destphone=document.forms.customerFileForm.elements["customerFile.destinationHomePhone"].value,destemail=document.forms.customerFileForm.elements["customerFile.destinationEmail"].value,destFax=document.forms.customerFileForm.elements["customerFile.destinationFax"].value,destAddress=destAdd1+"~"+destAdd2+"~"+destAdd3+"~"+destCountry+"~"+destState+"~"+destCity+"~"+destZip+"~"+destphone+"~"+destemail+"~"+destFax+"~"+first+"~"+last+"~"+prefix+"~"+companyDiv+"~"+status;
	var compDiv = '';
<c:if  test="${companies == 'Yes'}">
compDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
</c:if>
if(targetName=='P' && checkAddress==true){
createPrivateParty(destAddress);
}else if(targetName=='P' && checkAddress==false){
createPrivateParty(orgAddress);
}else{
javascript:openWindow('quotationPartnersPopup.html?decorator=popup&popup=true&partnerType=AC&flag=3&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv='+compDiv+'&decorator=popup&popup=true&fld_sixthDescription=customerFile.customerEmployer&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.billToName&fld_code=customerFile.billToCode');
}
}
function createPrivateParty(targetElement) {
	var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	if(last==''){
		alert('Last Name is required field.');
	}else{
	progressBarAutoSave('1');
	var url="privatePartyCode.html?ajax=1&decorator=simple&popup=true&partnerAdderss="+encodeURIComponent(targetElement); 
	http12.open("GET", url, true);
	http12.onreadystatechange = handleHttpResponseP;
	http12.send(null);
	}
	
}	
function handleHttpResponseP(){
	if (http12.readyState == 4) {
	      var results = http12.responseText
          results = results.trim();
          results=results.replace("[","");
          results=results.replace("]","");
          var res = results.split("#"); 
          if(res[0] != 'null'){
          document.forms['customerFileForm'].elements['customerFile.billToCode'].value=res[0];
		  document.forms['customerFileForm'].elements['customerFile.billToName'].value=res[1]+' '+res[2];
		  progressBarAutoSave('0');
		          }
  }
} 
function openAccountPopWindow(){
var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
javascript:openWindow('partnersPopup.html?partnerType=AC&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.accountName&fld_code=customerFile.accountCode');
}

function openBookingAgentPopWindow(){
	var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
	var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	javascript:openWindow('bookingAgentQuotationPopup.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.bookingAgentName&fld_code=customerFile.bookingAgentCode');
}
</script>



<script type="text/javascript">
function openOriginLocation() {
	var city = document.forms['customerFileForm'].elements['customerFile.originCity'].value;
	var country = document.forms['customerFileForm'].elements['oCountry'].value;
	var zip = document.forms['customerFileForm'].elements['customerFile.originZip'].value;
	var address1 = document.forms['customerFileForm'].elements['customerFile.originAddress1'].value;
	var address2 = document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address1+','+address2+ ',' +city+','+zip+','+country);
}
function openDestinationLocation() {
	var city = document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
	var country = document.forms['customerFileForm'].elements['dCountry'].value;
	var zip = document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
	var address1 = document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value;
	var address2 = document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address1+','+address2+ ',' +city+','+zip+','+country);
}
</script>

<SCRIPT LANGUAGE="JavaScript">
function completeTimeString() {stime1=document.forms.customerFileForm.elements["customerFile.surveyTime"].value,stime2=document.forms.customerFileForm.elements["customerFile.surveyTime2"].value,""==stime1.substring(stime1.indexOf(":")+1,stime1.length)||1==stime1.length||2==stime1.length?1==stime1.length||2==stime1.length?(2==stime1.length&&(document.forms.customerFileForm.elements["customerFile.surveyTime"].value=stime1+":00"),1==stime1.length&&(document.forms.customerFileForm.elements["customerFile.surveyTime"].value="0"+stime1+":00")):document.forms.customerFileForm.elements["customerFile.surveyTime"].value=stime1+"00":(-1==stime1.indexOf(":")&&3==stime1.length&&(document.forms.customerFileForm.elements["customerFile.surveyTime"].value="0"+stime1.substring(0,1)+":"+stime1.substring(1,stime1.length)),-1!=stime1.indexOf(":")||4!=stime1.length&&5!=stime1.length||(document.forms.customerFileForm.elements["customerFile.surveyTime"].value=stime1.substring(0,2)+":"+stime1.substring(2,4)),1==stime1.indexOf(":")&&(document.forms.customerFileForm.elements["customerFile.surveyTime"].value="0"+stime1)),""==stime2.substring(stime2.indexOf(":")+1,stime2.length)||1==stime2.length||2==stime2.length?1==stime2.length||2==stime2.length?(2==stime2.length&&(document.forms.customerFileForm.elements["customerFile.surveyTime2"].value=stime2+":00"),1==stime2.length&&(document.forms.customerFileForm.elements["customerFile.surveyTime2"].value="0"+stime2+":00")):document.forms.customerFileForm.elements["customerFile.surveyTime2"].value=stime2+"00":(-1==stime2.indexOf(":")&&3==stime2.length&&(document.forms.customerFileForm.elements["customerFile.surveyTime2"].value="0"+stime2.substring(0,1)+":"+stime2.substring(1,stime2.length)),-1!=stime2.indexOf(":")||4!=stime2.length&&5!=stime2.length||(document.forms.customerFileForm.elements["customerFile.surveyTime2"].value=stime2.substring(0,2)+":"+stime2.substring(2,4)),1==stime2.indexOf(":")&&(document.forms.customerFileForm.elements["customerFile.surveyTime2"].value="0"+stime2)),stime3=document.forms.customerFileForm.elements["customerFile.prefSurveyTime1"].value,stime4=document.forms.customerFileForm.elements["customerFile.prefSurveyTime2"].value,""==stime3.substring(stime3.indexOf(":")+1,stime3.length)||1==stime3.length||2==stime3.length?1==stime3.length||2==stime3.length?(2==stime3.length&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime1"].value=stime3+":00"),1==stime3.length&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime1"].value="0"+stime3+":00")):document.forms.customerFileForm.elements["customerFile.prefSurveyTime1"].value=stime3+"00":(-1==stime3.indexOf(":")&&3==stime3.length&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime1"].value="0"+stime3.substring(0,1)+":"+stime3.substring(1,stime3.length)),-1!=stime3.indexOf(":")||4!=stime3.length&&5!=stime3.length||(document.forms.customerFileForm.elements["customerFile.prefSurveyTime1"].value=stime3.substring(0,2)+":"+stime3.substring(2,4)),1==stime3.indexOf(":")&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime1"].value="0"+stime3)),""==stime4.substring(stime4.indexOf(":")+1,stime4.length)||1==stime4.length||2==stime4.length?1==stime4.length||2==stime4.length?(2==stime4.length&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime2"].value=stime4+":00"),1==stime4.length&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime2"].value="0"+stime4+":00")):document.forms.customerFileForm.elements["customerFile.prefSurveyTime2"].value=stime4+"00":(-1==stime4.indexOf(":")&&3==stime4.length&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime2"].value="0"+stime4.substring(0,1)+":"+stime4.substring(1,stime4.length)),-1!=stime4.indexOf(":")||4!=stime4.length&&5!=stime4.length||(document.forms.customerFileForm.elements["customerFile.prefSurveyTime2"].value=stime4.substring(0,2)+":"+stime4.substring(2,4)),1==stime4.indexOf(":")&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime2"].value="0"+stime4)),stime5=document.forms.customerFileForm.elements["customerFile.prefSurveyTime3"].value,stime6=document.forms.customerFileForm.elements["customerFile.prefSurveyTime4"].value,""==stime5.substring(stime5.indexOf(":")+1,stime5.length)||1==stime5.length||2==stime5.length?1==stime5.length||2==stime5.length?(2==stime5.length&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime3"].value=stime5+":00"),1==stime5.length&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime3"].value="0"+stime5+":00")):document.forms.customerFileForm.elements["customerFile.prefSurveyTime3"].value=stime5+"00":(-1==stime5.indexOf(":")&&3==stime5.length&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime3"].value="0"+stime5.substring(0,1)+":"+stime5.substring(1,stime5.length)),-1!=stime5.indexOf(":")||4!=stime5.length&&5!=stime5.length||(document.forms.customerFileForm.elements["customerFile.prefSurveyTime3"].value=stime5.substring(0,2)+":"+stime5.substring(2,4)),1==stime5.indexOf(":")&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime3"].value="0"+stime5)),""==stime6.substring(stime6.indexOf(":")+1,stime6.length)||1==stime6.length||2==stime6.length?1==stime6.length||2==stime6.length?(2==stime6.length&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime4"].value=stime6+":00"),1==stime6.length&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime4"].value="0"+stime6+":00")):document.forms.customerFileForm.elements["customerFile.prefSurveyTime4"].value=stime6+"00":(-1==stime6.indexOf(":")&&3==stime6.length&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime4"].value="0"+stime6.substring(0,1)+":"+stime6.substring(1,stime6.length)),-1!=stime6.indexOf(":")||4!=stime6.length&&5!=stime6.length||(document.forms.customerFileForm.elements["customerFile.prefSurveyTime4"].value=stime6.substring(0,2)+":"+stime6.substring(2,4)),1==stime6.indexOf(":")&&(document.forms.customerFileForm.elements["customerFile.prefSurveyTime4"].value="0"+stime6));}

</SCRIPT>


<script type="text/javascript">
function autoPopulate_customerFile_originCountry1(targetElement) {
	//var originCountryCode=targetElement.options[targetElement.selectedIndex].value;
	document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value=targetElement.options[targetElement.selectedIndex].value;
	document.forms['customerFileForm'].elements['oCountry'].value=targetElement.options[targetElement.selectedIndex].text;
	if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
		document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
	}else{
		document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
		document.forms['customerFileForm'].elements['customerFile.originState'].value = '';
		autoPopulate_customerFile_originCityCode(targetElement);
	}
	//targetElement.form.elements['customerFile.originCountry'].value=originCountryCode.substring(originCountryCode.indexOf(":")+2,originCountryCode.length);
}
</script>
<script type="text/javascript">
function autoPopulate_customerFile_destinationCountry1(targetElement) {
	//var destinationCountryCode=targetElement.options[targetElement.selectedIndex].value;
	document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value=targetElement.options[targetElement.selectedIndex].value;
	document.forms['customerFileForm'].elements['dCountry'].value=targetElement.options[targetElement.selectedIndex].text;
	if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;
	}else{
		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
		document.forms['customerFileForm'].elements['customerFile.destinationState'].value = '';
	}
	autoPopulate_customerFile_destinationCityCode(targetElement);
	//targetElement.form.elements['customerFile.destinationCountry'].value=destinationCountryCode.substring(destinationCountryCode.indexOf(":")+2,destinationCountryCode.length);
}
</script>
<script type="text/javascript">
function autoPopulate_customerFile_destinationCityCode(targetElement) {
	if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value != ''){
		if(document.forms['customerFileForm'].elements['customerFile.destinationState'].value == ''){
			document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
		}else{
			document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value+', '+document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
		}
	}
	//var sourceCode=targetElement.options[targetElement.selectedIndex].value;
    //document.forms['customerFileForm'].elements['customerFile.sourceCode'].value=sourceCode.substring(0,sourceCode.indexOf(":")-1);
	//targetElement.form.elements['customerFile.source'].value=sourceCode.substring(sourceCode.indexOf(":")+2,sourceCode.length);
}
</script>
<script type="text/javascript">
function autoPopulate_customerFile_originCityCode(targetElement) {
	if(document.forms['customerFileForm'].elements['customerFile.originCity'].value != ''){
		if(document.forms['customerFileForm'].elements['customerFile.originState'].value != ''){
			document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value+', '+document.forms['customerFileForm'].elements['customerFile.originState'].value;
		}else{
			document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
		}
	}
	//var sourceCode=targetElement.options[targetElement.selectedIndex].value;
    //document.forms['customerFileForm'].elements['customerFile.sourceCode'].value=sourceCode.substring(0,sourceCode.indexOf(":")-1);
	//targetElement.form.elements['customerFile.source'].value=sourceCode.substring(sourceCode.indexOf(":")+2,sourceCode.length);
}
</script>


<script type="text/javascript">
function autoPopulate_customerFile_originCountry(targetElement) {var oriCountry=targetElement.value,oriCountryCode="";"United States"==oriCountry&&(oriCountryCode="USA"),"India"==oriCountry&&(oriCountryCode="IND"),"Canada"==oriCountry&&(oriCountryCode="CAN"),"United States"==oriCountry||"Canada"==oriCountry||"India"==oriCountry?(document.forms.customerFileForm.elements["customerFile.originState"].disabled=!1,document.forms.customerFileForm.elements["customerFile.originZip"].focus()):(document.forms.customerFileForm.elements["customerFile.originState"].disabled=!0,document.forms.customerFileForm.elements["customerFile.originState"].value="",("Netherlands"==oriCountry||"Austria"==oriCountry||"Belgium"==oriCountry||"Bulgaria"==oriCountry||"Croatia"==oriCountry||"Cyprus"==oriCountry||"Czech Republic"==oriCountry||"Denmark"==oriCountry||"Estonia"==oriCountry||"Finland"==oriCountry||"France"==oriCountry||"Germany"==oriCountry||"Greece"==oriCountry||"Hungary"==oriCountry||"Iceland"==oriCountry||"Ireland"==oriCountry||"Italy"==oriCountry||"Latvia"==oriCountry||"Lithuania"==oriCountry||"Luxembourg"==oriCountry||"Malta"==oriCountry||"Poland"==oriCountry||"Portugal"==oriCountry||"Romania"==oriCountry||"Slovakia"==oriCountry||"Slovenia"==oriCountry||"Spain"==oriCountry||"Sweden"==oriCountry||"Turkey"==oriCountry||"United Kingdom"==oriCountry)&&document.forms.customerFileForm.elements["customerFile.originZip"].focus(),autoPopulate_customerFile_originCityCode(oriCountryCode,"special"));}
</script>
<script type="text/javascript">
function autoPopulate_customerFile_destinationCountry(targetElement) {var dCountry=targetElement.value,dCountryCode="";"United States"==dCountry&&(dCountryCode="USA"),"India"==dCountry&&(dCountryCode="IND"),"Canada"==dCountry&&(dCountryCode="CAN"),"United States"==dCountry||"Canada"==dCountry||"India"==dCountry?(document.forms.customerFileForm.elements["customerFile.destinationState"].disabled=!1,document.forms.customerFileForm.elements["customerFile.destinationZip"].focus()):(document.forms.customerFileForm.elements["customerFile.destinationState"].disabled=!0,document.forms.customerFileForm.elements["customerFile.destinationState"].value="",("Netherlands"==dCountry||"Austria"==dCountry||"Belgium"==dCountry||"Bulgaria"==dCountry||"Croatia"==dCountry||"Cyprus"==dCountry||"Czech Republic"==dCountry||"Denmark"==dCountry||"Estonia"==dCountry||"Finland"==dCountry||"France"==dCountry||"Germany"==dCountry||"Greece"==dCountry||"Hungary"==dCountry||"Iceland"==dCountry||"Ireland"==dCountry||"Italy"==dCountry||"Latvia"==dCountry||"Lithuania"==dCountry||"Luxembourg"==dCountry||"Malta"==dCountry||"Poland"==dCountry||"Portugal"==dCountry||"Romania"==dCountry||"Slovakia"==dCountry||"Slovenia"==dCountry||"Spain"==dCountry||"Sweden"==dCountry||"Turkey"==dCountry||"United Kingdom"==dCountry)&&document.forms.customerFileForm.elements["customerFile.destinationZip"].focus(),autoPopulate_customerFile_originCityCode(dCountryCode,"special"));}


function getOriginCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
http4.open("GET", url, true);
http4.onreadystatechange = handleHttpResponseCountryName;
http4.send(null);
}
function handleHttpResponseCountryName(){
         if (http4.readyState == 4){
            var results = http4.responseText
            results = results.trim();
            var res=results.split('#');
            if(res.length>=1){
            	document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value = res[0]; 
            	if(res[1]=='Y'){
            	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y';				
			 	}else{
			 	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='N';
			 	}try{
			 	document.forms['customerFileForm'].elements['customerFile.originCountry'].select;
			 	}catch(e){}		     	 
				}else{
                 
             }
         }
}

function getDestinationCountryCode(){
var countryName=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
http4.open("GET", url, true);
http4.onreadystatechange = httpDestinationCountryName;
http4.send(null);
}
function httpDestinationCountryName(){
         if (http4.readyState == 4){
            var results = http4.responseText
            results = results.trim();
            var res = results.split("#");
            if(res.length>=1){
					document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value = res[0];
					if(res[1]=='Y'){
					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='Y';
					}else{
					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='N';
					}	try{
					document.forms['customerFileForm'].elements['customerFile.destinationCountry'].select;
					}catch(e){}
			}else{
                 
             }
         }
    }
    
//function getStateOnLoad(){try{var targetElement=document.forms.customerFileForm.elements["customerFile.originState"];myArray1="${originStateListJson}";myArray1=myArray1.replace(/\n/g,"\\\\n").replace(/\r/g,"\\\\r");var res=JSON.parse(myArray1);for(targetElement.length=res.length+1,document.forms.customerFileForm.elements["customerFile.originState"].options[0].text="",document.forms.customerFileForm.elements["customerFile.originState"].options[0].value="",i=1;i<res.length+1;i++)""==res[i]?(document.forms.customerFileForm.elements["customerFile.originState"].options[i].text="",document.forms.customerFileForm.elements["customerFile.originState"].options[i].value=""):(stateVal=res[i].split("#"),document.forms.customerFileForm.elements["customerFile.originState"].options[i].text=stateVal[1],document.forms.customerFileForm.elements["customerFile.originState"].options[i].value=stateVal[0]);document.getElementById("originState").value="${customerFile.originState}"}catch(e){}}
function getStateOnLoad(){
	try{
	 var targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
	 var myArray1='${originStateListJson}';
	 myArray1=myArray1.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r");
	// alert(myArray1);
 	var res = JSON.parse(myArray1);
		targetElement.length = res.length+1;
		document.forms['customerFileForm'].elements['customerFile.originState'].options[0].text = '';
		document.forms['customerFileForm'].elements['customerFile.originState'].options[0].value = '';
		for(i=1;i<res.length+1;i++)
			{
			if(res[i] == ''){
			document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
			document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
			document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
			}
			}
			document.getElementById("originState").value = '${customerFile.originState}';
	}catch(e){}
}   
function getState(targetElement) {
 var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
//alert(url);
 http2.open("GET", url, true);
 http2.onreadystatechange = handleHttpResponse5;
 http2.send(null);

}
//function getDestinationStateOnLoad(){try{var targetElement=document.forms.customerFileForm.elements["customerFile.destinationState"];myArray1="${destinationStateListJson}";myArray1=myArray1.replace(/\n/g,"\\\\n").replace(/\r/g,"\\\\r");var res=JSON.parse(myArray1);for(targetElement.length=res.length+1,document.forms.customerFileForm.elements["customerFile.destinationState"].options[0].text="",document.forms.customerFileForm.elements["customerFile.destinationState"].options[0].value="",i=1;i<res.length+1;i++)""==res[i]||void 0==res[i]?(document.forms.customerFileForm.elements["customerFile.destinationState"].options[i].text="",document.forms.customerFileForm.elements["customerFile.destinationState"].options[i].value=""):(stateVal=res[i].split("#"),document.forms.customerFileForm.elements["customerFile.destinationState"].options[i].text=stateVal[1],document.forms.customerFileForm.elements["customerFile.destinationState"].options[i].value=stateVal[0]);document.getElementById("destinationState").value="${customerFile.destinationState}"}catch(e){}}
function getDestinationStateOnLoad(){
	try{
	 var targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
	 var myArray1='${destinationStateListJson}';
	 myArray1=myArray1.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r");
 	 var res = JSON.parse(myArray1);
		targetElement.length = res.length+1;
		document.forms['customerFileForm'].elements['customerFile.destinationState'].options[0].text = '';
		document.forms['customerFileForm'].elements['customerFile.destinationState'].options[0].value = '';
		for(i=1;i<res.length+1;i++)
			{
			if(res[i] == '' || res[i]==undefined){
			document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
			document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
			document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
			}
			}
			document.getElementById("destinationState").value = '${customerFile.destinationState}';	
	}catch(e){}
			//alert("22222");
}
function getDestinationState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
//alert(url);
 http3.open("GET", url, true);
 http3.onreadystatechange = handleHttpResponse6;
 http3.send(null);

}       


</script>


<script type="text/javascript">
/*	function generatePortalId() {
		//document.forms['customerFileForm'].elements['customerFile.customerPortalId'].value = document.forms['customerFileForm'].elements['customerFile.corpID'].value + document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked = true ;
		var daReferrer = document.referrer; 
		var email = document.forms['customerFileForm'].elements['customerFile.email'].value + ',' +document.forms['customerFileForm'].elements['customerFile.email2'].value; 
		var errorMsg = "here here here is the error error error error"; 
		var subject = ""; 
		var name = ""; 
		var formNotes = ""; 
		var body_message = "%0D%0DHi "+name+"%0D%0D "+formNotes; 
		
		var mailto_link = 'mailto:'+email+'?subject='+subject+'&body='+body_message; 
		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close(); 
	}
	*/
	function generatePortalId() {
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked = true ;
		var checkPortalBox = document.forms['customerFileForm'].elements['customerFile.secondaryEmailFlag'].checked;
		var emailID = document.forms['customerFileForm'].elements['customerFile.email'].value;
		if(checkPortalBox==true && emailID !=''){
			var emailID2 = document.forms['customerFileForm'].elements['customerFile.email2'].value;
			emailID=emailID+','+emailID2;
		}
		var emailTypeFlag=false;
		var emailTypeVOERFlag=false;
		var emailTypeBOURFlag=false;
		var emailTypeINTMFlag=false;
		var emailTypeEnglishFlag=false;
		var germanCheck=false;
		try{
			germanCheck=document.forms['customerFileForm'].elements['checkGermanForCustomerSave'].checked;
		}catch(e){}
		var englishIntmCheck=false;		
		try{
			englishIntmCheck=document.forms['customerFileForm'].elements['checkEnglishForCustomerSave'].checked;
		}catch(e){}
		
		<configByCorp:fieldVisibility componentId="component.customerfile.field.emailtypeflag">
			emailTypeFlag = true;
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.customerfile.field.emailtypeVOERflag">
		  emailTypeVOERFlag=true;
	    </configByCorp:fieldVisibility>
	    <configByCorp:fieldVisibility componentId="component.customerfile.field.emailtypeBOURflag">
        emailTypeBOURFlag=true;
    </configByCorp:fieldVisibility>
		    if(germanCheck){
		   	 emailTypeINTMFlag=true;
		   }
		    if(englishIntmCheck){
		    	emailTypeEnglishFlag=true;
		    }	    
   
		var portalIdName = document.forms['customerFileForm'].elements['customerFile.customerPortalId'].value;		
		var checkPortalStatus=document.forms['customerFileForm'].elements['customerFile.status'].value;		
		if(checkPortalStatus !='CNCL' && checkPortalStatus !='CLOSED'){
			if(emailID!=''){
				if(portalIdName!=''){
					window.open('sendMail.html?id=${customerFile.id}&emailTo='+emailID+'&userID='+portalIdName+'&emailTypeFlag='+emailTypeFlag+'&emailTypeVOERFlag='+emailTypeVOERFlag+'&emailTypeBOURFlag='+emailTypeBOURFlag+'&emailTypeINTMFlag='+emailTypeINTMFlag+'&emailTypeEnglishFlag='+emailTypeEnglishFlag+'&decorator=popup&popup=true&from=file','','width=550,height=170') ;
				} else{
					alert("Please create ID for Customer Portal");
				}
			}else {
				alert("Please enter primary email");
				document.forms['customerFileForm'].elements['customerFile.email'].focus();
			}
		} else{
			alert("You can't Reset password for Customer Portal ID because job is Closed/Cancelled");
			document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked=false;
		} 
	} 
function findCompanyDivisionByBookAg() {
	var bookCode= document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;	
    var url="findCompanyDivisionByBookAg.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
    http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponse4444;
    http6.send(null);
}
	
 function handleHttpResponse4444()
        {if(4==http6.readyState){var results=http6.responseText;results=results.trim();var res=results.split("@"),targetElement=document.forms.customerFileForm.elements["customerFile.companyDivision"];for(targetElement.length=res.length,i=1;i<res.length;i++){document.forms.customerFileForm.elements["customerFile.companyDivision"].options[i].text=res[i],document.forms.customerFileForm.elements["customerFile.companyDivision"].options[i].value=res[i];var cD=document.forms.customerFileForm.elements["customerFile.companyDivision"].value;49!=cD&&(document.forms.customerFileForm.elements["customerFile.companyDivision"].value=""),res.length>2&&""==cD&&(document.forms.customerFileForm.elements["customerFile.companyDivision"].options[0].selected=!0),2==res.length&&(document.forms.customerFileForm.elements["customerFile.companyDivision"].options[1].selected=!0)}getJobList()}findCompanyCodeAndJobFromCompanyDivision();} 
 function findCompanyCodeAndJobFromCompanyDivision(){
		var bookCode= document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
		var url="findCompanyCodeAndJobFromCompanyDivisionAjax.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode)
			httpCompanyDivisionAjax.open("GET", url, true);
	    	httpCompanyDivisionAjax.onreadystatechange = handleHttpResponseCompanyDivisionAjax;
	    	httpCompanyDivisionAjax.send(null);
	}

	function handleHttpResponseCompanyDivisionAjax(){
		if (httpCompanyDivisionAjax.readyState == 4){
	       var results = httpCompanyDivisionAjax.responseText
	       results = results.trim();
	       if(results!=''){
	    	   var res = results.split('~')
	    	   if(res[0]!=null && res[0]!='' && res[0]!='NA'){
	    		   document.forms['customerFileForm'].elements['customerFile.companyDivision'].value = res[0];
	    	   }
	    	   if(res[1]!=null && res[1]!='' && res[1]!='NA'){
	    		   document.forms['customerFileForm'].elements['customerFile.job'].value = res[1];
	    	   }
	       }
		}
	}
	var httpCompanyDivisionAjax = getHTTPObject();
 function findResetCompanyDivisionByBookAg() {
		var bookCode= '${customerFile.bookingAgentCode}';	
	    if(bookCode!=''){
	    var url="findCompanyDivisionByBookAg.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
	    http6.open("GET", url, true);
	    http6.onreadystatechange = resetHandleHttpResponse4444;
	    http6.send(null);
	   }
	}
		
	 function resetHandleHttpResponse4444()
	        {if(4==http6.readyState){var results=http6.responseText;results=results.trim();var res=results.split("@"),targetElement=document.forms.customerFileForm.elements["customerFile.companyDivision"];for(targetElement.length=res.length,i=1;i<res.length;i++){document.forms.customerFileForm.elements["customerFile.companyDivision"].options[i].text=res[i],document.forms.customerFileForm.elements["customerFile.companyDivision"].options[i].value=res[i];var cD=document.forms.customerFileForm.elements["customerFile.companyDivision"].value;49!=cD&&(document.forms.customerFileForm.elements["customerFile.companyDivision"].value=""),res.length>2&&""==cD&&(document.forms.customerFileForm.elements["customerFile.companyDivision"].options[0].selected=!0),2==res.length&&(document.forms.customerFileForm.elements["customerFile.companyDivision"].options[1].selected=!0)}getJobList()}}  

</script>


<SCRIPT LANGUAGE="JavaScript">

function findAccToName(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
    if(billToCode==''){
    	document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
    }
    var accountSearchValidation=document.forms['customerFileForm'].elements['accountSearchValidation'].value;  
    if(billToCode!=''){
    	<c:if test="${accountSearchValidation}">
     var url="accountName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;;
 	</c:if>
	<c:if test="${!accountSearchValidation}">
    var url="accountName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;;
	</c:if>
     http51.open("GET", url, true);
     http51.onreadystatechange = handleHttpResponseAcc;
     http51.send(null);
    }
}

function handleHttpResponseAcc(){if(4==http51.readyState){var results=http51.responseText;results=results.trim();var res=results.split("#");res.size()>=2?"Approved"==res[2]?document.forms.customerFileForm.elements["customerFile.accountName"].value=res[1]:(alert("Account code is not approved"),document.forms.customerFileForm.elements["customerFile.accountCode"].value="",document.forms.customerFileForm.elements["customerFile.accountName"].value="",document.forms.customerFileForm.elements["customerFile.accountCode"].select()):(alert("Account code not valid"),document.forms.customerFileForm.elements["customerFile.accountCode"].value="",document.forms.customerFileForm.elements["customerFile.accountName"].value="",document.forms.customerFileForm.elements["customerFile.accountCode"].select())}}
//Need Change dilip
function copyBillToCode(){
 	var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    var accountSearchValidation=document.forms['customerFileForm'].elements['accountSearchValidation'].value; 
 	<c:if test="${accountSearchValidation}">
	var url="accountName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;;
 	</c:if>
	<c:if test="${!accountSearchValidation}">
	var url="accountName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;;
 	</c:if>
    http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponseCopyBill;
    http6.send(null);
}

function handleHttpResponseCopyBill(){if(4==http6.readyState){var results=http6.responseText;if(results=results.trim(),results.length>=1){var code=document.forms.customerFileForm.elements["customerFile.accountCode"].value,name=document.forms.customerFileForm.elements["customerFile.accountName"].value;""==code&&""==name&&(document.forms.customerFileForm.elements["customerFile.accountCode"].value=document.forms.customerFileForm.elements["customerFile.billToCode"].value)}var res=results.split("#");res.size()>=2&&"Approved"==res[2]&&(document.forms.customerFileForm.elements["customerFile.accountName"].value=res[1])}}    
/* Code Modified By Kunal for ticket number: 5913 */
function checkFindPricingBillingPaybaleBILLName(){
	if(document.forms['customerFileForm'].elements['customerFile.billToCode'].value == ''){
		getNewCoordAndSale('change');
	}
	else{
		findPricingBillingPaybaleBILLName();
	}
}
function findPricingBillingPaybaleBILLName(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
    var compDivision=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
    var url="pricingBillingPaybaleName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
    http8.open("GET", url, true);
    http8.onreadystatechange = handleHttpResponse2001;
   
    http8.send(null);
}
var testAuditorStatus='';
function handleHttpResponse2001(){if(4==http8.readyState){var results=http8.responseText;results=results.trim();var res=results.split("#");if(res.length>=2){for(var test=res[0].trim(),x=document.getElementById("personPricing"),personPricing=document.getElementById("personPricing").selectedIndex,a=0;a<x.length;a++)""!=test&&null!=test&&test==document.forms.customerFileForm.elements["customerFile.personPricing"].options[a].value.trim()&&(document.forms.customerFileForm.elements["customerFile.personPricing"].options[a].selected="true");for(var testBilling=res[1].trim(),y=document.getElementById("personBilling"),personBilling=document.getElementById("personBilling").selectedIndex,a=0;a<y.length;a++)""!=testBilling&&null!=testBilling&&testBilling==document.forms.customerFileForm.elements["customerFile.personBilling"].options[a].value.trim()&&(document.forms.customerFileForm.elements["customerFile.personBilling"].options[a].selected="true");for(var testPayable=res[2].trim(),z=document.getElementById("personPayable"),personPayable=document.getElementById("personPayable").selectedIndex,a=0;a<z.length;a++)""!=testPayable&&null!=testPayable&&testPayable==document.forms.customerFileForm.elements["customerFile.personPayable"].options[a].value.trim()&&(document.forms.customerFileForm.elements["customerFile.personPayable"].options[a].selected="true");for(var testAuditor=res[3].trim(),t=document.getElementById("auditor"),personAuditor=document.getElementById("auditor").selectedIndex,a=0;a<t.length;a++)""!=testAuditor&&null!=testAuditor&&testAuditor==document.forms.customerFileForm.elements["customerFile.auditor"].options[a].value.trim()&&(testAuditorStatus="change",document.forms.customerFileForm.elements["customerFile.auditor"].options[a].selected="true")}}}

	function duplicateBilltocode(){

	           		var code=document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
                   var name=document.forms['customerFileForm'].elements['customerFile.accountName'].value;
	               if(code=="" || name==""){  
	           	    document.forms['customerFileForm'].elements['customerFile.accountCode'].value = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
		            document.forms['customerFileForm'].elements['customerFile.accountName'].value = document.forms['customerFileForm'].elements['customerFile.billToName'].value;		
	           }
	           
}

function getContract() {	
	var custJobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
	document.forms['customerFileForm'].elements['custJobType'].value= custJobType;
	var contractBillCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	document.forms['customerFileForm'].elements['contractBillCode'].value= contractBillCode;
	var custCreatedOn = document.forms['customerFileForm'].elements['customerFile.createdOn'].value;
    document.forms['customerFileForm'].elements['custCreatedOn'].value= custCreatedOn;
    var custCompanyDivision=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
    var bookingAgentCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
    if(custJobType!=''){
	var url="findContractbyJob.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(custJobType)+"&contractBillCode="+encodeURI(contractBillCode)+"&custCreatedOn="+encodeURI(custCreatedOn)+"&companyDivision="+encodeURI(custCompanyDivision)+"&bookingAgentCode="+encodeURI(bookingAgentCode);
     http5.open("GET", url, true);
     http5.onreadystatechange = handleHttpResponse3;
     http5.send(null);
	}
}
//function getContractONLOad() {var custJobType=document.forms.customerFileForm.elements["customerFile.job"].value;document.forms.customerFileForm.elements.custJobType.value=custJobType;var contractBillCode=document.forms.customerFileForm.elements["customerFile.billToCode"].value;document.forms.customerFileForm.elements.contractBillCode.value=contractBillCode;var custCreatedOn=document.forms.customerFileForm.elements["customerFile.createdOn"].value;document.forms.customerFileForm.elements.custCreatedOn.value=custCreatedOn;var custCompanyDivision=document.forms.customerFileForm.elements["customerFile.companyDivision"].value;if(""!=custJobType){targetElement=document.forms.customerFileForm.elements["customerFile.contract"];var contarctlist="${contracts}";contarctlist=contarctlist.replace("[",""),contarctlist=contarctlist.replace("]",""),contarctlist=" ,"+contarctlist;var contarctlistarr=contarctlist.split(",");targetElement.length=contarctlistarr.length;for(var i=0;i<contarctlistarr.length;i++)document.forms.customerFileForm.elements["customerFile.contract"].options[i].text=contarctlistarr[i].trim(),document.forms.customerFileForm.elements["customerFile.contract"].options[i].value=contarctlistarr[i].trim();var tempContract="${customerFile.contract}";try{tempContract=tempContract.trim()}catch(e){}document.getElementById("contract").value=tempContract,document.getElementById("contract").value=tempContract,onloadFindDefaultSettingFromPartner();var tempchk=document.forms.customerFileForm.elements["customerFile.billPayMethod"];(null==tempchk.value||""==tempchk.value)&&checkPriceContractOnload()}}
function getContractONLOad() {
	var custJobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
	document.forms['customerFileForm'].elements['custJobType'].value= custJobType;
	var contractBillCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	document.forms['customerFileForm'].elements['contractBillCode'].value= contractBillCode;
	var custCreatedOn = document.forms['customerFileForm'].elements['customerFile.createdOn'].value;
    document.forms['customerFileForm'].elements['custCreatedOn'].value= custCreatedOn;
    var custCompanyDivision=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
    if(custJobType!=''){
	//
    	 targetElement = document.forms['customerFileForm'].elements['customerFile.contract'];			
			//
			var contarctlist='${contracts}';
		    contarctlist=contarctlist.replace("[","");
		    contarctlist=contarctlist.replace("]","");    
		    contarctlist=" ,"+contarctlist;
		    var contarctlistarr=contarctlist.split(",");  
		    targetElement.length = contarctlistarr.length+1;
		    document.forms['customerFileForm'].elements['customerFile.contract'].options[0].text = '';
			document.forms['customerFileForm'].elements['customerFile.contract'].options[0].value = '';
				for(var i=1;i<contarctlistarr.length;i++){
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text =contarctlistarr[i].trim();
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value =contarctlistarr[i].trim();
				} 
				var tempContract='${customerFile.contract}';
				try{			
				tempContract=tempContract.trim();
				}catch(e){}
				 document.getElementById("contract").value = tempContract;
			//
				 //document.getElementById("contract").value =tempContract; 
				 onloadFindDefaultSettingFromPartner();
				 var tempchk=document.forms['customerFileForm'].elements['customerFile.billPayMethod'];
	             if(tempchk.value==null || tempchk.value==''){
	            checkPriceContractOnload();
	             }
    	//
	} }

function checkPriceContractOnload() {
	  var results ='${contractDiscountAddValue}';
      results = results.trim();
      var res = results.split("#");                           
      if(res.length>=1)
      {
      var billPayMethod = document.getElementById('billPayMethod').selectedIndex;
      var partnerCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
      if(billPayMethod == 0 ){
      document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value= res[5];                 
	     }else if(billPayMethod != 0 && (partnerCode==null || partnerCode=='')){
	    	 document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value= res[5]; 
	     }
	     }
      }

function onloadFindDefaultSettingFromPartner(){var cfContract=document.forms.customerFileForm.elements["customerFile.contract"].value,cfSource=document.forms.customerFileForm.elements["customerFile.source"].value;"undefined"==document.forms.customerFileForm.elements["customerFile.billToAuthorization"].value&&(document.forms.customerFileForm.elements["customerFile.billToAuthorization"].value="");var cfBillToAuth=document.forms.customerFileForm.elements["customerFile.billToAuthorization"].value,bCode=document.forms.customerFileForm.elements["customerFile.billToCode"].value;if((""==cfContract||""==cfSource||""==cfBillToAuth)&&""!=bCode){var results="${agentParentList1}";if(""!=results){var res=results.split("~");""==document.forms.customerFileForm.elements["customerFile.contract"].value&&"NA"!=res[0]&&(document.forms.customerFileForm.elements["customerFile.contract"].value=res[0]),""==document.forms.customerFileForm.elements["customerFile.source"].value&&"NA"!=res[3]&&(document.forms.customerFileForm.elements["customerFile.source"].value=res[3]),""==document.forms.customerFileForm.elements["customerFile.billToAuthorization"].value&&"NA"!=res[9]&&(document.forms.customerFileForm.elements["customerFile.billToAuthorization"].value=res[9])}}}
function getState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse5;
     http3.send(null);
	
	}
	
function getDestinationState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse6;
     http2.send(null);
	
	}

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function enableDisableAccountContact()
{
<configByCorp:fieldVisibility componentId="customerFile.contactName">		
var element = document.getElementById('accountContactHid');
var billtocode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
if(billtocode!='' && billtocode=='500270'){
element.style.display = 'block';
element.style.visibility = 'visible';
}else{
element.style.display = 'none';
}
</configByCorp:fieldVisibility>
}

function findBillToName() {
    var billToCode =  document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    if(billToCode==''){
    	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
	}
    if(billToCode!=''){
     var url="QuotationBookingAgentName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    } 
}
<c:set var="accountHollValidation" value="N" />
<configByCorp:fieldVisibility componentId="component.field.accountCode.Validation">
<c:set var="accountHollValidation" value="Y" />
</configByCorp:fieldVisibility>
function handleHttpResponse2(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
               // alert(results);
                 var res = results.split("#");                 
           		if(res.size() >= 2){ 
	           		if(res[2] == 'Approved'|| res[2] == 'New'){
	           			if(res[3]== '' || res[3] == document.forms['customerFileForm'].elements['customerFile.companyDivision'].value || res[3] == 'null'){
	           			document.forms['customerFileForm'].elements['customerFile.billToName'].value = res[1];
 						document.forms['customerFileForm'].elements['customerFile.customerEmployer'].value = res[1];
 						//document.forms['customerFileForm'].elements['customerFile.billToName'].select();
	           			}else{
		           			if(document.forms['customerFileForm'].elements['customerFile.companyDivision'].value != ''){
		           				alert("Company Division of selected bill to code is not "+ document.forms['customerFileForm'].elements['customerFile.companyDivision'].value); 
		           			}else{
		           				alert('Please select company division.');
		           			} 
		           			document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";		           			
						 	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
						 	document.forms['customerFileForm'].elements['customerFile.billToCode'].select();
		           		}	
	           			//
	           			var test = res[6];
	           			//alert(test);
							var x=document.getElementById("billPayMethod");
							var billPayMethod = document.getElementById('billPayMethod').selectedIndex;	
							
								for(var a = 0; a < x.length; a++){
									if(test == document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].value){
										document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].selected="true";
									}}
	           			//
 		           		}else if(res[2] != 'Approved'){
	           			alert("Bill To code is not approved" ); 
					    document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
					 	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
					 	document.forms['customerFileForm'].elements['customerFile.billToCode'].select();
					}	
	           }
	           else{
	               alert("Bill To code not valid");	
				   document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
				   document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
				   <c:if test="${accountHollValidation=='N'}">
				   document.forms['customerFileForm'].elements['customerFile.accountCode'].value="";
				   document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
				   </c:if>
				   document.forms['customerFileForm'].elements['customerFile.billToCode'].select();
				 }
           }
           enableDisableAccountContact();
}
  function handleHttpResponse3()        {

             if (http5.readyState == 4)             {
            	
                var results = http5.responseText
                results = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split("@");
              
                targetElement = document.forms['customerFileForm'].elements['customerFile.contract'];
				targetElement.length = res.length;
				
 				for(i=0;i<res.length;i++)
					{ 
								 
					if(res[i] == ''){						 
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value = '';
					}else{						 
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text =res[i];
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value =res[i];
					}
					}
					<c:if test="${empty customerFile.id}">	
								  
						document.forms['customerFileForm'].elements['customerFile.contract'].value=document.forms['customerFileForm'].elements['defaultcontract'].value;
					</c:if> 
					<c:if test="${not empty customerFile.id}">					 	
					document.getElementById("contract").value='${customerFile.contract}';
					</c:if> 
		         findDefaultSettingFromPartner();
             }
             var tempchk=document.forms['customerFileForm'].elements['customerFile.billPayMethod'];
             if(tempchk.value==null || tempchk.value==''){
             checkPriceContract();
             }
        }
function handleHttpResponse4(){
		if (http2.readyState == 4) {
              var results = http2.responseText
              results = results.trim();
              var res = results.split("#");
           		if(res.size() >= 2){ 
           			if(res[2] == 'Approved'|| res[2] == 'New'){
           				showOrHide(0);
           				document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value = res[1];
          				// document.forms['customerFileForm'].elements['customerFile.companyDivision'].value = ""
          				document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].select();
	           		}else{
	           			showOrHide(0);
	           			alert("Booking Agent code is not approved" ); 
					     document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value="";
					 	document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value="";
					 //	document.forms['customerFileForm'].elements['customerFile.companyDivision'].value = ""
					 	document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].select();
	           		}			
          		}else{
          			showOrHide(0);
               		alert("Booking Agent code not valid");
                     document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value="";
					// document.forms['customerFileForm'].elements['customerFile.companyDivision'].value = ""
					 document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].select();
		   		}
         
      }
} 
    function handleHttpResponse5()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("originState").value = '${customerFile.originState}';
             }
        }  
        
function handleHttpResponse6()
        {

             if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                
                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("destinationState").value = '${customerFile.destinationState}';
             }
        }  
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    
   	var http2 = getHTTPObject();
   	var http50 = getHTTPObject50();
	var http51 = getHTTPObject51();
    var http3 = getHTTPObject1();
	var http4 = getHTTPObject3();
	var http5 = getHTTPObject();
	var http6 = getHTTPObject2();
    var http8 = getHTTPObject8();
     var http9 = getHTTPObject9();
    var httpState = getHTTPObjectState()
    var http22 = getHTTPObject22();
    var http33 = getHTTPObject33();
    var http99 = getHTTPObject();
    var httpBilltoCode = getHTTPObjectBilltoCode();
    var http12 = getHTTPObject(); 
    var http45 = getHTTPObject45();
    var http46 = getHTTPObject46();
    var http49=getHTTPObject49();
    var http44 = getHTTPObject();
function getHTTPObjectBilltoCode()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getHTTPObject22()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject50()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject51()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http551 = getHTTPObject551(); 
function getHTTPObject551()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject33()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    function getHTTPObject9()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

 var http555 = getHTTPObject555();   
function getHTTPObject555()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    
    

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject45()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject46()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject49()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
  function getHTTPObjectState()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject2(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject3(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    
 function getHTTPObject8(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    
</script>


<script>	
function changeStatus(){
	document.forms['customerFileForm'].elements['formStatus'].value = '1';
}

function saveAuto(clickType){ 
	enableCheckBox();
	var quotStatus = document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value;
	var lName = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	if(quotStatus == 'Accepted'  && lName == ''){
 		alert("Please fill LastName");
 	 	return false;	
 	}
	progressBarAutoSave('1');
     if(!(clickType == 'save')){
     if ('${autoSavePrompt}' == 'No'){	
		var noSaveAction;
		var id1 = document.forms['customerFileForm'].elements['customerFile.id'].value;
		var billCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
		if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.quotaion'){
			noSaveAction = 'quotations.html';
		}
		if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
			noSaveAction = 'quotationServiceOrders.html?id='+id1+'&forQuotation=QC';
		}		
		if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.survey'){
			noSaveAction = 'surveyDetails.html?cid=${customerFile.id}&Quote=y';
		}
		if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.accountpolicy1'){
			noSaveAction = 'showAccountPolicyQuotation.html?id='+id1+'&jobNumber=${customerFile.sequenceNumber}&code='+billCode;
			}
		processAutoSave(document.forms['customerFileForm'],'saveQuotationFile!saveOnTabChange.html', noSaveAction );	
	}
	else{
	   var id1 = document.forms['customerFileForm'].elements['customerFile.id'].value;
	   var billCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	   if (document.forms['customerFileForm'].elements['formStatus'].value == '1'){
	       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the 'Quotation Form'");
		       if(agree){
		           document.forms['customerFileForm'].action = 'saveQuotationFile!saveOnTabChange.html';
		           document.forms['customerFileForm'].submit();
		       }else{
		           if(id1 != ''){
		           	   if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.quotaion'){
			               location.href = 'quotations.html';
			               }
			           if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
			               location.href = 'quotationServiceOrders.html?id='+id1+'&forQuotation=QC';
			               }
			   			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.survey'){
			   				location.href = 'surveyDetails.html?cid=${customerFile.id}&Quote=y';
							}
			   			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.accountpolicy1'){
							location.href = 'showAccountPolicyQuotation.html?id='+id1+'&jobNumber=${customerFile.sequenceNumber}&code='+billCode;
							}		               
      				 }
       			}
   			}else{
			   if(id1 != ''){
			   	   if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.quotaion'){
			               location.href = 'quotations.html';
			               }
			       if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
			           location.href = 'quotationServiceOrders.html?id='+id1+'&forQuotation=QC';
			       }
		   			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.survey'){
		   				location.href = 'surveyDetails.html?cid=${customerFile.id}&Quote=y';
						}
		   			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.accountpolicy1'){
						location.href = 'showAccountPolicyQuotation.html?id='+id1+'&jobNumber=${customerFile.sequenceNumber}&code='+billCode;
						}			       
   				}
   			}
		}
		}
     
  }


function changeStatus(){
   document.forms['customerFileForm'].elements['formStatus'].value = '1';
}
function showStatusReason(){
	var quotationStatus=document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value;
	<configByCorp:fieldVisibility componentId="component.field.qfStatusReason.showVOER">
	if(quotationStatus=='Rejected' || quotationStatus=='Cancelled'){
		document.getElementById('statusReasonId').style.display="block";
		document.getElementById('statusReasonId1').style.display="block";
	}else{
		document.getElementById('statusReasonId').style.display="none";
		document.getElementById('statusReasonId1').style.display="none";
	}
	</configByCorp:fieldVisibility>
}
function autoPopulate_customerFile_statusDate(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['customerFile.statusDate'].value=datam; 
		checkQuotation();
	}
	function findBookingAgentName(){
    var bookingAgentCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
    if(bookingAgentCode == ''){
    	document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value="";
    }
    if(bookingAgentCode != ''){
    	showOrHide(1)
     var url="QuotationBookingAgentName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(bookingAgentCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
    } 
}

function lastName(){
	var quotStatus = document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value;
	var lName = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	if(quotStatus == 'Accepted'  && lName == ''){
 		alert("Please fill LastName");
 	 	return false;	
 	}
 		return true;
}
var http50 = getHTTPObject50();
	function getHTTPObject50()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http51 = getHTTPObject51();
	function getHTTPObject51()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http52 = getHTTPObject52();
	function getHTTPObject52()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var http53 = getHTTPObject53();
	function getHTTPObject53()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http54 = getHTTPObject54();
	function getHTTPObject54()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http55 = getHTTPObject55();
	function getHTTPObject55()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http56 = getHTTPObject56();
	function getHTTPObject56()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
	var http66 = getHTTPObject66();
	function getHTTPObject66() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    } else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)   {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var httpRef = getHTTPObjectRef();
function getHTTPObjectRef(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
var httpRefSale = getHTTPObjectRefSale();
function getHTTPObjectRefSale(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }   }
    return xmlhttp;
}
function getRefCoord(){
	if(document.forms['customerFileForm'].elements['customerFile.coordinator'].value == ''){
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
	   	if(job!=''){
	     	var url = "findCoordRef.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
	     	httpRef.open("GET", url, true);
	     	httpRef.onreadystatechange = handleHttpResponseRef;
	     	httpRef.send(null);
	    } }}
	function handleHttpResponseRef(){	
	if (httpRef.readyState == 4){
		   var cordVal = "";
           var results = httpRef.responseText
           results = results.trim();
           res = results.split("~");
           for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].value = '';
			}else{
				cordVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.coordinator'].value = cordVal[0];
			}}}}		
function getRefSale(){
	if(document.forms['customerFileForm'].elements['customerFile.estimator'].value == ''){
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
	   	if(job!=''){
	     	var url = "findSaleRef.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
	     	httpRefSale.open("GET", url, true);
	     	httpRefSale.onreadystatechange = handleHttpResponseRefSale;
	     	httpRefSale.send(null);
	    } }}
function handleHttpResponseRefSale(){	
	if (httpRefSale.readyState == 4){
		   var saleVal = "";
           var results = httpRefSale.responseText
           results = results.trim();
           res = results.split("@");
           for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.estimator'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.estimator'].options[i].value = '';
			}else{
				saleVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.estimator'].value = saleVal[0];
			}}}}
function getNewCoordAndSale(temp){
	var cfContractType="";
	var checkUTSI="${company.UTSI}";
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
    var checkSoFlag = false;
    var bookAgCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
   	if(job!=''){
     	var url = "findCoord.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job)+"&cfContractType="+encodeURI(cfContractType)+"&checkUTSI="+encodeURI(checkUTSI)+"&bookAgCode="+encodeURI(bookAgCode)+"&checkSoFlag="+encodeURI(checkSoFlag);
     	http50.open("GET", url, true); 
     	http50.onreadystatechange = function(){ handleHttpResponse50(temp);};
     	http50.send(null);
    } } 
var pricingStatus='';
var billingStatus='';
var payableStatus='';
var approvedByStatus=''; 
function handleHttpResponse50(temp){	
		if (http50.readyState == 4){
                var results = http50.responseText
                results = results.trim();
                res = results.split("~");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.coordinator'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].text = '';
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].text = stateVal[1];
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].value = stateVal[0];
					}
				}
				if(temp=='load'){
				document.getElementById("coordinator").value = '${customerFile.coordinator}';
                }
                else{
                getRefCoord();
                getRefSale();
                }
			/*	if(document.getElementById("coordinator").value == ''){
					getRefCoord();
				}
				if(document.getElementById("estimator").value == ''){
					getRefSale();
				} */
				var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
				var perPricing =document.forms['customerFileForm'].elements['customerFile.personPricing'].value;
				var perBilling=document.forms['customerFileForm'].elements['customerFile.personBilling'].value;
				var perPayable=document.forms['customerFileForm'].elements['customerFile.personPayable'].value;
				var perAuditor=document.forms['customerFileForm'].elements['customerFile.auditor'].value;
				var perApprovedBy=document.forms['customerFileForm'].elements['customerFile.approvedBy'].value;
				if(billToCode=='' || perPricing=='' ){
				pricingStatus='change';
				getPricing(temp);
				}
				else if(billToCode=='' || perBilling==''){
				billingStatus='change';
				getBilling(temp);
				}
				else if(billToCode=='' || perApprovedBy==''){
				approvedByStatus='change';
				getApprovedBy(temp);
				}
				else if(billToCode=='' || perPayable==''){
				payableStatus='change';
				getPayable(temp);
				}
        }
}
function getPricing(temp){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
   	if(job!=''){
     	var url = "findPricing.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http52.open("GET", url, true);
     	http52.onreadystatechange = function(){handleHttpResponse52(temp);};
     	http52.send(null);
    }  
}
function handleHttpResponse52(temp){
		if (http52.readyState == 4){
                var results = http52.responseText
                results = results.trim(); 
                res = results.split("@");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.personPricing'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.personPricing'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.personPricing'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.personPricing'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.personPricing'].options[i].value = stateVal[0];
					}
					}
				if(('${customerFile.personPricing}' != '' || '${customerFile.personPricing}' != null) && '${customerFile.personPricing}'.length > 0){
					document.getElementById("personPricing").value = '${customerFile.personPricing}';
				}
             	var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
				var perPricing =document.forms['customerFileForm'].elements['customerFile.personPricing'].value;
				var perBilling=document.forms['customerFileForm'].elements['customerFile.personBilling'].value;
				var perPayable=document.forms['customerFileForm'].elements['customerFile.personPayable'].value;
				var perAuditor=document.forms['customerFileForm'].elements['customerFile.auditor'].value;
				var perApprovedBy=document.forms['customerFileForm'].elements['customerFile.approvedBy'].value;
				 if(billToCode=='' || perBilling==''){
				 billingStatus='change';
				getBilling(temp);
				}
				else if(billToCode=='' || perApprovedBy==''){
				approvedByStatus='change';
				getApprovedBy(temp);
				}
				else if(billToCode=='' || perPayable==''){
				payableStatus='change';
				getPayable(temp);
				}
				else if(temp=='change'){
				fillResponsiblePerson();
				}
        }
  }
   function getBilling(temp){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
   	if(job!=''){
     	var url = "findBilling.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http53.open("GET", url, true);
     	http53.onreadystatechange = function(){handleHttpResponse53(temp);};
     	http53.send(null);
    } 
}
function handleHttpResponse53(temp){
		if (http53.readyState == 4){
                var results = http53.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.personBilling'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.personBilling'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.personBilling'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.personBilling'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.personBilling'].options[i].value = stateVal[0];
					}
					}
 				if(('${customerFile.personBilling}' != '' || '${customerFile.personBilling}' != null) && '${customerFile.personBilling}'.length > 0){
					document.getElementById("personBilling").value = '${customerFile.personBilling}';
 				}
        var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
				var perPricing =document.forms['customerFileForm'].elements['customerFile.personPricing'].value;
				var perBilling=document.forms['customerFileForm'].elements['customerFile.personBilling'].value;
				var perPayable=document.forms['customerFileForm'].elements['customerFile.personPayable'].value;
				var perAuditor=document.forms['customerFileForm'].elements['customerFile.auditor'].value;
				var perApprovedBy=document.forms['customerFileForm'].elements['customerFile.approvedBy'].value;
				 if(billToCode=='' || perApprovedBy==''){
				 approvedByStatus='change';
				getApprovedBy(temp);
				}
				else if(billToCode=='' || perPayable==''){
				payableStatus='change';
				getPayable(temp);
				}
				else if(temp=='change'){
				fillResponsiblePerson();
				}
        }
  }
    function getPayable(temp){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
   	if(job!=''){
     	var url = "findPayable.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http54.open("GET", url, true);
     	http54.onreadystatechange = function(){handleHttpResponse54(temp);};
     	http54.send(null);
    } 
}
function handleHttpResponse54(temp){
		if (http54.readyState == 4){
                var results = http54.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.personPayable'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.personPayable'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.personPayable'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.personPayable'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.personPayable'].options[i].value = stateVal[0];
					}
					}
 				if(('${customerFile.personPayable}' != '' || '${customerFile.personPayable}' != null) && '${customerFile.personPayable}'.length > 0){
					document.getElementById("personPayable").value = '${customerFile.personPayable}';
 				}
          } 
          if(temp=='change'){
          fillResponsiblePerson();
          }
  }
   function getApprovedBy(temp){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
   	if(job!=''){
     	var url = "findApprovedBy.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http55.open("GET", url, true);
     	http55.onreadystatechange = function(){handleHttpResponse55(temp);};
     	http55.send(null);
    } 
}
function handleHttpResponse55(temp){
		if (http55.readyState == 4){
                var results = http55.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.approvedBy'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.approvedBy'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.approvedBy'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.approvedBy'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.approvedBy'].options[i].value = stateVal[0];
					}
					}
 				if(('${customerFile.approvedBy}' != '' || '${customerFile.approvedBy}' != null) && '${customerFile.approvedBy}'.length > 0){
					document.getElementById("approvedBy").value = '${customerFile.approvedBy}';
 				}
        var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
				var perPricing =document.forms['customerFileForm'].elements['customerFile.personPricing'].value;
				var perBilling=document.forms['customerFileForm'].elements['customerFile.personBilling'].value;
				var perPayable=document.forms['customerFileForm'].elements['customerFile.personPayable'].value;
				var perAuditor=document.forms['customerFileForm'].elements['customerFile.auditor'].value;
				var perApprovedBy=document.forms['customerFileForm'].elements['customerFile.approvedBy'].value;
				 if(billToCode=='' || perPayable==''){
				 payableStatus='change';
				getPayable(temp);
				}
				else if(temp=='change'){
				fillResponsiblePerson();
				}
        }
  }
function fillResponsiblePerson(){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
    var companyDivision = document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
   	if(job!=''){
     	var url = "fillResponsiblePerson.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job)+"&companyDivision="+encodeURI(companyDivision);
     	http66.open("GET", url, true);
     	http66.onreadystatechange = handleHttpResponse66;
     	http66.send(null);
    } 
}
function handleHttpResponse66(){ 
	   if (http66.readyState == 4){
	                var results = http66.responseText
	                results = results.trim();
	                results=results.replace("[",''); 
	                results = results.replace("]",''); 
	                var res = results.split("#");
	                var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	                var perBilling=document.forms['customerFileForm'].elements['customerFile.personBilling'].value;
	                var perAuditor=document.forms['customerFileForm'].elements['customerFile.auditor'].value;
	                var perPricing =document.forms['customerFileForm'].elements['customerFile.personPricing'].value;
					var perPayable=document.forms['customerFileForm'].elements['customerFile.personPayable'].value;
					//alert("Response1: " + res[0].length + "Response2: " + res[1].length +"Response3: " + res[2].length +"Response4: " + res[3].length);
					if(res[0]!= null && res[0]!='' && res[0]!= 'undefined'){
						if(perBilling=='' || billToCode=='' || billingStatus=='change' ){
							billingStatus='';
		                	document.forms['customerFileForm'].elements['customerFile.personBilling'].value=res[0];
		                }
	  				}
	  				if(res[1]!= null && res[1]!='' && res[1]!= 'undefined'){
			            if((perAuditor=='' || billToCode=='') && testAuditorStatus != 'change'){
			                document.forms['customerFileForm'].elements['customerFile.auditor'].value=res[1];
			            }
	  				}
	  				if(res[2]!= null && res[2]!='' && res[2]!= 'undefined'){
			            if(perPricing=='' || billToCode=='' || pricingStatus=='change'){
			            	pricingStatus='';
			                document.forms['customerFileForm'].elements['customerFile.personPricing'].value=res[2];
			            }
	  				}
	  				if(res[3]!= null && res[3]!='' && res[3]!= 'undefined'){
			            if(perPayable=='' || billToCode=='' || payableStatus=='change'){
			            	payableStatus='';
			                document.forms['customerFileForm'].elements['customerFile.personPayable'].value=res[3];
						}
	  				}
	           //      if(document.forms['customerFileForm'].elements['customerFile.coordinator'].value=='')
	               	if(res[4]!= null && res[4]!='' && res[4]!= 'undefined'){
		               	 document.forms['customerFileForm'].elements['customerFile.coordinator'].value=res[4];
	               	}
	               	if(res[5]!= null && res[5]!='' && res[5]!= 'undefined'){
	                 	document.forms['customerFileForm'].elements['customerFile.estimator'].value=res[5];
	               	}
	                 
	        }
	   }
  
function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
	}

	function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
		lastName=lastName.replace("~","'");
		document.getElementById(partnerNameId).value=lastName;
		document.getElementById(paertnerCodeId).value=partnercode;
		document.getElementById(autocompleteDivId).style.display = "none";	
		if(paertnerCodeId=='quotationBookingAgentCodeId'){
			findBookingAgentName();
			findCompanyDivisionByBookAg();
		}else if(paertnerCodeId=='originAgentCode'){
			findOriginAgentName();
		}else if(paertnerCodeId=='quotationBillToCodeId'){
			findBillToName();
			getContract();
			checkFindPricingBillingPaybaleBILLName();
			findAssignmentFromParentAgent();
			fillAccountCodeByBillToCode();
			fillBillToAuthority();
		}else if(paertnerCodeId=='quotationAccountCodeId'){
			findAccToName();
		}
	}
	
	hideAddressDetails();
	function hideAddressDetails(){
	<c:if test="${usertype=='AGENT'}">	
		 var img =  document.getElementById("viewDetails");
		 img.style.visibility = 'hidden';
		 </c:if>
		}
function viewPartnerDetailsForBillToCode(position) {
    var partnerCode = "";
    var originalCorpID='${customerFile.corpID}';
    partnerCode =  document.getElementById("quotationBillToCodeId").value;
    var url="viewPartnerDetailsForBillTo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&sessionCorpID=" + encodeURI(originalCorpID);
    ajax_showTooltip(url,position);
    return false
}
</script>