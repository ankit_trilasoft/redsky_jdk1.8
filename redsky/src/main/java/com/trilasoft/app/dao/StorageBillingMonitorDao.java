package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.StorageBillingMonitor;

public interface StorageBillingMonitorDao extends GenericDao<StorageBillingMonitor, Long>{
	public List checkBillingFlag(String corpId);
	public List getStorageBillingMonitorList(String corpId);
}
