package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.UpdateRuleResultDao;
import com.trilasoft.app.model.UpdateRuleResult;

public class UpdateRuleResultDaoHibernate extends GenericDaoHibernate<UpdateRuleResult, Long> implements UpdateRuleResultDao {
	
	public UpdateRuleResultDaoHibernate() {
		super(UpdateRuleResult.class);
	}
	public List findMaximumId() {
		return getHibernateTemplate().find("select max(id) from UpdateRuleResult");
	}
}
