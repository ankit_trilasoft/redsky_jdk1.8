//--Created By Bibhash Kumar pathak--

package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DataCatalog;


public interface DataCatalogDao extends GenericDao<DataCatalog, Long>{
	
	
	public List findMaximumId(); 

	public List findById(Long id);
	public List getList();
	public List<DataCatalog> searchdataCatalog(String tableName, String fieldName, String auditable, String description,String usDomestic, String defineByToDoRule,String visible, String charge);
    public List configurableTable(boolean b);
    public List configurableField(String tableName,boolean b);
    
    public List getCorpAuthoritiesForComponent();

	public boolean getFieldVisibilityAttrbute(String userCorpID, String componentId);
	 public List checkData(String tableName, String fieldName, String sessionCorpID);
	 public void mergeDataCatalog(DataCatalog dc);
}
