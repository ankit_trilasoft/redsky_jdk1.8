package com.trilasoft.app.webapp.action;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.Payroll;
import com.trilasoft.app.model.Sharing;
import com.trilasoft.app.service.PayrollManager;
import com.trilasoft.app.service.SharingManager;

public class SharingAction extends BaseAction{
	
	
	private Long id;
	private Long maxId;
	private List<Sharing> sharingList;
	private SharingManager sharingManager; 
	private Sharing sharing;
	private String sessionCorpID;
	private Map<String, String> compDevision;
	private PayrollManager payrollManager;
	
	private Set sharingSet;

	private List searchList;
	
	public List getSearchList() {
		return searchList;
	}

	public void setSearchList(List searchList) {
		this.searchList = searchList;
	}
	
	public String getComboList(String corpId){
		compDevision=payrollManager.getOpenCompnayDevisionCode(corpId);
		return SUCCESS;
	}

	public String sharingList(){
		
		sharingList=sharingManager.getAll();
		sharingSet=new HashSet(sharingList);		
	return SUCCESS;

	}
	
	public SharingAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	
	public String edit(){
		getComboList(sessionCorpID);
		if (id != null)
		{
			sharing=sharingManager.get(id);
			try{
				if(sharing !=null && sharing.getCompanyDivision()!=null && (!(sharing.getCompanyDivision().toString().trim().equals("")))){
					
					if(compDevision.containsKey(sharing.getCompanyDivision())){
						
					}else{
						compDevision=payrollManager.getOpenCompnayDevisionCode(sessionCorpID);
					}
					}
				}
			catch (Exception exception) {
				exception.printStackTrace();
			}
		}
		else
		{
			sharing=new Sharing();
			sharing.setCreatedOn(new Date());
			sharing.setValueDate(new Date());
			sharing.setUpdatedOn(new Date());
		}
		sharing.setCorpId(sessionCorpID);
		
		return SUCCESS;
	}

	
public String save() throws Exception {
	
		getComboList(sessionCorpID);
		boolean isNew = (sharing.getId() == null); 
		
		if(isNew)
	    {
			sharing.setCreatedOn(new Date());
	    }
	   
		sharing.setUpdatedOn(new Date());
		sharing.setCorpId(sessionCorpID);
		sharing.setUpdatedBy(getRequest().getRemoteUser());
		sharing= sharingManager.save(sharing);		
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "Sharing has been added successfully" : "Sharing has been updated successfully";
		saveMessage(getText(key));
		}
		if (!isNew) {   
        	return INPUT;   
        } else {   
        	maxId = Long.parseLong(sharingManager.findMaximumId().get(0).toString());
        	sharing = sharingManager.get(maxId);
          return SUCCESS;
        } 
	}

 @SkipValidation
    public String search(){        
     boolean code = (sharing.getCode() == null);
     boolean discription = (sharing.getDescription() == null);
    
        if(!code || !discription){
  	      searchList = sharingManager.searchSharing(sharing.getCode(),sharing.getDescription());
  	    sharingSet =new HashSet(searchList);
  	  }	    	   
   return SUCCESS;     
} 


	private String gotoPageString;
	public String saveOnTabChange() throws Exception {
 	String s = save();
 	return gotoPageString;
 }
 
 
	public void setId(Long id) {
		this.id = id;
	}


	public Sharing getSharing() {
		return sharing;
	}


	public void setSharing(Sharing sharing) {
		this.sharing = sharing;
	}


	public void setSharingManager(SharingManager sharingManager) {
		this.sharingManager = sharingManager;
	}


	


	public Set getSharingSet() {
		return sharingSet;
	}

	public void setSharingSet(Set sharingSet) {
		this.sharingSet = sharingSet;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}


	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}



	public List<Sharing> getSharingList() {
		return sharingList;
	}

	public void setSharingList(List<Sharing> sharingList) {
		this.sharingList = sharingList;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public Map<String, String> getCompDevision() {
		return compDevision;
	}

	public void setCompDevision(Map<String, String> compDevision) {
		this.compDevision = compDevision;
	}

	public void setPayrollManager(PayrollManager payrollManager) {
		this.payrollManager = payrollManager;
	}


	
}
	
	
