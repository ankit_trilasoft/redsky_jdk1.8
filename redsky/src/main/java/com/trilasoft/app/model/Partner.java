package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "partner")
@FilterDef(name = "partnerPrivateCorpID", parameters = { @ParamDef(name = "privateCorpID", type = "string") })
@Filter(name = "partnerPrivateCorpID", condition = "corpID in (:privateCorpID) ")
public class Partner extends BaseObject implements ListLinkData {

	private Long id;

	private String corpID;

	private String partnerCode;

	private String partnerPrefix;

	private String firstName;

	private String lastName;

	private String middleInitial;

	private String partnerSuffix;

	private String terminalAddress1;

	private String terminalAddress2;

	private String terminalAddress3;

	private String terminalAddress4;

	private String mailingCountry;

	private String mailingTelex;

	private String mailingPhone;

	private String mailingFax;

	private String mailingEmail;

	private String mailingCountryCode;

	private String mailingAddress1;

	private String mailingAddress2;

	private String mailingAddress3;

	private String mailingAddress4;

	private String mailingCity;

	private String mailingState;

	private String mailingZip;

	private String terminalPhone;

	private String terminalTelex;

	private String terminalFax;

	private String terminalCountryCode;

	private String terminalCountry;

	private String terminalCity;

	private String terminalState;

	private String terminalZip;

	private String terminalEmail;

	private String abbreviation;

	private String billingCountryCode;

	private String billingAddress1;

	private String billingAddress2;

	private String billingAddress3;

	private String billingAddress4;

	private String billingCity;

	private String billingCountry;

	private String billingEmail;

	private String billingFax;

	private String billingPhone;

	private String billingState;

	private String billingTelex;

	private String billingZip;
	private String billingInstruction;

	private Boolean isAccount;

	private Boolean isAgent;

	private Boolean isCarrier;

	private Boolean isVendor;

	private Boolean qc;

	private String billPayType;

	private String billPayOption;

	private String salesMan;

	private String updatedBy;

	private String warehouse;

	private String coordinator;

	private String billingInstructionCode;

	private String billToGroup;


	private String createdBy;

	private Date createdOn;

	
	private Date updatedOn;

	private Date effectiveDate;

	private BigDecimal longPercentage;
	
	private String status;

	private String companyDivision;

	/* ------------------------------------------ */
	
	private String needAuth;

	private String parent;

	private Boolean sea;

	private Boolean surface;

	private Boolean air;

	private Boolean isOwnerOp;

	// Aded after enhansment list for fate 27-02-2008

	private Boolean isPrivateParty;

	private String storageBillingGroup;

	private String typeOfVendor;

	private String accountHolder;

	private String paymentMethod;

	private String payOption;

	private String multiAuthorization;

	private Boolean payableUploadCheck;

	private Boolean invoiceUploadCheck;

	// As on Workflow Related Enhancements April 5 2009
	private String billingUser;

	private String payableUser;

	private String pricingUser;

	private String agentParent;

	private Boolean partnerPortalActive;

	private String partnerPortalId;

	private String associatedAgents;

	private Boolean viewChild;

	// Aded after enhansment list for fate 30-06-2009
	private String creditTerms;

	private String location1;

	private String location2;

	private String location3;

	private String location4;

	private String url;

	private String companyProfile;

	private String trackingUrl;

	private BigDecimal latitude;

	private BigDecimal longitude;

	private String yearEstablished;

	private String companyFacilities;

	private String companyCapabilities;

	private String companyDestiantionProfile;

	private String serviceRangeKms;

	private String serviceRangeMiles;

	private String fidiNumber;

	private String OMNINumber;

	private String IAMNumber;

	private String AMSANumber;

	private String WERCNumber;

	private String facilitySizeSQFT;
	
	private String facilitySizeSQMT;

	private String qualityCertifications;

	private String vanLineAffiliation;
	
	private String serviceLines;
	
	private Boolean accountingDefault;
	
	private String acctDefaultJobType;
	
	private Boolean stopNotAuthorizedInvoices;
	
	private Boolean doNotCopyAuthorizationSO;
	
	private String driverAgency;
	
	private String validNationalCode;
	
	private String cardNumber;
	
	private String cardStatus;
	
	private String accountId;
	
	private String customerId;
	
	private String licenseNumber;
	
	private String licenseState;
	
	private Boolean cardSettelment;
	
	private Boolean directDeposit;
	
	private Boolean fuelPurchase;
	
	private Boolean expressCash;
	
	private Boolean yruAccess;
	
	private String taxId;
	
	private String taxIdType;
	
	private String extReference;

	private Date startDate;
	
	private String accountManager;
	
	private Set<PartnerAccountRef> partnerAccountRef;
	
	private Set<PartnerRates> partnerRatess;
	private String classcode;
	private String vatNumber;
	private String aliasName;
	private Boolean networkGroup;
	private String contactName;
	private String  billingCurrency;
	private String bankCode;
	private String bankAccountNumber;
	private String agentParentName;
	private String collection;
	private Boolean isPartnerExtract=new Boolean(false);
	private String agentGroup;
	private String partnerType;
	private String vatBillingGroup;
	private String agentClassification;
	private String preferedagentType;
	private BigDecimal sellRate = new BigDecimal(0);
	private BigDecimal buyRate = new BigDecimal(0);
	private Boolean doNotInvoice;
	private String utsNumber;
	private String PAIMA ;
	private String LACMA ;
	private String eurovanNetwork;
	private String multipleCompanyDivision;
	private String defaultVat;
	private String privateContactName;
	private String defaultContact;
	// Implementing the toStrig method

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("partnerCode", partnerCode)
				.append("partnerPrefix", partnerPrefix)
				.append("firstName", firstName).append("lastName", lastName)
				.append("middleInitial", middleInitial)
				.append("partnerSuffix", partnerSuffix)
				.append("terminalAddress1", terminalAddress1)
				.append("terminalAddress2", terminalAddress2)
				.append("terminalAddress3", terminalAddress3)
				.append("terminalAddress4", terminalAddress4)
				.append("mailingCountry", mailingCountry)
				.append("mailingTelex", mailingTelex)
				.append("mailingPhone", mailingPhone)
				.append("mailingFax", mailingFax)
				.append("mailingEmail", mailingEmail)
				.append("mailingCountryCode", mailingCountryCode)
				.append("mailingAddress1", mailingAddress1)
				.append("mailingAddress2", mailingAddress2)
				.append("mailingAddress3", mailingAddress3)
				.append("mailingAddress4", mailingAddress4)
				.append("mailingCity", mailingCity)
				.append("mailingState", mailingState)
				.append("mailingZip", mailingZip)
				.append("terminalPhone", terminalPhone)
				.append("terminalTelex", terminalTelex)
				.append("terminalFax", terminalFax)
				.append("terminalCountryCode", terminalCountryCode)
				.append("terminalCountry", terminalCountry)
				.append("terminalCity", terminalCity)
				.append("terminalState", terminalState)
				.append("terminalZip", terminalZip)
				.append("terminalEmail", terminalEmail)
				.append("abbreviation", abbreviation)
				.append("billingCountryCode", billingCountryCode)
				.append("billingAddress1", billingAddress1)
				.append("billingAddress2", billingAddress2)
				.append("billingAddress3", billingAddress3)
				.append("billingAddress4", billingAddress4)
				.append("billingCity", billingCity)
				.append("billingCountry", billingCountry)
				.append("billingEmail", billingEmail)
				.append("billingFax", billingFax)
				.append("billingPhone", billingPhone)
				.append("billingState", billingState)
				.append("billingTelex", billingTelex)
				.append("billingZip", billingZip)
				.append("billingInstruction", billingInstruction)
				.append("isAccount", isAccount).append("isAgent", isAgent)
				.append("isCarrier", isCarrier).append("isVendor", isVendor)
				.append("qc", qc).append("billPayType", billPayType)
				.append("billPayOption", billPayOption)
				.append("salesMan", salesMan).append("updatedBy", updatedBy)
				.append("warehouse", warehouse)
				.append("coordinator", coordinator)
				.append("billingInstructionCode", billingInstructionCode)
				.append("billToGroup", billToGroup)
				.append("createdBy", createdBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("effectiveDate", effectiveDate)
				.append("longPercentage", longPercentage)
				.append("status", status)
				.append("companyDivision", companyDivision)
				.append("needAuth", needAuth).append("parent", parent)
				.append("sea", sea).append("surface", surface)
				.append("air", air).append("isOwnerOp", isOwnerOp)
				.append("isPrivateParty", isPrivateParty)
				.append("storageBillingGroup", storageBillingGroup)
				.append("typeOfVendor", typeOfVendor)
				.append("accountHolder", accountHolder)
				.append("paymentMethod", paymentMethod)
				.append("payOption", payOption)
				.append("multiAuthorization", multiAuthorization)
				.append("payableUploadCheck", payableUploadCheck)
				.append("invoiceUploadCheck", invoiceUploadCheck)
				.append("billingUser", billingUser)
				.append("payableUser", payableUser)
				.append("pricingUser", pricingUser)
				.append("agentParent", agentParent)
				.append("partnerPortalActive", partnerPortalActive)
				.append("partnerPortalId", partnerPortalId)
				.append("associatedAgents", associatedAgents)
				.append("viewChild", viewChild)
				.append("creditTerms", creditTerms)
				.append("location1", location1).append("location2", location2)
				.append("location3", location3).append("location4", location4)
				.append("url", url).append("companyProfile", companyProfile)
				.append("trackingUrl", trackingUrl)
				.append("latitude", latitude).append("longitude", longitude)
				.append("yearEstablished", yearEstablished)
				.append("companyFacilities", companyFacilities)
				.append("companyCapabilities", companyCapabilities)
				.append("companyDestiantionProfile", companyDestiantionProfile)
				.append("serviceRangeKms", serviceRangeKms)
				.append("serviceRangeMiles", serviceRangeMiles)
				.append("fidiNumber", fidiNumber)
				.append("OMNINumber", OMNINumber)
				.append("IAMNumber", IAMNumber)
				.append("AMSANumber", AMSANumber)
				.append("WERCNumber", WERCNumber)
				.append("facilitySizeSQFT", facilitySizeSQFT)
				.append("facilitySizeSQMT", facilitySizeSQMT)
				.append("qualityCertifications", qualityCertifications)
				.append("vanLineAffiliation", vanLineAffiliation)
				.append("serviceLines", serviceLines)
				.append("accountingDefault", accountingDefault)
				.append("acctDefaultJobType", acctDefaultJobType)
				.append("stopNotAuthorizedInvoices", stopNotAuthorizedInvoices)
				.append("doNotCopyAuthorizationSO", doNotCopyAuthorizationSO)
				.append("driverAgency", driverAgency)
				.append("validNationalCode", validNationalCode)
				.append("cardNumber", cardNumber)
				.append("cardStatus", cardStatus)
				.append("accountId", accountId)
				.append("customerId", customerId)
				.append("licenseNumber", licenseNumber)
				.append("licenseState", licenseState)
				.append("cardSettelment", cardSettelment)
				.append("directDeposit", directDeposit)
				.append("fuelPurchase", fuelPurchase)
				.append("expressCash", expressCash)
				.append("yruAccess", yruAccess).append("taxId", taxId)
				.append("taxIdType", taxIdType)
				.append("extReference", extReference)
				.append("startDate", startDate)
				.append("agentGroup", agentGroup) 
				.append("classcode", classcode)
				.append("classcode", classcode).append("vatNumber", vatNumber)
				.append("aliasName", aliasName)
				.append("networkGroup", networkGroup)
				.append("contactName", contactName)
				.append("billingCurrency", billingCurrency)
				.append("bankCode", bankCode)
				.append("bankAccountNumber", bankAccountNumber)
				.append("agentParentName", agentParentName)
				.append("collection",collection).append("isPartnerExtract",isPartnerExtract)
				.append("partnerType",partnerType).append("vatBillingGroup",vatBillingGroup) 
				.append("agentClassification",agentClassification)
				.append("preferedagentType",preferedagentType)
				.append("sellRate",sellRate)
				.append("buyRate",buyRate)
				.append("doNotInvoice",doNotInvoice)
				.append("utsNumber", utsNumber)
				.append("PAIMA",PAIMA)
				.append("LACMA",LACMA)
				.append("eurovanNetwork",eurovanNetwork)
				.append("multipleCompanyDivision", multipleCompanyDivision)
				.append("defaultVat", defaultVat)
				.append("privateContactName", privateContactName)
				.append("defaultContact", defaultContact)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Partner))
			return false;
		Partner castOther = (Partner) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(partnerCode, castOther.partnerCode)
				.append(partnerPrefix, castOther.partnerPrefix)
				.append(firstName, castOther.firstName)
				.append(lastName, castOther.lastName)
				.append(middleInitial, castOther.middleInitial)
				.append(partnerSuffix, castOther.partnerSuffix)
				.append(terminalAddress1, castOther.terminalAddress1)
				.append(terminalAddress2, castOther.terminalAddress2)
				.append(terminalAddress3, castOther.terminalAddress3)
				.append(terminalAddress4, castOther.terminalAddress4)
				.append(mailingCountry, castOther.mailingCountry)
				.append(mailingTelex, castOther.mailingTelex)
				.append(mailingPhone, castOther.mailingPhone)
				.append(mailingFax, castOther.mailingFax)
				.append(mailingEmail, castOther.mailingEmail)
				.append(mailingCountryCode, castOther.mailingCountryCode)
				.append(mailingAddress1, castOther.mailingAddress1)
				.append(mailingAddress2, castOther.mailingAddress2)
				.append(mailingAddress3, castOther.mailingAddress3)
				.append(mailingAddress4, castOther.mailingAddress4)
				.append(mailingCity, castOther.mailingCity)
				.append(mailingState, castOther.mailingState)
				.append(mailingZip, castOther.mailingZip)
				.append(terminalPhone, castOther.terminalPhone)
				.append(terminalTelex, castOther.terminalTelex)
				.append(terminalFax, castOther.terminalFax)
				.append(terminalCountryCode, castOther.terminalCountryCode)
				.append(terminalCountry, castOther.terminalCountry)
				.append(terminalCity, castOther.terminalCity)
				.append(terminalState, castOther.terminalState)
				.append(terminalZip, castOther.terminalZip)
				.append(terminalEmail, castOther.terminalEmail)
				.append(abbreviation, castOther.abbreviation)
				.append(billingCountryCode, castOther.billingCountryCode)
				.append(billingAddress1, castOther.billingAddress1)
				.append(billingAddress2, castOther.billingAddress2)
				.append(billingAddress3, castOther.billingAddress3)
				.append(billingAddress4, castOther.billingAddress4)
				.append(billingCity, castOther.billingCity)
				.append(billingCountry, castOther.billingCountry)
				.append(billingEmail, castOther.billingEmail)
				.append(billingFax, castOther.billingFax)
				.append(billingPhone, castOther.billingPhone)
				.append(billingState, castOther.billingState)
				.append(billingTelex, castOther.billingTelex)
				.append(billingZip, castOther.billingZip)
				.append(billingInstruction, castOther.billingInstruction)
				.append(isAccount, castOther.isAccount)
				.append(isAgent, castOther.isAgent)
				.append(isCarrier, castOther.isCarrier)
				.append(isVendor, castOther.isVendor)
				.append(qc, castOther.qc)
				.append(billPayType, castOther.billPayType)
				.append(billPayOption, castOther.billPayOption)
				.append(salesMan, castOther.salesMan)
				.append(updatedBy, castOther.updatedBy)
				.append(warehouse, castOther.warehouse)
				.append(coordinator, castOther.coordinator)
				.append(billingInstructionCode,
						castOther.billingInstructionCode)
				.append(billToGroup, castOther.billToGroup)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(effectiveDate, castOther.effectiveDate)
				.append(longPercentage, castOther.longPercentage)
				.append(status, castOther.status)
				.append(companyDivision, castOther.companyDivision)
				.append(needAuth, castOther.needAuth)
				.append(parent, castOther.parent)
				.append(sea, castOther.sea)
				.append(surface, castOther.surface)
				.append(air, castOther.air)
				.append(isOwnerOp, castOther.isOwnerOp)
				.append(isPrivateParty, castOther.isPrivateParty)
				.append(storageBillingGroup, castOther.storageBillingGroup)
				.append(typeOfVendor, castOther.typeOfVendor)
				.append(accountHolder, castOther.accountHolder)
				.append(paymentMethod, castOther.paymentMethod)
				.append(payOption, castOther.payOption)
				.append(multiAuthorization, castOther.multiAuthorization)
				.append(payableUploadCheck, castOther.payableUploadCheck)
				.append(invoiceUploadCheck, castOther.invoiceUploadCheck)
				.append(billingUser, castOther.billingUser)
				.append(payableUser, castOther.payableUser)
				.append(pricingUser, castOther.pricingUser)
				.append(agentParent, castOther.agentParent)
				.append(partnerPortalActive, castOther.partnerPortalActive)
				.append(partnerPortalId, castOther.partnerPortalId)
				.append(associatedAgents, castOther.associatedAgents)
				.append(viewChild, castOther.viewChild)
				.append(creditTerms, castOther.creditTerms)
				.append(location1, castOther.location1)
				.append(location2, castOther.location2)
				.append(location3, castOther.location3)
				.append(location4, castOther.location4)
				.append(url, castOther.url)
				.append(companyProfile, castOther.companyProfile)
				.append(trackingUrl, castOther.trackingUrl)
				.append(latitude, castOther.latitude)
				.append(longitude, castOther.longitude)
				.append(yearEstablished, castOther.yearEstablished)
				.append(companyFacilities, castOther.companyFacilities)
				.append(companyCapabilities, castOther.companyCapabilities)
				.append(companyDestiantionProfile,
						castOther.companyDestiantionProfile)
				.append(serviceRangeKms, castOther.serviceRangeKms)
				.append(serviceRangeMiles, castOther.serviceRangeMiles)
				.append(fidiNumber, castOther.fidiNumber)
				.append(OMNINumber, castOther.OMNINumber)
				.append(IAMNumber, castOther.IAMNumber)
				.append(AMSANumber, castOther.AMSANumber)
				.append(WERCNumber, castOther.WERCNumber)
				.append(facilitySizeSQFT, castOther.facilitySizeSQFT)
				.append(facilitySizeSQMT, castOther.facilitySizeSQMT)
				.append(qualityCertifications, castOther.qualityCertifications)
				.append(vanLineAffiliation, castOther.vanLineAffiliation)
				.append(serviceLines, castOther.serviceLines)
				.append(accountingDefault, castOther.accountingDefault)
				.append(acctDefaultJobType, castOther.acctDefaultJobType)
				.append(stopNotAuthorizedInvoices,
						castOther.stopNotAuthorizedInvoices)
				.append(doNotCopyAuthorizationSO,
						castOther.doNotCopyAuthorizationSO)
				.append(driverAgency, castOther.driverAgency)
				.append(validNationalCode, castOther.validNationalCode)
				.append(cardNumber, castOther.cardNumber)
				.append(cardStatus, castOther.cardStatus)
				.append(accountId, castOther.accountId)
				.append(customerId, castOther.customerId)
				.append(licenseNumber, castOther.licenseNumber)
				.append(licenseState, castOther.licenseState)
				.append(cardSettelment, castOther.cardSettelment)
				.append(directDeposit, castOther.directDeposit)
				.append(fuelPurchase, castOther.fuelPurchase)
				.append(expressCash, castOther.expressCash)
				.append(yruAccess, castOther.yruAccess)
				.append(taxId, castOther.taxId)
				.append(taxIdType, castOther.taxIdType)
				.append(extReference, castOther.extReference)
				.append(startDate, castOther.startDate)
				.append(accountManager, castOther.accountManager) 
				 
				.append(classcode, castOther.classcode)
				.append(vatNumber, castOther.vatNumber)
				.append(aliasName, castOther.aliasName)
				.append(networkGroup, castOther.networkGroup)
				.append(contactName, castOther.contactName)
				.append(billingCurrency, castOther.billingCurrency)
				.append(bankCode, castOther.bankCode)
				.append(bankAccountNumber, castOther.bankAccountNumber)
				.append(agentParentName, castOther.agentParentName)
				.append(isPartnerExtract, castOther.isPartnerExtract)
				.append(agentGroup, castOther.agentGroup)
				.append(collection, castOther.collection)
				.append(partnerType, castOther.partnerType)
				.append(vatBillingGroup, castOther.vatBillingGroup)
				.append(agentClassification,castOther.agentClassification)
				.append(preferedagentType,castOther.preferedagentType)
				.append(sellRate,castOther.sellRate)
				.append(buyRate,castOther.buyRate)
				.append(doNotInvoice, castOther.doNotInvoice)
				.append(utsNumber, castOther.utsNumber)
				.append(PAIMA, castOther.PAIMA)
				.append(LACMA, castOther.LACMA).append(eurovanNetwork, castOther.eurovanNetwork)
				.append(multipleCompanyDivision, castOther.multipleCompanyDivision)
				.append(defaultVat, castOther.defaultVat)
				.append(privateContactName, castOther.privateContactName)
				.append(defaultContact, castOther.defaultContact)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(partnerCode).append(partnerPrefix).append(firstName)
				.append(lastName).append(middleInitial).append(partnerSuffix)
				.append(terminalAddress1).append(terminalAddress2)
				.append(terminalAddress3).append(terminalAddress4)
				.append(mailingCountry).append(mailingTelex)
				.append(mailingPhone).append(mailingFax).append(mailingEmail)
				.append(mailingCountryCode).append(mailingAddress1)
				.append(mailingAddress2).append(mailingAddress3)
				.append(mailingAddress4).append(mailingCity)
				.append(mailingState).append(mailingZip).append(terminalPhone)
				.append(terminalTelex).append(terminalFax)
				.append(terminalCountryCode).append(terminalCountry)
				.append(terminalCity).append(terminalState).append(terminalZip)
				.append(terminalEmail).append(abbreviation)
				.append(billingCountryCode).append(billingAddress1)
				.append(billingAddress2).append(billingAddress3)
				.append(billingAddress4).append(billingCity)
				.append(billingCountry).append(billingEmail).append(billingFax)
				.append(billingPhone).append(billingState).append(billingTelex)
				.append(billingZip).append(billingInstruction)
				.append(isAccount).append(isAgent).append(isCarrier)
				.append(isVendor).append(qc).append(billPayType)
				.append(billPayOption).append(salesMan).append(updatedBy)
				.append(warehouse).append(coordinator)
				.append(billingInstructionCode).append(billToGroup)
				.append(createdBy).append(createdOn).append(updatedOn)
				.append(effectiveDate).append(longPercentage).append(status)
				.append(companyDivision).append(needAuth).append(parent)
				.append(sea).append(surface).append(air).append(isOwnerOp)
				.append(isPrivateParty).append(storageBillingGroup)
				.append(typeOfVendor).append(accountHolder)
				.append(paymentMethod).append(payOption)
				.append(multiAuthorization).append(payableUploadCheck)
				.append(invoiceUploadCheck).append(billingUser)
				.append(payableUser).append(pricingUser).append(agentParent)
				.append(partnerPortalActive).append(partnerPortalId)
				.append(associatedAgents).append(viewChild).append(creditTerms)
				.append(location1).append(location2).append(location3)
				.append(location4).append(url).append(companyProfile)
				.append(trackingUrl).append(latitude).append(longitude)
				.append(yearEstablished).append(companyFacilities)
				.append(companyCapabilities).append(companyDestiantionProfile)
				.append(serviceRangeKms).append(serviceRangeMiles)
				.append(fidiNumber).append(OMNINumber).append(IAMNumber)
				.append(AMSANumber).append(WERCNumber).append(facilitySizeSQFT)
				.append(facilitySizeSQMT).append(qualityCertifications)
				.append(vanLineAffiliation).append(serviceLines)
				.append(accountingDefault).append(acctDefaultJobType)
				.append(stopNotAuthorizedInvoices)
				.append(doNotCopyAuthorizationSO).append(driverAgency)
				.append(validNationalCode).append(cardNumber)
				.append(cardStatus).append(accountId).append(customerId)
				.append(licenseNumber).append(licenseState)
				.append(cardSettelment).append(directDeposit)
				.append(fuelPurchase).append(expressCash).append(yruAccess)
				.append(taxId).append(taxIdType).append(extReference)
				.append(startDate).append(accountManager) 
				.append(classcode).append(vatNumber).append(aliasName)
				.append(networkGroup).append(contactName)
				.append(billingCurrency).append(bankCode)
				.append(bankAccountNumber).append(bankAccountNumber)
				.append(agentParentName).append(agentParentName)
				.append(collection).append(isPartnerExtract).append(agentGroup).append(partnerType)
				.append(vatBillingGroup)
				.append(agentClassification)
				.append(preferedagentType)
				.append(sellRate)
				.append(buyRate)
				.append(doNotInvoice)
				.append(utsNumber)
				.append(LACMA).append(PAIMA).append(eurovanNetwork).append(multipleCompanyDivision)
				.append(defaultVat).append(privateContactName).append(defaultContact)
				.toHashCode();
	}

	@OneToMany(mappedBy = "partner", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public Set<PartnerRates> getPartnerRatess() {
		return partnerRatess;
	}

	public void setPartnerRatess(Set<PartnerRates> partnerRatess) {
		this.partnerRatess = partnerRatess;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 15)
	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	@Column
	public String getBillingAddress1() {
		return billingAddress1;
	}

	public void setBillingAddress1(String billingAddress1) {
		this.billingAddress1 = billingAddress1;
	}

	@Column
	public String getBillingAddress2() {
		return billingAddress2;
	}

	public void setBillingAddress2(String billingAddress2) {
		this.billingAddress2 = billingAddress2;
	}

	@Column
	public String getBillingAddress3() {
		return billingAddress3;
	}

	public void setBillingAddress3(String billingAddress3) {
		this.billingAddress3 = billingAddress3;
	}

	@Column
	public String getBillingAddress4() {
		return billingAddress4;
	}

	public void setBillingAddress4(String billingAddress4) {
		this.billingAddress4 = billingAddress4;
	}

	@Column(length = 20)
	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	@Column(length = 45)
	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	@Column(length = 3)
	public String getBillingCountryCode() {
		return billingCountryCode;
	}

	public void setBillingCountryCode(String billingCountryCode) {
		this.billingCountryCode = billingCountryCode;
	}

	@Column(length = 70)
	public String getBillingEmail() {
		return billingEmail;
	}

	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}

	@Column(length = 20)
	public String getBillingFax() {
		return billingFax;
	}

	public void setBillingFax(String billingFax) {
		this.billingFax = billingFax;
	}

	@Column(length = 150)
	public String getBillingInstruction() {
		return billingInstruction;
	}

	public void setBillingInstruction(String billingInstruction) {
		this.billingInstruction = billingInstruction;
	}

	@Column(length = 7)
	public String getBillingInstructionCode() {
		return billingInstructionCode;
	}

	public void setBillingInstructionCode(String billingInstructionCode) {
		this.billingInstructionCode = billingInstructionCode;
	}

	@Column(length = 100)
	public String getBillingPhone() {
		return billingPhone;
	}

	public void setBillingPhone(String billingPhone) {
		this.billingPhone = billingPhone;
	}

	@Column(length = 2)
	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	@Column(length = 20)
	public String getBillingTelex() {
		return billingTelex;
	}

	public void setBillingTelex(String billingTelex) {
		this.billingTelex = billingTelex;
	}

	@Column(length = 50)
	public String getBillingZip() {
		return billingZip;
	}

	public void setBillingZip(String billingZip) {
		this.billingZip = billingZip;
	}

	@Column(length = 3)
	public String getBillPayOption() {
		return billPayOption;
	}

	public void setBillPayOption(String billPayOption) {
		this.billPayOption = billPayOption;
	}

	@Column(length = 3)
	public String getBillPayType() {
		return billPayType;
	}

	public void setBillPayType(String billPayType) {
		this.billPayType = billPayType;
	}
	
	@Column(length = 15)
	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}


	@Column
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	@Column(length = 25)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(length = 15)
	public String getBillToGroup() {
		return billToGroup;
	}

	public void setBillToGroup(String billToGroup) {
		this.billToGroup = billToGroup;
	}

	@Column
	public Boolean getIsAccount() {
		return isAccount;
	}

	public void setIsAccount(Boolean isAccount) {
		this.isAccount = isAccount;
	}

	@Column
	public Boolean getIsAgent() {
		return isAgent;
	}

	public void setIsAgent(Boolean isAgent) {
		this.isAgent = isAgent;
	}

	@Column
	public Boolean getIsCarrier() {
		return isCarrier;
	}

	public void setIsCarrier(Boolean isCarrier) {
		this.isCarrier = isCarrier;
	}

	@Column
	public Boolean getIsVendor() {
		return isVendor;
	}

	public void setIsVendor(Boolean isVendor) {
		this.isVendor = isVendor;
	}

	

	@Column
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(nullable = true, precision = 15, scale = 5)
	public BigDecimal getLongPercentage() {
		return longPercentage;
	}

	public void setLongPercentage(BigDecimal longPercentage) {
		this.longPercentage = longPercentage;
	}

	@Column
	public String getMailingAddress1() {
		return mailingAddress1;
	}

	public void setMailingAddress1(String mailingAddress1) {
		this.mailingAddress1 = mailingAddress1;
	}

	@Column
	public String getMailingAddress2() {
		return mailingAddress2;
	}

	public void setMailingAddress2(String mailingAddress2) {
		this.mailingAddress2 = mailingAddress2;
	}

	@Column
	public String getMailingAddress3() {
		return mailingAddress3;
	}

	public void setMailingAddress3(String mailingAddress3) {
		this.mailingAddress3 = mailingAddress3;
	}

	@Column
	public String getMailingAddress4() {
		return mailingAddress4;
	}

	public void setMailingAddress4(String mailingAddress4) {
		this.mailingAddress4 = mailingAddress4;
	}

	@Column(length = 50)
	public String getMailingCity() {
		return mailingCity;
	}

	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}

	@Column(length = 45)
	public String getMailingCountry() {
		return mailingCountry;
	}

	public void setMailingCountry(String mailingCountry) {
		this.mailingCountry = mailingCountry;
	}

	@Column(length = 3)
	public String getMailingCountryCode() {
		return mailingCountryCode;
	}

	public void setMailingCountryCode(String mailingCountryCode) {
		this.mailingCountryCode = mailingCountryCode;
	}

	@Column(length = 65)
	public String getMailingEmail() {
		return mailingEmail;
	}

	public void setMailingEmail(String mailingEmail) {
		this.mailingEmail = mailingEmail;
	}

	@Column(length = 20)
	public String getMailingFax() {
		return mailingFax;
	}

	public void setMailingFax(String mailingFax) {
		this.mailingFax = mailingFax;
	}

	@Column(length = 100)
	public String getMailingPhone() {
		return mailingPhone;
	}

	public void setMailingPhone(String mailingPhone) {
		this.mailingPhone = mailingPhone;
	}

	@Column(length = 2)
	public String getMailingState() {
		return mailingState;
	}

	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}

	@Column(length = 20)
	public String getMailingTelex() {
		return mailingTelex;
	}

	public void setMailingTelex(String mailingTelex) {
		this.mailingTelex = mailingTelex;
	}

	@Column(length = 50)
	public String getMailingZip() {
		return mailingZip;
	}

	public void setMailingZip(String mailingZip) {
		this.mailingZip = mailingZip;
	}

	@Column(length = 1)
	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	@Column(length = 1)
	public String getNeedAuth() {
		return needAuth;
	}

	public void setNeedAuth(String needAuth) {
		this.needAuth = needAuth;
	}

	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 7)
	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	@Column(length = 8)
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	@Column(length = 10)
	public String getPartnerPrefix() {
		return partnerPrefix;
	}

	public void setPartnerPrefix(String partnerPrefix) {
		this.partnerPrefix = partnerPrefix;
	}

	@Column
	public Boolean getQc() {
		return qc;
	}

	public void setQc(Boolean qc) {
		this.qc = qc;
	}
	@Column(length = 15)
	public String getSalesMan() {
		return salesMan;
	}

	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}

	@Column(length = 10)
	public String getPartnerSuffix() {
		return partnerSuffix;
	}

	public void setPartnerSuffix(String partnerSuffix) {
		this.partnerSuffix = partnerSuffix;
	}

	@Column
	public String getTerminalAddress1() {
		return terminalAddress1;
	}

	public void setTerminalAddress1(String terminalAddress1) {
		this.terminalAddress1 = terminalAddress1;
	}

	@Column
	public String getTerminalAddress2() {
		return terminalAddress2;
	}

	public void setTerminalAddress2(String terminalAddress2) {
		this.terminalAddress2 = terminalAddress2;
	}

	@Column
	public String getTerminalAddress3() {
		return terminalAddress3;
	}

	public void setTerminalAddress3(String terminalAddress3) {
		this.terminalAddress3 = terminalAddress3;
	}

	@Column
	public String getTerminalAddress4() {
		return terminalAddress4;
	}

	public void setTerminalAddress4(String terminalAddress4) {
		this.terminalAddress4 = terminalAddress4;
	}

	@Column(length = 50)
	public String getTerminalCity() {
		return terminalCity;
	}

	public void setTerminalCity(String terminalCity) {
		this.terminalCity = terminalCity;
	}

	@Column(length = 45)
	public String getTerminalCountry() {
		return terminalCountry;
	}

	public void setTerminalCountry(String terminalCountry) {
		this.terminalCountry = terminalCountry;
	}

	@Column(length = 3)
	public String getTerminalCountryCode() {
		return terminalCountryCode;
	}

	public void setTerminalCountryCode(String terminalCountryCode) {
		this.terminalCountryCode = terminalCountryCode;
	}

	@Column(length = 70)
	public String getTerminalEmail() {
		return terminalEmail;
	}

	public void setTerminalEmail(String terminalEmail) {
		this.terminalEmail = terminalEmail;
	}

	@Column(length = 20)
	public String getTerminalFax() {
		return terminalFax;
	}

	public void setTerminalFax(String terminalFax) {
		this.terminalFax = terminalFax;
	}
	@Column(length = 100)
	public String getTerminalPhone() {
		return terminalPhone;
	}

	public void setTerminalPhone(String terminalPhone) {
		this.terminalPhone = terminalPhone;
	}

	@Column(length = 2)
	public String getTerminalState() {
		return terminalState;
	}

	public void setTerminalState(String terminalState) {
		this.terminalState = terminalState;
	}

	@Column(length = 20)
	public String getTerminalTelex() {
		return terminalTelex;
	}

	public void setTerminalTelex(String terminalTelex) {
		this.terminalTelex = terminalTelex;
	}

	@Column(length = 50)
	public String getTerminalZip() {
		return terminalZip;
	}

	public void setTerminalZip(String terminalZip) {
		this.terminalZip = terminalZip;
	}

	@Column(length = 25)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(length = 2)
	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	@Column(length = 25)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Boolean getIsPrivateParty() {
		return isPrivateParty;
	}

	public void setIsPrivateParty(Boolean isPrivateParty) {
		this.isPrivateParty = isPrivateParty;
	}

	@Column
	public Boolean getAir() {
		return air;
	}

	public void setAir(Boolean air) {
		this.air = air;
	}

	@Column
	public Boolean getSea() {
		return sea;
	}

	public void setSea(Boolean sea) {
		this.sea = sea;
	}

	@Column
	public Boolean getSurface() {
		return surface;
	}

	public void setSurface(Boolean surface) {
		this.surface = surface;
	}

	@Column
	public Boolean getIsOwnerOp() {
		return isOwnerOp;
	}

	public void setIsOwnerOp(Boolean isOwnerOp) {
		this.isOwnerOp = isOwnerOp;
	}

	@Transient
	public String getListCode() {
		return partnerCode;
	}

	@Transient
	public String getListDescription() {
		String lName = this.lastName;
		try{
			lName=URLEncoder.encode(lName, "UTF-8");
		     }catch(Exception e){}
		return lName;
	}

	@Transient
	public String getListSecondDescription() {
		String billingStateForPopup;
		String billingCountryCodeForPopup;
		if (this.billingCity == null) {
			this.billingCity = "";
		}
		if (this.billingState == null) {
			billingStateForPopup = "";
		} else {
			billingStateForPopup = "," + this.billingState;
		}
		if (this.billingCountryCode == null) {
			billingCountryCodeForPopup = "";
		} else {
			billingCountryCodeForPopup = "," + this.billingCountryCode;
		}
		String s = this.billingCity + billingStateForPopup
				+ billingCountryCodeForPopup;
		s = s.trim();
		if (s.indexOf(",") == (0 & s.lastIndexOf(","))) {
			s = s.replaceAll(",", "");
		}
		s = s.replaceAll(",,", ",");
		s = s.replaceAll(",,,", ",");
		return s;
	}

	@Transient
	public String getListThirdDescription() {
		return this.coordinator;
	}

	@Transient
	public String getListFourthDescription() {
		String mailingPhone = this.mailingPhone;
		 if (mailingPhone == null) {
			 mailingPhone = "";
		   }		
		 mailingPhone= mailingPhone.replaceAll("#", "");
		return mailingPhone; 
				
	}

	@Transient
	public String getListFifthDescription() {
		String termEmail = this.terminalEmail;
		if (termEmail == null) {
			termEmail = "";
		}
		return termEmail;
	}

	@Transient
	public String getListSixthDescription() {
		String lName = this.lastName.replaceAll("&", "%26");
		lName = lName.replaceAll("#", "%23");
		lName = lName.replaceAll("\"", "%22");
		return lName;

	}

	@Column(length = 50)
	public String getStorageBillingGroup() {
		return storageBillingGroup;
	}

	public void setStorageBillingGroup(String storageBillingGroup) {
		this.storageBillingGroup = storageBillingGroup;
	}

	@Column(length = 50)
	public String getTypeOfVendor() {
		return typeOfVendor;
	}

	public void setTypeOfVendor(String typeOfVendor) {
		this.typeOfVendor = typeOfVendor;
	}

	@Column(length = 50)
	public String getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	@Column(length = 50)
	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	@Column(length = 50)
	public String getPayOption() {
		return payOption;
	}

	public void setPayOption(String payOption) {
		this.payOption = payOption;
	}

	@Column(length = 5)
	public String getMultiAuthorization() {
		return multiAuthorization;
	}

	public void setMultiAuthorization(String multiAuthorization) {
		this.multiAuthorization = multiAuthorization;
	}

	@Column(length = 20)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column
	public Boolean getPayableUploadCheck() {
		return payableUploadCheck;
	}

	public void setPayableUploadCheck(Boolean payableUploadCheck) {
		this.payableUploadCheck = payableUploadCheck;
	}

	/**
	 * @return the companyDivision
	 */
	@Column(length = 10)
	public String getCompanyDivision() {
		return companyDivision;
	}

	/**
	 * @param companyDivision
	 *            the companyDivision to set
	 */
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	@Column
	public Boolean getInvoiceUploadCheck() {
		return invoiceUploadCheck;
	}

	public void setInvoiceUploadCheck(Boolean invoiceUploadCheck) {
		this.invoiceUploadCheck = invoiceUploadCheck;
	}

	/**
	 * @return the billingUser
	 */
	@Column(length = 82)
	public String getBillingUser() {
		return billingUser;
	}

	/**
	 * @param billingUser
	 *            the billingUser to set
	 */
	public void setBillingUser(String billingUser) {
		this.billingUser = billingUser;
	}

	/**
	 * @return the payableUser
	 */
	@Column(length = 82)
	public String getPayableUser() {
		return payableUser;
	}

	/**
	 * @param payableUser
	 *            the payableUser to set
	 */
	public void setPayableUser(String payableUser) {
		this.payableUser = payableUser;
	}

	/**
	 * @return the pricingUser
	 */
	@Column(length = 82)
	public String getPricingUser() {
		return pricingUser;
	}

	/**
	 * @param pricingUser
	 *            the pricingUser to set
	 */
	public void setPricingUser(String pricingUser) {
		this.pricingUser = pricingUser;
	}

	@Column(length = 8)
	public String getAgentParent() {
		return agentParent;
	}

	public void setAgentParent(String agentParent) {
		this.agentParent = agentParent;
	}

	@Column
	public Boolean getPartnerPortalActive() {
		return partnerPortalActive;
	}

	public void setPartnerPortalActive(Boolean partnerPortalActive) {
		this.partnerPortalActive = partnerPortalActive;
	}

	@Column
	public String getPartnerPortalId() {
		return partnerPortalId;
	}

	public void setPartnerPortalId(String partnerPortalId) {
		this.partnerPortalId = partnerPortalId;
	}

	@Column
	public String getAssociatedAgents() {
		return associatedAgents;
	}

	public void setAssociatedAgents(String associatedAgents) {
		this.associatedAgents = associatedAgents;
	}

	@Column
	public Boolean getViewChild() {
		return viewChild;
	}

	public void setViewChild(Boolean viewChild) {
		this.viewChild = viewChild;
	}

	@Column(length = 10)
	public String getCreditTerms() {
		return creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public String getLocation1() {
		return location1;
	}

	public void setLocation1(String location1) {
		this.location1 = location1;
	}

	public String getLocation2() {
		return location2;
	}

	public void setLocation2(String location2) {
		this.location2 = location2;
	}

	public String getLocation3() {
		return location3;
	}

	public void setLocation3(String location3) {
		this.location3 = location3;
	}

	public String getLocation4() {
		return location4;
	}

	public void setLocation4(String location4) {
		this.location4 = location4;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(length = 1500)
	public String getCompanyProfile() {
		return companyProfile;
	}

	public void setCompanyProfile(String companyProfile) {
		this.companyProfile = companyProfile;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	@Column
	public String getAMSANumber() {
		return AMSANumber;
	}

	public void setAMSANumber(String number) {
		AMSANumber = number;
	}

	

	@Column
	public String getFidiNumber() {
		return fidiNumber;
	}

	public void setFidiNumber(String fidiNumber) {
		this.fidiNumber = fidiNumber;
	}

	@Column
	public String getIAMNumber() {
		return IAMNumber;
	}

	public void setIAMNumber(String number) {
		IAMNumber = number;
	}

	@Column
	public String getOMNINumber() {
		return OMNINumber;
	}

	public void setOMNINumber(String number) {
		OMNINumber = number;
	}

	@Column
	public String getQualityCertifications() {
		return qualityCertifications;
	}

	public void setQualityCertifications(String qualityCertifications) {
		this.qualityCertifications = qualityCertifications;
	}

	@Column
	public String getServiceRangeKms() {
		return serviceRangeKms;
	}

	public void setServiceRangeKms(String serviceRangeKms) {
		this.serviceRangeKms = serviceRangeKms;
	}

	@Column
	public String getServiceRangeMiles() {
		return serviceRangeMiles;
	}

	public void setServiceRangeMiles(String serviceRangeMiles) {
		this.serviceRangeMiles = serviceRangeMiles;
	}

	@Column
	public String getWERCNumber() {
		return WERCNumber;
	}

	public void setWERCNumber(String number) {
		WERCNumber = number;
	}

	@Column
	public String getYearEstablished() {
		return yearEstablished;
	}

	public void setYearEstablished(String yearEstablished) {
		this.yearEstablished = yearEstablished;
	}

	@Column(length = 1500)
	public String getCompanyCapabilities() {
		return companyCapabilities;
	}

	public void setCompanyCapabilities(String companyCapabilities) {
		this.companyCapabilities = companyCapabilities;
	}

	@Column(length = 1500)
	public String getCompanyDestiantionProfile() {
		return companyDestiantionProfile;
	}

	public void setCompanyDestiantionProfile(String companyDestiantionProfile) {
		this.companyDestiantionProfile = companyDestiantionProfile;
	}

	@Column(length = 1500)
	public String getCompanyFacilities() {
		return companyFacilities;
	}

	public void setCompanyFacilities(String companyFacilities) {
		this.companyFacilities = companyFacilities;
	}

	@Column
	public String getServiceLines() {
		return serviceLines;
	}

	public void setServiceLines(String serviceLines) {
		this.serviceLines = serviceLines;
	}

	@Column
	public String getVanLineAffiliation() {
		return vanLineAffiliation;
	}

	public void setVanLineAffiliation(String vanLineAffiliation) {
		this.vanLineAffiliation = vanLineAffiliation;
	}
	@Column
	public String getTrackingUrl() {
		return trackingUrl;
	}

	public void setTrackingUrl(String trackingUrl) {
		this.trackingUrl = trackingUrl;
	}
	@Column
	public String getFacilitySizeSQFT() {
		return facilitySizeSQFT;
	}

	public void setFacilitySizeSQFT(String facilitySizeSQFT) {
		this.facilitySizeSQFT = facilitySizeSQFT;
	}
	@Column
	public String getFacilitySizeSQMT() {
		return facilitySizeSQMT;
	}

	public void setFacilitySizeSQMT(String facilitySizeSQMT) {
		this.facilitySizeSQMT = facilitySizeSQMT;
	}
	
	@OneToMany(mappedBy = "partner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<PartnerAccountRef> getPartnerAccountRef() {
		return partnerAccountRef;
	}
	
	public void setPartnerAccountRef(Set<PartnerAccountRef> partnerAccountRef) {
		this.partnerAccountRef = partnerAccountRef;
	}
	@Column
	public Boolean getAccountingDefault() {
		return accountingDefault;
	}

	public void setAccountingDefault(Boolean accountingDefault) {
		this.accountingDefault = accountingDefault;
	}
	@Column(length = 50)
	public String getAcctDefaultJobType() {
		return acctDefaultJobType;
	}

	public void setAcctDefaultJobType(String acctDefaultJobType) {
		this.acctDefaultJobType = acctDefaultJobType;
	}
	@Transient
	public String getListEigthDescription() {
		String validNationalCode = this.validNationalCode;
		if (validNationalCode == null) {
			validNationalCode = "";
		}
		return validNationalCode;
		
	}
	@Transient
	public String getListNinthDescription() {
		String billingPhone=this.billingPhone;
		if (billingPhone == null) {
			billingPhone = "";
		}
		return billingPhone;
	}
	@Transient
	public String getListTenthDescription() {
		String lName = this.lastName.replaceAll("&", "%26");
		lName = lName.replaceAll("#", "%23");
		lName = lName.replaceAll("\"", "%22");
		String fName = this.firstName.replaceAll("&", "%26");
		fName = fName.replaceAll("#", "%23");
		fName = fName.replaceAll("\"", "%22");
		return fName+" "+lName;
	}
	@Transient
	public String getListSeventhDescription() {
		String billemail=this.billingEmail;
		if (billemail == null) {
			billemail = "";
		}
		return billemail;	
	}

	@Column
	public Boolean getDoNotCopyAuthorizationSO() {
		return doNotCopyAuthorizationSO;
	}

	public void setDoNotCopyAuthorizationSO(Boolean doNotCopyAuthorizationSO) {
		this.doNotCopyAuthorizationSO = doNotCopyAuthorizationSO;
	}
	@Column
	public Boolean getStopNotAuthorizedInvoices() {
		return stopNotAuthorizedInvoices;
	}

	public void setStopNotAuthorizedInvoices(Boolean stopNotAuthorizedInvoices) {
		this.stopNotAuthorizedInvoices = stopNotAuthorizedInvoices;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Boolean getCardSettelment() {
		return cardSettelment;
	}

	public void setCardSettelment(Boolean cardSettelment) {
		this.cardSettelment = cardSettelment;
	}

	public String getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Boolean getDirectDeposit() {
		return directDeposit;
	}

	public void setDirectDeposit(Boolean directDeposit) {
		this.directDeposit = directDeposit;
	}

	public Boolean getExpressCash() {
		return expressCash;
	}

	public void setExpressCash(Boolean expressCash) {
		this.expressCash = expressCash;
	}

	public Boolean getFuelPurchase() {
		return fuelPurchase;
	}

	public void setFuelPurchase(Boolean fuelPurchase) {
		this.fuelPurchase = fuelPurchase;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getLicenseState() {
		return licenseState;
	}

	public void setLicenseState(String licenseState) {
		this.licenseState = licenseState;
	}

	public Boolean getYruAccess() {
		return yruAccess;
	}

	public void setYruAccess(Boolean yruAccess) {
		this.yruAccess = yruAccess;
	}

	public String getDriverAgency() {
		return driverAgency;
	}

	public void setDriverAgency(String driverAgency) {
		this.driverAgency = driverAgency;
	}

	public String getValidNationalCode() {
		return validNationalCode;
	}

	public void setValidNationalCode(String validNationalCode) {
		this.validNationalCode = validNationalCode;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getTaxIdType() {
		return taxIdType;
	}

	public void setTaxIdType(String taxIdType) {
		this.taxIdType = taxIdType;
	}

	public String getExtReference() {
		return extReference;
	}

	public void setExtReference(String extReference) {
		this.extReference = extReference;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getAccountManager() {
		return accountManager;
	}

	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}
	@Column(length=25)
	public String getClasscode() {
		return classcode;
	}

	public void setClasscode(String classcode) {
		this.classcode = classcode;
	}
	@Column(length=25)
	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
	@Column
	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public Boolean getNetworkGroup() {
		return networkGroup;
	}

	public void setNetworkGroup(Boolean networkGroup) {
		this.networkGroup = networkGroup;
	}

	@Column
	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	
	@Column
	public String getBillingCurrency() {
		return billingCurrency;
	}

	public void setBillingCurrency(String billingCurrency) {
		this.billingCurrency = billingCurrency;
	}

	@Column
	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	@Column
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	@Column
	public String getAgentParentName() {
		return agentParentName;
	}

	public void setAgentParentName(String agentParentName) {
		this.agentParentName = agentParentName;
	}
	@Column
	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}
	@Column
	public Boolean getIsPartnerExtract() {
		return isPartnerExtract;
	}

	public void setIsPartnerExtract(Boolean isPartnerExtract) {
		this.isPartnerExtract = isPartnerExtract;
	}
	@Column
	public String getAgentGroup() {
		return agentGroup;
	}

	public void setAgentGroup(String agentGroup) {
		this.agentGroup = agentGroup;
	}
	@Column
	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	@Column
	public String getVatBillingGroup() {
		return vatBillingGroup;
	}

	public void setVatBillingGroup(String vatBillingGroup) {
		this.vatBillingGroup = vatBillingGroup;
	}
	@Column
	public String getAgentClassification() {
		return agentClassification;
	}

	public void setAgentClassification(String agentClassification) {
		this.agentClassification = agentClassification;
	}
	@Transient
	public String getPreferedagentType() {
		return preferedagentType;
	}

	public void setPreferedagentType(String preferedagentType) {
		this.preferedagentType = preferedagentType;
	}
	@Column
	public BigDecimal getSellRate() {
		return sellRate;
	}

	public void setSellRate(BigDecimal sellRate) {
		this.sellRate = sellRate;
	}
	@Column
	public BigDecimal getBuyRate() {
		return buyRate;
	}

	public void setBuyRate(BigDecimal buyRate) {
		this.buyRate = buyRate;
	}

	public Boolean getDoNotInvoice() {
		return doNotInvoice;
	}

	public void setDoNotInvoice(Boolean doNotInvoice) {
		this.doNotInvoice = doNotInvoice;
	}

	@Column
	public String getUtsNumber() {
		return utsNumber;
	}

	public void setUtsNumber(String utsNumber) {
		this.utsNumber = utsNumber;
	}

	public String getPAIMA() {
		return PAIMA;
	}

	public void setPAIMA(String pAIMA) {
		PAIMA = pAIMA;
	}

	public String getLACMA() {
		return LACMA;
	}

	public void setLACMA(String lACMA) {
		LACMA = lACMA;
	}

	public String getEurovanNetwork() {
		return eurovanNetwork;
	}

	public void setEurovanNetwork(String eurovanNetwork) {
		this.eurovanNetwork = eurovanNetwork;
	}

	public String getMultipleCompanyDivision() {
		return multipleCompanyDivision;
	}

	public void setMultipleCompanyDivision(String multipleCompanyDivision) {
		this.multipleCompanyDivision = multipleCompanyDivision;
	}

	/**
	 * @return the defaultVat
	 */
	public String getDefaultVat() {
		return defaultVat;
	}

	/**
	 * @param defaultVat the defaultVat to set
	 */
	public void setDefaultVat(String defaultVat) {
		this.defaultVat = defaultVat;
	}

	/**
	 * @return the privateContactName
	 */
	public String getPrivateContactName() {
		return privateContactName;
	}

	/**
	 * @param privateContactName the privateContactName to set
	 */
	public void setPrivateContactName(String privateContactName) {
		this.privateContactName = privateContactName;
	}

	/**
	 * @return the defaultContact
	 */
	public String getDefaultContact() {
		return defaultContact;
	}

	/**
	 * @param defaultContact the defaultContact to set
	 */
	public void setDefaultContact(String defaultContact) {
		this.defaultContact = defaultContact;
	}
	
}
