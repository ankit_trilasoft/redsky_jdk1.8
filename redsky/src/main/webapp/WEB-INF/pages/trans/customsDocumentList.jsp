<%@ include file="/common/taglibs.jsp"%> 

<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
<title><fmt:message key="customDocList.customDocList" /></title>
<meta name="heading"
	content="<fmt:message key='customDocumentsList.customDocList'/>" />
	<script language="javascript" type="text/javascript">
  function clear_fields(){
  
			    
			    document.forms['searchForm'].elements['customDocCode'].value = "";
			    document.forms['searchForm'].elements['customDocDescription'].value = "";
		        //document.forms['searchForm'].elements['refMaster.parameter'].value = "";
}
  </script>
<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>
</head>
  <s:form id="searchForm" action="searchCustomsDoc" method="post" >
<div id="layer1" style="width: 100%;">
<s:hidden name="parameter" value="<%=request.getParameter("parameter")%>" />
<c:set var="parameter" value="<%=request.getParameter("parameter")%>" scope="session"/>
<c:set var="refMaster.id" value="<%=request.getParameter("id")%>" scope="session"/>
<c:set var="pmt" value="%{refMaster.parameter}" scope="session"/>
<c:set var="deletedId" value="<%=request.getParameter("id")%>" scope="session"/>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="customsDocumentSearchLists" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:12px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%"  >
<thead>
<tr>
<th><fmt:message key="refMaster.code"/></th>
<th><fmt:message key="refMaster.description"/></th>
<th>&nbsp;</th>
<%-- <th><fmt:message key="refMaster.parameter"/></th>--%>
</tr></thead>	
		<tbody>
		<tr>
			<td width="" align="left">
			    <%-- <s:textfield name="refMaster.code" required="true" cssClass="input-text" size="30"/> --%>
			    <%-- <s:textfield name="customDocCode" required="true" cssClass="input-text" size="30"/> --%>
			    <s:select cssClass="list-menu"   name="customDocCode"   list="%{getCodeList}" headerKey="" headerValue="" cssStyle="width:150px"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="customDocDescription" required="true" cssClass="input-text" size="30"/>
			</td>
			<td width="" align="center" style="border-left:hidden;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
			</tr>
		<%-- 	<td width="20" align="left">
			    <s:textfield name="refMaster.parameter" required="true" cssClass="input-text" size="12"/>
			</td>
		--%>		
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>


<div class="spnblk">&nbsp;</div>
	<display:table name="customDocumentList" class="table" requestURI="customsDocument.html" id="customDocumentList" export="true" pagesize="50" style="width:100%;">
		<c:set var="lowerStr" value="${refMasterList.parameter}"/>
		 <c:set var="fieldLength" value="${refMasterList.fieldLength}"/>
	<display:column sortable="true" property="code" titleKey="refMaster.code" style="width:100px;">
	</display:column>
				
	<display:column property="description" sortable="true" maxLength="50" titleKey="refMaster.description" style="width:100px;"/>
	
	
	<display:column  sortable="true" maxLength="80" title="Downloads" style="width:80px;">
	<c:if test="${not empty customDocumentList.address1}">
   <a href="${customDocumentList.address1}" target="_blank" >
   <img src="images/downarrow.png" border="0" align="middle" style="cursor:pointer;"/></a>
	</c:if>
   <c:if test="${empty customDocumentList.address1}">
 <c:out  value="N/A" />
	</c:if>
	
	</display:column>
	
	</display:table>
	
	</div>
	</s:form>
	