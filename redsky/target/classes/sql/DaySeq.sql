DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`DaySeq` $$
CREATE DEFINER=`root`@`localhost` FUNCTION `DaySeq`(corp char(10),ship char(20)) RETURNS varchar(300) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
BEGIN
DECLARE sequenceFirstDay date;
DECLARE sequenceLastDay date;
DECLARE sequenceDay varchar(25) DEFAULT 0;
delete FROM TempDays;
SELECT trackingstatus.beginPacking INTO sequenceFirstDay FROM trackingstatus where trackingstatus.corpid=corp  and trackingstatus.beginload is not null and trackingstatus.beginPacking is not null and trackingstatus.shipnumber=ship;
SELECT trackingstatus.beginload INTO sequenceLastDay FROM trackingstatus where trackingstatus.corpid=corp  and trackingstatus.beginload is not null and trackingstatus.beginPacking is not null and trackingstatus.shipnumber=ship;
  WHILE sequenceLastDay > sequenceFirstDay DO
  SET sequenceFirstDay = DATE_ADD(sequenceFirstDay, interval 1 day);

  SET sequenceDay=sequenceDay+1;
  
  Insert INTO TempDays values(sequenceDay);

  END WHILE;


RETURN sequenceFirstDay;

END $$

DELIMITER ;