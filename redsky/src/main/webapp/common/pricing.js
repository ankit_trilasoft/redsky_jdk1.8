<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1"%>

<script language="JavaScript" type="text/javascript">

function calculateRevenue(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid,field1,field2,field3) {
	 if(field1!='' && field2!='' & field3!=''){
		 rateOnActualizationDate(field1,field2,field3);
		 }	   
	   var revenueValue=0;
	    var revenueRound=0;
	    var oldRevenue=0;
	    var oldRevenueSum=0;
	    var balanceRevenue=0;
	    try{
		 <c:if test="${contractType}">
		var estimateSellQuantity=document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value;
		 if(estimateSellQuantity=='' ||estimateSellQuantity=='0' ||estimateSellQuantity=='0.0' ||estimateSellQuantity=='0.00') {
			 document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=1;
	         }			 
		 </c:if>
		}catch(e){}
	    oldRevenue=eval(document.forms['operationResourceForm'].elements[revenue].value);
	    <c:if test="${not empty serviceOrder.id}">
	    oldRevenueSum=eval(document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
	    </c:if>
	    var basisValue = document.forms['operationResourceForm'].elements[basis].value;
	    checkFloatNew('operationResourceForm',quantity,'Nothing');
	    var quantityValue = document.forms['operationResourceForm'].elements[quantity].value;
	    <c:if test="${contractType}"> 
	    checkFloatNew('operationResourceForm','estimateSellQuantity'+aid,'Nothing');
	    quantityValue = document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value;
	    </c:if>
	    checkFloatNew('operationResourceForm',sellRate,'Nothing');
	    var sellRateValue = document.forms['operationResourceForm'].elements[sellRate].value;
	    if(basisValue=="cwt" || basisValue== "%age"){
	    revenueValue=(quantityValue*sellRateValue)/100 ;
	    }
	    else if(basisValue=="per 1000"){
	    revenueValue=(quantityValue*sellRateValue)/1000 ;
	    }else{
	    revenueValue=(quantityValue*sellRateValue);
	    } 
	    var estimateDiscount=0.00;
	    try{
	    	 estimateDiscount=document.forms['operationResourceForm'].elements['estimateDiscount'+aid].value;
	    }catch(e){}
	    if(estimateDiscount!=0.00){
	    	revenueValue=(revenueValue*estimateDiscount)/100;
	    }	    
	    revenueRound=Math.round(revenueValue*10000)/10000;
	    balanceRevenue=revenueRound-oldRevenue;
	    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
	    balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
	    oldRevenueSum=oldRevenueSum+balanceRevenue; 
	    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
	    var revenueRoundNew=""+revenueRound;
	    if(revenueRoundNew.indexOf(".") == -1){
	    revenueRoundNew=revenueRoundNew+".00";
	    } 
	    if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
	    revenueRoundNew=revenueRoundNew+"0";
	    }

	    document.forms['operationResourceForm'].elements[revenue].value=revenueRoundNew;
	    try{
	        var buyRate1=document.forms['operationResourceForm'].elements['estimateSellRate'+aid].value;
	        var estCurrency1=document.forms['operationResourceForm'].elements['estSellCurrencyNew'+aid].value;
			if(estCurrency1.trim()!=""){
					var Q1=0;
					var Q2=0;
					Q1=buyRate1;
					Q2=document.forms['operationResourceForm'].elements['estSellExchangeRateNew'+aid].value;
		            var E1=Q1*Q2;
		            E1=Math.round(E1*10000)/10000;
		            document.forms['operationResourceForm'].elements['estSellLocalRateNew'+aid].value=E1;
		            var estLocalAmt1=0;
		            if(basisValue=="cwt" || basisValue== "%age"){
		            	estLocalAmt1=(quantityValue*E1)/100 ;
		                }
		                else if(basisValue=="per 1000"){
		                	estLocalAmt1=(quantityValue*E1)/1000 ;
		                }else{
		                	estLocalAmt1=(quantityValue*E1);
		                }	 
		            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
		            document.forms['operationResourceForm'].elements['estSellLocalAmountNew'+aid].value=estLocalAmt1;     
			}
			<c:if test="${contractType}"> 
				try{
				     var estimatePayableContractExchangeRate=document.forms['operationResourceForm'].elements['estimateContractExchangeRateNew'+aid].value;
				     var estLocalRate=document.forms['operationResourceForm'].elements['estSellLocalRateNew'+aid].value;
				     var estExchangeRate=document.forms['operationResourceForm'].elements['estSellExchangeRateNew'+aid].value
				     var recCurrencyRate=(estimatePayableContractExchangeRate*estLocalRate)/estExchangeRate;
				     var roundValue=Math.round(recCurrencyRate*10000)/10000;
				     document.forms['operationResourceForm'].elements['estimateContractRateNew'+aid].value=roundValue ;
					var Q11=0;
					var Q21=0;
					Q11=document.forms['operationResourceForm'].elements[quantity].value;
				    <c:if test="${contractType}"> 
				    Q11 = document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value;
				    </c:if>
					Q21=document.forms['operationResourceForm'].elements['estimateContractRateNew'+aid].value;
		            var E11=Q11*Q21;
		            var estLocalAmt11=0;
		            if(basisValue=="cwt" || basisValue== "%age"){
		            	estLocalAmt11=E11/100 ;
		                }
		                else if(basisValue=="per 1000"){
		                	estLocalAmt11=E11/1000 ;
		                }else{
		                	estLocalAmt11=E11;
		                }	 
		            estLocalAmt11=Math.round(estLocalAmt11*10000)/10000;    
		            document.forms['operationResourceForm'].elements['estimateContractRateAmmountNew'+aid].value=estLocalAmt11;
				}catch(e){}     
			</c:if>
	    }catch(e){}
	    <c:if test="${not empty serviceOrder.id}">
	    var newRevenueSum=""+oldRevenueSum;
	    if(newRevenueSum.indexOf(".") == -1){
	    newRevenueSum=newRevenueSum+".00";
	    } 
	    if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
	    newRevenueSum=newRevenueSum+"0";
	    } 
	    document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
	    </c:if>
	    if(revenueRoundNew>0){
	    document.forms['operationResourceForm'].elements[displayOnQuots].checked = true;
	    }
	    try{
	    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
	    calculateVatAmt(estVatPer,revenue,estAmt);
	    </c:if>
	    }catch(e){}
	    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage);
	   }     

var isLoadPricing='';
function loadSoPricing(){
    var oForm = document.forms[0].name; 
		if('${serviceOrder.id}' == '' || '${serviceOrder.id}' == null){
			alert('Please save the Order first before accessing the Pricing section.');
			return false;
		}
			findSoAllPricing();
	}
function findSoAllPricing(){
	  var oForm = document.forms[0].text; 
	var accUpdateAjax=document.forms['operationResourceForm'].elements['accUpdateAjax'].value;
	document.forms['operationResourceForm'].elements['accUpdateAjax'].value="";
new Ajax.Request('findAllSoPricingAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}&accUpdateAjax='+accUpdateAjax,
		  {
		    method:'get',
		    onSuccess: function(transport){
		      var response = transport.responseText || "no response text";
		    //alert(response);
		      var container = document.getElementById("pricingSoAjax");
		      pricingValue='';
		      findAllPricingAjax(); 
		      container.innerHTML = response;	
		      $("#accountLineList").find( "th:contains('Quantity')" ).css("border-left","medium solid #003366"); 
		      $("#accountLineList").find( "th:contains('Description')" ).css("border-left","medium solid #003366"); 
		      $("#accountLineList").find( "th:contains('Rec')" ).css("border-left","1px solid #3dafcb"); 
		       showOrHide(0);
		      container.show();
		     
		    },
		    
		    onLoading: function()
		       {
			       if(document.getElementById("loading")!=null){
		    	var loading = document.getElementById("loading").style.display = "block";
		    	
		    	 loading.innerHTML;
			       }
		    },
		    
		    onFailure: function(){ 
			    //alert('Something went wrong...')
			     }
	  });

}

function findAllPricingAjax(){
new Ajax.Request('findAllPricingLineAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}',
	  {
	    method:'get',
	    onSuccess: function(transport){
	      var response = transport.responseText || "";
	      response = response.trim();
	      idList = response;
	        if(idList.trim() == ''){
	        	document.getElementById("estimatedTotalExpense${serviceOrder.id}").value = 0.0;
	        	document.getElementById("estimatedTotalRevenue${serviceOrder.id}").value = 0.0;
	        	document.getElementById("estimatedGrossMargin${serviceOrder.id}").value = 0.0;
	        	document.getElementById("estimatedGrossMarginPercentage${serviceOrder.id}").value = 0.0;
				<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				setFieldValue('actualTotalRevenue${serviceOrder.id}',0.0);
				setFieldValue('actualTotalExpense${serviceOrder.id}',0.0);
				setFieldValue('actualGrossMargin${serviceOrder.id}',0.0);
				setFieldValue('actualGrossMarginPercentage${serviceOrder.id}',0.0);
	        	</c:if>
				<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				setFieldValue('revisedTotalRevenue${serviceOrder.id}',0.0);
				setFieldValue('revisedTotalExpense${serviceOrder.id}',0.0);
				setFieldValue('revisedGrossMargin${serviceOrder.id}',0.0);
				setFieldValue('revisedGrossMarginPercentage${serviceOrder.id}',0.0);
	        	</c:if>					        	
	        	return;
	        }
	        
	        disableRevenue('');					        
	        var id = idList.split(",");
	        var statusFlag='';
			  for (i=0;i<id.length;i++){
				  if(!document.getElementById('statusCheck'+id[i].trim()).checked){
					  document.getElementById('Inactivate').disabled = false;
					  statusFlag='YES';
					  return true;
				  }
			  }
			  if(statusFlag != 'YES'){
				  document.getElementById('Inactivate').disabled = true;
			  }		
											  					  
	      //showOrHide(0);
	    },
	    
	    onFailure: function(){ 
		    //alert('Something went wrong...')
		     }
	  });
}
	  function checkBillingComplete() { 
	      if(document.forms['operationResourceForm'].elements['billing.billComplete'].value !=''){ 
	          var agree = confirm("The billing has been completed, do you still want to add lines?");
	           if(agree){
	        	   findAllPricingLineId('AddLine');
	             }else {         }
	      }else{
	    	  findAllPricingLineId('AddLine');    
	      }
	  }
	  
	  var pricingValue='';
		function findAllPricingLineId(str,clickType){
			if(str == 'Inactive'){
				  inactiveCheck=str;
				  }
			  if(str == 'AddLine' && idList.length == 0){
	        	  autoSavePricingAjax(getHTTPObject(),str);
	        	  return false;
	          }else if(str == 'AddTemplate' && idList.length == 0){
	        	  autoSavePricingAjax(getHTTPObject(),str);
	        	  return false;
	          }else if(str == 'Inactive' && idList.length == 0){
	        	  alert('No Line is there.');
	        	  return false;
	          }else if(idList.length == 0){
	        	  if(str == 'Save'){
	        		  try{
	        			  <c:if test="${not empty serviceOrder.id}">
	        			  var totalExp=document.forms['operationResourceForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value;
		    			  document.forms['operationResourceForm'].elements['serviceOrder.estimatedTotalExpense'].value=totalExp;
		    			  var totalRev = document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value;
		    			  document.forms['operationResourceForm'].elements['serviceOrder.estimatedTotalRevenue'].value=totalRev;
		    			  var grossMargin =document.forms['operationResourceForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value;
		    			  document.forms['operationResourceForm'].elements['serviceOrder.estimatedGrossMargin'].value=grossMargin;
		    			  var grossMarginPercent =document.forms['operationResourceForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value; 
		    			  document.forms['operationResourceForm'].elements['serviceOrder.estimatedGrossMarginPercentage'].value=grossMarginPercent;
		    			  </c:if> 
	        		  }catch(e){}
	        		  saveForm(str);
	        	  }
	        	 
	          
	        	  if(str == 'TabChange'){
	        		  try{
		        		  <c:if test="${not empty serviceOrder.id}">
	        			  var totalExp=document.forms['operationResourceForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value;
		    			  document.forms['operationResourceForm'].elements['serviceOrder.estimatedTotalExpense'].value=totalExp;
		    			  var totalRev = document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value;
		    			  document.forms['operationResourceForm'].elements['serviceOrder.estimatedTotalRevenue'].value=totalRev;
		    			  var grossMargin =document.forms['operationResourceForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value;
		    			  document.forms['operationResourceForm'].elements['serviceOrder.estimatedGrossMargin'].value=grossMargin;
		    			  var grossMarginPercent =document.forms['operationResourceForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value; 
		    			  document.forms['operationResourceForm'].elements['serviceOrder.estimatedGrossMarginPercentage'].value=grossMarginPercent; 
		    			  </c:if>
	        		  }catch(e){}
	        		  ContainerAutoSave(clickType);
	        	  }
	        	  return false; 
	          }
			  var id = idList.split(",");
			
	          var delimeter1 = "-@-";
	          var check =saveValidationPricing(str);
	          if(check){
	        	  for (i=0;i<id.length;i++){
	        		if(pricingValue == ''){
			    			pricingValue  =  getPricingValue(id[i].trim());
			    		}else{
			    			pricingValue  =  pricingValue +delimeter1+ getPricingValue(id[i].trim());
			    		}
	             }
		     }
	          
			  if(pricingValue != ''){
				 
		    		autoSavePricingAjax(getHTTPObject(),str,clickType);
		      }
		  }
		
		
		function disableRevenue(type){
			if(type==''){
				var aidListIds=document.forms['operationResourceForm'].elements['aidListIds'].value;
				var accArr=aidListIds.split(",");
				for(var g=0;g<accArr.length;g++){
					var rev1=document.forms['operationResourceForm'].elements['category'+accArr[g]].value;
					if(rev1 == "Internal Cost"){
						document.forms['operationResourceForm'].elements['estimateRevenueAmount'+accArr[g]].readOnly=true;
						document.forms['operationResourceForm'].elements['estimateRevenueAmount'+accArr[g]].value=0;
						document.forms['operationResourceForm'].elements['estimateSellRate'+accArr[g]].readOnly=true;
						document.forms['operationResourceForm'].elements['estimateSellRate'+accArr[g]].value=0;
						document.forms['operationResourceForm'].elements['estimatePassPercentage'+accArr[g]].readOnly=true;
						document.forms['operationResourceForm'].elements['estimatePassPercentage'+accArr[g]].value=0;
						
						<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
						document.forms['operationResourceForm'].elements['revisionRevenueAmount'+accArr[g]].readOnly=true;
						document.forms['operationResourceForm'].elements['revisionRevenueAmount'+accArr[g]].value=0;
						document.forms['operationResourceForm'].elements['revisionSellRate'+accArr[g]].readOnly=true;
						document.forms['operationResourceForm'].elements['revisionSellRate'+accArr[g]].value=0;
						document.forms['operationResourceForm'].elements['revisionPassPercentage'+accArr[g]].readOnly=true;
						document.forms['operationResourceForm'].elements['revisionPassPercentage'+accArr[g]].value=0;
						calculateRevisionBlock(accArr[g],'','','','');
						</c:if>
						<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
							document.forms['operationResourceForm'].elements['actualRevenue'+accArr[g]].readOnly=true;
							document.forms['operationResourceForm'].elements['actualRevenue'+accArr[g]].value=0;
							document.forms['operationResourceForm'].elements['recRate'+accArr[g]].readOnly=true;
							document.forms['operationResourceForm'].elements['recRate'+accArr[g]].value=0;
							document.forms['operationResourceForm'].elements['recQuantity'+accArr[g]].readOnly=true;
							document.forms['operationResourceForm'].elements['recQuantity'+accArr[g]].value=0;
								<c:if test="${multiCurrency=='Y'}"> 
								document.forms['operationResourceForm'].elements['recCurrencyRate'+accArr[g]].readOnly=true;
								document.forms['operationResourceForm'].elements['recCurrencyRate'+accArr[g]].value=0;
								</c:if> 
								document.forms['operationResourceForm'].elements['payingStatus'+accArr[g]].value='I';
								calculateActualRevenue(accArr[g],'','BAS','','','');
						</c:if>
					}else{
						document.forms['operationResourceForm'].elements['estimateRevenueAmount'+accArr[g]].readOnly=false;
						document.forms['operationResourceForm'].elements['estimateSellRate'+accArr[g]].readOnly=false;
						document.forms['operationResourceForm'].elements['estimatePassPercentage'+accArr[g]].readOnly=false;
						
						<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
						document.forms['operationResourceForm'].elements['revisionRevenueAmount'+accArr[g]].readOnly=false;
						document.forms['operationResourceForm'].elements['revisionSellRate'+accArr[g]].readOnly=false;
						document.forms['operationResourceForm'].elements['revisionPassPercentage'+accArr[g]].readOnly=false;
						</c:if>
						<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
							document.forms['operationResourceForm'].elements['actualRevenue'+accArr[g]].readOnly=false;
							document.forms['operationResourceForm'].elements['recRate'+accArr[g]].readOnly=false;
							document.forms['operationResourceForm'].elements['recQuantity'+accArr[g]].readOnly=false;
								<c:if test="${multiCurrency=='Y'}"> 
								document.forms['operationResourceForm'].elements['recCurrencyRate'+accArr[g]].readOnly=false;
								</c:if> 
						</c:if>
						
					}
				}
				totalOfestimate();
				totalOfRevision();
			}else{
				var rev1=document.forms['operationResourceForm'].elements['category'+type].value;
				if(rev1 == "Internal Cost"){
					document.forms['operationResourceForm'].elements['estimateRevenueAmount'+type].readOnly=true;
					document.forms['operationResourceForm'].elements['estimateRevenueAmount'+type].value=0;
					document.forms['operationResourceForm'].elements['estimateSellRate'+type].readOnly=true;
					document.forms['operationResourceForm'].elements['estimateSellRate'+type].value=0;
					document.forms['operationResourceForm'].elements['estimatePassPercentage'+type].readOnly=true;
					document.forms['operationResourceForm'].elements['estimatePassPercentage'+type].value=0;
					
					<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					document.forms['operationResourceForm'].elements['revisionRevenueAmount'+type].readOnly=true;
					document.forms['operationResourceForm'].elements['revisionRevenueAmount'+type].value=0;
					document.forms['operationResourceForm'].elements['revisionSellRate'+type].readOnly=true;
					document.forms['operationResourceForm'].elements['revisionSellRate'+type].value=0;
					document.forms['operationResourceForm'].elements['revisionPassPercentage'+type].readOnly=true;
					document.forms['operationResourceForm'].elements['revisionPassPercentage'+type].value=0;
					calculateRevisionBlock(type,'','','','');
					</c:if>
					<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
						document.forms['operationResourceForm'].elements['actualRevenue'+type].readOnly=true;
						document.forms['operationResourceForm'].elements['actualRevenue'+type].value=0;
						document.forms['operationResourceForm'].elements['recRate'+type].readOnly=true;
						document.forms['operationResourceForm'].elements['recRate'+type].value=0;
						document.forms['operationResourceForm'].elements['recQuantity'+type].readOnly=true;
						document.forms['operationResourceForm'].elements['recQuantity'+type].value=0;
							<c:if test="${multiCurrency=='Y'}"> 
							document.forms['operationResourceForm'].elements['recCurrencyRate'+type].readOnly=true;
							document.forms['operationResourceForm'].elements['recCurrencyRate'+type].value=0;
							</c:if> 
							document.forms['operationResourceForm'].elements['payingStatus'+type].value='I';
							calculateActualRevenue(type,'','BAS','','','');
					</c:if>
				}else{
					document.forms['operationResourceForm'].elements['estimateRevenueAmount'+type].readOnly=false;
					document.forms['operationResourceForm'].elements['estimateSellRate'+type].readOnly=false;
					document.forms['operationResourceForm'].elements['estimatePassPercentage'+type].readOnly=false;
					
					<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					document.forms['operationResourceForm'].elements['revisionRevenueAmount'+type].readOnly=false;
					document.forms['operationResourceForm'].elements['revisionSellRate'+type].readOnly=false;
					document.forms['operationResourceForm'].elements['revisionPassPercentage'+type].readOnly=false;
					</c:if>
					<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
						document.forms['operationResourceForm'].elements['actualRevenue'+type].readOnly=false;
						document.forms['operationResourceForm'].elements['recRate'+type].readOnly=false;
						document.forms['operationResourceForm'].elements['recQuantity'+type].readOnly=false;
							<c:if test="${multiCurrency=='Y'}"> 
							document.forms['operationResourceForm'].elements['recCurrencyRate'+type].readOnly=false;
							</c:if> 
					</c:if>
					
				}
				totalOfestimate();
				totalOfRevision();
			}
		}
		
		function saveForm(str){
			showOrHide(1);
			document.getElementById("pricingBtnId").style.display="none";
			document.getElementById("submitBtn").style.display="block";
			
			<c:if test="${not empty serviceOrder.id}">
			var obj = document.getElementById('accInnerTable');
			  if(obj != null && obj != undefined){
				  try{
			
					document.getElementById('pricingFooterValue').value = pricingFooterValue;
					alert("D"+document.getElementById('pricingFooterValue').value)
				  }catch(e){
					  
				  }
			  }
		     </c:if> 	  	          
			
			
			var id = document.getElementById('submitButtonIdOFFMOVE');
			if(id == null || id == undefined){
				document.getElementById('submitButtonId').click();
			}else{
				document.getElementById('submitButtonIdOFFMOVE').click();
			}
		}
		
		
		function ContainerAutoSave(clickType){
			<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
				<c:set var="forwardingTabVal" value="Y" />
			</configByCorp:fieldVisibility>
			var check=false;
			 if(document.forms['operationResourceForm'].elements['serviceOrder.status'].value=='CNCL'){
				 var check = true;
			 }else{
				 var check =saveValidationPricing('autoTab');
			 }
		 if(check){ 	 
		        progressBarAutoSave('1');
				var elementsLen=document.forms['operationResourceForm'].elements.length;
				for(i=0;i<=elementsLen-1;i++) {
					
							document.forms['operationResourceForm'].elements[i].disabled=false;
				}
		        var id1 = document.forms['operationResourceForm'].elements['serviceOrder.id'].value;
		    	var jobNumber = document.forms['operationResourceForm'].elements['serviceOrder.shipNumber'].value;
		    	var idc = document.forms['operationResourceForm'].elements['customerFile.id'].value;
			if (document.forms['operationResourceForm'].elements['formStatus'].value == '1'){
			if ('${autoSavePrompt}' == 'No'){
					var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
		                noSaveAction = 'customerServiceOrders.html?id='+idc;
		              }
			if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.accounting'){
		                noSaveAction = 'accountLineList.html?sid='+id1;
		                }
			if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.containers'){
						 <c:if test="${forwardingTabVal!='Y'}">
							noSaveAction = 'containers.html?id='+id1;
						</c:if>
					  	<c:if test="${forwardingTabVal=='Y'}">
					  		noSaveAction = 'containersAjaxList.html?id='+id1;
					  </c:if>
		         	}
			if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.billing'){
		                noSaveAction = 'editBilling.html?id='+id1;
		                }
			if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.domestic'){
		                noSaveAction = 'editMiscellaneous.html?id='+id1;
		                }
			if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.status'){
		                var rel=document.forms['operationResourceForm'].elements['serviceOrder.job'].value;
		                if(rel=='RLO')
		                { 
		                	noSaveAction = 'editDspDetails.html?id='+id1; 
		                  relo="yes";
		                }else{
		                	noSaveAction =  'editTrackingStatus.html?id='+id1;
		                    } 
		                }
			if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.ticket'){
		                noSaveAction = 'customerWorkTickets.html?id='+id1;
		                }
			if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.claims'){
		                noSaveAction = 'claims.html?id='+id1;
		                }
			if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
						var cidVal='${customerFile.id}';
		                noSaveAction = 'editCustomerFile.html?id='+cidVal;
		                }
			if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.criticaldate'){
		        noSaveAction = 'soAdditionalDateDetails.html?sid='+id1;
		        }
		          processAutoSave(document.forms['operationResourceForm'], 'saveServiceOrder!saveOnTabChange.html', noSaveAction);
			}
		else{
		     if(!(clickType == 'save')){
		      var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='serviceOrderDetail.heading'/>");
		         if(agree){
		           document.forms['operationResourceForm'].action = 'saveServiceOrder!saveOnTabChange.html';
		           document.forms['operationResourceForm'].submit();
		       }else{
		           if(id1 != ''){
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
		               location.href = 'customerServiceOrders.html?id='+idc;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.accounting'){
		               location.href = 'accountLineList.html?sid='+id1;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.containers'){
			        	   <c:if test="${forwardingTabVal!='Y'}">
						 		location.href = 'containers.html?id='+id1;
				  			</c:if>
					  		<c:if test="${forwardingTabVal=='Y'}">
					  			location.href = 'containersAjaxList.html?id='+id1;
					  		</c:if>
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.billing'){
						location.href = 'editBilling.html?id='+id1;
						}
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.domestic'){
		               location.href = 'editMiscellaneous.html?id='+id1;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.status'){

		                var rel=document.forms['operationResourceForm'].elements['serviceOrder.job'].value;
		                if(rel=='RLO')
		                { 
		                	location.href = 'editDspDetails.html?id='+id1; 
		                  relo="yes";
		                }else{
		                	location.href =  'editTrackingStatus.html?id='+id1;
		                    } 
		                }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.ticket'){
		               location.href = 'customerWorkTickets.html?id='+id1;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.claims'){
		               location.href = 'claims.html?id='+id1;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
		        	   var cidVal='${customerFile.id}';
		               location.href = 'editCustomerFile.html?id='+cidVal;
		               }
		       	if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.criticaldate'){
		       		location.href = 'soAdditionalDateDetails.html?sid='+id1;
		            }
		             } }
		   }else{
		   if(id1 != ''){
		       if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
		               location.href = 'customerServiceOrders.html?id='+idc;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.accounting'){
		               location.href = 'accountLineList.html?sid='+id1;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.containers'){
			        	   <c:if test="${forwardingTabVal!='Y'}">
						 		location.href = 'containers.html?id='+id1;
				  			</c:if>
					  		<c:if test="${forwardingTabVal=='Y'}">
					  			location.href = 'containersAjaxList.html?id='+id1;
					  		</c:if>
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.billing'){
						location.href = 'editBilling.html?id='+id1;
					   }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.domestic'){
		               location.href = 'editMiscellaneous.html?id='+id1;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.status'){
		                var rel=document.forms['operationResourceForm'].elements['serviceOrder.job'].value;
		                if(rel=='RLO')
		                { 
		                	location.href = 'editDspDetails.html?id='+id1; 
		                  relo="yes";
		                }else{
		                	location.href =  'editTrackingStatus.html?id='+id1;
		                    }                 
		                }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.ticket'){
		               location.href = 'customerWorkTickets.html?id='+id1;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.claims'){
		               location.href = 'claims.html?id='+id1;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
		              var cidVal='${customerFile.id}';
		        	   location.href = 'editCustomerFile.html?id='+cidVal;
		               }
		       	if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.criticaldate'){
		       		location.href = 'soAdditionalDateDetails.html?sid='+id1;
		            }
		   } }  } }
		  else{	  
		  if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
		               location.href = 'customerServiceOrders.html?id='+idc;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.accounting'){
		               location.href = 'accountLineList.html?sid='+id1;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.containers'){
				        	   <c:if test="${forwardingTabVal!='Y'}">
							 		location.href = 'containers.html?id='+id1;
					  			</c:if>
						  		<c:if test="${forwardingTabVal=='Y'}">
						  			location.href = 'containersAjaxList.html?id='+id1;
						  		</c:if>
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.billing'){
						location.href = 'editBilling.html?id='+id1;
						}
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.domestic'){
		               location.href = 'editMiscellaneous.html?id='+id1;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.status'){               
		                   var rel=document.forms['operationResourceForm'].elements['serviceOrder.job'].value;
		                   if(rel=='RLO')
		                   { 
		                     location.href = 'editDspDetails.html?id='+id1; 
		                     relo="yes";
		                   }else{
		                	   location.href =  'editTrackingStatus.html?id='+id1;
		                       } 
		                }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.ticket'){
		               location.href = 'customerWorkTickets.html?id='+id1;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.claims'){
		               location.href = 'claims.html?id='+id1;
		               }
		           if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
		        	   var cidVal='${customerFile.id}';
		               location.href = 'editCustomerFile.html?id='+cidVal;
		               }
		       	if(document.forms['operationResourceForm'].elements['gotoPageString'].value == 'gototab.criticaldate'){
		       		location.href = 'soAdditionalDateDetails.html?sid='+id1;
		            }
		               } } }
		
		
		 function saveValidationPricing(type){
			  var ChargesMandatory='${checkContractChargesMandatory}';
			  var contractType = '${contractType}';
			  var routingflag=false;
			  var checkAccessQuo='${checkAccessQuotation}';
			  var moveTypeVal = '${serviceOrder.moveType}';	
			  <c:if test="${costElementFlag}">
			  routingflag=true;
			  </c:if>
		      var routing="";
		      try{
		    	  routing=document.forms['operationResourceForm'].elements['serviceOrder.routing'].value;
		      }catch(e){}		  
		  	document.forms['operationResourceForm'].elements['serviceOrder.lastName'].disabled=false;
		      var lastName="";
		      var ChargesMandatory='${checkContractChargesMandatory}';
		      try{
		      	lastName=document.forms['operationResourceForm'].elements['serviceOrder.lastName'].value;
		      }catch(e){}
		      var originCountry="";
		      try{
		      	//originCountry=document.forms['operationResourceForm'].elements['serviceOrder.originCountry'].value;
		      	originCountry=document.getElementById("originCountryCode").value;
		      }catch(e){}
		      var companyDivision="";
		      try{
		      	companyDivision=document.forms['operationResourceForm'].elements['serviceOrder.companyDivision'].value;
		      }catch(e){}
		      var destinationCountry="";
		      try{
		      	//destinationCountry=document.forms['operationResourceForm'].elements['serviceOrder.destinationCountry'].value;
		    	  destinationCountry=document.getElementById("destinationCountryCode").value;
		      }catch(e){}
		      var status="";
		      try{
		      	status=document.forms['operationResourceForm'].elements['serviceOrder.status'].value;
		      }catch(e){}
		      var job="";
		      try{
		      	job=document.forms['operationResourceForm'].elements['serviceOrder.job'].value;
		      }catch(e){}
		      var coordinator="";
		      try{
		      	coordinator=document.forms['operationResourceForm'].elements['serviceOrder.coordinator'].value;
		      }catch(e){}
		      var commodity="";
		      try{
		      	commodity=document.forms['operationResourceForm'].elements['serviceOrder.commodity'].value;
		      }catch(e){}
		      var originState="";
		      try{
		      	originState=document.forms['operationResourceForm'].elements['serviceOrder.originState'].value;
		      }catch(e){}
		      var originCity="";
		      try{
		      	originCity=document.forms['operationResourceForm'].elements['serviceOrder.originCity'].value;
		      }catch(e){}
		      var originZip="";
		      try{
		      	originZip=document.forms['operationResourceForm'].elements['serviceOrder.originZip'].value;
		      }catch(e){}
		      var destinationState="";
		      try{
		      	destinationState=document.forms['operationResourceForm'].elements['serviceOrder.destinationState'].value;
		      }catch(e){}
		      var destinationCity="";
		      try{
		      	destinationCity=document.forms['operationResourceForm'].elements['serviceOrder.destinationCity'].value;
		      }catch(e){}
		      var distance="";
		      try{
		      	distance=document.forms['operationResourceForm'].elements['serviceOrder.distance'].value;
		      }catch(e){}
		      var distanceInKmMiles="";
		      try{
		      	distanceInKmMiles=document.forms['operationResourceForm'].elements['serviceOrder.distanceInKmMiles'].value;
		      }catch(e){}
		      var mode="";
		      try{
		      	mode=document.forms['operationResourceForm'].elements['serviceOrder.mode'].value;
		      }catch(e){}        
		      
		if((lastName=='')&&(status='CNCL')&&(type=='saveButton')){
			   alert("Last name is required field");
			   document.forms['operationResourceForm'].elements['serviceOrder.lastName'].disabled=true;
			   showOrHideAutoSave('0');
			   return false;
		}else if((routing=='')&&(job!='RLO')&&(status!='CNCL') && routingflag && inactiveCheck!='Inactive'){
			 alert("Routing is required field.");
			  showOrHideAutoSave('0');
			  inactiveCheck="";
			 return false;
	  	}else if((originCountry=='')&&(status!='CNCL')){
		 alert("Origin country is required field. ");
		 showOrHideAutoSave('0');
		 return false;
		}else if((companyDivision=='')&&(status!='CNCL')){
		 alert("Company Division is required field.");
		 showOrHideAutoSave('0');
		 return false;
		}else if((destinationCountry=='')&&(status!='CNCL')){
		 alert("Destination country is required field.");
		 showOrHideAutoSave('0');
		 return false;
		}else if((job=='')&&(status!='CNCL')){
		 alert("Job Type is required field.");
		 showOrHideAutoSave('0');
		 return false;
		}else if((coordinator=='')&&(status!='CNCL')){
			if((checkAccessQuo==true || checkAccessQuo=='true') && moveTypeVal=='Quote'){
				return true;
			}else{
				alert("Coordinator is required field.");
				 showOrHideAutoSave('0');
				 return false;
			}
		}else if((commodity=='')&&(job!='RLO')&&(status!='CNCL')){
		 alert("Commodity is required field.");
		 showOrHideAutoSave('0');
		 return false;
		}else if((originState=='')&&(originCountry=="United States")&&(status!='CNCL')){
		 alert("State is a required field in Origin.");
		 showOrHideAutoSave('0');
		 return false;
		}else if((originCity=='')&&(status!='CNCL')){
		 alert("Origin city is required field.");
		 showOrHideAutoSave('0');
		 return false;
		}else if((originZip=='')&&(originCountry=="United States")&&(status!='CNCL')){
		 alert("Origin Postal Code is required field.");
		 showOrHideAutoSave('0');
		 return false;
		}else if((destinationState=='')&&(destinationCountry=="United States")&&(status!='CNCL')){
		 alert("State is a required field in Destination.");
		 showOrHideAutoSave('0');
		 return false;
		}else if((destinationCity=='')&&(status!='CNCL')){
		 alert("Destination city is required field.");
		 showOrHideAutoSave('0');
		 return false;
		}else if((distance!='')&& (distanceInKmMiles=='') && (mode=='Overland'||mode=='Truck')){
			 alert("Distance Unit is required field.");
			  showOrHideAutoSave('0');
			 return false;  
		}else if((ChargesMandatory=='1' || contractType=='true') && type!='Inactive'){
			if(idList!=''){			
				 var id = idList.split(",");
				 for (var i=0;i<id.length;i++){
					var test=document.forms['operationResourceForm'].elements['chargeCode'+id[i].trim()].value;
					if(test.trim()==''){
						var lineNo=document.forms['operationResourceForm'].elements['accountLineNumber'+id[i].trim()].value;
						 alert("Please select Charge Code for Pricing line # "+lineNo);
						 showOrHideAutoSave('0');
						 return false;
					}
				 }
				 return true; 
				}else{
					return true;	
				}
	    }else{	
		 return true;
		 }
		} 
		 
		 
		 var pricingFooterValue ='';
		  function autoSavePricingAjax(httpObj,str,clickType){
			  var check =saveValidationPricing(str);
			  if(check){
			  showOrHide(1);
			  var url1 = '';
			  var addURL ='';
			  var inActiveLine ='';
			  var divisionFlag='';
			  var setDescriptionChargeCode='';
			  var setDefaultDescriptionChargeCode='';
			  var soFlag ='';
			  var rollUpInvoiceFlag='';
			  try{
				  if(document.forms['operationResourceForm'].elements['setDescriptionChargeCode']!=undefined){
				  setDescriptionChargeCode = document.forms['operationResourceForm'].elements['setDescriptionChargeCode'].value;
				  }
				  }catch(e){}
			  try{
				  if(document.forms['operationResourceForm'].elements['setDefaultDescriptionChargeCode']!=undefined){
				  setDefaultDescriptionChargeCode = document.forms['operationResourceForm'].elements['setDefaultDescriptionChargeCode'].value;
				  }
			  }catch(e){}
				var actualDiscountFlag ="FALSE";
				try{
					<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					actualDiscountFlag="TRUE";
				   </c:if>
				 }catch(e){}
					var revisionDiscountFlag ="FALSE";
					try{
						<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
						revisionDiscountFlag="TRUE";
					   </c:if>
					 }catch(e){}				 			  
			  if(str == 'AddLine'){
				 // addURL = "&addPriceLine=YES";
				  
				  try{
					divisionFlag = document.forms['operationResourceForm'].elements['divisionFlag'].value;
				  }catch(e){}
			  }else if(str == 'AddTemplate'){
				 // addURL = "&addPriceLine=AddTemplate";				  
				  try{
					  rollUpInvoiceFlag = document.forms['operationResourceForm'].elements['rollUpInvoiceFlag'].value;
				  }catch(e){}
				  try{
						divisionFlag = document.forms['operationResourceForm'].elements['divisionFlag'].value;
					  }catch(e){}
				  var jobtypeSO = document.forms['operationResourceForm'].elements['serviceOrder.job'].value;
					var routingSO = document.forms['operationResourceForm'].elements['serviceOrder.routing'].value;
					var modeSO = document.forms['operationResourceForm'].elements['serviceOrder.mode'].value;
					var packingModeSO = document.forms['operationResourceForm'].elements['serviceOrder.packingMode'].value;
					var commoditySO = document.forms['operationResourceForm'].elements['serviceOrder.commodity'].value;
					var serviceTypeSO = document.forms['operationResourceForm'].elements['serviceOrder.serviceType'].value;
					var companyDivisionSO = document.forms['operationResourceForm'].elements['serviceOrder.companyDivision'].value;
					var reloServiceType=document.forms['operationResourceForm'].elements['reloServiceType'].value;
					while(reloServiceType.indexOf("#")>-1){
						reloServiceType=reloServiceType.replace('#',',');
					}
					url1 = '&jobtypeSO='+jobtypeSO+'&routingSO='+routingSO+'&modeSO='+modeSO+'&packingModeSO'+packingModeSO+'&commoditySO'+commoditySO;
					url1 = url1+'&serviceTypeSO'+serviceTypeSO+'&companyDivisionSO'+companyDivisionSO+'&reloServiceType'+reloServiceType;
			  }else if(str == 'Inactive'){
				  var id = idList.split(",");
					  for (i=0;i<id.length;i++){
						  try{
						  if(!document.getElementById('statusCheck'+id[i].trim()).checked){
							  if(inActiveLine == ''){
								  inActiveLine = id[i].trim();
							  }else{
								  inActiveLine = inActiveLine+','+id[i].trim();
							  }
						  }  
						  }catch(e){}
					
				  } 
				  if(inActiveLine == ''){
					  alert('Please uncheck any line.');
					  showOrHide(0);
					  return false;
				  }
			  }
			  <c:if test="${not empty serviceOrder.id}">
			  var totalExp=document.forms['operationResourceForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value;
			  document.forms['operationResourceForm'].elements['serviceOrder.estimatedTotalExpense'].value=totalExp;
			  var totalRev = document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value;
			  document.forms['operationResourceForm'].elements['serviceOrder.estimatedTotalRevenue'].value=totalRev;
			  var grossMargin =document.forms['operationResourceForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value;
			  document.forms['operationResourceForm'].elements['serviceOrder.estimatedGrossMargin'].value=grossMargin;
			  var grossMarginPercent =document.forms['operationResourceForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value; 
			  document.forms['operationResourceForm'].elements['serviceOrder.estimatedGrossMarginPercentage'].value=grossMarginPercent;
			  pricingFooterValue = totalExp+"-,-"+totalRev+"-,-"+grossMargin+"-,-"+grossMarginPercent;
			  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			  var actualTotalRevenue=document.forms['operationResourceForm'].elements['actualTotalRevenue'+${serviceOrder.id}].value;
			  document.forms['operationResourceForm'].elements['serviceOrder.actualRevenue'].value=actualTotalRevenue;
			  var actualTotalExpense = document.forms['operationResourceForm'].elements['actualTotalExpense'+${serviceOrder.id}].value;
			  document.forms['operationResourceForm'].elements['serviceOrder.actualExpense'].value=actualTotalExpense;
			  var actualGrossMargin =document.forms['operationResourceForm'].elements['actualGrossMargin'+${serviceOrder.id}].value;
			  document.forms['operationResourceForm'].elements['serviceOrder.actualGrossMargin'].value=actualGrossMargin;
			  var actualGrossMarginPercentage =document.forms['operationResourceForm'].elements['actualGrossMarginPercentage'+${serviceOrder.id}].value; 
			  document.forms['operationResourceForm'].elements['serviceOrder.actualGrossMarginPercentage'].value=actualGrossMarginPercentage;
			  var projectedActualRevenue =document.forms['operationResourceForm'].elements['projectedActualRevenue'+${serviceOrder.id}].value;
			  document.forms['operationResourceForm'].elements['serviceOrder.projectedActualRevenue'].value=projectedActualRevenue;
		      var projectedActualExpense =document.forms['operationResourceForm'].elements['projectedActualExpense'+${serviceOrder.id}].value; 
			  document.forms['operationResourceForm'].elements['serviceOrder.projectedActualExpense'].value=projectedActualExpense;
			  pricingFooterValue=pricingFooterValue+"-,-"+actualTotalExpense+"-,-"+actualTotalRevenue+"-,-"+actualGrossMargin+"-,-"+actualGrossMarginPercentage+"-,-"+projectedActualRevenue+"-,-"+projectedActualExpense;
             </c:if>
			  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			  var revisedTotalRevenue=document.forms['operationResourceForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value;
			  document.forms['operationResourceForm'].elements['serviceOrder.revisedTotalRevenue'].value=revisedTotalRevenue;
			  var revisedTotalExpense = document.forms['operationResourceForm'].elements['revisedTotalExpense'+${serviceOrder.id}].value;
			  document.forms['operationResourceForm'].elements['serviceOrder.revisedTotalExpense'].value=revisedTotalExpense;
			  var revisedGrossMargin =document.forms['operationResourceForm'].elements['revisedGrossMargin'+${serviceOrder.id}].value;
			  document.forms['operationResourceForm'].elements['serviceOrder.revisedGrossMargin'].value=revisedGrossMargin;
			  var revisedGrossMarginPercentage =document.forms['operationResourceForm'].elements['revisedGrossMarginPercentage'+${serviceOrder.id}].value; 
			  document.forms['operationResourceForm'].elements['serviceOrder.revisedGrossMarginPercentage'].value=revisedGrossMarginPercentage;
			  pricingFooterValue=pricingFooterValue+"-,-"+revisedTotalExpense+"-,-"+revisedTotalRevenue+"-,-"+revisedGrossMargin+"-,-"+revisedGrossMarginPercentage;
			  </c:if>
             </c:if> 
              soFlag=document.forms['operationResourceForm'].elements['isSOExtract'].value;       
             document.forms['operationResourceForm'].elements['isSOExtract'].value="";      			  
             var reloServiceType=document.forms['operationResourceForm'].elements['reloServiceType'].value;
				while(reloServiceType.indexOf("#")>-1){
					reloServiceType=reloServiceType.replace('#',',');
				}
			  $.ajax({
				  type: "POST",
				  url: "autoSOSavePricingAjax.html?ajax=1&decorator=simple&popup=true",
				  data: { sid: '${serviceOrder.id}', id: '${serviceOrder.id}',pricingValue: pricingValue, delimeter1: '-@-',delimeter2: '~',delimeter3: '-,-',
				    	pricingFooterValue: pricingFooterValue,addPriceLine: str,
				    	jobtypeSO: jobtypeSO,routingSO:routingSO,modeSO: modeSO,packingModeSO:packingModeSO,commoditySO:commoditySO,
				    	serviceTypeSO:serviceTypeSO,reloServiceType:reloServiceType,companyDivisionSO:companyDivisionSO,inActiveLine:inActiveLine,divisionFlag:divisionFlag,actualDiscountFlag:actualDiscountFlag,revisionDiscountFlag:revisionDiscountFlag,setDescriptionChargeCode:setDescriptionChargeCode,setDefaultDescriptionChargeCode:setDefaultDescriptionChargeCode,isSOExtract:soFlag,rollUpInvoiceFlag:rollUpInvoiceFlag
				        },
				 success: function (data, textStatus, jqXHR) {
					 if(str == 'AddTemplate'){
						 document.forms['operationResourceForm'].elements['defaultTemplate'].value = 'YES';
		              }
					 
				    		showOrHide(0);
				    		//alert('ok');
				    		if(str == 'AddLine' || str == 'AddTemplate' || str == 'Inactive'|| str==''){
				    			findSoAllPricing();
				              }
							  if(str == 'Save'){
								 saveForm('Save');
				              }
							  if(str == 'TabChange'){
								  ContainerAutoSave(clickType);
				              }
							
						},
			    error: function (data, textStatus, jqXHR) {
					    	showOrHide(0);
					    	//alert('Something went wrong...'); 
					    	
					    	if(str == 'AddLine' || str == 'AddTemplate' || str == 'Inactive'){
					    		findSoAllPricing();
				              }
							  if(str == 'Save'){
								 // saveForm('Save');
				              }
							  if(str == 'TabChange'){
								  ContainerAutoSave(clickType);
				              }
							
						}
				});
			  }
			  inactiveCheck="";	
		  }
		  
		  function saveForm(str){
				showOrHide(1);
				document.getElementById("pricingBtnId").style.display="none";
				document.getElementById("submitBtn").style.display="block";
				
				<c:if test="${not empty serviceOrder.id}">
				var obj = document.getElementById('accInnerTable');
				  if(obj != null && obj != undefined){
					  try{
				
						document.getElementById('pricingFooterValue').value = pricingFooterValue;
						alert("D"+document.getElementById('pricingFooterValue').value)
					  }catch(e){
						  
					  }
				  }
			     </c:if> 	  	          
				
				
				var id = document.getElementById('submitButtonIdOFFMOVE');
				if(id == null || id == undefined){
					document.getElementById('submitButtonId').click();
				}else{
					document.getElementById('submitButtonIdOFFMOVE').click();
				}
			}
		  
		  function calculateActualRevenue(aid,targetElement,type,field1,field2,field3) {
				 if(field1!='' && field2!='' & field3!=''){
					 rateOnActualizationDate(field1,field2,field3);
					 }	   
				  var chargeCode=document.forms['operationResourceForm'].elements['chargeCode'+aid].value;
				  var vendorCode=document.forms['operationResourceForm'].elements['vendorCode'+aid].value; 
				  var recGl=document.forms['operationResourceForm'].elements['recGl'+aid].value;
				  var payGl=document.forms['operationResourceForm'].elements['payGl'+aid].value;   
				  var actgCode=document.forms['operationResourceForm'].elements['actgCode'+aid].value;
				  var acref="${accountInterface}";
				  acref=acref.trim();
				  if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
					  alert("Vendor Code Missing. Please select");
					  document.forms['operationResourceForm'].elements[targetElement.name].value='';
				  }else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
					  alert("Charge code needs to be selected");
					  document.forms['operationResourceForm'].elements[targetElement.name].value='';
				  }	else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY' && acref =='Y'){
					  alert("Pay GL missing. Please select another charge code.");
					  document.forms['operationResourceForm'].elements[targetElement.name].value='';
				  }	else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC' && acref =='Y'){
					  alert("Receivable GL missing. Please select another charge code.");
					  document.forms['operationResourceForm'].elements[targetElement.name].value='';
				  }else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
					  alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
					  document.forms['operationResourceForm'].elements[targetElement.name].value='';
				  }else{		   
				 	 
				 var basis=document.forms['operationResourceForm'].elements['basis'+aid].value;
				 var baseRateVal=1;
				 var roundVal=0.00;
				 var recQuantity=document.forms['operationResourceForm'].elements['recQuantity'+aid].value;
				 var recRate=0.00;
				 recRate=document.forms['operationResourceForm'].elements['recRate'+aid].value;
				 if(recRate=='') {
					 recRate = document.forms['operationResourceForm'].elements['recRate'+aid].value=0.00;
			         }
				 var actualRevenue=document.forms['operationResourceForm'].elements['actualRevenue'+aid].value;
				 var actualExpense=document.forms['operationResourceForm'].elements['actualExpense'+aid].value;
			     var actualDiscount=0.00;
			     try{
			    	 actualDiscount=document.forms['operationResourceForm'].elements['actualDiscount'+aid].value;
			     }catch(e){}
				 if(recQuantity=='' ||recQuantity=='0' ||recQuantity=='0.0' ||recQuantity=='0.00') {
					 recQuantity = document.forms['operationResourceForm'].elements['recQuantity'+aid].value=1;
			         } 
			     if( basis=="cwt" || basis=="%age"){
			       	 baseRateVal=100;
			     }else if(basis=="per 1000"){
			       	 baseRateVal=1000;
			     } else {
			       	 baseRateVal=1;  	
				 }
			     actualRevenue=(recQuantity*recRate)/baseRateVal;
			 	    if(actualDiscount!=0.00){
			 	    	actualRevenue=(actualRevenue*actualDiscount)/100;
				    }
			 	   document.forms['operationResourceForm'].elements['actualRevenue'+aid].value=actualRevenue;
			 	  <c:if test="${contractType}">
					var contractCurrency=document.forms['operationResourceForm'].elements['contractCurrency'+aid].value;		
					var contractExchangeRate=document.forms['operationResourceForm'].elements['contractExchangeRate'+aid].value;
					var contractRate=document.forms['operationResourceForm'].elements['contractRate'+aid].value;
					var contractRateAmmount=document.forms['operationResourceForm'].elements['contractRateAmmount'+aid].value;
					var contractValueDate=document.forms['operationResourceForm'].elements['contractValueDate'+aid].value;
				 	if(contractCurrency!=''){
				 		var ss=findExchangeRateGlobal(contractCurrency);
				 		contractExchangeRate=parseFloat(ss);
				 		document.forms['operationResourceForm'].elements['contractExchangeRate'+aid].value=contractExchangeRate;
				 		document.forms['operationResourceForm'].elements['contractValueDate'+aid].value=currentDateGlobal();
				 		contractRate=recRate*contractExchangeRate;
				 		document.forms['operationResourceForm'].elements['contractRate'+aid].value=contractRate;
				 		contractRateAmmount=(recQuantity*contractRate)/baseRateVal;
				  	    if(actualDiscount!=0.00){
				  	    	contractRateAmmount=(contractRateAmmount*actualDiscount)/100;
				 	    }
				  	   document.forms['operationResourceForm'].elements['contractRateAmmount'+aid].value=contractRateAmmount;	 		
				 	}
			 	  </c:if>
					var recRateCurrency=document.forms['operationResourceForm'].elements['recRateCurrency'+aid].value;		
					var recRateExchange=document.forms['operationResourceForm'].elements['recRateExchange'+aid].value;
					var recCurrencyRate=document.forms['operationResourceForm'].elements['recCurrencyRate'+aid].value;
					var actualRevenueForeign=document.forms['operationResourceForm'].elements['actualRevenueForeign'+aid].value;
					var racValueDate=document.forms['operationResourceForm'].elements['racValueDate'+aid].value;
				 	if(recRateCurrency!=''){
				 		var ss=findExchangeRateGlobal(recRateCurrency);
				 		recRateExchange=parseFloat(ss);
				 		document.forms['operationResourceForm'].elements['recRateExchange'+aid].value=recRateExchange;
				 		document.forms['operationResourceForm'].elements['racValueDate'+aid].value=currentDateGlobal();
				 		recCurrencyRate=recRate*recRateExchange;
				 		document.forms['operationResourceForm'].elements['recCurrencyRate'+aid].value=recCurrencyRate;
				 		actualRevenueForeign=(recQuantity*recCurrencyRate)/baseRateVal;
				  	    if(actualDiscount!=0.00){
				  	    	actualRevenueForeign=(actualRevenueForeign*actualDiscount)/100;
				 	    }
				  	   document.forms['operationResourceForm'].elements['actualRevenueForeign'+aid].value=actualRevenueForeign;	 		
				 	}

			<c:if test="${contractType}">
			var payableContractCurrency=document.forms['operationResourceForm'].elements['payableContractCurrency'+aid].value;		
			var payableContractExchangeRate=document.forms['operationResourceForm'].elements['payableContractExchangeRate'+aid].value;
			var payableContractRateAmmount=document.forms['operationResourceForm'].elements['payableContractRateAmmount'+aid].value;
			var payableContractValueDate=document.forms['operationResourceForm'].elements['payableContractValueDate'+aid].value;
				if(payableContractCurrency!=''){
					var ss=findExchangeRateGlobal(payableContractCurrency);
					payableContractExchangeRate=parseFloat(ss);
					document.forms['operationResourceForm'].elements['payableContractExchangeRate'+aid].value=payableContractExchangeRate;
					document.forms['operationResourceForm'].elements['payableContractValueDate'+aid].value=currentDateGlobal();
					payableContractRateAmmount=actualExpense*payableContractExchangeRate;
				   document.forms['operationResourceForm'].elements['payableContractRateAmmount'+aid].value=payableContractRateAmmount;	 		
				}
			</c:if>
			var country=document.forms['operationResourceForm'].elements['country'+aid].value;		
			var exchangeRate=document.forms['operationResourceForm'].elements['exchangeRate'+aid].value;
			var localAmount=document.forms['operationResourceForm'].elements['localAmount'+aid].value;
			var valueDate=document.forms['operationResourceForm'].elements['valueDate'+aid].value;
				if(country!=''){
					var ss=findExchangeRateGlobal(country);
					exchangeRate=parseFloat(ss);
					document.forms['operationResourceForm'].elements['exchangeRate'+aid].value=exchangeRate;
					document.forms['operationResourceForm'].elements['valueDate'+aid].value=currentDateGlobal();
					localAmount=actualExpense*exchangeRate;
				   document.forms['operationResourceForm'].elements['localAmount'+aid].value=localAmount;	 		
				}
			 	  	var actualTotalRevenue=0.00;
			 		var actualTotalExpense=0.00;
			 		var tempActualTotalExpense=0.00;
			 		var tempActualTotalRevenue=0.00;
			 		var actualGrossMargin=0.00;
			 		var actualGrossMarginPercentage=0.00;
			 		var aidListIds=document.forms['operationResourceForm'].elements['aidListIds'].value;
					var accArr=aidListIds.split(",");
					for(var g=0;g<accArr.length;g++){
					 	tempActualTotalRevenue="0.00";
					 	if(document.forms['operationResourceForm'].elements['actualRevenue'+accArr[g]].value!=''){
					 		tempActualTotalRevenue=document.forms['operationResourceForm'].elements['actualRevenue'+accArr[g]].value;
					 	}			 	
					 	actualTotalRevenue=actualTotalRevenue+parseFloat(tempActualTotalRevenue);
					 	tempActualTotalExpense="0.00";
					 	if(document.forms['operationResourceForm'].elements['actualExpense'+accArr[g]].value!=''){
					 		tempActualTotalExpense=document.forms['operationResourceForm'].elements['actualExpense'+accArr[g]].value;
					 	}
					 	actualTotalExpense=actualTotalExpense+parseFloat(tempActualTotalExpense);
					}
				actualGrossMargin=actualTotalRevenue-actualTotalExpense;
				try{
					if((actualTotalRevenue==0)||(actualTotalRevenue==0.0)||(actualTotalRevenue==0.00)){
						actualGrossMarginPercentage=0.00;
					}else{
						actualGrossMarginPercentage=(actualGrossMargin*100)/actualTotalRevenue;
					}
				}catch(e){
					actualGrossMarginPercentage=0.00;
					}
				 <c:if test="${not empty serviceOrder.id}">
				document.forms['operationResourceForm'].elements['projectedActualRevenue'+${serviceOrder.id}].value=Math.round(actualTotalRevenue*10000)/10000;
				document.forms['operationResourceForm'].elements['projectedActualExpense'+${serviceOrder.id}].value=Math.round(actualTotalExpense*10000)/10000;		
				</c:if>
		 	  	 actualTotalRevenue=0.00;
		 		 actualTotalExpense=0.00;
		 		 tempActualTotalExpense=0.00;
		 		 tempActualTotalRevenue=0.00;
		 		 actualGrossMargin=0.00;
		 		 actualGrossMarginPercentage=0.00;
		 		var tempRecInvoiceNumber='';
				var tempPayingStatus=''; 		 
		 		for(var g=0;g<accArr.length;g++){
			 		tempRecInvoiceNumber=document.forms['operationResourceForm'].elements['recInvoiceNumber'+accArr[g]].value; 
			 	 	if(tempRecInvoiceNumber!=undefined && tempRecInvoiceNumber!=null && tempRecInvoiceNumber.trim()!=''){
					 	if(document.forms['operationResourceForm'].elements['actualRevenue'+accArr[g]].value!=""){
					 		tempActualTotalRevenue=document.forms['operationResourceForm'].elements['actualRevenue'+accArr[g]].value;
					 	}
					 	actualTotalRevenue=actualTotalRevenue+parseFloat(tempActualTotalRevenue);
			 	 	}
			 	 	tempPayingStatus=document.forms['operationResourceForm'].elements['payingStatus'+accArr[g]].value;
			 	 	if(tempPayingStatus!=undefined && tempPayingStatus!=null && tempPayingStatus.trim()!='' &&( tempPayingStatus.trim()=='A' || tempPayingStatus.trim()=='I')){
					 	if(document.forms['operationResourceForm'].elements['actualExpense'+accArr[g]].value!=""){
					 		tempActualTotalExpense=document.forms['operationResourceForm'].elements['actualExpense'+accArr[g]].value;
					 	}
				 	actualTotalExpense=actualTotalExpense+parseFloat(tempActualTotalExpense);
			 	 	} 	
		 		}
			actualGrossMargin=actualTotalRevenue-actualTotalExpense;
			try{
				if((actualTotalRevenue==0)||(actualTotalRevenue==0.0)||(actualTotalRevenue==0.00)){
					actualGrossMarginPercentage=0.00;
				}else{
					actualGrossMarginPercentage=(actualGrossMargin*100)/actualTotalRevenue;
				}
			}catch(e){
				actualGrossMarginPercentage=0.00;
				}		
			 <c:if test="${not empty serviceOrder.id}">
				document.forms['operationResourceForm'].elements['actualTotalRevenue'+${serviceOrder.id}].value=Math.round(actualTotalRevenue*10000)/10000;
				document.forms['operationResourceForm'].elements['actualTotalExpense'+${serviceOrder.id}].value=Math.round(actualTotalExpense*10000)/10000;
				document.forms['operationResourceForm'].elements['actualGrossMargin'+${serviceOrder.id}].value=Math.round(actualGrossMargin*10000)/10000;
				document.forms['operationResourceForm'].elements['actualGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(actualGrossMarginPercentage*10000)/10000;
				</c:if>
				  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				  <c:if test="${systemDefaultVatCalculationNew=='Y'}">
				   calculateVatSection('REC',aid,type);
				  </c:if>
				  <c:if test="${systemDefaultVatCalculationNew=='Y'}">
				   calculateVatSection('PAY',aid,type);
				  </c:if>
				  </c:if>
				  }	
			 }
		  
		  function totalOfestimate(){
			  // Code Gross Section
			  try{
		  	var estimatedTotalRevenue=0.00;
			var estimatedTotalExpense=0.00;
			var tempEstimatedTotalExpense=0.00;
			var tempEstimatedTotalRevenue=0.00;
			var estimatedGrossMargin=0.00;
			var estimatedGrossMarginPercentage=0.00;
			var aidListIds=document.forms['operationResourceForm'].elements['aidListIds'].value;
			var accArr=aidListIds.split(",");
			for(var g=0;g<accArr.length;g++){
				if(document.forms['operationResourceForm'].elements['estimateRevenueAmount'+accArr[g]].value!=""){
		 			tempEstimatedTotalRevenue=document.forms['operationResourceForm'].elements['estimateRevenueAmount'+accArr[g]].value;
				}
		 		estimatedTotalRevenue=estimatedTotalRevenue+parseFloat(tempEstimatedTotalRevenue);
		 		if(document.forms['operationResourceForm'].elements['estimateExpense'+accArr[g]].value!=""){
		 			tempEstimatedTotalExpense=document.forms['operationResourceForm'].elements['estimateExpense'+accArr[g]].value;
		 		}
		 		estimatedTotalExpense=estimatedTotalExpense+parseFloat(tempEstimatedTotalExpense);
			}
		estimatedGrossMargin=estimatedTotalRevenue-estimatedTotalExpense;
		try{
			if((estimatedTotalRevenue==0)||(estimatedTotalRevenue==0.0)||(estimatedTotalRevenue==0.00)){
				estimatedGrossMarginPercentage=0.00;
			}else{
				estimatedGrossMarginPercentage=(estimatedGrossMargin*100)/estimatedTotalRevenue;
			}
		}catch(e){
			estimatedGrossMarginPercentage=0.00;
			}
		 <c:if test="${not empty serviceOrder.id}">
		document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=Math.round(estimatedTotalRevenue*100)/100;
		document.forms['operationResourceForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=Math.round(estimatedTotalExpense*100)/100;
		document.forms['operationResourceForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=Math.round(estimatedGrossMargin*100)/100;
		document.forms['operationResourceForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(estimatedGrossMarginPercentage*100)/100;
		</c:if>	
			  }catch(e){}
		}
		  
		  
		  function totalOfRevision(){
			  // Code Gross Section
			  try{
		  	var revisedTotalRevenue=0.00;
			var revisedTotalExpense=0.00;
			var tempRevisedTotalExpense=0.00;
			var tempRevisedTotalRevenue=0.00;
			var revisedGrossMargin=0.00;
			var revisedGrossMarginPercentage=0.00;
			var aidListIds=document.forms['operationResourceForm'].elements['aidListIds'].value;
			var accArr=aidListIds.split(",");
			for(var g=0;g<accArr.length;g++){
				if(document.forms['operationResourceForm'].elements['revisionRevenueAmount'+accArr[g]].value!=""){
		 			tempRevisedTotalRevenue=document.forms['operationResourceForm'].elements['revisionRevenueAmount'+accArr[g]].value;
				}
				revisedTotalRevenue=revisedTotalRevenue+parseFloat(tempRevisedTotalRevenue);
		 		if(document.forms['operationResourceForm'].elements['revisionExpense'+accArr[g]].value!=""){
		 			tempRevisedTotalExpense=document.forms['operationResourceForm'].elements['revisionExpense'+accArr[g]].value;
		 		}
		 		revisedTotalExpense=revisedTotalExpense+parseFloat(tempRevisedTotalExpense);
			}
			revisedGrossMargin=revisedTotalRevenue-revisedTotalExpense;
		try{
			if((revisedTotalRevenue==0)||(revisedTotalRevenue==0.0)||(revisedTotalRevenue==0.00)){
				revisedGrossMarginPercentage=0.00;
			}else{
				revisedGrossMarginPercentage=(revisedGrossMargin*100)/revisedTotalRevenue;
			}
		}catch(e){
			revisedGrossMarginPercentage=0.00;
			}
		 <c:if test="${not empty serviceOrder.id}">
		document.forms['operationResourceForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value=Math.round(revisedTotalRevenue*100)/100;
		document.forms['operationResourceForm'].elements['revisedTotalExpense'+${serviceOrder.id}].value=Math.round(revisedTotalExpense*100)/100;
		document.forms['operationResourceForm'].elements['revisedGrossMargin'+${serviceOrder.id}].value=Math.round(revisedGrossMargin*100)/100;
		document.forms['operationResourceForm'].elements['revisedGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(revisedGrossMarginPercentage*100)/100;
		</c:if>	
			  }catch(e){}
		}  
		  
		  
		  
		 
		  function calculateRevisionBlock(aid,type1,field1,field2,field3) {
				 if(field1!='' && field2!='' & field3!=''){
					 rateOnActualizationDate(field1,field2,field3);
					 }	   
				 var basis=document.forms['operationResourceForm'].elements['basis'+aid].value;
				 var baseRateVal=1;
				 var roundVal=0.00;
				 var revisionSellQuantity=0.00;
					 <c:if test="${contractType}">
					 revisionSellQuantity=document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value;
					 if(revisionSellQuantity=='' ||revisionSellQuantity=='0' ||revisionSellQuantity=='0.0' ||revisionSellQuantity=='0.00') {
						 revisionSellQuantity = document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=1;
				         }			 
					 </c:if>
				 var revisionQuantity=document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value;
				 if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
					 revisionQuantity = document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=1;
			         } 
			     if( basis=="cwt" || basis=="%age"){
			       	 baseRateVal=100;
			     }else if(basis=="per 1000"){
			       	 baseRateVal=1000;
			     } else {
			       	 baseRateVal=1;  	
				 }
			     var revisionDiscount=0.00;
			     try{
			    	 revisionDiscount=document.forms['operationResourceForm'].elements['revisionDiscount'+aid].value;
			     }catch(e){}

			     //Code For revision Est	 
				 var revisionRate=0.00;
				 revisionRate=document.forms['operationResourceForm'].elements['revisionRate'+aid].value;
				 if(revisionRate=='') {
					 revisionRate = document.forms['operationResourceForm'].elements['revisionRate'+aid].value=0.00;
			         }
				 revisionBuyFX(aid);
				 var revisionExpense=0.00;
				 revisionExpense=document.forms['operationResourceForm'].elements['revisionExpense'+aid].value;
				 if(revisionExpense=='') {
					 revisionExpense = document.forms['operationResourceForm'].elements['revisionExpense'+aid].value=0.00;
			         }
				 revisionExpense=(revisionRate*revisionQuantity)/baseRateVal;
				    document.forms['operationResourceForm'].elements['revisionExpense'+aid].value=revisionExpense;

			     //Code For revision Sell	 
				 var revisionSellRate=0.00;
				 revisionSellRate=document.forms['operationResourceForm'].elements['revisionSellRate'+aid].value;
				 if(revisionSellRate=='') {
					 revisionSellRate = document.forms['operationResourceForm'].elements['revisionSellRate'+aid].value=0.00;
			         }
				  <c:if test="${multiCurrency=='Y'}">
				  revisionSellFX(aid);
				  </c:if>
				 var revisionRevenueAmount=0.00;
				 revisionRevenueAmount=document.forms['operationResourceForm'].elements['revisionRevenueAmount'+aid].value;
				 if(revisionRevenueAmount=='') {
					 revisionRevenueAmount = document.forms['operationResourceForm'].elements['revisionRevenueAmount'+aid].value=0.00;
			         }
				 <c:if test="${contractType}">
				 revisionRevenueAmount=(revisionSellRate*revisionSellQuantity)/baseRateVal;
				 </c:if>
				 <c:if test="${!contractType}">
				 revisionRevenueAmount=(revisionSellRate*revisionQuantity)/baseRateVal;
				 </c:if>
				 
				    if(revisionDiscount!=0.00){
				    	revisionRevenueAmount=(revisionRevenueAmount*revisionDiscount)/100;
				    }
				    document.forms['operationResourceForm'].elements['revisionRevenueAmount'+aid].value=revisionRevenueAmount;	
				  //Code For Revision Pass Percentage   
				    calculateRevisionPassPercentage('revisionRevenueAmount'+aid,'revisionExpense'+aid,'revisionPassPercentage'+aid);
				  // Code Gross Section
			 	  	var revisedTotalRevenue=0.00;
			 		var revisedTotalExpense=0.00;
			 		var tempRevisedTotalExpense=0.00;
			 		var tempRevisedTotalRevenue=0.00;
			 		var revisedGrossMargin=0.00;
			 		var revisedGrossMarginPercentage=0.00;
			 		var aidListIds=document.forms['operationResourceForm'].elements['aidListIds'].value;
					var accArr=aidListIds.split(",");
					for(var g=0;g<accArr.length;g++){
						if(document.forms['operationResourceForm'].elements['revisionRevenueAmount'+accArr[g]].value!=""){
				 			tempRevisedTotalRevenue=document.forms['operationResourceForm'].elements['revisionRevenueAmount'+accArr[g]].value;
						}
				 		revisedTotalRevenue=revisedTotalRevenue+parseFloat(tempRevisedTotalRevenue);
				 		if(document.forms['operationResourceForm'].elements['revisionExpense'+accArr[g]].value!=""){
				 			tempRevisedTotalExpense=document.forms['operationResourceForm'].elements['revisionExpense'+accArr[g]].value;
				 		}
				 		revisedTotalExpense=revisedTotalExpense+parseFloat(tempRevisedTotalExpense);
					}
				revisedGrossMargin=revisedTotalRevenue-revisedTotalExpense;
				try{
					if((revisedTotalRevenue==0)||(revisedTotalRevenue==0.0)||(revisedTotalRevenue==0.00)){
						revisedGrossMarginPercentage=0.00;
					}else{
						revisedGrossMarginPercentage=(revisedGrossMargin*100)/revisedTotalRevenue;
					}
				}catch(e){
					revisedGrossMarginPercentage=0.00;
					}
				 <c:if test="${not empty serviceOrder.id}">
				document.forms['operationResourceForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value=Math.round(revisedTotalRevenue*10000)/10000;
				document.forms['operationResourceForm'].elements['revisedTotalExpense'+${serviceOrder.id}].value=Math.round(revisedTotalExpense*10000)/10000;
				document.forms['operationResourceForm'].elements['revisedGrossMargin'+${serviceOrder.id}].value=Math.round(revisedGrossMargin*10000)/10000;
				document.forms['operationResourceForm'].elements['revisedGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(revisedGrossMarginPercentage*10000)/10000;
				</c:if>	 
				 <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				 <c:if test="${systemDefaultVatCalculationNew=='Y'}">
				 calculateVatSection('REV',aid,type1); 
				 </c:if>
				 </c:if>   
			 }
		  function changeStatus(){
		
			   document.forms['operationResourceForm'].elements['formStatus'].value='1';
			}
		
			 function findExchangeRateGlobal(currency){
				 var rec='1';
					<c:forEach var="entry" items="${currencyExchangeRate}">
						if('${entry.key}'==currency.trim()){
							rec='${entry.value}';
						}
					</c:forEach>
					return rec;
			 }
			 
			 function rateOnActualizationDate(field1,field2,field3){
					<c:if test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
				    <c:if test="${contractType}">	
					var currency=""; 
						var currentDate=currentDateRateOnActualization();
						try{
						currency=document.forms['operationResourceForm'].elements[field1.trim().split('~')[0]].value;
						document.forms['operationResourceForm'].elements[field2.trim().split('~')[0]].value=currentDate;
						document.forms['operationResourceForm'].elements[field3.trim().split('~')[0]].value=findExchangeRateRateOnActualization(currency);
						}catch(e){}
						try{
						currency=document.forms['operationResourceForm'].elements[field1.trim().split('~')[1]].value;
						document.forms['operationResourceForm'].elements[field2.trim().split('~')[1]].value=currentDate;
						document.forms['operationResourceForm'].elements[field3.trim().split('~')[1]].value=findExchangeRateRateOnActualization(currency);
						}catch(e){}		
					</c:if>
					</c:if>
				}
			 
			 function currentDateRateOnActualization(){
				 var mydate=new Date();
			   var daym;
			   var year=mydate.getFullYear()
			   var y=""+year;
			   if (year < 1000)
			   year+=1900
			   var day=mydate.getDay()
			   var month=mydate.getMonth()+1
			   if(month == 1)month="Jan";
			   if(month == 2)month="Feb";
			   if(month == 3)month="Mar";
				  if(month == 4)month="Apr";
				  if(month == 5)month="May";
				  if(month == 6)month="Jun";
				  if(month == 7)month="Jul";
				  if(month == 8)month="Aug";
				  if(month == 9)month="Sep";
				  if(month == 10)month="Oct";
				  if(month == 11)month="Nov";
				  if(month == 12)month="Dec";
				  var daym=mydate.getDate()
				  if (daym<10)
				  daym="0"+daym
				  var datam = daym+"-"+month+"-"+y.substring(2,4); 
				  return datam;
			}
			 
			 function checkChargeCodeMandatory(sid,aid,chargeCode){
			    	var ChargesMandatory='${checkContractChargesMandatory}';	
			    	var contractType = '${contractType}';
			    	if((ChargesMandatory=='1' || contractType=='true') && (chargeCode==undefined || chargeCode==null || chargeCode=='' || chargeCode.trim()=='')){
			    		alert('Charge code needs to be selected');
			    	}else{
			    		window.open('editAccountLine.html?sid='+sid+'&id='+aid+'',"width=250,height=70,scrollbars=1");
			    	}
			    }
			 
			 function selectPricingMarketPlace(category,id){
					try{
					 var  hidMarketPlace='hidMarketPlace'+id;
					 var  hidMarketPlace11='hidMarketPlace11'+id;
				      var val = document.getElementById(category).value;
				      var pricingFlag='${pricePointFlag}';
				      var e2 = document.getElementById(hidMarketPlace);
				      var e3 = document.getElementById(hidMarketPlace11);
				       if( pricingFlag=='true' && (val=='Origin' || val=='Destin')){
				    	   e2.style.display='block'; 
				    	   e3.style.display='none';
				       }else{
				    	   e2.style.display='none';
				    	   e3.style.display='none';
				    	   }
					}catch(e){}
				       disableRevenue(id);
				       		  }
			 
			 function chk(chargeCode,category,aid){
				  accountLineIdScript=aid;
					var val = document.forms['operationResourceForm'].elements[category].value;
					var quotationContract = document.forms['operationResourceForm'].elements['billing.contract'].value; 
					var accountCompanyDivision = document.forms['operationResourceForm'].elements['companyDivision'+aid].value;
					var originCountry=document.forms['operationResourceForm'].elements['serviceOrder.originCountryCode'].value;
					var destinationCountry=document.forms['operationResourceForm'].elements['serviceOrder.destinationCountryCode'].value;
					var mode;		
					if(document.forms['operationResourceForm'].elements['serviceOrder.mode']!=undefined){
						 mode=document.forms['operationResourceForm'].elements['serviceOrder.mode'].value; }else{mode="";}
					 		
						recGlVal = 'secondDescription';
						payGlVal = 'thirdDescription'; 
						var estimateContractCurrency='tenthDescription';
						var estimatePayableContractCurrency='fourthDescription';
						<c:if test="${contractType}">  
							estimateContractCurrency='contractCurrencyNew'+aid;
							estimatePayableContractCurrency='payableContractCurrencyNew'+aid;
						</c:if>
					if(val=='Internal Cost'){
						quotationContract="Internal Cost Management";
						javascript:openWindow('internalCostChargess.html?accountCompanyDivision='+accountCompanyDivision+'&originCountry='+originCountry+'&destinationCountry='+destinationCountry+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_eigthDescription=eigthDescription&fld_ninthDescription=ninthDescription&fld_tenthDescription='+estimateContractCurrency+'&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription='+estimatePayableContractCurrency+'&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code='+chargeCode);
					    document.forms['operationResourceForm'].elements[chargeCode].select();
					}
					else {
					if(quotationContract=='' || quotationContract==' '){
						alert("There is no pricing contract in billing: Please select.");
					}
					else{
						javascript:openWindow('chargess.html?contract='+quotationContract+'&originCountry='+originCountry+'&destinationCountry='+destinationCountry+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_eigthDescription=eigthDescription&fld_ninthDescription=ninthDescription&fld_tenthDescription='+estimateContractCurrency+'&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription='+estimatePayableContractCurrency+'&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code='+chargeCode+'&categoryType='+val);
					}
				}
				document.forms['operationResourceForm'].elements[chargeCode].focus();
			}  	
			 
				function winOpenForActgCode(vendorCode,vendorName,category,aid){
				
					   accountLineIdScript=aid;
					    var sid='${serviceOrder.id}';
					 	var val = document.forms['operationResourceForm'].elements[category].value;
					 	var finalVal = "OK";
					 	var indexCheck = val.indexOf('Origin'); 
					    openWindow('findVendorCode.html?sid='+sid+'&defaultListFlag=show&partnerType=PA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+vendorName+'&fld_code='+vendorCode+'&fld_seventhDescription=seventhDescription');
					 	document.forms['operationResourceForm'].elements[vendorCode].select(); 
				 }
			 
				function onlyNumberAllowed(evt){
					  var keyCode = evt.which ? evt.which : evt.keyCode;
					  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39); 
					}
				
				function checkAccountLineNumber(temp) { 
				       var accountLineNumber=document.forms['operationResourceForm'].elements[temp].value; 
				       var accountLineNumber1=document.forms['operationResourceForm'].elements[temp].value;
				       accountLineNumber=accountLineNumber.trim();
				       accountLineNumber1=accountLineNumber1.trim();
				       if(accountLineNumber!=""){
				       if(accountLineNumber>999)  {
				         alert("Manually you can not Enter Line # greater than 999");
				         document.forms['operationResourceForm'].elements[temp].value=0;
				       }
				       else if(accountLineNumber1.length<2)  { 
				       	document.forms['operationResourceForm'].elements[temp].value='00'+document.forms['operationResourceForm'].elements[temp].value;
				       }
				       else if(accountLineNumber1.length==2)  { 
				       	var aa = document.forms['operationResourceForm'].elements[temp].value;
				       	document.forms['operationResourceForm'].elements[temp].value='0'+aa;
				       }} } 
				
				function findPartnerDetailsPriceing(nameId,CodeId,DivId,partnerTypeWhereClause,id,evt){
					 var keyCode = evt.which ? evt.which : evt.keyCode;  
					 if(keyCode!=9){
					var nameValue=document.getElementById(nameId).value;
					if(nameValue.length<=1){
						document.getElementById(CodeId).value="";	
						}
					//document.getElementById(CodeId).value="";
					if(nameValue.length>=3){
						$.get("partnerDetailsPricingAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", 
								{partnerNameAutoCopmlete: nameValue,partnerNameId:nameId,paertnerCodeId:CodeId,autocompleteDivId:DivId,autocompleteCondition:partnerTypeWhereClause,pricingAccountLineId:id},
								function(data){
									 document.getElementById(DivId).style.display = "block";
									$("#"+DivId).html(data);
									   //var partnerDetails= document.getElementById('partnerDetailsAutoCompleteListSize').value;
						   				//if(partnerDetails=='true'){
						   				//	document.getElementById(nameId).value="";
						   				//	document.getElementById(CodeId).value="";
						   				//}
							});	
					     }else{
						document.getElementById(DivId).style.display = "none";	
					    }
					 }else{
						 document.getElementById(DivId).style.display = "none";
					 }
					}
				
				function changeBasis(basis,quantity,buyRate,expense,sellRate,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid,targetElement){  
					  var quantityValue = document.forms['operationResourceForm'].elements[quantity].value;
					  var basisValue = document.forms['operationResourceForm'].elements[basis].value; 
					  var oldBasis="old"+basis;
					  var oldQuantity="old"+quantity;
					  var oldQuantityValue = document.forms['operationResourceForm'].elements[oldQuantity].value;
					  var oldBasisValue = document.forms['operationResourceForm'].elements[oldBasis].value;  
					  if((document.forms['operationResourceForm'].elements['serviceOrder.mode'].value=="AIR")|| (document.forms['operationResourceForm'].elements['serviceOrder.mode'].value=="Air")){
					  if(basisValue=='cwt'){
					  document.forms['operationResourceForm'].elements[quantity].value=document.forms['operationResourceForm'].elements['miscellaneous.estimateGrossWeight'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.estimateGrossWeight'].value;
					  </c:if>
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualGrossWeight'].value;
						  <c:if test="${contractType}">
						  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualGrossWeight'].value;
						  </c:if>		  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualGrossWeight'].value;
					  </c:if>
					  }else if(basisValue=='kg'){
					  document.forms['operationResourceForm'].elements[quantity].value=document.forms['operationResourceForm'].elements['miscellaneous.estimateGrossWeightKilo'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.estimateGrossWeightKilo'].value;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualGrossWeightKilo'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualGrossWeightKilo'].value;
					  </c:if>		  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualGrossWeightKilo'].value;
					  </c:if>
					  }
					  else if(basisValue=='cft'){
					  document.forms['operationResourceForm'].elements[quantity].value=document.forms['operationResourceForm'].elements['miscellaneous.estimateCubicFeet'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.estimateCubicFeet'].value;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualCubicFeet'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualCubicFeet'].value;
					  </c:if>		  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualCubicFeet'].value;
					  </c:if>
					  }
					  else if(basisValue=='cbm'){
					  document.forms['operationResourceForm'].elements[quantity].value=document.forms['operationResourceForm'].elements['miscellaneous.estimateCubicMtr'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.estimateCubicMtr'].value;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualCubicMtr'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualCubicMtr'].value;
					  </c:if>		  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualCubicMtr'].value;
					  </c:if>
					  }
					  else if(basisValue=='each'){
					  document.forms['operationResourceForm'].elements[quantity].value=1;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=1;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=1;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=1;
					  </c:if>		  		  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=1;
					  </c:if>
					  }
					  else if(basisValue=='value'){
					  document.forms['operationResourceForm'].elements[quantity].value=1;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=1;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=1;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=1;
					  </c:if>		  	  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=1;
					  </c:if>
					  }
					  else if(basisValue=='flat'){
					  document.forms['operationResourceForm'].elements[quantity].value=1;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=1;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=1;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=1;
					  </c:if>		  		  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=1;
					  </c:if>
					  }
					  }
					  else{
					  if(basisValue=='cwt'){
					  document.forms['operationResourceForm'].elements[quantity].value=document.forms['operationResourceForm'].elements['miscellaneous.estimatedNetWeight'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.estimatedNetWeight'].value;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualNetWeight'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualNetWeight'].value;
					  </c:if>		  		  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualNetWeight'].value;
					  </c:if>
					  }else if(basisValue=='kg'){
					  document.forms['operationResourceForm'].elements[quantity].value=document.forms['operationResourceForm'].elements['miscellaneous.estimatedNetWeightKilo'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.estimatedNetWeightKilo'].value;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualNetWeightKilo'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualNetWeightKilo'].value;
					  </c:if>		  		  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.actualNetWeightKilo'].value;
					  </c:if>
					  }
					  else if(basisValue=='cft'){
					  document.forms['operationResourceForm'].elements[quantity].value=document.forms['operationResourceForm'].elements['miscellaneous.netEstimateCubicFeet'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.netEstimateCubicFeet'].value;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.netActualCubicFeet'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.netActualCubicFeet'].value;
					  </c:if>		  	  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.netActualCubicFeet'].value;
					  </c:if>
					  }
					  
					  else if(basisValue=='cbm'){
					  document.forms['operationResourceForm'].elements[quantity].value=document.forms['operationResourceForm'].elements['miscellaneous.netEstimateCubicMtr'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.netEstimateCubicMtr'].value;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.netActualCubicMtr'].value;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.netActualCubicMtr'].value;
					  </c:if>		  	  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=document.forms['operationResourceForm'].elements['miscellaneous.netActualCubicMtr'].value;
					  </c:if>
					  }
					  else if(basisValue=='each'){
					  document.forms['operationResourceForm'].elements[quantity].value=1;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=1;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=1;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=1;
					  </c:if>		  	  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=1;
					  </c:if>
					  }
					  else if(basisValue=='value'){
					  document.forms['operationResourceForm'].elements[quantity].value=1;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=1;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=1;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=1;
					  </c:if>		  		  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=1;
					  </c:if>
					  }
					  else if(basisValue=='flat'){
					  document.forms['operationResourceForm'].elements[quantity].value=1;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=1;
					  </c:if>
					  
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=1;
					  <c:if test="${contractType}">
					  document.forms['operationResourceForm'].elements['revisionSellQuantity'+aid].value=1;
					  </c:if>		  	  
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  document.forms['operationResourceForm'].elements['recQuantity'+aid].value=1;
					  </c:if>
					  } }
					  calculateExpense(basis,quantity,buyRate,expense,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid);
					  calculateRevenue(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid,'','','');
					  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  calculateRevisionBlock(aid,'BAS','','','');
					  </c:if>
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					  calculateActualRevenue(aid,targetElement,'BAS','','','');
					  </c:if>
						}
				var http88 = getHTTPObject88(); 
		    	 var httpTemplate = getHTTPObject88();
		    	 var http5513 = getHTTPObject88();
		    	 var httpCMMAgentInvoiceCheck =	getHTTPObject77(); 	
		    	 function getHTTPObject77() {
		    		    var xmlhttp;
		    		    if(window.XMLHttpRequest)  {
		    		        xmlhttp = new XMLHttpRequest();
		    		    }
		    		    else if (window.ActiveXObject) {
		    		        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		    		        if (!xmlhttp) {
		    		            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		    		        }
		    		    }
		    		    return xmlhttp;
		    		}
		    	 function getHTTPObject88() {
			    	    var xmlhttp;
			    	    if(window.XMLHttpRequest) {
			    	        xmlhttp = new XMLHttpRequest();
			    	    }
			    	    else if (window.ActiveXObject)  {
			    	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			    	        if (!xmlhttp)   {
			    	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
			    	        }  }
			    	    return xmlhttp;
			    	   }
				 function findRevisedEstimateQuantitys(chargeCode, category, estimateExpense, basis, estimateQuantity, estimateRate, estimateSellRate, estimateRevenueAmount, estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid){
						var charge = document.forms['operationResourceForm'].elements[chargeCode].value;
						var billingContract = document.forms['operationResourceForm'].elements['billing.contract'].value;
						var shipNumber=document.forms['operationResourceForm'].elements['serviceOrder.shipNumber'].value;
						var rev=document.forms['operationResourceForm'].elements[category].value;
						var comptetive = document.forms['operationResourceForm'].elements['inComptetive'].value;
						if(charge!=''){
						if(rev == "Internal Cost"){
						    if(charge=="VANPAY" ){
						     	getVanPayCharges("Estimate");
						    }else if(charge=="DOMCOMM" ){
						     	getDomCommCharges("Estimate");
						    }else if(charge=="SALESCM" && comptetive=="Y") { 
							    var totalExpense = eval(document.forms['operationResourceForm'].elements['inEstimatedTotalExpense'].value);
							    var totalRevenue =eval(document.forms['operationResourceForm'].elements['inEstimatedTotalRevenue'].value); 
							    var commisionRate =eval(document.forms['operationResourceForm'].elements['salesCommisionRate'].value); 
							    var grossMargin = eval(document.forms['operationResourceForm'].elements['grossMarginThreshold'].value);
							    var commision=((totalRevenue*commisionRate)/100);
							    var calGrossMargin=((totalRevenue-(totalExpense + commision))/totalRevenue);
							    calGrossMargin=calGrossMargin*100;
							    var calGrossMargin=Math.round(calGrossMargin);
							    if(calGrossMargin>=grossMargin)  {
							    	document.forms['operationResourceForm'].elements[estimateExpense].value=Math.round(commision*10000)/10000; 
							    	document.forms['operationResourceForm'].elements[basis].value='each';
							    	document.forms['operationResourceForm'].elements[estimateQuantity].value=1;
								  <c:if test="${contractType}">
								  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=1;
								  </c:if>			    	
							    } else { 
								    commision=((.8*totalRevenue)-totalExpense); 
								    commision=Math.round(commision*10000)/10000;
								    if(commision<0) { 
								    	document.forms['operationResourceForm'].elements[estimateExpense].value=0; 
								    	document.forms['operationResourceForm'].elements[basis].value='each';
								    	document.forms['operationResourceForm'].elements[estimateQuantity].value=1;
										  <c:if test="${contractType}">
										  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=1;
										  </c:if>			    					    	
								    } else {
								    	document.forms['operationResourceForm'].elements[estimateExpense].value=Math.round(commision*10000)/10000;
								    	document.forms['operationResourceForm'].elements[basis].value='each';
								    	document.forms['operationResourceForm'].elements[estimateQuantity].value=1;
										  <c:if test="${contractType}">
										  document.forms['operationResourceForm'].elements['estimateSellQuantity'+aid].value=1;
										  </c:if>			    					    	
								    } 
							    }  
						    }else if(charge=="SALESCM" && comptetive!="Y") {
						    var agree =confirm("This job is not competitive; do you still want to calculate commission?");
						    if(agree) {
						    var accountCompanyDivision =  document.forms['operationResourceForm'].elements['serviceOrder.companyDivision'].value 
						    var url="internalCostsExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=${serviceOrder.id}";
						    http88.open("GET", url, true);
						    http88.onreadystatechange = function(){ handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate,basis,aid);};
						    http88.send(null);
						    } else {
						    return false;
						    }
						    } else {
						    var accountCompanyDivision =  document.forms['operationResourceForm'].elements['serviceOrder.companyDivision'].value 
						    var url="internalCostsExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=${serviceOrder.id}";
						    http88.open("GET", url, true);
						    http88.onreadystatechange = function(){ handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate, basis,aid);};
						    http88.send(null);
						    }
						} else{	
						var url="invoiceExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=${serviceOrder.id}";
						    http88.open("GET", url, true);
						    http88.onreadystatechange = function(){ handleHttpResponse701(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);};
						    http88.send(null);
						}
					  }else{
					    alert("Please select chargeCode!");
					 } 
				   }
				 
				 function handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate, basis,aid) {
					    if (http88.readyState == 4)  {
					            var results = http88.responseText
					            results = results.trim(); 
					            var res = results.split("#");
					            if(res[3]=="AskUser1"){
					            var reply = prompt("Please Enter Quantity", "");
					            if(reply)   {
					            if((reply>0 || reply<9)|| reply=='.') {
					         	    estimateQuantity.value = Math.round(reply*100)/100;
					         	}  else  {
					         		alert("Please Enter legitimate Quantity");
					         	} }
					         	 else{
					         		document.getElementById(estimateQuantity).value = document.getElementById(estimateQuantity).value;
					  			  <c:if test="${contractType}">
								  document.getElementById('estimateSellQuantity'+aid).value=document.getElementById('estimateSellQuantity'+aid).value;
								  </c:if>
					         }  }
					         else{
					         if(res[1] == undefined){
					        	 document.getElementById(estimateQuantity).value ="0.00";
					 			  <c:if test="${contractType}">
								  document.getElementById('estimateSellQuantity'+aid).value="0.00";
								  </c:if>
					        	 
					         }else{
					        	 document.getElementById(estimateQuantity).value = Math.round(res[1]*100)/100 ;
					 			  <c:if test="${contractType}">
								  document.getElementById('estimateSellQuantity'+aid).value=Math.round(res[1]*100)/100 ;
								  </c:if>
					        	 
					         }  }
					         if(res[5]=="AskUser"){
					         var reply = prompt("Please Enter the value of Rate", "");
					         if(reply)  {
					         if((reply>0 || reply<9)|| reply=='.')  {
					        	 document.getElementById(estimateRate).value =Math.round(reply*10000)/10000 ;
					         	} else {
					         		alert("Please Enter legitimate Rate");
					         	} 	}
					         	 else{
					         		document.getElementById(estimateRate).value = document.getElementById(estimateRate).value;
					         } 
					         }else{
					         if(res[4] == undefined){
					        	 document.getElementById(estimateRate).value ="0.00";
					         }else{
					        	 document.getElementById(estimateRate).value = Math.round(res[4]*10000)/10000 ;
					         }  }
					         if(res[5]=="BuildFormula")
					         {
					         if(res[6] == undefined){
					        	 document.getElementById(estimateRate).value ="0.00";
					         }else{
					        	 document.getElementById(estimateRate).value = Math.round(res[6]*10000)/10000;
					         }  }
					        var Q1 = document.getElementById(estimateQuantity).value;
					        var Q2 = document.getElementById(estimateRate).value;
					        if(res[5]=="BuildFormula" && res[6] != undefined ){
					        	Q2 = res[6];
					        }
					        var E1=Q1*Q2;
					        var E2="";
					        E2=Math.round(E1*10000)/10000;
					        document.getElementById(estimateExpense).value = E2;
					        var type = res[8];
					          var typeConversion=res[10];
					          if(type == 'Division')  {
					          	 if(typeConversion==0)  {
					          	   E1=0*1;
					          	 document.getElementById(estimateExpense).value = E1;
					          	 } else  {	
					          		E1=(Q1*Q2)/typeConversion;
					          		E2=Math.round(E1*10000)/10000;
					          		document.getElementById(estimateExpense).value = E2;
					          	 }  }
					          if(type == ' ' || type == '')  {
					          		E1=(Q1*Q2);
					          		E2=Math.round(E1*10000)/10000;
					          		document.getElementById(estimateExpense).value = E2;
					          }
					          if(type == 'Multiply')  {
					          		E1=(Q1*Q2*typeConversion);
					          		E2=Math.round(E1*10000)/10000;
					          		document.getElementById(estimateExpense).value = E2;
					          } 
					          basis.value=res[9];

					         document.forms['operationResourceForm'].elements['oldEstimateSellDeviation'+aid].value=0;
					         document.forms['operationResourceForm'].elements['estimateDeviation'+aid].value=0;
					         document.forms['operationResourceForm'].elements['oldEstimateDeviation'+aid].value=0;
					         document.forms['operationResourceForm'].elements['estimateSellDeviation'+aid].value=0; 
					      }  
					}
				 
				 function handleHttpResponse701(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote, deviation,estimateDeviation, estimateSellDeviation,aid){
					     if (http88.readyState == 4)  {
					        var results = http88.responseText;
					        results = results.trim(); 

					      <c:if test="${!contractType}">
					        var res = results.split("#");
					        var multquantity=res[14];
				        if(res[17] == undefined){             
				        	document.getElementById(estimateRate).value ="0.00";
				        }else{                  
				        	document.getElementById(estimateRate).value = Math.round(res[17]*10000)/10000 ;
				        }
					        if(res[3]=="AskUser1"){ 
					           var reply = prompt("Please Enter Quantity", "");
					             if(reply) {
				  	             if((reply>0 || reply<9)|| reply=='.')  {
				  	            	document.getElementById(estimateQuantity).value = Math.round(reply*100)/100;
				  	 			  <c:if test="${contractType}">
				  				  document.getElementById('estimateSellQuantity'+aid).value=Math.round(reply*100)/100 ;
				  				  </c:if>
				  	             }else {
				  	                alert("Please Enter legitimate Quantity");
				  	             } 
					             }else{
					            	 document.getElementById(estimateQuantity).value = document.getElementById(estimateQuantity).value;
					  	 			  <c:if test="${contractType}">
					  				  document.getElementById('estimateSellQuantity'+aid).value=document.getElementById('estimateSellQuantity'+aid).value;
					  				  </c:if>
					             }
				       	}else{             
				            if(res[1] == undefined){
				            	document.getElementById(estimateQuantity).value ="0.00";
					 			  <c:if test="${contractType}">
				  				  document.getElementById('estimateSellQuantity'+aid).value="0.00";
				  				  </c:if>
				        	
				            }else{
				            	document.getElementById(estimateQuantity).value = Math.round(res[1]*100)/100;
					 			  <c:if test="${contractType}">
				  				  document.getElementById('estimateSellQuantity'+aid).value=Math.round(res[1]*100)/100;
				  				  </c:if>
				        	
				            }  
				       	}
					       	if(res[5]=="AskUser"){ 
				           var reply = prompt("Please Enter the value of Rate", "");
				           if(reply)  {
				               if((reply>0 || reply<9)|| reply=='.') {
				            	   document.getElementById(estimateSellRate).value = Math.round(reply*10000)/10000;
				               } else {
				               		alert("Please Enter legitimate Rate");
				               	} 
				            }else{
				            	document.getElementById(estimateRate).value = document.getElementById(estimateRate).value;
				           	} 
					       	}else{
				           if(res[4] == undefined || res[4] == '' || res[4] == null){
				        	   document.getElementById(estimateSellRate).value ="0.00";
				           }else{
				        	   document.getElementById(estimateSellRate).value = Math.round(res[4]*10000)/10000 ;
				           }  
					      	 }
					        if(res[5]=="BuildFormula"){
					          if(res[6] == undefined){
					        	  document.getElementById(estimateSellRate).value ="0.00";
					           }else{
					        	   document.getElementById(estimateSellRate).value = Math.round(res[6]*10000)/10000 ;
					           }  
				         }

					          
				        var Q1 = document.getElementById(estimateQuantity).value;
						  <c:if test="${contractType}">
						  Q1=document.getElementById('estimateSellQuantity'+aid).value;
						  </c:if>
				        var Q2 = document.getElementById(estimateSellRate).value;
				        if(res[5]=="BuildFormula" && res[6] != undefined){
				        	Q2 =res[6];
				        }
				        var oldRevenue = eval(document.getElementById(estimateRevenueAmount).value);
				        var oldExpense=eval(document.getElementById(estimateExpense).value);
				        var E1=Q1*Q2;
				        var E2="";
				        E2=Math.round(E1*10000)/10000;
				        document.getElementById(estimateRevenueAmount).value = E2;
				        var type = res[8];
				          var typeConversion=res[10];   
				          if(type == 'Division')   {
				          	 if(typeConversion==0) 	 {
				          	   E1=0*1;
				          	 document.getElementById(estimateRevenueAmount).value = E1;
				          	 } else {	
				          		E1=(Q1*Q2)/typeConversion;
				          		E2=Math.round(E1*10000)/10000;
				          		document.getElementById(estimateRevenueAmount).value = E2;
				          	 } }
				          if(type == ' ' || type == '') {
				          		E1=(Q1*Q2);
				          		E2=Math.round(E1*10000)/10000;
				          		document.getElementById(estimateRevenueAmount).value = E2;
				          }
				          if(type == 'Multiply')  {
				          		E1=(Q1*Q2*typeConversion);
				          		E2=Math.round(E1*10000)/10000;
				          		document.getElementById(estimateRevenueAmount).value = E2;
				          }

				          var Q11 = document.getElementById(estimateQuantity).value;
				          var Q22 = document.getElementById(estimateRate).value;
				          var E11=Q11*Q22;
				          var E22="";
				          E22=Math.round(E11*10000)/10000;
				          document.getElementById(estimateExpense).value = E22;
				            if(type == 'Division')  {
				            	 if(typeConversion==0)  {
				            	   E11=0*1;
				            	   document.getElementById(estimateExpense).value = E11;
				            	 } else  {	
				            		E11=(Q11*Q22)/typeConversion;
				            		E22=Math.round(E11*10000)/10000;
				            		document.getElementById(estimateExpense).value = E22;
				            	 }  }
				            if(type == ' ' || type == '')  {
				            		E11=(Q11*Q22);
				            		E22=Math.round(E11*10000)/10000;
				            		document.getElementById(estimateExpense).value = E22;
				            }
				            if(type == 'Multiply')  {
				            		E11=(Q11*Q22*typeConversion);
				            		E22=Math.round(E11*10000)/10000;
				            		document.getElementById(estimateExpense).value = E22;
				            }
					             
				            document.getElementById(basis).value=res[9];
				         var chargedeviation=res[15];
				         if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
				        	 var buyDependSellAccount=res[11];
				             var estimateRevenueAmt=0;
				             var finalEstimateRevenueAmount=0; 
				             var finalEstimateExpenceAmount=0;
				             var estimateSellRates=0; 
				             var sellDeviation=0;
				             var estimatepasspercentages=0;
				             var estimatepasspercentageRound=0;
				             var buyDeviation=0;
				             var oldEstDeviation =  document.getElementById(estimateDeviation).value;
				             var oldEstSellDeviation = document.getElementById(estimateSellDeviation).value;
				             sellDeviation=res[12];
				             buyDeviation=res[13]; 
				             document.getElementById(estimateSellDeviation).value=sellDeviation;
				             document.getElementById(estimateDeviation).value=buyDeviation;
					        
				             document.getElementById(deviation).value=chargedeviation; 
				             document.forms['operationResourceForm'].elements['buyDependSell'].value=res[11];
				            
				             estimateRevenueAmt=document.getElementById(estimateRevenueAmount).value; 
				             if(sellDeviation==0 && sellDeviation==undefined){
				            	 sellDeviation = document.getElementById(oldEstSellDeviation).value;
					         }

				             finalEstimateRevenueAmount=Math.round((estimateRevenueAmt*(sellDeviation/100))*10000)/10000; 
				             document.getElementById(estimateRevenueAmount).value=finalEstimateRevenueAmount;
				             if(buyDeviation==0 && buyDeviation==undefined){
				            	 buyDeviation = document.getElementById(oldEstDeviation).value;
					         }

				             if(buyDependSellAccount=="Y"){
				              	finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(buyDeviation/100))*10000)/10000;
				             }else{
				              	finalEstimateExpenceAmount= Math.round((estimateRevenueAmt*(buyDeviation/100))*10000)/10000;
				             }
				             document.getElementById(estimateExpense).value=finalEstimateExpenceAmount;
				             estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
				 	         estimatepasspercentageRound=Math.round(estimatepasspercentages);
				 	         if(document.getElementById(estimateExpense).value == '' || document.getElementById(estimateExpense).value == 0){
				 	        	document.getElementById(estimatePassPercentage).value='';
				 	         }else{
				 	        	document.getElementById(estimatePassPercentage).value=estimatepasspercentageRound;
				 	         }
				 	         // calculate estimate rate 
				 	        var estimateQuantity1 =0;
				 		    var estimateDeviation1=100;
				 		    estimateQuantity1= document.getElementById(estimateQuantity).value; 
				 		    var basis1 = document.getElementById(basis).value; 
				 		    var estimateExpense1 =eval(document.getElementById(estimateExpense).value);
				 		    //alert(estimateExpense1) 
				 		    var estimateRate1=0;
				 		    var estimateRateRound1=0; 
				 		    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
				 	         estimateQuantity1 = document.getElementById(estimateQuantity).value=1;
				 	         } 
				 	        if( basis1=="cwt" || basis1=="%age"){
				 	       	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
				 	       	  estimateRateRound1=Math.round(estimateRate1*10000)/10000;
				 	       		document.getElementById(estimateRate).value=estimateRateRound1;	  	
				 	       	}  else if(basis1=="per 1000"){
				 	       	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
				 	       	  estimateRateRound1=Math.round(estimateRate1*10000)/10000;
				 	       	document.getElementById(estimateRate).value=estimateRateRound1;		  	
				 	       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
				 	          estimateRate1=(estimateExpense1/estimateQuantity1); 
				 	       	  estimateRateRound1=Math.round(estimateRate1*10000)/10000; 
				 	       		document.getElementById(estimateRate).value=estimateRateRound1;		  	
				 		    }
				 		    
				 		    estimateDeviation1= document.getElementById(estimateDeviation).value 
				 		    if(estimateDeviation1!='' && estimateDeviation1>0){
					 		    estimateRate1=estimateRateRound1*100/estimateDeviation1;
					 		    estimateRateRound1=Math.round(estimateRate1*10000)/10000;
					 		    document.getElementById(estimateRate).value=estimateRateRound1;		
				 		    }
				         }else {
				        	 document.getElementById(estimateDeviation).value=0.00;
				        	 document.getElementById(estimateSellDeviation).value=0.00;
				             var bayRate=0
				             bayRate = res[17] ;
				             if(bayRate!=0 && bayRate!=undefined){
				               var estimateQuantityPay=0;
				               estimateQuantityPay= document.getElementById(estimateQuantity).value;
				               Q1=estimateQuantityPay;
				               Q2=bayRate; 
				               if(type == 'Division')   {
				               	 if(typeConversion==0) 	 {
				               	   E1=0*1;
				               		document.getElementById(estimateExpense).value = E1;
				               	 } else {	
				               		E1=(Q1*Q2)/typeConversion;
				               		E2=Math.round(E1*10000)/10000;
				               		document.getElementById(estimateExpense).value = E2;
				               	 } }
				               if(type == ' ' || type == '') {
				               		E1=(Q1*Q2);
				               		E2=Math.round(E1*10000)/10000;
				               		document.getElementById(estimateExpense).value = E2;
				               }
				               if(type == 'Multiply')  {
				               		E1=(Q1*Q2*typeConversion);
				               		E2=Math.round(E1*10000)/10000;
				               		document.getElementById(estimateExpense).value = E2;
				               }
				               document.getElementById(estimateRate).value = Q2;
				               var  estimatepasspercentages=0
				               var estimatepasspercentageRound=0;
				               var finalEstimateRevenueAmount=0; 
				               var finalEstimateExpenceAmount=0;
				               finalEstimateRevenueAmount=  document.getElementById(estimateRevenueAmount).value;
				               finalEstimateExpenceAmount= document.getElementById(estimateExpense).value;
				               estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
				  	           estimatepasspercentageRound=Math.round(estimatepasspercentages);
				  	           if(finalEstimateExpenceAmount == '' || finalEstimateExpenceAmount == 0.00 || finalEstimateExpenceAmount == 0){
				  	        	 document.getElementById(estimatePassPercentage).value='';
				  	           }else{
				  	        	 document.getElementById(estimatePassPercentage).value=estimatepasspercentageRound;
				  	           } 
				             }else{	           	   
				               var quantity =0;
				               var rate =0;
				               var estimatepasspercentages=0;
				               var estimatepasspercentageRound=0;
				               var finalEstimateRevenueAmount=0; 
				               var finalEstimateExpenceAmount=0;
				               quantity= document.getElementById(estimateQuantity).value;	               
				               rate= document.getElementById(estimateRate).value;	               
				               finalEstimateExpenceAmount = (quantity*rate); 
				               if( document.getElementById(basis).value=="cwt" || document.getElementById(basis).value=="%age")  {
				      	          finalEstimateExpenceAmount = finalEstimateExpenceAmount/100; 
				               }
				               if( document.getElementById(basis).value=="per 1000")  {
				      	        	finalEstimateExpenceAmount = finalEstimateExpenceAmount/1000; 
				               }
				               finalEstimateExpenceAmount= Math.round((finalEstimateExpenceAmount)*10000)/10000;
				               finalEstimateRevenueAmount=  document.getElementById(estimateRevenueAmount).value;
				               estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
				  	           estimatepasspercentageRound=Math.round(estimatepasspercentages);
				   
				  	           if(finalEstimateExpenceAmount == '' || finalEstimateExpenceAmount == 0.00 || finalEstimateExpenceAmount == 0){
				  	        	 document.getElementById(estimatePassPercentage).value='';
				  	           }else{
				  	        	 document.getElementById(estimatePassPercentage).value=estimatepasspercentageRound;
				  	           }
				            }
				             
				        }

				       //#7639 - Issue with Compute functionality in Account line and Pricing section start
				  	                   
				           //#7639 - Issue with Compute functionality in Account line and Pricing section End
				           
				/////////changes
				         try{
					     	    var basisValue = document.getElementById(basis).value;
					    	    checkFloatNew('operationResourceForm',estimateQuantity,'Nothing');
					    	    var quantityValue = document.getElementById(estimateQuantity).value;
					    	    		             
					 	        var buyRate1= document.getElementById('estimateRate'+aid).value;
						        var estCurrency1= document.getElementById('estCurrencyNew'+aid).value;
								if(estCurrency1.trim()!=""){
										var Q1=0;
										var estimateExpenseQ1=0;
										var Q2=0;
										Q1=buyRate1;
										Q2=document.getElementById('estExchangeRateNew'+aid).value;
							            var E1=Q1*Q2;
							            E1=Math.round(E1*10000)/10000;
							            document.getElementById('estLocalRateNew'+aid).value=E1;
							            var estLocalAmt1=0;
							            estimateExpenseQ1=document.getElementById(estimateExpense).value;
							            estLocalAmt1=estimateExpenseQ1*Q2; 
							            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
							            document.getElementById('estLocalAmountNew'+aid).value=estLocalAmt1;     
								}

						        buyRate1= document.getElementById('estimateSellRate'+aid).value;
						        estCurrency1= document.getElementById('estSellCurrencyNew'+aid).value;
								if(estCurrency1.trim()!=""){
										var Q1=0;
										var Q2=0;
										var estimateRevenueAmountQ1=0;
										Q1=buyRate1;
										Q2= document.getElementById('estSellExchangeRateNew'+aid).value;
							            var E1=Q1*Q2;
							            E1=Math.round(E1*10000)/10000;
							            document.getElementById('estSellLocalRateNew'+aid).value=E1;
							            var estLocalAmt1=0;
							            estimateRevenueAmountQ1=document.getElementById(estimateRevenueAmount).value;
							            estLocalAmt1=estimateRevenueAmountQ1*Q2; 
							            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000; 
							            document.getElementById('estSellLocalAmountNew'+aid).value=estLocalAmt1;     
								}
								var type01 = res[8];
							     var typeConversion01=res[10]; 					
								//#7298 - Copy esimates not calculating preset calculations in charges start
								  //#7298 - Copy esimates not calculating preset calculations in charges End						
								}catch(e){}
						        <c:if test="${systemDefaultVatCalculationNew=='Y'}">
								try{
							    calculateVatAmt('REVENUE',aid);
							    calculateVatAmt('EXPENSE',aid);
								}catch(e){}
						        </c:if> 					
				//////////changes
				            //--------------------Start--estimateRevenueAmount-------------------------//
				               var revenueValue= document.getElementById(estimateRevenueAmount).value;
				  	           var revenueRound=0;
				  	           var oldRevenueSum=0;
				  	           var balanceRevenue=0;
				  	           <c:if test="${not empty serviceOrder.id}">
				  	           	oldRevenueSum=eval(document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value);
				  	           </c:if> 	  	          
				  	           revenueRound=Math.round(revenueValue*10000)/10000;
				  	           balanceRevenue=revenueRound-oldRevenue;
				  	           oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
				  	           balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
				  	           oldRevenueSum=oldRevenueSum+balanceRevenue; 
				  	           oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
				  	           var revenueRoundNew=""+revenueRound;
				  	           if(revenueRoundNew.indexOf(".") == -1){
				  	           revenueRoundNew=revenueRoundNew+".00";
				  	           } 
				  	           if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
				  	           revenueRoundNew=revenueRoundNew+"0";
				  	           }
				  	         document.getElementById(estimateRevenueAmount).value=revenueRoundNew;
				  	           <c:if test="${not empty serviceOrder.id}">
				  	  	           var newRevenueSum=""+oldRevenueSum;
				  	  	           if(newRevenueSum.indexOf(".") == -1){
				  	  	           	newRevenueSum=newRevenueSum+".00";
				  	  	           } 
				  	  	           if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
				  	  	           	newRevenueSum=newRevenueSum+"0";
				  	  	           } 
				  	  	       document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value=newRevenueSum;
				  	           </c:if>
				  	           if(revenueRoundNew>0){
				  	        	 document.getElementById(displayOnQuote).checked = true;
				  	           }
				  	       //--------------------End--estimateRevenueAmount-------------------------//
				  	       
				           //-------------------Start---estimateExpense-------------------------//                
				              var expenseValue= document.getElementById(estimateExpense).value;
				              var expenseRound=0;
				              var oldExpenseSum=0;
				              var balanceExpense=0;
				              <c:if test="${not empty serviceOrder.id}">
				              	oldExpenseSum=eval(document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value);
				              </c:if>
				              expenseRound=Math.round(expenseValue*10000)/10000;
				              balanceExpense=expenseRound-oldExpense;
				              oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
				              balanceExpense=Math.round(balanceExpense*10000)/10000; 
				              oldExpenseSum=oldExpenseSum+balanceExpense;
				              oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
				              var expenseRoundNew=""+expenseRound;
				              if(expenseRoundNew.indexOf(".") == -1){
				              	expenseRoundNew=expenseRoundNew+".00";
				              } 
				              if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
				              	expenseRoundNew=expenseRoundNew+"0";
				              }
				              document.getElementById(estimateExpense).value=expenseRoundNew;
				              <c:if test="${not empty serviceOrder.id}">
					              var newExpenseSum=""+oldExpenseSum;
					              if(newExpenseSum.indexOf(".") == -1){
					              	newExpenseSum=newExpenseSum+".00";
					              } 
					              if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
					              	newExpenseSum=newExpenseSum+"0";
					              } 
					              document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value=newExpenseSum;
				              </c:if>
				              if(expenseRoundNew>0){
				            	  document.getElementById(displayOnQuote).checked = true;
				              }
				              //-------------------End---estimateExpense-------------------------//
				              			            
				              calculateGrossMargin();
					            </c:if>
					            <c:if test="${contractType}">
					            
						 	       var res = results.split("#"); 
							 	  	<c:choose>
						 			 <c:when test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
							 	         if(document.getElementById('estimateContractCurrencyNew'+aid).value!=res[18]){
							 	        	document.getElementById('estimateContractCurrencyNew'+aid).value=res[18]; 
							 	          rateOnActualizationDate('estSellCurrencyNew'+aid+'~estimateContractCurrencyNew'+aid,'estSellValueDateNew'+aid+'~estimateContractValueDateNew'+aid,'estSellExchangeRateNew'+aid+'~estimateContractExchangeRateNew'+aid);
							 	         }
						 	         </c:when>
						 	         <c:otherwise>
						 	        document.getElementById('estimateContractCurrencyNew'+aid).value=res[18];
						 	        document.getElementById('estimateContractValueDateNew'+aid).value=currentDateRateOnActualization();
						 	        document.getElementById('estimateContractExchangeRateNew'+aid).value=findExchangeRateRateOnActualization(res[18]);
						 	         </c:otherwise>
						 	    </c:choose>
							 	   document.getElementById('estimatePayableContractCurrencyNew'+aid).value=res[19];
							 	   document.getElementById('estimatePayableContractValueDateNew'+aid).value=currentDateRateOnActualization();
							 	   document.getElementById('estimatePayableContractExchangeRateNew'+aid).value=findExchangeRateRateOnActualization(res[19]);
//				            	   findExchangeContractRateByChargesNew('payable',aid);
					               var multquantity=res[14]
					              if(res[3]=="AskUser1"){ 
					               var reply = prompt("Please Enter Quantity", "");
					               if(reply) {
					               if((reply>0 || reply<9)|| reply=='.')  {
					            	   document.getElementById(estimateQuantity).value = Math.round(reply*100)/100;
					            	   <c:if test="${contractType}">
						  				  document.getElementById('estimateSellQuantity'+aid).value=Math.round(reply*100)/100;
						  				  </c:if>
					            	} else {
					            		alert("Please Enter legitimate Quantity");
					            	} }
					            	else{
					            		document.getElementById(estimateQuantity).value =document.getElementById(estimateQuantity).value ;
					            		<c:if test="${contractType}">
						  				  document.getElementById('estimateSellQuantity'+aid).value=document.getElementById('estimateSellQuantity'+aid).value;
						  				  </c:if>
					            	}
					            }else{             
					            if(res[1] == undefined){
					            	document.getElementById(estimateQuantity).value ="";
					            	<c:if test="${contractType}">
					  				  document.getElementById('estimateSellQuantity'+aid).value="";
					  				  </c:if>
					            }else{
					            	document.getElementById(estimateQuantity).value = Math.round(res[1]*100)/100;
					            	<c:if test="${contractType}">
					  				  document.getElementById('estimateSellQuantity'+aid).value= Math.round(res[1]*100)/100;
					  				  </c:if>
					            }  }
					            if(res[5]=="AskUser"){ 
					            var reply = prompt("Please Enter the value of Rate", "");
					            if(reply)  {
					            if((reply>0 || reply<9)|| reply=='.') {
					            	document.getElementById('estimateContractRateNew'+aid).value = Math.round(reply*10000)/100000;
					            	} else {
					            		alert("Please Enter legitimate Rate");
					            	} }
					            	else{
					            		document.getElementById('estimateContractRateNew'+aid).value = document.getElementById(estimateRate).value;
					            	} 
					            }else{
					            if(res[4] == undefined){
					            	document.getElementById('estimateContractRateNew'+aid).value ="";
					            }else{
					            	document.getElementById('estimateContractRateNew'+aid).value =Math.round(res[4]*10000)/10000 ;
					            }  }
					            if(res[5]=="BuildFormula")   {
					            if(res[6] == undefined){
					            	document.getElementById('estimateContractRateNew'+aid).value ="";
					            }else{
					            	document.getElementById('estimateContractRateNew'+aid).value =Math.round(res[6]*10000)/10000 ;
					            }  }
					           var Q1 = document.getElementById(estimateQuantity).value;
					           var Q2 = document.getElementById('estimateContractRateNew'+aid).value;
					           if(res[5]=="BuildFormula" && res[6] != undefined){
					        	   Q2 =res[6];
					           }
					            var oldRevenue = eval(document.getElementById(estimateRevenueAmount).value);
					            var oldExpense=eval(document.getElementById(estimateExpense).value);		           
					           var E1=Q1*Q2;
					           var E2="";
					           E2=Math.round(E1*10000)/10000;
					           document.getElementById('estimateContractRateAmmountNew'+aid).value = E2;
					           var type = res[8];
					             var typeConversion=res[10];   
					             if(type == 'Division')   {
					             	 if(typeConversion==0) 	 {
					             	   E1=0*1;
					             	  document.getElementById('estimateContractRateAmmountNew'+aid).value = E1;
					             	 } else {	
					             		E1=(Q1*Q2)/typeConversion;
					             		E2=Math.round(E1*10000)/10000;
					             		document.getElementById('estimateContractRateAmmountNew'+aid).value = E2;
					             	 } }
					             if(type == ' ' || type == '') {
					             		E1=(Q1*Q2);
					             		E2=Math.round(E1*10000)/10000;
					             		document.getElementById('estimateContractRateAmmountNew'+aid).value = E2;
					             }
					             if(type == 'Multiply')  {
					             		E1=(Q1*Q2*typeConversion);
					             		E2=Math.round(E1*10000)/10000;
					             		document.getElementById('estimateContractRateAmmountNew'+aid).value = E2;
					             } 
					             document.getElementById(basis).value=res[9];
					             var chargedeviation=res[15];
					             document.getElementById(deviation).value=chargedeviation;
					             if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){ 
					            	 document.getElementById('buyDependSellNew'+aid).value=res[11];
					             var buyDependSellAccount=res[11];
					             var estimateRevenueAmount1=0;
					             var finalEstimateRevenueAmount=0; 
					             var finalEstimateExpenceAmount=0;
					             var finalEstimateContractRateAmmount=0;
					             var estimateContractRateAmmount1=0;
					             var estimatePayableContractRateAmmount1=0;
					             var estimateSellRate1=0; 
					             var sellDeviation1=0;
					             var  estimatepasspercentage=0
					             var estimatepasspercentageRound=0;
					             var buyDeviation1=0;
					             sellDeviation1=res[12]
					             buyDeviation1=res[13] 
					             var estimateContractExchangeRate1=document.getElementById('estimateContractExchangeRateNew'+aid).value
					             estimateContractRateAmmount1 =document.getElementById('estimateContractRateAmmountNew'+aid).value
					             estimateRevenueAmount1=estimateContractRateAmmount1/estimateContractExchangeRate1;
					             estimateRevenueAmount1=Math.round(estimateRevenueAmount1*10000)/10000;
					             estimateRevenueAmount1=document.getElementById('estimateContractRateAmmountNew'+aid).value;  
					             finalEstimateRevenueAmount=Math.round((estimateRevenueAmount1*(sellDeviation1/100))*10000)/10000;
					             finalEstimateContractRateAmmount=Math.round((estimateContractRateAmmount1*(sellDeviation1/100))*10000)/10000;
					             document.getElementById('estimateContractRateAmmountNew'+aid).value=finalEstimateContractRateAmmount; 
					             if(buyDependSellAccount=="Y"){
					              finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(buyDeviation1/100))*10000)/10000;
					             }else{
					              finalEstimateExpenceAmount= Math.round((estimateRevenueAmount1*(buyDeviation1/100))*10000)/10000;
					             }
					             document.getElementById(estimateSellDeviation).value=sellDeviation1;
					             document.getElementById('oldEstimateSellDeviationNew'+aid).value=sellDeviation1;
					             document.getElementById(estimateDeviation).value=buyDeviation1;
					             document.getElementById('oldEstimateDeviationNew'+aid).value=buyDeviation1;
						    	 var estimatePayableContractExchangeRate=document.forms['operationResourceForm'].elements['estimatePayableContractExchangeRateNew'+aid].value
						 	    estimatePayableContractRateAmmount1=finalEstimateExpenceAmount*estimatePayableContractExchangeRate;
					             estimatePayableContractRateAmmount1=Math.round(estimatePayableContractRateAmmount1*10000)/10000;
					             document.getElementById('estimatePayableContractRateAmmountNew'+aid).value=estimatePayableContractRateAmmount1;
					             calEstimatePayableContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
					             }else {
					            	 document.getElementById(estimateSellDeviation).value=0;
					             document.getElementById('oldEstimateSellDeviationNew'+aid).value=0;
					             document.getElementById(estimateDeviation).value=0;
					             document.getElementById('oldEstimateDeviationNew'+aid).value=0;
					             var bayRate=0
					             bayRate = res[17] ;
					             if(bayRate!=0){
					             var estimateQuantityPay=0;
					             estimateQuantityPay= document.getElementById(estimateQuantity).value;
					             Q1=estimateQuantityPay;
					             Q2=bayRate; 
					             if(type == 'Division')   {
					             	 if(typeConversion==0) 	 {
					             	   E1=0*1;
					             	  document.getElementById('estimatePayableContractRateAmmountNew'+aid).value = E1;
					             	 } else {	
					             		E1=(Q1*Q2)/typeConversion;
					             		E2=Math.round(E1*10000)/10000;
					             		document.getElementById('estimatePayableContractRateAmmountNew'+aid).value = E2;
					             	 } }
					             if(type == ' ' || type == '') {
					             		E1=(Q1*Q2);
					             		E2=Math.round(E1*10000)/10000;
					             		document.getElementById('estimatePayableContractRateAmmountNew'+aid).value = E2;
					             }
					             if(type == 'Multiply')  {
					             		E1=(Q1*Q2*typeConversion);
					             		E2=Math.round(E1*10000)/10000;
					             		document.getElementById('estimatePayableContractRateAmmountNew'+aid).value = E2;
					             }
					             document.getElementById('estimatePayableContractRateNew'+aid).value = Q2;
					             } }
					             //calculateEstimateSellRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid); 
					             //calculateEstimateRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
					                //--------------------Start--estimateRevenueAmount-------------------------//
					             var estimatePayableContractRatecal= document.getElementById('estimatePayableContractRateNew'+aid).value
					             var estimatePayableContractRateAmmountcal= document.getElementById('estimatePayableContractRateAmmountNew'+aid).value
					             var  estimatePayableContractCurrencycal=document.getElementById('estimatePayableContractCurrencyNew'+aid).value;			        
								 if(estimatePayableContractCurrencycal.trim()!=""){
								 var estimatePayableContractExchangeRatecal=document.getElementById('estimatePayableContractExchangeRateNew'+aid).value;
					             var estimateRatecal=estimatePayableContractRatecal/estimatePayableContractExchangeRatecal;
					             estimateRatecal=Math.round(estimateRatecal*10000)/10000;
					             var estimateExpensecal=estimatePayableContractRateAmmountcal/estimatePayableContractExchangeRatecal;
					             estimateExpensecal=Math.round(estimateExpensecal*10000)/10000;
					             document.getElementById(estimateRate).value=estimateRatecal; 
					             document.getElementById(estimateExpense).value=estimateExpensecal; 
							     } 
								 var estimateRatecal= document.getElementById(estimateRate).value;
					             var estimateExpensecal= document.getElementById(estimateExpense).value;
							     var estCurrencycal=document.getElementById('estCurrencyNew'+aid).value;
								 if(estCurrencycal.trim()!=""){
								 var estExchangeRatecal = document.getElementById('estExchangeRateNew'+aid).value;
								 var estLocalRatecal=estimateRatecal*estExchangeRatecal;
								 estLocalRatecal=Math.round(estLocalRatecal*10000)/10000;
								 var estLocalAmountcal=estimateExpensecal*estExchangeRatecal;
								 estLocalAmountcal=Math.round(estLocalAmountcal*10000)/10000;
								 document.getElementById('estLocalRateNew'+aid).value=estLocalRatecal;
					             document.getElementById('estLocalAmountNew'+aid).value=estLocalAmountcal;
								 }  
								 var estimateContractRatecal= document.getElementById('estimateContractRateNew'+aid).value
					             var estimateContractRateAmmountcal= document.getElementById('estimateContractRateAmmountNew'+aid).value
					             var  estimateContractCurrencycal=document.getElementById('estimateContractCurrencyNew'+aid).value;
					             if(estimateContractCurrencycal.trim()!=""){
					             var estimateContractExchangeRatecal=document.getElementById('estimateContractExchangeRateNew'+aid).value;
					             var estimateSellRatecal=estimateContractRatecal/estimateContractExchangeRatecal;
					             estimateSellRatecal=Math.round(estimateSellRatecal*10000)/10000;
					             var estimateRevenueAmountcal=estimateContractRateAmmountcal/estimateContractExchangeRatecal;
					             estimateRevenueAmountcal=Math.round(estimateRevenueAmountcal*10000)/10000;
					             document.getElementById(estimateSellRate).value=estimateSellRatecal;
					             document.getElementById(estimateRevenueAmount).value=estimateRevenueAmountcal;
					             } 
					             
					             var estimateSellRatecal= document.getElementById(estimateSellRate).value;
					             var estimateRevenueAmountcal= document.getElementById(estimateRevenueAmount).value;
							     var estSellCurrencycal=document.getElementById('estSellCurrencyNew'+aid).value;
								 if(estSellCurrencycal.trim()!=""){
									 var estSellExchangeRatecal = document.getElementById('estSellExchangeRateNew'+aid).value;
									 var estSellLocalRatecal=estimateSellRatecal*estSellExchangeRatecal;
									 estSellLocalRatecal=Math.round(estSellLocalRatecal*10000)/10000;
									 var estSellLocalAmountcal=estimateRevenueAmountcal*estSellExchangeRatecal;
									 estSellLocalAmountcal=Math.round(estSellLocalAmountcal*10000)/10000;
									 document.getElementById('estSellLocalRateNew'+aid).value=estSellLocalRatecal;
						             document.getElementById('estSellLocalAmountNew'+aid).value=estSellLocalAmountcal;
								 }

					             var estimateRevenueAmountpasspercentages= document.getElementById(estimateRevenueAmount).value
								 var estimateExpensepasspercentages= document.getElementById(estimateExpense).value
					             var estimatepasspercentagesTest = (estimateRevenueAmountpasspercentages/estimateExpensepasspercentages)*100;
					 	         var estimatepasspercentageRoundTest=Math.round(estimatepasspercentagesTest);
					 	         if(document.getElementById(estimateExpense).value == '' || document.getElementById(estimateExpense).value == 0){
					 	   	       document.forms['operationResourceForm'].elements[estimatePassPercentage].value='';
					 	         }else{
					 	   	       document.getElementById(estimatePassPercentage).value=estimatepasspercentageRoundTest;
					 	         }  
					               
					  	         var type01 = res[8];
					             var typeConversion01=res[10];  
					             //#7298 - Copy esimates not calculating preset calculations in charges start
					              //#7298 - Copy esimates not calculating preset calculations in charges End 
					      				        var revenueValue=document.getElementById(estimateRevenueAmount).value;
					      		  	           var revenueRound=0;
					      		  	           var oldRevenueSum=0;
					      		  	           var balanceRevenue=0;
					      		  	           <c:if test="${not empty serviceOrder.id}">
					      		  	           	oldRevenueSum=eval(document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value);
					      		  	           </c:if> 	  	          
					      		  	           revenueRound=Math.round(revenueValue*10000)/100000;
					      		  	           balanceRevenue=revenueRound-oldRevenue;
					      		  	           oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
					      		  	           balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
					      		  	           oldRevenueSum=oldRevenueSum+balanceRevenue; 
					      		  	           oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
					      		  	           var revenueRoundNew=""+revenueRound;
					      		  	           if(revenueRoundNew.indexOf(".") == -1){
					      		  	           revenueRoundNew=revenueRoundNew+".00";
					      		  	           } 
					      		  	           if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
					      		  	           revenueRoundNew=revenueRoundNew+"0";
					      		  	           }
					      		  	         document.getElementById(estimateRevenueAmount).value=revenueRoundNew;
					      			                 
						        <c:if test="${systemDefaultVatCalculationNew=='Y'}">
						        try{
						        var estVatP='vatPers'+aid;
						        var revenueP='estimateRevenueAmount'+aid;
						        var estVatA='vatAmt'+aid;
						   
							    calculateVatAmt('REVENUE',aid);
							    calculateVatAmt('EXPENSE',aid);
						        }catch(e){}
						        </c:if>   		  	           
					  	           <c:if test="${not empty serviceOrder.id}">
					  	  	           var newRevenueSum=""+oldRevenueSum;
					  	  	           if(newRevenueSum.indexOf(".") == -1){
					  	  	           	newRevenueSum=newRevenueSum+".00";
					  	  	           } 
					  	  	           if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
					  	  	           	newRevenueSum=newRevenueSum+"0";
					  	  	           } 
					  	  	       document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value=newRevenueSum;
					  	           </c:if>
					  	           if(revenueRoundNew>0){
					  	        	 document.getElementById(displayOnQuote).checked = true;
					  	           }
					  	       //--------------------End--estimateRevenueAmount-------------------------//
					  	       
				               //-------------------Start---estimateExpense-------------------------//                
					              var expenseValue=document.getElementById(estimateExpense).value;
					              var expenseRound=0;
					              var oldExpenseSum=0;
					              var balanceExpense=0;
					              <c:if test="${not empty serviceOrder.id}">
					              	oldExpenseSum=eval(document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value);
					              </c:if>
					              expenseRound=Math.round(expenseValue*10000)/10000;
					              balanceExpense=expenseRound-oldExpense;
					              oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
					              balanceExpense=Math.round(balanceExpense*10000)/10000; 
					              oldExpenseSum=oldExpenseSum+balanceExpense;
					              oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
					              var expenseRoundNew=""+expenseRound;
					              if(expenseRoundNew.indexOf(".") == -1){
					              	expenseRoundNew=expenseRoundNew+".00";
					              } 
					              if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
					              	expenseRoundNew=expenseRoundNew+"0";
					              }
					              document.getElementById(estimateExpense).value=expenseRoundNew;
					              
					              <c:if test="${not empty serviceOrder.id}">
						              var newExpenseSum=""+oldExpenseSum;
						              if(newExpenseSum.indexOf(".") == -1){
						              	newExpenseSum=newExpenseSum+".00";
						              } 
						              if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
						              	newExpenseSum=newExpenseSum+"0";
						              } 
						              document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value=newExpenseSum;
					              </c:if>
					              if(expenseRoundNew>0){
					            	  document.getElementById(displayOnQuote).checked = true;
					              }
					              //-------------------End---estimateExpense-------------------------//
					              //calculateGrossMargin();
					             <c:if test="${not empty serviceOrder.id}">
					              var revenueValue=0;
					              var expenseValue=0;
					              var grossMarginValue=0; 
					              var grossMarginPercentageValue=0; 
					              revenueValue=eval(document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value);
					              expenseValue=eval(document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value); 
					              grossMarginValue=revenueValue-expenseValue;
					              grossMarginValue=Math.round(grossMarginValue*10000)/10000; 
					              document.getElementById('estimatedGrossMargin'+${serviceOrder.id}).value=grossMarginValue; 
					              if(document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value == '' || document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value == 0 || document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value == 0.00){
					            	  document.getElementById('estimatedGrossMarginPercentage'+${serviceOrder.id}).value='';
					            	   }else{
					            	     grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
					            	     grossMarginPercentageValue=Math.round(grossMarginPercentageValue*10000)/10000; 
					            	     document.getElementById('estimatedGrossMarginPercentage'+${serviceOrder.id}).value = grossMarginPercentageValue;
					            	   }
					            </c:if> 
						            </c:if>
					        }  
					    }
				 
				 var decimalValueCount = 0 ;
				  var count = 0;
				  var negativeCount=0;
				 function clearDecimal(){
						count = 0;
						decimalValueCount = 0;
					}
				 
				 function numbersonly(myfield, e) {
						var key;
						var keychar;
						var val = myfield.value;
						if(count > 0){	decimalValueCount++;    if(decimalValueCount > 2){ return true; }		}
								if(val.indexOf(".")<0)
								{  count=0;	}
						if (window.event)
						   key = window.event.keyCode;
						else if (e)
						   key = e.which;
						else
						   return true;
							keychar = String.fromCharCode(key);
						if ((key==null) || (key==0) || (key==8) ||     (key==9) || (key==13) || (key==27) )
						   return true;
						else if ((("0123456789").indexOf(keychar) > -1)){
						   return true;
						}
						else if ((count<1)&&(keychar == ".")){
						   count++;
						   return true;
						}
						else if ((negativeCount<1)&&(keychar == "-"))
						{
							negativeCount++;
						   return true;
						}
						else if ((negativeCount>0)&&(keychar == "-"))
						{
						   return false;
						}
						else if ((count>0)&&(keychar == ".")){
						   return false;
						}
						else
							return false;
					}
				 function showBillingDatails(id,type,position) {
			        	var url="showBillingDatailsFields.html?ajax=1&decorator=simple&popup=true&aid=" + encodeURI(id)+"&showBillingType=" + encodeURI(type);
			        	ajax_showTooltip(url,position);	
			        	 var containerdiv =  document.getElementById("para1").scrollLeft;
			    	  	var newposition = document.getElementById("ajax_tooltipObj").offsetLeft-containerdiv ;	
			    	    document.getElementById("ajax_tooltipObj").style.left = ((parseInt(newposition))+10)+'px';	 
			        	  }
				 
				 function getAccountlineField(aid,basis,quantity,chargeCode,contract,vendorCode){
					 var oForm = document.forms[0].name; 
			    	  accountLineBasis=document.forms['operationResourceForm'].elements[basis].value;
			    	  accountLineEstimateQuantity=document.forms['operationResourceForm'].elements[quantity].value;
			    	  chargeCode=document.forms['operationResourceForm'].elements[chargeCode].value;
			    	  contract=document.forms['operationResourceForm'].elements[contract].value;
			    	  vendorCode=document.forms['operationResourceForm'].elements[vendorCode].value;
			    	  <c:if test="${contractType}">
			    	  var estimatePayableContractCurrencyTemp=document.forms['operationResourceForm'].elements['estimatePayableContractCurrencyNew'+aid].value; 
			    	  var estimatePayableContractValueDateTemp=document.forms['operationResourceForm'].elements['estimatePayableContractValueDateNew'+aid].value;
			    	  var estimatePayableContractExchangeRateTemp=document.forms['operationResourceForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
			    	  var estimatePayableContractRateTemp=document.forms['operationResourceForm'].elements['estimatePayableContractRateNew'+aid].value;
			    	  var estimatePayableContractRateAmmountTemp=document.forms['operationResourceForm'].elements['estimatePayableContractRateAmmountNew'+aid].value;
					  </c:if>
			    	  var estCurrencyTemp=document.forms['operationResourceForm'].elements['estCurrencyNew'+aid].value; 
			    	  var estValueDateTemp=document.forms['operationResourceForm'].elements['estValueDateNew'+aid].value;
			    	  var estExchangeRateTemp=document.forms['operationResourceForm'].elements['estExchangeRateNew'+aid].value;
			    	  var estLocalRateTemp=document.forms['operationResourceForm'].elements['estLocalRateNew'+aid].value;
			    	  var estLocalAmountTemp=document.forms['operationResourceForm'].elements['estLocalAmountNew'+aid].value;

			    	  var estimateQuantityTemp=document.forms['operationResourceForm'].elements['estimateQuantity'+aid].value;
			    	  var estimateRateTemp=document.forms['operationResourceForm'].elements['estimateRate'+aid].value;
			    	  var estimateExpenseTemp=document.forms['operationResourceForm'].elements['estimateExpense'+aid].value;
			    	  var basisTemp=document.forms['operationResourceForm'].elements['basis'+aid].value;
			    	  <c:if test="${contractType}">
			    	  openWindow('getAccountlineField.html?formName='+oForm+'&aid='+aid+'&basisTemp='+encodeURI(basisTemp)+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateRateTemp='+estimateRateTemp+'&estimateExpenseTemp='+estimateExpenseTemp+'&estimatePayableContractCurrencyTemp='+estimatePayableContractCurrencyTemp+'&estimatePayableContractValueDateTemp='+estimatePayableContractValueDateTemp+'&estimatePayableContractExchangeRateTemp='+estimatePayableContractExchangeRateTemp+'&estimatePayableContractRateTemp='+estimatePayableContractRateTemp+'&estimatePayableContractRateAmmountTemp='+estimatePayableContractRateAmmountTemp+'&estCurrencyTemp='+estCurrencyTemp+'&estValueDateTemp='+estValueDateTemp+'&estExchangeRateTemp='+estExchangeRateTemp+'&estLocalRateTemp='+estLocalRateTemp+'&estLocalAmountTemp='+estLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&decorator=popup&popup=true',920,350);
			    	  </c:if>
			    	  <c:if test="${!contractType}">
			    	  openWindow('getAccountlineField.html?formName='+oForm+'&aid='+aid+'&basisTemp='+encodeURI(basisTemp)+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateRateTemp='+estimateRateTemp+'&estimateExpenseTemp='+estimateExpenseTemp+'&estCurrencyTemp='+estCurrencyTemp+'&estValueDateTemp='+estValueDateTemp+'&estExchangeRateTemp='+estExchangeRateTemp+'&estLocalRateTemp='+estLocalRateTemp+'&estLocalAmountTemp='+estLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&decorator=popup&popup=true',920,350);
			    	  </c:if>
			    	  }
				 
				  function onlyRateAllowed(evt) { 
					  var keyCode = evt.which ? evt.which : evt.keyCode; 
					  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
					 }
				  
					function onlyNumsAllowedPersent(evt){
						  var keyCode = evt.which ? evt.which : evt.keyCode;
						  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
						} 
					function changeEstimatePassPercentage(basis,quantity,buyRate,expense,sellRate,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt){
					       var estimate=0; 
					       var estimateround=0;
					       var estimateRevenue=0;
					       var estimateRevenueRound=0;
					       var sellRateValue=0;
					       var sellRateround=0;
					       var	estimatepasspercentage =0;
					       var quantityValue = 0;
					       var oldRevenue=0;
					       var oldRevenueSum=0;
					       var balanceRevenue=0; 
					       var buyRateValue=0;  
						   oldRevenue=eval(document.forms['operationResourceForm'].elements[revenue].value); 
						    <c:if test="${not empty serviceOrder.id}">
					        oldRevenueSum=eval(document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
					       </c:if> 
					       quantityValue = eval(document.forms['operationResourceForm'].elements[quantity].value); 
					       buyRateValue = eval(document.forms['operationResourceForm'].elements[buyRate].value); 
					       estimatepasspercentage= document.forms['operationResourceForm'].elements[estimatePassPercentage].value;
					       estimate = (quantityValue*buyRateValue); 
					   	   if(quantityValue>0 && estimate>0){
					   	   if( document.forms['operationResourceForm'].elements[basis].value=="cwt" || document.forms['operationResourceForm'].elements[basis].value=="%age"){
					       		estimate = estimate/100; 	  	
					       	} 
					       	if( document.forms['operationResourceForm'].elements[basis].value=="per 1000"){
					       		estimate = estimate/1000; 	  	
					       	}
					       	estimateRevenue=((estimate)*estimatepasspercentage/100)
					       	estimateRevenueRound=Math.round(estimateRevenue*10000)/10000; 
					  	    document.forms['operationResourceForm'].elements[revenue].value=estimateRevenueRound;   
						    if( document.forms['operationResourceForm'].elements[basis].value=="cwt" || document.forms['operationResourceForm'].elements[basis].value=="%age"){
					       	  sellRateValue=(estimateRevenue/quantityValue)*100;
					       	  sellRateround=Math.round(sellRateValue*10000)/10000;
					       	  
					       	  document.forms['operationResourceForm'].elements[sellRate].value=sellRateround;	  	
					       	}
						    else if( document.forms['operationResourceForm'].elements[basis].value=="per 1000"){
					       	  sellRateValue=(estimateRevenue/quantityValue)*1000;
					       	  sellRateround=Math.round(sellRateValue*10000)/10000; 
					       	  document.forms['operationResourceForm'].elements[sellRate].value=sellRateround;	  	
					       	} else if(document.forms['operationResourceForm'].elements[basis].value!="cwt" || document.forms['operationResourceForm'].elements[basis].value!="%age"||document.forms['operationResourceForm'].elements[basis].value!="per 1000")
					        {
					          sellRateValue=(estimateRevenue/quantityValue);
					       	  sellRateround=Math.round(sellRateValue*10000)/10000; 
					          document.forms['operationResourceForm'].elements[sellRate].value=sellRateround;	  	
						    }
						    balanceRevenue=estimateRevenueRound-oldRevenue;
					        oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
					        balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
					        oldRevenueSum=oldRevenueSum+balanceRevenue;  
					        <c:if test="${not empty serviceOrder.id}">
					        var newRevenueSum=""+oldRevenueSum;
					        if(newRevenueSum.indexOf(".") == -1){
					        newRevenueSum=newRevenueSum+".00";
					        } 
					        if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
					        newRevenueSum=newRevenueSum+"0";
					        } 
					        document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
					        </c:if>
					       if(estimateRevenueRound>0){
					        document.forms['operationResourceForm'].elements[displayOnQuots].checked = true;
					       }
					       try{
					    	    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
					    	    calculateVatAmt(estVatPer,revenue,estAmt);
					    	    </c:if>
					    	    }catch(e){}
						 calculateGrossMargin();
					}
					else{
					document.forms['operationResourceForm'].elements[estimatePassPercentage].value=0;}} 
					function checkFloat1(temp) { 
						  negativeCount = 0;
				    var check='';  
				    var i;
				    var dotcheck=0;
					var s = temp.value;
					if(s.indexOf(".") == -1){
					dotcheck=eval(s.length)
					}else{
					dotcheck=eval(s.indexOf(".")-1)
					}
					var fieldName = temp.name;  
					var count = 0;
					var countArth = 0;
				    for (i = 0; i < s.length; i++) {   
				        var c = s.charAt(i); 
				        if(c == '.') {
				        	count = count+1
				        }
				        if(c == '-') {
				        	countArth = countArth+1
				        }
				        if((i!=0)&&(c=='-')) {
				       	  alert("Invalid data." ); 
				          document.forms['operationResourceForm'].elements[fieldName].select();
				          document.forms['operationResourceForm'].elements[fieldName].value='';
				       	  return false;
				       	} 	
				        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))  {
				        	check='Invalid'; 
				        }
				    }
				    if(dotcheck>15){
				    check='tolong';
				    }
				    if(check=='Invalid'){ 
				    alert("Invalid data." ); 
				    document.forms['operationResourceForm'].elements[fieldName].select();
				    document.forms['operationResourceForm'].elements[fieldName].value='';
				    return false;
				    } else if(check=='tolong'){ 
				    alert("Data Too Long." ); 
				     document.forms['operationResourceForm'].elements[fieldName].select();
				    document.forms['operationResourceForm'].elements[fieldName].value='';
				    return false;
				    } else{
				    s=Math.round(s*10000)/10000;
				    var value=""+s;
				    if(value.indexOf(".") == -1){
				    value=value+".00";
				    } 
				    if((value.indexOf(".")+3 != value.length)){
				    value=value+"0";
				    }
				    document.forms['operationResourceForm'].elements[fieldName].value=value;
				    return true;
				    }
				    }
					function getAccountlineFieldRev(aid,basis,quantity,chargeCode,contract,vendorCode){ 
						  var basis=document.forms['operationResourceForm'].elements[basis].value;
						  var revisionQuantity=document.forms['operationResourceForm'].elements[quantity].value;
						  var chargeCode=document.forms['operationResourceForm'].elements[chargeCode].value;
						  var contract=document.forms['operationResourceForm'].elements[contract].value;  
						  var vendorCode=document.forms['operationResourceForm'].elements[vendorCode].value;  
						  var strValue="";
						  strValue=basis+"~"+revisionQuantity+"~"+chargeCode+"~"+contract+"~"+vendorCode;
						  var revisionCurrency=document.forms['operationResourceForm'].elements['revisionCurrency'+aid].value; 
						  var revisionValueDate=document.forms['operationResourceForm'].elements['revisionValueDate'+aid].value;
						  var revisionLocalRate=document.forms['operationResourceForm'].elements['revisionLocalRate'+aid].value;
						  var revisionExchangeRate=document.forms['operationResourceForm'].elements['revisionExchangeRate'+aid].value;
						  var revisionLocalAmount=document.forms['operationResourceForm'].elements['revisionLocalAmount'+aid].value;
						  strValue=strValue+"~"+revisionCurrency+"~"+revisionValueDate+"~"+revisionLocalRate+"~"+revisionExchangeRate+"~"+revisionLocalAmount;
						  var revisionExpense=document.forms['operationResourceForm'].elements['revisionExpense'+aid].value;
						  var revisionDiscount=0.00;
						  <c:if test="${chargeDiscountFlag}">
						  revisionDiscount=document.forms['operationResourceForm'].elements['revisionDiscount'+aid].value;
						  </c:if>
						  var revisionRate=document.forms['operationResourceForm'].elements['revisionRate'+aid].value;
						  strValue=strValue+"~"+revisionExpense+"~"+revisionDiscount+"~"+revisionRate;
						  <c:if test="${contractType}">
						  var revisionPayableContractCurrency=document.forms['operationResourceForm'].elements['revisionPayableContractCurrency'+aid].value; 
						  var revisionPayableContractValueDate=document.forms['operationResourceForm'].elements['revisionPayableContractValueDate'+aid].value;
						  var revisionPayableContractRate=document.forms['operationResourceForm'].elements['revisionPayableContractRate'+aid].value;
						  var revisionPayableContractExchangeRate=document.forms['operationResourceForm'].elements['revisionPayableContractExchangeRate'+aid].value;
						  var revisionPayableContractRateAmmount=document.forms['operationResourceForm'].elements['revisionPayableContractRateAmmount'+aid].value;
						  strValue=strValue+"~"+revisionPayableContractCurrency+"~"+revisionPayableContractValueDate+"~"+revisionPayableContractRate+"~"+revisionPayableContractExchangeRate+"~"+revisionPayableContractRateAmmount;
					 	 </c:if>	  
						  openWindow('getAccountlineFieldRev.html?ajax=1&aid='+aid+'&strValue='+strValue+'&decorator=popup&popup=true',920,350);	
					}
					function checkNotesForRevnue(aid,url){
						var chargeCode=document.forms['operationResourceForm'].elements['chargeCode'+aid].value;
						if(chargeCode==''){
							alert("Charge code needs to be selected");
					}else{
						openWindow('notess.html?id='+aid+'&notesId='+aid+'&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage1&fieldId=countReceivableDetailNotes&decorator=popup&popup=true',755,500);
					}
					}
					
					function invoiceDetailsMethod(aid){
						  var chargeCode=document.forms['operationResourceForm'].elements['chargeCode'+aid].value;
						  var vendorCode=document.forms['operationResourceForm'].elements['vendorCode'+aid].value; 
						  var recGl=document.forms['operationResourceForm'].elements['recGl'+aid].value;
						  var payGl=document.forms['operationResourceForm'].elements['payGl'+aid].value;   
						  var actgCode=document.forms['operationResourceForm'].elements['actgCode'+aid].value;
						  var payingStatus=document.forms['operationResourceForm'].elements['payingStatus'+aid].value;
						  var receivedDate=document.forms['operationResourceForm'].elements['receivedDate'+aid].value; 
						  var invoiceNumber=document.forms['operationResourceForm'].elements['invoiceNumber'+aid].value;
						  var invoiceDate=document.forms['operationResourceForm'].elements['invoiceDate'+aid].value;   
						  var valueDate=document.forms['operationResourceForm'].elements['valueDate'+aid].value;
						  var type='PAY';
						  var acref="${accountInterface}";
						  acref=acref.trim();
						  if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
							  alert("Vendor Code Missing. Please select");
						  }else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
							  alert("Charge code needs to be selected");
						  }	else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY' && acref =='Y'){
							  alert("Pay GL missing. Please select another charge code.");
						  }	else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC' && acref =='Y'){
							  alert("Receivable GL missing. Please select another charge code.");
						  }else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
							  alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
						  }else{		   
						  openWindow('invoiceDetailsForm.html?ajax=1&aid='+aid+'&payingStatus='+payingStatus+'&receivedDate='+receivedDate+'&invoiceNumber='+invoiceNumber+'&invoiceDate='+invoiceDate+'&vendorCode='+vendorCode+'&valueDate='+valueDate+'&decorator=popup&popup=true',920,350);
						  }
					}
					
					function getAccountlineFieldPay(aid,basis,quantity,chargeCode,contract,vendorCode){
						 var oForm = document.forms[0].name; 
						  var basis=document.forms['operationResourceForm'].elements[basis].value;
						  var recQuantity=document.forms['operationResourceForm'].elements[quantity].value;
						  var chargeCode=document.forms['operationResourceForm'].elements[chargeCode].value;
						  var contract=document.forms['operationResourceForm'].elements[contract].value;  
						  var vendorCode=document.forms['operationResourceForm'].elements[vendorCode].value; 
						  var recGl=document.forms['operationResourceForm'].elements['recGl'+aid].value;
						  var payGl=document.forms['operationResourceForm'].elements['payGl'+aid].value;   
						  var actgCode=document.forms['operationResourceForm'].elements['actgCode'+aid].value;
						  var type='PAY';
						  var acref="${accountInterface}";
						  acref=acref.trim();
						  if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
							  alert("Vendor Code Missing. Please select");
						  }else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
							  alert("Charge code needs to be selected");
						  }	else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY' && acref =='Y'){
							  alert("Pay GL missing. Please select another charge code.");
						  }	else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC' && acref =='Y'){
							  alert("Receivable GL missing. Please select another charge code.");
						  }else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
							  alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
						  }else{		   
						  var strValue="";
						  strValue=basis+"~"+recQuantity+"~"+chargeCode+"~"+contract+"~"+vendorCode;
						  var country=document.forms['operationResourceForm'].elements['country'+aid].value; 
						  var valueDate=document.forms['operationResourceForm'].elements['valueDate'+aid].value;
						  var exchangeRate=document.forms['operationResourceForm'].elements['exchangeRate'+aid].value;
						  var localAmount=document.forms['operationResourceForm'].elements['localAmount'+aid].value;
						  strValue=strValue+"~"+country+"~"+valueDate+"~"+exchangeRate+"~"+localAmount;
						  var actualExpense=document.forms['operationResourceForm'].elements['actualExpense'+aid].value;
						  var actualDiscount=0.00;
						  <c:if test="${chargeDiscountFlag}">
						  actualDiscount=document.forms['operationResourceForm'].elements['actualDiscount'+aid].value;
						  </c:if>
						  strValue=strValue+"~"+actualExpense+"~"+actualDiscount;
						  <c:if test="${contractType}">
						  var payableContractCurrency=document.forms['operationResourceForm'].elements['payableContractCurrency'+aid].value; 
						  var payableContractValueDate=document.forms['operationResourceForm'].elements['payableContractValueDate'+aid].value;
						  var payableContractExchangeRate=document.forms['operationResourceForm'].elements['payableContractExchangeRate'+aid].value;
						  var payableContractRateAmmount=document.forms['operationResourceForm'].elements['payableContractRateAmmount'+aid].value;
						  strValue=strValue+"~"+payableContractCurrency+"~"+payableContractValueDate+"~"+payableContractExchangeRate+"~"+payableContractRateAmmount;
					 	 </c:if>	  
						  openWindow('getAccountlineFieldPay.html?ajax=1&aid='+aid+'&strValue='+strValue+'&formName='+oForm+'&decorator=popup&popup=true',920,350);
						  }
					}
					 function discriptionView1(discription){ 
						 document.getElementById(discription).style.height="15px"; 
						 SetCursorToTextEnd(discription);
						 }
					 function SetCursorToTextEnd(textControlID){ 
							var text = document.getElementById(textControlID); 
							text.scrollTop = 1;
						   }
					 
					 function discriptionView(discription){ 
						 document.getElementById(discription).style.height="50px"; 
						 }
					 
					 function checkCompanyDiv(id){
						  var companyDivision = validateFieldValue('companyDivision'+id);
						  if(companyDivision==null || companyDivision==undefined || companyDivision.trim()==''){
							  alert("Company Division is required field");
							  companyDivision = validateFieldValue('tempCompanyDivision'+id);
							 document.forms['operationResourceForm'].elements['companyDivision'+id].value=companyDivision;
						  }else if(validateFieldValue('payAccDate'+id)!='' || validateFieldValue('recAccDate'+id)!='') { 	 
							  	alert('You can not change the CompanyDivision as Sent To Acc has been already filled');
							 	 companyDivision = validateFieldValue('tempCompanyDivision'+id);
								 document.forms['operationResourceForm'].elements['companyDivision'+id].value=companyDivision;
						  }else{
						  }
					  }
					 
					 function selectAdd(str){
							var id = idList.split(",");
							  for (i=0;i<id.length;i++){
								  if(str.checked == true){
									  document.getElementById('additionalService'+id[i].trim()).checked = true;
								  }else{
									  document.getElementById('additionalService'+id[i].trim()).checked = false;
								  }
							  }
							}
					
					 function addDefaultSOPricingLineAjax(str){
							var check =saveValidationPricing(str);
							  if(check){
							var customerFileContract=document.forms['operationResourceForm'].elements['billing.contract'].value;
							if(customerFileContract==''){
							 	alert("Please select Pricing Contract from billing");
							 	return false;
							}
							if(document.forms['operationResourceForm'].elements['billing.billComplete'].value!=''){ 
						          var agree = confirm("The billing has been completed, do you still want to add lines?");
						           if(agree)
						             {
						        	   var jobtypeSO = document.forms['operationResourceForm'].elements['serviceOrder.job'].value;
										var routingSO = document.forms['operationResourceForm'].elements['serviceOrder.routing'].value;
										var modeSO = document.forms['operationResourceForm'].elements['serviceOrder.mode'].value;
										var packingModeSO = document.forms['operationResourceForm'].elements['serviceOrder.packingMode'].value;
										var commoditySO = document.forms['operationResourceForm'].elements['serviceOrder.commodity'].value;
										var serviceTypeSO = document.forms['operationResourceForm'].elements['serviceOrder.serviceType'].value;
										var companyDivisionSO = document.forms['operationResourceForm'].elements['serviceOrder.companyDivision'].value;
										 var reloServiceType=document.forms['operationResourceForm'].elements['reloServiceType'].value;
											while(reloServiceType.indexOf("#")>-1){
												reloServiceType=reloServiceType.replace('#',',');
											}

										var url1 = '&jobtypeSO='+jobtypeSO+'&routingSO='+routingSO+'&modeSO='+modeSO+'&packingModeSO='+packingModeSO+'&commoditySO='+commoditySO;
										url1 = url1+'&serviceTypeSO='+serviceTypeSO+'&companyDivisionSO='+companyDivisionSO+'&reloServiceType='+reloServiceType;
										//alert(url1)
										//var url1 = getTemplateParam();
										showOrHide(1);
										
										var url="findSoDefaultPricingLineAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}"+url1;
										httpTemplate.open("POST", url, true);
										httpTemplate.onreadystatechange = handleHttpResponseForAddTemplate;
										httpTemplate.send(null);
						             }else{}
							}else{
							var jobtypeSO = document.forms['operationResourceForm'].elements['serviceOrder.job'].value;
							var routingSO = document.forms['operationResourceForm'].elements['serviceOrder.routing'].value;
							var modeSO = document.forms['operationResourceForm'].elements['serviceOrder.mode'].value;
							var packingModeSO = document.forms['operationResourceForm'].elements['serviceOrder.packingMode'].value;
							var commoditySO = document.forms['operationResourceForm'].elements['serviceOrder.commodity'].value;
							var serviceTypeSO = document.forms['operationResourceForm'].elements['serviceOrder.serviceType'].value;
							var companyDivisionSO = document.forms['operationResourceForm'].elements['serviceOrder.companyDivision'].value;
							var reloServiceType=document.forms['operationResourceForm'].elements['reloServiceType'].value;
							while(reloServiceType.indexOf("#")>-1){
								reloServiceType=reloServiceType.replace('#',',');
							}
							var url1 = '&jobtypeSO='+jobtypeSO+'&routingSO='+routingSO+'&modeSO='+modeSO+'&packingModeSO='+packingModeSO+'&commoditySO='+commoditySO;
							url1 = url1+'&serviceTypeSO='+serviceTypeSO+'&companyDivisionSO='+companyDivisionSO+'&reloServiceType='+reloServiceType;
							//alert(url1)
							//var url1 = getTemplateParam();
							showOrHide(1);
							
							var url="findSoDefaultPricingLineAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}"+url1;
							httpTemplate.open("POST", url, true);
							httpTemplate.onreadystatechange = handleHttpResponseForAddTemplate;
							httpTemplate.send(null);
							}
							  }
						}
					 function handleHttpResponseForAddTemplate(){
							if (httpTemplate.readyState == 4){
								showOrHide(0);
								 var results = httpTemplate.responseText;
							     results = results.trim();
							     if(results == 'NO'){
							    	 alert('No Default Template Found');
							    	 return false;
							     }else{
							    	findAllPricingLineId('AddTemplate');
							     }
						 	}
						}
					 
					 function checkChargeCode(chargeCode,category,aid){
						 var charge= document.forms['operationResourceForm'].elements[chargeCode].value;
						 if(charge.trim()!=""){
						 progressBarAutoSave('1');
					    var val = document.forms['operationResourceForm'].elements[category].value;
					    var jobType='${serviceOrder.job}'; 
					    var routing='${serviceOrder.routing}'; 
					    var soCompanyDivision =	'${serviceOrder.companyDivision}'; 
					    var sid='${serviceOrder.id}'; 
					    var accountCompanyDivision = document.forms['operationResourceForm'].elements['companyDivision'+aid].value;
						var quotationContract = document.forms['operationResourceForm'].elements['billing.contract'].value;
						var myRegxp = /^[a-zA-Z0-9-_\'('&'\)']+$/;
						if(charge.match(myRegxp)==false){
						   alert( "Please enter a valid charge code." );
					       document.forms['operationResourceForm'].elements[chargeCode].value='';
					       progressBarAutoSave('0');
						}
						else{
						if(val=='Internal Cost'){
						 quotationContract="Internal Cost Management";
						 var url="checkInternalCostChargeCode.html?ajax=1&decorator=simple&popup=true&accountCompanyDivision="+encodeURIComponent(accountCompanyDivision)+"&chargeCode="+encodeURIComponent(charge)+"&jobType="+encodeURIComponent(jobType)+"&routing="+encodeURIComponent(routing)+"&soId="+sid+"&soCompanyDivision="+encodeURIComponent(soCompanyDivision); 
						 http5.open("GET", url, true);
					     http5.onreadystatechange =function(){handleHttpResponseChargeCode(chargeCode,category,aid);} ;
					     http5.send(null);
						}
						else{
						if(quotationContract=='' || quotationContract==' '){
						alert("There is no pricing contract in billing: Please select.");
						document.forms['operationResourceForm'].elements[chargeCode].value = "";
						progressBarAutoSave('0');
						}
						else{
						 var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+quotationContract+"&chargeCode="+charge+"&jobType="+jobType+"&routing="+routing+"&soCompanyDivision="+soCompanyDivision; 
					     http5.open("GET", url, true);
					     http5.onreadystatechange =function(){handleHttpResponseChargeCode(chargeCode,category,aid);} ;
					     http5.send(null);
						}}}}}
					 function fillDiscription(discription,chargeCode,category,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate,payableContractCurrency,payableContractExchangeRate,payableContractValueDate,chargeCodeTemp,aid)
						{ 
							var category = document.forms['operationResourceForm'].elements[category].value;
							var quotationContract = document.forms['operationResourceForm'].elements['billing.contract'].value;
							var chargeCode1 = document.forms['operationResourceForm'].elements[chargeCode].value;
							var discriptionValue = document.forms['operationResourceForm'].elements[discription].value; 
							var jobtype = document.forms['operationResourceForm'].elements['serviceOrder.job'].value; 
						    var routing = document.forms['operationResourceForm'].elements['serviceOrder.routing'].value; 
						    var companyDivision = document.forms['operationResourceForm'].elements['serviceOrder.companyDivision'].value;
						    var sid='${serviceOrder.id}'; 
								var url = "findQuotationDiscription.html?ajax=1&contract="+quotationContract+"&chargeCode="+chargeCode1+"&decorator=simple&popup=true&jobType="+jobtype+"&routing1="+routing+"&soId="+sid+"&companyDivision="+companyDivision;
						     	http50.open("GET", url, true);
						     	http50.onreadystatechange =  function(){fillDiscriptionResponse(discription,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate,payableContractCurrency,payableContractExchangeRate,payableContractValueDate,chargeCode,chargeCodeTemp,aid);};
						     	http50.send(null);
						}
					 
					 function checkAccountInterface(vendorCode,estimateVendorName,actgCode){			                	  
						 <c:if test="${accountInterface!='Y'}">
						 	checkVendorName(vendorCode,estimateVendorName);
					     </c:if>
					     <c:if test="${accountInterface=='Y'}">
					       vendorCodeForActgCode(vendorCode,estimateVendorName,actgCode);
					     </c:if>
					 }
					 
					 function closeMypricingDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
							document.getElementById(autocompleteDivId).style.display = "none";	
							if(document.getElementById(paertnerCodeId).value==''){
								document.getElementById(partnerNameId).value="";	
							}
							}
					 
					 function calculateExpense(basis,quantity,buyRate,expense,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid){
						  var expenseValue=0;
						    var expenseRound=0;
						    var oldExpense=0;
						    var oldExpenseSum=0;
						    var balanceExpense=0;
						    oldExpense=eval(document.forms['operationResourceForm'].elements[expense].value);
						    <c:if test="${not empty serviceOrder.id}">
						    oldExpenseSum=eval(document.forms['operationResourceForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
						    </c:if>
						    var basisValue = document.forms['operationResourceForm'].elements[basis].value;
						    checkFloatNew('operationResourceForm',quantity,'Nothing');    
						    var quantityValue = document.forms['operationResourceForm'].elements[quantity].value;
						    checkFloatNew('operationResourceForm',buyRate,'Nothing');    
						    var buyRateValue = document.forms['operationResourceForm'].elements[buyRate].value;
						    if(basisValue=="cwt" || basisValue== "%age"){
						    expenseValue=(quantityValue*buyRateValue)/100 ;
						    }
						    else if(basisValue=="per 1000"){
						    expenseValue=(quantityValue*buyRateValue)/1000 ;
						    }else{
						    expenseValue=(quantityValue*buyRateValue);
						    }
						    expenseRound=Math.round(expenseValue*10000)/10000;
						    balanceExpense=expenseRound-oldExpense;
						    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
						    balanceExpense=Math.round(balanceExpense*10000)/10000; 
						    oldExpenseSum=oldExpenseSum+balanceExpense;
						    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
						    var expenseRoundNew=""+expenseRound;
						    if(expenseRoundNew.indexOf(".") == -1){
						    expenseRoundNew=expenseRoundNew+".00";
						    } 
						    if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
						    expenseRoundNew=expenseRoundNew+"0";
						    }
						    document.forms['operationResourceForm'].elements[expense].value=expenseRoundNew;
						    try{
						        var buyRate1=document.forms['operationResourceForm'].elements['estimateRate'+aid].value;
						        var estCurrency1=document.forms['operationResourceForm'].elements['estCurrencyNew'+aid].value;
								if(estCurrency1.trim()!=""){
										var Q1=0;
										var Q2=0;
										Q1=buyRate1;
										Q2=document.forms['operationResourceForm'].elements['estExchangeRateNew'+aid].value;
							            var E1=Q1*Q2;
							            E1=Math.round(E1*10000)/10000;
							            document.forms['operationResourceForm'].elements['estLocalRateNew'+aid].value=E1;
							            var estLocalAmt1=0;
							            if(basisValue=="cwt" || basisValue== "%age"){
							            	estLocalAmt1=(quantityValue*E1)/100 ;
							                }
							                else if(basisValue=="per 1000"){
							                	estLocalAmt1=(quantityValue*E1)/1000 ;
							                }else{
							                	estLocalAmt1=(quantityValue*E1);
							                }	 
							            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
							            document.forms['operationResourceForm'].elements['estLocalAmountNew'+aid].value=estLocalAmt1;     
								}
								<c:if test="${contractType}">
								try{
								     var estimatePayableContractExchangeRate=document.forms['operationResourceForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
								     var estLocalRate=document.forms['operationResourceForm'].elements['estLocalRateNew'+aid].value;
								     var estExchangeRate=document.forms['operationResourceForm'].elements['estExchangeRateNew'+aid].value
								     var recCurrencyRate=(estimatePayableContractExchangeRate*estLocalRate)/estExchangeRate;
								     var roundValue=Math.round(recCurrencyRate*10000)/10000;
								     document.forms['operationResourceForm'].elements['estimatePayableContractRateNew'+aid].value=roundValue ; 
									var Q11=0;
									var Q21=0;
									Q11=document.forms['operationResourceForm'].elements[quantity].value;
									Q21=document.forms['operationResourceForm'].elements['estimatePayableContractRateNew'+aid].value;
						            var E11=Q11*Q21;
						            var estLocalAmt11=0;
						            if(basisValue=="cwt" || basisValue== "%age"){
						            	estLocalAmt11=E11/100 ;
						                }
						                else if(basisValue=="per 1000"){
						                	estLocalAmt11=E11/1000 ;
						                }else{
						                	estLocalAmt11=E11;
						                }	 
						            estLocalAmt11=Math.round(estLocalAmt11*10000)/10000;    
						            document.forms['operationResourceForm'].elements['estimatePayableContractRateAmmountNew'+aid].value=estLocalAmt11;
								}catch(e){}     
							</c:if>
						    }catch(e){}
						    <c:if test="${not empty serviceOrder.id}">
						    var newExpenseSum=""+oldExpenseSum;
						    if(newExpenseSum.indexOf(".") == -1){
						    newExpenseSum=newExpenseSum+".00";
						    } 
						    if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
						    newExpenseSum=newExpenseSum+"0";
						    } 
						    document.forms['operationResourceForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
						    </c:if>
						    if(expenseRoundNew>0){
						    document.forms['operationResourceForm'].elements[displayOnQuots].checked = true;
						    }
						    try{
						        <c:if test="${systemDefaultVatCalculationNew=='Y'}">
						        calculateVatAmt(estVatPer,revenue,estAmt);
						        </c:if>
						        }catch(e){}
						    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage);
						       }
					 
					 function calculateestimatePassPercentage(revenue,expense,estimatePassPercentage){
						    var revenueValue=0;
						    var expenseValue=0;
						    var estimatePassPercentageValue=0; 
						    checkFloatNew('operationResourceForm',estimatePassPercentage,'Nothing'); 
						    revenueValue=eval(document.forms['operationResourceForm'].elements[revenue].value);
						    expenseValue=eval(document.forms['operationResourceForm'].elements[expense].value); 
						    try{
						    	estimatePassPercentageValue=(revenueValue/expenseValue)*100;
						    }catch(e){
						    	estimatePassPercentageValue=0.00;
						        } 
						    estimatePassPercentageValue=Math.round(estimatePassPercentageValue);
						    if(document.forms['operationResourceForm'].elements[expense].value == '' || document.forms['operationResourceForm'].elements[expense].value == 0||document.forms['operationResourceForm'].elements[expense].value == 0.00){
						  	   document.forms['operationResourceForm'].elements[estimatePassPercentage].value='';
						  	   }else{ 
						  	   	document.forms['operationResourceForm'].elements[estimatePassPercentage].value = estimatePassPercentageValue;
						  	   } 
						    calculateGrossMargin();
						    }
					 
					 function calculateGrossMargin(){
						    <c:if test="${not empty serviceOrder.id}">
						    var revenueValue=0;
						    var expenseValue=0;
						    var grossMarginValue=0; 
						    var grossMarginPercentageValue=0; 
						    revenueValue=eval(document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
						    expenseValue=eval(document.forms['operationResourceForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value); 
						    grossMarginValue=revenueValue-expenseValue;
						    grossMarginValue=Math.round(grossMarginValue*10000)/10000; 
						    document.forms['operationResourceForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=grossMarginValue; 
						    if(document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0||document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0.00){
						  	    document.forms['operationResourceForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value='';
						  	   }else{
						  	     grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
						  	     grossMarginPercentageValue=Math.round(grossMarginPercentageValue*10000)/10000; 
						  	   	document.forms['operationResourceForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValue;
						  	   }
						  	</c:if>    
						    }
					 
					 function revisionBuyFX(aid){
						 var basis=document.forms['operationResourceForm'].elements['basis'+aid].value;
						 var baseRateVal=1;
						 var roundVal=0.00;
						 var revisionQuantity=document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value;
						 if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
							 revisionQuantity = document.forms['operationResourceForm'].elements['revisionQuantity'+aid].value=1;
					         } 
					     if( basis=="cwt" || basis=="%age"){
					       	 baseRateVal=100;
					     }else if(basis=="per 1000"){
					       	 baseRateVal=1000;
					     } else {
					       	 baseRateVal=1;  	
						 }
					     var revisionDiscount=0.00;
					     try{
					    	 revisionDiscount=document.forms['operationResourceForm'].elements['revisionDiscount'+aid].value;
					     }catch(e){}
						 var revisionRate=0.00;
						 revisionRate=document.forms['operationResourceForm'].elements['revisionRate'+aid].value;
						 if(revisionRate=='') {
							 revisionRate = document.forms['operationResourceForm'].elements['revisionRate'+aid].value=0.00;
					         }
						  <c:if test="${contractType}">
							var revisionPayableContractCurrency=document.forms['operationResourceForm'].elements['revisionPayableContractCurrency'+aid].value;		
							var revisionPayableContractExchangeRate=document.forms['operationResourceForm'].elements['revisionPayableContractExchangeRate'+aid].value;
							var revisionPayableContractRate=document.forms['operationResourceForm'].elements['revisionPayableContractRate'+aid].value;
							var revisionPayableContractRateAmmount=document.forms['operationResourceForm'].elements['revisionPayableContractRateAmmount'+aid].value;
							var revisionPayableContractValueDate=document.forms['operationResourceForm'].elements['revisionPayableContractValueDate'+aid].value;
						 	if(revisionPayableContractCurrency!=''){
						 		var ss=findExchangeRateGlobal(revisionPayableContractCurrency);
						 		revisionPayableContractExchangeRate=parseFloat(ss);
						 		document.forms['operationResourceForm'].elements['revisionPayableContractExchangeRate'+aid].value=revisionPayableContractExchangeRate;
						 		document.forms['operationResourceForm'].elements['revisionPayableContractValueDate'+aid].value=currentDateGlobal();
						 		revisionPayableContractRate=revisionRate*revisionPayableContractExchangeRate;
						 		document.forms['operationResourceForm'].elements['revisionPayableContractRate'+aid].value=revisionPayableContractRate;
						 		revisionPayableContractRateAmmount=(revisionQuantity*revisionPayableContractRate)/baseRateVal;
						  	    document.forms['operationResourceForm'].elements['revisionPayableContractRateAmmount'+aid].value=revisionPayableContractRateAmmount;	 		
						 	}
						  </c:if>
						  
							var revisionCurrency=document.forms['operationResourceForm'].elements['revisionCurrency'+aid].value;		
							var revisionExchangeRate=document.forms['operationResourceForm'].elements['revisionExchangeRate'+aid].value;
							var revisionLocalRate=document.forms['operationResourceForm'].elements['revisionLocalRate'+aid].value;
							var revisionLocalAmount=document.forms['operationResourceForm'].elements['revisionLocalAmount'+aid].value;
							var revisionValueDate=document.forms['operationResourceForm'].elements['revisionValueDate'+aid].value;
						 	if(revisionCurrency!=''){
						 		var ss=findExchangeRateGlobal(revisionCurrency);
						 		revisionExchangeRate=parseFloat(ss);
						 		document.forms['operationResourceForm'].elements['revisionExchangeRate'+aid].value=revisionExchangeRate;
						 		document.forms['operationResourceForm'].elements['revisionValueDate'+aid].value=currentDateGlobal();
						 		revisionLocalRate=revisionRate*revisionExchangeRate;
						 		document.forms['operationResourceForm'].elements['revisionLocalRate'+aid].value=revisionLocalRate;
						 		revisionLocalAmount=(revisionQuantity*revisionLocalRate)/baseRateVal;
						  	   document.forms['operationResourceForm'].elements['revisionLocalAmount'+aid].value=revisionLocalAmount;	 		
						 	}
					 }
					 
					 function validateFieldValue(fieldId){
							
						  var result1 =" ";
						  try {
							  var result =document.forms['operationResourceForm'].elements[fieldId];
							  
								 if(result == null || result ==  undefined){
									 return result1;
								 }else{
									 return result.value;
								 }
						  }catch(e) {
							  return result1;
						  }
					  }
					 
					 var httpvandor = getHTTPObject(); 
						function vendorCodeForActgCode(vendorCode,estimateVendorName,actgCode){
						    var vendorId = document.forms['operationResourceForm'].elements[vendorCode].value;
							if(vendorId == ''){
								document.forms['operationResourceForm'].elements[estimateVendorName].value="";
							}
							if(vendorId != ''){
								var myRegxp = /([a-zA-Z0-9_-]+)$/;
								if(myRegxp.test(vendorId)==false){
									alert("Vendor code not valid" );
						            document.forms['operationResourceForm'].elements[estimateVendorName].value="";
								 	 document.forms['operationResourceForm'].elements[vendorCode].value="";
								}else{
									var aid=vendorCode.replace('vendorCode','');
									aid=aid.trim();
									var companyDivision=document.forms['operationResourceForm'].elements['companyDivision'+aid].value;
								     var url=""
								    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}">
								    var actgCodeFlag='';
								    try{
								    	actgCodeFlag=document.forms['operationResourceForm'].elements['actgCodeFlag'].value;
								    }catch(e){} 
								     url="vendorNameForActgCodeLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision)+"&actgCodeFlag="+encodeURI(actgCodeFlag);	     
								    </c:if>
								    <c:if test="${companyDivisionAcctgCodeUnique=='N' || companyDivisionAcctgCodeUnique==''}"> 
								     url="vendorNameForUniqueActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision);
								    </c:if>
							        httpvandor.open("GET", url, true);
								    httpvandor.onreadystatechange = function(){handleHttpResponse22219(vendorCode,estimateVendorName,actgCode);} ;
								    httpvandor.send(null);
						    } }	    
						}
						function handleHttpResponse22219(vendorCode,estimateVendorName,actgCode){
							if (httpvandor.readyState == 4){
					                var results = httpvandor.responseText
					                results = results.trim(); 
					               
					                var res = results.split("#");  
					                if(res.length >= 2){ 
							                if(res[3]=='Approved'){
							                	if(res[2] != "null" && res[2] !=''){
							                		document.forms['operationResourceForm'].elements[estimateVendorName].value = res[1];
									                document.forms['operationResourceForm'].elements[actgCode].value = res[2];
									            }  else { 
									                document.forms['operationResourceForm'].elements[estimateVendorName].value=res[1]; 
													document.forms['operationResourceForm'].elements[actgCode].value="";
													document.forms['operationResourceForm'].elements[vendorCode].select(); 
									            }
									        }else{
									            alert("Partner code is not approved" ); 
												document.forms['operationResourceForm'].elements[estimateVendorName].value="";
												document.forms['operationResourceForm'].elements[vendorCode].value="";
												document.forms['operationResourceForm'].elements[actgCode].value="";
												document.forms['operationResourceForm'].elements[vendorCode].select(); 
									        }     
							            }else{
							                alert("Partner code not valid" ); 
											document.forms['operationResourceForm'].elements[estimateVendorName].value="";
											document.forms['operationResourceForm'].elements[vendorCode].value="";
											document.forms['operationResourceForm'].elements[actgCode].value="";
											document.forms['operationResourceForm'].elements[vendorCode].select(); 
										}  } } 
						
						function checkFloatNew(formName,fieldName,message)
						 {   var i;
						 	var s = document.forms[formName].elements[fieldName].value;
						 	var count = 0;
						 	if(s.trim()=='.') {
						 		document.forms[formName].elements[fieldName].value='0.00';
						 	}else{
						     for (i = 0; i < s.length; i++)
						     {   
						         var c = s.charAt(i); 
						         if(c == '.') {
						         	count = count+1
						         }
						         if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) && (c != '-') ) 
						         {
						 	        document.forms[formName].elements[fieldName].value='0.0000';
						         } }    } }
						
						function selectiveInvoiceCheck(rowId,targetElement){
							 progressBarAutoSave('1');
						     var selectiveInvoice=true;
							 if(targetElement.checked==false)
						     {
								 selectiveInvoice=false;
						     }
							 var sid = document.forms['operationResourceForm'].elements['serviceOrder.id'].value;
						     var url="updateSelectiveInvoice.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&id="+rowId+"&selectiveInvoice="+selectiveInvoice;
						     httpCMMAgentInvoiceCheck.open("GET", url, true);
						     httpCMMAgentInvoiceCheck.onreadystatechange = selectiveInvoiceCheckResponse;
						     httpCMMAgentInvoiceCheck.send(null);
						 }
						 function selectiveInvoiceCheckResponse(){
							  if (httpCMMAgentInvoiceCheck.readyState == 4)
						     {
						        var results = httpCMMAgentInvoiceCheck.responseText  
						     }
							  progressBarAutoSave('0');
						 }
						 
						 function currentDateGlobal(){
							 var mydate=new Date();
						     var daym;
						     var year=mydate.getFullYear()
						     var y=""+year;
						     if (year < 1000)
						     year+=1900
						     var day=mydate.getDay()
						     var month=mydate.getMonth()+1
						     if(month == 1)month="Jan";
						     if(month == 2)month="Feb";
						     if(month == 3)month="Mar";
							  if(month == 4)month="Apr";
							  if(month == 5)month="May";
							  if(month == 6)month="Jun";
							  if(month == 7)month="Jul";
							  if(month == 8)month="Aug";
							  if(month == 9)month="Sep";
							  if(month == 10)month="Oct";
							  if(month == 11)month="Nov";
							  if(month == 12)month="Dec";
							  var daym=mydate.getDate()
							  if (daym<10)
							  daym="0"+daym
							  var datam = daym+"-"+month+"-"+y.substring(2,4); 
							  return datam;
						 }
						 function getPricingValue(id){
							 // alert(id);
							  var categoryType =document.forms['operationResourceForm'].elements['category'+id].value; // 0
							  var chargeCode = document.forms['operationResourceForm'].elements['chargeCode'+id].value; // 1
							  var vendorCode = document.forms['operationResourceForm'].elements['vendorCode'+id].value; // 2
							  var vendorName = document.forms['operationResourceForm'].elements['estimateVendorName'+id].value; // 3
							  try{
									//var basis = document.forms['operationResourceForm'].elements['basis'+id].value; //4
									var basis = document.getElementById('basis'+id).value;
									//alert(basis);
							  		basis = basis.replace("%","Percentage");
							  }catch(e){
								 // alert("basis error."+e)
								  basis="";
							  }
							  var rollUpInInvoiceCheck = false;
							  try{
								 rollUpInInvoiceCheck = document.forms['operationResourceForm'].elements['rollUpInInvoice'+id].checked; // 14
								  }catch(e){					  }			  
							  var estimateQuantity = document.forms['operationResourceForm'].elements['estimateQuantity'+id].value;
							  var estimateRate =document.forms['operationResourceForm'].elements['estimateRate'+id].value;
							  var estimateSellRate =document.forms['operationResourceForm'].elements['estimateSellRate'+id].value;
							  var estimateExpense =document.forms['operationResourceForm'].elements['estimateExpense'+id].value;
							  var estimatePassPercentage =document.forms['operationResourceForm'].elements['estimatePassPercentage'+id].value;
							  var estimateRevenueAmount =document.forms['operationResourceForm'].elements['estimateRevenueAmount'+id].value;
							  var quoteDescription =document.forms['operationResourceForm'].elements['quoteDescription'+id].value;
							  
							  var displayOnQuote =document.forms['operationResourceForm'].elements['displayOnQuote'+id].checked;
							  var additionalService =document.forms['operationResourceForm'].elements['additionalService'+id].checked;
							  try{
							  var statusCheck = document.forms['operationResourceForm'].elements['statusCheck'+id].checked; // 14
							  }catch(e){}
							  var companyDivision = document.forms['operationResourceForm'].elements['companyDivision'+id].value; 
							 // alert(additionalService);
							  
							  var division = validateFieldValue('division'+id); //15
							  var vatDecr = validateFieldValue('vatDecr'+id);
							  var vatPers = validateFieldValue('vatPers'+id);
							  var vatAmt = validateFieldValue('vatAmt'+id);
							  var deviation = validateFieldValue('deviation'+id);
							  var estimateDeviation = validateFieldValue('estimateDeviation'+id);
							  var estimateSellDeviation = validateFieldValue('estimateSellDeviation'+id);  //21
							  
							  var delimeter2 = "~";
							  var delimeter = "-,-";
							  
							  var values = id+"~"+categoryType+ delimeter +chargeCode+ delimeter +vendorCode+ delimeter +vendorName;
							  values = values+ delimeter +basis+ delimeter +estimateQuantity+ delimeter +estimateRate+ delimeter +estimateSellRate+ delimeter +estimateExpense;
							  values = values+ delimeter +estimatePassPercentage+ delimeter +estimateRevenueAmount+ delimeter +quoteDescription;
							  values = values+ delimeter +displayOnQuote+ delimeter +additionalService+ delimeter +statusCheck;
							  values = values+ delimeter +division+ delimeter +vatDecr+ delimeter +vatPers+ delimeter +vatAmt+ delimeter +deviation+ delimeter +estimateDeviation;
							  values = values+ delimeter +estimateSellDeviation; // 0 - 21
							  
							  var pricingHiddenValue = getPricingHiddenValue(id);  // 22 - 37
							  var pricingHiddenValue1 = getPricingHiddenValue1(id); // 38 - 59
							  
							  values = values + delimeter + pricingHiddenValue;
							  values = values + delimeter + pricingHiddenValue1;
							  
							  values = values + delimeter + validateFieldValue('accountLineNumber'+id); //60
							  values = values + delimeter + companyDivision;//61
							  values = values + delimeter + validateFieldValue('pricePointAgent'+id);//62
							  values = values + delimeter + validateFieldValue('pricePointMarket'+id);//63
							  values = values + delimeter + validateFieldValue('pricePointTariff'+id);//64
							  values = values + delimeter + validateFieldValue('estimateDiscount'+id);//65
								 
							  values = values + delimeter + validateFieldValue('actualRevenue'+id);//66
							  values = values + delimeter + validateFieldValue('recRate'+id);//67
							  values = values + delimeter + validateFieldValue('recQuantity'+id);//68
							  values = values + delimeter + validateFieldValue('actualExpense'+id);//69
							  values = values + delimeter + validateFieldValue('actualDiscount'+id);//70
							  
							  values = values + delimeter + validateFieldValue('payableContractCurrency'+id);//71
							  values = values + delimeter + validateFieldValue('payableContractExchangeRate'+id);//72
							  values = values + delimeter + validateFieldValue('payableContractRateAmmount'+id);//73
							  values = values + delimeter + validateFieldValue('payableContractValueDate'+id);//74
							  values = values + delimeter + validateFieldValue('country'+id);//75
							  values = values + delimeter + validateFieldValue('exchangeRate'+id);//76
							  values = values + delimeter + validateFieldValue('localAmount'+id);//77
							  values = values + delimeter + validateFieldValue('valueDate'+id);//78
							  
							  values = values + delimeter + validateFieldValue('contractCurrency'+id);//79
							  values = values + delimeter + validateFieldValue('contractExchangeRate'+id);//80
							  values = values + delimeter + validateFieldValue('contractRate'+id);//81
							  values = values + delimeter + validateFieldValue('contractValueDate'+id);//82
							  values = values + delimeter + validateFieldValue('contractRateAmmount'+id);//83
							  values = values + delimeter + validateFieldValue('recRateCurrency'+id);//84
							  values = values + delimeter + validateFieldValue('recRateExchange'+id);//85
							  values = values + delimeter + validateFieldValue('recCurrencyRate'+id);//86
							  values = values + delimeter + validateFieldValue('actualRevenueForeign'+id);//87
							  values = values + delimeter + validateFieldValue('racValueDate'+id);//88

							  values = values + delimeter + validateFieldValue('revisionPayableContractCurrency'+id);//89
							  values = values + delimeter + validateFieldValue('revisionPayableContractValueDate'+id);//90
							  values = values + delimeter + validateFieldValue('revisionPayableContractExchangeRate'+id);//91
							  values = values + delimeter + validateFieldValue('revisionPayableContractRate'+id);//92
							  values = values + delimeter + validateFieldValue('revisionPayableContractRateAmmount'+id);//93
							  values = values + delimeter + validateFieldValue('revisionContractCurrency'+id);//94
							  values = values + delimeter + validateFieldValue('revisionContractValueDate'+id);//95
							  values = values + delimeter + validateFieldValue('revisionContractExchangeRate'+id);//96
							  values = values + delimeter + validateFieldValue('revisionContractRate'+id);//97
							  values = values + delimeter + validateFieldValue('revisionContractRateAmmount'+id);//98
							  values = values + delimeter + validateFieldValue('revisionCurrency'+id);//99
							  values = values + delimeter + validateFieldValue('revisionValueDate'+id);//100
							  values = values + delimeter + validateFieldValue('revisionExchangeRate'+id);//101
							  values = values + delimeter + validateFieldValue('revisionLocalRate'+id);//102
							  values = values + delimeter + validateFieldValue('revisionLocalAmount'+id);//103
							  values = values + delimeter + validateFieldValue('revisionSellCurrency'+id);//104
							  values = values + delimeter + validateFieldValue('revisionSellValueDate'+id);//105
							  values = values + delimeter + validateFieldValue('revisionSellExchangeRate'+id);//106
							  values = values + delimeter + validateFieldValue('revisionSellLocalRate'+id);//107
							  values = values + delimeter + validateFieldValue('revisionSellLocalAmount'+id);//108

							  values = values + delimeter + validateFieldValue('revisionQuantity'+id);//109
							  values = values + delimeter + validateFieldValue('revisionDiscount'+id);//110
							  values = values + delimeter + validateFieldValue('revisionRate'+id);//111
							  values = values + delimeter + validateFieldValue('revisionSellRate'+id);//112
							  values = values + delimeter + validateFieldValue('revisionExpense'+id);//113
							  values = values + delimeter + validateFieldValue('revisionRevenueAmount'+id);//114
							  values = values + delimeter + validateFieldValue('revisionPassPercentage'+id);//115
							  values = values + delimeter + validateFieldValue('revisionVatDescr'+id);//116
							  values = values + delimeter + validateFieldValue('revisionVatPercent'+id);//117
							  values = values + delimeter + validateFieldValue('revisionVatAmt'+id);//118
							  values = values + delimeter + validateFieldValue('recVatDescr'+id);//119
							  values = values + delimeter + validateFieldValue('recVatPercent'+id);//120
							  values = values + delimeter + validateFieldValue('recVatAmt'+id);//121
							  values = values + delimeter + validateFieldValue('payVatDescr'+id);//122
							  values = values + delimeter + validateFieldValue('payVatPercent'+id);//123
							  values = values + delimeter + validateFieldValue('payVatAmt'+id);//124

							  values = values + delimeter + validateFieldValue('payingStatus'+id);//125
							  values = values + delimeter + validateFieldValue('receivedDate'+id);//126
							  values = values + delimeter + validateFieldValue('invoiceNumber'+id);//127
							  values = values + delimeter + validateFieldValue('invoiceDate'+id);//128
							  values = values + delimeter + validateFieldValue('estimateSellQuantity'+id);//129
							  values = values + delimeter + validateFieldValue('revisionSellQuantity'+id);//130
							  values = values + delimeter + validateFieldValue('accountLineCostElement'+id);//131
							  values = values + delimeter + validateFieldValue('accountLineScostElementDescription'+id);//132
							  values = values + delimeter + validateFieldValue('estExpVatAmt'+id);//133
							  values = values + delimeter + validateFieldValue('revisionExpVatAmt'+id);//134
							  values = values + delimeter + rollUpInInvoiceCheck; //135
							  values = values + delimeter + validateFieldValue('recVatGl'+id);//136
							  values = values + delimeter + validateFieldValue('payVatGl'+id);//137
							  values = values + delimeter + validateFieldValue('qstRecVatGl'+id);//138
							  values = values + delimeter + validateFieldValue('qstPayVatGl'+id);//139
							  values = values + delimeter + validateFieldValue('qstRecVatAmt'+id);//140
							  values = values + delimeter + validateFieldValue('qstPayVatAmt'+id);//141

							  return values;
						  }
						 
						 function getPricingHiddenValue(id){
							  var estCurrencyNew = validateFieldValue('estCurrencyNew'+id); //22
							  var estSellCurrencyNew = validateFieldValue('estSellCurrencyNew'+id);
							  
							  var estValueDateNew = validateFieldValue('estValueDateNew'+id); //24
							  var estSellValueDateNew = validateFieldValue('estSellValueDateNew'+id); //25
							  
							  //alert(estValueDateNew);
							  //alert(estSellValueDateNew);
							  
							  var estExchangeRateNew = validateFieldValue('estExchangeRateNew'+id);
							  var estSellExchangeRateNew = validateFieldValue('estSellExchangeRateNew'+id);
							  var estLocalRateNew = validateFieldValue('estLocalRateNew'+id);  //28
							  var estSellLocalRateNew = validateFieldValue('estSellLocalRateNew'+id); //29
							  
							  var estLocalAmountNew = validateFieldValue('estLocalAmountNew'+id); //30
							  var estSellLocalAmountNew = validateFieldValue('estSellLocalAmountNew'+id);
							  var revisionCurrencyNew = validateFieldValue('revisionCurrencyNew'+id);
							  var countryNew = validateFieldValue('countryNew'+id);
							  
							  var revisionValueDateNew = validateFieldValue('revisionValueDateNew'+id); // 34
							  var valueDateNew = validateFieldValue('valueDateNew'+id);
							  var revisionExchangeRateNew = validateFieldValue('revisionExchangeRateNew'+id);
							  var exchangeRateNew = validateFieldValue('exchangeRateNew'+id); //37
							  
							  var delimeter = "-,-";
							  
							  var values = estCurrencyNew +delimeter+ estSellCurrencyNew +delimeter+ estValueDateNew +delimeter+ estSellValueDateNew;
							  values = values +delimeter+ estExchangeRateNew +delimeter+ estSellExchangeRateNew +delimeter+ estLocalRateNew +delimeter+ estSellLocalRateNew;
							  values = values +delimeter+ estLocalAmountNew +delimeter+ estSellLocalAmountNew +delimeter+ revisionCurrencyNew +delimeter+ countryNew;
							  values = values +delimeter+ revisionValueDateNew +delimeter+ valueDateNew +delimeter+ revisionExchangeRateNew +delimeter+ exchangeRateNew;
							  
							  return values;
						  }
						 
						 function getPricingHiddenValue1(id){
							  var delimeter = "-,-";
							  var values = validateFieldValue('estimatePayableContractCurrencyNew'+id); // 38
							  values = values +delimeter+ validateFieldValue('estimateContractCurrencyNew'+id); //39
							  values = values +delimeter+ validateFieldValue('estimatePayableContractValueDateNew'+id);
							  
							  values = values +delimeter+ validateFieldValue('estimateContractValueDateNew'+id); //41
							  values = values +delimeter+ validateFieldValue('estimatePayableContractExchangeRateNew'+id); //42
							  
							  values = values +delimeter+ validateFieldValue('estimateContractExchangeRateNew'+id);
							  values = values +delimeter+ validateFieldValue('estimatePayableContractRateNew'+id);
							  values = values +delimeter+ validateFieldValue('estimateContractRateNew'+id);
							  values = values +delimeter+ validateFieldValue('estimatePayableContractRateAmmountNew'+id);
							  
							  values = values +delimeter+ validateFieldValue('estimateContractRateAmmountNew'+id); // 47
							  values = values +delimeter+ validateFieldValue('revisionContractCurrencyNew'+id);
							  values = values +delimeter+ validateFieldValue('revisionPayableContractCurrencyNew'+id);
							  values = values +delimeter+ validateFieldValue('contractCurrencyNew'+id); // 50
							  
							  values = values +delimeter+ validateFieldValue('payableContractCurrencyNew'+id);
							  values = values +delimeter+ validateFieldValue('revisionContractValueDateNew'+id);
							  values = values +delimeter+ validateFieldValue('revisionPayableContractValueDateNew'+id);
							  values = values +delimeter+ validateFieldValue('contractValueDateNew'+id);
							  values = values +delimeter+ validateFieldValue('payableContractValueDateNew'+id); // 55
							  values = values +delimeter+ validateFieldValue('revisionContractExchangeRateNew'+id);
							  values = values +delimeter+ validateFieldValue('revisionPayableContractExchangeRateNew'+id);
							  values = values +delimeter+ validateFieldValue('contractExchangeRateNew'+id);
							  values = values +delimeter+ validateFieldValue('payableContractExchangeRateNew'+id); // 59
							  
							  
							  return values;
						  }
						 
						 function calculateRevisionPassPercentage(revenue,expense,estimatePassPercentage){
							    var revenueValue=0;
							    var expenseValue=0;
							    var estimatePassPercentageValue=0; 
							    checkFloatNew('operationResourceForm',estimatePassPercentage,'Nothing'); 
							    revenueValue=eval(document.forms['operationResourceForm'].elements[revenue].value);
							    expenseValue=eval(document.forms['operationResourceForm'].elements[expense].value); 
							    try{
							    	estimatePassPercentageValue=(revenueValue/expenseValue)*100;
							    }catch(e){
							    	estimatePassPercentageValue=0.00;
							        } 
							    estimatePassPercentageValue=Math.round(estimatePassPercentageValue);
							    if(document.forms['operationResourceForm'].elements[expense].value == '' || document.forms['operationResourceForm'].elements[expense].value == 0||document.forms['operationResourceForm'].elements[expense].value == 0.00){
							  	   document.forms['operationResourceForm'].elements[estimatePassPercentage].value='';
							  	   }else{ 
							  	   	document.forms['operationResourceForm'].elements[estimatePassPercentage].value = estimatePassPercentageValue;
							  	   } 
							    }
						 
						 function changeVatAmt(estVatDec,estVatPer,revenue,estAmt,estSellAmt) {
					    	    var aid=estVatDec.replace('vatDecr','');
					    	    aid=aid.trim();
						    	calculateVatSection('EST',aid,'EST');
						     }  
							function calculateVatAmt(estVatPer,revenue,estAmt){
					    	    var aid=estVatPer.replace('vatPers' ,'');
					    	    aid=aid.trim();
								calculateVatAmtSection('EST',aid);		 
							} 
							function calculateVatSectionAll(type,aid,type1){
								  var vatExclude=document.forms['operationResourceForm'].elements['VATExclude'+aid].value;
								  if(vatExclude==null || vatExclude==false || vatExclude=='false'){
								  document.forms['operationResourceForm'].elements['payVatDescr'+aid].disabled = false;
								  document.forms['operationResourceForm'].elements['recVatDescr'+aid].disabled = false;
								  var chargeCode=document.forms['operationResourceForm'].elements['chargeCode'+aid].value;
								  var vendorCode=document.forms['operationResourceForm'].elements['vendorCode'+aid].value; 
								  var recGl=document.forms['operationResourceForm'].elements['recGl'+aid].value;
								  var payGl=document.forms['operationResourceForm'].elements['payGl'+aid].value;   
								  var actgCode=document.forms['operationResourceForm'].elements['actgCode'+aid].value;
								  var acref="${accountInterface}";
								  acref=acref.trim();
								  if(vendorCode.trim()=='' && type1!='BAS' && type1=='PAY'){
									  alert("Vendor Code Missing. Please select");
								  }else if(chargeCode.trim()=='' && type1!='BAS' && type1!='FX' && (type1=='REC' || type1=='PAY')){
									  alert("Charge code needs to be selected");
								  }	else if(payGl.trim()=='' && type1!='BAS' && type1=='PAY' && acref =='Y'){
									  alert("Pay GL missing. Please select another charge code.");
								  }	else if(recGl.trim()=='' && type1!='BAS' && type1=='REC' && acref =='Y'){
									  alert("Receivable GL missing. Please select another charge code.");
								  }else if(actgCode.trim()=='' && type1!='BAS' && acref =='Y' && type1=='PAY'){
									  alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
								  }else if(type=='EXPENSE'){
									       <c:forEach var="entry" items="${payVatPercentList}">
									       if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value=='${entry.key}') { 
									     document.forms['operationResourceForm'].elements['payVatPercent'+aid].value='${entry.value}'; 
									     calculateVatAmtSectionAll(type,aid);
									    }
									   </c:forEach>
									        if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value==''){
									         document.forms['operationResourceForm'].elements['payVatPercent'+aid].value=0;
									         calculateVatAmtSectionAll(type,aid);
									        } 
										     <configByCorp:fieldVisibility componentId="accountLine.payVatGl">
										     var relo1='';
											 	<c:forEach var="entry1" items="${payVatPayGLMap}" > 
											 	if(relo1 ==''){
											 	  if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
											 		 document.forms['operationResourceForm'].elements['payVatGl'+aid].value='${entry1.value}'; 
											 	      relo1 ="yes";
											 	      }   }
											 	     </c:forEach>
											 	    relo1='';
												 	<c:forEach var="entry1" items="${qstPayVatPayGLMap}" > 
												 	if(relo1 ==''){
												 	  if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
												 		 document.forms['operationResourceForm'].elements['qstPayVatGl'+aid].value='${entry1.value}'; 
												 	      relo1 ="yes";
												 	      }   }
												 	     </c:forEach>		 	     
										 </configByCorp:fieldVisibility>
									          
								        
								  }else if(type=='REVENUE'){
								       <c:forEach var="entry" items="${estVatPersentList}">
								       if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
								     document.forms['operationResourceForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
								     calculateVatAmtSectionAll(type,aid);
								    }
								   </c:forEach>
								        if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value==''){
								         document.forms['operationResourceForm'].elements['recVatPercent'+aid].value=0;
								         calculateVatAmtSectionAll(type,aid);
								        }
								        <configByCorp:fieldVisibility componentId="accountLine.recVatGL"> 
								        var relo1=""  
								            <c:forEach var="entry1" items="${euvatRecGLMap}">
								               if(relo1==""){ 
								            	   
								               if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value=='${entry1.key}') {
								            	   document.forms['operationResourceForm'].elements['recVatGl'+aid].value='${entry1.value}';  
								               relo1="yes"; 
								              }  }
								           </c:forEach>
								           relo1='';
								    	 	<c:forEach var="entry1" items="${qstEuvatRecGLMap}" > 
								    	 	if(relo1 ==''){
								    	 	  if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value=='${entry1.key}'){
								    	 		 document.forms['operationResourceForm'].elements['qstRecVatGl'+aid].value='${entry1.value}'; 
								    	 	      relo1 ="yes";
								    	 	      }   }
								    	 	     </c:forEach>        
								          </configByCorp:fieldVisibility> 
								        
								  }else {
								  }
								  }else{
					  				  document.forms['operationResourceForm'].elements['payVatDescr'+aid].value="";
									  document.forms['operationResourceForm'].elements['recVatDescr'+aid].value="";			  
									  document.forms['operationResourceForm'].elements['payVatDescr'+aid].disabled = true;
									  document.forms['operationResourceForm'].elements['recVatDescr'+aid].disabled = true;
									  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								document.forms['operationResourceForm'].elements['recVatAmt'+aid].value=0;
								document.forms['operationResourceForm'].elements['recVatPercent'+aid].value=0;
								document.forms['operationResourceForm'].elements['payVatAmt'+aid].value=0;
								document.forms['operationResourceForm'].elements['payVatPercent'+aid].value=0;
								document.forms['operationResourceForm'].elements['recVatAmtTemp'+aid].value=0;
								document.forms['operationResourceForm'].elements['payVatAmtTemp'+aid].value=0;
								
								</c:if>
								document.forms['operationResourceForm'].elements['vatAmt'+aid].value=0;			
								document.forms['operationResourceForm'].elements['estExpVatAmt'+aid].value=0;			
								<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								document.forms['operationResourceForm'].elements['revisionVatAmt'+aid].value=0;
								document.forms['operationResourceForm'].elements['revisionExpVatAmt'+aid].value=0;			
								</c:if>
								  }
								 }
							function calculateVatAmtSectionAll(type,aid){
								  var vatExclude=document.forms['operationResourceForm'].elements['VATExclude'+aid].value;
								  if(vatExclude==null || vatExclude==false || vatExclude=='false'){
								  document.forms['operationResourceForm'].elements['payVatDescr'+aid].disabled = false;
								  document.forms['operationResourceForm'].elements['recVatDescr'+aid].disabled = false;
								 
								 if(type=='EXPENSE'){
									  try{	 
										   var payVatAmt=0.00;
										   var actualExpense=0.00;
										   if(document.forms['operationResourceForm'].elements['payVatPercent'+aid].value!=''){
										    var payVatPercent=document.forms['operationResourceForm'].elements['payVatPercent'+aid].value;
										    actualExpense = document.forms['operationResourceForm'].elements['actualExpense'+aid].value;
										    <c:if test="${contractType}"> 
											actualExpense=  document.forms['operationResourceForm'].elements['localAmount'+aid].value;
											</c:if>
											  
											var firstPersent="";
											var secondPersent=""; 
											var relo="" 
											     <c:forEach var="entry" items="${qstPayVatPayGLAmtMap}">
											        if(relo==""){ 
											        if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value=='${entry.key}') {
												        var arr='${entry.value}';
												        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
												        	firstPersent=arr.split(':')[0];
												        	secondPersent=arr.split(':')[1];
												        }
											        relo="yes"; 
											       }  }
											    </c:forEach> 
											    if(firstPersent!='' && secondPersent!=''){
												    var p1=parseFloat(firstPersent);
												    var p2=parseFloat(secondPersent);
												    var temp=0.00;
												    payVatAmt=(actualExpense*p1)/100;
												    temp=payVatAmt;
												    payVatAmt=Math.round(payVatAmt*10000)/10000; 
												    document.forms['operationResourceForm'].elements['payVatAmt'+aid].value=payVatAmt;
													payVatAmt=(actualExpense*p2)/100;
													temp=temp+payVatAmt;
													payVatAmt=Math.round(payVatAmt*10000)/10000; 
													document.forms['operationResourceForm'].elements['qstPayVatAmt'+aid].value=payVatAmt;
													document.forms['operationResourceForm'].elements['payVatAmtTemp'+aid].value=temp;
											    }else{
												    payVatAmt=(actualExpense*payVatPercent)/100;
												    payVatAmt=Math.round(payVatAmt*10000)/10000; 
												    document.forms['operationResourceForm'].elements['payVatAmt'+aid].value=payVatAmt;
												    document.forms['operationResourceForm'].elements['payVatAmtTemp'+aid].value=payVatAmt;
												    document.forms['operationResourceForm'].elements['qstPayVatAmt'+aid].value=0.00;						    
											    }
										   }
										 }catch(e){}  

										  try{
										   var estExpVatAmt=0;
										   if(document.forms['operationResourceForm'].elements['payVatPercent'+aid].value!=''){
										    var payVatPercent=eval(document.forms['operationResourceForm'].elements['payVatPercent'+aid].value);
										    var estimateExpense= eval(document.forms['operationResourceForm'].elements['estimateExpense'+aid].value);
										    <c:if test="${contractType}">
										    estimateExpense= eval(document.forms['operationResourceForm'].elements['estLocalAmountNew'+aid].value); 
											</c:if>
											estExpVatAmt=(estimateExpense*payVatPercent)/100;
											estExpVatAmt=Math.round(estExpVatAmt*10000)/10000; 
										    document.forms['operationResourceForm'].elements['estExpVatAmt'+aid].value=estExpVatAmt;
										    }
											}catch(e){} 

											  try{
										   var revisionExpVatAmt=0;
										   if(document.forms['operationResourceForm'].elements['payVatPercent'+aid].value!=''){
										    var payVatPercent=eval(document.forms['operationResourceForm'].elements['payVatPercent'+aid].value);
										    var revisionExpense= eval(document.forms['operationResourceForm'].elements['revisionExpense'+aid].value);
										    <c:if test="${contractType}">
										    revisionExpense= eval(document.forms['operationResourceForm'].elements['revisionLocalAmount'+aid].value); 
											</c:if>
											revisionExpVatAmt=(revisionExpense*payVatPercent)/100;
											revisionExpVatAmt=Math.round(revisionExpVatAmt*10000)/10000; 
										    document.forms['operationResourceForm'].elements['revisionExpVatAmt'+aid].value=revisionExpVatAmt;
										    }
											}catch(e){} 
									 
								 }else if(type=='REVENUE'){
									   try{
										   var recVatAmt=0;
										   if(document.forms['operationResourceForm'].elements['recVatPercent'+aid].value!=''){
										    var recVatPercent=eval(document.forms['operationResourceForm'].elements['recVatPercent'+aid].value);
										    var actualRevenue= eval(document.forms['operationResourceForm'].elements['actualRevenue'+aid].value);
										    <c:if test="${contractType}"> 
										    actualRevenue=  document.forms['operationResourceForm'].elements['actualRevenueForeign'+aid].value;
											</c:if>
											var firstPersent="";
											var secondPersent=""; 
											var relo="" 
											     <c:forEach var="entry" items="${qstEuvatRecGLAmtMap}">
											        if(relo==""){ 
											        if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
												        var arr='${entry.value}';
												        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
												        	firstPersent=arr.split(':')[0];
												        	secondPersent=arr.split(':')[1];
												        }
											        relo="yes"; 
											       }  }
											    </c:forEach> 
											    if(firstPersent!='' && secondPersent!=''){
												    var p1=parseFloat(firstPersent);
												    var p2=parseFloat(secondPersent);
												    var temp=0.00;
													recVatAmt=(actualRevenue*p1)/100;
													temp=recVatAmt;
													recVatAmt=Math.round(recVatAmt*10000)/10000; 
													document.forms['operationResourceForm'].elements['recVatAmt'+aid].value=recVatAmt;
													recVatAmt=(actualRevenue*p2)/100;
													temp=temp+recVatAmt;
													recVatAmt=Math.round(recVatAmt*10000)/10000;
													document.forms['operationResourceForm'].elements['qstRecVatAmt'+aid].value=recVatAmt;
													document.forms['operationResourceForm'].elements['recVatAmtTemp'+aid].value=temp;
											    }else{
												    recVatAmt=(actualRevenue*recVatPercent)/100;
												    recVatAmt=Math.round(recVatAmt*10000)/10000; 
												    document.forms['operationResourceForm'].elements['recVatAmt'+aid].value=recVatAmt;
												    document.forms['operationResourceForm'].elements['recVatAmtTemp'+aid].value=recVatAmt;
												    document.forms['operationResourceForm'].elements['qstRecVatAmt'+aid].value=0.00;
											    }
										    }
										   }catch(e){}  
									  try{
										   var estVatAmt=0;
										   if(document.forms['operationResourceForm'].elements['recVatPercent'+aid].value!=''){
										    var estVatPercent=eval(document.forms['operationResourceForm'].elements['recVatPercent'+aid].value);
										    var estimateRevenueAmount= eval(document.forms['operationResourceForm'].elements['estimateRevenueAmount'+aid].value);
										    <c:if test="${contractType}">
										    estimateRevenueAmount= eval(document.forms['operationResourceForm'].elements['estSellLocalAmountNew'+aid].value); 
											</c:if>
											estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
											estVatAmt=Math.round(estVatAmt*10000)/10000; 
										    document.forms['operationResourceForm'].elements['vatAmt'+aid].value=estVatAmt;
										    }
											}catch(e){} 


											  try{
										   var revisionVatAmt=0;
										   if(document.forms['operationResourceForm'].elements['recVatPercent'+aid].value!=''){
										    var revisionVatPercent=eval(document.forms['operationResourceForm'].elements['recVatPercent'+aid].value);
										    var revisionRevenueAmount= eval(document.forms['operationResourceForm'].elements['revisionRevenueAmount'+aid].value);
										    <c:if test="${contractType}">
										    revisionRevenueAmount= eval(document.forms['operationResourceForm'].elements['revisionSellLocalAmount'+aid].value); 
											</c:if>
										    revisionVatAmt=(revisionRevenueAmount*revisionVatPercent)/100;
										    revisionVatAmt=Math.round(revisionVatAmt*10000)/10000; 
										    document.forms['operationResourceForm'].elements['revisionVatAmt'+aid].value=revisionVatAmt;
										    }
											}catch(e){} 


									 
								 }else{
								 }
								  }else{
					  				  document.forms['operationResourceForm'].elements['payVatDescr'+aid].value="";
									  document.forms['operationResourceForm'].elements['recVatDescr'+aid].value="";				  
									  document.forms['operationResourceForm'].elements['payVatDescr'+aid].disabled = true;
									  document.forms['operationResourceForm'].elements['recVatDescr'+aid].disabled = true;					  
									  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
							document.forms['operationResourceForm'].elements['recVatAmt'+aid].value=0;
							document.forms['operationResourceForm'].elements['recVatPercent'+aid].value=0;
							document.forms['operationResourceForm'].elements['payVatAmt'+aid].value=0;
							document.forms['operationResourceForm'].elements['payVatPercent'+aid].value=0;
							document.forms['operationResourceForm'].elements['recVatAmtTemp'+aid].value=0;
							document.forms['operationResourceForm'].elements['payVatAmtTemp'+aid].value=0;
							
							</c:if>
							document.forms['operationResourceForm'].elements['vatAmt'+aid].value=0;			
							document.forms['operationResourceForm'].elements['estExpVatAmt'+aid].value=0;			
							<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
							document.forms['operationResourceForm'].elements['revisionVatAmt'+aid].value=0;
							document.forms['operationResourceForm'].elements['revisionExpVatAmt'+aid].value=0;			
									  </c:if>
								  }
							 }
							
							function calculateVatSection(type,aid,type1){
								  var vatExclude=document.forms['operationResourceForm'].elements['VATExclude'+aid].value;
								  if(vatExclude==null || vatExclude==false || vatExclude=='false'){
								  document.forms['operationResourceForm'].elements['payVatDescr'+aid].disabled = false;
								  document.forms['operationResourceForm'].elements['recVatDescr'+aid].disabled = false;
								
								  var chargeCode=document.forms['operationResourceForm'].elements['chargeCode'+aid].value;
								  var vendorCode=document.forms['operationResourceForm'].elements['vendorCode'+aid].value; 
								  var recGl=document.forms['operationResourceForm'].elements['recGl'+aid].value;
								  var payGl=document.forms['operationResourceForm'].elements['payGl'+aid].value;   
								  var actgCode=document.forms['operationResourceForm'].elements['actgCode'+aid].value;
								  var acref="${accountInterface}";
								  acref=acref.trim();
								  if(vendorCode.trim()=='' && type1!='BAS' && type1=='PAY'){
									  alert("Vendor Code Missing. Please select");
								  }else if(chargeCode.trim()=='' && type1!='BAS' && type1!='FX' && (type1=='REC' || type1=='PAY')){
									  alert("Charge code needs to be selected");
								  }	else if(payGl.trim()=='' && type1!='BAS' && type1=='PAY' && acref =='Y'){
									  alert("Pay GL missing. Please select another charge code.");
								  }	else if(recGl.trim()=='' && type1!='BAS' && type1=='REC' && acref =='Y'){
									  alert("Receivable GL missing. Please select another charge code.");
								  }else if(actgCode.trim()=='' && type1!='BAS' && acref =='Y' && type1=='PAY'){
									  alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
								  }else if(type=='EST'){
								       <c:forEach var="entry" items="${estVatPersentList}">
								       if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
								     document.forms['operationResourceForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
								     calculateVatAmtSection(type,aid);
								    }
								   </c:forEach>
								        if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value==''){
								         document.forms['operationResourceForm'].elements['recVatPercent'+aid].value=0;
								         calculateVatAmtSection(type,aid);
								        }
									       <c:forEach var="entry" items="${payVatPercentList}">
									       if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value=='${entry.key}') { 
									     document.forms['operationResourceForm'].elements['payVatPercent'+aid].value='${entry.value}'; 
									     calculateVatAmtSection(type,aid);
									    }
									   </c:forEach>
									        if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value==''){
									         document.forms['operationResourceForm'].elements['payVatPercent'+aid].value=0;
									         calculateVatAmtSection(type,aid);
									        }   
								        
								  }else if(type=='REV'){
								       <c:forEach var="entry" items="${estVatPersentList}">
								       if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
								     document.forms['operationResourceForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
								     calculateVatAmtSection(type,aid);
								    }
								   </c:forEach>
								        if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value==''){
								         document.forms['operationResourceForm'].elements['recVatPercent'+aid].value=0;
								         calculateVatAmtSection(type,aid);
								        }
									       <c:forEach var="entry" items="${payVatPercentList}">
									       if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value=='${entry.key}') { 
									     document.forms['operationResourceForm'].elements['payVatPercent'+aid].value='${entry.value}'; 
									     calculateVatAmtSection(type,aid);
									    }
									   </c:forEach>
									        if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value==''){
									         document.forms['operationResourceForm'].elements['payVatPercent'+aid].value=0;
									         calculateVatAmtSection(type,aid);
									        }   
								        
								  }else if(type=='REC'){
								       <c:forEach var="entry" items="${estVatPersentList}">
								       if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
								     document.forms['operationResourceForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
								     calculateVatAmtSection(type,aid);
								    }
								   </c:forEach>
								        if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value==''){
								         document.forms['operationResourceForm'].elements['recVatPercent'+aid].value=0;
								         calculateVatAmtSection(type,aid);
								        } 

								        <configByCorp:fieldVisibility componentId="accountLine.recVatGL"> 
								        var relo1=""  
								            <c:forEach var="entry1" items="${euvatRecGLMap}">
								               if(relo1==""){ 
								            	   
								               if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value=='${entry1.key}') {
								            	   document.forms['operationResourceForm'].elements['recVatGl'+aid].value='${entry1.value}';  
								               relo1="yes"; 
								              }  }
								           </c:forEach>
								           relo1='';
								    	 	<c:forEach var="entry1" items="${qstEuvatRecGLMap}" > 
								    	 	if(relo1 ==''){
								    	 	  if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value=='${entry1.key}'){
								    	 		 document.forms['operationResourceForm'].elements['qstRecVatGl'+aid].value='${entry1.value}'; 
								    	 	      relo1 ="yes";
								    	 	      }   }
								    	 	     </c:forEach>        
								          </configByCorp:fieldVisibility> 
								          
								  }else if(type=='PAY'){
								       <c:forEach var="entry" items="${payVatPercentList}">
								       if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value=='${entry.key}') { 
								     document.forms['operationResourceForm'].elements['payVatPercent'+aid].value='${entry.value}'; 
								     calculateVatAmtSection(type,aid);
								    }
								   </c:forEach>
								        if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value==''){
								         document.forms['operationResourceForm'].elements['payVatPercent'+aid].value=0;
								         calculateVatAmtSection(type,aid);
								        } 

									     <configByCorp:fieldVisibility componentId="accountLine.payVatGl">
									     var relo1='';
										 	<c:forEach var="entry1" items="${payVatPayGLMap}" > 
										 	if(relo1 ==''){
										 	  if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
										 		 document.forms['operationResourceForm'].elements['payVatGl'+aid].value='${entry1.value}'; 
										 	      relo1 ="yes";
										 	      }   }
										 	     </c:forEach>
										 	    relo1='';
											 	<c:forEach var="entry1" items="${qstPayVatPayGLMap}" > 
											 	if(relo1 ==''){
											 	  if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
											 		 document.forms['operationResourceForm'].elements['qstPayVatGl'+aid].value='${entry1.value}'; 
											 	      relo1 ="yes";
											 	      }   }
											 	     </c:forEach>		 	     
									 </configByCorp:fieldVisibility>
								          
								  }else{
								  }
								  }else{
					  				  document.forms['operationResourceForm'].elements['payVatDescr'+aid].value="";
									  document.forms['operationResourceForm'].elements['recVatDescr'+aid].value="";			  
									  document.forms['operationResourceForm'].elements['payVatDescr'+aid].disabled = true;
									  document.forms['operationResourceForm'].elements['recVatDescr'+aid].disabled = true;
									  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								document.forms['operationResourceForm'].elements['recVatAmt'+aid].value=0;
								document.forms['operationResourceForm'].elements['recVatPercent'+aid].value=0;
								document.forms['operationResourceForm'].elements['payVatAmt'+aid].value=0;
								document.forms['operationResourceForm'].elements['payVatPercent'+aid].value=0;
								document.forms['operationResourceForm'].elements['recVatAmtTemp'+aid].value=0;
								document.forms['operationResourceForm'].elements['payVatAmtTemp'+aid].value=0;
								
								</c:if>
								document.forms['operationResourceForm'].elements['vatAmt'+aid].value=0;			
								document.forms['operationResourceForm'].elements['estExpVatAmt'+aid].value=0;			
								<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								document.forms['operationResourceForm'].elements['revisionVatAmt'+aid].value=0;
								document.forms['operationResourceForm'].elements['revisionExpVatAmt'+aid].value=0;			
									  </c:if>
								  }
								 }
							 function calculateVatAmtSection(type,aid){
								  var vatExclude=document.forms['operationResourceForm'].elements['VATExclude'+aid].value;
								  if(vatExclude==null || vatExclude==false || vatExclude=='false'){
								  document.forms['operationResourceForm'].elements['payVatDescr'+aid].disabled = false;
								  document.forms['operationResourceForm'].elements['recVatDescr'+aid].disabled = false;
							  
							 if(type=='EST'){
									  try{
								   var estVatAmt=0;
								   if(document.forms['operationResourceForm'].elements['recVatPercent'+aid].value!=''){
								    var estVatPercent=eval(document.forms['operationResourceForm'].elements['recVatPercent'+aid].value);
								    var estimateRevenueAmount= eval(document.forms['operationResourceForm'].elements['estimateRevenueAmount'+aid].value);
								    <c:if test="${contractType}">
								    estimateRevenueAmount= eval(document.forms['operationResourceForm'].elements['estSellLocalAmountNew'+aid].value); 
									</c:if>
									estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
									estVatAmt=Math.round(estVatAmt*10000)/10000; 
								    document.forms['operationResourceForm'].elements['vatAmt'+aid].value=estVatAmt;
								    }
									}catch(e){} 
								  try{
								   var estExpVatAmt=0;
								   if(document.forms['operationResourceForm'].elements['payVatPercent'+aid].value!=''){
								    var payVatPercent=eval(document.forms['operationResourceForm'].elements['payVatPercent'+aid].value);
								    var estimateExpense= eval(document.forms['operationResourceForm'].elements['estimateExpense'+aid].value);
								    <c:if test="${contractType}">
								    estimateExpense= eval(document.forms['operationResourceForm'].elements['estLocalAmountNew'+aid].value); 
									</c:if>
									estExpVatAmt=(estimateExpense*payVatPercent)/100;
									estExpVatAmt=Math.round(estExpVatAmt*10000)/10000; 
								    document.forms['operationResourceForm'].elements['estExpVatAmt'+aid].value=estExpVatAmt;
								    }
									}catch(e){} 
									
							  }else if(type=='REV'){ 
								  try{
							   var revisionVatAmt=0;
							   if(document.forms['operationResourceForm'].elements['recVatPercent'+aid].value!=''){
							    var revisionVatPercent=eval(document.forms['operationResourceForm'].elements['recVatPercent'+aid].value);
							    var revisionRevenueAmount= eval(document.forms['operationResourceForm'].elements['revisionRevenueAmount'+aid].value);
							    <c:if test="${contractType}">
							    revisionRevenueAmount= eval(document.forms['operationResourceForm'].elements['revisionSellLocalAmount'+aid].value); 
								</c:if>
							    revisionVatAmt=(revisionRevenueAmount*revisionVatPercent)/100;
							    revisionVatAmt=Math.round(revisionVatAmt*10000)/10000; 
							    document.forms['operationResourceForm'].elements['revisionVatAmt'+aid].value=revisionVatAmt;
							    }
								}catch(e){} 
								  try{
							   var revisionExpVatAmt=0;
							   if(document.forms['operationResourceForm'].elements['payVatPercent'+aid].value!=''){
							    var payVatPercent=eval(document.forms['operationResourceForm'].elements['payVatPercent'+aid].value);
							    var revisionExpense= eval(document.forms['operationResourceForm'].elements['revisionExpense'+aid].value);
							    <c:if test="${contractType}">
							    revisionExpense= eval(document.forms['operationResourceForm'].elements['revisionLocalAmount'+aid].value); 
								</c:if>
								revisionExpVatAmt=(revisionExpense*payVatPercent)/100;
								revisionExpVatAmt=Math.round(revisionExpVatAmt*10000)/10000; 
							    document.forms['operationResourceForm'].elements['revisionExpVatAmt'+aid].value=revisionExpVatAmt;
							    }
								}catch(e){} 
							  }else if(type=='REC'){				  
							   try{
							   var recVatAmt=0;
							   if(document.forms['operationResourceForm'].elements['recVatPercent'+aid].value!=''){
							    var recVatPercent=eval(document.forms['operationResourceForm'].elements['recVatPercent'+aid].value);
							    var actualRevenue= eval(document.forms['operationResourceForm'].elements['actualRevenue'+aid].value);
							    <c:if test="${contractType}"> 
							    actualRevenue=  document.forms['operationResourceForm'].elements['actualRevenueForeign'+aid].value;
								</c:if>
								var firstPersent="";
								var secondPersent=""; 
								var relo="" 
								     <c:forEach var="entry" items="${qstEuvatRecGLAmtMap}">
								        if(relo==""){ 
								        if(document.forms['operationResourceForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
									        var arr='${entry.value}';
									        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
									        	firstPersent=arr.split(':')[0];
									        	secondPersent=arr.split(':')[1];
									        }
								        relo="yes"; 
								       }  }
								    </c:forEach> 
								    if(firstPersent!='' && secondPersent!=''){
									    var p1=parseFloat(firstPersent);
									    var p2=parseFloat(secondPersent);
									    var temp=0.00;
										recVatAmt=(actualRevenue*p1)/100;
										temp=recVatAmt;
										recVatAmt=Math.round(recVatAmt*10000)/10000; 
										document.forms['operationResourceForm'].elements['recVatAmt'+aid].value=recVatAmt;
										recVatAmt=(actualRevenue*p2)/100;
										temp=temp+recVatAmt;
										recVatAmt=Math.round(recVatAmt*10000)/10000;
										document.forms['operationResourceForm'].elements['qstRecVatAmt'+aid].value=recVatAmt;
										document.forms['operationResourceForm'].elements['recVatAmtTemp'+aid].value=temp;
								    }else{
									    recVatAmt=(actualRevenue*recVatPercent)/100;
									    recVatAmt=Math.round(recVatAmt*10000)/10000; 
									    document.forms['operationResourceForm'].elements['recVatAmt'+aid].value=recVatAmt;
									    document.forms['operationResourceForm'].elements['recVatAmtTemp'+aid].value=recVatAmt;
									    document.forms['operationResourceForm'].elements['qstRecVatAmt'+aid].value=0.00;
								    }
							    }
							   }catch(e){}    
							  }else if(type=='PAY'){
								  try{	 
								   var payVatAmt=0.00;
								   var actualExpense=0.00;
								   if(document.forms['operationResourceForm'].elements['payVatPercent'+aid].value!=''){
								    var payVatPercent=document.forms['operationResourceForm'].elements['payVatPercent'+aid].value;
								    actualExpense = document.forms['operationResourceForm'].elements['actualExpense'+aid].value;
								    <c:if test="${contractType}"> 
									actualExpense=  document.forms['operationResourceForm'].elements['localAmount'+aid].value;
									</c:if>
									  
									var firstPersent="";
									var secondPersent=""; 
									var relo="" 
									     <c:forEach var="entry" items="${qstPayVatPayGLAmtMap}">
									        if(relo==""){ 
									        if(document.forms['operationResourceForm'].elements['payVatDescr'+aid].value=='${entry.key}') {
										        var arr='${entry.value}';
										        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
										        	firstPersent=arr.split(':')[0];
										        	secondPersent=arr.split(':')[1];
										        }
									        relo="yes"; 
									       }  }
									    </c:forEach> 
									    if(firstPersent!='' && secondPersent!=''){
										    var p1=parseFloat(firstPersent);
										    var p2=parseFloat(secondPersent);
										    var temp=0.00;
										    payVatAmt=(actualExpense*p1)/100;
										    temp=payVatAmt;
										    payVatAmt=Math.round(payVatAmt*10000)/10000; 
										    document.forms['operationResourceForm'].elements['payVatAmt'+aid].value=payVatAmt;
											payVatAmt=(actualExpense*p2)/100;
											temp=temp+payVatAmt;
											payVatAmt=Math.round(payVatAmt*10000)/10000; 
											document.forms['operationResourceForm'].elements['qstPayVatAmt'+aid].value=payVatAmt;
											document.forms['operationResourceForm'].elements['payVatAmtTemp'+aid].value=temp;
									    }else{
										    payVatAmt=(actualExpense*payVatPercent)/100;
										    payVatAmt=Math.round(payVatAmt*10000)/10000; 
										    document.forms['operationResourceForm'].elements['payVatAmt'+aid].value=payVatAmt;
										    document.forms['operationResourceForm'].elements['payVatAmtTemp'+aid].value=payVatAmt;
										    document.forms['operationResourceForm'].elements['qstPayVatAmt'+aid].value=0.00;						    
									    }
								   }
								 }catch(e){}  
								  
							  }else{
							  }
								  }else{
					  				  document.forms['operationResourceForm'].elements['payVatDescr'+aid].value="";
									  document.forms['operationResourceForm'].elements['recVatDescr'+aid].value="";					  				  
									  document.forms['operationResourceForm'].elements['payVatDescr'+aid].disabled = true;
									  document.forms['operationResourceForm'].elements['recVatDescr'+aid].disabled = true;
									  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
							document.forms['operationResourceForm'].elements['recVatAmt'+aid].value=0;
							document.forms['operationResourceForm'].elements['recVatPercent'+aid].value=0;
							document.forms['operationResourceForm'].elements['payVatAmt'+aid].value=0;
							document.forms['operationResourceForm'].elements['payVatPercent'+aid].value=0;
							document.forms['operationResourceForm'].elements['recVatAmtTemp'+aid].value=0;
							document.forms['operationResourceForm'].elements['payVatAmtTemp'+aid].value=0;
							
							</c:if>
							document.forms['operationResourceForm'].elements['vatAmt'+aid].value=0;			
							document.forms['operationResourceForm'].elements['estExpVatAmt'+aid].value=0;			
							<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
							document.forms['operationResourceForm'].elements['revisionVatAmt'+aid].value=0;
							document.forms['operationResourceForm'].elements['revisionExpVatAmt'+aid].value=0;			
							</c:if>
									  
								  }
							    
							 }
							
							 function isSoExtFlag(){
							     document.forms['operationResourceForm'].elements['isSOExtract'].value="yes";
						       }
							
							 function calculateRevenueNew(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid){
								   var revenueValue=0;
								    var revenueRound=0;
								    var oldRevenue=0;
								    var oldRevenueSum=0;
								    var balanceRevenue=0;
								    oldRevenue=eval(document.forms['operationResourceForm'].elements[revenue].value);
								    <c:if test="${not empty serviceOrder.id}">
								    oldRevenueSum=eval(document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
								    </c:if>
								    var basisValue = document.forms['operationResourceForm'].elements[basis].value;
								    checkFloatNew('operationResourceForm',quantity,'Nothing');
								    var quantityValue = document.forms['operationResourceForm'].elements[quantity].value;
								    checkFloatNew('operationResourceForm',sellRate,'Nothing');
								    var sellRateValue = document.forms['operationResourceForm'].elements[sellRate].value;
								    if(basisValue=="cwt" || basisValue== "%age"){
								    revenueValue=(quantityValue*sellRateValue)/100 ;
								    }
								    else if(basisValue=="per 1000"){
								    revenueValue=(quantityValue*sellRateValue)/1000 ;
								    }else{
								    revenueValue=(quantityValue*sellRateValue);
								    } 
								    revenueRound=Math.round(revenueValue*10000)/10000;
								    balanceRevenue=revenueRound-oldRevenue;
								    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
								    balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
								    oldRevenueSum=oldRevenueSum+balanceRevenue; 
								    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
								    var revenueRoundNew=""+revenueRound;
								    if(revenueRoundNew.indexOf(".") == -1){
								    revenueRoundNew=revenueRoundNew+".00";
								    } 
								    if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
								    revenueRoundNew=revenueRoundNew+"0";
								    }
								    document.forms['operationResourceForm'].elements[revenue].value=revenueRoundNew;
								    try{
								        var buyRate1=document.forms['operationResourceForm'].elements['estimateSellRate'+aid].value;
								        var estCurrency1=document.forms['operationResourceForm'].elements['estSellCurrencyNew'+aid].value;
										if(estCurrency1.trim()!=""){
												var Q1=0;
												var Q2=0;
												Q1=buyRate1;
												Q2=document.forms['operationResourceForm'].elements['estSellExchangeRateNew'+aid].value;
									            var E1=Q1*Q2;
									            E1=Math.round(E1*10000)/10000;
									            document.forms['operationResourceForm'].elements['estSellLocalRateNew'+aid].value=E1;
									            var estLocalAmt1=0;
									            if(basisValue=="cwt" || basisValue== "%age"){
									            	estLocalAmt1=(quantityValue*E1)/100 ;
									                }
									                else if(basisValue=="per 1000"){
									                	estLocalAmt1=(quantityValue*E1)/1000 ;
									                }else{
									                	estLocalAmt1=(quantityValue*E1);
									                }	 
									            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
									            document.forms['operationResourceForm'].elements['estSellLocalAmountNew'+aid].value=estLocalAmt1;     
										}
										<c:if test="${contractType}">
											try{
												var estimateContractExchangeRate=0;
												estimateContractExchangeRate=document.forms['operationResourceForm'].elements['estimateContractExchangeRateNew'+aid].value; 
												var QbuyRate1=0;
												QbuyRate1=buyRate1;
												document.forms['operationResourceForm'].elements['estimateContractRateNew'+aid].value=QbuyRate1*estimateContractExchangeRate
												var Q11=0;
												var Q21=0; 
												Q11=document.forms['operationResourceForm'].elements[quantity].value;
												Q21=document.forms['operationResourceForm'].elements['estimateContractRateNew'+aid].value;
									            var E11=Q11*Q21;
									            var estLocalAmt11=0;
									            if(basisValue=="cwt" || basisValue== "%age"){
									            	estLocalAmt11=E11/100 ;
									                }
									                else if(basisValue=="per 1000"){
									                	estLocalAmt11=E11/1000 ;
									                }else{
									                	estLocalAmt11=E11;
									                }	 
									            estLocalAmt11=Math.round(estLocalAmt11*10000)/10000;    
									            document.forms['operationResourceForm'].elements['estimateContractRateAmmountNew'+aid].value=estLocalAmt11;
											}catch(e){}     
										</c:if>
								    }catch(e){}
								    <c:if test="${not empty serviceOrder.id}">
								    var newRevenueSum=""+oldRevenueSum;
								    if(newRevenueSum.indexOf(".") == -1){
								    newRevenueSum=newRevenueSum+".00";
								    } 
								    if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
								    newRevenueSum=newRevenueSum+"0";
								    } 
								    document.forms['operationResourceForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
								    </c:if>
								    if(revenueRoundNew>0){
								    document.forms['operationResourceForm'].elements[displayOnQuots].checked = true;
								    }
								    try{
								    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
								    calculateVatAmt(estVatPer,revenue,estAmt);
								    </c:if>
								    }catch(e){} 	  
								   calculateGrossMargin();
								 }
							 function copyPartnerPricingDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId,pricingAccountLineId){
									lastName=lastName.replace("~","'");
									document.getElementById(partnerNameId).value=lastName;
									document.getElementById(paertnerCodeId).value=partnercode;
									document.getElementById(autocompleteDivId).style.display = "none";	
								} 
							
							 var http50 = getHTTPObject50();
								function getHTTPObject50() {
							    var xmlhttp;
							    if(window.XMLHttpRequest)  {
							        xmlhttp = new XMLHttpRequest();
							    }
							    else if (window.ActiveXObject)  {
							        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							        if (!xmlhttp)  {
							            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
							        }
							    }
							    return xmlhttp;
							}
							 
								function fillDiscriptionResponse(discription,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate){ 
									   if (http50.readyState == 4)
									       {
									           var result= http50.responseText
									           result = result.trim();   
									           result = result.replace('[','');
									           result=result.replace(']','');
									           var res = result.split("#"); 
									           var discriptionValue = document.forms['operationResourceForm'].elements[discription].value; 
									           if(result.indexOf('NoDiscription')<0) {

									           if(res[5]==''){   
									        	   document.forms['operationResourceForm'].elements[discription].value = res[0]; 
										           }else{
										        	   document.forms['operationResourceForm'].elements[discription].value = res[5]; 
										           }
									            
									           document.forms['operationResourceForm'].elements[recGl].value = res[1];     
									           document.forms['operationResourceForm'].elements[payGl].value = res[2]; 
									           document.forms['operationResourceForm'].elements[contractCurrency].value = res[3];        
									           }
									        }
									   <c:if test="${contractType}">
									   		var contCurrency = document.forms['operationResourceForm'].elements[contractCurrency].value
									    	var url="findExchangeRate.html?ajax=1&decorator=simple&popup=true&country="+encodeURI(contCurrency);
										    http77.open("GET", url, true);
										    http77.onreadystatechange = function(){handleHttpResponseContractRate(contractExchangeRate,contractValueDate);};
										    http77.send(null);
										</c:if>
									}

									function handleHttpResponseContractRate(contractExchangeRate,contractValueDate) { 
									    if (http77.readyState == 4) {
									       var results = http77.responseText
									       results = results.trim(); 
									       results = results.replace('[','');
									       results=results.replace(']',''); 
									       if(results.length>1) {
									         document.forms['operationResourceForm'].elements[contractExchangeRate].value=results 
									         } else {
									         document.forms['operationResourceForm'].elements[contractExchangeRate].value=1;
									         }
									         var mydate=new Date();
									         var daym;
									         var year=mydate.getFullYear()
									         var y=""+year;
									         if (year < 1000)
									         year+=1900
									         var day=mydate.getDay()
									         var month=mydate.getMonth()+1
									         if(month == 1)month="Jan";
									         if(month == 2)month="Feb";
									         if(month == 3)month="Mar";
											  if(month == 4)month="Apr";
											  if(month == 5)month="May";
											  if(month == 6)month="Jun";
											  if(month == 7)month="Jul";
											  if(month == 8)month="Aug";
											  if(month == 9)month="Sep";
											  if(month == 10)month="Oct";
											  if(month == 11)month="Nov";
											  if(month == 12)month="Dec";
											  var daym=mydate.getDate()
											  if (daym<10)
											  daym="0"+daym
											  var datam = daym+"-"+month+"-"+y.substring(2,4); 
											  document.forms['operationResourceForm'].elements[contractValueDate].value=datam;  
									 }   }
									
									function selectAllActive(str){ 
										 <c:set var="DMMContractTypeValue" value="false"/> 
											var billingDMMContractTypePage1 = document.forms['operationResourceForm'].elements['billingDMMContractTypePage'].value; 
											if(billingDMMContractTypePage1=='true'){
												<c:set var="DMMContractTypeValue" value="true"/> 
											}
											<c:choose> 
											 <c:when test="${((trackingStatus.accNetworkGroup) && (DMMContractTypeValue) && (trackingStatus.soNetworkGroup) )}"> 
											 alert("You cannot deactivate all accountlines because this is a DMM order. May be in UTSI instance, few accountlines have been invoiced.")
											 document.getElementById('selectallActive').checked=true;
											 </c:when>
										     <c:otherwise> 
											var id = idList.split(",");
											  for (i=0;i<id.length;i++){
												  if(str.checked == true){
													  try{
														  if(!document.getElementById('statusCheck'+id[i]).disabled){
															  document.getElementById('statusCheck'+id[i].trim()).checked = true;
															  if(document.getElementById('Inactivate')!=undefined){
															  document.getElementById('Inactivate').disabled = true; 
															  } 
														  }
													  }catch(e){}
													
												  }else{
													  try{
														  if(!document.getElementById('statusCheck'+id[i]).disabled){
															  document.getElementById('statusCheck'+id[i].trim()).checked = false;
															  if(document.getElementById('Inactivate')!=undefined){
															  document.getElementById('Inactivate').disabled = false; 
															  } 
														  }
													  }catch(e){}
													 
												  } }</c:otherwise></c:choose> }
									
									function inactiveStatusCheck(rowId,targetElement){
										<c:set var="DMMContractTypeValue" value="false"/> 
										var billingDMMContractTypePage1 = document.forms['operationResourceForm'].elements['billingDMMContractTypePage'].value; 
										if(billingDMMContractTypePage1=='true'){
											<c:set var="DMMContractTypeValue" value="true"/> 
										}
										<c:choose> 
										 <c:when test="${((trackingStatus.accNetworkGroup) && (DMMContractTypeValue) && (trackingStatus.soNetworkGroup) )}"> 
										 progressBarAutoSave('1');
									     var url="checkNetworkAgentInvoice.html?ajax=1&decorator=simple&popup=true&networkAgentId=" + encodeURI(rowId);
									     httpNetworkAgent.open("GET", url, true);
									     httpNetworkAgent.onreadystatechange = function(){ checkNetworkAgentInvoiceResponse(rowId,targetElement);}; 
									     httpNetworkAgent.send(null);
									     </c:when>
									     <c:otherwise>  
										   if(targetElement.checked==false){
										      var userCheckStatus = document.forms['operationResourceForm'].elements['accountIdCheck'].value;
										      if(userCheckStatus == ''){
											  	document.forms['operationResourceForm'].elements['accountIdCheck'].value = rowId;
										      }else{
										       var userCheckStatus=	document.forms['operationResourceForm'].elements['accountIdCheck'].value = userCheckStatus + ',' + rowId;
										      document.forms['operationResourceForm'].elements['accountIdCheck'].value = userCheckStatus.replace( ',,' , ',' );
										      }}
										   if(targetElement.checked){
										     var userCheckStatus = document.forms['operationResourceForm'].elements['accountIdCheck'].value;
										     var userCheckStatus=document.forms['operationResourceForm'].elements['accountIdCheck'].value = userCheckStatus.replace( rowId , '' );
										     userCheckStatus=userCheckStatus.replace( ',,' , ',' )
										     var len=userCheckStatus.length-1;
										     if(len==userCheckStatus.lastIndexOf(",")){
										    	 userCheckStatus=userCheckStatus.substring(0,len);
										         }if(userCheckStatus.indexOf(",")==0){
										    	 userCheckStatus=userCheckStatus.substring(1,userCheckStatus.length);}
										     document.forms['operationResourceForm'].elements['accountIdCheck'].value = userCheckStatus;} 
										     accountIdCheck(); </c:otherwise></c:choose>}
									
									function accountIdCheck(){
									    var accountIdCheck = document.forms['operationResourceForm'].elements['accountIdCheck'].value;
									    accountIdCheck=accountIdCheck.trim(); 
									    if(accountIdCheck==''){
									    	 if(document.getElementById('Inactivate')!=undefined){
									        document.forms['operationResourceForm'].elements['Inactivate'].disabled=true;
									    	 }
									        document.getElementById("selectallActive").checked=true;
									    }else if(accountIdCheck==','){
									    	 if(document.getElementById('Inactivate')!=undefined){
									    document.forms['operationResourceForm'].elements['Inactivate'].disabled=true;
									    	 }
									    document.getElementById("selectallActive").checked=true;
									    }else if(accountIdCheck!=''){
									    	 if(document.getElementById('Inactivate')!=undefined){
									      document.forms['operationResourceForm'].elements['Inactivate'].disabled=false;
									    	 }
									      document.getElementById("selectallActive").checked=false;
									    }} 
								
						 </script>
