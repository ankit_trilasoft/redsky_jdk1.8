/**
 * FormDesignManagementServicePortBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class FormDesignManagementServicePortBindingStub extends org.apache.axis.client.Stub implements com.keysurvey.api.v68.form.design.FormDesignManagementService {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[36];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
        _initOperationDesc4();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getQuestions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "withAnswer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestion"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSQuestion[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteQuestion");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getQuestion");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "withAnswer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestion"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSQuestion.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addQuestion");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "question"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestion"), com.keysurvey.api.v68.form.design.WSQuestion.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        oper.setReturnClass(long.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAnswers");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswer"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSAnswer[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAnswer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "answerId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswer"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSAnswer.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addAnswer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "answer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswer"), com.keysurvey.api.v68.form.design.WSAnswer.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        oper.setReturnClass(long.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getForm");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSForm"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSForm.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteAnswer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "answerId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFormTree");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormTree"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSFormTree.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createCompleteForm");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "form"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSForm"), com.keysurvey.api.v68.form.design.WSForm.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formDesign"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormDesign"), com.keysurvey.api.v68.form.design.WSFormDesign.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "submissionPolicy"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSSubmissionPolicy"), com.keysurvey.api.v68.form.design.WSSubmissionPolicy.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formNotes"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormNotes"), com.keysurvey.api.v68.form.design.WSFormNotes.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestion"), com.keysurvey.api.v68.form.design.WSQuestion[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        oper.setReturnClass(long.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createForm");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "form"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSForm"), com.keysurvey.api.v68.form.design.WSForm.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestion"), com.keysurvey.api.v68.form.design.WSQuestion[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        oper.setReturnClass(long.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateForm");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "form"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSForm"), com.keysurvey.api.v68.form.design.WSForm.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteForm");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createCompleteQuestion");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "question"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestion"), com.keysurvey.api.v68.form.design.WSQuestion.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestionSettings"), com.keysurvey.api.v68.form.design.WSQuestionSettings.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        oper.setReturnClass(long.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateQuestion");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "question"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestion"), com.keysurvey.api.v68.form.design.WSQuestion.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateAnswer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "answer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswer"), com.keysurvey.api.v68.form.design.WSAnswer.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getSubmissionPolicy");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSSubmissionPolicy"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSSubmissionPolicy.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("setSubmissionPolicy");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "submissionPolicy"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSSubmissionPolicy"), com.keysurvey.api.v68.form.design.WSSubmissionPolicy.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFormNotes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormNotes"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSFormNotes.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("setFormNotes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formNotes"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormNotes"), com.keysurvey.api.v68.form.design.WSFormNotes.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFormDesign");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormDesign"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSFormDesign.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("setFormDesign");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formDesign"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormDesign"), com.keysurvey.api.v68.form.design.WSFormDesign.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getQuestionSettings");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestionSettings"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSQuestionSettings.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("setQuestionSettings");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionSetting"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestionSettings"), com.keysurvey.api.v68.form.design.WSQuestionSettings.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("moveQuestion");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "offset"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getQuestionPosition");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        oper.setReturnClass(int.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("moveAnswer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "answerId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "offset"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAnswerPosition");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "answerId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        oper.setReturnClass(int.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAnswerByPosition");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "position"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswer"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSAnswer.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[29] = oper;

    }

    private static void _initOperationDesc4(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getQuestionByPosition");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "position"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestion"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSQuestion.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[30] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("setFormLogic");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formLogic"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormLogic"), com.keysurvey.api.v68.form.design.WSFormLogic.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[31] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFormLogic");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "formId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormLogic"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSFormLogic.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[32] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getQuestionLogic");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestionLogic"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSQuestionLogic.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[33] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("setQuestionLogic");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "questionLogic"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestionLogic"), com.keysurvey.api.v68.form.design.WSQuestionLogic.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[34] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getForms");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "accountId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), long.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSForm"));
        oper.setReturnClass(com.keysurvey.api.v68.form.design.WSForm[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSSecurityException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSInternalServerException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSIllegalArgumentException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObjectNotFoundException"),
                      "com.keysurvey.api.v68.WSFault",
                      new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault"), 
                      true
                     ));
        _operations[35] = oper;

    }

    public FormDesignManagementServicePortBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public FormDesignManagementServicePortBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public FormDesignManagementServicePortBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAdvancedLogic");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSAdvancedLogic.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswer");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSAnswer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswerRestriction");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSAnswerRestriction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswerRestrictionType");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSAnswerRestrictionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswerSortType");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSAnswerSortType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSBaseQuestion");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSBaseQuestion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSCheckAllThatApplyQuestion");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSCheckAllThatApplyQuestion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSDropdownQuestion");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSDropdownQuestion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSForm");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSForm.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormAlign");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSFormAlign.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormatAnswer");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSFormatAnswer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormDesign");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSFormDesign.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormLogic");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSFormLogic.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormNotes");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSFormNotes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormTree");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSFormTree.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFuzzyJumpLogic");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSFuzzyJumpLogic.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSGoToLogic");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSGoToLogic.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSListBoxQuestion");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSListBoxQuestion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSLogic");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSLogic.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSMultiLineQuestion");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSMultiLineQuestion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSNumericAllocationQuestion");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSNumericAllocationQuestion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSPickOneOrOtherQuestion");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSPickOneOrOtherQuestion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSPickOneWithCommentQuestion");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSPickOneWithCommentQuestion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestion");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSQuestion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestionLogic");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSQuestionLogic.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestionSettings");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSQuestionSettings.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSRankGridQuestion");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSRankGridQuestion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSSectionHeaderQuestion");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSSectionHeaderQuestion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSSingleLineQuestion");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSSingleLineQuestion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSSkipLogic");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSSkipLogic.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSSubmissionPolicy");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.form.design.WSSubmissionPolicy.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSFault");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.WSFault.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://v68.api.keysurvey.com", "WSObject");
            cachedSerQNames.add(qName);
            cls = com.keysurvey.api.v68.WSObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.keysurvey.api.v68.form.design.WSQuestion[] getQuestions(long formId, boolean withAnswer) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getQuestions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(formId), new java.lang.Boolean(withAnswer)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSQuestion[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSQuestion[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSQuestion[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void deleteQuestion(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "deleteQuestion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(questionId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSQuestion getQuestion(long questionId, boolean withAnswer) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getQuestion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(questionId), new java.lang.Boolean(withAnswer)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSQuestion) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSQuestion) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSQuestion.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public long addQuestion(com.keysurvey.api.v68.form.design.WSQuestion question) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "addQuestion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {question});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Long) _resp).longValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Long) org.apache.axis.utils.JavaUtils.convert(_resp, long.class)).longValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSAnswer[] getAnswers(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getAnswers"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(questionId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSAnswer[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSAnswer[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSAnswer[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSAnswer getAnswer(long answerId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getAnswer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(answerId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSAnswer) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSAnswer) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSAnswer.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public long addAnswer(com.keysurvey.api.v68.form.design.WSAnswer answer) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "addAnswer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {answer});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Long) _resp).longValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Long) org.apache.axis.utils.JavaUtils.convert(_resp, long.class)).longValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSForm getForm(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getForm"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(formId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSForm) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSForm) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSForm.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void deleteAnswer(long answerId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "deleteAnswer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(answerId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSFormTree getFormTree(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getFormTree"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(formId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSFormTree) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSFormTree) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSFormTree.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public long createCompleteForm(com.keysurvey.api.v68.form.design.WSForm form, com.keysurvey.api.v68.form.design.WSFormDesign formDesign, com.keysurvey.api.v68.form.design.WSSubmissionPolicy submissionPolicy, com.keysurvey.api.v68.form.design.WSFormNotes formNotes, com.keysurvey.api.v68.form.design.WSQuestion[] questionList) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "createCompleteForm"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {form, formDesign, submissionPolicy, formNotes, questionList});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Long) _resp).longValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Long) org.apache.axis.utils.JavaUtils.convert(_resp, long.class)).longValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public long createForm(com.keysurvey.api.v68.form.design.WSForm form, com.keysurvey.api.v68.form.design.WSQuestion[] questionList) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "createForm"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {form, questionList});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Long) _resp).longValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Long) org.apache.axis.utils.JavaUtils.convert(_resp, long.class)).longValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void updateForm(com.keysurvey.api.v68.form.design.WSForm form) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "updateForm"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {form});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void deleteForm(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "deleteForm"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(formId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public long createCompleteQuestion(com.keysurvey.api.v68.form.design.WSQuestion question, com.keysurvey.api.v68.form.design.WSQuestionSettings questionSettings) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "createCompleteQuestion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {question, questionSettings});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Long) _resp).longValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Long) org.apache.axis.utils.JavaUtils.convert(_resp, long.class)).longValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void updateQuestion(com.keysurvey.api.v68.form.design.WSQuestion question) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "updateQuestion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {question});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void updateAnswer(com.keysurvey.api.v68.form.design.WSAnswer answer) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "updateAnswer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {answer});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSSubmissionPolicy getSubmissionPolicy(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getSubmissionPolicy"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(formId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSSubmissionPolicy) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSSubmissionPolicy) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSSubmissionPolicy.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void setSubmissionPolicy(com.keysurvey.api.v68.form.design.WSSubmissionPolicy submissionPolicy) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "setSubmissionPolicy"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {submissionPolicy});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSFormNotes getFormNotes(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getFormNotes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(formId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSFormNotes) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSFormNotes) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSFormNotes.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void setFormNotes(com.keysurvey.api.v68.form.design.WSFormNotes formNotes) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "setFormNotes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {formNotes});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSFormDesign getFormDesign(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getFormDesign"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(formId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSFormDesign) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSFormDesign) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSFormDesign.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void setFormDesign(com.keysurvey.api.v68.form.design.WSFormDesign formDesign) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "setFormDesign"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {formDesign});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSQuestionSettings getQuestionSettings(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getQuestionSettings"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(questionId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSQuestionSettings) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSQuestionSettings) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSQuestionSettings.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void setQuestionSettings(com.keysurvey.api.v68.form.design.WSQuestionSettings questionSetting) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "setQuestionSettings"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {questionSetting});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void moveQuestion(long questionId, int offset) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "moveQuestion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(questionId), new java.lang.Integer(offset)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public int getQuestionPosition(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getQuestionPosition"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(questionId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Integer) _resp).intValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Integer) org.apache.axis.utils.JavaUtils.convert(_resp, int.class)).intValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void moveAnswer(long answerId, int offset) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "moveAnswer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(answerId), new java.lang.Integer(offset)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public int getAnswerPosition(long answerId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getAnswerPosition"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(answerId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Integer) _resp).intValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Integer) org.apache.axis.utils.JavaUtils.convert(_resp, int.class)).intValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSAnswer getAnswerByPosition(long questionId, int position) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getAnswerByPosition"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(questionId), new java.lang.Integer(position)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSAnswer) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSAnswer) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSAnswer.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSQuestion getQuestionByPosition(long formId, int position) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[30]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getQuestionByPosition"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(formId), new java.lang.Integer(position)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSQuestion) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSQuestion) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSQuestion.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void setFormLogic(com.keysurvey.api.v68.form.design.WSFormLogic formLogic) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[31]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "setFormLogic"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {formLogic});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSFormLogic getFormLogic(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[32]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getFormLogic"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(formId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSFormLogic) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSFormLogic) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSFormLogic.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSQuestionLogic getQuestionLogic(long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[33]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getQuestionLogic"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(questionId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSQuestionLogic) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSQuestionLogic) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSQuestionLogic.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void setQuestionLogic(long questionId, com.keysurvey.api.v68.form.design.WSQuestionLogic questionLogic) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[34]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "setQuestionLogic"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(questionId), questionLogic});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.keysurvey.api.v68.form.design.WSForm[] getForms(long accountId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[35]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "getForms"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Long(accountId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.keysurvey.api.v68.form.design.WSForm[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.keysurvey.api.v68.form.design.WSForm[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.keysurvey.api.v68.form.design.WSForm[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.keysurvey.api.v68.WSFault) {
              throw (com.keysurvey.api.v68.WSFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
