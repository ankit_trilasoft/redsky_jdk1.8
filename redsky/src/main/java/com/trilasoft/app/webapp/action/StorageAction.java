package com.trilasoft.app.webapp.action;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.BookStorage;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Location;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.StandardDeductions;
import com.trilasoft.app.model.Storage;
import com.trilasoft.app.model.StorageLibrary;
import com.trilasoft.app.model.SubcontractorCharges;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.BookStorageManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.LocationManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.StorageLibraryManager;
import com.trilasoft.app.service.StorageManager;
import com.trilasoft.app.service.WorkTicketManager;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

public class StorageAction extends BaseAction implements Preparable {

	private StorageManager storageManager;

	private LocationManager locationManager;

	private List serviceOrders;

	private List storages;
	private List stoLoRearranges;
	private List booksStorages;

	private Storage storage;
	
	private Storage oldStorage;

	private Location location;

	private Location locationOld;

	private Long id;

	private Long id1;
	
	private Long id2;

	private String locationId;

	private ServiceOrder serviceOrder;

	private WorkTicket workTicket;

	private BookStorage bookStorage;

	private BookStorageManager bookStorageManager;
	private StorageLibraryManager storageLibraryManager;
	private StorageLibrary storageLibrary;
	private WorkTicketManager workTicketManager;

	private String sessionCorpID;

	private Long autoIdNumber;

	private String idCheck;

	private String firstname;
	private String userCheck;
	private String lastName;
    private List storageLibraryVolumeList;
    private String storageId;
    private String ticket;
    private Date workTicketrelease;
    Date currentdate = new Date();
    private String voxmeIntergartionFlag;
    private Company company;
	private CompanyManager companyManager;
	private  Map<String, String> state;
	private RefMasterManager refMasterManager;
    static final Logger logger = Logger.getLogger(StorageAction.class);
    private Map<String, String> releaseTypeRadio;
    
	public StorageAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		if(company!=null){
		if(company.getVoxmeIntegration()!=null){
			voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
			}
		}
		releaseTypeRadio = new HashMap();
		releaseTypeRadio.put("C","Complete");
		releaseTypeRadio.put("P","Partial");
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

	}

	public void setStorageManager(StorageManager storageManager) {
		this.storageManager = storageManager;
	}

	public void setBookStorageManager(BookStorageManager bookStorageManager) {
		this.bookStorageManager = bookStorageManager;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public List getServiceOrders() {
		return serviceOrders;
	}

	public List getStorages() {
		return storages;
	}

	public List getBookStorages() {
		return booksStorages;
	}

	public String list() {
		storages = storageManager.getAll();
		return SUCCESS;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public BookStorage getBookStorage() {
		return bookStorage;
	}

	public void setBookStorage(BookStorage bookStorage) {
		this.bookStorage = bookStorage;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public WorkTicket getWorkTicket() {
		return workTicket;
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}

	public String delete() {
		storageManager.remove(storage.getId());
		saveMessage(getText("storage.deleted"));

		return SUCCESS;
	}

	private String gotoPageString;

	private String validateFormNav;

	public String getValidateFormNav() {

		return validateFormNav;

	}

	public void setValidateFormNav(String validateFormNav) {

		this.validateFormNav = validateFormNav;

	}

	public String getGotoPageString() {

		return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

		this.gotoPageString = gotoPageString;

	}

	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//if (option enabled for this company in the system parameters table then call save) {
		validateFormNav = "OK";
		String s = saveTraceStorage(); // else simply navigate to the requested page)
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return gotoPageString;
	}
	
	@SkipValidation
	  public String storageExtractFile(){   
	     return SUCCESS;     
	}
	public String jobSTLFlag="";
	public String salesPortalFlag="";
	String stateDescription="";
	String MailAddress="";
	String MailCity="";
	String MailState="";
	String MailCountry="";
	String MailZip="";
	public void storageReport() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		StringBuffer query = new StringBuffer();
		StringBuffer fileName = new StringBuffer();
		String salesPortalStorageReport="";
		String header = new String();
		if(salesPortalFlag!=null && !salesPortalFlag.equals("") &&  salesPortalFlag.equalsIgnoreCase("Yes")){
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal();
			if(user.getSalesPortalAccess()){
				salesPortalStorageReport=" s.bookingAgentCode='"+user.getParentAgent()+"' and s.job='STO' ";
			}else{
				salesPortalStorageReport=" s.salesMan='"+user.getUsername()+"' and s.job='STO' ";
			}
		}
		List storageExtractList = storageManager.storageExtract(sessionCorpID,jobSTLFlag,salesPortalStorageReport);
		//System.out.println("\n\nSize::" + storageExtractList.size());

		try {

			if (!storageExtractList.isEmpty()) {
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1 = new File("StorageExtract");
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls");
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				//            1                      2                    3                  4                  5                      6                 7                        8                      
				header = "ShipNumber" + "\t" + "First Name"+ "\t" +"Last Name" + "\t" + "Bill To" + "\t" + " Bill To Name" + "\t" + "Location Agent Code" + "\t" + " Location Agent Name" + "\t" + 	" Po # " + "\t" + " Ref # " + "\t" + " PO Auth Expires" + "\t";
				//                   9
				header=header+ " RegNumber" + "\t"; 
				//              	10
				header=header+ " Commodity" + "\t"; 
				//                   11
				header=header+ " ActualWeight " + "\t"; 
				//                 12
				header=header+ " Load (T) " + "\t" ;
				//                  13
				header=header+ " Load (A) " + "\t" ;
				//                  14
				header=header+ " DeliveryA " + "\t";
				//                  15
				header=header+ " Storage Out Date " + "\t" ;
				//                  16
				header=header+ " Insurance Entitlement " + "\t" ;
				//                   17
				header=header+ " Billed Through " + "\t" ;
				//                   18
				header=header+ " Currency " + "\t" ;
				
				header=header+ " Insval_Ac " + "\t" ;
				//                   19
				header=header+ " OnHand " + "\t";
				//                   20
				header=header+ " StoPerMonth " + "\t" ;
				//                    21
				header=header+ " InsPerMonth "+ "\t" ;
				//                     22
				header=header+ " OA Name " + "\t" ;
				//                  23
				header=header+ " BA Name " + "\t" ;
				//                 24
				header=header+ " OA Address1 \t" ;
				//                 25
				header=header+ " OA City " + "\t" ;
				//                26
				header=header+ " OA State " + "\t" ;
				//                 27
				header=header+ " OA Zip " + "\t";
				//                 28
				header=header+ " OA Country " + "\t" ;
				//                 29
				header=header+ " OA Phone " + "\t";
				if(sessionCorpID.equals("CWMS")){
					//            30
				header=header+ " DA Name" + "\t";
				//                 31
				header=header+ " DA Address1" + "\t";
				//                 32
				header=header+ " DA City" + "\t";
				//                  33
				header=header+ " DA State" + "\t";
				//                  34
				header=header+ " DA Zip" + "\t";
				//                  35
				header=header+ " DA Country" + "\t";
				//                  36
				header=header+ " DA Phone" + "\t";
				}
				//                37
				header=header+ " Job" + "\t" ;
				//                  38
				header=header+ " CompanyDivision " + "\t" ;
				//                  39
				header=header+ " Warehouse" +"\t" ;
				//                  40
				header=header+ " Person Billing " + "\t" ;
				//                  41
				header=header+ " Coordinator "+"\t" ;
				//                  42
				header=header+ " SubOA Code " +"\t" ;
				//                  43
				header=header+ " SubOA Name "+ "\t";
				if(sessionCorpID.equals("CWMS")){
				//                 44
				header=header+ " Sub DA Code " +"\t" ;
				//                 45
				header=header+ " Sub DA Name " +"\t" ;
				}
				//                 46
				header=header+ " Location "+ "\t" ;
				//                 47
				header=header+ " No Charge "+ "\t" ;
				//                 48
				header=header+ " Charge Code " + "\t" ;
				//                 49
				header=header+ " Charge Description " + "\t" ;
				//                 50
				header=header+ " Vendor Name " + "\t" ;
				//                52
				header=header+ " Rate " + "\t" ;
				//                 53
				header=header+ " Account Name " + "\t" ;
				//                  54
				header=header+ " Storage Email Address "+ "\t" ;
				if(sessionCorpID.equals("SSCW")){
					//              55
				header=header+ " Mailing  Address1 "+ "\t";
				//                 56
				header=header+ " Mailing Address2 "+ "\t";
				//					57
				header=header+ " Mailing Address3 "+ "\t";
				//                  58
				header=header+ " MailingCity "+ "\t" ;
				//                  59
				header=header+ " MailingState "+ "\t" ;
				//                  60
				header=header+ " MailingCountry "+ "\t" ;
				//                  61
				header=header+ " MailingZip "+ "\t" ;
				//                  62
				header=header+ " Mailing Email address "+ "\t" ;
				//                  63
				header=header+ " Mailing Phone numbers "+ "\t" ;
				//                 64
				header=header+ " Billing address1 "+ "\t" ;
				//                 65
				header=header+ " Billing address2 "+ "\t" ;
				//                 66
				header=header+ " Billing address3 "+ "\t" ;
				//                  67
				header=header+ " Billing city "+ "\t" ;
				//                  68
				header=header+ " Billing state "+ "\t" ;
				//                  69
				header=header+ " Billing country "+ "\t" ;
				//                   70
				header=header+ " Billing zip "+ "\t" ;
				//                  71
				header=header+ " Billing Email Address "+ "\t" ;
				//                  72
				header=header+ " Billing Phone Numbers " ;
				}
				header=header+ "\n";
				outputStream.write(header.getBytes());

				Iterator it = storageExtractList.iterator();  
				//state = refMasterManager.findStateCodeDescription(sessionCorpID, "STATE");
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();
					//String desc=state.get(row[47].toString()+"~"+row[52].toString());
					if (row[0] != null) {
						String shipNo = row[0].toString();
						outputStream.write(("`" + shipNo + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[1] != null) {
						if (row[1].toString().trim().equals("")) {
							outputStream.write(("" + "\t").getBytes());
						} else {
							outputStream.write((row[1].toString() + "\t").getBytes());
						}

					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[2] != null) {
						
						outputStream.write((row[2].toString() + "\t").getBytes());
					
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[3] != null) {
						String pONo = row[3].toString();
						outputStream.write((pONo + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[4] != null) {
						outputStream.write((row[4].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[5] != null) {
						outputStream.write((row[5].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[6] != null) {
						outputStream.write((row[6].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[7] != null) {
						String dAOMNINo = row[7].toString();
						outputStream.write((dAOMNINo + "\t").getBytes());

					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[8] != null) {
						String netWtLbs = row[8].toString();
						outputStream.write((netWtLbs + "\t").getBytes());

					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[9] != null) {
						String shipmnetNo = row[9].toString();
						outputStream.write((shipmnetNo + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[10] != null) {
						if (row[10].toString().equals("0.00") || row[10].toString().equals("0")) {
							outputStream.write(("" + "\t").getBytes());
						} else {
							outputStream.write((row[10].toString() + "\t").getBytes());
						}

					} else {
						outputStream.write("\t".getBytes());

					}
					if (row[11] != null) {
						outputStream.write((row[11].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());

					}
					if (row[12] != null) {
						outputStream.write((row[12].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());

					}
					if (row[13] != null) {
						outputStream.write((row[13].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());

					}
					if (row[14] != null) {
						outputStream.write((row[14].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());

					}

					if (row[15] != null) {
						if (row[15].toString().equals("0.00") || row[15].toString().equals("0") || row[15].toString().equals("0.000")) {
							outputStream.write(("" + "\t").getBytes());
						} else {
							outputStream.write((row[15].toString() + "\t").getBytes());
						}

					} else {
						outputStream.write("\t".getBytes());

					}

					if (row[16] != null) {
						outputStream.write((row[16].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());

					}
					if (row[17] != null) {
						if (row[17].toString().equals("0.00") || row[17].toString().equals("0") || row[17].toString().equals("0.000")) {
							outputStream.write(("" + "\t").getBytes());
						} else {
							outputStream.write((row[17].toString() + "\t").getBytes());
						}
					} else {
						outputStream.write("\t".getBytes());

					}
					if (row[18] != null) {
						if (row[18].toString().equals("0.00") || row[18].toString().equals("0") || row[18].toString().equals("0.000")) {
							outputStream.write(("" + "\t").getBytes());
						} else {
							outputStream.write((row[18].toString() + "\t").getBytes());
						}
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[19] != null) {
						outputStream.write((row[19].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[20] != null) {
						if (row[20].toString().equals("0.00") || row[20].toString().equals("0") || row[20].toString().equals("0.000")) {
							outputStream.write(("" + "\t").getBytes());
						} else {
							outputStream.write((row[20].toString() + "\t").getBytes());
						}
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[21] != null) {
						outputStream.write((row[21].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[22] != null) {
						outputStream.write((row[22].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[23] != null) {
						outputStream.write((row[23].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[24] != null) {
						outputStream.write((row[24].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[25] != null) {
						outputStream.write((row[25].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[26] != null) {
						outputStream.write((row[26].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[27] != null) {
						outputStream.write((row[27].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[28] != null) {
						outputStream.write((row[28].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[29] != null) {
						outputStream.write((row[29].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[30] != null) {
						outputStream.write((row[30].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[31] != null) {
						outputStream.write((row[31].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if(sessionCorpID.equals("CWMS")){
					if (row[32] != null) {
						outputStream.write((row[32].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[33] != null) {
						outputStream.write((row[33].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[34] != null) {
						outputStream.write((row[34].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[35] != null) {
						outputStream.write((row[35].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[36] != null) {
						outputStream.write((row[36].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[37] != null) {
						outputStream.write((row[37].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[38] != null) {
						outputStream.write((row[38].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					}
					if (row[39] != null) {
						outputStream.write((row[39].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[40] != null) {
						outputStream.write((row[40].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[41] != null) {
						outputStream.write((row[41].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[42] != null) {
						outputStream.write((row[42].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[43] != null) {
						outputStream.write((row[43].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[44] != null && !(row[44].equals(""))) {
						outputStream.write(("`"+row[44].toString()+ "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[45] != null) {
						outputStream.write((row[45].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if(sessionCorpID.equals("CWMS")){
					if (row[46] != null && !(row[46].equals(""))) {
						outputStream.write(("`"+row[46].toString()+ "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[47] != null) {
						outputStream.write((row[47].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					}
					if (row[48] != null) {
						outputStream.write((row[48].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[49] != null) {
						outputStream.write((row[49].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[50] != null) {
						outputStream.write((row[50].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[51] != null) {
						outputStream.write((row[51].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[52] != null) {
						outputStream.write((row[52].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[53] != null) {
						if (row[53].toString().equals("0.00") || row[53].toString().equals("0") || row[53].toString().equals("0.000")) {
							outputStream.write(("" + "\t").getBytes());
						} else {
							outputStream.write((row[53].toString() + "\t").getBytes());
						}
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[54] != null) {
						outputStream.write((row[54].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[55] != null) {
						outputStream.write((row[55].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if(sessionCorpID.equals("SSCW")){
					if (row[56] != null) {
						//MailAddress=row[54].toString();
						outputStream.write((row[56].toString() + "\t").getBytes());
					}else {
						outputStream.write("\t".getBytes());
					}
					if (row[57] != null) {
						//MailAddress=row[54].toString();
						outputStream.write((row[57].toString() + "\t").getBytes());
					}else {
						outputStream.write("\t".getBytes());
					}
					if (row[58] != null) {
						//MailAddress=row[54].toString();
						outputStream.write((row[58].toString() + "\t").getBytes());
					}else {
						outputStream.write("\t".getBytes());
					}
					if (row[59] != null) {
						//MailCity=row[55].toString();
						outputStream.write((row[59].toString() + "\t").getBytes());
					}else {
						outputStream.write("\t".getBytes());
					}
					if (row[60] != null) {
						
						/*if(row[47]!=null && !row[47].toString().equalsIgnoreCase("") && row[52]!=null && !row[52].toString().equalsIgnoreCase("")){
							stateDescription=state.get(row[47].toString().trim()+"~"+row[52].toString().trim());
						}
						*/
						/*stateDescription=row[56].toString();
						if(stateDescription!=null && !stateDescription.equals("") ){
							stateDescription=stateDescription+", ";
						}
						if(stateDescription==null){
							stateDescription="";
						}*/
						//stateDescription=row[56].toString();
						outputStream.write((row[60].toString() + "\t").getBytes());
						 
					}else {
						outputStream.write("\t".getBytes());
					}
					if (row[61] != null) {
						outputStream.write((row[61].toString() + "\t").getBytes());
					}else {
						outputStream.write("\t".getBytes());
					}
					if (row[62] != null) {
						outputStream.write((row[62].toString() + "\t").getBytes());
					}else {
						outputStream.write("\t".getBytes());
					}
					if (row[63] != null) {
						outputStream.write((row[63].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[64] != null) {
						outputStream.write((row[64].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[65] != null) {
						outputStream.write((row[65].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[66] != null) {
						outputStream.write((row[66].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[67] != null) {
						outputStream.write((row[67].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[68] != null) {
						outputStream.write((row[68].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[69] != null) {
						outputStream.write((row[69].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[70] != null) {
						outputStream.write((row[70].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[71] != null) {
						outputStream.write((row[71].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[72] != null) {
						outputStream.write((row[72].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[73] != null) {
						outputStream.write((row[73].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					}
					outputStream.write("\n".getBytes());

				}

			}

			else {
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1 = new File("StorageExtract");
				response.setContentType("application/vnd.ms-excel");
				//response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls");
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				header = "NO Record Found , thanks";
				outputStream.write(header.getBytes());
			}
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
		}
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}

	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (id != null) {
			storage = storageManager.get(id);
			List workTicketList = workTicketManager.getWorkTicketByTicekt(storage.getTicket());
			workTicket = (WorkTicket)workTicketList.get(0);
			serviceOrder = workTicket.getServiceOrder();
		} else {
			storage = new Storage();
			storage.setCreatedOn(new Date());
			storage.setUpdatedOn(new Date());
		}
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String saveTraceStorage() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (storage.getId() == null);
		storage.setCorpID(sessionCorpID);
		storage.setUpdatedOn(new Date());
		storage.setUpdatedBy(getRequest().getRemoteUser());
		if (isNew) {
			storage.setCreatedOn(new Date());
		}
		if (storage.getPieces() == null) {
			storage.setPieces(0);
		}
		storageManager.save(storage);
		bookStorage = (BookStorage) storageManager.getByIdNum(storage.getIdNum()).get(0);
		bookStorage.setDescription(storage.getDescription());
		bookStorage.setPieces(storage.getPieces());
		bookStorage.setPrice(storage.getPrice());
		bookStorage.setContainerId(storage.getContainerId());
		bookStorage.setItemNumber(storage.getItemNumber());
		bookStorage.setItemTag(storage.getItemTag());
		bookStorage.setLocationId(storage.getLocationId());
		bookStorage.setCreatedBy(getRequest().getRemoteUser());
		bookStorage.setCreatedOn(new Date());
		bookStorage.setUpdatedBy(getRequest().getRemoteUser());
		bookStorage.setUpdatedOn(new Date());

		bookStorageManager.save(bookStorage);
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (cancel != null) {
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return "cancel";
		}

		if (delete != null) {
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return delete();
		}

		//storage.setShipmentNumbers(shipmentNumbers);

		boolean isNew = (storage.getId() == null);
		// storage.setWorkTicket(workTicket);
		// storage.setServiceOrder(serviceOrder);
		storage.setCorpID(sessionCorpID);
		if (isNew) {
			storage.setCreatedOn(new Date());
		}
		storage.setUpdatedOn(new Date());

		storageManager.save(storage);

		String key = (isNew) ? "storage.added" : "storage.updated";
		saveMessage(getText(key));

		if (!isNew) {
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return INPUT;
		} else {
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	}

	public String access() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		idCheck = idCheck.trim();
		if (idCheck.indexOf(",") == 0) {
			idCheck = idCheck.substring(1);
		}
		if (idCheck.lastIndexOf(",") == idCheck.length() - 1) {
			idCheck = idCheck.substring(0, idCheck.length() - 1);
		}
		String[] arrayid = idCheck.split(",");
		int arrayLength = arrayid.length;
		for (int i = 0; i < arrayLength; i++) {
			id = Long.parseLong(arrayid[i]);
			if (id != null) {
				workTicket = workTicketManager.get(id1);
				serviceOrder = workTicket.getServiceOrder();
				storage = storageManager.get(id);
				location = locationManager.getByLocation(storage.getLocationId());
				if(location==null){
					String message="Location ID missing so Access not possible!";
					errorMessage(getText(message));
					hitFlag="0";
					return ERROR;
				}
				BookStorage bookStorageNew = new BookStorage();
				bookStorageNew.setDescription(storage.getDescription());
				bookStorageNew.setCorpID(sessionCorpID);
				bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
				bookStorageNew.setCreatedOn(new Date());
				bookStorageNew.setUpdatedOn(new Date());
				bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
				bookStorageNew.setWhat("A");
				bookStorageNew.setStorageId(storage.getStorageId());
				bookStorageNew.setShipNumber(storage.getShipNumber());
				bookStorageNew.setIdNum(storage.getIdNum());
				bookStorageNew.setDated(new Date());
				bookStorageNew.setServiceOrderId(workTicket.getServiceOrderId());

				if (storage.getPieces() == null) {
					bookStorageNew.setPieces(0);
				} else {
					bookStorageNew.setPieces(storage.getPieces());
				}
				bookStorageNew.setPrice(storage.getPrice());
				bookStorageNew.setContainerId(storage.getContainerId());
				bookStorageNew.setItemTag(storage.getItemTag());
				bookStorageNew.setOldLocation("");
				bookStorageNew.setLocationId(storage.getLocationId());
				bookStorageNew.setTicket(storage.getTicket());
				bookStorageNew.setStorage(storage);
				bookStorageManager.save(bookStorageNew);

				storage.setUpdatedOn(new Date());
				storage.setUpdatedBy(storage.getUpdatedBy());

				storageManager.save(storage);
				//storages = new ArrayList( workTicket.getStorages());
				storages = workTicketManager.getStorageList(workTicket.getShipNumber());

			} 
		}
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
    public String storagesRelease(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	workTicket = workTicketManager.get(id);
    	idCheck = idCheck.trim();
		if (idCheck.indexOf(",") == 0) {
			idCheck = idCheck.substring(1);
		}
		if (idCheck.lastIndexOf(",") == idCheck.length() - 1) {
			idCheck = idCheck.substring(0, idCheck.length() - 1);
		}
		String[] arrayid = idCheck.split(",");
		for (int i = 0; i < arrayid.length; i++) {
			Long stid = Long.parseLong(arrayid[i]);
			if (stid != null) {
				storage = storageManager.get(stid);
			}
		}
    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
	private Long autoSequenceNumber;
	private BigDecimal balanceWeight;
	private BigDecimal balanceVolume;
	private Integer balancePieces;
	private String releaseType;
	public String release() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		idCheck = idCheck.trim();
		if (idCheck.indexOf(",") == 0) {
			idCheck = idCheck.substring(1);
		}
		if (idCheck.lastIndexOf(",") == idCheck.length() - 1) {
			idCheck = idCheck.substring(0, idCheck.length() - 1);
		}
		String[] arrayid = idCheck.split(",");
		int arrayLength = arrayid.length;
		for (int i = 0; i < arrayLength; i++) {
			id = Long.parseLong(arrayid[i]);
			if (id != null) {
				workTicket = workTicketManager.get(id1);
				serviceOrder = workTicket.getServiceOrder();
				storage = storageManager.get(id);
				location = locationManager.getByLocation(storage.getLocationId());
				if(location==null){
					String message="Location ID missing so Release not possible!";
					errorMessage(getText(message));
					hitFlag="0";
					return ERROR;
				}
				BookStorage bookStorageNew = new BookStorage();	
				bookStorageNew.setDescription(storage.getDescription());
				bookStorageNew.setCorpID(sessionCorpID);
				bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
				bookStorageNew.setCreatedOn(new Date());
				bookStorageNew.setUpdatedOn(new Date());
				bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
				bookStorageNew.setWhat("R");
				bookStorageNew.setShipNumber(storage.getShipNumber());
				bookStorageNew.setIdNum(storage.getIdNum());
				bookStorageNew.setDated(new Date());
				bookStorageNew.setServiceOrderId(workTicket.getServiceOrderId());
				if((releaseType!=null && !releaseType.equalsIgnoreCase("")) && releaseType.equalsIgnoreCase("P")){
					if(balancePieces==null || balancePieces.equals("")){
						if(storage.getPieces() == null){
							bookStorageNew.setPieces(0);
						}else{
							bookStorageNew.setPieces(storage.getPieces());
						}
					}else{
						bookStorageNew.setPieces(balancePieces);
					}
				}else{
					if (storage.getPieces() == null) {
						bookStorageNew.setPieces(0);
					} else {
						bookStorageNew.setPieces(storage.getPieces());
					}
				}
				bookStorageNew.setPrice(storage.getPrice());
				bookStorageNew.setContainerId(storage.getContainerId());
				bookStorageNew.setItemTag(storage.getItemTag());
				bookStorageNew.setOldLocation("");
				bookStorageNew.setWarehouse(storage.getWarehouse());
				bookStorageNew.setLocationId(storage.getLocationId());
				bookStorageNew.setTicket(workTicket.getTicket());
				bookStorageNew.setStorage(storage);
				bookStorageManager.save(bookStorageNew);
	
				if((releaseType!=null && !releaseType.equalsIgnoreCase("")) && releaseType.equalsIgnoreCase("P")){
					if(balanceWeight!=null && !balanceWeight.equals("")){
						storage.setOriginalMeasQuantity(storage.getMeasQuantity());
						storage.setMeasQuantity(storage.getMeasQuantity().subtract(balanceWeight));
					}else{
						storage.setMeasQuantity(storage.getMeasQuantity());
					}
					if(balanceVolume!=null && !balanceVolume.equals("")){						
						storage.setOriginalVolume(storage.getVolume());
						storage.setVolume(storage.getVolume().subtract(balanceVolume));
					}else{
						storage.setVolume(storage.getVolume());
					}
					if(balancePieces!=null && !balancePieces.equals("")){
						storage.setOriginalPieces(storage.getPieces());
						storage.setPieces(storage.getPieces()-balancePieces);
					}else{
						if(storage.getPieces() == null){
							storage.setPieces(0);
						}else{
							storage.setPieces(storage.getPieces());
						}
					}
				}
				if(location.getCapacity().equalsIgnoreCase("1")){
					location.setOccupied("");
					location.setUpdatedOn(new Date());
					location.setUpdatedBy(getRequest().getRemoteUser());
					locationManager.save(location);
					}
			    if(workTicketrelease!=null){
                    storage.setReleaseDate(workTicketrelease);	
				}else{
					storage.setReleaseDate(workTicket.getDate2());
				}
				storage.setUpdatedOn(new Date());
				storage.setUpdatedBy(storage.getUpdatedBy());
				storage.setToRelease(workTicket.getTicket());
	
				storageManager.save(storage);
				if((releaseType!=null && !releaseType.equalsIgnoreCase("")) && releaseType.equalsIgnoreCase("P")){
					if((balanceVolume!=null && Double.parseDouble(balanceVolume.toString())>0) || ((balanceWeight!=null && Double.parseDouble(balanceWeight.toString())>0))){
					Storage storageNew = new Storage();
					
					storageNew.setCorpID(sessionCorpID);
					storageNew.setCreatedOn(new Date());
					storageNew.setUpdatedOn(new Date());
					storageNew.setUpdatedBy(getRequest().getRemoteUser());
					storageNew.setCreatedBy(getRequest().getRemoteUser());
					storageNew.setToRelease(workTicket.getTicket());
					storageNew.setDescription(storage.getDescription());
					storageNew.setReleaseDate(null);
					storageNew.setShipNumber(storage.getShipNumber());
					storageNew.setContainerId(storage.getContainerId());
					storageNew.setHandoutStatus(storage.getHandoutStatus());
					storageNew.setHoTicket(storage.getHoTicket());
					List autoNumList = storageManager.findMaximum();
					if(autoNumList.get(0) != null){
						autoSequenceNumber = Long.parseLong(autoNumList.get(0).toString()) + 1;
					}else{
						autoSequenceNumber = Long.parseLong("1");
					}
					storageNew.setIdNum(autoSequenceNumber);
					
					storageNew.setItemNumber(storage.getItemNumber());
					storageNew.setItemTag(storage.getItemTag());
					storageNew.setJobNumber(storage.getJobNumber());
					storageNew.setLocationId(storage.getLocationId());
					storageNew.setModel(storage.getModel());
					storageNew.setPrice(storage.getPrice());
					storageNew.setSerial(storage.getSerial());
					storageNew.setServiceOrderId(storage.getServiceOrderId());
					storageNew.setTicket(storage.getTicket());
					storageNew.setUnit(storage.getUnit());
					storageNew.setVolUnit(storage.getVolUnit());
					storageNew.setWarehouse(storage.getWarehouse());
					
					if(balanceWeight!=null && !balanceWeight.equals("")){
						storageNew.setMeasQuantity(balanceWeight);
					}else{
						storageNew.setMeasQuantity(storage.getMeasQuantity());
					}
					if(balanceVolume!=null && !balanceVolume.equals("")){
						storageNew.setVolume(balanceVolume);
					}else{
						storageNew.setVolume(storage.getVolume());
					}
					if(balancePieces!=null && !balancePieces.equals("")){
						storageNew.setPieces(balancePieces);
					}else{
						if(storage.getPieces() == null){
							storageNew.setPieces(0);
						}else{
							storageNew.setPieces(storage.getPieces());
						}
					}
					storageManager.save(storageNew);
					
					BookStorage bookStorageNew1 = new BookStorage();	
					bookStorageNew1.setDescription(storage.getDescription());
					bookStorageNew1.setCorpID(sessionCorpID);
					bookStorageNew1.setCreatedBy(getRequest().getRemoteUser());
					bookStorageNew1.setCreatedOn(new Date());
					bookStorageNew1.setUpdatedOn(new Date());
					bookStorageNew1.setUpdatedBy(getRequest().getRemoteUser());
					bookStorageNew1.setWhat("L");
					bookStorageNew1.setShipNumber(storage.getShipNumber());
					bookStorageNew1.setIdNum(autoSequenceNumber);
					bookStorageNew1.setDated(new Date());
					bookStorageNew1.setServiceOrderId(workTicket.getServiceOrderId());
					if(balancePieces!=null && !balancePieces.equals("")){
						bookStorageNew.setPieces(balancePieces);
					}else{
						if(storage.getPieces() == null){
							bookStorageNew.setPieces(0);
						}else{
							bookStorageNew.setPieces(storage.getPieces());
						}
					}
					bookStorageNew1.setWarehouse(storage.getWarehouse());
					bookStorageNew1.setPrice(storage.getPrice());
					bookStorageNew1.setContainerId(storage.getContainerId());
					bookStorageNew1.setItemTag(storage.getItemTag());
					bookStorageNew1.setOldLocation("");
					bookStorageNew1.setLocationId(storage.getLocationId());
					bookStorageNew1.setTicket(workTicket.getTicket());
					bookStorageNew1.setStorage(storage);
					bookStorageManager.save(bookStorageNew1);
				}
				}
				storages = workTicketManager.getStorageList(workTicket.getShipNumber());
	
			} 
		}
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String accessStorageUnit() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");

		idCheck = idCheck.trim();
		if (idCheck.indexOf(",") == 0) {
			idCheck = idCheck.substring(1);
		}
		if (idCheck.lastIndexOf(",") == idCheck.length() - 1) {
			idCheck = idCheck.substring(0, idCheck.length() - 1);
		}
		String[] arrayid = idCheck.split(",");
		int arrayLength = arrayid.length;
		for (int i = 0; i < arrayLength; i++) {
			id = Long.parseLong(arrayid[i]);
			if (id != null) {
				workTicket = workTicketManager.get(id1);
				serviceOrder = workTicket.getServiceOrder();
				storage = storageManager.get(id);
				storageLibrary=storageLibraryManager.getByStorageId(storage.getStorageId(), sessionCorpID);
				location = locationManager.getByLocation(storage.getLocationId());	
				if(storageLibrary==null && location==null){
					String message="Storage ID and Location ID missing so Access not possible!";
					errorMessage(getText(message));
					hitFlag="0";
					return ERROR;
				}
				if(storageLibrary==null){
					String message="Storage ID missing so Access not possible!";
					errorMessage(getText(message));
					hitFlag="0";
					return ERROR;
				}
				if(location==null){
					String message="Location ID missing so Access not possible!";
					errorMessage(getText(message));
					hitFlag="0";
					return ERROR;
				}
				BookStorage bookStorageNew = new BookStorage();
				bookStorageNew.setDescription(storage.getDescription());
				bookStorageNew.setCorpID(sessionCorpID);
				bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
				bookStorageNew.setCreatedOn(new Date());
				bookStorageNew.setUpdatedOn(new Date());
				bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
				bookStorageNew.setWhat("A");
				bookStorageNew.setStorageId(storage.getStorageId());
				bookStorageNew.setShipNumber(storage.getShipNumber());
				bookStorageNew.setIdNum(storage.getIdNum());
				bookStorageNew.setDated(new Date());
				bookStorageNew.setServiceOrderId(workTicket.getServiceOrderId());

				if (storage.getPieces() == null) {
					bookStorageNew.setPieces(0);
				} else {
					bookStorageNew.setPieces(storage.getPieces());
				}
				bookStorageNew.setPrice(storage.getPrice());
				bookStorageNew.setContainerId(storage.getContainerId());
				bookStorageNew.setItemTag(storage.getItemTag());
				bookStorageNew.setOldLocation("");
				bookStorageNew.setLocationId(storage.getLocationId());
				bookStorageNew.setTicket(storage.getTicket());
				bookStorageNew.setStorage(storage);
				bookStorageManager.save(bookStorageNew);

				storage.setUpdatedOn(new Date());
				storage.setUpdatedBy(storage.getUpdatedBy());

				storageManager.save(storage);
				//storages = new ArrayList( workTicket.getStorages());
				storages = workTicketManager.getStorageList(workTicket.getShipNumber());

			} 
		}
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String releaseStorage() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		idCheck = idCheck.trim();
		if (idCheck.indexOf(",") == 0) {
			idCheck = idCheck.substring(1);
		}
		if (idCheck.lastIndexOf(",") == idCheck.length() - 1) {
			idCheck = idCheck.substring(0, idCheck.length() - 1);
		}
		String[] arrayid = idCheck.split(",");
		int arrayLength = arrayid.length;
		for (int i = 0; i < arrayLength; i++) {
			id = Long.parseLong(arrayid[i]);
			if (id != null) {
				//workTicket = workTicketManager.get(id1);
				//serviceOrder = workTicket.getServiceOrder();
				storage = storageManager.get(id);				
				workTicket = workTicketManager.get(Long.parseLong(workTicketManager.getWorkTicketIdByStorageTicket(storage.getTicket(),sessionCorpID)));
				serviceOrder = workTicket.getServiceOrder();
				storageLibrary=storageLibraryManager.getByStorageId(storage.getStorageId(), sessionCorpID);
				location = locationManager.getByLocation(storage.getLocationId());	
				if(storageLibrary==null && location==null){
					String message="Storage ID and Location ID missing so Release not possible!";
					errorMessage(getText(message));
					hitFlag="0";
					return ERROR;
				}
				if(storageLibrary==null){
					String message="Storage ID missing so Release not possible!";
					errorMessage(getText(message));
					hitFlag="0";
					return ERROR;
				}				
				if(location==null){
					String message="Location ID missing so Release not possible!";
					errorMessage(getText(message));
					hitFlag="0";
					return ERROR;
				}
				BookStorage bookStorageNew = new BookStorage();
	
				bookStorageNew.setDescription(storage.getDescription());
				bookStorageNew.setCorpID(sessionCorpID);
				bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
				bookStorageNew.setCreatedOn(new Date());
				bookStorageNew.setUpdatedOn(new Date());
				bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
				bookStorageNew.setWhat("R");
				bookStorageNew.setStorageId(storage.getStorageId());
				bookStorageNew.setShipNumber(storage.getShipNumber());
				bookStorageNew.setIdNum(storage.getIdNum());
				bookStorageNew.setDated(new Date());
				bookStorageNew.setServiceOrderId(workTicket.getServiceOrderId());
	
				if (storage.getPieces() == null) {
					bookStorageNew.setPieces(0);
				} else {
					bookStorageNew.setPieces(storage.getPieces());
				}
				bookStorageNew.setPrice(storage.getPrice());
				bookStorageNew.setContainerId(storage.getContainerId());
				bookStorageNew.setItemTag(storage.getItemTag());
				bookStorageNew.setOldLocation("");
				bookStorageNew.setLocationId(storage.getLocationId());
				bookStorageNew.setTicket(workTicket.getTicket());
				bookStorageNew.setStorage(storage);
				bookStorageManager.save(bookStorageNew);
							
				BigDecimal cuMultiplyer=new BigDecimal(35.3146).setScale(4,3);
                BigDecimal cuDivisor=new BigDecimal(35.3146);
                BigDecimal currentVolume=storage.getVolume();
                BigDecimal subcbm1=new BigDecimal("0");
                BigDecimal subcft1=new BigDecimal("0");
                BigDecimal availcbm1=new BigDecimal("0");
                BigDecimal availcft1=new BigDecimal("0");
                BigDecimal cft=storageLibrary.getUsedVolumeCft();
                BigDecimal cbm=storageLibrary.getUsedVolumeCbm();
                BigDecimal availcft=storageLibrary.getAvailVolumeCft();
                BigDecimal availcbm=storageLibrary.getAvailVolumeCbm();
                BigDecimal usedsubcftloc=new BigDecimal("0");
                BigDecimal usedsubcbmloc=new BigDecimal("0");
                BigDecimal cftloc = new BigDecimal("0");
                BigDecimal cbmloc = new BigDecimal("0");
                if(location.getUtilizedVolCft()!=null && !(location.getUtilizedVolCft().equals(""))){
                	cftloc=location.getUtilizedVolCft();
                }
                if(location.getUtilizedVolCbm()!=null && !(location.getUtilizedVolCbm().equals(""))){
                	cbmloc = location.getUtilizedVolCbm();
                }
                
                if(storage.getVolUnit().equalsIgnoreCase("Cft")){ 
                	// For storage Library
                    BigDecimal subcft=cft.subtract(currentVolume);
                    BigDecimal subcbm=cbm.subtract(currentVolume.divide(cuDivisor, 3, 3));
                    subcft1=subcft;
                    subcbm1=subcbm;
                    BigDecimal avalcft=availcft.add(currentVolume);
                    BigDecimal avalcbm=availcbm.add(currentVolume.divide(cuDivisor, 3, 3));
                    availcft1=avalcft;                  
                    availcbm1=avalcbm;  
                    // For location
                    BigDecimal subcftloc=cftloc.subtract(currentVolume);
                    BigDecimal subcbmloc=cbmloc.subtract(currentVolume.divide(cuDivisor, 3, 3));
                    usedsubcftloc = subcftloc;
                    usedsubcbmloc = subcbmloc;
                }else{   
                	// For storage Library
                    BigDecimal subcft=cft.subtract(currentVolume.multiply(cuMultiplyer));
                    BigDecimal subcbm=cbm.subtract(currentVolume);
                    subcft1=subcft;
                    subcbm1=subcbm;
                    BigDecimal avalcft=availcft.add(currentVolume.multiply(cuMultiplyer));
                    BigDecimal avalcbm=availcbm.add(currentVolume);
                    availcft1=avalcft;                  
                    availcbm1=avalcbm; 
                    // For location
                    BigDecimal subcftloc=cftloc.subtract(currentVolume.multiply(cuMultiplyer));
                    BigDecimal subcbmloc=cbmloc.subtract(currentVolume);
                    usedsubcftloc = subcftloc;
                    usedsubcbmloc = subcbmloc;
                }
                /*if(location.getCapacity().equalsIgnoreCase("1")){               	
                    location.setUtilizedVolCft(usedsubcftloc);
                    location.setUtilizedVolCbm(usedsubcbmloc);
					location.setOccupied("");
					location.setUpdatedOn(new Date());
					location.setUpdatedBy(getRequest().getRemoteUser());
					locationManager.save(location);
				}else{				
                    location.setUtilizedVolCft(usedsubcftloc);
                    location.setUtilizedVolCbm(usedsubcbmloc);
					location.setUpdatedOn(new Date());
					location.setUpdatedBy(getRequest().getRemoteUser());
					locationManager.save(location);	
				    }       
                
                
				if(storageLibrary.getStorageMode().equalsIgnoreCase("1")){
				  storageLibrary.setStorageAssigned(false);
				  storageLibrary.setUpdatedOn(new Date());
				  storageLibrary.setUpdatedBy(getRequest().getRemoteUser());
				  storageLibrary.setUsedVolumeCft(subcft1);
				  storageLibrary.setUsedVolumeCbm(subcbm1);
				  storageLibrary.setAvailVolumeCft(availcft1);
				  storageLibrary.setAvailVolumeCbm(availcbm1);
				  storageLibraryManager.save(storageLibrary);
				}else{
					storageLibrary.setUpdatedOn(new Date());
					storageLibrary.setUpdatedBy(getRequest().getRemoteUser());
					storageLibrary.setUsedVolumeCft(subcft1);
					storageLibrary.setUsedVolumeCbm(subcbm1);
					storageLibrary.setAvailVolumeCft(availcft1);
					storageLibrary.setAvailVolumeCbm(availcbm1);
					if(storageLibrary.getUsedVolumeCft().ROUND_UP==0){
						storageLibrary.setStorageAssigned(false);
					}
					storageLibraryManager.save(storageLibrary);
				}*/
				storage.setReleaseDate(new Date());
				storage.setUpdatedOn(new Date());
				storage.setUpdatedBy(storage.getUpdatedBy());
				storage.setToRelease(workTicket.getTicket());	
				storageManager.save(storage);
				//storages = new ArrayList( workTicket.getStorages());
				storages = workTicketManager.getStorageList(workTicket.getShipNumber());
	
			} 
		}
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String moveStorageRearrange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		if (userCheck.equals("") || userCheck.equals(",")) {
		}else{
			userCheck = userCheck.trim();
			if (userCheck.indexOf(",") == 0) {
				userCheck = userCheck.substring(1);
			}
			if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
				userCheck = userCheck.substring(0, userCheck.length() - 1);
			}
			String[] arrayId = userCheck.split(",");
			int arrayLength = arrayId.length;
			for (int i = 0; i < arrayLength; i++) {
				String [] locationIdNew = arrayId[i].split("~");
				
				List<Storage> storagesListCommon = storageManager.findByLocationRearrange(locationIdNew[0],locationIdNew[1]);
				Iterator<Storage> itSubCont = storagesListCommon.iterator();
				while(itSubCont.hasNext()){
					Storage storage = itSubCont.next();
					BookStorage bookStorageNew = new BookStorage();
					bookStorageNew.setDescription(storage.getDescription());
					bookStorageNew.setCorpID(sessionCorpID);
					bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
					bookStorageNew.setCreatedOn(new Date());
					bookStorageNew.setUpdatedOn(new Date());
					bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
					bookStorageNew.setWhat("M");
					bookStorageNew.setStorageId(storage.getStorageId());
					bookStorageNew.setShipNumber(storage.getShipNumber());
					bookStorageNew.setIdNum(storage.getIdNum());
					bookStorageNew.setDated(new Date());
					bookStorageNew.setServiceOrderId(storage.getServiceOrderId());

					if (storage.getPieces() == null) {
						bookStorageNew.setPieces(0);
					} else {
						bookStorageNew.setPieces(storage.getPieces());
					}
					bookStorageNew.setPrice(storage.getPrice());
					bookStorageNew.setContainerId(storage.getContainerId());
					bookStorageNew.setItemTag(storage.getItemTag());
					bookStorageNew.setOldLocation(storage.getLocationId());
					bookStorageNew.setLocationId(locationId);
					bookStorageNew.setTicket(storage.getTicket());
					bookStorageNew.setStorage(storage);
					bookStorageManager.save(bookStorageNew);

					locationOld = locationManager.getByLocation(storage.getLocationId());
					if(locationOld.getCapacity().equalsIgnoreCase("1")){
					locationOld.setOccupied("");
					}
					locationOld.setUpdatedBy(getRequest().getRemoteUser());
					locationOld.setUpdatedOn(new Date());
					locationManager.save(locationOld);

					location = locationManager.getByLocation(locationId);
					location.setOccupied("X");
					location.setUpdatedBy(getRequest().getRemoteUser());
					location.setUpdatedOn(new Date());
					locationManager.save(location);

					storage.setLocationId(locationId);
					storage.setUpdatedOn(new Date());
					storage.setUpdatedBy(getRequest().getRemoteUser());

					storageManager.save(storage);
					//storages = new ArrayList( workTicket.getStorages() );
					stoLoRearranges = storageLibraryManager.searchStoLocRearrangeList(sessionCorpID, locationId,"");
				}
				
			}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}	
	
	
	public String move() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (id != null) {
			workTicket = workTicketManager.get(id1);
			serviceOrder = workTicket.getServiceOrder();
			storage = storageManager.get(id);

			BookStorage bookStorageNew = new BookStorage();
			bookStorageNew.setDescription(storage.getDescription());
			bookStorageNew.setCorpID(sessionCorpID);
			bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
			bookStorageNew.setCreatedOn(new Date());
			bookStorageNew.setUpdatedOn(new Date());
			bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
			bookStorageNew.setWhat("M");
			bookStorageNew.setShipNumber(storage.getShipNumber());
			bookStorageNew.setIdNum(storage.getIdNum());
			bookStorageNew.setDated(new Date());
			bookStorageNew.setServiceOrderId(workTicket.getServiceOrderId());

			if (storage.getPieces() == null) {
				bookStorageNew.setPieces(0);
			} else {
				bookStorageNew.setPieces(storage.getPieces());
			}
			bookStorageNew.setPrice(storage.getPrice());
			bookStorageNew.setContainerId(storage.getContainerId());
			bookStorageNew.setItemTag(storage.getItemTag());
			bookStorageNew.setOldLocation(storage.getLocationId());
			bookStorageNew.setLocationId(locationId);
			bookStorageNew.setTicket(storage.getTicket());
			bookStorageNew.setStorage(storage);
			bookStorageManager.save(bookStorageNew);

			locationOld = locationManager.getByLocation(storage.getLocationId());
			locationOld.setOccupied("");
			locationManager.save(locationOld);

			location = locationManager.getByLocation(locationId);
			location.setOccupied("X");
			locationManager.save(location);

			storage.setLocationId(locationId);
			storage.setUpdatedOn(new Date());
			storage.setUpdatedBy(storage.getUpdatedBy());

			storageManager.save(storage);
			//storages = new ArrayList( workTicket.getStorages() );
			storages = workTicketManager.getStorageList(workTicket.getShipNumber());
		} else {

		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String moveUnit() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (id != null) {
			workTicket = workTicketManager.get(id1);
			serviceOrder = workTicket.getServiceOrder();
			storage = storageManager.get(id);
			storageLibrary=storageLibraryManager.getByStorageId(storage.getStorageId(), sessionCorpID);
			if(storageLibrary.getStorageMode().equalsIgnoreCase("5")){
				//Storage storage = itSubCont.next();
				BookStorage bookStorageNew = new BookStorage();
				bookStorageNew.setDescription(storage.getDescription());
				bookStorageNew.setCorpID(sessionCorpID);
				bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
				bookStorageNew.setCreatedOn(new Date());
				bookStorageNew.setUpdatedOn(new Date());
				bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
				bookStorageNew.setWhat("M");
				bookStorageNew.setStorageId(storage.getStorageId());
				bookStorageNew.setShipNumber(storage.getShipNumber());
				bookStorageNew.setIdNum(storage.getIdNum());
				bookStorageNew.setDated(new Date());
				bookStorageNew.setServiceOrderId(storage.getServiceOrderId());

				if (storage.getPieces() == null) {
					bookStorageNew.setPieces(0);
				} else {
					bookStorageNew.setPieces(storage.getPieces());
				}
				bookStorageNew.setPrice(storage.getPrice());
				bookStorageNew.setContainerId(storage.getContainerId());
				bookStorageNew.setItemTag(storage.getItemTag());
				bookStorageNew.setOldLocation(storage.getLocationId());
				bookStorageNew.setLocationId(locationId);
				bookStorageNew.setTicket(storage.getTicket());
				bookStorageNew.setStorage(storage);
				bookStorageManager.save(bookStorageNew);

				locationOld = locationManager.getByLocation(storage.getLocationId());
				if(locationOld.getCapacity().equalsIgnoreCase("1")){
				locationOld.setOccupied("");
				}
				locationOld.setUpdatedBy(getRequest().getRemoteUser());
				locationOld.setUpdatedOn(new Date());
				locationManager.save(locationOld);

				location = locationManager.getByLocation(locationId);
				location.setOccupied("X");
				location.setUpdatedBy(getRequest().getRemoteUser());
				location.setUpdatedOn(new Date());
				locationManager.save(location);

				storage.setLocationId(locationId);
				storage.setUpdatedOn(new Date());
				storage.setUpdatedBy(getRequest().getRemoteUser());

				storageManager.save(storage);
				
			}else{
			List<Storage> storagesListCommon = storageManager.findByLocationInternalRearrange(storage.getStorageId(),storage.getLocationId(),storage.getTicket(),storageLibrary.getStorageMode());
				Iterator<Storage> itSubCont = storagesListCommon.iterator();
				while(itSubCont.hasNext()){
					Storage storage = itSubCont.next();
					BookStorage bookStorageNew = new BookStorage();
					bookStorageNew.setDescription(storage.getDescription());
					bookStorageNew.setCorpID(sessionCorpID);
					bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
					bookStorageNew.setCreatedOn(new Date());
					bookStorageNew.setUpdatedOn(new Date());
					bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
					bookStorageNew.setWhat("M");
					bookStorageNew.setStorageId(storage.getStorageId());
					bookStorageNew.setShipNumber(storage.getShipNumber());
					bookStorageNew.setIdNum(storage.getIdNum());
					bookStorageNew.setDated(new Date());
					bookStorageNew.setServiceOrderId(storage.getServiceOrderId());

					if (storage.getPieces() == null) {
						bookStorageNew.setPieces(0);
					} else {
						bookStorageNew.setPieces(storage.getPieces());
					}
					bookStorageNew.setPrice(storage.getPrice());
					bookStorageNew.setContainerId(storage.getContainerId());
					bookStorageNew.setItemTag(storage.getItemTag());
					bookStorageNew.setOldLocation(storage.getLocationId());
					bookStorageNew.setLocationId(locationId);
					bookStorageNew.setTicket(storage.getTicket());
					bookStorageNew.setStorage(storage);
					bookStorageManager.save(bookStorageNew);

					locationOld = locationManager.getByLocation(storage.getLocationId());
					if(locationOld.getCapacity().equalsIgnoreCase("1")){
					locationOld.setOccupied("");
					}
					locationOld.setUpdatedBy(getRequest().getRemoteUser());
					locationOld.setUpdatedOn(new Date());
					locationManager.save(locationOld);

					location = locationManager.getByLocation(locationId);
					location.setOccupied("X");
					location.setUpdatedBy(getRequest().getRemoteUser());
					location.setUpdatedOn(new Date());
					locationManager.save(location);

					storage.setLocationId(locationId);
					storage.setUpdatedOn(new Date());
					storage.setUpdatedBy(getRequest().getRemoteUser());

					storageManager.save(storage);
				}
					storages = workTicketManager.getStorageList(workTicket.getShipNumber());
					//storages = new ArrayList( workTicket.getStorages() );
					//stoLoRearranges = storageLibraryManager.searchStoLocRearrangeList(sessionCorpID, locationId,"");
				}
	
			
			/*BookStorage bookStorageNew = new BookStorage();
			bookStorageNew.setDescription(storage.getDescription());
			bookStorageNew.setCorpID(sessionCorpID);
			bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
			bookStorageNew.setCreatedOn(new Date());
			bookStorageNew.setUpdatedOn(new Date());
			bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
			bookStorageNew.setWhat("M");
			bookStorageNew.setStorageId(storage.getStorageId());
			bookStorageNew.setShipNumber(storage.getShipNumber());
			bookStorageNew.setIdNum(storage.getIdNum());
			bookStorageNew.setDated(new Date());
			bookStorageNew.setServiceOrderId(workTicket.getServiceOrderId());

			if (storage.getPieces() == null) {
				bookStorageNew.setPieces(0);
			} else {
				bookStorageNew.setPieces(storage.getPieces());
			}
			bookStorageNew.setPrice(storage.getPrice());
			bookStorageNew.setContainerId(storage.getContainerId());
			bookStorageNew.setItemTag(storage.getItemTag());
			bookStorageNew.setOldLocation(storage.getLocationId());
			bookStorageNew.setLocationId(locationId);
			bookStorageNew.setTicket(storage.getTicket());
			bookStorageNew.setStorage(storage);
			bookStorageManager.save(bookStorageNew);

			locationOld = locationManager.getByLocation(storage.getLocationId());
			if(locationOld.getCapacity().equalsIgnoreCase("1")){
				locationOld.setOccupied("");
			}			
			locationManager.save(locationOld);
			location = locationManager.getByLocation(locationId);
			location.setOccupied("X");
			locationManager.save(location);

			storage.setLocationId(locationId);
			storage.setUpdatedOn(new Date());
			storage.setUpdatedBy(storage.getUpdatedBy());

			storageManager.save(storage);
			//storages = new ArrayList( workTicket.getStorages() );
*/		
		} else {

		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	public String moveStorageUnit() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (id != null) {
			workTicket = workTicketManager.get(id1);
			serviceOrder = workTicket.getServiceOrder();
			storage = storageManager.get(id);
			oldStorage = storageManager.get(id2);
			storageLibrary=storageLibraryManager.getByStorageId(storage.getStorageId(), sessionCorpID);
			//if(storageLibrary.getStorageMode().equalsIgnoreCase("5")){
				//Storage storage = itSubCont.next();
				BookStorage bookStorageNew = new BookStorage();
				bookStorageNew.setDescription(oldStorage.getDescription());
				bookStorageNew.setCorpID(sessionCorpID);
				bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
				bookStorageNew.setCreatedOn(new Date());
				bookStorageNew.setUpdatedOn(new Date());
				bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
				bookStorageNew.setWhat("M");
				bookStorageNew.setStorageId(storage.getStorageId());
				bookStorageNew.setOldStorage(storageId);
				bookStorageNew.setShipNumber(oldStorage.getShipNumber());
				bookStorageNew.setIdNum(oldStorage.getIdNum());
				bookStorageNew.setDated(new Date());
				bookStorageNew.setServiceOrderId(oldStorage.getServiceOrderId());

				if (oldStorage.getPieces() == null) {
					bookStorageNew.setPieces(0);
				} else {
					bookStorageNew.setPieces(oldStorage.getPieces());
				}
				bookStorageNew.setPrice(oldStorage.getPrice());
				bookStorageNew.setContainerId(oldStorage.getContainerId());
				bookStorageNew.setItemTag(oldStorage.getItemTag());
				bookStorageNew.setOldLocation("");
				bookStorageNew.setLocationId(oldStorage.getLocationId());
				bookStorageNew.setTicket(oldStorage.getTicket());
				bookStorageNew.setStorage(oldStorage);
				bookStorageManager.save(bookStorageNew);

			/*	locationOld = locationManager.getByLocation(storage.getLocationId());
				if(locationOld.getCapacity().equalsIgnoreCase("1")){
				locationOld.setOccupied("");
				}
				locationOld.setUpdatedBy(getRequest().getRemoteUser());
				locationOld.setUpdatedOn(new Date());
				locationManager.save(locationOld);

				location = locationManager.getByLocation(locationId);
				location.setOccupied("X");
				location.setUpdatedBy(getRequest().getRemoteUser());
				location.setUpdatedOn(new Date());
				locationManager.save(location);*/
                if(storageLibrary.getStorageMode().equalsIgnoreCase("1")){
                	storageLibrary.setStorageAssigned(false);
                }
				storageLibrary.setUpdatedBy(getRequest().getRemoteUser());
				storageLibrary.setUpdatedOn(new Date());
				
				storageLibraryManager.save(storageLibrary);
				
				oldStorage.setStorageId(storage.getStorageId());
				oldStorage.setUpdatedOn(new Date());
				oldStorage.setUpdatedBy(getRequest().getRemoteUser());
				hitFlag="1";
				storageManager.save(oldStorage);
				
			/*}else{
			List<Storage> storagesListCommon = storageManager.findByLocationInternalRearrange(storage.getStorageId(),storage.getLocationId(),storage.getTicket(),storageLibrary.getStorageMode());
				Iterator<Storage> itSubCont = storagesListCommon.iterator();
				while(itSubCont.hasNext()){
					Storage storage = itSubCont.next();
					BookStorage bookStorageNew = new BookStorage();
					bookStorageNew.setDescription(storage.getDescription());
					bookStorageNew.setCorpID(sessionCorpID);
					bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
					bookStorageNew.setCreatedOn(new Date());
					bookStorageNew.setUpdatedOn(new Date());
					bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
					bookStorageNew.setWhat("M");
					bookStorageNew.setStorageId(storage.getStorageId());
					bookStorageNew.setOldStorage(storageId);
					bookStorageNew.setShipNumber(storage.getShipNumber());
					bookStorageNew.setIdNum(storage.getIdNum());
					bookStorageNew.setDated(new Date());
					bookStorageNew.setServiceOrderId(storage.getServiceOrderId());

					if (storage.getPieces() == null) {
						bookStorageNew.setPieces(0);
					} else {
						bookStorageNew.setPieces(storage.getPieces());
					}
					bookStorageNew.setPrice(storage.getPrice());
					bookStorageNew.setContainerId(storage.getContainerId());
					bookStorageNew.setItemTag(storage.getItemTag());
					bookStorageNew.setOldLocation("");
					bookStorageNew.setLocationId(storage.getLocationId());
					bookStorageNew.setTicket(storage.getTicket());
					bookStorageNew.setStorage(storage);
					bookStorageManager.save(bookStorageNew);

					locationOld = locationManager.getByLocation(storage.getLocationId());
					if(locationOld.getCapacity().equalsIgnoreCase("1")){
					locationOld.setOccupied("");
					}
					locationOld.setUpdatedBy(getRequest().getRemoteUser());
					locationOld.setUpdatedOn(new Date());
					locationManager.save(locationOld);

					location = locationManager.getByLocation(locationId);
					location.setOccupied("X");
					location.setUpdatedBy(getRequest().getRemoteUser());
					location.setUpdatedOn(new Date());
					locationManager.save(location);

					storage.setLocationId(storage.getLocationId());
					storage.setUpdatedOn(new Date());
					storage.setUpdatedBy(getRequest().getRemoteUser());
					hitFlag="1";
					storageManager.save(storage);
				}*/
					storages = workTicketManager.getStorageList(workTicket.getShipNumber());
				//}
	
			
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	

	public String search() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean itemNumber = (storage.getItemNumber() == null);
		boolean locationId = (storage.getLocationId() == null);
		boolean jobNumber = (storage.getJobNumber() == null);
		boolean ticket = (storage.getTicket() == null);
		if (!itemNumber || !jobNumber || !locationId || !ticket) {
			storages = storageManager.findByItemNumber(storage.getItemNumber(), storage.getJobNumber(), storage.getLocationId(), storage.getTicket());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String searchStorageTrace() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean itemNumber = (storage.getDescription() == null);
		boolean locationId = (storage.getLocationId() == null);
		boolean jobNumber = (storage.getShipNumber() == null);
		boolean ticket = (storage.getTicket() == null);
		boolean storageId = (storage.getStorageId() == null);
		if (!itemNumber || !jobNumber || !locationId || !ticket || !storageId) {
			storages = storageManager.search(storage.getDescription(), storage.getShipNumber(), storage.getLocationId() ,(storage.getStorageId()==null?"": storage.getStorageId()), storage.getTicket(), firstname, lastName, storage.getItemTag(), sessionCorpID);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String storageLibraryVolume(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	storageLibraryVolumeList=storageManager.storageLibraryVolumeList(storageId);
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;
	}
	
	public String handOut(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String handOutList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		storages = storageManager.handOutList(ticket, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	private String hitFlag;
	private String wtktServiceId;
	private String actual;
	public String releaseHandout() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		idCheck = idCheck.trim();
		if (idCheck.indexOf(",") == 0) {
			idCheck = idCheck.substring(1);
		}
		if (idCheck.lastIndexOf(",") == idCheck.length() - 1) {
			idCheck = idCheck.substring(0, idCheck.length() - 1);
		}
		String[] arrayid = idCheck.split(",");
		int arrayLength = arrayid.length;
		for (int i = 0; i < arrayLength; i++) {
			id = Long.parseLong(arrayid[i]);
			if (id != null) {				
				storage = storageManager.get(id);
				storageLibrary=storageLibraryManager.getByStorageId(storage.getStorageId(), sessionCorpID);
				location = locationManager.getByLocation(storage.getLocationId());
				if(storageLibrary==null && location==null){
					String message="Storage ID and Location ID missing so Release not possible!";
					errorMessage(getText(message));
					hitFlag="0";
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
					return ERROR;
				}
				if(storageLibrary==null){
					String message="Storage ID missing so Release not possible!";
					errorMessage(getText(message));
					hitFlag="0";
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
					return ERROR;
				}	
				if(location==null){
					String message="Location ID missing so Release not possible!";
					errorMessage(getText(message));
					hitFlag="0";
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
					return ERROR;
				}
				workTicket = workTicketManager.get(Long.parseLong(workTicketManager.getWorkTicketIdByStorageTicket(storage.getTicket(),sessionCorpID)));
				serviceOrder = workTicket.getServiceOrder();
				BookStorage bookStorageNew = new BookStorage();
	
				bookStorageNew.setDescription(storage.getDescription());
				bookStorageNew.setCorpID(sessionCorpID);
				bookStorageNew.setCreatedBy(getRequest().getRemoteUser());
				bookStorageNew.setCreatedOn(new Date());
				bookStorageNew.setUpdatedOn(new Date());
				bookStorageNew.setUpdatedBy(getRequest().getRemoteUser());
				bookStorageNew.setWhat("R");
				bookStorageNew.setStorageId(storage.getStorageId());
				bookStorageNew.setShipNumber(storage.getShipNumber());
				bookStorageNew.setIdNum(storage.getIdNum());
				bookStorageNew.setDated(new Date());
				bookStorageNew.setServiceOrderId(workTicket.getServiceOrderId());
	
				if (storage.getPieces() == null) {
					bookStorageNew.setPieces(0);
				} else {
					bookStorageNew.setPieces(storage.getPieces());
				}
				bookStorageNew.setPrice(storage.getPrice());
				bookStorageNew.setContainerId(storage.getContainerId());
				bookStorageNew.setItemTag(storage.getItemTag());
				bookStorageNew.setOldLocation("");
				bookStorageNew.setLocationId(storage.getLocationId());
				bookStorageNew.setTicket(workTicket.getTicket());
				bookStorageNew.setStorage(storage);
				bookStorageManager.save(bookStorageNew);
						
				BigDecimal cuMultiplyer=new BigDecimal(35.3146).setScale(4,3);
                BigDecimal cuDivisor=new BigDecimal(35.3146);
                BigDecimal currentVolume=storage.getVolume();
                BigDecimal subcbm1=new BigDecimal("0");
                BigDecimal subcft1=new BigDecimal("0");
                BigDecimal availcbm1=new BigDecimal("0");
                BigDecimal availcft1=new BigDecimal("0");
                BigDecimal cft=storageLibrary.getUsedVolumeCft();
                BigDecimal cbm=storageLibrary.getUsedVolumeCbm();
                BigDecimal availcft=storageLibrary.getAvailVolumeCft();
                BigDecimal availcbm=storageLibrary.getAvailVolumeCbm();
                BigDecimal usedsubcftloc=new BigDecimal("0");
                BigDecimal usedsubcbmloc=new BigDecimal("0");
                BigDecimal cftloc = new BigDecimal("0");
                BigDecimal cbmloc = new BigDecimal("0");
                if(location.getUtilizedVolCft()!=null && !(location.getUtilizedVolCft().equals(""))){
                	cftloc=location.getUtilizedVolCft();
                }
                if(location.getUtilizedVolCbm()!=null && !(location.getUtilizedVolCbm().equals(""))){
                	cbmloc = location.getUtilizedVolCbm();
                }
                
                if(storage.getVolUnit().equalsIgnoreCase("Cft")){ 
                	// For storage Library
                    BigDecimal subcft=cft.subtract(currentVolume);
                    BigDecimal subcbm=cbm.subtract(currentVolume.divide(cuDivisor, 3, 3));
                    subcft1=subcft;
                    subcbm1=subcbm;
                    BigDecimal avalcft=availcft.add(currentVolume);
                    BigDecimal avalcbm=availcbm.add(currentVolume.divide(cuDivisor, 3, 3));
                    availcft1=avalcft;                  
                    availcbm1=avalcbm;  
                    // For location
                    BigDecimal subcftloc=cftloc.subtract(currentVolume);
                    BigDecimal subcbmloc=cbmloc.subtract(currentVolume.divide(cuDivisor, 3, 3));
                    usedsubcftloc = subcftloc;
                    usedsubcbmloc = subcbmloc;
                }else{   
                	// For storage Library
                    BigDecimal subcft=cft.subtract(currentVolume.multiply(cuMultiplyer));
                    BigDecimal subcbm=cbm.subtract(currentVolume);
                    subcft1=subcft;
                    subcbm1=subcbm;
                    BigDecimal avalcft=availcft.add(currentVolume.multiply(cuMultiplyer));
                    BigDecimal avalcbm=availcbm.add(currentVolume);
                    availcft1=avalcft;                  
                    availcbm1=avalcbm; 
                    // For location
                    BigDecimal subcftloc=cftloc.subtract(currentVolume);
                    BigDecimal subcbmloc=cbmloc.subtract(currentVolume.divide(cuDivisor, 3, 3));
                    usedsubcftloc = subcftloc;
                    usedsubcbmloc = subcbmloc;
                }
                if(location.getCapacity().equalsIgnoreCase("1")){               	
                    location.setUtilizedVolCft(usedsubcftloc);
                    location.setUtilizedVolCbm(usedsubcbmloc);
					location.setOccupied("");
					location.setUpdatedOn(new Date());
					location.setUpdatedBy(getRequest().getRemoteUser());
					locationManager.save(location);
				}else{				
                    location.setUtilizedVolCft(usedsubcftloc);
                    location.setUtilizedVolCbm(usedsubcbmloc);
					location.setUpdatedOn(new Date());
					location.setUpdatedBy(getRequest().getRemoteUser());
					locationManager.save(location);	
				    }       
                
                
				if(storageLibrary.getStorageMode().equalsIgnoreCase("1")){
				  storageLibrary.setStorageAssigned(false);
				  storageLibrary.setUpdatedOn(new Date());
				  storageLibrary.setUpdatedBy(getRequest().getRemoteUser());
				  storageLibrary.setUsedVolumeCft(subcft1);
				  storageLibrary.setUsedVolumeCbm(subcbm1);
				  storageLibrary.setAvailVolumeCft(availcft1);
				  storageLibrary.setAvailVolumeCbm(availcbm1);
				  storageLibraryManager.save(storageLibrary);
				}else{
					storageLibrary.setUpdatedOn(new Date());
					storageLibrary.setUpdatedBy(getRequest().getRemoteUser());
					storageLibrary.setUsedVolumeCft(subcft1);
					storageLibrary.setUsedVolumeCbm(subcbm1);
					storageLibrary.setAvailVolumeCft(availcft1);
					storageLibrary.setAvailVolumeCbm(availcbm1);
					if(storageLibrary.getUsedVolumeCft().ROUND_UP==0){
						storageLibrary.setStorageAssigned(false);
					}
					storageLibraryManager.save(storageLibrary);
				}
				storage.setReleaseDate(new Date());
				storage.setUpdatedOn(new Date());
				storage.setUpdatedBy(storage.getUpdatedBy());
				storage.setToRelease(workTicket.getTicket());	
				storageManager.save(storage);				
			} 
		}
		if(actual.equalsIgnoreCase("actualise")){
			workTicket = workTicketManager.get(Long.parseLong(wtktServiceId));
			workTicketManager.updateTicketStatus(workTicket.getTicket(),sessionCorpID);
		}
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	/*
	 public String listStorages() {  
	 System.out.println(storage.getLocationId());
	 storages = storageManager.findByLocation(storage.getLocationId());
	 return SUCCESS;   
	 }
	 */
	public void setLocationManager(LocationManager locationManager) {
		this.locationManager = locationManager;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Location getLocationOld() {
		return locationOld;
	}

	public void setLocationOld(Location locationOld) {
		this.locationOld = locationOld;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getAutoIdNumber() {
		return autoIdNumber;
	}

	public void setAutoIdNumber(Long autoIdNumber) {
		this.autoIdNumber = autoIdNumber;
	}

	public String getIdCheck() {
		return idCheck;
	}

	public void setIdCheck(String idCheck) {
		this.idCheck = idCheck;
	}

	public StorageLibrary getStorageLibrary() {
		return storageLibrary;
	}

	public void setStorageLibrary(StorageLibrary storageLibrary) {
		this.storageLibrary = storageLibrary;
	}

	public void setStorageLibraryManager(StorageLibraryManager storageLibraryManager) {
		this.storageLibraryManager = storageLibraryManager;
	}

	public List getStoLoRearranges() {
		return stoLoRearranges;
	}

	public void setStoLoRearranges(List stoLoRearranges) {
		this.stoLoRearranges = stoLoRearranges;
	}

	public String getUserCheck() {
		return userCheck;
	}

	public void setUserCheck(String userCheck) {
		this.userCheck = userCheck;
	}

	public List getStorageLibraryVolumeList() {
		return storageLibraryVolumeList;
	}

	public void setStorageLibraryVolumeList(List storageLibraryVolumeList) {
		this.storageLibraryVolumeList = storageLibraryVolumeList;
	}

	public String getStorageId() {
		return storageId;
	}

	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getWtktServiceId() {
		return wtktServiceId;
	}

	public void setWtktServiceId(String wtktServiceId) {
		this.wtktServiceId = wtktServiceId;
	}

	public String getActual() {
		return actual;
	}

	public void setActual(String actual) {
		this.actual = actual;
	}

	public Date getWorkTicketrelease() {
		return workTicketrelease;
	}

	public void setWorkTicketrelease(Date workTicketrelease) {
		this.workTicketrelease = workTicketrelease;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public Company getCompany() {
		return company;
	}

	public CompanyManager getCompanyManager() {
		return companyManager;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getJobSTLFlag() {
		return jobSTLFlag;
	}

	public void setJobSTLFlag(String jobSTLFlag) {
		this.jobSTLFlag = jobSTLFlag;
	}

	public String getSalesPortalFlag() {
		return salesPortalFlag;
	}

	public void setSalesPortalFlag(String salesPortalFlag) {
		this.salesPortalFlag = salesPortalFlag;
	}

	public Map<String, String> getState() {
		return state;
	}

	public void setState(Map<String, String> state) {
		this.state = state;
	}

	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getStateDescription() {
		return stateDescription;
	}

	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	public String getMailAddress() {
		return MailAddress;
	}

	public void setMailAddress(String mailAddress) {
		MailAddress = mailAddress;
	}

	public String getMailCity() {
		return MailCity;
	}

	public void setMailCity(String mailCity) {
		MailCity = mailCity;
	}

	public String getMailState() {
		return MailState;
	}

	public void setMailState(String mailState) {
		MailState = mailState;
	}

	public String getMailCountry() {
		return MailCountry;
	}

	public void setMailCountry(String mailCountry) {
		MailCountry = mailCountry;
	}

	public String getMailZip() {
		return MailZip;
	}

	public void setMailZip(String mailZip) {
		MailZip = mailZip;
	}

	public Map<String, String> getReleaseTypeRadio() {
		return releaseTypeRadio;
	}

	public void setReleaseTypeRadio(Map<String, String> releaseTypeRadio) {
		this.releaseTypeRadio = releaseTypeRadio;
	}

	public BigDecimal getBalanceWeight() {
		return balanceWeight;
	}

	public void setBalanceWeight(BigDecimal balanceWeight) {
		this.balanceWeight = balanceWeight;
	}

	public BigDecimal getBalanceVolume() {
		return balanceVolume;
	}

	public void setBalanceVolume(BigDecimal balanceVolume) {
		this.balanceVolume = balanceVolume;
	}

	public Integer getBalancePieces() {
		return balancePieces;
	}

	public void setBalancePieces(Integer balancePieces) {
		this.balancePieces = balancePieces;
	}

	public String getReleaseType() {
		return releaseType;
	}

	public void setReleaseType(String releaseType) {
		this.releaseType = releaseType;
	}

	public Long getAutoSequenceNumber() {
		return autoSequenceNumber;
	}

	public void setAutoSequenceNumber(Long autoSequenceNumber) {
		this.autoSequenceNumber = autoSequenceNumber;
	}

	public Long getId2() {
		return id2;
	}

	public void setId2(Long id2) {
		this.id2 = id2;
	}

	public Storage getOldStorage() {
		return oldStorage;
	}

	public void setOldStorage(Storage oldStorage) {
		this.oldStorage = oldStorage;
	}

}
