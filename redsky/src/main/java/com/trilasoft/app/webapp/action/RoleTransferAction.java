package com.trilasoft.app.webapp.action;
import java.util.ArrayList;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.service.RoleManager;
import org.appfuse.service.UserExistsException;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.CustomerFileManager;



public class RoleTransferAction extends BaseAction implements Preparable {
		private String sessionCorpID;
		 private List totalUser;
		 private String roleList;
		 private String userList;
		 private List pickAllRole; 
		 private List userRoleListData = new ArrayList();
		 private List remainUser = new ArrayList();
		 private UserManager userManager;
		 private String transferFrom;
		 private String roleTransfer;
		 private String transferTo;
		 private RoleManager roleManager;
		 private String activeStatus;
		public RoleTransferAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
		}
	    public void prepare() throws Exception {
	    	
	    }		
	    @SkipValidation
		public String list() {
			totalUser=userManager.getAllUserList(sessionCorpID);
			return SUCCESS;
	    }	  
		@SkipValidation
		public String pickAllRole()
		{
			pickAllRole=userManager.getAllUserRole(userList,sessionCorpID,activeStatus);
			return SUCCESS;
		}
		@SkipValidation
		public String pickAllUser()
		{
			remainUser=userManager.pickAllUser(roleList,sessionCorpID,userList,activeStatus);
			return SUCCESS;
		}
		private List<User> userNew;
		private CustomerFileManager customerFileManager;
		@SkipValidation
		public String updateUserWithRoles()
		{
			int rec=userManager.updateUserWithRoles(transferFrom,roleTransfer,transferTo,sessionCorpID);
			userNew = customerFileManager.findUserForRoleTransfer(transferTo);
			if (!(userNew == null || userNew.isEmpty())) {
				for (User userNew1 : userNew) {				
					String tempUserRole=userNew1.getRoles().toString();
					if(!(tempUserRole.indexOf(roleTransfer)>-1))
					{
						try{
						userNew1.addRole(roleManager.getRole(roleTransfer));
						userManager.saveUser(userNew1);
						}catch(UserExistsException e){}
					}
					}
			}
		return SUCCESS;
		}
		@SkipValidation
		public String InactiveUserList() {
			if(activeStatus.equalsIgnoreCase("true"))
			{
			totalUser=userManager.getAllInactiveUserList(sessionCorpID);
			}
			else{ 				
				totalUser=userManager.getAllUserList(sessionCorpID);	
			}
			return SUCCESS;
	    }
		
		public String getSessionCorpID() {
			return sessionCorpID;
		}
		public void setSessionCorpID(String sessionCorpID) {
			this.sessionCorpID = sessionCorpID;
		}
		public List getTotalUser() {
			return totalUser;
		}
		public void setTotalUser(List totalUser) {
			this.totalUser = totalUser;
		}
		public void setUserManager(UserManager userManager) {
			this.userManager = userManager;
		}
		public String getRoleList() {
			return roleList;
		}
		public void setRoleList(String roleList) {
			this.roleList = roleList;
		}
		public String getUserList() {
			return userList;
		}
		public void setUserList(String userList) {
			this.userList = userList;
		}
		public List getPickAllRole() {
			return pickAllRole;
		}
		public void setPickAllRole(List pickAllRole) {
			this.pickAllRole = pickAllRole;
		}
		public List getUserRoleListData() {
			return userRoleListData;
		}
		public void setUserRoleListData(List userRoleListData) {
			this.userRoleListData = userRoleListData;
		}
		public List getRemainUser() {
			return remainUser;
		}
		public void setRemainUser(List remainUser) {
			this.remainUser = remainUser;
		}
		public String getTransferFrom() {
			return transferFrom;
		}
		public void setTransferFrom(String transferFrom) {
			this.transferFrom = transferFrom;
		}
		public String getRoleTransfer() {
			return roleTransfer;
		}
		public void setRoleTransfer(String roleTransfer) {
			this.roleTransfer = roleTransfer;
		}
		public String getTransferTo() {
			return transferTo;
		}
		public void setTransferTo(String transferTo) {
			this.transferTo = transferTo;
		}
		public List<User> getUserNew() {
			return userNew;
		}
		public void setUserNew(List<User> userNew) {
			this.userNew = userNew;
		}
		public void setCustomerFileManager(CustomerFileManager customerFileManager) {
			this.customerFileManager = customerFileManager;
		}
		public void setRoleManager(RoleManager roleManager) {
			this.roleManager = roleManager;
		}
		public String getActiveStatus() {
			return activeStatus;
		}
		public void setActiveStatus(String activeStatus) {
			this.activeStatus = activeStatus;
		}
		



}



 

