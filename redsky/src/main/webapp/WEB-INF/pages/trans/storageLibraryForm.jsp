<%@ include file="/common/taglibs.jsp"%>
<head>
<title>Storage Library</title>
<meta name="heading" content="Storage Library" />
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
<style type="text/css">

</style>
<script language="javascript" type="text/javascript">
function checkField(){
 
	if(document.forms['StorageLibraryForm'].elements['storageLibrary.storageId'].value == ''){    	
		alert('Please fill the Storage Id.');
		return false;
	}
	if(document.forms['StorageLibraryForm'].elements['storageLibrary.storageType'].value == ''){    	
		alert('Please select the Storage type.');
		return false;
	}
	
	if(document.forms['StorageLibraryForm'].elements['storageLibrary.storageMode'].value == ''){    	
		alert('Please select the Storage mode.');
		return false;
	}
	<c:if test="${empty storageLibrary.id}">
	if(document.forms['StorageLibraryForm'].elements['storageLibrary.locationId'].value == ''){    	
		alert('Please select the Location id.');
		return false;
	}
	</c:if>
	
}



function findVolumeCft()
{
   var cubicMeter = document.forms['StorageLibraryForm'].elements['storageLibrary.volumeCbm'].value;
   if(cubicMeter>=0)
   {
   var cubicFeet=(35.3146) * cubicMeter;   
   var cubicFeetValue=cubicFeet.toFixed(2);
   document.forms['StorageLibraryForm'].elements['storageLibrary.volumeCft'].value=cubicFeetValue;
   var usedVolumeCft = document.forms['StorageLibraryForm'].elements['storageLibrary.usedVolumeCft'].value;
   var availVolumeCft = cubicFeetValue-usedVolumeCft;
   var availVolumeCftValue=availVolumeCft.toFixed(2);
   document.forms['StorageLibraryForm'].elements['storageLibrary.availVolumeCft'].value=availVolumeCftValue;
   var usedVolCbm=document.forms['StorageLibraryForm'].elements['storageLibrary.usedVolumeCbm'].value;
   var availVolumeCbm=cubicMeter-usedVolCbm;
   var availVolumeCbmValue=availVolumeCbm.toFixed(2);
   document.forms['StorageLibraryForm'].elements['storageLibrary.availVolumeCbm'].value=availVolumeCbmValue;
   document.forms['StorageLibraryForm'].elements['storageLibrary.volumeCft'].focus(); 
   }
  
}

function findVolumeCbm()
{
   var cubicFeet = document.forms['StorageLibraryForm'].elements['storageLibrary.volumeCft'].value;
   if(cubicFeet>=0)
   {
   var cubicMeter =(0.0283168466) * cubicFeet;
   var cubicMeterValue=cubicMeter.toFixed(2);
   document.forms['StorageLibraryForm'].elements['storageLibrary.volumeCbm'].value=cubicMeterValue;
   var usedVolumeCbm = document.forms['StorageLibraryForm'].elements['storageLibrary.usedVolumeCbm'].value;
   var availVolumeCbm = cubicMeterValue-usedVolumeCbm;
   var availVolumeCbmValue=availVolumeCbm.toFixed(2);
   document.forms['StorageLibraryForm'].elements['storageLibrary.availVolumeCbm'].value=availVolumeCbmValue;
   var usedVolumeCft = document.forms['StorageLibraryForm'].elements['storageLibrary.usedVolumeCft'].value;
   var availVolumeCft=cubicFeet-usedVolumeCft;
   var availVolumeCftValue=availVolumeCft.toFixed(2);
   document.forms['StorageLibraryForm'].elements['storageLibrary.availVolumeCft'].value=availVolumeCftValue;
   document.forms['StorageLibraryForm'].elements['storageLibrary.volumeCbm'].focus(); 
   }
  
}

function findUtilizedVolCft()
{
   var cubicMeter = document.forms['StorageLibraryForm'].elements['storageLibrary.usedVolumeCbm'].value;
   if(cubicMeter>=0)
   {
   var cubicFeet=(35.3146) * cubicMeter;   
   var cubicFeetValue=cubicFeet.toFixed(2);
   document.forms['StorageLibraryForm'].elements['storageLibrary.usedVolumeCft'].value=cubicFeetValue;
   var volumeCft = document.forms['StorageLibraryForm'].elements['storageLibrary.volumeCft'].value;
   var availVolumeCft = volumeCft-cubicFeetValue;
   var availVolumeCftValue=availVolumeCft.toFixed(2);
   document.forms['StorageLibraryForm'].elements['storageLibrary.availVolumeCft'].value=availVolumeCftValue;
   
   var availVolumeCbm=(0.0283168466) * availVolumeCft;
   var availVolumeCbmValue=availVolumeCbm.toFixed(2);
   document.forms['StorageLibraryForm'].elements['storageLibrary.availVolumeCbm'].value=availVolumeCbmValue;
   document.forms['StorageLibraryForm'].elements['storageLibrary.usedVolumeCft'].focus(); 
   }
   
}

function findUtilizedVolCbm()
{
   var cubicFeet = document.forms['StorageLibraryForm'].elements['storageLibrary.usedVolumeCft'].value;
   if(cubicFeet>=0)
   {
   var cubicMeter =(0.0283168466) * cubicFeet;
   var cubicMeterValue=cubicMeter.toFixed(2);
   document.forms['StorageLibraryForm'].elements['storageLibrary.usedVolumeCbm'].value=cubicMeterValue;
   var volumeCbm = document.forms['StorageLibraryForm'].elements['storageLibrary.volumeCbm'].value;
   var volumeCft = document.forms['StorageLibraryForm'].elements['storageLibrary.volumeCft'].value;
   var availVolumeCbm = volumeCbm-cubicMeterValue;
   var availVolumeCbmValue=availVolumeCbm.toFixed(2);
   document.forms['StorageLibraryForm'].elements['storageLibrary.availVolumeCbm'].value=availVolumeCbmValue;
   var availVolumeCft=volumeCft-cubicFeet;
   var availVolumeCftValue=availVolumeCft.toFixed(2);
   document.forms['StorageLibraryForm'].elements['storageLibrary.availVolumeCft'].value=availVolumeCftValue;
   document.forms['StorageLibraryForm'].elements['storageLibrary.usedVolumeCbm'].focus(); 
   }
   
}

function findDescriptionByCode(sel){ 
	var value = sel.options[sel.selectedIndex].text;
	document.forms['StorageLibraryForm'].elements['storageLibrary.storageDes'].value=value;
}

function onlyFloat(targetElement)
{   var i;
	var s = targetElement.value;
	var count = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if (((c < '0') || (c > '9')) && (c != '.') || (count>'1')) 
        {
        	alert("Enter valid Number");
	        document.getElementById(targetElement.id).value=0;
            document.getElementById(targetElement.id).select();
	        return false;
        }
    }
    return true;
}

function findLocationId() {
		      
		 	  openWindow('findAllLocationId.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=storageLibrary.locationId',1024,500);            
		 	  document.forms['StorageLibraryForm'].elements['storageLibrary.locationId'].select();
		 }
	
</script>
</head>
<body style="color:#222222;background-color:#dddddd">
<s:form id="StorageLibraryForm" name="StorageLibraryForm" action="saveStorageLibrary.html" method="post" validate="true">
	<s:hidden name="storageLibrary.id"/>
	<s:hidden name="storageLibrary.storageDes"/>
	<s:hidden name="description"/>
	<s:hidden name="secondDescription"/>
    <s:hidden name="thirdDescription"/>
    <s:hidden name="fourthDescription"/>
    <s:hidden name="fifthDescription"/>
    <s:hidden name="sixthDescription"/>
	<div id="Layer1" style="width:100%;">
	<div id="newmnav">
     	<ul>
     			<li><a href="storageLibraries.html"><span>Storage Library List</span></a></li>
        		<li id="newmnav1" style="background:#FFF"><a href="storageLibraries.html" class="current"><span>Storage library <img src="images/navarrow.gif" align="absmiddle" /></span></a></li>       
      </ul>
  </div>
  <div class="spn" style="!margin-bottom:2px;">&nbsp;</div>
  <div id="content" align="center">
   <div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="0" cellpadding="0" border="0" style="width:650px;">
	<tbody>
			<tr>
				<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">	     
				<tbody>
				
						<tr>
							<td align="right" width="20px"></td>
							<td align="right" class="listwhitetext" width="200px">Storage Id<font color="red" size="2">*</font></td>
                            <td class="listwhitetext"><s:textfield name="storageLibrary.storageId" maxlength="25" required="true" cssClass="input-text" cssStyle="width:248px" tabindex="1"/></td>
                            <c:if test="${empty storageLibrary.id}">
                            <td class="listwhitetext" align="right" width="">Location<font color="red" size="2">*</font></td>
							<td width="230px" ><s:textfield name="storageLibrary.locationId"  cssClass="input-text" cssStyle="width:170px;" tabindex="2" readonly="true" onkeydown="return onlyDel(event,this);"/>
							<img align="right" style="vertical-align:top;" class="openpopup" width="17" height="20" onclick="findLocationId();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
							</c:if>
							<c:if test="${not empty storageLibrary.id}">
                            <td class="listwhitetext" align="right" width=""></td>
							<td width="230px" ><s:hidden name="storageLibrary.locationId"  cssClass="input-textUpper" cssStyle="width:170px;" />
							</td>
							</c:if>
							
							<td></td>
						</tr>
                        <tr>
                        <td></td>
                        <td align="right" class="listwhitetext">Storage Type<font color="red" size="2">*</font></td>
						<td><s:select name="storageLibrary.storageType" list="%{storageType}" headerKey=""  headerValue="" cssStyle="width:250px" cssClass="list-menu" onchange="findDescriptionByCode(this);" tabindex="2"/>				
						
						</td> 					
						</tr>
						<tr></tr><tr></tr><tr>
							<td></td>
							<td align="right" class="listwhitetext">Storage Mode<font color="red" size="2">*</font></td>
							<td><s:select name="storageLibrary.storageMode" cssClass="list-menu" cssStyle="width:252px" list="%{storageMode}" headerKey=""  headerValue="" tabindex="3"/></td>
							<td></td>
						</tr>
						<tr></tr><tr></tr><tr>
							<td></td>
							<td align="right" class="listwhitetext">Storage Description<font color="red" size="2"></font></td>
							 <td class="listwhitetext"><s:textarea name="storageLibrary.storageDescription" cssClass="input-text" cssStyle="width:250px;height:100px" tabindex="4"/></td>
							<td></td>					
						</tr>
	
						<tr></tr><tr>
							<td></td>
							<td align="right" class="listwhitetext" >Volume</td>
							<td class="listwhitetext" ><s:textfield name="storageLibrary.volumeCft" maxlength="10" cssClass="input-text" tabindex="5" cssStyle="width:95px;text-align:right" onkeyup="onlyFloat(this);" onchange="findVolumeCbm();" />Cft&nbsp;&nbsp;&nbsp;&nbsp;	
							<s:textfield name="storageLibrary.volumeCbm" maxlength="10" cssClass="input-text" tabindex="6" cssStyle="width:95px;text-align:right" onkeyup="onlyFloat(this);" onchange="findVolumeCft();" />Cbm</td>
						    <td></td>
						</tr>
						
						<tr></tr><tr>
							<td></td>
							<td align="right" class="listwhitetext" >Used Volume</td>
							<td class="listwhitetext" ><s:textfield name="storageLibrary.usedVolumeCft"  maxlength="10" cssClass="input-text" tabindex="7" cssStyle="width:95px;text-align:right" onkeyup="onlyFloat(this);" onchange="findUtilizedVolCbm();" />Cft&nbsp;&nbsp;&nbsp;&nbsp;	
							<s:textfield name="storageLibrary.usedVolumeCbm" maxlength="10" cssClass="input-text" tabindex="8" cssStyle="width:95px;text-align:right" onkeyup="onlyFloat(this);" onchange="findUtilizedVolCft();" />Cbm</td>
						    <td></td>
						</tr>
						
						<tr></tr><tr>
							<td></td>
							<td align="right" class="listwhitetext">Available Volume</td>
							<td class="listwhitetext"><s:textfield name="storageLibrary.availVolumeCft"  maxlength="10" readonly="true" cssClass="input-textUpper" tabindex="9" cssStyle="width:95px;text-align:right" onkeyup="onlyFloat(this);" onchange="findAvailVolumeCbm();"/>Cft&nbsp;&nbsp;&nbsp;&nbsp;	
							<s:textfield name="storageLibrary.availVolumeCbm" maxlength="10" readonly="true" cssClass="input-textUpper" tabindex="10" cssStyle="width:95px;text-align:right" onkeyup="onlyFloat(this);" onchange="findAvailVolumeCft();" />Cbm</td>
						    <td></td>
						</tr>
						<tr>
                        <td></td>
                        <td align="right" class="listwhitetext">Owner</td>
						<td><s:select name="storageLibrary.owner" list="%{storageOwner}" headerKey=""  headerValue="" cssStyle="width:252px" cssClass="list-menu" tabindex="10"/>				
						</td> 					
						</tr>
						<tr><c:set var="isStorageLocked" value="false"/>
								<c:if test="${storageLibrary.storageLocked}">
									<c:set var="isStorageLocked" value="true"/>
				                </c:if>
				       </tr>
				       <tr>
							<td></td>
							<td align="right" class="listwhitetext">Storage Locked</td>
							<td class="listwhitetext"><s:checkbox name="storageLibrary.storageLocked" value="${isStorageLocked}" fieldValue="true" tabindex="11"/></td>
						    <td></td>
						</tr>
						<tr><c:set var="isStorageAssigned" value="false"/>
								<c:if test="${storageLibrary.storageAssigned}">
									<c:set var="isStorageAssigned" value="true"/>
				                </c:if>
				         </tr><tr>
							<td></td>
							<td align="right" class="listwhitetext">Storage Assigned</td>
							<td class="listwhitetext"><s:checkbox name="storageLibrary.storageAssigned" value="${isStorageAssigned}" fieldValue="true" tabindex="12"/></td>
						    <td></td>
						</tr>
						<tr> <td height="15px"></td></tr>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<table>
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>			
		<tr><fmt:formatDate var="serviceCreatedOnFormattedValue" value="${storageLibrary.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='storage.createdOn' /></b></td>
				<s:hidden name="storageLibrary.createdOn" value="${serviceCreatedOnFormattedValue}"/>
				<td style="font-size:.90em ;width:130px"><fmt:formatDate value="${storageLibrary.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message 	key='storage.createdBy' /></b></td>
				<c:if test="${not empty storageLibrary.id}">
					<s:hidden name="storageLibrary.createdBy" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="%{storageLibrary.createdBy}" /></td>
				</c:if>
				<c:if test="${empty storageLibrary.id}">
					<s:hidden name="storageLibrary.createdBy"
						value="${pageContext.request.remoteUser}" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="${pageContext.request.remoteUser}" /></td>
				</c:if>

				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message
					key='storage.updatedOn' /></b></td>
					<fmt:formatDate var="serviceUpdatedOnFormattedValue" value="${storageLibrary.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="storageLibrary.updatedOn" value="${serviceUpdatedOnFormattedValue}" />
				<td style="font-size:.90em; width:130px"><fmt:formatDate value="${storageLibrary.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message	key='storage.updatedBy' /></b></td>
				<c:if test="${not empty storageLibrary.id}"> 
                <s:hidden name="storageLibrary.updatedBy"/>

				<td style="width:85px ; font-size:.90em"><s:label name="updatedBy" value="%{storageLibrary.updatedBy}"/></td>
				
				</c:if>
				
				<c:if test="${empty storageLibrary.id}">
				
				<s:hidden name="storageLibrary.updatedBy" value="${pageContext.request.remoteUser}"/>
				
				<td style="width:85px ; font-size:.90em"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				
				</c:if> 
			</tr>
		</tbody>
	</table>


</div>

	   <s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:75px; height:25px" tabindex="5" onclick="return checkField();"/>
	   <s:submit cssClass="cssbutton" method="cancel" key="button.cancel" cssStyle="width:75px; height:25px" tabindex="6"/>
	   <s:reset cssClass="cssbutton" key="button.reset" cssStyle="width:75px; height:25px"/>
	
</s:form>
</body>