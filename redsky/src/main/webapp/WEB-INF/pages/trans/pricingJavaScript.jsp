<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ include file="/common/taglibs.jsp"%>

 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript">
	var isLoadPricing='';
	var idList='';
	var clickType1='';
	var inactiveCheck='';
	var http = getHTTPObject();
	var httpTemplate = getHTTPObject();
	var httpFindLine = getHTTPObject();
</script>

<script type="text/javascript">
function setFieldValue(id,result){
	document.getElementById(id).value = result;
}

function loadPricing(){
	if('${serviceOrder.id}' == '' || '${serviceOrder.id}' == null){
		alert('Please save Quotes first before access pricing.');
		return false;
	}
	if(isLoadPricing ==''){
		findAllPricing();
		isLoadPricing = 'YES';
	}
}
function findExchangeRateRateOnActualization(currency){
	 var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
			if('${entry.key}'==currency.trim()){
				rec='${entry.value}';
			}
		</c:forEach>
		return rec;
}
function currentDateRateOnActualization(){
	 var mydate=new Date();
  var daym;
  var year=mydate.getFullYear()
  var y=""+year;
  if (year < 1000)
  year+=1900
  var day=mydate.getDay()
  var month=mydate.getMonth()+1
  if(month == 1)month="Jan";
  if(month == 2)month="Feb";
  if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}
function rateOnActualizationDate(field1,field2,field3){
	var fXRateOnActualizationDate=document.forms['serviceOrderForm'].elements['billing.fXRateOnActualizationDate'].value;
	<c:if test="${multiCurrency=='Y'}">
	if(fXRateOnActualizationDate!=null && fXRateOnActualizationDate!=undefined && fXRateOnActualizationDate !='' && (fXRateOnActualizationDate ==true || fXRateOnActualizationDate=='true')){
    <c:if test="${contractType}">	
	var currency=""; 
		var currentDate=currentDateRateOnActualization();
		try{
		currency=document.getElementById(field1.trim().split('~')[0]).value;
		document.getElementById(field2.trim().split('~')[0]).value=currentDate;
		document.getElementById(field3.trim().split('~')[0]).value=findExchangeRateRateOnActualization(currency);
		}catch(e){}
		try{
		currency=document.getElementById(field1.trim().split('~')[1]).value;
		document.getElementById(field2.trim().split('~')[1]).value=currentDate;
		document.getElementById(field3.trim().split('~')[1]).value=findExchangeRateRateOnActualization(currency);
		}catch(e){}		
		</c:if>
	}
	</c:if>
}
function findAllPricing(){
    showOrHide(1);
	var url="findAllPricingAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
	http.open("POST", url, true);
	http.onreadystatechange = handleHttpResponseForPricing;
	http.send(null);
}

function handleHttpResponseForPricing(){
	if (http.readyState == 4){
		showOrHide(0);
        var results= http.responseText; 
 	    var pricingDiv = document.getElementById("pricingAjax");
 	    pricingDiv.innerHTML = results;
 	    
 	   accountIdCheck();
 	  findAllPricingLineId1();
 	 pricingOverFlow();
 	addValidation();
 	}
}
function addValidation(){
var ChargesMandatory='${checkContractChargesMandatory}';
if(ChargesMandatory=='1'){
$('#accountLineList th:nth-child(4)').html('Charge Code<font size="2" color="red">*</font>');
}
}
function vatDescrEnable(){
		var aidListIds=document.getElementById('aidListIds').value;		
	var accArr=aidListIds.split(",");
	for(var g=0;g<accArr.length;g++){
		  var vatExclude=document.getElementById('VATExclude'+accArr[g]).value;					 
		  try{
		  if(vatExclude=='true' || vatExclude==true){
			  document.getElementById('payVatDescr'+accArr[g]).value="";
			  document.getElementById('recVatDescr'+accArr[g]).value="";			  		  
			  document.getElementById('payVatDescr'+accArr[g]).disabled=true;
			  document.getElementById('recVatDescr'+accArr[g]).disabled=true;
			document.getElementById('vatAmt'+accArr[g]).value=0;			
			document.getElementById('estExpVatAmt'+accArr[g]).value=0;			
		  }else{
			  document.getElementById('payVatDescr'+accArr[g]).disabled=false;
			  document.getElementById('recVatDescr'+accArr[g]).disabled=false;
		  }
		  }catch(e){}
	}
}

function findAllPricingLineId1(){
	  var url="findAllPricingLineAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}";
	  httpFindLine.open("POST", url, true);
	  httpFindLine.onreadystatechange = function(){handleHttpResponseForGetLineAjax1()};
	  httpFindLine.send(null);
}

function handleHttpResponseForGetLineAjax1(){
	  if (httpFindLine.readyState == 4){
		  var results = httpFindLine.responseText;
        results = results.trim();
        idList = results;
        if(idList.trim() == ''){
        	document.getElementById("estimatedTotalExpense${serviceOrder.id}").value = 0.0;
        	document.getElementById("estimatedTotalRevenue${serviceOrder.id}").value = 0.0;
        	document.getElementById("estimatedGrossMargin${serviceOrder.id}").value = 0.0;
        	document.getElementById("estimatedGrossMarginPercentage${serviceOrder.id}").value = 0.0;
        	return;
        }
        var id = idList.split(",");
        var statusFlag='';
		  for (i=0;i<id.length;i++){
			  if(!document.getElementById('statusCheck'+id[i].trim()).checked){
				  document.getElementById('Inactivate').disabled = false;
				  statusFlag='YES';
				  return true;
			  }
		  }
		  if(statusFlag != 'YES'){
			  document.getElementById('Inactivate').disabled = true;
		  }
		  vatDescrEnable();		  
        //alert(idList);
	  }
}

function selectAll(str){
	var id = idList.split(",");
	  for (i=0;i<id.length;i++){
		  if(str.checked == true){
			  document.getElementById('displayOnQuote'+id[i].trim()).checked = true;
		  }else{
			  document.getElementById('displayOnQuote'+id[i].trim()).checked = false;
		  }
	  }
}

function selectAdd(str){
	var id = idList.split(",");
	  for (i=0;i<id.length;i++){
		  if(str.checked == true){
			  document.getElementById('additionalService'+id[i].trim()).checked = true;
		  }else{
			  document.getElementById('additionalService'+id[i].trim()).checked = false;
		  }
	  }
}
function selectAllRollInvoice(str){
	var id = idList.split(",");
	  for (i=0;i<id.length;i++){
		  if(str.checked == true){
			  document.getElementById('rollUpInInvoice'+id[i].trim()).checked = true;
		  }else{
			  document.getElementById('rollUpInInvoice'+id[i].trim()).checked = false;
		  }
	  }
}

function selectAllAct(str){
	var id = idList.split(",");
	  for (i=0;i<id.length;i++){
		  if(str.checked == true){
			  document.getElementById('statusCheck'+id[i].trim()).checked = true;
			  document.getElementById('Inactivate').disabled = true;
		  }else{
			  document.getElementById('statusCheck'+id[i].trim()).checked = false;
			  document.getElementById('Inactivate').disabled = false;
		  }
	  }
}

function getTotal(){
	var id = idList.split(",");
	var totalExp = 0.00;
	var totalRev = 0.00;
	for(i = 0; i < id.length; i++){
		var exp = document.getElementById('estimateExpense'+id[i]).value;
		var rev = document.getElementById('estimateRevenueAmount'+id[i]).value;
		if(exp.length > 0){
			totalExp = totalExp + parseFloat(exp);
			totalRev = totalRev + parseFloat(rev);
		}
	}
	document.getElementById('estimatedTotalExpense${serviceOrder.id}').value = Math.round(totalExp*10000)/10000;
	document.getElementById('estimatedTotalRevenue${serviceOrder.id}').value = Math.round(totalRev*10000)/10000;;
}

function addPricingLineAjax(){
	showOrHide(1);
	var url="addPricingLineAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
	http.open("POST", url, true);
	http.onreadystatechange = handleHttpResponseForAddPricingLine;
	http.send(null);
}
function handleHttpResponseForAddPricingLine(){
	if (http.readyState == 4){
		findAllPricing();
 	}
}

function addDefaultPricingLineAjax(str,str1){
	  if((str == 'AddLine' || str1 == 'AddTemplate') && idList.length == 0){
		  clickType1="ADD";
		  }	
	 var check =saveValidation(str);
	  if(check){
	var customerFileContract=document.forms['serviceOrderForm'].elements['serviceOrder.contract'].value;
	if(customerFileContract==''){
	 	alert("Please select  Pricing Contract from Quotes File");
	 	return false;
	}
	
	var jobtypeSO = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	var routingSO = document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
	var modeSO = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
	var packingModeSO = document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
	var commoditySO = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
	var serviceTypeSO = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value;
	var companyDivisionSO = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
	 var reloServiceType=document.forms['serviceOrderForm'].elements['reloServiceType'].value;
		while(reloServiceType.indexOf("#")>-1){
			reloServiceType=reloServiceType.replace('#',',');
		}

	var url1 = '&jobtypeSO='+jobtypeSO+'&routingSO='+routingSO+'&modeSO='+modeSO+'&packingModeSO='+packingModeSO+'&commoditySO='+commoditySO+'&reloServiceType='+reloServiceType;
	url1 = url1+'&serviceTypeSO='+serviceTypeSO+'&companyDivisionSO='+companyDivisionSO;
	//alert(url1)
	//var url1 = getTemplateParam();
	showOrHide(1);
	
	var url="findDefaultPricingLineAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}"+url1;
	httpTemplate.open("POST", url, true);
	httpTemplate.onreadystatechange = handleHttpResponseForAddTemplate;
	httpTemplate.send(null);
	  }
}
function handleHttpResponseForAddTemplate(){
	if (httpTemplate.readyState == 4){
		showOrHide(0);
		 var results = httpTemplate.responseText;
	     results = results.trim();
	     if(results == 'NO'){
	    	 alert('No Default Template Found');
	    	 return false;
	     }else{
	    	findAllPricingLineId('AddTemplate');
	     }
 	}
}

function handleHttpResponseForPricingLine(){
	if (http.readyState == 4){
		findAllPricing();
 	}
}

function toggle_visibility(id) {
    var e = document.getElementById(id);
    if(e != null && e != undefined){
    	if(e.style.display == 'block')
    	   e.style.display = 'none';
    	else
    	   e.style.display = 'block';
    }
}

function showOrHide123(action) { 
	document.getElementById("overlay").style.visibility = action;
} 
</script>

<script type="text/javascript">

function getValueFromId(id) {
	return document.getElementById(id).value;
}

var accountLineIdScript=""; 
function chk(chargeCode,category,aid){
	  accountLineIdScript=aid;
		var val = document.getElementById(category).value;
		var quotationContract = document.forms['serviceOrderForm'].elements['serviceOrder.contract'].value; 
		var originCountry=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
		var destinationCountry=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
		var mode;		
		var accountCompanyDivision = document.getElementById('companyDivision'+aid).value;
		if(document.forms['serviceOrderForm'].elements['serviceOrder.mode']!=undefined){
			 mode=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value; }else{mode="";}
		
			recGlVal = 'secondDescription';
			payGlVal = 'thirdDescription'; 
			var estimateContractCurrency='tenthDescription';
			var estimatePayableContractCurrency='fourthDescription';
			<c:if test="${contractType}">  
				estimateContractCurrency='contractCurrencyNew'+aid;
				estimatePayableContractCurrency='payableContractCurrencyNew'+aid;
			</c:if>
		if(val=='Internal Cost'){
			quotationContract="Internal Cost Management";
			javascript:openWindow('internalCostChargess.html?accountCompanyDivision='+accountCompanyDivision+'&originCountry='+originCountry+'&destinationCountry='+destinationCountry+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=quoteDescription'+aid+'&fld_eigthDescription=eigthDescription&fld_ninthDescription=ninthDescription&fld_tenthDescription='+estimateContractCurrency+'&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription='+estimatePayableContractCurrency+'&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code='+chargeCode+'&quotescharges=true');
			document.getElementById(chargeCode).select();
		}
		else {
		if(quotationContract=='' || quotationContract==' '){
			//alert("There is no pricing contract in quotes: Please select.");
			alert("The Pricing Contract is not selected in the Quotes Level.Please select the contract.")
		}
		else{
			javascript:openWindow('pricingCharge.html?contract='+quotationContract+'&originCountry='+originCountry+'&destinationCountry='+destinationCountry+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=quoteDescription'+aid+'&fld_eigthDescription=eigthDescription&fld_ninthDescription=ninthDescription&fld_tenthDescription='+estimateContractCurrency+'&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription='+estimatePayableContractCurrency+'&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code='+chargeCode+'&categoryType='+val);
		}
	}
		document.getElementById(chargeCode).focus();
}

function accountIdCheck(){
    var accountIdCheck = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
    accountIdCheck=accountIdCheck.trim(); 
    var inactive = document.getElementById('Inactivate');
    var selectallActive = document.getElementById('selectallActive');
    
    if(accountIdCheck == '' || accountIdCheck == ','){
    	if(inactive != null)
    		inactive.disabled = true; 
    	if(selectallActive != null)
    		selectallActive.checked=true;
    }else if(accountIdCheck != ''){
    	if(inactive != null)
    		inactive.disabled = false; 
    	if(selectallActive != null)
    		selectallActive.checked=false;
    }
}

function calculateRevenue(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid,field1,field2,field3) {
	 if(field1!='' && field2!='' & field3!=''){
		 rateOnActualizationDate(field1,field2,field3);
		 }	   
	   var revenueValue=0;
	    var revenueRound=0;
	    var oldRevenue=0;
	    var oldRevenueSum=0;
	    var balanceRevenue=0;
	    try{
		 <c:if test="${contractType}">
			var estimateSellQuantity=document.getElementById('estimateSellQuantity'+aid).value;
			 if(estimateSellQuantity=='' ||estimateSellQuantity=='0' ||estimateSellQuantity=='0.0' ||estimateSellQuantity=='0.00') {
				 document.getElementById('estimateSellQuantity'+aid).value=1;
		         }			 
		</c:if>	    
		}catch(e){}
	    oldRevenue=eval(getValueFromId(revenue));
	    <c:if test="${not empty serviceOrder.id}">
	    oldRevenueSum=eval(getValueFromId('estimatedTotalRevenue${serviceOrder.id}'));
	    </c:if>
	    var basisValue = getValueFromId(basis);
	    checkFloatNew('serviceOrderForm',quantity,'Nothing');
	    var quantityValue = getValueFromId(quantity);
	  <c:if test="${contractType}">
	  checkFloatNew('serviceOrderForm','estimateSellQuantity'+aid,'Nothing');
	  quantityValue=getValueFromId('estimateSellQuantity'+aid);
	  </c:if>
	    checkFloatNew('serviceOrderForm',sellRate,'Nothing');
	    var sellRateValue = getValueFromId(sellRate);
	    if(basisValue=="cwt" || basisValue== "%age"){
	    revenueValue=(quantityValue*sellRateValue)/100 ;
	    }
	    else if(basisValue=="per 1000"){
	    revenueValue=(quantityValue*sellRateValue)/1000 ;
	    }else{
	    revenueValue=(quantityValue*sellRateValue);
	    } 
	    revenueRound=Math.round(revenueValue*10000)/10000;
	    balanceRevenue=revenueRound-oldRevenue;
	    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
	    balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
	    oldRevenueSum=oldRevenueSum+balanceRevenue; 
	    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
	    var revenueRoundNew=""+revenueRound;
	    if(revenueRoundNew.indexOf(".") == -1){
	    revenueRoundNew=revenueRoundNew+".00";
	    } 
	    while((revenueRoundNew.indexOf(".")+5 != revenueRoundNew.length)){
	    	revenueRoundNew=revenueRoundNew+"0";
	    } 
	    document.getElementById(revenue).value=revenueRoundNew;
	    try{
	        var buyRate1=document.getElementById('estimateSellRate'+aid).value;
	        var estCurrency1= document.getElementById('estSellCurrencyNew'+aid).value;
			if(estCurrency1.trim()!=""){
					var Q1=0;
					var Q2=0;
					Q1=buyRate1;
					Q2=getValueFromId('estSellExchangeRateNew'+aid);
		            var E1=Q1*Q2;
		            E1=Math.round(E1*10000)/10000;
		            document.getElementById('estSellLocalRateNew'+aid).value = E1;
		            //document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=E1;
		            var estLocalAmt1=0;
		            if(basisValue=="cwt" || basisValue== "%age"){
		            	estLocalAmt1=(quantityValue*E1)/100 ;
		                }
		                else if(basisValue=="per 1000"){
		                	estLocalAmt1=(quantityValue*E1)/1000 ;
		                }else{
		                	estLocalAmt1=(quantityValue*E1);
		                }	 
		            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;   
		            document.getElementById('estSellLocalAmountNew'+aid).value=estLocalAmt1;
		            //document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estLocalAmt1;     
			}
			<c:if test="${contractType}">
				try{
				     var estimatePayableContractExchangeRate=getValueFromId('estimateContractExchangeRateNew'+aid); //document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value;
				     var estLocalRate= getValueFromId('estSellLocalRateNew'+aid); //document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value;
				     var estExchangeRate=getValueFromId('estSellExchangeRateNew'+aid); //document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value
				     var recCurrencyRate=(estimatePayableContractExchangeRate*estLocalRate)/estExchangeRate;
				     var roundValue=Math.round(recCurrencyRate*10000)/10000;
				     document.getElementById('estimateContractRateNew'+aid).value = roundValue;
				     //document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value=roundValue ;
					
					var Q11=0;
					var Q21=0;
					Q11=getValueFromId(quantity); //document.forms['serviceOrderForm'].elements[quantity].value;
					  <c:if test="${contractType}">
					  Q11=getValueFromId('estimateSellQuantity'+aid);
					  </c:if>
					Q21=getValueFromId('estimateContractRateNew'+aid); //document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
		            var E11=Q11*Q21;
		            var estLocalAmt11=0;
		            if(basisValue=="cwt" || basisValue== "%age"){
		            	estLocalAmt11=E11/100 ;
		                }
		                else if(basisValue=="per 1000"){
		                	estLocalAmt11=E11/1000 ;
		                }else{
		                	estLocalAmt11=E11;
		                }	 
		            estLocalAmt11=Math.round(estLocalAmt11*10000)/10000; 
		            document.getElementById('estimateContractRateAmmountNew'+aid).value=estLocalAmt11;
				}catch(e){}     
			</c:if>
	    }catch(e){}
	    <c:if test="${not empty serviceOrder.id}">
	    var newRevenueSum=""+oldRevenueSum;
	    if(newRevenueSum.indexOf(".") == -1){
	    newRevenueSum=newRevenueSum+".00";
	    } 
	    if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
	    newRevenueSum=newRevenueSum+"0";
	    } 
	    //totalRev();
	    document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value = newRevenueSum;
	    //document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
	    </c:if>
	    if(revenueRoundNew>0){
	    	document.getElementById(displayOnQuots).checked = true;
	    }
	    try{
	    <c:if test="${systemDefaultVatCalculationNew=='Y'}">	    
	    calculateVatAmt('REVENUE',aid);
	    </c:if>
	    }catch(e){}
	    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage);
	 }
	 
function calculateestimatePassPercentage(revenue,expense,estimatePassPercentage){
    var revenueValue=0;
    var expenseValue=0;
    var estimatePassPercentageValue=0; 
    checkFloatNew('serviceOrderForm',estimatePassPercentage,'Nothing'); 
    revenueValue=eval(getValueFromId(revenue));
    expenseValue=eval(getValueFromId(expense)); 
    try{
    	estimatePassPercentageValue=(revenueValue/expenseValue)*100;
    }catch(e){
    	estimatePassPercentageValue=0.00;
        }    
    estimatePassPercentageValue=Math.round(estimatePassPercentageValue);
    
    if(getValueFromId(expense) == '' || getValueFromId(expense) == 0 || getValueFromId(expense) == 0.00){
    	document.getElementById(estimatePassPercentage).value = '';
  	   }else{ 
  		 document.getElementById(estimatePassPercentage).value = estimatePassPercentageValue;
  	   } 
    calculateGrossMargin();
    }

function calculateGrossMargin(){
    <c:if test="${not empty serviceOrder.id}">
    var revenueValue=0;
    var expenseValue=0;
    var grossMarginValue=0; 
    var grossMarginPercentageValue=0; 
    revenueValue=eval(getValueFromId('estimatedTotalRevenue'+${serviceOrder.id}));
    expenseValue=eval(getValueFromId('estimatedTotalExpense'+${serviceOrder.id})); 
    grossMarginValue=revenueValue-expenseValue;
    grossMarginValue=Math.round(grossMarginValue*10000)/10000; 
    document.getElementById('estimatedGrossMargin'+${serviceOrder.id}).value = grossMarginValue; 
    if(getValueFromId('estimatedTotalRevenue'+${serviceOrder.id}) == '' || getValueFromId('estimatedTotalRevenue'+${serviceOrder.id}) == 0 || getValueFromId('estimatedTotalRevenue'+${serviceOrder.id}) == 0.00){
    	document.getElementById('estimatedGrossMarginPercentage'+${serviceOrder.id}).value = '';
  	   }else{
  	     grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
  	     grossMarginPercentageValue=Math.round(grossMarginPercentageValue*10000)/10000;
  	   document.getElementById('estimatedGrossMarginPercentage'+${serviceOrder.id}).value = grossMarginPercentageValue;
  	   }
  	</c:if>    
    }
    
function calculateExpense(basis,quantity,buyRate,expense,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid){
	  var expenseValue=0;
	    var expenseRound=0;
	    var oldExpense=0;
	    var oldExpenseSum=0;
	    var balanceExpense=0;
	    oldExpense=eval(getValueFromId(expense));
	    <c:if test="${not empty serviceOrder.id}">
	    oldExpenseSum=eval(getValueFromId('estimatedTotalExpense'+${serviceOrder.id}));
	    </c:if>
	    var basisValue = getValueFromId(basis);
	    checkFloatNew('serviceOrderForm',quantity,'Nothing');    
	    var quantityValue = getValueFromId(quantity);
	    checkFloatNew('serviceOrderForm',buyRate,'Nothing');    
	    var buyRateValue = getValueFromId(buyRate);
	    if(basisValue=="cwt" || basisValue== "%age"){
	    expenseValue=(quantityValue*buyRateValue)/100 ;
	    }
	    else if(basisValue=="per 1000"){
	    expenseValue=(quantityValue*buyRateValue)/1000 ;
	    }else{
	    expenseValue=(quantityValue*buyRateValue);
	    }
	    expenseRound=Math.round(expenseValue*10000)/10000;
	    balanceExpense=expenseRound-oldExpense;
	    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
	    balanceExpense=Math.round(balanceExpense*10000)/10000; 
	    oldExpenseSum=oldExpenseSum+balanceExpense;
	    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
	    var expenseRoundNew=""+expenseRound;
	    if(expenseRoundNew.indexOf(".") == -1){
	    expenseRoundNew=expenseRoundNew+".00";
	    } 
	    while((expenseRoundNew.indexOf(".")+5 != expenseRoundNew.length)){
	    	expenseRoundNew=expenseRoundNew+"0";
	    } 
	    document.getElementById(expense).value = expenseRoundNew;
	    try{
	        var buyRate1= getValueFromId('estimateRate'+aid);
	        var estCurrency1= getValueFromId('estCurrencyNew'+aid);
			if(estCurrency1.trim()!=""){
					var Q1=0;
					var Q2=0;
					Q1=buyRate1;
					Q2= getValueFromId('estExchangeRateNew'+aid);
		            var E1=Q1*Q2;
		            E1=Math.round(E1*10000)/10000;
		            document.getElementById('estLocalRateNew'+aid).value = E1;
		            var estLocalAmt1=0;
		            if(basisValue=="cwt" || basisValue== "%age"){
		            	estLocalAmt1=(quantityValue*E1)/100 ;
		                }
		                else if(basisValue=="per 1000"){
		                	estLocalAmt1=(quantityValue*E1)/1000 ;
		                }else{
		                	estLocalAmt1=(quantityValue*E1);
		                }	 
		            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
		            document.getElementById('estLocalAmountNew'+aid).value=estLocalAmt1;     
			}
			<c:if test="${contractType}">
			try{
			     var estimatePayableContractExchangeRate= getValueFromId('estimatePayableContractExchangeRateNew'+aid); //document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
			     var estLocalRate= getValueFromId('estLocalRateNew'+aid); //document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value;
			     var estExchangeRate= getValueFromId('estExchangeRateNew'+aid); //document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value
			     var recCurrencyRate=(estimatePayableContractExchangeRate*estLocalRate)/estExchangeRate;
			     var roundValue=Math.round(recCurrencyRate*10000)/10000;
			     document.getElementById('estimatePayableContractRateNew'+aid).value =roundValue ;
				
				var Q11=0;
				var Q21=0;
				Q11= getValueFromId(quantity); //document.forms['serviceOrderForm'].elements[quantity].value;
				Q21= getValueFromId('estimatePayableContractRateNew'+aid); //document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
	            var E11=Q11*Q21;
	            var estLocalAmt11=0;
	            if(basisValue=="cwt" || basisValue== "%age"){
	            	estLocalAmt11=E11/100 ;
	                }
	                else if(basisValue=="per 1000"){
	                	estLocalAmt11=E11/1000 ;
	                }else{
	                	estLocalAmt11=E11;
	                }	 
	            estLocalAmt11=Math.round(estLocalAmt11*10000)/10000; 
	            document.getElementById('estimatePayableContractRateAmmountNew'+aid).value =estLocalAmt11;
			}catch(e){}     
		</c:if>
			
	    }catch(e){}
	    <c:if test="${not empty serviceOrder.id}">
	    var newExpenseSum=""+oldExpenseSum;
	    if(newExpenseSum.indexOf(".") == -1){
	    newExpenseSum=newExpenseSum+".00";
	    } 
	    if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
	    newExpenseSum=newExpenseSum+"0";
	    }
	   <%-- document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value =newExpenseSum;  --%>
	    getTotal();
	    </c:if>
	    if(expenseRoundNew>0){
	    	document.getElementById(displayOnQuots).checked = true;
	    }
	    try{
	        <c:if test="${systemDefaultVatCalculationNew=='Y'}">
	        calculateVatAmt('EXPENSE',aid);
	        </c:if>
	        }catch(e){}
	    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage);
  }
function changeVatAmt(type,aid){
	  var vatExclude=document.getElementById('VATExclude'+aid).value;
	  if(vatExclude==null || vatExclude==false || vatExclude=='false'){
		  document.getElementById('recVatDescr'+aid).disabled = false;
		  document.getElementById('payVatDescr'+aid).disabled = false;

  if(type=='REVENUE'){
    	 <c:forEach var="entry" items="${estVatPersentList}">
            if(document.getElementById('recVatDescr'+aid).value == '${entry.key}') {
            	document.getElementById('recVatPercent'+aid).value='${entry.value}'; 
			    calculateVatAmt(type,aid);
        	}
    	</c:forEach>
      		if(document.getElementById('recVatDescr'+aid).value == ''){
      			document.getElementById('recVatPercent'+aid).value=0;
			    calculateVatAmt(type,aid);
      		}   
    }else{
       	 <c:forEach var="entry" items="${payVatPercentList}">
         if(document.getElementById('payVatDescr'+aid).value == '${entry.key}') {
         	document.getElementById('payVatPercent'+aid).value='${entry.value}'; 
			    calculateVatAmt(type,aid);
     	}
 	</c:forEach>
   		if(document.getElementById('payVatDescr'+aid).value == ''){
   			document.getElementById('payVatPercent'+aid).value=0;
			    calculateVatAmt(type,aid);
   		}      
    }
	  }else{
		  document.getElementById('payVatDescr'+aid).value="";
		  document.getElementById('recVatDescr'+aid).value="";		  	  
		  document.getElementById('recVatDescr'+aid).disabled = true;
		  document.getElementById('payVatDescr'+aid).disabled = true;
			document.getElementById('vatAmt'+aid).value=0;			
			document.getElementById('estExpVatAmt'+aid).value=0;			
	  }    
      		       
}

function calculateVatAmt(type,aid){	
	  var vatExclude=document.getElementById('VATExclude'+aid).value;
	  if(vatExclude==null || vatExclude==false || vatExclude=='false'){
		  document.getElementById('recVatDescr'+aid).disabled = false;
		  document.getElementById('payVatDescr'+aid).disabled = false;
	
	var estVatAmt=0;
	 if(type=='REVENUE'){
		 var estimateRevenue= eval(document.getElementById('estimateRevenueAmount'+aid).value);
		 <c:if test="${contractType}">
		 estimateRevenue= eval(document.getElementById('estSellLocalAmountNew'+aid).value);
		 </c:if>
		 var recVatPercent=0.00;
		 if(document.getElementById('recVatPercent'+aid).value!=''){
			 recVatPercent=document.getElementById('recVatPercent'+aid).value;
		 }
		 if(recVatPercent != ''){
			 estVatAmt=(estimateRevenue*recVatPercent)/100;
			 estVatAmt=Math.round(estVatAmt*10000)/10000;
			 document.getElementById('vatAmt'+aid).value=estVatAmt; 
		 }
	 }else{
		 
		 var estimateExpense= eval(document.getElementById('estimateExpense'+aid).value);
		 <c:if test="${contractType}">
		 estimateExpense= eval(document.getElementById('estLocalAmountNew'+aid).value);
		 </c:if>
		 var payVatPercent=0.00;
		 if(document.getElementById('payVatPercent'+aid).value!=''){
			 payVatPercent=document.getElementById('payVatPercent'+aid).value;
		 }
		 if(payVatPercent != ''){
			 estVatAmt=(estimateExpense*payVatPercent)/100;
			 estVatAmt=Math.round(estVatAmt*10000)/10000;
			 document.getElementById('estExpVatAmt'+aid).value=estVatAmt; 
		 }
	 }	
	  }else{
		  document.getElementById('payVatDescr'+aid).value="";
		  document.getElementById('recVatDescr'+aid).value="";		  		  	  
		  document.getElementById('recVatDescr'+aid).disabled = true;
		  document.getElementById('payVatDescr'+aid).disabled = true;	
			document.getElementById('vatAmt'+aid).value=0;			
			document.getElementById('estExpVatAmt'+aid).value=0;			
	  }    
	     
}

function changeBasis(basis,quantity,buyRate,expense,sellRate,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid){
	  var quantityValue = getValueFromId(quantity);// document.forms['serviceOrderForm'].elements[quantity].value;
	  var basisValue = getValueFromId(basis);//document.forms['serviceOrderForm'].elements[basis].value; 
	  var oldBasis="old"+basis;
	  var oldQuantity="old"+quantity;
	  var oldQuantityValue = getValueFromId(oldQuantity); //document.forms['serviceOrderForm'].elements[oldQuantity].value;
	  var oldBasisValue = getValueFromId(oldBasis); //document.forms['serviceOrderForm'].elements[oldBasis].value;  
	  if((document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value=="AIR")|| (document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value=="Air")){
	  if(basisValue=='cwt'){
		  document.getElementById(quantity).value = document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;
		  </c:if>
		  
	  }else if(basisValue=='kg'){
		  document.getElementById(quantity).value = document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value;
		  </c:if>
		  
	  }
	  else if(basisValue=='cft'){
		  document.getElementById(quantity).value =document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;
		  </c:if>
		  
	  }
	  else if(basisValue=='cbm'){
		  document.getElementById(quantity).value =document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value;
		  </c:if>
		  
	  }
	  else if(basisValue=='each'){
		  document.getElementById(quantity).value=1;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=1;
		  </c:if>
		  
	  }
	  else if(basisValue=='value'){
		  document.getElementById(quantity).value=1;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=1;
		  </c:if>
		  
	  }
	  else if(basisValue=='flat'){
		  document.getElementById(quantity).value=1;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=1;
		  </c:if>
		  
	  }
	  }
	  else{
	  if(basisValue=='cwt'){
		  document.getElementById(quantity).value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;
		  </c:if>
		  
	  }else if(basisValue=='kg'){
		  document.getElementById(quantity).value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value;
		  </c:if>
		  
	  }
	  else if(basisValue=='cft'){
		  document.getElementById(quantity).value=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value;
		  </c:if>
		  
	  }
	  else if(basisValue=='cbm'){
		  document.getElementById(quantity).value=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value;
		  </c:if>
		  
	  }
	  else if(basisValue=='each'){
		  document.getElementById(quantity).value=1;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=1;
		  </c:if>
		  
	  }
	  else if(basisValue=='value'){
		  document.getElementById(quantity).value=1;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=1;
		  </c:if>
		  
	  }
	  else if(basisValue=='flat'){
		  document.getElementById(quantity).value=1;
		  <c:if test="${contractType}">
		  document.getElementById('estimateSellQuantity'+aid).value=1;
		  </c:if>
	  }
	  }
	calculateExpense(basis,quantity,buyRate,expense,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid);
	calculateRevenue(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid,'','','');
	}
	
function changeEstimatePassPercentage(basis,quantity,buyRate,expense,sellRate,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt){
    var estimate=0; 
    var estimateround=0;
    var estimateRevenue=0;
    var estimateRevenueRound=0;
    var sellRateValue=0;
    var sellRateround=0;
    var	estimatepasspercentage =0;
    var quantityValue = 0;
    var oldRevenue=0;
    var oldRevenueSum=0;
    var balanceRevenue=0; 
    var buyRateValue=0;  
	   oldRevenue=eval(getValueFromId(revenue)); 
	    <c:if test="${not empty serviceOrder.id}">
     oldRevenueSum=eval(getValueFromId('estimatedTotalRevenue'+${serviceOrder.id}));
    </c:if> 
    quantityValue = eval(getValueFromId(quantity)); 
    buyRateValue = eval(getValueFromId(buyRate)); 
    estimatepasspercentage= getValueFromId(estimatePassPercentage);
    estimate = (quantityValue*buyRateValue);
    
	   if(quantityValue>0 && estimate>0){
	   if( getValueFromId(basis) == "cwt" || getValueFromId(basis) == "%age"){
    		estimate = estimate/100; 	  	
    	} 
    	if( getValueFromId(basis) =="per 1000"){
    		estimate = estimate/1000; 	  	
    	}
    	estimateRevenue=((estimate)*estimatepasspercentage/100)
    	estimateRevenueRound=Math.round(estimateRevenue*10000)/10000; 
    	document.getElementById(revenue).value = estimateRevenueRound;   
	    if( getValueFromId(basis) =="cwt" || getValueFromId(basis) =="%age"){
    	  sellRateValue=(estimateRevenue/quantityValue)*100;
    	  sellRateround=Math.round(sellRateValue*10000)/10000;
    	  document.getElementById(sellRate).value = sellRateround;	  	
    	}
	    else if( getValueFromId(basis) == "per 1000"){
    	  sellRateValue=(estimateRevenue/quantityValue)*1000;
    	  sellRateround=Math.round(sellRateValue*10000)/10000; 
    	  document.getElementById(sellRate).value = sellRateround;	  	
    	} else if(getValueFromId(basis) != "cwt" || getValueFromId(basis) != "%age" || getValueFromId(basis) != "per 1000")
     {
       sellRateValue=(estimateRevenue/quantityValue);
    	  sellRateround=Math.round(sellRateValue*10000)/10000; 
    	  document.getElementById(sellRate).value = sellRateround;	  	
	    }
	    balanceRevenue=estimateRevenueRound-oldRevenue;
     oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
     balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
     oldRevenueSum=oldRevenueSum+balanceRevenue;  
     <c:if test="${not empty serviceOrder.id}">
     var newRevenueSum=""+oldRevenueSum;
     if(newRevenueSum.indexOf(".") == -1){
     newRevenueSum=newRevenueSum+".00";
     } 
     if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
     newRevenueSum=newRevenueSum+"0";
     } 
     document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value = newRevenueSum;
     </c:if>
    if(estimateRevenueRound>0){
    	document.getElementById(displayOnQuots).checked = true;
    }
    try{
        <c:if test="${systemDefaultVatCalculationNew=='Y'}">
   	    var aid=basis.replace('basis' ,'');
	    aid=aid.trim();
	    calculateVatAmt('REVENUE',aid);
	    calculateVatAmt('EXPENSE',aid);
        </c:if>
        }catch(e){}
	 calculateGrossMargin();
}
else{
	document.getElementById(estimatePassPercentage).value=0;
}
}

function fillDiscription(discription,chargeCode,category,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate,payableContractCurrency,payableContractExchangeRate,payableContractValueDate,aid)
{
	var chargeCodeValidationVal=document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value;
	 if(chargeCodeValidationVal=='' || chargeCodeValidationVal==null){
	var category = getValueFromId(category);
	var quotationContract = document.forms['serviceOrderForm'].elements['serviceOrder.contract'].value;
	var chargeCode = getValueFromId(chargeCode);
	var discriptionValue = getValueFromId(discription);  
	var jobtype = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
    var routing = document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
    var companyDivision = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
    var sid='${serviceOrder.id}'; 
    showOrHide(1);
		var url = "findQuotationDiscription.html?ajax=1&contract="+quotationContract+"&chargeCode="+chargeCode+"&decorator=simple&popup=true&jobType="+jobtype+"&routing1="+routing+"&soId="+sid+"&companyDivision="+companyDivision;
     	http50.open("GET", url, true);
     	http50.onreadystatechange =  function(){fillDiscriptionResponse(discription,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate,payableContractCurrency,payableContractExchangeRate,payableContractValueDate,aid);};
     	http50.send(null);
	 }
		
}

function fillDiscriptionResponse(discription,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate,payableContractCurrency,payableContractExchangeRate,payableContractValueDate,aid)
{
   if (http50.readyState == 4){
	   showOrHide(0);
           var result= http50.responseText
           result = result.trim();
           result = result.replace('[','');
           result=result.replace(']','');
           var res = result.split("#"); 
           var discriptionValue = getValueFromId(discription); 
           <configByCorp:fieldVisibility componentId="component.field.Description.DefaultChargeCode"> 
	           if(result.indexOf('NoDiscription')<0) {
		           if(getValueFromId(discription).trim()==''){  
			           if(res[8]==''){   
		        	   document.getElementById(discription).value = res[0];
			           }else{
			        	   document.getElementById(discription).value = res[8];
			           }
		           } 
	           } 
           </configByCorp:fieldVisibility>
           <configByCorp:fieldVisibility componentId="component.field.Description.ChargeCode">
           if(res[8]==''){   
           		document.getElementById(discription).value = res[0];
           }else{
        	   document.getElementById(discription).value = res[8];
           }
           </configByCorp:fieldVisibility>
           <configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">		           
           if(res[5]=='Y'){
        	   		document.getElementById('rollUpInInvoice'+aid).checked=true;
	            }else{
	            	document.getElementById('rollUpInInvoice'+aid).checked=false;          
	            }
           </configByCorp:fieldVisibility>
        }
   		<c:if test="${costElementFlag}">
    	try{
            var ctype= document.forms['serviceOrderForm'].elements['category'+aid].value;
      		 ctype=ctype.trim();
      		if(ctype==''){
      			document.forms['serviceOrderForm'].elements['category'+aid].value = res[6];
      		}
    	}catch(e){}
    	</c:if>   
    	try{
            var VATExclude=res[7];
            VATExclude=VATExclude.trim();
      		if(VATExclude=='Y'){
      			document.getElementById('VATExclude'+aid).value = true;
      		  document.getElementById('payVatDescr'+aid).value="";
    		  document.getElementById('recVatDescr'+aid).value="";      			
      			document.getElementById('payVatDescr'+aid).disabled = true;
      			document.getElementById('recVatDescr'+aid).disabled = true;
			document.getElementById('vatAmt'+aid).value=0;			
			document.getElementById('estExpVatAmt'+aid).value=0;			

      			
      		}else{
      			document.getElementById('VATExclude'+aid).value = false;
      			document.getElementById('payVatDescr'+aid).disabled = false;
      			document.getElementById('recVatDescr'+aid).disabled = false;
      		}
    	}catch(e){}
    	
	}
	
function checkChargeCode(chargeCode,category,aid)
{
	var chargeCodeValidationVal=document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value;
	 if(chargeCodeValidationVal=='' || chargeCodeValidationVal==null){
	var charge= getValueFromId(chargeCode);
	if(charge.trim()!=""){
	 progressBarAutoSave('1');
    var val = getValueFromId(category);
    var jobType='${serviceOrder.job}'; 
    var routing='${serviceOrder.routing}'; 
    var sid='${serviceOrder.id}'; 
    var soCompanyDivision =	'${serviceOrder.companyDivision}';
    var accountCompanyDivision = document.getElementById('companyDivision'+aid).value;
	var quotationContract = document.forms['serviceOrderForm'].elements['serviceOrder.contract'].value;
	var myRegxp = /^[a-zA-Z0-9-_\'('&'\)']+$/;
	if(charge.match(myRegxp)==false){
	   alert( "Please enter a valid charge code." );
	   document.getElementById(chargeCode).value='';
       progressBarAutoSave('0');
	
	}
	else{
	if(val=='Internal Cost'){
	 quotationContract="Internal Cost Management";
	 var url="checkInternalCostChargeCode.html?ajax=1&decorator=simple&popup=true&accountCompanyDivision="+encodeURIComponent(accountCompanyDivision)+"&chargeCode="+encodeURIComponent(charge)+"&jobType="+encodeURIComponent(jobType)+"&routing="+encodeURIComponent(routing)+"&soId="+sid+"&soCompanyDivision="+encodeURIComponent(soCompanyDivision); 
	 http5.open("GET", url, true);
     http5.onreadystatechange =function(){handleHttpResponseChargeCode(chargeCode,category,aid);} ;
     http5.send(null);
	}
	else{
	if(quotationContract=='' || quotationContract==' ')
	{
	alert("There is no pricing contract in Quotation File: Please select.");
	document.getElementById(chargeCode).value='';
	 progressBarAutoSave('0');
	}
	else{
	 var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+quotationContract+"&chargeCode="+charge+"&jobType="+jobType+"&routing="+routing+"&soCompanyDivision="+soCompanyDivision; 
     http5.open("GET", url, true);
     http5.onreadystatechange = function(){handleHttpResponseChargeCode(chargeCode,category,aid);} ;
     http5.send(null);
	}
	}
	}
	}
	 }
}

function handleHttpResponseChargeCode(chargeCode,category,aid){
     if (http5.readyState == 4){
        var results = http5.responseText
        results = results.trim(); 
        var res = results.split("#"); 
        if(results.length>4){
        	<c:if test="${costElementFlag}">
        	try{
                var ctype= document.getElementById('category'+aid).value;
          		 ctype=ctype.trim();
          		if(ctype==''){
          			document.getElementById('category'+aid).value = res[12];
          		}
        	}catch(e){}
        	</c:if>            
        	populateCostElementData(aid);        	
        	<c:if test="${contractType}">  
        	document.getElementById('contractCurrencyNew'+aid).value = res[9];
        	document.getElementById('payableContractCurrencyNew'+aid).value=res[10];
            accountLineIdScript=aid;            
            checkChargesDataFromSO(); 
        	</c:if>
         }else{
        	 document.getElementById("ChargeNameDivId"+aid).style.display = "none";
        	 document.getElementById(chargeCode).focus();
            alert("Charge Code does not exist according to the Pricing Contract selected in the Quote. Please select another Charge Code." );
            document.getElementById(chargeCode).value = "";  
	   }
     }
     progressBarAutoSave('0');
}
function getHTTPObject77() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var populateCostElementDatahttp=getHTTPObject77(); 
function populateCostElementData(aid){
	 if(aid==''){
		 aid=accountLineIdScript;
	 }
	<c:if test="${costElementFlag}">
	var chargeCode= document.getElementById('chargeCode'+aid).value;
	var billingContract = document.forms['serviceOrderForm'].elements['billing.contract'].value;
	var sid='${serviceOrder.id}';
	var url="populateCostElementDataAjax.html?ajax=1&decorator=simple&popup=true&contract="+encodeURIComponent(billingContract)+"&chargeCode="+encodeURIComponent(chargeCode)+"&soId="+sid;
	populateCostElementDatahttp.open("GET", url, true);
	populateCostElementDatahttp.onreadystatechange = function(){handleHttpResponsePopulateCostElementData(aid);} ;
	populateCostElementDatahttp.send(null);
	</c:if>
}
function handleHttpResponsePopulateCostElementData(aid) { 
	if (populateCostElementDatahttp.readyState == 4) {
		var results = populateCostElementDatahttp.responseText
		results = results.trim();
	    if(results.length>1){
		    try{
	    	document.getElementById('accountLineCostElement'+aid).value=results.split("~")[0];
	    	document.getElementById('accountLineScostElementDescription'+aid).value=results.split("~")[1];
		    }catch(e){}
	    }
	    
    }
}
function findRevisedEstimateQuantitys(chargeCode, category, estimateExpense, basis, estimateQuantity, estimateRate, estimateSellRate, estimateRevenueAmount, estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
{
var charge = document.getElementById(chargeCode).value;
var billingContract = document.forms['serviceOrderForm'].elements['serviceOrder.contract'].value;
var shipNumber=document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
var rev=document.getElementById(category).value;
var comptetive = document.forms['serviceOrderForm'].elements['inComptetive'].value;
if(charge!=''){
if(rev == "Internal Cost"){
    if(charge=="VANPAY" ){
     	getVanPayCharges("Estimate");
    }else if(charge=="DOMCOMM" ){
     	getDomCommCharges("Estimate");
    }else if(charge=="SALESCM" && comptetive=="Y") { 
	    var totalExpense = eval(document.forms['serviceOrderForm'].elements['inEstimatedTotalExpense'].value);
	    var totalRevenue =eval(document.forms['serviceOrderForm'].elements['inEstimatedTotalRevenue'].value); 
	    var commisionRate =eval(document.forms['serviceOrderForm'].elements['salesCommisionRate'].value); 
	    var grossMargin = eval(document.forms['serviceOrderForm'].elements['grossMarginThreshold'].value);
	    var commision=((totalRevenue*commisionRate)/100);
	    var calGrossMargin=((totalRevenue-(totalExpense + commision))/totalRevenue);
	    calGrossMargin=calGrossMargin*100;
	    var calGrossMargin=Math.round(calGrossMargin);
	    if(calGrossMargin>=grossMargin)  {
	    	document.getElementById(estimateExpense).value=Math.round(commision*10000)/10000; 
	    	document.getElementById(basis).value='each';
	    	document.getElementById(estimateQuantity).value=1;
			  <c:if test="${contractType}">
			  document.getElementById('estimateSellQuantity'+aid).value=1;
			  </c:if>			    	
    	
	    } else { 
		    commision=((.8*totalRevenue)-totalExpense); 
		    commision=Math.round(commision*10000)/10000;
		    if(commision<0) { 
		    	document.getElementById(estimateExpense).value=0; 
		    	document.getElementById(basis).value='each';
		    	document.getElementById(estimateQuantity).value=1;
				  <c:if test="${contractType}">
				  document.getElementById('estimateSellQuantity'+aid).value=1;
				  </c:if>
		    } else {
		    	document.getElementById(estimateExpense).value=Math.round(commision*10000)/10000;
		    	document.getElementById(basis).value='each';
		    	document.getElementById(estimateQuantity).value=1;
				  <c:if test="${contractType}">
				  document.getElementById('estimateSellQuantity'+aid).value=1;
				  </c:if>
		    } 
	    }  
    }else if(charge=="SALESCM" && comptetive!="Y") {
    var agree =confirm("This job is not competitive; do you still want to calculate commission?");
    if(agree) {
    var accountCompanyDivision =  document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value 
    var url="internalCostsExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=${serviceOrder.id}";
    http88.open("GET", url, true);
    http88.onreadystatechange = function(){ handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate,basis);};
    http88.send(null);
    } else {
    return false;
    }
    } else {
    var accountCompanyDivision =  document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value 
    var url="internalCostsExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=${serviceOrder.id}";
    http88.open("GET", url, true);
    http88.onreadystatechange = function(){ handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate, basis);};
    http88.send(null);
    }
} else{	
	var url="invoiceExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=${serviceOrder.id}";
    http88.open("GET", url, true);
    http88.onreadystatechange = function(){ handleHttpResponse701(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote, deviation,estimateDeviation, estimateSellDeviation,aid);};
    http88.send(null);
	} 
   }else{
    alert("Please select chargeCode!");
   }
}

function handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate, basis) {
    if (http88.readyState == 4)  {
            var results = http88.responseText
            results = results.trim(); 
            var res = results.split("#");
            if(res[3]=="AskUser1"){
            var reply = prompt("Please Enter Quantity", "");
            if(reply)   {
            if((reply>0 || reply<9)|| reply=='.') {
         	    estimateQuantity.value = Math.round(reply*100)/100;
         	}  else  {
         		alert("Please Enter legitimate Quantity");
         	} }
         	 else{
         		document.getElementById(estimateQuantity).value = document.getElementById(estimateQuantity).value;
  			  <c:if test="${contractType}">
			  document.getElementById('estimateSellQuantity'+aid).value=document.getElementById('estimateSellQuantity'+aid).value;
			  </c:if>
         }  }
         else{
         if(res[1] == undefined){
        	 document.getElementById(estimateQuantity).value ="0.00";
 			  <c:if test="${contractType}">
			  document.getElementById('estimateSellQuantity'+aid).value="0.00";
			  </c:if>
        	 
         }else{
        	 document.getElementById(estimateQuantity).value = Math.round(res[1]*100)/100 ;
 			  <c:if test="${contractType}">
			  document.getElementById('estimateSellQuantity'+aid).value=Math.round(res[1]*100)/100 ;
			  </c:if>
        	 
         }  }
         if(res[5]=="AskUser"){
         var reply = prompt("Please Enter the value of Rate", "");
         if(reply)  {
         if((reply>0 || reply<9)|| reply=='.')  {
        	 document.getElementById(estimateRate).value =Math.round(reply*10000)/10000 ;
         	} else {
         		alert("Please Enter legitimate Rate");
         	} 	}
         	 else{
         		document.getElementById(estimateRate).value = document.getElementById(estimateRate).value;
         } 
         }else{
         if(res[4] == undefined){
        	 document.getElementById(estimateRate).value ="0.00";
         }else{
        	 document.getElementById(estimateRate).value = Math.round(res[4]*10000)/10000 ;
         }  }
         if(res[5]=="BuildFormula")
         {
         if(res[6] == undefined){
        	 document.getElementById(estimateRate).value ="0.00";
         }else{
        	 document.getElementById(estimateRate).value = Math.round(res[6]*10000)/10000;
         }  }
        var Q1 = document.getElementById(estimateQuantity).value;
        var Q2 = document.getElementById(estimateRate).value;
        if(res[5]=="BuildFormula" && res[6] != undefined ){
        	Q2 = res[6];
        }
        var E1=Q1*Q2;
        var E2="";
        E2=Math.round(E1*10000)/10000;
        document.getElementById(estimateExpense).value = E2;
        var type = res[8];
          var typeConversion=res[10];
          if(type == 'Division')  {
          	 if(typeConversion==0)  {
          	   E1=0*1;
          	 document.getElementById(estimateExpense).value = E1;
          	 } else  {	
          		E1=(Q1*Q2)/typeConversion;
          		E2=Math.round(E1*10000)/10000;
          		document.getElementById(estimateExpense).value = E2;
          	 }  }
          if(type == ' ' || type == '')  {
          		E1=(Q1*Q2);
          		E2=Math.round(E1*10000)/10000;
          		document.getElementById(estimateExpense).value = E2;
          }
          if(type == 'Multiply')  {
          		E1=(Q1*Q2*typeConversion);
          		E2=Math.round(E1*10000)/10000;
          		document.getElementById(estimateExpense).value = E2;
          } 
          basis.value=res[9];

         document.forms['serviceOrderForm'].elements['oldEstimateSellDeviation'].value=0;
         document.forms['serviceOrderForm'].elements['estimateDeviation'].value=0;
         document.forms['serviceOrderForm'].elements['oldEstimateDeviation'].value=0;
         document.forms['serviceOrderForm'].elements['estimateSellDeviation'].value=0; 
      }  
}

function handleHttpResponse701(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote, deviation,estimateDeviation, estimateSellDeviation,aid)
	{
	     if (http88.readyState == 4)  {
	        var results = http88.responseText;
	        results = results.trim(); 

	      <c:if test="${!contractType}">
	        var res = results.split("#");
	        var multquantity=res[14];
        if(res[17] == undefined){             
        	document.getElementById(estimateRate).value ="0.00";
        }else{                  
        	document.getElementById(estimateRate).value = Math.round(res[17]*10000)/10000 ;
        }
	        if(res[3]=="AskUser1"){ 
	           var reply = prompt("Please Enter Quantity", "");
	             if(reply) {
  	             if((reply>0 || reply<9)|| reply=='.')  {
  	            	document.getElementById(estimateQuantity).value = Math.round(reply*100)/100;
  	 			  <c:if test="${contractType}">
  				  document.getElementById('estimateSellQuantity'+aid).value=Math.round(reply*100)/100 ;
  				  </c:if>
  	             }else {
  	                alert("Please Enter legitimate Quantity");
  	             } 
	             }else{
	            	 document.getElementById(estimateQuantity).value = document.getElementById(estimateQuantity).value;
	  	 			  <c:if test="${contractType}">
	  				  document.getElementById('estimateSellQuantity'+aid).value=document.getElementById('estimateSellQuantity'+aid).value;
	  				  </c:if>
	             }
       	}else{             
            if(res[1] == undefined){
            	document.getElementById(estimateQuantity).value ="0.00";
	 			  <c:if test="${contractType}">
  				  document.getElementById('estimateSellQuantity'+aid).value="0.00";
  				  </c:if>
        	
            }else{
            	document.getElementById(estimateQuantity).value = Math.round(res[1]*100)/100;
	 			  <c:if test="${contractType}">
  				  document.getElementById('estimateSellQuantity'+aid).value=Math.round(res[1]*100)/100;
  				  </c:if>
        	
            }  
       	}
	       	if(res[5]=="AskUser"){ 
           var reply = prompt("Please Enter the value of Rate", "");
           if(reply)  {
               if((reply>0 || reply<9)|| reply=='.') {
            	   document.getElementById(estimateSellRate).value = Math.round(reply*10000)/10000;
               } else {
               		alert("Please Enter legitimate Rate");
               	} 
            }else{
            	document.getElementById(estimateRate).value = document.getElementById(estimateRate).value;
           	} 
	       	}else{
           if(res[4] == undefined){
        	   document.getElementById(estimateSellRate).value ="0.00";
           }else{
        	   document.getElementById(estimateSellRate).value = Math.round(res[4]*10000)/10000 ;
           }  
	      	 }
	        if(res[5]=="BuildFormula"){
	          if(res[6] == undefined){
	        	  document.getElementById(estimateSellRate).value ="0.00";
	           }else{
	        	   document.getElementById(estimateSellRate).value = Math.round(res[6]*10000)/10000 ;
	           }  
         }

	          
        var Q1 = document.getElementById(estimateQuantity).value;
		  <c:if test="${contractType}">
		  Q1=document.getElementById('estimateSellQuantity'+aid).value;
		  </c:if>
        var Q2 = document.getElementById(estimateSellRate).value;
        if(res[5]=="BuildFormula" && res[6] != undefined){
        	Q2 =res[6];
        }
        var oldRevenue = eval(document.getElementById(estimateRevenueAmount).value);
        var oldExpense=eval(document.getElementById(estimateExpense).value);
        var E1=Q1*Q2;
        var E2="";
        E2=Math.round(E1*10000)/10000;
        document.getElementById(estimateRevenueAmount).value = E2;
        var type = res[8];
          var typeConversion=res[10];   
          if(type == 'Division')   {
          	 if(typeConversion==0) 	 {
          	   E1=0*1;
          	 document.getElementById(estimateRevenueAmount).value = E1;
          	 } else {	
          		E1=(Q1*Q2)/typeConversion;
          		E2=Math.round(E1*10000)/10000;
          		document.getElementById(estimateRevenueAmount).value = E2;
          	 } }
          if(type == ' ' || type == '') {
          		E1=(Q1*Q2);
          		E2=Math.round(E1*10000)/10000;
          		document.getElementById(estimateRevenueAmount).value = E2;
          }
          if(type == 'Multiply')  {
          		E1=(Q1*Q2*typeConversion);
          		E2=Math.round(E1*10000)/10000;
          		document.getElementById(estimateRevenueAmount).value = E2;
          }

          var Q11 = document.getElementById(estimateQuantity).value;
          var Q22 = document.getElementById(estimateRate).value;
          var E11=Q11*Q22;
          var E22="";
          E22=Math.round(E11*10000)/10000;
          document.getElementById(estimateExpense).value = E22;
            if(type == 'Division')  {
            	 if(typeConversion==0)  {
            	   E11=0*1;
            	   document.getElementById(estimateExpense).value = E11;
            	 } else  {	
            		E11=(Q11*Q22)/typeConversion;
            		E22=Math.round(E11*10000)/10000;
            		document.getElementById(estimateExpense).value = E22;
            	 }  }
            if(type == ' ' || type == '')  {
            		E11=(Q11*Q22);
            		E22=Math.round(E11*10000)/10000;
            		document.getElementById(estimateExpense).value = E22;
            }
            if(type == 'Multiply')  {
            		E11=(Q11*Q22*typeConversion);
            		E22=Math.round(E11*10000)/10000;
            		document.getElementById(estimateExpense).value = E22;
            }
	             
            document.getElementById(basis).value=res[9];
         var chargedeviation=res[15];
         if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
        	 var buyDependSellAccount=res[11];
             var estimateRevenueAmt=0;
             var finalEstimateRevenueAmount=0; 
             var finalEstimateExpenceAmount=0;
             var estimateSellRates=0; 
             var sellDeviation=0;
             var estimatepasspercentages=0;
             var estimatepasspercentageRound=0;
             var buyDeviation=0;
             var oldEstDeviation =  document.getElementById(estimateDeviation).value;
             var oldEstSellDeviation = document.getElementById(estimateSellDeviation).value;
             sellDeviation=res[12];
             buyDeviation=res[13]; 
             document.getElementById(estimateSellDeviation).value=sellDeviation;
             document.getElementById(estimateDeviation).value=buyDeviation;
	        
             document.getElementById(deviation).value=chargedeviation; 
             document.forms['serviceOrderForm'].elements['buyDependSell'].value=res[11];
            
             estimateRevenueAmt=document.getElementById(estimateRevenueAmount).value; 
             if(sellDeviation==0 && sellDeviation==undefined){
            	 sellDeviation = document.getElementById(oldEstSellDeviation).value;
	         }

             finalEstimateRevenueAmount=Math.round((estimateRevenueAmt*(sellDeviation/100))*10000)/10000; 
             document.getElementById(estimateRevenueAmount).value=finalEstimateRevenueAmount;
             if(buyDeviation==0 && buyDeviation==undefined){
            	 buyDeviation = document.getElementById(oldEstDeviation).value;
	         }

             if(buyDependSellAccount=="Y"){
              	finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(buyDeviation/100))*10000)/10000;
             }else{
              	finalEstimateExpenceAmount= Math.round((estimateRevenueAmt*(buyDeviation/100))*10000)/10000;
             }
             document.getElementById(estimateExpense).value=finalEstimateExpenceAmount;
             estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
 	         estimatepasspercentageRound=Math.round(estimatepasspercentages);
 	         if(document.getElementById(estimateExpense).value == '' || document.getElementById(estimateExpense).value == 0){
 	        	document.getElementById(estimatePassPercentage).value='';
 	         }else{
 	        	document.getElementById(estimatePassPercentage).value=estimatepasspercentageRound;
 	         }
 	         // calculate estimate rate 
 	        var estimateQuantity1 =0;
 		    var estimateDeviation1=100;
 		    estimateQuantity1= document.getElementById(estimateQuantity).value; 
 		    var basis1 = document.getElementById(basis).value; 
 		    var estimateExpense1 =eval(document.getElementById(estimateExpense).value);
 		    //alert(estimateExpense1) 
 		    var estimateRate1=0;
 		    var estimateRateRound1=0; 
 		    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
 	         estimateQuantity1 = document.getElementById(estimateQuantity).value=1;
 	         } 
 	        if( basis1=="cwt" || basis1=="%age"){
 	       	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
 	       	  estimateRateRound1=Math.round(estimateRate1*10000)/10000;
 	       		document.getElementById(estimateRate).value=estimateRateRound1;	  	
 	       	}  else if(basis1=="per 1000"){
 	       	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
 	       	  estimateRateRound1=Math.round(estimateRate1*10000)/10000;
 	       	document.getElementById(estimateRate).value=estimateRateRound1;		  	
 	       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
 	          estimateRate1=(estimateExpense1/estimateQuantity1); 
 	       	  estimateRateRound1=Math.round(estimateRate1*10000)/10000; 
 	       		document.getElementById(estimateRate).value=estimateRateRound1;		  	
 		    }
 		    
 		    estimateDeviation1= document.getElementById(estimateDeviation).value 
 		    if(estimateDeviation1!='' && estimateDeviation1>0){
	 		    estimateRate1=estimateRateRound1*100/estimateDeviation1;
	 		    estimateRateRound1=Math.round(estimateRate1*10000)/10000;
	 		    document.getElementById(estimateRate).value=estimateRateRound1;		
 		    }
         }else {
        	 document.getElementById(estimateDeviation).value=0.00;
        	 document.getElementById(estimateSellDeviation).value=0.00;
             var bayRate=0
             bayRate = res[17] ;
             if(bayRate!=0 && bayRate!=undefined){
               var estimateQuantityPay=0;
               estimateQuantityPay= document.getElementById(estimateQuantity).value;
               Q1=estimateQuantityPay;
               Q2=bayRate; 
               if(type == 'Division')   {
               	 if(typeConversion==0) 	 {
               	   E1=0*1;
               		document.getElementById(estimateExpense).value = E1;
               	 } else {	
               		E1=(Q1*Q2)/typeConversion;
               		E2=Math.round(E1*10000)/10000;
               		document.getElementById(estimateExpense).value = E2;
               	 } }
               if(type == ' ' || type == '') {
               		E1=(Q1*Q2);
               		E2=Math.round(E1*10000)/10000;
               		document.getElementById(estimateExpense).value = E2;
               }
               if(type == 'Multiply')  {
               		E1=(Q1*Q2*typeConversion);
               		E2=Math.round(E1*10000)/10000;
               		document.getElementById(estimateExpense).value = E2;
               }
               document.getElementById(estimateRate).value = Q2;
               var  estimatepasspercentages=0
               var estimatepasspercentageRound=0;
               var finalEstimateRevenueAmount=0; 
               var finalEstimateExpenceAmount=0;
               finalEstimateRevenueAmount=  document.getElementById(estimateRevenueAmount).value;
               finalEstimateExpenceAmount= document.getElementById(estimateExpense).value;
               estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
  	           estimatepasspercentageRound=Math.round(estimatepasspercentages);
  	           if(finalEstimateExpenceAmount == '' || finalEstimateExpenceAmount == 0.00 || finalEstimateExpenceAmount == 0){
  	        	 document.getElementById(estimatePassPercentage).value='';
  	           }else{
  	        	 document.getElementById(estimatePassPercentage).value=estimatepasspercentageRound;
  	           } 
             }else{	           	   
               var quantity =0;
               var rate =0;
               var estimatepasspercentages=0;
               var estimatepasspercentageRound=0;
               var finalEstimateRevenueAmount=0; 
               var finalEstimateExpenceAmount=0;
               quantity= document.getElementById(estimateQuantity).value;	               
               rate= document.getElementById(estimateRate).value;	               
               finalEstimateExpenceAmount = (quantity*rate); 
               if( document.getElementById(basis).value=="cwt" || document.getElementById(basis).value=="%age")  {
      	          finalEstimateExpenceAmount = finalEstimateExpenceAmount/100; 
               }
               if( document.getElementById(basis).value=="per 1000")  {
      	        	finalEstimateExpenceAmount = finalEstimateExpenceAmount/1000; 
               }
               finalEstimateExpenceAmount= Math.round((finalEstimateExpenceAmount)*10000)/10000;
               finalEstimateRevenueAmount=  document.getElementById(estimateRevenueAmount).value;
               estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
  	           estimatepasspercentageRound=Math.round(estimatepasspercentages);
   
  	           if(finalEstimateExpenceAmount == '' || finalEstimateExpenceAmount == 0.00 || finalEstimateExpenceAmount == 0){
  	        	 document.getElementById(estimatePassPercentage).value='';
  	           }else{
  	        	 document.getElementById(estimatePassPercentage).value=estimatepasspercentageRound;
  	           }
            }
             
        }

       //#7639 - Issue with Compute functionality in Account line and Pricing section start
  	                   
           //#7639 - Issue with Compute functionality in Account line and Pricing section End
           
/////////changes
         try{
	     	    var basisValue = document.getElementById(basis).value;
	    	    checkFloatNew('serviceOrderForm',estimateQuantity,'Nothing');
	    	    var quantityValue = document.getElementById(estimateQuantity).value;
	    	    		             
	 	        var buyRate1= document.getElementById('estimateRate'+aid).value;
		        var estCurrency1= document.getElementById('estCurrencyNew'+aid).value;
				if(estCurrency1.trim()!=""){
						var Q1=0;
						var estimateExpenseQ1=0;
						var Q2=0;
						Q1=buyRate1;
						Q2=document.getElementById('estExchangeRateNew'+aid).value;
			            var E1=Q1*Q2;
			            E1=Math.round(E1*10000)/10000;
			            document.getElementById('estLocalRateNew'+aid).value=E1;
			            var estLocalAmt1=0;
			            estimateExpenseQ1=document.getElementById(estimateExpense).value;
			            estLocalAmt1=estimateExpenseQ1*Q2; 
			            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
			            document.getElementById('estLocalAmountNew'+aid).value=estLocalAmt1;     
				}

		        buyRate1= document.getElementById('estimateSellRate'+aid).value;
		        estCurrency1= document.getElementById('estSellCurrencyNew'+aid).value;
				if(estCurrency1.trim()!=""){
						var Q1=0;
						var Q2=0;
						var estimateRevenueAmountQ1=0;
						Q1=buyRate1;
						Q2= document.getElementById('estSellExchangeRateNew'+aid).value;
			            var E1=Q1*Q2;
			            E1=Math.round(E1*10000)/10000;
			            document.getElementById('estSellLocalRateNew'+aid).value=E1;
			            var estLocalAmt1=0;
			            estimateRevenueAmountQ1=document.getElementById(estimateRevenueAmount).value;
			            estLocalAmt1=estimateRevenueAmountQ1*Q2; 
			            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000; 
			            document.getElementById('estSellLocalAmountNew'+aid).value=estLocalAmt1;     
				}
				var type01 = res[8];
			     var typeConversion01=res[10]; 					
				//#7298 - Copy esimates not calculating preset calculations in charges start
				  //#7298 - Copy esimates not calculating preset calculations in charges End						
				}catch(e){}
		        <c:if test="${systemDefaultVatCalculationNew=='Y'}">
				try{
			    calculateVatAmt('REVENUE',aid);
			    calculateVatAmt('EXPENSE',aid);
				}catch(e){}
		        </c:if> 					
//////////changes
            //--------------------Start--estimateRevenueAmount-------------------------//
               var revenueValue= document.getElementById(estimateRevenueAmount).value;
  	           var revenueRound=0;
  	           var oldRevenueSum=0;
  	           var balanceRevenue=0;
  	           <c:if test="${not empty serviceOrder.id}">
  	           	oldRevenueSum=eval(document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value);
  	           </c:if> 	  	          
  	           revenueRound=Math.round(revenueValue*10000)/10000;
  	           balanceRevenue=revenueRound-oldRevenue;
  	           oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
  	           balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
  	           oldRevenueSum=oldRevenueSum+balanceRevenue; 
  	           oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
  	           var revenueRoundNew=""+revenueRound;
  	           if(revenueRoundNew.indexOf(".") == -1){
  	           revenueRoundNew=revenueRoundNew+".00";
  	           } 
  	           if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
  	           revenueRoundNew=revenueRoundNew+"0";
  	           }
  	         document.getElementById(estimateRevenueAmount).value=revenueRoundNew;
  	           <c:if test="${not empty serviceOrder.id}">
  	  	           var newRevenueSum=""+oldRevenueSum;
  	  	           if(newRevenueSum.indexOf(".") == -1){
  	  	           	newRevenueSum=newRevenueSum+".00";
  	  	           } 
  	  	           if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
  	  	           	newRevenueSum=newRevenueSum+"0";
  	  	           } 
  	  	       document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value=newRevenueSum;
  	           </c:if>
  	           if(revenueRoundNew>0){
  	        	 document.getElementById(displayOnQuote).checked = true;
  	           }
  	       //--------------------End--estimateRevenueAmount-------------------------//
  	       
           //-------------------Start---estimateExpense-------------------------//                
              var expenseValue= document.getElementById(estimateExpense).value;
              var expenseRound=0;
              var oldExpenseSum=0;
              var balanceExpense=0;
              <c:if test="${not empty serviceOrder.id}">
              	oldExpenseSum=eval(document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value);
              </c:if>
              expenseRound=Math.round(expenseValue*10000)/10000;
              balanceExpense=expenseRound-oldExpense;
              oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
              balanceExpense=Math.round(balanceExpense*10000)/10000; 
              oldExpenseSum=oldExpenseSum+balanceExpense;
              oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
              var expenseRoundNew=""+expenseRound;
              if(expenseRoundNew.indexOf(".") == -1){
              	expenseRoundNew=expenseRoundNew+".00";
              } 
              if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
              	expenseRoundNew=expenseRoundNew+"0";
              }
              document.getElementById(estimateExpense).value=expenseRoundNew;
              <c:if test="${not empty serviceOrder.id}">
	              var newExpenseSum=""+oldExpenseSum;
	              if(newExpenseSum.indexOf(".") == -1){
	              	newExpenseSum=newExpenseSum+".00";
	              } 
	              if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
	              	newExpenseSum=newExpenseSum+"0";
	              } 
	              document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value=newExpenseSum;
              </c:if>
              if(expenseRoundNew>0){
            	  document.getElementById(displayOnQuote).checked = true;
              }
              //-------------------End---estimateExpense-------------------------//
              			            
              calculateGrossMargin();
	            </c:if>
	            <c:if test="${contractType}">
	            
		 	       var res = results.split("#"); 
			 	  	<c:choose>
		 			 <c:when test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
			 	         if(document.getElementById('estimateContractCurrencyNew'+aid).value!=res[18]){
			 	        	document.getElementById('estimateContractCurrencyNew'+aid).value=res[18]; 
			 	          rateOnActualizationDate('estSellCurrencyNew'+aid+'~estimateContractCurrencyNew'+aid,'estSellValueDateNew'+aid+'~estimateContractValueDateNew'+aid,'estSellExchangeRateNew'+aid+'~estimateContractExchangeRateNew'+aid);
			 	         }
		 	         </c:when>
		 	         <c:otherwise>
		 	        document.getElementById('estimateContractCurrencyNew'+aid).value=res[18];
		 	        document.getElementById('estimateContractValueDateNew'+aid).value=currentDateRateOnActualization();
		 	        document.getElementById('estimateContractExchangeRateNew'+aid).value=findExchangeRateRateOnActualization(res[18]);
		 	         </c:otherwise>
		 	    </c:choose>
			 	   document.getElementById('estimatePayableContractCurrencyNew'+aid).value=res[19];
			 	   document.getElementById('estimatePayableContractValueDateNew'+aid).value=currentDateRateOnActualization();
			 	   document.getElementById('estimatePayableContractExchangeRateNew'+aid).value=findExchangeRateRateOnActualization(res[19]);
//            	   findExchangeContractRateByChargesNew('payable',aid);
	               var multquantity=res[14]
	              if(res[3]=="AskUser1"){ 
	               var reply = prompt("Please Enter Quantity", "");
	               if(reply) {
	               if((reply>0 || reply<9)|| reply=='.')  {
	            	   document.getElementById(estimateQuantity).value = Math.round(reply*100)/100;
	            	   <c:if test="${contractType}">
		  				  document.getElementById('estimateSellQuantity'+aid).value=Math.round(reply*100)/100;
		  				  </c:if>
	            	} else {
	            		alert("Please Enter legitimate Quantity");
	            	} }
	            	else{
	            		document.getElementById(estimateQuantity).value =document.getElementById(estimateQuantity).value ;
	            		<c:if test="${contractType}">
		  				  document.getElementById('estimateSellQuantity'+aid).value=document.getElementById('estimateSellQuantity'+aid).value;
		  				  </c:if>
	            	}
	            }else{             
	            if(res[1] == undefined){
	            	document.getElementById(estimateQuantity).value ="";
	            	<c:if test="${contractType}">
	  				  document.getElementById('estimateSellQuantity'+aid).value="";
	  				  </c:if>
	            }else{
	            	document.getElementById(estimateQuantity).value = Math.round(res[1]*100)/100;
	            	<c:if test="${contractType}">
	  				  document.getElementById('estimateSellQuantity'+aid).value= Math.round(res[1]*100)/100;
	  				  </c:if>
	            }  }
	            if(res[5]=="AskUser"){ 
	            var reply = prompt("Please Enter the value of Rate", "");
	            if(reply)  {
	            if((reply>0 || reply<9)|| reply=='.') {
	            	document.getElementById('estimateContractRateNew'+aid).value = Math.round(reply*10000)/100000;
	            	} else {
	            		alert("Please Enter legitimate Rate");
	            	} }
	            	else{
	            		document.getElementById('estimateContractRateNew'+aid).value = document.getElementById(estimateRate).value;
	            	} 
	            }else{
	            if(res[4] == undefined){
	            	document.getElementById('estimateContractRateNew'+aid).value ="";
	            }else{
	            	document.getElementById('estimateContractRateNew'+aid).value =Math.round(res[4]*10000)/10000 ;
	            }  }
	            if(res[5]=="BuildFormula")   {
	            if(res[6] == undefined){
	            	document.getElementById('estimateContractRateNew'+aid).value ="";
	            }else{
	            	document.getElementById('estimateContractRateNew'+aid).value =Math.round(res[6]*10000)/10000 ;
	            }  }
	           var Q1 = document.getElementById(estimateQuantity).value;
	           var Q2 = document.getElementById('estimateContractRateNew'+aid).value;
	           if(res[5]=="BuildFormula" && res[6] != undefined){
	        	   Q2 =res[6];
	           }
	            var oldRevenue = eval(document.getElementById(estimateRevenueAmount).value);
	            var oldExpense=eval(document.getElementById(estimateExpense).value);		           
	           var E1=Q1*Q2;
	           var E2="";
	           E2=Math.round(E1*10000)/10000;
	           document.getElementById('estimateContractRateAmmountNew'+aid).value = E2;
	           var type = res[8];
	             var typeConversion=res[10];   
	             if(type == 'Division')   {
	             	 if(typeConversion==0) 	 {
	             	   E1=0*1;
	             	  document.getElementById('estimateContractRateAmmountNew'+aid).value = E1;
	             	 } else {	
	             		E1=(Q1*Q2)/typeConversion;
	             		E2=Math.round(E1*10000)/10000;
	             		document.getElementById('estimateContractRateAmmountNew'+aid).value = E2;
	             	 } }
	             if(type == ' ' || type == '') {
	             		E1=(Q1*Q2);
	             		E2=Math.round(E1*10000)/10000;
	             		document.getElementById('estimateContractRateAmmountNew'+aid).value = E2;
	             }
	             if(type == 'Multiply')  {
	             		E1=(Q1*Q2*typeConversion);
	             		E2=Math.round(E1*10000)/10000;
	             		document.getElementById('estimateContractRateAmmountNew'+aid).value = E2;
	             } 
	             document.getElementById(basis).value=res[9];
	             var chargedeviation=res[15];
	             document.getElementById(deviation).value=chargedeviation;
	             if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){ 
	            	 document.getElementById('buyDependSellNew'+aid).value=res[11];
	             var buyDependSellAccount=res[11];
	             var estimateRevenueAmount1=0;
	             var finalEstimateRevenueAmount=0; 
	             var finalEstimateExpenceAmount=0;
	             var finalEstimateContractRateAmmount=0;
	             var estimateContractRateAmmount1=0;
	             var estimatePayableContractRateAmmount1=0;
	             var estimateSellRate1=0; 
	             var sellDeviation1=0;
	             var  estimatepasspercentage=0
	             var estimatepasspercentageRound=0;
	             var buyDeviation1=0;
	             sellDeviation1=res[12]
	             buyDeviation1=res[13] 
	             var estimateContractExchangeRate1=document.getElementById('estimateContractExchangeRateNew'+aid).value
	             estimateContractRateAmmount1 =document.getElementById('estimateContractRateAmmountNew'+aid).value
	             estimateRevenueAmount1=estimateContractRateAmmount1/estimateContractExchangeRate1;
	             estimateRevenueAmount1=Math.round(estimateRevenueAmount1*10000)/10000;
	             estimateRevenueAmount1=document.getElementById('estimateContractRateAmmountNew'+aid).value;  
	             finalEstimateRevenueAmount=Math.round((estimateRevenueAmount1*(sellDeviation1/100))*10000)/10000;
	             finalEstimateContractRateAmmount=Math.round((estimateContractRateAmmount1*(sellDeviation1/100))*10000)/10000;
	             document.getElementById('estimateContractRateAmmountNew'+aid).value=finalEstimateContractRateAmmount; 
	             if(buyDependSellAccount=="Y"){
	              finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(buyDeviation1/100))*10000)/10000;
	             }else{
	              finalEstimateExpenceAmount= Math.round((estimateRevenueAmount1*(buyDeviation1/100))*10000)/10000;
	             }
	             document.getElementById(estimateSellDeviation).value=sellDeviation1;
	             document.getElementById('oldEstimateSellDeviationNew'+aid).value=sellDeviation1;
	             document.getElementById(estimateDeviation).value=buyDeviation1;
	             document.getElementById('oldEstimateDeviationNew'+aid).value=buyDeviation1;
		    	 var estimatePayableContractExchangeRate=document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value
		 	    estimatePayableContractRateAmmount1=finalEstimateExpenceAmount*estimatePayableContractExchangeRate;
	             estimatePayableContractRateAmmount1=Math.round(estimatePayableContractRateAmmount1*10000)/10000;
	             document.getElementById('estimatePayableContractRateAmmountNew'+aid).value=estimatePayableContractRateAmmount1;
	             calEstimatePayableContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	             }else {
	            	 document.getElementById(estimateSellDeviation).value=0;
	             document.getElementById('oldEstimateSellDeviationNew'+aid).value=0;
	             document.getElementById(estimateDeviation).value=0;
	             document.getElementById('oldEstimateDeviationNew'+aid).value=0;
	             var bayRate=0
	             bayRate = res[17] ;
	             if(bayRate!=0){
	             var estimateQuantityPay=0;
	             estimateQuantityPay= document.getElementById(estimateQuantity).value;
	             Q1=estimateQuantityPay;
	             Q2=bayRate; 
	             if(type == 'Division')   {
	             	 if(typeConversion==0) 	 {
	             	   E1=0*1;
	             	  document.getElementById('estimatePayableContractRateAmmountNew'+aid).value = E1;
	             	 } else {	
	             		E1=(Q1*Q2)/typeConversion;
	             		E2=Math.round(E1*10000)/10000;
	             		document.getElementById('estimatePayableContractRateAmmountNew'+aid).value = E2;
	             	 } }
	             if(type == ' ' || type == '') {
	             		E1=(Q1*Q2);
	             		E2=Math.round(E1*10000)/10000;
	             		document.getElementById('estimatePayableContractRateAmmountNew'+aid).value = E2;
	             }
	             if(type == 'Multiply')  {
	             		E1=(Q1*Q2*typeConversion);
	             		E2=Math.round(E1*10000)/10000;
	             		document.getElementById('estimatePayableContractRateAmmountNew'+aid).value = E2;
	             }
	             document.getElementById('estimatePayableContractRateNew'+aid).value = Q2;
	             } }
	             //calculateEstimateSellRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid); 
	             //calculateEstimateRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	                //--------------------Start--estimateRevenueAmount-------------------------//
	             var estimatePayableContractRatecal= document.getElementById('estimatePayableContractRateNew'+aid).value
	             var estimatePayableContractRateAmmountcal= document.getElementById('estimatePayableContractRateAmmountNew'+aid).value
	             var  estimatePayableContractCurrencycal=document.getElementById('estimatePayableContractCurrencyNew'+aid).value;			        
				 if(estimatePayableContractCurrencycal.trim()!=""){
				 var estimatePayableContractExchangeRatecal=document.getElementById('estimatePayableContractExchangeRateNew'+aid).value;
	             var estimateRatecal=estimatePayableContractRatecal/estimatePayableContractExchangeRatecal;
	             estimateRatecal=Math.round(estimateRatecal*10000)/10000;
	             var estimateExpensecal=estimatePayableContractRateAmmountcal/estimatePayableContractExchangeRatecal;
	             estimateExpensecal=Math.round(estimateExpensecal*10000)/10000;
	             document.getElementById(estimateRate).value=estimateRatecal; 
	             document.getElementById(estimateExpense).value=estimateExpensecal; 
			     } 
				 var estimateRatecal= document.getElementById(estimateRate).value;
	             var estimateExpensecal= document.getElementById(estimateExpense).value;
			     var estCurrencycal=document.getElementById('estCurrencyNew'+aid).value;
				 if(estCurrencycal.trim()!=""){
				 var estExchangeRatecal = document.getElementById('estExchangeRateNew'+aid).value;
				 var estLocalRatecal=estimateRatecal*estExchangeRatecal;
				 estLocalRatecal=Math.round(estLocalRatecal*10000)/10000;
				 var estLocalAmountcal=estimateExpensecal*estExchangeRatecal;
				 estLocalAmountcal=Math.round(estLocalAmountcal*10000)/10000;
				 document.getElementById('estLocalRateNew'+aid).value=estLocalRatecal;
	             document.getElementById('estLocalAmountNew'+aid).value=estLocalAmountcal;
				 }  
				 var estimateContractRatecal= document.getElementById('estimateContractRateNew'+aid).value
	             var estimateContractRateAmmountcal= document.getElementById('estimateContractRateAmmountNew'+aid).value
	             var  estimateContractCurrencycal=document.getElementById('estimateContractCurrencyNew'+aid).value;
	             if(estimateContractCurrencycal.trim()!=""){
	             var estimateContractExchangeRatecal=document.getElementById('estimateContractExchangeRateNew'+aid).value;
	             var estimateSellRatecal=estimateContractRatecal/estimateContractExchangeRatecal;
	             estimateSellRatecal=Math.round(estimateSellRatecal*10000)/10000;
	             var estimateRevenueAmountcal=estimateContractRateAmmountcal/estimateContractExchangeRatecal;
	             estimateRevenueAmountcal=Math.round(estimateRevenueAmountcal*10000)/10000;
	             document.getElementById(estimateSellRate).value=estimateSellRatecal;
	             document.getElementById(estimateRevenueAmount).value=estimateRevenueAmountcal;
	             } 
	             
	             var estimateSellRatecal= document.getElementById(estimateSellRate).value;
	             var estimateRevenueAmountcal= document.getElementById(estimateRevenueAmount).value;
			     var estSellCurrencycal=document.getElementById('estSellCurrencyNew'+aid).value;
				 if(estSellCurrencycal.trim()!=""){
					 var estSellExchangeRatecal = document.getElementById('estSellExchangeRateNew'+aid).value;
					 var estSellLocalRatecal=estimateSellRatecal*estSellExchangeRatecal;
					 estSellLocalRatecal=Math.round(estSellLocalRatecal*10000)/10000;
					 var estSellLocalAmountcal=estimateRevenueAmountcal*estSellExchangeRatecal;
					 estSellLocalAmountcal=Math.round(estSellLocalAmountcal*10000)/10000;
					 document.getElementById('estSellLocalRateNew'+aid).value=estSellLocalRatecal;
		             document.getElementById('estSellLocalAmountNew'+aid).value=estSellLocalAmountcal;
				 }

	             var estimateRevenueAmountpasspercentages= document.getElementById(estimateRevenueAmount).value
				 var estimateExpensepasspercentages= document.getElementById(estimateExpense).value
	             var estimatepasspercentagesTest = (estimateRevenueAmountpasspercentages/estimateExpensepasspercentages)*100;
	 	         var estimatepasspercentageRoundTest=Math.round(estimatepasspercentagesTest);
	 	         if(document.getElementById(estimateExpense).value == '' || document.getElementById(estimateExpense).value == 0){
	 	   	       document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
	 	         }else{
	 	   	       document.getElementById(estimatePassPercentage).value=estimatepasspercentageRoundTest;
	 	         }  
	               
	  	         var type01 = res[8];
	             var typeConversion01=res[10];  
	             //#7298 - Copy esimates not calculating preset calculations in charges start
	              //#7298 - Copy esimates not calculating preset calculations in charges End 
	      				        var revenueValue=document.getElementById(estimateRevenueAmount).value;
	      		  	           var revenueRound=0;
	      		  	           var oldRevenueSum=0;
	      		  	           var balanceRevenue=0;
	      		  	           <c:if test="${not empty serviceOrder.id}">
	      		  	           	oldRevenueSum=eval(document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value);
	      		  	           </c:if> 	  	          
	      		  	           revenueRound=Math.round(revenueValue*10000)/100000;
	      		  	           balanceRevenue=revenueRound-oldRevenue;
	      		  	           oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
	      		  	           balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
	      		  	           oldRevenueSum=oldRevenueSum+balanceRevenue; 
	      		  	           oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
	      		  	           var revenueRoundNew=""+revenueRound;
	      		  	           if(revenueRoundNew.indexOf(".") == -1){
	      		  	           revenueRoundNew=revenueRoundNew+".00";
	      		  	           } 
	      		  	           if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
	      		  	           revenueRoundNew=revenueRoundNew+"0";
	      		  	           }
	      		  	         document.getElementById(estimateRevenueAmount).value=revenueRoundNew;
	      			                 
		        <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		        try{
		        var estVatP='vatPers'+aid;
		        var revenueP='estimateRevenueAmount'+aid;
		        var estVatA='vatAmt'+aid;
		   
			    calculateVatAmt('REVENUE',aid);
			    calculateVatAmt('EXPENSE',aid);
		        }catch(e){}
		        </c:if>   		  	           
	  	           <c:if test="${not empty serviceOrder.id}">
	  	  	           var newRevenueSum=""+oldRevenueSum;
	  	  	           if(newRevenueSum.indexOf(".") == -1){
	  	  	           	newRevenueSum=newRevenueSum+".00";
	  	  	           } 
	  	  	           if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
	  	  	           	newRevenueSum=newRevenueSum+"0";
	  	  	           } 
	  	  	       document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value=newRevenueSum;
	  	           </c:if>
	  	           if(revenueRoundNew>0){
	  	        	 document.getElementById(displayOnQuote).checked = true;
	  	           }
	  	       //--------------------End--estimateRevenueAmount-------------------------//
	  	       
               //-------------------Start---estimateExpense-------------------------//                
	              var expenseValue=document.getElementById(estimateExpense).value;
	              var expenseRound=0;
	              var oldExpenseSum=0;
	              var balanceExpense=0;
	              <c:if test="${not empty serviceOrder.id}">
	              	oldExpenseSum=eval(document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value);
	              </c:if>
	              expenseRound=Math.round(expenseValue*10000)/10000;
	              balanceExpense=expenseRound-oldExpense;
	              oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
	              balanceExpense=Math.round(balanceExpense*10000)/10000; 
	              oldExpenseSum=oldExpenseSum+balanceExpense;
	              oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
	              var expenseRoundNew=""+expenseRound;
	              if(expenseRoundNew.indexOf(".") == -1){
	              	expenseRoundNew=expenseRoundNew+".00";
	              } 
	              if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
	              	expenseRoundNew=expenseRoundNew+"0";
	              }
	              document.getElementById(estimateExpense).value=expenseRoundNew;
	              
	              <c:if test="${not empty serviceOrder.id}">
		              var newExpenseSum=""+oldExpenseSum;
		              if(newExpenseSum.indexOf(".") == -1){
		              	newExpenseSum=newExpenseSum+".00";
		              } 
		              if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
		              	newExpenseSum=newExpenseSum+"0";
		              } 
		              document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value=newExpenseSum;
	              </c:if>
	              if(expenseRoundNew>0){
	            	  document.getElementById(displayOnQuote).checked = true;
	              }
	              //-------------------End---estimateExpense-------------------------//
	              //calculateGrossMargin();
	             <c:if test="${not empty serviceOrder.id}">
	              var revenueValue=0;
	              var expenseValue=0;
	              var grossMarginValue=0; 
	              var grossMarginPercentageValue=0; 
	              revenueValue=eval(document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value);
	              expenseValue=eval(document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value); 
	              grossMarginValue=revenueValue-expenseValue;
	              grossMarginValue=Math.round(grossMarginValue*10000)/10000; 
	              document.getElementById('estimatedGrossMargin'+${serviceOrder.id}).value=grossMarginValue; 
	              if(document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value == '' || document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value == 0 || document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value == 0.00){
	            	  document.getElementById('estimatedGrossMarginPercentage'+${serviceOrder.id}).value='';
	            	   }else{
	            	     grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
	            	     grossMarginPercentageValue=Math.round(grossMarginPercentageValue*10000)/10000; 
	            	     document.getElementById('estimatedGrossMarginPercentage'+${serviceOrder.id}).value = grossMarginPercentageValue;
	            	   }
	            </c:if> 
		            </c:if>
	        }  
	    }
	    
	    
function checkVendorName(vendorCode,estimateVendorName){
    var vendorId = document.getElementById(vendorCode).value;
	if(vendorId == ''){
		document.getElementById(estimateVendorName).value="";
	}
	if(vendorId != ''){
		var myRegxp = /([a-zA-Z0-9_-]+)$/;
		if(myRegxp.test(vendorId)==false){
			alert("Vendor code not valid" );
			document.getElementById(estimateVendorName).value="";
			document.getElementById(vendorCode).value="";
		}else{
			showOrHide(1);
		    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
		     http5.open("GET", url, true);
		     http5.onreadystatechange =function(){handleHttpResponsevendorCode(vendorCode,estimateVendorName);} ;
		     http5.send(null);
	    } }     
}

function handleHttpResponsevendorCode(vendorCode,estimateVendorName){
	if (http5.readyState == 4){
		showOrHide(0);
            var results = http5.responseText
            results = results.trim();
            var res = results.split("#"); 
	           if(res.size() >= 2){ 
	           		if(res[2] == 'Approved'){
	           			document.getElementById(estimateVendorName).value = res[1];
	           			document.getElementById(vendorCode).select();
	           		}else{
	           			alert("Vendor code is not approved" ); 
	           			document.getElementById(estimateVendorName).value="";
	           			document.getElementById(vendorCode).value="";
	           			document.getElementById(vendorCode).select();
	           		}
            }else{
                 alert("Vendor code not valid" );
                 document.getElementById(estimateVendorName).value="";
                 document.getElementById(vendorCode).value="";
                 document.getElementById(vendorCode).select();
		   }
 }
}

function getAccountlineField(aid,basis,quantity,chargeCode,contract,vendorCode){
	  accountLineBasis=document.getElementById(basis).value;
	  accountLineEstimateQuantity=document.getElementById(quantity).value;
	  chargeCode=document.getElementById(chargeCode).value;
	  contract=document.forms['serviceOrderForm'].elements[contract].value;
	  vendorCode=document.getElementById(vendorCode).value;
	  <c:if test="${contractType}">
	  var estimatePayableContractCurrencyTemp=document.getElementById('estimatePayableContractCurrencyNew'+aid).value; 
	  var estimatePayableContractValueDateTemp=document.getElementById('estimatePayableContractValueDateNew'+aid).value;
	  var estimatePayableContractExchangeRateTemp=document.getElementById('estimatePayableContractExchangeRateNew'+aid).value;
	  var estimatePayableContractRateTemp=document.getElementById('estimatePayableContractRateNew'+aid).value;
	  var estimatePayableContractRateAmmountTemp=document.getElementById('estimatePayableContractRateAmmountNew'+aid).value;
	  </c:if>
	  var estCurrencyTemp=document.getElementById('estCurrencyNew'+aid).value; 
	  var estValueDateTemp=document.getElementById('estValueDateNew'+aid).value;
	  var estExchangeRateTemp=document.getElementById('estExchangeRateNew'+aid).value;
	  var estLocalRateTemp=document.getElementById('estLocalRateNew'+aid).value;
	  var estLocalAmountTemp=document.getElementById('estLocalAmountNew'+aid).value;

	  var estimateQuantityTemp=document.getElementById('estimateQuantity'+aid).value;
	  var estimateRateTemp=document.getElementById('estimateRate'+aid).value;
	  var estimateExpenseTemp=document.getElementById('estimateExpense'+aid).value;
	  var basisTemp=document.getElementById('basis'+aid).value;
	  <c:if test="${contractType}">
	  openWindow('getAccountlineField.html?aid='+aid+'&basisTemp='+encodeURI(basisTemp)+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateRateTemp='+estimateRateTemp+'&estimateExpenseTemp='+estimateExpenseTemp+'&estimatePayableContractCurrencyTemp='+estimatePayableContractCurrencyTemp+'&estimatePayableContractValueDateTemp='+estimatePayableContractValueDateTemp+'&estimatePayableContractExchangeRateTemp='+estimatePayableContractExchangeRateTemp+'&estimatePayableContractRateTemp='+estimatePayableContractRateTemp+'&estimatePayableContractRateAmmountTemp='+estimatePayableContractRateAmmountTemp+'&estCurrencyTemp='+estCurrencyTemp+'&estValueDateTemp='+estValueDateTemp+'&estExchangeRateTemp='+estExchangeRateTemp+'&estLocalRateTemp='+estLocalRateTemp+'&estLocalAmountTemp='+estLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&decorator=popup&popup=true',920,350);
	  </c:if>
	  <c:if test="${!contractType}">
	  openWindow('getAccountlineField.html?aid='+aid+'&basisTemp='+encodeURI(basisTemp)+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateRateTemp='+estimateRateTemp+'&estimateExpenseTemp='+estimateExpenseTemp+'&estCurrencyTemp='+estCurrencyTemp+'&estValueDateTemp='+estValueDateTemp+'&estExchangeRateTemp='+estExchangeRateTemp+'&estLocalRateTemp='+estLocalRateTemp+'&estLocalAmountTemp='+estLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&decorator=popup&popup=true',920,350);
	  </c:if>
}

function showBillingDatails(id,type,position) {
	var url="showBillingDatailsFields.html?ajax=1&decorator=simple&popup=true&aid=" + encodeURI(id)+"&showBillingType=" + encodeURI(type);
	ajax_showTooltip(url,position);	
	 var containerdiv =  document.getElementById("para1").scrollLeft;
  	var newposition = document.getElementById("ajax_tooltipObj").offsetLeft-containerdiv ;	
    document.getElementById("ajax_tooltipObj").style.left = ((parseInt(newposition))+10)+'px';	 
	  }

function getAccountlineFieldNew(aid,basis,quantity,chargeCode,contract,vendorCode){
	  accountLineBasis=document.getElementById(basis).value;
	  accountLineEstimateQuantity=document.getElementById(quantity).value;
	  <c:if test="${contractType}">
	  accountLineEstimateQuantity=document.getElementById('estimateSellQuantity'+aid).value;
	  </c:if>
	  chargeCode=document.getElementById(chargeCode).value;
	  contract=document.forms['serviceOrderForm'].elements[contract].value;  
	  vendorCode=document.getElementById(vendorCode).value;  
	  <c:if test="${contractType}">
		  var estimateContractCurrencyTemp=document.getElementById('estimateContractCurrencyNew'+aid).value; 
		  var estimateContractValueDateTemp=document.getElementById('estimateContractValueDateNew'+aid).value;
		  var estimateContractExchangeRateTemp=document.getElementById('estimateContractExchangeRateNew'+aid).value;
		  var estimateContractRateTemp=document.getElementById('estimateContractRateNew'+aid).value;
		  var estimateContractRateAmmountTemp=document.getElementById('estimateContractRateAmmountNew'+aid).value;
	  </c:if>	  
		  var estSellCurrencyTemp=document.getElementById('estSellCurrencyNew'+aid).value; 
		  var estSellValueDateTemp=document.getElementById('estSellValueDateNew'+aid).value;
		  var estSellExchangeRateTemp=document.getElementById('estSellExchangeRateNew'+aid).value;
		  var estSellLocalRateTemp=document.getElementById('estSellLocalRateNew'+aid).value;
		  var estSellLocalAmountTemp=document.getElementById('estSellLocalAmountNew'+aid).value;

		  var estimateQuantityTemp=document.getElementById('estimateQuantity'+aid).value;
		  var estimateSellRateTemp=document.getElementById('estimateSellRate'+aid).value;
		  var estimateRevenueAmountTemp=document.getElementById('estimateRevenueAmount'+aid).value;
		  var basisTemp=document.getElementById('basis'+aid).value;
		  var estimateSellQuantity=1;
		  <c:if test="${contractType}">
		  estimateSellQuantity=document.getElementById('estimateSellQuantity'+aid).value;
		  </c:if>
		  
	  <c:if test="${contractType}">
	      openWindow('getAccountlineFieldNew.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateSellRateTemp='+estimateSellRateTemp+'&estimateRevenueAmountTemp='+estimateRevenueAmountTemp+'&estimateContractCurrencyTemp='+estimateContractCurrencyTemp+'&estimateContractValueDateTemp='+estimateContractValueDateTemp+'&estimateContractExchangeRateTemp='+estimateContractExchangeRateTemp+'&estimateContractRateTemp='+estimateContractRateTemp+'&estimateContractRateAmmountTemp='+estimateContractRateAmmountTemp+'&estSellCurrencyTemp='+estSellCurrencyTemp+'&estSellValueDateTemp='+estSellValueDateTemp+'&estSellExchangeRateTemp='+estSellExchangeRateTemp+'&estSellLocalRateTemp='+estSellLocalRateTemp+'&estSellLocalAmountTemp='+estSellLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&estimateSellQuantity='+estimateSellQuantity+'&decorator=popup&popup=true',920,350);
      </c:if>
      <c:if test="${!contractType}">
	      openWindow('getAccountlineFieldNew.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateSellRateTemp='+estimateSellRateTemp+'&estimateRevenueAmountTemp='+estimateRevenueAmountTemp+'&estSellCurrencyTemp='+estSellCurrencyTemp+'&estSellValueDateTemp='+estSellValueDateTemp+'&estSellExchangeRateTemp='+estSellExchangeRateTemp+'&estSellLocalRateTemp='+estSellLocalRateTemp+'&estSellLocalAmountTemp='+estSellLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&estimateSellQuantity='+estimateSellQuantity+'&decorator=popup&popup=true',920,350);
      </c:if>
	  
}
	  
function winOpenForActgCode(vendorCode,vendorName,category,aid){
	   accountLineIdScript=aid;
	    var sid='${serviceOrder.id}';
	 	var val = document.getElementById(category).value;
	 	var finalVal = "OK";
	 	var indexCheck = val.indexOf('Origin'); 
	    openWindow('findVendorCodePricing.html?sid='+sid+'&defaultListFlag=show&partnerType=PA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+vendorName+'&fld_code='+vendorCode+'&fld_seventhDescription=seventhDescription',screen.width-30,500);
	    document.getElementById(vendorCode).select(); 
}

function checkAccountLineNumber(temp) {
    var accountLineNumber=document.getElementById(temp).value; 
    var accountLineNumber1=document.getElementById(temp).value;
    accountLineNumber=accountLineNumber.trim();
    accountLineNumber1=accountLineNumber1.trim();
    if(accountLineNumber!=""){
	    if(accountLineNumber>999){
	      alert("Manually you can not Enter Line # greater than 999");
	      document.getElementById(temp).value=0;
	    }else if(accountLineNumber1.length<2){
	    	document.getElementById(temp).value='00'+document.getElementById(temp).value;
	    }else if(accountLineNumber1.length==2){
	    	var aa = document.getElementById(temp).value;
	    	document.getElementById(temp).value = '0'+aa;
	    }
    }
  }
	  
	  
	  function getTemplateParam(){
		  var jobtypeSO = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
			var routingSO = document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
			var modeSO = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
			var packingModeSO = document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
			var commoditySO = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
			var serviceTypeSO = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value;
			var companyDivisionSO = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
			var reloServiceType=document.forms['serviceOrderForm'].elements['reloServiceType'].value;
				while(reloServiceType.indexOf("#")>-1){
					reloServiceType=reloServiceType.replace('#',',');
				}

			var url1 = '&jobtypeSO='+jobtypeSO+'&routingSO='+routingSO+'&modeSO='+modeSO+'&packingModeSO='+packingModeSO+'&commoditySO='+commoditySO;
			url1 = url1+'&serviceTypeSO='+serviceTypeSO+'&companyDivisionSO='+companyDivisionSO+'&reloServiceType='+reloServiceType;
			return url1;
	  }
	  
	  var pricingValue='';
	  var pricingFooterValue ='';
	  function autoSavePricingAjax(httpObj,str,clickType){
		  var check =saveValidation(str);
		  if(check){
		  showOrHide(1);
		  var url1 = '';
		  var addURL ='';
		  var inActiveLine ='';
		  var setDescriptionChargeCode='';
		  var setDefaultDescriptionChargeCode='';
		  var rollUpInvoiceFlag='';
		  try{
			  setDescriptionChargeCode = document.forms['serviceOrderForm'].elements['setDescriptionChargeCode'].value;
			  }catch(e){}
		  try{
			  setDefaultDescriptionChargeCode = document.forms['serviceOrderForm'].elements['setDefaultDescriptionChargeCode'].value;
		  }catch(e){}		  
		  if(str == 'AddLine'){
			  var divisionFlag='';
			  try{
				divisionFlag = document.forms['serviceOrderForm'].elements['divisionFlag'].value;
			  }catch(e){}			  				
		  }else if(str == 'AddTemplate'){
			 // addURL = "&addPriceLine=AddTemplate";
			  try{
				  rollUpInvoiceFlag = document.forms['serviceOrderForm'].elements['rollUpInvoiceFlag'].value;
			  }catch(e){}
			  var jobtypeSO = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
				var routingSO = document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
				var modeSO = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
				var packingModeSO = document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
				var commoditySO = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
				var serviceTypeSO = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value;
				var companyDivisionSO = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
				var reloServiceType=document.forms['serviceOrderForm'].elements['reloServiceType'].value;
				while(reloServiceType.indexOf("#")>-1){
					reloServiceType=reloServiceType.replace('#',',');
				}
				
				url1 = '&jobtypeSO='+jobtypeSO+'&routingSO='+routingSO+'&modeSO='+modeSO+'&packingModeSO'+packingModeSO+'&commoditySO'+commoditySO;
				url1 = url1+'&serviceTypeSO'+serviceTypeSO+'&companyDivisionSO'+companyDivisionSO+'&reloServiceType'+reloServiceType;
		  }else if(str == 'Inactive'){
			  var id = idList.split(",");
			  for (i=0;i<id.length;i++){
				  if(!document.getElementById('statusCheck'+id[i].trim()).checked){
					  if(inActiveLine == ''){
						  inActiveLine = id[i].trim();
					  }else{
						  inActiveLine = inActiveLine+','+id[i].trim();
					  }
				  }
			  }
			  if(inActiveLine == ''){
				  alert('Please uncheck any line.');
				  showOrHide(0);
				  return false;
			  }
		  }
		  
		  var totalExp = document.getElementById('estimatedTotalExpense${serviceOrder.id}').value;
		  var totalRev = document.getElementById('estimatedTotalRevenue${serviceOrder.id}').value;
		  var grossMargin = document.getElementById('estimatedGrossMargin${serviceOrder.id}').value;
		  var grossMarginPercent = document.getElementById('estimatedGrossMarginPercentage${serviceOrder.id}').value;
		  
		  pricingFooterValue = totalExp+"-,-"+totalRev+"-,-"+grossMargin+"-,-"+grossMarginPercent;
			var reloServiceType=document.forms['serviceOrderForm'].elements['reloServiceType'].value;
			while(reloServiceType.indexOf("#")>-1){
				reloServiceType=reloServiceType.replace('#',',');
			}
		  
		  $.ajax({
			  type: "POST",
			  url: "autoSavePricingAjax.html?ajax=1&decorator=simple&popup=true",
			  data: { sid: '${serviceOrder.id}', id: '${serviceOrder.id}',pricingValue: pricingValue, delimeter1: '-@-',delimeter2: '~',delimeter3: '-,-',
			    	pricingFooterValue: pricingFooterValue,addPriceLine: str,
			    	jobtypeSO: jobtypeSO,routingSO:routingSO,modeSO: modeSO,packingModeSO:packingModeSO,commoditySO:commoditySO,
			    	serviceTypeSO:serviceTypeSO,reloServiceType:reloServiceType,companyDivisionSO:companyDivisionSO,inActiveLine:inActiveLine,divisionFlag:divisionFlag,setDescriptionChargeCode:setDescriptionChargeCode,setDefaultDescriptionChargeCode:setDefaultDescriptionChargeCode,rollUpInvoiceFlag:rollUpInvoiceFlag
			        },
			 success: function (data, textStatus, jqXHR) {
				 if(str == 'AddTemplate'){
					 document.forms['serviceOrderForm'].elements['defaultTemplate'].value = 'YES';
	              }
				 
			    		showOrHide(0);
			    		//alert('ok');
			    		if(str == 'AddLine' || str == 'AddTemplate' || str == 'Inactive'){
							  findAllPricing();
			              }
						  if(str == 'Save'){
							  saveForm('Save');
			              }
						  if(str == 'TabChange'){
							  saveAutoTabChange('none');
			              }
					},
		    error: function (data, textStatus, jqXHR) {
				    	showOrHide(0);
				    	//alert('Something went wrong...'); 
				    	
				    	if(str == 'AddLine' || str == 'AddTemplate' || str == 'Inactive'){
							  findAllPricing();
			              }
						  if(str == 'Save'){
							  saveForm('Save');
			              }
						  if(str == 'TabChange'){
							  saveAutoTabChange('none');
			              }
					}
			});
		  
		  }
			 clickType1="";
			 inactiveCheck="";				  
		<%--  
		//pricingFooterValue = "&pricingFooterValue="+pricingFooterValue;
		  var url="autoSavePricingAjax.html?ajax=1&sid=${serviceOrder.id}&id=${serviceOrder.id}&decorator=simple&popup=true&pricingValue="+pricingValue+"&delimeter1=-@-&delimeter2=~&delimeter3=,"+addURL+pricingFooterValue+url1;
		  httpObj.open("POST", url, true);
		  httpObj.onreadystatechange = function(){handleHttpResponseForPricingSaveAjax(httpObj,str)};
		  httpObj.send(null);
		  --%>
	  }
	  
	  
	  function handleHttpResponseForPricingSaveAjax(httpObj,str){
		  if (httpObj.readyState == 4){
			  showOrHide(0);
			  pricingValue='';
			  if(str == 'AddLine' || str == 'AddTemplate'){
				  findAllPricing();
              }
			  if(str == 'Save'){
				  saveForm('Save');
              }
		  }
	  }
	  
	  function resetPricing(){
		  isLoadPricing = '';
		  idList='';
		  animatedcollapse.toggle('pricing1');
	  }
	  
	  function findAllPricingLineId(str,clickType){
		 // alert(idList);
		  showOrHide(1);
		  pricingValue == '';
		  if((str == 'AddLine' || str == 'AddTemplate') && idList.length == 0){
		  clickType1="ADD";
		  }
		  if(str == 'Inactive'){
			  inactiveCheck=str;
			  }
		  if(str == 'AddLine' && idList.length == 0){
        	  autoSavePricingAjax(getHTTPObject(),str);
        	  return false;
          }else if(str == 'AddTemplate' && idList.length == 0){
        	  autoSavePricingAjax(getHTTPObject(),str);
        	  return false;
          }else if(str == 'Inactive' && idList.length == 0){
        	  alert('No Line is there.');
        	  return false;
          }else if(str == 'TabChange' && idList.length == 0){
        	  autoSavePricingAjax(getHTTPObject(),str);
        	  return false;
          }else if(idList.length == 0){
        	  if(str == 'Save'){
        		  saveForm(str);
        	  }
        	  return false;
          }
		  
		  var id = idList.split(",");
          var delimeter1 = "-@-";
          var check =saveValidation(str);
          if(check){
        	  for (i=0;i<id.length;i++){
        		if(pricingValue == ''){
		    			pricingValue  =  getPricingValue(id[i].trim());
		    		}else{
		    			pricingValue  =  pricingValue +delimeter1+ getPricingValue(id[i].trim());
		    		}
             }
	     }
		  
		  if(pricingValue != ''){
	    		autoSavePricingAjax(getHTTPObject(),str,clickType);
	      }
	  }
	  
	  
	  function getPricingHiddenValue(id){
		  var estCurrencyNew = validateFieldValue('estCurrencyNew'+id); //22
		  var estSellCurrencyNew = validateFieldValue('estSellCurrencyNew'+id);
		  
		  var estValueDateNew = validateFieldValue('estValueDateNew'+id); //24
		  var estSellValueDateNew = validateFieldValue('estSellValueDateNew'+id); //25
		  
		  //alert(estValueDateNew);
		  //alert(estSellValueDateNew);
		  
		  var estExchangeRateNew = validateFieldValue('estExchangeRateNew'+id);
		  var estSellExchangeRateNew = validateFieldValue('estSellExchangeRateNew'+id);
		  var estLocalRateNew = validateFieldValue('estLocalRateNew'+id);  //28
		  var estSellLocalRateNew = validateFieldValue('estSellLocalRateNew'+id); //29
		  
		  var estLocalAmountNew = validateFieldValue('estLocalAmountNew'+id); //30
		  var estSellLocalAmountNew = validateFieldValue('estSellLocalAmountNew'+id);
		  var revisionCurrencyNew = validateFieldValue('revisionCurrencyNew'+id);
		  var countryNew = validateFieldValue('countryNew'+id);
		  
		  var revisionValueDateNew = validateFieldValue('revisionValueDateNew'+id); // 34
		  var valueDateNew = validateFieldValue('valueDateNew'+id);
		  var revisionExchangeRateNew = validateFieldValue('revisionExchangeRateNew'+id);
		  var exchangeRateNew = validateFieldValue('exchangeRateNew'+id); //37
		  
		  var delimeter = "-,-";
		  
		  var values = estCurrencyNew +delimeter+ estSellCurrencyNew +delimeter+ estValueDateNew +delimeter+ estSellValueDateNew;
		  values = values +delimeter+ estExchangeRateNew +delimeter+ estSellExchangeRateNew +delimeter+ estLocalRateNew +delimeter+ estSellLocalRateNew;
		  values = values +delimeter+ estLocalAmountNew +delimeter+ estSellLocalAmountNew +delimeter+ revisionCurrencyNew +delimeter+ countryNew;
		  values = values +delimeter+ revisionValueDateNew +delimeter+ valueDateNew +delimeter+ revisionExchangeRateNew +delimeter+ exchangeRateNew;
		  
		  return values;
	  }
	  
	  // getPricingHiddenValue1 for contractType = true
	  function getPricingHiddenValue1(id){
		  var delimeter = "-,-";
		  var values = validateFieldValue('estimatePayableContractCurrencyNew'+id); // 38
		  values = values +delimeter+ validateFieldValue('estimateContractCurrencyNew'+id); //39
		  values = values +delimeter+ validateFieldValue('estimatePayableContractValueDateNew'+id);
		  
		  values = values +delimeter+ validateFieldValue('estimateContractValueDateNew'+id); //41
		  values = values +delimeter+ validateFieldValue('estimatePayableContractExchangeRateNew'+id); //42
		  
		  values = values +delimeter+ validateFieldValue('estimateContractExchangeRateNew'+id);
		  values = values +delimeter+ validateFieldValue('estimatePayableContractRateNew'+id);
		  values = values +delimeter+ validateFieldValue('estimateContractRateNew'+id);
		  values = values +delimeter+ validateFieldValue('estimatePayableContractRateAmmountNew'+id);
		  
		  values = values +delimeter+ validateFieldValue('estimateContractRateAmmountNew'+id); // 47
		  values = values +delimeter+ validateFieldValue('revisionContractCurrencyNew'+id);
		  values = values +delimeter+ validateFieldValue('revisionPayableContractCurrencyNew'+id);
		  values = values +delimeter+ validateFieldValue('contractCurrencyNew'+id); // 50
		  
		  values = values +delimeter+ validateFieldValue('payableContractCurrencyNew'+id);
		  values = values +delimeter+ validateFieldValue('revisionContractValueDateNew'+id);
		  values = values +delimeter+ validateFieldValue('revisionPayableContractValueDateNew'+id);
		  values = values +delimeter+ validateFieldValue('contractValueDateNew'+id);
		  values = values +delimeter+ validateFieldValue('payableContractValueDateNew'+id); // 55
		  values = values +delimeter+ validateFieldValue('revisionContractExchangeRateNew'+id);
		  values = values +delimeter+ validateFieldValue('revisionPayableContractExchangeRateNew'+id);
		  values = values +delimeter+ validateFieldValue('contractExchangeRateNew'+id);
		  values = values +delimeter+ validateFieldValue('payableContractExchangeRateNew'+id); // 59
		  
		  
		  return values;
	  }
	  function getPricingValue(id){
		  var categoryType = document.getElementById('category'+id).value; // 0
		  var chargeCode = document.getElementById('chargeCode'+id).value; // 1
		  var vendorCode = document.getElementById('vendorCode'+id).value; // 2
		  var vendorName = document.getElementById('estimateVendorName'+id).value; // 3
		  var basis = document.getElementById('basis'+id).value; //4
		  basis = basis.replace("%","Percentage");
		  var estimateQuantity = document.getElementById('estimateQuantity'+id).value;
		  var estimateRate = document.getElementById('estimateRate'+id).value;
		  var estimateSellRate = document.getElementById('estimateSellRate'+id).value;
		  var estimateExpense = document.getElementById('estimateExpense'+id).value;
		  var estimatePassPercentage = document.getElementById('estimatePassPercentage'+id).value;
		  var estimateRevenueAmount = document.getElementById('estimateRevenueAmount'+id).value;
		  var quoteDescription = document.getElementById('quoteDescription'+id).value;
		  
		  var displayOnQuote = document.getElementById('displayOnQuote'+id).checked;
		  var additionalService = document.getElementById('additionalService'+id).checked;
		  var statusCheck = document.getElementById('statusCheck'+id).checked; // 14
		  var companyDivision = document.getElementById('companyDivision'+id).value; 
		 // alert(additionalService);
		  
		  var division = validateFieldValue('division'+id); //15
		  var vatDecr = validateFieldValue('vatDecr'+id);
		  var vatPers = validateFieldValue('vatPers'+id);
		  var vatAmt = validateFieldValue('vatAmt'+id);
		  var deviation = validateFieldValue('deviation'+id);
		  var estimateDeviation = validateFieldValue('estimateDeviation'+id);
		  var estimateSellDeviation = validateFieldValue('estimateSellDeviation'+id);  //21
		  try{
		  var rollUpInInvoiceCheck = document.getElementById('rollUpInInvoice'+id).checked; // 73
		  }catch(e){}	
		  var delimeter2 = "~";
		  var delimeter = "-,-";
		  
		  var values = id+"~"+categoryType+ delimeter +chargeCode+ delimeter +vendorCode+ delimeter +vendorName;
		  values = values+ delimeter +basis+ delimeter +estimateQuantity+ delimeter +estimateRate+ delimeter +estimateSellRate+ delimeter +estimateExpense;
		  values = values+ delimeter +estimatePassPercentage+ delimeter +estimateRevenueAmount+ delimeter +quoteDescription;
		  values = values+ delimeter +displayOnQuote+ delimeter +additionalService+ delimeter +statusCheck;
		  values = values+ delimeter +division+ delimeter +vatDecr+ delimeter +vatPers+ delimeter +vatAmt+ delimeter +deviation+ delimeter +estimateDeviation;
		  values = values+ delimeter +estimateSellDeviation; // 0 - 21
		  
		  var pricingHiddenValue = getPricingHiddenValue(id);  // 22 - 37
		  var pricingHiddenValue1 = getPricingHiddenValue1(id); // 38 - 59
		  
		  values = values + delimeter + pricingHiddenValue;
		  values = values + delimeter + pricingHiddenValue1;
		  
		  values = values + delimeter + validateFieldValue('accountLineNumber'+id); //60
		  values = values + delimeter + companyDivision;//61
		  values = values + delimeter + validateFieldValue('pricePointAgent'+id);//62
		  values = values + delimeter + validateFieldValue('pricePointMarket'+id);//63
		  values = values + delimeter + validateFieldValue('pricePointTariff'+id);//64
		  values = values + delimeter + validateFieldValue('estimateSellQuantity'+id);//65
		  values = values + delimeter + validateFieldValue('accountLineCostElement'+id);//66
		  values = values + delimeter + validateFieldValue('accountLineScostElementDescription'+id);//67

		  values = values + delimeter + validateFieldValue('recVatPercent'+id);//68
		  values = values + delimeter + validateFieldValue('recVatDescr'+id);//69
		  values = values + delimeter + validateFieldValue('payVatPercent'+id);//70
		  values = values + delimeter + validateFieldValue('payVatDescr'+id);//71
		  values = values + delimeter + validateFieldValue('estExpVatAmt'+id);//72
		  values = values + delimeter + rollUpInInvoiceCheck;//73
		  		  
		  return values;
	  }
	   function calculateEstimateRate(id){
		    var accountlineId=document.forms['serviceOrderForm'].elements['accId'].value
		    var accEstimateQuantity=document.forms['serviceOrderForm'].elements['accEstimateQuantity'].value
		    var accEstimateRate=document.forms['serviceOrderForm'].elements['accEstimateRate'].value
		    var accEstimateExpense=document.forms['serviceOrderForm'].elements['accEstimateExpense'].value 
		    var expenseValue=0;
		    var expenseRound=0;
		    var oldExpense=0;
		    var oldExpenseSum=0;
		    var balanceExpense=0;
		    oldExpense=eval(document.getElementById('estimateExpense'+id).value);
		    <c:if test="${not empty serviceOrder.id}">
		    oldExpenseSum=eval(document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value);
		    </c:if> 
		    expenseValue=accEstimateExpense;
		    expenseRound=Math.round(expenseValue*10000)/10000;
		    balanceExpense=expenseRound-oldExpense;
		    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
		    balanceExpense=Math.round(balanceExpense*10000)/10000; 
		    oldExpenseSum=oldExpenseSum+balanceExpense;
		    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
		    var expenseRoundNew=""+expenseRound;
		    if(expenseRoundNew.indexOf(".") == -1){
		    expenseRoundNew=expenseRoundNew+".00";
		    } 
		    if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
		    expenseRoundNew=expenseRoundNew+"0";
		    }
		    document.getElementById('estimateExpense'+id).value=expenseRoundNew;
		    document.getElementById('estimateRate'+id).value=accEstimateRate;
		    estimateQuantity=document.getElementById('estimateQuantity'+id).value;
		    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {   
		    	document.getElementById('estimateQuantity'+id).value=accEstimateQuantity;
		     }
		    <c:if test="${not empty serviceOrder.id}">
		    var newExpenseSum=""+oldExpenseSum;
		    if(newExpenseSum.indexOf(".") == -1){
		    newExpenseSum=newExpenseSum+".00";
		    } 
		    if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
		    newExpenseSum=newExpenseSum+"0";
		    } 
		    document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value=newExpenseSum;
		   
		    </c:if>
		    if(expenseRoundNew>0){
		    	document.getElementById('displayOnQuote'+id).checked = true;
		    }
		    try{
			    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
			    calculateVatAmt('EXPENSE',id);
			    </c:if>
			    }catch(e){}
		    var revenue='estimateRevenueAmount'+id;
		    var expense='estimateExpense'+id;
		    var estimatePassPercentage='estimatePassPercentage'+id;
		    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage); 
		    }
	   function calculateAccountingACToBillingCurrency(accLineList,currency,extRate,valueDate){
			if(accLineList.trim()!=""){
		  	 var str=accLineList.split(",");
		  	 for(i=0;i<str.length;i++){
		  		var oldRevenue=0;
			    var oldRevenueSum=0;
			    var balanceRevenue=0; 
				var aid=str[i];
				oldRevenue=eval(document.getElementById('estimateRevenueAmount'+aid).value); 
				document.getElementById('estSellCurrencyNew'+aid).value=currency;
				document.getElementById('estSellValueDateNew'+aid).value=valueDate;
				document.getElementById('estSellExchangeRateNew'+aid).value=extRate;
		  		<c:if test="${contractType}">
		  		var extSellRateBelow=0;
		  		var extEstimateContractRateEXAbove=0;
		  		var extEstimateContractRateAbove=0;
		  		var estSellLocalRate1=0;
		  		var roundValue=0;
		  		var estimateQuantity1=0;
		  		extSellRateBelow=document.getElementById('estSellExchangeRateNew'+aid).value;
		  		extEstimateContractRateEXAbove=document.getElementById('estimateContractExchangeRateNew'+aid).value;
		  		extEstimateContractRateAbove=document.getElementById('estimateContractRateNew'+aid).value;
		  		try{
					  	if((extEstimateContractRateAbove!='')&&(extEstimateContractRateAbove!='0.00')&&(extEstimateContractRateAbove!='0')&&(extEstimateContractRateAbove!='0.0')){
					  		estSellLocalRate1=(extSellRateBelow*extEstimateContractRateAbove)/extEstimateContractRateEXAbove; 
					        roundValue=Math.round(estSellLocalRate1*10000)/10000;
					        document.getElementById('estSellLocalRateNew'+aid).value=roundValue;
					    }else{
			  		        var recRateExcha1=document.getElementById('estSellExchangeRateNew'+aid).value
			  		        var recRat1=document.getElementById('estimateSellRate'+aid).value
			  		        var recCurrencyR1=recRat1*recRateExcha1;
			  		        var roundValue4=Math.round(recCurrencyR1*10000)/10000;
			  		      document.getElementById('estSellLocalRateNew'+aid).value=roundValue4 ; 
					    }
		  		}catch(e){
		  			document.getElementById('estSellLocalRateNew'+aid).value='0.00';
			  		}
		  		
		  		try{
		  			estSellLocalRate1=document.getElementById('estSellLocalRateNew'+aid).value;
			  		estimateQuantity1=document.getElementById('estimateQuantity'+aid).value;
					  <c:if test="${contractType}">
					  estimateQuantity1=document.getElementById('estimateSellQuantity'+aid).value;
					  </c:if>
			  		
			  	  	var basis1 =document.getElementById('basis'+aid).value; 
			        var estimateSellLocalAmountRound=0;
			        
	    	   	    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	    	   	    	estimateQuantity1 =document.getElementById('estimateQuantity'+aid).value=1;
						  <c:if test="${contractType}">
						  estimateQuantity1=document.getElementById('estimateSellQuantity'+aid).value=1;
						  </c:if>	    	   	    	
	    	            } 
	    	        if(basis1=="cwt" || basis1=="%age"){
	    	        	estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/100;
	    	        	   estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*10000)/10000;
	    	        	   document.getElementById('estSellLocalAmountNew'+aid).value=estimateSellLocalAmountRound;	  	
	    	          	}  else if(basis1=="per 1000"){
	    	          		estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/1000;
	    	          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*10000)/10000;
	    	          		document.getElementById('estSellLocalAmountNew'+aid).value=estimateSellLocalAmountRound;		  	
	    	          	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	    	          		estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1); 
	    	          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*10000)/10000; 
	    	          		document.getElementById('estSellLocalAmountNew'+aid).value=estimateSellLocalAmountRound;		  	
	    	     	    }
	    	           estimateDeviation1=document.getElementById('estimateSellDeviation'+aid).value;
	    	           if(estimateDeviation1!='' && estimateDeviation1>0){
	    	          	estimateSellLocalAmountRound=estimateSellLocalAmountRound*estimateDeviation1/100;
	    	          	estimateSellLocalAmountRound=Math.round(estimateSellLocalAmountRound*10000)/10000;
	    	          	document.getElementById('estSellLocalAmountNew'+aid).value = estimateSellLocalAmountRound;	 
	    	         } 
		  		}catch(e){
		  			document.getElementById('estSellLocalAmountNew'+aid).value = '0.00';
				  		}
		  		</c:if>
		  		<c:if test="${!contractType}">
		  		var estSellLocalRate1=0;
						extSellRateBelow=document.getElementById('estSellExchangeRateNew'+aid).value;
	  				estSellLocalRate1=document.getElementById('estSellLocalRateNew'+aid).value;
	  			var estimateQuantity1=0;
		    	   var roundValue=0; 
		    	   try{
		    	  	 if(extSellRateBelow==0) {
		    	  		document.getElementById('estimateSellRate'+aid).value=0*1;
		    	     } else{
		    	       var amount1=estSellLocalRate1/extSellRateBelow; 
		    	       roundValue=Math.round(amount1*10000)/10000;
		    	       document.getElementById('estimateSellRate'+aid).value=roundValue ; 
		    	     }}catch(e){
		    	    	 document.getElementById('estimateSellRate'+aid).value='0.00' ;
			    	     }
		    	     var estSelRate=document.getElementById('estimateSellRate'+aid).value;
		    	     estimateQuantity1=document.getElementById('estimateQuantity'+aid).value;
		    	     var basis1 =document.getElementById('basis'+aid).value;
			    	   try{
				    	      var amount1=estSelRate*estimateQuantity1;
				    	        if(basis1=="cwt" || basis1=="%age"){
				    	        	amount1=(amount1)/100;
				    	          	}  else if(basis1=="per 1000"){
				    	          		amount1=(amount1)/1000;
				    	          	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
				    	          		amount1=amount1;
				    	     	    }
				    	       roundValue=Math.round(amount1*10000)/10000;
				    	       document.getElementById('estimateRevenueAmount'+aid).value=roundValue ; 
				    	   }catch(e){
				    		   document.getElementById('estimateRevenueAmount'+aid).value='0.00' ;
					    	     }
				    	try{
					    	  var estimateRevenue=document.getElementById('estimateRevenueAmount'+aid).value;
					    	  var estimate=document.getElementById('estimateExpense'+aid).value;
				    		  var estimatepasspercentage1 = (estimateRevenue/estimate)*100;
				    		  roundValue=Math.round(estimatepasspercentage1);
				    		  document.getElementById('estimatePassPercentage'+aid).value=roundValue;
				    	}catch(e){
				    		document.getElementById('estimatePassPercentage'+aid).value='0';
				    		 	}
		  		</c:if>
		  		
		  		
			    <c:if test="${not empty serviceOrder.id}">
			    oldRevenueSum=eval(document.getElementById('estimatedTotalRevenue${serviceOrder.id}').value);
			    
			    
			    var revenueValue=0;
			    revenueValue=eval(document.getElementById('estimateRevenueAmount'+aid).value); 
                var revenueRound=0;
			    revenueRound=Math.round(revenueValue*10000)/10000;
			    balanceRevenue=revenueRound-oldRevenue;
			    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
			    balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
			    oldRevenueSum=oldRevenueSum+balanceRevenue; 
			    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
		         
			    var newRevenueSum=""+oldRevenueSum; 
			    if(newRevenueSum.indexOf(".") == -1){
			    newRevenueSum=newRevenueSum+".00";
			    } 
			    if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
			    newRevenueSum=newRevenueSum+"0";
			    }  
			    document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value=newRevenueSum;
	              var revenueValuesum=0;
	              var expenseValuesum=0;
	              var grossMarginValuesum=0; 
	              var grossMarginPercentageValuesum=0; 
	              revenueValuesum=eval(document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value);
	              expenseValuesum=eval(document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value); 
	              grossMarginValuesum=revenueValuesum-expenseValuesum;
	              grossMarginValuesum=Math.round(grossMarginValuesum*10000)/10000; 
	              document.getElementById('estimatedGrossMargin'+${serviceOrder.id}).value=grossMarginValuesum; 
	              if(document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value == '' || document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value == 0 || document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value == 0.00){
	            	  document.getElementById('estimatedGrossMarginPercentage'+${serviceOrder.id}).value='';
	            	   }else{
	            		   grossMarginPercentageValuesum=(grossMarginValuesum*100)/revenueValuesum; 
	            		   grossMarginPercentageValuesum=Math.round(grossMarginPercentageValuesum*10000)/10000; 
	            		   document.getElementById('estimatedGrossMarginPercentage'+${serviceOrder.id}).value = grossMarginPercentageValuesum;
	            	   }
	            </c:if>  
			    try{
				    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
				    calculateVatAmt('REVENUE',aid);
				    calculateVatAmt('EXPENSE',aid);
				    </c:if>
				    }catch(e){}
		  	}
			}
	  	 } 
	   function calculateEstimateSellRate(){

		    var accountlineId=document.forms['serviceOrderForm'].elements['accId'].value;
		    var accEstimateQuantity=document.forms['serviceOrderForm'].elements['accEstimateQuantity'].value;
		    var accEstimateSellRate=document.forms['serviceOrderForm'].elements['accEstimateSellRate'].value; 
		    var accEstimateRevenueAmount=document.forms['serviceOrderForm'].elements['accEstimateRevenueAmount'].value; 
		    var expenseValue=0;
		    var expenseRound=0;
		    var oldExpense=0;
		    var oldExpenseSum=0;
		    var balanceExpense=0;
		    oldExpense=eval(document.getElementById('estimateRevenueAmount'+accountlineId).value);
		    <c:if test="${not empty serviceOrder.id}">
		    oldExpenseSum=eval(document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value);
		    </c:if> 
		    expenseValue=accEstimateRevenueAmount;
		    expenseRound=Math.round(expenseValue*10000)/10000;
		    balanceExpense=expenseRound-oldExpense;
		    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
		    balanceExpense=Math.round(balanceExpense*10000)/10000; 
		    oldExpenseSum=oldExpenseSum+balanceExpense;
		    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
		    var expenseRoundNew=""+expenseRound;
		    if(expenseRoundNew.indexOf(".") == -1){
		    expenseRoundNew=expenseRoundNew+".00";
		    } 
		    if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
		    expenseRoundNew=expenseRoundNew+"0";
		    } 
		    document.getElementById('estimateRevenueAmount'+accountlineId).value=expenseRoundNew;
		    document.getElementById('estimateSellRate'+accountlineId).value=accEstimateSellRate;
		    estimateQuantity=document.getElementById('estimateQuantity'+accountlineId).value;
	        <c:if test="${contractType}">
	        estimateSellQuantity=document.getElementById('estimateSellQuantity'+accountlineId).value;
	        </c:if>
			    
		    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {   
		    	document.getElementById('estimateQuantity'+accountlineId).value=accEstimateQuantity;
		     }
		    <c:if test="${contractType}">
		    if(estimateSellQuantity=='' ||estimateSellQuantity=='0' ||estimateSellQuantity=='0.0' ||estimateSellQuantity=='0.00') {   
		    	document.getElementById('estimateSellQuantity'+accountlineId).value=accEstimateQuantity;
			     }			     
		     </c:if>
		    <c:if test="${not empty serviceOrder.id}">
		    var newExpenseSum=""+oldExpenseSum;
		    if(newExpenseSum.indexOf(".") == -1){
		    newExpenseSum=newExpenseSum+".00";
		    } 
		    if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
		    newExpenseSum=newExpenseSum+"0";
		    } 
		    document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value=newExpenseSum;
		    </c:if>
		    if(expenseRoundNew>0){
		    	document.getElementById('displayOnQuote'+accountlineId).checked = true;
		    }
		    var revenue='estimateRevenueAmount'+accountlineId;
		    var expense='estimateExpense'+accountlineId;
		    var estimatePassPercentage='estimatePassPercentage'+accountlineId;
		    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage); 
		    	  
	  }   	
	   function calculateVatAmtEstTemp(vatPer,vatAmt,aid){
			  var vatExclude=document.getElementById('VATExclude'+aid).value;
			  if(vatExclude==null || vatExclude==false || vatExclude=='false'){
				  document.getElementById('recVatDescr'+aid).disabled = false;
					<c:if test="${systemDefaultVatCalculationNew=='Y'}">
					var estVatAmt=0;
					if(document.getElementById(vatPer).value!=''){
					var estVatPercent=eval(document.getElementById(vatPer).value);
					var estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['accEstimateRevenueAmount'].value);
					<c:if test="${contractType}">
					  estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['accEstSellLocalAmount'].value);
					</c:if>
					estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
					estVatAmt=Math.round(estVatAmt*10000)/10000;
					document.getElementById(vatAmt).value=estVatAmt;
					}
				    </c:if>
			  }else{
				  document.getElementById('recVatDescr'+aid).value="";			  
				  document.getElementById('recVatDescr'+aid).disabled = true;				  				  
			document.getElementById('vatAmt'+aid).value=0;								

				  
			  }
		}    
	   function calculateVatAmtEstExpTemp(vatPer,vatAmt,aid){
			  var vatExclude=document.getElementById('VATExclude'+aid).value;
			  if(vatExclude==null || vatExclude==false || vatExclude=='false'){		 
				  document.getElementById('payVatDescr'+aid).disabled = false;		   
					<c:if test="${systemDefaultVatCalculationNew=='Y'}">
					var estVatAmt=0;
					if(document.getElementById(vatPer)!=undefined && document.getElementById(vatPer).value!=''){
					var estVatPercent=eval(document.getElementById(vatPer).value);
					var estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['accEstimateExpense'].value);
					<c:if test="${contractType}">
					 estimateRevenueAmount= eval(document.getElementById('estLocalAmountNew'+aid).value);
					</c:if>
					estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
					
					estVatAmt=Math.round(estVatAmt*10000)/10000;
					document.getElementById(vatAmt).value=estVatAmt;
					}
					</c:if>
			  }else{
				  document.getElementById('payVatDescr'+aid).value="";			  
				  document.getElementById('payVatDescr'+aid).disabled = true;				  
			document.getElementById('estExpVatAmt'+aid).value=0;			

				  
			  }
		}
	  function validateFieldValue(fieldId){
		  var result1 =" ";
		  try {
			  var result = document.getElementById(fieldId);
				 if(result == null || result ==  undefined){
					 return result1;
				 }else{
					 return result.value;
				 }
		  }catch(e) {
			  return result1;
		  }
	  }
	  
	  function validateFieldValueForNumber(fieldId){
		  var result1 ="0";
		  try {
			  var result = document.getElementById(fieldId);
				 if(result == null || result ==  undefined){
					 return result1;
				 }else{
					 return result.value;
				 }
		  }catch(e) {
			  return result1;
		  }
	  }
	  
	  var http5513 = getHTTPObject();
	  
	  function fillCurrencyByChargeCode(aid) {
		  	if(aid=='temp'){
		  		aid=accountLineIdScript;
		  	}
	  		<c:if test="${contractType}"> 
	  	    var vendorCode = document.getElementById('vendorCode'+aid).value;  
	  	    var url="fillCurrencyByChargeCode.html?ajax=1&decorator=simple&popup=true&vendorCode="+vendorCode;  
	  	     http5513.open("GET", url, true); 
	  	     http5513.onreadystatechange = function(){ handleHttpResponse4113(aid);};
	  	     http5513.send(null); 
	  	     </c:if> 
	  	}
	  function handleHttpResponse4113(aid) {
	  	    if (http5513.readyState == 4)  {
	  			       var results = http5513.responseText
	  			       results = results.trim(); 
	  			       if(results!=""){
	  		                var res = results.split("~");
	  		            if(res[0].trim()!=''){
	  		            	document.getElementById('estCurrencyNew'+aid).value=res[0];		                	
	  		            	document.getElementById('revisionCurrencyNew'+aid).value=res[0];
	  		            	document.getElementById('countryNew'+aid).value=res[0]; 
	  		            	document.getElementById('estExchangeRateNew'+aid).value=res[1];
	  		            	document.getElementById('revisionExchangeRateNew'+aid).value=res[1];
	  		            	document.getElementById('exchangeRateNew'+aid).value=res[1];
	  		            	
	  		            	var datam = date_convert(new Date());
	  		            	<%--
	  						var mydate=new Date();
	  	  		          var daym;
	  	  		          var year=mydate.getFullYear()
	  	  		          var y=""+year;
	  	  		          if (year < 1000)
	  	  		          year+=1900
	  	  		          var day=mydate.getDay()
	  	  		          var month=mydate.getMonth()+1
	  	  		          if(month == 1)month="Jan";
	  	  		          if(month == 2)month="Feb";
	  	  		          if(month == 3)month="Mar";
	  	  				  if(month == 4)month="Apr";
	  	  				  if(month == 5)month="May";
	  	  				  if(month == 6)month="Jun";
	  	  				  if(month == 7)month="Jul";
	  	  				  if(month == 8)month="Aug";
	  	  				  if(month == 9)month="Sep";
	  	  				  if(month == 10)month="Oct";
	  	  				  if(month == 11)month="Nov";
	  	  				  if(month == 12)month="Dec";
	  	  				  var daym=mydate.getDate()
	  	  				  if (daym<10)
	  	  				  daym="0"+daym
	  	  				  var datam = daym+"-"+month+"-"+y.substring(2,4);
	  	  				--%>
	  	  				document.getElementById('estValueDateNew'+aid).value=datam;
	  	  				document.getElementById('revisionValueDateNew'+aid).value=datam;
	  	  				document.getElementById('valueDateNew'+aid).value=datam;
	  		            }

	  		          <c:if test="${contractType}">
	    		        try{
	    	  		        var buyRate1 = document.getElementById('estimateRate'+aid).value;
	    	  		        var estCurrency1 = document.getElementById('estCurrencyNew'+aid).value;
	    	  		        var basisValue=document.getElementById('basis'+aid).value;
	    	    	  		var quantityValue1=document.getElementById('estimateQuantity'+aid).value;
	    		  		    if(quantityValue1=='' ||quantityValue1=='0' ||quantityValue1=='0.0' ||quantityValue1=='0.00') {   
	    		  		    	document.getElementById('estimateQuantity'+aid).value=1;
	    		  		      }
	    	  		       var quantityValue=0;
	    	  		     	quantityValue=document.getElementById('estimateQuantity'+aid).value;
	    	  				if(estCurrency1.trim()!=""){
	    		  		          try{
	    			  			        var val1=0.00;
	    			  			        val1=document.getElementById('estExchangeRateNew'+aid).value;
	    			  			        var val2=0.00;
	    			  			        val2=document.getElementById('estimatePayableContractRateNew'+aid).value;
	    			  			        var val3=0.00;
	    			  			        val3=document.getElementById('estimatePayableContractExchangeRateNew'+aid).value;
	    								if((val2!='')&&(val2!='0.00')&&(val2!='0.0')&&(val2!='0')){
	    				  			    var recCurrencyRate1=(val1*val2)/val3;
	    			  			        var roundValue=Math.round(recCurrencyRate1*10000)/10000;
	    			  			      document.getElementById('estLocalRateNew'+aid).value=roundValue;
	    								}else{
	    		  						var Q1=0;
	    		  						var Q2=0;
	    		  						Q1=buyRate1;
	    		  						Q2=document.getElementById('estExchangeRateNew'+aid).value;
	    		  			            var E1=Q1*Q2;
	    		  			            E1=Math.round(E1*10000)/10000;
	    		  			          document.getElementById('estLocalRateNew'+aid).value=E1;
	    								}			  			          			        
	    			  		          }catch(e){ 
	    			  	  		          }
	    	  			            var E1=document.getElementById('estLocalRateNew'+aid).value;
	    	  			            var estLocalAmt1=0;
	    	  			            if(basisValue=="cwt" || basisValue== "%age"){
	    	  			            	estLocalAmt1=(quantityValue*E1)/100 ;
	    	  			                }
	    	  			                else if(basisValue=="per 1000"){
	    	  			                	estLocalAmt1=(quantityValue*E1)/1000 ;
	    	  			                }else{
	    	  			                	estLocalAmt1=(quantityValue*E1);
	    	  			                }	 
	    	  			            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
	    	  			          document.getElementById('estLocalAmountNew'+aid).value=estLocalAmt1;     
	    	  				}
	    	  		      }catch(e){}
	  		          </c:if>		  		            
	  			       }
	  	        } 
	  	    }
	  	 function calEstimatePayableContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) 
			{
			  		  var estimateQuantity1 =0;
			  		    var estimateDeviation1=100;
			  		    estimateQuantity1=document.getElementById(estimateQuantity).value; 
			  		    var basis1 =document.getElementById(basis).value; 
			  		    var estimateExpense1 =eval(document.getElementById('estimatePayableContractRateAmmountNew'+aid).value); 
			  	        var estimateRate1=0;
			  		    var estimateRateRound=0; 
			  		    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
			  	       estimateQuantity1 =document.getElementById(estimateQuantity).value=1;
			  	       } 
			  	      if( basis1=="cwt" || basis1=="%age"){
			  	     	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
			  	     	  estimateRateRound=Math.round(estimateRate1*10000)/10000;
			  	     	document.getElementById('estimatePayableContractRateNew'+aid).value=estimateRateRound;	  	
			  	     	}  else if(basis1=="per 1000"){
			  	     	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
			  	     	  estimateRateRound=Math.round(estimateRate1*10000)/10000;
			  	     	document.getElementById('estimatePayableContractRateNew'+aid).value=estimateRateRound;		  	
			  	     	} else  {
			  	        estimateRate1=(estimateExpense1/estimateQuantity1); 
			  	     	  estimateRateRound=Math.round(estimateRate1*10000)/10000; 
			  	     	document.getElementById('estimatePayableContractRateNew'+aid).value=estimateRateRound;		  	
			  		    }
			  		    estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value 
			  		    if(estimateDeviation1!='' && estimateDeviation1>0){
			  		    estimateRate1=estimateRateRound*100/estimateDeviation1;
			  		    estimateRateRound=Math.round(estimateRate1*10000)/10000;
			  		  document.getElementById('estimatePayableContractRateNew'+aid).value=estimateRateRound;		
			  		    } 
			  	 }
	  	function  calculateEstimateSellRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
	  	{
	  		
	 	   <c:if test="${contractType}">
	 	        var country1 =document.getElementById('estimateContractCurrencyNew'+aid).value; 
	 	        if(country1=='') {
	 	          alert("Please select Contract Currency ");
	 	         document.getElementById('estimateContractRateNew'+aid).value=0;
	 	        document.getElementById('estimateContractExchangeRateNew'+aid).value=1;
	 	        } else if(country1!=''){
	 	        var estimateContractExchangeRate1=document.getElementById('estimateContractExchangeRateNew'+aid).value
	 	        var estimateContractRate1=document.getElementById('estimateContractRateNew'+aid).value
	 	        estimateContractRate1=Math.round(estimateContractRate1*10000)/10000;
	 	        var estimateSellRate1=estimateContractRate1/estimateContractExchangeRate1;
	 	        var roundValue=Math.round(estimateSellRate1*10000)/10000;
	 	       document.getElementById(estimateSellRate).value=roundValue ; 
	 	        calEstSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	 	        calEstimateContractRateAmmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	 	        }   
	 	   </c:if>
	 	   }
	  	
	  	function calEstSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
	  	{
	  		<c:if test="${multiCurrency=='Y'}">
	  		          var country1 =document.getElementById('estSellCurrencyNew'+aid).value; 
	  		          if(country1=='') {
	  		          alert("Please select billing currency "); 
	  		        document.getElementById('estSellLocalRateNew'+aid).value=0; 
	  		        } else if(country1!=''){  
	  		        var recRateExchange1=document.getElementById('estSellExchangeRateNew'+aid).value
	  		        var recRate1=document.getElementById(estimateSellRate).value
	  		        var recCurrencyRate1=recRate1*recRateExchange1;
	  		        var roundValue=Math.round(recCurrencyRate1*10000)/10000;
	  		      document.getElementById('estSellLocalRateNew'+aid).value=roundValue ; 
	  		        calculateEstimateSellLocalAmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	  		        } 
	  	     </c:if>		        
	  		  }
	  	function calEstimateContractRateAmmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) 
	  	{

	 	   <c:if test="${contractType}">
	 	   var estimate=0;
	 	   var estimaternd=0;
	 	   var receivableSellDeviation=0;
	 	       var quantity = document.getElementById(estimateQuantity).value;
	 	       var rate = document.getElementById('estimateContractRateNew'+aid).value;
	 	   	   if(quantity<999999999999&& rate<999999999999) {
	 	  	 	estimate = (quantity*rate);
	 	  	 	estimaternd=Math.round(estimate*10000)/10000;
	 	  	 	} else { 
	 	  	 	 estimate=0;
	 	  	 	 estimaternd=Math.round(estimate*10000)/10000;
	 	  	 	} 
	 	         if( document.getElementById(basis).value=="cwt" || document.getElementById(basis).value=="%age") {
	 	      	    estimate = estimate/100; 
	 	      	    estimaternd=Math.round(estimate*10000)/10000;	 
	 	      	 document.getElementById('estimateContractRateAmmountNew'+aid).value = estimaternd; 
	 	      	      }
	 	          else if( document.getElementById(basis).value=="per 1000")  {
	 	      	    estimate = estimate/1000; 
	 	      	    estimaternd=Math.round(estimate*10000)/10000;	 
	 	      	 document.getElementById('estimateContractRateAmmountNew'+aid).value = estimaternd; 
	 	      	    } else{
	 	      	    	document.getElementById('estimateContractRateAmmountNew'+aid).value = estimaternd;
	 	              } 
	 	           estimateDeviation = document.getElementById(estimateSellDeviation).value;
	 	             if(estimateDeviation!='' && estimateDeviation>0){
	 	               estimaternd=estimaternd*estimateDeviation/100;
	 	               estimaternd=Math.round(estimaternd*10000)/10000;
	 	              document.getElementById('estimateContractRateAmmountNew'+aid).value = estimaternd;	 
	 	           }
	 	             calculateEstimateRevenueAmountByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)  
	 	   </c:if>
	 	  		}
	  	
	  	function calculateEstimateSellLocalAmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
	    {
	    	   var estimateQuantity1 =0;
	    	   var estLocalRate1 =0;
	    	   	    estimateQuantity1=document.getElementById(estimateQuantity).value; 
	    	   	    var basis1 =document.getElementById(basis).value; 
	    	   	    estSellLocalRate1 =document.getElementById('estSellLocalRateNew'+aid).value;  
	    	   	    var estimateSellLocalAmount1=0;
	    	   	    var estimateSellLocalAmountRound=0; 
	    	   	    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	    	            estimateQuantity =document.getElementById(estimateQuantity).value=1;
	    	            } 
	    	           if( basis1=="cwt" || basis1=="%age"){
	    	        	   estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/100;
	    	        	   estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*10000)/10000;
	    	        	   document.getElementById('estSellLocalAmountNew'+aid).value=estimateSellLocalAmountRound;	  	
	    	          	}  else if(basis1=="per 1000"){
	    	          		estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/1000;
	    	          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*10000)/10000;
	    	          		document.getElementById('estSellLocalAmountNew'+aid).value=estimateSellLocalAmountRound;		  	
	    	          	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	    	          		estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1); 
	    	          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*10000)/10000; 
	    	          		document.getElementById('estSellLocalAmountNew'+aid).value=estimateSellLocalAmountRound;		  	
	    	   	    }
	    	           estimateDeviation1=document.getElementById(estimateSellDeviation).value;
	  	             if(estimateDeviation1!='' && estimateDeviation1>0){
	  	            	estimateSellLocalAmountRound=estimateSellLocalAmountRound*estimateDeviation1/100;
	  	            	estimateSellLocalAmountRound=Math.round(estimateSellLocalAmountRound*10000)/10000;
	  	            	document.getElementById('estSellLocalAmountNew'+aid).value = estimateSellLocalAmountRound;	 
	  	           }  
	  	    	   var contractRate=0;
	 			  <c:if test="${contractType}">
	         contractRate = document.getElementById('estimateContractRateNew'+aid).value;
	         </c:if>
	       if(contractRate==0){
	    	           calculateEstimateRevenueNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	       }
	    	   }
	  	
	  	function calculateEstimateRevenueAmountByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) 
	  	{
		  	 
	 	   var estSellLocalAmount1= document.getElementById('estimateContractRateAmmountNew'+aid).value ;
	 	   var estSellExchangeRate1= document.getElementById('estimateContractExchangeRateNew'+aid).value*1 ;
	 	   var roundValue=0; 
	 	  	 if(estSellExchangeRate1 ==0) {
	 	  		document.getElementById(estimateRevenueAmount).value=0*1;
	 	     } else if(estSellExchangeRate1 >0) {
	 	       var amount1=estSellLocalAmount1/estSellExchangeRate1; 
	 	       roundValue=Math.round(amount1*10000)/10000;
	 	      document.getElementById(estimateRevenueAmount).value=roundValue ; 
	 	     }
	 	     var estimatepasspercentageRound=0;
	 	     var estimatepasspercentage1 = 0;
	 	     var estimateExpense1 = document.getElementById(estimateExpense).value; 
	 	     estimatepasspercentage1 = (roundValue/estimateExpense1)*100;
	 	  	 estimatepasspercentageRound=Math.round(estimatepasspercentage1);
	 	  	 if(document.getElementById(estimateExpense).value == '' || document.getElementById(estimateExpense).value == 0){
	 	  		document.getElementById(estimatePassPercentage).value='';
	 	  	   }else{
	 	  		document.getElementById(estimatePassPercentage).value=estimatepasspercentageRound;
	 	  	   }  
	 }
	  	
	  	function  calculateEstimateRevenueNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
	       {
	    	   var contractRate=0;
				  <c:if test="${contractType}">
	        contractRate = document.getElementById('estimateContractRateNew'+aid).value;
	        </c:if>
	      if(contractRate==0){
	    	   var estSellLocalAmount1= document.getElementById('estSellLocalAmountNew'+aid).value ;
	    	   var estSellExchangeRate1= document.getElementById('estSellExchangeRateNew'+aid).value*1 ;
	    	   var roundValue=0; 
	    	  	 if(estSellExchangeRate1 ==0) {
	    	  		document.getElementById(estimateRevenueAmount).value=0*1;
	    	     } else if(estSellExchangeRate1 >0) {
	    	       var amount1=estSellLocalAmount1/estSellExchangeRate1; 
	    	       roundValue=Math.round(amount1*10000)/10000;
	    	       document.getElementById(estimateRevenueAmount).value=roundValue ; 
	    	     }
	    	     var estimatepasspercentageRound=0;
	    	     var estimatepasspercentage1 = 0;
	    	     var estimateExpense1 = document.getElementById(estimateExpense).value; 
	    	     estimatepasspercentage1 = (roundValue/estimateExpense1)*100;
	    	  	 estimatepasspercentageRound=Math.round(estimatepasspercentage1);
	    	  	 if(document.getElementById(estimateExpense).value == '' || document.getElementById(estimateExpense).value == 0){
	    	  		document.getElementById(estimatePassPercentage).value='';
	    	  	   }else{
	    	  		 document.getElementById(estimatePassPercentage).value=estimatepasspercentageRound;
	    	  	   } 
	    	  	 calculateSellEstimateRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid); 
	    	  	calculateEstimateSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	            }else{
	       	        calEstSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid); 
	             }
	  	  	
	           }
	  	
	  	function calculateSellEstimateRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
	       {
	      	    var estimateQuantity1 =0;
	      	    var estimateDeviation1=100;
	      	    estimateQuantity1=document.getElementById(estimateQuantity).value; 
	      	    var basis1 =document.getElementById(basis).value; 
	      	    var estimateRevenueAmount1 =eval(document.getElementById(estimateRevenueAmount).value); 
	      	    var estimateSellRate1=0;
	      	    var estimateSellRateRound=0; 
	      	    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	               estimateQuantity1 =document.getElementById(estimateQuantity).value=1;
	               } 
	              if( basis1=="cwt" || basis1=="%age"){
	           	   estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1)*100;
	           	   estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000;
	           	document.getElementById(estimateSellRate).value=estimateSellRateRound;	  	
	             	}  else if(basis1=="per 1000"){
	             		estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1)*1000;
	             		estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000;
	             		document.getElementById(estimateSellRate).value=estimateSellRateRound;		  	
	             	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	             		estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1); 
	             		estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000; 
	             		document.getElementById(estimateSellRate).value=estimateSellRateRound;		  	
	      	    }
	              estimateSellDeviation1=document.getElementById(estimateSellDeviation).value 
	      	    if(estimateSellDeviation1!='' && estimateSellDeviation1>0){
	      	    	estimateSellRateRound=estimateSellRateRound*100/estimateSellDeviation1;
	      	    	estimateSellRateRound=Math.round(estimateSellRateRound*10000)/10000;
	      	    	document.getElementById(estimateSellRate).value=estimateSellRateRound;		
	      	    }  }
	  	
	  	function calculateEstimateSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
	       {
	    	    var estimateQuantity1 =0;
	    	    var estimateRevenueAmount1 =0;
	    		    estimateQuantity1=document.getElementById(estimateQuantity).value; 
	    		    var basis1 =document.getElementById(basis).value; 
	    		    estimateRevenueAmount1 =document.getElementById('estSellLocalAmountNew'+aid).value; 
	    		    var estimateSellRate1=0;
	    		    var estimateSellRateRound=0; 
	    		    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	    	         estimateQuantity1 =document.getElementById(estimateQuantity).value=1;
	    	         } 
	    	        if( basis1=="cwt" || basis1=="%age"){
	    	        	estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1)*100;
	    	        	estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000;
	    	        	document.getElementById(estimateSellRate).value=estimateSellRateRound;	  	
	    	       	}  else if(basis1=="per 1000"){
	    	       		estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1)*1000;
	    	       		estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000;
	    	       		document.getElementById(estimateSellRate).value=estimateSellRateRound;		  	
	    	       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	    	       		estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1); 
	    	       		estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000; 
	    	       		document.getElementById(estimateSellRate).value=estimateSellRateRound;		  	
	    		    }
	    	        estimateSellDeviation1=document.getElementById(estimateSellDeviation).value 
	    	   	    if(estimateSellDeviation1!='' && estimateSellDeviation1>0){
	    	   	    	estimateSellRate1=estimateSellRateRound*100/estimateSellDeviation1;
	    	   	    	estimateSellRateRound=Math.round(estimateSellRate1*10000)/10000;
	    	   	    	document.getElementById('estSellLocalRateNew'+aid).value=estimateSellRateRound;		
	    	   	    }  
	    	 }	
	  	
	  	function  calculateEstimateRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) {
		  	   <c:if test="${contractType}">
		  	        var country1 =document.getElementById('estimatePayableContractCurrencyNew'+aid).value; 
		  	        if(country1=='') {
		  	          alert("Please select Contract Currency ");
		  	        document.getElementById('estimatePayableContractRateNew'+aid).value=0;
		  	      document.getElementById('estimatePayableContractExchangeRateNew'+aid).value=1;
		  	        } else if(country1!=''){
		  	        var estimatePayableContractExchangeRate1=document.getElementById('estimatePayableContractExchangeRateNew'+aid).value
		  	        var estimatePayableContractRate1=document.getElementById('estimatePayableContractRateNew'+aid).value
		  	        estimatePayableContractRate1=Math.round(estimatePayableContractRate1*10000)/10000;
		  	        var estimateRate1=estimatePayableContractRate1/estimatePayableContractExchangeRate1;
		  	        var roundValue=Math.round(estimateRate1*10000)/10000;
		  	      document.getElementById(estimateRate).value=roundValue ;  
		  	        calEstLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		  	        //calRecCurrencyRate();
		  	        calEstimatePayableContractRateAmmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		  	        }   
		  	   </c:if>
		  	   }
	  	
	  	function calEstLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
	  	 {
	  	   
	          var country1 =document.getElementById('estCurrencyNew'+aid).value; 
	          if(country1=='') {
	          alert("Please select currency "); 
	          document.getElementById('estLocalRateNew'+aid).value=0; 
	        } else if(country1!=''){  
	        var recRateExchange1=document.getElementById('estExchangeRateNew'+aid).value
	        var recRate1=document.getElementById(estimateRate).value
	        var recCurrencyRate1=recRate1*recRateExchange1;
	        var roundValue=Math.round(recCurrencyRate1*10000)/10000;
	        document.getElementById('estLocalRateNew'+aid).value=roundValue ; 
	         calculateEstimateLocalAmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	        } 
	  }
	  	
	  	function calEstimatePayableContractRateAmmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) 
	  	 {
	  	   <c:if test="${contractType}">
	  	   var estimate1=0;
	  	   var estimaternd=0;
	  	   var receivableSellDeviation1=0;
	  	       var quantity1 = document.getElementById(estimateQuantity).value;
	  	       var rate1 = document.getElementById('estimatePayableContractRateNew'+aid).value;
	  	   	   if(quantity1<999999999999&& rate1<999999999999) {
	  	  	 	estimate1 = (quantity1*rate1);
	  	  	 	estimaternd=Math.round(estimate1*10000)/10000;
	  	  	 	} else { 
	  	  	 	 estimate1=0;
	  	  	 	 estimaternd=Math.round(estimate1*10000)/10000;
	  	  	 	} 
	  	         if( document.getElementById(basis).value=="cwt" || document.getElementById(basis).value=="%age") {
	  	      	    estimate1 = estimate1/100; 
	  	      	    estimaternd=Math.round(estimate1*10000)/10000;	 
	  	      	document.getElementById('estimatePayableContractRateAmmountNew'+aid).value = estimaternd; 
	  	      	      }
	  	          else if( document.getElementById(basis).value=="per 1000")  {
	  	      	    estimate1 = estimate1/1000; 
	  	      	    estimaternd=Math.round(estimate1*10000)/10000;	 
	  	      	document.getElementById('estimatePayableContractRateAmmountNew'+aid).value = estimaternd; 
	  	      	    } else{
	  	      	    document.getElementById('estimatePayableContractRateAmmountNew'+aid).value = estimaternd;
	  	              } 
	  	         estimateDeviation1=document.getElementById(estimateDeviation).value;
	  	             if(estimateDeviation1!='' && estimateDeviation1>0){
	  	               estimaternd=estimaternd*estimateDeviation1/100;
	  	               estimaternd=Math.round(estimaternd*10000)/10000;
	  	             document.getElementById('estimatePayableContractRateAmmountNew'+aid).value = estimaternd;	 
	  	           }
	  	             calculateEstimateExpenseByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);   
	  	   </c:if>
	  	  		}
	  	
	  	function calculateEstimateLocalAmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
	  	{
	  		var estimateQuantity1 =0;
	  		var estLocalRate1 =0;
	  			    estimateQuantity1=document.getElementById(estimateQuantity).value; 
	  			    var basis1 =document.getElementById(basis).value; 
	  			    estLocalRate1 =document.getElementById('estLocalRateNew'+aid).value;  
	  			    var estimateLocalAmount1=0;
	  			    var estimateLocalAmountRound=0; 
	  			    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	  		         estimateQuantity1 =document.getElementById(estimateQuantity).value=1;
	  		         } 
	  		        if( basis1=="cwt" || basis1=="%age"){
	  		       	  estimateLocalAmount1=(estLocalRate1*estimateQuantity1)/100;
	  		       	  estimateLocalAmountRound=Math.round(estimateLocalAmount1*10000)/10000;
	  		       	document.getElementById('estLocalAmountNew'+aid).value=estimateLocalAmountRound;	  	
	  		       	}  else if(basis1=="per 1000"){
	  		       	  estimateLocalAmount1=(estLocalRate1*estimateQuantity1)/1000;
	  		       	  estimateLocalAmountRound=Math.round(estimateLocalAmount1*10000)/10000;
	  		       	document.getElementById('estLocalAmountNew'+aid).value=estimateLocalAmountRound;		  	
	  		       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	  		          estimateLocalAmount1=(estLocalRate1*estimateQuantity1); 
	  		       	  estimateLocalAmountRound=Math.round(estimateLocalAmount1*10000)/10000; 
	  		       	document.getElementById('estLocalAmountNew'+aid).value=estimateLocalAmountRound;		  	
	  			    }
	  		        estimateDeviation1=document.getElementById(estimateDeviation).value;
	  		        if(estimateDeviation1!='' && estimateDeviation1>0){
	  		        	estimateLocalAmountRound=estimateLocalAmountRound*estimateDeviation1/100;
	  		        	estimateLocalAmountRound=Math.round(estimateLocalAmountRound*10000)/10000;
	  		        	document.getElementById('estLocalAmountNew'+aid).value = estimateLocalAmountRound;	 
	  		      }
	  		        var contractRate=0;
	  		  	  <c:if test="${contractType}">
	  		        contractRate = document.getElementById('estimatePayableContractRateNew'+aid).value;
	  		        </c:if>  
	  		        if(contractRate==0){   
	  			    calculateEstimateExpenseNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	  		        }
	  		}
	  	
	  	function calculateEstimateExpenseNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
	    {
	  		  	  var contractRate=0;
	  		  	  <c:if test="${contractType}">
	  		        contractRate = document.getElementById('estimatePayableContractRateNew'+aid).value;
	  		        </c:if> 
	  		        if(contractRate==0){  
	  		     var estLocalAmount1= document.getElementById('estLocalAmountNew'+aid).value ;
	  		     var estExchangeRate1= document.getElementById('estExchangeRateNew'+aid).value*1 ;
	  		     var roundValue=0; 
	  		    	 if(estExchangeRate1 ==0) {
	  		    		document.getElementById(estimateExpense).value=0*1;
	  		       } else if(estExchangeRate1 >0) {
	  		         var amount1=estLocalAmount1/estExchangeRate1; 
	  		         roundValue=Math.round(amount1*10000)/10000;
	  		       document.getElementById(estimateExpense).value=roundValue ; 
	  		       }
	  		       var estimatepasspercentageRound=0;
	  		       var estimatepasspercentage1 = 0;
	  		       var estimateRevenue1 = document.getElementById(estimateRevenueAmount).value; 
	  		       estimatepasspercentage1 = (estimateRevenue1/roundValue)*100;
	  		    	 estimatepasspercentageRound=Math.round(estimatepasspercentage1);
	  		    	 if(document.getElementById(estimateExpense).value == '' || document.getElementById(estimateExpense).value == 0){
	  		    		document.getElementById(estimatePassPercentage).value='';
	  		    	   }else{
	  		    		 document.getElementById(estimatePassPercentage).value=estimatepasspercentageRound;
	  		    	   } 
	  		       calculateEstimateRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	  		       calculateEstimateLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
	  		        }else{
	  		      	  calEstLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);    
	  		        }
	  		    } 

	   function calculateEstimateRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
	   {
	  			    var estimateQuantity1 =0;
	  			    var estimateDeviation1=100;
	  			    estimateQuantity1=document.getElementById(estimateQuantity).value; 
	  			    var basis1 =document.getElementById(basis).value; 
	  			    var estimateExpense1 =eval(document.getElementById(estimateExpense).value); 
	  			    var estimateRate1=0;
	  			    var estimateRateRound=0; 
	  			    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	  		         estimateQuantity1 =document.getElementById(estimateQuantity).value=1;
	  		         } 
	  		        if( basis1=="cwt" || basis1=="%age"){
	  		       	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
	  		       	  estimateRateRound=Math.round(estimateRate1*10000)/10000;
	  		       	document.getElementById(estimateRate).value=estimateRateRound;	  	
	  		       	}  else if(basis1=="per 1000"){
	  		       	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
	  		       	  estimateRateRound=Math.round(estimateRate1*10000)/10000;
	  		       	document.getElementById(estimateRate).value=estimateRateRound;		  	
	  		       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	  		          estimateRate1=(estimateExpense1/estimateQuantity1); 
	  		       	  estimateRateRound=Math.round(estimateRate1*10000)/10000; 
	  		       	document.getElementById(estimateRate).value=estimateRateRound;		  	
	  			    }
	  			    estimateDeviation1=document.getElementById(estimateDeviation).value 
	  			    if(estimateDeviation1!='' && estimateDeviation1>0){
	  			    	estimateRateRound=estimateRateRound*100/estimateDeviation1;
	  			    	estimateRateRound=Math.round(estimateRateRound*10000)/10000;
	  			    	document.getElementById(estimateRate).value=estimateRateRound;		
	  			    }  }


	  		    function calculateEstimateLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
	  		    {
	  		        var estimateQuantity1 =0;
	  		        var estimateExpense1 =0;
	  		    	    estimateQuantity1=document.getElementById(estimateQuantity).value; 
	  		    	    var basis1 =document.getElementById(basis).value; 
	  		    	    estimateExpense1 =document.getElementById('estLocalAmountNew'+aid).value; 
	  		    	    var estimateRate1=0;
	  		    	    var estimateRateRound=0; 
	  		    	    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	  		             estimateQuantity1 =document.getElementById(estimateQuantity).value=1;
	  		             } 
	  		            if( basis1=="cwt" || basis1=="%age"){
	  		           	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
	  		           	  estimateRateRound=Math.round(estimateRate1*10000)/10000;
	  		           	document.getElementById('estLocalRateNew'+aid).value=estimateRateRound;	  	
	  		           	}  else if(basis1=="per 1000"){
	  		           	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
	  		           	  estimateRateRound=Math.round(estimateRate1*10000)/10000;
	  		           	document.getElementById('estLocalRateNew'+aid).value=estimateRateRound;		  	
	  		           	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	  		              estimateRate1=(estimateExpense1/estimateQuantity1); 
	  		           	  estimateRateRound=Math.round(estimateRate1*10000)/10000; 
	  		           	document.getElementById('estLocalRateNew'+aid).value=estimateRateRound;		  	
	  		    	    }  
	  		            estimateDeviation1=document.getElementById(estimateDeviation).value 
	  		    	    if(estimateDeviation1!='' && estimateDeviation1>0){
	  		    	    	estimateRateRound=estimateRateRound*100/estimateDeviation1;
	  		    	    	estimateRateRound=Math.round(estimateRateRound*10000)/10000;
	  		    	    	document.getElementById('estLocalRateNew'+aid).value=estimateRateRound;		
	  		    	    }
	  		     }


	  function calculateEstimateExpenseByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
	  { 
	  		  	   var estLocalAmount1= document.getElementById('estimatePayableContractRateAmmountNew'+aid).value ;
	  		  	   var estExchangeRate1= document.getElementById('estimatePayableContractExchangeRateNew'+aid).value*1 ;
	  		  	   var roundValue=0; 
	  		  	  	 if(estExchangeRate1 ==0) {
	  		  	  	document.getElementById(estimateExpense).value=0*1;
	  		  	     } else if(estExchangeRate1 >0) {
	  		  	       var amount1=estLocalAmount1/estExchangeRate1; 
	  		  	       roundValue=Math.round(amount1*10000)/10000;
	  		  	   document.getElementById(estimateExpense).value=roundValue ; 
	  		  	     }
	  		  	     var estimatepasspercentageRound=0;
	  		  	     var estimatepasspercentage1 = 0;
	  		  	     var estimateRevenue1 = document.getElementById(estimateRevenueAmount).value; 
	  		  	     estimatepasspercentage1 = (estimateRevenue1/roundValue)*100;
	  		  	  	 estimatepasspercentageRound=Math.round(estimatepasspercentage1);
	  		  	  	 if(document.getElementById(estimateExpense).value == '' || document.getElementById(estimateExpense).value == 0){
	  		  	  	document.getElementById(estimatePassPercentage).value='';
	  		  	  	   }else{
	  		  	  		document.getElementById(estimatePassPercentage).value=estimatepasspercentageRound;
	  		  	  	   }  
	  		  		 }	
	  
	  /* function getFieldValueForSave1(id){
		  var categoryType = document.getElementById('category'+id).value;
		  var chargeCode = document.getElementById('chargeCode'+id).value;
		  var vendorCode = document.getElementById('vendorCode'+id).value;
		  var vendorName = document.getElementById('estimateVendorName'+id).value;
		  var basis = document.getElementById('basis'+id).value;
		  var estimateQuantity = document.getElementById('estimateQuantity'+id).value;
		  var estimateRate = document.getElementById('estimateRate'+id).value;
		  var estimateSellRate = document.getElementById('estimateSellRate'+id).value;
		  var estimateExpense = document.getElementById('estimateExpense'+id).value;
		  var estimatePassPercentage = document.getElementById('estimatePassPercentage'+id).value;
		  var estimateRevenueAmount = document.getElementById('estimateRevenueAmount'+id).value;
		  var quoteDescription = document.getElementById('quoteDescription'+id).value;
		  
		  var displayOnQuote = document.getElementById('displayOnQuote'+id).value;
		  var additionalService = document.getElementById('additionalService'+id).value;
		  var statusCheck = document.getElementById('statusCheck'+id).value;
		  
		  var url1 = "&id="+id+"&categoryType="+categoryType+"&chargeCode="+chargeCode+"&vendorCode="+vendorCode+"&vendorName="+vendorName;
		  url1 = url1+"&basis1="+basis+"&estQuantity="+estimateQuantity+"&estRate="+estimateRate+"&estSellRate="+estimateSellRate+"&estExpense="+estimateExpense;
		  url1 = url1+"&estPassPercentage="+estimatePassPercentage+"&estRevenueAmount="+estimateRevenueAmount;
		  url1 = url1+"&quoteDescription="+quoteDescription+"&displayOnQuote="+displayOnQuote+"&additionalService="+additionalService+"&statusCheck="+statusCheck;
		  
		  var url="autoSavePricingAjax.html?ajax=1&decorator=simple&popup=true"+url1;
		  
		  return url;
	  } */
	  
	  function calculateRevenueNew(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid){
	  	   var revenueValue=0;
	 	    var revenueRound=0;
	 	    var oldRevenue=0;
	 	    var oldRevenueSum=0;
	 	    var balanceRevenue=0;
	 	    oldRevenue=eval(document.getElementById(revenue).value);
	 	    <c:if test="${not empty serviceOrder.id}">
	 	    oldRevenueSum=eval(document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value);
	 	    </c:if>
	 	    var basisValue = document.getElementById(basis).value;
	 	    checkFloatNew('serviceOrderForm',quantity,'Nothing');
	 	    var quantityValue = document.getElementById(quantity).value;
		    <c:if test="${contractType}"> 
		    checkFloatNew('serviceOrderForm','estimateSellQuantity'+aid,'Nothing');
		    quantityValue = document.getElementById('estimateSellQuantity'+aid).value;
		    </c:if>
	 	    
	 	    checkFloatNew('serviceOrderForm',sellRate,'Nothing');
	 	    var sellRateValue = document.getElementById(sellRate).value;
	 	    if(basisValue=="cwt" || basisValue== "%age"){
	 	    revenueValue=(quantityValue*sellRateValue)/100 ;
	 	    }
	 	    else if(basisValue=="per 1000"){
	 	    revenueValue=(quantityValue*sellRateValue)/1000 ;
	 	    }else{
	 	    revenueValue=(quantityValue*sellRateValue);
	 	    } 
	 	    revenueRound=Math.round(revenueValue*10000)/10000;
	 	    balanceRevenue=revenueRound-oldRevenue;
	 	    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
	 	    balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
	 	    oldRevenueSum=oldRevenueSum+balanceRevenue; 
	 	    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
	 	    var revenueRoundNew=""+revenueRound;
	 	    if(revenueRoundNew.indexOf(".") == -1){
	 	    revenueRoundNew=revenueRoundNew+".00";
	 	    } 
	 	    if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
	 	    revenueRoundNew=revenueRoundNew+"0";
	 	    }
	 	   document.getElementById(revenue).value=revenueRoundNew;
	 	    try{
	 	        var buyRate1=document.getElementById('estimateSellRate'+aid).value;
	 	        var estCurrency1=document.getElementById('estSellCurrencyNew'+aid).value;
	 			if(estCurrency1.trim()!=""){
	 					var Q1=0;
	 					var Q2=0;
	 					Q1=buyRate1;
	 					Q2=document.getElementById('estSellExchangeRateNew'+aid).value;
	 		            var E1=Q1*Q2;
	 		            E1=Math.round(E1*10000)/10000;
	 		           document.getElementById('estSellLocalRateNew'+aid).value=E1;
	 		            var estLocalAmt1=0;
	 		            if(basisValue=="cwt" || basisValue== "%age"){
	 		            	estLocalAmt1=(quantityValue*E1)/100 ;
	 		                }
	 		                else if(basisValue=="per 1000"){
	 		                	estLocalAmt1=(quantityValue*E1)/1000 ;
	 		                }else{
	 		                	estLocalAmt1=(quantityValue*E1);
	 		                }	 
	 		            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
	 		           document.getElementById('estSellLocalAmountNew'+aid).value=estLocalAmt1;     
	 			}
	 			<c:if test="${contractType}">
	 				try{
	 					var estimateContractExchangeRate=0;
						estimateContractExchangeRate=document.getElementById('estimateContractExchangeRateNew'+aid).value; 
						var QbuyRate1=0;
						QbuyRate1=buyRate1;
						document.getElementById('estimateContractRateNew'+aid).value=QbuyRate1*estimateContractExchangeRate
	 					var Q11=0;
	 					var Q21=0;
	 					Q11=document.getElementById(quantity).value;
					    <c:if test="${contractType}"> 
					    Q11 = document.getElementById('estimateSellQuantity'+aid).value;
					    </c:if>	 					
	 					Q21=document.getElementById('estimateContractRateNew'+aid).value;
	 		            var E11=Q11*Q21;
	 		            var estLocalAmt11=0;
	 		            if(basisValue=="cwt" || basisValue== "%age"){
	 		            	estLocalAmt11=E11/100 ;
	 		                }
	 		                else if(basisValue=="per 1000"){
	 		                	estLocalAmt11=E11/1000 ;
	 		                }else{
	 		                	estLocalAmt11=E11;
	 		                }	 
	 		            estLocalAmt11=Math.round(estLocalAmt11*10000)/10000;    
	 		           document.getElementById('estimateContractRateAmmountNew'+aid).value=estLocalAmt11;
	 				}catch(e){}     
	 			</c:if>
	 	    }catch(e){}
	 	    <c:if test="${not empty serviceOrder.id}">
	 	    var newRevenueSum=""+oldRevenueSum;
	 	    if(newRevenueSum.indexOf(".") == -1){
	 	    newRevenueSum=newRevenueSum+".00";
	 	    } 
	 	    if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
	 	    newRevenueSum=newRevenueSum+"0";
	 	    } 
	 	   document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value=newRevenueSum;
	 	    </c:if>
	 	    if(revenueRoundNew>0){
	 	    	document.getElementById(displayOnQuots).checked = true;
	 	    }
	 	    try{
	 	    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		    calculateVatAmt('REVENUE',aid);

	 	    </c:if>
	 	    }catch(e){} 	  
	 	   calculateGrossMargin();
	 	 }
	  function checkCompanyDiv(id){
		  var companyDivision = document.getElementById('companyDivision'+id).value;
		  if(companyDivision==null || companyDivision==undefined || companyDivision.trim()==''){
			  alert("Company Division is required field");
			  document.getElementById('companyDivision'+id).value=document.getElementById('tempCompanyDivision'+id).value;
		  }else if(document.getElementById('payAccDate'+id).value!='' || document.getElementById('recAccDate'+id).value!='') { 	 
				alert('You can not change the CompanyDivision as Sent To Acc has been already filled');
				document.getElementById('companyDivision'+id).value=document.getElementById('tempCompanyDivision'+id).value;
		  }else{
		  }	
	  }
	  function selectPricingMarketPlace(category,id){
		  var  hidMarketPlace='hidMarketPlace'+id;
			 var  hidMarketPlace11='hidMarketPlace11'+id;
		      var val = document.getElementById(category).value;
		      var pricingFlag='${pricePointFlag}';
		      var e2 = document.getElementById(hidMarketPlace);
		      var e3 = document.getElementById(hidMarketPlace11);
		       if(pricingFlag=='true' && (val=='Origin' || val=='Destin')){
		    	   e2.style.display='block'; 
		    	   e3.style.display='none';
		       }else{
		    	   e2.style.display='none';
		    	   e3.style.display='none';
		    	   }
       		  }
	  function marketPlaceCode(vendorCode,category,aid,vendorName,estimateRate,contractType){		 	
		  var val = document.getElementById(category).value;
		  var mode=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
		  var packingMode=document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
		  var jobType=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
		  var weight="";
		  var volume="";
		  var storageUnit='${weightType}';
		 // var storageUnit='lbscft';
		  var soid='<%=request.getParameter("id") %>';		 
		  if(storageUnit=='lbscft'){
			//weight=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;
		   // volume=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value;
		    weight=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;
		    volume=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;
		  }else if(storageUnit=='kgscbm'){
			//weight=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value;
			//volume=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value;
			weight=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value;
			volume=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value;
		  }
		  var countryCode="";
		  var packService="";
		  if(val=='Origin'){
			  countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value
		  }else if(val=='Destin'){
			  countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value
		  }
		  var pricePointexpired='${pricePointexpired}';		 
		  if(pricePointexpired=='true' || pricePointexpired==true){
			  if(jobType=='STO'){
			if(weight!='' && volume!=''){
			  window.open('getMarketArea.html?packingMode='+packingMode+'&jobType='+jobType+'&vendorCodeId='+vendorCode+'&vendorNameId='+vendorName+'&estimateRateid='+estimateRate+'&marketCountryCode='+countryCode+'&packService='+val+'&pricingMode='+mode+'&pricingWeight='+weight+'&pricingVolume='+volume+'&pricingStorageUnit='+storageUnit+'&contractType='+contractType+'&aid='+aid+'&soid='+soid+'&decorator=popup&popup=true','myWindow','width=950,height=600,left=100');
			 }else{
				alert("Please Enter the Following Data - Estimated Weight and Volume..."); 
			 }
			  }else{
			  if(mode!='' && weight!='' && volume!='' && packingMode!='' ){			  
			  if(mode=='Air'){
			  window.open('getMarketArea.html?packingMode='+packingMode+'&jobType='+jobType+'&vendorCodeId='+vendorCode+'&vendorNameId='+vendorName+'&estimateRateid='+estimateRate+'&marketCountryCode='+countryCode+'&packService='+val+'&pricingMode='+mode+'&pricingWeight='+weight+'&pricingVolume='+volume+'&pricingStorageUnit='+storageUnit+'&contractType='+contractType+'&aid='+aid+'&soid='+soid+'&decorator=popup&popup=true','myWindow','width=950,height=600,left=100');
			  }else if(mode=='Sea'){
				  if(packingMode=='RORO' || packingMode=='GRP' || packingMode=='BKBLK' || packingMode=='LCL' || packingMode=='LVAN' || packingMode=='T/W' || packingMode=='LOOSE' || packingMode=='LVCO' || packingMode=='LVCNT' || packingMode=='LVCNTR'){
					  packService="LCL";
					  }
				  if(packService!=''){				  
			     window.open('getMarketArea.html?packingMode='+packingMode+'&jobType='+jobType+'&vendorCodeId='+vendorCode+'&vendorNameId='+vendorName+'&estimateRateid='+estimateRate+'&marketCountryCode='+countryCode+'&packService='+val+'&pricingMode='+mode+'&pricingWeight='+weight+'&pricingVolume='+volume+'&pricingStorageUnit='+storageUnit+'&contractType='+contractType+'&aid='+aid+'&soid='+soid+'&decorator=popup&popup=true','myWindow','width=950,height=600,left=100');
				  }else{
				alert("The Selected Pack Mode is not Supported by Price Point!");
				  }
			  }else{
			   alert("The Selected Mode is not supported by Price Point!");
			  }	
		  }else{
		    alert("Please Enter the Following Data - Mode, Estimated Weight, Estimated Volume and Packing Mode ...");
		  }
	      }
		  }else{
		   alert("Your PricePoint subscription has expired. \n Please send email to support@redskymobility.com for renewal of the subscription.");  
		  }
		  
	  }
	
	function updatePricPoint(accId,pricePointReadId,pricePointTariffId){
		  var agree =confirm("Are You Sure To Update Quote Detail");
		    if(agree) {
		showOrHide(1);
		//alert("accId"+accId)
		//alert("pricePointReadId"+pricePointReadId)
		//alert("pricePointTariffId"+pricePointTariffId)
		var soid='<%=request.getParameter("id") %>';
		//alert(soid)	
		$.get("updatePricPointAjax.html?ajax=1&decorator=simple&popup=true", 
				{aid: accId,priceReadId:pricePointReadId,priceTariffId:pricePointTariffId,soid:soid},
				function(data){					
					findAllPricing();
			});
		    }else{
		    }
	}
	function findPartnerDetailsPriceing(nameId,CodeId,DivId,partnerTypeWhereClause,id,evt){
		 var keyCode = evt.which ? evt.which : evt.keyCode;  
		 if(keyCode!=9){
		var nameValue=document.getElementById(nameId).value;
		//document.getElementById(CodeId).value="";
		if(nameValue.length<=1){
			document.getElementById(CodeId).value="";	
			}
		if(nameValue.length>=3){
			$.get("partnerDetailsPricingAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", 
					{partnerNameAutoCopmlete: nameValue,partnerNameId:nameId,paertnerCodeId:CodeId,autocompleteDivId:DivId,autocompleteCondition:partnerTypeWhereClause,pricingAccountLineId:id},
					function(data){
						 document.getElementById(DivId).style.display = "block";
						$("#"+DivId).html(data);
						   //var partnerDetails= document.getElementById('partnerDetailsAutoCompleteListSize').value;
			   				//if(partnerDetails=='true'){
			   				//	document.getElementById(nameId).value="";
			   				//	document.getElementById(CodeId).value="";
			   				//}
				});	
		     }else{
			document.getElementById(DivId).style.display = "none";	
		    }
		}else{
			 document.getElementById(DivId).style.display = "none";
		}
	}
	 function closeMypricingDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
			document.getElementById(autocompleteDivId).style.display = "none";	
			if(document.getElementById(paertnerCodeId).value==''){
				document.getElementById(partnerNameId).value="";	
			}
			}

			function copyPartnerPricingDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId,pricingAccountLineId){
				lastName=lastName.replace("~","'");
				document.getElementById(partnerNameId).value=lastName;
				document.getElementById(paertnerCodeId).value=partnercode;
				document.getElementById(autocompleteDivId).style.display = "none";	
				fillCurrencyByChargeCode(pricingAccountLineId);
			}
/**********************************************************************/
 			function calculateEstimateByChargeCodeBlock(aid,type1){
			    //Code For estimate Est	 
				 estimateBuyByChargeCodeFX(aid);
			    //Code For estimate Sell	 
				  <c:if test="${multiCurrency=='Y'}">
				  estimateSellByChargeCodeFX(aid);
				  </c:if>
				  //Code For Estimate Pass Percentage   
				  try{
					  calculateestimatePassPercentage('estimateRevenueAmount'+aid,'estimateExpense'+aid,'estimatePassPercentage'+aid);
				  }catch(e){}
				  // Code Gross Section
				  	var estimatedTotalRevenue=0.00;
					var estimatedTotalExpense=0.00;
					var tempEstimatedTotalExpense=0.00;
					var tempEstimatedTotalRevenue=0.00;
					var estimatedGrossMargin=0.00;
					var estimatedGrossMarginPercentage=0.00;
					var aidListIds=document.getElementById('aidListIds').value;
					var accArr=aidListIds.split(",");
					for(var g=0;g<accArr.length;g++){
						if(document.getElementById('estimateRevenueAmount'+accArr[g]).value!=""){
				 			tempEstimatedTotalRevenue=document.getElementById('estimateRevenueAmount'+accArr[g]).value;
						}
				 		estimatedTotalRevenue=estimatedTotalRevenue+parseFloat(tempEstimatedTotalRevenue);
				 		if(document.getElementById('estimateExpense'+accArr[g]).value!=""){
				 			tempEstimatedTotalExpense=document.getElementById('estimateExpense'+accArr[g]).value;
				 		}
				 		estimatedTotalExpense=estimatedTotalExpense+parseFloat(tempEstimatedTotalExpense);
					}
				estimatedGrossMargin=estimatedTotalRevenue-estimatedTotalExpense;
				try{
					if((estimatedTotalRevenue==0)||(estimatedTotalRevenue==0.0)||(estimatedTotalRevenue==0.00)){
						estimatedGrossMarginPercentage=0.00;
					}else{
						estimatedGrossMarginPercentage=(estimatedGrossMargin*100)/estimatedTotalRevenue;
					}
				}catch(e){
					estimatedGrossMarginPercentage=0.00;
					}
				 <c:if test="${not empty serviceOrder.id}">
				document.getElementById('estimatedTotalRevenue'+${serviceOrder.id}).value=Math.round(estimatedTotalRevenue*10000)/10000;
				document.getElementById('estimatedTotalExpense'+${serviceOrder.id}).value=Math.round(estimatedTotalExpense*10000)/10000;
				document.getElementById('estimatedGrossMargin'+${serviceOrder.id}).value=Math.round(estimatedGrossMargin*10000)/10000;
				document.getElementById('estimatedGrossMarginPercentage'+${serviceOrder.id}).value=Math.round(estimatedGrossMarginPercentage*10000)/10000;
				</c:if>	 
				 <c:if test="${systemDefaultVatCalculationNew=='Y'}">
				    calculateVatAmt('REVENUE',aid);
				    calculateVatAmt('EXPENSE',aid);
				 </c:if>
			}
			function estimateBuyByChargeCodeFX(aid){
					var roundVal=0.00;
					var estimatePayableContractCurrency=document.getElementById('estimatePayableContractCurrencyNew'+aid).value;		
					var estimatePayableContractExchangeRate=document.getElementById('estimatePayableContractExchangeRateNew'+aid).value;
					var estimatePayableContractRate=document.getElementById('estimatePayableContractRateNew'+aid).value;
					var estimatePayableContractRateAmmount=document.getElementById('estimatePayableContractRateAmmountNew'+aid).value;
					var estimateCurrency=document.getElementById('estCurrencyNew'+aid).value;
					var estimateExchangeRate=document.getElementById('estExchangeRateNew'+aid).value;
					var estimateLocalRate=document.getElementById('estLocalRateNew'+aid).value;
					var estimateLocalAmount=document.getElementById('estLocalAmountNew'+aid).value;
					
				 	if(estimatePayableContractCurrency!=''){
				 		roundVal=estimatePayableContractRate/estimatePayableContractExchangeRate;
				 		document.getElementById('estimateRate'+aid).value=roundVal;
				 		if(estimateCurrency!=''){
					 		document.getElementById('estLocalRateNew'+aid).value=roundVal*estimateExchangeRate;
					 	}
				 		roundVal=estimatePayableContractRateAmmount/estimatePayableContractExchangeRate;
				 		document.getElementById('estimateExpense'+aid).value=roundVal;
				 		if(estimateCurrency!=''){
				 		document.getElementById('estLocalAmountNew'+aid).value=roundVal*estimateExchangeRate;
				 		}
				 	}
			}
			function estimateSellByChargeCodeFX(aid){
					var roundVal=0.00;
					var estimateContractCurrency=document.getElementById('estimateContractCurrencyNew'+aid).value;		
					var estimateContractExchangeRate=document.getElementById('estimateContractExchangeRateNew'+aid).value;
					var estimateContractRate=document.getElementById('estimateContractRateNew'+aid).value;
					var estimateContractRateAmmount=document.getElementById('estimateContractRateAmmountNew'+aid).value;
					var estimateSellCurrency=document.getElementById('estSellCurrencyNew'+aid).value;		
					var estimateSellExchangeRate=document.getElementById('estSellExchangeRateNew'+aid).value;
					var estimateSellLocalRate=document.getElementById('estSellLocalRateNew'+aid).value;
					var estimateSellLocalAmount=document.getElementById('estSellLocalAmountNew'+aid).value;
					
				 	if(estimateContractCurrency!=''){
				 		roundVal=estimateContractRate/estimateContractExchangeRate;
				 		document.getElementById('estimateSellRate'+aid).value=roundVal;
				 		if(estimateSellCurrency!=''){
					 		document.getElementById('estSellLocalRateNew'+aid).value=roundVal*estimateSellExchangeRate;
					 	}
				 		roundVal=estimateContractRateAmmount/estimateContractExchangeRate;
				 		document.getElementById('estimateRevenueAmount'+aid).value=roundVal;
				 		if(estimateSellCurrency!=''){
				 		document.getElementById('estSellLocalAmountNew'+aid).value=roundVal*estimateSellExchangeRate;
				 		}
				 	}
			}
 
			
			function autoCompleterAjaxCallChargeCode(id,divid,idCondition){
				<configByCorp:fieldVisibility componentId="component.charges.Autofill.chargeCode">
				 document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value="charge";
				 document.getElementById("chargeCodeValidationMessage").value="";
				 	var ChargeName = document.getElementById('chargeCode'+id).value;
					var contract =document.forms['serviceOrderForm'].elements['billing.contract'].value;
					var idval='chargeCode'+id;
					contract=contract.trim();
					var category =  document.getElementById('category'+id).value;
					var accountCompanyDivision = document.getElementById('companyDivision'+id).value;
					var originCountry=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
					var destinationCountry=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
					var mode;		
					if(document.forms['serviceOrderForm'].elements['serviceOrder.mode']!=undefined){
						 mode=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value; 
						 }else{
							 mode="";
							 }
					var pageCondition="";
					if(category=='Internal Cost'){
				 		pageCondition='AccountInternalCategory';
				 	}
				 	if(category!='Internal Cost'){
				 		pageCondition='AccountNonInternalCategory';
				 	}
					if(contract!=""){
						if(ChargeName.length>=3){
				            $.get("ChargeCodeAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", {
				            	searchName:ChargeName,
				            	contract: contract,
				            	chargeCodeId:idval,
				            	autocompleteDivId: divid,
				            	id:id,
				            	idCondition:idCondition,
				            	searchcompanyDivision:accountCompanyDivision,
				            	originCountry:originCountry,
				            	destinationCountry:destinationCountry,
				            	serviceMode:mode,
				            	categoryCode:category,
				            	pageCondition:pageCondition,
				            	costElementFlag:${costElementFlag}
				            }, function(idval) {
				                document.getElementById(divid).style.display = "block";
				                $("#" + divid).html(idval)
				            })
				        } else {
				            document.getElementById(divid).style.display = "none"
				        }
					}
					else{
						alert("There is No Billing Contract: Please select.");
						document.getElementById("contract"+id).focus();
					}
					</configByCorp:fieldVisibility>	
			 }
			 function getHTTPObjectInternalCostVendorCode() {
				    var xmlhttp;
				    if(window.XMLHttpRequest)  {
				        xmlhttp = new XMLHttpRequest();
				    }
				    else if (window.ActiveXObject)
				    {
				        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				        if (!xmlhttp) {
				            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
				        } }
				    return xmlhttp;
				}
				var httpInternalCost = getHTTPObjectInternalCostVendorCode(); 
				function findInternalCostVendorCode(aid){
					var vendorCode='vendorCode'+aid;
					var estimateVendorName='estimateVendorName'+aid;
				    var rev = document.getElementById('category'+aid).value;
				    var companyDivision = document.getElementById('companyDivision'+aid).value; 
				    if(rev == "Internal Cost"){
				    	document.getElementById(vendorCode).value=""; 
				    	document.getElementById(estimateVendorName).value="";  
					    var url="findInternalCostVendorCode.html?ajax=1&decorator=simple&popup=true&companyDivision="+encodeURI(companyDivision);
					    httpInternalCost.open("GET", url, true);
					    httpInternalCost.onreadystatechange = function(){ httpInternalCostVendorCode(vendorCode,estimateVendorName,aid);};
					    httpInternalCost.send(null);
				   } }
				function httpInternalCostVendorCode(vendorCode,estimateVendorName,aid) { 
				    if (httpInternalCost.readyState == 4)  {
				        var result= httpInternalCost.responseText
				        result = result.trim();   
				        if(result !=""){
				        	document.getElementById(vendorCode).value= result;
				        	checkVendorName(vendorCode,estimateVendorName);
				        	fillCurrencyByChargeCode(aid);
				         }  } } 			
			 function copyChargeDetails(chargecode,chargecodeId,autocompleteDivId,id,idCondition){
						document.getElementById(chargecodeId).value=chargecode;
						document.getElementById(autocompleteDivId).style.display = "none";
						document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value='';
						fillDiscription('quoteDescription'+id,'chargeCode'+id,'category'+id,'recGl'+id,'payGl'+id,'contractCurrency'+id,'contractExchangeRate'+id,'contractValueDate'+id,'payableContractCurrency'+id,'payableContractExchangeRate'+id,'payableContractValueDate'+id,'accChargeCodeTemp'+id,id);
						checkChargeCode('chargeCode'+id,'category'+id,id);
						findInternalCostVendorCode(id);
					}
			 function closeMyChargeDiv(autocompleteDivId,chargecode,chargecodeId,id,idCondition){
						document.getElementById(autocompleteDivId).style.display = "none";
						 if(document.getElementById(chargecodeId).value==''){
							document.getElementById("quoteDescription"+id).value="";	
						}
						 	document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value='';
							fillDiscription('quoteDescription'+id,'chargeCode'+id,'category'+id,'recGl'+id,'payGl'+id,'contractCurrency'+id,'contractExchangeRate'+id,'contractValueDate'+id,'payableContractCurrency'+id,'payableContractExchangeRate'+id,'payableContractValueDate'+id,'accChargeCodeTemp'+id,id);
							checkChargeCode('chargeCode'+id,'category'+id,id);
						}
			 function closeMyChargeDiv2(chargecodeId,id){
					 if(document.getElementById(chargecodeId).value==''){
						document.getElementById("quoteDescription"+id).value="";	
					}
					 	document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value='';
					 	document.forms['serviceOrderForm'].elements['chargeCodeValidationMessage'].value='NothingData';
					}

			 function enablePreviousFunction(chargeCode,category,id,idCondition){
		    	 if(document.getElementById("chargeCodeValidationMessage").value!='NothingData'){
		    	 	document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value='';
					document.getElementById('ChargeNameDivId'+id).style.display = "none";
					fillDiscription('quoteDescription'+id,'chargeCode'+id,'category'+id,'recGl'+id,'payGl'+id,'contractCurrency'+id,'contractExchangeRate'+id,'contractValueDate'+id,'payableContractCurrency'+id,'payableContractExchangeRate'+id,'payableContractValueDate'+id,'accChargeCodeTemp'+id,id);
					checkChargeCode('chargeCode'+id,'category'+id,id);
		    	 }
			}
 /*********************************************************************/		
</script>
