<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>SO Piece List </b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>



<display:table name="cartonSO" class="table" requestURI="" id="cartonList" export="false" defaultsort="1" pagesize="100" style="width:99%; margin-left:5px;margin-top:2px;">   
     <display:column title="Piece#"><a href="#" onclick="goToUrlChild(${cartonList.id});" ><c:out value="${cartonList.idNumber}" /></a></display:column>
    <display:column property="cntnrNumber"  titleKey="carton.cntnrNumber"  style="width:70px;"/>
    <display:column property="cartonType"  titleKey="carton.cartonType"  style="width:100px"/>
  	<display:column property="pieces"  title="Pieces"  style="width:70px;text-align: right"/>
</display:table>   