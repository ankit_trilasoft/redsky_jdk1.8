<%@ include file="/common/taglibs.jsp"%> 

<head>
	<meta name="heading" content="<fmt:message key='payrollAllocationForm.title'/>"/> 
	<title><fmt:message key="payrollAllocationForm.title"/></title>
	
<script language="javascript" type="text/javascript">
	function changeStatus(){
	document.forms['payrollAllocationForm'].elements['formStatus'].value = '1';
    }
	
	function autoSave(clickType){		
	if(!(clickType == 'save')){
		if ('${autoSavePrompt}' == 'No'){

			var noSaveAction = '<c:out value="${payrollAllocation.id}"/>';

			var id1 = document.forms['payrollAllocationForm'].elements['payrollAllocation.id'].value;

			if(document.forms['payrollAllocationForm'].elements['gotoPageString'].value == 'gototab.payrollList'){

			noSaveAction = 'payrollAllocations.html';

			}
			
		processAutoSave(document.forms['payrollAllocationForm'], 'savepayrollAllocation!saveOnTabChange.html', noSaveAction);

		}else{
	var id1 = document.forms['payrollAllocationForm'].elements['payrollAllocation.id'].value;	
	if (document.forms['payrollAllocationForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='payrollAllocationForm.title'/>");
		if(agree){
			document.forms['payrollAllocationForm'].action = 'savepayrollAllocation!saveOnTabChange.html';
			document.forms['payrollAllocationForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['payrollAllocationForm'].elements['gotoPageString'].value == 'gototab.payrollAList'){
				location.href = 'payrollAllocations.html';
				}
		    }
		}
	}else{
	if(id1 != ''){
		if(document.forms['payrollAllocationForm'].elements['gotoPageString'].value == 'gototab.payrollAList'){
				location.href = 'payrollAllocations.html';
				}
	}
	}
}
}
}
	function checkJobAndService(){
		var jobValue = document.forms['payrollAllocationForm'].elements['payrollAllocation.job'].value;
		var serviceValue = document.forms['payrollAllocationForm'].elements['payrollAllocation.service'].value;
		if(jobValue.trim()==''){
			alert('Please select Job.');
			return false;
		}else if(serviceValue.trim()==''){
			alert('Please select Services.');
			return false;
		}else{
			return true;
		}
	}
	
</script>	
</head>
<body style="background-color:#444444;">
<s:form id="payrollAllocationForm" name="payrollAllocationForm" action="savepayrollAllocation" onsubmit="return checkJobAndService()" method="post" validate="true">
  <s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:choose>
<c:when test="${gotoPageString == 'gototab.payrollAList' }">
    <c:redirect url="/payrollAllocations.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>     

<div id="Layer1" style="width:80%" onkeydown="changeStatus();">

<div id="newmnav">
		  <ul>
		  	  <li><a href="payrollAllocations.html"><span>Payroll Allocation List</span></a></li>
		     <!--<li><a onclick="setReturnString('gototab.payrollAList');return autoSave();"><span>Payroll Allocation List</span></a></li>
		     --><li id="newmnav1" style="background:#FFF "><a class="current"><span>Payroll Allocation Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
	
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" ><span></span></div>
   <div class="center-content">
  	<table class="listwhitetext" border="0" cellpadding="2">
		  <tbody>  	
		  	<tr>
		  		<td align="left" height="3px" width="80px"></td>
		  		<td align="left" colspan="3"></td>
		  	</tr>
		  	<tr>
		  		<td align="right"  width="100px"><fmt:message key="payrollAllocationForm.code"/></td>
		  		<td align="left" colspan="3"><s:select cssClass="list-menu"   name="payrollAllocation.code" list="%{payroll}" headerKey=" " headerValue=" " cssStyle="width:310px" onchange="changeStatus();" tabindex="1"/></td> 
			</tr> 
		  	<tr>
		  		<td align="right" ><fmt:message key="payrollAllocationForm.job"/><font color="red" size="2">*</font></td>
		  		<td align="left" colspan="3"><s:select cssClass="list-menu"   name="payrollAllocation.job" list="%{job}" headerKey=" " headerValue=" " cssStyle="width:310px" onchange="changeStatus();" tabindex="2"/></td> 
 		    </tr>
		  	    <tr>
		  	   
		  		<td align="right"><fmt:message key="payrollAllocationForm.service"/><font color="red" size="2">*</font></td>
		  		<td align="left" colspan="3"><s:select cssClass="list-menu"   name="payrollAllocation.service" headerKey=" " headerValue=" " list="%{tcktservc}" cssStyle="width:310px" onchange="changeStatus();" tabindex="3"/></td> 
		  		
		  	</tr> 
		  	
		  	 <tr>
		  	   
		  		<td align="right"><fmt:message key="payrollAllocationForm.bucket"/></td>
		  		<td align="left" colspan="3"><s:textfield name="payrollAllocation.bucket"  maxlength="25" size="56"  cssClass="input-text" readonly="false" tabindex="4"/></td>
		  		
		  	</tr> 
		  	 <tr>
			  	 <td align="right"><fmt:message key="payrollAllocationForm.calculation"/></td>
			  	 <td align="left" colspan="3"><s:select cssClass="list-menu"   name="payrollAllocation.calculation" list="%{revcalculation}" headerKey=" " headerValue=" " cssStyle="width:310px" onchange="changeStatus();" tabindex="5"/></td> 
		  	</tr> 
		  	
		  	<tr>
		  		<td align="right"><fmt:message key="payrollAllocationForm.revenueRate1"/></td>
		  		<td align="left" colspan="3"><s:textfield name="payrollAllocation.revenueRate1" cssStyle="text-align:right"  maxlength="25" size="20"  cssClass="input-text" readonly="false" tabindex="6"/></td>
		  		
		  	</tr> 
		  	<tr>
		  		<td align="right"><fmt:message key="payrollAllocationForm.revenueRate2"/></td>
		  		<td align="left" colspan="3"><s:textfield name="payrollAllocation.revenueRate2" cssStyle="text-align:right" maxlength="25" size="20"  cssClass="input-text" readonly="false" tabindex="7"/></td>
		  		
		  	</tr> 
		  	<tr>
		  		<td align="right">Rate Type</td>
		  		<td align="left"><s:select cssClass="list-menu"   name="payrollAllocation.rateType" list="%{prate}" headerKey=" " headerValue=" " cssStyle="width:130px" onchange="changeStatus();" tabindex="8"/></td>
		  	</tr>
		  	<tr>	
		  		<td align="right"><fmt:message key="payrollform.companyDivision"/></td>
		  		<td align="left"><s:select cssClass="list-menu"   name="payrollAllocation.companyDivision" headerKey=" " headerValue=" " list="%{compDevision}" cssStyle="width:195px" onchange="changeStatus();" tabindex="9"/></td> 
		  	</tr>
		  	<tr>	
		  		<td align="right">Mode</td>
		  		<td align="left"><s:select cssClass="list-menu" name="payrollAllocation.mode" list="%{mode}" cssStyle="width:130px" onchange="changeStatus();"/></td> 
		  	</tr>  
		  	<tr>
		  		<td align="left" height="13px" ></td>
		  		
		  	</tr>
		 </tbody>
	</table> 
		 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
 		  <table class="detailTabLabel" border="0" style="width:700px">
				<tbody>
					<tr>
						<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='payrollAllocation.createdOn'/></b></td>
							<td style="width:200px">
							<fmt:formatDate var="payrollAllocationCreatedOnFormattedValue" value="${payrollAllocation.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="payrollAllocation.createdOn" value="${payrollAllocationCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${payrollAllocation.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='payrollAllocation.createdBy' /></b></td>
							<c:if test="${not empty payrollAllocation.id}">
								<s:hidden name="payrollAllocation.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{payrollAllocation.createdBy}"/></td>
							</c:if>
							<c:if test="${empty payrollAllocation.id}">
								<s:hidden name="payrollAllocation.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='payrollAllocation.updatedOn'/></b></td>
							<fmt:formatDate var="payrollAllocationupdatedOnFormattedValue" value="${payrollAllocation.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="payrollAllocation.updatedOn" value="${payrollAllocationupdatedOnFormattedValue}"/>
							<td style="width:200px"><fmt:formatDate value="${payrollAllocation.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='payrollAllocation.updatedBy' /></b></td>
							<c:if test="${not empty payrollAllocation.id}">
								<s:hidden name="payrollAllocation.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{payrollAllocation.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty payrollAllocation.id}">
								<s:hidden name="payrollAllocation.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
						</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table>
		  
		  <table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save" />  
        		</td>
       
                <td align="right">
        		<c:if test="${not empty payrollAllocation.id}">
        		<input type="button" class="cssbutton1" style="width:40px;"  value="Add" onclick="location.href='<c:url value="/payrollAllocationForm.html"/>'" /> 
        		 </c:if>
	           
        		</td><td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
        		
       	  	</tr>		  	
		  </tbody>
		  </table>
</div>
<s:hidden name="payrollAllocation.id"/>

</s:form>