<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>

<%Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";
while(it.hasNext()) {
    role=(Role)it.next();
    if(userRole.isEmpty()){
    	 userRole = role.getName();
    }else{
         userRole = userRole+role.getName();
}
}
%>
<head>   
    <title>SO Quick View</title>   
    <meta name="heading" content="SO Quick View"/>   
 
 
<style type="text/css">

/* collapse */

div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>   
 <script language="JavaScript">

</script> 
</head>  
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
<script language="javascript" type="text/javascript"><%@ include file="/common/formCalender.js"%></script>    
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
 <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" /> 
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<s:form id="soQuickViewForm" action="soQuickViewForm">
<c:set var="rolesAssignFlag" value="N" />
<c:set var="rolesAssign" value="${roles}" />
<c:if test="${fn:contains(rolesAssign, 'ROLE_SUPERVISOR')}">
<c:set var="rolesAssignFlag" value="Y" />
</c:if>
<c:set var="modeWeight" value="" />
<c:set var="modeVolumeDesc" value="" />
<c:if test="${serviceOrder.mode=='Air'}">
<c:set var="modeWeight" value="${miscellaneous.actualGrossWeight}" />
<c:set var="modeVolumeDesc" value="${miscellaneous.actualCubicFeet}" />
</c:if>
<c:if test="${serviceOrder.mode!='Air' && serviceOrder.mode!=''}">
<c:set var="modeWeight" value="${miscellaneous.actualNetWeight}" />
<c:set var="modeVolumeDesc" value="${miscellaneous.netActualCubicFeet}" />
</c:if>
<c:set var="soCoordinator" value="${fn:trim(serviceOrder.coordinator)}" />
<c:set var="appUserName" value="${fn:trim(userName)}" />
<c:set var="soCoordinator" value="${fn:toUpperCase(soCoordinator)}" />
<c:set var="appUserName" value="${fn:toUpperCase(appUserName)}" />	

 <div id="Layer4" style="width:750px;">
 <div id="newmnav" style="float:left;">   

 <ul>
  <li id="newmnav1" style="background:#FFF "><a class="current"><span>SO Quick View</span></a></li>
</ul>

</div>
<%-- <table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;height:22px;float: none; "><tr>
<td width="20px" align="left" style="padding-left:3px;vertical-align: bottom; padding-bottom: 1px;">
<a><img class="openpopup" onclick="" src="${pageContext.request.contextPath}/images/Shipper-Ico.jpg" alt="Shippers Detail" title="Shippers Detail" /></a> 
</td>
</tr>
</table> --%>		
<div class="spn">&nbsp;</div>

</div>
<div id="Layer1"  style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
	<table style="margin:0 0 0 20px; padding:0px;width:96%;" cellpadding="3" cellspacing="0" >
					<tbody>
						<tr>											
							<td align="" class="listwhitetext">SO#</td>
							<td align="" class="listwhitetext">First Name</td>
							<td align="" class="listwhitetext">Last Name</td>
							<td align="" class="listwhitetext">Origin City</td>
							<td align="" class="listwhitetext">Origin State</td>
							<td align="" class="listwhitetext">Origin Country</td>
							<td align="" class="listwhitetext">
             					<c:if test="${serviceOrder.vip}">
             						<div style="position:absolute;top:-15px;">
            							<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
            						</div>
            					</c:if>
            				</td>
							</tr>				
							
							<tr>											
							<td><s:textfield cssClass="input-textUpper" name="serviceOrder.shipNumber" cssStyle="width:120px" readonly="true" /></td>
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.firstName" cssStyle="width:120px" readonly="true" /></td>
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.lastName" cssStyle="width:120px" readonly="true"/></td>
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.originCity" cssStyle="width:120px" readonly="true"/></td>
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.originState" cssStyle="width:120px" readonly="true"/></td>
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.originCountry" cssStyle="width:120px" readonly="true"/></td>
							</tr>
							<tr><td style="height:5px;"></td></tr>
							
							<tr>
							<td align="" class="listwhitetext">Bill to Party</td>
							<td align="" class="listwhitetext">Booking Agent Name</td>
							<td align="" class="listwhitetext">Coordinator</td>
							<td align="" class="listwhitetext">Destination City</td>
							<td align="" class="listwhitetext">Destination State</td>
							<td align="" class="listwhitetext">Destination Country</td>
						</tr>
						<tr>
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.billToName" cssStyle="width:120px" readonly="true"/></td>
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.bookingAgentName" cssStyle="width:120px" readonly="true"/></td>
							<td><s:textfield cssClass="input-textUpper" name="serviceOrder.coordinator" cssStyle="width:120px" readonly="true" /></td>												
							<td><s:textfield cssClass="input-textUpper" name="serviceOrder.destinationCity" cssStyle="width:120px" readonly="true" /></td>
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.destinationState" cssStyle="width:120px" readonly="true" /></td>
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.destinationCountry" cssStyle="width:120px" readonly="true"/></td>
						</tr>
						<tr><td style="height:5px;"></td></tr>
							<tr>
							
							<td align="" class="listwhitetext">Sales Person</td>
							<td align="" class="listwhitetext">Service</td>
							<td align="" class="listwhitetext">Origin Agent</td>
							<td align="" class="listwhitetext">Destination Agent</td>	
							<td align="" class="listwhitetext">Quoted Amount</td>
							<td align="" class="listwhitetext">Total Invoice Amount</td>	
							</tr>
							<tr>	
							
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.salesMan" cssStyle="width:120px" readonly="true" /></td>
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.serviceType" cssStyle="width:120px" readonly="true" /></td>
							<td><s:textfield cssClass="input-textUpper upper-case" name="trackingStatus.originAgent" cssStyle="width:120px" readonly="true"/></td>
							<td><s:textfield cssClass="input-textUpper upper-case" name="trackingStatus.destinationAgent" cssStyle="width:120px" readonly="true"/></td>
							<td><s:textfield cssClass="input-textUpper" name="serviceOrder.estimatedTotalRevenue" cssStyle="width:120px" readonly="true" /></td>
							<td><s:textfield cssClass="input-textUpper" name="serviceOrder.projectedActualRevenue" cssStyle="width:120px" readonly="true" /></td>
							
						</tr>
					<c:if test="${serviceOrder.job!='RLO'}">						
						<tr><td style="height:5px;"></td></tr>
						<tr>											
							<td align="" class="listwhitetext">Packing Date (Target)</td>
							<td align="" class="listwhitetext">Packing Date (Actual)</td>
							<td align="" class="listwhitetext">Loading Date (Target)</td>
							<td align="" class="listwhitetext">Loading Date (Actual)</td>
							<td align="" class="listwhitetext">Delivery Date (Target)</td>
							<td align="" class="listwhitetext">Delivery Date (Actual)</td>
						</tr>
						<tr>											
							<td>
								<fmt:parseDate pattern="yyyy-MM-dd" value="${trackingStatus.beginPacking}" var="parsedBeginPacking" />
								<fmt:formatDate pattern="dd-MMM-yy" value="${parsedBeginPacking}" var="formattedBeginPacking" />
								<input type="text" class="input-textUpper" name="trackingStatus.beginPacking" value="${formattedBeginPacking}" cssStyle="width:120px" readonly="true" />
							</td>
							<td>
								<fmt:parseDate pattern="yyyy-MM-dd" value="${trackingStatus.packA}" var="parsedPackA" />
								<fmt:formatDate pattern="dd-MMM-yy" value="${parsedPackA}" var="formattedPackA" />
								<input type="text" class="input-textUpper" name="trackingStatus.packA" value="${formattedPackA}" cssStyle="width:120px" readonly="true" />
							</td>
							<td>
								<fmt:parseDate pattern="yyyy-MM-dd" value="${trackingStatus.beginLoad}" var="parsedBeginLoad" />
								<fmt:formatDate pattern="dd-MMM-yy" value="${parsedBeginLoad}" var="formattedBeginLoad" />
								<input type="text" class="input-textUpper" name="trackingStatus.beginLoad" value="${formattedBeginLoad}" cssStyle="width:120px" readonly="true" />
							</td>
							<td>
								<fmt:parseDate pattern="yyyy-MM-dd" value="${trackingStatus.loadA}" var="parsedLoadA" />
								<fmt:formatDate pattern="dd-MMM-yy" value="${parsedLoadA}" var="formattedLoadA" />
								<input type="text" class="input-textUpper" name="trackingStatus.loadA" value="${formattedLoadA}" cssStyle="width:120px" readonly="true" />
							</td>
							<td>
								<fmt:parseDate pattern="yyyy-MM-dd" value="${trackingStatus.deliveryShipper}" var="parsedDeliveryShipper" />
								<fmt:formatDate pattern="dd-MMM-yy" value="${parsedDeliveryShipper}" var="formattedDeliveryShipper" />
								<input type="text" class="input-textUpper" name="trackingStatus.deliveryShipper" cssStyle="width:120px" value="${formattedDeliveryShipper}" readonly="true" />
							</td>
							<td>
								<fmt:parseDate pattern="yyyy-MM-dd" value="${trackingStatus.deliveryA}" var="parsedDeliveryA" />
								<fmt:formatDate pattern="dd-MMM-yy" value="${parsedDeliveryA}" var="formattedDeliveryA" />
								<input type="text" class="input-textUpper" name="trackingStatus.deliveryA" cssStyle="width:120px" value="${formattedDeliveryA}" readonly="true" /></td>
						</tr>
					</c:if>
					
					<tr><td style="height:3px;"></td></tr>
						<tr>											
							<td align="" class="listwhitetext">Transit&nbsp;Days</td>
							<td align="" class="listwhitetext">Requested&nbsp;Delivery&nbsp;Date</td>
							<td align="" class="listwhitetext">Source</td>
							<td align="" class="listwhitetext">Pack&nbsp;Mode</td>
							<td align="" class="listwhitetext">ETD</td>
							<td align="" class="listwhitetext">ETA</td>
						</tr>
						<tr>											
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.transitDays" cssStyle="width:120px" readonly="true" /></td>
							<td>
							<fmt:parseDate pattern="yyyy-MM-dd" value="${trackingStatus.targetdeliveryShipper}" var="parsedtargetdeliveryShipper" />
								<fmt:formatDate pattern="dd-MMM-yy" value="${parsedtargetdeliveryShipper}" var="formattedtargetdeliveryShipper" />
								<input type="text" class="input-textUpper" name="" cssStyle="width:120px" value="${formattedtargetdeliveryShipper}" readonly="true" />
							</td>
							<td><s:textfield cssClass="input-textUpper upper-case" value="${source}" cssStyle="width:120px" readonly="true"/></td>
							<td><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.packingMode" cssStyle="width:120px" readonly="true"/></td>
							<td>
								<fmt:parseDate pattern="yyyy-MM-dd" value="${etDepart}" var="parsedetDepart" />
								<fmt:formatDate pattern="dd-MMM-yy" value="${parsedetDepart}" var="formattedEtDepart" />
								<input type="text" class="input-textUpper" name="" cssStyle="width:120px" value="${formattedEtDepart}" readonly="true" />
							</td>
							<td>
							<fmt:parseDate pattern="yyyy-MM-dd" value="${etArrival}" var="parsedetArrival" />
								<fmt:formatDate pattern="dd-MMM-yy" value="${parsedetArrival}" var="formattedEtArrival" />
								<input type="text" class="input-textUpper" name="" cssStyle="width:120px" value="${formattedEtArrival}" readonly="true" />
							</td>
						</tr>
						
						<tr><td style="height:3px;"></td></tr>
						<tr>											
							<td align="" class="listwhitetext">Container&nbsp;Number</td>
							<td align="" class="listwhitetext">Estimated&nbsp;Weight</td>
							<td align="" class="listwhitetext">Actual&nbsp;Weight</td>
						</tr>
						<tr>											
							<td><s:textfield cssClass="input-textUpper" value="${containerNumber}" cssStyle="width:120px" readonly="true" /></td>
							<td><s:textfield cssClass="input-textUpper" name="miscellaneous.estimatedNetWeight" cssStyle="width:120px" readonly="true" /></td>
							<td><s:textfield cssClass="input-textUpper" name="miscellaneous.actualNetWeight" cssStyle="width:120px" readonly="true"/></td>
						</tr>
					
					<configByCorp:fieldVisibility componentId="component.accountline.accrualbutton.show">
					<tr><td style="height:0px;"></td></tr>
					<tr>	
					<c:choose>
							<c:when test="${appUserName==soCoordinator || rolesAssignFlag=='Y'}">
							<c:if test="${serviceOrder.billToCode!='' && customerFile.accountCode!='' && modeWeight!='' && modeVolumeDesc!='' && billing.insuranceHas!=''}">
								<c:if test="${serviceOrder.billToCode!=null && customerFile.accountCode!=null && modeWeight!=null && modeVolumeDesc!=null && billing.insuranceHas!=null}">
								<c:if test="${not empty (serviceOrder.billToCode && customerFile.accountCode && modeWeight && modeVolumeDesc && billing.insuranceHas)}">										
								<td align="" class="listwhitetext" id="hideTd1"></td>
								</c:if>
								</c:if>
								</c:if>
								<c:if test="${empty serviceOrder.billToCode || empty customerFile.accountCode || empty modeWeight || empty modeVolumeDesc || empty billing.insuranceHas}">
								<td align="" class="listwhitetext" id="hideTd1"></td>
								</c:if>
								</c:when>
								<c:otherwise>
								<td align="" class="listwhitetext" id="hideTd1"></td>
								</c:otherwise>
								</c:choose>	
								<td colspan="2">
     						<table cellspacing="0" cellpadding="0" style="margin:0px;padding:0px"><tr>						
							<td colspan="0" class="listwhitetext">Accrual Ready Date </td>
							<td width="45"></td>
							<td style="padding-left:4px;" class="listwhitetext">Accrual Updated By </td>
							</tr>
							</table>
							</td>
							<td align="" class="listwhitetext"></td>
							<td align="" class="listwhitetext"></td>
							<td align="" class="listwhitetext"></td>
						</tr>
						
						 <tr>
						 <c:choose>
							<c:when test="${appUserName==soCoordinator || rolesAssignFlag=='Y'}">
							<c:if test="${serviceOrder.billToCode!='' && customerFile.accountCode!='' && modeWeight!='' && modeVolumeDesc!='' && billing.insuranceHas!=''}">
								<c:if test="${serviceOrder.billToCode!=null && customerFile.accountCode!=null && modeWeight!=null && modeVolumeDesc!=null && billing.insuranceHas!=null}">
								<c:if test="${not empty (serviceOrder.billToCode && customerFile.accountCode && modeWeight && modeVolumeDesc && billing.insuranceHas)}">
    								<td rowspan="2"  style="vertical-align:top" id="hideTd1">
    								<input type="button" id="accrualReady" value="Accrual Ready &#8827;" style="width:122px;height:21px;"
                                        class="cssbutton1" onclick="fillField('${serviceOrder.id}','${serviceOrder.corpID}');" theme="simple" />
                           			</td>
								</c:if>
								</c:if>
								</c:if>
								<c:if test="${empty serviceOrder.billToCode || empty customerFile.accountCode || empty modeWeight || empty modeVolumeDesc || empty billing.insuranceHas}">
    								<td rowspan="2"  style="vertical-align:top" id="hideTd1">
    									<input type="button" id="accrualReady" value="Accrual Ready &#8827;" style="width:122px;height:21px;"
                                        class="cssbutton1" onclick="javascript:alert('You can not change the Accrual Ready Date.')" theme="simple" />
                           			</td>
								</c:if>
							</c:when>
						<c:otherwise>
							<td rowspan="2"  style="vertical-align:top" id="hideTd1">
    							<input type="button" id="accrualReady" value="Accrual Ready &#8827;" style="width:122px;height:21px;"
                                        class="cssbutton1" onclick="javascript:alert('You can not change the Accrual Ready Date.')" theme="simple" />
                           	</td>
						</c:otherwise>
						</c:choose>
						<c:if test="${not empty serviceOrder.accrualReadyDate}">
						<td colspan="1">
     						<table cellspacing="0" cellpadding="0" style="margin:0px;padding:0px"><tr>
						
    								<fmt:parseDate pattern="yyyy-MM-dd" value="${serviceOrder.accrualReadyDate}" var="accrualReadyDateFill" />
                                  	<fmt:formatDate pattern="dd-MMM-yy" value="${accrualReadyDateFill}" var="accrualReadyDate" />
    								<c:if test="${rolesAssignFlag!='Y'}">
    								<td><s:textfield id="accrualReadyDate" cssClass="input-textUpper" name="serviceOrder.accrualReadyDate" value="${accrualReadyDate}" cssStyle="width:120px" maxlength="13" readonly="true" /></td>
    								</c:if>
    								<c:if test="${rolesAssignFlag=='Y'}">
    									<td><s:textfield id="accrualReadyDate" cssClass="input-textUpper" name="serviceOrder.accrualReadyDate" value="${accrualReadyDate}" cssStyle="width:72px" maxlength="13" onkeydown="return onlyDel(event,this)" /></td>
    								</c:if>
    								<c:if test="${rolesAssignFlag=='Y'}">
    								    <c:if test="${empty serviceOrder.billToCode || empty customerFile.accountCode || empty modeWeight || empty modeVolumeDesc || empty billing.insuranceHas}">
    										<td><img id="accrualReadyDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the Accrual Ready Date.')"/></td>
    									</c:if>
    									<c:if test="${serviceOrder.billToCode!='' && customerFile.accountCode!='' && modeWeight!='' && modeVolumeDesc!='' && billing.insuranceHas!=''}">
    									<c:if test="${serviceOrder.billToCode!=null && customerFile.accountCode!=null && modeWeight!=null && modeVolumeDesc!=null && billing.insuranceHas!=null}">
										<c:if test="${not empty (serviceOrder.billToCode && customerFile.accountCode && modeWeight && modeVolumeDesc && billing.insuranceHas)}">
											<td><img id="accrualReadyDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</c:if>
										</c:if>
										</c:if>
    								</c:if>
    								<c:if test="${rolesAssignFlag=='N'}">
    								<td width="18"></td>
    								</c:if>
    				
    					</tr>
    					</table>
    					</td>
    						</c:if>
    					<td colspan="2">
     						<table cellspacing="0" cellpadding="0" style="margin:0px;padding:0px"><tr>
     					<c:if test="${empty serviceOrder.accrualReadyDate}">
     						<td><s:textfield cssClass="input-textUpper" id="accrualReadyDate" name="serviceOrder.accrualReadyDate" cssStyle="width:120px" maxlength="11" readonly="true" /></td>
    					<c:if test="${rolesAssignFlag=='Y'}">
    									<c:if test="${empty serviceOrder.billToCode || empty customerFile.accountCode || empty modeWeight || empty modeVolumeDesc || empty billing.insuranceHas}">
    										<td><img id="accrualReadyDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the Accrual Ready Date.')"/></td>
    									</c:if>
    									<c:if test="${serviceOrder.billToCode!='' && customerFile.accountCode!='' && modeWeight!='' && modeVolumeDesc!='' && billing.insuranceHas!=''}">
    									<c:if test="${serviceOrder.billToCode!=null && customerFile.accountCode!=null && modeWeight!=null && modeVolumeDesc!=null && billing.insuranceHas!=null}">
										<c:if test="${not empty (serviceOrder.billToCode && customerFile.accountCode && modeWeight && modeVolumeDesc && billing.insuranceHas)}">
											<td><img id="accrualReadyDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</c:if>
										</c:if>
										</c:if>
    					</c:if> 
    					<c:if test="${rolesAssignFlag=='N'}">
    					<td width="18"></td>
    					</c:if>
    					</c:if>
                        <td><s:textfield cssClass="input-textUpper" id="accrualReadyUpdatedBy" name="serviceOrder.accrualReadyUpdatedBy" cssStyle="width:120px;margin-left:3px;" maxlength="11" readonly="true" /></td>
                        </tr></table>
    					</td>
                        </tr>
                        
						
					</configByCorp:fieldVisibility>
					<tr><td style="height:15px;"></td></tr>
					</tbody>	
				</table>
				</div>
				<div class="bottom-header"><span></span></div>
			</div>
		</div>
	</div>
 </s:form> 
 <script language="JavaScript">
         
		function fillField(id, sessionCorpID) {
			var rolassinflag='${appUserName==soCoordinator}';
			var accrualDate=document.getElementById("accrualReadyDate").value;
			<c:choose>
			 <c:when test="${rolesAssignFlag!='Y'}">
				if(rolassinflag=='true' && accrualDate!=''){
					alert("You can not change the Accrual Ready Date,It has already filled.");
					return false;
				}else{
				$.get("autoFillAccrualFields.html?ajax=1&decorator=simple&popup=true", 
						{sid:id,sessionCorpID:sessionCorpID,accrualDate:accrualDate,roleSupervisor:''},
						function(data){
							result = data.trim();
							if(data.length<500){
							document.getElementById("accrualReadyUpdatedBy").value = data.split("~")[0].trim();
							document.getElementById("accrualReadyDate").value = data.split("~")[1].trim();
							}
						});
				}
				</c:when>
				<c:otherwise>
				$.get("autoFillAccrualFields.html?ajax=1&decorator=simple&popup=true", 
						{sid:id,sessionCorpID:sessionCorpID,accrualDate:accrualDate,roleSupervisor:'Y'},
						function(data){
							result = data.trim();
							if(data.length<500){
							document.getElementById("accrualReadyUpdatedBy").value = data.split("~")[0].trim();
							document.getElementById("accrualReadyDate").value = data.split("~")[1].trim();
							}
						});
				</c:otherwise>
			 </c:choose>
		}
		function checkCoordJob() {
			<%if(userRole.contains("ROLE_SUPERVISOR")){ %> 
            var shipNumber = '${serviceOrder.shipNumber}';
            var sessionCorpID = '${serviceOrder.corpID}';
            $.get("checkJobForCoordinator.html?ajax=1&decorator=simple&popup=true", 
                        {sessionCorpID:sessionCorpID,shipNumber:shipNumber},
                        function(data){
                            if(data.trim() == 'Show'){
                                // document.getElementById('accrualReady').style.visibility = 'visible'
                                 document.getElementById('hideTd1').style.display = "block";
                                 document.getElementById('hideTd').style.display = "block";
                            }else{
                                 document.getElementById('hideTd1').style.display = "none";
                                 document.getElementById('hideTd').style.display = "none";
                            }
                    });
            <%} %> 
        }
        //checkCoordJob();
		setOnSelectBasedMethods([]);
		setCalendarFunctionality();
	</script>