package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="roleaccess")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

public class RoleAccess extends BaseObject{
	
	 private String corpID;
	 private Long id;
	 private String role;
	 private String menu1;
	 private String menu2;
	 private String menu3;
	 private String menu4;
	 private String menu5;
	 private String menu6;
	 private String menu7;
	 private String action;
	 private String createdBy;
	 private Date createdOn;
	 private String updatedBy;
	 private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("role", role).append("menu1", menu1).append("menu2",
				menu2).append("menu3", menu3).append("menu4", menu4).append(
				"menu5", menu5).append("menu6", menu6).append("menu7", menu7)
				.append("action", action).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof RoleAccess))
			return false;
		RoleAccess castOther = (RoleAccess) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(role, castOther.role).append(menu1,
				castOther.menu1).append(menu2, castOther.menu2).append(menu3,
				castOther.menu3).append(menu4, castOther.menu4).append(menu5,
				castOther.menu5).append(menu6, castOther.menu6).append(menu7,
				castOther.menu7).append(action, castOther.action).append(
				createdBy, castOther.createdBy).append(createdOn,
				castOther.createdOn).append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(role)
				.append(menu1).append(menu2).append(menu3).append(menu4)
				.append(menu5).append(menu6).append(menu7).append(action)
				.append(createdBy).append(createdOn).append(updatedBy).append(
						updatedOn).toHashCode();
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMenu1() {
		return menu1;
	}
	public void setMenu1(String menu1) {
		this.menu1 = menu1;
	}
	public String getMenu2() {
		return menu2;
	}
	public void setMenu2(String menu2) {
		this.menu2 = menu2;
	}
	public String getMenu3() {
		return menu3;
	}
	public void setMenu3(String menu3) {
		this.menu3 = menu3;
	}
	public String getMenu4() {
		return menu4;
	}
	public void setMenu4(String menu4) {
		this.menu4 = menu4;
	}
	public String getMenu5() {
		return menu5;
	}
	public void setMenu5(String menu5) {
		this.menu5 = menu5;
	}
	public String getMenu6() {
		return menu6;
	}
	public void setMenu6(String menu6) {
		this.menu6 = menu6;
	}
	public String getMenu7() {
		return menu7;
	}
	public void setMenu7(String menu7) {
		this.menu7 = menu7;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

}
