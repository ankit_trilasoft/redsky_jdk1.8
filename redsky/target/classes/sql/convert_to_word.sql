DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`CONVERT_TO_WORD` $$
CREATE DEFINER=`root`@`%` FUNCTION `CONVERT_TO_WORD`(n1 DOUBLE,cur char(10)) RETURNS varchar(300) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
BEGIN
DECLARE onee char(10);
DECLARE two char(10);
DECLARE three char(10);
DECLARE four char(10);
DECLARE five char(10);
DECLARE six char(10);
DECLARE seven char(10);
DECLARE eight char(10);
DECLARE nine char(10);
DECLARE ten char(10);
DECLARE Eleven char(10);
DECLARE Twelve char(10);
DECLARE thirteen char(10);
DECLARE fourteen char(10);
DECLARE fifteen char(10);
DECLARE sixteen char(10);
DECLARE seventeen char(10);
DECLARE eighteen char(10);
DECLARE nineteen char(10);
DECLARE twenty char(10);
DECLARE thirty char(10);
DECLARE forty char(10);
DECLARE fifty char(10);
DECLARE sixty char(10);
DECLARE seventy char(10);
DECLARE eighty char(10);
DECLARE ninety char(10);
DECLARE hundred char(10);
DECLARE onehundred char(30);
DECLARE thousand char(10);
DECLARE lakh char(10);
DECLARE onethousand char(20);
DECLARE onelakh char(10);
DECLARE TEnlakh char(10);
DECLARE onecrore char(10);
DECLARE crore char(10);
DECLARE digitCount int;
DECLARE temp int ;
Declare temp1 int;
Declare unit int;
Declare deci int;
Declare hun int;
Declare thous1 char(20);
Declare thous char(20);
Declare Tthous char(20);
Declare temp2 int;
Declare unit1 char(10);
Declare deci1 char(10);
Declare hun1 char(10);
Declare lakh1 char(10);
Declare lakh3 char(10);
Declare lakh2 int;
Declare TENlakh2 INT;
Declare TENlakh1 char(20);
Declare TENlakh3 char(20);
declare n2 char(10);
declare deci4 char(10);
declare unit4 char(10);
declare paise char(10);
declare temp4 int;
declare word1 int;
declare Rupees char(10);
declare temp7 int;
declare n3 int;
declare n4 int;

SET onee = 'One';
SET two = 'Two';
SET three = 'Three';
SET four = 'Four';
SET five = 'Five';
SET six = 'Six';
SET seven = 'Seven';
SET eight = 'Eight';
SET nine = 'Nine';
SET ten = 'Ten';
SET Eleven = 'Eleven';
SET Twelve = 'Twelve';
SET thirteen = 'Thirteen';
SET fourteen = 'Fourteen';
SET fifteen = 'Fifteen';
SET sixteen = 'Sixteen';
SET seventeen = 'Seventeen';
SET eighteen = 'Eighteen';
SET nineteen = 'Nineteen';
SET twenty = 'Twenty';
SET thirty = 'Thirty';
SET forty = 'Forty';
SET fifty = 'Fifty';
SET sixty = 'Sixty';
SET seventy = 'Seventy';
SET eighty = 'Eighty';
SET ninety = 'Ninety';
SET onehundred = 'One Hundred';
SET hundred = 'Hundred';
SET thousand = 'Thousand';
SET onethousand = 'One Thousand';
SET lakh = 'Lakh';
SET onelakh = 'One Lakh';
SET crore = 'crore';
SET Tenlakh='Ten Lakh';
SET onecrore = 'One Crore';
SET digitCount=0;

SET n2 =SUBSTRING_INDEX(n1, '.', -1);
SET n1 =SUBSTRING_INDEX(n1, '.', 1);
SET word1 = CHAR_LENGTH(n2);
IF word1 = 1 THEN
SET n2 =   CONCAT(n2,0);
END IF;
IF word1 > 2 THEN
SET n2 = 0;
END IF;
set n3 = IsNumeric(n1);
set n4 = IsNumeric(n1);
if(n3=0) then
Set n1=0;
end if;
if(n4=0) then
Set n2=0;
end if;
if(n1 > 10000000) then
Set n1=0;
Set n2=0;
end if;
SET temp4=n2;
SET deci4=temp4 DIV 10;
SET unit4=temp4 % 10;
CASE cur
WHEN 'INR'  THEN
set Rupees='Rupees';
set paise='Paise';
WHEN 'USD'  THEN
set Rupees='Dollar';
set paise='Cents';
WHEN 'AUD'  THEN
set Rupees='Dollar';
set paise='Cents';
WHEN 'GBP'  THEN
set Rupees='Pound';
set paise='Pens';
WHEN 'EUR'  THEN
set Rupees='Euro';
set paise='Cents';
WHEN 'AED'  THEN
set Rupees='Dirham';
set paise='Fils';
WHEN 'MYR'  THEN
set Rupees= case when n2>0 then 'and Cents ' else '' end ;
set paise=case when n2>0 then '' else '' end;
else
set Rupees='';
set paise='';
set n2=0;
end case;
CASE n2
WHEN 0 THEN
SET paise='';
SET deci4= '';
SET unit4='';
WHEN 1 THEN
SET deci4= onee;
SET unit4='';
WHEN 2 THEN
SET deci4= two;
SET unit4='';
WHEN 3 THEN
SET deci4= three;
SET unit4='';
WHEN 4 THEN
SET deci4= four;
SET unit4='';
WHEN  5 THEN
SET deci4= five;
SET unit4='';
WHEN 6 THEN
SET deci4= six;
SET unit4='';
WHEN 7 THEN
SET deci4= seven;
SET unit4='';
WHEN  8 THEN
SET deci4= eight;
SET unit4='';
WHEN  9 THEN
SET deci4= nine;
SET unit4='';
WHEN 10 THEN
SET deci4= ten;
SET unit4='';
WHEN 11 THEN
SET deci4= Eleven;
SET unit4='';
WHEN  12 THEN
SET deci4= Twelve;
SET unit4='';
WHEN 13 THEN
SET deci4= thirteen;
SET unit4='';
WHEN  14 THEN
SET deci4= fourteen;
SET unit4='';
WHEN 15 THEN
SET deci4= fifteen;
SET unit4='';
WHEN 16 THEN
SET deci4= sixteen;
SET unit4='';
WHEN 17 THEN
SET deci4= seventeen;
SET unit4='';
WHEN 18 THEN
SET deci4= eighteen;
SET unit4='';
WHEN 19 THEN
SET deci4= nineteen;
SET unit4='';
ELSE
CASE deci4
WHEN 2 THEN
SET deci4= twenty;
WHEN 3 THEN
SET deci4= thirty;
WHEN 4 THEN
SET deci4= forty;
WHEN 5 THEN
SET deci4= fifty;
WHEN 6 THEN
SET deci4= sixty;
WHEN 7 THEN
SET deci4= seventy;
WHEN 8 THEN
SET deci4= eighty;
WHEN 9 THEN
SET deci4= ninety;
END CASE;
END CASE;
CASE unit4
WHEN 1 THEN
SET unit4= onee;
WHEN 2 THEN
SET unit4= two;
WHEN 3 THEN
SET unit4= three;
WHEN 4 THEN
SET unit4= four;
WHEN 5 THEN
SET unit4= five;
WHEN 6 THEN
SET unit4= six;
WHEN 7 THEN
SET unit4= seven;
WHEN 8 THEN
SET unit4= Eight;
WHEN 9 THEN
SET unit4= nine;
WHEN 0 THEN
SET unit4= '';
END CASE;
CASE n1
WHEN 0 THEN
RETURN '';
WHEN 1 THEN
RETURN concat(onee,' ',Rupees);
WHEN  2 THEN
RETURN concat(two,' ',Rupees);
WHEN 3 THEN
RETURN concat(three,' ',Rupees);
WHEN 4 THEN
RETURN concat(four,' ',Rupees);
WHEN  5 THEN
RETURN concat(five,' ',Rupees);
WHEN 6 THEN
RETURN concat(six,' ',Rupees);
WHEN 7 THEN
RETURN concat(seven,' ',Rupees);
WHEN  8 THEN
RETURN concat(eight,' ',Rupees);
WHEN  9 THEN
RETURN concat(nine,' ',Rupees);
WHEN 10 THEN
RETURN concat(ten,' ',Rupees);
WHEN 11 THEN
RETURN concat(Eleven,' ',Rupees);
WHEN  12 THEN
RETURN concat(Twelve,' ',Rupees);
WHEN 13 THEN
RETURN concat(thirteen,' ',Rupees);
WHEN  14 THEN
RETURN concat(fourteen,' ',Rupees);
WHEN 15 THEN
RETURN concat(fifteen,' ',Rupees);
WHEN 16 THEN
RETURN concat(sixteen,' ',Rupees);
WHEN 17 THEN
RETURN concat(seventeen,' ',Rupees);
WHEN 18 THEN
RETURN concat(eighteen,' ',Rupees);
WHEN 19 THEN
RETURN concat(nineteen,' ',Rupees);
WHEN 20 THEN
RETURN concat(twenty,' ',Rupees);
WHEN 30 THEN
RETURN concat(thirty,' ',Rupees);
WHEN 40 THEN
RETURN concat(forty,' ',Rupees);
WHEN 50 THEN
RETURN concat(fifty,' ',Rupees);
WHEN 60 THEN
RETURN concat(sixty,' ',Rupees);
WHEN 70 THEN
RETURN concat(seventy,' ',Rupees);
WHEN  80 THEN
RETURN concat(eighty,' ',Rupees);
WHEN 90 THEN
RETURN concat(ninety,' ',Rupees);
WHEN 100 THEN
RETURN concat(onehundred,' ',Rupees);
WHEN  1000 THEN
RETURN concat(onethousand,' ',Rupees);
WHEN 100000 THEN
RETURN concat(oneLakh,' ',Rupees);
WHEN 1000000 THEN
RETURN concat(TenLakh,' ',Rupees);
WHEN 10000000 THEN
RETURN concat(oneCrore,' ',Rupees);
ELSE
SET temp=n1;
SET temp1=n1;
SET temp2=n1;
SET digitCount=CHAR_LENGTH(temp);
if(digitCount=2) THEN
   IF n1 > 20 THEN
SET deci1=temp DIV 10;
SET unit1=temp1 % 10;
CASE deci1
WHEN 2 THEN
SET deci1= twenty;
WHEN 3 THEN
SET deci1= thirty;
WHEN 4 THEN
SET deci1= forty;
WHEN 5 THEN
SET deci1= fifty;
WHEN 6 THEN
SET deci1= sixty;
WHEN 7 THEN
SET deci1= seventy;
WHEN 8 THEN
SET deci1= eighty;
WHEN 9 THEN
SET deci1= ninety;
ELSE
SET deci1= onee;
END CASE;
CASE unit1
WHEN 1 THEN
SET unit1= onee;
WHEN 2 THEN
SET unit1= two;
WHEN 3 THEN
SET unit1= three;
WHEN 4 THEN
SET unit1= four;
WHEN 5 THEN
SET unit1= five;
WHEN 6 THEN
SET unit1= six;
WHEN 7 THEN
SET unit1= seven;
WHEN 8 THEN
SET unit1= Eight;
WHEN 9 THEN
SET unit1= nine;
END CASE;
RETURN CONCAT(deci1,' ',unit1,' ',Rupees);
END IF;
END IF;
if digitCount=3 THEN
   IF n1 > 100 THEN
SET unit1=temp1 % 10;
SET hun=temp DIV 10;
SET deci=hun % 10;
SET hun=hun DIV 10;
CASE hun
WHEN 1 THEN
SET hun1= onee;
WHEN 2 THEN
SET hun1= two;
WHEN 3 THEN
SET hun1= three;
WHEN 4 THEN
SET hun1= four;
WHEN 5 THEN
SET hun1= five;
WHEN 6 THEN
SET hun1= six;
WHEN 7 THEN
SET hun1= seven;
WHEN 8 THEN
SET hun1= Eight;
WHEN 9 THEN
SET hun1= nine;
END CASE;
IF deci =1 THEN
CASE unit1
WHEN 0 THEN
SET deci1= Ten;
WHEN 1 THEN
SET deci1= Eleven;
WHEN 2 THEN
SET deci1= Twelve;
WHEN 3 THEN
SET deci1= thirteen;
WHEN 4 THEN
SET deci1=fourteen;
WHEN 5 THEN
SET deci1= fifteen;
WHEN 6 THEN
SET deci1= sixteen;
WHEN 7 THEN
SET deci1= seventeen;
WHEN 8 THEN
SET deci1= eighteen;
WHEN 9 THEN
SET deci1= nineteen;
END CASE;
SET unit1= '';
END IF;
IF deci = 0 THEN
CASE unit1
WHEN 1 THEN
SET deci1= onee;
WHEN 2 THEN
SET deci1= two;
WHEN 3 THEN
SET deci1= three;
WHEN 4 THEN
SET deci1=four;
WHEN 5 THEN
SET deci1= five;
WHEN 6 THEN
SET deci1= six;
WHEN 7 THEN
SET deci1= seven;
WHEN 8 THEN
SET deci1= eight;
WHEN 9 THEN
SET deci1= nine;
ELSE
SET deci1='';
END CASE;
SET unit1= '';
END IF;
IF deci > 1 THEN
CASE deci
WHEN 2 THEN
SET deci1= twenty;
WHEN 3 THEN
SET deci1= thirty;
WHEN 4 THEN
SET deci1= forty;
WHEN 5 THEN
SET deci1= fifty;
WHEN 6 THEN
SET deci1= sixty;
WHEN 7 THEN
SET deci1= seventy;
WHEN 8 THEN
SET deci1= Eighty;
WHEN 9 THEN
SET deci1= ninety;
END CASE;
CASE unit1
WHEN 1 THEN
SET unit1= onee;
WHEN 2 THEN
SET unit1= two;
WHEN 3 THEN
SET unit1= three;
WHEN 4 THEN
SET unit1= four;
WHEN 5 THEN
SET unit1= five;
WHEN 6 THEN
SET unit1= six;
WHEN 7 THEN
SET unit1= seven;
WHEN 8 THEN
SET unit1= Eight;
WHEN 9 THEN
SET unit1= nine;
WHEN 0 THEN
CASE deci
WHEN 2 THEN
SET deci1= twenty;
SET unit1='';
WHEN 3 THEN
SET deci1= thirty;
SET unit1='';
WHEN 4 THEN
SET deci1= forty;
SET unit1='';
WHEN 5 THEN
SET deci1= fifty;
SET unit1='';
WHEN 6 THEN
SET deci1= sixty;
SET unit1='';
WHEN 7 THEN
SET deci1= seventy;
SET unit1='';
WHEN 8 THEN
SET deci1= Eighty;
SET unit1='';
WHEN 9 THEN
SET deci1= ninety;
SET unit1='';
END CASE;
END CASE;
END IF;
END IF;
RETURN CONCAT(hun1,' ',hundred,' ',deci1,' ',unit1,' ',Rupees,' ',deci4,' ',unit4,' ',paise);
END IF;
if digitCount=4 THEN
   IF n1 > 1000 THEN
SET unit1=temp1 % 10;
SET hun=temp1 DIV 10;
SET deci=hun % 10;
SET hun=hun DIV 10;
SET thous=hun DIV 10;
SET hun=hun % 10;
CASE thous
WHEN 1 THEN
SET thous1= onee;
WHEN 2 THEN
SET thous1= two;
WHEN 3 THEN
SET thous1= three;
WHEN 4 THEN
SET thous1= four;
WHEN 5 THEN
SET thous1= five;
WHEN 6 THEN
SET thous1= six;
WHEN 7 THEN
SET thous1= seven;
WHEN 8 THEN
SET thous1= Eight;
WHEN 9 THEN
SET thous1= nine;
END CASE;
CASE hun
WHEN 1 THEN
SET hun1= onee;
WHEN 2 THEN
SET hun1= two;
WHEN 3 THEN
SET hun1= three;
WHEN 4 THEN
SET hun1= four;
WHEN 5 THEN
SET hun1= five;
WHEN 6 THEN
SET hun1= six;
WHEN 7 THEN
SET hun1= seven;
WHEN 8 THEN
SET hun1= Eight;
WHEN 9 THEN
SET hun1= nine;
WHEN 0 THEN
SET hun1 ='';
SET hundred='';
END CASE;
IF deci = 1 THEN
CASE unit1
WHEN 0 THEN
SET deci1= Ten;
WHEN 1 THEN
SET deci1= Eleven;
WHEN 2 THEN
SET deci1= Twelve;
WHEN 3 THEN
SET deci1= thirteen;
WHEN 4 THEN
SET deci1=fourteen;
WHEN 5 THEN
SET deci1= fifteen;
WHEN 6 THEN
SET deci1= sixteen;
WHEN 7 THEN
SET deci1= seventeen;
WHEN 8 THEN
SET deci1= eighteen;
WHEN 9 THEN
SET deci1= nineteen;
END CASE;
SET unit1= '';
END IF;
IF deci = 0 THEN
CASE unit1
WHEN 1 THEN
SET deci1= onee;
WHEN 2 THEN
SET deci1= two;
WHEN 3 THEN
SET deci1= three;
WHEN 4 THEN
SET deci1=four;
WHEN 5 THEN
SET deci1= five;
WHEN 6 THEN
SET deci1= six;
WHEN 7 THEN
SET deci1= seven;
WHEN 8 THEN
SET deci1= eight;
WHEN 9 THEN
SET deci1= nine;
ELSE
SET deci1='';
END CASE;
SET unit1= '';
END IF;
IF deci > 1 THEN
CASE deci
WHEN 2 THEN
SET deci1= twenty;
WHEN 3 THEN
SET deci1= thirty;
WHEN 4 THEN
SET deci1= forty;
WHEN 5 THEN
SET deci1= fifty;
WHEN 6 THEN
SET deci1= sixty;
WHEN 7 THEN
SET deci1= seventy;
WHEN 8 THEN
SET deci1= Eighty;
WHEN 9 THEN
SET deci1= ninety;
END CASE;
CASE unit1
WHEN 1 THEN
SET unit1= onee;
WHEN 2 THEN
SET unit1= two;
WHEN 3 THEN
SET unit1= three;
WHEN 4 THEN
SET unit1= four;
WHEN 5 THEN
SET unit1= five;
WHEN 6 THEN
SET unit1= six;
WHEN 7 THEN
SET unit1= seven;
WHEN 8 THEN
SET unit1= Eight;
WHEN 9 THEN
SET unit1= nine;
WHEN 0 THEN
CASE deci
WHEN 2 THEN
SET deci1= twenty;
SET unit1='';
WHEN 3 THEN
SET deci1= thirty;
SET unit1='';
WHEN 4 THEN
SET deci1= forty;
SET unit1='';
WHEN 5 THEN
SET deci1= fifty;
SET unit1='';
WHEN 6 THEN
SET deci1= sixty;
SET unit1='';
WHEN 7 THEN
SET deci1= seventy;
SET unit1='';
WHEN 8 THEN
SET deci1= Eighty;
SET unit1='';
WHEN 9 THEN
SET deci1= ninety;
SET unit1='';
END CASE;
END CASE;
END IF;
END IF;
RETURN CONCAT(thous1,' ',thousand,' ',hun1,' ',hundred,' ',deci1,' ',unit1,' ',Rupees,' ',deci4,' ',unit4,' ',paise);
END IF;
if digitCount=5 THEN
   IF n1 > 9999 THEN
SET unit1=temp1 % 10;
SET hun=temp1 DIV 10;
SET deci=hun % 10;
SET hun=hun DIV 10;
SET thous=hun DIV 10;
SET Tthous=hun DIV 10;
SET thous=Tthous % 10;
SET Tthous=Tthous DIV 10;
SET hun=hun % 10;
CASE Tthous
WHEN 1 THEN
CASE thous
WHEN 0 THEN
SET thous1= Ten;
SET Tthous='';
WHEN 1 THEN
SET thous1= Eleven;
SET Tthous='';
WHEN 2 THEN
SET thous1= Twelve;
SET Tthous='';
WHEN 3 THEN
SET thous1= thirteen;
SET Tthous='';
WHEN 4 THEN
SET thous1= fourteen;
SET Tthous='';
WHEN 5 THEN
SET thous1= fifteen;
SET Tthous='';
WHEN 6 THEN
SET thous1= sixteen;
SET Tthous='';
WHEN 7 THEN
SET thous1= seventeen;
SET Tthous='';
WHEN 8 THEN
SET Tthous='';
SET thous1= eighteen;
WHEN 9 THEN
SET Tthous='';
SET thous1= nineteen;
SET Tthous='';
ELSE
SET thous1='';
END CASE;
WHEN 2 THEN
SET thous1= twenty;
WHEN 3 THEN
SET thous1= thirty;
WHEN 4 THEN
SET thous1= forty;
WHEN 5 THEN
SET thous1= fifty;
WHEN 6 THEN
SET thous1= sixty;
WHEN 7 THEN
SET thous1= seventy;
WHEN 8 THEN
SET thous1= Eighty;
WHEN 9 THEN
SET thous1= ninety;
END CASE;
IF Tthous > 1 THEN
CASE thous
WHEN 0 THEN
SET Tthous= '';
WHEN 1 THEN
SET Tthous= onee;
WHEN 2 THEN
SET Tthous= two;
WHEN 3 THEN
SET Tthous= three;
WHEN 4 THEN
SET Tthous= four;
WHEN 5 THEN
SET Tthous= five;
WHEN 6 THEN
SET Tthous= six;
WHEN 7 THEN
SET Tthous= seven;
WHEN 8 THEN
SET Tthous= Eight;
WHEN 9 THEN
SET Tthous= nine;
END CASE;
END IF;
IF Tthous = 1 THEN
SET Tthous= '';
END IF;
CASE hun
WHEN 1 THEN
SET hun1= onee;
WHEN 2 THEN
SET hun1= two;
WHEN 3 THEN
SET hun1= three;
WHEN 4 THEN
SET hun1= four;
WHEN 5 THEN
SET hun1= five;
WHEN 6 THEN
SET hun1= six;
WHEN 7 THEN
SET hun1= seven;
WHEN 8 THEN
SET hun1= Eight;
WHEN 9 THEN
SET hun1= nine;
WHEN 0 THEN
SET hun1 ='';
SET hundred='';
END CASE;
IF deci = 1 THEN
CASE unit1
WHEN 0 THEN
SET deci1= Ten;
WHEN 1 THEN
SET deci1= Eleven;
WHEN 2 THEN
SET deci1= Twelve;
WHEN 3 THEN
SET deci1= thirteen;
WHEN 4 THEN
SET deci1=fourteen;
WHEN 5 THEN
SET deci1= fifteen;
WHEN 6 THEN
SET deci1= sixteen;
WHEN 7 THEN
SET deci1= seventeen;
WHEN 8 THEN
SET deci1= eighteen;
WHEN 9 THEN
SET deci1= nineteen;
END CASE;
SET unit1= '';
END IF;
IF deci = 0 THEN
CASE unit1
WHEN 1 THEN
SET deci1= onee;
WHEN 2 THEN
SET deci1= two;
WHEN 3 THEN
SET deci1= three;
WHEN 4 THEN
SET deci1=four;
WHEN 5 THEN
SET deci1= five;
WHEN 6 THEN
SET deci1= six;
WHEN 7 THEN
SET deci1= seven;
WHEN 8 THEN
SET deci1= eight;
WHEN 9 THEN
SET deci1= nine;
ELSE
SET deci1='';
END CASE;
SET unit1= '';
END IF;
IF deci > 1 THEN
CASE deci
WHEN 2 THEN
SET deci1= twenty;
WHEN 3 THEN
SET deci1= thirty;
WHEN 4 THEN
SET deci1= forty;
WHEN 5 THEN
SET deci1= fifty;
WHEN 6 THEN
SET deci1= sixty;
WHEN 7 THEN
SET deci1= seventy;
WHEN 8 THEN
SET deci1= Eighty;
WHEN 9 THEN
SET deci1= ninety;
END CASE;
CASE unit1
WHEN 1 THEN
SET unit1= onee;
WHEN 2 THEN
SET unit1= two;
WHEN 3 THEN
SET unit1= three;
WHEN 4 THEN
SET unit1= four;
WHEN 5 THEN
SET unit1= five;
WHEN 6 THEN
SET unit1= six;
WHEN 7 THEN
SET unit1= seven;
WHEN 8 THEN
SET unit1= Eight;
WHEN 9 THEN
SET unit1= nine;
WHEN 0 THEN
CASE deci
WHEN 2 THEN
SET deci1= twenty;
SET unit1='';
WHEN 3 THEN
SET deci1= thirty;
SET unit1='';
WHEN 4 THEN
SET deci1= forty;
SET unit1='';
WHEN 5 THEN
SET deci1= fifty;
SET unit1='';
WHEN 6 THEN
SET deci1= sixty;
SET unit1='';
WHEN 7 THEN
SET deci1= seventy;
SET unit1='';
WHEN 8 THEN
SET deci1= Eighty;
SET unit1='';
WHEN 9 THEN
SET deci1= ninety;
SET unit1='';
END CASE;
END CASE;
END IF;
END IF;
RETURN CONCAT(thous1,' ',Tthous,' ',thousand,' ',hun1,' ',hundred,' ',deci1,' ',unit1,' ',Rupees,' ',deci4,' ',unit4,' ',paise);
END IF;
if digitCount=6 THEN
   IF n1 > 100000 THEN
SET unit1=temp1 % 10;
SET hun=temp1 DIV 10;
SET deci=hun % 10;
SET hun=hun DIV 10;
SET thous=hun DIV 10;
SET Tthous=hun DIV 10;
SET thous=Tthous DIV 10;
SET thous=Tthous % 10;
SET Tthous=Tthous DIV 10;
SET lakh2=Tthous DIV 10;
SET Tthous=Tthous % 10;
SET temp7=Tthous;
SET hun=hun % 10;
CASE lakh2
WHEN 1 THEN
SET lakh1= onee;
WHEN 2 THEN
SET lakh1= two;
WHEN 3 THEN
SET lakh1= three;
WHEN 4 THEN
SET lakh1= four;
WHEN 5 THEN
SET lakh1= five;
WHEN 6 THEN
SET lakh1= six;
WHEN 7 THEN
SET lakh1= seven;
WHEN 8 THEN
SET lakh1= Eight;
WHEN 9 THEN
SET lakh1= nine;
END CASE;
CASE temp7
WHEN 0 THEN
SET thous1='';
WHEN 1 THEN
CASE thous
WHEN 0 THEN
SET thous1= Ten;
SET Tthous='';
WHEN 1 THEN
SET thous1= Eleven;
SET Tthous='';
WHEN 2 THEN
SET thous1= Twelve;
SET Tthous='';
WHEN 3 THEN
SET thous1= thirteen;
SET Tthous='';
WHEN 4 THEN
SET thous1= fourteen;
SET Tthous='';
WHEN 5 THEN
SET thous1= fifteen;
SET Tthous='';
WHEN 6 THEN
SET thous1= sixteen;
SET Tthous='';
WHEN 7 THEN
SET thous1= seventeen;
SET Tthous='';
WHEN 8 THEN
SET Tthous='';
SET thous1= eighteen;
WHEN 9 THEN
SET Tthous='';
SET thous1= nineteen;
SET Tthous='';
ELSE
SET thous1='';
END CASE;
WHEN 2 THEN
SET thous1= twenty;
WHEN 3 THEN
SET thous1= thirty;
WHEN 4 THEN
SET thous1= forty;
WHEN 5 THEN
SET thous1= fifty;
WHEN 6 THEN
SET thous1= sixty;
WHEN 7 THEN
SET thous1= seventy;
WHEN 8 THEN
SET thous1= Eighty;
WHEN 9 THEN
SET thous1= ninety;
END CASE;
IF temp7 > 1 THEN
CASE thous
WHEN 0 THEN
SET Tthous= '';
WHEN 1 THEN
SET Tthous= onee;
WHEN 2 THEN
SET Tthous= two;
WHEN 3 THEN
SET Tthous= three;
WHEN 4 THEN
SET Tthous= four;
WHEN 5 THEN
SET Tthous= five;
WHEN 6 THEN
SET Tthous= six;
WHEN 7 THEN
SET Tthous= seven;
WHEN 8 THEN
SET Tthous= Eight;
WHEN 9 THEN
SET Tthous= nine;
END CASE;
END IF;
IF temp7 = 0 THEN
CASE thous
WHEN 0 THEN
SET Tthous= '';
SET thous1='';
SET thousand='';
WHEN 1 THEN
SET Tthous= onee;
WHEN 2 THEN
SET Tthous= two;
WHEN 3 THEN
SET Tthous= three;
WHEN 4 THEN
SET Tthous= four;
WHEN 5 THEN
SET Tthous= five;
WHEN 6 THEN
SET Tthous= six;
WHEN 7 THEN
SET Tthous= seven;
WHEN 8 THEN
SET Tthous= Eight;
WHEN 9 THEN
SET Tthous= nine;
END CASE;
END IF;
IF thous = 0 THEN
SET Tthous= '';
END IF;
CASE hun
WHEN 1 THEN
SET hun1= onee;
WHEN 2 THEN
SET hun1= two;
WHEN 3 THEN
SET hun1= three;
WHEN 4 THEN
SET hun1= four;
WHEN 5 THEN
SET hun1= five;
WHEN 6 THEN
SET hun1= six;
WHEN 7 THEN
SET hun1= seven;
WHEN 8 THEN
SET hun1= Eight;
WHEN 9 THEN
SET hun1= nine;
WHEN 0 THEN
SET hun1 ='';
SET hundred='';
END CASE;
IF deci = 1 THEN
CASE unit1
WHEN 0 THEN
SET deci1= Ten;
WHEN 1 THEN
SET deci1= Eleven;
WHEN 2 THEN
SET deci1= Twelve;
WHEN 3 THEN
SET deci1= thirteen;
WHEN 4 THEN
SET deci1=fourteen;
WHEN 5 THEN
SET deci1= fifteen;
WHEN 6 THEN
SET deci1= sixteen;
WHEN 7 THEN
SET deci1= seventeen;
WHEN 8 THEN
SET deci1= eighteen;
WHEN 9 THEN
SET deci1= nineteen;
END CASE;
SET unit1= '';
END IF;
IF deci = 0 THEN
CASE unit1
WHEN 1 THEN
SET deci1= onee;
WHEN 2 THEN
SET deci1= two;
WHEN 3 THEN
SET deci1= three;
WHEN 4 THEN
SET deci1=four;
WHEN 5 THEN
SET deci1= five;
WHEN 6 THEN
SET deci1= six;
WHEN 7 THEN
SET deci1= seven;
WHEN 8 THEN
SET deci1= eight;
WHEN 9 THEN
SET deci1= nine;
ELSE
SET deci1='';
END CASE;
SET unit1= '';
END IF;
IF deci > 1 THEN
CASE deci
WHEN 2 THEN
SET deci1= twenty;
WHEN 3 THEN
SET deci1= thirty;
WHEN 4 THEN
SET deci1= forty;
WHEN 5 THEN
SET deci1= fifty;
WHEN 6 THEN
SET deci1= sixty;
WHEN 7 THEN
SET deci1= seventy;
WHEN 8 THEN
SET deci1= Eighty;
WHEN 9 THEN
SET deci1= ninety;
END CASE;
CASE unit1
WHEN 1 THEN
SET unit1= onee;
WHEN 2 THEN
SET unit1= two;
WHEN 3 THEN
SET unit1= three;
WHEN 4 THEN
SET unit1= four;
WHEN 5 THEN
SET unit1= five;
WHEN 6 THEN
SET unit1= six;
WHEN 7 THEN
SET unit1= seven;
WHEN 8 THEN
SET unit1= Eight;
WHEN 9 THEN
SET unit1= nine;
WHEN 0 THEN
CASE deci
WHEN 2 THEN
SET deci1= twenty;
SET unit1='';
WHEN 3 THEN
SET deci1= thirty;
SET unit1='';
WHEN 4 THEN
SET deci1= forty;
SET unit1='';
WHEN 5 THEN
SET deci1= fifty;
SET unit1='';
WHEN 6 THEN
SET deci1= sixty;
SET unit1='';
WHEN 7 THEN
SET deci1= seventy;
SET unit1='';
WHEN 8 THEN
SET deci1= Eighty;
SET unit1='';
WHEN 9 THEN
SET deci1= ninety;
SET unit1='';
END CASE;
END CASE;
END IF;
END IF;
RETURN CONCAT(lakh1,' ',lakh,' ',thous1,' ',Tthous,' ',thousand,' ',hun1,' ',hundred,' ',deci1,' ',unit1,' ',Rupees,' ',deci4,' ',unit4,' ',paise);
END IF;
if digitCount=7 THEN
   IF n1 > 999999 THEN
SET unit1=temp1 % 10;
SET hun=temp1 DIV 10;
SET deci=hun % 10;
SET hun=hun DIV 10;
SET thous=hun DIV 10;
SET Tthous=hun DIV 10;
SET thous=Tthous DIV 10;
SET thous=Tthous % 10;
SET Tthous=Tthous DIV 10;
SET lakh2=Tthous DIV 10;
SET TENlakh2=lakh2 DIV 10;
SET lakh2=lakh2 %10;
SET Tthous=Tthous % 10;
SET hun=hun % 10;
SET temp7=Tthous;
CASE TENlakh2
WHEN 1 THEN
CASE lakh2
WHEN 0 THEN
SET lakh1= Ten;
SET lakh3= '';
WHEN 1 THEN
SET lakh1= Eleven;
SET lakh3='';
WHEN 2 THEN
SET lakh1= Twelve;
SET lakh3='';
WHEN 3 THEN
SET lakh1= thirteen;
SET lakh3='';
WHEN 4 THEN
SET lakh1= fourteen;
SET lakh3='';
WHEN 5 THEN
SET lakh1= fifteen;
SET lakh3='';
WHEN 6 THEN
SET lakh1= sixteen;
SET lakh3='';
WHEN 7 THEN
SET lakh1= seventeen;
SET lakh3='';
WHEN 8 THEN
SET lakh1= eighteen;
SET lakh3='';
WHEN 9 THEN
SET lakh1= nineteen;
SET lakh3='';
END CASE;
WHEN 2 THEN
SET lakh1= twenty;
WHEN 3 THEN
SET lakh1= thirty;
WHEN 4 THEN
SET lakh1= forty;
WHEN 5 THEN
SET lakh1= fifty;
WHEN 6 THEN
SET lakh1= sixty;
WHEN 7 THEN
SET lakh1= seventy;
WHEN 8 THEN
SET lakh1= Eighty;
WHEN 9 THEN
SET lakh1= ninety;
END CASE;
if TENlakh2 > 1 then
CASE lakh2
WHEN 0 THEN
SET lakh3= '';
WHEN 1 THEN
SET lakh3= onee;
WHEN 2 THEN
SET lakh3= two;
WHEN 3 THEN
SET lakh3= three;
WHEN 4 THEN
SET lakh3= four;
WHEN 5 THEN
SET lakh3= five;
WHEN 6 THEN
SET lakh3= six;
WHEN 7 THEN
SET lakh3= seven;
WHEN 8 THEN
SET lakh3= Eight;
WHEN 9 THEN
SET lakh3= nine;
END CASE;
end if;
CASE temp7
WHEN 0 THEN
CASE thous
WHEN 0 THEN
SET Tthous= '';
SET thous1= '';
WHEN 1 THEN
SET Tthous= onee;
SET thous1= '';
WHEN 2 THEN
SET Tthous= two;
SET thous1= '';
WHEN 3 THEN
SET Tthous= three;
SET thous1= '';
WHEN 4 THEN
SET Tthous= four;
SET thous1= '';
WHEN 5 THEN
SET Tthous= five;
SET thous1= '';
WHEN 6 THEN
SET Tthous= six;
SET thous1= '';
WHEN 7 THEN
SET Tthous= seven;
SET thous1= '';
WHEN 8 THEN
SET Tthous= Eight;
SET thous1= '';
WHEN 9 THEN
SET Tthous= nine;
SET thous1= '';
END CASE;
WHEN 1 THEN
CASE thous
WHEN 0 THEN
SET thous1= Ten;
SET Tthous='';
WHEN 1 THEN
SET thous1= Eleven;
SET Tthous='';
WHEN 2 THEN
SET thous1= Twelve;
SET Tthous='';
WHEN 3 THEN
SET thous1= thirteen;
SET Tthous='';
WHEN 4 THEN
SET thous1= fourteen;
SET Tthous='';
WHEN 5 THEN
SET thous1= fifteen;
SET Tthous='';
WHEN 6 THEN
SET thous1= sixteen;
SET Tthous='';
WHEN 7 THEN
SET thous1= seventeen;
SET Tthous='';
WHEN 8 THEN
SET Tthous='';
SET thous1= eighteen;
WHEN 9 THEN
SET Tthous='';
SET thous1= nineteen;
SET Tthous='';
END CASE;
WHEN 2 THEN
SET thous1= twenty;
WHEN 3 THEN
SET thous1= thirty;
WHEN 4 THEN
SET thous1= forty;
WHEN 5 THEN
SET thous1= fifty;
WHEN 6 THEN
SET thous1= sixty;
WHEN 7 THEN
SET thous1= seventy;
WHEN 8 THEN
SET thous1= Eighty;
WHEN 9 THEN
SET thous1= ninety;
END CASE;
IF temp7 > 1 THEN
CASE thous
WHEN 0 THEN
SET Tthous= '';
WHEN 1 THEN
SET Tthous= onee;
WHEN 2 THEN
SET Tthous= two;
WHEN 3 THEN
SET Tthous= three;
WHEN 4 THEN
SET Tthous= four;
WHEN 5 THEN
SET Tthous= five;
WHEN 6 THEN
SET Tthous= six;
WHEN 7 THEN
SET Tthous= seven;
WHEN 8 THEN
SET Tthous= Eight;
WHEN 9 THEN
SET Tthous= nine;
END CASE;
END IF;
IF  temp7= 0 THEN
IF  thous= 0 THEN
SET Tthous = '';
SET thous1 = '';
END IF;
END IF;
IF  Tthous = '' THEN
IF  thous1= '' THEN
SET thousand = '';
END IF;
END IF;
CASE hun
WHEN 1 THEN
SET hun1= onee;
WHEN 2 THEN
SET hun1= two;
WHEN 3 THEN
SET hun1= three;
WHEN 4 THEN
SET hun1= four;
WHEN 5 THEN
SET hun1= five;
WHEN 6 THEN
SET hun1= six;
WHEN 7 THEN
SET hun1= seven;
WHEN 8 THEN
SET hun1= Eight;
WHEN 9 THEN
SET hun1= nine;
WHEN 0 THEN
SET hun1 ='';
SET hundred='';
END CASE;
IF deci = 1 THEN
CASE unit1
WHEN 0 THEN
SET deci1= Ten;
WHEN 1 THEN
SET deci1= Eleven;
WHEN 2 THEN
SET deci1= Twelve;
WHEN 3 THEN
SET deci1= thirteen;
WHEN 4 THEN
SET deci1=fourteen;
WHEN 5 THEN
SET deci1= fifteen;
WHEN 6 THEN
SET deci1= sixteen;
WHEN 7 THEN
SET deci1= seventeen;
WHEN 8 THEN
SET deci1= eighteen;
WHEN 9 THEN
SET deci1= nineteen;
END CASE;
SET unit1= '';
END IF;
IF deci = 0 THEN
CASE unit1
WHEN 1 THEN
SET deci1= onee;
WHEN 2 THEN
SET deci1= two;
WHEN 3 THEN
SET deci1= three;
WHEN 4 THEN
SET deci1=four;
WHEN 5 THEN
SET deci1= five;
WHEN 6 THEN
SET deci1= six;
WHEN 7 THEN
SET deci1= seven;
WHEN 8 THEN
SET deci1= eight;
WHEN 9 THEN
SET deci1= nine;
ELSE
SET deci1='';
END CASE;
SET unit1= '';
END IF;
IF deci > 1 THEN
CASE deci
WHEN 2 THEN
SET deci1= twenty;
WHEN 3 THEN
SET deci1= thirty;
WHEN 4 THEN
SET deci1= forty;
WHEN 5 THEN
SET deci1= fifty;
WHEN 6 THEN
SET deci1= sixty;
WHEN 7 THEN
SET deci1= seventy;
WHEN 8 THEN
SET deci1= Eighty;
WHEN 9 THEN
SET deci1= ninety;
END CASE;
CASE unit1
WHEN 1 THEN
SET unit1= onee;
WHEN 2 THEN
SET unit1= two;
WHEN 3 THEN
SET unit1= three;
WHEN 4 THEN
SET unit1= four;
WHEN 5 THEN
SET unit1= five;
WHEN 6 THEN
SET unit1= six;
WHEN 7 THEN
SET unit1= seven;
WHEN 8 THEN
SET unit1= Eight;
WHEN 9 THEN
SET unit1= nine;
WHEN 0 THEN
CASE deci
WHEN 2 THEN
SET deci1= twenty;
SET unit1='';
WHEN 3 THEN
SET deci1= thirty;
SET unit1='';
WHEN 4 THEN
SET deci1= forty;
SET unit1='';
WHEN 5 THEN
SET deci1= fifty;
SET unit1='';
WHEN 6 THEN
SET deci1= sixty;
SET unit1='';
WHEN 7 THEN
SET deci1= seventy;
SET unit1='';
WHEN 8 THEN
SET deci1= Eighty;
SET unit1='';
WHEN 9 THEN
SET deci1= ninety;
SET unit1='';
END CASE;
END CASE;
END IF;
END IF;
RETURN CONCAT(lakh1,' ',lakh3,' ',lakh,' ',thous1,' ',Tthous,' ',thousand,' ',hun1,' ',hundred,' ',deci1,' ',unit1,' ',Rupees,' ',deci4,' ',unit4,' ',paise);
END IF;
END CASE;
END$$
