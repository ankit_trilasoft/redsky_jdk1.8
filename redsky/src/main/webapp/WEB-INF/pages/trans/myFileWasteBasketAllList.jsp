<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
<title><fmt:message key="myFileList.title"/></title>   
<meta name="heading" content="<fmt:message key='myFileList.heading'/>"/>   
<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
</c:if>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();
</script>
<script>
function clear_fields(){  		    	
	document.forms['myFileForm'].elements['fileTypes'].value = "";
	document.forms['myFileForm'].elements['descriptions'].value ="";
	document.forms['myFileForm'].elements['fileIds'].value ="";
	document.forms['myFileForm'].elements['createdBys'].value ="";
	document.forms['myFileForm'].elements['createdOns'].value ="";
}


function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to recover this file?");
	var did = targetElement;
	var fileTypes = document.forms['myFileForm'].elements['fileTypes'].value;
	var descriptions = document.forms['myFileForm'].elements['descriptions'].value;
	var fileIds = document.forms['myFileForm'].elements['fileIds'].value;
	var createdBys = document.forms['myFileForm'].elements['createdBys'].value;
	var createdOns = document.forms['myFileForm'].elements['createdOns'].value;
	if (agree){
			location.href="revocerWasteBasketAllList.html?did="+did+"&fileTypes="+fileTypes+"&descriptions="+descriptions+"&fileIds="+fileIds+"&createdBys="+createdBys+"&createdOns="+createdOns+"&relatedDocs=No&active=false";
	}else{
		return false;
	}
}

function generatePortalId() {
		var daReferrer = document.referrer; 
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		
		if(checkBoxId ==''){
			alert('Please select the one or more document to e-mail.');
		}else{
			window.open('attachMail.html?checkBoxId='+checkBoxId+'&decorator=popup&popup=true&from=file','','width=650,height=170');
		}
} 

function checkStatusId(rowId, targetElement) {
		var Status = targetElement.checked;
		var url="updateMyfileStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse1;
        http22.send(null);		
} 

function handleHttpResponse1(){
      if (http22.readyState == 4){
           var result= http22.responseText         
      }
}	

function checkStatusAccId(rowId, targetElement) {
		var Status = targetElement.checked;
		var url="updateMyfileAccStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse2;
        http22.send(null);
} 
function handleHttpResponse2(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}

function checkStatusPartnerId(rowId, targetElement){
		var Status = targetElement.checked;
		var url="updateMyfilePartnerStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse3;
        http22.send(null);
}
function handleHttpResponse3(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}	


var http22 = getHTTPObject22();

function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

// 1 visible, 0 hidden   
function userStatusCheck(targetElement){
    	if(targetElement.checked){
      		var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
      		if(userCheckStatus == ''){
	  			document.forms['myFileForm'].elements['userCheck'].value = targetElement.value;
      		}else{
      			var userCheckStatus=	document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus + ',' + targetElement.value;
      			document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
    	}
  	 	if(targetElement.checked==false){
     		var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
     		var userCheckStatus=document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( targetElement.value , '' );
     		document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
   		}
}

function downloadDoc(){
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
		if(seqNo == '' ){
			seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
		}
		if(checkBoxId =='' || checkBoxId ==','){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=DWNLD&seqNo='+seqNo;
			location.href=url;
		}
} 
function emailDoc() {
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		
		if(checkBoxId ==''){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=EMAIL';
			location.href=url;
		}
}

function checkAll(){
	document.forms['myFileForm'].elements['userCheck'].value = "";
	document.forms['myFileForm'].elements['userCheck'].value = "";
	var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['myFileForm'].elements['DD'][i].checked = true ;
		userStatusCheck(document.forms['myFileForm'].elements['DD'][i]);
	}
}

function uncheckAll(){
	var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['myFileForm'].elements['DD'][i].checked = false ;
		userStatusCheck(document.forms['myFileForm'].elements['DD'][i]);
	}
	document.forms['myFileForm'].elements['userCheck'].value="";
}
function show(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'block';
     }
}
function hide(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'none';
     }else{
          document.getElementById(theTable).style.display = 'none';
     }
}
</script>

   
<style>
#mainPopup {
padding-left:10px;
padding-right:10px;
}
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;
margin-top:-18px;
padding:2px 0;
text-align:right;
width:100%;
}
</style>
</head>


<s:form cssClass="form_magn" id="myFileForm" action="searchWasteFiles" method="post" >  
<c:set var="fileForId" value="<%=request.getParameter("fileForId") %>"/>
<s:hidden name="fileForId" value="<%=request.getParameter("fileForId") %>"/>
<s:hidden name="fileForId" value="<%=request.getParameter("fileForId") %>"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
<c:set var="fileId" value="<%=request.getParameter("id") %>"/>

<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>" />
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>" />
<s:hidden name="userCheck"/> 
<s:hidden name="customerFile.id" />
<s:hidden name="workTicket.id" />
<s:hidden name="workTicket.ticket" />

<s:hidden name="serviceOrder.id" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />

<c:if test="${myFileFor=='CF'}">
	<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
	<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
	<c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
	<c:set var="noteFor" value="CustomerFile" scope="session"/>
	<c:if test="${empty customerFile.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty customerFile.id}">
		<c:set var="isTrue" value="true" scope="request"/>
	</c:if>
</c:if>

<c:if test="${myFileFor!='CF'}"> 
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>	
<s:hidden name="customerFile.sequenceNumber" />
<div id="layer6" style="width:100%; ">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-4px;"><span></span></div>
    <div class="center-content">
	<table class="table" width="99%">
		<thead>
			<tr>
			    <th><fmt:message key="myFile.fileType" /></th>
				<th>Description</th>
				<th>File Number</th>
				<th>Uploaded By</th>
				<th>Uploaded On</th>
			</tr>
		</thead>
		<tbody>
			<tr>
			    <td><s:select name="fileTypes" cssClass="list-menu" list="%{docsList}" headerKey="" headerValue=""/></td>
				<td><s:textfield name="descriptions" required="true" cssClass="input-text" /></td>
				<td><s:textfield name="fileIds" required="true" cssClass="input-text" /></td>
				<td><s:textfield name="createdBys" required="true" cssClass="input-text" /></td>
				<c:if test="${not empty createdOns}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="createdOns" /></s:text>
				<td align="left" style="width:145px;"><s:textfield cssClass="input-text" id="createdOns" name="createdOns" value="%{customerFiledate1FormattedValue}" required="true" size="6" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true"/><img id="createdOns-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
				</c:if>
				<c:if test="${empty createdOns}">
				<td align="left" style="width:145px;"><s:textfield cssClass="input-text" id="createdOns" name="createdOns" required="true" size="6" maxlength="11" readonly="true"/><img id="createdOns-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
				</c:if>	
			</tr>
			<tr>
				<td colspan="4"></td>
				<td colspan="" style="border-left: hidden;" align="right"><s:submit cssClass="cssbutton" method="searchWasteFiles" cssStyle="width:60px; height:25px;!margin-bottom: 10px;" key="button.search"/>
			    <input type="button" class="cssbutton" value="Clear" style="width:60px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/></td> 
				
			</tr>
		</tbody>
	</table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
    
<div class="spn">&nbsp;</div>
<div style="!margin-top:8px; ">
<%-- 
      <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
 --%> 
</div>
</div>
</c:if>

<div id="Layer1" style="width:100%;">
<div id="Layer4" style="width:100%;">
<div id="newmnav" style="float:left;">
				  <ul>
				        <li><a href="docProcess.html"><span>Document List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    	<li id="newmnav1" style="background:#FFF "><a class="current" href="#"><span>Waste Basket</span></a></li>
					</ul>
		</div>
		<div id="chkAllButton"  class="listwhitetext" style="display:none;" >
								<input type="radio"  style="vertical-align: middle;" name="chk" onClick="checkAll()" /><strong style="vertical-align: bottom;">Check All</strong>
								<input type="radio" style="vertical-align: middle;"  name="chk" onClick="uncheckAll()"  /><strong style="vertical-align: bottom;">Uncheck All</strong>
							</div>
		<div class="spn">&nbsp;</div>
		 <div style="padding-bottom:3px;"></div>
		 </div>


<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;">
	<tbody>
		<tr>
			<td>
				<s:set name="myFiles" value="myFiles" scope="request"/>  
				<display:table name="myFiles" class="table" requestURI="" id="myFileList" export="true" defaultsort="4" defaultorder="descending" pagesize="25" style="width:100%;" >   
				    <display:column title=" " style="width: 15px;"><input type="checkbox" style="margin-left:10px;" id="checkboxId" name="DD" value="${myFileList.id}" onclick="userStatusCheck(this)"/></display:column>
				     
				    <display:column property="fileType" sortable="true" titleKey="myFile.fileType" style="width:175px;"/>
					<display:column property="mapFolder" sortable="true" title="Map" style="width:50px;"/>
				    <display:column property="description" sortable="true" title="Description" maxLength="20" style="width:160px;"></display:column>
				    <display:column property="fileId" sortable="true" title="File Number" maxLength="20" style="width:120px;"/>
				    <display:column titleKey="myFile.fileFileName"  maxLength="20" style="width:140px;cursor: pointer;">
				    	<a onclick="javascript:openWindow('ImageServletAction.html?id=${myFileList.id}&decorator=popup&popup=true',900,600);"><c:out value="${myFileList.fileFileName}" escapeXml="false"/></a>
				    </display:column>
				     <display:column property="fileSize" sortable="true" title="Size" style="width:100px;"></display:column>
				    <display:column title="Cust Portal" style="width:25px;">
				    <c:if test="${myFileList.isCportal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId(${myFileList.id},this)" checked disabled="disabled"/>
				    </c:if>
					<c:if test="${myFileList.isCportal == false || myFileList.isCportal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId(${myFileList.id},this)" disabled="disabled"/>
					</c:if>
				    </display:column>
				    
				    <display:column title="Acc Portal" style="width:25px;">
				    <c:if test="${myFileList.isAccportal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId(${myFileList.id},this)" checked disabled="disabled"/>
				    </c:if>
					<c:if test="${myFileList.isAccportal == false || myFileList.isAccportal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId(${myFileList.id},this)" disabled="disabled"/>
					</c:if>
				    </display:column>
				    
				    <display:column title="Partner Portal" style="width:25px;">
				    <c:if test="${myFileList.isPartnerPortal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId(${myFileList.id},this)" checked disabled="disabled"/>
				    </c:if>
					<c:if test="${myFileList.isPartnerPortal == false || myFileList.isPartnerPortal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId(${myFileList.id},this)" disabled="disabled"/>
					</c:if>
				    </display:column>
				    <display:column property="docSent" sortable="true" title="Docs Xferred" format="{0,date,dd-MMM-yyyy}" style="width:85px;"/>
				    <display:column property="createdBy" sortable="true" title="Uploaded&nbsp;By" />
				    <display:column property="createdOn" sortable="true" title="Uploaded&nbsp;On" format="{0,date,dd-MMM-yyyy}" style="width:85px;"/>
				   
				    <display:column title="Recover" style="width:45px;">
				    	<a><img align="middle" onclick="confirmSubmit(${myFileList.id});" style="margin: 0px 0px 0px 8px;" src="images/recover.png"/></a>
				    </display:column>
				     
				   
				    <display:setProperty name="paging.banner.item_name" value="document"/>   
				    <display:setProperty name="paging.banner.items_name" value="documents"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Document List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Document List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Document List.pdf"/>   
				</display:table>  
  
  
			</td>
		</tr>
	</tbody>
</table>
</div>  
<c:out value="${buttons}" escapeXml="false" />
<%-- <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15" value="Email" onclick="emailDoc();"/> --%>
<input type="button" class="cssbutton" style="margin-right: 5px;height: 25px;width:70px;" name="dwnldBtn"  value="Download" onclick="downloadDoc();"/>
</s:form>
<script type="text/javascript"> 
   try{
    var len = document.forms['myFileForm'].elements['DD'].length;
   }
   catch(e){}
   try{
    if(len>1){
    	show('chkAllButton');
    }
    }
    catch(e){}
</script>
 <script type="text/javascript">
 	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
 </script>