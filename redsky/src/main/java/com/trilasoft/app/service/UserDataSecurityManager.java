package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.UserDataSecurity;

public interface UserDataSecurityManager extends GenericManager<UserDataSecurity, Long>{
	public List findById(Long id);
}
