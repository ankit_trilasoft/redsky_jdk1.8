<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<%@page import="java.util.*"%>
<head>
<title>HandOut</title>
<meta name="heading" content="HandOut" />

<script language="javascript" type="text/javascript">
function clear_fields(){
	document.forms['handOutForm'].elements['ticket'].select();
	document.forms['handOutForm'].elements['ticket'].value='';		
}


function IsEmpty(aTextField) {
	if ((aTextField.value.length==0) || (aTextField.value==null)) {
	return true;
	}
	else { return false; }
}

function validateForm(form)
{  
	if(form.ticket.value==''){
		alert('Ticket number is a required field.');
		form.ticket.select();
		return false;
	}
	if (!IsNumeric(form.ticket.value))
	{
	alert('Please enter only numbers.');
	form.ticket.value='';
	form.ticket.select();
	return false;
	}	
	return true;
}

function IsNumeric(sText)
{
	var ValidChars = "0123456789";
	var IsNumber=true;
	var Char;
	for (i = 0; i < sText.length && IsNumber == true; i++)
	{
	Char = sText.charAt(i);
	if (ValidChars.indexOf(Char) == -1)
	{
	IsNumber = false;
	}
	}
	return IsNumber;
}

</script>

 </head>

<s:form id="handOutForm" action="handoutProcessing" method="post">
<div id="Layer1" style="width:100%">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="wtktServiceId" />	
<s:hidden name="serviceCheck" />
<s:hidden name="showContinue" />
<s:hidden name="warehouse" />				
<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%">
<tbody>
  <tr>
	<td>
     <div id="content" align="center">
        <div id="liquid-round-top">
		<div class="top"><span></span></div>
			<div class="center-content">
				<table>
					<tbody>
						<tr>
							<td style="width:30%;"></td>
							<td class="listwhitetext">Enter Ticket Number<font style="color:red;">*</font></td>
							<td><s:textfield name="ticket" id="ticket" cssClass="input-text" size="10" required="true" /></td>
                            <td style="width:20%;"></td>
							<td style="border-left:hidden;" class="listwhitetext">
						    <s:submit cssClass="cssbutton" name="continueBtn" id="continueBtn" value="Continue" cssStyle="width:65px; height:25px" tabindex="1" onclick="return validateForm(form);"/></td>
						    <td style="border-left:hidden;" class="listwhitetext">
						    <input type="button" class="cssbutton" value="Clear" style="width:65px; height:25px" onclick="clear_fields();" tabindex="0"/></td>
						</tr>
					</tbody>
				</table>
			</div>
      <div class="bottom-header"><span></span></div>
      </div>
    </div> 
   </td>
</tr>
</tbody>
</table>
</div>
<c:if test="${serviceCheck == 'HO'}" >
		<c:redirect url="/handOutList.html?ticket=${ticket}&wtktServiceId=${wtktServiceId}"  />
</c:if> 
<c:if test="${serviceCheck == 'RC'}" >
		<c:redirect url="/searchUnitLocates.html?id=${wtktServiceId}&warehouse=${warehouse}&locationId=''&type=''&occupied='' "  />
</c:if>  	
</s:form>
<script language="javascript" type="text/javascript">
try{
	document.forms['handOutForm'].elements['ticket'].focus();
	document.forms['handOutForm'].elements['ticket'].value="";
}catch(e){}

</script>
