package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ScrachCardDao;
import com.trilasoft.app.model.ScrachCard;
import com.trilasoft.app.service.ScrachCardManager;

public class ScrachCardManagerImpl  extends GenericManagerImpl<ScrachCard, Long> implements ScrachCardManager {   
	ScrachCardDao scrachCardDao;   
  
	public ScrachCardManagerImpl(ScrachCardDao scrachCardDao) {   
        super(scrachCardDao);   
        this.scrachCardDao = scrachCardDao;   
    } 
	
	public List getScrachCardByUser(String userName){
		return scrachCardDao.getScrachCardByUser(userName);
	}
	public List getSchedulerScrachCardByUser(String userName ,Date workDate){
		return scrachCardDao.getSchedulerScrachCardByUser(userName,workDate);
	}
	public List getSchedulerScrachCardByDate(Date workDate){
		return scrachCardDao.getSchedulerScrachCardByDate(workDate);
	}
}
