package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.PricingFreightDao;
import com.trilasoft.app.model.PricingFreight;
import com.trilasoft.app.service.PricingFreightManager;

public class PricingFreightManagerImpl extends GenericManagerImpl<PricingFreight,Long> implements PricingFreightManager{
	PricingFreightDao pricingFreightDao;
	
	public PricingFreightManagerImpl(PricingFreightDao pricingFreightDao) {
		super(pricingFreightDao);
		this.pricingFreightDao=pricingFreightDao;
		
	}

	public List checkById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List getFreightList(Long pricingControlId) {
		return pricingFreightDao.getFreightList(pricingControlId);
	}

	public List getMarkUpValue(BigDecimal price, String contract, String mode) {
		return pricingFreightDao.getMarkUpValue(price, contract, mode);
	}

	public List getPricingFreightRecord(Long id, String sessionCorpID) {
		return pricingFreightDao.getPricingFreightRecord(id, sessionCorpID);
	}

}
