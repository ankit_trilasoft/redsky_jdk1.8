package com.trilasoft.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="userdatasecurity")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class UserDataSecurity extends BaseObject {
	private Long id;
	private Long userDataSecurityId;
	private Long userId;
	private String corpID;
	private String updatedBy;
	private Date updatedOn;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append(
				"userDataSecurityId", userDataSecurityId).append("userId",
				userId).append("corpID", corpID).append("updatedBy", updatedBy).append("updatedOn", updatedOn).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof UserDataSecurity))
			return false;
		UserDataSecurity castOther = (UserDataSecurity) other;
		return new EqualsBuilder().append(id, castOther.id).append(
				userDataSecurityId, castOther.userDataSecurityId).append(
				userId, castOther.userId).append(corpID, castOther.corpID).append(updatedBy, castOther.updatedBy).append(updatedOn, castOther.updatedOn)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(userDataSecurityId)
				.append(userId).append(corpID).append(updatedBy).append(updatedOn).toHashCode();
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Long getUserDataSecurityId() {
		return userDataSecurityId;
	}
	public void setUserDataSecurityId(Long userDataSecurityId) {
		this.userDataSecurityId = userDataSecurityId;
	}
	@Column
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
}
