<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title><fmt:message key="locationDetail.title" /></title>
<meta name="heading" content="<fmt:message key='locationDetail.heading'/>" />

<link href='<s:url value="/css/main.css"/>' rel="stylesheet"
	type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
<script type="text/javascript">
		function setFlagValueMo(){
		  	document.forms['locationaddForm'].elements['hitFlag'].value = '1';
		}
</script>
	<script type="text/javascript"> 
		function forwardToMyMessage(){ 
			document.forms['locationaddForm'].elements['bookStorage.price'].value=document.forms['locationaddForm'].elements['storage.price'].value;		
			document.forms['locationaddForm'].elements['bookStorage.description'].value=document.forms['locationaddForm'].elements['storage.description'].value;		
			document.forms['locationaddForm'].elements['bookStorage.containerId'].value=document.forms['locationaddForm'].elements['storage.containerId'].value;		
			document.forms['locationaddForm'].elements['bookStorage.pieces'].value=document.forms['locationaddForm'].elements['storage.pieces'].value;		
			//document.forms['locationaddForm'].elements['storage.locationId'].value=document.forms['locationaddForm'].elements['location.locationId'].value;
			document.forms['locationaddForm'].elements['bookStorage.locationId'].value=document.forms['locationaddForm'].elements['storage.locationId'].value;	
			document.forms['locationaddForm'].elements['bookStorage.idNum'].value=document.forms['locationaddForm'].elements['storage.idNum'].value;	
			document.forms['locationaddForm'].elements['bookStorage.itemTag'].value=document.forms['locationaddForm'].elements['storage.itemTag'].value;	
			document.forms['locationaddForm'].elements['bookStorage.createdOn'].value=document.forms['locationaddForm'].elements['storage.createdOn'].value;
			document.forms['locationaddForm'].elements['bookStorage.createdBy'].value=document.forms['locationaddForm'].elements['storage.createdBy'].value;
			document.forms['locationaddForm'].elements['bookStorage.updatedOn'].value=document.forms['locationaddForm'].elements['storage.updatedOn'].value;
			document.forms['locationaddForm'].elements['bookStorage.updatedBy'].value=document.forms['locationaddForm'].elements['storage.updatedBy'].value;
			//document.forms['locationaddForm'].elements['storage.itemNumber'].value=document.forms['locationaddForm'].elements['storage.idNum'].value;	
			document.forms['locationaddForm'].elements['bookStorage.shipNumber'].value=document.forms['locationaddForm'].elements['shipNumber'].value;	
			document.forms['locationaddForm'].elements['storage.shipNumber'].value=document.forms['locationaddForm'].elements['shipNumber'].value;
			//document.forms['locationaddForm'].elements['bookStorage.itemNumber'].value=document.forms['locationaddForm'].elements['storage.idNum'].value;	
				
		}
	
		function notExists(){
			alert("The Ticket information is not saved yet");
		}	
	</script>
	<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || ( keyCode==110); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
	
	function chkSelect()
	{
		if (checkFloat('locationaddForm','storage.measQuantity','Invalid data in Weight') == false)
           {
              document.forms['locationaddForm'].elements['storage.measQuantity'].focus();
              return false
           }
           if (checkFloat('locationaddForm','storage.volume','Invalid data in Volume') == false)
           {
              document.forms['locationaddForm'].elements['storage.volume'].focus();
              return false
           }
           if (checkFloat('locationaddForm','storage.pieces','Invalid data in Pieces') == false)
           {
              document.forms['locationaddForm'].elements['storage.pieces'].focus();
              return false
           }
           if (checkFloat('locationaddForm','storage.price','Invalid data in Price') == false)
           {
              document.forms['locationaddForm'].elements['storage.price'].focus();
              return false
           }	
	}
	
	
</script>


</head>

<s:form id="locationaddForm" action="saveLocation" method="post" validate="true">
<div id="Layer1" >

	<s:hidden name="shipNumber" value="${shipNumber}"/>
	<s:hidden name="id" value="${id}"/> 
	<s:hidden name="ticket" value="${ticket}"/>	
	<s:hidden name="id1" value="${id1}"/>
	<s:hidden name="countTicketLocationNotes" value="<%=request.getParameter("countTicketLocationNotes") %>"/>
	<c:set var="countTicketLocationNotes" value="<%=request.getParameter("countTicketLocationNotes") %>" />
	<s:hidden name="location.id" value="%{location.id}" />
	
		<div id="newmnav">
		   			 <ul>
					  	<li><a href="editWorkTicketUpdate.html?id=<%=request.getParameter("id1") %>"><span>Work Ticket</span></a></li>
					  	<li><a href="bookStorages.html?id=<%=request.getParameter("id1") %>"><span>Location List</span></a></li>
						<li id="newmnav1" style="background:#FFF"><a class="current"><span>Add Item To Location<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					  	<li><a href="storages.html?id=<%=request.getParameter("id1") %>"><span>Access/ Release Items</span></a></li>
					  	<li><a href="storagesMove.html?id=<%=request.getParameter("id1") %>"><span>Move Items</span></a></li>
					</ul>
		</div><div class="spn">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
	<table class="" cellspacing="1" cellpadding="0" border="0" width="100%">
		<tbody>
			<tr>
				<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>

						<tr>
							<td class="listwhite" align="right" width="20px"></td>
							<td class="listwhitetext" align="right" width="150px">Location</td>
							<td><s:textfield name="storage.locationId" value="%{location.locationId}" readonly="true" cssClass="input-text"/></td>
							<td class="listwhitetext" align="right" width="300px"></td>
							<td></td>
							<c:if test="${empty location.id}">
								<td align="right" width="400px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty location.id}">
							
							<c:choose>
							
								<c:when test="${countTicketLocationNotes == '0' || countTicketLocationNotes == '' || countTicketLocationNotes == null}">
								
								<td align="right" width="400px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${id1}&notesId=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${id1}&notesId=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);" ></a></td>
								</c:when>
								<c:otherwise>
								
								<td align="right" width="400px"><img src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${id1}&notesId=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${id1}&notesId=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right" valign="top">Description</td>
							<td colspan="2"><s:textarea name="storage.description" cols="30" rows="4" tabindex="1" cssClass="textarea"/></td>
							
							<c:if test="${not empty storage.id}">
								<td class="listwhitetext" align="right" valign="top">Storage Action</td>
								<td align="left" valign="top"><s:textfield name="bookStorage.what" size="20" maxlength="10" cssClass="input-text" tabindex="5" /></td>
							</c:if>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Container ID</td>
							<td><s:textfield name="storage.containerId" size="20" maxlength="10" cssClass="input-text" tabindex="3" /></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Model</td>
							<td><s:textfield name="storage.model" size="20" maxlength="10" cssClass="input-text" tabindex="4" /></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Serial</td>
							<td><s:textfield name="storage.serial" size="20" maxlength="10" cssClass="input-text" tabindex="5" /></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Weight</td>
							<td><s:textfield name="storage.measQuantity" size="20" maxlength="10" cssClass="input-text" tabindex="6" cssStyle="text-align:right"/></td>
							<td align="left" colspan="" valign="middle" class="listwhitetext"><fmt:message key='labels.units'/><s:radio name="storage.unit" list="%{weightunits}"  onchange="changeStatus();" tabindex="7" /></td>
							
							
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Volume</td>
							<td><s:textfield name="storage.volume" size="20" maxlength="10" cssClass="input-text" tabindex="7" cssStyle="text-align:right"/></td>
							<td align="left" colspan="" valign="middle" class="listwhitetext"><fmt:message key='labels.units'/><s:radio name="storage.volUnit" list="%{volumeunits}"  onchange="changeStatus();" tabindex="7" /></td>
							
							
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Item Tag</td>
							<td><s:textfield name="storage.itemTag" size="20" maxlength="10" cssClass="input-text" tabindex="8" /></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Pieces</td>
							<td><s:textfield name="storage.pieces" size="20" maxlength="7"  cssClass="input-text" cssStyle="text-align:right" tabindex="9" /></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Price</td>
							<td><s:textfield name="storage.price" size="20" maxlength="7"  cssClass="input-text" tabindex="10" cssStyle="text-align:right"/></td>
							<td></td>
						</tr>
						<tr>
						<td></td>
						<td align="right" class="listwhitetext">Warehouse</td> 
						<td colspan="3"><s:select name="storage.warehouse" list="%{house}" cssStyle="width:135px" cssClass="list-menu" headerKey="" headerValue="" onchange="" /></td>
						</tr>
					<tr>
							<td></td>
							<td class="listwhite" align="right"></td>
							<td><s:hidden name="workTicket.ticket" value="${ticket}" /></td>
							<s:hidden name="storage.idNum" required="true" cssClass="text medium" />
							<s:hidden name="storage.id" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.id" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.price" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.createdOn" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.createdBy" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.updatedOn" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.updatedBy" required="true" cssClass="text medium" />
							<s:hidden key="bookStorage.description" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.containerId" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.corpID" required="true"   />
							<s:hidden name="bookStorage.type"/>
							<s:hidden name="bookStorage.dated"/>
							<s:hidden name="bookStorage.storageId"/>
							<s:hidden name="storage.storageId"/>
							<s:hidden name="storage.serviceOrderId" value="%{workTicket.serviceOrderId}" />
							<s:hidden name="bookStorage.serviceOrderId" value="%{workTicket.serviceOrderId}" />
							 
							<s:hidden name="storage.corpID" />
							<s:hidden name="bookStorage.model" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.itemTag" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.ticket" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.jobNumber" required="true" cssClass="text medium"/>
							<s:hidden name="storage.jobNumber" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.measQuantity" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.idNum" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.itemNumber" required="true" cssClass="text medium" />
							<s:hidden name="storage.itemNumber" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.pieces" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.locationId" value="%{location.locationId}" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.oldLocation"  cssClass="text medium" />
							<s:hidden name="storage.shipNumber" required="true" cssClass="text medium"/>
							<s:hidden name="bookStorage.shipNumber" required="true" cssClass="text medium" />
						</tr>
					</tbody>
				</table>
			  </td>
			</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	<table>
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<!--<tr>
				<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='storage.createdOn' /></td>
				     <td><fmt:formatDate var="storageCreatedOnFormattedValue" value="${storage.createdOn}" pattern="${displayDateTimeFormat}"/></td>
				<s:hidden name="storage.createdOn" value="${storageCreatedOnFormattedValue}"/>
				<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='storage.createdBy' /></td>
					<c:if test="${not empty storage.id}">
						<s:hidden name="storage.createdBy" />
						<td><s:label name="createdBy" value="%{storage.createdBy}"/></td>
					</c:if>
					<c:if test="${empty storage.id}">
						<s:hidden name="storage.createdBy" value="${pageContext.request.remoteUser}"/>
						<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
					</c:if>
				<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='storage.updatedOn' /></td>
						
				<td><fmt:formatDate var="storageUpdatedOnFormattedValue" value="${storage.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
				<s:hidden name="storage.updatedOn" value="${storageUpdatedOnFormattedValue}"/>
				<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='storage.updatedBy' /></td>
					<c:if test="${not empty storage.id}">
						<s:hidden name="storage.updatedBy" />
						<td><s:label name="updatedBy" value="%{storage.updatedBy}"/></td>
					</c:if>
					<c:if test="${empty storage.id}">
						<s:hidden name="storage.updatedBy" value="${pageContext.request.remoteUser}"/>
						<td><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
					</c:if>
			</tr>
		--> 
		<tr><fmt:formatDate var="serviceCreatedOnFormattedValue" value="${storage.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='storage.createdOn' /></b></td>
				<s:hidden name="storage.createdOn" value="${serviceCreatedOnFormattedValue}"/>
				<td style="font-size:.90em ;width:130px"><fmt:formatDate value="${storage.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message 	key='storage.createdBy' /></b></td>
				<c:if test="${not empty storage.id}">
					<s:hidden name="storage.createdBy" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="%{storage.createdBy}" /></td>
				</c:if>
				<c:if test="${empty storage.id}">
					<s:hidden name="storage.createdBy"
						value="${pageContext.request.remoteUser}" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="${pageContext.request.remoteUser}" /></td>
				</c:if>

				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message
					key='storage.updatedOn' /></b></td>
					<fmt:formatDate var="serviceUpdatedOnFormattedValue" value="${storage.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="storage.updatedOn" value="${serviceUpdatedOnFormattedValue}" />
				<td style="font-size:.90em; width:130px"><fmt:formatDate value="${storage.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message	key='storage.updatedBy' /></b></td>
				<c:if test="${not empty storage.id}"> 
                <s:hidden name="storage.updatedBy"/>

				<td style="width:85px ; font-size:.90em"><s:label name="updatedBy" value="%{storage.updatedBy}"/></td>
				
				</c:if>
				
				<c:if test="${empty storage.id}">
				
				<s:hidden name="storage.updatedBy" value="${pageContext.request.remoteUser}"/>
				
				<td style="width:85px ; font-size:.90em"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				
				</c:if> 
			</tr>
		</tbody>
	</table>
	
</div>
	<div align="left">
		<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" key="button.save" onmouseover="return chkSelect();" onclick="forwardToMyMessage();return chkSelect();"/> 
		<s:reset type="button" key="Reset" cssClass="cssbutton" cssStyle="width:55px; height:25px"/>
	</div>
	
	
	<c:if test="${hitFlag == 1}" >
		<c:redirect url="/bookStorages.html?id=${workTicket.id}"  />
	</c:if>
</s:form>