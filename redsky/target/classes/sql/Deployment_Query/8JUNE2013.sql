// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1;

update app_user set newsUpdateFlag=true ;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('AccountLine','displayOnQuote' ,'Create','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','displayOnQuote' ,'Update','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','additionalService' ,'Create','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','additionalService' ,'Update','amishra',now(),'amishra',now(),false,'','','');

ALTER TABLE `redsky`.`cportalresourcemgmt` MODIFY COLUMN `jobType` VARCHAR(65) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `collection` VARCHAR(20) DEFAULT NULL AFTER `claimBy`;

DROP VIEW IF EXISTS `redsky`.`partner`;
CREATE VIEW `partner` AS select `a`.`id` AS `id`,`a`.`parent` AS `parent`,`a`.`middleInitial` AS `middleInitial`,`a`.`billingAddress1` AS `billingAddress1`,`a`.`billingAddress2` AS `billingAddress2`,`a`.`billingAddress3` AS `billingAddress3`,`a`.`billingAddress4` AS `billingAddress4`,`a`.`billingCity` AS `billingCity`,`a`.`billingCountry` AS `billingCountry`,`a`.`billingCountryCode` AS `billingCountryCode`,`a`.`billingEmail` AS `billingEmail`,`a`.`billingFax` AS `billingFax`,`a`.`billingPhone` AS `billingPhone`,`a`.`billingState` AS `billingState`,`a`.`billingTelex` AS `billingTelex`,`a`.`billingZip` AS `billingZip`,`a`.`effectiveDate` AS `effectiveDate`,`a`.`isAccount` AS `isAccount`,`a`.`isAgent` AS `isAgent`,`a`.`isCarrier` AS `isCarrier`,`a`.`isVendor` AS `isVendor`,`a`.`mailingAddress1` AS `mailingAddress1`,`a`.`mailingAddress2` AS `mailingAddress2`,`a`.`mailingAddress3` AS `mailingAddress3`,`a`.`mailingAddress4` AS `mailingAddress4`,`a`.`mailingCity` AS `mailingCity`,`a`.`mailingCountry` AS `mailingCountry`,`a`.`mailingCountryCode` AS `mailingCountryCode`,`a`.`mailingEmail` AS `mailingEmail`,`a`.`mailingFax` AS `mailingFax`,`a`.`mailingPhone` AS `mailingPhone`,`a`.`mailingState` AS `mailingState`,`a`.`mailingTelex` AS `mailingTelex`,`a`.`mailingZip` AS `mailingZip`,`a`.`partnerPrefix` AS `partnerPrefix`,`a`.`partnerSuffix` AS `partnerSuffix`,`a`.`terminalAddress1` AS `terminalAddress1`,`a`.`terminalAddress2` AS `terminalAddress2`,`a`.`terminalAddress3` AS `terminalAddress3`,`a`.`terminalAddress4` AS `terminalAddress4`,`a`.`terminalCity` AS `terminalCity`,`a`.`terminalCountry` AS `terminalCountry`,`a`.`terminalCountryCode` AS `terminalCountryCode`,`a`.`terminalEmail` AS `terminalEmail`,`a`.`terminalFax` AS `terminalFax`,`a`.`terminalPhone` AS `terminalPhone`,`a`.`terminalState` AS `terminalState`,`a`.`terminalTelex` AS `terminalTelex`,`a`.`terminalZip` AS `terminalZip`,`a`.`isPrivateParty` AS `isPrivateParty`,`a`.`air` AS `air`,`a`.`sea` AS `sea`,`a`.`surface` AS `surface`,`a`.`isOwnerOp` AS `isOwnerOp`,`a`.`typeOfVendor` AS `typeOfVendor`,`a`.`agentParent` AS `agentParent`,`a`.`location1` AS `location1`,`a`.`location2` AS `location2`,`a`.`location3` AS `location3`,`a`.`location4` AS `location4`,`a`.`companyProfile` AS `companyProfile`,`a`.`url` AS `url`,`a`.`latitude` AS `latitude`,`a`.`longitude` AS `longitude`,`a`.`trackingUrl` AS `trackingUrl`,`a`.`yearEstablished` AS `yearEstablished`,`a`.`companyFacilities` AS `companyFacilities`,`a`.`companyCapabilities` AS `companyCapabilities`,`a`.`companyDestiantionProfile` AS `companyDestiantionProfile`,`a`.`serviceRangeKms` AS `serviceRangeKms`,`a`.`serviceRangeMiles` AS `serviceRangeMiles`,`a`.`fidiNumber` AS `fidiNumber`,`a`.`OMNINumber` AS `OMNINumber`,`a`.`IAMNumber` AS `IAMNumber`,`a`.`AMSANumber` AS `AMSANumber`,`a`.`WERCNumber` AS `WERCNumber`,`a`.`facilitySizeSQFT` AS `facilitySizeSQFT`,`a`.`qualityCertifications` AS `qualityCertifications`,`a`.`vanLineAffiliation` AS `vanLineAffiliation`,`a`.`serviceLines` AS `serviceLines`,`a`.`facilitySizeSQMT` AS `facilitySizeSQMT`,`b`.`corpid` AS `corpid`,`b`.`warehouse` AS `warehouse`,`a`.`createdBy` AS `createdBy`,`a`.`createdOn` AS `createdOn`,`a`.`updatedBy` AS `updatedBy`,`a`.`updatedOn` AS `updatedOn`,`a`.`firstName` AS `firstName`,`a`.`lastName` AS `lastName`,if((`a`.`isPrivateParty` is true),`a`.`status`,`b`.`status`) AS `status`,`a`.`nextVanLocation` AS `nextVanLocation`,`a`.`nextReportOn` AS `nextReportOn`,`b`.`billingInstruction` AS `billingInstruction`,`b`.`billingInstructionCode` AS `billingInstructionCode`,`b`.`coordinator` AS `coordinator`,`b`.`salesMan` AS `salesMan`,`b`.`qc` AS `qc`,`b`.`abbreviation` AS `abbreviation`,`b`.`billPayOption` AS `billPayOption`,`b`.`billPayType` AS `billPayType`,`b`.`billToGroup` AS `billToGroup`,`b`.`longPercentage` AS `longPercentage`,`b`.`needAuth` AS `needAuth`,`a`.`partnerCode` AS `partnerCode`,`b`.`storageBillingGroup` AS `storageBillingGroup`,`b`.`accountHolder` AS `accountHolder`,`b`.`paymentMethod` AS `paymentMethod`,`b`.`payOption` AS `payOption`,`b`.`multiAuthorization` AS `multiAuthorization`,`b`.`payableUploadCheck` AS `payableUploadCheck`,`b`.`companyDivision` AS `companyDivision`,`b`.`invoiceUploadCheck` AS `invoiceUploadCheck`,`b`.`billingUser` AS `billingUser`,`b`.`payableUser` AS `payableUser`,`b`.`pricingUser` AS `pricingUser`,`b`.`partnerPortalActive` AS `partnerPortalActive`,`b`.`partnerPortalId` AS `partnerPortalId`,`b`.`associatedAgents` AS `associatedAgents`,`b`.`viewChild` AS `viewChild`,`b`.`creditTerms` AS `creditTerms`,`b`.`accountingDefault` AS `accountingDefault`,`b`.`acctDefaultJobType` AS `acctDefaultJobType`,`b`.`stopNotAuthorizedInvoices` AS `stopNotAuthorizedInvoices`,`b`.`doNotCopyAuthorizationSO` AS `doNotCopyAuthorizationSO`,`b`.`partnerPublicId` AS `partnerPublicId`,`b`.`driverAgency` AS `driverAgency`,`b`.`validNationalCode` AS `validNationalCode`,`b`.`cardNumber` AS `cardNumber`,`b`.`cardStatus` AS `cardStatus`,`b`.`accountId` AS `accountId`,`b`.`customerId` AS `customerId`,`b`.`licenseNumber` AS `licenseNumber`,`b`.`licenseState` AS `licenseState`,`b`.`cardSettelment` AS `cardSettelment`,`b`.`directDeposit` AS `directDeposit`,`b`.`fuelPurchase` AS `fuelPurchase`,`b`.`expressCash` AS `expressCash`,`b`.`creditCheck` AS `creditCheck`,`b`.`creditAmount` AS `creditAmount`,`b`.`creditCurrency` AS `creditCurrency`,`b`.`yruAccess` AS `yruAccess`,`b`.`taxId` AS `taxId`,`b`.`taxIdType` AS `taxIdType`,`b`.`extReference` AS `extReference`,`a`.`startDate` AS `startDate`,`b`.`accountManager` AS `accountManager`,`b`.`classcode` AS `classcode`,`a`.`vatNumber` AS `vatNumber`,`a`.`utsNumber` AS `utsNumber`,`a`.`isNetworkPartner` AS `isNetworkPartner`,`a`.`vanLastLocation` AS `vanLastLocation`,`a`.`vanLastReportOn` AS `vanLastReportOn`,`a`.`vanAvailCube` AS `vanAvailCube`,`a`.`vanLastReportTime` AS `vanLastReportTime`,`a`.`currentVanAgency` AS `currentVanAgency`,`a`.`currentVanID` AS `currentVanID`,`a`.`currentTractorAgency` AS `currentTractorAgency`,`a`.`currentTractorID` AS `currentTractorID`,`b`.`driverType` AS `driverType`,`b`.`noDispatch` AS `noDispatch`,`a`.`aliasName` AS `aliasName`,`a`.`networkGroup` AS `networkGroup`,`b`.`insuranceAuthorized` AS `insuranceAuthorized`,`b`.`mc` AS `mc`,`b`.`soAllDrivers` AS `soAllDrivers`,`a`.`contactName` AS `contactName`,`a`.`billingCurrency` AS `billingCurrency`,`a`.`bankCode` AS `bankCode`,`a`.`bankAccountNumber` AS `bankAccountNumber`,`a`.`agentParentName` AS `agentParentName`,`a`.`ugwwNetworkGroup` AS `ugwwNetworkGroup`,`b`.collection AS collection
from (`partnerpublic` `a` join `partnerprivate` `b`) where (`a`.`id` = `b`.`partnerPublicId`);

ALTER TABLE `redsky`.`mssgrid` MODIFY COLUMN `length` DOUBLE DEFAULT NULL,  MODIFY COLUMN `width` DOUBLE DEFAULT NULL,  MODIFY COLUMN `height` DOUBLE DEFAULT NULL;

ALTER TABLE `redsky`.`mss` MODIFY COLUMN `mssOrderOriginPhoneNumbers` VARCHAR(100) ,MODIFY COLUMN `mssOrderDestinationPhoneNumbers` VARCHAR(100) ;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `vanlineMaximumAmount` DECIMAL(19,4) ,ADD COLUMN `vanlineExceptionChargeCode` VARCHAR(225) DEFAULT '' AFTER `vanlineMaximumAmount`,ADD COLUMN `ownbillingVanline` bit(1) DEFAULT b'0' AFTER `vanlineExceptionChargeCode`;

update systemdefault set ownbillingVanline = false ;

CREATE TABLE `redsky`.`t20VisionLogInfo` (
  `id` BIGINT NOT NULL DEFAULT NULL AUTO_INCREMENT,
  `corpID` VARCHAR(10) NOT NULL,
  `entryNumber` BIGINT(20),
  `invoiceNumber` VARCHAR(100),
  `orderNumber` VARCHAR(50),
  `orderNumberLine` BIGINT(20),
  `status` VARCHAR(10),
  `createdBy` VARCHAR(80),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(80),
  `updatedOn` DATETIME,
  `error` VARCHAR(500),
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('ServiceOrder','job' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),('CustomerFile','job' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');
      
Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)
values('TSFT', 'ltolares', 'ltolares', 20, 'COLLECTION','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'International', 'International', 20, 'COLLECTION','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'Domestic', 'Domestic', 20, 'COLLECTION','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'fivy', 'fivy', 20, 'COLLECTION','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'Logistics', 'Logistics', 20, 'COLLECTION','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'Storage', 'Storage', 20, 'COLLECTION','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','COLLECTION',20,true,now(),'dkumar',now(),'dkumar',1,1,'');      

ALTER TABLE `redsky`.`integrationloginfo` MODIFY COLUMN `detailedStatus` VARCHAR(1000);

ALTER TABLE `redsky`.`t20VisionLogInfo` MODIFY COLUMN `entryNumber` VARCHAR(20),
MODIFY COLUMN `orderNumberLine` VARCHAR(20) DEFAULT NULL;

ALTER TABLE `redsky`.`frequentlyaskedquestions` ADD COLUMN `jobType` VARCHAR(65) AFTER `sequenceNumber`;

update partnerprivate set collection=accountholder where corpid='SSCW';


