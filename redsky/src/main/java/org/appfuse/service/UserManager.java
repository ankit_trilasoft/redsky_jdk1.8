package org.appfuse.service;

import java.util.Date;
import java.util.List;

import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.appfuse.dao.UserDao;
import org.appfuse.model.User;

import com.opensymphony.xwork2.ActionContext;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * <p><a href="UserManager.java.html"><i>View Source</i></a></p>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 *  Modified by <a href="mailto:dan@getrolling.com">Dan Kibler </a> 
 */
public interface UserManager {
    
    public void setUserDao(UserDao userDao);

    /**
     * Retrieves a user by userId.  An exception is thrown if user not found
     *
     * @param userId
     * @return User
     */
    public User getUser(String userId);
    
    /**
     * Finds a user by their username.
     * @param username
     * @return User a populated user object
     */
    public User getUserByUsername(String username) throws UsernameNotFoundException;

    /**
     * Retrieves a list of users, filtering with parameters on a user object
     * @param user parameters to filter on
     * @return List
     */
    public List getUsers(User user);

    /**
     * Saves a user's information
     *
     * @param user the user's information
     * @throws UserExistsException
     */
    public void saveUser(User user) throws UserExistsException;

    /**
     * Removes a user from the database by their userId
     *
     * @param userId the user's id
     */
    public void removeUser(String userId);
    public List findUserRoles(String corpID);
    public List findUserRoles(String corpID,String category);
    public List findUserPassword(String corpID);
    public List findUserDetails(String userName);
	public void resetPassword(String passwordNew,String confirmPasswordNew,String pwdHintQues,String pwdHint,String userName);
	public void findPwordHintQ(String pwdHintQues,String userName);
	public void forgotPassword(String pwdHintQues,String pwdHint,String userName);
	public void removeUserPhotograph(String location,String userName);

	public List getDateSecuritySet();

	public List getPermissionList();

	public List getPermission(String permissionName);

	public List getSuperVisor(String corpID);

	public List getPartnerExpirationDate(String sessionCorpID, String username);
	public void updatePasswordExpiryDate(String username, String sessionCorpID, Date compareWithDate);

	public void setExpirePassword(String username, String sessionCorpID);

	public void updatePassword(String passwordNew, Long id);

	public List findDateDiff(Date pwdexpiryDate, Date compareWithDate);
	public List getContactListByPartnerCode(String partnerCode,String corpID);
	public List getFromEmailAddress(String corpID);

	public List findUserByUsername(String coordinator);
	public List getAllUserList(String corpID);
	public List getAllUserRole(String userList,String sessionCorpID,String activeStatus);
	public List pickAllUser(String roleList,String sessionCorpID,String userList,String activeStatus);
	public int updateUserWithRoles(String transferFrom,String roleTransfer,String transferTo,String sessionCorpID);
	public List getAllUserContactListByPartnerCode(String partnerCode,String corpID);
	/* Modified by Kunal Kumar Sharma for ticket number: 6838 */
	public List getAllUserContactListByPartnerCode(String partnerCode,String corpID,String name, String emailId);
	/* Modification ends */
	public List findCoordinatorByUsername(String coordinator,String sessionCorpID);

	public List findCoordinatorByNetworkCoordinator(String coordinator, String corpID);
	//Added For #7588
	public void updateUserNewsUpdateFlag(String corpid,Boolean flag,String username);
	public String doNotEmailFlag(String email);
	public String findToolTip(String username, String corpID);

	public List getSearchContactListByPartnerCode(String partnerCode,String corpID,String name, String emailId);

	public List findUserDetail(String coordinator, String sessionCorpID);
	public void updateUserInfo(String updateBy,String userID,String datasecurityID);
	public void updateUserRoleInfo(String updateBy,String userID,String roleID);
	public void updateUserLoginFailureReason(String userName,String chng,String chng1);
	public int getDefaultContactPersonCount(String partnerCode, String corpID);

	public List getUsersByParentAgent(String parentAgent);
	public List getAllInactiveUserList(String corpID);
	
	/* sonu contact search*/
	public List getContactList(String partnerCode,String corpID,String name, String emailId,String phone_number);
	
	/* sonu agent request search*/
	public List getagentRequestList(String partnerCode,String corpID,String name, String emailId,String phone_number,String status);
	public List getUserRequestDetailList(String email);
	public List findUserUsername(String coordinator);
}
