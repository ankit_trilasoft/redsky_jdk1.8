<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<c:choose>
<c:when test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Single section'}">
<table cellpadding="0" cellspacing="0"  border="0" width="" style="margin: 0px">
			<tr>
				<td>				
					<fieldset style="margin-top:6px;min-height:150px;min-width:400px; ">
						<legend>Documents</legend>
							<div style="height:145px;width:auto;overflow-x:auto;overflow-y:auto;">
								<div style="max-width:980px; white-space: nowrap;">
									<table cellpadding="0" cellspacing="0"  border="0" width="" style="margin: 0px">
										 <c:if test="${not empty incExcDocList}">
								           <c:forEach var="entry" items="${incExcDocList}" varStatus="rowCounter">
								           <td class="listwhitetext" style="min-width:150px;padding-right:20px;">
								           <c:set var="checkOpt" value="false" />
                    						<c:forTokens items="${incExcDocServiceList}" delims="#" var="kc">
										    <c:if test="${kc == entry.id && !checkOpt}">
										    	<c:set var="checkOpt" value="true" />
										    </c:if>
											</c:forTokens> 
								           
								            <c:if test="${checkOpt}">
								           	<input type="checkbox" style="vertical-align:middle;" name="docChecked" id="${entry.id}" checked="checked" onclick="pickCheckBoxDocDataSec();"/>
								           	</c:if>
                          					<c:if test="${!checkOpt}">
                          					<input type="checkbox" style="vertical-align:middle;" name="docChecked" id="${entry.id}" onclick="pickCheckBoxDocDataSec();"/>
                          					</c:if>
								           	&nbsp;${entry.description}
								           	<c:choose>
									          <c:when test="${rowCounter.count % 2 == 0 && rowCounter.count != 0}">
									          <tr></tr>
									          </c:when>
									         <c:when test="${rowCounter.count != -1}">
									          </c:when>
										          <c:otherwise>
										          </c:otherwise>
										        </c:choose>
										       </td>
								           </c:forEach>
								          </c:if>
									</table>
								</div>
							</div>
					</fieldset>
				</td>
			</tr>
	</table>
</c:when>
<c:when test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Separate section'}">
<table cellpadding="0" cellspacing="0"  border="0" width="" style="margin: 0px">
			<tr>
				<td style="">				
					<fieldset style="margin-top:6px;min-height:150px;min-width:400px; ">
						<legend>Documents</legend>
							<div style="height:145px;width:auto;overflow-x:auto;overflow-y:auto;">
								<div style="max-width:980px; white-space: nowrap;">
									<table cellpadding="0" cellspacing="0"  border="0" width="" style="margin: 0px">
										 <c:if test="${not empty incExcDocList}">
								           <c:forEach var="entry" items="${incExcDocList}" varStatus="rowCounter">
								           <td class="listwhitetext" style="min-width:150px;padding-right:20px;">
								           <c:set var="checkOpt" value="false" />
								           <c:set var="keyValV" value="${entry.id}:DOC" />
                    						<c:forTokens items="${incExcDocServiceList}" delims="#" var="kc">
										    <c:if test="${kc == keyValV && !checkOpt}">
										    	<c:set var="checkOpt" value="true" />
										    </c:if>
											</c:forTokens> 
								           
								            <c:if test="${checkOpt}">
								           	<input type="checkbox" style="vertical-align:middle;" name="docChecked" id="${entry.id}" value="DOC" checked="checked" onclick="pickCheckBoxDocData();"/>
								           	</c:if>
                          					<c:if test="${!checkOpt}">
                          					<input type="checkbox" style="vertical-align:middle;" name="docChecked" id="${entry.id}" value="DOC" onclick="pickCheckBoxDocData();"/>
                          					</c:if>
								           	&nbsp;${entry.description}
								           	<c:choose>
									          <c:when test="${rowCounter.count % 2 == 0 && rowCounter.count != 0}">
									          <tr></tr>
									          </c:when>
									         <c:when test="${rowCounter.count != -1}">
									          </c:when>
										          <c:otherwise>
										          </c:otherwise>
										        </c:choose>
										       </td>
								           </c:forEach>
								          </c:if>
									</table>
								</div>
							</div>
					</fieldset>
				</td>
			</tr>
	</table>
</c:when>
<c:otherwise></c:otherwise>
</c:choose>