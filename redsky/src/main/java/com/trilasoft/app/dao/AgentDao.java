package com.trilasoft.app.dao;
import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.Agent;

public interface AgentDao extends GenericDao<Agent, Long> {   
    public List<Agent> findByLastName(String lastName);  
    public List checkById(Long id);
    public List findMaximumCarrierNumber(String shipNum);
}
