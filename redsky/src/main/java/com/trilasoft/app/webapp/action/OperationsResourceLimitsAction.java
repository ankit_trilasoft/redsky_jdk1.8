package com.trilasoft.app.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.trilasoft.app.model.EquipMaterialsCost;
import com.trilasoft.app.model.EquipMaterialsLimits;
import com.trilasoft.app.model.OperationsResourceLimits;
import com.trilasoft.app.service.EquipMaterialsCostManager;
import com.trilasoft.app.service.EquipMaterialsLimitsManager;
import com.trilasoft.app.service.OperationsResourceLimitsManager;
import com.trilasoft.app.service.RefMasterManager;

public class OperationsResourceLimitsAction extends BaseAction {

	private OperationsResourceLimits operationsResourceLimits;	
	private OperationsResourceLimitsManager operationsResourceLimitsManager;
	private EquipMaterialsLimits equipMaterialsLimits;	
	private EquipMaterialsLimitsManager equipMaterialsLimitsManager;
	private EquipMaterialsCostManager equipMaterialsCostManager;
	private List resourceLimits;
	private Long id;
	private String sessionCorpID;
	private String gotoPageString;
	private String validateFormNav;
	private String hitFlag;
	private String hub;
	private String category;
	 private String valFlag;
	static final Logger logger = Logger.getLogger(ItemsJEquipAction.class);
	private EquipMaterialsCost equipMaterialsCost;
	Date currentdate = new Date();
    private RefMasterManager refMasterManager;
    private Map<String, String> resourceCategory;
    private Map<String, String> comapnyDivisionCodeDescp;
    private List itemList;
    private String flagType;
    private String resource;
    private String branch;
    private String division;
    private String cngres;
    private String cngwhouse;
    private String backUpOnSave;
    private Map<String, String> house;
    private Map<String, String> invCategMap=new HashMap<String, String>();
	
	public String saveOnTabChange() throws Exception {
		String s = save();
		validateFormNav = "OK";
		return gotoPageString;
	}

	public OperationsResourceLimitsAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public String edit() {
		getComboList(sessionCorpID);
		hub1 = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house1 = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		distinctHubDescription=new HashMap<String, String>();
		distinctHubDescription.putAll(hub1);
		distinctHubDescription.putAll(house1);
		if (id != null) {
			operationsResourceLimits = operationsResourceLimitsManager.get(id);
		} else {
			operationsResourceLimits = new OperationsResourceLimits();
			operationsResourceLimits.setCorpId(sessionCorpID);
			operationsResourceLimits.setCreatedBy(getRequest().getRemoteUser());
			operationsResourceLimits.setCreatedOn(new Date());
			operationsResourceLimits.setUpdatedOn(new Date());
			operationsResourceLimits.setUpdatedBy(getRequest().getRemoteUser());
		}

		return SUCCESS;
	}
	public String editEquipMaterialLimit() {
		getComboList(sessionCorpID);
		hub1 = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house1 = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		distinctHubDescription=new HashMap<String, String>();
		if(hub1!=null)
		distinctHubDescription.putAll(hub1);
		if(house1!=null)
		distinctHubDescription.putAll(house1);
		if (id != null) {
			equipMaterialsLimits = equipMaterialsLimitsManager.get(id);
			equipMaterialsCost=equipMaterialsCostManager.getByMaterailId(equipMaterialsLimits.getId());
		} else {
			equipMaterialsLimits = new EquipMaterialsLimits();
			equipMaterialsLimits.setCorpId(sessionCorpID);
			equipMaterialsLimits.setCreatedBy(getRequest().getRemoteUser());
			equipMaterialsLimits.setCreatedOn(new Date());
			equipMaterialsLimits.setUpdatedOn(new Date());
			equipMaterialsLimits.setUpdatedBy(getRequest().getRemoteUser());
			equipMaterialsCost=new EquipMaterialsCost();
			equipMaterialsCost.setCorpID(sessionCorpID);
			equipMaterialsCost.setCreatedBy(getRequest().getRemoteUser());
			equipMaterialsCost.setCreatedOn(new Date());
			equipMaterialsCost.setUpdatedOn(new Date());
			equipMaterialsCost.setUpdatedBy(getRequest().getRemoteUser());
		}

		return SUCCESS;
	}
	private List checkResource;
	private String dummsectionName;
	
	public String saveOnEdit() throws Exception {
		getComboList(sessionCorpID);
		equipMaterialsLimits.setCorpId(sessionCorpID);
		equipMaterialsLimits.setUpdatedOn(new Date());
		equipMaterialsLimits.setUpdatedBy(getRequest().getRemoteUser());
		if(equipMaterialsLimits.getCategory()==null || equipMaterialsLimits.getCategory().equals("")){
			String key="Category is Required";
			errorMessage(key);
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			valFlag="openSaveBlock";
			return INPUT;
		}
		if((equipMaterialsLimits.getResource()==null || equipMaterialsLimits.getResource().equals("")) && (cngres.equals("true"))){
			String key="Description is Required";
			errorMessage(key);
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			valFlag="openSaveBlock";
			return INPUT;
		}
		if((equipMaterialsLimits.getBranch()==null || equipMaterialsLimits.getBranch().equals("")) && cngwhouse.equals("true"))  {
			String key="Warehouse is Required";
			errorMessage(key);
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			valFlag="openSaveBlock";
			return INPUT;
		}
		if(equipMaterialsLimits.getMaxResourceLimit()==null || equipMaterialsLimits.getMaxResourceLimit().equals("")){
			String key="Max Resource Limit is Required";
			errorMessage(key);
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			valFlag="openSaveBlock";
			return INPUT;
		}
		if(equipMaterialsLimits.getMinResourceLimit()==null || equipMaterialsLimits.getMinResourceLimit().equals("")){
			String key="Min Resource Limit is Required";
			errorMessage(key);
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			valFlag="openSaveBlock";
			return INPUT;
		}
		if(equipMaterialsLimits.getDivision()==null || equipMaterialsLimits.getDivision().equals("")){
			String key="Division is Required";
			errorMessage(key);
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			valFlag="openSaveBlock";
			return INPUT;
		}
		if((equipMaterialsLimits.getMaxResourceLimit()!=null && !equipMaterialsLimits.getMaxResourceLimit().equals("")) && (equipMaterialsLimits.getMinResourceLimit()!=null && !equipMaterialsLimits.getMinResourceLimit().equals(""))){
			if((equipMaterialsLimits.getMaxResourceLimit().compareTo(equipMaterialsLimits.getMinResourceLimit()))==-1){
				String key="Min Limit should not Exceed Max Limit ";
				errorMessage(key);
				getRequest().setAttribute("backUpOnSave",backUpOnSave);
				valFlag="openSaveBlock";
				return INPUT;
			}
			
		}
		
		if(equipMaterialsCost.getUnitCost()==null || equipMaterialsCost.getUnitCost().equals("")){			
			String key="Unit Cost is Required";
			errorMessage(key);
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			valFlag="openSaveBlock";
			return INPUT;			
		
	}
		
		boolean isNew = (equipMaterialsLimits.getId() == null);
		if(isNew){
			if(equipMaterialsLimits.getQty()==null || (String.valueOf(equipMaterialsLimits.getQty()).equals(""))){
				equipMaterialsLimits.setQty(Integer.parseInt(String.valueOf(equipMaterialsLimits.getMaxResourceLimit())));
			}
		}
		
		
		try{
			 equipMaterialsLimitsManager.updateEquipMaterialObject(equipMaterialsLimits,cngres,cngwhouse);
			if(isNew){
				
				equipMaterialsCost.setCorpID(sessionCorpID);
				equipMaterialsCost.setCreatedOn(new Date());
				equipMaterialsCost.setCreatedBy(getRequest().getRemoteUser());
				equipMaterialsCost.setEquipMaterialsId(equipMaterialsLimits.getId());
				equipMaterialsCostManager.save(equipMaterialsCost);
				//equipMaterialsCost.setCreatedOn(new Date());
				//equipMaterialsCost.setCreatedBy(getRequest().getRemoteUser());
				
			}else{
				equipMaterialsCost.setCorpID(sessionCorpID);
				equipMaterialsCost.setUpdatedOn(new Date());
				equipMaterialsCost.setUpdatedBy(getRequest().getRemoteUser());
				equipMaterialsCostManager.save(equipMaterialsCost);
				
			}
		String key = (isNew) ? "New Item successfully added" : "Item has been successfully updated";
		saveMessage(getText(key));
		hitFlag="1";
		
		/*if(operationsResourceLimits.getHubId().equalsIgnoreCase("DC-Metro")){
			operationsResourceLimits = operationsResourceLimitsManager.get(operationsResourceLimits.getId());
			OperationsResourceLimits operResourceLimits = new OperationsResourceLimits() ;
			operResourceLimits.setCategory(operationsResourceLimits.getCategory());
			operResourceLimits.setCorpId(operationsResourceLimits.getCorpId());
			operResourceLimits.setCreatedBy(getRequest().getRemoteUser());
			operResourceLimits.setCreatedOn(new Date());
			operResourceLimits.setHubId("0");
			operResourceLimits.setResource(operationsResourceLimits.getResource());
			operResourceLimits.setResourceLimit(operationsResourceLimits.getResourceLimit());
			operResourceLimits.setUpdatedBy(getRequest().getRemoteUser());
			operResourceLimits.setUpdatedOn(new Date());
			operationsResourceLimitsManager.save(operResourceLimits);
		}*/
		return SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
	   		String key = "Item already exists. Please update existing entry.";
			errorMessage(getText(key));
			operationsResourceLimits.setResource(dummsectionName);
			return INPUT; 
		}
	}
	
	
	
	public String saveEquipMaterialLimit() throws Exception {
		getComboList(sessionCorpID);
		equipMaterialsLimits.setCorpId(sessionCorpID);
		equipMaterialsLimits.setUpdatedOn(new Date());
		equipMaterialsLimits.setUpdatedBy(getRequest().getRemoteUser());
		if(equipMaterialsLimits.getCategory()==null || equipMaterialsLimits.getCategory().equals("")){
			String key="Category is Required";
			errorMessage(key);
			valFlag="openSaveBlock";
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			return INPUT;
		}
		if((equipMaterialsLimits.getResource()==null || equipMaterialsLimits.getResource().equals("")) ){
			String key="Description is Required";
			errorMessage(key);
			valFlag="openSaveBlock";
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			return INPUT;
		}
		if((equipMaterialsLimits.getBranch()==null || equipMaterialsLimits.getBranch().equals("")) ){
			String key="Warehouse is Required";
			errorMessage(key);
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			return INPUT;
		}
		if(equipMaterialsLimits.getMaxResourceLimit()==null || equipMaterialsLimits.getMaxResourceLimit().equals("")){
			String key="Max Resource Limit is Required";
			errorMessage(key);
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			valFlag="openSaveBlock";
			return INPUT;
		}
		if(equipMaterialsLimits.getMinResourceLimit()==null || equipMaterialsLimits.getMinResourceLimit().equals("")){
			String key="Min Resource Limit is Required";
			errorMessage(key);
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			valFlag="openSaveBlock";
			return INPUT;
		}
		if(equipMaterialsLimits.getDivision()==null || equipMaterialsLimits.getDivision().equals("")){
			String key="Division is Required";
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			errorMessage(key);
			valFlag="openSaveBlock";
			return INPUT;
		}
		if((equipMaterialsLimits.getMaxResourceLimit()!=null && !equipMaterialsLimits.getMaxResourceLimit().equals("")) && (equipMaterialsLimits.getMinResourceLimit()!=null && !equipMaterialsLimits.getMinResourceLimit().equals(""))){
			if((equipMaterialsLimits.getMaxResourceLimit().compareTo(equipMaterialsLimits.getMinResourceLimit()))==-1){
				String key="Min Limit should not Exceed Max Limit ";
				errorMessage(key);
				getRequest().setAttribute("backUpOnSave",backUpOnSave);
				valFlag="openSaveBlock";
				return INPUT;
			}
			
		}
		if(equipMaterialsCost.getUnitCost()==null || equipMaterialsCost.getUnitCost().equals("")){			
				String key="Unit Cost is Required";
				getRequest().setAttribute("backUpOnSave",backUpOnSave);
				errorMessage(key);
				valFlag="openSaveBlock";
				return INPUT;			
			
		}
		boolean isNew = (equipMaterialsLimits.getId() == null);
		/**if(isNew){
			if(equipMaterialsLimits.getQty()==null || (String.valueOf(equipMaterialsLimits.getQty()).equals(""))){
				//int i=0;				
				equipMaterialsLimits.setQty(equipMaterialsLimits.getMaxResourceLimit().intValue());
			}
		}*/
		
		try{
			equipMaterialsLimits= equipMaterialsLimitsManager.save(equipMaterialsLimits);
			if(isNew){
				
				equipMaterialsCost.setCorpID(sessionCorpID);
				equipMaterialsCost.setCreatedOn(new Date());
				equipMaterialsCost.setCreatedBy(getRequest().getRemoteUser());
				equipMaterialsCost.setEquipMaterialsId(equipMaterialsLimits.getId());
				equipMaterialsCostManager.save(equipMaterialsCost);
				//equipMaterialsCost.setCreatedOn(new Date());
				//equipMaterialsCost.setCreatedBy(getRequest().getRemoteUser());
				
			}else{
				equipMaterialsCost.setCorpID(sessionCorpID);
				equipMaterialsCost.setUpdatedOn(new Date());
				equipMaterialsCost.setUpdatedBy(getRequest().getRemoteUser());
				equipMaterialsCostManager.save(equipMaterialsCost);
				
			}
		String key = (isNew) ? "New Item successfully added" : "Item has been successfully updated";
		saveMessage(getText(key));
		hitFlag="1";
		
		/*if(operationsResourceLimits.getHubId().equalsIgnoreCase("DC-Metro")){
			operationsResourceLimits = operationsResourceLimitsManager.get(operationsResourceLimits.getId());
			OperationsResourceLimits operResourceLimits = new OperationsResourceLimits() ;
			operResourceLimits.setCategory(operationsResourceLimits.getCategory());
			operResourceLimits.setCorpId(operationsResourceLimits.getCorpId());
			operResourceLimits.setCreatedBy(getRequest().getRemoteUser());
			operResourceLimits.setCreatedOn(new Date());
			operResourceLimits.setHubId("0");
			operResourceLimits.setResource(operationsResourceLimits.getResource());
			operResourceLimits.setResourceLimit(operationsResourceLimits.getResourceLimit());
			operResourceLimits.setUpdatedBy(getRequest().getRemoteUser());
			operResourceLimits.setUpdatedOn(new Date());
			operationsResourceLimitsManager.save(operResourceLimits);
		}*/
		return SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
	   		String key = "Item already exists. Please update existing entry.";
			errorMessage(getText(key));
			getRequest().setAttribute("backUpOnSave",backUpOnSave);
			System.out.println("hiden valueeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"+backUpOnSave);
			operationsResourceLimits.setResource(dummsectionName);
			return INPUT; 
		}
	}
	public String save() throws Exception {
		getComboList(sessionCorpID);
		operationsResourceLimits.setCorpId(sessionCorpID);
		operationsResourceLimits.setUpdatedOn(new Date());
		operationsResourceLimits.setUpdatedBy(getRequest().getRemoteUser());
		boolean isNew = (operationsResourceLimits.getId() == null);
		
		try{
		operationsResourceLimits = operationsResourceLimitsManager.save(operationsResourceLimits);
		String key = (isNew) ? "Operations Resource Limits added successfully" : "Operations Resource Limits updated successfully";
		saveMessage(getText(key));
		hitFlag="1";
		
		if(operationsResourceLimits.getHubId().equalsIgnoreCase("DC-Metro")){
			operationsResourceLimits = operationsResourceLimitsManager.get(operationsResourceLimits.getId());
			OperationsResourceLimits operResourceLimits = new OperationsResourceLimits() ;
			operResourceLimits.setCategory(operationsResourceLimits.getCategory());
			operResourceLimits.setCorpId(operationsResourceLimits.getCorpId());
			operResourceLimits.setCreatedBy(getRequest().getRemoteUser());
			operResourceLimits.setCreatedOn(new Date());
			operResourceLimits.setHubId("0");
			operResourceLimits.setResource(operationsResourceLimits.getResource());
			operResourceLimits.setResourceLimit(operationsResourceLimits.getResourceLimit());
			operResourceLimits.setUpdatedBy(getRequest().getRemoteUser());
			operResourceLimits.setUpdatedOn(new Date());
			operationsResourceLimitsManager.save(operResourceLimits);
		}
		return SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
	   		String key = "The limit for this resource has already been assigned. Please select another resource";
			errorMessage(getText(key));
			operationsResourceLimits.setResource(dummsectionName);
			return INPUT; 
		}
	}
	private  Map<String, String> distinctHubDescription;
	private  Map<String, String> hub1;
	private  Map<String, String> house1;
	
	@SkipValidation
	public String equipMaterialControlList() {
		hub1 = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house1 = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		distinctHubDescription=new HashMap<String, String>();
		distinctHubDescription.putAll(hub1);
		distinctHubDescription.putAll(house1);
		getComboList(sessionCorpID);
		resourceLimits = equipMaterialsLimitsManager.getAll();
		return SUCCESS;
	}
	
	@SkipValidation
	public String list() {
		hub1 = refMasterManager.findByParameterWhareHouse(sessionCorpID, "OPSHUB");
		house1 = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		distinctHubDescription=new HashMap<String, String>();
		distinctHubDescription.putAll(hub1);
		distinctHubDescription.putAll(house1);
		getComboList(sessionCorpID);
		resourceLimits = operationsResourceLimitsManager.getResourceLimitsByHub(hub, "NA");
		return SUCCESS;
	}

	@SkipValidation
	public String search() {
		getComboList(sessionCorpID);
		resourceLimits = operationsResourceLimitsManager.getResourceLimitsByHub(hub, category);
		return SUCCESS;
	}
	@SkipValidation
	public String searchEquipMaterialLimit() {
		getComboList(sessionCorpID);
		if(branch==null){
			branch="";
		}
		if(division==null){
			division="";
		}
		if(category==null){
			category="";
		}
		resourceLimits = equipMaterialsLimitsManager.getEquipMaterialLimitList(branch,division,category);
		return SUCCESS;
	}
	public String getComboList(String corpID) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
  	   	resourceCategory = refMasterManager.findByParameter(corpID, "Resource_Category");
  	    comapnyDivisionCodeDescp = equipMaterialsLimitsManager.getCompanyDivisionMap(corpID);
  	    house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
  	    invCategMap.put("E","Equipment");
  	    invCategMap.put("M","Material");
  	   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String findResourceList() {
		itemList = operationsResourceLimitsManager.getItemListByCategory(category, resource, sessionCorpID);
		return SUCCESS;
	}
	private String rowIndex;
	@SkipValidation
	public String assignResourcesNew() {
		itemList = operationsResourceLimitsManager.getItemListByCategory(category, resource, sessionCorpID);
		return SUCCESS;
	}
	
	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getHub() {
		return hub;
	}

	public void setHub(String hub) {
		this.hub = hub;
	}

	public List getResourceLimits() {
		return resourceLimits;
	}

	public void setResourceLimits(List resourceLimits) {
		this.resourceLimits = resourceLimits;
	}

	public OperationsResourceLimits getOperationsResourceLimits() {
		return operationsResourceLimits;
	}

	public void setOperationsResourceLimits(
			OperationsResourceLimits operationsResourceLimits) {
		this.operationsResourceLimits = operationsResourceLimits;
	}

	public void setOperationsResourceLimitsManager(
			OperationsResourceLimitsManager operationsResourceLimitsManager) {
		this.operationsResourceLimitsManager = operationsResourceLimitsManager;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getResourceCategory() {
		return resourceCategory;
	}

	public void setResourceCategory(Map<String, String> resourceCategory) {
		this.resourceCategory = resourceCategory;
	}

	public List getItemList() {
		return itemList;
	}

	public void setItemList(List itemList) {
		this.itemList = itemList;
	}

	public String getFlagType() {
		return flagType;
	}

	public void setFlagType(String flagType) {
		this.flagType = flagType;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public List getCheckResource() {
		return checkResource;
	}

	public void setCheckResource(List checkResource) {
		this.checkResource = checkResource;
	}

	public String getDummsectionName() {
		return dummsectionName;
	}

	public void setDummsectionName(String dummsectionName) {
		this.dummsectionName = dummsectionName;
	}

	public String getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(String rowIndex) {
		this.rowIndex = rowIndex;
	}

	public Map<String, String> getDistinctHubDescription() {
		return distinctHubDescription;
	}

	public void setDistinctHubDescription(Map<String, String> distinctHubDescription) {
		this.distinctHubDescription = distinctHubDescription;
	}

	public Map<String, String> getHub1() {
		return hub1;
	}

	public void setHub1(Map<String, String> hub1) {
		this.hub1 = hub1;
	}

	public Map<String, String> getHouse1() {
		return house1;
	}

	public void setHouse1(Map<String, String> house1) {
		this.house1 = house1;
	}

	public EquipMaterialsLimits getEquipMaterialsLimits() {
		return equipMaterialsLimits;
	}

	public void setEquipMaterialsLimits(EquipMaterialsLimits equipMaterialsLimits) {
		this.equipMaterialsLimits = equipMaterialsLimits;
	}

	public void setEquipMaterialsLimitsManager(
			EquipMaterialsLimitsManager equipMaterialsLimitsManager) {
		this.equipMaterialsLimitsManager = equipMaterialsLimitsManager;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public EquipMaterialsCost getEquipMaterialsCost() {
		return equipMaterialsCost;
	}

	public void setEquipMaterialsCost(EquipMaterialsCost equipMaterialsCost) {
		this.equipMaterialsCost = equipMaterialsCost;
	}

	public void setEquipMaterialsCostManager(
			EquipMaterialsCostManager equipMaterialsCostManager) {
		this.equipMaterialsCostManager = equipMaterialsCostManager;
	}

	public Map<String, String> getHouse() {
		return house;
	}

	public void setHouse(Map<String, String> house) {
		this.house = house;
	}

	public String getCngres() {
		return cngres;
	}

	public void setCngres(String cngres) {
		this.cngres = cngres;
	}

	public String getCngwhouse() {
		return cngwhouse;
	}

	public void setCngwhouse(String cngwhouse) {
		this.cngwhouse = cngwhouse;
	}

	public Map<String, String> getInvCategMap() {
		return invCategMap;
	}

	public void setInvCategMap(Map<String, String> invCategMap) {
		this.invCategMap = invCategMap;
	}

	public String getValFlag() {
		return valFlag;
	}

	public void setValFlag(String valFlag) {
		this.valFlag = valFlag;
	}

	public Map<String, String> getComapnyDivisionCodeDescp() {
		return comapnyDivisionCodeDescp;
	}

	public void setComapnyDivisionCodeDescp(
			Map<String, String> comapnyDivisionCodeDescp) {
		this.comapnyDivisionCodeDescp = comapnyDivisionCodeDescp;
	}

	public String getBackUpOnSave() {
		return backUpOnSave;
	}

	public void setBackUpOnSave(String backUpOnSave) {
		this.backUpOnSave = backUpOnSave;
	}

}