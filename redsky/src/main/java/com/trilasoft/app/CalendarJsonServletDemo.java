package com.trilasoft.app;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.gson.Gson;
import com.trilasoft.app.service.AppInitServletManager;

/**
 * Servlet implementation class CalendarJsonServletDemo
 */
public class CalendarJsonServletDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	AppInitServletManager appInitServletManager ;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CalendarJsonServletDemo() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Override
	public void init() throws ServletException {

		super.init();
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		User user=null;
		String corpid=null;
		String fromdt=request.getParameter("surveyFrom");
		String todate=request.getParameter("surveyTo");
		String consult=request.getParameter("consult");
		String surveyCity=request.getParameter("surveyCity");
		String surveyJob=request.getParameter("surveyJob");
		if(consult==null){
			consult="";
		}
		if(surveyCity==null){
			surveyCity="";
		}
		if(surveyJob==null){
			surveyJob="";
		}
		System.out.println("consult"+consult);
		System.out.println("surveyCity"+surveyCity);
		System.out.println("surveyJob"+surveyJob);
		StringBuilder newSurveyFromBuilder=null;
		StringBuilder newSurveyFromBuilder1=null;
		try {
			SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat(
					"yyyy-MM-dd");
			newSurveyFromBuilder = new StringBuilder(
					dateformatYYYYMMDD.format(new SimpleDateFormat(
							"dd-MMM-yy").parse(fromdt)));
			fromdt= newSurveyFromBuilder.toString();
		} catch (ParseException e) {

			e.printStackTrace();
		}
		try {
			SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat(
					"yyyy-MM-dd");
			newSurveyFromBuilder1 = new StringBuilder(
					dateformatYYYYMMDD.format(new SimpleDateFormat(
							"dd-MMM-yy").parse(todate)));
			todate= newSurveyFromBuilder1.toString();
		} catch (ParseException e) {

			e.printStackTrace();
		}
		
		if(fromdt!=null){
		System.out.println("Req From date..."+fromdt);	
		}
		if(todate!=null){
			System.out.println("Req to date..."+todate);	
			
		}
		List l = new ArrayList();
		try{
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			 user = (User) auth.getPrincipal();	
			 corpid = user.getCorpID();
			
			}catch(Exception ex)
			{
				 ex.printStackTrace();	
			}
		Map<String, Object> myData = new HashMap<String, Object>();
		Connection con=null;
		Statement st=null;
		ResultSet rs=null;
		//
		ApplicationContext applicationContext=null;
		AppInitServletManager appInitServletManager=null;
		if (applicationContext == null){
            System.out.println("setting context in get.............");
            applicationContext = WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
        }
        if (applicationContext != null && applicationContext.containsBean("appInitServletManager")){
        	System.out.println("get......................................");
        	appInitServletManager=(AppInitServletManager)applicationContext.getBean("appInitServletManager");
           // AccessBean thisAccessBean = (AccessBean) applicationContext.getBean("accessKeys");
            //req.setAttribute("keys", thisAccessBean.toString());
            System.out.println("setting keys");
        }
		//
		List list=appInitServletManager.getListForSurveyCalander(corpid,fromdt,todate,consult,surveyCity,surveyJob);
		Iterator data11 = list.iterator();
		CalendarDTO d=null;
		while (data11.hasNext()) {
			Object[] row = (Object[]) data11.next();
			 d = new CalendarDTO();	
			 String name="";
			// name=rs.getString("consultants");
			System.out.println("consultant"+name);
			String surDate="";					
			//String dt[]=surDate.split(" ");					
			//System.out.println("surDate"+surDate);
			String fromTime="";
			//System.out.println("surDate"+fromTime);
			String toTime="";
			//System.out.println("surDate"+toTime);
			int idd=0;
			//System.out.println("surDate"+idd);
			//if(dt!=null && (dt.length>0)){
			    if(row[0]!=null){
				d.setId(Integer.parseInt(row[0].toString()));
				 idd=Integer.parseInt(row[0].toString());
			    }
			    if(row[1]!=null){
			    	name=row[1].toString();
			    }else{
			    	name="";
			    }
			    if(row[2]!=null){
			    	//name=row[2].toString();
			    	d.setStart(row[2].toString());
			    	d.setEnd(row[2].toString());
			    }
			    if(row[3]!=null){
			    	fromTime=row[3].toString();
			    	//d.setStart(row[2].toString());
			    }else{
			    	fromTime="";	
			    }
			    if(row[4]!=null){
			    	toTime=row[4].toString();
			    	//d.setStart(row[2].toString());
			    }else{
			    	toTime="";
			    }
			    //d.setStart(surDate);					    
			   // d.setEnd(surDate);
			    d.setTitle("Survey Scheduled By "+name+"\n"+"From "+fromTime+" To "+toTime+" id "+idd);
			    d.setAllDay(false);
			    if(fromTime!=null){
			    d.setFromtime(fromTime);
			    }
			    l.add(d);
			
		
		
		}
		
	
		myData.put("id", 111);
		myData.put("title", "event1");
		myData.put("start", "2013-10-07");
		myData.put("url", "http://yahoo.com/");	
		CalendarDTO c = new CalendarDTO();
		 c.setId(1);
		 c.setStart("2013-10-07");
		 c.setEnd("2013-10-07");
		 c.setTitle("Task in Progress");		    
		CalendarDTO d1 = new CalendarDTO();
		    d1.setId(2);
		    d1.setStart("2013-10-08");
		    d1.setEnd("2013-10-08");
		    d1.setTitle("Task in Progress");	
		    //l.add(c);
			//l.add(d);	
		    Collections.sort(l);
			String json = new Gson().toJson(l);
			 System.out.println("json lennnnnnnnnnnnnnnnn"+json.length());
		    response.setContentType("application/json");
		  //  get.setContentType("application/json");
		 // Code commented for JDK 1.8 Upgrade
		//    response.setCharacterEncoding("UTF-8");
		    try {
		    	response.getWriter().write(json);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("22222222222222222222222222222222222222222222222222222222222222222222222222222222222");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	
	public void setAppInitServletManager(AppInitServletManager appInitServletManager) {
		this.appInitServletManager = appInitServletManager;
	}

}
