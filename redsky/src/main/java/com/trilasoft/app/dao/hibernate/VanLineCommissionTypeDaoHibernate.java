package com.trilasoft.app.dao.hibernate;

import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.VanLineCommissionTypeDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.VanLineCommissionType;


public class VanLineCommissionTypeDaoHibernate extends GenericDaoHibernate<VanLineCommissionType, Long> implements VanLineCommissionTypeDao {

	private HibernateUtil hibernateUtil;
	
	public VanLineCommissionTypeDaoHibernate() {   
        super(VanLineCommissionType.class);   
    }
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
    }
	
	static String replaceWord(String original, String find, String replacement) {
		int i = original.indexOf(find);
		if (i < 0) {
			return original;
		}
		String partBefore = original.substring(0, i);
		String partAfter = original.substring(i + find.length());
		return partBefore + replacement + partAfter;
	}

	public List<VanLineCommissionType> vanLineCommissionTypeList(){
		List commsionTypeList = null;
        String columns = "id, code, type, description, glCode, pc";
        commsionTypeList = getHibernateTemplate().find("select " + columns + " from VanLineCommissionType");
        try {
        	commsionTypeList = hibernateUtil.convertScalarToComponent(columns, commsionTypeList, VanLineCommissionType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return commsionTypeList;
	}
	
	public List<VanLineCommissionType> searchVanLineCommissionType(String code, String type, String description, String glCode){
		List commsionTypeList = null;
        String columns = "id, code, type, description, glCode, pc";
        commsionTypeList = getHibernateTemplate().find("select " + columns + " from VanLineCommissionType where code like '" + code.replaceAll("'", "''").replaceAll(":", "''") + "%' AND type like '" +type.replaceAll("'", "''").replaceAll(":", "''") + "%' AND description like '" +description.replaceAll("'", "''").replaceAll(":", "''") + "%' AND glCode like '" +glCode.replaceAll("'", "''").replaceAll(":", "''") + "%'");
        try {
        	commsionTypeList = hibernateUtil.convertScalarToComponent(columns, commsionTypeList, VanLineCommissionType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return commsionTypeList;
		
	}
	
	public List isExisted(String code, String corpId) {
		return getHibernateTemplate().find("from VanLineCommissionType where lower(code) = lower('"+code+"') and corpID='"+corpId+"'");
	}

	public List<VanLineCommissionType> getGLCodePopupList(String chargesGl) {
		
		return getHibernateTemplate().find("from VanLineCommissionType where (glCode='"+chargesGl+"' or glCode='' or glCode is null)");
	}
	
	public List glList(String chargesGl){
		return getHibernateTemplate().find("select distinct type from VanLineCommissionType where glCode=?",chargesGl);
	}

	public List searchVanLineCommissionPopup(String code, String type, String description, String glCode) {
		List commsionTypeList = null;
        String columns = "id, code, type, description, glCode, pc";
        commsionTypeList = getHibernateTemplate().find("select " + columns + " from VanLineCommissionType where code like '" + code.replaceAll("'", "''").replaceAll(":", "''") + "%' AND type = 'D' AND glCode like '" +glCode.replaceAll("'", "''").replaceAll(":", "''") + "%' and description like '" +description.replaceAll("'", "''").replaceAll(":", "''") + "%' ");
        try {
        	commsionTypeList = hibernateUtil.convertScalarToComponent(columns, commsionTypeList, VanLineCommissionType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return commsionTypeList;
	}
}
