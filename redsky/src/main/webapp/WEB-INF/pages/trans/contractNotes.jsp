<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="notesList.title"/></title>   
    <meta name="heading" content="<fmt:message key='notesList.heading'/>"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
	
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-32.5px;
padding:2px 0px;
text-align:right;
width:90%;
}
</style>
</head>
<s:form id="notesListForm" action="searchNotes" method="post" >  
<c:set var="notess" value="${notess}" />
<c:if test="${param.popup}"> 
	<c:set var="imageId" value="<%=request.getParameter("imageId") %>" />
	<s:hidden name="imageId" value="<%=request.getParameter("imageId") %>" />
	<c:set var="fieldId" value="<%=request.getParameter("fieldId") %>" />
	<s:hidden name="fieldId" value="<%=request.getParameter("fieldId") %>" />
</c:if>
<c:set var="noteFor" value="<%=request.getParameter("noteFor") %>"/>
<s:hidden name="id" value="<%=request.getParameter("id1") %>" />
<c:set var="id" value="<%=request.getParameter("id1") %>"/>
<s:hidden name="id1" value="<%=request.getParameter("id1") %>" />
<c:set var="subType" value="<%=request.getParameter("subType") %>"/>
<s:hidden name="pId" value="<%=request.getParameter("id") %>" />
<c:set var="pId" value="<%=request.getParameter("pId") %>"/>
<c:set var="accId" value="${accountProfile.id} " />
<s:hidden name="accId" value="${accountProfile.id}" />
<c:set var="accCont" value="${accountContact.id}" />
<s:hidden name="accCont" value="${accountContact.id}" />
<s:hidden name="accountProfile.id" />
<s:hidden name="accountProfile.corpID" />
<s:hidden name="accountProfile.partnerCode" />
<s:hidden name="accountContact.id" />
<s:hidden name="accountContact.partnerCode" />
<s:hidden name="accountContact.jobType" />
<s:hidden name="accountProfile.type" />
<s:hidden name="notes.notesId" />
<c:set var="for" value="<%=request.getParameter("for") %>"/>
<s:hidden name="for" value="<%=request.getParameter("for") %>" />
<% if (pageContext.findAttribute("accountProfile.id") != null){%>
	<s:hidden name="notes.customerNumber" value="XXXXXXXX" />
<%} %>
<% if (pageContext.findAttribute("accountContact.id") != null){%>
	<s:hidden name="notes.customerNumber" value="YYYYYYYY" />
<%} %>

<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>   
<% 
System.out.println( "1>>>>>>>>>>>>>>>>>>>>>>>>>>Notess List value= " +pageContext.findAttribute("notess") );
System.out.println( "\n2>>>>>>>>>>>>>>>>>>>>>>>>>>accountProfile.id value= " +pageContext.findAttribute("accountProfile.id") );
System.out.println( "\n3>>>>>>>>>>>>>>>>>>>>>>>>>>accountContact.id value= " +pageContext.findAttribute("accountContact.id") );
System.out.println( "\n4>>>>>>>>>>>>>>>>>>>>>>>>>>accountNotesFor value= " +pageContext.findAttribute("accountNotesFor") );
System.out.println( "\n5>>>>>>>>>>>>>>>>>>>>>>>>>>param.popup value= " +pageContext.findAttribute("param.popup") );
System.out.println( "\n6>>>>>>>>>>>>>>>>>>>>>>>>>>popupbuttons value= " +pageContext.findAttribute("popupbuttons") );
System.out.println( "\n7>>>>>>>>>>>>>>>>>>>>>>>>>>noteFor value= " +pageContext.findAttribute("noteFor") );
System.out.println( "\n8>>>>>>>>>>>>>>>>>>>>>>>>>>notesListForm value= " +pageContext.findAttribute("notesListForm") );
System.out.println( "\n9>>>>>>>>>>>>>>>>>>>>>>>>>>notesList value= " +pageContext.findAttribute("notesList") );
System.out.println( "\n9>>>>>>>>>>>>>>>>>>>>>>>>>>for value= " +request.getParameter("for") );
%>
<% System.out.println("********************************popupbuttons***************************************************"); %>
<c:set var="popupbuttons"> 
	<% if (pageContext.findAttribute("accountProfile.id") != null){ %>
	<input type="button" class="cssbutton" style="margin-right: 5px;height: 25px;width:90px; font-size: 15"  
       	onclick="location.href='<c:url value="/editNewNoteForPartner.html?id=${accId}&accountNotesFor=AP&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&pId=${pId}&fieldId=${fieldId} "/>'"  
       	value="<fmt:message key="button.addNewNote"/>"/> 
    <%}
	 if (pageContext.findAttribute("accountContact.id") != null){%>
	<input type="button" class="cssbutton" style="margin-right: 5px;height: 25px;width:90px; font-size: 15"  
       	onclick="location.href='<c:url value="/editNewNoteForPartner.html?id=${accCont }&accountNotesFor=AC&decorator=popup&popup=true&subType=${subType}&imageId=${imageId}&pId=${pId}&fieldId=${fieldId}&test=test "/>'"  
       	value="<fmt:message key="button.addNewNote"/>"/> 
   <%} %>
</c:set>
<% System.out.println("********************************tab Code ***************************************************"); %>
<c:if test="${param.popup}">
<% System.out.println("********************************tab Code 1.1***************************************************");
if(request.getParameter("for") != null && request.getParameter("for").equalsIgnoreCase("List")){
 System.out.println("********************************tab Code 1.2***************************************************"); %>
<c:if test="${noteFor=='AccountContact'}"> 
<% System.out.println("********************************tab Code 1.3***************************************************"); %>
<div id="newmnav">
				  <ul>
				    <li id="newmnav1" style="background:#FFF "><a class="current" ><span>List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    <li><a href="relatedNotesForContract.html?id=${pId}&id1=${id}&noteFor=${noteFor}&for=Related&decorator=popup&popup=true"><span>Related Notes</span></a></li>
				  </ul>
		</div><div class="spnblk">&nbsp;</div><br>
</c:if>
<c:if test="${noteFor=='AccountProfile'}"> 
<% System.out.println("********************************tab Code 2.3***************************************************"); %>
<div id="newmnav">
				  <ul>
				    <li id="newmnav1" style="background:#FFF "><a class="current" ><span>List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    <li><a href="relatedNotesForProfile.html?id=${pId}&id1=${id}&noteFor=${noteFor}&for=Related&decorator=popup&popup=true"><span>Related Notes</span></a></li>
				  </ul>
		</div><div class="spnblk">&nbsp;</div><br>
	
</c:if>
<%} %>
</c:if>
<% System.out.println("******************************** related tab Code ***************************************************"); %>
<c:if test="${param.popup}"> 
<% System.out.println("******************************** related tab Code 1.1 ***************************************************");
if(request.getParameter("for") != null && request.getParameter("for").equalsIgnoreCase("Related")){
 System.out.println("******************************** related tab Code 1.2 ***************************************************"); %>
<c:if test="${noteFor=='AccountContact'}">
<% System.out.println("******************************** related tab Code 1.3 ***************************************************"); %>
<div id="newmnav">
				  <ul>
				    <li><a onclick="openNotesPopup(this);"><span>Notes List</span></a></li>
				    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Related Notes<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				  </ul>
		</div><div class="spnblk">&nbsp;</div><br>
</c:if>
<c:if test="${noteFor=='AccountProfile'}">
 <% System.out.println("******************************** related tab Code 2.2 ***************************************************"); %>
<div id="newmnav">
				  <ul>
				    <li><a onclick="openNotesPopup1(this);"><span>Notes List</span></a></li>
				    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Related Notes<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				  </ul>
		</div><div class="spnblk">&nbsp;</div><br>
</c:if>
<%} %>
</c:if>
<%  System.out.println( "===========================  display table Starting ================================");%>
<div id="Layer1">				
				<s:set name="notess" value="notess" scope="request"/> 

				<c:if test="${param.popup}"> 
				<display:table name="notess" class="table" requestURI="" id="notesList" export="true" defaultsort="2" pagesize="10" style="width:700px">   
				<%  System.out.println( "===========================  display table================================");%>
				<% if (pageContext.findAttribute("accountProfile.id") != null){
					System.out.println( "notesId column ----111111111"); %>
					<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForPartner.html?from=list&id1=${accId}&imageId=${imageId}&pId=${pId}&fieldId=${fieldId}&accountNotesFor=AP&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
				<% }else if(pageContext.findAttribute("accountContact.id") != null){
					System.out.println( "notesId column ----22222222");%>
					<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForPartner.html?from=list&id1=${accCont}&imageId=${imageId}&pId=${pId}&fieldId=${fieldId}&accountNotesFor=AC&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
				<% }else{
					System.out.println( "notesId column ----otherwise");%>
					<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForPartner.html?from=list&id1=${id}&imageId=${imageId}&pId=${pId}&fieldId=${fieldId}&accountNotesFor=AC&decorator=popup&popup=true" paramId="id" paramProperty="id"/>
				<%} %>
				<% System.out.println( "noteType column");%>
					<display:column property="noteType" sortable="true" titleKey="notes.noteType" style="width:100px"/>
				  
				 <% System.out.println( "noteSubType column");%>  
				    <c:if test="${noteFor=='AccountContact'}">
				    <display:column property="noteSubType" sortable="true" title="Type&nbsp;Of&nbsp;Contact" style="width:80px"/>
				    </c:if>
				    <c:if test="${noteFor!='AccountContact'}">
				    <display:column property="noteSubType" sortable="true" titleKey="notes.noteSubType" style="width:80px"/>
				    </c:if>
				     <% System.out.println( "subject column");%> 
				    <display:column property="subject" sortable="true" titleKey="notes.subject" maxLength="10" style="width:80px"/>
				     <% System.out.println( "note column");%> 
				    <display:column property="note" sortable="true" title="Note" maxLength="17"/>
					 <% System.out.println( "createdOn column");%> 
					<display:column property="createdOn" sortable="true" titleKey="notes.createdOn" style="width:65px" format="{0,date,dd-MMM-yyyy}" />
				     <% System.out.println( "updatedBy column");%>
				    <display:column property="updatedBy" sortable="true" title="Modified&nbsp;By"  style="width:45px"/>
				    
				    <display:setProperty name="paging.banner.item_name" value="customerfilenotes"/>   
				    <display:setProperty name="paging.banner.items_name" value="notes"/>   
				    <display:setProperty name="export.excel.filename" value="CustomerFileNotes List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="CustomerFileNotes List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="CustomerFileNotes List.pdf"/>   
				</display:table>  
			</c:if>
</div>   
<c:if test="${param.popup}"> 
	<input type="button" class="cssbutton" style="margin-right: 5px;height: 25px;width:60px; font-size: 15" value="Close" onclick="window.close();"/>
</c:if>
<c:set var="noteFrom" value="${noteFor}" scope="session"/>
</s:form>

<script language="javascript" type="text/javascript">
	function clear_fields(){
		document.forms['notesListForm'].elements['notes.noteSubType'].value = "";
		document.forms['notesListForm'].elements['notes.noteStatus'].value = "";
	}
	
	function openNotesPopup(targetElement){
		var id = document.forms['notesListForm'].elements['accountContact.id'].value;
		var notesId = document.forms['notesListForm'].elements['accountContact.partnerCode'].value;
		var noteSubType = document.forms['notesListForm'].elements['accountContact.jobType'].value;
		var pId = document.forms['notesListForm'].elements['pId'].value;
		var imgID = targetElement.id;
		openWindow('accountContactNotes.html?id='+id+'&notesId='+notesId+'&accountNotesFor=AC&noteFor=AccountContact&subType='+noteSubType+'&imageId='+imgID+'&pId='+pId+'&for=List&fromTab=yes&fieldId=countAccountContactNotes&decorator=popup&popup=true',740,400);
	}

	function openNotesPopup1(targetElement){
		var id = document.forms['notesListForm'].elements['accountProfile.id'].value;
		var notesId = document.forms['notesListForm'].elements['accountProfile.partnerCode'].value;
		var noteSubType = document.forms['notesListForm'].elements['accountProfile.type'].value;
		var imgID = targetElement.id;
		var pId = document.forms['notesListForm'].elements['pId'].value;
		openWindow('accountNotes.html?id='+id+'&notesId='+notesId+'&accountNotesFor=AP&noteFor=AccountProfile&subType='+noteSubType+'&imageId='+imgID+'&pId='+pId+'&for=List&fromTab=yes&fieldId=countAccountProfileNotes&decorator=popup&popup=true',740,400);
	}
</script> 
 
<script type="text/javascript">   
    highlightTableRows("notesList");   
    Form.focusFirstElement($("notesListForm")); 
</script> 