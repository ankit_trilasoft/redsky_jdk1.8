<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="fn1" uri="http://java.sun.com/jsp/jstl/functions" %>
<head>
<title>Tasks</title>
<style type="text/css">
span.pagelinks {
display:block; font-size:0.95em; margin-bottom:5px; margin-top:-18px; padding:2px 5px; text-align:right;
float:right; width:47%;
}
.handcursor{ cursor:hand; cursor:pointer; font-color:black;
}
</style>
</head>
<div id="newmnav" style="float:left;">
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<ul><c:if test="${not empty soObj.id && soObj.controlFlag=='C' && (empty soObj.moveType || soObj.moveType=='BookedMove')}">
		<sec-auth:authComponent componentId="module.tab.serviceorder.serviceorderTab">
		<li ><a href="editServiceOrderUpdate.html?id=${soObj.id}"><span>S/O Details</span></a>
		</li>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.serviceorder.billingTab">
		<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			<li><a href="editBilling.html?id=${soObj.id}" ><span>Billing</span></a></li>
		</sec-auth:authComponent>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		<c:choose>
			<%-- <c:when test='${soObj.status == "CNCL" || soObj.status == "DWND" || soObj.status == "DWNLD"}'>
			   <li><a href="accountLineList.html?sid=${soObj.id}"><span>Accounting</span></a></li>	
			</c:when> --%>
			<c:when test='${soObj.job == "" || soObj.job == null }'>
		 		<li><a href="accountLineList.html?sid=${soObj.id}"><span>Accounting</span></a></li>	
			</c:when>
			<c:otherwise> 
		       <li><a href="accountLineList.html?sid=${soObj.id}"><span>Accounting</span></a>
		       </li>
		    </c:otherwise>
	  </c:choose>
	  </sec-auth:authComponent>
	  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
	  <c:choose>
			
			<c:when test='${soObj.job == "" || soObj.job == null }'>
		 		<li><a href="pricingList.html?sid=${soObj.id}"><span>Accounting</span></a></li>	
			</c:when>
			<c:otherwise> 
		       <li><a href="pricingList.html?sid=${soObj.id}"><span>Accounting</span></a>
		       </li>
		    </c:otherwise>
	  </c:choose>
	  </sec-auth:authComponent>
	  <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 <li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 </configByCorp:fieldVisibility>
	    <c:if test="${fn1:indexOf(oiJobList,soObj.job)>=0}"> 
	         <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	         <li><a href="operationResource.html?id=${soObj.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	   </c:if>
	  	<sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
         <li><a href="costingDetail.html?sid=${soObj.id}"><span>Costing</span></a></li>
  	 </sec-auth:authComponent>
	  <sec-auth:authComponent componentId="module.tab.serviceorder.forwardingTab">
	  <c:if test="${soObj.job !='RLO'}">		
		 <c:if test="${forwardingTabVal!='Y'}">
				<li><a href="containers.html?id=${soObj.id}" ><span>Forwarding</span></a></li>
		</c:if>
		<c:if test="${forwardingTabVal=='Y'}">
			<li><a href="containersAjaxList.html?id=${soObj.id}" ><span>Forwarding</span></a></li>
		</c:if>
	</c:if>
	</sec-auth:authComponent>
	 <sec-auth:authComponent componentId="module.tab.serviceorder.domesticTab">
		<c:if test="${soObj.job !='INT' && soObj.job !='JVS'}">
		<c:if test="${soObj.job !='RLO'}"> 
			<li><a href="editMiscellaneous.html?id=${soObj.id}"><span>Domestic</span></a></li>
		</c:if>
		</c:if>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
	  <c:if test="${serviceOrder.job =='INT'}">
	    <li><a href="editMiscellaneous.html?id=${soObj.id}"><span>Domestic</span></a></li>
	  </c:if>
	  </sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.notes.statusTab">
			  		<c:if test="${soObj.job =='RLO'}"> 
	 				<li><a href="editDspDetails.html?id=${soObj.id}"><span>Status</span></a></li>
					</c:if>
					<c:if test="${soObj.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${soObj.id}"><span>Status</span></a></li>
					</c:if>				  		
</sec-auth:authComponent>	
<sec-auth:authComponent componentId="module.tab.notes.ticketTab">
			  	<c:if test="${soObj.job !='RLO'}"> 
			  		<li><a href="customerWorkTickets.html?id=${soObj.id}" ><span>Ticket</span></a></li>
			  		</c:if>
			  	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  	<c:if test="${soObj.job !='RLO'}"> 
			  		<li><a href="claims.html?id=${soObj.id}"><span>Claims</span></a></li>
			  	</c:if>
			  	</sec-auth:authComponent>
			  	<!--<sec-auth:authComponent componentId="module.tab.notes.customerFileTab">
			  		<li><a href="editCustomerFile.html?id=${soObj.customerFileId}" ><span>Customer File</span></a></li>
			  	</sec-auth:authComponent>
	-->
	<sec-auth:authComponent componentId="module.tab.serviceorder.reportTab">	
			<li><a onmouseover="return chkSelect();" onclick="window.open('subModuleReports.html?id=${soObj.id}&jobNumber=${soObj.shipNumber}&regNumber=${soObj.registrationNumber}&companyDivision=${soObj.companyDivision}&jobType=${soObj.job}&modes=${soObj.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
	</sec-auth:authComponent>
	 <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<!--<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}&id=${soObj.id}"><span>Survey Details</span></a></li>
		    	</c:if>
		    	--><c:if test="${not empty customerFile.id && mmValidation =='Yes'}"> 
		    		<li><a href="inventoryDetailsForm.html?cid=${customerFile.id}&id=${soObj.id}"><span>Inventory Details</span></a></li>
		    	</c:if>
		    </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.customerFile.serviceOrderTab">
		    <c:if test="${ usertype=='AGENT' && surveyTab}">
		  		 <li><a href="surveyDetails.html?cid=${customerFile.id}&id=${soObj.id}"><span>Survey Details</span></a></li>
		    </c:if>
		     </sec-auth:authComponent>
  </c:if>
  <c:if test="${not empty soObj.id && (soObj.controlFlag=='Q' || soObj.moveType=='Quote')}">  
	<sec-auth:authComponent componentId="module.tab.notes.serviceOrdersTab">
	  <li ><a href="editQuotationServiceOrderUpdate.html?id=${soObj.id}" class=""><span>Quotes</span></a></li>
	   <c:if test="${voxmeIntergartionFlag=='true'}">             
		  <li><a href='surveyDetails.html?cid=${customerFile.id}&Quote=y'><span>Survey Details</span></a></li>              
             </c:if>
	</sec-auth:authComponent> 
  </c:if>
  <c:if test="${not empty customerFile.id && customerFile.controlFlag=='C' && (empty customerFile.moveType || customerFile.moveType=='BookedMove')}">
  <sec-auth:authComponent componentId="module.tab.notes.customerFileTab">
<li ><a href="editCustomerFile.html?id=${customerFile.id}"  ><span>Customer File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
 <c:if test="${voxmeIntergartionFlag=='true'}">             
		  <li><a href='surveyDetails.html?cid=${customerFile.id}'><span>Survey Details</span></a></li>              
             </c:if>
</sec-auth:authComponent>
  </c:if>
  <c:if test="${not empty customerFile.id && (customerFile.controlFlag=='Q' || customerFile.moveType=='Quote')}">
  
			  	<sec-auth:authComponent componentId="module.tab.notes.quotationFileTab">
			  		    <li  ><a href="QuotationFileForm.html?id=${customerFile.id}"><span>Quotation File</span></a></li>
			  		     <c:if test="${voxmeIntergartionFlag=='true'}">             
		  <li><a href='surveyDetails.html?cid=${customerFile.id}&Quote=y'><span>Survey Details</span></a></li>              
             </c:if>
			   	</sec-auth:authComponent>
			   	 
	
  </c:if>
   <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
  </ul>
  </div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float:left;">
	<tr>
		<c:if test="${not empty soObj.id}">
			<c:if test="${soQuickView=='Y'}">
				<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
					<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${soObj.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
				</td>
			</c:if>
		</c:if>
	</tr>
</table>
<s:form id="tasksForm" name= "tasksForm" action="" method="post" validate="true">
<s:hidden name="newIdList" value=""/>
<c:set var="tableName" value="<%=request.getParameter("tableName") %>"/>
<s:hidden name="tableName"  id="from" value="<%=request.getParameter("tableName") %>" />
<c:set var="id" value="<%=request.getParameter("id") %>"/>
<s:hidden name="id"  id="from" value="<%=request.getParameter("id") %>" />
<c:set var="soId" value="<%=request.getParameter("soId") %>"/>
<s:hidden name="soId"  id="from" value="<%=request.getParameter("soId") %>" />

<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="!margin-top: -14px;margin-top:25px;"><span></span></div>
   <div class="center-content">
   <c:if test="${not empty soObj.id}">
<table  cellspacing="1" cellpadding="0" border="0" width="100%">
   <tbody>
    <tr><td align="left" class="listwhitebox">
       <table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" width="100%">
         <tbody>                  
             <tr>
               <td align="left" colspan="16">        
			<table style="margin:0px;padding:0px;">
             <tr>
             <td align="right" class="listwhitebox">Shipper</td>
             <td align="left" colspan="2"><s:textfield name="soObj.firstName" size="25"  cssClass="input-textUpper" readonly="true" tabindex="1"/>
             
             <td align="left" ><s:textfield name="soObj.lastName" cssClass="input-textUpper" size="25" readonly="true" tabindex="1"/></td>            
             <td align="right" class="listwhitebox" width="30">Origin</td>
             
             <td align="left"><s:textfield name="soObj.originCityCode"  cssClass="input-textUpper" size="15" readonly="true" tabindex="1" /></td>
             <td align="left"><s:textfield name="soObj.originCountryCode" cssClass="input-textUpper"  size="5" readonly="true" tabindex="1" /></td>
             
             <td align="right" class="listwhitebox" width="27">Type</td>
             <td align="left"><s:textfield name="soObj.job" cssClass="input-textUpper" size="7" readonly="true" tabindex="1" /></td>
             
             <td align="right" class="listwhitebox" width="55">Commodity</td>
             <td align="left"><s:textfield name="soObj.commodity" cssClass="input-textUpper"  size="6" readonly="true" tabindex="1" /></td>
             
             <td align="right" class="listwhitebox" width="39">Routing</td>
             <td align="left"><s:textfield name="soObj.routing" cssClass="input-textUpper" size="6" readonly="true" tabindex="1" /></td>
           
             </tr>             
             </table>
			
             </td>
             </tr>
			<tr>
             <td align="left" colspan="16">        
     		<table style="margin:0px;padding:0px;">
             <tr> 
             <td align="right" width="37">S/O#</td>
             <td align="left"><s:textfield name="soObj.shipNumber" cssClass="input-textUpper"  size="19" readonly="true" tabindex="1"/></td>
             <td align="right" width="26">Reg#</td>
             <td align="left"><s:textfield name="soObj.registrationNumber" cssClass="input-textUpper"  size="25" readonly="true" tabindex="1"/></td>
                     
             <td align="right" width="31">Destin</td>
             <td align="left"><s:textfield name="soObj.destinationCityCode" cssClass="input-textUpper"  size="15" readonly="true" tabindex="1"/></td>
             <td align="left"><s:textfield name="soObj.destinationCountryCode" cssClass="input-textUpper" size="5" readonly="true" tabindex="1"/></td>
            
             <td align="right" width="27">Mode</td>
             <td align="left"><s:textfield name="soObj.mode" cssClass="input-textUpper" size="7" readonly="true" tabindex="1"/></td>
            
             <td align="right">Billing Party</td>
             <td align="left" colspan="5"><s:textfield name="soObj.billToName" cssClass="input-textUpper" size="26" readonly="true" tabindex="1"/></td>
             </tr>
             </table>     		
			</td>
			</tr>
			</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</c:if>
<c:if test="${not empty customerFile.id}">
<c:if test="${empty soObj.id}">
	<table class="" cellspacing="1" cellpadding="0" border="0">
			<tbody>
			<tr><td align="left" class="listwhitebox" style="width:100%">
				<table class="detailTabLabel" border="0" cellspacing="2" style="width:100%">
				  <tbody>  	
				  	<tr>
						<td align="right">Shipper</td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.firstName" required="true" size="25" readonly="true"/></td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.lastName" required="true" size="25" readonly="true"/></td>
						<td align="right">&nbsp;</td>
						<td align="right">Origin</td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.originCityCode" required="true" size="25" readonly="true"/></td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.originCountryCode" required="true" size="5" readonly="true"/></td>
						<td align="right">Type</td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.job" required="true" size="5" readonly="true"/></td>
						<td align="right">&nbsp;</td>
						<td align="right">Destination</td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.destinationCityCode" required="true" size="25" readonly="true"/></td>
						<td><s:textfield cssClass="input-textUpper"name="customerFile.destinationCountryCode" required="true" size="5" readonly="true"/></td>
					</tr>
				  </tbody>
				 </table>
				</td>
			</tr>
			</tbody>
		</table>
</c:if>
</c:if>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

 <div id="newmnav">
	<ul>
	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Tasks List</span></a></li>
	<c:if test="${tableName=='serviceorder'|| tableName=='accountline' || tableName=='trackingstatus'|| tableName=='miscellaneous'|| tableName=='customerfile' || tableName=='billing'}">
	<li><a href="relatedTasks.html?id=${id}&tableName=${tableName}&relatedTask=relatedTask"  ><span>Related Tasks</span></a></li>	
	</c:if>
	<c:if test="${tableName=='workticket' || tableName=='claim'}">
	<li><a href="relatedTasks.html?id=${id}&tableName=${tableName}&soId=${soId}&relatedTask=relatedTask"  ><span>Related Tasks</span></a></li>	
	</c:if>	
	<%-- <li ><a href="tasksCheckList.html?id=${id}&shipnumber=${soObj.shipNumber}" ><span>Tasks Check List</span></a></li> --%>
			  	
	</ul>
</div>
<div class="spn">&nbsp;</div>
	
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="!margin-top: -14px;"><span></span></div>
   <div class="center-content">
   <table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%">
     <tbody>
     </tbody>
     <tr><td ><img src="${pageContext.request.contextPath}/images/follow-up.jpg" border="0" style="cursor:auto;"/></td></tr>
   <tr>
   <td>
    <tr>
	<td height="10" width="100%" align="left" >
	<div onClick="javascript:animatedcollapse.toggle('comingUp')" style="margin: 0px">	
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Coming Up</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countDueWeek}&nbsp;</font>)
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
	</div> 
	<div id="comingUp">
	<s:set name="notes2" value="notes2" scope="request"/> 
	<display:table name="notes2" class="table" requestURI="" id="notes2List"  defaultsort="6" defaultorder="ascending" export="" pagesize="10"> 
	<display:column sortable="true" maxLength="50" titleKey="toDoReminder.subject" sortProperty="subject">
	<c:if test="${ notes2List.subject != ''}">
		<div onclick="javascript:openWindow('notesControl.html?id=${notes2List.id}&activity=Yes&decorator=popup&popup=true',900,400);" class="red">
		<font color ="#003366" ><u>	<c:out value="${notes2List.subject}"></c:out>
		</u></font></div>
	</c:if>
	<c:if test="${notes2List.subject == ''}">
		<div onclick="javascript:openWindow('notesControl.html?id=${notes2List.id}&activity=Yes&decorator=popup&popup=true',900,400);" class="red">
		<font color ="#003366" > - </font></div>
	</c:if>
	</display:column>   
	<display:column property="note" maxLength="50" style="text-align:justify" sortable="true"  paramId="id" paramProperty="id" titleKey="toDoReminder.notes"/> 
	<display:column property="noteType" sortable="true"  paramId="id" paramProperty="id" titleKey="notes.noteType" style="width:100px"/>
	<display:column property="name" sortable="true"  paramId="id" paramProperty="id" title="Name" style="width:100px"/> 
	<display:column property="followUpFor" sortable="true"  paramId="id" paramProperty="id" title="Follow Up For" style="width:100px"/> 
	<display:column property="forwardDate" sortable="true"  paramId="id" paramProperty="id" titleKey="toDoReminder.date" style="width:80px" format="{0,date,dd-MMM-yyyy}"/>
	<display:column headerClass="containeralign" style="width:30px; text-align: right" property="remindTime" sortable="true"  paramId="id" paramProperty="id" title="Time" />
	</display:table>
	
</div>
</td>
</tr>
<tr>
	<td height="10" width="100%" align="left" >
	<div onClick="javascript:animatedcollapse.toggle('dueToday')" style="margin: 0px">	
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
		&nbsp;&nbsp;<font color="black">Due Today</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countToday}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>	
	</div>
	<div id="dueToday">
	<s:set name="notes" value="notes" scope="request"/> 
	<display:table name="notes" class="table" requestURI="" id="notesList"  defaultsort="4" export="" pagesize="10"> 
	<display:column sortable="true" maxLength="50" titleKey="toDoReminder.subject" sortProperty="subject">
			<c:if test="${ notesList.subject != ''}">
		<div onclick="javascript:openWindow('notesControl.html?id=${notesList.id}&activity=Yes&decorator=popup&popup=true',900,400);" class="red">
			<font color ="#003366" ><u><c:out value="${notesList.subject}"></c:out></u></font>
		</a></div></c:if>
		<c:if test="${notesList.subject == ''}">
		<div onclick="javascript:openWindow('notesControl.html?id=${notesList.id}&activity=Yes&decorator=popup&popup=true',900,400);" class="red">
		<font color ="#003366" > - </font></div>
	</c:if>
	</display:column>   
	<display:column property="note" maxLength="50" sortable="true" paramId="id" paramProperty="id" titleKey="toDoReminder.notes"/> 
	<display:column property="noteType" sortable="true"  paramId="id" paramProperty="id" titleKey="notes.noteType" style="width:100px"/> 
	<display:column property="name" sortable="true"  paramId="id" paramProperty="id" title="Name" style="width:100px"/> 
	<display:column property="followUpFor" sortable="true"  paramId="id" paramProperty="id" title="Follow Up For" style="width:100px"/> 
	<display:column property="forwardDate" sortable="true"  paramId="id" paramProperty="id" titleKey="toDoReminder.date" style="width:80px" format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="remindTime" sortable="true"  paramId="id" paramProperty="id" title="Time" headerClass="containeralign" style="width:30px; text-align: right"/>
	</display:table>
	
</div>
</td>
</tr> 
<tr>
	<td height="10" width="100%" align="left">	
	<div onClick="javascript:animatedcollapse.toggle('overdue')" style="margin: 0px">	
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Over Due</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countOverDue}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	<!--<font id="bobcontent4-title" class="handcursor">
	&nbsp;&nbsp;<font color="black">Over Due</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countOverDue}&nbsp;</font>)</font>
	--></div>
	<div id="overdue">
	<s:set name="notes1" value="notes1" scope="request"/> 
	<display:table name="notes1" class="table" requestURI="" id="notes1List"  defaultsort="6" defaultorder="ascending" export="false" pagesize="10"> 
	<display:column sortable="true" maxLength="50" titleKey="toDoReminder.subject" sortProperty="subject">
		<c:if test="${ notes1List.subject != ''}">
		<div onclick="javascript:openWindow('notesControl.html?id=${notes1List.id}&activity=Yes&decorator=popup&popup=true',900,400);" class="red">
		<font color ="#003366" ><u>	<c:out value="${notes1List.subject}"></c:out></u></font>
		</a></div></c:if>
		<c:if test="${notes1List.subject == ''}">
		<div onclick="javascript:openWindow('notesControl.html?id=${notes1List.id}&activity=Yes&decorator=popup&popup=true',900,400);" class="red">
		<font color ="#003366" > - </font></div>
	</c:if>
	</display:column>   
	<display:column property="note" maxLength="50" style="text-align:justify" sortable="true"  paramId="id" paramProperty="id" titleKey="toDoReminder.notes"/> 
	<display:column property="noteType" sortable="true"  paramId="id" paramProperty="id" titleKey="notes.noteType" style="width:100px"/> 	
	<display:column property="name" sortable="true"  paramId="id" paramProperty="id" title="Name" style="width:100px"/> 
	<display:column property="followUpFor" sortable="true"  paramId="id" paramProperty="id" title="Follow Up For" style="width:100px"/> 
	<display:column property="forwardDate" sortable="true"  paramId="id" paramProperty="id" titleKey="toDoReminder.date" style="width:80px" format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="remindTime" sortable="true"  paramId="id" paramProperty="id" title="Time" headerClass="containeralign" style="width:30px; text-align: right"/>
	</display:table>
	
</div>
</td>
</tr>
<tr>
<td colspan="3" align="left">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;margin:0px;padding:0px;">
<tr>
<td width="200px"><img src="${pageContext.request.contextPath}/images/alert-List.jpg" border="0" style="cursor:auto;"/></td>
</tr>
</table>
</td>
</tr>
<tr>
	<td height="10" width="100%" align="left" >
	<div onClick="javascript:animatedcollapse.toggle('alert')" style="margin: 0px">	
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Alert</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countAlertToday}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	<!--<font id="bobcontent2-title" class="handcursor">
	&nbsp;&nbsp;<font color="black">Due Today</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countResultToday}&nbsp;</font>)</font>
	--></div>
	<div id="alert">
	<s:set name="toDayAlertList" value="toDayAlertList" scope="request"/>
	<display:table name="toDayAlertList" class="table" requestURI="" id="toDayAlertList"  defaultsort="10" export="" pagesize="10" style="margin-bottom:2px;">

<display:column title="Remove">
<input type="checkbox" name="check_box" id="${toDayAlertList.id}" value="${toDayAlertList.id}" onclick="makeAsMarked(this);"/>
</display:column>
<display:column sortable="true" title="File Number" sortProperty="alertFileNumber">
	<a href="javascript:showFilePage('${toDayAlertList.id}','${toDayAlertList.xtranId}','${toDayAlertList.tableName}');" ><c:out value="${toDayAlertList.alertFileNumber}" /></a>
</display:column>
<display:column property="alertShipperName" sortable="true" title="Shipper Name"/>
<display:column property="description" sortable="true" title="Field Updated"/>
<display:column  sortable="true" title="Old Value">

    <c:if test="${fn1:contains(toDayAlertList.oldValue,'00:00')}">
       		<c:out value= "${fn1:substringBefore(toDayAlertList.oldValue,'00:00')}"></c:out>   
       </c:if>
       <c:if test="${!fn1:contains(toDayAlertList.oldValue,'00:00')}">
       		<c:out value="${toDayAlertList.oldValue }"></c:out>
       </c:if>
       </display:column>
    <display:column  sortable="true" title="New Value">
    
       <c:if test="${fn1:contains(toDayAlertList.newValue,'00:00')}">
       		<c:out value= "${fn1:substringBefore(toDayAlertList.newValue,'00:00')}"></c:out>   
       </c:if>
       <c:if test="${!fn1:contains(toDayAlertList.newValue,'00:00')}">
       		<c:out value="${toDayAlertList.newValue }"></c:out>
       </c:if>
    </display:column>
    <display:column property="user" sortable="true" title="Modify By"/>
    <display:column title="Modify Date" sortable="true" style="width:125px"><fmt:formatDate value="${toDayAlertList.dated}" pattern="${displayDateTimeFormat}"/></display:column>
    <display:column property="alertRole" sortable="true" title="Role"/>
    <display:column property="alertUser" sortable="true" title="Name"/>
    <display:setProperty name="export.excel.filename" value="Reports List.xls"/> 
    <display:setProperty name="export.csv.filename" value="Reports List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="Reports List.pdf"/> 
	 </display:table>
<input type="button" class="cssbutton1" style="width:120px; height:23px;margin-bottom:5px;" align="top"  name="showLine" id="showLine" value="Dismiss Alerts" onclick="makeAsViewed();"/>	
</div>
</td>
</tr>
<tr>
<td colspan="3" align="left">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;margin:0px;padding:0px;">
<tr>
<td width="200px"><img src="${pageContext.request.contextPath}/images/todo-List.jpg" border="1" style="cursor:auto;"/></td>
<!--<td align="left" width="250px" ><input type="button"  class="groupButton" align="bottom" value="Group by Rules"  onclick="openGroupByRules();"/></td>
<td align="left" ><input type="button" class="groupButton"  align="bottom" value="Group by Person"  onclick="openGroupByPerson();"/></td>
<td align="left" ><input type="button" class="groupButton"  align="bottom" value="Group by Shipper"  onclick="openGroupByShipper();"/></td>
--></tr>
</table>
</td>
</tr>
 <tr height="1px"></tr>	
  
  <tr><td></td></tr> 
    <tr>
	<td height="20" width="100%" align="left" >
	<div onClick="javascript:animatedcollapse.toggle('dueTodaychild1')" style="margin: 0px">	
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Due Today</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countResultToday}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	<!--<font id="bobcontent2-title" class="handcursor">
	&nbsp;&nbsp;<font color="black">Due Today</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countResultToday}&nbsp;</font>)</font>
	--></div>
	<div id="dueTodaychild1">
	<s:set name="toDoResultsToday" value="toDoResultsToday" scope="request"/>
	<display:table name="toDoResultsToday" class="table" requestURI="" id="toDoResultRow2"  defaultsort="9" export="" pagesize="10"> 
	<c:if test="${toDoResultRow2.resultRecordType!='AccountLineList' }">
	<display:column sortable="true" titleKey="toDoList.file" sortProperty="fileNumber">
	<a style="width:100px" HREF="javascript:updateCheckUserName('${toDoResultRow2.resultRecordId}','${toDoResultRow2.fieldToValidate1}','${toDoResultRow2.url}','${toDoResultRow2.fieldToValidate1}','${toDoResultRow2.fieldToValidate2}','${toDoResultRow2.resultRecordId}','${toDoResultRow2.supportingId }','N');"><c:out value="${toDoResultRow2.fileNumber}" /></a>
	</display:column>
	</c:if>
	<c:if test="${toDoResultRow2.resultRecordType=='AccountLineList' }">
	<display:column sortable="true" titleKey="toDoList.file" sortProperty="fileNumber" >
	<a HREF="javascript:updateCheckUserName('${toDoResultRow2.resultRecordId}','${toDoResultRow2.fieldToValidate1}','${toDoResultRow2.url}','${toDoResultRow2.fieldToValidate1}','${toDoResultRow2.fieldToValidate2}','${toDoResultRow2.resultRecordId}','${toDoResultRow2.supportingId }','Y');"><c:out value="${toDoResultRow2.fileNumber}" /></a>
	</display:column>
	</c:if>
	<display:column property="messagedisplayed" style="width:20%" sortable="true"   titleKey="toDoRulemessage.message"/>
	<display:column property="fieldDisplay" sortable="true"   titleKey="toDoRule.fieldtovalidate"/>
	<display:column headerClass="containeralign" style="text-align: right" property="durationAddSub" sortable="true"  title="OD"/>
	<display:column property="shipper" sortable="true"   titleKey="toDoList.shipper"/>
	<display:column property="rolelist" sortable="true"   title="Role"/>
	<display:column property="owner" sortable="true"   title="Name"/>
	<display:column property="checkedUserName" sortable="true"   title="viewedBy"/>
	<display:column property="billToName" sortable="true"   title="Bill To Name"/>
	<display:column title="Note" sortable="true" style="text-align: center; width:10%;" maxLength="40">
	
	<c:if test="${toDoResultRow2.isNotesAdded == false}">
		<c:if test="${toDoResultRow2.note!=''}">
		<a HREF="javascript:openNote('${toDoResultRow2.noteId}','${toDoResultRow2.noteType}','${toDoResultRow2.noteSubType}');"><c:out value="${toDoResultRow2.note}" /></a>
	    </c:if>
	    <c:if test="${toDoResultRow2.note==''}">
		<img src="${pageContext.request.contextPath}/images/open.png" onclick="openRulesNotes('${toDoResultRow2.fileNumber}','${toDoResultRow2.todoRuleId}','${toDoResultRow2.id}','${toDoResultRow2.noteType}','${toDoResultRow2.noteSubType}');"/>
	   </c:if>
	</c:if>
	<c:if test="${toDoResultRow2.isNotesAdded== true}">
	<a HREF="javascript:openNote('${toDoResultRow2.noteId}','${toDoResultRow2.noteType}','${toDoResultRow2.noteSubType}');"><c:out value="${toDoResultRow2.note}" /></a>
	</c:if>
	</display:column>
	</display:table>
	
</div>
</td>
</tr>
<tr>
	<td height="10" width="100%" align="left" >
	<div onClick="javascript:animatedcollapse.toggle('overduechild1')" style="margin: 0px">
	 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Over Due</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countResultOverDue}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	</div>
	<div id="overduechild1">
	<s:set name="toDoResults" value="toDoResults" scope="request"/> 
	<display:table name="toDoResults" class="table" requestURI="" id="toDoResultRow3"  defaultsort="4" export="" pagesize="10">
	<c:if test="${toDoResultRow3.resultRecordType!='AccountLineList' }">
	<display:column sortable="true" titleKey="toDoList.file" sortProperty="fileNumber"> 
	<a HREF="javascript:updateCheckUserName('${toDoResultRow3.resultRecordId}','${toDoResultRow3.fieldToValidate1}','${toDoResultRow3.url}','${toDoResultRow3.fieldToValidate1}','${toDoResultRow3.fieldToValidate2}','${toDoResultRow3.resultRecordId}','${toDoResultRow3.supportingId }','N');"><c:out value="${toDoResultRow3.fileNumber}" /></a>
	</display:column>
	</c:if>
	<c:if test="${toDoResultRow3.resultRecordType=='AccountLineList' }">
	<display:column sortable="true" titleKey="toDoList.file" >
	<a HREF="javascript:updateCheckUserName('${toDoResultRow3.resultRecordId}','${toDoResultRow3.fieldToValidate1}','${toDoResultRow3.url}','${toDoResultRow3.fieldToValidate1}','${toDoResultRow3.fieldToValidate2}','','${toDoResultRow3.resultRecordId}','Y');"><c:out value="${toDoResultRow3.fileNumber}" /></a>
	</display:column>
	</c:if>
	<display:column property="messagedisplayed"  style="width:20%" sortable="true"     titleKey="toDoRulemessage.message"/>
    <display:column property="fieldDisplay" sortable="true"  titleKey="toDoRule.fieldtovalidate"/>
  	<display:column headerClass="containeralign" style="text-align: right" property="durationAddSub" sortable="true"   title="OD"/>
	<display:column property="shipper" sortable="true"   titleKey="toDoList.shipper"/>
	<display:column property="rolelist" sortable="true"   title="Role"/>
	<display:column property="owner" sortable="true"   title="Name"/>
	<display:column property="checkedUserName" sortable="true" title="viewedBy"/>
	<display:column property="billToName" sortable="true"   title="Bill To Name"/>
	<display:column title="Note" sortable="true" style="text-align: center;width:10%;" maxLength="40">
	<c:if test="${toDoResultRow3.isNotesAdded == false}">
		<c:if test="${toDoResultRow3.note!=''}">
		<a HREF="javascript:openNote('${toDoResultRow3.noteId}','${toDoResultRow3.noteType}','${toDoResultRow3.noteSubType}');"><c:out value="${toDoResultRow3.note}" /></a>
	    </c:if>
	    <c:if test="${toDoResultRow3.note==''}">
		<img src="${pageContext.request.contextPath}/images/open.png" onclick="openRulesNotes('${toDoResultRow3.fileNumber}','${toDoResultRow3.todoRuleId}','${toDoResultRow3.id}','${toDoResultRow3.noteType}','${toDoResultRow3.noteSubType}');"/>
		</c:if>
	</c:if>
	<c:if test="${toDoResultRow3.isNotesAdded== true}">
		<a HREF="javascript:openNote('${toDoResultRow3.noteId}','${toDoResultRow3.noteType}','${toDoResultRow3.noteSubType}');"><c:out value="${toDoResultRow3.note}" /></a>
	</c:if>
	   	  
	</display:column>
	</display:table>
	
</div>
</td>
</tr> 
<tr>
<td colspan="3" align="left">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;margin:0px;padding:0px;">
<tr>
<td width="200px"><img src="${pageContext.request.contextPath}/images/check-List.jpg" border="1" style="cursor:auto;"/></td>
</tr>
</table>
</td>
</tr>
  <tr>
	<td height="10" width="100%" align="left" >
	 <div onClick="javascript:animatedcollapse.toggle('dueTodaychild2')" style="margin: 0px">
	 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Due Today</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countCheckListTodayResult}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	</div>
	<div id="dueTodaychild2">
	<display:table name="checkListTodayList" class="table" requestURI="" id="checkListTodayListId"  defaultsort="3" export="" pagesize="10">
	<display:column sortable="true" title="File ID" sortProperty="resultNumber">
	<a HREF="javascript:gotoRespectiveFile('${checkListTodayListId.resultId}','${checkListTodayListId.url}');"><c:out value="${checkListTodayListId.resultNumber}" /></a>
	</display:column>
	<display:column property="messageDisplayed"  style="width:20%" sortable="true"     titleKey="toDoRulemessage.message"/>
   	<display:column headerClass="containeralign" style="text-align: right" property="duration" sortable="true"   title="OD"/>
	<display:column property="shipper" sortable="true"   title="Shipper's Name"/>
	<display:column property="owner" sortable="true"   title="Name"/>
	</display:table>
	
</div>
</td>
</tr>
<tr>
	<td height="10" width="100%" align="left" >
	 <div onClick="javascript:animatedcollapse.toggle('overduechild2')" style="margin: 0px">
	 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Over Due </font> &nbsp;&nbsp;(<font color="red">&nbsp;${countCheckListOverDueResult}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	</div>
	<div id="overduechild2">
	<display:table name="checkListOverDueList" class="table" requestURI="" id="checkListOverDueListId"  defaultsort="3" export="" pagesize="10">
	<display:column sortable="true" title="File ID" sortProperty="resultNumber">
	<a HREF="javascript:gotoRespectiveFile('${checkListOverDueListId.resultId}','${checkListOverDueListId.url}');"><c:out value="${checkListOverDueListId.resultNumber}" /></a>
	</display:column>
	<display:column property="messageDisplayed"  style="width:20%" sortable="true"     titleKey="toDoRulemessage.message"/>
   	<display:column headerClass="containeralign" style="text-align: right" property="duration" sortable="true"   title="OD"/>
	<display:column property="shipper" sortable="true"   title="Shipper's Name"/>
	<display:column property="owner" sortable="true"   title="Name"/>
	</display:table>
	
</div>
</td>
</tr>

		<c:if test="${tableName=='serviceorder'|| tableName=='accountline' || tableName=='trackingstatus'|| tableName=='miscellaneous'|| tableName=='billing'}">
<tr>
	<td height="10" width="100%" align="left" >
	 <div id="bobcontent17-title" class="handcursor">
	 
	 <table width="100%" cellspacing="0" cellpadding="0" style="margin: 0px">
	<tbody><tr>
	<td class="headtab_left">
	</td>
	
	<td nowrap="" class="headtab_center">
	&nbsp;&nbsp;<font color="black">Agent TDR Email Verification</font>&nbsp;&nbsp;(<font color="red">&nbsp;${countAgentTdrEmail}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_special">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</tbody>
	</table>
	</div>
	<div id="bobcontent17" class="switchgroup1">
	<s:set name ="agentTdrEmailSet" value = "agentTdrEmailSet" scope="request"/>
	<display:table name="agentTdrEmailSet" class="table" requestURI="" id="agentTdrEmailSetID"  defaultsort="4" export="" pagesize="10">
   	<display:column sortable="true" title="File Number" sortProperty="fileNumber">
	<a HREF="javascript:updateCheckUserName1('${agentTdrEmailSetID.resultRecordId}','${agentTdrEmailSetID.fieldToValidate1}','${agentTdrEmailSetID.url}','${agentTdrEmailSetID.fieldToValidate1}','${agentTdrEmailSetID.fieldToValidate2}','${agentTdrEmailSetID.resultRecordId}','${agentTdrEmailSetID.resultRecordId}','','${agentTdrEmailSetID.ruleNumber}','${agentTdrEmailSetID.supportingId}');"><c:out value="${agentTdrEmailSetID.fileNumber}" /></a>
	</display:column>
	<display:column property="messagedisplayed"  style="width:20%" sortable="true"     titleKey="toDoRulemessage.message"/>
    <display:column property="fieldDisplay" sortable="true"  titleKey="toDoRule.fieldtovalidate"/>
   	<display:column headerClass="containeralign" style="text-align: right" property="durationAddSub" sortable="true"   title="OD"/>
	<display:column property="shipper" sortable="true"   title="Shipper's Name"/>
	<display:column property="agentName" sortable="true"   title="Agent Name"/>
	<display:column property="rolelist" sortable="true"   title="Agent Role"/>
	<display:column property="emailNotification" sortable="true" title="Agent Email"/>
	</display:table>
	
</div>
</td>
</tr>  
 </c:if>
     </td>
     </tr>
     </table>
    </div>
<div class="bottom-header" style="margin-top:35px;!margin-top:45px;"><span></span></div>
</div>
</div>
</s:form>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('comingUp', 'fade=1,hide=1,persist=0')
animatedcollapse.addDiv('dueToday', 'fade=1,hide=1,persist=0')
animatedcollapse.addDiv('overdue', 'fade=1,hide=1,persist=0')
animatedcollapse.addDiv('alert', 'fade=1,hide=1,persist=0')
animatedcollapse.addDiv('dueTodaychild1', 'fade=1,hide=1,persist=0')
animatedcollapse.addDiv('overduechild1', 'fade=1,hide=1,persist=0')
animatedcollapse.addDiv('dueTodaychild2', 'fade=1,hide=1,persist=0')
animatedcollapse.addDiv('overduechild2', 'fade=0,hide=1,persist=0')
animatedcollapse.init()
</script>
<script type="text/javascript">
function updateCheckUserName(target, target1,gotourl,field,field1,id,sid,isAccLine){
	var str="0";
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">
	str="1";
	</configByCorp:fieldVisibility>
    
   	var recordId= target;
   	var fieldToValidate=target1;
   	var newWindow="";
   	var url="updateUserCheck.html?ajax=1&decorator=simple&popup=true&recordId="+encodeURI(recordId)+"&fieldToValidate=" + encodeURI(fieldToValidate);
   	http2.open("GET", url, true);	
	http2.onreadystatechange = httpGoToURL;
	if(isAccLine == 'N'){		
		if((str=="1") && (gotourl=='editServicePartner' || gotourl=='editVehicle')){
			gotourl = 'containersAjaxList';
			field = field+""+id;
			newWindow = window.open(gotourl+'.html?&id='+sid+'&from=rule&field='+field+'&field1='+field1, '_blank');
			newWindow.focus();
		}else{
	        newWindow = window.open(gotourl+'.html?from=rule&field='+field+'&field1='+field1+'&id='+id+'&sid='+sid, '_blank');
			newWindow.focus();
		}		
	}else{
			newWindow = window.open(gotourl+'.html?from=rule&field='+field+'&field1='+field1+'&sid='+sid, '_blank');
			newWindow.focus();
	}	
    http2.send(null);
	}
	
	function httpGoToURL()
    {  
                        
       var results = http2.responseText;
       results = results.trim();
    }
	var http2 = getHTTPObject();
	function getHTTPObject()
	{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP.4.0");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP.4.0");
	        }
	    }
	    return xmlhttp;
	}
	function openRulesNotes(target, target1, target2, target3, target4){

	    openWindow('openRulesNotes.html?fileNumber='+target+'&tdrID='+target1+'&resultRecordId='+target2+'&noteType='+target3+'&noteSubType='+target4+'&decorator=popup&popup=true',650,305);

	    }
	 function openNote(id,noteType,noteSubType)
	 {

	  	openWindow('openRulesNotess.html?id='+id+'&noteType='+noteType+'&noteSubType='+noteSubType+'&decorator=popup&popup=true',650,305);

	 }
	 function makeAsMarked(target){
	      var targetId=target.id;
		     if(target.checked==true){
	    		var newIds=document.forms['tasksForm'].elements['newIdList'].value;
	    			if(newIds==''){
	    				newIds=targetId;
	    			}else{
						    newIds=newIds + ',' + targetId;
	    			}
	    		document.forms['tasksForm'].elements['newIdList'].value=newIds;
	    	  }	else if(target.checked==false){
	    			var newIds=document.forms['tasksForm'].elements['newIdList'].value;
	    			if(newIds.indexOf(targetId)>-1){
	    				newIds=newIds.replace(targetId,"");
	    				newIds=newIds.replace(",,",",");
	    				var len=newIds.length-1;
					    if(len==newIds.lastIndexOf(",")){
	    					newIds=newIds.substring(0,len);
	    				}
	    				if(newIds.indexOf(",")==0){
	    					newIds=newIds.substring(1,newIds.length);
		    
	    				}
	    				document.forms['tasksForm'].elements['newIdList'].value=newIds;
	    			}
	    		}
	    }
	    function makeAsViewed()  {
			var newIds=document.forms['tasksForm'].elements['newIdList'].value;
	  	  var url="makeAsViewed.html?ajax=1&decorator=simple&popup=true&newIdList="+newIds;
	  	http2.open("GET", url, true); 
	  	http2.onreadystatechange = handleHttpResponse1111;
	  	http2.send(null); 
		}
	    function handleHttpResponse1111(){
			if (http2.readyState == 4){
     		var results = http2.responseText
     		   results = results.trim(); 
     		 	if(results.length>1){
     		 	}
     		 	 window.location.reload();             
			}
}
	    function showFilePage(historyId,xtranId,tableName)  {
			   if(tableName.trim()=='customerfile')				               {			            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');			               }
	           if(tableName.trim()=='trackingstatus')			               {				            	    window.open('editTrackingStatus.html?id='+xtranId, '_blank');				               }
	           if(tableName.trim()=='serviceorder')				               {				            	    window.open('editServiceOrderUpdate.html?id='+xtranId, '_blank');				               }
	           if(tableName.trim()=='miscellaneous')				               {				            	    window.open('editServiceOrderUpdate.html?id='+xtranId, '_blank');				               }
	           //if(tableName.trim()=='accountline')				               {				            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');				               }
	           if(tableName.trim()=='workticket')				               {				            	    window.open('editWorkTicketUpdate.html?id='+xtranId, '_blank');				               }
//	           if(tableName.trim()=='service')				               {				            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');				               }
	           if(tableName.trim()=='billing')				               {				            	    window.open('editBilling.html?id='+xtranId, '_blank');				               }
//		               if(tableName.trim()=='carton')				               {				            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');				               }
//			               if(tableName.trim()=='refMaster')				               {				            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');				               }
//				               if(tableName.trim()=='servicePartner')				               {				            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');				               }
//	           if(tableName.trim()=='notes')				               {				            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');				               }
	           if(tableName.trim()=='container')				               {				            	    window.open('containers.html?id='+xtranId, '_blank');				               }
	         
	     	  var url="setIsViewFlag.html?ajax=1&decorator=simple&popup=true&historyId="+historyId;
	     	 http2.open("GET", url, true); 
	     	http2.onreadystatechange = function(){ handleHttpResponse2222(xtranId,tableName);};
	     	http2.send(null); 
		}
	function handleHttpResponse2222(xtran,tableName){
				if (http2.readyState == 4){
	        		var results = http2.responseText
	        		   results = results.trim(); 
	        		 	if(results.length>1){
	        		 	}
	        		 	 window.location.reload();             
				}
	}
	
	function updateCheckUserName1(target, target1,gotourl,field,field1,id,sid,isAccLine,ruleNumber,supportingId)
	{
		var str="0";
		<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">
		str="1";
		</configByCorp:fieldVisibility>
	   	var recordId= target;
	   	var fieldToValidate=target1;
	   	var url="updateUserCheck.html?ajax=1&decorator=simple&popup=true&recordId="+encodeURI(recordId)+"&fieldToValidate=" + encodeURI(fieldToValidate);
	   	http2.open("GET", url, true);	
		http2.onreadystatechange = httpGoToURL;
		if((str=="1") && (gotourl=='editServicePartner' || gotourl=='editVehicle')){
			gotourl = 'containersAjaxList';
			if(field!='' && field.indexOf("brokerDetails")!= -1){
				
			}else{
			field = field+""+id;
			}
			var newWindow = window.open(gotourl+'.html?from=rule&field='+field+'&field1='+field1+'&ruleNumber='+ruleNumber+'&id='+supportingId, '_blank');
			newWindow.focus();
		}else{
			var newWindow = window.open(gotourl+'.html?from=rule&field='+field+'&field1='+field1+'&ruleNumber='+ruleNumber+'&id='+id, '_blank');
			newWindow.focus();
		}
			
	    http2.send(null);
		}
	   	function httpGoToURL()
	        {  
	                            
	           var results = http2.responseText;
	           results = results.trim();
	        }
	   	
	function openClientEmail(email,shipNumber,lastName,firstName){
		var subject ="SO# "+shipNumber+" Name: "+firstName+" "+lastName;
		var mailto_link = 'mailto:'+encodeURIComponent(email)+'?subject='+encodeURIComponent(subject)+'&body=';		
		window.location.href = mailto_link;
	}
</script>
<script type="text/javascript">
try{
<c:if test="${not empty toDayAlertList}">
		document.forms['tasksForm'].elements['showLine'].disabled=false;
</c:if>
<c:if test="${empty toDayAlertList}">
		document.forms['tasksForm'].elements['showLine'].disabled=true;
</c:if>

}
catch(e){}
</script>