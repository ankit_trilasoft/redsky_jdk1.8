<%@ include file="/common/taglibs.jsp"%> 
 
<head> 
    <title><fmt:message key="partnerVanLineRef.title"/></title> 
    <meta name="heading" content="<fmt:message key='partnerVanLineRef.heading'/>"/> 
   <style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}
</style>     
<script>
function goToCustomerDetail(targetValue){
		var pType=document.forms['vanLineRef'].elements['partnerType'].value;
        document.forms['vanLineRef'].elements['id'].value = targetValue;
        document.forms['vanLineRef'].action = 'editPartnerVanlineRef.html?from=list&partnerType='+pType;
        document.forms['vanLineRef'].submit();
}

function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure you wish to remove this row?");
	var partnerCodeForRef=document.forms['vanLineRef'].elements['partnerCodeForRef'].value;
	var partnerType=document.forms['vanLineRef'].elements['partnerType'].value;
	if (agree){
	     location.href = "deleteVanlineRef.html?id="+encodeURI(targetElement)+"&partnerCodeForRef="+encodeURI(partnerCodeForRef)+"&partnerType="+encodeURI(partnerType);
     }else{
		return false;
	}
}
function disabledAll(){
	var elementsLen=document.forms['vanLineRef'].elements.length;
	for(i=0;i<=elementsLen-1;i++){
		if(document.forms['vanLineRef'].elements[i].type=='text'){
			document.forms['vanLineRef'].elements[i].readOnly =true;
			document.forms['vanLineRef'].elements[i].className = 'input-textUpper';
		}else if(document.forms['vanLineRef'].elements[i].type=='textarea'){
			document.forms['vanLineRef'].elements[i].readOnly =true;
			document.forms['vanLineRef'].elements[i].className = 'textareaUpper';
		}else{
			document.forms['vanLineRef'].elements[i].disabled=true;
		} 
	}
}

</script>
</head> 
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>"/>

<s:form id="vanLineRef"> 
<c:set var ="partnerCodeForRef" value="<%=request.getParameter("partnerCodeForRef") %>"/>
<s:hidden name="partnerCodeForRef"  value="<%=request.getParameter("partnerCodeForRef") %>"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partner.id"/>
<s:hidden name="sessionCorpID" value="${sessionCorpID}" />
<c:set var="buttons">
	<c:if test="${sessionCorpID == 'TSFT'}"> 
    	<input type="button" class="cssbutton" style="margin-right: 5px; width:45px;" 
        onclick="location.href='<c:url value="/editPartnerVanlineRef.html?partnerCodeForRef=${partnerCodeForRef}&partnerType=${partnerType}"/>'" 
        value="<fmt:message key="button.add"/>"/> 
     </c:if>
</c:set>
<div id="layer4" style="width:100%;">
<div id="newmnav"> 
<ul>

<c:if test="${partnerType == 'AG'}">
	<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Agent Detail</span></a></li>
	<li><a href="findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=${partnerType}&id=${partner.id}"><span>Agent Profile</span></a></li>
	<c:if test="${sessionCorpID!='TSFT' }">
	<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
	</c:if>
	<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
</c:if>
<c:if test="${partnerType == 'VN'}">
	<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Vendor Detail</span></a></li>
	<c:if test="${sessionCorpID!='TSFT' }">
	<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
	</c:if>
</c:if>
<c:if test="${partnerType == 'CR'}">
	<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Carrier Detail</span></a></li>
</c:if>
<li id="newmnav1" style="background:#FFF "><a class="current"><span>Vanline Ref List</span></a></li>
<c:if test="${partnerType == 'AG'}">
	<li><a href="baseList.html?id=${partner.id}"><span>Base</span></a></li>
	<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
	<!--<c:if test='${partner.latitude == ""  || partner.longitude == "" || partner.latitude == null  || partner.longitude == null}'>
		 <li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
	</c:if>
    <c:if test='${partner.latitude != "" && partner.longitude != "" && partner.latitude != null && partner.longitude != null}'>
		<li><a href="partnerRateGrids.html?partnerId=${partner.id}"><span>Rate Matrix</span></a></li>         
    </c:if> 
--></c:if>
<c:if test="${partnerType == 'OO'}">
	<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Owner Ops Detail</span></a></li>
</c:if>
<c:if test="${partnerType != 'AG'}">
	<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
	<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
	<c:if test='${paramValue == "View"}'>
		<li><a href="searchPartnerView.html"><span>Partner List</span></a></li>
	</c:if>
	<c:if test='${paramValue != "View"}'>
		<li><a href="partnerPublics.html?partnerType=${partnerType}"><span>Partner List</span></a></li>
	</c:if>
</c:if>
	<c:if test="${partnerType == 'AG'}">
		<li><a href="partnerReloSvcs.html?id=${partner.id}&partnerType=${partnerType}"><span>Services</span></a></li>
	</c:if>	
	<c:if test="${partner.partnerPortalActive == true  && partnerType == 'AG'}">
	      <configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
			<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users & contacts</span></a></li>
			<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
	        </configByCorp:fieldVisibility>
	</c:if>
	  <c:if test="${partnerType == 'AG'}">
			  <div id="AGR" >
			       <li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
			  </div>
	</c:if>
</ul>
</div><div class="spn">&nbsp;</div>
	
		
		
   <div id="content" align="center">
   <div id="liquid-round-top" style="!margin-top:-46px;">
   <div class="top"><span></span></div>
   <div class="center-content">
		<table style="margin-bottom: 3px">
			<tr>
		  		<td align="right" class="listwhitetext">Code</td>
				<td><s:textfield key="partner.partnerCode" required="true" readonly="true" cssClass="text medium"/></td>
				<td align="right" class="listwhitetext" width="70px">Name</td>
				<td><s:textfield key="partner.lastName" required="true" size="35" readonly="true" cssClass="text medium"/></td>
			</tr>
			<tr>
		  		<td align="right" class="listwhitetext" height="10"></td>
		  	</tr>
		</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<s:set name="partnerVanlineRefs" value="partnerVanlineRefs" scope="request"/> 
<display:table name="partnerVanlineRefs" class="table" requestURI="" id="partnerVanlineRefList" export="false" pagesize="25" style="width:99%;margin-left:5px;"> 
   	<display:column property="jobType" sortable="true" href="editPartnerVanlineRef.html?partnerType=${partnerType}" paramId="id" paramProperty="id" titleKey="partnerVanLineRef.jobType"></display:column>
   	<display:column property="vanLineCode" sortable="true" titleKey="partnerVanLineRef.vanlineCode"/> 
 	<c:if test="${sessionCorpID == 'TSFT'}">
 	<display:column title="Remove" style="width:45px; text-align: center;">
		<a><img align="middle" onclick="confirmSubmit(${partnerVanlineRefList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	</display:column>
	</c:if>
</display:table>
<c:out value="${buttons}" escapeXml="false" />
<s:hidden name="id"/>
</div> 
</s:form>
<script type="text/javascript">
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/partnerVanlineRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>   
<script type="text/javascript">
try{
	<c:if test="${not empty partner.id && partner.corpID!='TSFT'}">
	 disabledAll();
	</c:if>	
	<c:if test="${partnerType=='AG'}">
	var checkRoleAG = 0;
	//Modified By subrat BUG #6471 
	<sec-auth:authComponent componentId="module.tab.partner.AgentDEV">
		checkRoleAG  = 14;
	</sec-auth:authComponent>
	if(checkRoleAG < 14){
		document.getElementById('AGR').style.visibility = 'visible';
	}else{
		document.getElementById('AGR').style.visibility = 'hidden';
	}
	</c:if>
}catch(e){}

</script>