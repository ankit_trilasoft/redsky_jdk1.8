<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<title>Commision Structure</title>
<STYLE type=text/css>
</style>
<!-- Modified By Dilip at 16-May-2013 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->


<script type="text/JavaScript">
function addRow(tableID) {

	  var table = document.getElementById(tableID);
	    var rowCount = table.rows.length;
		var row = table.insertRow(rowCount); 
	    var newlineid=document.getElementById('newlineid').value;
	    if(newlineid==''){
	        newlineid=rowCount;
	      }else{
	      	newlineid=newlineid+"~"+rowCount;
	      }
	    document.getElementById('newlineid').value=newlineid;
	 	
		var cell1 = row.insertCell(0);
		var element1 = document.createElement("input");
		element1.type = "text";
		element1.style.width="50px";
		element1.setAttribute("class", "input-text" );
		element1.id='gfm'+rowCount;
		element1.setAttribute("onkeydown","return onlyFloatNumsAllowed(event);");
		//element1.name='grossFromList';
		cell1.appendChild(element1);

		var cell2 = row.insertCell(1);
		var element2 = document.createElement("input");
		element2.type = "text";
		element2.style.width="50px";
		element2.setAttribute("class", "input-text" );
		element2.id='gtm'+rowCount;
		element2.setAttribute("onkeydown","return onlyFloatNumsAllowed(event);");
		//element2.name='grossToList';
		cell2.appendChild(element2);

		var cell3 = row.insertCell(2);
		var element3 = document.createElement("input");
		element3.type = "text";
		element3.style.width="50px";
		element3.setAttribute("class", "input-text" );
		element3.id='sba'+rowCount;
		element3.setAttribute("onkeydown","return onlyFloatNumsAllowed(event);");
		//element3.name='salesBaList';
		cell3.appendChild(element3);

		var cell4 = row.insertCell(3);
		var element4 = document.createElement("input");
		element4.type = "text";
		element4.style.width="50px";
		element4.setAttribute("class", "input-text" );
		element4.id='sbo'+rowCount;
		element4.setAttribute("onkeydown","return onlyFloatNumsAllowed(event);");
		//element4.name='salesBoList';
		cell4.appendChild(element4);
		
		var cell5 = row.insertCell(4);
		var element5 = document.createElement("input");
		element5.type = "text";
		element5.style.width="50px";
		element5.setAttribute("class", "input-text" );
		element5.id='fwa'+rowCount;
		element5.setAttribute("onkeydown","return onlyFloatNumsAllowed(event);");
		//element5.name='forwardingList';
		cell5.appendChild(element5);
            
        }

function save(){

    var grossMarginFromListServer="";   	
    var grossMarginToListServer="";
    var salesBaListServer="";
    var salesBoListServer="";
   	var forwarderListServer="";
	 	     
   	var id='';	 
		   
		    if(document.forms['commisionStructureForm'].grossFromList!=undefined){
		        if(document.forms['commisionStructureForm'].grossFromList.length!=undefined){
		 	      for (i=0; i<document.forms['commisionStructureForm'].grossFromList.length; i++){	
		 	    	 id=document.forms['commisionStructureForm'].grossFromList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(grossMarginFromListServer==''){
	 	            	grossMarginFromListServer=id+": "+document.forms['commisionStructureForm'].grossFromList[i].value;
	 	             }else{
	 	            	grossMarginFromListServer= grossMarginFromListServer+"~"+id+": "+document.forms['commisionStructureForm'].grossFromList[i].value;
	 	             }
		 	        }	
		 	      }else{	
		 	    	 id=document.forms['commisionStructureForm'].grossFromList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	        grossMarginFromListServer=id+": "+document.forms['commisionStructureForm'].grossFromList.value;
		 	      }	        
		       }		   

		    if(document.forms['commisionStructureForm'].grossToList!=undefined){
		        if(document.forms['commisionStructureForm'].grossToList.length!=undefined){
		 	      for (i=0; i<document.forms['commisionStructureForm'].grossToList.length; i++){	
		 	    	 id=document.forms['commisionStructureForm'].grossToList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(grossMarginToListServer==''){
	 	            	grossMarginToListServer=id+": "+document.forms['commisionStructureForm'].grossToList[i].value;
	 	             }else{
	 	            	grossMarginToListServer= grossMarginToListServer+"~"+id+": "+document.forms['commisionStructureForm'].grossToList[i].value;
	 	             }
		 	        }	
		 	      }else{	
		 	    	 id=document.forms['commisionStructureForm'].grossToList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	        grossMarginToListServer=id+": "+document.forms['commisionStructureForm'].grossToList.value;
		 	      }	        
		       }		   
		   
		    if(document.forms['commisionStructureForm'].salesBaList!=undefined){
		        if(document.forms['commisionStructureForm'].salesBaList.length!=undefined){
		 	      for (i=0; i<document.forms['commisionStructureForm'].salesBaList.length; i++){	
		 	    	 id=document.forms['commisionStructureForm'].salesBaList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(salesBaListServer==''){
	 	            	salesBaListServer=id+": "+document.forms['commisionStructureForm'].salesBaList[i].value;
	 	             }else{
	 	            	salesBaListServer= salesBaListServer+"~"+id+": "+document.forms['commisionStructureForm'].salesBaList[i].value;
	 	             }
		 	        }	
		 	      }else{	
		 	    	 id=document.forms['commisionStructureForm'].salesBaList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	        salesBaListServer=id+": "+document.forms['commisionStructureForm'].salesBaList.value;
		 	      }	        
		       }		   

		    if(document.forms['commisionStructureForm'].salesBoList!=undefined){
		        if(document.forms['commisionStructureForm'].salesBoList.length!=undefined){
		 	      for (i=0; i<document.forms['commisionStructureForm'].salesBoList.length; i++){	
		 	    	 id=document.forms['commisionStructureForm'].salesBoList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(salesBoListServer==''){
	 	            	salesBoListServer=id+": "+document.forms['commisionStructureForm'].salesBoList[i].value;
	 	             }else{
	 	            	salesBoListServer= salesBoListServer+"~"+id+": "+document.forms['commisionStructureForm'].salesBoList[i].value;
	 	             }
		 	        }	
		 	      }else{	
		 	    	 id=document.forms['commisionStructureForm'].salesBoList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	        salesBoListServer=id+": "+document.forms['commisionStructureForm'].salesBoList.value;
		 	      }	        
		       }		   

		    if(document.forms['commisionStructureForm'].forwardingList!=undefined){
		        if(document.forms['commisionStructureForm'].forwardingList.length!=undefined){
		 	      for (i=0; i<document.forms['commisionStructureForm'].forwardingList.length; i++){	
		 	    	 id=document.forms['commisionStructureForm'].forwardingList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(forwarderListServer==''){
	 	            	forwarderListServer=id+": "+document.forms['commisionStructureForm'].forwardingList[i].value;
	 	             }else{
	 	            	forwarderListServer= forwarderListServer+"~"+id+": "+document.forms['commisionStructureForm'].forwardingList[i].value;
	 	             }
		 	        }	
		 	      }else{	
		 	    	 id=document.forms['commisionStructureForm'].forwardingList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	        forwarderListServer=id+": "+document.forms['commisionStructureForm'].forwardingList.value;
		 	      }	        
		       }		   
		    
			 document.getElementById('grossMarginFromListServer').value=grossMarginFromListServer;			 
			 document.getElementById('grossMarginToListServer').value=grossMarginToListServer;  
			 document.getElementById('salesBaListServer').value=salesBaListServer; 		     		       
			 document.getElementById('salesBoListServer').value=salesBoListServer;  
			 document.getElementById('forwarderListServer').value=forwarderListServer; 		     		       

		     var commissionStructorListServer="";
		     var newlineid=document.getElementById('newlineid').value;
		     if(newlineid!=''){
		            var arrayLine=newlineid.split("~");
		            for(var i=0;i<arrayLine.length;i++){		            	              
		                if(commissionStructorListServer==''){
		                	commissionStructorListServer=document.getElementById('gfm'+arrayLine[i]).value+":"+document.getElementById('gtm'+arrayLine[i]).value+" :"+document.getElementById('sba'+arrayLine[i]).value+" :"+document.getElementById('sbo'+arrayLine[i]).value+" :"+document.getElementById('fwa'+arrayLine[i]).value;
		                }else{
		                	commissionStructorListServer=commissionStructorListServer+"~"+document.getElementById('gfm'+arrayLine[i]).value+":"+document.getElementById('gtm'+arrayLine[i]).value+" :"+document.getElementById('sba'+arrayLine[i]).value+" :"+document.getElementById('sbo'+arrayLine[i]).value+" :"+document.getElementById('fwa'+arrayLine[i]).value;
		                }
		              }
		         }
		     
		    document.getElementById('commissionStructorListServer').value =commissionStructorListServer;
		    saveCommissionValue();			   
}
	function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
	} 

function saveCommissionValue(){
    var grossMF=document.getElementById('grossMarginFromListServer').value;   	
    var grossMT=document.getElementById('grossMarginToListServer').value;
    var salesBA=document.getElementById('salesBaListServer').value;
    var salesBO=document.getElementById('salesBoListServer').value;
   	var forwaDR=document.getElementById('forwarderListServer').value;
   	var commiNL=document.getElementById('commissionStructorListServer').value;
   	if((grossMF!='')||(grossMT!='')||(salesBA!='')||(salesBO!='')||(forwaDR!='')||(commiNL!='')){   
	    var url="saveCommissionValueAjax.html?ajax=1&grossMF="+grossMF+"&grossMT="+grossMT+"&salesBA="+salesBA+"&salesBO="+salesBO+"&forwaDR="+forwaDR+"&commiNL="+commiNL+"&decorator=simple&popup=true&sContarct=${sContarct}&sChargeCode=";
	    http1986.open("GET", url, true);
	    http1986.onreadystatechange = handleHttpResponseAccListData;
	    http1986.send(null); 
   	}
}
function handleHttpResponseAccListData(){
    if (http1986.readyState == 4){
        alert("Commission Structure Successfully Updated.");
        document.getElementById('grossMarginFromListServer').value="";   	
       	document.getElementById('grossMarginToListServer').value="";
        document.getElementById('salesBaListServer').value="";
        document.getElementById('salesBoListServer').value="";
       	document.getElementById('forwarderListServer').value="";
       	document.getElementById('commissionStructorListServer').value="";
       	document.getElementById('newlineid').value="";
    }
}
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
var http1986 = getHTTPObject();
</script>

<s:form name="commisionStructureForm" id="commisionStructureForm" action="" method="post">
<s:hidden name="newlineid" id="newlineid" value=""/>
<s:hidden name="commissionStructorListServer" id="commissionStructorListServer" value=""/>
<s:hidden name="grossMarginFromListServer" id="grossMarginFromListServer" value=""/>
<s:hidden name="grossMarginToListServer" id="grossMarginToListServer" value=""/>
<s:hidden name="salesBaListServer" id="salesBaListServer" value=""/>
<s:hidden name="salesBoListServer" id="salesBoListServer" value=""/>
<s:hidden name="forwarderListServer" id="forwarderListServer" value=""/>

		<div id="otabs" style="margin-top: 10px; margin-bottom:0px;">

		  <ul>
		    <li><a class="current"><span>Commission Structure</span></a></li>		    
		  </ul>
		</div>
		<div class="spn" style="line-height:0px;">&nbsp;</div>
		<div id="para1" style="clear:both;">
		 <table class="table" id="dataTable" style="width:500px;!margin-top:-6px;">
			 <thead>
			 <tr>
				 <th width="50px">Gross Margin %</th>
				 <th width="50px">Gross Margin %</th>
				 <th width="50px">Sales - BA %</th>
				 <th width="50px">Back Office %</th>
				 <th width="50px">Forwarding %</th>
			 </tr>
			 </thead>
		 <tbody>
			 <c:forEach var="individualItem" items="${commissionStructureList}" >
				 <tr>
				 <td  class="listwhitetext" align="right" >
				 <input type="text" value="${individualItem.grossMarginFrom}" maxlength="15" class="input-text" name="grossFromList" id="gfm${individualItem.id}" onkeydown="return onlyFloatNumsAllowed(event);" style="width:50px;" />
				 </td>
				 <td  class="listwhitetext" align="right" >
				 <input type="text" value="${individualItem.grossMarginTo}" maxlength="15" class="input-text" name="grossToList" id="gtm${individualItem.id}" onkeydown="return onlyFloatNumsAllowed(event);" style="width:50px;" />
				 </td>
				 <td  class="listwhitetext" align="right" >
				 <input type="text" value="${individualItem.salesBa}" maxlength="15" class="input-text" name="salesBaList" id="sba${individualItem.id}" onkeydown="return onlyFloatNumsAllowed(event);" style="width:50px;" />
				 </td>
				 <td  class="listwhitetext" align="right" >
				<input type="text" value="${individualItem.backOffice}" maxlength="15" class="input-text" name="salesBoList" id="sbo${individualItem.id}" onkeydown="return onlyFloatNumsAllowed(event);" style="width:50px;"/>				 
				 </td>
				 <td  class="listwhitetext" align="right" >
				 <input type="text" value="${individualItem.forwarding}" maxlength="15" class="input-text" name="forwardingList" id="fwa${individualItem.id}" onkeydown="return onlyFloatNumsAllowed(event);" style="width:50px;" />
				 </td>
				 </tr>
			 </c:forEach>
		 </tbody>		 		  
		 </table>
		 <table>
		 <tr>
		      	<td>
		      	<input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:65px; height:25px;margin-left:38px;" value="Add Lines" onclick="addRow('dataTable');" />
		      	</td>
		      	<td>
				<input type="button" class="cssbutton1" id="saveLine" name="saveLine" style="width:60px; height:25px;margin-left:5px;" value="Save" onclick="save();" />
		      	</td>		 
	     </tr>			 
		 
		 </table>
		 </div>
</s:form>