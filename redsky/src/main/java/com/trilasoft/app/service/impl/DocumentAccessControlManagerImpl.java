package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DocumentAccessControlDao;
import com.trilasoft.app.model.DocumentAccessControl;
import com.trilasoft.app.service.DocumentAccessControlManager;

public class DocumentAccessControlManagerImpl extends GenericManagerImpl<DocumentAccessControl, Long> implements DocumentAccessControlManager {
	DocumentAccessControlDao documentAccessControlDao; 
     
	public DocumentAccessControlManagerImpl(DocumentAccessControlDao documentAccessControlDao) { 
        super(documentAccessControlDao); 
        this.documentAccessControlDao = documentAccessControlDao; 
	}
	
	public List isExisted(String fileType, String sessionCorpID){
		return documentAccessControlDao.isExisted(fileType, sessionCorpID);
	}
}
