<%@ include file="/common/taglibs.jsp"%>

<head>
	<title><fmt:message key="searchstorageList2.title" /></title>
	<meta name="heading" content="<fmt:message key='searchstorageList2.heading'/>" />
	
	<script language="javascript" type="text/javascript">

	function clear_fields()
	{
		var i;
		for(i=0;i<=1;i++)
		{
				document.forms['searchForm'].elements[i].value = "";
		}
	}
	</script>
	
</head>

<c:set var="buttons">
    <input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/workTickets.html"/>'" 
        value="<fmt:message key="button.done"/>"/>
</c:set> 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>

<s:form id="searchForm" action="searchStorages2" method="post" validate="true">   

<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-left.jpg'/>"/></td>
      <td id="searchLabelCenter" width="50" align="center" class="detailActiveTabLabel content-head">Search</td>
      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-right.jpg'/>"/></td>
      <td></td>
    </tr>
</tbody></table>

<table class="table" style="width:992px"  >
<thead>
<tr>
<th><fmt:message key="storage.itemNumber"/></th>
<th><fmt:message key="storage.jobNumber"/></th>
<th></th>
</tr></thead>	
		<tbody>
		<tr>
			<td width="30" align="left">
			    <s:textfield name="storage.itemNumber" required="true" cssClass="input-text" size="20"/>
			</td>
			<td width="30" align="left">
			    <s:textfield name="storage.jobNumber" required="true" cssClass="input-text" size="20"/>
			</td>
			<td width="130px" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
	
</s:form>

<c:out value="${searchresults}" escapeXml="false" />

<c:set var="buttons">
	
	<input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/storages2.html"/>'"
		value="<fmt:message key="button.done"/>" />
</c:set>


<s:set name="storages" value="storages" scope="request" />
<display:table name="storages" class="table" requestURI=""
	id="storageList" export="true" pagesize="25">
	<tr>
		<display:column property="jobNumber" sortable="true" titleKey="workTicket.jobNumber" url="/editStorage2.html?from=list" paramId="id" paramProperty="id"/>
	 	<display:column property="itemNumber" titleKey="storage.itemNumber"/>
	 	<display:column property="releaseDate" titleKey="storage.releaseDate" format="{0,date,dd-MMM-yyyy}"/>
		<display:column property="toRelease" titleKey="storage.toRelease" format="{0,date,dd-MMM-yyyy}"/>
		<display:column property="description" titleKey="storage.description"/>
		<display:column property="containerId" titleKey="storage.containerId"/>
		<display:column property="itemTag" titleKey="storage.itemTag"/>
		<display:column property="pieces" titleKey="storage.pieces"/>
		<display:column property="price" titleKey="storage.price"/>
	</tr>
	<display:setProperty name="paging.banner.item_name" value="storage" />
	<display:setProperty name="paging.banner.items_name" value="storage" />

	<display:setProperty name="export.excel.filename"
		value="Storage List.xls" />
	<display:setProperty name="export.csv.filename"
		value="Storage List.csv" />
	<display:setProperty name="export.pdf.filename"
		value="Storage List.pdf" />
</display:table>

<!-- <c:out value="${buttons}" escapeXml="false" /> -->

<script type="text/javascript"> 
    highlightTableRows("searchstorageList2"); 
</script>
