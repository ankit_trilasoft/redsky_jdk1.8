package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.BrokerExamDetails;

public interface BrokerExamDetailsDao  extends GenericDao<BrokerExamDetails, Long>{
	public List findByBrokerExamDetailsList(String shipNumber);
	public void updateBrokerDetailsAjaxFromList(Long id,String fieldName,String fieldValue,String sessionCorpID,String shipNumber, String userName,String fieldType);

}
