ALTER TABLE `redsky`.`frequentlyaskedquestions` MODIFY COLUMN `sequenceNumber` BIGINT(20) UNSIGNED;

ALTER TABLE `redsky`.`itemsjequip` ADD COLUMN `template` bit(1) DEFAULT b'0' AFTER `controlled`;

ALTER TABLE `redsky`.`itemsjbkequip` MODIFY COLUMN `cost` DOUBLE DEFAULT 0;

ALTER TABLE `redsky`.`itemsjbkequip` ADD COLUMN `day` VARCHAR(15) DEFAULT NULL AFTER `comments`;

ALTER TABLE `redsky`.`itemsjbkequip` ADD COLUMN `beginDate` DATETIME DEFAULT NULL AFTER `day`;

ALTER TABLE `redsky`.`itemsjbkequip` ADD COLUMN `endDate` DATETIME DEFAULT NULL AFTER `beginDate`;

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','QSURVEYRECIPIENT',200,true,now(),'dkumar',now(),'dkumar',1,1,'');
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','QSURVEYSUBJECT',200,true,now(),'dkumar',now(),'dkumar',1,1,'');
 
update networkdatafields set type='CMM' where modelName='AccountLine' and toFieldName=''   and fieldName in ('estSellCurrency','estSellValueDate','revisionSellCurrency','revisionSellValueDate','recRateCurrency');

update networkdatafields set type='CMM' where modelName='AccountLine' and toFieldName in ('estiamtePayableContractCurrency','estiamtePayableContractValueDate','estiamtePayableContractRate','estiamtePayableContractRateAmmount','revisionPayableContractCurrency','revisionPayableContractValueDate','revisionPayableContractRate','revisionPayableContractRateAmmount','payableContractCurrency','payableContractValueDate','payableContractRateAmmount');

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('AccountLine','estSellCurrency' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractCurrency','DMM'),
('AccountLine','revisionSellCurrency' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractCurrency','DMM'),
('AccountLine','recRateCurrency' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','payableContractCurrency','DMM'),
('AccountLine','estSellCurrency' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractCurrency','DMM'),
('AccountLine','revisionSellCurrency' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractCurrency','DMM'),
('AccountLine','recRateCurrency' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','payableContractCurrency','DMM'),
('AccountLine','estSellValueDate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractValueDate','DMM'),
('AccountLine','revisionSellValueDate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractValueDate','DMM'),
('AccountLine','racValueDate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','payableContractValueDate','DMM'),
('AccountLine','estSellValueDate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractValueDate','DMM'),
('AccountLine','revisionSellValueDate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractValueDate','DMM'),
('AccountLine','racValueDate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','payableContractValueDate','DMM');

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `excludeFromParentQualityUpdate` BIT(1) DEFAULT b'0' AFTER `claimsUser`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `excludeFromParentQualityUpdateRelo` BIT(1) DEFAULT b'0' AFTER `excludeFromParentQualityUpdate`;

ALTER TABLE `redsky`.`entitlement` ADD COLUMN `parentId` BIGINT(20) UNSIGNED DEFAULT 0 AFTER `partnerCode`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `excludePublicFromParentUpdate` BIT(1) DEFAULT b'0' AFTER `excludeFromParentQualityUpdateRelo`;

DROP table IF EXISTS `redsky`.`refsurveyemailsubject`;

DROP table IF EXISTS `redsky`.`refsurveyemailrecipient`;

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `shipmentType` VARCHAR(1) DEFAULT NULL AFTER `internal`;

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','SHIPTYPE',1,true,now(),'subrat',now(),'subrat',1,1,'');

Insert into refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby) values('TSFT', 'G', 'Government',1, 'SHIPTYPE',now(),'subrat',now(),'subrat'),('TSFT', 'M', 'Military',1, 'SHIPTYPE',now(),'subrat',now(),'subrat'),('TSFT', 'N', 'National Account',1, 'SHIPTYPE',now(),'subrat',now(),'subrat'),('TSFT', 'C', 'C.O.D',1, 'SHIPTYPE',now(),'subrat',now(),'subrat');

ALTER TABLE `redsky`.`operationsresourcelimits` ADD UNIQUE `resourceUnique` USING BTREE(`resource`, `corpId`, `hubId`);

ALTER TABLE `redsky`.`itemsjbkequip` ADD COLUMN `ticketTransferStatus` bit(1) DEFAULT b'0' AFTER `endDate`;

DROP VIEW IF EXISTS `redsky`.`partner`;
CREATE VIEW `partner` AS select `a`.`id` AS `id`,`a`.`parent` AS `parent`,`a`.`middleInitial` AS `middleInitial`,`a`.`billingAddress1` AS `billingAddress1`,`a`.`billingAddress2` AS `billingAddress2`,`a`.`billingAddress3` AS `billingAddress3`,`a`.`billingAddress4` AS `billingAddress4`,`a`.`billingCity` AS `billingCity`,`a`.`billingCountry` AS `billingCountry`,`a`.`billingCountryCode` AS `billingCountryCode`,`a`.`billingEmail` AS `billingEmail`,`a`.`billingFax` AS `billingFax`,`a`.`billingPhone` AS `billingPhone`,`a`.`billingState` AS `billingState`,`a`.`billingTelex` AS `billingTelex`,`a`.`billingZip` AS `billingZip`,`a`.`effectiveDate` AS `effectiveDate`,`a`.`isAccount` AS `isAccount`,`a`.`isAgent` AS `isAgent`,`a`.`isCarrier` AS `isCarrier`,`a`.`isVendor` AS `isVendor`,`a`.`mailingAddress1` AS `mailingAddress1`,`a`.`mailingAddress2` AS `mailingAddress2`,`a`.`mailingAddress3` AS `mailingAddress3`,`a`.`mailingAddress4` AS `mailingAddress4`,`a`.`mailingCity` AS `mailingCity`,`a`.`mailingCountry` AS `mailingCountry`,`a`.`mailingCountryCode` AS `mailingCountryCode`,`a`.`mailingEmail` AS `mailingEmail`,`a`.`mailingFax` AS `mailingFax`,`a`.`mailingPhone` AS `mailingPhone`,`a`.`mailingState` AS `mailingState`,`a`.`mailingTelex` AS `mailingTelex`,`a`.`mailingZip` AS `mailingZip`,`a`.`partnerPrefix` AS `partnerPrefix`,`a`.`partnerSuffix` AS `partnerSuffix`,`a`.`terminalAddress1` AS `terminalAddress1`,`a`.`terminalAddress2` AS `terminalAddress2`,`a`.`terminalAddress3` AS `terminalAddress3`,`a`.`terminalAddress4` AS `terminalAddress4`,`a`.`terminalCity` AS `terminalCity`,`a`.`terminalCountry` AS `terminalCountry`,`a`.`terminalCountryCode` AS `terminalCountryCode`,`a`.`terminalEmail` AS `terminalEmail`,`a`.`terminalFax` AS `terminalFax`,`a`.`terminalPhone` AS `terminalPhone`,`a`.`terminalState` AS `terminalState`,`a`.`terminalTelex` AS `terminalTelex`,`a`.`terminalZip` AS `terminalZip`,`a`.`isPrivateParty` AS `isPrivateParty`,`a`.`air` AS `air`,`a`.`sea` AS `sea`,`a`.`surface` AS `surface`,`a`.`isOwnerOp` AS `isOwnerOp`,`a`.`typeOfVendor` AS `typeOfVendor`,`a`.`agentParent` AS `agentParent`,`a`.`location1` AS `location1`,`a`.`location2` AS `location2`,`a`.`location3` AS `location3`,`a`.`location4` AS `location4`,`a`.`companyProfile` AS `companyProfile`,`a`.`url` AS `url`,`a`.`latitude` AS `latitude`,`a`.`longitude` AS `longitude`,`a`.`trackingUrl` AS `trackingUrl`,`a`.`yearEstablished` AS `yearEstablished`,`a`.`companyFacilities` AS `companyFacilities`,`a`.`companyCapabilities` AS `companyCapabilities`,`a`.`companyDestiantionProfile` AS `companyDestiantionProfile`,`a`.`serviceRangeKms` AS `serviceRangeKms`,`a`.`serviceRangeMiles` AS `serviceRangeMiles`,`a`.`fidiNumber` AS `fidiNumber`,`a`.`OMNINumber` AS `OMNINumber`,`a`.`IAMNumber` AS `IAMNumber`,`a`.`AMSANumber` AS `AMSANumber`,`a`.`WERCNumber` AS `WERCNumber`,`a`.`facilitySizeSQFT` AS `facilitySizeSQFT`,`a`.`qualityCertifications` AS `qualityCertifications`,`a`.`vanLineAffiliation` AS `vanLineAffiliation`,`a`.`serviceLines` AS `serviceLines`,`a`.`facilitySizeSQMT` AS `facilitySizeSQMT`,`b`.`corpid` AS `corpid`,`b`.`warehouse` AS `warehouse`,`a`.`createdBy` AS `createdBy`,`a`.`createdOn` AS `createdOn`,`a`.`updatedBy` AS `updatedBy`,`a`.`updatedOn` AS `updatedOn`,`a`.`firstName` AS `firstName`,`a`.`lastName` AS `lastName`,if((`a`.`isPrivateParty` is true),`a`.`status`,`b`.`status`) AS `status`,`a`.`nextVanLocation` AS `nextVanLocation`,`a`.`nextReportOn` AS `nextReportOn`,`b`.`billingInstruction` AS `billingInstruction`,`b`.`billingInstructionCode` AS `billingInstructionCode`,`b`.`coordinator` AS `coordinator`,`b`.`salesMan` AS `salesMan`,`b`.`qc` AS `qc`,`b`.`abbreviation` AS `abbreviation`,`b`.`billPayOption` AS `billPayOption`,`b`.`billPayType` AS `billPayType`,`b`.`billToGroup` AS `billToGroup`,`b`.`longPercentage` AS `longPercentage`,`b`.`needAuth` AS `needAuth`,`a`.`partnerCode` AS `partnerCode`,`b`.`storageBillingGroup` AS `storageBillingGroup`,`b`.`accountHolder` AS `accountHolder`,`b`.`paymentMethod` AS `paymentMethod`,`b`.`payOption` AS `payOption`,`b`.`multiAuthorization` AS `multiAuthorization`,`b`.`payableUploadCheck` AS `payableUploadCheck`,`b`.`companyDivision` AS `companyDivision`,`b`.`invoiceUploadCheck` AS `invoiceUploadCheck`,`b`.`billingUser` AS `billingUser`,`b`.`payableUser` AS `payableUser`,`b`.`pricingUser` AS `pricingUser`,`b`.`partnerPortalActive` AS `partnerPortalActive`,`b`.`partnerPortalId` AS `partnerPortalId`,`b`.`associatedAgents` AS `associatedAgents`,`b`.`viewChild` AS `viewChild`,`b`.`creditTerms` AS `creditTerms`,`b`.`accountingDefault` AS `accountingDefault`,`b`.`acctDefaultJobType` AS `acctDefaultJobType`,`b`.`stopNotAuthorizedInvoices` AS `stopNotAuthorizedInvoices`,`b`.`doNotCopyAuthorizationSO` AS `doNotCopyAuthorizationSO`,`b`.`partnerPublicId` AS `partnerPublicId`,`b`.`driverAgency` AS `driverAgency`,`b`.`validNationalCode` AS `validNationalCode`,`b`.`cardNumber` AS `cardNumber`,`b`.`cardStatus` AS `cardStatus`,`b`.`accountId` AS `accountId`,`b`.`customerId` AS `customerId`,`b`.`licenseNumber` AS `licenseNumber`,`b`.`licenseState` AS `licenseState`,`b`.`cardSettelment` AS `cardSettelment`,`b`.`directDeposit` AS `directDeposit`,`b`.`fuelPurchase` AS `fuelPurchase`,`b`.`expressCash` AS `expressCash`,`b`.`creditCheck` AS `creditCheck`,`b`.`creditAmount` AS `creditAmount`,`b`.`creditCurrency` AS `creditCurrency`,`b`.`yruAccess` AS `yruAccess`,`b`.`taxId` AS `taxId`,`b`.`taxIdType` AS `taxIdType`,`b`.`extReference` AS `extReference`,`a`.`startDate` AS `startDate`,`b`.`accountManager` AS `accountManager`,`b`.`classcode` AS `classcode`,`a`.`vatNumber` AS `vatNumber`,`a`.`utsNumber` AS `utsNumber`,`a`.`isNetworkPartner` AS `isNetworkPartner`,`a`.`vanLastLocation` AS `vanLastLocation`,`a`.`vanLastReportOn` AS `vanLastReportOn`,`a`.`vanAvailCube` AS `vanAvailCube`,`a`.`vanLastReportTime` AS `vanLastReportTime`,`a`.`currentVanAgency` AS `currentVanAgency`,`a`.`currentVanID` AS `currentVanID`,`a`.`currentTractorAgency` AS `currentTractorAgency`,`a`.`currentTractorID` AS `currentTractorID`,`b`.`driverType` AS `driverType`,`b`.`noDispatch` AS `noDispatch`,`a`.`aliasName` AS `aliasName`,`a`.`networkGroup` AS `networkGroup`,`b`.`insuranceAuthorized` AS `insuranceAuthorized`,`b`.`mc` AS `mc`,`b`.`soAllDrivers` AS `soAllDrivers`,`a`.`contactName` AS `contactName`,`a`.`billingCurrency` AS `billingCurrency`,`a`.`bankCode` AS `bankCode`,`a`.`bankAccountNumber` AS `bankAccountNumber`,`a`.`agentParentName` AS `agentParentName` from (`partnerpublic` `a` join `partnerprivate` `b`) where (`a`.`id` = `b`.`partnerPublicId`);

ALTER TABLE `redsky`.`glcoderategrid` ADD UNIQUE `uniqueRateGrid`(`corpID`, `gridJob`, `gridRouting`,`costElementId`);

select concat("update serviceorder set shipmenttype='M' where id='",m.id,"';") FROM miscellaneous m,serviceorder s where m.id=s.id and m.millitaryShipment = 'Y';

update serviceorder set shipmenttype = 'G' where job = 'GST';

ALTER TABLE `redsky`.`standarddeductions` MODIFY COLUMN `chargeCode` VARCHAR(25);

ALTER TABLE `redsky`.`billing` MODIFY COLUMN `charge` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL, MODIFY COLUMN `insuranceCharge` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;




