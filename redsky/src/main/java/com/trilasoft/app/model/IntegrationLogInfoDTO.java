package com.trilasoft.app.model;

import java.util.Date;

public class IntegrationLogInfoDTO {
	
	private Long id;
	private String source;
	private String destination;
	private String integrationId;
	private String sourceOrderNum;
	private String destOrderNum;
	private String operation;
	private String orderComplete;
	private String statusCode;
	private Date effectiveDate;
	private String detailedStatus;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String ugwwOA;
	private String ugwwDA;
	private String ugwwBA;
	private String registrationNumber;
	private String sO;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getIntegrationId() {
		return integrationId;
	}
	public void setIntegrationId(String integrationId) {
		this.integrationId = integrationId;
	}
	public String getSourceOrderNum() {
		return sourceOrderNum;
	}
	public void setSourceOrderNum(String sourceOrderNum) {
		this.sourceOrderNum = sourceOrderNum;
	}
	public String getDestOrderNum() {
		return destOrderNum;
	}
	public void setDestOrderNum(String destOrderNum) {
		this.destOrderNum = destOrderNum;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getOrderComplete() {
		return orderComplete;
	}
	public void setOrderComplete(String orderComplete) {
		this.orderComplete = orderComplete;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getDetailedStatus() {
		return detailedStatus;
	}
	public void setDetailedStatus(String detailedStatus) {
		this.detailedStatus = detailedStatus;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getUgwwOA() {
		return ugwwOA;
	}
	public void setUgwwOA(String ugwwOA) {
		this.ugwwOA = ugwwOA;
	}
	public String getUgwwDA() {
		return ugwwDA;
	}
	public void setUgwwDA(String ugwwDA) {
		this.ugwwDA = ugwwDA;
	}
	public String getUgwwBA() {
		return ugwwBA;
	}
	public void setUgwwBA(String ugwwBA) {
		this.ugwwBA = ugwwBA;
	}
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	public String getsO() {
		return sO;
	}
	public void setsO(String sO) {
		this.sO = sO;
	}
	
	
	
	
	
	}
