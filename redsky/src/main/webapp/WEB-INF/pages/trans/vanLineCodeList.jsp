<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>Vanline List </b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
	<display:table name="vanLineCodeList" class="table" requestURI="" id="vanLineCodeLists" export="false" defaultsort="1" >
		<display:column title="Vanline Code" ><a onclick="goToUrlVanline('${vanLineCodeLists.vanLineCode}','${agentType}'),ajax_hideTooltip();" ><c:out value="${vanLineCodeLists.vanLineCode}" /></a></display:column>      
	</display:table>
</div>