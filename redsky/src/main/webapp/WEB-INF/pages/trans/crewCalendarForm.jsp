<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.lang.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.ArrayList" %>

<%
ArrayList<String> crewTime = new ArrayList<String>();
crewTime.add("6.00am");
crewTime.add("7.00am");
crewTime.add("8.00am");
crewTime.add("9.00am");
crewTime.add("10.00am");
crewTime.add("11.00am");
crewTime.add("12.00pm");
crewTime.add("13.00pm");
crewTime.add("14.00pm");
crewTime.add("15.00pm");
crewTime.add("16.00pm");
crewTime.add("17.00pm");
crewTime.add("18.00pm");
crewTime.add("19.00pm");
pageContext.setAttribute("crewTime", crewTime);
%>
<head>   
    <title>Crew Calendar</title>   
    <meta name="heading" content="Crew Calendar"/>     
   <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<style>
.table tr td{ border: 1px solid #74B3DC !important;  padding: 0.4em; border-right:none ;}
.table {  border: 1px solid #74B3DC;  border-collapse:inherit !important;}
.table tr td hover { border: 1px solid #74B3DC;  background:transparent !important;}
.tableHeaderTable2 { background: url("images/bg_listheader.png") repeat-x scroll 0 0 #BCD2EF; border-color: #3DAFCB;  border-style: solid; border-width: 1px;
    color: #15428B;  font-family: arial,verdana;   font-size: 11px; font-weight: bold; height: 25px; padding: 2px 3px 3px 5px; text-decoration: none;}
.list-columnmain3 { background-color: #CCCCCC; border: 1px solid #99BBE8; color: #DF5F00; font-family: arial,verdana; font-size: 11px; padding-left: 3px;}
.table tbody tr:hover, .table tr.over, .contribTable tr:hover {border:1px solid #74b3dc !important;}
</style>
<script language="javascript" type="text/javascript">
function selectSearchField(){		
	var wareHouse= document.forms['crewCalenderForm'].elements['crewCalendarwRHouse'].value;	
	var date= document.forms['crewCalenderForm'].elements['crewCalendarDate'].value;
		
	if(wareHouse==''){
		alert('Please select Warehouse to continue.....!');	
		return false;
	}else if(date==''){
		alert('Please select Date to continue.....!');	
		return false;
	}else{
		return true;
	}
}
function goToSearch(){
	var selectedSearch= selectSearchField();
	if(selectedSearch){	
		 document.forms['crewCalenderForm'].action = 'crewCalenderList.html?decorator=popup&popup=true';
		 document.forms['crewCalenderForm'].submit();					
	}
}
function opsCalendarForm(){
	location.href = 'opsCalendar.html?decorator=popup&popup=true';
}
function openWorkTicket(tempAgent){
	parent.window.open("editWorkTicketUpdate.html?id="+tempAgent) 
}

function dynamicPricingForm(){
	location.href = 'dynamicPricingCalenderView.html?decorator=popup&popup=true';
}

function planningCalendar(){
	location.href = 'planningCalendarList.html?decorator=popup&popup=true';
}
function workPlanning(){
	location.href = 'workPlan.html?decorator=popup&popup=true';
}
</script>
</head>

<s:form id="crewCalenderForm" action="" method="post" onsubmit="" > 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<div id="layer1" style="width:810px; margin: 0px auto;">
	<div id="newmnav">
	<ul>
		<li><a onclick="opsCalendarForm();"><span>Ops Calendar</span></a></li>
		<configByCorp:fieldVisibility componentId="component.field.Resource.DynamicPricingView">
		<li><a onclick="dynamicPricingForm();"><span>Price Calendar</span></a></li>
		</configByCorp:fieldVisibility>
		<c:if test="${checkCrewListRole == true && hubWarehouseLimit=='Crw'}">
		<li id="newmnav1" style="background:#FFF"><a class="current"><span>Crew Calendar</span></a></li>
		</c:if>
		<li><a onclick="workPlanning();"><span>Work Planning</span></a></li>
		<configByCorp:fieldVisibility componentId="component.field.Resource.OpsCalendarView">	
		<li><a onclick="planningCalendar();"><span>Planning Calendar</span></a></li>
		</configByCorp:fieldVisibility>
	</ul>
	</div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="left" >
<div id="liquid-round">
<div class="top" style="margin-top: 10px;!margin-top:-4px;"><span></span></div>
<div class="center-content">
<table class="" style="width:100%;margin-top:2px;" border="0" cellpadding="0" cellspacing="0">		
	<tbody>
		<tr>
		<td style="text-align:right;width:3%;" class="listwhitetext" >View:&nbsp;</td>			
			<td width="14%" style="border-right:hidden !important;">
			     <s:select cssClass="list-menu" name="crewCalendarwRHouse" list="%{house}" cssStyle="width:175px" headerKey="" headerValue="" />
			</td>		
				<td style="width:14%;text-align:right;" class="listwhitetext">Select Date:&nbsp;</td>				
			 <c:if test="${not empty crewCalendarDate}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="crewCalendarDate" /></s:text>
				<td width="67" ><s:textfield cssClass="input-text" id="date1" name="crewCalendarDate" value="%{customerFiledate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
				<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty crewCalendarDate}">
				<td width="100"><s:textfield cssClass="input-text" id="date1" name="crewCalendarDate" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
				<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>				
	<td width="200" >  
		<input type="button" class="cssbutton" style="width:65px;"  value="Search" onclick="return goToSearch();"/> 		
	</td>	
		</tr>
	</tbody>
</table>
</div>
</div>
<div class="bottom-header" ><span></span></div>
</div>
<table style="margin:0px;padding:0px;border:none;width:100%" cellpadding="0" cellspacing="0">
<tr>
<td valign="top">

	<table class="table" id="dataTable" cellpadding="0" cellspacing="0" style="width:100%;margin:0px;vertical-align:top;">
		<tr > 
		<td  class="tableHeaderTable2"><b>Crew</b></td>
		<td style="white-space:nowrap;padding:0px !important;border-right:none !important;"></td>
			<c:forEach var="crewTime" items="${crewTime}">
	     		<td  class="tableHeaderTable2"><c:out value="${crewTime}" /></td>
	    	</c:forEach>
		</tr>
		
		<!-- Code Commented by Ankit for 13835 Start -->
		<%			
				Map <String, String> crewCalanderList = (Map <String, String>)request.getAttribute("crewCalanderList");	
					if(crewCalanderList!=null && !crewCalanderList.isEmpty()){
				Iterator dateIterator = crewCalanderList.entrySet().iterator(); 
				String crewName1="";
				while (dateIterator.hasNext()) {
					Map.Entry entry = (Map.Entry) dateIterator.next();
					String keyData = (String) entry.getKey();
					String crewName=keyData.split("~")[0]; 
					String beginHour=keyData.split("~")[1];
					beginHour=beginHour.substring(0, beginHour.indexOf(":")); 
					String endHour=keyData.split("~")[2];
					endHour=endHour.substring(0, endHour.indexOf(":")); 
					beginHour=beginHour.trim();
					endHour=endHour.trim();
					int calspanValue=1;
					int calspanValue1=1;
					int a= Integer.parseInt(beginHour);
					int b= Integer.parseInt(endHour);
					calspanValue1=(b-a);
					if(beginHour.trim().equals("06")){
						
					}
					if(beginHour.trim().equals("07")){
						calspanValue=2;
						
					}
					if(beginHour.trim().equals("08")){
						calspanValue=3;
						
					}
					if(beginHour.trim().equals("09")){
						calspanValue=4;
						
					}
					if(beginHour.trim().equals("10")){
						calspanValue=5;
						
					}
					if(beginHour.trim().equals("11")){
						calspanValue=6;
						
					}
					if(beginHour.trim().equals("12")){
						calspanValue=7;
						
					}
					if(beginHour.trim().equals("13")){
						calspanValue=8;
						
					}
					if(beginHour.trim().equals("14")){
						calspanValue=9;
						
					}
					if(beginHour.trim().equals("15")){
						calspanValue=10;
						
					}
					if(beginHour.trim().equals("16")){
						calspanValue=11;
						
					}
					if(beginHour.trim().equals("17")){
						calspanValue=12;
						
					} 
					if(beginHour.trim().equals("18")){
						calspanValue=13;
						
					} 
					if(beginHour.trim().equals("19")){
						calspanValue=14;
						
					} 
					int abc=15-(calspanValue+calspanValue1);
					String dateByValue = (String) entry.getValue();
					
					dateByValue=dateByValue.replace("[", "");
					dateByValue=dateByValue.replace("]", "");
					String dateByValueDisplay=dateByValue.substring(0, dateByValue.indexOf(",ticketId"));
					String ticketId=dateByValue.substring(dateByValue.indexOf(",ticketId")+10);
					
				%>
				<tr>
				<%
				if(!(crewName.equalsIgnoreCase(crewName1))) {	%>
				<td class="listwhitetext" style="background:#CCCCCC;" width="100px"><b><%=crewName%></b></td>	
				<%} else {%>
				<td class="listwhitetext" style="background:#CCCCCC;"></td>
				<%}%>	
				<td colspan="<%=calspanValue%>" style="white-space:nowrap;padding:0px !important;background:#CCCCCC;border-right:none !important;"></td> 
				<td class="listwhitetext" style="background: none repeat scroll 0px 0px green; color: white;" colspan="<%=calspanValue1%>" onclick="openWorkTicket('<%=ticketId%>')"><b><%=dateByValueDisplay%></b></td> 
					<%
					if (abc!=0){%>
						<td  style="background:#CCCCCC;" colspan="<%=abc%>"></td>
					<%}else{%>
					
					<%}%>
					</tr>
									
			<% crewName1 =crewName ;}%>
			<%}%> 
			<!-- Code Commented by Ankit for 13835 End-->
			
		</table>
		</td>
		</tr>
		</table>	

</s:form>
<script type="text/javascript"> 
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
	try{  
		<c:if test="${empty crewCalendarDate}">
  			document.forms['crewCalenderForm'].elements['crewCalendarDate'].value = '${crewCalTempDate}';   
  		</c:if>
  		<c:if test="${not empty crewCalendarDate}">
  			document.forms['crewCalenderForm'].elements['crewCalendarDate'].value = document.forms['crewCalenderForm'].elements['crewCalendarDate'].value;
  		</c:if>
  	}catch(e){}	
</script>

  