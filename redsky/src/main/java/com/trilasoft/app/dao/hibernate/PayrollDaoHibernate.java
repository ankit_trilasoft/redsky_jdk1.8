//---Created By Bibhash----

package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;
import com.trilasoft.app.dao.PayrollDao;
import com.trilasoft.app.dao.hibernate.PayrollDaoHibernate.GenralDTO;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.Payroll;
 
public class PayrollDaoHibernate extends GenericDaoHibernate<Payroll, Long> implements PayrollDao {
	private HibernateUtil hibernateUtil;
	private List payrolls;

	public PayrollDaoHibernate() {
		super(Payroll.class);
	}

	public List findById(Long id) {
		return getHibernateTemplate().find("select id from Payroll where id=?", id);
	}
	
	public List findMaximumId() {
    	return getHibernateTemplate().find("select max(id) from Payroll");
    }
	

	static String replaceWord(String original, String find, String replacement) {
		int i = original.indexOf(find);
		if (i < 0) {
			return original; // return original if 'find' is not in it.
		}
		String partBefore = original.substring(0, i);
		String partAfter = original.substring(i + find.length());
		return partBefore + replacement + partAfter;
	}

	public List<Payroll> searchPayroll(String firstName, String lastName, String warehouse, String active, String userName,String union,String sessionCorpID) {
		if(union.equalsIgnoreCase("true")){
			if(!warehouse.equalsIgnoreCase(""))
			{
				return getHibernateTemplate().find( "from Payroll where firstName like '" + replaceWord(firstName, "'", "''") + "%' AND lastName like '" + replaceWord(lastName, "'", "''") + "%'   AND warehouse = '" + replaceWord(warehouse, "'", "''") + "'  AND active like '" + active + "%' AND userName like '" + replaceWord(userName, "'", "''") + "%' and corpID='"+sessionCorpID+"' and unionName<>null");
			}
			else
			{
				return getHibernateTemplate().find( "from Payroll where firstName like '" + replaceWord(firstName, "'", "''") + "%' AND lastName like '" + replaceWord(lastName, "'", "''") + "%'   AND warehouse like '" + replaceWord(warehouse, "'", "''") + "%'  AND active like '" + active + "%' AND userName like '" + replaceWord(userName, "'", "''") + "%' and corpID='"+sessionCorpID+"' and unionName<>null");
			}
		}else{
			if(!warehouse.equalsIgnoreCase(""))
			{
				return getHibernateTemplate().find( "from Payroll where firstName like '" + replaceWord(firstName, "'", "''") + "%' AND lastName like '" + replaceWord(lastName, "'", "''") + "%'   AND warehouse = '" + replaceWord(warehouse, "'", "''") + "'  AND active like '" + active + "%' AND userName like '" + replaceWord(userName, "'", "''") + "%' and corpID='"+sessionCorpID+"' ");
			}
			else
			{
				return getHibernateTemplate().find( "from Payroll where firstName like '" + replaceWord(firstName, "'", "''") + "%' AND lastName like '" + replaceWord(lastName, "'", "''") + "%'   AND warehouse like '" + replaceWord(warehouse, "'", "''") + "%'  AND active like '" + active + "%' AND userName like '" + replaceWord(userName, "'", "''") + "%' and corpID='"+sessionCorpID+"' ");
			}
		}
	}

	public List checkUserName(String userName) {
		if(userName!=null){
			userName = userName.replaceAll("'", "''");
		}
		return getHibernateTemplate().find("from Payroll where userName='"+userName+"'");
	}

	public Map<String, String> getCompnayDevisionCode(String corpId) {
		List<CompanyDivision> list;
		list= getHibernateTemplate().find("from CompanyDivision where corpID='"+corpId+"'");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
	   	for(CompanyDivision companyDivision : list){
	   		parameterMap.put(companyDivision.getCompanyCode(), companyDivision.getDescription());
			}
	   	return  parameterMap;
	}
	public  class DTOPayrollExtract
	{
		private Object CoCode;
		private Object BatchID;
		private Object fileNo;		
		private Object RegHours;
		private Object OTHours;
		private Object Earnings3Code;
		private Object Earnings3Amount;
		private Object Hours3Code;
		private Object Hours3Amount;
		private Object RateCode;
		private Object pay;
		private Object taxFrequency;		
		private Object regEarnings;
		private Object OTEarnings;		
		private Object dot;
		private Object doubleOverTime;
		private Object paidRevenueShare;
		private Object specialAction;
		private Object Employee;
		
		public Object getBatchID() {
			return BatchID;
		}
		public void setBatchID(Object batchID) {
			BatchID = batchID;
		}
		public Object getCoCode() {
			return CoCode;
		}
		public void setCoCode(Object coCode) {
			CoCode = coCode;
		}
		public Object getEarnings3Amount() {
			return Earnings3Amount;
		}
		public void setEarnings3Amount(Object earnings3Amount) {
			Earnings3Amount = earnings3Amount;
		}
		public Object getEarnings3Code() {
			return Earnings3Code;
		}
		public void setEarnings3Code(Object earnings3Code) {
			Earnings3Code = earnings3Code;
		}
		public Object getFileNo() {
			return fileNo;
		}
		public void setFileNo(Object fileNo) {
			this.fileNo = fileNo;
		}
		public Object getHours3Amount() {
			return Hours3Amount;
		}
		public void setHours3Amount(Object hours3Amount) {
			Hours3Amount = hours3Amount;
		}
		public Object getHours3Code() {
			return Hours3Code;
		}
		public void setHours3Code(Object hours3Code) {
			Hours3Code = hours3Code;
		}
		public Object getOTHours() {
			return OTHours;
		}
		public void setOTHours(Object hours) {
			OTHours = hours;
		}
		public Object getRateCode() {
			return RateCode;
		}
		public void setRateCode(Object rateCode) {
			RateCode = rateCode;
		}
		public Object getRegHours() {
			return RegHours;
		}
		public void setRegHours(Object regHours) {
			RegHours = regHours;
		}		
		public Object getPay() {
			return pay;
		}
		public void setPay(Object pay) {
			this.pay = pay;
		}
		public Object getTaxFrequency() {
			return taxFrequency;
		}
		public void setTaxFrequency(Object taxFrequency) {
			this.taxFrequency = taxFrequency;
		}		
		public Object getRegEarnings() {
			return regEarnings;
		}
		public void setRegEarnings(Object regEarnings) {
			this.regEarnings = regEarnings;
		}
		public Object getOTEarnings() {
			return OTEarnings;
		}
		public void setOTEarnings(Object oTEarnings) {
			OTEarnings = oTEarnings;
		}
		
		public Object getDot() {
			return dot;
		}
		public void setDot(Object dot) {
			this.dot = dot;
		}
		public Object getDoubleOverTime() {
			return doubleOverTime;
		}
		public void setDoubleOverTime(Object doubleOverTime) {
			this.doubleOverTime = doubleOverTime;
		}
		public Object getPaidRevenueShare() {
			return paidRevenueShare;
		}
		public void setPaidRevenueShare(Object paidRevenueShare) {
			this.paidRevenueShare = paidRevenueShare;
		}
		public Object getSpecialAction() {
			return specialAction;
		}
		public void setSpecialAction(Object specialAction) {
			this.specialAction = specialAction;
		}
		
		public Object getEmployee() {
			return Employee;
		}
		public void setEmployee(Object Employee) {
			this.Employee = Employee;
		}
		
	} 
	
	public class DTOPayrollWorkDayExtract
	{
		private Object Id;
		private Object employeeId;
		private Object firstName;		
		private Object lastName;
		private Object custFirst;
		private Object custLast;
		private Object orderNum;
		private Object msPriKey;
		private Object serviceDate;
		private Object serviceId;
		private Object jobId;
		private Object jobDescription;		
		private Object punchTime;
		private Object punchType;		
		private Object punchDescription;
		private Object timeZone;
		private Object region;
		private Object jobStart;
		private Object jobEnd;
		private Object estJobStart;		
		private Object estJobEnd;
		private Object roundedStart;		
		private Object roundedEnd;
		private Object lsPriKey;
		private Object active;
		private Object punchInID;
		private Object CTAPConfirmed;
		private Object MVPConfirmed;		
		private Object comments;
		private Object systemDate;		
		private Object WDPunchResponse;
		private Object laborType;
		private Object MSPunchResponse;
		private Object branch;
		private Object punchOutTime;
		public Object getId() {
			return Id;
		}
		public void setId(Object id) {
			Id = id;
		}
		public Object getEmployeeId() {
			return employeeId;
		}
		public void setEmployeeId(Object employeeId) {
			this.employeeId = employeeId;
		}
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getCustFirst() {
			return custFirst;
		}
		public void setCustFirst(Object custFirst) {
			this.custFirst = custFirst;
		}
		public Object getCustLast() {
			return custLast;
		}
		public void setCustLast(Object custLast) {
			this.custLast = custLast;
		}
		public Object getOrderNum() {
			return orderNum;
		}
		public void setOrderNum(Object orderNum) {
			this.orderNum = orderNum;
		}
		public Object getMsPriKey() {
			return msPriKey;
		}
		public void setMsPriKey(Object msPriKey) {
			this.msPriKey = msPriKey;
		}
		public Object getServiceDate() {
			return serviceDate;
		}
		public void setServiceDate(Object serviceDate) {
			this.serviceDate = serviceDate;
		}
		public Object getServiceId() {
			return serviceId;
		}
		public void setServiceId(Object serviceId) {
			this.serviceId = serviceId;
		}
		public Object getJobId() {
			return jobId;
		}
		public void setJobId(Object jobId) {
			this.jobId = jobId;
		}
		public Object getJobDescription() {
			return jobDescription;
		}
		public void setJobDescription(Object jobDescription) {
			this.jobDescription = jobDescription;
		}
		public Object getPunchTime() {
			return punchTime;
		}
		public void setPunchTime(Object punchTime) {
			this.punchTime = punchTime;
		}
		public Object getPunchType() {
			return punchType;
		}
		public void setPunchType(Object punchType) {
			this.punchType = punchType;
		}
		public Object getPunchDescription() {
			return punchDescription;
		}
		public void setPunchDescription(Object punchDescription) {
			this.punchDescription = punchDescription;
		}
		public Object getTimeZone() {
			return timeZone;
		}
		public void setTimeZone(Object timeZone) {
			this.timeZone = timeZone;
		}
		public Object getRegion() {
			return region;
		}
		public void setRegion(Object region) {
			this.region = region;
		}
		public Object getJobStart() {
			return jobStart;
		}
		public void setJobStart(Object jobStart) {
			this.jobStart = jobStart;
		}
		public Object getJobEnd() {
			return jobEnd;
		}
		public void setJobEnd(Object jobEnd) {
			this.jobEnd = jobEnd;
		}
		public Object getEstJobStart() {
			return estJobStart;
		}
		public void setEstJobStart(Object estJobStart) {
			this.estJobStart = estJobStart;
		}
		public Object getEstJobEnd() {
			return estJobEnd;
		}
		public void setEstJobEnd(Object estJobEnd) {
			this.estJobEnd = estJobEnd;
		}
		public Object getRoundedStart() {
			return roundedStart;
		}
		public void setRoundedStart(Object roundedStart) {
			this.roundedStart = roundedStart;
		}
		public Object getRoundedEnd() {
			return roundedEnd;
		}
		public void setRoundedEnd(Object roundedEnd) {
			this.roundedEnd = roundedEnd;
		}
		public Object getLsPriKey() {
			return lsPriKey;
		}
		public void setLsPriKey(Object lsPriKey) {
			this.lsPriKey = lsPriKey;
		}
		public Object getActive() {
			return active;
		}
		public void setActive(Object active) {
			this.active = active;
		}
		public Object getPunchInID() {
			return punchInID;
		}
		public void setPunchInID(Object punchInID) {
			this.punchInID = punchInID;
		}
		public Object getCTAPConfirmed() {
			return CTAPConfirmed;
		}
		public void setCTAPConfirmed(Object cTAPConfirmed) {
			CTAPConfirmed = cTAPConfirmed;
		}
		public Object getMVPConfirmed() {
			return MVPConfirmed;
		}
		public void setMVPConfirmed(Object mVPConfirmed) {
			MVPConfirmed = mVPConfirmed;
		}
		public Object getComments() {
			return comments;
		}
		public void setComments(Object comments) {
			this.comments = comments;
		}
		public Object getSystemDate() {
			return systemDate;
		}
		public void setSystemDate(Object systemDate) {
			this.systemDate = systemDate;
		}
		public Object getWDPunchResponse() {
			return WDPunchResponse;
		}
		public void setWDPunchResponse(Object wDPunchResponse) {
			WDPunchResponse = wDPunchResponse;
		}
		public Object getLaborType() {
			return laborType;
		}
		public void setLaborType(Object laborType) {
			this.laborType = laborType;
		}
		public Object getMSPunchResponse() {
			return MSPunchResponse;
		}
		public void setMSPunchResponse(Object mSPunchResponse) {
			MSPunchResponse = mSPunchResponse;
		}
		public Object getBranch() {
			return branch;
		}
		public void setBranch(Object branch) {
			this.branch = branch;
		}
		public Object getPunchOutTime() {
			return punchOutTime;
		}
		public void setPunchOutTime(Object punchOutTime) {
			this.punchOutTime = punchOutTime;
		}
		} 
	
	
	
	public List findsClaimsExtracts(String query,String corpId,String recievedFormCheckFlag){
		String column ="";
		//String column = "select s.shipnumber,c.claimnumber,c.firstname as 'First Name',c.lastname as 'Last Name' , s.job, b.billtocode, b.billtoname, date_format(c.createdon,'%Y-%m-%d') 'opened', date_format(c.closedate,'%Y-%m-%d') 'closed', date_format( c.cancelled,'%Y-%m-%d') 'cancelled', c.reimbursement 'RequestedAmt', c.recivereimbursement 'ReceievedAmt', ifnull(l.paidcompensationcustomer,0.00) 'Paidshipper', ifnull(l.paidCompensation3Party, 0.00) 'Paid3rdparty', l.warehouse, l.lossaction,  l.lossType,l.damageByAction,l.chargeBackTo,l.chargeBackAmount,l.itemDescription,l.lossComment, (ifnull(l.paidcompensationcustomer,0.00) + ifnull(l.paidCompensation3Party, 0.00)) as 'Total Paid', c.reimbursement as 'Reimbursement', c.recivereimbursement as 'Received Reimbursement', c.uvlamount as 'UVL Amount',date_format(c.requestreimbursement,'%Y-%m-%d') as 'Reimbursement Requested',date_format(c.insureracknowledgement,'%Y-%m-%d') as 'Reimbursement Received',date_format(c.uvlrecived,'%Y-%m-%d') as 'Settlement',s.routing ,s.companyDivision, cu.accountName, m.millitaryShipment from customerfile cu ,miscellaneous m , claim c,loss l,serviceorder s, billing b where c.claimnumber = l.claimnumber and c.serviceorderid = s.id and s.id = b.id and l.lossaction not in ('C','W') and s.corpid='"+corpId+"' and  l.corpid='"+corpId+"' and c.corpid='"+corpId+"' and b.corpid='"+corpId+"' and cu.id=s.customerFileId and m.id=s.id  " ;		
		if(recievedFormCheckFlag==null || recievedFormCheckFlag.equalsIgnoreCase("")){
			column = "select s.shipnumber,c.claimnumber,c.firstname as 'First Name',c.lastname as 'Last Name' , s.job, b.billtocode, b.billtoname, date_format(c.createdon,'%Y-%m-%d') 'opened', date_format(c.closedate,'%Y-%m-%d') 'closed', date_format( c.cancelled,'%Y-%m-%d') 'cancelled',c.reimbursementCurrency 'Currency ', c.reimbursement 'RequestedAmt', c.recivereimbursement 'ReceievedAmt', ifnull(l.paidcompensationcustomer,0.00) 'Paidshipper', ifnull(l.paidCompensation3Party, 0.00) 'Paid3rdparty', l.warehouse, l.lossaction,  r.description,l.damageByAction,l.chargeBackTo,l.chargeBackAmount,l.itemDescription,l.lossComment, (ifnull(l.paidcompensationcustomer,0.00) + ifnull(l.paidCompensation3Party, 0.00)) as 'Total Paid', c.reimbursement as 'Reimbursement', c.recivereimbursement as 'Received Reimbursement', c.uvlamount as 'UVL Amount',date_format(c.requestreimbursement,'%Y-%m-%d') as 'Reimbursement Requested',date_format(c.insureracknowledgement,'%Y-%m-%d') as 'Reimbursement Received',date_format(c.uvlrecived,'%Y-%m-%d') as 'Settlement',s.routing ,s.companyDivision, cu.accountName, m.millitaryShipment, s.bookingAgentCode,s.bookingAgentName,t.originAgentCode, t.originAgent, t.destinationAgentCode, t.destinationAgent, t.originSubAgentCode, t.originSubAgent, t.destinationSubAgentCode, t.destinationSubAgent, l.selfBlame,c.claimPerson,date_format(c.requestForm,'%Y-%m-%d'),date_format(c.formSent,'%Y-%m-%d'),date_format(c.formRecived,'%Y-%m-%d'),c.propertyDamageAmount,c.customerSvcAmount,c.lostDamageAmount,c.totalPaidToCustomerAmount,c.insurerCode,c.insurer from customerfile cu inner join serviceorder s on cu.id = s.customerfileid and s.corpid='"+corpId+"' inner join miscellaneous m on m.id=s.id and m.corpid='"+corpId+"' inner join trackingstatus t on t.id=s.id and t.corpid='"+corpId+"' inner join billing b on s.id = b.id and b.corpid='"+corpId+"' inner join claim c on c.serviceorderid = s.id and c.corpid='"+corpId+"' inner join loss l on c.claimnumber = l.claimnumber and  l.corpid='"+corpId+"' left outer join refmaster r on r.code=l.lossType and r.corpid in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"') and r.parameter ='LOSS' where  ";
		}else{
			column = "select s.shipnumber,c.claimnumber,c.firstname as 'First Name',c.lastname as 'Last Name' , s.job, b.billtocode, b.billtoname, date_format(c.formRecived,'%Y-%m-%d') 'opened', date_format(c.closedate,'%Y-%m-%d') 'closed', date_format( c.cancelled,'%Y-%m-%d') 'cancelled',c.reimbursementCurrency 'Currency ', c.reimbursement 'RequestedAmt', c.recivereimbursement 'ReceievedAmt', ifnull(l.paidcompensationcustomer,0.00) 'Paidshipper', ifnull(l.paidCompensation3Party, 0.00) 'Paid3rdparty', l.warehouse, l.lossaction,  r.description,l.damageByAction,l.chargeBackTo,l.chargeBackAmount,l.itemDescription,l.lossComment, (ifnull(l.paidcompensationcustomer,0.00) + ifnull(l.paidCompensation3Party, 0.00)) as 'Total Paid', c.reimbursement as 'Reimbursement', c.recivereimbursement as 'Received Reimbursement', c.uvlamount as 'UVL Amount',date_format(c.requestreimbursement,'%Y-%m-%d') as 'Reimbursement Requested',date_format(c.insureracknowledgement,'%Y-%m-%d') as 'Reimbursement Received',date_format(c.uvlrecived,'%Y-%m-%d') as 'Settlement',s.routing ,s.companyDivision, cu.accountName, m.millitaryShipment, s.bookingAgentCode,s.bookingAgentName,t.originAgentCode, t.originAgent, t.destinationAgentCode, t.destinationAgent, t.originSubAgentCode, t.originSubAgent, t.destinationSubAgentCode, t.destinationSubAgent, l.selfBlame,c.claimPerson,date_format(c.requestForm,'%Y-%m-%d'),date_format(c.formSent,'%Y-%m-%d'),date_format(c.formRecived,'%Y-%m-%d'),c.propertyDamageAmount,c.customerSvcAmount,c.lostDamageAmount,c.totalPaidToCustomerAmount,c.insurerCode,c.insurer from customerfile cu inner join serviceorder s on cu.id = s.customerfileid and s.corpid='"+corpId+"' inner join miscellaneous m on m.id=s.id and m.corpid='"+corpId+"' inner join trackingstatus t on t.id=s.id and t.corpid='"+corpId+"' inner join billing b on s.id = b.id and b.corpid='"+corpId+"' inner join claim c on c.serviceorderid = s.id and c.corpid='"+corpId+"' left outer join loss l on c.claimnumber = l.claimnumber and  l.corpid='"+corpId+"' and l.lossaction not in ('C','W') left outer join refmaster r on r.code=l.lossType and r.corpid in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"') and r.parameter ='LOSS' where  ";
		}
		String order="order by c.claimnumber"; 
		List listAvailCrew= this.getSession().createSQLQuery(column+" "+query+" "+order).list(); 
		return listAvailCrew;
	}
	

	public List findsComplaintsExtracts(String query,String corpId){ 
	   		
	    String column=" select concat(s.firstname,' ',s.lastname) as 'Name Client', s.shipnumber as 'UTS Job'," +
	    	"concat('(',s.origincountrycode,')',s.origincity,'->','(',s.destinationcountrycode,')',s.destinationcity) as 'From -> To'," +
	    	"n.supplier as 'Involved Member',date_format(t.deliveryA,'%Y-%m-%d') as 'Date Job Delivered'," +
	    	"if(n.notesubtype='Issue',date_format(n.createdon,'%Y-%m-%d'),'') as 'Date Received Complaint'," +
	    	"n.grading as 'Grading', n.issuetype as 'Type of Complaints', " +
	    	"trim(both ',' from group_concat(if(n.notesubtype='Issue',if(n.subject=null || n.subject='','',concat(n.subject,if(n.note=null || n.note='','',concat('/ ',n.note)))),''))) as 'Remarks'," +
	     	"case when n.notestatus='CMP' then 'Yes' else 'No' end as 'Solved?'," +
	     	"trim(both ',' from group_concat(if(n.notesubtype='Nature',if(n.subject=null || n.subject='','',concat(n.subject,if(n.note=null || n.note='','',concat('/ ',n.note)))),''))) as 'Follow Up UTS'," +
	     	"trim(both ',' from group_concat(if(n.notesubtype='Resolution',if(n.subject=null || n.subject='','',concat(n.subject,if(n.note=null || n.note='','',concat('/ ',n.note)))),''))) as 'Improvement Measure UTS'" +
	     	"from serviceorder s,trackingstatus t,notes n where s.id=t.id " +
	     	"and s.corpid='"+corpId+"' and t.corpid='"+corpId+"' and n.corpid='"+corpId+"' and n.grading is not null and n.grading!='' " +
	     	"and n.issuetype is not null and n.issuetype!='' and n.corpid ='"+corpId+"' ";
	    String group="group by n.grading, n.issuetype,s.shipnumber order by s.shipnumber";
	    
		List listComplaintsNotes= this.getSession().createSQLQuery(column+" "+query+" "+group).list(); 
		return listComplaintsNotes;
	}
	
	public List findsStorageExtracts(String warehouseS, String typeS, String corpID){
		String availQuery="";
		if(warehouseS.trim().equalsIgnoreCase("")){
			availQuery = "select left(l.locationid,2) yy , l.locationid xx, if ((l.capacity = '3' and st.shipnumber is not null),'X',l.occupied) AS Occupied, l.type, st.shipnumber, st.description,  st.itemtag, st.itemnumber, containerid,st.storageId,s.firstname as firstName,s.lastname as lastName , s.billtocode, st.createdon , mid(l.locationid, 6,3) cc, mid(l.locationid, 9,4)dd, b.insuranceValueActual, b.storageOut,s.job from location l left outer join storage st on st.locationid = l.locationid and st.releasedate is null left outer join serviceorder s on st.serviceorderid = s.id left outer join billing b on b.id = s.id and b.corpid = '"+corpID+"' where l.corpid = '"+corpID+"' and l.type like '"+typeS+"%'";
		}else{
			if(warehouseS.equalsIgnoreCase("0") || warehouseS.equalsIgnoreCase("1") || warehouseS.equalsIgnoreCase("2") || warehouseS.equalsIgnoreCase("3") || warehouseS.equalsIgnoreCase("4") || warehouseS.equalsIgnoreCase("5") || warehouseS.equalsIgnoreCase("6") || warehouseS.equalsIgnoreCase("7") || warehouseS.equalsIgnoreCase("8") || warehouseS.equalsIgnoreCase("9")){
				warehouseS = "0"+warehouseS;	
			}
			int lenghtW = warehouseS.length();
			availQuery = "select left(l.locationid,'"+lenghtW+"') yy , l.locationid xx, if ((l.capacity = '3' and st.shipnumber is not null),'X',l.occupied) AS Occupied, l.type, st.shipnumber, st.description,  st.itemtag, st.itemnumber, containerid,st.storageId,s.firstname as firstName,s.lastname as lastName, s.billtocode, st.createdon , mid(l.locationid, 6,3) cc, mid(l.locationid, 9,4)dd, b.insuranceValueActual, b.storageOut,s.job from location l left outer join storage st on st.locationid = l.locationid and st.releasedate is null left outer join serviceorder s on st.serviceorderid = s.id left outer join billing b on b.id = s.id and b.corpid = '"+corpID+"' where l.corpid = '"+corpID+"' and left(l.locationid,'"+lenghtW+"') = '"+warehouseS+"' and l.type like '"+typeS+"%'";
		}
		List listAvailCrew= this.getSession().createSQLQuery(availQuery)
		.addScalar("yy", Hibernate.STRING) 
		.addScalar("xx", Hibernate.STRING) 
		.addScalar("Occupied", Hibernate.STRING)
		.addScalar("l.type", Hibernate.STRING) 
		.addScalar("st.shipnumber", Hibernate.STRING)
		.addScalar("st.description", Hibernate.STRING)
		.addScalar("st.itemtag", Hibernate.STRING) 
		.addScalar("st.itemnumber", Hibernate.STRING) 
		.addScalar("containerid", Hibernate.STRING)
		.addScalar("st.storageId", Hibernate.STRING)
		.addScalar("firstName", Hibernate.STRING)
		.addScalar("lastName", Hibernate.STRING)
		.addScalar("s.billtocode", Hibernate.STRING) 
		.addScalar("st.createdon", Hibernate.DATE) 
		.addScalar("cc", Hibernate.STRING)
		.addScalar("dd", Hibernate.STRING) 
		.addScalar("b.insuranceValueActual", Hibernate.STRING)
		.addScalar("b.storageOut", Hibernate.DATE)
		.addScalar("s.job", Hibernate.STRING)
		.list();
		return listAvailCrew;
	}
	public List<GenralDTO> findPayrollExtractList(String beginDates, String endDates,String corpId, String hub) {
		List <Object> payrollExtract = new ArrayList<Object>();
		int taxFrequencyDay=0;
		List getDiff = this.getSession().createSQLQuery("SELECT DATEDIFF('"+endDates+"', '"+beginDates+"')").list();	
		Long diff = Long.parseLong(getDiff.get(0).toString());
		if(diff<=7){
			taxFrequencyDay=1;
		}else{
			taxFrequencyDay=2;
		}
		/*String query="SELECT ref2.flex1 as CoCode,crew.`warehouse` AS BatchID,crew.`employeeId` AS fileNo,CONCAT('0',timesheet.`distributionCode`,lpad(crew.`warehouse`,2,'0')) AS TempDept," +
				"IF(timesheet.`action` not IN('H','P','S','V','F') ,round(sum(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`regularHours`,1))),2),0) AS RegHours," +
				"round(sum(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`OverTime`,1))),2) AS OTHours," +
				"if(sum(IF((ifnull(timesheet.paidRevenueShare,0)+ifnull(timesheet.adjustmentToRevenue,0))<>0, (ifnull(timesheet.paidRevenueShare,0)+ifnull(timesheet.adjustmentToRevenue,0)),0))>0,'Z','') AS Earnings3Code," +
				"round(sum(IF((ifnull(timesheet.paidRevenueShare,0)+ifnull(timesheet.adjustmentToRevenue,0))<>0, (ifnull(timesheet.paidRevenueShare,0)+ifnull(timesheet.adjustmentToRevenue,0)),0)),2) AS Earnings3Amount," +
				"IF((IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`doubleOverTime`,1))) > 0,'D',IF(timesheet.`action` IN('H','P','S','V','F'),timesheet.`action`,if(timesheet.`action`='Z','F','') ) ) AS Hours3Code," +
				"round(sum(IF((IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`doubleOverTime`,1))) > 0,timesheet.`doubleOverTime`,IF(timesheet.`action` IN('H','P','S','V','F'),timesheet.`regularHours`,if(timesheet.`action`='Z',timesheet.`regularHours`,'')))))AS Hours3Amount," +
				"IF(timesheet.differentPayBasis='B',2,0) AS RateCode " +
				"FROM `crew` crew LEFT OUTER JOIN `timesheet` timesheet ON crew.`userName` = timesheet.`userName` LEFT OUTER JOIN `workticket` workticket ON timesheet.`ticket` = workticket.`ticket` LEFT OUTER JOIN `payroll` payroll ON timesheet.`distributioncode` = payroll.`code` and  payroll.corpID='"+corpId+"' " +
				"INNER JOIN refmaster ref ON crew.warehouse = ref.code and ref.parameter = 'HOUSE' and ref.corpid in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"') and ref.flex1 like '%"+hub+"%' " +
                "INNER JOIN refmaster ref2 ON ref2.corpid in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"') and ref2.parameter = 'OPSHUB' and ref2.code = ref.flex1 " +
				"WHERE date_format(timesheet.workDate,'%Y-%m-%d') >= '"+beginDates+"' AND date_format(timesheet.workDate,'%Y-%m-%d') <= '"+endDates+"' AND timesheet.`corpID` = '"+corpId+"' AND timesheet.`distributionCode` <> '999' and timesheet.action <> 'L' and (workticket.corpid = '"+corpId+"' or workticket.corpid is null) " +
				"group by crew.`warehouse`, crew.`lastName`, timesheet.`userName`, CONCAT(timesheet.`distributionCode`,workticket.`warehouse`,timesheet.`differentPayBasis`), timesheet.distributioncode " +
				"order by crew.`warehouse`, crew.`lastName`, timesheet.`userName`, CONCAT(timesheet.`distributionCode`,workticket.`warehouse`,timesheet.`differentPayBasis`), timesheet.distributioncode";*/
		String query="";	
		if(hub.equalsIgnoreCase("MR3")){
		 query="SELECT ref.flex5 as CoCode," +
				"1 AS BatchID," + 
				" crew.`employeeId` AS File," +
				" 'X' As specialAction," +
				" 1 AS Pay," +
				" "+taxFrequencyDay+" AS TaxFrequency," +				
				" IF(timesheet.`action` not IN('H','P','S','V','BE') ,round(sum(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`regularHours`,1))),2),'0') AS regularHours, " +
				" round(sum(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`OverTime`,1))),2) AS overTime,"+
				
				/*" if(sum(IF((timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`)<>0,(timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`),0))>0,'RS', " +
				" IF(sum(IF((timesheet.doubleOverTime is null or timesheet.doubleOverTime=''),'0',timesheet.doubleOverTime)) > 0,'DT', " +
				" IF(timesheet.`action` IN('H','P','S','V','BE'),timesheet.`action`,if(timesheet.`action`=' RS ','BE','') ) ))AS Hours3Code," +*/
				
				" if(sum(IF((timesheet.`paidRevenueShare`)<>0,(timesheet.`paidRevenueShare`),0))>0,'RS', '') AS Hours3Code," +

/*" round(sum(IF(timesheet.`action` IN('H','P','S','V','BE'),timesheet.`regularHours`,IF(timesheet.`doubleOverTime` > 0,timesheet.`doubleOverTime`," +
" IF(timesheet.`paidRevenueShare`>0,ROUND(timesheet.`regularHours`,1)+ROUND(timesheet.`overTime`,1)+ROUND(timesheet.`doubleOverTime`,1),0)))),2)AS Hours3Amount," +*/
				
				" round(sum(IF(timesheet.`paidRevenueShare`>0,ROUND(timesheet.`regularHours`,1),'0')),2) AS Hours3Amount," +
				
				" (IF(timesheet.`action` not IN('H','P','S','V','BE') ,round(sum(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`regularHours`,1))),2),'0')*if(timesheet.regularPayPerHour is null or timesheet.regularPayPerHour='','0',timesheet.regularPayPerHour)) AS RegEarnings," +
				" (round(sum(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`OverTime`,1))),2)*if(timesheet.overTimePayPerHour is null or timesheet.overTimePayPerHour='','0',timesheet.overTimePayPerHour)) As OTEarnings," +
				
				/*" if(sum(IF((timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`)<>0,(timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`),0))>0,'RS', " +
				" IF(sum(IF((timesheet.doubleOverTime is null or timesheet.doubleOverTime=''),'0',timesheet.doubleOverTime)) > 0,'DT', " +
				" IF(timesheet.`action` IN('H','P','S','V','BE'),timesheet.`action`,if(timesheet.`action`=' RS ','BE','') ) ))AS Earnings3Code," +*/
				
" if(sum(IF((timesheet.`paidRevenueShare`)<>0,(timesheet.`paidRevenueShare`),0))>0,'RS','') AS Earnings3Code," +
				
				/*" round(if(timesheet.`action` IN('H','P','S','V','BE'),(sum(timesheet.regularHours)*if(timesheet.regularPayPerHour is null or timesheet.regularPayPerHour='','0',timesheet.regularPayPerHour))," +
				" if(timesheet.doubleOverTime<>0.00,(timesheet.doubleOverTime*if(timesheet.doubleOverTimePayPerHour is null or timesheet.doubleOverTimePayPerHour='','0.00',timesheet.doubleOverTimePayPerHour)), round(sum(IF((timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`)<>0, "+
				" (timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`),0)),2))),2) AS Earnings3Amount,"+*/
				
" round(if(timesheet.`action` IN('H','P','S','V','BE'),(sum(timesheet.regularHours)*if(timesheet.regularPayPerHour is null or timesheet.regularPayPerHour='','0',timesheet.regularPayPerHour))," +
" round(sum(IF((timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`)<>0, "+
" (timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`),0)),2)),2) AS Earnings3Amount,"+

				
				" concat(if(crew.`lastname` is null,'',trim(crew.`lastname`)),', ',if(crew.`firstname` is null,'',trim(crew.`firstname`))) AS Employee"+
				
				" FROM `crew` crew LEFT OUTER JOIN `timesheet` timesheet ON crew.`userName` = timesheet.`userName` LEFT OUTER JOIN `workticket` workticket ON timesheet.`ticket` = workticket.`ticket` LEFT OUTER JOIN `payroll` payroll ON timesheet.`distributioncode` = payroll.`code` and  payroll.corpID='"+corpId+"' " +
				" INNER JOIN refmaster ref ON crew.warehouse = ref.code and ref.parameter = 'HOUSE' and ref.corpid = '"+corpId+"' and ref.flex5 like '%"+hub+"%' " +
				" WHERE date_format(timesheet.workDate,'%Y-%m-%d') >= '"+beginDates+"' AND date_format(timesheet.workDate,'%Y-%m-%d') <= '"+endDates+"' AND timesheet.`corpID` = '"+corpId+"' AND timesheet.`distributionCode` <> '999' and timesheet.action <> 'L' and (workticket.corpid = '"+corpId+"' or workticket.corpid is null) " +
				" group by crew.`employeeId`, timesheet.`distributionCode`,timesheet.`action`,timesheet.`ticket` " +
				" order by crew.employeeId ";
			}else{/*
				 query="SELECT ref.flex5 as CoCode," +
				 "1 AS BatchID," + 
					" crew.`employeeId` AS File," +
					" 'X' As specialAction," +
					" 1 AS Pay," +
					" "+taxFrequencyDay+" AS TaxFrequency," +					
					" IF(timesheet.`action` not IN('H','P','S','V','BE') ,round(sum(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`regularHours`,1))),2),'0') AS regularHours, " +
					" round(sum(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`OverTime`,1))),2) AS overTime,"+
					" if(sum(IF((timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`)<>0,(timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`),0))>0,'RS', " +
					" IF(sum(IF((timesheet.doubleOverTime is null or timesheet.doubleOverTime=''),'0',timesheet.doubleOverTime)) > 0,'DT', " +
					" IF(timesheet.`action` IN('H','P','S','V','BE'),timesheet.`action`,if(timesheet.`action`=' RS ','BE','') ) ))AS Hours3Code," +
					" round(sum(IF(timesheet.`action` IN('H','P','S','V','BE'),timesheet.`regularHours`,IF(timesheet.`doubleOverTime` > 0,timesheet.`doubleOverTime`," +
					" IF(timesheet.`paidRevenueShare`>0,ROUND(timesheet.`regularHours`,1)+ROUND(timesheet.`overTime`,1)+ROUND(timesheet.`doubleOverTime`,1),0)))),2)AS Hours3Amount," +
					" (IF(timesheet.`action` not IN('H','P','S','V','BE') ,round(sum(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`regularHours`,1))),2),'0')*if(timesheet.regularPayPerHour is null or timesheet.regularPayPerHour='','0',timesheet.regularPayPerHour)) AS RegEarnings," +
					" (round(sum(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`OverTime`,1))),2)*if(timesheet.overTimePayPerHour is null or timesheet.overTimePayPerHour='','0',timesheet.overTimePayPerHour)) As OTEarnings," +
					" if(sum(IF((timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`)<>0,(timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`),0))>0,'RS', " +
					" IF(sum(IF((timesheet.doubleOverTime is null or timesheet.doubleOverTime=''),'0',timesheet.doubleOverTime)) > 0,'DT', " +
					" IF(timesheet.`action` IN('H','P','S','V','BE'),timesheet.`action`,if(timesheet.`action`=' RS ','BE','') ) ))AS Earnings3Code," +
					" round(if(timesheet.`action` IN('H','P','S','V','BE'),(sum(timesheet.regularHours)*if(timesheet.regularPayPerHour is null or timesheet.regularPayPerHour='','0',timesheet.regularPayPerHour))," +
					" if(timesheet.doubleOverTime<>0.00,(timesheet.doubleOverTime*if(timesheet.doubleOverTimePayPerHour is null or timesheet.doubleOverTimePayPerHour='','0.00',timesheet.doubleOverTimePayPerHour)), round(sum(IF((timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`)<>0, "+
					" (timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`),0)),2))),2) AS Earnings3Amount,"+
					" concat(if(crew.`lastname` is null,'',crew.`lastname`),' ',if(crew.`firstname` is null,'',crew.`firstname`)) AS Employee"+
					
					" FROM `crew` crew LEFT OUTER JOIN `timesheet` timesheet ON crew.`userName` = timesheet.`userName` LEFT OUTER JOIN `workticket` workticket ON timesheet.`ticket` = workticket.`ticket` LEFT OUTER JOIN `payroll` payroll ON timesheet.`distributioncode` = payroll.`code` and  payroll.corpID='"+corpId+"' " +
					" INNER JOIN refmaster ref ON crew.warehouse = ref.code and ref.parameter = 'HOUSE' and ref.corpid = '"+corpId+"' and ref.flex5 like '%"+hub+"%' " +
					" WHERE date_format(timesheet.workDate,'%Y-%m-%d') >= '"+beginDates+"' AND date_format(timesheet.workDate,'%Y-%m-%d') <= '"+endDates+"' AND timesheet.`corpID` = '"+corpId+"' AND timesheet.`distributionCode` <> '999' and crew.`employeeId`<>'' and timesheet.action <> 'L' and (workticket.corpid = '"+corpId+"' or workticket.corpid is null) " +
					" group by crew.`employeeId`, timesheet.`distributionCode`,timesheet.`action`,timesheet.`ticket` " +
					" order by crew.employeeId ";
			*/}
		
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n"+hub+">>>>>>>>>>>>>>>>>>>>>"+query+"\n\n\n\n\n\n\n\n\n\n");
		
		List payrollExtractList= this.getSession().createSQLQuery(query).list(); 
		Iterator iterator=payrollExtractList.iterator();	
		while(iterator.hasNext())
		{
			Object []row= (Object [])iterator.next();
			DTOPayrollExtract dtoPayrollExtract = new DTOPayrollExtract();
			dtoPayrollExtract.setCoCode((row[0]));
			dtoPayrollExtract.setBatchID((row[1]));
			dtoPayrollExtract.setFileNo((row[2]));
			dtoPayrollExtract.setSpecialAction((row[3]));
			dtoPayrollExtract.setPay((row[4]));
			dtoPayrollExtract.setTaxFrequency((row[5]));			
			dtoPayrollExtract.setRegHours((row[6]));
			dtoPayrollExtract.setOTHours((row[7]));
			dtoPayrollExtract.setHours3Code((row[8]));
			dtoPayrollExtract.setHours3Amount((row[9]));
			dtoPayrollExtract.setRegEarnings((row[10]));
			dtoPayrollExtract.setOTEarnings((row[11]));			
			dtoPayrollExtract.setEarnings3Code((row[12]));
			dtoPayrollExtract.setEarnings3Amount((row[13]));
			dtoPayrollExtract.setEmployee((row[14]));
			
			payrollExtract.add(dtoPayrollExtract);			
		}
		Map<String, DTOFile> masterDtoFile= new HashMap<String, DTOFile>();		
		Map<String, Double> hoursCodeMap= new HashMap<String, Double>();
		Map<String, Double> hoursValueMap= new HashMap<String, Double>();
		Map<String, Set<String>> noOfColumnMap= new HashMap<String, Set<String>>();
		
		Set<String> ss= null;
		DTOFile dtoFile=new DTOFile();
		Iterator it = payrollExtract.iterator();
		while(it.hasNext()){	
			Object listObject = it.next();			
			dtoFile=new DTOFile();
			if(((DTOPayrollExtract) listObject).getCoCode() == null){
				dtoFile.setCoCode("");
			}else{
				dtoFile.setCoCode(((DTOPayrollExtract) listObject).getCoCode().toString());
			}
			if(((DTOPayrollExtract) listObject).getBatchID() == null){
				dtoFile.setBatchID("");
			}else{
				dtoFile.setBatchID(((DTOPayrollExtract) listObject).getBatchID().toString());
			}			
			if(((DTOPayrollExtract) listObject).getRegHours() == null){
				dtoFile.setRegHours("");
			}else{
				dtoFile.setRegHours(((DTOPayrollExtract) listObject).getRegHours().toString());
			}
			if(((DTOPayrollExtract) listObject).getOTHours() == null){
				dtoFile.setoTHours("");
			}else{
				dtoFile.setoTHours(((DTOPayrollExtract) listObject).getOTHours().toString());
			}
			if(((DTOPayrollExtract) listObject).getPay() == null){
				dtoFile.setPay("");
			}else{
				dtoFile.setPay(((DTOPayrollExtract) listObject).getPay().toString());
			}
			if(((DTOPayrollExtract) listObject).getTaxFrequency() == null){
				dtoFile.setTaxFrequency("");
			}else{
				dtoFile.setTaxFrequency(((DTOPayrollExtract) listObject).getTaxFrequency().toString());
			}
			if(((DTOPayrollExtract) listObject).getSpecialAction() == null){
				dtoFile.setSpecialAction("");
			}else{
				dtoFile.setSpecialAction(((DTOPayrollExtract) listObject).getSpecialAction().toString());
			}
			if(((DTOPayrollExtract) listObject).getRegEarnings() == null){
				dtoFile.setRegEarnings("");
			}else{
				dtoFile.setRegEarnings(((DTOPayrollExtract) listObject).getRegEarnings().toString());
			}
			if(((DTOPayrollExtract) listObject).getOTEarnings() == null){
				dtoFile.setoTEarnings("");
			}else{
				dtoFile.setoTEarnings(((DTOPayrollExtract) listObject).getOTEarnings().toString());
			}			
			
			if(((DTOPayrollExtract) listObject).getHours3Code() == null){
				dtoFile.setHours3Code("");
			}else{
				dtoFile.setHours3Code(((DTOPayrollExtract) listObject).getHours3Code().toString());
			}
			if(((DTOPayrollExtract) listObject).getEarnings3Code() == null){
				dtoFile.setEarnings3Code("");
			}else{
				dtoFile.setEarnings3Code(((DTOPayrollExtract) listObject).getEarnings3Code().toString());
			}
			if(((DTOPayrollExtract) listObject).getEmployee() == null){
				dtoFile.setEmployee("");
			}else{
				dtoFile.setEmployee(((DTOPayrollExtract) listObject).getEmployee().toString());
			}
			
			
			if(masterDtoFile.isEmpty()){
				masterDtoFile.put(((DTOPayrollExtract) listObject).getFileNo().toString(), dtoFile);
			}else{
				if(masterDtoFile!=null && !masterDtoFile.isEmpty() && masterDtoFile.containsKey(((DTOPayrollExtract) listObject).getFileNo().toString())){
					DTOFile dtoTemp=masterDtoFile.get(((DTOPayrollExtract) listObject).getFileNo().toString());
					dtoTemp.setCoCode(((DTOPayrollExtract) listObject).getCoCode().toString());
					dtoTemp.setBatchID(((DTOPayrollExtract) listObject).getBatchID().toString());
					dtoTemp.setPay(((DTOPayrollExtract) listObject).getPay().toString());
					dtoTemp.setTaxFrequency(((DTOPayrollExtract) listObject).getTaxFrequency().toString());
					dtoTemp.setSpecialAction(((DTOPayrollExtract) listObject).getSpecialAction().toString());
					dtoTemp.setEmployee(((DTOPayrollExtract) listObject).getEmployee().toString());
					
					if(!((DTOPayrollExtract) listObject).getHours3Code().toString().equalsIgnoreCase("")){
					dtoTemp.setHours3Code(((DTOPayrollExtract) listObject).getHours3Code().toString());
					}
					if(!((DTOPayrollExtract) listObject).getEarnings3Code().toString().equalsIgnoreCase("")){
					dtoTemp.setEarnings3Code(((DTOPayrollExtract) listObject).getEarnings3Code().toString());
					}
					BigDecimal d1=new BigDecimal(0);
					BigDecimal d2=new BigDecimal(0);
						try{
						d1=new BigDecimal(((DTOFile) dtoTemp).getRegHours().toString());
						}catch(Exception e){}
						try{
						d2=new BigDecimal(((DTOPayrollExtract) listObject).getRegHours().toString());
						}catch(Exception e){}
					dtoTemp.setRegHours(d1.add(d2));
					try{
						d1=new BigDecimal(((DTOFile) dtoTemp).getoTHours().toString());
						}catch(Exception e){}
						try{
						d2=new BigDecimal(((DTOPayrollExtract) listObject).getOTHours().toString());
						}catch(Exception e){}
					dtoTemp.setoTHours(d1.add(d2));
					try{
						d1=new BigDecimal(((DTOFile) dtoTemp).getRegEarnings().toString());
						}catch(Exception e){}
						try{
						d2=new BigDecimal(((DTOPayrollExtract) listObject).getRegEarnings().toString());
						}catch(Exception e){}
					
					dtoTemp.setRegEarnings(d1.add(d2));
					/*System.out.print(">>>>>>>>>>>>>>>>>>>FileNO\n"+fileNo+"\n");
					System.out.print(">>>>>>>>>>>>>>>>>>>d1\n"+d1+"\n");
					System.out.print(">>>>>>>>>>>>>>>>>>>d2\n"+d2+"\n");*/
					try{
						d1=new BigDecimal(((DTOFile) dtoTemp).getoTEarnings().toString());
						}catch(Exception e){}
						try{
						d2=new BigDecimal(((DTOPayrollExtract) listObject).getOTEarnings().toString());
						}catch(Exception e){}
						
					dtoTemp.setoTEarnings(d1.add(d2));
					
					masterDtoFile.put(((DTOPayrollExtract) listObject).getFileNo().toString(), dtoTemp);
				}else{
					masterDtoFile.put(((DTOPayrollExtract) listObject).getFileNo().toString(), dtoFile);
				}
			}
			
			if((((DTOPayrollExtract) listObject).getEarnings3Code()!=null)&&(!((DTOPayrollExtract) listObject).getEarnings3Code().toString().trim().equalsIgnoreCase(""))){
				
				if(hoursCodeMap.isEmpty()){
					hoursCodeMap.put(((DTOPayrollExtract) listObject).getFileNo().toString()+"~"+((DTOPayrollExtract) listObject).getHours3Code().toString(), Double.parseDouble(((DTOPayrollExtract) listObject).getHours3Amount().toString()));
				}else{
					if(hoursCodeMap!=null && !hoursCodeMap.isEmpty() && hoursCodeMap.containsKey(((DTOPayrollExtract) listObject).getFileNo().toString()+"~"+((DTOPayrollExtract) listObject).getHours3Code().toString())){
						Double dd=hoursCodeMap.get(((DTOPayrollExtract) listObject).getFileNo().toString()+"~"+((DTOPayrollExtract) listObject).getHours3Code().toString());
						dd=dd+Double.parseDouble(((DTOPayrollExtract) listObject).getHours3Amount().toString());
						hoursCodeMap.put(((DTOPayrollExtract) listObject).getFileNo().toString()+"~"+((DTOPayrollExtract) listObject).getHours3Code().toString(),dd);
					}else{
						hoursCodeMap.put(((DTOPayrollExtract) listObject).getFileNo().toString()+"~"+((DTOPayrollExtract) listObject).getHours3Code().toString(), Double.parseDouble(((DTOPayrollExtract) listObject).getHours3Amount().toString()));					
					}
				}
				
				if(hoursValueMap.isEmpty()){
					hoursValueMap.put(((DTOPayrollExtract) listObject).getFileNo().toString()+"~"+((DTOPayrollExtract) listObject).getEarnings3Code().toString(), Double.parseDouble(((DTOPayrollExtract) listObject).getEarnings3Amount().toString()));
				}else{
					if(hoursValueMap!=null && !hoursValueMap.isEmpty() && hoursValueMap.containsKey(((DTOPayrollExtract) listObject).getFileNo().toString()+"~"+((DTOPayrollExtract) listObject).getEarnings3Code().toString())){
						Double dd=hoursValueMap.get(((DTOPayrollExtract) listObject).getFileNo().toString()+"~"+((DTOPayrollExtract) listObject).getEarnings3Code().toString());
						dd=dd+Double.parseDouble(((DTOPayrollExtract) listObject).getEarnings3Amount().toString());
						hoursValueMap.put(((DTOPayrollExtract) listObject).getFileNo().toString()+"~"+((DTOPayrollExtract) listObject).getEarnings3Code().toString(),dd);
					}else{
						hoursValueMap.put(((DTOPayrollExtract) listObject).getFileNo().toString()+"~"+((DTOPayrollExtract) listObject).getEarnings3Code().toString(), Double.parseDouble(((DTOPayrollExtract) listObject).getEarnings3Amount().toString()));					
					}
				}
				
				if(noOfColumnMap.isEmpty()){
					ss= new TreeSet<String>();
					ss.add(((DTOPayrollExtract) listObject).getEarnings3Code().toString());
					noOfColumnMap.put(((DTOPayrollExtract) listObject).getFileNo().toString(),ss);
				}else{
					if(noOfColumnMap!=null && !noOfColumnMap.isEmpty() && noOfColumnMap.containsKey(((DTOPayrollExtract) listObject).getFileNo().toString())){
						ss=noOfColumnMap.get(((DTOPayrollExtract) listObject).getFileNo().toString());
						ss.add(((DTOPayrollExtract) listObject).getEarnings3Code().toString());
						noOfColumnMap.put(((DTOPayrollExtract) listObject).getFileNo().toString(),ss);
					}else{
						ss= new TreeSet<String>();
						ss.add(((DTOPayrollExtract) listObject).getEarnings3Code().toString());
						noOfColumnMap.put(((DTOPayrollExtract) listObject).getFileNo().toString(),ss);
					}
				}
		}
			
		}
		Integer maxSize=1;
		for (Map.Entry<String, Set<String>> entry1 : noOfColumnMap.entrySet()) {
			if(entry1.getValue().size()>maxSize){
				maxSize=entry1.getValue().size();
			}
		}
		List al=new ArrayList();
		GenralDTO genralDTO= new GenralDTO();
		genralDTO.setMasterDtoFile(masterDtoFile);
		genralDTO.setHoursCodeMap(hoursCodeMap);
		genralDTO.setHoursValueMap(hoursValueMap);
		genralDTO.setSize(maxSize);
		al.add(genralDTO);
		return al;
	}
	public class GenralDTO{
		private Map<String, DTOFile> masterDtoFile;
		private Map<String, Double> hoursCodeMap;
		private Map<String, Double> hoursValueMap;
		private int size;
		public Map<String, DTOFile> getMasterDtoFile() {
			return masterDtoFile;
		}
		public void setMasterDtoFile(Map<String, DTOFile> masterDtoFile) {
			this.masterDtoFile = masterDtoFile;
		}
		public Map<String, Double> getHoursCodeMap() {
			return hoursCodeMap;
		}
		public void setHoursCodeMap(Map<String, Double> hoursCodeMap) {
			this.hoursCodeMap = hoursCodeMap;
		}
		public Map<String, Double> getHoursValueMap() {
			return hoursValueMap;
		}
		public void setHoursValueMap(Map<String, Double> hoursValueMap) {
			this.hoursValueMap = hoursValueMap;
		}
		public int getSize() {
			return size;
		}
		public void setSize(int size) {
			this.size = size;
		}
		
	}
	public  class DTOFile {
		private Object earnings3Code;
		private Object hours3Code;
		private Object coCode;
		private Object batchID;
		private Object tempDept;
		private Object regHours;
		private Object oTHours;
		private Object pay;
		private Object taxFrequency;
		private Object tempRate;
		private Object regEarnings;
		private Object oTEarnings;
		private Object memoCode;
		private Object memoAmount;
		private Object specialAction;
		private Object Employee;
		
		
		public Object getCoCode() {
			return coCode;
		}
		public void setCoCode(Object coCode) {
			this.coCode = coCode;
		}
		public Object getBatchID() {
			return batchID;
		}
		public void setBatchID(Object batchID) {
			this.batchID = batchID;
		}
		public Object getTempDept() {
			return tempDept;
		}
		public void setTempDept(Object tempDept) {
			this.tempDept = tempDept;
		}
		public Object getRegHours() {
			return regHours;
		}
		public void setRegHours(Object regHours) {
			this.regHours = regHours;
		}
		public Object getoTHours() {
			return oTHours;
		}
		public void setoTHours(Object oTHours) {
			this.oTHours = oTHours;
		}
		public Object getPay() {
			return pay;
		}
		public void setPay(Object pay) {
			this.pay = pay;
		}
		public Object getTaxFrequency() {
			return taxFrequency;
		}
		public void setTaxFrequency(Object taxFrequency) {
			this.taxFrequency = taxFrequency;
		}
		public Object getTempRate() {
			return tempRate;
		}
		public void setTempRate(Object tempRate) {
			this.tempRate = tempRate;
		}
		public Object getRegEarnings() {
			return regEarnings;
		}
		public void setRegEarnings(Object regEarnings) {
			this.regEarnings = regEarnings;
		}
		public Object getoTEarnings() {
			return oTEarnings;
		}
		public void setoTEarnings(Object oTEarnings) {
			this.oTEarnings = oTEarnings;
		}
		public Object getMemoCode() {
			return memoCode;
		}
		public void setMemoCode(Object memoCode) {
			this.memoCode = memoCode;
		}
		public Object getMemoAmount() {
			return memoAmount;
		}
		public void setMemoAmount(Object memoAmount) {
			this.memoAmount = memoAmount;
		}
		public Object getHours3Code() {
			return hours3Code;
		}
		public void setHours3Code(Object hours3Code) {
			this.hours3Code = hours3Code;
		}
		public Object getEarnings3Code() {
			return earnings3Code;
		}
		public void setEarnings3Code(Object earnings3Code) {
			this.earnings3Code = earnings3Code;
		}
		public Object getSpecialAction() {
			return specialAction;
		}
		public void setSpecialAction(Object specialAction) {
			this.specialAction = specialAction;
		}
		
		public Object getEmployee() {
			return Employee;
		}
		public void setEmployee(Object employee) {
			Employee = employee;
		}
		
	}
	public  class DTOPayrollExtractInternal
	{
		private Object Warehouse;
		private Object CrewName;
		private Object RegPay;
		private Object RegHours;
		private Object OTHours;
		private Object Earnings3Code;
		private Object Earnings3Amount;
		private Object Hours3Code;
		private Object Hours3Amount;
		private Object RateCode;
		
		
		public Object getCrewName() {
			return CrewName;
		}
		public void setCrewName(Object crewName) {
			CrewName = crewName;
		}
		public Object getEarnings3Amount() {
			return Earnings3Amount;
		}
		public void setEarnings3Amount(Object earnings3Amount) {
			Earnings3Amount = earnings3Amount;
		}
		public Object getEarnings3Code() {
			return Earnings3Code;
		}
		public void setEarnings3Code(Object earnings3Code) {
			Earnings3Code = earnings3Code;
		}
		public Object getHours3Amount() {
			return Hours3Amount;
		}
		public void setHours3Amount(Object hours3Amount) {
			Hours3Amount = hours3Amount;
		}
		public Object getHours3Code() {
			return Hours3Code;
		}
		public void setHours3Code(Object hours3Code) {
			Hours3Code = hours3Code;
		}
		public Object getOTHours() {
			return OTHours;
		}
		public void setOTHours(Object hours) {
			OTHours = hours;
		}
		public Object getRateCode() {
			return RateCode;
		}
		public void setRateCode(Object rateCode) {
			RateCode = rateCode;
		}
		public Object getRegHours() {
			return RegHours;
		}
		public void setRegHours(Object regHours) {
			RegHours = regHours;
		}
		public Object getRegPay() {
			return RegPay;
		}
		public void setRegPay(Object regPay) {
			RegPay = regPay;
		}
		public Object getWarehouse() {
			return Warehouse;
		}
		public void setWarehouse(Object warehouse) {
			Warehouse = warehouse;
		}
	}
	public List findPayrollInternalList(String beginDates, String endDates, String corpId) {

		String query="SELECT crew.`warehouse` AS BatchID,crew.`employeeId` AS EmployeeNo, concat_ws(', ', `crew`.lastname, `crew`.firstname) AS CrewName, DATE_FORMAT(workdate,'%m/%d/%Y') AS workdate, timesheet.action As Action, CONCAT('0',timesheet.`distributionCode`,lpad(crew.`warehouse`,2,'0')) AS TempDept,IF(timesheet.`action` not IN('H','P','S','V','F') , round(sum(IF(timesheet.`paidRevenueShare`>0,0, ROUND(timesheet.`regularHours`,1))),2),0) AS RegHours, round(sum(IF(timesheet.`paidRevenueShare`>0,0, ROUND(timesheet.`OverTime`,1))),2) AS OTHours, if(sum(IF((timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`)<>0, (timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`),0))>0,'Z','') AS Earnings3Code, round(sum(IF((timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`)<>0, (timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`),0)),2) AS Earnings3Amount,IF((IF(timesheet.`paidRevenueShare`>0,0, ROUND(timesheet.`doubleOverTime`,1))) > 0,'D', IF(timesheet.`action` IN('H','P','S','V','F'),timesheet.`action`, if(timesheet.`action`='Z','F','') ) ) AS Hours3Code, round(sum(IF((IF(timesheet.`paidRevenueShare`>0,0, ROUND(timesheet.`doubleOverTime`,1))) > 0,timesheet.`doubleOverTime`, IF(timesheet.`action` IN('H','P','S','V','F'),timesheet.`regularHours`, if(timesheet.`action`='Z',timesheet.`regularHours`,'')))))AS Hours3Amount, IF(timesheet.`differentPayBasis`='B'and(IF(timesheet.`paidRevenueShare`>0,0, ROUND(timesheet.`regularHours`,1))+IF(timesheet.`paidRevenueShare`>0,0, ROUND(timesheet.`OverTime`,1))+IF(timesheet.`paidRevenueShare`>0,0, ROUND(timesheet.`doubleOverTime`,1)))>0,2,0) AS RateCode, crew.`payhour` AS PayHour, crew.`typeofWork` AS TypeofWork,workticket.`ticket` AS ticket,workticket.`warehouse` AS warehouse,workticket.`service` AS service, timesheet.`distributioncode` AS DistributionCode, workticket.`jobType` AS jobType,IF(timesheet.`paidRevenueShare`>0,ROUND(timesheet.`regularHours`,1)+ROUND(timesheet.`overTime`,1)+ROUND(timesheet.`doubleOverTime`,1),0)AS Earnings3hours,timesheet.paidRevenueShare AS paidRevenueShare,timesheet.adjustmentToRevenue AS adjustmentToRevenue FROM `crew` crew LEFT OUTER JOIN `timesheet` timesheet ON crew.`userName` = timesheet.`userName` LEFT OUTER JOIN `workticket` workticket ON timesheet.`ticket` = workticket.`ticket` LEFT OUTER JOIN `payroll` payroll ON timesheet.`distributioncode` =payroll.`code` and payroll.corpID ='"+corpId+"' WHERE date_format(timesheet.workDate,'%Y-%m-%d')>= '"+beginDates+"' AND date_format(timesheet.workDate,'%Y-%m-%d') <= '"+endDates+"' AND timesheet.`corpID` = '"+corpId+"' and timesheet.action <> 'L'and (workticket.corpid ='"+corpId+"'  or workticket.corpid is null) group by crew.`warehouse`, crew.`employeeId`,timesheet.`workdate`,timesheet.action, CONCAT(timesheet.`distributionCode`,workticket.`warehouse`,timesheet.`differentPayBasis`),workticket.`ticket`  order by crew.`warehouse`, crew.`lastName`, timesheet.`userName`, timesheet.`workdate`, CONCAT(timesheet.`distributionCode`,workticket.`warehouse`,timesheet.`differentPayBasis`),timesheet.distributioncode";
		List payrollExtractList= this.getSession().createSQLQuery(query)		 
		.addScalar("BatchID", Hibernate.STRING) 
		.addScalar("EmployeeNo", Hibernate.STRING)
		.addScalar("CrewName", Hibernate.STRING) 
		.addScalar("workdate", Hibernate.STRING)
		.addScalar("Action", Hibernate.STRING)
		.addScalar("TempDept", Hibernate.STRING) 
		.addScalar("RegHours", Hibernate.STRING)		
		.addScalar("OTHours", Hibernate.STRING)  
		.addScalar("Earnings3Code", Hibernate.STRING)
		.addScalar("Earnings3Amount", Hibernate.STRING)		
		.addScalar("Hours3Code", Hibernate.STRING)  
		.addScalar("Hours3Amount", Hibernate.STRING)
		.addScalar("RateCode", Hibernate.STRING)
		.addScalar("PayHour", Hibernate.STRING)
		.addScalar("TypeofWork", Hibernate.STRING)
		.addScalar("ticket", Hibernate.LONG)
		.addScalar("warehouse", Hibernate.STRING)
		.addScalar("service", Hibernate.STRING)
		.addScalar("DistributionCode", Hibernate.STRING)
		.addScalar("jobType", Hibernate.STRING)
		.addScalar("Earnings3hours", Hibernate.STRING)
		.addScalar("paidRevenueShare", Hibernate.STRING)
		.addScalar("adjustmentToRevenue", Hibernate.STRING)
		.list(); 
		
		//System.out.println("\n\n Payroll Internal Extract QUERY=======>"+query);
		
		//System.out.println("\n\n SIZE=======>"+payrollExtractList.size());
		
		/*Iterator iterator=payrollExtractList.iterator();	
		while(iterator.hasNext())
		{
			Object []row= (Object [])iterator.next();
			DTOPayrollExtract dtoPayrollExtract = new DTOPayrollExtract();
			dtoPayrollExtract.setCoCode((row[0]));
			dtoPayrollExtract.setBatchID((row[1]));
			dtoPayrollExtract.setFileNo((row[2]));
			dtoPayrollExtract.setTempDept(row[3]); 
			dtoPayrollExtract.setRegHours(row[4]);
			dtoPayrollExtract.setOTHours(row[5]);
			dtoPayrollExtract.setEarnings3Code((row[6]));
			dtoPayrollExtract.setEarnings3Amount(row[7]); 
			dtoPayrollExtract.setHours3Code(row[8]); 
			dtoPayrollExtract.setHours3Amount(row[9]);
			dtoPayrollExtract.setRateCode(row[10]); 
			payrollExtract.add(dtoPayrollExtract);
			
		}*/
		return payrollExtractList;
	
	}

	public List<Payroll> getActiveList() {
		return getHibernateTemplate().find( "from Payroll where active = 'true' ");
	}

	public int updateSickPersonal(String fromDate, String toDate, String sessionCorpID) {
		String fromDateNew="";
		String toDateNew="";
		String vacationHoursQuery="";
		String anniveQuery="";
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( new SimpleDateFormat("dd-MMM-yy").parse(fromDate) ) );
	        fromDateNew = nowYYYYMMDD.toString();
	        SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD1 = new StringBuilder( dateformatYYYYMMDD1.format( new SimpleDateFormat("dd-MMM-yy").parse(toDate) ) );
	        toDateNew = nowYYYYMMDD1.toString();
	 	}catch (ParseException e) {
			e.printStackTrace();
		}
	 	
	 	String query="select t.userName, action, sum(regularHours), " +
	 			"round(DATEDIFF(now(),c.hired)/365.25,2) as tenureYear, c.hired  " +
	 			"from timesheet t, crew c where action in  ('S','P') " +
	 			"and t.userName = c.userName " +
	 			"and workDate between '"+fromDateNew+"' " +
	 			"and '"+toDateNew+"' and c.corpID = '"+sessionCorpID+"' " +
	 			"and companyDivision = 'SSC' and terminatedDate is null " +
	 			"and c.unionName <= sysdate() and c.unionName is not null group by t.userName, action" ;
	 	List sickPersonalList= this.getSession().createSQLQuery(query).list();
	 	int listSize=sickPersonalList.size();
	 	Iterator iterator=sickPersonalList.iterator();
	 	while(iterator.hasNext())
		{
			Object []row= (Object [])iterator.next();
			String userName=row[0].toString();
			String action=row[1].toString();
			String regHours=row[2].toString();
			if(action.equalsIgnoreCase("S"))
			{
				getHibernateTemplate().bulkUpdate("update Payroll set sickUsed="+regHours+" where userName='"+userName+"'");
			}
			if(action.equalsIgnoreCase("P"))
			{
				getHibernateTemplate().bulkUpdate("update Payroll set personalDayUsed="+regHours+" where userName='"+userName+"'");
			}
						
			
		}
	 	
	 	String anniverSaryQuery="select t.userName, action, sum(regularHours), " +
			"round(DATEDIFF(now(),c.hired)/365.25,2) as tenureYear, c.hired  " +
			"from timesheet t, crew c where " +
			" t.userName = c.userName " +
			"and workDate between '"+fromDateNew+"' " +
			"and '"+toDateNew+"' and c.corpID = '"+sessionCorpID+"' " +
			"and companyDivision = 'SSC' and terminatedDate is null " +
			"and c.unionName <= sysdate() and  c.unionName is not null group by t.userName" ;
			
	 		List aaniverSaryList= this.getSession().createSQLQuery(anniverSaryQuery).list();
			
			Iterator iterator1=aaniverSaryList.iterator();
			while(iterator1.hasNext())
			{
			Object []row= (Object [])iterator1.next();
			String userName=row[0].toString();
			String tenure=row[3].toString();
			String hired=row[4].toString();
			
			getHibernateTemplate().bulkUpdate("update Payroll set tenure="+tenure+" where userName='"+userName+"'");
			
			try{
				String query3="select if((DATEDIFF('"+toDateNew+"','"+hired+"')/365)>0,DATE_ADD('"+hired+"',INTERVAL (FLOOR(DATEDIFF('"+toDateNew+"','"+hired+"')/365)) YEAR),'"+hired+"')";
				List annivHoursList=this.getSession().createSQLQuery(query3).list();
				
				String query4="select lastUpdatedLeave from companydivision where companyCode= 'SSC' and corpID = '"+sessionCorpID+"'";
				List lastupdateleaveDate=this.getSession().createSQLQuery(query4).list();
				
				//List dateDiffVacation=this.getSession().createSQLQuery("SELECT DATEDIFF(now(),'"+annivHoursList.get(0).toString()+"')").list();
				List getAniversaryYear = this.getSession().createSQLQuery("select lastAnniversaryDate from crew where corpid='"+sessionCorpID+"' and userName='"+userName+"'").list();
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date anvDate = new Date();
				if(getAniversaryYear!=null && !getAniversaryYear.isEmpty() && getAniversaryYear.get(0)!=null){
					anvDate = sdf.parse(getAniversaryYear.get(0).toString());
				}
				int anvYear = anvDate.getYear();
				int curryear = new Date().getYear();
				int anvMonth = anvDate.getMonth()+1;
				int currMonth = new Date().getMonth()+1;
				
				if(lastupdateleaveDate!=null && !lastupdateleaveDate.isEmpty()){
					Date date1 = sdf.parse(lastupdateleaveDate.get(0).toString());
		        	Date date2 = sdf.parse(annivHoursList.get(0).toString());
					if(date2.after(date1) && (new Date()).before(date2)){
						String vacationHrsList="";
						String query2="select r.bucket2 from refmaster r where r.parameter='VACATIONHRS' and r.code >= "+tenure+" and r.corpid = '"+sessionCorpID+"' limit 1";
						List list1=this.getSession().createSQLQuery(query2).list();
						if((list1!=null)&&(!list1.isEmpty())&&(list1.get(0)!=null)){
							vacationHrsList=list1.get(0).toString();
						}
										
						String vacationUsedList="";
						String query1="select vacationUsed from crew where corpid='"+sessionCorpID+"' and userName='"+userName+"'";
						List list=this.getSession().createSQLQuery(query1).list();
						if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
							vacationUsedList=list.get(0).toString();
						}
						
						Double  vacationHrsLeft=new Double(0.0);
						Double bd= new Double (vacationUsedList);
						Double bd1= new Double (vacationHrsList);
						boolean comparsionResult = (bd.doubleValue() > bd1.doubleValue());
							if (comparsionResult){
								 vacationHrsLeft=(bd)-(bd1);
								 vacationHrsLeft= (bd1)-(vacationHrsLeft);					 
							 }else{
								vacationHrsLeft= new Double (vacationHrsList);
							 }
							
						vacationHoursQuery="update Payroll set vacationHrs='"+vacationHrsLeft+"' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(vacationHoursQuery);
						
						anniveQuery="update Payroll  set lastAnniversaryDate='"+annivHoursList.get(0).toString()+"' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(anniveQuery);
						
						String vacationQuery="update Payroll  set vacationUsed='0.0' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(vacationQuery);
				       }else if(date2.after(date1) && date2.before(new Date())){
						
					/*	String query12="select c.username,  ifnull(sum(regularhours),0) from  crew c  " +
						"left outer join timesheet t on t.username = c.username " +
						"and  t.workdate between c.lastAnniversaryDate and '"+toDateNew+"' " +
						"and action='V' where c.corpid = '"+sessionCorpID+"' " +
						"and c.companydivision = 'SSC' " +
						"and c.terminateddate is null " +
						"and c.unionname <= sysdate() and c.userName='"+userName+"'" +
						"and c.unionname is not null group by c.username, action" ;
				
					try{
				List vactionList= this.getSession().createSQLQuery(query12).list();
				listSize=vactionList.size();
				Iterator iterator12=vactionList.iterator();
				while(iterator12.hasNext()){
					Object []row2= (Object [])iterator12.next();
				String userName1=row2[0].toString();
				String regHours1=row2[1].toString();
				
				getHibernateTemplate().bulkUpdate("update Payroll set vacationUsed="+regHours1+" where userName='"+userName1+"'");
				
				}
					}catch(Exception ex)
					{
						System.out.println("\n\n\n\n\n\n\n updateVacation ex---->"+ex);
					}*/
						
						
						String vacationHrsList="";
						String query2="select r.bucket2 from refmaster r where r.parameter='VACATIONHRS' and r.code >= "+tenure+" and r.corpid = '"+sessionCorpID+"' limit 1";
						List list1=this.getSession().createSQLQuery(query2).list();
						if((list1!=null)&&(!list1.isEmpty())&&(list1.get(0)!=null)){
							vacationHrsList=list1.get(0).toString();
						}
										
						String vacationUsedList="";
						String query1="select vacationUsed from crew where corpid='"+sessionCorpID+"' and userName='"+userName+"'";
						List list=this.getSession().createSQLQuery(query1).list();
						if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
							vacationUsedList=list.get(0).toString();
						}
						
						Double  vacationHrsLeft=new Double(0.0);
						Double bd= new Double (vacationUsedList);
						Double bd1= new Double (vacationHrsList);
						boolean comparsionResult = (bd.doubleValue() > bd1.doubleValue());
							if (comparsionResult){
								 vacationHrsLeft=(bd)-(bd1);
								 vacationHrsLeft= (bd1)-(vacationHrsLeft);					 
							 }else{
								vacationHrsLeft= new Double (vacationHrsList);
							 }
							
						vacationHoursQuery="update Payroll set vacationHrs='"+vacationHrsLeft+"' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(vacationHoursQuery);
						
						anniveQuery="update Payroll  set lastAnniversaryDate='"+annivHoursList.get(0).toString()+"' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(anniveQuery);
						
						String vacationQuery="update Payroll  set vacationUsed='0.0' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(vacationQuery);
					}else if(date2.after(date1) && date2.compareTo(new Date())==0){
						String vacationHrsList="";
						
						/*anniveQuery="update Payroll  set lastAnniversaryDate='"+annivHoursList.get(0).toString()+"' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(anniveQuery);
						
						String query12="select c.username,  ifnull(sum(regularhours),0) from  crew c  " +
						"left outer join timesheet t on t.username = c.username " +
						"and  t.workdate between c.lastAnniversaryDate and '"+toDateNew+"' " +
						"and action='V' where c.corpid = '"+sessionCorpID+"' " +
						"and c.companydivision = 'SSC' " +
						"and c.terminateddate is null " +
						"and c.unionname <= sysdate() and c.userName='"+userName+"'" +
						"and c.unionname is not null group by c.username, action" ;
				
					try{
				List vactionList= this.getSession().createSQLQuery(query12).list();
				listSize=vactionList.size();
				Iterator iterator12=vactionList.iterator();
				while(iterator12.hasNext()){
					Object []row2= (Object [])iterator12.next();
				String userName1=row2[0].toString();
				String regHours1=row2[1].toString();
				
				getHibernateTemplate().bulkUpdate("update Payroll set vacationUsed="+regHours1+" where userName='"+userName1+"'");
				
				}
					}catch(Exception ex)
					{
						System.out.println("\n\n\n\n\n\n\n updateVacation ex---->"+ex);
					}*/
						String query2="select r.bucket2 from refmaster r where r.parameter='VACATIONHRS' and r.code >= "+tenure+" and r.corpid = '"+sessionCorpID+"' limit 1";
						List list1=this.getSession().createSQLQuery(query2).list();
						if((list1!=null)&&(!list1.isEmpty())&&(list1.get(0)!=null)){
							vacationHrsList=list1.get(0).toString();
						}
										
						String vacationUsedList="";
						String query1="select vacationUsed from crew where corpid='"+sessionCorpID+"' and userName='"+userName+"'";
						List list=this.getSession().createSQLQuery(query1).list();
						if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
							vacationUsedList=list.get(0).toString();
						}
						
						Double  vacationHrsLeft=new Double(0.0);
						Double bd= new Double (vacationUsedList);
						Double bd1= new Double (vacationHrsList);
						boolean comparsionResult = (bd.doubleValue() > bd1.doubleValue());
							if (comparsionResult){
								 vacationHrsLeft=(bd)-(bd1);
								 vacationHrsLeft= (bd1)-(vacationHrsLeft);					 
							 }else{
								vacationHrsLeft= new Double (vacationHrsList);
							 }
							
						vacationHoursQuery="update Payroll set vacationHrs='"+vacationHrsLeft+"' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(vacationHoursQuery);
						
						anniveQuery="update Payroll  set lastAnniversaryDate='"+annivHoursList.get(0).toString()+"' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(anniveQuery);
						
						String vacationQuery="update Payroll  set vacationUsed='0.0' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(vacationQuery);
					}else if(curryear-anvYear==1 && anvMonth==currMonth 
							&& (anvDate.getDate() - new Date().getDate()<7 || new Date().getDate() - anvDate.getDate() <7))
					{

						String vacationHrsList="";
						String query2="select r.bucket2 from refmaster r where r.parameter='VACATIONHRS' and r.code >= "+tenure+" and r.corpid = '"+sessionCorpID+"' limit 1";
						List list1=this.getSession().createSQLQuery(query2).list();
						if((list1!=null)&&(!list1.isEmpty())&&(list1.get(0)!=null)){
							vacationHrsList=list1.get(0).toString();
						}
										
						String vacationUsedList="";
						String query1="select vacationUsed from crew where corpid='"+sessionCorpID+"' and userName='"+userName+"'";
						List list=this.getSession().createSQLQuery(query1).list();
						if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
							vacationUsedList=list.get(0).toString();
						}
						
						Double  vacationHrsLeft=new Double(0.0);
						Double bd= new Double (vacationUsedList);
						Double bd1= new Double (vacationHrsList);
						boolean comparsionResult = (bd.doubleValue() > bd1.doubleValue());
							if (comparsionResult){
								 vacationHrsLeft=(bd)-(bd1);
								 vacationHrsLeft= (bd1)-(vacationHrsLeft);					 
							 }else{
								vacationHrsLeft= new Double (vacationHrsList);
							 }
							
						vacationHoursQuery="update Payroll set vacationHrs='"+vacationHrsLeft+"' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(vacationHoursQuery);
						
						anniveQuery="update Payroll  set lastAnniversaryDate='"+annivHoursList.get(0).toString()+"' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(anniveQuery);
						
						String vacationQuery="update Payroll  set vacationUsed='0.0' where userName='"+userName+"'";
						getHibernateTemplate().bulkUpdate(vacationQuery);
					}
				}else{
					String vacationHrsList="";
					String query2="select r.bucket2 from refmaster r where r.parameter='VACATIONHRS' and r.code >= "+tenure+" and r.corpid = '"+sessionCorpID+"' limit 1";
					List list1=this.getSession().createSQLQuery(query2).list();
					if((list1!=null)&&(!list1.isEmpty())&&(list1.get(0)!=null)){
						vacationHrsList=list1.get(0).toString();
					}
									
					String vacationUsedList="";
					String query1="select vacationUsed from crew where corpid='"+sessionCorpID+"' and userName='"+userName+"'";
					List list=this.getSession().createSQLQuery(query1).list();
					if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
						vacationUsedList=list.get(0).toString();
					}
					
					Double  vacationHrsLeft=new Double(0.0);
					Double bd= new Double (vacationUsedList);
					Double bd1= new Double (vacationHrsList);
					boolean comparsionResult = (bd.doubleValue() > bd1.doubleValue());
						if (comparsionResult){
							 vacationHrsLeft=(bd)-(bd1);
							 vacationHrsLeft= (bd1)-(vacationHrsLeft);					 
						 }else{
							vacationHrsLeft= new Double (vacationHrsList);
						 }
						
					vacationHoursQuery="update Payroll set vacationHrs='"+vacationHrsLeft+"' where userName='"+userName+"'";
					getHibernateTemplate().bulkUpdate(vacationHoursQuery);
					
					
					anniveQuery="update Payroll  set lastAnniversaryDate='"+annivHoursList.get(0).toString()+"' where userName='"+userName+"'";
					getHibernateTemplate().bulkUpdate(anniveQuery);
					
				}
			}catch(Exception ex)
			{
				System.out.println("\n\n\n\n\n\n\n ex---->"+ex);
			}
			}
			//String abcr="update CompanyDivision set lastUpdatedLeave=now() where companyCode= 'SSC' and corpID = '"+sessionCorpID+"'";
			getHibernateTemplate().bulkUpdate("update CompanyDivision set lastUpdatedLeave=now() where companyCode= 'SSC' and corpID = '"+sessionCorpID+"'");
	 	return listSize;
	}
	
	public int updateVacation(String fromDate, String toDate, String sessionCorpID) {
		String fromDateNew="";
		String toDateNew="";
		int listSize=0;
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( new SimpleDateFormat("dd-MMM-yy").parse(fromDate) ) );
	        fromDateNew = nowYYYYMMDD.toString();
	        SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD1 = new StringBuilder( dateformatYYYYMMDD1.format( new SimpleDateFormat("dd-MMM-yy").parse(toDate) ) );
	        toDateNew = nowYYYYMMDD1.toString();
	 	}catch (ParseException e) {
			e.printStackTrace();
		}
		String query1="select c.username,  ifnull(sum(regularhours),0) from  crew c  " +
			"left outer join timesheet t on t.username = c.username " +
			"and  t.workdate between c.lastAnniversaryDate and '"+toDateNew+"' " +
			"and action='V' where c.corpid = '"+sessionCorpID+"' " +
			"and c.companydivision = 'SSC' " +
			"and c.terminateddate is null " +
			"and c.unionname <= sysdate() " +
			"and c.unionname is not null group by c.username, action" ;
	
		try{
	List vactionList= this.getSession().createSQLQuery(query1).list();
	listSize=vactionList.size();
	Iterator iterator1=vactionList.iterator();
	while(iterator1.hasNext()){
		Object []row= (Object [])iterator1.next();
	String userName1=row[0].toString();
	String regHours1=row[1].toString();
	getHibernateTemplate().bulkUpdate("update Payroll set vacationUsed="+regHours1+" where userName='"+userName1+"'");	
	}

		}catch(Exception ex)
		{
			System.out.println("\n\n\n\n\n\n\n updateVacation ex---->"+ex);
		}
	
	return  listSize;
	}

	public List getSickHours(String userName, String sessionCorpID) {
		String query="select CONCAT((if(sickhours is null,0,sickhours)+ if(sickCarriedOver is null,0,sickCarriedOver) - if(sickused is null,0,sickused)),'#',if(companydivision is null,'',companydivision)) from crew where username='"+userName+"' and companydivision = 'SSC' and unionname is not null and unionname <= sysdate() and corpID='"+sessionCorpID+"'";
		List sickHoursList= this.getSession().createSQLQuery(query).list();
		return sickHoursList;
	}

	public List getPersonalHours(String userName, String sessionCorpID) {
		String query="select CONCAT(if(personalDayUsed is null,'',personalDayUsed),'#',if(companydivision is null,'',companydivision)) from crew where username='"+userName+"' and companydivision = 'SSC' and unionname is not null and unionname <= sysdate() and corpID='"+sessionCorpID+"'";
		List personalHoursList= this.getSession().createSQLQuery(query).list();
		return personalHoursList;
	}

	public List getHealthWelfareExtract(String beginDate, String endDate, String corpID) {
		List healthWelfareList = new ArrayList();
		String query = "select t.crewname, workdate,  c.terminateddate, " +
					"if(round(sum(regularhours),1)>8, 8,round(sum(regularhours),1)) 'Daily Hrs', " +
					"c.socialSecurityNumber, c.warehouse "+
					"from timesheet t, crew c " +
					"where workdate between '"+beginDate+"' and '"+endDate+"' " +
							"and c.username = t.username " +
							"and ifnull(c.beginhealthwelf,c.unionname) is not null " +
							"and action not in ('E','L','U','K','S') " +
							"and c.companydivision = 'SSC' " +
							"and c.beginhealthwelf <= '"+endDate+"' " +
							"and t.corpid = c.corpid and t.corpid = '"+corpID+"' " +
									"group by t.crewname, workdate " +
									"order by t.crewname, workdate";
		
		List list = this.getSession().createSQLQuery(query).list();
		
		List hwRateList = this.getSession().createSQLQuery("select healthwelfarerate from systemdefault where corpid = '"+corpID+"'").list();
		BigDecimal hwRate = new BigDecimal(0);
		BigDecimal hwRate1 = new BigDecimal(0);
		BigDecimal hwRate2 = new BigDecimal("6.35");
		//hwRate2.setScale(2, BigDecimal.ROUND_UP);
		if(!hwRateList.isEmpty()){
			hwRate1  = new BigDecimal(hwRateList.get(0).toString());
		}
		String crewName = "";
		BigDecimal calA = new BigDecimal(0);
		
		SortedMap <String, String> crewMap = new TreeMap<String, String>();
		try {
			Iterator it = list.iterator();
			
			String key = new String("");
			String samekey = new String("");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String validateDate="2014-07-31";
			Date date2 = sdf.parse(validateDate);
			while(it.hasNext()){	
				Object []row= (Object [])it.next();
				crewName = row[0] == null ? "" : row[0].toString();	
				if(row[1]!=null && !row[1].toString().equals("")){
			    Date date1 = sdf.parse(row[1].toString());	 
			    if(date1.after(date2)){
			    	hwRate=	hwRate1;
			    }else{
			    	hwRate= hwRate2;
			    }
				}else{
					hwRate=	hwRate1;	
				}
				key = crewName;
				if (key.equals(samekey)){
					String calATemp = crewMap.get(key);
					if(calATemp != null){
						BigDecimal calATempB = new BigDecimal(calATemp.substring(0, calATemp.indexOf("#")));
						calA = calATempB.add(new BigDecimal(row[3] == null ? "0.00" : row[3].toString()));
					}
					String terminateDate = "";
					if(row[2] != null){
						terminateDate = "Y";
					}else{
						terminateDate = "N";
					}
					
					String mapValue = calA.toString().concat("#").concat(terminateDate).concat("~").concat(row[4] == null ? "" : row[4].toString()).concat("@").concat(row[5] == null ? "" : row[5].toString());
					
					crewMap.put(crewName, mapValue);
				}else{
					String terminateDate = "";
					if(row[2] != null){
						terminateDate = "Y";
					}else{
						terminateDate = "N";
					}
					String mapValue = row[3] == null ? "0.00" : row[3].toString().concat("#").concat(terminateDate).concat("~").concat(row[4] == null ? "" : row[4].toString()).concat("@").concat(row[5] == null ? "" : row[5].toString());
					crewMap.put(crewName, mapValue);
				}
				samekey = crewName;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BigDecimal calHoursA = new BigDecimal(0);
		BigDecimal tempHours = new BigDecimal(0);
		
		BigDecimal finalHrs = new BigDecimal(0);
		BigDecimal hwAmt = new BigDecimal(0);
		String ssNumber = "";
		String wareHouse = "";
		
		/*BigDecimal hwRate = new BigDecimal(0);
		if(!hwRateList.isEmpty()){
			hwRate  = new BigDecimal(hwRateList.get(0).toString());
		}*/
		Iterator crewIterator = crewMap.entrySet().iterator();
		while (crewIterator.hasNext()) {
			String terminateDate = "";
			
			Map.Entry entry = (Map.Entry) crewIterator.next();
			String crew = (String) entry.getKey();
			
			String valueString = crewMap.get(crew);
			calHoursA = new BigDecimal(valueString.substring(0, valueString.indexOf("#")));
			terminateDate = valueString.substring(valueString.indexOf("#")+1, valueString.indexOf("~"));
			ssNumber = valueString.substring(valueString.indexOf("~")+1, valueString.indexOf("@"));
			wareHouse = valueString.substring(valueString.indexOf("@")+1, valueString.length()); 
			
			if(terminateDate.equalsIgnoreCase("Y")){
				if(calHoursA.intValue() < 140){
					finalHrs = calHoursA;
				}else{
					finalHrs = new BigDecimal(140);
				}
				hwAmt = finalHrs.multiply(hwRate);
			}
			if(terminateDate.equalsIgnoreCase("N")){
				if(calHoursA.intValue() >= 140){
					finalHrs = new BigDecimal(140);
					hwAmt = finalHrs.multiply(hwRate);
				}
				if(calHoursA.intValue() < 140){	
					tempHours = new BigDecimal(140);
					
					String queryB = "select  round(sum(if (regularhours =0, 8,regularhours)),1)" +
								 "from timesheet t, crew c " +
								 "where workdate between '"+beginDate+"' and '"+endDate+"' " +
									"and c.username = t.username " +
									"and t.crewname = '"+crew+"' " +
									"and ifnull(c.beginhealthwelf,c.unionname) is not null " +
									"and c.terminateddate is null " +
									"and action in ('U','K','S') " +
									"and c.companydivision = 'SSC' " +
									"and c.beginhealthwelf <= '"+endDate+"' " +
									"and t.corpid = c.corpid and t.corpid = '"+corpID+"' " +
										"group by  t.crewname " +
										"order by t.crewname";
					
					List listB = this.getSession().createSQLQuery(queryB).list();
					if(!listB.isEmpty()){
						BigDecimal hrs = new BigDecimal(listB.get(0).toString());
						finalHrs = tempHours.subtract(hrs);
						if(finalHrs.intValue() < calHoursA.intValue()){
							finalHrs = calHoursA;
						}
					 	hwAmt = finalHrs.multiply(hwRate);
					}else{
						finalHrs = tempHours;
					 	hwAmt = finalHrs.multiply(hwRate);
					}
				}
			}
			
			HWDTO whRateDTO = new HWDTO();
			whRateDTO.setCrewName(crew);
			whRateDTO.setSSNumber(ssNumber);
			whRateDTO.setWarehouse(wareHouse);
			whRateDTO.setFinalHrs(finalHrs);
			whRateDTO.setHwAmt(hwAmt);
			healthWelfareList.add(whRateDTO);
		}
		
		return healthWelfareList;
	}
	
	public class HWDTO {
		private Object crewName;
		
		private Object sSNumber;
		
		private Object warehouse;
		
		private Object finalHrs;
		
		private Object hwAmt;
		
		public Object getCrewName() {
			return crewName;
		}
		public void setCrewName(Object crewName) {
			this.crewName = crewName;
		}
		public Object getFinalHrs() {
			return finalHrs;
		}
		public void setFinalHrs(Object finalHrs) {
			this.finalHrs = finalHrs;
		}
		public Object getHwAmt() {
			return hwAmt;
		}
		public void setHwAmt(Object hwAmt) {
			this.hwAmt = hwAmt;
		}
		public Object getSSNumber() {
			return sSNumber;
		}
		public void setSSNumber(Object number) {
			sSNumber = number;
		}
		public Object getWarehouse() {
			return warehouse;
		}
		public void setWarehouse(Object warehouse) {
			this.warehouse = warehouse;
		}

	}

	public List getVacationHours(String userName, String sessionCorpID) {
		String query="select CONCAT(ifNUll(carryOverAllowed,0)+ifNUll(vacationHrs,0)-(ifNUll(vacationUsed,0)+ifNUll(unscheduledLeave,0)),'#',companydivision) from crew where username='"+userName+"' and companydivision = 'SSC' and unionname is not null and unionname <= sysdate() and corpID='"+sessionCorpID+"'";
		List vacationHoursList= this.getSession().createSQLQuery(query).list();
		return  vacationHoursList;
	}

	

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}

	public List findStorageLibraryExtract(String locWarehouse, String typeSto, String sessionCorpID) {
		String query="";
		if(locWarehouse.trim().equals("")){
		query = "SELECT sl.`storageId`, sl.`storageType`, sl.`volumeCft`, sl.`usedVolumeCft`, sl.`availVolumeCft`, sl.`owner`, sl.`volumeCbm`, sl.`usedVolumeCbm`, sl.`availVolumeCbm`, (replace(replace(replace(replace(replace(replace(st.description,'\r\n',' '),'\r',' '),'\t',' '),'\"',' '),' ',' '),'\n',' ')) description, st.`itemTag`, st.`itemNumber`, st.`containerId`,if(l.`occupied`='X','Y',''), l.`warehouse`, l.`locationId`, l.`type`,s.`shipNumber`, s.`firstName`, s.`lastName`, s.`billToCode`, date_format(st.`createdOn`,'%Y-%m-%d'), st.ticket, st.volume FROM storagelibrary sl, storage st, location l, serviceorder s where sl.corpID='"+sessionCorpID+"' and st.corpID='"+sessionCorpID+"' and l.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and sl.storageId=st.storageId and s.id=st.serviceOrderId and l.locationId=st.locationId and st.releasedate is null and sl.storageType like '"+typeSto+"%'";
		}else{
		query = "SELECT sl.`storageId`, sl.`storageType`, sl.`volumeCft`, sl.`usedVolumeCft`, sl.`availVolumeCft`, sl.`owner`, sl.`volumeCbm`, sl.`usedVolumeCbm`, sl.`availVolumeCbm`, (replace(replace(replace(replace(replace(replace(st.description,'\r\n',' '),'\r',' '),'\t',' '),'\"',' '),' ',' '),'\n',' ')) description, st.`itemTag`, st.`itemNumber`, st.`containerId`,if(l.`occupied`='X','Y',''), l.`warehouse`, l.`locationId`, l.`type`,s.`shipNumber`, s.`firstName`, s.`lastName`, s.`billToCode`, date_format(st.`createdOn`,'%Y-%m-%d'), st.ticket, st.volume FROM storagelibrary sl, storage st, location l, serviceorder s where sl.corpID='"+sessionCorpID+"' and st.corpID='"+sessionCorpID+"' and l.corpID='"+sessionCorpID+"' and s.corpID='"+sessionCorpID+"' and sl.storageId=st.storageId and s.id=st.serviceOrderId and l.locationId=st.locationId and st.releasedate is null and l.`warehouse` = '"+locWarehouse+"' and sl.storageType like '"+typeSto+"%' ";
		}
		List storageLib= this.getSession().createSQLQuery(query).list();	
    	return storageLib;
    }

	public  class DTOPaychexExtract
	{
		private Object valueWH;
		private Object valueID;
		private Object valueDC;
		private Object valueRH;
		private Object valueOH;
		private Object valueED;
		private Object valueEA;
		private Object valueTD;
		private Object valueRA;
		private Object valueRC;
		private Object regularHours;
		private Object overTime;
		private Object doubleOverTime;
		private Object paidRevenueShare;
		private Object action;
		public Object getValueDC() {
			return valueDC;
		}
		public void setValueDC(Object valueDC) {
			this.valueDC = valueDC;
		}
		public Object getValueEA() {
			return valueEA;
		}
		public void setValueEA(Object valueEA) {
			this.valueEA = valueEA;
		}
		public Object getValueED() {
			return valueED;
		}
		public void setValueED(Object valueED) {
			this.valueED = valueED;
		}
		public Object getValueID() {
			return valueID;
		}
		public void setValueID(Object valueID) {
			this.valueID = valueID;
		}
		public Object getValueOH() {
			return valueOH;
		}
		public void setValueOH(Object valueOH) {
			this.valueOH = valueOH;
		}
		public Object getValueRA() {
			return valueRA;
		}
		public void setValueRA(Object valueRA) {
			this.valueRA = valueRA;
		}
		public Object getValueRC() {
			return valueRC;
		}
		public void setValueRC(Object valueRC) {
			this.valueRC = valueRC;
		}
		public Object getValueRH() {
			return valueRH;
		}
		public void setValueRH(Object valueRH) {
			this.valueRH = valueRH;
		}
		public Object getValueTD() {
			return valueTD;
		}
		public void setValueTD(Object valueTD) {
			this.valueTD = valueTD;
		}
		public Object getValueWH() {
			return valueWH;
		}
		public void setValueWH(Object valueWH) {
			this.valueWH = valueWH;
		}
		public Object getDoubleOverTime() {
			return doubleOverTime;
		}
		public void setDoubleOverTime(Object doubleOverTime) {
			this.doubleOverTime = doubleOverTime;
		}
		public Object getOverTime() {
			return overTime;
		}
		public void setOverTime(Object overTime) {
			this.overTime = overTime;
		}
		public Object getRegularHours() {
			return regularHours;
		}
		public void setRegularHours(Object regularHours) {
			this.regularHours = regularHours;
		}
		public Object getPaidRevenueShare() {
			return paidRevenueShare;
		}
		public void setPaidRevenueShare(Object paidRevenueShare) {
			this.paidRevenueShare = paidRevenueShare;
		}
		public Object getAction() {
			return action;
		}
		public void setAction(Object action) {
			this.action = action;
		} 
	}
	
	public List findPaychexExtractList(String beginDates, String endDates, String corpId, String hub) {
		List <Object> paychexExtract = new ArrayList<Object>(); 
		String query=" SELECT crew.`warehouse` AS WH,crew.`employeeId` AS ID,CONCAT('0',timesheet.`distributionCode`,lpad(crew.`warehouse`,2,'0')) AS DC,"+
                 " IF(timesheet.`action` not IN('H','P','S','V','BE') ,round(sum(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`regularHours`,1))),2),'0') AS RH,"+
                 " round(sum(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`OverTime`,1))),2) AS OH,"+
                 " if(sum(IF((timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`)<>0,(timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`),0))>0,'RS','') AS ED,"+
                 " round(sum(IF((timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`)<>0,(timesheet.`paidRevenueShare`+timesheet.`adjustmentToRevenue`),0)),2) AS EA,"+
                 " IF((IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`doubleOverTime`,1))) > 0,'D',IF(timesheet.`action` IN('H','P','S','V','BE'),timesheet.`action`,if(timesheet.`action`=' RS ','BE','') ) ) AS TD,"+
                 " round(sum(     IF((IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`doubleOverTime`,1))) > 0,timesheet.`doubleOverTime`,      IF(timesheet.`action` IN('H','P','S','V','BE'),timesheet.`regularHours`,         if(timesheet.`action`=' RS ',timesheet.`regularHours`,'') ) )     )) AS RA,"+
                 " IF(timesheet.`differentPayBasis`='B'and(IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`regularHours`,1))+IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`OverTime`,1))+IF(timesheet.`paidRevenueShare`>0,0,ROUND(timesheet.`doubleOverTime`,1)))>0,2,0) AS RC,"+
                 " sum(IF((timesheet.regularHours is null or timesheet.regularHours=''),'0',timesheet.regularHours)) AS regularHours,"+
                 " sum(IF((timesheet.overTime is null or timesheet.overTime=''),'0',timesheet.overTime)) AS overTime,"+
                 " sum(IF((timesheet.doubleOverTime is null or timesheet.doubleOverTime=''),'0',timesheet.doubleOverTime)) AS doubleOverTime,"+
                 " sum(IF((timesheet.paidRevenueShare is null or timesheet.paidRevenueShare=''),'0',timesheet.paidRevenueShare)) AS paidRevenueShare, timesheet.`action` as action"+
                 " FROM `crew` crew LEFT OUTER JOIN `timesheet` timesheet ON crew.`userName` = timesheet.`userName`"+
                 " LEFT OUTER JOIN `workticket` workticket ON timesheet.`ticket` = workticket.`ticket`"+ 
                 " INNER JOIN refmaster ref ON crew.warehouse = ref.code and ref.flex3 = '"+hub+"' and parameter = 'HOUSE' and ref.corpid = '"+corpId+"'"+ 
                 " INNER JOIN refmaster refp ON refp.parameter = 'PAYROLL' and refp.corpid = '"+corpId+"' and refp.code = timesheet.`distributioncode`"+
                 " WHERE  date_format(timesheet.workDate,'%Y-%m-%d') >= '"+beginDates+"' AND date_format(timesheet.workDate,'%Y-%m-%d') <= '"+endDates+"'"+
                 " AND timesheet.`corpID` = '"+corpId+"' AND timesheet.`distributionCode` <> '999' "+
                 " and timesheet.action <> 'L' and crew.corpid = '"+corpId+"' and (workticket.corpid = '"+corpId+"' or  workticket.corpid is null )  group by crew.`employeeId`, timesheet.`distributionCode`,timesheet.`action`";
	//System.out.println("\n\n\n\n\nsdbfjjsdhfh >>>>>>>>>>>>> "+query);
		List payrollExtractList= this.getSession().createSQLQuery(query)  
		.addScalar("WH", Hibernate.STRING)  
		.addScalar("ID", Hibernate.STRING) 
		.addScalar("DC", Hibernate.STRING)
		.addScalar("RH", Hibernate.STRING) 
		.addScalar("OH", Hibernate.STRING)
		.addScalar("ED", Hibernate.STRING)
		.addScalar("EA", Hibernate.STRING) 
		.addScalar("TD", Hibernate.STRING) 
		.addScalar("RA", Hibernate.STRING)  
		.addScalar("RC", Hibernate.STRING) 
		.addScalar("regularHours", Hibernate.STRING) 
		.addScalar("overTime", Hibernate.STRING) 
		.addScalar("doubleOverTime", Hibernate.STRING) 
		.addScalar("paidRevenueShare", Hibernate.STRING) 
		.addScalar("action", Hibernate.STRING) 
	    .list(); 
	
		Iterator iterator=payrollExtractList.iterator(); 
		while(iterator.hasNext())
	
		{
			Object []row= (Object [])iterator.next();
			DTOPaychexExtract dtoPaychexExtract = new DTOPaychexExtract();
			dtoPaychexExtract.setValueWH((row[0]));
			dtoPaychexExtract.setValueID((row[1]));
			dtoPaychexExtract.setValueDC((row[2]));
			dtoPaychexExtract.setValueRH(row[3]); 
			dtoPaychexExtract.setValueOH(row[4]);
			dtoPaychexExtract.setValueED(row[5]);
			dtoPaychexExtract.setValueEA((row[6]));
			dtoPaychexExtract.setValueTD(row[7]); 
			dtoPaychexExtract.setValueRA(row[8]); 
			dtoPaychexExtract.setValueRC(row[9]); 
			dtoPaychexExtract.setRegularHours(row[10]); 
			dtoPaychexExtract.setOverTime(row[11]); 
			dtoPaychexExtract.setDoubleOverTime(row[12]); 
			dtoPaychexExtract.setPaidRevenueShare(row[13]); 
			dtoPaychexExtract.setAction(row[14]); 
			paychexExtract.add(dtoPaychexExtract); 
	
		}
	return paychexExtract;
		
	}

	public List getPkCodeList(String vlCode, String sessionCorpID) {
		String query="";
		query = "select partnerCode from partner where validNationalCode='"+vlCode+"' and corpid='"+sessionCorpID+"' and isOwnerOp is true";
		List list= this.getSession().createSQLQuery(query).list();	
    	return list;
	}
	public void updateSickPersonalDay(){
		getHibernateTemplate().bulkUpdate("update Payroll set personalDayUsed='0.0' where unionName is not null and corpid='SSCW'");	
	}
	public List getAllCrewName(String sessionCorpID){
		String query="";
		query="select username from crew where corpid='"+sessionCorpID+"' and active='true' order by username";
		List list= this.getSession().createSQLQuery(query).list();	
    	return list;
	}
	public List findPayrollWorkDayExtractList(String beginDates,String endDates,String corpId, String hub){

		List <Object> payrollExtract = new ArrayList<Object>();
		int taxFrequencyDay=0;
		List getDiff = this.getSession().createSQLQuery("SELECT DATEDIFF('"+endDates+"', '"+beginDates+"')").list();	
		Long diff = Long.parseLong(getDiff.get(0).toString());
		if(diff<=7){
			taxFrequencyDay=1;
		}else{
			taxFrequencyDay=2;
		}
		
		String query="";	
			 query="SELECT timesheet.id as Id," +
						" crew.employeeId AS employeeId," +
						" crew.firstName As firstName," +
						" crew.lastName As lastName,"
						+ " '' as custFirst,'' as custLast,"
						+ " workticket.shipnumber as orderNum,"
						+ " timesheet.ticket as msPriKey,'' as serviceDate,"
						+ " '' as serviceId,'' as jobId, workticket.service as jobDescription,"
						+ " concat(date_format(timesheet.workDate,'%m/%d/%Y'), ' ', timesheet.beginHours) as punchTime,"
						+ " 'Punched In' as punchType, '' as punchDescription, '' as timeZone,'' as region,"
						+ "concat(date_format(timesheet.workDate,'%m/%d/%Y'), ' ', timesheet.beginHours) as jobStart, "
						+ "concat(date_format(timesheet.workDate,'%m/%d/%Y'), ' ', timesheet.beginHours) as jobEnd,"
						+ "concat(date_format(timesheet.workDate,'%m/%d/%Y'), ' ', timesheet.beginHours) as estJobStart,"
						+ "concat(date_format(timesheet.workDate,'%m/%d/%Y'), ' ', timesheet.beginHours) as estJobEnd,"
						+ "concat(date_format(timesheet.workDate,'%m/%d/%Y'), ' ', timesheet.beginHours) as roundedStart,"
						+ "concat(date_format(timesheet.workDate,'%m/%d/%Y'), ' ', timesheet.beginHours) as roundedEnd,"
						+ "timesheet.ticket as lsPriKey,'FALSE' as active,"
						+ "'0' as punchInID,"
						+ "'TRUE' as CTAPConfirmed,"
						+ "'TRUE' as MVPConfirmed,"
						+ " '' as comments,"
						+ "date_format(timesheet.createdOn,'%m/%d/%Y %H:%i') as systemDate,"
						+ "'SUCCESS' as WDPunchResponse,"
						+ "timesheet.crewType as laborType,"
						+ "'SUCCESS' as MSPunchResponse,"
						+ " if(length(timesheet.warehouse)=1,concat('##',timesheet.warehouse),if(length(timesheet.warehouse)=2,concat('#',timesheet.warehouse),timesheet.warehouse)) as branch,concat(date_format(timesheet.workDate,'%m/%d/%Y'), ' ', timesheet.endHours) as punchOutTime " +
						" FROM `crew` crew LEFT OUTER JOIN `timesheet` timesheet ON crew.`userName` = timesheet.`userName` LEFT OUTER JOIN `workticket` workticket ON timesheet.`ticket` = workticket.`ticket` LEFT OUTER JOIN `payroll` payroll ON timesheet.`distributioncode` = payroll.`code` and  payroll.corpID='"+corpId+"' " +
						" INNER JOIN refmaster ref ON crew.warehouse = ref.code and ref.parameter = 'HOUSE' and ref.corpid = '"+corpId+"'  " +
						" WHERE date_format(timesheet.workDate,'%Y-%m-%d') >= '"+beginDates+"' AND date_format(timesheet.workDate,'%Y-%m-%d') <= '"+endDates+"' AND timesheet.`corpID` = '"+corpId+"' AND timesheet.`distributionCode` <> '999' and timesheet.action <> 'L' and timesheet.beginHours<>'' and timesheet.endHours<>'' and crew.employeeId<>'' and (workticket.corpid = '"+corpId+"' or workticket.corpid is null) " +
						//" group by crew.`employeeId`, timesheet.`distributionCode`,timesheet.`action`,timesheet.`ticket` " +
						" order by crew.employeeId,timesheet.workDate ";
				 
		List payrollExtractList= this.getSession().createSQLQuery(query).list(); 
		Iterator iterator=payrollExtractList.iterator();	
		while(iterator.hasNext())
		{
			Object []row= (Object [])iterator.next();
			DTOPayrollWorkDayExtract dtoPayrollWorkDayExtract = new DTOPayrollWorkDayExtract();
			dtoPayrollWorkDayExtract.setId((row[0]));
			dtoPayrollWorkDayExtract.setEmployeeId((row[1]));
			dtoPayrollWorkDayExtract.setFirstName((row[2]));
			dtoPayrollWorkDayExtract.setLastName((row[3]));
			dtoPayrollWorkDayExtract.setCustFirst((row[4]));
			dtoPayrollWorkDayExtract.setCustLast((row[5]));			
			dtoPayrollWorkDayExtract.setOrderNum((row[6]));
			dtoPayrollWorkDayExtract.setMsPriKey((row[7]));
			dtoPayrollWorkDayExtract.setServiceDate((row[8]));
			dtoPayrollWorkDayExtract.setServiceId((row[9]));
			dtoPayrollWorkDayExtract.setJobId((row[10]));
			List l =  this.getSession().createSQLQuery("select description from refmaster where code='"+row[11]+"' and parameter='TCKTSERVC' and corpid='"+corpId+"'").list();
			if(l!=null && !l.isEmpty()){
				String s[] = l.get(0).toString().split(":");
				dtoPayrollWorkDayExtract.setJobDescription(s[1]);
			}
			else{dtoPayrollWorkDayExtract.setJobDescription("");}
			
			dtoPayrollWorkDayExtract.setPunchTime((row[12]));
			dtoPayrollWorkDayExtract.setPunchType((row[13]));
			dtoPayrollWorkDayExtract.setPunchDescription((row[14]));
			dtoPayrollWorkDayExtract.setTimeZone((row[15]));
			dtoPayrollWorkDayExtract.setRegion((row[16]));
			dtoPayrollWorkDayExtract.setJobStart((row[17]));
			dtoPayrollWorkDayExtract.setJobEnd((row[18]));
			dtoPayrollWorkDayExtract.setEstJobStart((row[19]));
			dtoPayrollWorkDayExtract.setEstJobEnd((row[20]));
			dtoPayrollWorkDayExtract.setRoundedStart((row[21]));
			dtoPayrollWorkDayExtract.setRoundedEnd((row[22]));
			dtoPayrollWorkDayExtract.setLsPriKey((row[23]));
			dtoPayrollWorkDayExtract.setActive((row[24]));
			dtoPayrollWorkDayExtract.setPunchInID((row[25]));
			dtoPayrollWorkDayExtract.setCTAPConfirmed((row[26]));
			dtoPayrollWorkDayExtract.setMVPConfirmed((row[27]));
			dtoPayrollWorkDayExtract.setComments((row[28]));
			dtoPayrollWorkDayExtract.setSystemDate((row[29]));
			dtoPayrollWorkDayExtract.setWDPunchResponse((row[30]));
			List l1 =  this.getSession().createSQLQuery("select description from refmaster where code='"+row[31]+"' and parameter='CREWTYPE' and corpid='"+corpId+"'").list();
			if(l1!=null && !l1.isEmpty()){
				dtoPayrollWorkDayExtract.setLaborType(l1.get(0).toString());
			}
			else{dtoPayrollWorkDayExtract.setLaborType("");}
			//dtoPayrollWorkDayExtract.setLaborType((row[31]));
			dtoPayrollWorkDayExtract.setMSPunchResponse((row[32]));
			dtoPayrollWorkDayExtract.setBranch((row[33]));
			dtoPayrollWorkDayExtract.setPunchOutTime((row[34]));
			payrollExtract.add(dtoPayrollWorkDayExtract);			
		}
		
		return payrollExtract;
	
	}

	public Map<String, String> getOpenCompnayDevisionCode(String corpId) {
		List<CompanyDivision> list;
		list= getHibernateTemplate().find("from CompanyDivision where corpID='"+corpId+"' and closeddivision is false ");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
	   	for(CompanyDivision companyDivision : list){
	   		parameterMap.put(companyDivision.getCompanyCode(), companyDivision.getDescription());
			}
	   	return  parameterMap;
	}
	
}