package com.trilasoft.app.dao;


import java.util.List;
import java.util.Set;


import java.util.List;


import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.AuthorizationNo;

public interface AuthorizationNoDao extends GenericDao<AuthorizationNo, Long>  {

	public List findByJobAuth(String authNo, String shipNumber);

	public List searchAccAuthorization(String shipNumber);
	public List searchAuthorizationNo(String shipNumber);
	public List findAuthorizationNo(String authNoInvoice);
	public List findMaximum(String shipNumber);
	public List findAuthNumber(String shipNumber, String sessionCorpID);
	
}
