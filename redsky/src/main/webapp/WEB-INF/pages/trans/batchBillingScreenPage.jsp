<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<title>Batch Billing Screen</title>
<STYLE type=text/css>
#loader {filter:alpha(opacity=70);-moz-opacity:0.7;-khtml-opacity: 0.7;opacity: 0.7;position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
} 
</style>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery.min.js"></script>
<!-- Modification closed here -->
   
   
<script type="text/javascript">
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
  var http2 = getHTTPObject();
  var httpTemplate = getHTTPObject();
  function openBookingAgentPopWindow(){ 
	  var companyDivs=document.forms['collectiveBilling'].elements['companyDivs'].value;
		javascript:openWindow("partnersPopup.html?partnerType=AC&onlyAC=y&flag=10&firstName=&lastName=&phone=&email=&compDiv="+companyDivs+"&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescriptionValidation&fld_description=vendorDescription&fld_code=billCode&fld_seventhDescription=");
		////document.forms['billingForm'].elements['billing.billToCode'].select();
	}
  function openBookingAgentPopWindow1(){
		javascript:openWindow("partnersPopup.html?partnerType=AC&onlyAC=y&flag=10&firstName=&lastName=&phone=&email=&compDiv=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescriptionValidation&fld_description=vendorDescription&fld_code=billCode&fld_seventhDescription=");
	}
  function findCollectiveBillToName(){
	    var billToCode = document.forms['collectiveBilling'].elements['billCode'].value;
	    billToCode=billToCode.trim();
	    if(billToCode==''){
	    	document.forms['collectiveBilling'].elements['vendorDescription'].value="";
	    }
	    var accountSearchValidation = document.forms['collectiveBilling'].elements['accountSearchValidation'].value;
	    if(billToCode!=''){
	    	showHide("block");
	    	document.forms['collectiveBilling'].elements['vendorDescription'].value="";
	    	<c:if test="${accountSearchValidation}">
	     	var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;
	     	</c:if> 
	    	<c:if test="${!accountSearchValidation}">
	     	var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;
	     	</c:if> 
	     	http2.open("GET", url, true);
	     	http2.onreadystatechange = function(){ handleHttpResponse2(billToCode);};
	     	http2.send(null);
	    }
	}
  function handleHttpResponse2(billToCode){
		if (http2.readyState == 4){
              var results = http2.responseText
              results = results.trim();
              var companyDivision=document.forms['collectiveBilling'].elements['companyDivs'].value; 
              var res = results.split("~"); 
                   if(res.length >= 2){ 
	         			if(res[5]== 'Approved'){
	         				if(res[10]== 'false'){
	         					showHide("none");
			           			document.forms['collectiveBilling'].elements['vendorDescription'].value = res[0];
	         				}else{
	         					showHide("none");
		         				alert("Please Select Other BillToCode, Selected Code Is MultiAuthorized.");
			           			document.forms['collectiveBilling'].elements['vendorDescription'].value="";
			           			document.forms['collectiveBilling'].elements['billCode'].value="";
			           			document.forms['collectiveBilling'].elements['vendorDescription'].focus();
			           			     				}
	         			 }else{
	         				showHide("none");
		           			alert("Bill To code not approved");
		           			document.forms['collectiveBilling'].elements['vendorDescription'].value="";
		           			document.forms['collectiveBilling'].elements['billCode'].value="";
		           			document.forms['collectiveBilling'].elements['vendorDescription'].focus();
					 	}
		     		}else{
		     			showHide("none");
	         			alert("Bill To code not valid");
	           			document.forms['collectiveBilling'].elements['vendorDescription'].value="";
	           			document.forms['collectiveBilling'].elements['billCode'].value="";
	           			document.forms['collectiveBilling'].elements['vendorDescription'].focus();
          			}
     		}
	}	
  
  function checkParnerCodeForInv(){
	  var billCode = document.getElementById("collectiveBilling_billCode").value;
	  if(billCode != ""){
      new Ajax.Request('/redsky/checkParnerCodeForInv.html?billCode='+billCode+'&decorator=simple&popup=true',
                 {
                   method:'get',
                   onSuccess: function(transport){
                     var response = transport.responseText || "no response text";
                     if(response.trim()=='true'){
                    	alert("Not able to generate an invoice for the chosen Bill To.");
                    	//document.getElementById("collectiveBilling_billCode").value="";
                    	//document.getElementById("collectiveBilling_vendorDescription").value="";
                     }else{
                    	 generateInvoice();
                     }
                   },
                   onFailure: function(){ 
                       }
                 });
  		}else{
		  alert('Please fill Bill To Code to continue.');
		  return false
	  }
  }  
  
  
  function checkBillToCodeForInv(){
	var billCode = document.getElementById("collectiveBilling_billCode").value;
	  if(billCode != ""){
	      new Ajax.Request('/redsky/checkParnerCodeForInv.html?billCode='+billCode+'&decorator=simple&popup=true',
	                 {
	                   method:'get',
	                   onSuccess: function(transport){
	                     var response = transport.responseText || "no response text";
	                     if(response.trim()=='true'){
	                        alert("Not able to generate an invoice for the chosen Bill To.");
	                        document.getElementById("collectiveBilling_billCode").value="";
	                        document.getElementById("collectiveBilling_vendorDescription").value="";
	                     }
	                   },
	                   onFailure: function(){ 
	                       }
	                 });
	  }
  } 
  
  function VATExluceValidation(id,vat,ship,accList){
	  if(document.getElementById(id).checked == true){
		  <c:if test="${systemDefaultVatCalculation == 'true'}">
		  	if(vat == 'YES'){
		  		alert('Please enter Receivable VAT in S/O '+ship+' for the Account line No. '+accList);
		  		document.getElementById(id).checked = false;
				return false;
		 	}
		  </c:if>
	  }
	  return true;
  }
  var globalVatMessage='';
  function VATExluceValidationForAll(id,vat,ship,accList){
		  <c:if test="${systemDefaultVatCalculation == 'true'}">
		  	if(vat == 'YES'){
		  		if(globalVatMessage==''){
		  			globalVatMessage='Please enter Receivable VAT in S/O '+ship+' for the Account line No. '+accList;
		  		}else{
		  			globalVatMessage+=', \n S/O '+ship+' for the Account line No. '+accList;
		  		}
				return false;
		 	}
		  </c:if>
	 
	  return true;
  }
function checkMandatoryField(){
	var billToCode=document.forms['collectiveBilling'].elements['billCode'].value;
	var description=document.forms['collectiveBilling'].elements['vendorDescription'].value;
	if(billToCode.trim()==""){
		 alert("Please enter all the mandatory fields.");
		 document.forms['collectiveBilling'].elements['billCode'].focus();
		 return false;
	}
	if(description.trim()==""){
		 return false;
	}	
}
function checkMandatoryField1(){
	var billToCode=document.forms['collectiveBilling'].elements['billCode'].value;
	var description=document.forms['collectiveBilling'].elements['vendorDescription'].value;
	var dmm=false;
	try{
		if(document.forms['collectiveBilling'].elements['dmm'].checked){
			dmm=true;
		}
	}catch(e){}
	if(billToCode.trim()=="" && dmm){
		 alert("Please enter all the mandatory fields.");
		 document.forms['collectiveBilling'].elements['billCode'].focus();
		 return false;
	}
	if(description.trim()=="" && dmm){
		 return false;
	}	
}
function enterNotAllow(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	if(keyCode==13)
	{
    	return false;
	}
	else
	{
    	return true;
	}
}

function checkInvStatusForPIR(){
	var billCode = document.getElementById("collectiveBilling_billCode").value;
	  if(billCode != ""){
	      new Ajax.Request('/redsky/checkParnerCodeForInv.html?billCode='+billCode+'&decorator=simple&popup=true',
	                 {
	                   method:'get',
	                   onSuccess: function(transport){
	                     var response = transport.responseText || "no response text";
	                     if(response.trim()=='true'){
	                        alert("Not able to generate an invoice for the chosen Bill To.");
	                        //document.getElementById("collectiveBilling_billCode").value="";
	                        //document.getElementById("collectiveBilling_vendorDescription").value="";
	                     }else{
	                    	 updatePaymentStatusToPIR(); 
	                     }
	                   },
	                   onFailure: function(){ 
	                       }
	                 });
	  }else{
		  alert('Please fill Bill To Code to continue.');
		  return false
	  }
  } 
function updatePaymentStatusToPIR(){
		var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
		 var aidList="";
		 var sidList="";
		    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
		        var obj = jsonPreviewInvoiceLineList[i];
				var sid=obj['accountId'];	
				aidList=obj['accIdList'];	
				if(document.getElementById(sid).checked){
					if(sidList==""){
						sidList=aidList;
					}else{
						sidList=sidList+","+aidList;
					}
				}
		    }
	  var pir='';
	  try{
		  pir=document.forms['collectiveBilling'].elements['pir'].value;
	  }catch(e){}
	  if(pir==null || pir==undefined || pir==""){
		  pir='';
	  }	  
	  if(pir==""){
		  alert("Please enter PIR Number.");
	  }else if(sidList.trim()==""){
		  alert("Please select at least one service order.");
	  }else{
				if(sidList!=''){
				var companyDivs=document.forms['collectiveBilling'].elements['companyDivs'].value;
				if(companyDivs==null || companyDivs==undefined ){
					companyDivs="";
				}
			 	var url="updatePaymentStatusToPIR.html?ajax=1&decorator=simple&popup=true&aidList="+encodeURI(sidList)+"&accCompanyDivision="+encodeURI(companyDivs)+"&pir="+encodeURI(pir);
			 	httpTemplate.open("GET", url, true);
				httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
				httpTemplate.send(null);
				}
	  }
}
function handleHttpResponseForTemplate(){
	if (httpTemplate.readyState == 4){
        alert('Account lines has been updated');
	}
}
function showAstrik(){
	<c:if test="${!utsiCheck}">
	var dmm34=false;
	try{
		if(document.forms['collectiveBilling'].elements['dmm'].checked){
			dmm34=true;
		}
	}catch(e){}
	var astr = document.getElementById('astr');
	if(dmm34){
		astr.style.display = 'block';
		astr.style.cssFloat="right";
	}else{
		astr.style.display = 'none';
	}
	</c:if>
}
function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
	lastName=lastName.replace("~","'");
	document.getElementById(partnerNameId).value=lastName;
	document.getElementById(paertnerCodeId).value=partnercode;
	document.getElementById(autocompleteDivId).style.display = "none";	
}
</script>
<meta name="heading"
	content="Invoice Processing" />
</head>
<div id="otabs" style="margin-bottom:23px;">
		  <ul>		  
		    <li ><a class="current"><span>Invoice Processing</span></a></li>
		  </ul>
		</div>
		<c:set var="batchInvoiceNonCMMDMM" value="false"/>
		<configByCorp:fieldVisibility componentId="component.field.BatchPayable.VoerCMMDMM">
		<c:set var="batchInvoiceNonCMMDMM" value="true"/>
		</configByCorp:fieldVisibility>
		<c:choose>
		<c:when test="${batchInvoiceNonCMMDMM==true || batchInvoiceNonCMMDMM=='true'}">
<s:form id="collectiveBilling" name="collectiveBilling" action="batchBillingScreen.html" onsubmit="return checkMandatoryField1();" method="post" >
<c:set var="newAccountline" value="N" />
<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
<c:set var="newAccountline" value="Y" />
</sec-auth:authComponent>
  <s:hidden name="secondDescription" />
  <s:hidden name="secondDescriptionValidation" />
  <s:hidden name="description" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription"/>
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<s:hidden name="seventhDescription" /> 
	<s:hidden name="htFlag" value="0"/>
	<s:hidden name="selectCurrency" value=""/>
	<s:hidden name="collectiveInvoiceSoId" value="" />
	<s:hidden name="batchInvoiceNonCMMDMMFlag" value="YES" />
	<s:hidden name="shipNumberForTickets"/>
	<s:hidden name="serviceOrderId" value="" /> 
	<s:hidden name="accountSearchValidation" /> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>	
<div id="content" align="center" style="width:100%">
<div id="liquid-round-top">
    <div class="top" ><span></span></div>
      <div class="center-content">
      <table class="detailTabLabel" cellspacing="2" cellpadding="2" border="0" >
				<tbody>
					<tr>
					 <td align="right" width="100px" class="listwhitebox">Bill To Code<font color="red" size="2" id="astr">*</font></td>
				       <td width="118px"><s:textfield id="batchbillToCode" name="billCode" cssClass="input-text"  size="20" onblur="findCollectiveBillToName();" onchange="valid(this,'special');" onkeydown="return enterNotAllow(event);" cssStyle="width:142px;"/>
					  <img class="openpopup" style="vertical-align:top;" width="17" height="20" onclick="javascript:openBookingAgentPopWindow1();" src="<c:url value='/images/open-popup.gif'/>" /></td>
					 <td align="right" width="105px" class="listwhitebox">Bill To Name</td>
					 <c:if test="${accountSearchValidation}">
					<td><s:textfield id="batchbillNameId" name="vendorDescription" cssClass="input-text" onkeyup="findPartnerDetails('batchbillNameId','batchbillToCode','batchBillNameDivId',' and (isAccount=true or isPrivateParty=true)','${serviceOrder.companyDivision}',event);" onchange="findPartnerDetailsByName('batchbillNameId','batchbillToCode');"size="28"   cssStyle="width:190px;"/>
				      <div id="batchBillNameDivId" class="autocomplete"style="z-index: 9999; position: absolute; margin-top: 2px; left: 201px;"></div>
					 </td>
					  </c:if>	
					  <c:if test="${!accountSearchValidation}">
				      <td><s:textfield id="batchbillNameId" name="vendorDescription" cssClass="input-text" onkeyup="findPartnerDetails('batchbillNameId','batchbillToCode','batchBillNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true  or isCarrier=true  )','${serviceOrder.companyDivision}',event);"  onchange="findPartnerDetailsByName('batchbillNameId','batchbillToCode');" size="28"   cssStyle="width:190px;"/>
                     <div id="batchBillNameDivId" class="autocomplete"style="z-index: 9999; position: absolute; margin-top: 2px; left: 201px;"></div>
					 </td>
                   </c:if>
					<td align="right" width="100px" class="listwhitebox">Company Division</td>
					<td>
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
					<tr>
					<td><s:select cssClass="list-menu" name="companyDivs" list="%{companyDivis}" value="%{selectedcompanyDivsList}" multiple="true" headerKey="" headerValue="" cssStyle="width:170px; height:70px; overflow-y:scroll;" /></td>

 						</tr>
 						</table>
 					</td> 
 										 
					 <configByCorp:fieldVisibility componentId="component.field.BatchPayable.pir">
					 <td align="right" class="listwhitetext" width="100px">PIR</td>
					 <td align="left">
					 <s:textfield name="pir" cssClass="input-text"  size="15" onchange="" />
					 </td>
					 	</configByCorp:fieldVisibility>	
					 	<c:if test="${!utsiCheck}">
							 <td align="right" class="listwhitetext" width="100px">DMM</td>
							 <td align="left">
							 <c:set var="isDMM" value="false"/>
							<c:if test="${dmm}">
									<c:set var="isDMM" value="true"/>
							</c:if>						
							 <s:checkbox name="dmm" value="${isDMM}" fieldValue="true"  onclick="showAstrik();"/>
							 </td>
					 	</c:if>
					</tr>
					 <tr>
					<td align="right"  height="10"></td>
					</tr>
					<tr>
					<td align="right" class="listwhitebox">Job Type</td>
					<td colspan="1">
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
					<tr>
					<td><s:select cssClass="list-menu" name="batchJob" list="%{job}" value="%{selectedJobList}" multiple="true" headerKey="" headerValue="" cssStyle="width:170px; height:70px; overflow-y:scroll" /></td>

 						</tr>
 						</table>
 					</td>
					<td align="right" class="listwhitebox">Assignment</td>
					<td colspan="1">
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
					<tr>
					<td><s:select cssClass="list-menu" name="assignType" list="%{collectiveAssignmentTypes}" value="%{selectedAssignmentList}" multiple="true" headerKey="" headerValue="" cssStyle="width:195px; height:70px; overflow-y:scroll"/></td>

 						</tr>
 						</table>
 						</td>
 					</tr>
	 										<tr>
						<td align="right"  height="10"></td>
						</tr>
					<tr>
					  <td colspan="6" class="listwhitetext" align="left" style="font-size:11px; font-style: italic; padding-left:100px;"><!-- sandeep -->
   							<b>&nbsp; Use Control + mouse to select multiple Company Division/Job/Assignment Type</b>
 					  </td>					 		
					</tr>
					<tr>
					<td align="right"  height="10"></td>
					</tr>
				</tbody>
	  </table>    
    </div>
<div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>
</div>
<s:submit   cssClass="cssbutton" cssStyle="width:155px; height:25px;margin-left:25px;margin-top:20px;" key="Preview Invoice Lines" />
<input class="cssbutton" style="width:55px; height:25px; margin-top:20px;" type="button" value="Reset" onclick="clearFields();"/>
<!-- <div class="spnblk">&nbsp;</div> -->

<display:table name="previewAccountLines" class="table" requestURI="" id="previewAccountLines" style="width:100%;" >
<c:if test="${previewAccountLines!='[]' && previewAccountLines!=null}">
<display:column style="width:4%;padding:0px;text-align:left;" title="<input type='checkbox' id='selectall' style='margin-left:11px; vertical-align:middle;' name='selectall' onclick='selectAll1(this);'/>Select&nbsp;All" media="html" >
<c:if test="${dmm==null || dmm!=true}"> 
<c:if test="${previewAccountLines.auditComplete==null || previewAccountLines.auditComplete=='' || fn1:contains( doNotInvoicePartnerList, previewAccountLines.billToCode )}"> 
<input align="right" type="checkbox" style="margin-left:16px;" id="${previewAccountLines.accountId}"  disabled="disabled"/>
</c:if>
<c:if test="${previewAccountLines.auditComplete!=null && previewAccountLines.auditComplete!='' && !(fn1:contains( doNotInvoicePartnerList, previewAccountLines.billToCode ))}"> 
<input align="right" type="checkbox" style="margin-left:16px;" id="${previewAccountLines.accountId}" onclick="updateGenrateInvoiceList1('${previewAccountLines.job}','${previewAccountLines.totalActualRevenue}','${previewAccountLines.distributionAmount}',this,'${previewAccountLines.recRateCurrency}','${previewAccountLines.shipNumber}','${previewAccountLines.accountId}','${previewAccountLines.VATExclude}','${previewAccountLines.accLineList}','${previewAccountLines.recGlListMissing}','${previewAccountLines.id}','${previewAccountLines.accountLineIdList}');" />
</c:if>
</c:if>
<c:if test="${dmm!=null && dmm==true}">
<input align="right" type="checkbox" style="margin-left:16px;" id="${previewAccountLines.accountId}" onclick="updateGenrateInvoiceList1('${previewAccountLines.job}','${previewAccountLines.totalActualRevenue}','${previewAccountLines.distributionAmount}',this,'${previewAccountLines.recRateCurrency}','${previewAccountLines.shipNumber}','${previewAccountLines.accountId}','${previewAccountLines.VATExclude}','${previewAccountLines.accLineList}','${previewAccountLines.recGlListMissing}','${previewAccountLines.id}','${previewAccountLines.accountLineIdList}');" />
</c:if>
</display:column>
</c:if>
<display:column title="#" style="width:1%">
<c:out value="${previewAccountLines_rowNum}"/>
</display:column>
<c:choose>
<c:when test="${newAccountline=='Y'}">
<display:column sortable="true" sortProperty="shipNumber" title="Ship Number" style="width:4%" ><a href="pricingList.html?sid=${previewAccountLines.id}" target="_blank" ><c:out value="${previewAccountLines.shipNumber}"></c:out></a></display:column>    
</c:when>
<c:otherwise>
<display:column sortable="true" sortProperty="shipNumber" title="Ship Number" style="width:4%" ><a href="accountLineList.html?sid=${previewAccountLines.id}" target="_blank" ><c:out value="${previewAccountLines.shipNumber}"></c:out></a></display:column>
</c:otherwise>
</c:choose>   
  <display:column property="name" sortable="true" title="Shipper"  style="width:9%"/>
  <display:column property="billToCode" sortable="true" title="Bill To Code"  style="width:5%"/>
  <display:column property="billToName" sortable="true" title="Bill To Name"  style="width:8%"/>
  <display:column property="companyDivision" sortable="true" title="Company Division"  style="width:5%"/>
 <configByCorp:fieldVisibility componentId="component.field.BatchPayable.OCountryDCountryVAT">
  <display:column property="originCountry" sortable="true" title="Origin Country"  style="width:9%"/>
  <display:column property="destinationCountry" sortable="true" title="Destination Country"  style="width:5%"/>
  <display:column property="vatDescription" sortable="true" title="VAT Descr"  style="width:8%"/>
  </configByCorp:fieldVisibility>
  <display:column sortProperty="totalActualRevenue" headerClass="containeralign" sortable="true" title="Invoice Amount" style="width:6%;">
  <div align="left" style="margin:0px; text-align:right;width:100%;">
  <fmt:formatNumber type="number" maxFractionDigits="2"  minFractionDigits="2" groupingUsed="true" value="${previewAccountLines.totalActualRevenue}" />
  </div>
  </display:column>
  <display:column property="recRateCurrency" sortable="true" title="Invoice Currency"  style="width:4%"/>
</display:table>
<c:if test="${previewAccountLines!='[]' && previewAccountLines!=null}">
<input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:110px; height:25px;margin-left:25px;" value="Generate Invoice" onclick="generateInvoice();" />
</c:if>
<configByCorp:fieldVisibility componentId="component.field.BatchPayable.pir">
<input type="button" class="cssbutton1" id="preInvoice" name="preInvoice" style="width:110px; height:25px;margin-left:25px;" value="Pre - Invoice" onclick="updatePaymentStatusToPIR();" />
</configByCorp:fieldVisibility>
<div id="loader" style="text-align:center; display:none">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tr>
			<td align="center">
				<table cellspacing="0" cellpadding="3" align="center">
					<tr><td height="200px"></td></tr>
					<tr>
				       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
				           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
				       </td>
				    </tr>
				    <tr>
				      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
				           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
				       </td>
				    </tr>
		       </table>
		     </td>
	  	</tr>
	</table>
</div>
</s:form>		
		</c:when>
		<c:otherwise>
<s:form id="collectiveBilling" name="collectiveBilling" action="batchBillingScreen.html" onsubmit="return checkMandatoryField();" method="post" >
  <s:hidden name="secondDescription" />
  <s:hidden name="secondDescriptionValidation" />
  <s:hidden name="description" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription"/>
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<s:hidden name="seventhDescription" /> 
	<s:hidden name="htFlag" value="0"/>
	<s:hidden name="selectCurrency" value=""/>
	<s:hidden name="collectiveInvoiceSoId" value="" />
	<s:hidden name="shipNumberForTickets"/>
	<s:hidden name="serviceOrderId" value="" /> 
	<s:hidden name="accountSearchValidation" /> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/> 
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>	
<div id="content" align="center" style="width:100%">
<div id="liquid-round-top">
    <div class="top" ><span></span></div>
      <div class="center-content">
      <table class="detailTabLabel" cellspacing="2" cellpadding="2" border="0" >
				<tbody>
					<tr>
					 <td align="right" class="listwhitebox">Bill To Code<font color="red" size="2">*</font></td>
					  <td ><s:textfield id="batchbillToCode" name="billCode" cssClass="input-text"  size="20" onblur="findCollectiveBillToName();" onchange="valid(this,'special');" onkeydown="return enterNotAllow(event);" cssStyle="width:142px;"/>
					  <img class="openpopup" style="vertical-align:top;" width="17" height="20" onclick="javascript:openBookingAgentPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
					 <td align="left" width="65px" class="listwhitebox">Bill To Name</td>
                     <c:if test="${accountSearchValidation}">
					<td><s:textfield id="batchbillNameId" name="vendorDescription" cssClass="input-text"  onkeyup="findPartnerDetails08('batchbillNameId','batchbillToCode','batchBillNameDivId',' and (isAccount=true  or isPrivateParty=true)',event);" size="28"  cssStyle="width:190px;"/>
					 <div id="batchBillNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
					 </td>
					  </c:if>	
					  <c:if test="${!accountSearchValidation}">
				      <td><s:textfield id="batchbillNameId" name="vendorDescription" cssClass="input-text" onkeyup="findPartnerDetails08('batchbillNameId','batchbillToCode','batchBillNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true  or isCarrier=true )',event);"  size="28"   cssStyle="width:190px;"/>
                     <div id="batchBillNameDivId" class="autocomplete"style="z-index: 9999; position: absolute; margin-top: 2px; left: 201px;"></div>
					 </td>
                   </c:if>					 
					 <td align="right" class="listwhitebox">Company Division</td>
					 <td><s:select name="companyDivs" list="%{companyDivis}" cssClass="list-menu"  cssStyle="width:140px" headerKey="" headerValue=""/></td>
					 <configByCorp:fieldVisibility componentId="component.field.BatchPayable.pir">
					 <td align="right" class="listwhitetext" width="100px">PIR</td>
					  <td align="left">
					 <s:textfield name="pir" cssClass="input-text"  size="15" onchange="" />
					 </td>
					 	</configByCorp:fieldVisibility>	
					 	<c:if test="${!utsiCheck}">
							 <td align="right" class="listwhitetext" width="100px">DMM</td>
							 <td align="left">
							 <c:set var="isDMM" value="false"/>
							<c:if test="${dmm}">
									<c:set var="isDMM" value="true"/>
							</c:if>						
							 <s:checkbox name="dmm" value="${isDMM}" fieldValue="true"  />
							 </td>
					 	</c:if>
					</tr>
					 <tr>
					<td align="right"  height="10"></td>
					</tr>
					<tr>
					<td align="right" class="listwhitebox">Job Type</td>
					<td colspan="3">
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
					<tr>
					<td><s:select cssClass="list-menu" name="batchJob" list="%{job}" value="%{selectedJobList}" multiple="true" headerKey="" headerValue="" cssStyle="width:170px; height:80px" /></td>

 						</tr>
 						</table>
 					</td>
					<td align="right" class="listwhitebox">Assignment</td>
					<td colspan="1">
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
					<tr>
					<td><s:select cssClass="list-menu" name="assignType" list="%{collectiveAssignmentTypes}" value="%{selectedAssignmentList}" multiple="true" headerKey="" headerValue="" cssStyle="width:140px; height:80px"/></td>

 						</tr>
 						</table>
 						</td>
										 <td align="right" class="listwhitetext">Billing&nbsp;Currency</td>
										 <td align="left"><s:select  cssClass="list-menu"  name="billingCurrencyType" cssStyle="width:60px" list="%{country}" headerKey="" onchange="" headerValue="" tabindex="12"  /></td>
 						
 					</tr>
	 										<tr>
						<td align="right"  height="10"></td>
						</tr>
					<tr>
					  <td colspan="6" class="listwhitetext" align="center" style="font-size:11	px; font-style: italic;">
   							<b>&nbsp; Use Control + mouse to select multiple Job/Assignment Type</b>
 					  </td>					 		
					</tr>
					<tr>
					<td align="right"  height="10"></td>
					</tr>
				</tbody>
	  </table>    
    </div>
<div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>
</div>
<s:submit   cssClass="cssbutton" cssStyle="width:155px; height:25px;margin-left:25px;margin-top:10px;" key="Preview Invoice Lines" />
<input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onclick="clearFields();"/>
<div class="spnblk">&nbsp;</div>

<display:table name="previewAccountLines" class="table" requestURI="" id="previewAccountLines" style="width:100%;" >
<c:if test="${previewAccountLines!='[]' && previewAccountLines!=null}">
<display:column style="width:4%;padding:0px;text-align:left;" title="<input type='checkbox' id='selectall' style='margin-left:11px; vertical-align:middle;' name='selectall' onclick='recPayGlValidationAll(this);'/>Select&nbsp;All" media="html" > 
<c:if test="${fn1:contains( doNotInvoicePartnerList, previewAccountLines.billToCode ) }"> 
<input align="right" type="checkbox" style="margin-left:16px;" id="${previewAccountLines.accountId}" disabled="disabled"/>
</c:if>
<c:if test="${!( fn1:contains( doNotInvoicePartnerList, previewAccountLines.billToCode ))}"> 
<input align="right" type="checkbox" style="margin-left:16px;" id="${previewAccountLines.accountId}" onclick="recPayGlValidation('${previewAccountLines.job}','${previewAccountLines.totalActualRevenue}','${previewAccountLines.distributionAmount}',this,'${previewAccountLines.recRateCurrency}','${previewAccountLines.shipNumber}','${previewAccountLines.accountId}','${previewAccountLines.VATExclude}','${previewAccountLines.accLineList}','${previewAccountLines.recGlListMissing}','${previewAccountLines.id}','${previewAccountLines.accountLineIdList}');" />
</c:if>
</display:column>
</c:if>
<display:column title="#" style="width:1%">
<c:out value="${previewAccountLines_rowNum}"/>
</display:column>
<c:choose>
<c:when test="${newAccountline=='Y'}">
<display:column sortable="true" sortProperty="shipNumber" title="Ship Number" style="width:4%" ><a href="pricingList.html?sid=${previewAccountLines.id}" target="_blank" ><c:out value="${previewAccountLines.shipNumber}"></c:out></a></display:column>    
</c:when>
<c:otherwise>
<display:column sortable="true" sortProperty="shipNumber" title="Ship Number" style="width:4%" ><a href="accountLineList.html?sid=${previewAccountLines.id}" target="_blank" ><c:out value="${previewAccountLines.shipNumber}"></c:out></a></display:column>    
 </c:otherwise>
 </c:choose>  
  <display:column property="name" sortable="true" title="Shipper"  style="width:9%"/>
  <display:column property="billToCode" sortable="true" title="Bill To Code"  style="width:5%"/>
  <display:column property="billToName" sortable="true" title="Bill To Name"  style="width:8%"/>
  <display:column property="companyDivision" sortable="true" title="Company Division"  style="width:5%"/>
 <configByCorp:fieldVisibility componentId="component.field.BatchPayable.OCountryDCountryVAT">
  <display:column property="originCountry" sortable="true" title="Origin Country"  style="width:9%"/>
  <display:column property="destinationCountry" sortable="true" title="Destination Country"  style="width:5%"/>
  <display:column property="vatDescription" sortable="true" title="VAT Descr"  style="width:8%"/>
  </configByCorp:fieldVisibility>
   <configByCorp:fieldVisibility componentId="component.field.BatchPayable.OCountryDCountryVAT">
<display:column  sortable="true" title="PIR" style="width:4%" >
        <c:if test="${previewAccountLines.paymentStatus==''}">
         </c:if>
           <c:if test = "${fn:contains(previewAccountLines.paymentStatus, 'PIR') && (!(fn:contains(previewAccountLines.paymentStatus, 'NoData')))}">
    <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
      </c:if>
      
   
     <c:if test = "${fn:contains(previewAccountLines.paymentStatus, 'PIR') && ((fn:contains(previewAccountLines.paymentStatus, 'NoData')))}">
         <img id="active" src="${pageContext.request.contextPath}/images/stop_round.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
        </c:if>
        </display:column>
 <display:column  sortable="true" title="Audit" style="width:4%" >
 
        <c:if test = "${fn:contains(previewAccountLines.auditCompleteDate, 'Found') && (!(fn:contains(previewAccountLines.auditCompleteDate, 'NoData')))}">
        <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
        </c:if>
        <c:if test = "${((fn:contains(previewAccountLines.auditCompleteDate, 'Found'))) && ((fn:contains(previewAccountLines.auditCompleteDate, 'NoData')))}">
        <img id="active" src="${pageContext.request.contextPath}/images/stop_round.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
        </c:if>
          <c:if test = "${(!(fn:contains(previewAccountLines.auditCompleteDate, 'Found'))) && ((fn:contains(previewAccountLines.auditCompleteDate, 'NoData')))}">
          <img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
        </c:if>
        </display:column>
        </configByCorp:fieldVisibility>
  <display:column sortProperty="totalActualRevenue" headerClass="containeralign" sortable="true" title="Invoice Amount" style="width:6%;">
  <div align="left" style="margin:0px; text-align:right;width:100%;">
  <fmt:formatNumber type="number" maxFractionDigits="2"  minFractionDigits="2" groupingUsed="true" value="${previewAccountLines.totalActualRevenue}" />
  </div>
  </display:column>
  <display:column sortProperty="totalActualRevenueForeign" headerClass="containeralign" sortable="true" title="Currency Amount" style="width:6%;">
  <div align="left" style="margin:0px; text-align:right;width:100%;">
  <fmt:formatNumber type="number" maxFractionDigits="2"  minFractionDigits="2" groupingUsed="true" value="${previewAccountLines.totalActualRevenueForeign}" />
  </div>
  </display:column>  
  <display:column property="recRateCurrency" sortable="true" title="Invoice Currency"  style="width:4%"/>
</display:table>
<c:if test="${previewAccountLines!='[]' && previewAccountLines!=null}">
<input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:110px; height:25px;margin-left:25px;" value="Generate Invoice" onclick="generateInvoice();" />
<configByCorp:fieldVisibility componentId="component.field.BatchPayable.pir">
<input type="button" class="cssbutton1" id="preInvoice" name="preInvoice" style="width:110px; height:25px;margin-left:25px;" value="Pre - Invoice" onclick="updatePaymentStatusToPIR();" />
</configByCorp:fieldVisibility>
</c:if>
<div id="loader" style="text-align:center; display:none">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tr>
			<td align="center">
				<table cellspacing="0" cellpadding="3" align="center">
					<tr><td height="200px"></td></tr>
					<tr>
				       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
				           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
				       </td>
				    </tr>
				    <tr>
				      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
				           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
				       </td>
				    </tr>
		       </table>
		     </td>
	  	</tr>
	</table>
</div>
</s:form>		
		</c:otherwise>
		</c:choose>
		<script type="text/javascript">
		function clearFields(){
			try{
				document.forms['collectiveBilling'].elements['billCode'].value = "";
				document.forms['collectiveBilling'].elements['vendorDescription'].value = "";
				document.forms['collectiveBilling'].elements['companyDivs'].value = "";
				<configByCorp:fieldVisibility componentId="component.field.BatchPayable.pir">
					document.forms['collectiveBilling'].elements['pir'].value = "";
				</configByCorp:fieldVisibility>
				document.forms['collectiveBilling'].elements['batchJob'].value = "";
				document.forms['collectiveBilling'].elements['assignType'].value = "";
				<c:if test="${!utsiCheck}">
					document.forms['collectiveBilling'].elements['dmm'].checked = false;
				</c:if>
				document.forms['collectiveBilling'].elements['billingCurrencyType'].value = "";
			}catch(e){}
		}
		function showHide(action){
			document.getElementById("loader").style.display = action;
		}
		  function recPayGlValidation(job,actualRevenue,distributionAmt,targetElement,recRateCurrency,snum,id,vat,accList,recGlList,soid,batchAccIdList){
			  var accNo='';
			  var accLine = [];
			  var temp=recGlList.split(",");
			  for(var l=0;l<temp.length;l++){
				  if(temp[l].indexOf('N')>-1){
					  accLine.push(temp[l].split("~")[0]);
				  }
			  }		
			  if(accLine.length > 0){
				  accLine.sort();
				  for (var j = 0; j < accLine.length; j++) {
					  if(accNo==''){
						  accNo=accLine[j];
					  }else{
						  accNo=accNo+", "+accLine[j];
					  }
					}
			  }
			  document.forms['collectiveBilling'].elements['shipNumberForTickets'].value=snum;
			  if(document.getElementById(id).checked == true && recGlList != null && recGlList!=undefined && recGlList.trim()!='' && recGlList.indexOf('N')>-1){
		  		alert('Please enter Receivable GL Code for '+snum+' for line #'+accNo);
		  		document.getElementById(id).checked = false;
				return false;
			  }else{
				  updateGenrateInvoiceList(job,actualRevenue,distributionAmt,targetElement,recRateCurrency,snum,id,vat,accList,recGlList,batchAccIdList);
				  document.forms['collectiveBilling'].elements['serviceOrderId'].value = soid;
			  }
		  }  
		
		 function updateGenrateInvoiceList(job,actualRevenue,distributionAmt,targetElement,recRateCurrency,snum,id,vat,accList,recGlList,batchAccIdList)  {
			  if(!VATExluceValidation(id,vat,snum,accList)){
				  return false;
			  }
			  var newIds=document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value;
			  var targetId=targetElement.id;
				var actualRevenueTemp=0;
				var distributionAmtTemp=0;
				var flag="notFound";
				var flag1="notFound";
				actualRevenueTemp=actualRevenue;
				actualRevenue = parseFloat(actualRevenue);
				distributionAmtTemp=distributionAmt;
				var flagSim="";
				var systemDefaultmiscVl="${systemDefaultmiscVl}";

						  if((targetElement.checked==true)){
									  if(systemDefaultmiscVl.indexOf(job)>-1){ 
							   		      if((actualRevenueTemp>distributionAmtTemp)||(actualRevenueTemp<distributionAmtTemp)){
							   		    	alert("Actual revenue total ("+ actualRevenueTemp +") and distribution amount total ("+distributionAmtTemp+") are not same. So, cannot generate invoice.")
							   		    	document.getElementById(targetId).checked=false;  
							   		      }else{
							   				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
							   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
							   			        var obj = jsonPreviewInvoiceLineList[i];

									  			var tempShip=obj['shipNumber'];
									  			var tempJob=obj['job'];
									  			var tempId1=obj['accountId'];
									  			var recRateCurrencyNew=obj['recRateCurrency'];
									  			var actualRevenueTemp1=0;
									  			var distributionAmtTemp1=0;
									  			actualRevenueTemp1=obj['totalActualRevenue'];
									  			distributionAmtTemp1=obj['distributionAmount'];
									  			 if(tempId1==id)
									  			 {
									  				var selectCurrency=document.forms['collectiveBilling'].elements['selectCurrency'].value;
									                 <c:if test="${multiCurrency=='Y'}"> 
											   		if(selectCurrency!=""){
												   		
												   		if(selectCurrency!=recRateCurrencyNew)  {
													   							   			flagSim="NotMatch";
													   	if(flag1=="notFound"){					   							   			
											   			alert("You cannot select service orders with different invoice currency. Please select service orders with same invoice currency.");
													   	}									   			
										   				var jsonPreviewInvoiceLineList1 = JSON.parse('${jsonPreviewInvoiceLine}');
										   			    for(var i=0;i<jsonPreviewInvoiceLineList1.length;i++){
										   			        var obj = jsonPreviewInvoiceLineList1[i];
											  			var tempShip1=obj['shipNumber'];
											  			var tempId11=obj['accountId'];
											  			 if(tempShip1.trim()==snum.trim())
											  			 {		
											   			document.getElementById(tempId11).checked=false;
											  			 }
										   			    }	
											   			flag="found";								   			
											   			flag1="found";
												   		}
												   		else{					   		flagSim="Match";				   		}
											   		}else{				   						   		flagSim="Match";			   		}

											   		
													</c:if>
							 	                    <c:if test="${multiCurrency!='Y'}"> 
							 	                   		flagSim="Match";
										            </c:if>
											   		if(flagSim=="Match"){									   									  				 
									  				 if(systemDefaultmiscVl.indexOf(tempJob)>-1){ 
									  					if((actualRevenueTemp1>distributionAmtTemp1)||(actualRevenueTemp1<distributionAmtTemp1)){
									  					}else{
										  					if(flag=="notFound")
											  				 { 								  					
												  			 var tempId=obj['id'];
												  			 var billCodeTemp=obj['billToCode'];
												  			 var companyDivTemp=obj['companyDivision'];
												  			 										  			 
													  		  if(newIds==''){
														  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
													  		  }
													  		  else{
														  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew)<0)
														  		  {
													  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
														  		  }
													  		  }
													  		  document.getElementById(tempId1).checked=true;  
													  		  document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
													  		document.forms['collectiveBilling'].elements['selectCurrency'].value=recRateCurrencyNew;
											  				 }
									  					}
									  				 }else{
									  					if(flag=="notFound")
										  				 { 
											  			 var tempId=obj['id'];
											  			var billCodeTemp=obj['billToCode'];
											  			var companyDivTemp=obj['companyDivision'];
		 									  			   if(newIds==''){
													  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
												  		  }
												  		  else{
													  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew)<0)
													  		  {
												  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
													  		  }
												  		  }
												  		  document.getElementById(tempId1).checked=true;  
												  		  document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
												  		  document.forms['collectiveBilling'].elements['selectCurrency'].value=recRateCurrencyNew;
									  				 }
									  				 }
											   		}
									  			 }
									  			 }
							   		      }
							   		  }else{	
							   				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
							   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
							   			        var obj = jsonPreviewInvoiceLineList[i];
								  			var tempShip=obj['shipNumber'];
								  			var tempJob=obj['job'];
								  			var tempId1=obj['accountId'];
								  			var recRateCurrencyNew=obj['recRateCurrency'];
								  			var actualRevenueTemp1=0;
								  			var distributionAmtTemp1=0;
								  			actualRevenueTemp1=obj['totalActualRevenue'];
								  			distributionAmtTemp1=obj['distributionAmount'];
								  			
								  			 if(tempId1==id)
								  			 {
								  				var selectCurrency=document.forms['collectiveBilling'].elements['selectCurrency'].value;
								                 <c:if test="${multiCurrency=='Y'}">
										   		if(selectCurrency!=""){
										   			
											   		if(selectCurrency!=recRateCurrencyNew)  {					   			flagSim="NotMatch";
												   	if(flag1=="notFound"){					   							   			
											   			alert("You cannot select service orders with different invoice currency. Please select service orders with same invoice currency.");
													   	}									   			
									   				var jsonPreviewInvoiceLineList1 = JSON.parse('${jsonPreviewInvoiceLine}');
									   			    for(var i=0;i<jsonPreviewInvoiceLineList1.length;i++){
									   			        var obj = jsonPreviewInvoiceLineList1[i];
										  			var tempShip1=obj['shipNumber'];
										  			var tempId11=obj['accountId'];
										  			 if(tempShip1.trim()==snum.trim())
										  			 {		
										   			document.getElementById(id).checked=false;
										  			 }
									   			    }
										   			flag="found";
										   			flag1="found";
											   		}
											   		else{					   		flagSim="Match";				   		}
										   		}else{				   						   		flagSim="Match";			   		}
												</c:if>
						 	                    <c:if test="${multiCurrency!='Y'}"> 
						 	                   		flagSim="Match";
									            </c:if>
										   		if(flagSim=="Match"){
								  				 
								  				 if(systemDefaultmiscVl.indexOf(tempJob)>-1){ 
								  					if((actualRevenueTemp1>distributionAmtTemp1)||(actualRevenueTemp1<distributionAmtTemp1)){
								  					}else{
									  					if(flag=="notFound")
										  				 { 							  					
											  			 var tempId=obj['id'];
											  			var billCodeTemp=obj['billToCode'];
											  			var companyDivTemp=obj['companyDivision'];
												  		  if(newIds==''){
													  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
												  		  }
												  		  else{
													  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew)<0)
													  		  {
												  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
													  		  }
												  		  }
												  		  document.getElementById(tempId1).checked=true;  
												  		  document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
												  		  document.forms['collectiveBilling'].elements['selectCurrency'].value=recRateCurrencyNew;
										  				 }
								  					}
								  				 }else{
									  					if(flag=="notFound")
										  				 { 
										  			 var tempId=obj['id'];
										  			var billCodeTemp=obj['billToCode'];
										  			var companyDivTemp=obj['companyDivision'];
											  			   if(newIds==''){
												  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
											  		  }
											  		  else{
												  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew)<0)
												  		  {
											  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
												  		  }
											  		  }
											  		  document.getElementById(tempId1).checked=true;  
											  		  document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
											  		  document.forms['collectiveBilling'].elements['selectCurrency'].value=recRateCurrencyNew;
										  				 }
								  				 }
								  			   }
								  			 }
							   			    }
											
							   		  }
							   		  checkAllFlag('checked');
						    }
						  
						   if((targetElement.checked==false) ){
						
						    var newIds=document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value;
			   				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
			   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
			   			        var obj = jsonPreviewInvoiceLineList[i];
							  			var tempShip=obj['shipNumber'];
							  			var tempId1=obj['accountId'];
							  			var sid=obj['id'];
							  			var billCodeTemp=obj['billToCode'];
							  			var companyDivTemp=obj['companyDivision'];
							  			var recRateCurrencyNew=obj['recRateCurrency'];
							  		     sid=sid+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
							  			 if(tempId1==id)
							  			 {
												
									    		if(newIds.indexOf(sid)>-1){
										  			  newIds=newIds.replace(sid,"");
										  			  newIds=newIds.replace(",,",",");
										  			 newIds=newIds.replace(",.00","");
										  			 if(newIds == ".00"){
										  				newIds=newIds.replace(".00",""); 
										  			 }
										  			  var len=newIds.length-1;
										  			  if(len==newIds.lastIndexOf(",")){
										  			  newIds=newIds.substring(0,len);
										  			  }
										  			  if(newIds.indexOf(",")==0){
										  			  newIds=newIds.substring(1,newIds.length);
										  			  }
										  			  document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
										  			 document.getElementById("selectall").checked=false;
									    		}
									    		if(newIds==""){
													   document.forms['collectiveBilling'].elements['selectCurrency'].value="";
													   document.getElementById("selectall").checked=false;  
									    		}							    								  			 
							  				document.getElementById(id).checked=false;  
							  			 }
							  			 }
							  				checkAllFlag('notchecked');
							    }
		 }
		  function updateGenrateInvoiceList1(job,actualRevenue,distributionAmt,targetElement,recRateCurrency,snum,id,vat,accList,recGlList,soid,batchAccIdList)  {
		  if(!VATExluceValidation(id,vat,snum,accList)){
			  return false;
		  }
		  document.forms['collectiveBilling'].elements['serviceOrderId'].value = soid;
		  document.forms['collectiveBilling'].elements['shipNumberForTickets'].value=snum;
		  var newIds=document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value;
		  var targetId=targetElement.id;
			var actualRevenueTemp=0;
			var distributionAmtTemp=0;
			var flag="notFound";
			var flag1="notFound";
			actualRevenueTemp=actualRevenue;
			actualRevenue = parseFloat(actualRevenue);
			distributionAmtTemp=distributionAmt;
			var flagSim="";
			var systemDefaultmiscVl="${systemDefaultmiscVl}";
				var dmm34=false;
					try{
						if(document.forms['collectiveBilling'].elements['dmm'].checked){
							dmm34=true;
						}
					}catch(e){}
					  if((targetElement.checked==true)){
								  if(systemDefaultmiscVl.indexOf(job)>-1){ 
						   		      if((actualRevenueTemp>distributionAmtTemp)||(actualRevenueTemp<distributionAmtTemp)){
						   		    	alert("Actual revenue total ("+ actualRevenueTemp +") and distribution amount total ("+distributionAmtTemp+") are not same. So, cannot generate invoice.")
						   		    	document.getElementById(targetId).checked=false;  
						   		      }else{
							   				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
							   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
							   			        var obj = jsonPreviewInvoiceLineList[i];
								  			var tempShip=obj['shipNumber'];
								  			var tempJob=obj['job']+'';
								  			var tempId1=obj['accountId'];
								  			var recRateCurrencyNew=obj['recRateCurrency'];
								  			var actualRevenueTemp1=0;
								  			var distributionAmtTemp1=0;
								  			actualRevenueTemp1=obj['totalActualRevenue'];
								  			distributionAmtTemp1=obj['distributionAmount'];
								  			 if(tempId1==id)
								  			 {
								  				var selectCurrency=document.forms['collectiveBilling'].elements['selectCurrency'].value;
								                 <c:if test="${multiCurrency=='Y'}"> 
										   		if(selectCurrency!=""){
											   		
											   		if(selectCurrency!=recRateCurrencyNew)  {
												   							   			flagSim="NotMatch";
												   	if(flag1=="notFound"){					   							   			
										   			alert("You cannot select service orders with different invoice currency. Please select service orders with same invoice currency.");
												   	}									   			
									   				var jsonPreviewInvoiceLineList1 = JSON.parse('${jsonPreviewInvoiceLine}');
									   			    for(var i=0;i<jsonPreviewInvoiceLineList1.length;i++){
									   			        var obj = jsonPreviewInvoiceLineList1[i];
										  			var tempShip1=obj['shipNumber'];
										  			var tempId11=obj['accountId'];
										  			 if(tempShip1.trim()==snum.trim())
										  			 {		
										   			document.getElementById(id).checked=false;
										  			 }
									   			    }	
										   			flag="found";								   			
										   			flag1="found";
											   		}
											   		else{					   		flagSim="Match";				   		}
										   		}else{				   						   		flagSim="Match";			   		}

										   		
												</c:if>
						 	                    <c:if test="${multiCurrency!='Y'}"> 
						 	                   		flagSim="Match";
									            </c:if>
										   		if(flagSim=="Match"){									   									  				 
								  				 if(systemDefaultmiscVl.indexOf(tempJob)>-1){ 
								  					if((actualRevenueTemp1>distributionAmtTemp1)||(actualRevenueTemp1<distributionAmtTemp1)){
								  					}else{
									  					if(flag=="notFound")
										  				 { 								  					
											  			 var tempId=obj['id'];
											  			 var billCodeTemp=obj['billToCode'];
											  			 var companyDivTemp=obj['companyDivision'];
											  			 if(!dmm34){
											  			var auditComplete=obj['auditComplete']; 	
											  			if(auditComplete!=null && auditComplete!=''){				  			 
												  		  if(newIds==''){
													  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
												  		  }
												  		  else{
													  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew)<0)
													  		  {
												  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
													  		  }
												  		  }
												  		  document.getElementById(tempId1).checked=true; 
											  			 } 
											  			 }else{
													  		  if(newIds==''){
														  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
													  		  }
													  		  else{
														  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew)<0)
														  		  {
													  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
														  		  }
													  		  }
													  		  document.getElementById(tempId1).checked=true; 
											  			 }
												  		  document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
												  		document.forms['collectiveBilling'].elements['selectCurrency'].value=recRateCurrencyNew;
										  				 }
								  					}
								  				 }else{
								  					if(flag=="notFound")
									  				 { 
										  			 var tempId=obj['id'];
										  			var billCodeTemp=obj['billToCode'];
										  			var companyDivTemp=obj['companyDivision'];
										  			if(!dmm34){
											  			var auditComplete=obj['auditComplete']; 	
											  			if(auditComplete!=null && auditComplete!=''){				  			 
											  			   if(newIds==''){
												  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
											  		  }
											  		  else{
												  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew)<0)
												  		  {
											  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
												  		  }
											  		  }
											  		  document.getElementById(tempId1).checked=true;  
											  			}
										  			}else{
											  			   if(newIds==''){
														  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
													  		  }
													  		  else{
														  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew)<0)
														  		  {
													  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
														  		  }
													  		  }
													  		  document.getElementById(tempId1).checked=true;  
										  			}
											  		  document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
											  		  document.forms['collectiveBilling'].elements['selectCurrency'].value=recRateCurrencyNew;
								  				 }
								  				 }
										   		}
								  			 }
							   			    }
						   		      }
						   		  }else{	
						   				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
						   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
						   			        var obj = jsonPreviewInvoiceLineList[i];
							  			var tempShip=obj['shipNumber'];
							  			var tempJob=obj['job']+'';
							  			var tempId1=obj['accountId'];
							  			var recRateCurrencyNew=obj['recRateCurrency'];
							  			var actualRevenueTemp1=0;
							  			var distributionAmtTemp1=0;
							  			actualRevenueTemp1=obj['totalActualRevenue'];
							  			distributionAmtTemp1=obj['distributionAmount'];
							  			
							  			 if(tempId1==id)
							  			 {
							  				var selectCurrency=document.forms['collectiveBilling'].elements['selectCurrency'].value;
							                 <c:if test="${multiCurrency=='Y'}">
									   		if(selectCurrency!=""){
									   			
										   		if(selectCurrency!=recRateCurrencyNew)  {					   			flagSim="NotMatch";
											   	if(flag1=="notFound"){					   							   			
										   			alert("You cannot select service orders with different invoice currency. Please select service orders with same invoice currency.");
												   	}									   			
								   				var jsonPreviewInvoiceLineList1 = JSON.parse('${jsonPreviewInvoiceLine}');
								   			    for(var i=0;i<jsonPreviewInvoiceLineList1.length;i++){
								   			        var obj = jsonPreviewInvoiceLineList1[i];
									  			var tempShip1=obj['shipNumber'];
									  			var tempId11=obj['accountId'];
									  			 if(tempShip1.trim()==snum.trim())
									  			 {		
									   			document.getElementById(id).checked=false;
									  			 }
								   			    }
									   			flag="found";
									   			flag1="found";
										   		}
										   		else{					   		flagSim="Match";				   		}
									   		}else{				   						   		flagSim="Match";			   		}
											</c:if>
					 	                    <c:if test="${multiCurrency!='Y'}"> 
					 	                   		flagSim="Match";
								            </c:if>
									   		if(flagSim=="Match"){
							  				 
							  				 if(systemDefaultmiscVl.indexOf(tempJob)>-1){ 
							  					if((actualRevenueTemp1>distributionAmtTemp1)||(actualRevenueTemp1<distributionAmtTemp1)){
							  					}else{
								  					if(flag=="notFound")
									  				 { 							  					
										  			 var tempId=obj['id'];
										  			var billCodeTemp=obj['billToCode'];
										  			var companyDivTemp=obj['companyDivision'];
										  			if(!dmm34){
											  			var auditComplete=obj['auditComplete']; 	
											  			if(auditComplete!=null && auditComplete!=''){				  			 
											  		  if(newIds==''){
												  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
											  		  }
											  		  else{
												  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew)<0)
												  		  {
											  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
												  		  }
											  		  }
											  		  document.getElementById(tempId1).checked=true;  
											  			}
										  			}else{
												  		  if(newIds==''){
													  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
												  		  }
												  		  else{
													  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew)<0)
													  		  {
												  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
													  		  }
												  		  }
												  		  document.getElementById(tempId1).checked=true;  
										  			}
											  		  document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
											  		  document.forms['collectiveBilling'].elements['selectCurrency'].value=recRateCurrencyNew;
									  				 }
							  					}
							  				 }else{
								  					if(flag=="notFound")
									  				 { 
									  			 var tempId=obj['id'];
									  			var billCodeTemp=obj['billToCode'];
									  			var companyDivTemp=obj['companyDivision'];
									  			if(!dmm34){
										  			var auditComplete=obj['auditComplete']; 	
										  			if(auditComplete!=null && auditComplete!=''){				  			 
										  			   if(newIds==''){
											  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
										  		  }
										  		  else{
											  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew)<0)
											  		  {
										  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
											  		  }
										  		  }
										  		  document.getElementById(tempId1).checked=true;  
										  			}
									  			}else{
										  			   if(newIds==''){
													  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
												  		  }
												  		  else{
													  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew)<0)
													  		  {
												  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+"~"+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
													  		  }
												  		  }
												  		  document.getElementById(tempId1).checked=true;  
										  			
									  			}
										  		  document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
										  		  document.forms['collectiveBilling'].elements['selectCurrency'].value=recRateCurrencyNew;
									  				 }
							  				 }
							  			   }
							  			 }
						   			    }
										
						   		  }
						   		  checkAllFlag('checked');
					    }
					  
					   if((targetElement.checked==false) ){
					
					    var newIds=document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value;
		   				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
		   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
		   			        var obj = jsonPreviewInvoiceLineList[i];
						  			var tempShip=obj['shipNumber'];
						  			var tempId1=obj['accountId'];
						  			var sid=obj['id'];
						  			var billCodeTemp=obj['billToCode'];
						  			var companyDivTemp=obj['companyDivision'];
						  			var recRateCurrencyNew=obj['recRateCurrency'];
						  			sid=sid+'~'+billCodeTemp+'~'+companyDivTemp+'~'+actualRevenue+'~'+batchAccIdList+'~'+recRateCurrencyNew;
						  			if(!dmm34){
							  			var auditComplete=obj['auditComplete']; 	
							  			if(auditComplete!=null && auditComplete!=''){				  			 
						  			 if(tempId1==id)
						  			 {
								    		if(newIds.indexOf(sid)>-1){
									  			  newIds=newIds.replace(sid,"");
									  			  newIds=newIds.replace(",,",",");
									  			  newIds=newIds.replace(",.00","");
                                                 if(newIds == ".00"){
                                                    newIds=newIds.replace(".00",""); 
                                                 }
									  			  var len=newIds.length-1;
									  			  if(len==newIds.lastIndexOf(",")){
									  			  newIds=newIds.substring(0,len);
									  			  }
									  			  if(newIds.indexOf(",")==0){
									  			  newIds=newIds.substring(1,newIds.length);
									  			  }
									  			  document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
									  			  document.getElementById("selectall").checked=false;
								    		}
								    		if(newIds==""){
												   document.forms['collectiveBilling'].elements['selectCurrency'].value="";
												   document.getElementById("selectall").checked=false;
								    		}							    								  			 
						  				document.getElementById(id).checked=false;  
						  				document.getElementById("selectall").checked=false;  
						  			 }
							  			}
						  			}else{
							  			 if(tempId1==id)
							  			 {
												
									    		if(newIds.indexOf(sid)>-1){
										  			  newIds=newIds.replace(sid,"");
										  			  newIds=newIds.replace(",,",",");
										  			  var len=newIds.length-1;
										  			  if(len==newIds.lastIndexOf(",")){
										  			  newIds=newIds.substring(0,len);
										  			  }
										  			  if(newIds.indexOf(",")==0){
										  			  newIds=newIds.substring(1,newIds.length);
										  			  }
										  			  document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
									    		}
									    		if(newIds==""){
													   document.forms['collectiveBilling'].elements['selectCurrency'].value="";
									    		}							    								  			 
							  				document.getElementById(id).checked=false;  
							  				document.getElementById("selectall").checked=false;  
							  			 }
						  			}
		   			    }
						  				checkAllFlag('notchecked');
						    }
		}
		  function checkAllFlag(target){
			  var checkAllF='N';
			  if(target=='notchecked'){		  
	   				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
	   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
	   			        var obj = jsonPreviewInvoiceLineList[i];
					var sid=obj['accountId'];		
					if(document.getElementById(sid).checked){
						checkAllF='Y'
					}
	   			    }
			  }else{
	   				var jsonPreviewInvoiceLineList1 = JSON.parse('${jsonPreviewInvoiceLine}');
	   			    for(var i=0;i<jsonPreviewInvoiceLineList1.length;i++){
	   			        var obj = jsonPreviewInvoiceLineList1[i];
					var sid=obj['accountId'];		
					if(!document.getElementById(sid).checked){
						checkAllF='Y'
					}
	   			    }
			  }
			  if((target=='notchecked') && (checkAllF=='N')){
				  document.getElementById('selectall').checked=false;
			  }
			  if((target!='notchecked') && (checkAllF=='N')){
				  document.getElementById('selectall').checked=true;
			  }	  
		  }
		  function selectAll1(evt){
			  var systemDefaultmiscVl="${systemDefaultmiscVl}";
				var dmm34=false;
				try{
					if(document.forms['collectiveBilling'].elements['dmm'].checked){
						dmm34=true;
					}
				}catch(e){}
			  if((evt.checked==true)){
				  var flag='YES';
				  var cur='';
				  var newIds='';
				  var flag2='';
				  var newIds1='';
				  globalVatMessage='';
		   			var jsonPreviewInvoiceLineList1 = JSON.parse('${jsonPreviewInvoiceLine}');
	   			    for(var i=0;i<jsonPreviewInvoiceLineList1.length;i++){
	   			        var obj = jsonPreviewInvoiceLineList1[i];
					if(flag!='NO'){
						var accountId=obj['accountId']+'';
						if(document.getElementById(accountId).disabled!=true){
						var job=obj['job'];
						var totalActualRevenue=obj['totalActualRevenue'];
						var distributionAmount=obj['distributionAmount'];
						var recRateCurrency=obj['recRateCurrency'];
						var shipNumber=obj['shipNumber'];
						
						var VATExclude=obj['VATExclude'];
						var accLineList=obj['accLineList'];
		  				var tempId=obj['id'];
			  			var billCodeTemp=obj['billToCode'];
			  			var companyDivTemp=obj['companyDivision'];
			  			var batchAccIdList=obj['accountLineIdList'];
			  			flag2='YES';  						
			  			if(document.getElementById(accountId).disabled!=true){
			  			if(!VATExluceValidationForAll(accountId,VATExclude,shipNumber,accLineList)){
							  flag='NO1';
							  flag2='NO'; 					
						}
			  			}
						<c:if test="${multiCurrency!='Y'}"> 
							if(systemDefaultmiscVl.indexOf(job)>-1){ 
					   		      if((totalActualRevenue>distributionAmount)||(totalActualRevenue<distributionAmount)){
									  flag='NO';
									  alert("Actual revenue total and distribution amount total are not same. So, cannot generate invoice.")
									  document.getElementById('selectall').checked=false;
					   		      }
							}		
						</c:if>
						if(cur!=''){
							if(cur!=recRateCurrency){
								 flag='NO';
								 alert("You cannot select service orders with different invoice currency. Please select service orders with same invoice currency.");
								 document.getElementById('selectall').checked=false;
							}
						}else{
							if(document.getElementById(accountId).disabled!=true){
							cur=recRateCurrency;
							}
						}
						if((flag!='NO')&&(flag2!='NO')){
							if(document.getElementById(accountId).disabled!=true){
							if(!dmm34){
					  			var auditComplete=obj['auditComplete']; 	
					  			if(auditComplete!=null && auditComplete!=''){				  			 
				  			   if(newIds==''){
						  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+totalActualRevenue+'~'+batchAccIdList+'~'+recRateCurrency;
					  		   }else{
						  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+totalActualRevenue+'~'+batchAccIdList+'~'+recRateCurrency)<0)
						  		  {
					  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+totalActualRevenue+'~'+batchAccIdList+'~'+recRateCurrency;
						  		  }
					  		   }
				  			   if(newIds1==''){
				  				 newIds1=accountId;
						  		   }else{
							  		  if(newIds1.indexOf(accountId)<0)
							  		  {
							  			newIds1=newIds1 + ',' + accountId;
							  		  }
						  		   }
					  			}
							}else{
					  			   if(newIds==''){
								  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+totalActualRevenue+'~'+batchAccIdList+'~'+recRateCurrency;
							  		   }else{
								  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+totalActualRevenue+'~'+batchAccIdList+'~'+recRateCurrency)<0)
								  		  {
							  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+totalActualRevenue+'~'+batchAccIdList+'~'+recRateCurrency;
								  		  }
							  		   }
						  			   if(newIds1==''){
						  				 newIds1=accountId+'';
								  		   }else{
									  		  if(newIds1.indexOf(accountId)<0)
									  		  {
									  			newIds1=newIds1 + ',' + accountId;
									  		  }
								  		   }
								
							}			  		   		  			
						}
						}
					}
					}		
	   			    }
				if((flag!='NO')&&(flag!='NO1')){
					document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
					document.forms['collectiveBilling'].elements['selectCurrency'].value=cur;		
	   				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
	   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
	   			        var obj = jsonPreviewInvoiceLineList[i];

				if(!dmm34){
		  			var auditComplete=obj['auditComplete']; 	
		  			if(auditComplete!=null && auditComplete!=''){				  			 
				var sid=obj['accountId']; 
				if(document.getElementById(sid).disabled!=true){
				document.getElementById(sid).checked=true;
				}
		  			}
				}else{
					var sid=obj['accountId'];
					
					if(document.getElementById(sid).disabled!=true){
					document.getElementById(sid).checked=true;	
					}
				}
	   			    }
				}else if((flag!='NO')&&(flag=='NO1')){
					alert(globalVatMessage);
					document.getElementById('selectall').checked=false;
					if(newIds1!=''){
						document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
						document.forms['collectiveBilling'].elements['selectCurrency'].value=cur;										
		   				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
		   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
		   			        var obj = jsonPreviewInvoiceLineList[i];

						if(!dmm34){
				  			var auditComplete=obj['auditComplete']; 	
				  			if(auditComplete!=null && auditComplete!=''){				  			 
						var sid=obj['accountId']+'';
							if(newIds1.indexOf(sid)>-1){
								
							if(document.getElementById(sid).disabled!=true){
								document.getElementById(sid).checked=true;
							}
							}
				  			}
						}else{
							var sid=obj['accountId']+'';
							if(newIds1.indexOf(sid)>-1){
								
						if(document.getElementById(sid).disabled!=true){
							document.getElementById(sid).checked=true;
								}
							}
						}
		   			    }	
					}			
				}else if((flag=='NO')&&(flag!='NO1')){			
				}else{
				}
			  }else{
					document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value='';
					document.forms['collectiveBilling'].elements['selectCurrency'].value='';		
	   				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
	   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
	   			        var obj = jsonPreviewInvoiceLineList[i];

				if(!dmm34){
		  			var auditComplete=obj['auditComplete']; 	
		  			if(auditComplete!=null && auditComplete!=''){				  			 
				var sid=obj['accountId'];
				if(document.getElementById(sid).disabled!=true){
				document.getElementById(sid).checked=false;
				}
		  			}
				}else{
					var sid=obj['accountId'];
					if(document.getElementById(sid).disabled!=true){
					document.getElementById(sid).checked=false;		
					}
				}
	   			    }
			  }
		  } 
		 
		  function recPayGlValidationAll(evt){
				var jsonPreviewInvoiceLineList1 = JSON.parse('${jsonPreviewInvoiceLine}');
				var accNo='';
				 var accLine = [];
				 var globalRecGlMessage='';  
   			    for(var i=0;i<jsonPreviewInvoiceLineList1.length;i++){
   			        var obj = jsonPreviewInvoiceLineList1[i];
					var shipNumber=obj['shipNumber'];
					var recGlListMissing=obj['recGlListMissing'];
					  var temp=recGlListMissing.split(",");
					  accNo='';
					  accLine = [];
					  for(var l=0;l<temp.length;l++){
						  if(temp[l].indexOf('N')>-1){
							  accLine.push(temp[l].split("~")[0]);
						  }
					  }
					  if(accLine.length > 0){
						  accLine.sort();
						  for (var j = 0; j < accLine.length; j++) {
							  if(accNo==''){
								  accNo=accLine[j];
							  }else{
								  accNo=accNo+", "+accLine[j];
							  }
							}
					  } 	  					  
					  if(recGlListMissing.indexOf("N")>-1){
				  		if(globalRecGlMessage=='' ){
				  			globalRecGlMessage='Please enter Receivable GL Code for\n '+shipNumber+' for line #'+accNo;
				  		}else{
				  			globalRecGlMessage+=',\n '+shipNumber+' for line #'+accNo;
				  		}		
					  }			  			  					
   			    }
			  if(evt.checked==true && globalRecGlMessage!=''){
				  alert(globalRecGlMessage);
				  document.getElementById('selectall').checked=false;
				  return false;
				}else{
					selectAll(evt);
				}
		  }  
		  function selectAll(evt){
			  var systemDefaultmiscVl="${systemDefaultmiscVl}";
			  if((evt.checked==true)){
				  var flag='YES';
				  var cur='';
				  var newIds='';
				  var flag2='';
				  var newIds1='';
				  globalVatMessage=''
		   				var jsonPreviewInvoiceLineList1 = JSON.parse('${jsonPreviewInvoiceLine}');
	   			    for(var i=0;i<jsonPreviewInvoiceLineList1.length;i++){
	   			        var obj = jsonPreviewInvoiceLineList1[i];
					if(flag!='NO'){
						var accountId=obj['accountId'];
						if(document.getElementById(accountId).disabled!=true){
						var job=obj['job'];
						var totalActualRevenue=obj['totalActualRevenue'];
						var distributionAmount=obj['distributionAmount'];
						var recRateCurrency=obj['recRateCurrency'];
						var shipNumber=obj['shipNumber'];
						
						var VATExclude=obj['VATExclude'];
						var accLineList=obj['accLineList'];
		  				var tempId=obj['id'];
			  			var billCodeTemp=obj['billToCode'];
			  			var companyDivTemp=obj['companyDivision'];
			  			var batchAccIdList=obj['accountLineIdList'];
			  			flag2='YES';  						
			  			if(document.getElementById(accountId).disabled!=true){
			  			if(!VATExluceValidationForAll(accountId,VATExclude,shipNumber,accLineList)){
							  flag='NO1';
							  flag2='NO'; 					
						}
			  			}
						<c:if test="${multiCurrency!='Y'}"> 
							if(systemDefaultmiscVl.indexOf(job)>-1){ 
					   		      if((totalActualRevenue>distributionAmount)||(totalActualRevenue<distributionAmount)){
									  flag='NO';
									  alert("Actual revenue total and distribution amount total are not same. So, cannot generate invoice.")
									  document.getElementById('selectall').checked=false;
					   		      }
							}		
						</c:if>
						if(cur!=''){
							if(cur!=recRateCurrency){
								 flag='NO';
								 alert("You cannot select service orders with different invoice currency. Please select service orders with same invoice currency.");
								 document.getElementById('selectall').checked=false;
							}
						}else{
							if(document.getElementById(accountId).disabled!=true){
							cur=recRateCurrency;
							}
						}
						if((flag!='NO')&&(flag2!='NO')){ 
							if(document.getElementById(accountId).disabled!=true){ 
							if(newIds==''){
						  		  newIds=tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+totalActualRevenue+'~'+batchAccIdList+'~'+recRateCurrency;
					  		   }else{
						  		  if(newIds.indexOf(tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+totalActualRevenue+'~'+batchAccIdList+'~'+recRateCurrency)<0)
						  		  {
					  		  		newIds=newIds + ',' + tempId+'~'+billCodeTemp+'~'+companyDivTemp+'~'+totalActualRevenue+'~'+batchAccIdList+'~'+recRateCurrency;
						  		  }
					  		   }
				  			   if(newIds1==''){
				  				 newIds1=accountId+'';
						  		   }else{
							  		  if(newIds1.indexOf(accountId)<0)
							  		  {
							  			newIds1=newIds1 + ',' + accountId;
							  		  }
						  		   }			  		   		  			
						} 
						}
					}
					}		
	   			    }
				if((flag!='NO')&&(flag!='NO1')){
					document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
					document.forms['collectiveBilling'].elements['selectCurrency'].value=cur;		
	   				var jsonPreviewInvoiceLineList1 = JSON.parse('${jsonPreviewInvoiceLine}');
	   			    for(var i=0;i<jsonPreviewInvoiceLineList1.length;i++){
	   			        var obj = jsonPreviewInvoiceLineList1[i];

				var sid=obj['accountId'];
				if(document.getElementById(sid).disabled!=true){
				document.getElementById(sid).checked=true;
				}
	   			    }
				}else if((flag!='NO')&&(flag=='NO1')){
					alert(globalVatMessage);
					document.getElementById('selectall').checked=false;
					if(newIds1!=''){
						document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value=newIds;
						document.forms['collectiveBilling'].elements['selectCurrency'].value=cur;
		   				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
		   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
		   			        var obj = jsonPreviewInvoiceLineList[i];

						var sid=obj['accountId']+'';
						try{
							if(newIds1.indexOf(sid)>-1){ 
							if(document.getElementById(sid).disabled!=true){
								document.getElementById(sid).checked=true;
							}
							}
		   			 	}catch(e){}
		   			    }			
					}			
				}else if((flag=='NO')&&(flag!='NO1')){			
				}else{
				}
			  }else{
					document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value='';
					document.forms['collectiveBilling'].elements['selectCurrency'].value='';		
	   				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
	   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
	   			        var obj = jsonPreviewInvoiceLineList[i];

				var sid=obj['accountId'];
				if(document.getElementById(sid).disabled!=true){
				document.getElementById(sid).checked=false;
				}
	   			    }
			  }
		  }
	function generateInvoice(){
		  var sidList=document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value;
		  var billToCode=document.forms['collectiveBilling'].elements['billCode'].value;
		  var companyDivs=document.forms['collectiveBilling'].elements['companyDivs'].value; 
		  var billingCurrency="";
		  try{
			  billingCurrency=document.forms['collectiveBilling'].elements['billingCurrencyType'].value; 
		  }catch(e){}
		  if(billingCurrency==null || billingCurrency==undefined || billingCurrency==""){
			  billingCurrency='';
		  } 	
		  var pir='EXCLUDE_PIR';
		  <configByCorp:fieldVisibility componentId="component.field.BatchPayable.pir">
			  pir=document.forms['collectiveBilling'].elements['pir'].value;
			  if(pir==null || pir==undefined || pir==""){
				  pir='';
			  } 			  
		  </configByCorp:fieldVisibility>
		  var invoiceImt="false";
		  var ship="";
				var jsonPreviewInvoiceLineList = JSON.parse('${jsonPreviewInvoiceLine}');
   			    for(var i=0;i<jsonPreviewInvoiceLineList.length;i++){
   			        var obj = jsonPreviewInvoiceLineList[i];
			var sid=obj['accountId'];
			if(document.getElementById(sid).checked){
				var totalActualRevenue=obj['totalActualRevenue'];
						if(totalActualRevenue =='' || totalActualRevenue=='0' || totalActualRevenue=='0.00' || totalActualRevenue=='0.00'){
							invoiceImt="true";
							if(ship==""){
								ship=obj['shipNumber'];
							}else{
								ship=ship+","+obj['shipNumber'];
							}
						}
					}
   			  }

		  if(sidList.trim()==""){
			  alert("Please select at least one service order.");
		  }else if(invoiceImt=="true"){
			  alert("Total invoice amount Zero, cannot generate invoice for "+ship);
		  }else{
			  var sid = document.forms['collectiveBilling'].elements['serviceOrderId'].value;
			  var shipNum = document.forms['collectiveBilling'].elements['shipNumberForTickets'].value;
			  openWindow('activeGeneratedInvoiceList.html?buttonType=invoice&sidGeneratedInvoice=&billCode='+billToCode+'&companyDivs='+companyDivs+'&pir='+pir+'&billingCurrency='+billingCurrency+'&sid='+sid+'&shipNumberForTickets='+shipNum+'&decorator=popup&popup=true',700,300);
		  }
		 }
		
		showAstrik();
		$(document).ready(function(){
		$('#previewAccountLines tr ').has('input[type=checkbox]:disabled').css('background-color', '#c3c3c3');
		})
function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
	}
		</script>