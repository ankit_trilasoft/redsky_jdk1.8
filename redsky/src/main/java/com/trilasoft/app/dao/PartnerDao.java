package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.AccountContact;
import com.trilasoft.app.model.Partner;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface PartnerDao extends GenericDao<Partner, Long> {   
   
	public List<Partner> findByLastName(String lastName);
	public List<Partner> findForPartner(String lastName, String partnerCode, String billingCountryCode, String findFor);
	public List<Partner> searchForPartner(String lastName, String partnerCode, String billingCountryCode,String billingStateCode, String billingCountry, String findFor);
	public List<Partner> findByBillingCountryCode(String billingCountryCode);
	public List<Partner> findByBroker();
	/*public List<Partner> findByBrokerAccRef();*/
	public List<Partner> findByCarriers(String sessionCorpID);
	/*public List<Partner> findByCarriersAccRef();*/
	public List<Partner> findByAccount();
	public List<Partner> findByOrigin(String origin);
	public List<Partner> findByOriginAccRef(String origin);
	public List<Partner> findByFreight(String origin);
	
	public List<Partner> findByDestination(String destination,String userType);
	public List<Partner> findByDestAccRef(String destination);

	public List<Partner> findByAgent();
	public List findMaximum();
	public List findPartnerCode(String partnerCode);
	public List findMaxByCode();
	public List getPartnerPopupList(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag);
	public List	getCrewPartnerPopupList(String corpID, String lastName,String aliasName, String partnerCode, String billingCountry, String billingStateCode,String extReference);
	public List getPartnerPopupAllList(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode,String extReference);
	public List<Partner> findByBillToCode(String shipNumber);
	public List partnerExtract(String billToCode,String corpID);
	public List<Partner> findByOwner();
	public List<Partner> findByCrewDrivers();
	public List<Partner> findByAllOwner();
	public List<Partner> findByOwnerOp(String perId);
	public String findByOwnerOpLastNameByCode(String perId);
	public List<Partner> findMultiAuthorization(String accPartnerCode);
	public List findPartnerForActgCode(String partnerCode,String corpID,String companyDivision); 
	public List findPartnerForActgCodeLine(String partnerCode,String corpID,String companyDivision,String actgCodeFlag, Boolean cmmDmmFlag); 
	public List findSubContExtractSeq(String corpID);
	public int updateSubContrcExtractSeq(String subConExtractSeq,String corpID);
	public List getPartnerPopupListAdmin(String partnerType,String corpID,String firstName,String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode, String status, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, Boolean isIgInactive, String externalRef,String vanlineCode,String typeOfVendor, String fidiNumber,String OMNINumber,String AMSANumber,String WERCNumber,String IAMNumber,String utsNumber ,String eurovanNetwork ,String PAIMA,String LACMA);
	public List getPartnerGeoMapDataList(String corpID, Boolean isAgt, Boolean isVndr, Boolean isCarr,String minLatitude,String maxLatitude,String minLogitude,String maxLogitude,Boolean isUts,Boolean isFidi,Boolean isOmniNo,Boolean isAmsaNo,Boolean isWercNo,Boolean isIamNo,Boolean isAct,String country,String lastName);	
	public List<Partner> findByOwnerOps(String perId);
	public List<Partner> checkValidNationalCode(String vlCode,String sessionCorpID);
	public List getPartnerPopupListCompDiv(String partnerType, String corpID, String compDiv, String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode, String extReference,String vanlineCode,Boolean cmmDmmFlag);
	public List getCountryCode(String mailingCountry);
	public List financePartnerExtract(String beginDate,String endDate, String companyDivision,String corpID);
	public List financePartnerDataExtract(String corpID,String beginDate,String endDate);
	public void disableUser(Long id, String sessionCorpID, String status);
	public void updatePrtExtractFlag(String corpid,String partnercode);
	public List getPartnerChildAgentsList(String partnerCode, String sessionCorpID);
	public List getPartnerIncludeChildAgentsList(String partnerCode, String sessionCorpID,String pageListType);
	public List synInvoicePartnerExtract(String sysInvoiceDates, String companyDivision, String corpID);
	
	public List getAllAgent(String sessionUserName);
	
	public List getQuotationPartnerPopupList(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag);
	public List getQuotationPartnerPopupListCompDiv(String partnerType,String corpID,String compDiv,String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference,String vanlineCode,Boolean cmmDmmFlag);
	
	
	
	public List searchAgentList(String firstName, String lastName, String partnerCode, String countryCode, String stateCode, String country, String status, String partnerIds, String sessionCorpID);
	public List findAcctRefNum(String code, String corpId);
	public List findPartnerProfile(String code, String corpId);
	
	public List<Partner> financeAgentPartnerExtract(String beginDate, String endDate, String companyDivision,String corpID);
	public List<Partner> findVendorCode(Long sid, String sessionCorpID, String jobType,Long cid );
	public List searchDefaultVandor(Long sid, String job, String partnerType, String sessionCorpID, String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, Long cid,  String externalRef);
	public List findBookingAgentCode(String companyDivision, String sessionCorpID);
	public List searchBookingAgent(String companyDivision, String sessionCorpID, String lastName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode);
	public List ratingList(String partnerCode, String sessionCorpID);
	public List getFeedbackList(String partnerCode, String year, String corpID);
	public List getPartnerActivityList(String partnerCode, String sessionCorpID);
	public List getPartnerSSCWActivityList(String partnerCode, String corpID);
	public List getPartnerListNew(String lastName, String countryCode, String sessionCorpID, String billingCity);
	public List getPartnerPopupListAccount(String partnerType,String corpID,String firstName, String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode, String status, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, String externalRef, Boolean cmmDmmFlag);
	
	public List getPartnerVanLinePopupList(String partnerType, String corpID, String firstName, String lastName,String aliasName ,String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String vanLineCODE, Boolean isPrivateParty, Boolean isAccount, Boolean isAgent, Boolean isVendor, Boolean isCarrier, Boolean isOwnerOp, Boolean cmmDmmFlag);
	
	public List getAgentCompanyDivisionVanline(String string, String sessionCorpID, String firstName, String lastName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String vanLineCODE,String aliasName, String companyDivision);
	public List findPartnerId(String partnerCode, String sessionCorpID);
	public List<Partner> findByCarriersForHeavy();
	public List getCarrierListForHeavy(String corpID,String lastName, String partnerCode);
	public List getPayToForHeavy(String corpID,String partnerCode);
	public List ratingListSixMonths(String partnerCode, String sessionCorpID);
	public List ratingListOneYr(String partnerCode, String sessionCorpID);
	public List ratingListTwoYr(String partnerCode, String sessionCorpID);
	
	public List accountRatingList(String partnerCode, String sessionCorpID, String days);
	public List getAccountFeedbackList(String partnerCode, String year, String sessionCorpID);
	public List getPartnerAccountActivityList(String partnerCode, String sessionCorpID);
	public List<Partner> findByNetwork();
	public List findByDestinationRelo(String destination, String jobRelo,String jobReloName,String sessionCorpID);
	public List findPartnerRelo(String lastName, String partnerCode, String billingCountryCode, String billingState, String billingCountry, String jobRelo,String jobReloName,String searchOfVendorCode,String sessionCorpID,boolean cmmdmmflag);
	public List vendorNameForUniqueActgCode(String partnerCode, String sessionCorpID, Boolean cmmDmmFlag);
	public String getAccountCrossReference(String vendorCode,String companyDiv ,String sessionCorpID);
	public String getAccountCrossReferenceUnique(String vendorCode,String sessionCorpID);
	public List getDriverLocationList( String corpID);
	public List getAllDriverLocation(String driverTypeValue,String corpID);
	public List getAllInvolvementOfDriver(String driverId,String sessionCorpID);
	public String getDriverHomeAddress(String driverId);
	public List partnerSAPExtract(String billToCode, String corpID);
	public List partnerSAPExtractForUGCN(String billToCode, String corpID);
	public List financeSAPPartnerExtract(String beginDate, String endDate, String companyDivision,String corpID);
	public List synSAPInvoicePartnerExtract(String sysInvoiceDates, String companyDivision, String corpID);
	public List<Partner> financeSAPAgentPartnerExtract(String beginDate, String endDate, String companyDivision,String corpID);
	public List trackingInfoByShipNumber(String sessionCorpID,String sequenceNumber);
	public Map<String, String> driverDropDownList(String sessionCorpID);
	public List nextDriverLocationList(String driverId,String sessionCorpID);
	public List findActgCodeForVendorCode(String vendorCode, String sessionCorpID,String companyDivision, String companyDivisionAcctgCodeUnique);
	public List getAllInvolvementOfDriverMap(String driverId,String sessionCorpID);
	public Map<String, String> findDriverTypeList(String sessionCorpID);
	public List getAmmount(String partnerCode, String sessionCorpID);
	public List getPartnerPopupListForContractAgentForm(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode,String extReference,String customerVendor);
	public List getPartnerNetworkGroupPopupList(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode);
	public List partnerExtractUTSI(String billToCode,String corpID);
	public List financePartnerExtractUTSI(String beginDate,String endDate, String companyDivision,String corpID);
	public List synInvoicePartnerExtractUTSI(String sysInvoiceDates, String companyDivision, String corpID);
	public List<Partner> financeAgentPartnerExtractUTSI(String beginDate, String endDate, String companyDivision,String corpID);
	public List basedAtName(String partnerCode, String sessionCorpID);
	public List basedAtNameForInvoicing(String partnerCode, String sessionCorpID);
	public String checkPartnerType(String billToCode);
	public String checkDriverIdAjax(String driverCode,String corpId);
	public List financeAgentPartnerExtractHoll(String beginDate, String endDate, String companyDivision,String corpID);
	public List synInvoicePartnerExtractHoll(String sysInvoiceDates, String companyDivision, String corpID);
	public List partnerExtractHoll(String billToCode, String corpID);
	public List financePartnerExtractHoll(String beginDate, String endDate, String companyDivision,String corpID);
	public List findPartnerForActgCodeVanLine(String vendorCode, String sessionCorpID);
	public List<String> getPartnerTypeList();
	public Set getVanlineJobDetails();
	public List AFASPartnerExtract(String beginDate,String endDate,String corpID);
	public Map<String, AccountContact> accountContactMap(String sessionCorpID);
	public List getPartnerExtract(String billToCode,String corpID);
	public List<Partner> findByNetworkPartnerCode(String networkPartnerCode,String sessionCorpID, String lastName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference, String aliasName, String vanlineCode);
	public List findByNetworkBillToName(String networkPartnerCode, String sessionCorpID, String networkBillToCode);
	public List findNetworkPartnerDetailsForAutoComplete(String partnerName,String networkPartnerCode, String sessionCorpID);
	public String codeCheckPartner(String partnerCode,String corpId,String partnerType);
	public List getPartnerPopupListWithBookingAgentSet(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag);
	public List findByOriginforClientDirective(String origin, String corpID, String billToCode);
	public List findPartnerDetailsForAutoCompleteNew(String partnerName,String partnerNameType,String corpid,Boolean cmmDmmFlag,String billToCode);
	public List getPartnerVanLinePopupListNew(String partnerType, String corpID, String firstName, String lastName,String aliasName ,String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String vanLineCODE, Boolean isPrivateParty, Boolean isAccount, Boolean isAgent, Boolean isVendor, Boolean isCarrier, Boolean isOwnerOp, Boolean cmmDmmFlag,String billToCode);
	public List getPartnerPopupListNew(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag,String billToCode);
	public List<Partner> vendorNameAutocompleteAjaxlist(String partnerNameAutoCopmlete);
	public List getAgentPopupListCompDiv(String partnerType,String corpID,String compDiv,String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String extReference,String vanlineCode,Boolean cmmDmmFlag,String agentGroup);
	public List getAgentPopupList(String partnerType,String corpID,String lastName,String aliasName, String partnerCode, String billingCountryCode,String billingCountry, String billingStateCode,String extReference,String customerVendor,String vanlineCode,Boolean cmmDmmFlag,String agentGroup);
	public String checkParnerCodeForInv(String billName, String sessionCorpID);
	public List findByOriginAgent(String origin);
	public List agentRatingList(String partnerCode, String sessionCorpID, String days);
	public List getAgentRatingFeedbackList(String partnerCode, String days, String corpID);
	public List synInvoiceMasBilltoPartnerExtract(String sysInvoiceDates, String corpID);
	public List synInvoiceMasPartnerVendorExtract(String sysInvoiceDates, String corpID);
	public int updateBillToExtractAccess(String sessionCorpID,List partnerList,  String userName) ;
	public int updateVendorExtractAccess(String sessionCorpID,List partnerList,  String userName) ;
	public Map<String, String> checkBilliToCode(String sessionCorpID);
	public List getOADACode(String partnerCode);
}
