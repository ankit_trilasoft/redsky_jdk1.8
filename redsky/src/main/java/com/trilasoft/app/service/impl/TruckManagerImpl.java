package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.TruckDao;
import com.trilasoft.app.model.Truck;
import com.trilasoft.app.service.TruckManager;

public class TruckManagerImpl extends GenericManagerImpl<Truck, Long> implements TruckManager {
	TruckDao truckDao; 
    
	 
    public TruckManagerImpl(TruckDao truckDao) { 
        super(truckDao); 
        this.truckDao = truckDao; 
    }

    public List searchTruck(String warehouse, String localNum, String desc, String tagNum, String corpId, String type,String state,String ownerPayTo,String truckStatus,String dotInspFlag) {
	    return truckDao.searchTruck(warehouse, localNum, desc, tagNum, corpId,type,state,ownerPayTo,truckStatus,dotInspFlag);
    }
    
    public List searchTruckDotInsp(String warehouse, String localNum, String desc, String tagNum, String corpId, String type,String state,String ownerPayTo,String truckStatus) {
	    return truckDao.searchTruckDotInsp(warehouse, localNum, desc, tagNum, corpId,type,state,ownerPayTo,truckStatus);
    }
    public Map<String, String> findDefaultStateList(String bucket2, String corpId){
    	return truckDao.findDefaultStateList(bucket2, corpId);
    }
    public List  findDriverList(String bucket2, String corpId)
    {
    	return truckDao.findDriverList(bucket2, corpId);
    }
    
    public List  findOwnerOperatorList(String parentId,String corpId){
    	return truckDao.findOwnerOperatorList(parentId, corpId);
    }
    public List  findTruckDriverList(String partnerCode,String truckNumber,String corpId){
    	return truckDao.findTruckDriverList(partnerCode, truckNumber, corpId);
    }
    public List  findTruckDescriptionType(String truckType, String corpId){
    	return truckDao.findTruckDescriptionType(truckType, corpId);
    }
    public List  findCarrierListInDomestic(String truckNumber, String corpId){
    	return truckDao.findCarrierListInDomestic(truckNumber, corpId);
    }
    public List findControlExpirationsListInDomestic(String parentId,Date deliveryLastDay,String corpId){
    	return truckDao.findControlExpirationsListInDomestic(parentId,deliveryLastDay, corpId);
    }
    public List findControlExpirationsListForDriver(String parentId,Date deliveryLastDay,String corpId){
    	return truckDao.findControlExpirationsListForDriver(parentId, deliveryLastDay, corpId);
    }
    public List findTruckTrailer(String corpId){
    	return truckDao.findTruckTrailer(corpId);
    }
    public List searchTruckTrailer(String warehouse, String localNum, String desc, String tagNum, String corpId, String type,String state,String ownerPayTo){
    	return truckDao.searchTruckTrailer(warehouse, localNum, desc, tagNum, corpId, type, state, ownerPayTo);
    }
    public List  findTrailerListInDomestic(String truckNumber, String corpId){
    	return truckDao.findTrailerListInDomestic(truckNumber, corpId);
    }
    public List findAgentVanName(String truckVanLineCode,String corpId){
    	return truckDao.findAgentVanName(truckVanLineCode, corpId);
    }
    public List  findVanListInDomestic(String truckNumber, String corpId){
    	return truckDao.findVanListInDomestic(truckNumber, corpId);
    }
    public List isExisted(String truckNo, String sessionCorpID){
    	return truckDao.isExisted(truckNo, sessionCorpID);
    }
    
    public List searchTruckDueSixty(String warehouse, String localNum, String desc, String tagNum, String corpId, String type,String state,String ownerPayTo,String truckStatus ,int myListFor) {
	    return truckDao.searchTruckDueSixty(warehouse, localNum, desc, tagNum, corpId,type,state,ownerPayTo,truckStatus , myListFor);
    }
}
