package com.trilasoft.app.webapp.action;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FilenameUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporterParameter;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.util.FileSizeUtil;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.DocumentBundle;
import com.trilasoft.app.model.EmailSetup;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.PrintDailyPackage;
import com.trilasoft.app.model.Reports;
import com.trilasoft.app.service.AuditSetupManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.DocumentBundleManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ReportsManager;


public class DocumentBundleAction extends BaseAction implements Preparable{
	private String sessionCorpID;
	private Map<String, String> routing;
	private Map<String, String> service; 
	private List mode;
	private Map<String, String> jobs;
	private List documentBundleList;
	private List tableList;
	private List fieldList;
	private String tableName;
	private Long id;
	private List multiplServiceTypes;
	private List multiplJobType;
	private List multiplRouting;
	private List documentBundleFormsList;
	private String emailTable;
	private String emailField;
	private String bundleName;
	private String serviceType;
	private String routingType;
	private String serviceMode;
	private String job;
	private String fieldName;
	private String tabId; 
	
	private DocumentBundle documentBundle;
	private RefMasterManager refMasterManager;
	private DocumentBundleManager documentBundleManager;
	private AuditSetupManager auditSetupManager;
	private ReportsManager reportsManager;
	private Company company;
	private CompanyManager companyManager;
	private Reports reports;
	private UserManager userManager;
	private EmailSetupManager emailSetupManager;
	private EmailSetup emailSetup;
	private User user;
	private List emailFieldList = new ArrayList();
	private List emailTableList = new ArrayList();
	private MyFileManager myFileManager;
	private File uploadFile;
	private String uploadFileContentType;
	private String uploadFileFileName;
	private ErrorLogManager errorLogManager;
	
	static final Logger logger = Logger.getLogger(DocumentBundleAction.class);
	Date currentdate = new Date();
	
	public DocumentBundleAction(){
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if(auth != null){
				User user = (User) auth.getPrincipal(); 
				this.sessionCorpID = user.getCorpID();
			}
		} catch (Exception e) {e.printStackTrace();} 
	}
	
	public void prepare() throws Exception {
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		company= companyManager.findByCorpID(sessionCorpID).get(0);
	}
	
	public void getComboList() throws Exception {
		routing = refMasterManager.findByParameter(sessionCorpID, "ROUTING");
		mode = refMasterManager.findByParameters(sessionCorpID, "MODE");
		jobs = refMasterManager.findByParameter(sessionCorpID, "JOB");
		service = refMasterManager.findByParameter(sessionCorpID, "SERVICE");
		tableList=auditSetupManager.auditSetupTable("true");
		emailTableList=auditSetupManager.auditSetupTable("true");
	}
	
	@SkipValidation
	public String list() throws Exception{
		getComboList();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		fieldList=auditSetupManager.auditSetupField(tableName,"true");
		documentBundleList = documentBundleManager.getListByCorpId(sessionCorpID,tabId);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String edit() throws Exception {
		getComboList();
		if (id != null) {
			documentBundle = documentBundleManager.get(id);
			if(documentBundle.getFormsId()!=null && !documentBundle.getFormsId().equals("")){
				String[] arr = documentBundle.getFormsId().split(",");
				int arrayLength = arr.length;
				String reportId="";
				for(int i=0;i<arrayLength;i++){
					if(reportId.equals("")){
						reportId = arr[i];
					}else{
						reportId = reportId+","+arr[i];
					}
				}
				documentBundleFormsList = documentBundleManager.findFormsByFormsId(reportId);
			}
			if(documentBundle.getRouting() != null && !documentBundle.getRouting().equals("")){
				multiplRouting = new ArrayList();
				String[] routingType = documentBundle.getRouting().split(",");
				for (String rType : routingType) {
					multiplRouting.add(rType.trim());
				}
			}
			if(documentBundle.getJob()!=null && !documentBundle.getJob().equals("")){
				multiplJobType = new ArrayList();
				String[] jobType = documentBundle.getJob().split(",");
				for (String jType : jobType){
					multiplJobType.add(jType.trim());
				}
			}
			if(documentBundle.getServiceType()!=null && !documentBundle.getServiceType().equals("")){
				multiplServiceTypes = new ArrayList();
				String[] serviceType = documentBundle.getServiceType().split(",");
				for (String sType : serviceType){
					multiplServiceTypes.add(sType.trim());
				}
			}
			if(documentBundle.getEmail()!=null && !documentBundle.getEmail().equals("")){
				String emailVal = documentBundle.getEmail();
				String[] emailValue = emailVal.split("\\.");
 				emailTable= emailValue[0];
				emailField = emailValue[1];
				emailFieldList = auditSetupManager.auditSetupField(emailTable,"true");
			}
		} else {
			documentBundle = new DocumentBundle(); 
			documentBundle.setCreatedOn(new Date());
			documentBundle.setUpdatedOn(new Date());
		} 
		fieldList=auditSetupManager.auditSetupField(documentBundle.getSendPointTableName(),"true");
		return SUCCESS;
	}
	private String documentBundleURL;
	@SkipValidation
	public String save() throws Exception {
		getComboList();
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				boolean isNew = (documentBundle.getId() == null);
				if (isNew) { 
					documentBundle.setCreatedOn(new Date());
					documentBundle.setCreatedBy(getRequest().getRemoteUser());
		        }
				documentBundle.setCorpId(sessionCorpID);
				documentBundle.setUpdatedOn(new Date());
				documentBundle.setUpdatedBy(getRequest().getRemoteUser());
				if((emailTable!=null && !emailTable.equals("")) && (emailField!=null && !emailField.equals(""))){
					documentBundle.setEmail(emailTable+"."+emailField);
				}
				documentBundle.setStatus(true);
				documentBundle.setFormsId(reportsId);
				documentBundle = documentBundleManager.save(documentBundle);
				String key = (isNew) ? "Document Bundle details has been added Sucessfully" : "Document Bundle details has been updated Sucessfully";
			    saveMessage(getText(key));
				fieldList=auditSetupManager.auditSetupField(documentBundle.getSendPointTableName(),"true");
				emailFieldList = auditSetupManager.auditSetupField(emailTable,"true");
				documentBundleURL = "?id="+documentBundle.getId();
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
	}
	
	@SkipValidation
	public String search() throws Exception{
		getComboList();
		fieldList=auditSetupManager.auditSetupField(tableName,"true");
		documentBundleList = documentBundleManager.searchDocumentBundle(bundleName,serviceType,serviceMode,job,tableName,fieldName,sessionCorpID,routingType);
		return SUCCESS;
	}
	
	private String reportsId;
	@SkipValidation
	public void saveReportIdInDocumentBundleAjax() {
		documentBundleManager.updateFormsIdInDocumentBundle(reportsId,id);
	}
	
	private String returnAjaxStringValue;
	private String jrxmlName;
	public String getReportDescAndIdByJrxmlNameAjax(){
		returnAjaxStringValue = documentBundleManager.getReportDescAndIdByJrxmlName(jrxmlName, sessionCorpID);
		return SUCCESS;
	}
	
	@SkipValidation
	public void deleteReportIdInDocumentBundle(){
		if(reportsId.contains(",")){
			reportsId = reportsId.substring(0, reportsId.lastIndexOf(','));
		}else{
			reportsId ="";
		}
		documentBundleManager.updateFormsIdInDocumentBundle(reportsId,id);
	}
	
	public String documentBundleChildListAjax() throws Exception {
		documentBundleFormsList = documentBundleManager.findFormsByFormsId(reportsId);
		return SUCCESS;
	}
	
	
	public String moveDocumentBundleWasteBasket() throws Exception{
		documentBundle = documentBundleManager.get(id);
		documentBundle.setStatus(false);
		documentBundleManager.save(documentBundle);
		documentBundleList = documentBundleManager.getListByCorpId(sessionCorpID,tabId);
		return SUCCESS;
	}
	
	public String recoverDocumentBundle() throws Exception{
		documentBundle = documentBundleManager.get(id);
		documentBundle.setStatus(true);
		documentBundleManager.save(documentBundle);
		documentBundleList = documentBundleManager.getListByCorpId(sessionCorpID,tabId);
		return SUCCESS;
	}
	
	public String delete() throws Exception{
		documentBundleManager.remove(id);
		documentBundleList = documentBundleManager.getListByCorpId(sessionCorpID,"wasteBasket");
		return SUCCESS;
	}
	private String jobNumber;
	private String sequenceNumber;
	private List emailList;
	private String clickedField;
	public String getRecordFromDocumentBundleAjax() throws Exception{
		emailList = documentBundleManager.getRecordForEmail(routingType,serviceType,serviceMode,job,fieldName,sessionCorpID,jobNumber,sequenceNumber,firstName,lastName,clickedField);		
		return SUCCESS;
	}
	private String receipientTo;
	private String reportSubject;
	private String reportBody;
	private String recipientCC;
	private String firstName;
	private String lastName;
	private String reportSignaturePart;
	private String fromName;
	private Long dbId;
	private String myFileId;
	
	public String openEmailPopUp(){
		
		String tempRecipient="";
		String tempRecipientArr[]=receipientTo.split(",");
		for(String str1:tempRecipientArr){
			if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
				if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
				tempRecipient += str1;
			}
		}
		receipientTo=tempRecipient;	
		
		tempRecipient="";
		tempRecipientArr=recipientCC.split(",");
		for(String str1:tempRecipientArr){
			if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
				if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
				tempRecipient += str1;
			}
		}
		
		String appendReportBody = "Please find the attached documentation and instructions for Mr "+ firstName+" "+lastName+" / " + jobNumber;
		recipientCC=tempRecipient;
		reportSubject = "S/O# "+jobNumber+" "+firstName+" "+lastName+"";
		fromName = user.getEmail();
		reportSignaturePart = user.getSignature();
		documentBundle = documentBundleManager.get(dbId);
		reportBody =appendReportBody+"\n"+ documentBundle.getDescription();
		
		String fileName = "";
		String fileId = "";
		if(reportsId!=null && !reportsId.equals("")){
			documentBundleFormsList = documentBundleManager.findFormsByFormsId(reportsId);
		}
		
		if(myFileId!=null && !myFileId.equalsIgnoreCase("")){
			String arr[]=myFileId.split("~");
			String myFileIdTemp = "";
			 for(int x=0;x<arr.length;x++){
				 if(!myFileIdTemp.equalsIgnoreCase("")){
					 myFileIdTemp = myFileIdTemp+","+arr[x];
				 }else{
					 myFileIdTemp = arr[x];
				 }
			 }
		fileCabinetList = documentBundleManager.findFileFromFileCabinet(sessionCorpID,jobNumber,myFileIdTemp);
		}
		return SUCCESS;
	}
	private List fileCabinetList = new ArrayList();
	public String getFileFromFileCabinetPopUp(){
			fileCabinetList = documentBundleManager.findFileFromFileCabinet(sessionCorpID,jobNumber,"");
		return SUCCESS;
	}
	
	 	private File[] fileUpload;
	    private String[] fileUploadFileName;
	    private String[] fileUploadContentType;
	public void emailForDocumentBundle() throws Exception{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String uploadFilePath="";
			Map parameters = new HashMap();
			 parameters.put("Service Order Number", jobNumber);
			 parameters.put("Corporate ID", sessionCorpID);
			 String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			 uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "DocumentBundle" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
			 int maxId = Integer.parseInt(myFileManager.getMaxId(sessionCorpID).get(0).toString())+1;
				   // copy the uploaded files into pre-configured location
		       try{
				for (int i = 0; i < fileUpload.length; i++) {
		            File uploadedFile = fileUpload[i];
		            String fileName = fileUploadFileName[i];
		            String fileContentType = fileUploadContentType[i];
		            String fileNameWithoutExt = FilenameUtils.getBaseName(fileName);
		            String fileExt = FilenameUtils.getExtension(fileName);
		            if(fileName != null && !fileName.isEmpty()){
		            fileName= fileNameWithoutExt+"_"+maxId+"."+fileExt;
		            }
		           
		            File fileToCreate = new File(uploadDir,fileName);
		            try {
		            	 FileUtils.copyFile(uploadedFile, fileToCreate);
		            } catch (IOException ex) {
		                System.out.println("Could not copy file " + fileName);
		                ex.printStackTrace();
		            }
		            
		            if(fileName != null && !fileName.isEmpty()){
						System.out.println(fileName);
						if(uploadFilePath.isEmpty()){
							uploadFilePath =(uploadDir+fileName );
						}else{
							uploadFilePath =uploadFilePath+"~"+(uploadDir+fileName );
						}
						File newFile1 = new File(uploadDir + fileName);
						MyFile myFile = new MyFile();
						  myFile.setCustomerNumber(sequenceNumber);
						  myFile.setFileId(jobNumber);
						  myFile.setFileContentType(fileContentType);
						  myFile.setFileFileName(fileName);
						  myFile.setLocation(uploadDir+fileName );
						  myFile.setFileType("General");
						  myFile.setDocumentCategory("General");
						  myFile.setDescription("General");
						  myFile.setCorpID(sessionCorpID);
						  myFile.setActive(true);
						  myFile.setCreatedBy(getRequest().getRemoteUser()+"_"+"DOCBUNDLE");
						  myFile.setUpdatedBy(getRequest().getRemoteUser()+"_"+"DOCBUNDLE");
						  myFile.setCreatedOn(new Date());
						  myFile.setUpdatedOn(new Date());
						  myFile.setNetworkLinkId(null);
						  FileSizeUtil fileSizeUtil = new FileSizeUtil(); 
						  myFile.setFileSize(fileSizeUtil.FindFileSize(newFile1));
						  myFile.setMapFolder("");
						  if(reports != null){
						  if(reports.getSecureForm() != null && reports.getSecureForm().equalsIgnoreCase("Yes")){
					    	myFile.setIsSecure(true);
						  }else{
					    	myFile.setIsSecure(false);
						  }  }
						 myFileManager.save(myFile); 
					
					}
		            
		        }
				
		       }catch(Exception e){
		    	   e.printStackTrace();  
		       }
				
			byte[] output;
			
			if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
				Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			} else {
				Locale locale = new Locale("en", "US");
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
			List<String> files = new ArrayList<String>();
			String filePath="";
			String attachedfilePath="";
			JasperPrint jasperPrint = new JasperPrint();
			HttpServletResponse response = getResponse();
			ServletOutputStream ouputStream = response.getOutputStream();
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			String[] reportId = reportsId.split(",");
			for (int i=0;i<reportId.length;i++) {
				try{
					jasperPrint = reportsManager.generateReport(Long.parseLong(reportId[i]), parameters);
					reports = reportsManager.get(Long.parseLong(reportId[i]));
					String temp = reports.getReportName();
					String fileName = temp.substring(0, temp.indexOf("."));
					if((reports.getPdf()!=null && !reports.getPdf().equals("")) && reports.getPdf().equalsIgnoreCase("true")){
						response.setContentType("application/pdf");
						response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
						output = JasperExportManager.exportReportToPdf(jasperPrint);
						
						File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
								dirPath.mkdirs();
						}			
						String tempFileFileName = fileName;
						String fileFileName = tempFileFileName+"_"+maxId+".pdf";
						files.add(uploadDir + fileFileName);
						File newFile = new File(uploadDir + fileFileName);
						filePath =(uploadDir+fileFileName );
						if (newFile.exists()) {
							newFile.delete();
						}
						if(!attachedfilePath.equalsIgnoreCase("")){
							attachedfilePath=filePath+"~"+attachedfilePath;
				        }else{
				        	attachedfilePath=filePath;
				        }
						
						FileOutputStream fos = new FileOutputStream(newFile);
						DataOutputStream bos =  new DataOutputStream(fos);
						bos.write(output);
						bos.flush();
						bos.close();
						
						MyFile myFile = new MyFile();
						  myFile.setCustomerNumber(sequenceNumber);
						  myFile.setFileId(jobNumber);
						  myFile.setFileContentType("application/pdf");
						  myFile.setFileFileName(fileFileName);
						  myFile.setLocation(filePath);
						  myFile.setFileType("General");
						  myFile.setDocumentCategory("General");
						  myFile.setDescription("General");
						  myFile.setCorpID(sessionCorpID);
						  myFile.setActive(true);
						  myFile.setCreatedBy(getRequest().getRemoteUser()+"_"+"DOCBUNDLE");
						  myFile.setUpdatedBy(getRequest().getRemoteUser()+"_"+"DOCBUNDLE");
						  myFile.setCreatedOn(new Date());
						  myFile.setUpdatedOn(new Date());
						  myFile.setNetworkLinkId(null);
						  FileSizeUtil fileSizeUtil = new FileSizeUtil(); 
						  myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));		
						  myFile.setMapFolder("");
						  if(reports.getSecureForm() != null && reports.getSecureForm().equalsIgnoreCase("Yes")){
					    	myFile.setIsSecure(true);
						  }else{
					    	myFile.setIsSecure(false);
						  }  
						myFileManager.save(myFile); 
						
					}
					if((reports.getDocx()!=null && !reports.getDocx().equals("")) && reports.getDocx().equalsIgnoreCase("true")){
						response.setContentType("application/docx");
					    response.setHeader("Content-Disposition", "attachment; filename="+ fileName +".docx");
						
					    JRDocxExporter exporterDocx = new JRDocxExporter();
						exporterDocx.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
						exporterDocx.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
						exporterDocx.setParameter(JRDocxExporterParameter.FLEXIBLE_ROW_HEIGHT, Boolean.TRUE);
						exporterDocx.exportReport();
						output = byteArrayOutputStream.toByteArray();
						
						File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
							dirPath.mkdirs();
						}			
						String tempFileFileName = fileName;
						String fileFileName = tempFileFileName+"_"+maxId+".docx";
						files.add(uploadDir + fileFileName);
						File newFile = new File(uploadDir + fileFileName);
						filePath =(uploadDir+fileFileName );
						if (newFile.exists()) {
							newFile.delete();
						}
						if(!attachedfilePath.equalsIgnoreCase("")){
							attachedfilePath=filePath+"~"+attachedfilePath;
				        }else{
				        	attachedfilePath=filePath;
				        }
						
						FileOutputStream fos = new FileOutputStream(newFile);
						fos.write(output);
						fos.flush();
						fos.close();
						System.out.println(fileFileName);
						MyFile myFile = new MyFile();
						  myFile.setCustomerNumber(sequenceNumber);
						  myFile.setFileId(jobNumber);
						  myFile.setFileContentType("application/octet-stream");
						  myFile.setFileFileName(fileFileName);
						  myFile.setLocation(filePath);
						  myFile.setFileType("General");
						  myFile.setDocumentCategory("General");
						  myFile.setDescription("General");
						  myFile.setCorpID(sessionCorpID);
						  myFile.setActive(true);
						  myFile.setCreatedBy(getRequest().getRemoteUser()+"_"+"DOCBUNDLE");
						  myFile.setUpdatedBy(getRequest().getRemoteUser()+"_"+"DOCBUNDLE");
						  myFile.setCreatedOn(new Date());
						  myFile.setUpdatedOn(new Date());
						  myFile.setNetworkLinkId(null);
						  FileSizeUtil fileSizeUtil = new FileSizeUtil(); 
						  myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));		
						  myFile.setMapFolder("");
						  if(reports.getSecureForm() != null && reports.getSecureForm().equalsIgnoreCase("Yes")){
					    	myFile.setIsSecure(true);
						  }else{
					    	myFile.setIsSecure(false);
						  }  
						myFileManager.save(myFile); 
						
					}
				}catch(Exception e){
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	 e.printStackTrace();
				}
				
			}
			if(myFileId != null && !myFileId.equalsIgnoreCase("")){
				try{
					String fileName = "";
					String arr[]=myFileId.split("~");
					String myFileIdTemp = "";
					 for(int x=0;x<arr.length;x++){
						MyFile myFile = myFileManager.get(Long.parseLong(arr[x]));
						fileName = myFile.getFileFileName();
					 
						if(uploadFilePath.isEmpty()){
							uploadFilePath =(uploadDir+fileName );
						}else{
							uploadFilePath =uploadFilePath+"~"+(uploadDir+fileName );
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
				
				if(!uploadFilePath.isEmpty()){
					if(attachedfilePath == null || attachedfilePath.isEmpty()){
						attachedfilePath = uploadFilePath;
					}else{
						attachedfilePath = attachedfilePath+"~"+uploadFilePath;
					}
				}
				
				EmailSetup emailSetup = new EmailSetup();
				emailSetup.setRetryCount(0);
				emailSetup.setRecipientTo(receipientTo);
				emailSetup.setRecipientCc(recipientCC);
				emailSetup.setRecipientBcc("");
				emailSetup.setSubject(reportSubject);
				reportBody=reportBody.replaceAll("\r\n", "<br>");
				emailSetup.setBody(reportBody);
				emailSetup.setSignature(user.getEmail());
				emailSetup.setAttchedFileLocation(attachedfilePath);
				emailSetup.setDateSent(new Date());
				emailSetup.setEmailStatus("SaveForEmail");
				emailSetup.setCorpId(sessionCorpID);
				emailSetup.setCreatedOn(new Date());
				emailSetup.setCreatedBy("EMAILSETUP");
				emailSetup.setUpdatedOn(new Date());
				emailSetup.setUpdatedBy("EMAILSETUP");
				reportSignaturePart = reportSignaturePart.replaceAll("\n", "<br>");
				reportSignaturePart = reportSignaturePart.replaceAll("\r\n", "<br>");
				emailSetup.setSignaturePart(reportSignaturePart);
				emailSetup.setModule("ServiceOrder");
				emailSetup.setFileNumber(jobNumber);		
				emailSetup=emailSetupManager.save(emailSetup);
				  
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
	}
	private String fileTypeName;
	public String generateFile() throws Exception{
		
		 Map parameters = new HashMap();
		 parameters.put("Service Order Number", jobNumber);
		 parameters.put("Corporate ID", sessionCorpID);
		 company= companyManager.findByCorpID(sessionCorpID).get(0);
			if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
				Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			} else {
				Locale locale = new Locale("en", "US");
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
			
	 	byte[] output;
	 	JasperPrint jasperPrint = new JasperPrint();
		HttpServletResponse response = getResponse();
		ServletOutputStream ouputStream = response.getOutputStream();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		
		jasperPrint = reportsManager.generateReport(Long.parseLong(reportsId), parameters);
		reports = reportsManager.get(Long.parseLong(reportsId));
		String temp = reports.getReportName();
		String fileName = temp.substring(0, temp.indexOf("."));
		try{
			if(fileTypeName!=null && !fileTypeName.equals("") && fileTypeName.equalsIgnoreCase("PDF")){
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "attachment; filename="+fileName+".pdf");
				
				output = JasperExportManager.exportReportToPdf(jasperPrint);
				ouputStream.write(output);
			}
			if(fileTypeName!=null && !fileTypeName.equals("") && fileTypeName.equalsIgnoreCase("DOCX")){
				response.setContentType("application/docx");
			    response.setHeader("Content-Disposition", "attachment; filename="+ fileName +".docx");
				
			    JRDocxExporter exporterDocx = new JRDocxExporter();
				exporterDocx.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporterDocx.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
				exporterDocx.setParameter(JRDocxExporterParameter.FLEXIBLE_ROW_HEIGHT, Boolean.TRUE);
				exporterDocx.exportReport();
				ouputStream.write(byteArrayOutputStream.toByteArray());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		ouputStream.flush();
		ouputStream.close();
		return null;
	}
		
	@SkipValidation
	public String uploadFileFromEmail(){
		return SUCCESS;
		}
	
	@SkipValidation
	public void uploadFileFromStatus(String uploadDir,int maxId){
		try{
		File fileToCreate = new File(uploadDir,this.uploadFileFileName.split("\\.")[0].toString()+"_"+maxId+"."+this.uploadFileFileName.split("\\.")[1].toString()); // Create file name  same as original
	    FileUtils.copyFile(this.uploadFile,fileToCreate); // Just copy temp file content tos this file	
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public File getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(File uploadFile) {
		this.uploadFile = uploadFile;
	}

public String getUploadFileContentType() {
		return uploadFileContentType;
	}

	public void setUploadFileContentType(String uploadFileContentType) {
		this.uploadFileContentType = uploadFileContentType;
	}

	public String getUploadFileFileName() {
		return uploadFileFileName;
	}

	public void setUploadFileFileName(String uploadFileFileName) {
		this.uploadFileFileName = uploadFileFileName;
	}

	/*	@SkipValidation
	public String uploadMyDocBundle(){
		return SUCCESS;
	}
*/
	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Map<String, String> getrouting() {
		return routing;
	}

	public void setrouting(Map<String, String> routing) {
		this.routing = routing;
	}

	public List getMode() {
		return mode;
	}

	public void setMode(List mode) {
		this.mode = mode;
	}

	public Map<String, String> getJobs() {
		return jobs;
	}

	public void setJobs(Map<String, String> jobs) {
		this.jobs = jobs;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setDocumentBundleManager(DocumentBundleManager documentBundleManager) {
		this.documentBundleManager = documentBundleManager;
	}

	public List getDocumentBundleList() {
		return documentBundleList;
	}

	public void setDocumentBundleList(List documentBundleList) {
		this.documentBundleList = documentBundleList;
	}

	public void setAuditSetupManager(AuditSetupManager auditSetupManager) {
		this.auditSetupManager = auditSetupManager;
	}

	public Map<String, String> getRouting() {
		return routing;
	}

	public void setRouting(Map<String, String> routing) {
		this.routing = routing;
	}

	public List getTableList() {
		return tableList;
	}

	public void setTableList(List tableList) {
		this.tableList = tableList;
	}

	public List getFieldList() {
		return fieldList;
	}

	public void setFieldList(List fieldList) {
		this.fieldList = fieldList;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List getMultiplServiceTypes() {
		return multiplServiceTypes;
	}

	public void setMultiplServiceTypes(List multiplServiceTypes) {
		this.multiplServiceTypes = multiplServiceTypes;
	}

	public List getMultiplJobType() {
		return multiplJobType;
	}

	public void setMultiplJobType(List multiplJobType) {
		this.multiplJobType = multiplJobType;
	}

	public List getDocumentBundleFormsList() {
		return documentBundleFormsList;
	}

	public void setDocumentBundleFormsList(List documentBundleFormsList) {
		this.documentBundleFormsList = documentBundleFormsList;
	}

	public DocumentBundle getDocumentBundle() {
		return documentBundle;
	}

	public void setDocumentBundle(DocumentBundle documentBundle) {
		this.documentBundle = documentBundle;
	}

	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}

	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}

	public String getJrxmlName() {
		return jrxmlName;
	}

	public void setJrxmlName(String jrxmlName) {
		this.jrxmlName = jrxmlName;
	}

	public String getReportsId() {
		return reportsId;
	}

	public void setReportsId(String reportsId) {
		this.reportsId = reportsId;
	}

	public String getEmailTable() {
		return emailTable;
	}

	public void setEmailTable(String emailTable) {
		this.emailTable = emailTable;
	}

	public String getEmailField() {
		return emailField;
	}

	public void setEmailField(String emailField) {
		this.emailField = emailField;
	}

	public String getBundleName() {
		return bundleName;
	}

	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getServiceMode() {
		return serviceMode;
	}

	public void setServiceMode(String serviceMode) {
		this.serviceMode = serviceMode;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getTabId() {
		return tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public Map<String, String> getService() {
		return service;
	}

	public void setService(Map<String, String> service) {
		this.service = service;
	}

	public List getMultiplRouting() {
		return multiplRouting;
	}

	public void setMultiplRouting(List multiplRouting) {
		this.multiplRouting = multiplRouting;
	}

	public String getRoutingType() {
		return routingType;
	}

	public void setRoutingType(String routingType) {
		this.routingType = routingType;
	}

	public void setReportsManager(ReportsManager reportsManager) {
		this.reportsManager = reportsManager;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public Reports getReports() {
		return reports;
	}

	public void setReports(Reports reports) {
		this.reports = reports;
	}

	public String getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getReceipientTo() {
		return receipientTo;
	}

	public void setReceipientTo(String receipientTo) {
		this.receipientTo = receipientTo;
	}

	public String getReportSubject() {
		return reportSubject;
	}

	public void setReportSubject(String reportSubject) {
		this.reportSubject = reportSubject;
	}

	public String getReportBody() {
		return reportBody;
	}

	public void setReportBody(String reportBody) {
		this.reportBody = reportBody;
	}

	public String getRecipientCC() {
		return recipientCC;
	}

	public void setRecipientCC(String recipientCC) {
		this.recipientCC = recipientCC;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public EmailSetup getEmailSetup() {
		return emailSetup;
	}

	public void setEmailSetup(EmailSetup emailSetup) {
		this.emailSetup = emailSetup;
	}

	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getReportSignaturePart() {
		return reportSignaturePart;
	}

	public void setReportSignaturePart(String reportSignaturePart) {
		this.reportSignaturePart = reportSignaturePart;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public Long getDbId() {
		return dbId;
	}

	public void setDbId(Long dbId) {
		this.dbId = dbId;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getFileTypeName() {
		return fileTypeName;
	}

	public void setFileTypeName(String fileTypeName) {
		this.fileTypeName = fileTypeName;
	}

	public String getDocumentBundleURL() {
		return documentBundleURL;
	}

	public void setDocumentBundleURL(String documentBundleURL) {
		this.documentBundleURL = documentBundleURL;
	}

	public List getEmailFieldList() {
		return emailFieldList;
	}

	public void setEmailFieldList(List emailFieldList) {
		this.emailFieldList = emailFieldList;
	}

	public List getEmailTableList() {
		return emailTableList;
	}

	public void setEmailTableList(List emailTableList) {
		this.emailTableList = emailTableList;
	}

	public List getEmailList() {
		return emailList;
	}

	public void setEmailList(List emailList) {
		this.emailList = emailList;
	}

	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}

	public String getClickedField() {
		return clickedField;
	}

	public void setClickedField(String clickedField) {
		this.clickedField = clickedField;
	}

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}

	public File[] getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File[] fileUpload) {
		this.fileUpload = fileUpload;
	}

	public String[] getFileUploadFileName() {
		return fileUploadFileName;
	}

	public void setFileUploadFileName(String[] fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}

	public String[] getFileUploadContentType() {
		return fileUploadContentType;
	}

	public void setFileUploadContentType(String[] fileUploadContentType) {
		this.fileUploadContentType = fileUploadContentType;
	}

	public List getFileCabinetList() {
		return fileCabinetList;
	}

	public void setFileCabinetList(List fileCabinetList) {
		this.fileCabinetList = fileCabinetList;
	}

	public String getMyFileId() {
		return myFileId;
	}

	public void setMyFileId(String myFileId) {
		this.myFileId = myFileId;
	}

	
}
