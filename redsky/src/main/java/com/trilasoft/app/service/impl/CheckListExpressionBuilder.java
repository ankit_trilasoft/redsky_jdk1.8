package com.trilasoft.app.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.trilasoft.app.model.CheckList;

public class CheckListExpressionBuilder {
	public static String getFromClause(String entity) {
		String fromClause = entity.toLowerCase();
		if (entity.equalsIgnoreCase("serviceorder")) {
			//fromClause += " as serviceorder,customerfile as customerfile, billing as billing,trackingstatus as trackingstatus";
			//fromClause += " serviceorder inner join customerfile on serviceorder.customerFileId=customerfile.id inner join trackingstatus on serviceorder.id=trackingstatus.id inner join miscellaneous on serviceorder.id=miscellaneous.id inner join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId";
			fromClause += " serviceorder inner join customerfile customerfile on serviceorder.customerFileId=customerfile.id inner join trackingstatus trackingstatus on serviceorder.id=trackingstatus.id inner join billing billing on serviceorder.id=billing.id left outer join myfile myfile on serviceorder.shipNumber=myfile.fileId ";
		}
		if (entity.equalsIgnoreCase("TrackingStatus")) {
			fromClause += " as trackingstatus inner join serviceorder serviceorder on trackingstatus.id=serviceorder.id inner join customerfile customerfile on customerfile.id=serviceorder.customerfileId inner join billing billing on  serviceorder.id=billing.id left outer join myfile myfile on serviceorder.shipNumber=myfile.fileId ";
			//fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId";
		}
		
		if (entity.equalsIgnoreCase("billing")) {
			fromClause += " as billing inner join serviceorder serviceorder on billing.id=serviceorder.id inner join customerfile customerfile on customerfile.id=serviceorder.customerfileId inner join trackingstatus trackingstatus on  serviceorder.id=trackingstatus.id left outer join myfile myfile on serviceorder.shipNumber=myfile.fileId ";

		}	else if (entity.equalsIgnoreCase("customerfile")) {
			fromClause += " customerfile left outer join serviceorder serviceorder on customerfile.id=serviceorder.customerFileId left outer join billing billing on serviceorder.id=billing.id left outer join trackingstatus trackingstatus on serviceorder.id=trackingstatus.id left outer join myfile myfile on serviceorder.shipNumber=myfile.fileId ";
		}else if(entity.equalsIgnoreCase("QuotationFile")){
			fromClause="customerfile";
			entity="customerfile";
			fromClause += " customerfile left outer join serviceorder serviceorder on customerfile.id=serviceorder.customerFileId left outer join billing billing on serviceorder.id=billing.id left outer join trackingstatus trackingstatus on serviceorder.id=trackingstatus.id left outer join myfile myfile on serviceorder.shipNumber=myfile.fileId ";
		}else if(entity.equalsIgnoreCase("Claim")){
			fromClause += " claim left outer join serviceorder on claim.serviceorderid=serviceorder.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join billing on serviceorder.id=billing.id left outer join customerfile on customerfile.id=serviceorder.customerFileId left outer join myfile myfile on serviceorder.shipNumber=myfile.fileId";
		}else if(entity.equalsIgnoreCase("DspDetails")){
			fromClause += " dspdetails left outer join serviceorder on dspdetails.id=serviceorder.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join billing on serviceorder.id=billing.id left outer join customerfile on customerfile.id=serviceorder.customerFileId left outer join myfile myfile on serviceorder.shipNumber=myfile.fileId ";
			
		}
		

		/*else if (entity.equalsIgnoreCase("vehicle")) {
			fromClause += " vehicle left outer join serviceorder on vehicle.serviceOrderId=serviceorder.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join billing on serviceorder.id=billing.id left outer join customerfile on customerfile.id=serviceorder.customerFileId";

		}
		
		else if (entity.equalsIgnoreCase("AccountLine")) {
				fromClause += " accountline left outer join serviceorder on accountline.serviceOrderId=serviceorder.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join billing on serviceorder.id=billing.id left outer join customerfile on customerfile.id=serviceorder.customerFileId";

		}
		
		else if (entity.equalsIgnoreCase("WorkTicket")) {
			fromClause += " workticket left outer join serviceorder on workticket.serviceOrderId=serviceorder.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join billing on serviceorder.id=billing.id left outer join customerfile on customerfile.id=serviceorder.customerFileId left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join refmaster on workticket.warehouse=refmaster.code and refmaster.parameter = 'HOUSE'";

		}
		
		else if (entity.equalsIgnoreCase("AccountLineList")) {
			fromClause = "serviceorder";
			fromClause += " serviceorder left outer join customerfile on serviceorder.customerFileId=customerfile.id left outer join trackingstatus on serviceorder.id=trackingstatus.id left outer join miscellaneous on serviceorder.id=miscellaneous.id left outer join billing on serviceorder.id=billing.id left outer join servicepartner on serviceorder.id=servicepartner.serviceOrderId";

		}*/
		
		return fromClause;
	}

	public static String getCoordinator(String entity) {
		String fromClause = entity;
		if (entity.equals("CustomerFile")) {
			fromClause = "customerfile.coordinator";
		}else{
			fromClause = "serviceorder.coordinator";
		}
		return fromClause;
	}
	public static String getAuditor(String entity) {
		String fromClause = entity;
		if (entity.equals("CustomerFile") || entity.equals("QuotationFile")) {
			fromClause = "customerfile.auditor";
		} else{
			fromClause = "billing.auditor";	
		}		
		return fromClause;
	}
	public static String getOrginAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder")) {
			fromClause = "trackingstatus.originAgentCode";
		}
		if (entity.equals("CustomerFile")) {
			fromClause = "customerfile.originAgentCode";
		}
		return fromClause;
	}
	public static String getDestinationAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("Billing") || entity.equals("Miscellaneous") || entity.equals("TrackingStatus")) {
			fromClause = "trackingstatus.destinationAgentCode";
		}
		return fromClause;
	}
	
	public static String getForwarderAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("Billing") || entity.equals("Miscellaneous") || entity.equals("TrackingStatus")) {
			fromClause = "trackingstatus.forwarderCode";
		}
		return fromClause;
	}
	public static String getBrokerAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("Billing") || entity.equals("Miscellaneous") || entity.equals("TrackingStatus")) {
			fromClause = "trackingstatus.brokerCode";
		}
		return fromClause;
	}
	public static String getOrginSubAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("Billing") || entity.equals("Miscellaneous") || entity.equals("TrackingStatus")) {
			fromClause = "trackingstatus.originSubAgentCode";
		}
		return fromClause;
	}
	public static String getDestinationSubAgent(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("Billing") || entity.equals("Miscellaneous") || entity.equals("TrackingStatus")) {
			fromClause = "trackingstatus.destinationSubAgentCode";
		}
		return fromClause;
	}
	public static String getMmCounselor(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("Billing") || entity.equals("Miscellaneous") || entity.equals("TrackingStatus")) {
			fromClause = "serviceorder.mmCounselor";
		}
		return fromClause;
	}
	public static String getForwarder(String entity) {
		String fromClause = "";
		if (entity.equals("ServiceOrder") || entity.equals("Billing") || entity.equals("Miscellaneous") || entity.equals("TrackingStatus")) {
			fromClause = "billing.personForwarder";
		}
		return fromClause;
	}
	public static String getFileNumber(String entity) {
		String fromClause = entity;
		if (entity.equals("ServiceOrder") || entity.equals("Billing") || entity.equals("Miscellaneous") || entity.equals("TrackingStatus")) {
			fromClause = "serviceorder.shipNumber";
		}
		else if (entity.equals("CustomerFile")) {
			fromClause = "customerfile.sequenceNumber";

		} else if (entity.equals("ServicePartner")) {
			fromClause = "serviceorder.shipNumber";

		} else if (entity.equals("Vehicle")) {
			fromClause = "serviceorder.shipNumber";

		}
		else if (entity.equals("AccountLine")) {
			fromClause = "serviceorder.shipNumber";

		}
		else if (entity.equals("WorkTicket")) {
			fromClause = "workticket.ticket";

		}
		else if (entity.equals("AccountLineList")) {
			fromClause = "serviceorder.shipNumber";

		}else if(entity.equals("QuotationFile")){
			fromClause = "customerfile.sequenceNumber";	
		}else if(entity.equals("Claim")){
			fromClause="claim.claimNumber";	
		}else if(entity.equalsIgnoreCase("DspDetails")){
			fromClause = "serviceorder.shipNumber";
		}
		return fromClause;
	}

	public static String getBilling(String entity) {
		String fromClause = entity;
		fromClause = "billing.personBilling";
		return fromClause;
	}
	public static String getClaimHandLer(String entity) {
		String fromClause = "";
		if (entity.equals("Billing")) {
			fromClause = "billing.claimHandler";
		}
		return fromClause;
	}
	public static String getPayable(String entity) {
		String fromClause = entity;
		fromClause = "billing.personPayable";
		return fromClause;
	}
	
	public static String getPricing(String entity) {
		String fromClause = entity;
		fromClause = "billing.personPricing";
		return fromClause;
	}
	
	public static String getConsultant(String entity) {
		String fromClause = entity;
		fromClause = "customerfile.estimator";
		return fromClause;
	}
	
	public static String getSalesMan(String entity) {
		String fromClause = entity;
		fromClause = "serviceorder.salesMan";
		return fromClause;
	}
	
	public static String getWarehousemanager(String entity) {
		String fromClause = entity;
		fromClause = "refmaster.billCrew";
		return fromClause;
	}
	
	

	public static String getShipper(String entity) {
		String fromClause = entity;
		fromClause = "if(serviceorder.firstName is null or serviceorder.firstName='' ,'',serviceorder.firstName), if(serviceorder.lastName is null or serviceorder.lastName='','',serviceorder.lastName)";
		return fromClause;
	}

	public static String getJoinClause(String entity) {
		String joinClause = entity;
		if (entity.equals("TrackingStatus")) {
			joinClause = "serviceorder.sequenceNumber=customerfile.sequenceNumber and serviceorder.shipNumber= trackingstatus.shipNumber and serviceorder.shipNumber=myfile.shipNumber and serviceorder.shipNumber=billing.shipNumber and serviceorder.shipNumber=accountline.shipNumber and serviceorder.shipNumber=servicepartner.shipNumber and serviceorder.shipNumber=vehicle.shipNumber";
		}
		if (entity.equals("ServiceOrder")) {
			joinClause = "customerfile.id=serviceorder.customerfileId and serviceorder.id= trackingstatus.id and serviceorder.id=billing.id";
		}//else if (entity.equals("customerFile")){
		else if (entity.equals("CustomerFile")) {
			joinClause = " customerfile.sequenceNumber= serviceorder.sequenceNumber and customerfile.sequenceNumber= miscellaneous.sequenceNumber and customerfile.sequenceNumber=billing.sequenceNumber";
		} else if (entity.equals("Miscellaneous")) {
			joinClause = " miscellaneous.shipNumber= serviceorder.shipNumber";
		} else if (entity.equals("ServicePartner")) {
			joinClause = " servicepartner.shipNumber= serviceorder.shipNumber and servicepartner.shipNumber=trackingstatus.shipNumber and servicepartner.shipNumber=billing.shipNumber";
		} else if (entity.equals("Vehicle")) {
			joinClause = " vehicle.shipNumber= serviceorder.shipNumber and vehicle.shipNumber=trackingstatus.shipNumber and vehicle.shipNumber=billing.shipNumber";
		}
		return joinClause;
	}

	public static String buildExpression(String entity, String dateExpression, String sessionCorpID, CheckList checkList) {
			String queryParameter="";
	/*		if(entity.equalsIgnoreCase("QuotationFile")){
				entity="customerfile";
				}*/
			queryParameter= " from " + getFromClause(entity) + " where "+getTableCorpId(entity)+".corpID='"+sessionCorpID+ "' "  
			+ getSubQuery(checkList,entity)+ " "+getSubQueryForBillToExcludes(checkList,entity)+" AND "	 + dateExpression +" AND " + getStopDate(checkList) + " AND  "+getstatus(entity)+" group by "+getTableId(entity)+" "+getMyFileQuery(checkList)+"";
		
		return queryParameter;
	}

	private static String getStopDate(CheckList checkList) {
		String stopDateQuery="";
		String serviceOrderDateQuery="";
		String customerFileDateQuery="";
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		
		if(checkList.getServiceOrderDate()==null){
			serviceOrderDateQuery="serviceorder.createdOn>='2010-01-01'";
		}else{
			StringBuilder serviceOrderFormattedDate = new StringBuilder(dateformatYYYYMMDD1.format(checkList.getServiceOrderDate()));
			serviceOrderDateQuery="serviceorder.createdOn>='"+serviceOrderFormattedDate+"'";
		}
		if(checkList.getCustomerFileDate()==null){
			customerFileDateQuery="customerfile.createdOn>='2010-01-01'";
		}else{
			StringBuilder customerFileFormattedDate = new StringBuilder(dateformatYYYYMMDD1.format(checkList.getCustomerFileDate()));
			customerFileDateQuery="customerfile.createdOn>='"+customerFileFormattedDate+"'";
		}
		
		stopDateQuery="(" +serviceOrderDateQuery+ " or " +customerFileDateQuery+ ")";
		
		return stopDateQuery;
	}
	private static String getstatus(String entity) {
		String query="";
		if (entity.equalsIgnoreCase("ServiceOrder")){
			query = " serviceorder.status <>'CNCL'";
		}
		else if (entity.equalsIgnoreCase("CustomerFile")){
			query = " customerfile.status<>'CNCL' and customerfile.controlFlag='C' ";
		}
		else if (entity.equalsIgnoreCase("ServicePartner")){
			query = " servicepartner.id";
		}
		else if (entity.equalsIgnoreCase("Vehicle")){
			query = " serviceorder.status <>'CNCL'";
		}
		else if (entity.equalsIgnoreCase("AccountLine")){
			query = " serviceorder.status <>'CNCL'";
		}
		else if (entity.equalsIgnoreCase("WorkTicket")){
			query = " serviceorder.status <>'CNCL'";
		}
		
		else if (entity.equalsIgnoreCase("AccountLineList")){
			query = " serviceorder.status <>'CNCL'";
		}
		
		else if (entity.equalsIgnoreCase("Billing")){
			query = " serviceorder.status <>'CNCL'";
		}
		
		else if (entity.equalsIgnoreCase("Miscellaneous")){
			query = " serviceorder.status <>'CNCL'";
		}else if(entity.equalsIgnoreCase("QuotationFile")){
			query = " customerfile.status<>'CNCL' and customerfile.controlFlag='Q' ";
		}else if(entity.equalsIgnoreCase("Claim")){
			query = " serviceorder.status <>'CNCL'";
		}else if(entity.equalsIgnoreCase("TrackingStatus")){
			query = " serviceorder.status <>'CNCL'";
		}else if(entity.equals("DspDetails")){
			query = " serviceorder.status <>'CNCL'";
		}
		
		return query;
		
	}
	private static String getTableId(String entity) {
		String query="";
		if (entity.equalsIgnoreCase("ServiceOrder")){
			query = " serviceorder.id";
		}
		else if (entity.equalsIgnoreCase("CustomerFile")){
			query = " customerfile.id";
		}
		else if (entity.equalsIgnoreCase("ServicePartner")){
			query = " servicepartner.id";
		}
		else if (entity.equalsIgnoreCase("Vehicle")){
			query = " vehicle.id";
		}
		else if (entity.equalsIgnoreCase("AccountLine")){
			query = " accountline.id";
		}
		else if (entity.equalsIgnoreCase("WorkTicket")){
			query = " workticket.id";
		}
		
		else if (entity.equalsIgnoreCase("AccountLineList")){
			query = " serviceorder.id";
		}
		
		else if (entity.equalsIgnoreCase("Billing")){
			query = " billing.id";
		}
		
		else if (entity.equalsIgnoreCase("Miscellaneous")){
			query = " miscellaneous.id";
		}else if(entity.equalsIgnoreCase("QuotationFile")){
			query = " customerfile.id";
		}else if(entity.equalsIgnoreCase("Claim")){
			query = " claim.id";
		}else if(entity.equalsIgnoreCase("TrackingStatus")){
			query = " trackingstatus.id";
		}else if(entity.equals("DspDetails")){
			query = " dspdetails.id";
		}
		
		return query;
		
	}
	private static String getTableCorpId(String entity) {
		String query="";
		if (entity.equalsIgnoreCase("ServiceOrder")){
			query = " serviceorder";
		}
		else if (entity.equalsIgnoreCase("CustomerFile")){
			query = " customerfile";
		}
		else if (entity.equalsIgnoreCase("ServicePartner")){
			query = " servicepartner";
		}
		else if (entity.equalsIgnoreCase("Vehicle")){
			query = " vehicle";
		}
		else if (entity.equalsIgnoreCase("AccountLine")){
			query = " accountline";
		}
		else if (entity.equalsIgnoreCase("WorkTicket")){
			query = " workticket";
		}
		
		else if (entity.equalsIgnoreCase("AccountLineList")){
			query = " serviceorder";
		}
		
		else if (entity.equalsIgnoreCase("Billing")){
			query = " billing";
		}
		
		else if (entity.equalsIgnoreCase("Miscellaneous")){
			query = " miscellaneous";
		}else if(entity.equalsIgnoreCase("QuotationFile")){
			query = " customerfile";
		}else if(entity.equalsIgnoreCase("Claim")){
			query = " claim";
		}else if(entity.equalsIgnoreCase("TrackingStatus")){
			query = " trackingstatus";
		}else if(entity.equals("DspDetails")){
			query = " dspdetails";
		}
		
		return query;
		
	}

	private static String getMyFileQuery(CheckList checkList) {
		String query=" having (documenttypes is null or documenttypes not like concat('%"+checkList.getDocumentType()+"%')) ";
		return query;
	}

	private static String getSubQuery(CheckList checkList,String entity) {
		String subQuery="";
		String partnerCode="";
		if(!checkList.getPartnerCode().equalsIgnoreCase("")){
           if(checkList.getPartnerCode().contains(",")){
           String abcPartner[]=checkList.getPartnerCode().split(",");
        	for (String string : abcPartner){
				if(partnerCode.equals("")){
					partnerCode="'"+string.replaceAll("'", "''")+"'";
				}else{
					partnerCode=partnerCode+",'"+string.replaceAll("'", "''")+"'";
				}
			} 
           }else{
        	   partnerCode="'"+checkList.getPartnerCode().replaceAll("'", "''")+"'";
           }
		
			String query="";
			if (entity.equalsIgnoreCase("ServiceOrder")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND billing.billToCode in ("+partnerCode+") ";
			}
			else if (entity.equalsIgnoreCase("CustomerFile")){
				//subQuery = " AND customerfile.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND customerfile.billToCode in ("+partnerCode+") ";
			}
			else if (entity.equalsIgnoreCase("ServicePartner")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND billing.billToCode in ("+partnerCode+") ";
			}
			else if (entity.equalsIgnoreCase("Vehicle")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND billing.billToCode in ("+partnerCode+") ";
			}
			else if (entity.equalsIgnoreCase("AccountLine")){
				//subQuery = " AND accountline.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND accountline.billToCode in ("+partnerCode+") ";
			}
			else if (entity.equalsIgnoreCase("WorkTicket")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND billing.billToCode in ("+partnerCode+") ";
			}
			
			else if (entity.equalsIgnoreCase("AccountLineList")){
				//subQuery = " AND accountline.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND accountline.billToCode in ("+partnerCode+") ";
			}
			
			else if (entity.equalsIgnoreCase("Billing")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND billing.billToCode in ("+partnerCode+") ";
			}
			
			else if (entity.equalsIgnoreCase("Miscellaneous")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND billing.billToCode in ("+partnerCode+") ";
			}else if(entity.equalsIgnoreCase("QuotationFile")){
				//subQuery = " AND customerfile.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND customerfile.billToCode in ("+partnerCode+") ";
			}else if(entity.equalsIgnoreCase("Claim")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND billing.billToCode in ("+partnerCode+") ";
			}else if(entity.equalsIgnoreCase("TrackingStatus")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND billing.billToCode in ("+partnerCode+") ";
			}else if(entity.equals("DspDetails")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				subQuery = " AND billing.billToCode in ("+partnerCode+") ";
			}
		}
		    if(checkList.getJobType() != null && !checkList.getJobType().equalsIgnoreCase("") && (entity.equalsIgnoreCase("ServiceOrder") || entity.equalsIgnoreCase("ServicePartner") || entity.equalsIgnoreCase("Vehicle") || entity.equalsIgnoreCase("AccountLine") || entity.equalsIgnoreCase("AccountLineList") || entity.equalsIgnoreCase("Billing") || entity.equalsIgnoreCase("Miscellaneous") || entity.equalsIgnoreCase("TrackingStatus") || entity.equalsIgnoreCase("DspDetails"))){
				subQuery += " AND serviceorder.job = '"+checkList.getJobType()+"' ";
			}else if(checkList.getJobType() != null && !checkList.getJobType().equalsIgnoreCase("") && (entity.equalsIgnoreCase("CustomerFile") || entity.equalsIgnoreCase("QuotationFile"))){
				subQuery += " AND customerfile.job = '"+checkList.getJobType()+"' ";
			}
		    if(checkList.getRouting() != null && !checkList.getRouting().equalsIgnoreCase("") && (entity.equalsIgnoreCase("ServiceOrder") || entity.equalsIgnoreCase("ServicePartner") || entity.equalsIgnoreCase("Vehicle") || entity.equalsIgnoreCase("AccountLine") || entity.equalsIgnoreCase("AccountLineList") || entity.equalsIgnoreCase("Billing") || entity.equalsIgnoreCase("Miscellaneous") || entity.equalsIgnoreCase("TrackingStatus") || entity.equalsIgnoreCase("DspDetails"))){
					subQuery += " AND serviceorder.routing = '"+checkList.getRouting()+"' ";
			}
		    if(checkList.getService() != null && !checkList.getService().equalsIgnoreCase("") && (entity.equalsIgnoreCase("ServiceOrder") || entity.equalsIgnoreCase("ServicePartner") || entity.equalsIgnoreCase("Vehicle") || entity.equalsIgnoreCase("AccountLine") || entity.equalsIgnoreCase("AccountLineList") || entity.equalsIgnoreCase("Billing") || entity.equalsIgnoreCase("Miscellaneous") || entity.equalsIgnoreCase("TrackingStatus") || entity.equalsIgnoreCase("DspDetails"))){
				subQuery += " AND serviceorder.serviceType = '"+checkList.getService()+"' ";
			}
		    if(checkList.getMode() != null && !checkList.getMode().equalsIgnoreCase("") && (entity.equalsIgnoreCase("ServiceOrder") || entity.equalsIgnoreCase("ServicePartner") || entity.equalsIgnoreCase("Vehicle") || entity.equalsIgnoreCase("AccountLine") || entity.equalsIgnoreCase("AccountLineList") || entity.equalsIgnoreCase("Billing") || entity.equalsIgnoreCase("Miscellaneous") || entity.equalsIgnoreCase("TrackingStatus") || entity.equalsIgnoreCase("DspDetails"))){
				subQuery += " AND serviceorder.mode = '"+checkList.getMode()+"' ";
			}
			if(checkList.getImportCountry()!=null && !checkList.getImportCountry().equalsIgnoreCase("") && (entity.equalsIgnoreCase("ServiceOrder") || entity.equalsIgnoreCase("ServicePartner") || entity.equalsIgnoreCase("Vehicle") || entity.equalsIgnoreCase("AccountLine") || entity.equalsIgnoreCase("AccountLineList") || entity.equalsIgnoreCase("Billing") || entity.equalsIgnoreCase("Miscellaneous") || entity.equalsIgnoreCase("TrackingStatus") || entity.equalsIgnoreCase("DspDetails"))){
				subQuery +=" AND serviceorder.originCountry='"+checkList.getImportCountry()+"' ";
			}else if(checkList.getImportCountry()!=null && !checkList.getImportCountry().equalsIgnoreCase("") && (entity.equalsIgnoreCase("CustomerFile") || entity.equalsIgnoreCase("QuotationFile"))){
				subQuery +=" AND customerfile.originCountry='"+checkList.getImportCountry()+"' ";
			}
			if(checkList.getExportCountry()!=null && !checkList.getExportCountry().equalsIgnoreCase("") && (entity.equalsIgnoreCase("ServiceOrder") || entity.equalsIgnoreCase("ServicePartner") || entity.equalsIgnoreCase("Vehicle") || entity.equalsIgnoreCase("AccountLine") || entity.equalsIgnoreCase("AccountLineList") || entity.equalsIgnoreCase("Billing") || entity.equalsIgnoreCase("Miscellaneous") || entity.equalsIgnoreCase("TrackingStatus") || entity.equalsIgnoreCase("DspDetails"))){
				subQuery +=" AND serviceorder.destinationCountry='"+checkList.getExportCountry()+"' ";
			}else if(checkList.getExportCountry()!=null && !checkList.getExportCountry().equalsIgnoreCase("") && (entity.equalsIgnoreCase("CustomerFile") || entity.equalsIgnoreCase("QuotationFile"))){
				subQuery +=" AND customerfile.destinationCountry='"+checkList.getExportCountry()+"' ";
			}
	
		return subQuery;
	}
	private static String getSubQueryForBillToExcludes(CheckList checkList,String entity){
		String SubQueryForBillToExcludes="";
		
		String billToExcludesCode="";
		if(!checkList.getBillToExcludes().equalsIgnoreCase("")){
			billToExcludesCode="";
       if(checkList.getBillToExcludes().contains(",")){
           String abcPartner[]=checkList.getBillToExcludes().split(",");
        	for (String string : abcPartner){
				if(billToExcludesCode.equals("")){
					billToExcludesCode="'"+string.replaceAll("'", "''")+"'";
				}else{
					billToExcludesCode=billToExcludesCode+",'"+string.replaceAll("'", "''")+"'";
				}
			} 
           }else{
        	   billToExcludesCode="'"+checkList.getBillToExcludes().replaceAll("'", "''")+"'";  
           }
			String query="";
			if (entity.equalsIgnoreCase("ServiceOrder")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND billing.billToCode not in ("+billToExcludesCode+") ";
			}
			else if (entity.equalsIgnoreCase("CustomerFile")){
				//subQuery = " AND customerfile.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND customerfile.billToCode not in ("+billToExcludesCode+") ";
			}
			else if (entity.equalsIgnoreCase("ServicePartner")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND billing.billToCode not in ("+billToExcludesCode+") ";
			}
			else if (entity.equalsIgnoreCase("Vehicle")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND billing.billToCode not in ("+billToExcludesCode+") ";
			}
			else if (entity.equalsIgnoreCase("AccountLine")){
				//subQuery = " AND accountline.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND accountline.billToCode not in ("+billToExcludesCode+") ";
			}
			else if (entity.equalsIgnoreCase("WorkTicket")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND billing.billToCode not in ("+billToExcludesCode+") ";
			}
			
			else if (entity.equalsIgnoreCase("AccountLineList")){
				//subQuery = " AND accountline.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND accountline.billToCode not in ("+billToExcludesCode+") ";
			}
			
			else if (entity.equalsIgnoreCase("Billing")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND billing.billToCode not in ("+billToExcludesCode+") ";
			}
			
			else if (entity.equalsIgnoreCase("Miscellaneous")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND billing.billToCode not in ("+billToExcludesCode+") ";
			}else if(entity.equalsIgnoreCase("QuotationFile")){
				//subQuery = " AND customerfile.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND customerfile.billToCode not in ("+billToExcludesCode+") ";
			}else if(entity.equalsIgnoreCase("Claim")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND billing.billToCode not in ("+billToExcludesCode+") ";
			}else if(entity.equalsIgnoreCase("TrackingStatus")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND billing.billToCode not in ("+billToExcludesCode+") ";
			}else if(entity.equals("DspDetails")){
				//subQuery = " AND billing.billToCode = '"+checkList.getPartnerCode()+"' ";
				SubQueryForBillToExcludes = " AND billing.billToCode not in ("+billToExcludesCode+") ";
			}
		
		}
		
		return SubQueryForBillToExcludes;
	}
}
