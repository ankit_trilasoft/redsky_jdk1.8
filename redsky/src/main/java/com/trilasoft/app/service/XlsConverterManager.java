package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.Location;

public interface XlsConverterManager extends GenericManager<Location,Long> {

	String saveData(String sessionCorpID, String file, String userName);

	String saveStorageData(String sessionCorpID, String file, String userName);

	String saveRateGridData(String sessionCorpID, String file, String userName,String charge, String contractName,String fileExt);

	String saveDelRateGridData(String sessionCorpID, String file,String userName, String charge, String contractName, String tempIds,String fileExt);

	String saveXlsFileDefaultAccountline(String sessionCorpID, String file, String userName,String fileExt,Boolean costElementFlag);
	public void updateDefaultAccountlinethroughProcedure(String sessionCorpID,Boolean costElementFlag,String userName);
}
