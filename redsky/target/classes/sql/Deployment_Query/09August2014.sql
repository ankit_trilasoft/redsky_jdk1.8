// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;


delete from errorlog;

DROP TABLE IF EXISTS `redsky`.`sodashboard`;
CREATE TABLE  `redsky`.`sodashboard` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipnumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `serviceOrderId` bigint(20) NOT NULL,
  `role` varchar(29) CHARACTER SET utf8 DEFAULT '',
  `actualSurvey` datetime DEFAULT NULL,
  `estweight` varchar(21) CHARACTER SET utf8 DEFAULT NULL,
  `estvolume` decimal(19,2) DEFAULT NULL,
  `actualLoading` datetime DEFAULT NULL,
  `actualweight` varchar(21) CHARACTER SET utf8 DEFAULT NULL,
  `actVolume` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `driver` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `truck` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actualDelivery` datetime DEFAULT NULL,
  `claims` varchar(400) DEFAULT '',
  `qc` datetime DEFAULT NULL,
  `corpID` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `estimateSurvey` datetime DEFAULT NULL,
  `estimateDelivery` datetime DEFAULT NULL,
  `estimateLoading` datetime DEFAULT NULL,
  `customFlag` varchar(45) DEFAULT '',
  `customDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `serviceorderid` (`serviceOrderId`)
) ENGINE=InnoDB AUTO_INCREMENT=2261901 DEFAULT CHARSET=latin1;


################
#9382 as per my req
	
ALTER TABLE `redsky`.`sodashboard` ADD COLUMN `customerfileid` BIGINT(20) UNSIGNED NOT NULL ;

ALTER TABLE `redsky`.`sodashboard` ADD INDEX `corpid`(`corpID`);

insert into sodashboard(shipnumber,serviceorderid,corpid)
select shipnumber,id,corpid from serviceorder;

select if(loadA is null,beginLoad,loadA),if(deliveryA is null,deliveryShipper,deliveryA)
,if(cf.survey is null,cf.actualsurveydate,cf.survey),
if(
(select count(*) from companydivision c where c.corpid=s.corpid and c.bookingagentcode=s.bookingagentcode)>0,
if(cf.actualsurveydate is not null,cf.actualsurveydate,
if(date_format(cf.survey,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d'),'DT',
if(date_format(cf.survey,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d'),'OD',cf.survey))),
if((select distinct flex5 from refmaster where parameter='SERVICE' and code=s.servicetype) like '%survey%',
if(cf.actualsurveydate is not null,cf.actualsurveydate,
if(date_format(cf.survey,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d'),'DT',
if(date_format(cf.survey,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d'),'OD','NA'))),'NA')),

if(
(select count(*) from companydivision c where c.corpid=s.corpid and c.bookingagentcode=s.bookingagentcode)>0,
if(t.loadA is not null,t.loadA,
if(date_format(t.beginLoad,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d'),'DT',
if(date_format(t.beginLoad,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d'),'OD',cf.survey))),
if((select distinct flex5 from refmaster where parameter='SERVICE' and code=s.servicetype) like '%Loading%',
if(t.loadA is not null,t.loadA,
if(date_format(t.beginLoad,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d'),'DT',
if(date_format(t.beginLoad,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d'),'OD','NA'))),'NA')),

if(
(select count(*) from companydivision c where c.corpid=s.corpid and c.bookingagentcode=s.bookingagentcode)>0,
if(t.deliveryA is not null,t.deliveryA,
if(date_format(t.deliveryShipper,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d'),'DT',
if(date_format(t.deliveryShipper,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d'),'OD',cf.survey))),
if((select distinct flex5 from refmaster where parameter='SERVICE' and code=s.servicetype) like '%Delivery%',
if(t.deliveryA is not null,t.deliveryA,
if(date_format(t.deliveryShipper,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d'),'DT',
if(date_format(t.deliveryShipper,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d'),'OD','NA'))),'NA'))

from trackingstatus t,serviceorder s,customerfile cf where
s.id=t.id and cf.id=s.customerfileid ;





select concat("update sodashboard set estimateLoading='",beginLoad,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and beginLoad is not null;


select concat("update sodashboard set estimateDelivery='",deliveryShipper,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
 s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and deliveryShipper is not null;


select concat("update sodashboard set estimateSurvey='",cf.survey,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
 s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and cf.survey is not null;


select concat("update sodashboard set actualLoading='",t.loadA,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and t.loadA is not null;




select concat("update sodashboard set actualSurvey='",cf.actualsurveydate,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and cf.actualsurveydate is not null;



select concat("update sodashboard set actualDelivery='",t.deliveryA,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and t.deliveryA is not null;






select estimatednetweightkilo,ActualNetWeightKilo,NetEstimateCubicMtr,NetActualCubicMtr
from miscellaneous m,systemdefault sd ,serviceorder s
where sd.corpid=m.corpid and sd.weightunit='Kgs'
and s.id=m.id and m.unit1=sd.weightunit;



select concat("update sodashboard set estweight='",estimatednetweightkilo,"' where serviceorderid='",m.id,"';")
from miscellaneous m,systemdefault sd ,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Kgs'
and m.unit1=sd.weightunit and estimatednetweightkilo is not null
and so.serviceorderid=m.id;


select concat("update sodashboard set actualweight='",actualnetweightkilo,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Kgs'
and s.id=m.id and m.unit1=sd.weightunit and actualnetweightkilo is not null
and so.serviceorderid=s.id;


select concat("update sodashboard set estvolume='",NetEstimateCubicMtr,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Kgs'
and s.id=m.id and m.unit1=sd.weightunit and NetEstimateCubicMtr is not null
and so.serviceorderid=s.id;


select concat("update sodashboard set ActVolume='",NetActualCubicMtr,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Kgs'
and s.id=m.id and m.unit1=sd.weightunit and NetActualCubicMtr is not null
and so.serviceorderid=s.id;








select concat("update sodashboard set estweight='",m.EstimatedNetWeight,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Lbs'
and s.id=m.id  and m.unit1=sd.weightunit and m.EstimatedNetWeight is not null
and so.serviceorderid=s.id;


select concat("update sodashboard set actualweight='",m.ActualNetWeight,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Lbs'
and s.id=m.id and m.unit1=sd.weightunit and m.ActualNetWeight is not null
and so.serviceorderid=s.id;


select concat("update sodashboard set estvolume='",NetEstimateCubicFeet,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Lbs'
and s.id=m.id and m.unit1=sd.weightunit and NetEstimateCubicFeet is not null
and so.serviceorderid=s.id;


select concat("update sodashboard set ActVolume='",NetActualCubicFeet,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Lbs'
and s.id=m.id and m.unit1=sd.weightunit and NetActualCubicFeet is not null
and so.serviceorderid=s.id;





select concat("update sodashboard set customFlag='",t.clearCustomTA,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and t.clearCustomTA is not null;



select concat("update sodashboard set claims='",cl.ClaimNumber,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so,claim cl where
s.id=t.id and cf.id=s.customerfileid and cl.serviceorderid=s.id
and so.serviceorderid=s.id and cl.ClaimNumber is not null;





select concat("update sodashboard set qc='",b.AuditComplete,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so,billing b where
s.id=t.id and cf.id=s.customerfileid and b.id=s.id
and so.serviceorderid=s.id and b.AuditComplete is not null;





select concat("update sodashboard set driver='",replace(m.DriverName,'\'',''),"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so,miscellaneous m where
 s.id=t.id and cf.id=s.customerfileid and m.id=s.id
and so.serviceorderid=s.id and m.DriverName <>'';




select concat("update sodashboard set truck='",m.Carrier,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so,miscellaneous m where
 s.id=t.id and cf.id=s.customerfileid and m.id=s.id
and so.serviceorderid=s.id and m.Carrier <>'';





select concat("update sodashboard set role='",(trim( both '/' from concat(if(t.originagent is not null and t.originagent<>'','OA/',''),
if(destinationagent is not null and destinationagent <>'','DA/',''),
if(t.originsubagent is not null and t.originsubagent<>'','OSA/',''),
if(t.destinationsubagent is not null and t.destinationsubagent<>'','DSA/',''),
if(t.brokercode is not null and t.brokercode<>'','BR/',''),
if(t.forwardercode is not null and t.forwarder<>'','FR/',''),
if(s.bookingagentcode is not null and s.bookingagentcode<>'','BA/',''),
if(t.networkPartnerCode is not null and t.networkpartnercode<>'','NA/',''),
if(m.haulingAgentCode is not null and m.haulingagentcode<>'','HA/','')))),"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so,miscellaneous m where
 s.id=t.id and cf.id=s.customerfileid and m.id=s.id
and so.serviceorderid=s.id ;

###########

###########
As discussed with manish and subrat
select code,trim(both ' ' from code),description from redsky.refmaster where parameter='TYPEOFVENDOR';

update redsky.refmaster set code=trim(both ' ' from code) where parameter='TYPEOFVENDOR';
##########

ALTER TABLE `redsky`.`refquoteservices` 
ADD COLUMN `companyDivision` VARCHAR(10) NULL DEFAULT NULL  AFTER `displayOrder` , 
ADD COLUMN `commodity` VARCHAR(5) NULL DEFAULT NULL AFTER `companyDivision` , 
ADD COLUMN `packingMode` VARCHAR(15) NULL DEFAULT NULL  AFTER `commodity` , 
ADD COLUMN `originCountry` VARCHAR(45) NULL DEFAULT NULL  AFTER `packingMode` , 
ADD COLUMN `destinationCountry` VARCHAR(45) NULL DEFAULT NULL  AFTER `originCountry` ;

update refquoteservices set companyDivision='', commodity='', packingMode='', originCountry='', destinationCountry='';

ALTER TABLE `redsky`.`company`
ADD COLUMN `privateCustomerTCode` VARCHAR(8) NULL DEFAULT NULL  AFTER `singleCompanyDivisionInvoicing`;

ALTER TABLE `redsky`.`company` ADD COLUMN `minimumBillAmount` DECIMAL(19,2) AFTER `privateCustomerTCode`;

ALTER TABLE `redsky`.`todoresult` ADD COLUMN `agentName` VARCHAR(255);

Insert into refmaster
(corpid,code,description,flex1,flex2,bucket2,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('CWMS', '1', '','0','20','0', 50, 'OFFCOMMISSION','en',now(),'CWMS_SETUP',now(),'CWMS_SETUP'),
('CWMS', '2', 'Revenue','20.01','34.9','7', 50, 'OFFCOMMISSION','en',now(),'CWMS_SETUP',now(),'CWMS_SETUP'),
('CWMS', '3', 'Margin','35.0','99.9','40', 50, 'OFFCOMMISSION','en',now(),'CWMS_SETUP',now(),'CWMS_SETUP');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('CWMS','OFFCOMMISSION',50,true,now(),'CWMS_SETUP',now(),'CWMS_SETUP',1,1,'');

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('TrackingStatus','bookingAgentContact' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','bookingAgentEmail' ,'Update','amishra',now(),'amishra',now(),false,'','','');

ALTER TABLE `redsky`.`miscellaneous` ADD COLUMN 
`totalDiscountPercentage` DECIMAL(7,2) NULL DEFAULT NULL  AFTER `discount` ;

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `hsrgSoInsertRelovision` DATETIME AFTER `quoteAcceptReason`;

ALTER TABLE `redsky`.`company` ADD COLUMN `UGRNNetting` bit(1) DEFAULT  b'0' AFTER `minimumBillAmount`;

update networkdatafields set type='CMM/DMM'  where modelname='Billing' and fieldName='billToAuthority';

ALTER TABLE `redsky`.`systemdefault` MODIFY COLUMN `commissionable` VARCHAR(110) ;

//As per sunil sir
select parameter,code,flex4,corpid from refmaster  where code ='sea' and corpid='TSFT' and
parameter='MODE';

update refmaster set flex4='VEHICLE' where code ='sea' and corpid='TSFT' and
parameter='MODE';
//