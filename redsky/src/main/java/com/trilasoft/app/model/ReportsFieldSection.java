package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="reportsfieldsection")
public class ReportsFieldSection extends BaseObject {
	private Long id;
	private String corpId;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String fieldvalue;
	private String fieldname;
	private Long reportId;
	private String type;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id",id)
				.append("corpId",corpId)
				.append("updatedBy",updatedBy)
				.append("createdBy",createdBy)
				.append("createdOn",createdOn)
				.append("updatedOn",updatedOn)
				.append("fieldvalue",fieldvalue)
				.append("fieldname",fieldname)
				.append("reportId",reportId)
				.append("type",type)
				.toString();
	}

	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ReportsFieldSection))
			return false;
		ReportsFieldSection castOther = (ReportsFieldSection) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(fieldvalue, castOther.fieldvalue)
				.append(fieldname, castOther.fieldname)
				.append(reportId, castOther.reportId)
				.append(type, castOther.type)
				.isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(updatedBy).append(createdBy).append(createdOn).append(updatedOn).append(fieldvalue).append(fieldname)
				.append(reportId).append(type)
				.toHashCode();
	}
	
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getFieldvalue() {
		return fieldvalue;
	}
	public void setFieldvalue(String fieldvalue) {
		this.fieldvalue = fieldvalue;
	}
	@Column
	public String getFieldname() {
		return fieldname;
	}
	public void setFieldname(String fieldname) {
		this.fieldname = fieldname;
	}
	@Column
	public Long getReportId() {
		return reportId;
	}
	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}


