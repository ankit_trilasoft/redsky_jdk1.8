package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.StorageLibraryDao;
import com.trilasoft.app.model.Location;
import com.trilasoft.app.model.StorageLibrary;

public class StorageLibraryDaoHibernate extends GenericDaoHibernate<StorageLibrary,Long> implements StorageLibraryDao{

	public StorageLibraryDaoHibernate() {
		super(StorageLibrary.class);
	}


	public List storageLibrarySearch(String storageId, String storageType,String storageMode) {
		if(storageId==null){storageId="";} 
		if(storageType==null){storageType="";} 
		if(storageMode==null){storageMode="";}
		return getHibernateTemplate().find("from StorageLibrary where storageId like '%" + storageId.replaceAll("'", "''").replaceAll(":", "''") + "%' AND storageType like '" + storageType + "%' AND storageMode like '%"+storageMode.replaceAll("'", "''").replaceAll(":", "''") +"%' ");
	}

	public List storageLibraryList(String sessionCorpID) {
		return getHibernateTemplate().find("from StorageLibrary where corpID=?",sessionCorpID);
	}

	public List findStorageLibraryList(String sessionCorpID) {
		return getHibernateTemplate().find("from StorageLibrary where corpID = '"+sessionCorpID+"' AND storageLocked is false AND (storageAssigned is false OR storageMode='3' OR storageMode='5') ");
	}

	public List storageLibraryUnitSearch(String storageId, String storageType,String storageMode) {
		return getHibernateTemplate().find("from StorageLibrary where storageId like '%" + storageId.replaceAll("'", "''").replaceAll(":", "''") + "%' AND storageType like '" + storageType + "%' AND storageMode like '%"+storageMode.replaceAll("'", "''").replaceAll(":", "''") +"%' AND storageLocked is false AND (storageAssigned is false OR storageMode='3' OR storageMode='5') ");
	}
	
	public StorageLibrary getByStorageId(String storageId,String sessionCorpID){
		List list = getHibernateTemplate().find("from StorageLibrary where storageId = '" +storageId+"' and corpId ='"+sessionCorpID+"' ");
		if(list!=null && !list.isEmpty()){
			return (StorageLibrary)list.get(0);
		}else{
			return null;
		}
	}

	public List findAllocateStorage(String sessionCorpID) {
		return getHibernateTemplate().find("from StorageLibrary where corpID='"+sessionCorpID+"' AND storageLocked is false");
	}


	public List findStoLocRearrangeList(String sessionCorpID) {
		getHibernateTemplate().setMaxResults(500);
		return getHibernateTemplate().find("from Storage where corpID='"+sessionCorpID+"' and storageId is not null and storageId!='' and locationId is not null and locationId!='' and releaseDate is null ");
	}

	public List searchStoLocRearrangeList(String sessionCorpID,	String locationId, String ticket) {
		if(locationId==null){locationId="";} 
		if(ticket==null){ticket="";} 
		return getHibernateTemplate().find("from Storage where locationId like '%" + locationId.replaceAll("'", "''").replaceAll(":", "''") + "%' AND storageId like '%" + ticket.replaceAll("'", "''").replaceAll(":", "''") + "%' and storageId is not null and storageId!='' and locationId is not null and locationId!='' and releaseDate is null ");
	}


}
