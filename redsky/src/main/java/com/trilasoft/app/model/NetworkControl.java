package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="networkcontrol")

public class NetworkControl extends BaseObject{
	
	private Long id;
	private String sourceCorpId;
	private String soNumber;
	private String sourceSoNum;
	private String sourceInfo;
	private String sourceStatus;
	private String corpId;
	private String status;
	private String action;
	private String actionStatus;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String childAgentCode;
	private String childAgentType;
	private String companyDiv;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("sourceCorpId", sourceCorpId)
				.append("soNumber", soNumber)
				.append("sourceSoNum", sourceSoNum)
				.append("sourceInfo", sourceInfo)
				.append("sourceStatus", sourceStatus).append("corpId", corpId)
				.append("status", status).append("action", action)
				.append("actionStatus", actionStatus)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("childAgentCode", childAgentCode)
				.append("childAgentType", childAgentType)
				.append("companyDiv", companyDiv).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof NetworkControl))
			return false;
		NetworkControl castOther = (NetworkControl) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(sourceCorpId, castOther.sourceCorpId)
				.append(soNumber, castOther.soNumber)
				.append(sourceSoNum, castOther.sourceSoNum)
				.append(sourceInfo, castOther.sourceInfo)
				.append(sourceStatus, castOther.sourceStatus)
				.append(corpId, castOther.corpId)
				.append(status, castOther.status)
				.append(action, castOther.action)
				.append(actionStatus, castOther.actionStatus)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(childAgentCode, castOther.childAgentCode)
				.append(childAgentType, castOther.childAgentType)
				.append(companyDiv, castOther.companyDiv).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(sourceCorpId)
				.append(soNumber).append(sourceSoNum).append(sourceInfo)
				.append(sourceStatus).append(corpId).append(status)
				.append(action).append(actionStatus).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(childAgentCode).append(childAgentType)
				.append(companyDiv).toHashCode();
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getSourceCorpId() {
		return sourceCorpId;
	}
	public void setSourceCorpId(String sourceCorpId) {
		this.sourceCorpId = sourceCorpId;
	}
	@Column
	public String getSoNumber() {
		return soNumber;
	}
	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}
	@Column
	public String getSourceSoNum() {
		return sourceSoNum;
	}
	public void setSourceSoNum(String sourceSoNum) {
		this.sourceSoNum = sourceSoNum;
	}
	@Column
	public String getSourceInfo() {
		return sourceInfo;
	}
	public void setSourceInfo(String sourceInfo) {
		this.sourceInfo = sourceInfo;
	}
	@Column
	public String getSourceStatus() {
		return sourceStatus;
	}
	public void setSourceStatus(String sourceStatus) {
		this.sourceStatus = sourceStatus;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	@Column
	public String getActionStatus() {
		return actionStatus;
	}
	public void setActionStatus(String actionStatus) {
		this.actionStatus = actionStatus;
	}
	@Column
	public String getChildAgentCode() {
		return childAgentCode;
	}

	public void setChildAgentCode(String childAgentCode) {
		this.childAgentCode = childAgentCode;
	}
	@Column
	public String getChildAgentType() {
		return childAgentType;
	}
	public void setChildAgentType(String childAgentType) {
		this.childAgentType = childAgentType;
	}
	@Column
	public String getCompanyDiv() {
		return companyDiv;
	}
	public void setCompanyDiv(String companyDiv) {
		this.companyDiv = companyDiv;
	}
}
