delimiter $$

DROP trigger IF EXISTS `redsky`.`Trigger_after_update_BookStorage` $$

CREATE  TRIGGER redsky.Trigger_after_update_BookStorage
after update ON redsky.bookstorage
FOR EACH ROW
BEGIN

	DECLARE l_volUnit varchar(3);
	select volUnit into l_volUnit from storage where idNum=NEW.idNum;
call storageBookAndRelease(NEW.locationid,NEW.what,null,NEW.updatedOn,NEW.updatedBy,0,NEW.corpid,l_volUnit,NEW.storageid,NEW.oldStorage,NEW.idNum);
end;
$$


delimiter;