package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DataMaintainenceDao;
import com.trilasoft.app.model.DataMaintainence;

public class DataMaintainenceDaoHibernate extends GenericDaoHibernate<DataMaintainence, Long> implements DataMaintainenceDao {

	public DataMaintainenceDaoHibernate() {
		super(DataMaintainence.class);		
	}
	public List executeQuery(String query ){
		List<String> list =new ArrayList<String>();
		try{
		list= this.getSession().createSQLQuery(query).list();
		
		}catch (Exception e) {
		   e.printStackTrace();
		}
		
		return list;
         	
		}
	public String bulkUpdate(String query){
		int result;
		try{
			result=this.getSession().createSQLQuery(query).executeUpdate();
			}catch(Exception e){
			return e.toString();
			}
		return "success"; 
	}
}
