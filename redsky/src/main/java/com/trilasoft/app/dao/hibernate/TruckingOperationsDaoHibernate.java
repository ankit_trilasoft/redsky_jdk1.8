package com.trilasoft.app.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.TruckingOperationsDao;
import com.trilasoft.app.model.TruckingOperations;

public class TruckingOperationsDaoHibernate extends GenericDaoHibernate<TruckingOperations, Long> implements TruckingOperationsDao {

	public TruckingOperationsDaoHibernate() {
		super(TruckingOperations.class);
	}

	public List workTicketTruck(Long ticket,String corpID){
		return getHibernateTemplate().find("from TruckingOperations where ticket='"+ticket+"' and corpID='"+corpID+"' group by localTruckNumber");
	}
	public List findWorkTicketTruck(String localTruckNumber,String corpID){
		return getHibernateTemplate().find("select description from Truck where localNumber='"+localTruckNumber.replaceAll("'", "''")+"' and corpID='"+corpID+"'");
	}
	public int updateDeleteStatus(Long id){
		return getHibernateTemplate().bulkUpdate("delete TruckingOperations where id='"+id+"'");
	}

	public void updateWorkTicketTrucks(Long ticket, int trucks,String TargetActual,boolean workticketQueue,String ResourceValue, String sessionCorpID) {
		List numList=this.getSession().createSQLQuery("select count(distinct localTruckNumber) from truckingoperations  where ticket ="+ticket+" and corpID='"+sessionCorpID+"'").list();
		int numberTrucks=Integer.parseInt(numList.get(0).toString());
		//if(numberTrucks!=0){
			getHibernateTemplate().bulkUpdate("update WorkTicket set trucks="+numberTrucks+" where ticket="+ticket+" and corpID='"+sessionCorpID+"'");
		//}
		if(workticketQueue && ResourceValue.equalsIgnoreCase("Assign")){
			if(TargetActual.equalsIgnoreCase("T")){
				getHibernateTemplate().bulkUpdate("update WorkTicket set targetActual='D' where ticket="+ticket+" and corpID='"+sessionCorpID+"'");	
			}else if(TargetActual.equalsIgnoreCase("P")){
				getHibernateTemplate().bulkUpdate("update WorkTicket set targetActual='D' where ticket="+ticket+" and corpID='"+sessionCorpID+"'");	
			}
		}
	}
	public String findSurveyTool(String corpID, String job, String companyDivision){

		try {
			return getHibernateTemplate().find(
					"select surveyTool from RefJobType where corpID='" + corpID
							+ "' and job='" + job + "' and companyDivision='"
							+ companyDivision + "'").toString();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
	    	 e.printStackTrace();
		}
		return "";
	
	}
	public String findSendMoveCloudStatusCrew(String sessionCorpID ,String fileNumber){

		String emailStatus1="";
		List l1 =  this.getSession().createSQLQuery("select detailedStatus from integrationloginfo where corpId='"+ sessionCorpID + "' and sourceOrderNum='" + fileNumber+ "' and destOrderNum<>'' ").list();
		if((l1!=null) && (!l1.isEmpty()) && (l1.get(0)!=null)){
		
			emailStatus1= l1.get(l1.size() - 1).toString();
		}
		emailStatus1 =emailStatus1.trim();
		return emailStatus1;
		
	}
}