package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.TaskCheckList;

public interface TaskCheckListManager extends GenericManager<TaskCheckList,Long> {
	public List getById();

	public TaskCheckList getTaskCheckListData(String shipnumber, Long id);
}
