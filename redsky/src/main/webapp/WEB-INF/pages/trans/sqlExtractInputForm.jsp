<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<head> 
<title><fmt:message key="sqlExtractInputForm.title" /></title>
<meta name="heading" content="<fmt:message key='sqlExtractInputForm.heading'/>" />
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
</head>
<script type="text/javascript">
</script>
<s:form id="sqlExtractInputForm" name="sqlExtractInputForm" action="extractSQLData" method="post" >
<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
<s:hidden name="sqlExtract.isdateWhere1"/>
<s:hidden name="sqlExtract.isdateWhere2"/>
<s:hidden name="sqlExtract.isdateWhere3"/>
<s:hidden name="sqlExtract.isdateWhere4"/>
<s:hidden name="sqlExtract.isdateWhere5"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="whereClause1" value="<%=request.getParameter("whereClause1") %>"/>
<c:set var="whereClause1" value="<%=request.getParameter("whereClause1") %>"/>
<s:hidden  name="whereClause2" value="<%=request.getParameter("whereClause2") %>"/>
<c:set var="whereClause2" value="<%=request.getParameter("whereClause2") %>"/>
<s:hidden  name="whereClause3" value="<%=request.getParameter("whereClause3") %>"/>
<c:set var="whereClause3" value="<%=request.getParameter("whereClause3") %>"/>
<s:hidden  name="whereClause4" value="<%=request.getParameter("whereClause4") %>"/>
<c:set var="whereClause4" value="<%=request.getParameter("whereClause4") %>"/>
<s:hidden  name="whereClause5" value="<%=request.getParameter("whereClause5") %>"/>
<c:set var="whereClause5" value="<%=request.getParameter("whereClause5") %>"/>
<s:hidden  name="groupBy" value="<%=request.getParameter("groupBy") %>"/>
<c:set var="groupBy" value="<%=request.getParameter("groupBy") %>"/>	
<s:hidden  name="orderBy" value="<%=request.getParameter("orderBy") %>"/>
<c:set var="orderBy" value="<%=request.getParameter("orderBy") %>"/>	

<div id="content" align="center" >

<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
	<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0"  style="">
		<tbody>  
		
		<tr><td width="4"> </td>
<td  class="bgblue" colspan="5">Extract Parameters</td></tr>
<tr height="5px"></tr>
		<c:if test="${not empty whereClause1}">
		<tr >
		<td></td>
		<td class="subcontenttabChild" align="right">Field</td>
		<td width="5px" class="subcontenttabChild"></td>
		<td class="subcontenttabChild">Condition</td>
		<td width="5px" class="subcontenttabChild"></td>
		<td class="subcontenttabChild">Value</td>
		</c:if>
		</tr>
		<tr height="5px"></tr>
			
			<tr>
			<td></td>
				<c:if test="${not empty whereClause1}">
				<td align="right" style="width:75px;" class="listwhitetext"><s:label name="sqlExtract.displayLabel1"></s:label></td>
				<td>:</td>
				<td width="" align="left"><s:select cssClass="list-menu" name="defaultCondition1"  value="%{sqlExtract.defaultCondition1}" list="%{conditionList}" headerKey="" headerValue="" cssStyle="width:80px"/></td>
				<c:if test="${sqlExtract.isdateWhere1==false}">
				<td></td>
				<td width="" align="left"><s:textfield name="whereClause1Value" required="true" cssClass="input-text" size="30"/></td>
				</c:if>
				<c:if test="${sqlExtract.isdateWhere1==true}">
						<c:if test="${not empty whereClause1Value}">
							<s:text id="whereClause1Value" name="${FormDateValue}"> <s:param name="value" value="whereClause1Value" /></s:text>
							<td width="500px"><s:textfield cssClass="input-text" id="whereClause1Value" name="whereClause1Value" value="%{whereClause1Value}" size="10" maxlength="11" readonly="true" /><img id="whereClause1Value_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
				<c:if test="${empty whereClause1Value}" >
						<td></td>
						<td width="500px" ><s:textfield cssClass="input-text" id="whereClause1Value" name="whereClause1Value" required="true" size="10" maxlength="11" readonly="true" /><img id="whereClause1Value_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>	
				</c:if>
				</c:if>
			</tr>
		<tr height="5px"></tr>
			<tr>
			<td></td>
				<c:if test="${not empty whereClause2}">
				<td style="width:75px;" align="right" class="listwhitetext" ><s:label name="sqlExtract.displayLabel2"></s:label></td>
				<td>:</td>
				<td width="" align="left"><s:select cssClass="list-menu" name="defaultCondition2" value="%{sqlExtract.defaultCondition2}"  list="%{conditionList}" headerKey="" headerValue="" cssStyle="width:80px"/></td>
				<c:if test="${sqlExtract.isdateWhere2==false}">
				<td></td>
				<td width="" align="left"><s:textfield name="whereClause2Value" required="true" cssClass="input-text" size="30"/></td>
				</c:if>
				<c:if test="${sqlExtract.isdateWhere2==true}">
						<c:if test="${not empty whereClause2Value}">
						<s:text id="whereClause2Value" name="${FormDateValue}"> <s:param name="value" value="whereClause2Value" /></s:text>
					<td width="500px"><s:textfield cssClass="input-text" id="whereClause2Value" name="whereClause2Value" value="%{whereClause2Value}" size="10" maxlength="11" readonly="true" /><img id="whereClause2Value_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty whereClause2Value}">
				<td></td>
				<td width="500px"><s:textfield cssClass="input-text" id="whereClause2Value" name="whereClause2Value"  size="10" maxlength="11" readonly="true" /><img id="whereClause2Value_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				</c:if>
				</c:if>
			</tr>
			<tr height="5px"></tr>
			<tr>
			<td></td>
				<c:if test="${not empty whereClause3}">
					<td style="width:75px;" align="right" class="listwhitetext"><s:label name="sqlExtract.displayLabel3"></s:label></td>
					<td>:</td>
					<td width="" align="left"><s:select cssClass="list-menu" name="defaultCondition3"  value="%{sqlExtract.defaultCondition3}"  list="%{conditionList}" headerKey="" headerValue="" cssStyle="width:80px"/></td>
					<c:if test="${sqlExtract.isdateWhere3==false}">
					<td></td>
					<td width="" align="left"><s:textfield name="whereClause3Value" required="true" cssClass="input-text" size="30"/></td>
					</c:if><c:if test="${sqlExtract.isdateWhere3==true}">
						<c:if test="${not empty whereClause3Value}">
						<s:text id="whereClause3Value" name="${FormDateValue}"> <s:param name="value" value="whereClause3Value" /></s:text>
					<td width="500px"><s:textfield cssClass="input-text" id="whereClause3Value" name="whereClause3Value" value="%{whereClause3Value}" size="10" maxlength="11" readonly="true" /><img id="whereClause3Value_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty whereClause3Value}">
				<td></td>
				<td width="500px"><s:textfield cssClass="input-text" id="whereClause3Value" name="whereClause3Value"  size="10" maxlength="11" readonly="true" /><img id="whereClause3Value_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				</c:if>
				</c:if>
			</tr>
			<tr height="5px"></tr>
			<tr>
			<td></td>
				<c:if test="${not empty whereClause4}">
					<td style="width:75px;" align="right" class="listwhitetext"><s:label name="sqlExtract.displayLabel4"></s:label></td>
					<td>:</td>
					<td width="" align="left"><s:select cssClass="list-menu" name="defaultCondition4" value="%{sqlExtract.defaultCondition4}"  list="%{conditionList}" headerKey="" headerValue="" cssStyle="width:80px"/></td>
					<c:if test="${sqlExtract.isdateWhere4==false}">
					<td></td>
					<td width="" align="left"><s:textfield name="whereClause4Value" required="true" cssClass="input-text" size="30"/></td>
					</c:if>
					<c:if test="${sqlExtract.isdateWhere4==true}">
						<c:if test="${not empty whereClause4Value}">
						<s:text id="whereClause4Value" name="${FormDateValue}"> <s:param name="value" value="whereClause4Value" /></s:text>
					<td width="500px"><s:textfield cssClass="input-text" id="whereClause4Value" name="whereClause4Value" value="%{whereClause4}" size="10" maxlength="11" readonly="true" /><img id="whereClause4Value_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty whereClause4Value}">
				<td></td>
				<td width="500px"><s:textfield cssClass="input-text" id="whereClause4Value" name="whereClause4Value" size="10" maxlength="11" readonly="true" /><img id="whereClause4Value_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				</c:if>
				</c:if>
			</tr>
			<tr height="5px"></tr>
			<tr>
			<td></td>
				<c:if test="${not empty whereClause5}">
					<td style="width:75px;" align="right" class="listwhitetext"><s:label name="sqlExtract.displayLabel5"></s:label></td>
					<td>:</td>
					<td width="" align="left"><s:select cssClass="list-menu" name="defaultCondition5" value="%{sqlExtract.defaultCondition5}"  list="%{conditionList}" headerKey="" headerValue="" cssStyle="width:80px"/></td>
					<c:if test="${sqlExtract.isdateWhere5==false}">
					<td></td>
					<td width="" align="left"><s:textfield name="whereClause5Value" required="true" cssClass="input-text" size="30"/></td>
					</c:if>
					<c:if test="${sqlExtract.isdateWhere5==true}">
						<c:if test="${not empty whereClause5Value}">
						<s:text id="whereClause5Value" name="${FormDateValue}"> <s:param name="value" value="whereClause5" /></s:text>
					<td width="500px"><s:textfield cssClass="input-text" id="whereClause5Value" name="whereClause5Value" value="%{whereClause5Value}" size="10" maxlength="11" readonly="true" /><img id="whereClause5Value_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty whereClause5Value}">
				<td></td>
				<td width="500px"><s:textfield cssClass="input-text" id="whereClause5Value" name="whereClause5Value"  size="10" maxlength="11" readonly="true" /><img id="whereClause5Value_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				</c:if>
				</c:if>
			</tr>
			<tr height="5px"></tr>
			<tr height="5px"></tr>
				</tbody>
			</table>
			
		<table> 
		
		<tr>
		
				<td> </td>
				</tr>
				<c:if test="${not empty orderBy}">
					<tr><td  class="bgblue" colspan="5">Order By</td></tr>
					<tr><td colspan="5"></td></tr>
					<tr>
					<td class="listwhitebox" align="right" width="30px"><s:label name="sqlExtract.orderBy"></s:label></td><td width="5">:</td>
					
					<td width="" align="left"><s:select cssClass="list-menu" name="orderByValue" key="order" list="{'Ascending','Descending'}" headerKey="" headerValue="" cssStyle="width:80px"/></td>
					</tr>
				</c:if>
			
		</table>
		
		<table>
		<tr>
		<td>
		<td align="left" colspan="1">
		<!--<input type="button" Class="cssbutton1"  type="button" value="Extraxt" onclick="extractData(${id})" /></td>
		
		--><s:submit  type="button" cssClass="cssbuttonA" value="Extract"/></td>
		
		<td align="left"  width="20px"><A href="javascript: self.close ()"><input type="button" style="width:100px" Class="cssbutton1" type="button" value="Close Window"/></A></td>
		</td>
		</tr>
		
		<tr height="15px"></tr>	
		
			</table>
			
	
			 </div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	    <div id="mydiv" style="position:absolute"></div>
</s:form>
<c:if test="${hitFlag == 1}" >
<c:redirect url="/sqlExtractInputForm.html?decorator=popup&popup=true&id=${id}&whereClause1=${whereClause1}&whereClause2=${whereClause2}&whereClause3=${whereClause3}&whereClause4=${whereClause4}&whereClause5=${whereClause5}&orderBy=${orderBy}&groupBy=${groupBy}"/>
</c:if>
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>