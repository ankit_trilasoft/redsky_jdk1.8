<%@ include file="/common/taglibs.jsp"%> 


<head> 

<title><fmt:message key="portalList.title"/></title> 

<meta name="heading" content="<fmt:message key='portalList.heading'/>"/> 

<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
			for(i=0;i<=1;i++)
			{
					document.forms['searchForm'].elements[i].value = "";
			}
}
</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:0px;
!margin-bottom:2px;
margin-top:-17px;
padding:2px 0px;
text-align:right;
width:100%;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>
</head> 

<s:form id="searchForm" action="searchQuotation" method="post" validate="true">   
<div id="Layer1" style="width: 100%">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" width="90%" >
<thead>
<tr>
<th><fmt:message key="portal.referenceNumber"/></th>
<th><fmt:message key="portal.quoteStatus"/></th>
<th>&nbsp;</th>

</tr></thead> 
<tbody>
  <tr>
   <td>
       <s:textfield name="partnerQuote.quote" required="true" cssClass="text medium"/>
   </td>
   <td>
       <s:textfield name="partnerQuote.quoteStatus" required="true" cssClass="text medium"/>
   </td>
   <td>
       <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" method="search" key="button.search"/>  
       <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
   </td>
   </tr>   
  </tbody>
 </table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<c:out value="${searchresults}" escapeXml="false" /> 

<c:out value="${buttons}" escapeXml="false" /> 
<s:set name="partnerQuotess" value="partnerQuotess" scope="partnerQuotess"/>

<div id="newmnav" style="float:left;margin-bottom:1px;margin-top:-10px;">
		<ul>
		  <li><a><span>Requests</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
<display:table name="partnerQuotess" class="table" requestURI="" id="portalList"  defaultsort="1" export="true" pagesize="10"> 

<display:column property="quote" sortable="true" href="portalReference.html" paramId="id" paramProperty="id" titleKey="portal.shipNumber"/> 
<display:column property="quoteStatus" sortable="true" titleKey="portal.quoteStatus"/> 
<display:column property="corpID" sortable="true" titleKey="portal.Requestor"/> 
<display:column property="quoteType" sortable="true" titleKey="portal.services"/> 
<display:column property="originCity" sortable="true" titleKey="portal.origincity"/> 
<display:column property="destinationCity" sortable="true" titleKey="portal.DestinationCity"/> 

<display:setProperty name="paging.banner.item_name" value="partnerQuote"/> 
<display:setProperty name="paging.banner.items_name" value="Record"/> 
<display:setProperty name="export.excel.filename" value="Requests.xls"/>
<display:setProperty name="export.csv.filename" value="Requests.csv"/>
</display:table> 


 </div>
<c:out value="${buttons}" escapeXml="false" /> 

</s:form>
<script type="text/javascript"> 

highlightTableRows("portalList"); 

</script> 
