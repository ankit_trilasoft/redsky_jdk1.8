<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ include file="/common/taglibs.jsp"%>
<configByCorp:timeZone/>
<configByCorp:AutoSavePrompt/>
<s:hidden name="userPortalCheckType"   value="${userPortalCheckType}"/>
<c:set var="userPortalCheckType"  value="${userPortalCheckType}"/>
<s:hidden name="soTab"   value="${soTab}"/>
<c:set var="soTab"  value="${soTab}"/>
<s:hidden name="soSortOrder"   value="${soSortOrder}"/>
<c:set var="soSortOrder"  value="${soSortOrder}"/>
<s:hidden name="csoSortOrder"   value="${csoSortOrder}"/>
<c:set var="csoSortOrder"  value="${csoSortOrder}"/>

<s:hidden name="ticketSortOrder"   value="${ticketSortOrder}"/>
<c:set var="ticketSortOrder"  value="${ticketSortOrder}"/>

<s:hidden name="claimSortOrder"   value="${claimSortOrder}"/>
<c:set var="claimSortOrder"  value="${claimSortOrder}"/>

<s:hidden name="orderForJob"   value="${orderForJob}"/>
<c:set var="orderForJob"  value="${orderForJob}"/>

<s:hidden name="orderForCso"   value="${orderForCso}"/>
<c:set var="orderForCso"  value="${orderForCso}"/>

<s:hidden name="orderForTicket"   value="${orderForTicket}"/>
<c:set var="orderForTicket"  value="${orderForTicket}"/>

<s:hidden name="orderForClaim"   value="${orderForClaim}"/>
<c:set var="orderForClaim"  value="${orderForClaim}"/>

<s:hidden name="defaultCss"   value="${defaultCss}"/>
<c:set var="defaultCss"  value="${defaultCss}"/>

<s:hidden name="soLastName"   value="${soLastName}"/>
<c:set var="soLastName"  value="${soLastName}" scope="request"/>

<s:hidden name="globalSessionCorp"   value="${globalSessionCorp}"/>
<c:set var="globalSessionCorp"  value="${globalSessionCorp}" scope="session"/>

<fmt:setTimeZone value="${sessionTimeZone}" scope="session"/>
<c:set var="displayDateFormat" value="dd-MMM-yy" scope="session"/>
<c:set var="displayDateTimeFormat" value="dd-MMM-yyyy HH:mm" scope="application"/>
<c:set var="displayDateTimeEditFormat" value="dd-MMM-yyyy HH:mm" scope="application"/>
<c:set var="corpTimeZone" value="${sessionTimeZone}" scope="session"/>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <%@ include file="/common/meta.jsp" %>
        <title><decorator:title/> | <fmt:message key="webapp.name"/></title>

        <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/theme.css'/>" />
        <link rel="stylesheet" type="text/css" media="print" href="<c:url value='/styles/${appConfig["csstheme"]}/print.css'/>" />
		
		<script language="javascript" type="text/javascript">
		
  //function loadjscssfile(filename, filetype){
  //if (filetype=="css"){ //if filename is an external CSS file
  //var fileref=document.createElement("link")
  //fileref.setAttribute("rel", "stylesheet")
  //fileref.setAttribute("type", "text/css")
  //var defaultCss = '${defaultCss}';
  //fileref.setAttribute("href", 'styles/'+defaultCss+'/'+defaultCss+'.css')
 //}
 //if (typeof fileref!="undefined")
  //document.getElementsByTagName("head")[0].appendChild(fileref)
  //document.write(getCalendarStyles());
//}
</script>
  <script type="text/javascript" src="scripts/ajax.js"></script>
<%--    <script type="text/javascript" src="<c:url value='/scripts/prototype.js'/>"></script>
 --%>     <script type="text/javascript" src="<c:url value='/scripts/scriptaculous.js'/>"></script>
     <script type="text/javascript" src="<c:url value='/scripts/global.js'/>"></script>
      <script type="text/javascript" src="<c:url value='/scripts/listDetail.js'/>"></script>        
	<script type="text/javascript" src="<c:url value='/scripts/util.js'/>"></script> 
	             
        <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/theme.css'/>" />
        <link rel="stylesheet" type="text/css" media="print" href="<c:url value='/styles/${appConfig["csstheme"]}/print.css'/>" />
        <script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/ajaxtabs.js"></script>	
		<script type="text/javascript" src="scripts/ajax.js"></script>
		<script type="text/javascript" src="<c:url value='/scripts/animatedcollapse.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/scripts/builder.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/scripts/global.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/scripts/listDetail.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/scripts/masks.js'/>"></script>
<%--         <script type="text/javascript" src="<c:url value='/scripts/prototype.js'/>"></script> 
 --%>	    <script type="text/javascript" src="<c:url value='/scripts/scriptaculous.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/scripts/effects.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/scripts/controls.js'/>"></script>
		
		<script type="text/javascript" src="<c:url value='/scripts/switchcontent.js'/>"></script>		
		<script type="text/javascript" src="<c:url value='/scripts/util.js'/>"></script>   


       
	       
        <decorator:head/>
        
    </head>
<body<decorator:getProperty property="body.id" writeEntireProperty="true"/><decorator:getProperty property="body.class" writeEntireProperty="true"/>>

    <div id="page">
        <div id="content" class="clearfix">
        	<p></p>
        	<div id="mainPopup" class="clearfix">
			<authz:authorize ifNotGranted="ROLE_ANONYMOUS">
	                <decorator:body/>
			</authz:authorize>
			<authz:authorize ifAnyGranted="ROLE_ANONYMOUS">
	                <%@ include file="/common/messages.jsp" %>
	                <decorator:body/>
			</authz:authorize>
			</div>
    </div>
</body>
</html>

