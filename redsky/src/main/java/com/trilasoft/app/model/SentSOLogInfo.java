package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;


@Entity
@Table(name = "sentsologinfo")
public class SentSOLogInfo extends BaseObject{

	private Long id;
	private String soNumber;
	private Long soId;
	private String corpId;
	private Date sentDate;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("soNumber", soNumber).append("soId", soId)
				.append("corpId", corpId).append("sentDate", sentDate)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof SentSOLogInfo))
			return false;
		SentSOLogInfo castOther = (SentSOLogInfo) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(soNumber, castOther.soNumber)
				.append(soId, castOther.soId).append(corpId, castOther.corpId)
				.append(sentDate, castOther.sentDate).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(soNumber).append(soId)
				.append(corpId).append(sentDate).toHashCode();
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getSoNumber() {
		return soNumber;
	}
	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}
	@Column
	public Long getSoId() {
		return soId;
	}
	public void setSoId(Long soId) {
		this.soId = soId;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public Date getSentDate() {
		return sentDate;
	}
	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
}
