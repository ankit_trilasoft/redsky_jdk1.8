package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Port;

public interface PortDao extends GenericDao<Port,Long>
{
public List findByPort(String pName);
public List searchByPort(String portCode,String portName,String country,String modeType, String corpId,String active);
public List findMaximumId();
public List portExisted(String portCode, String corpId);
public List portListByCorpId(String corpId);
public List searchByPortCountry(String portCode, String portName, String country, String modeType, String sessionCorpID);
public int updatePortActiveStatus(String Status, Long id, String updatedBy);
public List getPortName(String country, String sessionCorpID, String portCode);
public List getPOLAutoComplete(String sessionCorpID,String polCode,String mode);
public List getPOEAutoComplete(String sessionCorpID,String poeCode,String mode);
}
