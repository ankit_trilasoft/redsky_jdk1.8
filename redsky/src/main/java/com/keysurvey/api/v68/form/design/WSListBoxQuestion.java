/**
 * WSListBoxQuestion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class WSListBoxQuestion  extends com.keysurvey.api.v68.form.design.WSBaseQuestion  implements java.io.Serializable {
    private com.keysurvey.api.v68.form.design.WSAnswerRestriction restrict;

    public WSListBoxQuestion() {
    }

    public WSListBoxQuestion(
           long versionId,
           com.keysurvey.api.v68.form.design.WSAnswerSortType answerSortType,
           long formId,
           long questionId,
           java.lang.String text,
           com.keysurvey.api.v68.form.design.WSAnswer[] answers,
           com.keysurvey.api.v68.form.design.WSAnswerRestriction restrict) {
        super(
            versionId,
            answerSortType,
            formId,
            questionId,
            text,
            answers);
        this.restrict = restrict;
    }


    /**
     * Gets the restrict value for this WSListBoxQuestion.
     * 
     * @return restrict
     */
    public com.keysurvey.api.v68.form.design.WSAnswerRestriction getRestrict() {
        return restrict;
    }


    /**
     * Sets the restrict value for this WSListBoxQuestion.
     * 
     * @param restrict
     */
    public void setRestrict(com.keysurvey.api.v68.form.design.WSAnswerRestriction restrict) {
        this.restrict = restrict;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSListBoxQuestion)) return false;
        WSListBoxQuestion other = (WSListBoxQuestion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.restrict==null && other.getRestrict()==null) || 
             (this.restrict!=null &&
              this.restrict.equals(other.getRestrict())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRestrict() != null) {
            _hashCode += getRestrict().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSListBoxQuestion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSListBoxQuestion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("restrict");
        elemField.setXmlName(new javax.xml.namespace.QName("", "restrict"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswerRestriction"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
