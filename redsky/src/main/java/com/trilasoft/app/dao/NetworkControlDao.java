package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.NetworkControl;

public interface NetworkControlDao extends GenericDao <NetworkControl, Long> {

}
