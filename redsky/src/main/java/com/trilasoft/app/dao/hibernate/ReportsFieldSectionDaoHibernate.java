package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ReportsFieldSectionDao;
import com.trilasoft.app.model.MaterialDto;
import com.trilasoft.app.model.ReportsFieldSection;

public class ReportsFieldSectionDaoHibernate extends GenericDaoHibernate<ReportsFieldSection, Long> implements ReportsFieldSectionDao {
	
	public ReportsFieldSectionDaoHibernate(){
		super(ReportsFieldSection.class); 
		
	}
	
	public void saveReportFieldSection(ReportsFieldSection reportsFieldSection) {
		getHibernateTemplate().saveOrUpdate(reportsFieldSection);
		
	}
	
	public  class ReportFieldSectionDTO
	 {		
		 
		 
		 private Object id;
		 private Object fieldvalue;
		 private Object fieldname;
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getFieldvalue() {
			return fieldvalue;
		}
		public void setFieldvalue(Object fieldvalue) {
			this.fieldvalue = fieldvalue;
		}
		public Object getFieldname() {
			return fieldname;
		}
		public void setFieldname(Object fieldname) {
			this.fieldname = fieldname;
		}
		 
	 }
	
	public List reportfieldSectionList(Long id ,String corpId, String type) {
		List list1 = new ArrayList();
		List listMasterList = new ArrayList();
		String fieldSection="SELECT id,fieldvalue,fieldname FROM reportsfieldsection where reportId='"+id+"' and corpId='"+corpId+"' and type='"+type+"' order by id;";
		listMasterList=this.getSession().createSQLQuery(fieldSection).list();	
		Iterator it=listMasterList.iterator();
		while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           ReportFieldSectionDTO reportfieldDto=new ReportFieldSectionDTO();
	           reportfieldDto.setId(Long.parseLong(row[0].toString()));
	           if(row[1] == null){
	        	   reportfieldDto.setFieldvalue("");
	        	   }else{
	        		   reportfieldDto.setFieldvalue(row[1].toString());
	        		   }
	           if(row[2] == null){
	        	   reportfieldDto.setFieldname("");
	        	   }else{
	        		   reportfieldDto.setFieldname(row[2].toString());
	        		   }
	           
	           list1.add(reportfieldDto);
	      }
		if(list1.isEmpty()){
			return null;
		}else{
			return list1;
		}
	}
	
	public String updateReportsFieldSection(String fieldnameUpdated,String fieldvalueUpdated,String sessionCorpID, String type){    	
    	String[] idandValue= new String[10];	
        String check="Updated";
    	
    	
    	if(fieldnameUpdated!=null && (!(fieldnameUpdated.trim().equals("")))){
			  String[] arrayStr= new String[50];
			  arrayStr =fieldnameUpdated.split("~");
			   for (String string : arrayStr) {
				   idandValue= string.split(":");
				   String query="";
				   if(idandValue.length>1){
					   query="update reportsfieldsection set fieldname='"+idandValue[1].trim()+"',updatedOn=now(),updatedBy='"+ServletActionContext.getRequest().getRemoteUser()+"' where id='"+idandValue[0]+"' and type='"+type+"' and corpId='"+sessionCorpID+"'";
				   }else{
					   query="delete from reportsfieldsection where id='"+idandValue[0]+"' and corpId='"+sessionCorpID+"'";
				   }
				   this.getSession().createSQLQuery(query).executeUpdate();
				}
		}
    	if(fieldvalueUpdated!=null && (!(fieldvalueUpdated.trim().equals("")))){
			  String[] arrayStr= new String[50];
			  arrayStr =fieldvalueUpdated.split("~");
			   for (String string : arrayStr) {
				   idandValue= string.split(":");
				   String query="";
				   if(idandValue.length>1){
					   query="update reportsfieldsection set fieldvalue='"+idandValue[1].trim()+"',updatedOn=now(),updatedBy='"+ServletActionContext.getRequest().getRemoteUser()+"' where id='"+idandValue[0]+"' and type='"+type+"' and corpId='"+sessionCorpID+"'";
				   }else{
					   query="delete from reportsfieldsection where id='"+idandValue[0]+"' and corpId='"+sessionCorpID+"'";
				   }
				   this.getSession().createSQLQuery(query).executeUpdate();
				}
		}
    	
    	return check;
    }

}
