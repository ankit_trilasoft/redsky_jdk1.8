package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;  
import com.trilasoft.app.dao.UserlogfileDao;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DtoTotalInvoice;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;

import com.trilasoft.app.model.DtoAccountInvoicePost;
import com.trilasoft.app.model.Userlogfile;

public class UserlogfileDaoHibernate extends GenericDaoHibernate<Userlogfile, Long> implements UserlogfileDao  {
	private HibernateUtil hibernateUtil;
	   
	public UserlogfileDaoHibernate() {   
        super(Userlogfile.class);   
    }

	public int updateUserLogOut(String sessionId) {
		
		return getHibernateTemplate().bulkUpdate("update Userlogfile set logout=now() where sessionId='"+sessionId+"'"); 
	}

	public List search(String userName, String userType, Date login) {
		String logindate="";
		if (!(login == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(login));
			logindate = nowYYYYMMDD1.toString();
		} 
		if(logindate.trim().equals("")) 
		return getHibernateTemplate().find("from Userlogfile where userName like '"+userName.replaceAll("'", "''").replaceAll(":", "''") +"%' and userType like '"+ userType.replaceAll("'", "''").replaceAll(":", "''") +"%' and login like '%"+logindate+"%'");
		else  if(!(logindate.trim().equals("")))
		return getHibernateTemplate().find("from Userlogfile where userName like '"+userName.replaceAll("'", "''").replaceAll(":", "''") +"%' and userType like '"+userType.replaceAll("'", "''").replaceAll(":", "''") +"%' and (DATE_FORMAT(login,'%Y-%m-%d')) = '"+logindate+"'");
		else 
		return getHibernateTemplate().find("from Userlogfile where userName like '"+userName.replaceAll("'", "''").replaceAll(":", "''") +"%' and userType like '"+userType.replaceAll("'", "''").replaceAll(":", "''") +"%' and login like '%"+logindate+"%'");
			
	}

	public List getexcludeIPsList(String CorpID) {
		return getHibernateTemplate().find("select excludeIPs from SystemDefault where excludeIPs is not null and corpID=?", CorpID);
		
	}

	public  class DtoPermission{
		 private Object permission;
		 private Object setName;
		 private Object setDescription;
		 private Object filterName;
		 private Object tableName;
		 private Object fieldName;
		 private Object filterValues;
		 
		public Object getPermission() {
			return permission;
		}

		public void setPermission(Object permission) {
			this.permission = permission;
		}

		public Object getFieldName() {
			return fieldName;
		}

		public void setFieldName(Object fieldName) {
			this.fieldName = fieldName;
		}

		public Object getFilterName() {
			return filterName;
		}

		public void setFilterName(Object filterName) {
			this.filterName = filterName;
		}

		public Object getFilterValues() {
			return filterValues;
		}

		public void setFilterValues(Object filterValues) {
			this.filterValues = filterValues;
		}

		public Object getSetDescription() {
			return setDescription;
		}

		public void setSetDescription(Object setDescription) {
			this.setDescription = setDescription;
		}

		public Object getSetName() {
			return setName;
		}

		public void setSetName(Object setName) {
			this.setName = setName;
		}

		public Object getTableName() {
			return tableName;
		}

		public void setTableName(Object tableName) {
			this.tableName = tableName;
		}
	}
	
	public List getPermission(String CorpID, String userName) 
	{
    List list1=new ArrayList();
	String query="select dss.name from datasecurityset dss, userdatasecurity uds , app_user ap where ap.id=uds.user_id and dss.id=uds.userdatasecurity_id and ap.username='"+userName+"' and ap.corpID='"+CorpID+"';";
	List lista=getSession().createSQLQuery(query).list(); 
	Iterator it=lista.iterator();
	while(it.hasNext())
	{
		Object row= (Object)it.next();
		DtoPermission permissionDTO = new DtoPermission();
		permissionDTO.setPermission(row.toString());
		list1.add(permissionDTO);
	} 
	return list1;
}

	public List getAssignedSecurity(String sessionCorpID, Long partnerId) {
		List list1=new ArrayList();
		List partnerCodeList=this.getSession().createSQLQuery("select partnerCode from partnerpublic where id='"+partnerId+"' ").list();
		String query="select dset.name 'Set Name',dset.description 'Set Desc', " +
		"fil.name 'Filter', fil.tableName 'Table', fil.fieldName 'Field', " +
		"fil.filterValues 'Value' " +
		"from datasecurityset dset, datasecuritypermission per,datasecurityfilter fil " +
		"where dset.id = per.datasecurityset_id " +
		"and fil.id = per.datasecurityfilter_id " +
		"and filtervalues = '"+partnerCodeList.get(0).toString()+"' " +
		"and dset.corpid in ('"+sessionCorpID+"','TSFT')";
		
		List lista=getSession().createSQLQuery(query).list(); 
		Iterator it=lista.iterator();
		while(it.hasNext())
		{
			
			Object []row=(Object[]) it.next();
			DtoPermission permissionDTO = new DtoPermission();
			permissionDTO.setSetName(row[0].toString());
			permissionDTO.setSetDescription(row[1].toString());
			permissionDTO.setFilterName(row[2].toString());
			permissionDTO.setTableName(row[3].toString());
			permissionDTO.setFieldName(row[4].toString());
			permissionDTO.setFilterValues(row[5].toString());
			list1.add(permissionDTO);
		} 
		return list1;
	}
	public  class DtoUserLogFile{
		private Object parentAgent;
		private Object lastname;
		private Object corpID;
		private Object userName;
		private Object userType;
		private Object IPaddress;
		private Object DNS;
		private Object login;
		private Object logout;
		private Object terminalCountry;
		private Object lastLogin;
		public Object getLastLogin() {
			return lastLogin;
		}
		public void setLastLogin(Object lastLogin) {
			this.lastLogin = lastLogin;
		}
		public Object getCorpID() {
			return corpID;
		}
		public void setCorpID(Object corpID) {
			this.corpID = corpID;
		}
		public Object getDNS() {
			return DNS;
		}
		public void setDNS(Object dns) {
			DNS = dns;
		}
		public Object getIPaddress() {
			return IPaddress;
		}
		public void setIPaddress(Object paddress) {
			IPaddress = paddress;
		}
		public Object getLastname() {
			return lastname;
		}
		public void setLastname(Object lastname) {
			this.lastname = lastname;
		}
		public Object getLogin() {
			return login;
		}
		public void setLogin(Object login) {
			this.login = login;
		}
		public Object getLogout() {
			return logout;
		}
		public void setLogout(Object logout) {
			this.logout = logout;
		}
		public Object getParentAgent() {
			return parentAgent;
		}
		public void setParentAgent(Object parentAgent) {
			this.parentAgent = parentAgent;
		}
		public Object getUserName() {
			return userName;
		}
		public void setUserName(Object userName) {
			this.userName = userName;
		}
		public Object getUserType() {
			return userType;
		}
		public void setUserType(Object userType) {
			this.userType = userType;
		}
		public Object getTerminalCountry() {
			return terminalCountry;
		}
		public void setTerminalCountry(Object terminalCountry) {
			this.terminalCountry = terminalCountry;
		}
	}
	public List searchuUserLogFile(String sessionCorpID, String userNameSearch, String userTypeSearch, String loginDateSearch, String excludeIPsList) {
        List list1=new ArrayList();
        String query=""; 
        if(!(excludeIPsList.trim().equals(""))){
        	query="select distinct ap.parentAgent,p.lastname,u.corpID,u.userName,u.userType,u.IPaddress,u.DNS,u.login,u.logout,p.terminalCountry,max(u.login) from app_user ap left outer join partnerpublic p on ap.parentAgent=p.partnercode and p.corpid  IN ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"'),userlogfile u where u.userName like '"+userNameSearch+"%' and u.userType like '"+userTypeSearch+"%' and u.corpid ='"+sessionCorpID+"' and u.login like '"+loginDateSearch+"%'  and IPaddress not in ("+excludeIPsList+")  and ap.username=u.username and ap.userType=u.userType  group by u.userName order by u.id desc;";	
        }else{
        	query="select distinct ap.parentAgent,p.lastname,u.corpID,u.userName,u.userType,u.IPaddress,u.DNS,u.login,u.logout,p.terminalCountry,max(u.login) from app_user ap left outer join partnerpublic p on ap.parentAgent=p.partnercode and p.corpid  IN ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"'),userlogfile u where u.userName like '"+userNameSearch+"%' and u.userType like '"+userTypeSearch+"%' and u.corpid ='"+sessionCorpID+"' and u.login like '"+loginDateSearch+"%' and ap.username=u.username and ap.userType=u.userType group by u.userName order by u.id desc;";
        } 
		List lista= this.getSession().createSQLQuery(query).list(); 
		Iterator it=lista.iterator();
			while(it.hasNext())
			{
				Object []row= (Object [])it.next();
				DtoUserLogFile dtoUserLogFile = new DtoUserLogFile();
				if(row[0]!=null) dtoUserLogFile.setParentAgent(row[0].toString());else dtoUserLogFile.setParentAgent("");
				if(row[1]!=null) dtoUserLogFile.setLastname((row[1]).toString());else dtoUserLogFile.setLastname("");
				dtoUserLogFile.setCorpID(row[2].toString());
				dtoUserLogFile.setUserName((row[3]).toString());
				dtoUserLogFile.setUserType(row[4].toString());
				dtoUserLogFile.setIPaddress(row[5].toString());
				dtoUserLogFile.setDNS(row[6].toString());
				dtoUserLogFile.setLogin((Date)row[7]);
				dtoUserLogFile.setLogout((Date)row[8]);
				if(row[9]!=null) dtoUserLogFile.setTerminalCountry(row[9].toString());else dtoUserLogFile.setTerminalCountry("");
				dtoUserLogFile.setLastLogin((Date)row[10]);
				list1.add(dtoUserLogFile);
				
			}
			
			return list1; 
	}

	public List findLogTime(String sessionCorpID, String userNamePermission, String loginDate) {
		getHibernateTemplate().setMaxResults(100);
		 return getHibernateTemplate().find("from Userlogfile where userName like '"+userNamePermission.replaceAll("'", "''").replaceAll(":", "''") +"%' and login like '"+loginDate+"%' and corpID='"+sessionCorpID+"' order by login desc");
	}

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
}