<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<title>Agent Detail For SO</title>
<meta name="heading"
	content="Agent Detail For SO" />
	<script type="text/javascript">
function setOriginDetail(){
	var soSetId = document.forms['popupOAGConformation'].elements['soSetId'].value;
	var ship=document.forms['popupOAGConformation'].elements['shipNumber'].value;
	var type=document.forms['popupOAGConformation'].elements['copyType'].value;
	location.href="copyAgentDetail.html?soGetId=${serviceOrder.id}&type="+type+"&soSetId="+soSetId+"&shipNumber="+ship+"&decorator=popup&popup=true";
}
function setOriginAgDetail(soSetId,shipNumber){
	document.getElementById('okButton').disabled=false;
	document.forms['popupOAGConformation'].elements['soSetId'].value = soSetId ;
	document.forms['popupOAGConformation'].elements['shipNumber'].value = shipNumber ;
}
window.onload = function (){
<c:if test="${hitFlag == 1}">
var type=document.forms['popupOAGConformation'].elements['copyType'].value;
<c:if test="${type=='origin'}">
alert('OA details have been added to ${shipNumber}');
</c:if>
<c:if test="${type=='destination'}">
alert('DA details have been added to ${shipNumber}');
</c:if>
window.open('editTrackingStatus.html?id=${soSetId}','_blank');
self.close();
</c:if>
}
</script>
</head>
<body>
<s:form action="" name="popupOAGConformation">
<s:hidden name="soSetId" />
<s:hidden name="hitFlag" />
<s:hidden name="shipNumber" />
<c:set var="copyType" value = "<%=request.getParameter("copyType")%>" />
<s:hidden name="copyType" value = "<%=request.getParameter("copyType")%>" />
</s:form >
	     <div id="otabs" style="margin-top: 10px; margin-bottom:0px;">
		  <ul>			
	  		<c:if test="${copyType=='origin'}">
	  		<li><a class="current"><span>Origin Agent For SO</span></a></li>
	  		</c:if>
	  		<c:if test="${copyType=='destination'}">
	  		<li><a class="current"><span>Destination Agent For SO</span></a></li>
	  		</c:if>   				   
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
		<div>
		<table cellspacing="0" cellpadding="0" border="0" class="table"> 
			<thead> 	
	  	    <tr>
	  	      <th height="5px"></th>
	  	      <th><b>Mode</b></th> 			
	  		  <th><b>Service Order</b></th>	  			
	  		  <th><b>City, State, Country</b></th>
	  		</tr>
	  		</thead>
	  	    <c:forEach var="soList1" items="${soList}">	  	
	  	    <tbody>    
	  	    <tr id="solistfor${soList1.id}">
	  	    	<td>
	  	    	<c:if test="${copyType=='origin' && soList1.originAgentExSO==''}">
	  	    	<input type="radio" class="input-textUpper" name="radioOrgAgDetail" id="radioOrgAgDetail" value="${soList1.id}" onclick="setOriginAgDetail('${soList1.id}','${soList1.shipNumber}');">	       
	  			</c:if>
	  			<c:if test="${copyType=='destination' && soList1.destinationAgentExSO==''}">
	  	    	<input type="radio" class="input-textUpper" name="radioOrgAgDetail"  id="radioOrgAgDetail" value="${soList1.id}" onclick="setOriginAgDetail('${soList1.id}','${soList1.shipNumber}');">	       
	  			</c:if>
	  			<c:if test="${copyType=='destination' && soList1.destinationAgentExSO!=''}">
	  	    	<input type="radio" class="input-textUpper" name="radioOrgAgDetail" disabled="disabled" id="radioOrgAgDetail" value="${soList1.id}" onclick="setOriginAgDetail('${soList1.id}','${soList1.shipNumber}');">	       
	  			</c:if>
	  			<c:if test="${copyType=='origin' && soList1.originAgentExSO!=''}">
	  	    	<input type="radio" class="input-textUpper" name="radioOrgAgDetail" disabled="disabled" id="radioOrgAgDetail" value="${soList1.id}" onclick="setOriginAgDetail('${soList1.id}','${soList1.shipNumber}');">	       
	  			</c:if>
	  			</td>
	  			<td><input type="text" class="input-textUpper" readonly="readonly" value="${soList1.mode}"></td>
	  			<td><input type="text" class="input-text" readonly="readonly" value="${soList1.shipNumber}" ></td>
	  		    <td>
	  		    <c:if test="${copyType=='origin'}">
	  		    <input type="text" class="input-textUpper" style="width:200px;" readonly="readonly" value="${soList1.originCity}, ${soList1.originState}, ${soList1.originCountry}">
	  		    </c:if>
	  		    <c:if test="${copyType=='destination'}">
	  		    <input type="text" class="input-textUpper" style="width:200px;" readonly="readonly" value="${soList1.destinationCity}, ${soList1.destinationState}, ${soList1.destinationCountry}">
	  		    </c:if>
	  		    </td>
	  		</tr>
	  		</tbody>
	  		</c:forEach>
</table>
</div>
<input type="button"  Class="cssbutton" Style="width:60px;" disabled="true" id="okButton"  value="OK" onclick="setOriginDetail()"/>
<input type="button"  Class="cssbutton" Style="width:60px; margin-left:10px;"  value="Cancel" onclick="self.close()"/>
</body>


