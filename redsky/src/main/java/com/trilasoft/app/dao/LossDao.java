package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Loss;
import com.trilasoft.app.model.LossPicture;

import java.util.Date;
import java.util.List;

public interface LossDao extends GenericDao<Loss, Long> {
	public List<Loss> findByShipNumber(String shipNumber);

	public List<Loss> findByfindClaimLossTickets(String lossNumber);

	public List findMaximum();

	public List findMaximumId();

	public List checkById(Long id);

	public List findLossByIdNumber(int idNum);

	/*public Boolean ticketExists(Long ticket,String lossNumber);
	  public Boolean checkTicket(Long ticket,String lossNumber);
	*/
	public List<Loss> findLossList();

	public List findWH(Long claimNumber, String corpID);
	
	public List findMaximumLoss(Long claimNumber, String sessionCorpID);

	public List findMinimumLoss(Long claimNumber, String sessionCorpID);

	public List findCountLoss(Long claimNumber, String sessionCorpID);
	
	public List goNextLossChild(Long claimNumber, Long id, String sessionCorpID);

	public List goPrevLossChild(Long claimNumber,Long id, String sessionCorpID);
	
	public List goLossChild(Long claimNum, Long id, String sessionCorpID);
	public int updateLossChequeNumberCustmer(String chequeNumberCustmer,Long claimNumber, String shipNumber);
	public int updateLossChequeNumber3Party(String chequeNumber3Party,Long claimNumber, String shipNumber);
	public List lossList(Long claimId);
	public List findMaximumIdNumber1(String corpID);
	public List findMaximumIdNumber(Long id);
	public Long findRemoteLoss(String idNumber, Long id);
	public List lossForOtherCorpId(Long id,String corpId);
	public List getpertaininglossvalue(Long claimNumber, String itmClmsItem, String sessionCorpID);
	public List<LossPicture> getLossPictureList(String lossId,String claimId);
	public List getLossPictureData(Long id, String corpID);
	public List getLossDocumentImageServletAction(Long id, String sessionCorpID);
}
