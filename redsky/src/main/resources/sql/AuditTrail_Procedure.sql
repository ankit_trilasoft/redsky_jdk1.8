DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`add_tblHistory` $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_tblHistory`(
IN pId BIGINT(20),
IN pTableName VARCHAR(100),
IN pFieldNameList LONGTEXT,
IN pOldValueList LONGTEXT,
IN pNewValueList LONGTEXT,
IN pUser VARCHAR(255),
IN pCorpID VARCHAR(25),
IN pDated TIMESTAMP
)
BEGIN

DECLARE pFieldName LONGTEXT;
DECLARE remainingFields LONGTEXT;
DECLARE pOldValue LONGTEXT;
DECLARE remainingOldValues LONGTEXT;
DECLARE pNewValue LONGTEXT;
DECLARE remainingNewValues LONGTEXT;
DECLARE pUserId LONGTEXT;

DECLARE pAuditable int(2);
DECLARE pDesc VARCHAR(1500);
SET pAuditable = 0;
SET pDesc = "";

SELECT SUBSTRING_INDEX(pFieldNameList,'~',1) INTO pFieldName;
SELECT SUBSTRING_INDEX(pFieldName,'.',-1) INTO pFieldName;
SELECT MID(pFieldNameList,LOCATE('~',pFieldNameList)+1,CHAR_LENGTH(pFieldNameList)) into remainingFields;

SELECT SUBSTRING_INDEX(pOldValueList,'~',1) into pOldValue;
SELECT MID(pOldValueList,LOCATE('~',pOldValueList)+1,CHAR_LENGTH(pOldValueList)) into remainingOldValues;

SELECT SUBSTRING_INDEX(pNewValueList,'~',1) into pNewValue;
SELECT MID(pNewValueList,LOCATE('~',pNewValueList)+1,CHAR_LENGTH(pNewValueList)) into remainingNewValues;
			
			

WHILE pFieldName <> '' DO
		begin
    	DECLARE CONTINUE HANDLER FOR 1329
    	SET pAuditable = 0;
    	
		SELECT id,description into pAuditable,pDesc FROM auditSetup where tablename = pTableName 
		AND fieldname = pFieldName and auditable='true' and corpID = pCorpID  limit 0,1;
		
		end;
		
		IF (pAuditable != 0) THEN
			INSERT INTO `history` (`xtranId`, `user`, `tableName`, `fieldName`, `oldValue`, `newValue`, corpID, description,dated)
			VALUES (pId, pUser, pTableName, pFieldName, pOldValue, pNewValue, pCorpID, pDesc,now());
        End IF ;

    SELECT SUBSTRING_INDEX(remainingFields,'~',1) INTO pFieldName;
    SELECT SUBSTRING_INDEX(pFieldName,'.',-1) INTO pFieldName;
    SET pFieldNameList = remainingFields;
    SELECT MID(pFieldNameList,LOCATE('~',pFieldNameList)+1,CHAR_LENGTH(pFieldNameList)) into remainingFields;

    SELECT SUBSTRING_INDEX(remainingOldValues,'~',1) into pOldValue;
    SET pOldValueList = remainingOldValues;
    SELECT MID(pOldValueList,LOCATE('~',pOldValueList)+1,CHAR_LENGTH(pOldValueList)) into remainingOldValues;

    SELECT SUBSTRING_INDEX(remainingNewValues,'~',1) into pNewValue;
    SET pNewValueList = remainingNewValues;
    SELECT MID(pNewValueList,LOCATE('~',pNewValueList)+1,CHAR_LENGTH(pNewValueList)) into remainingNewValues;

	 SET pAuditable = 0;

END WHILE;

END $$

DELIMITER ;