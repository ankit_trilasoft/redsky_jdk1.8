<%--
/**
 * Implementation of View that contains list details.
 * This file represents the basic view on "ExchangeRate" in Redsky.
 * @File Name	exchangeRateList
 * @Author      Ravi Mishra
 * @Version     V01.0
 * @Since       1.0
 * @Date        15-Dec-2008
 * --%>
<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head> 
	<c:if test="${historyExchangeFlag == false }">  
    <title><fmt:message key="exchangeRate.title"/></title>
    <meta name="heading" content="<fmt:message key='exchangeRate.heading'/>"/>   
     </c:if>  
     <c:if test="${historyExchangeFlag == true }">  
    <title><fmt:message key="historyExchangeRate.title"/></title>
    <meta name="heading" content="<fmt:message key='historyExchangeRate.heading'/>"/>   
     </c:if>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
    <style>
<%@ include file="/common/calenderStyle.css"%>
</style>   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- java script method for clear(remove) currency drop down  -->  
<script language="javascript" type="text/javascript">

function clear_fields(){
			    document.forms['exchangeList'].elements['exchangeCurrency'].value = "";
			    document.forms['exchangeList'].elements['valueDate'].value = "";
			    
}
</script>  
<!-- java script method for remove exchange rate  -->    
<script language="javascript" type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Exchange Rate?");
	var did = targetElement;
	if (agree){
		 location.href="serchDeleteExchnageRate.html?id=${contracts.id}&chagneId="+did;		
	}else{
		return false;
	}
}
		
</script>  
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:-3px;!margin-bottom:3px;margin-top:-10px;!margin-top:-15px;padding:2px 0px;text-align:right;width:100%;!width:100%;}
form {margin-top:-40px;!margin-top:0px;}
div#main {margin:-5px 0 0;!margin:0px;}
.table th.centeralign {text-align:center;}

</style>
</head>
<!-- Add button -->  
 <c:if test="${historyExchangeFlag == false }">
<c:set var="buttons"> 	
    <input type="button" class="cssbutton" style="width:55px;" 
        onclick="location.href='<c:url value="/exchangeRateForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>
        <configByCorp:fieldVisibility componentId="component.button.exchangerate.XE.uploadButton">
        <input type="button" class="cssbutton" style="width:90px;" 
        onclick="location.href='<c:url value="/uploadXmlAction.html"/>'"  
        value="Update Rate"/> 
        </configByCorp:fieldVisibility>  
        
   </c:set>           
    </c:if>
<!-- Clear button -->   
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px;" align="top" method="searchByCurrency" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px;" onclick="clear_fields();"/>  
</c:set>   

<!-- Starting of form tag -->   
<s:form id="exchangeList" action="serchExchnageRate" method="post" >  
<div id="Layer1" style="width: 100%;">
<div id="otabs">
  <ul>
    <li><a class="current"><span>Search</span></a></li>
  </ul>
</div>
<div class="spnblk">&nbsp;</div> 
<div id="content" align="center" style="margin-bottom: 26px;">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="table" border="0" style="width:100%;">
<thead>
<tr>
<th><fmt:message key="exchangeRate.currency"/></th>
 <c:if test="${historyExchangeFlag == true }">
<th><fmt:message key="historyExchangeRate.valueDate"/>
</th>
</c:if>
<th style="border-left: hidden;"></th>
<th style="border-left: hidden;"></th>
</tr></thead>	
		<tbody>
		<tr>
			<td>
				<s:select name="exchangeCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:195px;" headerKey="" headerValue="" onchange="changeStatus();"/>
			 </td>
			 <c:if test="${historyExchangeFlag == true }">  
 
			<c:if test="${not empty valueDate}">
				<s:text id="FormattedValue" name="${FormDateValue}"><s:param name="value" value="valueDate" /></s:text>
				<td><s:textfield cssClass="input-text" id="date4" name="valueDate" value="%{FormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
				<img id="date4_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty valueDate}">
				<td><s:textfield cssClass="input-text" id="date4" name="valueDate" required="true" cssStyle="width:65px;" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
				<img id="date4_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
			</td>   			   
			</c:if>
			<td width="230px" colspan="2" style="border-left: hidden;text-align:right;">
			<c:out value="${searchbuttons}" escapeXml="false" />   
		</td>
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header" style="!margin-top:50px;"><span></span></div>
</div>
</div>
<s:hidden name="historyExchangeFlag"></s:hidden>
<s:set name="exchangeRateList" value="exchangeRateList" scope="request"/>
 <c:if test="${historyExchangeFlag == false }">
<div id="newmnav" style="!margin-bottom:-12px;margin-top: -10px;">
	   <ul><li id="newmnav1"><a href="exchangeRatelist.html" class="current"><span>Exchange Rate List</span></a></li>
	    <c:if test="${historyFx == true }">
	    <li> <a href="historyExchangeList.html"><span>History&nbsp;Exchange&nbsp;List</span></a></li>
	    </c:if>
	   </ul>
     </div>
     </c:if>
     <c:if test="${historyExchangeFlag == true }">
     <div id="newmnav" style="!margin-bottom:-12px;margin-top: -10px;">
	   <ul><li><a href="exchangeRatelist.html"><span>Exchange Rate List</span></a></li>
	    <c:if test="${historyFx == true }">
	    <li id="newmnav1"> <a href="historyExchangeList.html" class="current"><span>History&nbsp;Exchange&nbsp;List</span></a></li>
	    </c:if>
	   </ul>
     </div>
     </c:if>     
     <div class="spnblk" >&nbsp;</div>
<!-- JSTL(display:..........) tag for displaying thi list  --> 
<display:table name="exchangeRateList" class="table" requestURI="" id="exchangeRateListId" export="true" pagesize="50" defaultsort="1" style="text-align:left;width:100%;margin-top:9px;">   
<c:if test="${historyExchangeFlag == false }">
<display:column property="currency" sortable="true" style="text-align:left;width: 100px" titleKey="exchangeRate.currency" href="exchangeRateForm.html" paramId="id"  paramProperty="id" />   
</c:if>
<c:if test="${historyExchangeFlag == true }">
<display:column property="currency" sortable="true" style="text-align:left;width: 100px" titleKey="exchangeRate.currency" href="historyExchangeRateForm.html" paramId="id"  paramProperty="id" />   
</c:if>
<display:column property="baseCurrency" sortable="true" style="text-align:left;width: 100px" titleKey="exchangeRate.baseCurrency"/> 
<display:column property="valueDate" sortable="true" titleKey="exchangeRate.valueDate" style="width:100px" format="{0,date,dd-MMM-yyyy}"/> 
<display:column headerClass="containeralign" title="Official Rate" style="text-align:right;width: 90px">
<fmt:formatNumber type="number"  
            value="${exchangeRateListId.officialRate}"   minFractionDigits="4" groupingUsed="true"/>
</display:column>
<display:column headerClass="containeralign" title="Base Currency Rate" style="text-align:right;width: 90px">
<fmt:formatNumber type="number"  
            value="${exchangeRateListId.baseCurrencyRate}"   minFractionDigits="4" groupingUsed="true"/>
</display:column>
<display:column headerClass="containeralign" title="Currency Base Rate" style="text-align:right;width: 90px">
<fmt:formatNumber type="number"  
            value="${exchangeRateListId.currencyBaseRate}"   minFractionDigits="4" groupingUsed="true"/>
</display:column>

<display:column property="marginApplied" headerClass="containeralign" sortable="true" style="text-align:right;width:80px" title="Margin Applied"/> 
<c:if test="${historyExchangeFlag == false }">
<display:column  title="Manual Update" headerClass="centeralign" style="width:40px;text-align:center;">
<c:choose> 
			<c:when test="${exchangeRateListId.manualUpdate=='Y'}">
					<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
			</c:when>
			<c:when test="${exchangeRateListId.manualUpdate == 'N'}">
					<img src="${pageContext.request.contextPath}/images/cancel001.gif"  /> 
			</c:when> 							
			<c:otherwise>
			</c:otherwise>			
		</c:choose>	
		</display:column>
		
</c:if>	
<c:if test="${historyExchangeFlag == false }">
<display:column title="Remove" headerClass="centeralign" style="width: 15px; text-align: center;">
<a>
  <img align="middle" title="" onclick="confirmSubmit('${exchangeRateListId.id}');" style="margin: 0px 0px 0px 0px;" src="images/recycle.gif"/>
</a>        			
</display:column>
</c:if>
<display:setProperty name="export.excel.filename" value="Exchange Rate List.xls"/>   
<display:setProperty name="export.csv.filename" value="Exchange Rate List.csv"/>   
<display:setProperty name="export.pdf.filename" value="Exchange Rate List.pdf"/> 			
</display:table> 

</div>
<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:set var="isTrue" value="false" scope="session"/>

</s:form>
<!-- End of form tag  --> 
 <script type="text/javascript">
 setOnSelectBasedMethods([]);
setCalendarFunctionality();
if(${historyExchangeFlag}==true){
document.forms['exchangeList'].elements['valueDate'].value = "${valueDateFormatter1}";
}
</script>
