/**
 * 
 */
package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.InventoryLocationDao;
import com.trilasoft.app.model.InventoryLocation;
import com.trilasoft.app.model.MoversDTO;
import com.trilasoft.app.model.RefMasterDTO;

/**
 * @author GVerma
 *
 */
public class InventoryLocationDaoHibernate extends GenericDaoHibernate<InventoryLocation, Long>  implements InventoryLocationDao {

	public InventoryLocationDaoHibernate() {
		super(InventoryLocation.class);
		// TODO Auto-generated constructor stub
	}

	public List getAllLocation(String corpId, String shipNumber) {
		//return getHibernateTemplate().find(" from InventoryLocation where corpId='"+corpId+"' and shipNumber='"+shipNumber+"'");
		List <MoversDTO> movValueList = new ArrayList<MoversDTO>(); 
		MoversDTO moverDto = null;
		List list = getSession()
		.createSQLQuery("select distinct " +
				"l.locationType," + //0
				"l.workTicket," + //1
				"if(l.locationType='Origin',wt.address1,wt.destinationAddress1) as add1," + //2
				"if(l.locationType='Origin',wt.address2,wt.destinationAddress2) as add2," + //3
				"if(l.locationType='Origin',wt.address3,wt.destinationAddress3) as add3," + //4
				"if(l.locationType='Origin',wt.city,wt.destinationCity) as city," + //5
				"if(l.locationType='Origin',wt.state,wt.destinationState) as state," + //6
				"if(l.locationType='Origin',wt.zip,wt.destinationZip) as zip," + //7
				"if(l.locationType='Origin',wt.originCountry,wt.destinationCountry) as country," + //8
				"ip.locationId," +//9
				"r.roomDesc," +//10
				"r.floor ," + //12
				"if(r.hasDamage,'Y','N') as damage ," +//13
				"r.comment ," +//14
				"ip1.roomId  "  + //15
				"from inventorylocation l " +
				"left outer join inventorypath ip on ip.locationId=l.id and ip.corpid='"+corpId+"' " +
				"left outer join room r on  l.id=r.locationId and r.corpid='"+corpId+"' " +
				"left outer join inventorypath ip1 on ip1.roomId=r.id and ip1.corpid='"+corpId+"'   " +
				"left outer join workticket wt on l.workTicket = wt.ticket and wt.corpid='"+corpId+"' " +
				"where  l.corpid ='"+corpId+"' and l.shipnumber = '"+shipNumber+"' order by 2,1 desc") 
				.addScalar("l.locationType", Hibernate.STRING)
				.addScalar("l.workTicket", Hibernate.STRING)
				.addScalar("add1", Hibernate.STRING)
				.addScalar("add2", Hibernate.STRING)
				.addScalar("add3", Hibernate.STRING)
				.addScalar("city", Hibernate.STRING)
				.addScalar("state", Hibernate.STRING)
				.addScalar("zip", Hibernate.STRING)
				.addScalar("country", Hibernate.STRING)
				.addScalar("ip.locationId", Hibernate.LONG)
				.addScalar("r.roomDesc", Hibernate.STRING)
				.addScalar("r.floor", Hibernate.STRING)
				.addScalar("damage", Hibernate.STRING)
				.addScalar("r.comment", Hibernate.STRING)
				.addScalar("ip1.roomId", Hibernate.LONG)
				.list(); 
		Iterator it=list.iterator();
		  while(it.hasNext()) {
			  Object [] row=(Object[])it.next();
			  moverDto = new MoversDTO();
			  moverDto.setLocation(row[0]!=null  ? row[0].toString() :"" );
			  moverDto.setTicket(row[1]!=null  ? row[1].toString() :"" );
			  String add =row[2]!=null ? row[2].toString() :"";
			  if(add!=""){
				  if(row[3]!=null && !row[3].toString().trim().equals("") )
					  add = add + ", "+(row[3]!=null  ? row[3].toString() :""); 
			  }else{
				  add = row[3]!=null && !row[3].toString().trim().equals("") ? row[3].toString() :"";
			  }
			  if(add!=""){
				  if(row[4]!=null && !row[4].toString().trim().equals("") )
					  add = add + ", "+(row[4]!=null  ? row[4].toString() :""); 
			  }else{
				  add = row[4]!=null && !row[4].toString().trim().equals("") ? row[4].toString() :"";
			  }
			  if(add!=""){
				  if(row[5]!=null && !row[5].toString().trim().equals("") )
					  add = add + ", "+(row[5]!=null  ? row[5].toString() :""); 
			  }else{
				  add = row[5]!=null && !row[5].toString().trim().equals("")  ? row[5].toString() :"";
			  }
			  if(add!=""){
				  if(row[6]!=null && !row[6].toString().trim().equals("") )
					  add = add + ", "+(row[6]!=null  ? row[6].toString() :""); 
			  }else{
				  add = row[6]!=null && !row[6].toString().trim().equals("") ? row[6].toString() :"";
			  }
			  if(add!=""){
				  if(row[8]!=null && !row[8].toString().trim().equals("") )
					  add = add + ", "+(row[8]!=null  ? row[8].toString() :""); 
			  }else{
				  add = row[8]!=null && !row[8].toString().trim().equals("") ? row[8].toString() :"";
			  }
			  if(add!=""){
				  if(row[7]!=null && !row[7].toString().trim().equals("") )
					  add = add + ", "+(row[7]!=null  ? row[7].toString() :""); 
			  }else{
				  add = row[7]!=null && !row[7].toString().trim().equals("") ? row[7].toString() :"";
			  }
			  moverDto.setAddress(add);
			 // moverDto.setCity(row[5]!=null ? row[5].toString() :"");
			 // moverDto.setState(row[6]!=null ? row[6].toString() :"");
			 // moverDto.setZip(row[7]!=null ? row[7].toString() :"");
			  //moverDto.setCountry(row[8]!=null ? row[8].toString() :"");
			  moverDto.setPhotos(row[9]!=null ? row[9].toString() :"");
			  moverDto.setRooms(row[10]!=null  ? row[10].toString() :"");
			  moverDto.setFloors(row[11]!=null  ? row[11].toString() :"");
			  moverDto.setHasDamage(row[12]!=null ? row[12].toString() :"");
			  moverDto.setComments(row[13]!=null ? row[13].toString() :"");
			  moverDto.setdPhotos(row[14]!=null ? row[14].toString() :"");
			  movValueList.add(moverDto);
		  }
		  return movValueList;
	}
	
	public List checkInventoryLocation(String corpID, String locType,String ticket){
		return getHibernateTemplate().find(" from InventoryLocation where corpId='"+corpID+"' and locationType='"+locType+"' and workTicket='"+ticket+"' ");
	}

}
