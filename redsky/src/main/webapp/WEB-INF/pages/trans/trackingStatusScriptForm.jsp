<SCRIPT LANGUAGE="JavaScript">  
//  function used to calculate the entitle net weight. 
function calcNetWeight1(){
var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.entitleGrossWeight'].value;
var Q2 = document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeight'].value; 
Q1 = Math.round(Q1*100)/100
Q2 = Math.round(Q2*100)/100
var E1='';
	if(Q1<Q2){
		alert("Grossweight should be greater than Tareweight");
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
	}else if(Q1 != undefined && Q2 != undefined){
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value=E2;
		var E3 = E2*0.4536;
		var E4=Math.round(E3*10000)/10000;
		var Q3 = Q1*0.4536;
		var Q4=Math.round(Q3*10000)/10000;
		var Q5 = Q2*0.4536;
		var Q6=Math.round(Q5*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=Q4;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeightKilo'].value=Q6;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E4;
	}else if(Q1 == undefined && Q2 == undefined){
		E1=0;
		var E2=Math.round(E1*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value=E2;
		var E3 = E2*0.4536;
		var E4=Math.round(E3*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E4;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=0;
	}else if(Q2 == undefined){
		E1=Q1;
		var E2=Math.round(E1*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value=E2;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
		var E3 = E2*0.4536;
		var E4=Math.round(E3*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=E4;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E4;
	}else if(Q1 == undefined){
		alert("Grossweight should be greater than Tareweight");
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
        document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
	}
} 
//  End of function.
    
    	function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
			document.getElementById(autocompleteDivId).style.display = "none";
			if(document.getElementById(paertnerCodeId).value==''){
				document.getElementById(partnerNameId).value="";	
			}
			}

			function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
				lastName=lastName.replace("~","'");
				document.getElementById(partnerNameId).value=lastName;
				document.getElementById(paertnerCodeId).value=partnercode;
				document.getElementById(autocompleteDivId).style.display = "none";	
				if(paertnerCodeId=='networkPartnerCodeId'){
					checkNetworkPartnerName();
				}else if(paertnerCodeId=='trackingOriginAgentCodeId'){
					resetContactEmail('OA');
					checkVendorName();
					chkIsRedSky();
				}else if(paertnerCodeId=='trackingDestinationAgentCodeId'){
					resetContactEmail('DA');
					checkVendorNameDest();
					chkIsDestinRedSky();
				}else if(paertnerCodeId=='trackingOriginSubAgentCodeId'){
					resetContactEmail('SOA');
					checkVendorNameSub();
					chkIsOSubRedSky();
				}else if(paertnerCodeId=='trackingDestinationSubAgentCodeId'){
					resetContactEmail('SDA');
					checkVendorNameDestSub();
                     chkIsDSubRedSky();
				}else if(paertnerCodeId=='trackingBrokerCodeId'){
					resetContactEmail('BC');
					checkBroker();
					okValidateListFromBroker();
				}else if(paertnerCodeId=='trackingforwarderCodeId'){
					resetContactEmail('FC');
					checkForwarderName();
					checkForwarder();
					okValidateListFromForwarder();
				}
			}
       
//  function used to calculate the estimate net weight.  
function calcNetWeight2(){
var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.estimateGrossWeight'].value;
var Q2 = document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeight'].value;
Q1 = Math.round(Q1*100)/100
Q2 = Math.round(Q2*100)/100

var E1='';
	if(Q1<Q2){
		alert("Grossweight should be greater than Tareweight");
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
	}else if(Q1 != undefined && Q2 != undefined){
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value=E2;
		var E3 = E2*0.4536;
		var E4=Math.round(E3*10000)/10000;
		var Q3 = Q1*0.4536;
		var Q4=Math.round(Q3*10000)/10000;
		var Q5 = Q2*0.4536;
		var Q6=Math.round(Q5*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=Q4;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E4;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeightKilo'].value=Q6;
	}else if(Q1 == undefined && Q2 == undefined){
		E1=0;
		var E2=Math.round(E1*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value=E2;
		var E3 = E2*0.4536;
        var E4=Math.round(E3*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E4;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=0;
	}else if(Q2 == undefined){
		E1=Q1;
		var E2=Math.round(E1*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value=E2;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
		var E3 = E2*0.4536;
        var E4=Math.round(E3*10000)/10000;
        document.forms['trackingStatusForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=E4;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E4;
	}else if(Q1 == undefined){
		alert("Grossweight should be greater than Tareweight");
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
        document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
	}
} 
//  End of function.
    
//  function used to calculate the actual net weight. 
function calcNetWeight3(){
		var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.actualGrossWeight'].value;
     	var Q2 = document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeight'].value;
     	Q1 = Math.round(Q1*100)/100
        Q2 = Math.round(Q2*100)/100
		var E1='';
			if(Q1<Q2){
				alert("Grossweight should be greater than Tareweight");
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
		        document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){
				E1=Q1-Q2;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value=E2;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*0.4536;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*0.4536;
				var Q6=Math.round(Q5*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualGrossWeightKilo'].value=Q4;
		        document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value=E4;
		        document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeightKilo'].value=Q6;
			}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value=E2;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value=E4;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualGrossWeightKilo'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value=E2;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*10000)/10000;
		        document.forms['trackingStatusForm'].elements['miscellaneous.actualGrossWeightKilo'].value=E4;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualGrossWeightKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
		        document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
			}
}
//  End of function.
    
//  function used to calculate the rwgh net weight. 
function calcNetWeight4(){
	
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.rwghGross'].value;
		var Q2 = document.forms['trackingStatusForm'].elements['miscellaneous.rwghTare'].value;
		Q1 = Math.round(Q1*100)/100
		   Q2 = Math.round(Q2*100)/100
		
		var E1='';
			if(Q1<Q2){
				alert("Grossweight should be greater than Tareweight");
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghTare'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghTareKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){	
				E1=Q1-Q2;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value=E2;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*0.4536;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*0.4536;
				var Q6=Math.round(Q5*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghGrossKilo'].value=Q4;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghTareKilo'].value=Q6;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value=E4;
			}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value=E2;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value=E4;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghGrossKilo'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value=E2;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghTareKilo'].value=0;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghGrossKilo'].value=E4;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghTare'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghGrossKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghTareKilo'].value=0;
			}
}
//  End of function.
    
//  function used to calculate the chargeable net weight. 
function calcNetWeight5(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.chargeableGrossWeight'].value;
		var Q2 = document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeight'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
        Q2 = Math.round(Q2*100)/100
			if(Q1<Q2){
				alert("Grossweight should be greater than Tareweight");
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){	
			     E1=Q1-Q2;
				 var E2=Math.round(E1*10000)/10000;
				 document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value=E2;
				 var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*0.4536;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*0.4536;
				var Q6=Math.round(Q5*10000)/10000;
				 document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E4;
				 document.forms['trackingStatusForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=Q4;
				 document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=Q6;

			}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value=E2;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E4;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value=E2;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=E4;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
			}
	}
//  End of function.
	
//for weight and volume adjusable

//  End of function.
function calcGrossVolume1(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.entitleCubicFeet'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100

			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.entitleCubicMtr'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.entitleCubicMtr'].value=0;
			}
			
	}
//  End of function.    

function calcGrossVolume2(){
        var Q1 =document.forms['trackingStatusForm'].elements['miscellaneous.estimateCubicFeet'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.estimateCubicMtr'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.estimateCubicMtr'].value=0;
			}
			
	}
//  End of function. 
function calcGrossVolume3(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.actualCubicFeet'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualCubicMtr'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.actualCubicMtr'].value=0;
			}
	}
//  End of function. 
function calcGrossVolume4(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.chargeableCubicFeet'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableCubicMtr'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.chargeableCubicMtr'].value=0;
			}
	}
//  End of function. 
function calcGrossVolume5(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.rwghCubicFeet'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghCubicMtr'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.rwghCubicMtr'].value=0;
			}
	}
//  End of function. 
function calcNetVolume1(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.netEntitleCubicFeet'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.netEntitleCubicMtr'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.netEntitleCubicMtr'].value=0;
			}
	}
//  End of function. 
function calcNetVolume2(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.netEstimateCubicFeet'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.netEstimateCubicMtr'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.netEstimateCubicMtr'].value=0;
			}
	}
//  End of function. 
function calcNetVolume3(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.netActualCubicFeet'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.netActualCubicMtr'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.netActualCubicMtr'].value=0;
			}
	}


//  End of function. 
function calcNetVolume4(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetCubicFeet'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetCubicMtr'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetCubicMtr'].value=0;
			} } 
//  End of function. 
function calcNetVolume5(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetCubicFeet'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetCubicMtr'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetCubicMtr'].value=0;
			} } 
//  End of function.  

function calcNetVolumeMtr1(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.netEntitleCubicMtr'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.netEntitleCubicFeet'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.netEntitleCubicFeet'].value=0;
			} } 
//  End of function.  
function calcNetVolumeMtr2(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.netEstimateCubicMtr'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.netEstimateCubicFeet'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.netEstimateCubicFeet'].value=0;
			} }  
//  End of function. 
function calcNetVolumeMtr3(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.netActualCubicMtr'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.netActualCubicFeet'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.netActualCubicFeet'].value=0;
			} } 
//  End of function. 
function calcNetVolumeMtr4(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetCubicMtr'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetCubicFeet'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetCubicFeet'].value=0;
			} } 
//  End of function. 
function calcNetVolumeMtr5(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetCubicMtr'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetCubicFeet'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetCubicFeet'].value=0;
			} }  
//  End of function.  
function calcGrossVolumeMtr1(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.entitleCubicMtr'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.entitleCubicFeet'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.entitleCubicFeet'].value=0;
			} }  
//  End of function. 
function calcGrossVolumeMtr2(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.estimateCubicMtr'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.estimateCubicFeet'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.estimateCubicFeet'].value=0;
			} }  
//  End of function. 
function calcGrossVolumeMtr3(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.actualCubicMtr'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualCubicFeet'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.actualCubicFeet'].value=0;
			} } 
//  End of function. 
function calcGrossVolumeMtr4(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.chargeableCubicMtr'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableCubicFeet'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.chargeableCubicFeet'].value=0;
			} } 
//  End of function. 
function calcGrossVolumeMtr5(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.rwghCubicMtr'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghCubicFeet'].value=E3;
			}
			else{
			 document.forms['trackingStatusForm'].elements['miscellaneous.rwghCubicFeet'].value =0;
			} } 
//  End of function.  
function calcNetWeightKgs1(){
var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.entitleGrossWeightKilo'].value;
var Q2 = document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeightKilo'].value;

var E1='';
Q1 = Math.round(Q1*100)/100
Q2 = Math.round(Q2*100)/100
	if(Q1<Q2){
		alert("Grossweight should be greater than Tareweight");
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
	}else if(Q1 != undefined && Q2 != undefined){
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E2;
		        var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleGrossWeight'].value=Q4;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeight'].value=Q6;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value=E4;
	}else if(Q1 == undefined && Q2 == undefined){
		E1=0;
		var E2=Math.round(E1*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E2;
		var E3 = E2*2.2046;
		var E4=Math.round(E3*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value=E4;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleGrossWeight'].value=0;
	}else if(Q2 == undefined){
		E1=Q1;
		var E2=Math.round(E1*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E2;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeight'].value=0;
		var E3 = E2*2.2046;
		var E4=Math.round(E3*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleGrossWeight'].value=E4;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value=E4;
	}else if(Q1 == undefined){
		alert("Grossweight should be greater than Tareweight");
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleGrossWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value=0;
        document.forms['trackingStatusForm'].elements['miscellaneous.entitleTareWeight'].value=0;
	} 
} 
//  End of function.  
function calcNetWeightKgs2(){
var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.estimateGrossWeightKilo'].value;
var Q2 = document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeightKilo'].value; 
var E1='';
Q1 = Math.round(Q1*100)/100
Q2 = Math.round(Q2*100)/100
	if(Q1<Q2){
		alert("Grossweight should be greater than Tareweight");
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
	}else if(Q1 != undefined && Q2 != undefined){
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E2;
        var E3 = E2*2.2046;
		var E4=Math.round(E3*10000)/10000;
		var Q3 = Q1*2.2046;
		var Q4=Math.round(Q3*10000)/10000;
		var Q5 = Q2*2.2046;
		var Q6=Math.round(Q5*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateGrossWeight'].value=Q4;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value=E4;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeight'].value=Q6;
	}else if(Q1 == undefined && Q2 == undefined){
		E1=0;
		var E2=Math.round(E1*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E2;
		var E3 = E2*2.2046;
		var E4=Math.round(E3*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value=E4;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateGrossWeight'].value=0;
	}else if(Q2 == undefined){
		E1=Q1;
		var E2=Math.round(E1*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E2;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeight'].value=0;
		var E3 = E2*2.2046;
		var E4=Math.round(E3*10000)/10000;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateGrossWeight'].value=E4
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value=E4;
	}else if(Q1 == undefined){
		alert("Grossweight should be greater than Tareweight");
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateGrossWeight'].value=0;
		document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
        document.forms['trackingStatusForm'].elements['miscellaneous.estimateTareWeight'].value=0;
	} } 
//  End of function. 
//  function used to calculate the actual net weight.  
function calcNetWeightKgs3(){ 
		var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.actualGrossWeightKilo'].value;
     	var Q2 = document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeightKilo'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
        Q2 = Math.round(Q2*100)/100
			if(Q1<Q2){
				alert("Grossweight should be greater than Tareweight");
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
		        document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){
				E1=Q1-Q2;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualGrossWeight'].value=Q4;
		        document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value=E4;
		        document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeight'].value=Q6;
			}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value=E4;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualGrossWeight'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value=E2;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeight'].value=0;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualGrossWeight'].value=E4;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualGrossWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeight'].value=0;
			} } 
//  End of function. 
//  function used to calculate the rwgh net weight.  
function calcNetWeightKgs4(){
	
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.rwghGrossKilo'].value;
		var Q2 = document.forms['trackingStatusForm'].elements['miscellaneous.rwghTareKilo'].value;
		
		var E1='';
		Q1 = Math.round(Q1*100)/100
Q2 = Math.round(Q2*100)/100
			if(Q1<Q2){
				alert("Grossweight should be greater than Tareweight");
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghTare'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghTareKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){	
				E1=Q1-Q2;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghGross'].value=Q4;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghTare'].value=Q6;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value=E4;
			}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value=E4;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghGross'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value=E2;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghTare'].value=0;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghGross'].value=E4;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghTareKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghGross'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghTare'].value=0;
			} } 
//  End of function. 
//  function used to calculate the chargeable net weight.  
function calcNetWeightKgs5(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value
		var Q2 = document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeightKilo'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
Q2 = Math.round(Q2*100)/100
			if(Q1<Q2){
				alert("Grossweight should be greater than Tareweight");
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){	
			     E1=Q1-Q2;
				 var E2=Math.round(E1*10000)/10000;
				 document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E2;
				 var E3 = E2*2.2046;
				 var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
				 document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value=E4;
				 document.forms['trackingStatusForm'].elements['miscellaneous.chargeableGrossWeight'].value=Q4;
				 document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeight'].value=Q6; 
			}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				 var E4=Math.round(E3*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value=E4;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableGrossWeight'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E2;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
				var E3 = E2*2.2046;
				 var E4=Math.round(E3*10000)/10000;
				 document.forms['trackingStatusForm'].elements['miscellaneous.chargeableGrossWeight'].value=E4;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableGrossWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
			} }  
//  End of function.  
//  End of function. 
function calcNetWeightLbs1(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
			} } 
function calcNetWeightLbs2(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
			} } 
	function calcNetWeightLbs3(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
			} }  
	function calcNetWeightLbs4(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
			} } 
	function calcNetWeightLbs5(){
        var Q1 =document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value=0;
			} } 
//  End of function. 
function calcNetWeightOnlyKgs1(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeightKilo'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.entitleNetWeight'].value=0;
			} } 
function calcNetWeightOnlyKgs2(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeightKilo'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
			} } 
	function calcNetWeightOnlyKgs3(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value=0;
			} } 
	function calcNetWeightOnlyKgs4(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeightKilo'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
			} } 
	function calcNetWeightOnlyKgs5(){
        var Q1 = document.forms['trackingStatusForm'].elements['miscellaneous.rwghNetKilo'].value;
		var E1='';
		Q1 = Math.round(Q1*100)/100
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value=E3;
			}
			else{
			document.forms['trackingStatusForm'].elements['miscellaneous.rwghNet'].value=0;
			} }  
function findTareWeight(){
     var sid = document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerTareWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse6;
     http2.send(null);
} }
function findVolumeWeight(){
     var sid = document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerVolumeWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse5;
     http2.send(null);
}
} 
//  function used to put the alert to indicate that weights is available in Conatainer/Carton section. 
 function findWeightKilo(){
     var sid = document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerWeightKiloList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse33;
     http4.send(null); 
}
} 
function handleHttpResponse33()
        {
             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 
                 }else{
                 	document.forms['trackingStatusForm'].elements['miscellaneous.actualGrossWeightKilo'].value = '${miscellaneous.actualGrossWeightKilo}';
                 	document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value = '${miscellaneous.actualNetWeightKilo}';
                 	calcNetWeightKgs3();
                 } } }  }   
//  End of function. 
//  function used to put the alert to indicate that weights is available in Conatainer/Carton section.  
function findNetWeightKilo(){
     var sid = document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }else{
     var url="containerNetWeightKiloList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse22;
     http4.send(null);
     } } 
function handleHttpResponse22() { 
             if (http4.readyState == 4)  {
                var results = http4.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 
                 }else{
                 	document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeightKilo'].value = '${miscellaneous.actualNetWeightKilo}';
                 } 	} }  }  
//  End of function. 
//  function used to put the alert to indicate that weights is available in Conatainer/Carton section. 
function findTareWeightKilo(){
     var sid = document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerTareWeightKiloList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse66;
     http4.send(null);
} } 
function handleHttpResponse66()  { 
             if (http4.readyState == 4) {
                var results = http4.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 
                 }else{
               
                 	document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeightKilo'].value = '${miscellaneous.actualTareWeightKilo}';
                 	calcNetWeightKgs3();	
                 } } } }     
//  End of function. 
//  function used to put the alert to indicate that volume is available in Conatainer/Carton section. 
function findVolumeWeightCbm(){
     var sid = document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerVolumeWeightCbmList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse55;
     http4.send(null);
} } 
function handleHttpResponse55()  {
             if (http4.readyState == 4)  {
                var results = http4.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 
                 }else{
                 	document.forms['trackingStatusForm'].elements['miscellaneous.actualCubicMtr'].value = '${miscellaneous.actualCubicMtr}';
                 	document.forms['trackingStatusForm'].elements['miscellaneous.actualCubicFeet'].value = '${miscellaneous.actualCubicFeet}';
                 } } }  }  
//  End of function.  
//Functions for Expirations of vendor and carriers 
var httpValidateDestin = getHTTPObjectDestinValidate();
function getHTTPObjectDestinValidate(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
function validateListForDestinAgent(){ 
 if(document.forms['trackingStatusForm'].elements['validateFlagDestin'].value=='OK'){
        var parentId=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
        var deliveryLastDay=document.forms['trackingStatusForm'].elements['trackingStatus.deliveryLastDay'].value;                
	    var url="controlExpirationsListInDomestic.html?ajax=1&decorator=simple&popup=true&parentId=" + encodeURI(parentId)+"&deliveryLastDay=" + encodeURI(deliveryLastDay);
	    httpValidateDestin.open("GET", url, true);
	    httpValidateDestin.onreadystatechange = handleHttpDestinResponse;
	    httpValidateDestin.send(null);	    
	      }
} 
function handleHttpDestinResponse(){
		if (httpValidateDestin.readyState == 4){
                var results = httpValidateDestin.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                               
                if(res !=''){        
                var totalElement= res.indexOf(',');                
                if(totalElement>0)
                {
                var res = res.split(",");
                var res11 = res[0].split("#");
                var res22 = res[1].split("#");  
                var res33 = "";
                if(res[2] != undefined)
                {
                	res33 = res[2].split("#");
                }
                var ExpiryFor = "";
                 if(res11[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res11[1]+":   "+res11[2];
                 }
                 if(res22[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res22[1]+":   "+res22[2];
                 }
                 if(res33[1] != "" &&  res33[1] != undefined)
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res33[1]+":   "+res33[2];
                 }
                if(res11[0] =='Yes' || res22[0].trim() =='Yes')
                 {
                 alert("This Destination Agent cannot be selected due to the folllowing expirations: "+ExpiryFor);
                 document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";
                 document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";
			     document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
                 }else{
                 var agree =confirm("This Destination Agent has the following expirations: "+ExpiryFor+"\nDo you wish to proceed?");                 
                 if(agree) 
			     {
			     } 
			     else 
			     { 
			         document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";	
			         document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";		        
			         document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select(); 
			     } }  } 
                 else{
                 var res = res.split("#");
                 var ExpiryFor = ""; 
                 ExpiryFor = ExpiryFor+"\n"+res[1]+":   "+res[2];               
                 if(res[0] =='Yes')
                 {
                 alert("This Destination Agent cannot be selected due to the expirations: "+ExpiryFor);
                 document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";	
                 document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";		     
			     document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();			     
                 }else{                 
                 var agree =confirm("This Destination Agent has expirations: "+ExpiryFor+"\nDo you wish to proceed?");
			     if(agree) 
			     {			     
			     } 
			     else 
			     { 
			         document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";
			         document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";
			         document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();			         
			     }   } }  }   }
      document.forms['trackingStatusForm'].elements['validateFlagDestin'].value=''; 
      } 
 function okValidateListFromDestin(){
document.forms['trackingStatusForm'].elements['validateFlagDestin'].value='OK';
} 
var httpValidateBroker = getHTTPObjectBrokerValidate();
function getHTTPObjectBrokerValidate(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
function validateListForBroker(){ 
 if(document.forms['trackingStatusForm'].elements['validateFlagBroker'].value=='OK'){
        var parentId=document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].value;
        var deliveryLastDay=document.forms['trackingStatusForm'].elements['trackingStatus.deliveryLastDay'].value;                
	    var url="controlExpirationsListInDomestic.html?ajax=1&decorator=simple&popup=true&parentId=" + encodeURI(parentId)+"&deliveryLastDay=" + encodeURI(deliveryLastDay);
	    httpValidateBroker.open("GET", url, true);
	    httpValidateBroker.onreadystatechange = handleHttpBrokerResponse;
	    httpValidateBroker.send(null);	    
	      } } 
function handleHttpBrokerResponse(){
		if (httpValidateBroker.readyState == 4){
                var results = httpValidateBroker.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                               
                if(res !=''){        
                var totalElement= res.indexOf(',');                
                if(totalElement>0)
                {
                var res = res.split(",");
                var res11 = res[0].split("#");
                var res22 = res[1].split("#");  
                var res33 = "";
                if(res[2] != undefined)
                {
                	res33 = res[2].split("#");
                }
                var ExpiryFor = "";
                 if(res11[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res11[1]+":   "+res11[2];
                 }
                 if(res22[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res22[1]+":   "+res22[2];
                 }
                 if(res33[1] != "" &&  res33[1] != undefined)
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res33[1]+":   "+res33[2];
                 }
                if(res11[0] =='Yes' || res22[0].trim() =='Yes')
                 {
                 alert("This Broker cannot be selected due to the folllowing expirations: "+ExpiryFor);
                 document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].value="";
                 document.forms['trackingStatusForm'].elements['trackingStatus.brokerName'].value="";
			     document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].select();
                 }else{
                 var agree =confirm("This Broker has the following expirations: "+ExpiryFor+"\nDo you wish to proceed?");                 
                 if(agree) 
			     {
			     } 
			     else 
			     { 
			         document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].value="";	
			         document.forms['trackingStatusForm'].elements['trackingStatus.brokerName'].value="";		        
			         document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].select(); 
			     }   }  } 
                 else{
                 var res = res.split("#");
                 var ExpiryFor = ""; 
                 ExpiryFor = ExpiryFor+"\n"+res[1]+":   "+res[2];               
                 if(res[0] =='Yes')
                 {
                 alert("This Broker cannot be selected due to the expirations: "+ExpiryFor);
                 document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].value="";	
                 document.forms['trackingStatusForm'].elements['trackingStatus.brokerName'].value="";		     
			     document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].select();			     
                 }else{                 
                 var agree =confirm("This Broker has expirations: "+ExpiryFor+"\nDo you wish to proceed?");
			     if(agree) 
			     {			     
			     } 
			     else 
			     { 
			         document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].value="";
			         document.forms['trackingStatusForm'].elements['trackingStatus.brokerName'].value="";
			         document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].select();			         
			     }    }    }   }               
      }
      document.forms['trackingStatusForm'].elements['validateFlagBroker'].value=''; 
      } 
 function okValidateListFromBroker(){
document.forms['trackingStatusForm'].elements['validateFlagBroker'].value='OK';
} 
var httpValidateForwarder = getHTTPObjectForwarderValidate();
function getHTTPObjectForwarderValidate(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    
function validateListForForwarder(){ 
 if(document.forms['trackingStatusForm'].elements['validateFlagForwarder'].value=='OK'){
        var parentId=document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value;
        var deliveryLastDay=document.forms['trackingStatusForm'].elements['trackingStatus.deliveryLastDay'].value;                
	    var url="controlExpirationsListInDomestic.html?ajax=1&decorator=simple&popup=true&parentId=" + encodeURI(parentId)+"&deliveryLastDay=" + encodeURI(deliveryLastDay);
	    httpValidateForwarder.open("GET", url, true);
	    httpValidateForwarder.onreadystatechange = handleHttpForwarderResponse;
	    httpValidateForwarder.send(null);	    
	      } } 
function handleHttpForwarderResponse(){
		if (httpValidateForwarder.readyState == 4){
                var results = httpValidateForwarder.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                               
                if(res !=''){        
                var totalElement= res.indexOf(',');                
                if(totalElement>0)
                {
                var res = res.split(",");
                var res11 = res[0].split("#");
                var res22 = res[1].split("#");  
                var res33 = "";
                if(res[2] != undefined)
                {
                	res33 = res[2].split("#");
                }
                var ExpiryFor = "";
                 if(res11[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res11[1]+":   "+res11[2];
                 }
                 if(res22[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res22[1]+":   "+res22[2];
                 }
                 if(res33[1] != "" &&  res33[1] != undefined)
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res33[1]+":   "+res33[2];
                 }
                if(res11[0] =='Yes' || res22[0].trim() =='Yes')
                 {
                 alert("This Forwarder cannot be selected due to the folllowing expirations: "+ExpiryFor);
                 document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value="";
                 document.forms['trackingStatusForm'].elements['trackingStatus.forwarder'].value="";
			     document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].select();
                 }else{
                 var agree =confirm("This Forwarder has the following expirations: "+ExpiryFor+"\nDo you wish to proceed?");                 
                 if(agree) 
			     {
			     } 
			     else 
			     { 
			         document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value="";	
			         document.forms['trackingStatusForm'].elements['trackingStatus.forwarder'].value="";		        
			         document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].select(); 
			     }   }  } 
                 else{
                 var res = res.split("#");
                 var ExpiryFor = ""; 
                 ExpiryFor = ExpiryFor+"\n"+res[1]+":   "+res[2];               
                 if(res[0] =='Yes')
                 {
                 alert("This Forwarder cannot be selected due to the expirations: "+ExpiryFor);
                 document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value="";	
                 document.forms['trackingStatusForm'].elements['trackingStatus.forwarder'].value="";		     
			     document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].select();			     
                 }else{                 
                 var agree =confirm("This Forwarder has expirations: "+ExpiryFor+"\nDo you wish to proceed?");
			     if(agree) 
			     {			     
			     } 
			     else 
			     { 
			         document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value="";
			         document.forms['trackingStatusForm'].elements['trackingStatus.forwarder'].value="";
			         document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].select();			         
			     }   } } }               
      }
      document.forms['trackingStatusForm'].elements['validateFlagForwarder'].value=''; 
      } 
 function okValidateListFromForwarder(){
document.forms['trackingStatusForm'].elements['validateFlagForwarder'].value='OK';
} 
var httpValidateSubOaAg = getHTTPObjectSubOaAgValidate();
function getHTTPObjectSubOaAgValidate(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}   
var searchFlag=''; 
function validateForEXSO(fieldId){
	if(searchFlag=='TRUE'){
	 	var savedAgentExSo="${"+fieldId+"}";
	 	var presentAgentExSo=document.forms['trackingStatusForm'].elements[fieldId].value;
		if(presentAgentExSo!='' && presentAgentExSo != savedAgentExSo){
			document.forms['trackingStatusForm'].elements[fieldId].value='';
			searchFlag='';
		}  } } 
function validateListForSubOaAg(){ 
	 if(document.forms['trackingStatusForm'].elements['validateFlagSubOaAg'].value=='OK'){
	    var parentId=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
	    var deliveryLastDay=document.forms['trackingStatusForm'].elements['trackingStatus.deliveryLastDay'].value;                
	    var url="controlExpirationsListInDomestic.html?ajax=1&decorator=simple&popup=true&parentId=" + encodeURI(parentId)+"&deliveryLastDay=" + encodeURI(deliveryLastDay);
	    httpValidateSubOaAg.open("GET", url, true);
	    httpValidateSubOaAg.onreadystatechange = handleHttpSubOaAgResponse;
	    httpValidateSubOaAg.send(null);	    
	 } } 
function handleHttpSubOaAgResponse(){
		if (httpValidateSubOaAg.readyState == 4){
                var results = httpValidateSubOaAg.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                               
                if(res !=''){        
                var totalElement= res.indexOf(',');                
                if(totalElement>0)
                {
                var res = res.split(",");
                var res11 = res[0].split("#");
                var res22 = res[1].split("#");  
                var res33 = "";
                if(res[2] != undefined)
                {
                	res33 = res[2].split("#");
                }
                var ExpiryFor = "";
                 if(res11[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res11[1]+":   "+res11[2];
                 }
                 if(res22[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res22[1]+":   "+res22[2];
                 }
                 if(res33[1] != "" &&  res33[1] != undefined)
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res33[1]+":   "+res33[2];
                 }
                if(res11[0] =='Yes' || res22[0].trim() =='Yes')
                 {
                 alert("This Sub Origin Agent cannot be selected due to the folllowing expirations: "+ExpiryFor);
                 document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value="";
                 document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value="";
			     document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
                 }else{
                 var agree =confirm("This Sub Origin Agent has the following expirations: "+ExpiryFor+"\nDo you wish to proceed?");                 
                 if(agree) 
			     {
			     } 
			     else 
			     { 
			         document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value="";	
			         document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value="";		        
			         document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
			         
			     }   }  } 
                 else{
                 var res = res.split("#");
                 var ExpiryFor = ""; 
                 ExpiryFor = ExpiryFor+"\n"+res[1]+":   "+res[2];               
                 if(res[0] =='Yes')
                 {
                 alert("This Sub Origin Agent cannot be selected due to the expirations: "+ExpiryFor);
                 document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value="";	
                 document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value="";		     
			     document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();			     
                 }else{                 
                 var agree =confirm("This Sub Origin Agent has expirations: "+ExpiryFor+"\nDo you wish to proceed?");
			     if(agree) 
			     {			     
			     } 
			     else 
			     { 
			         document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value="";
			         document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value="";
			         document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();			         
			     }   }   }   }    }
      document.forms['trackingStatusForm'].elements['validateFlagSubOaAg'].value='';
      
      } 
 function okValidateListFromSubOaAg(){
document.forms['trackingStatusForm'].elements['validateFlagSubOaAg'].value='OK';
} 
var httpValidateSubDaAg = getHTTPObjectSubDaAgValidate();
function getHTTPObjectSubDaAgValidate(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    
function validateListForSubDaAg(){ 
 if(document.forms['trackingStatusForm'].elements['validateFlagSubDaAg'].value=='OK'){
        var parentId=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
        var deliveryLastDay=document.forms['trackingStatusForm'].elements['trackingStatus.deliveryLastDay'].value;                
	    var url="controlExpirationsListInDomestic.html?ajax=1&decorator=simple&popup=true&parentId=" + encodeURI(parentId)+"&deliveryLastDay=" + encodeURI(deliveryLastDay);
	    httpValidateSubDaAg.open("GET", url, true);
	    httpValidateSubDaAg.onreadystatechange = handleHttpSubDaAgResponse;
	    httpValidateSubDaAg.send(null);	    
	      } } 
function handleHttpSubDaAgResponse(){
		if (httpValidateSubDaAg.readyState == 4){
                var results = httpValidateSubDaAg.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                               
                if(res !=''){        
                var totalElement= res.indexOf(',');                
                if(totalElement>0)
                {
                var res = res.split(",");
                var res11 = res[0].split("#");
                var res22 = res[1].split("#");  
                var res33 = "";
                if(res[2] != undefined)
                {
                	res33 = res[2].split("#");
                }
                var ExpiryFor = "";
                 if(res11[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res11[1]+":   "+res11[2];
                 }
                 if(res22[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res22[1]+":   "+res22[2];
                 }
                 if(res33[1] != "" &&  res33[1] != undefined)
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res33[1]+":   "+res33[2];
                 }
                if(res11[0] =='Yes' || res22[0].trim() =='Yes')
                 {
                 alert("This Sub Destination Agent cannot be selected due to the folllowing expirations: "+ExpiryFor);
                 document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value="";
                 document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";
			     document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
                 }else{
                 var agree =confirm("This Sub Destination Agent has the following expirations: "+ExpiryFor+"\nDo you wish to proceed?");                 
                 if(agree) 
			     {
			     } 
			     else 
			     { 
			         document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value="";	
			         document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";		        
			         document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select(); 
			     } }  } 
                 else{
                 var res = res.split("#");
                 var ExpiryFor = ""; 
                 ExpiryFor = ExpiryFor+"\n"+res[1]+":   "+res[2];               
                 if(res[0] =='Yes')
                 {
                 alert("This Sub Destination Agent cannot be selected due to the expirations: "+ExpiryFor);
                 document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value="";	
                 document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";		     
			     document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();			     
                 }else{                 
                 var agree =confirm("This Sub Destination Agent has expirations: "+ExpiryFor+"\nDo you wish to proceed?");
			     if(agree) 
			     {			     
			     } 
			     else 
			     { 
			         document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value="";
			         document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";
			         document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();			         
			     }  } }  }   }
      document.forms['trackingStatusForm'].elements['validateFlagSubDaAg'].value='';
      
      } 
 function okValidateListFromSubDaAg(){
document.forms['trackingStatusForm'].elements['validateFlagSubDaAg'].value='OK';
} 
//End of Functions. 
function CheckTotalPercentage()
{			
	var v1=document.forms['trackingStatusForm'].elements['miscellaneous.originScore'].value;
	if(v1<0 || v1>100)
	{
		alert("1780 Score % should be between 0 and 100.");		
		document.forms['trackingStatusForm'].elements['miscellaneous.originScore'].value="";
	}	 } 
window.onload = function(){
animatedcollapse.hide('weightnvolumetracsub');
animatedcollapse.hide('weightnvolumetracsub2');
animatedcollapse.hide('weights');
} 
function toCheckWeightnVolumeRadio1()
{
     var weightTabSub1  =  parseInt(document.forms['trackingStatusForm'].elements['weightTabSub1'].value);
     var weightTabSub2  =  parseInt(document.forms['trackingStatusForm'].elements['weightTabSub2'].value);
	if(document.forms['trackingStatusForm'].elements['weightnVolumeRadio'].checked == true)
	{  if(weightTabSub1%2==0){
        animatedcollapse.toggle('weightnvolumetracsub');
        weightTabSub1 =weightTabSub1+1;
        document.forms['trackingStatusForm'].elements['weightTabSub1'].value=String(weightTabSub1);
        }    
    }
	else if(document.forms['trackingStatusForm'].elements['weightnVolumeRadio2'].checked ==true)
	{ if(weightTabSub2%2==0){
         animatedcollapse.toggle('weightnvolumetracsub2');
         weightTabSub2 =weightTabSub2+1;
         document.forms['trackingStatusForm'].elements['weightTabSub2'].value=String(weightTabSub2);
       }	 } }
function checkMetricOrPoundUnitTab() {
 var weightTab      =  parseInt(document.forms['trackingStatusForm'].elements['weightTab'].value);
 var weightTabSub1  =  parseInt(document.forms['trackingStatusForm'].elements['weightTabSub1'].value);
 var weightTabSub2  =  parseInt(document.forms['trackingStatusForm'].elements['weightTabSub2'].value);
 if(weightTab%2==1){ 
 if(document.forms['trackingStatusForm'].elements['weightnVolumeRadio'].checked==true){
   animatedcollapse.toggle('weightnvolumetracsub');
   weightTabSub1 =weightTabSub1+1;
   document.forms['trackingStatusForm'].elements['weightTabSub1'].value=String(weightTabSub1);
  }
if(document.forms['trackingStatusForm'].elements['weightnVolumeRadio2'].checked==true){
  animatedcollapse.toggle('weightnvolumetracsub2');
    weightTabSub2 =weightTabSub2+1;
   document.forms['trackingStatusForm'].elements['weightTabSub2'].value=String(weightTabSub2);
  }
  weightTab =weightTab+1;
  document.forms['trackingStatusForm'].elements['weightTab'].value=String(weightTab);
  }else{
       if(weightTabSub1%2==0){
       animatedcollapse.toggle('weightnvolumetracsub');
       weightTabSub1 =weightTabSub1+1;
       document.forms['trackingStatusForm'].elements['weightTabSub1'].value=String(weightTabSub1);
       }
       if(weightTabSub2%2==0){
         animatedcollapse.toggle('weightnvolumetracsub2');
         weightTabSub2 =weightTabSub2+1;
         document.forms['trackingStatusForm'].elements['weightTabSub2'].value=String(weightTabSub2);
       }
        weightTab =weightTab+1;
      document.forms['trackingStatusForm'].elements['weightTab'].value=String(weightTab);
     } } 
function toIncrementWeightTabSub1(){
     var weightTabSub1=  parseInt(document.forms['trackingStatusForm'].elements['weightTabSub1'].value);
      weightTabSub1 =weightTabSub1+1;
      document.forms['trackingStatusForm'].elements['weightTabSub1'].value=String(weightTabSub1);
 }
function  toIncrementWeightTabSub2(){
         var weightTabSub2=  parseInt(document.forms['trackingStatusForm'].elements['weightTabSub2'].value);
          weightTabSub2 =weightTabSub2+1;
         document.forms['trackingStatusForm'].elements['weightTabSub2'].value=String(weightTabSub2); 
}
function checkVendorNameInLand(){ 
    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.vendorId'].value;
	vendorId=vendorId.trim();
	if(vendorId == ''){
	
		document.forms['trackingStatusForm'].elements['trackingStatus.vendorName'].value="";
		 }
	if(vendorId != ''){ 
    document.forms['trackingStatusForm'].elements['trackingStatus.vendorId'].value=vendorId;
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2In;
     http2.send(null);
    }  }
function handleHttpResponse2In(){
	if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();
            var res = results.split("#"); 
	           if(res.length >= 2){ 
	           		if(res[2] == 'Approved'){
	           			document.forms['trackingStatusForm'].elements['trackingStatus.vendorName'].value = res[1];
	           			document.forms['trackingStatusForm'].elements['trackingStatus.vendorId'].select(); 
		                	          	
	           		}else{
	           			alert("Vendor code is not approved." ); 
					    document.forms['trackingStatusForm'].elements['trackingStatus.vendorName'].value="";
					    document.forms['trackingStatusForm'].elements['trackingStatus.vendorId'].value="";
					    document.forms['trackingStatusForm'].elements['trackingStatus.vendorId'].select();
	           		}
            }else{
                 alert("Vendor code not valid." );
                 document.forms['trackingStatusForm'].elements['trackingStatus.vendorName'].value="";
			 	 document.forms['trackingStatusForm'].elements['trackingStatus.vendorId'].value="";
			 	 document.forms['trackingStatusForm'].elements['trackingStatus.vendorId'].select();
		   } } }
		   
function closeAutoDivId(){
	document.getElementById("vendorDivId").style.display = "none";
	if(document.forms['trackingStatusForm'].elements['trackingStatus.vendorId'].value==""){
	document.forms['trackingStatusForm'].elements['trackingStatus.vendorName'].value="";
	document.forms['trackingStatusForm'].elements['trackingStatus.vendorId'].value="";
	}
}

</script>
