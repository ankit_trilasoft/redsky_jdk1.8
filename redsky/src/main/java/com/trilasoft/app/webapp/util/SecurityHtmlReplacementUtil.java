package com.trilasoft.app.webapp.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.PropertyUtils;

import com.trilasoft.app.webapp.json.JSONArray;
import com.trilasoft.app.webapp.json.JSONException;

public class SecurityHtmlReplacementUtil {
	

	public static String getDisableKeyBoardInputScript(){
		return "<script language=javascript type=text/javascript>"+
			   " function disableKeys(evt) { return false; }"+
			   ""+
			   "document.onkeypress = disableKeys;"+
			   ""+
			   "</script>";
	}


}
