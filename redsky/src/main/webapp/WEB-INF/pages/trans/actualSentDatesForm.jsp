<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%-- Shift 1 --%>
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%> 

<head>
<meta name="heading" content="<fmt:message key='actualSentDatesForm.heading'/>"/> 
<title><fmt:message key="actualSentDatesForm.title"/></title> 
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
<%-- Shift 2 --%>
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>

</head>

<s:form id="actualSentDatesForm" action="updateActualDate.html?btntype=yes&decorator=simple&popup=true" method="post">  
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/> 
<s:hidden name="recInvoiceCheck" /> 
<s:hidden name="shipNumber" value="${shipNumber}"/> 
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:set name="actualSenttoDatesList" value="actualSenttoDatesList" scope="request"/> 
<table border="0" class="detailTabLabel" cellpadding="0" cellspacing="0"> 
	<tr><td width="41%"></td>
	<td>
	<table  border="0" align="left"  cellpadding="0" cellspacing="0" class="detailTabLabel"  style="width:390px"><tr>
	<td align="left" class="listwhitetext"><b>Please select the accountlines for reset Actual Sent to Client Dates: </b></td> 
	</tr></table> 
	
	<table border="0" style="width:390px"> 
	<tr></td>
	<td>
<display:table name="actualSenttoDatesList" class="table" requestURI="" id="actualSenttoDatesList" style="width:380px" defaultsort="1" pagesize="100" >   
 		
		<display:column property="recInvoiceNumber" sortable="true" title="Invoice#" style="width:70px" />
		 <display:column property="sendActualToClient" sortable="true" title="Actual Sent to client Dates"   style="width:120px" format="{0,date,dd-MMM-yyyy}"/> 
	   <display:column title=""   style="width:10px"> 
	     <input type="checkbox"  style="margin-left:10px;"  onclick="actualSenttoDateCheck('${actualSenttoDatesList.recInvoiceNumber}',this)" />
        </display:column>
	     
</display:table> 
</td>
</tr>
</table>
<table border="0" align="left" class="detailTabLabel" cellpadding="0" cellspacing="0" style="width:390px"> 
<tr>
   <td align="left"><input type="button" class="cssbutton1" style="" value="Cancel" onclick="window.close();"/>  
   </td>
   <td align="left"><s:submit cssClass="cssbutton1" type="button" value="Reset Actual Sent to Client Dates " name="resetActualDate"  cssStyle="width:200px;" onclick="" onmouseover="" />  
   </td>
</tr>
</table></td>
	</tr></table> 
</s:form>

<%-- Script Shifted from Top to Botton on 10-Sep-2012 By Kunal --%>
<%-- Shift 1 --%>
<script type="text/javascript">
  function pick() { 
	  parent.window.opener.document.location.reload();  
	  window.close();
  } 
</script>
<%-- Shift 2 --%>
<script type="text/javascript">  
	function actualSenttoDateCheck(rowId,targetElement) 
	{ 
			if(targetElement.checked)
		    {
			      var userCheckStatus = document.forms['actualSentDatesForm'].elements['recInvoiceCheck'].value;
			      if(userCheckStatus == '')
			      {
				  	document.forms['actualSentDatesForm'].elements['recInvoiceCheck'].value = rowId;
			      }
			      else
			      {
			      	 var userCheckStatus=	document.forms['actualSentDatesForm'].elements['recInvoiceCheck'].value = userCheckStatus + ',' + rowId;
			      	document.forms['actualSentDatesForm'].elements['recInvoiceCheck'].value = userCheckStatus.replace( ',,' , ',' );
			      }
		    }
		   if(targetElement.checked==false)
		    {
			     var userCheckStatus = document.forms['actualSentDatesForm'].elements['recInvoiceCheck'].value;
			     var userCheckStatus=document.forms['actualSentDatesForm'].elements['recInvoiceCheck'].value = userCheckStatus.replace( rowId , '' );
			     document.forms['actualSentDatesForm'].elements['recInvoiceCheck'].value = userCheckStatus.replace( ',,' , ',' );
			}
	     DateCheck();
	}
	    
	function DateCheck(){
	    var invoiceCheck = document.forms['actualSentDatesForm'].elements['recInvoiceCheck'].value;
	    invoiceCheck=invoiceCheck.trim(); 
	    if(invoiceCheck=='')
	    {
	    	document.forms['actualSentDatesForm'].elements['resetActualDate'].disabled=true;
	    }
	    else if(invoiceCheck==',')
	    {
	    	document.forms['actualSentDatesForm'].elements['resetActualDate'].disabled=true;
	    }
	    else if(invoiceCheck!='')
	    {
	     	 document.forms['actualSentDatesForm'].elements['resetActualDate'].disabled=false;
	    }
	}
</script> 
<%-- Shifting Closed Here --%>

<script type="text/javascript"> 
try{
if(document.forms['actualSentDatesForm'].elements['btntype'].value=='yes'){
pick();
} 
}
catch(e){}
try
{
DateCheck();
}
catch(e){}
</script>  