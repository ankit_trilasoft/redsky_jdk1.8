package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.InventoryData;
import com.trilasoft.app.model.InventoryPath;
import com.trilasoft.app.model.LumpSumInsurance;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.LumpSumInsuranceManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.VehicleManager;

public class LumpSumInsuranceAction extends BaseAction {
	private Vehicle vehicle; 
	private LumpSumInsurance lumpsuminsurance;
	private LumpSumInsuranceManager lumpSumInsuranceManager;
	private ServiceOrder serviceOrder;
	private ServiceOrderManager serviceOrderManager;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private RefMasterManager refMasterManager;
	private VehicleManager vehicleManager;
	private UserManager userManager;
	private User user;
	private Billing billing;
	private BillingManager billingManager; 
	private String sessionCorpID;
	private Long id;
	private Long sid;
	private String commodity;
	private String newField;
	private String fieldValue;
	private Map<String,String> vehicleTypeList;
	private String invListflag;
	private List vehicleList;
	private List itemInsuranceList;
	private String idNumber;
	private String vehicleStatus;
	
	public LumpSumInsuranceAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
	}
	public String getComboList(String corpID){
		vehicleTypeList = refMasterManager.findByParameter(corpID, "VEHICLETYPE");
		return SUCCESS;
	}
	@SkipValidation
	public String lumpSumInsuranceList(){
		try {
			getComboList(sessionCorpID);
			itemInsuranceList=lumpSumInsuranceManager.getItemList(sid,sessionCorpID);
			vehicleList=lumpSumInsuranceManager.getVehicle(sid,sessionCorpID);
			if(itemInsuranceList!=null && !itemInsuranceList.isEmpty()){
				invListflag="1";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	@SkipValidation
	public String addRowToInsurance() throws Exception{
		getComboList(sessionCorpID);
		try {
			serviceOrder=serviceOrderManager.get(sid);
			trackingStatus=trackingStatusManager.get(sid);
			 getComboList(sessionCorpID);
			 lumpsuminsurance=new  LumpSumInsurance(); 
			 lumpsuminsurance.setCreatedOn(new Date());
			 lumpsuminsurance.setCreatedBy(getRequest().getRemoteUser());
			 lumpsuminsurance.setUpdatedOn(new Date());
			 lumpsuminsurance.setUpdatedBy(getRequest().getRemoteUser());
			 lumpsuminsurance.setCorpID(sessionCorpID);
			 lumpsuminsurance.setServiceOrderID(sid);
			 lumpsuminsurance=lumpSumInsuranceManager.save(lumpsuminsurance);
			 itemInsuranceList=lumpSumInsuranceManager.getItemList(sid,sessionCorpID);
			vehicleList=lumpSumInsuranceManager.getVehicle(sid,sessionCorpID);
			if(itemInsuranceList!=null && !itemInsuranceList.isEmpty()){
				invListflag="1";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
		      if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup()){
		    	  lumpsuminsurance.setNetworkSynchedId(lumpsuminsurance.getId());
		    	  lumpsuminsurance = lumpSumInsuranceManager.save(lumpsuminsurance);
		    	  List linkedShipNumber=findLinkerShipNumber(serviceOrder);
				  List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
				  synchornizeLumpsuminsuranceData(serviceOrderRecords,lumpsuminsurance,serviceOrder,trackingStatus,true);
		      }
		 }catch(Exception e){
			 
		 }
		return SUCCESS;
	}
	
	@SkipValidation
	private void synchornizeLumpsuminsuranceData(List<Object> records,	LumpSumInsurance lumpSumInsuranceOld,ServiceOrder serviceOrder,TrackingStatus trackingStatus ,boolean isNew) {
		
		Long invId= new Long(0);
		Date createdOn = new Date();
		String cratedBy = "";
		Iterator  it=records.iterator(); 
			while(it.hasNext()){
				ServiceOrder so=(ServiceOrder)it.next(); 
				TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(so.getId());
			    if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup() && serviceOrder.getShipNumber()!=null && so.getShipNumber()!=null && (!(serviceOrder.getShipNumber().toString().trim().equalsIgnoreCase(so.getShipNumber().toString().trim()))) ){
				LumpSumInsurance lumpsuminsuranceNew =new  LumpSumInsurance();
				if(!isNew){
					try{
					String lumpSumInsuranceId =  lumpSumInsuranceManager.getLinkedLumpSumInsuranceData(lumpSumInsuranceOld.getId(),so.getId());
					if(!(lumpSumInsuranceId.equals(""))){
					lumpsuminsuranceNew =lumpSumInsuranceManager.get(Long.parseLong(lumpSumInsuranceId));
					invId = lumpsuminsuranceNew.getId();
					cratedBy = lumpsuminsuranceNew.getCreatedBy();
					createdOn = lumpsuminsuranceNew.getCreatedOn();
					}
					}catch(Exception ex){
						String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
				    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
				    	  //errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
				    	 ex.printStackTrace();
					}
				}
				try{
					 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
					 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					 beanUtilsBean2.copyProperties(lumpsuminsuranceNew, lumpSumInsuranceOld);
					 if(isNew){
						 lumpsuminsuranceNew.setId(null);
						 lumpsuminsuranceNew.setCreatedBy("Networking");
						 lumpsuminsuranceNew.setCreatedOn(new Date());
					 }else{
						 lumpsuminsuranceNew.setId(invId);
						 lumpsuminsuranceNew.setCreatedBy(cratedBy);
						 lumpsuminsuranceNew.setCreatedOn(createdOn);
					 }
					 
					
					 lumpsuminsuranceNew.setCorpID(so.getCorpID());
					 lumpsuminsuranceNew.setServiceOrderID(so.getId()); 
					 lumpsuminsuranceNew.setUpdatedBy("Networking");
					 lumpsuminsuranceNew.setUpdatedOn(new Date());
					  
					 lumpsuminsuranceNew.setNetworkSynchedId(lumpSumInsuranceOld.getId());
					 lumpsuminsuranceNew= lumpSumInsuranceManager.save(lumpsuminsuranceNew);
					 
				}catch(Exception ex){
					String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
			    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
			    	  //errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	 ex.printStackTrace();
				}
			}
		
			}


	}
	
	@SkipValidation
	private List findLinkerShipNumber(ServiceOrder serviceOrder) {
		List linkedShipNumberList= new ArrayList();
		if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
			linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
		}else{
			linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
		}
		return linkedShipNumberList;
	}
	
	@SkipValidation
	private List<Object> findServiceOrderRecords(List linkedShipNumber,	ServiceOrder serviceOrderLocal) {
		List<Object> recordList= new ArrayList();
		Iterator it =linkedShipNumber.iterator();
		while(it.hasNext()){
			String shipNumber= it.next().toString();
			ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
			recordList.add(serviceOrderRemote);
		}
		return recordList;
		}
	
	private String vehicleIdNumber;
	private Long autoId;
	@SkipValidation
	public String addRowToVehicle() throws Exception{
		getComboList(sessionCorpID);
		try {
			serviceOrder=serviceOrderManager.get(sid);
			trackingStatus = trackingStatusManager.get(sid);
			getComboList(sessionCorpID);
			 vehicle=new Vehicle();
			 vehicle.setCreatedOn(new Date());
			 vehicle.setCreatedBy(getRequest().getRemoteUser());
			 vehicle.setUpdatedOn(new Date());
			 vehicle.setUpdatedBy(getRequest().getRemoteUser());
			 vehicle.setCorpID(sessionCorpID);
			 vehicle.setServiceOrderId(sid);
			 vehicle.setShipNumber(serviceOrder.getShipNumber());
			 List maxIdNumber = vehicleManager.findMaximumIdNumber(serviceOrder.getShipNumber());
			 if ( maxIdNumber.get(0) == null ) 
			  {          
				 vehicleIdNumber = "01";
   }else {
			    	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
			  	if((autoId.toString()).length() == 1) 
			  		{
			  		vehicleIdNumber = "0"+(autoId.toString());
			  		}else {
			  			vehicleIdNumber=autoId.toString();
			  			        
			  			}
			  	} 
			 vehicle.setIdNumber(vehicleIdNumber);
			 vehicle.setServiceOrder(serviceOrder);
			 vehicleManager.save(vehicle);
			itemInsuranceList=lumpSumInsuranceManager.getItemList(sid,sessionCorpID);
			vehicleList=lumpSumInsuranceManager.getVehicle(sid,sessionCorpID);
			if(itemInsuranceList!=null && !itemInsuranceList.isEmpty()){
				invListflag="1";
			}
			try{
			      if(serviceOrder.getIsNetworkRecord()){
			    	  List linkedShipNumber=findLinkerShipNumber(serviceOrder);
					  List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
					  synchornizeVehicle(serviceOrderRecords,vehicle,true,serviceOrder,trackingStatus);
			      }
			 }catch(Exception e){
				 e.printStackTrace();
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;	
	}
	
	private void synchornizeVehicle(List<Object> serviceOrderRecords,Vehicle vehicle, boolean isNew ,ServiceOrder serviceOrderold,TrackingStatus trackingStatus) {
		
		Iterator  it=serviceOrderRecords.iterator();
		while(it.hasNext()){
			ServiceOrder serviceOrder=(ServiceOrder)it.next();
			TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrder.getId());
			try{
				if((!(serviceOrder.getShipNumber().equals(serviceOrderold.getShipNumber())))){	
				if(isNew){
					Vehicle vehicleNew= new Vehicle();
					BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
					beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					beanUtilsBean.copyProperties(vehicleNew, vehicle);
					
					vehicleNew.setId(null);
					vehicleNew.setCorpID(serviceOrder.getCorpID());
					vehicleNew.setShipNumber(serviceOrder.getShipNumber());
					vehicleNew.setSequenceNumber(serviceOrder.getSequenceNumber());
					vehicleNew.setServiceOrder(serviceOrder);
					vehicleNew.setServiceOrderId(serviceOrder.getId());
					vehicleNew.setCreatedBy("Networking");
					vehicleNew.setUpdatedBy("Networking");
					vehicleNew.setCreatedOn(new Date());
					vehicleNew.setUpdatedOn(new Date()); 
					// added to fixed issue of merge functionality in 2 step linking
					try{
					 List maxIdNumber = vehicleManager.findMaximumIdNumber(serviceOrder.getShipNumber());
				       if ( maxIdNumber.get(0) == null ) 
				        {          
				      	 idNumber = "01";
			         }else {
				          	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
				        	if((autoId.toString()).length() == 1) 
				        		{
				        			idNumber = "0"+(autoId.toString());
				        		}else {
				        			 idNumber=autoId.toString();
				        			        
				        			}
				        	} 
				    }catch(Exception e){
						System.out.println("Error in id number");
					}
				    vehicleNew.setIdNumber(idNumber);
					vehicleNew = vehicleManager.save(vehicleNew);
					
					 if(vehicleNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
						 vehicleNew.setUgwIntId(vehicleNew.getId().toString());
						 vehicleNew= vehicleManager.save(vehicleNew);
						}
					
					 
				}else{
					Vehicle vehicleTo= vehicleManager.getForOtherCorpid(vehicleManager.findRemoteVehicle(vehicle.getIdNumber(),serviceOrder.getId()));  
	    			Long id=vehicleTo.getId();
	    			String createdBy=vehicleTo.getCreatedBy();
	    			Date createdOn=vehicleTo.getCreatedOn();
	    			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
					beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					beanUtilsBean.copyProperties(vehicleTo, vehicle);
					
					vehicleTo.setId(id);
					vehicleTo.setCreatedBy(createdBy);
					vehicleTo.setCreatedOn(createdOn);
					vehicleTo.setCorpID(serviceOrder.getCorpID());
					vehicleTo.setShipNumber(serviceOrder.getShipNumber());
					vehicleTo.setSequenceNumber(serviceOrder.getSequenceNumber());
					vehicleTo.setServiceOrder(serviceOrder);
					vehicleTo.setServiceOrderId(serviceOrder.getId());
					vehicleTo.setUpdatedBy(vehicle.getCorpID()+":"+getRequest().getRemoteUser());
					vehicleTo.setUpdatedOn(new Date()); 
					/*// added to fixed issue of merge functionality in 2 step linking
					try{
					 List maxIdNumber = vehicleManager.findMaximumIdNumber(serviceOrder.getShipNumber());
				       if ( maxIdNumber.get(0) == null ) 
				        {          
				      	 idNumber = "01";
			         }else {
				          	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
				        	if((autoId.toString()).length() == 1) 
				        		{
				        			idNumber = "0"+(autoId.toString());
				        		}else {
				        			 idNumber=autoId.toString();
				        			        
				        			}
				        	} 
				    }catch(Exception e){
						System.out.println("Error in id number");
					}
				    vehicleTo.setIdNumber(idNumber);*/
					vehicleTo =vehicleManager.save(vehicleTo);
					 if(vehicleTo.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
						 vehicleTo.setUgwIntId(vehicleTo.getId().toString());
						 vehicleTo= vehicleManager.save(vehicleTo);
					}
					
				}
				}
			}catch(Exception ex){
				System.out.println("\n\n\n\n error while synchronizing vehicle"+ex);	
				 ex.printStackTrace();
			}
		}
		
		}
	public String documentFileVehicleDelete(){
		vehicle=vehicleManager.get(id);
		getComboList(sessionCorpID);
		try {
			//vehicleManager.remove(id);
			vehicleManager.updateStatus(id,vehicleStatus,getRequest().getRemoteUser());
			getComboList(sessionCorpID);
			itemInsuranceList=lumpSumInsuranceManager.getItemList(sid,sessionCorpID);
			vehicleList=lumpSumInsuranceManager.getVehicle(sid,sessionCorpID);
			if(itemInsuranceList!=null && !itemInsuranceList.isEmpty()){
				invListflag="1";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
		Long serviceId=    vehicle.getServiceOrderId();
		serviceOrder=serviceOrderManager.get(serviceId);
        trackingStatus = trackingStatusManager.get(serviceId);
        if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup()){
       	List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
			deleteVehicle(serviceOrderRecords,vehicle.getIdNumber(),vehicleStatus);
     }
     }catch(Exception ex){
   	  System.out.println("\n\n\n\n error while removing container--->"+ex);
     }
		return SUCCESS;	
	}
	
	private void deleteVehicle(List<Object> serviceOrderRecords,String idNumber,String vehicleStatus) { 
		Iterator  it=serviceOrderRecords.iterator();
		while(it.hasNext()){
			ServiceOrder serviceOrder=(ServiceOrder)it.next();
			vehicleManager.deleteNetworkVehicle(serviceOrder.getId(),idNumber,vehicleStatus);
		}
			
	}
	
	public String documentFileArticleDelete(){
		lumpsuminsurance=lumpSumInsuranceManager.get(id);
		getComboList(sessionCorpID);
		try {
			lumpSumInsuranceManager.remove(id);
			getComboList(sessionCorpID);
			itemInsuranceList=lumpSumInsuranceManager.getItemList(sid,sessionCorpID);
			vehicleList=lumpSumInsuranceManager.getVehicle(sid,sessionCorpID);
			if(itemInsuranceList!=null && !itemInsuranceList.isEmpty()){
				invListflag="1";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
			Long serviceId=    lumpsuminsurance.getServiceOrderID();
			serviceOrder=serviceOrderManager.get(serviceId);
	        trackingStatus = trackingStatusManager.get(serviceId);
	        if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup()){
	       	List linkedShipNumber=findLinkerShipNumber(serviceOrder);
				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
				deleteLumpSumInsurance(serviceOrderRecords,lumpsuminsurance.getNetworkSynchedId());
	     }
	     }catch(Exception ex){
	   	  System.out.println("\n\n\n\n error while removing container--->"+ex);
	     }
		return SUCCESS;	
	}
	
	
	private void deleteLumpSumInsurance(List<Object> serviceOrderRecords,Long networkSynchedId) { 
		Iterator  it=serviceOrderRecords.iterator();
		while(it.hasNext()){
			ServiceOrder serviceOrder=(ServiceOrder)it.next();
			lumpSumInsuranceManager.deleteNetworkLumpsuminsurance(serviceOrder.getId(),networkSynchedId);
		}
			
	}
	public void updateLumpInsurance(){
		try {
			serviceOrder=serviceOrderManager.get(sid);
			lumpSumInsuranceManager.updateInsurance(newField,fieldValue,id,sid,serviceOrder.getCustomerFileId(),sessionCorpID,getRequest().getRemoteUser());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
			trackingStatus = trackingStatusManager.get(sid);
			lumpsuminsurance =lumpSumInsuranceManager.get(id); 
	        if(serviceOrder.getIsNetworkRecord()  && trackingStatus.getSoNetworkGroup()){
	        	List linkedShipNumber=findLinkerShipNumber(serviceOrder);
				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
	        	
				updateLinkedLumpInsuranceData(serviceOrderRecords,newField,fieldValue,lumpsuminsurance.getNetworkSynchedId());
	      }
	      }catch(Exception ex){
	    	  System.out.println("\n\n\n\n error while removing container--->"+ex);
	      }
		
	}
	
	private void updateLinkedLumpInsuranceData(List<Object> serviceOrderRecords,String newField,String fieldValue,Long networkSynchedId) {
		
		Iterator  it=serviceOrderRecords.iterator();
	    while(it.hasNext()){
		ServiceOrder serviceOrder=(ServiceOrder)it.next();
		lumpSumInsuranceManager.updateLinkedInsurance (newField,fieldValue,networkSynchedId,serviceOrder.getId());
	}

	}
	
	public void updateLumpVehicle(){
		try {
			serviceOrder=serviceOrderManager.get(sid);
			lumpSumInsuranceManager.updateVehicle(newField,fieldValue,id,sid,vehicleIdNumber,sessionCorpID);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
			trackingStatus = trackingStatusManager.get(sid);
			vehicle=vehicleManager.get(id); 
	        if(serviceOrder.getIsNetworkRecord()  && trackingStatus.getSoNetworkGroup()){
	        	List linkedShipNumber=findLinkerShipNumber(serviceOrder);
				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
	        	
				updateLinkedVehicleData(serviceOrderRecords,newField,fieldValue,vehicle.getIdNumber());
	      }
	      }catch(Exception ex){
	    	  System.out.println("\n\n\n\n error while removing container--->"+ex);
	      }
	}
	private void updateLinkedVehicleData(List<Object> serviceOrderRecords,String newField,String fieldValue,String IdNumber) {
		
		Iterator  it=serviceOrderRecords.iterator();
	    while(it.hasNext()){
		ServiceOrder serviceOrder=(ServiceOrder)it.next();
		lumpSumInsuranceManager.updateLinkedVehicleData (newField,fieldValue,serviceOrder.getId(),IdNumber);
	}

	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public LumpSumInsurance getLumpsuminsurance() {
		return lumpsuminsurance;
	}
	public void setLumpsuminsurance(LumpSumInsurance lumpsuminsurance) {
		this.lumpsuminsurance = lumpsuminsurance;
	}
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public void setLumpSumInsuranceManager(
			LumpSumInsuranceManager lumpSumInsuranceManager) {
		this.lumpSumInsuranceManager = lumpSumInsuranceManager;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public void setVehicleManager(VehicleManager vehicleManager) {
		this.vehicleManager = vehicleManager;
	}
	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSid() {
		return sid;
	}
	public void setSid(Long sid) {
		this.sid = sid;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}







	public String getCommodity() {
		return commodity;
	}







	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}







	public String getNewField() {
		return newField;
	}







	public void setNewField(String newField) {
		this.newField = newField;
	}







	public String getFieldValue() {
		return fieldValue;
	}







	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}







	public Map<String, String> getVehicleTypeList() {
		return vehicleTypeList;
	}







	public void setVehicleTypeList(Map<String, String> vehicleTypeList) {
		this.vehicleTypeList = vehicleTypeList;
	}







	public String getInvListflag() {
		return invListflag;
	}







	public void setInvListflag(String invListflag) {
		this.invListflag = invListflag;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}
	public List getVehicleList() {
		return vehicleList;
	}
	public void setVehicleList(List vehicleList) {
		this.vehicleList = vehicleList;
	}
	public List getItemInsuranceList() {
		return itemInsuranceList;
	}
	public void setItemInsuranceList(List itemInsuranceList) {
		this.itemInsuranceList = itemInsuranceList;
	}
	public String getVehicleIdNumber() {
		return vehicleIdNumber;
	}
	public void setVehicleIdNumber(String vehicleIdNumber) {
		this.vehicleIdNumber = vehicleIdNumber;
	}
	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}
	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getVehicleStatus() {
		return vehicleStatus;
	}
	public void setVehicleStatus(String vehicleStatus) {
		this.vehicleStatus = vehicleStatus;
	}
}

