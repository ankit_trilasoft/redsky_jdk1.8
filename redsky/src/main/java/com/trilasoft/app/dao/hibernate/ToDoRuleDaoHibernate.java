package com.trilasoft.app.dao.hibernate;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.lang.time.DateUtils;
import org.apache.velocity.runtime.directive.Parse;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.dao.DataAccessResourceFailureException;
import com.trilasoft.app.dao.ToDoRuleDao;
import com.trilasoft.app.dao.hibernate.CustomerFileDaoHibernate.DTOMerge;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TaskCheckList;
import com.trilasoft.app.model.ToDoResult;
import com.trilasoft.app.model.ToDoRule;
import com.trilasoft.app.model.CheckListResult;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.ToDoResultManager;



public class ToDoRuleDaoHibernate extends GenericDaoHibernate<ToDoRule, Long> implements ToDoRuleDao{
 	private List toDoRules;
 	private List<ToDoResult> toDoResults;
	private HibernateUtil hibernateUtil;
	private CompanyManager companyManager;
	public ToDoRuleDaoHibernate() {   
	    super(ToDoRule.class);   
	    }
	
	 public List findMaximumId() {
	    	return getHibernateTemplate().find("select max(id) from ToDoRule");
	    	
	    }

	public List checkById(Long id) {
		return getHibernateTemplate().find("select id from ToDoRule where id=?",id);
	}
	
	static String replaceWord(String original, String find, String replacement)
	{
		int i = original.indexOf(find);
	    if (i < 0) {
	        return original;  // return original if 'find' is not in it.
	    }
	  
	    String partBefore = original.substring(0, i);
	    String partAfter  = original.substring(i + find.length());
	  
	    return partBefore + replacement + partAfter;

	}

	public List <ToDoRule>searchById(Long id, String fieldToValidate,String testdate,String entitytablerequired,String messagedisplayed,String status, String toDoRuleCheckEnable, Boolean isAgentTdr) {
		String toDoRuleIsAgent="";
		if(id==null){
			String stautsCheck=""; 
			String checkEnableFlag = "";
			
			String query = "from ToDoRule where fieldToValidate1 like '%"+fieldToValidate.replaceAll("'", "''") +"%' AND testdate like '%" +testdate.replaceAll("'", "''")+"%' AND entitytablerequired like '%" +entitytablerequired.replaceAll("'", "''") + "%' AND messagedisplayed like '%" + messagedisplayed.replaceAll("'", "''") + "%' and (docType is null or docType='') ";
			
			if(!status.equalsIgnoreCase("All")){
				stautsCheck= " AND status ='"+ status+"'";
			}
			if(isAgentTdr){
				toDoRuleIsAgent=" AND publishRule=true";
			}else{
				toDoRuleIsAgent=" AND publishRule=false";
			}
			if(toDoRuleCheckEnable.equalsIgnoreCase("true") || toDoRuleCheckEnable.equalsIgnoreCase("false")){
				checkEnableFlag = " AND CheckEnable="+toDoRuleCheckEnable+"";
			}
			
			String queryString = new StringBuilder(query).append(stautsCheck).append(checkEnableFlag).append(toDoRuleIsAgent).toString();
			
			System.err.println("\n\n\nQueryString------>>"+queryString);
			
			toDoRules = getHibernateTemplate().find(queryString);
			/*if(!status.equalsIgnoreCase("All"))
			{
				toDoRules = getHibernateTemplate().find("from ToDoRule where fieldToValidate1 like '"+fieldToValidate.replaceAll("'", "''") +"%' AND testdate like '" +testdate.replaceAll("'", "''")+"%' AND entitytablerequired like '" +entitytablerequired.replaceAll("'", "''") + "%' AND messagedisplayed like '%" + messagedisplayed.replaceAll("'", "''") + "%' AND status like '" + status.replaceAll("'", "''") +"%' AND CheckEnable="+checkEnable+"");
			}
			if(status.equalsIgnoreCase("All"))
			{
				toDoRules = getHibernateTemplate().find("from ToDoRule where fieldToValidate1 like '"+fieldToValidate.replaceAll("'", "''") +"%' AND testdate like '" +testdate.replaceAll("'", "''")+"%' AND entitytablerequired like '" +entitytablerequired.replaceAll("'", "''") + "%' AND messagedisplayed like '%" + messagedisplayed.replaceAll("'", "''") + "%' AND CheckEnable="+checkEnable+"");
			}*/
		}
		else
		{
			if(isAgentTdr){
				toDoRuleIsAgent=" AND publishRule=true";
			}else{
				toDoRuleIsAgent=" AND publishRule=false";
			}
			toDoRules = getHibernateTemplate().find("from ToDoRule where  fieldToValidate1 like '%"+fieldToValidate.replaceAll("'", "''") +"%' AND testdate like '%" +testdate.replaceAll("'", "''")+"%' AND entitytablerequired like '%" +entitytablerequired.replaceAll("'", "''") + "%' AND messagedisplayed like '%" + messagedisplayed.replaceAll("'", "''") + "%' and (docType is null or docType='') "+toDoRuleIsAgent+" and ruleNumber=?",id);
		}
		return toDoRules;
	}
	public List <ToDoRule>searchByDocCheckListId(Long id, String docType,String testdate,String entitytablerequired,String messagedisplayed,String status, String toDoRuleCheckEnable) {
		if(id==null){

			String stautsCheck=""; 
			String checkEnableFlag = "";
			
			String query = "from ToDoRule where docType like '%"+docType.replaceAll("'", "''") +"%' AND testdate like '%" +testdate.replaceAll("'", "''")+"%' AND entitytablerequired like '%" +entitytablerequired.replaceAll("'", "''") + "%' AND messagedisplayed like '%" + messagedisplayed.replaceAll("'", "''") + "%' and docType is not null and docType!='' ";
			
			if(!status.equalsIgnoreCase("All")){
				stautsCheck= "AND status ='"+ status+"'";
			}
			
			if(toDoRuleCheckEnable.equalsIgnoreCase("true") || toDoRuleCheckEnable.equalsIgnoreCase("false")){
				checkEnableFlag = "AND CheckEnable="+toDoRuleCheckEnable+"";
			}
			
			String queryString = new StringBuilder(query).append(stautsCheck).append(checkEnableFlag).toString();
			
			System.err.println("\n\n\nQueryString------>>"+queryString);
			
			toDoRules = getHibernateTemplate().find(queryString);
			/*if(!status.equalsIgnoreCase("All"))
			{
				toDoRules = getHibernateTemplate().find("from ToDoRule where fieldToValidate1 like '"+fieldToValidate.replaceAll("'", "''") +"%' AND testdate like '" +testdate.replaceAll("'", "''")+"%' AND entitytablerequired like '" +entitytablerequired.replaceAll("'", "''") + "%' AND messagedisplayed like '%" + messagedisplayed.replaceAll("'", "''") + "%' AND status like '" + status.replaceAll("'", "''") +"%' AND CheckEnable="+checkEnable+"");
			}
			if(status.equalsIgnoreCase("All"))
			{
				toDoRules = getHibernateTemplate().find("from ToDoRule where fieldToValidate1 like '"+fieldToValidate.replaceAll("'", "''") +"%' AND testdate like '" +testdate.replaceAll("'", "''")+"%' AND entitytablerequired like '" +entitytablerequired.replaceAll("'", "''") + "%' AND messagedisplayed like '%" + messagedisplayed.replaceAll("'", "''") + "%' AND CheckEnable="+checkEnable+"");
			}*/
			
		}
		else
		{
			toDoRules = getHibernateTemplate().find("from ToDoRule where  docType like '%"+docType.replaceAll("'", "''") +"%' AND testdate like '%" +testdate.replaceAll("'", "''")+"%' AND entitytablerequired like '%" +entitytablerequired.replaceAll("'", "''") + "%' AND messagedisplayed like '%" + messagedisplayed.replaceAll("'", "''") + "%' and docType is not null and docType!='' and ruleNumber=?",id);
		}
		return toDoRules;
	}
	
	
	public List getIntegrationLogInfoLog(){
		return getHibernateTemplate().find("from IntegrationLogInfo");
	}
	
	
	public String getLinkedShipnumber(String sequenceNumber, String sessionCorpID){
		String shipNumber ="";
		List<ServiceOrder> l1 = getHibernateTemplate().find(" from ServiceOrder where shipNumber='"+sequenceNumber+"'");
		for (ServiceOrder serviceOrder : l1) {
				if(sessionCorpID.equalsIgnoreCase("UGWW")){
					shipNumber = serviceOrder.getShipNumber();
				}else{
					if(serviceOrder.getIsNetworkRecord()){
					String query = "select if(findnetworkcorpid(t.originAgentCode)= 'UGWW' , t.originAgentExSO , if(findnetworkcorpid(t.destinationAgentCode)= 'UGWW',t.destinationAgentExSO,if(findnetworkcorpid(t.networkPartnerCode)= 'UGWW',t.networkAgentExSO,if(findnetworkcorpid(s.bookingagentcode)= 'UGWW',s.bookingAgentShipNumber,'')))) as shipNumber  from serviceorder s ,trackingstatus t where s.id='"+serviceOrder.getId()+"' and t.id = s.id ";
					List shipNumberList = this.getSession().createSQLQuery(query).list();
					if(shipNumberList!=null && !shipNumberList.isEmpty() && shipNumberList.get(0)!= null ){
						String tempShip =shipNumberList.get(0).toString().trim();
						if(tempShip.contains("UGWW"))
							shipNumber = tempShip;
					}
				}
			}
		}
		return shipNumber;
	}

	public List getLinkSOs(String sequenceNumber, String sessionCorpID){
		String shipNumber ="";
		List l1 = new ArrayList();
		if(sessionCorpID.equals("UGWW")){
			l1 = getHibernateTemplate().find("select shipNumber from ServiceOrder where sequenceNumber='"+sequenceNumber+"'");
		}else{
			String query = "select if(findnetworkcorpid(t.originAgentCode)= 'UGWW' , t.originAgentExSO , if(findnetworkcorpid(t.destinationAgentCode)= 'UGWW',t.destinationAgentExSO,if(findnetworkcorpid(t.networkPartnerCode)= 'UGWW',t.networkAgentExSO,if(findnetworkcorpid(s.bookingagentcode)= 'UGWW',s.bookingAgentShipNumber,'')))) as shipNumber  from serviceorder s ,trackingstatus t where s.sequenceNumber='"+sequenceNumber+"' and t.id = s.id ";
			l1 = this.getSession().createSQLQuery(query).list();
		}
		return l1;
	}
	public  List<Long> getModelIDsByShip(String...ships ){
		List <Long> list = new ArrayList<Long>();
		String query = "select id from "+ships[0]+" where shipnumber in ('"+ships[1]+"','"+ships[2]+"') ";
		List  l1 = this.getSession().createSQLQuery(query).list();
		for (Object object : l1) {
			Long l = new Long(object.toString());
			list.add(l);
		}
		return list;
	}
	
	
	public int getCount(String model,String shipNumber){
		int count = 0;
		try {
			String query= "select count(id) from "+model+" where shipnumber = '"+shipNumber+"'";
			List l =  this.getSession().createSQLQuery(query).list();
			if(l !=null && !l.isEmpty() && l.get(0)!=null &&!l.get(0).equals("") ){
				count = Integer.parseInt(l.get(0).toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return count;
	}
	
	public List<ToDoRule> getRulesForEntity(String entity) {
		toDoRules= getHibernateTemplate().find("from ToDoRule where status='Tested' and checkEnable=true and entitytablerequired = ?", entity);
		return toDoRules;
	}

	
	public int removeAll(String shipNumber, String IDs, String Model){
		int i = 0;
		if(IDs!=null && !IDs.equals(""))
			i= this.getSession().createSQLQuery("delete from "+Model+" where id not in ("+IDs+")  and shipNumber='"+shipNumber+"'").executeUpdate();
		else
			i= this.getSession().createSQLQuery("delete from "+Model+" where shipNumber='"+shipNumber+"'").executeUpdate();
		
		return i;
	}
	
	
	public List<ToDoRule> getAllRules(String sessionCorpID) {
		
		toDoRules= getHibernateTemplate().find("from ToDoRule where status='Tested' and checkEnable=true and corpID in ('TSFT','"+sessionCorpID+"') and (docType is null or docType='') ");
		//toDoRules= getHibernateTemplate().find("from ToDoRule where corpID='"+sessionCorpID+"'");
		return toDoRules;
	}
	public List <ToDoRule> getRuleByTypes (String service,String mode,String routing,String jobType,String billToCode, String bookingAgent,String corpId){
		toDoRules= getHibernateTemplate().find("from ToDoRule where status='Tested' and checkEnable=true and corpID in ('"+corpId+"') "
				+ " and (service is null or service ='' or service like '%"+service+"%')"
				+ " and (billToCode is null or billToCode ='' or billToCode like '%"+billToCode+"%') "
				+ " and (bookingAgent is null or bookingAgent ='' or bookingAgent like '%"+bookingAgent+"%') "
				+ " and (jobType is null or jobType ='' or jobType like '%"+jobType+"%') "
				+ " and (mode is null or mode ='' or mode like '%"+mode+"%') "
				+ " and (routing is null or routing='' or routing like '%"+routing+"%') ");
		//toDoRules= getHibernateTemplate().find("from ToDoRule where corpID='"+sessionCorpID+"'");
		return toDoRules;
	}
	public List linkSequenceNumberList(String sessionCorpID,String bookingAgentcode, String agentType ,String childAgentType,String childAgentCode, ServiceOrder soObj,String listTarget){
		String query="";
		/*if(agentType.equals("BA")){
				query = "select distinct(s.sequenceNumber) from customerfile c , serviceorder s , trackingstatus t  where c.controlflag in ('Q','C') and c.corpid='"+sessionCorpID+"'  and c.status <> 'rejected' and c.id = s.customerFileId and  s.corpid='"+sessionCorpID+"' and  s.id=t.id and t.corpid='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD','DWNLD') and s.controlflag in ('Q','C') and s.isNetworkRecord is false  and ((s.lastname  like '%"+soObj.getLastName()+"%' and s.origincountrycode = '"+soObj.getOriginCountryCode()+"' and s.destinationcountrycode ='"+soObj.getDestinationCountryCode()+"' ) or s.bookingagentcode ='"+bookingAgentcode+"' )";
		}
		if(agentType.equals("OA")){
			query = "select distinct(s.sequenceNumber) from customerfile c , serviceorder s , trackingstatus t where c.controlflag in ('Q','C') and c.corpid='"+sessionCorpID+"'  and c.status <> 'rejected' and c.id = s.customerFileId and s.corpid='"+sessionCorpID+"' and s.id=t.id and t.corpid='"+sessionCorpID+"' and  s.status not in ('CNCL','CLSD','DWNLD') and s.controlflag in ('Q','C') and s.isNetworkRecord is false and ((s.lastname like '%"+soObj.getLastName()+"%' and s.origincountrycode = '"+soObj.getOriginCountryCode()+"' and s.destinationcountrycode ='"+soObj.getDestinationCountryCode()+"' ) or  t.originAgentCode='"+bookingAgentcode+"') ";
		}
		if(agentType.equals("DA")){
			query = "select distinct(s.sequenceNumber) from customerfile c , serviceorder s , trackingstatus t where c.controlflag in ('Q','C') and c.corpid='"+sessionCorpID+"'  and c.status <> 'rejected'  and c.id = s.customerFileId and s.corpid='"+sessionCorpID+"' and s.id=t.id and t.corpid='"+sessionCorpID+"' and  s.status not in ('CNCL','CLSD','DWNLD') and s.controlflag in ('Q','C') and s.isNetworkRecord is false and ((s.lastname like '%"+soObj.getLastName()+"%' and s.origincountrycode = '"+soObj.getOriginCountryCode()+"' and s.destinationcountrycode ='"+soObj.getDestinationCountryCode()+"' ) or  t.destinationAgentCode='"+bookingAgentcode+"') ";
		}
		if(agentType.equals("NwA")){*/
			//query = "select distinct concat(c.sequenceNumber,'~',concat(c.sequenceNumber,', ',c.firstName,' ',c.lastName)) from customerfile c , serviceorder s , trackingstatus t where c.controlflag in ('Q','C') and c.corpid='"+sessionCorpID+"'  and c.status <> 'rejected'   and c.id = s.customerFileId and s.corpid='"+sessionCorpID+"' and s.id=t.id and t.corpid='"+sessionCorpID+"' and  s.status not in ('CNCL','CLSD','DWNLD') and s.controlflag in ('Q','C') and s.isNetworkRecord is false and s.origincountrycode = '"+soObj.getOriginCountryCode()+"' and s.destinationcountrycode ='"+soObj.getDestinationCountryCode()+"' and (s.lastname like '%"+soObj.getLastName()+"%' or t.networkPartnerCode='"+bookingAgentcode+"' or  t.originAgentCode='"+bookingAgentcode+"' or s.bookingagentcode ='"+bookingAgentcode+"' or  t.destinationAgentCode='"+bookingAgentcode+"') ";
			//query = "select distinct s.customerFileId from customerfile c , serviceorder s , trackingstatus t where c.controlflag in ('Q','C') and c.corpid='"+sessionCorpID+"'  and c.status <> 'rejected'   and c.id = s.customerFileId and s.corpid='"+sessionCorpID+"' and s.id=t.id and t.corpid='"+sessionCorpID+"' and  s.status not in ('CNCL','CLSD','DWNLD') and s.controlflag in ('Q','C') and s.isNetworkRecord is false and s.origincountrycode = '"+soObj.getOriginCountryCode()+"' and s.destinationcountrycode ='"+soObj.getDestinationCountryCode()+"' and (s.lastname like '%"+soObj.getLastName()+"%' or t.networkPartnerCode='"+bookingAgentcode+"' or  t.originAgentCode='"+bookingAgentcode+"' or s.bookingagentcode ='"+bookingAgentcode+"' or  t.destinationAgentCode='"+bookingAgentcode+"')  order by c.firstName,c.lastName ";
			//}
		//if(listTarget == null || listTarget.equals("")){
			query = "select distinct s.customerFileId from customerfile c , serviceorder s , trackingstatus t where c.controlflag in ('Q','C') and c.corpid='"+sessionCorpID+"'  and c.status <> 'rejected'   and c.id = s.customerFileId and s.corpid='"+sessionCorpID+"' and s.id=t.id and t.corpid='"+sessionCorpID+"' and  s.status not in ('CNCL','CLSD','DWNLD') and s.controlflag in ('Q','C') and  ((s.origincountrycode = '"+soObj.getOriginCountryCode()+"' and s.destinationcountrycode ='"+soObj.getDestinationCountryCode()+"') or s.firstname like '%"+soObj.getFirstName()+"%' ) and (s.lastname like '%"+soObj.getLastName()+"%' or t.networkPartnerCode='"+bookingAgentcode+"' or  t.originAgentCode='"+bookingAgentcode+"' or s.bookingagentcode ='"+bookingAgentcode+"' or  t.destinationAgentCode='"+bookingAgentcode+"')  order by c.firstName,c.lastName ";
		//}else{
		//	query = "select distinct s.customerFileId from customerfile c , serviceorder s , trackingstatus t where c.controlflag in ('Q','C') and c.corpid='"+sessionCorpID+"'  and c.status <> 'rejected'   and c.id = s.customerFileId and s.corpid='"+sessionCorpID+"' and s.id=t.id and t.corpid='"+sessionCorpID+"' and  s.status not in ('CNCL','CLSD','DWNLD') and s.controlflag in ('Q','C') and s.firstname like '%"+soObj.getFirstName()+"%' and (s.lastname like '%"+soObj.getLastName()+"%' or t.networkPartnerCode='"+bookingAgentcode+"' or  t.originAgentCode='"+bookingAgentcode+"' or s.bookingagentcode ='"+bookingAgentcode+"' or  t.destinationAgentCode='"+bookingAgentcode+"')  order by c.firstName,c.lastName ";
		//}
		List list = this.getSession().createSQLQuery(query).list();
		return  list;
	}
	
	public List findLinkSO(String sessionCorpID, String seqNumber,String bookingAgentcode, String agentType, ServiceOrder soObj,String hitFlag){
		List <Object> mergeList = new ArrayList<Object>();
		String  query ="";
	/*	if(agentType.equals("BA")){
			query="select s.shipnumber,s.commodity,s.mode,if(s.firstname is not null and s.firstname!='',concat(s.firstname,' ',s.lastname),s.lastname) as shipper,s.id  " +
				"from serviceorder s ,trackingstatus t where s.sequencenumber='"+seqNumber+"' and s.status not in ('CNCL','CLSD','DWNLD') and  s.corpid='"+sessionCorpID+"' and " +
				" s.id=t.id and t.corpid='"+sessionCorpID+"'  and s.controlflag in ('Q','C') and s.isNetworkRecord is false and " +
						" ( (s.lastname  like '%"+soObj.getLastName()+"%' and s.origincountrycode = '"+soObj.getOriginCountryCode()+"' and s.destinationcountrycode ='"+soObj.getDestinationCountryCode()+"' ) or s.bookingagentcode ='"+bookingAgentcode+"' )";
		}
		if(agentType.equals("OA")){
			query="select s.shipnumber,s.commodity,s.mode,if(s.firstname is not null and s.firstname!='',concat(s.firstname,' ',s.lastname),s.lastname) as shipper,s.id  " +
				"from serviceorder s ,trackingstatus t where s.sequencenumber='"+seqNumber+"' and s.status not in ('CNCL','CLSD','DWNLD') and  s.corpid='"+sessionCorpID+"' and " +
				" s.id=t.id and t.corpid='"+sessionCorpID+"'  and s.controlflag in ('Q','C') and s.isNetworkRecord is false and " +
						" ((s.lastname like '%"+soObj.getLastName()+"%' and s.origincountrycode = '"+soObj.getOriginCountryCode()+"' and s.destinationcountrycode ='"+soObj.getDestinationCountryCode()+"' ) or t.originAgentCode='"+bookingAgentcode+"' ) ";
		}
		if(agentType.equals("DA")){
			query="select s.shipnumber,s.commodity,s.mode,if(s.firstname is not null and s.firstname!='',concat(s.firstname,' ',s.lastname),s.lastname) as shipper,s.id  " +
				"from serviceorder s ,trackingstatus t where s.sequencenumber='"+seqNumber+"' and s.status not in ('CNCL','CLSD','DWNLD') and  s.corpid='"+sessionCorpID+"' and " +
				" s.id=t.id and t.corpid='"+sessionCorpID+"' and s.controlflag in ('Q','C') and s.isNetworkRecord is false and " +
						" ((s.lastname like '%"+soObj.getLastName()+"%' and s.origincountrycode = '"+soObj.getOriginCountryCode()+"' and s.destinationcountrycode ='"+soObj.getDestinationCountryCode()+"' ) or t.destinationAgentCode='"+bookingAgentcode+"' ) ";
		}
		if(agentType.equals("NwA")){*/
			query="select s.shipnumber,s.commodity,s.mode,if(s.firstname is not null and s.firstname!='',concat(s.firstname,' ',s.lastname),s.lastname) as shipper,s.id  " +
					", s.originCity , s.destinationCity ,s.ugwintid " +
				" from serviceorder s ,trackingstatus t where s.sequencenumber='"+seqNumber+"' and s.status not in ('CNCL','CLSD','DWNLD') and  s.corpid='"+sessionCorpID+"' and " +
				" s.id=t.id and t.corpid='"+sessionCorpID+"'  and s.controlflag in ('Q','C')  " +
						"and (s.lastname like '%"+soObj.getLastName()+"%'" +
									" or t.networkPartnerCode='"+bookingAgentcode+"' " +
											"or  t.originAgentCode='"+bookingAgentcode+"' " +
														"or s.bookingagentcode ='"+bookingAgentcode+"' " +
																"or  t.destinationAgentCode='"+bookingAgentcode+"') ";
		//}
		/*if(hitFlag.equals("name")){
			query = query +" and s.firstname like '%"+soObj.getFirstName()+"%'" ;
		}else{
			query = query +" and s.origincountrycode = '"+soObj.getOriginCountryCode()+"' " +
			"and s.destinationcountrycode ='"+soObj.getDestinationCountryCode()+"' " ;
		}*/
		query = query +" and (s.firstname like '%"+soObj.getFirstName()+"%' or (s.origincountrycode = '"+soObj.getOriginCountryCode()+"' and s.destinationcountrycode ='"+soObj.getDestinationCountryCode()+"' ))";
		
		List list = this.getSession().createSQLQuery(query)
		.addScalar("s.shipnumber",  Hibernate.STRING)
		.addScalar("s.commodity",  Hibernate.STRING)
		.addScalar("s.mode",  Hibernate.STRING)
		.addScalar("shipper",  Hibernate.STRING)
		.addScalar("s.id",  Hibernate.STRING)
		.addScalar("s.originCity", Hibernate.STRING)
		.addScalar("s.destinationCity", Hibernate.STRING)
		.addScalar("s.ugwintid", Hibernate.STRING)
		.list();
		 Iterator it=list.iterator();
		  DTOMerge dto=null;
		  while(it.hasNext())
		  {
			  Object [] row=(Object[])it.next();
			  dto = new DTOMerge();
			  dto.setShipNumber(row[0]);
			  dto.setCommodity(row[1]);
			  dto.setMode(row[2]);
			  dto.setShipper(row[3]);
			  dto.setId(row[4]);
			  dto.setoCity(row[5]);
			  dto.setdCity(row[6]);
			  dto.setUgwIntId(row[7]);
			  mergeList.add(dto);
		  }
		return  mergeList;
	}
	public  class DTOMerge{	
		private Object shipNumber;
		private Object commodity;
		private Object mode;
		private Object shipper;
		private Object id;
		private Object oCity;
		private Object dCity;
		private Object ugwIntId;
		
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getCommodity() {
			return commodity;
		}
		public void setCommodity(Object commodity) {
			this.commodity = commodity;
		}
		public Object getMode() {
			return mode;
		}
		public void setMode(Object mode) {
			this.mode = mode;
		}
		public Object getShipper() {
			return shipper;
		}
		public void setShipper(Object shipper) {
			this.shipper = shipper;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getoCity() {
			return oCity;
		}
		public void setoCity(Object oCity) {
			this.oCity = oCity;
		}
		public Object getdCity() {
			return dCity;
		}
		public void setdCity(Object dCity) {
			this.dCity = dCity;
		}
		public Object getUgwIntId() {
			return ugwIntId;
		}
		public void setUgwIntId(Object ugwIntId) {
			this.ugwIntId = ugwIntId;
		}
		
	 }
	
	
	
	
	public List findNetwokLinkedFiles(String sessionCorpID){
		List networkFiles = new ArrayList();
		String query = "select distinct " +
				"s.bookingAgentName  as agent ," +
				"s.shipNumber as shipNumber ," +
				"s.lastName as lastName ," +
				"s.firstName as firstName ," +
				"s.job as jobType ," +
				"s.commodity as commodity ," +
				"s.mode as mode ," +
				"s.originCountry as origin ," +
				"s.originCityCode as originCity ," +
				"s.destinationCountry as destin ," +
				"s.destinationCityCode as destinCity ," +
				"s.bookingAgentCode as agentCode ," +
				"s.id as id ," +
				"'BA' as type ," +
				"nc.childAgentType as childAgentType ," +
				"nc.childAgentCode as childAgentCode  ," +		
				"nc.companyDiv  as companyDivision  ," +
				"s.shipmentType as shipmentType ," +
				"t.bookingAgentEmail as email ," +
				"ua.action as action ,"+
				"datediff(date_format(now(),'%Y-%m-%d'),nc.createdOn) as daysDiff "+
				"from serviceorder s " +
				"left outer join ugwwactiontracker ua on s.shipnumber=ua.sonumber and ua.corpid='"+sessionCorpID+"'" +
				",networkcontrol nc , trackingstatus t , companydivision c " +
				"where " +
				//"nc.sourceCorpid=findnetworkcorpid(s.bookingAgentCode)" +
				"nc.sourceCorpid='UGWW' and c.corpid='UGWW'" +
				" and t.id=s.id  " +
				"and nc.sourceSoNum = s.shipnumber " +
				"and nc.actionStatus='Pending' and nc.corpId='"+sessionCorpID+"' " +
				"and c.bookingagentcode = s.bookingAgentCode  and s.bookingAgentCode is not null and s.bookingAgentCode!='' group by nc.sourceSoNum  " ;
				// "and  ( findnetworkcorpid(t.originAgentCode)= '"+sessionCorpID+"' or findnetworkcorpid(t.destinationAgentCode)= '"+sessionCorpID+"' or findnetworkcorpid(t.networkPartnerCode)= '"+sessionCorpID+"') " ;
		 Long StartTime = System.currentTimeMillis();
		
			
		List l1 = this.getSession().createSQLQuery(query).list();
		
		 Long timeTaken =  System.currentTimeMillis() - StartTime;
		 
		 logger.warn("\n\nTime taken to execute list 1 Query : "+timeTaken+"\n\n");
		
		query=	"select distinct " +
				"t.originAgent  as agent ," +
				"s.shipNumber as shipNumber ," +
				"s.lastName as lastName ," +
				"s.firstName as firstName ," +
				"s.job as jobType ," +
				"s.commodity as commodity ," +
				"s.mode as mode ," +
				"s.originCountry as origin ," +
				"s.originCityCode as originCity ," +
				"s.destinationCountry as destin ," +
				"s.destinationCityCode as destinCity ," +
				"t.originAgentCode as agentCode ," +
				"s.id as id," +
				"'OA' as type ," +
				"nc.childAgentType as childAgentType ," +
				"nc.childAgentCode as childAgentCode  ," +		
				"nc.companyDiv  as companyDivision  ," +
				"s.shipmentType as shipmentType ," +
				"t.originAgentEmail as email ," +
				"ua.action as action ,"+
				"datediff(date_format(now(),'%Y-%m-%d'),nc.createdOn) as daysDiff "+
				"from serviceorder s " +
				"left outer join ugwwactiontracker ua on s.shipnumber=ua.sonumber and ua.corpid='"+sessionCorpID+"'" +
				",networkcontrol nc , trackingstatus t , companydivision c " +
				"where " +
				//"nc.sourceCorpid=findnetworkcorpid(t.originAgentCode)" +
				"nc.sourceCorpid='UGWW' and c.corpid='UGWW' " +
				" and t.id=s.id  " +
				" and nc.sourceSoNum = s.shipnumber " +
				"and nc.actionStatus='Pending' and nc.corpId='"+sessionCorpID+"' and " +
				"c.bookingagentcode = t.originAgentCode and t.originAgentCode is not null and t.originAgentCode!='' and s.bookingAgentCode!=t.originAgentCode group by nc.sourceSoNum  " ;
				//"and  ( findnetworkcorpid(s.bookingAgentCode)= '"+sessionCorpID+"' or findnetworkcorpid(t.destinationAgentCode)= '"+sessionCorpID+"' or findnetworkcorpid(t.networkPartnerCode)= '"+sessionCorpID+"') " ;
		
			StartTime = System.currentTimeMillis();
		
			
			
			List l2 = this.getSession().createSQLQuery(query).list();
			
			timeTaken =  System.currentTimeMillis() - StartTime;
				
			logger.warn("\n\nTime taken to execute list 2 Query : "+timeTaken+"\n\n");
			
		query="select distinct " +
				"t.destinationAgent  as agent ," +
				"s.shipNumber as shipNumber ," +
				"s.lastName as lastName , " +
				"s.firstName as firstName ," +
				"s.job as jobType ," +
				"s.commodity as commodity , " +
				"s.mode as mode , " +
				"s.originCountry as origin ," +
				"s.originCityCode as originCity ," +
				"s.destinationCountry as destin , " +
				"s.destinationCityCode as destinCity ," +
				"t.destinationAgentCode as agentCode ," +
				"s.id as id ," +
				"'DA' as type ," +
				"nc.childAgentType as childAgentType ," +
				"nc.childAgentCode as childAgentCode  ," +		
				"nc.companyDiv  as companyDivision  ," +
				"s.shipmentType as shipmentType ," +
				"t.destinationAgentEmail as email ," +
				"ua.action as action ,"+
				"datediff(date_format(now(),'%Y-%m-%d'),nc.createdOn) as daysDiff "+
				"from serviceorder s " +
				"left outer join ugwwactiontracker ua on s.shipnumber=ua.sonumber and ua.corpid='"+sessionCorpID+"'" +
				",networkcontrol nc , trackingstatus t , companydivision c " +
				"where " +
				//"nc.sourceCorpid=findnetworkcorpid(t.destinationAgentCode)" +
				"nc.sourceCorpid='UGWW' and c.corpid='UGWW'" +
				" and t.id=s.id  " +
				"and nc.sourceSoNum = s.shipnumber " +
				"and nc.actionStatus='Pending' and nc.corpId='"+sessionCorpID+"' " +
				"and c.bookingagentcode = t.destinationAgentCode  and  t.destinationAgentCode is not null and t.destinationAgentCode!='' and t.destinationAgentCode!=s.bookingAgentCode and t.destinationAgentCode!=t.originAgentCode group by nc.sourceSoNum " ;
				//"and ( findnetworkcorpid(s.bookingAgentCode)= '"+sessionCorpID+"' or findnetworkcorpid(t.originAgentCode)= '"+sessionCorpID+"' or findnetworkcorpid(t.networkPartnerCode)= '"+sessionCorpID+"') " ;
				
				StartTime = System.currentTimeMillis();
		
				
		
				List l3 = this.getSession().createSQLQuery(query).list();	
				
				timeTaken =  System.currentTimeMillis() - StartTime;
				
				logger.warn("\n\nTime taken to execute list 3 Query : "+timeTaken+"\n\n");	
		
		 query ="select distinct " +
				"t.networkPartnerName as agent ," +
				"s.shipNumber as shipNumber ," +
				"s.lastName as lastName , " +
				"s.firstName as firstName ," +
				"s.job as jobType ," +
				"s.commodity as commodity , " +
				"s.mode as mode , " +
				"s.originCountry as origin ," +
				"s.originCityCode as originCity ," +
				"s.destinationCountry as destin , " +
				"s.destinationCityCode as destinCity ," +
				"t.networkPartnerCode as agentCode ," +
				"s.id as id ," +
				"'NwA' as type ," +
				"nc.childAgentType as childAgentType ," +
				"nc.childAgentCode as childAgentCode  ," +		
				"nc.companyDiv  as companyDivision  ," +
				"s.shipmentType as shipmentType ," +
				"t.networkEmail as email ," +
				"ua.action as action , "+
				"datediff(date_format(now(),'%Y-%m-%d'),nc.createdOn) as daysDiff "+
				"from serviceorder s " +
				"left outer join ugwwactiontracker ua on s.shipnumber=ua.sonumber and ua.corpid='"+sessionCorpID+"'" +
				",networkcontrol nc , trackingstatus t , companydivision c " +
				"where " +
				//"nc.sourceCorpid=findnetworkcorpid(t.networkPartnerCode)" +
				"nc.sourceCorpid='UGWW' and c.corpid='UGWW' " +
				" and t.id=s.id  " +
				"and nc.sourceSoNum = s.shipnumber " +
				"and nc.actionStatus='Pending' and nc.corpId='"+sessionCorpID+"' " +
				"and c.bookingagentcode = t.networkPartnerCode and  t.networkPartnerCode is not null and t.networkPartnerCode!='' and t.networkPartnerCode!=t.destinationAgentCode  and  t.networkPartnerCode!=s.bookingAgentCode and t.networkPartnerCode!=t.originAgentCode group by nc.sourceSoNum  ";
			//	"and ( findnetworkcorpid(s.bookingAgentCode)= '"+sessionCorpID+"' or findnetworkcorpid(t.originAgentCode)= '"+sessionCorpID+"' or findnetworkcorpid(t.destinationAgentCode)= '"+sessionCorpID+"') " ;
		 StartTime = System.currentTimeMillis();
			
		
	
			 List l4 = this.getSession().createSQLQuery(query).list();		
			 
				timeTaken =  System.currentTimeMillis() - StartTime;
			
			logger.warn("\n\nTime taken to execute list 4 Query : "+timeTaken+"\n\n");	
			
		 
			
			 query ="select distinct " +
				"t.originSubAgent as agent ," +
				"s.shipNumber as shipNumber ," +
				"s.lastName as lastName , " +
				"s.firstName as firstName ," +
				"s.job as jobType ," +
				"s.commodity as commodity , " +
				"s.mode as mode , " +
				"s.originCountry as origin ," +
				"s.originCityCode as originCity ," +
				"s.destinationCountry as destin , " +
				"s.destinationCityCode as destinCity ," +
				"t.originSubAgentCode as agentCode ," +
				"s.id as id ," +
				"'SOA' as type ," +
				"nc.childAgentType as childAgentType ," +
				"nc.childAgentCode as childAgentCode  ," +		
				"nc.companyDiv  as companyDivision  ," +
				"s.shipmentType as shipmentType ," +
				"t.subOriginAgentEmail as email  ," +
				"ua.action as action ,"+
				"datediff(date_format(now(),'%Y-%m-%d'),nc.createdOn) as daysDiff "+
				"from serviceorder s " +
				"left outer join ugwwactiontracker ua on s.shipnumber=ua.sonumber and ua.corpid='"+sessionCorpID+"'" +
				",networkcontrol nc , trackingstatus t , companydivision c " +
				"where " +
				//"nc.sourceCorpid=findnetworkcorpid(t.networkPartnerCode)" +
				"nc.sourceCorpid='UGWW' and c.corpid='UGWW' " +
				" and t.id=s.id  " +
				"and nc.sourceSoNum = s.shipnumber " +
				"and nc.actionStatus='Pending' and nc.corpId='"+sessionCorpID+"' " +
				" and c.bookingagentcode = t.originSubAgentCode and  t.originSubAgentCode is not null and t.originSubAgentCode!='' and t.originSubAgentCode!=t.destinationAgentCode  and  t.originSubAgentCode!=s.bookingAgentCode and t.originSubAgentCode!=t.originAgentCode" +
				" and t.originSubAgentCode!=t.destinationAgentCode  group by nc.sourceSoNum  ";
			//	"and ( findnetworkcorpid(s.bookingAgentCode)= '"+sessionCorpID+"' or findnetworkcorpid(t.originAgentCode)= '"+sessionCorpID+"' or findnetworkcorpid(t.destinationAgentCode)= '"+sessionCorpID+"') " ;
		 StartTime = System.currentTimeMillis();
			
		
	
			 List l6 = this.getSession().createSQLQuery(query).list();		
			 
				timeTaken =  System.currentTimeMillis() - StartTime;
			
			logger.warn("\n\nTime taken to execute list 6 Query : "+timeTaken+"\n\n");	
			
			

			 query ="select distinct " +
				"t.destinationSubAgent as agent ," +
				"s.shipNumber as shipNumber ," +
				"s.lastName as lastName , " +
				"s.firstName as firstName ," +
				"s.job as jobType ," +
				"s.commodity as commodity , " +
				"s.mode as mode , " +
				"s.originCountry as origin ," +
				"s.originCityCode as originCity ," +
				"s.destinationCountry as destin , " +
				"s.destinationCityCode as destinCity ," +
				"t.destinationSubAgentCode as agentCode ," +
				"s.id as id ," +
				"'SDA' as type ," +
				"nc.childAgentType as childAgentType ," +
				"nc.childAgentCode as childAgentCode  ," +		
				"nc.companyDiv  as companyDivision  ," +
				"s.shipmentType as shipmentType ," +
				"t.subDestinationAgentEmail as email ," +
				"ua.action as action ,"+
				"datediff(date_format(now(),'%Y-%m-%d'),nc.createdOn) as daysDiff "+
				"from serviceorder s " +
				"left outer join ugwwactiontracker ua on s.shipnumber=ua.sonumber and ua.corpid='"+sessionCorpID+"'" +
				",networkcontrol nc , trackingstatus t , companydivision c " +
				"where " +
				//"nc.sourceCorpid=findnetworkcorpid(t.networkPartnerCode)" +
				"nc.sourceCorpid='UGWW' and c.corpid='UGWW' " +
				" and t.id=s.id  " +
				"and nc.sourceSoNum = s.shipnumber " +
				"and nc.actionStatus='Pending' and nc.corpId='"+sessionCorpID+"' " +
				"and c.bookingagentcode = t.destinationSubAgentCode and  t.destinationSubAgentCode is not null and t.destinationSubAgentCode!='' and t.destinationSubAgentCode!=t.destinationAgentCode  and  t.destinationSubAgentCode!=s.bookingAgentCode and t.destinationSubAgentCode!=t.originAgentCode group by nc.sourceSoNum  ";
			//	"and ( findnetworkcorpid(s.bookingAgentCode)= '"+sessionCorpID+"' or findnetworkcorpid(t.originAgentCode)= '"+sessionCorpID+"' or findnetworkcorpid(t.destinationAgentCode)= '"+sessionCorpID+"') " ;
		 StartTime = System.currentTimeMillis();
			
		
	
			 List l7 = this.getSession().createSQLQuery(query).list();		
			 
				timeTaken =  System.currentTimeMillis() - StartTime;
			
			logger.warn("\n\nTime taken to execute list 7 Query : "+timeTaken+"\n\n");	
			
			
			
			StartTime = System.currentTimeMillis();
				
			
		  List l5 = new ArrayList();
		  boolean add = l5.addAll(l1);
		 		add= l5.addAll(l2);
		 		add= l5.addAll(l3);
		 		add= l5.addAll(l4);
		 		add= l5.addAll(l6);
		 		add= l5.addAll(l7);
		 		
		 timeTaken =  System.currentTimeMillis() - StartTime;
		 	logger.warn("\n\nTime taken to creating list 5  : "+timeTaken+"\n\n");	
		 
		/*List l1 = this.getSession().createSQLQuery(query)
		.addScalar("agent", Hibernate.STRING)
		.addScalar("shipNumber", Hibernate.STRING)
		.addScalar("lastName", Hibernate.STRING)
		.addScalar("firstName", Hibernate.STRING)
		.addScalar("jobType", Hibernate.STRING)
		.addScalar("commodity", Hibernate.STRING)
		.addScalar("mode", Hibernate.STRING)
		.addScalar("origin", Hibernate.STRING)
		.addScalar("originCity", Hibernate.STRING)
		.addScalar("destin", Hibernate.STRING)
		.addScalar("destinCity", Hibernate.STRING)
		.addScalar("agentCode", Hibernate.STRING)
		.addScalar("id", Hibernate.LONG)
		.addScalar("type", Hibernate.STRING)
		.addScalar("childAgentType",  Hibernate.STRING)
		.addScalar("childAgentCode",  Hibernate.STRING)
		.list();*/
		
		Iterator it=l5.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			netWorkDTO  dto= new netWorkDTO();
			if(row[0] == null){dto.setAgent("");}else{dto.setAgent(row[0]);}
			if(row[1] == null){dto.setShipNumber("");}else{dto.setShipNumber(row[1]);}
			if(row[2] == null){dto.setLastName("");}else{dto.setLastName(row[2]);}
			if(row[3] == null){dto.setFirstName("");}else{dto.setFirstName(row[3]);}
			if(row[4] == null){dto.setJobType("");}else{dto.setJobType(row[4]);}
			if(row[5] == null){dto.setCommodity("");}else{dto.setCommodity(row[5]);}
			if(row[6] == null){dto.setMode("");}else{dto.setMode(row[6]);}
			if(row[7] == null){dto.setOrigin("");}else{dto.setOrigin(row[7]);}
			if(row[8] == null){dto.setOriginCity("");}else{dto.setOriginCity(row[8]);}
			if(row[9] == null){dto.setDestin("");}else{dto.setDestin(row[9]);}
			if(row[10] == null){dto.setDestinCity("");}else{dto.setDestinCity(row[10]);}
			if(row[11] == null){dto.setAgentCode("");}else{dto.setAgentCode(row[11]);}
			if(row[12] == null){dto.setId("");}else{dto.setId(row[12]);}
			if(row[13] == null){dto.setType("");}else{dto.setType(row[13]);}
			if(row[14]== null){dto.setChildAgentType("");}else{dto.setChildAgentType(row[14]);}
			if(row[15]== null){dto.setChildAgentCode("");}else{dto.setChildAgentCode(row[15]);}
			if(row[16]== null){dto.setCompDiv("");}else{dto.setCompDiv(row[16]);}
			if(row[17]== null){dto.setShipmentType("");}else{dto.setShipmentType(row[17]);}
			if(row[18]== null){dto.setEmail("");}else{dto.setEmail(row[18]);}
			if(row[19]== null){dto.setAction("");}else{dto.setAction(row[19]);}
			if(row[20]== null){dto.setDaysDiff("");}else{dto.setDaysDiff(row[20]);}
			networkFiles.add(dto);
		  }
		
		
		return networkFiles;
	
	}
	
	public  class netWorkDTO{
		private Object id;
		private Object agent;
		private Object shipNumber;
		private Object lastName;
		private Object firstName;
		private Object jobType;
		private Object commodity;
		private Object service;
		private Object mode;
		private Object origin;
		private Object originCity;
		private Object destin;
		private Object destinCity;
		private Object agentCode;
		private Object type;
		private Object childAgentType;
		private Object childAgentCode;
		private Object compDiv;
		private Object shipmentType;
		private Object email;
		private Object action;
		private Object daysDiff;
		
		
		public Object getAgent() {
			return agent;
		}
		public void setAgent(Object agent) {
			this.agent = agent;
		}
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getJobType() {
			return jobType;
		}
		public void setJobType(Object jobType) {
			this.jobType = jobType;
		}
		public Object getCommodity() {
			return commodity;
		}
		public void setCommodity(Object commodity) {
			this.commodity = commodity;
		}
		public Object getService() {
			return service;
		}
		public void setService(Object service) {
			this.service = service;
		}
		public Object getMode() {
			return mode;
		}
		public void setMode(Object mode) {
			this.mode = mode;
		}
		public Object getOrigin() {
			return origin;
		}
		public void setOrigin(Object origin) {
			this.origin = origin;
		}
		public Object getOriginCity() {
			return originCity;
		}
		public void setOriginCity(Object originCity) {
			this.originCity = originCity;
		}
		public Object getDestin() {
			return destin;
		}
		public void setDestin(Object destin) {
			this.destin = destin;
		}
		public Object getDestinCity() {
			return destinCity;
		}
		public void setDestinCity(Object destinCity) {
			this.destinCity = destinCity;
		}
		public Object getAgentCode() {
			return agentCode;
		}
		public void setAgentCode(Object agentCode) {
			this.agentCode = agentCode;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getType() {
			return type;
		}
		public void setType(Object type) {
			this.type = type;
		}
		public Object getChildAgentType() {
			return childAgentType;
		}
		public void setChildAgentType(Object childAgentType) {
			this.childAgentType = childAgentType;
		}
		public Object getChildAgentCode() {
			return childAgentCode;
		}
		public void setChildAgentCode(Object childAgentCode) {
			this.childAgentCode = childAgentCode;
		}
		public Object getCompDiv() {
			return compDiv;
		}
		public void setCompDiv(Object compDiv) {
			this.compDiv = compDiv;
		}
		public Object getShipmentType() {
			return shipmentType;
		}
		public void setShipmentType(Object shipmentType) {
			this.shipmentType = shipmentType;
		}
		public Object getEmail() {
			return email;
		}
		public void setEmail(Object email) {
			this.email = email;
		}
		public Object getAction() {
			return action;
		}
		public void setAction(Object action) {
			this.action = action;
		}
		public Object getDaysDiff() {
			return daysDiff;
		}
		public void setDaysDiff(Object daysDiff) {
			this.daysDiff = daysDiff;
		}
	}
	
	public void updateNetworkControlSO(String sessionCorpID, String shipNumber , String sourceShipNumber ,String action,String childAgentCode){
		String query="" ;
		if(shipNumber.equals("")){
			query= "update networkcontrol set actionStatus='"+action+"'  where sourceSoNum='"+sourceShipNumber+"' and corpId='"+sessionCorpID+"' and childAgentCode='"+childAgentCode+"' and soNumber='' ";
		}else{
			query= "update networkcontrol set actionStatus='"+action+"' , soNumber='"+shipNumber+"' where sourceSoNum='"+sourceShipNumber+"' and corpId='"+sessionCorpID+"' and childAgentCode='"+childAgentCode+"' ";
		}
		this.getSession().createSQLQuery(query).executeUpdate();
	}
	
	
	public List executeRule(String ruleExpression,ToDoRule todoRule) {
		List tempErrorList = new ArrayList();
		String message = "";
			try {
				Long StartTime = System.currentTimeMillis();
				Date startTime = new Date();
				List l = this.getSession().createSQLQuery(ruleExpression).list();
				Long timeTaken = System.currentTimeMillis()
						- StartTime;
				Date endTime = new Date();
				System.out.println("Time To Execute ToDoRule, "
						+ todoRule.getRuleNumber() + ", "+ todoRule.getId()+ ", "+todoRule.getCorpID() +", " +startTime+", " +endTime+", "
						+ timeTaken);
				return l;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				message = e.getMessage();
				e.printStackTrace();
				tempErrorList.add(message);
			}
			return tempErrorList;
	}
	//	 For Today's Note
	public List<Notes> findByStatus(String reminderStatus,String corpId,String createdBy) {
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr =dfm.format(filterdate);
		Company company = companyManager.findByCorpID(corpId).get(0);
		Date cal = new Date();
        //logger.warn("system Date : "+cal);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
        String ss =formatter.format(cal);
        //logger.warn("system Date After formate : "+ss);
		//List status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate='"+compareWithDateStr+"'and corpID='"+corpId+"' and (createdBy='"+createdBy+"' or forwardToUser='"+createdBy+"' or followUpFor='"+createdBy+"' or updatedBy='"+createdBy+"')");
        Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
        List status = new ArrayList();
		if(createdBy!=null && !createdBy.equalsIgnoreCase("") &&  !createdBy.equalsIgnoreCase("UNASSIGNED")){
			status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate=date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and followUpFor='"+createdBy+"'");
		}else if(createdBy!=null && createdBy.equalsIgnoreCase("UNASSIGNED")){
			
			status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and  noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate=date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and (followUpFor='' or followUpFor is null )");
		}else{
			
			status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and  noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate=date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' ");
		}
		//status.size();
		return status;
	}
	
//	 For Today's Note for Corp_ID
	public List<Notes> findByStatus(String reminderStatus,String corpId) {
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr = dfm.format(filterdate);
		Company company = companyManager.findByCorpID(corpId).get(0);
		Date cal = new Date();
        //logger.warn("system Date : "+cal);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
        String ss =formatter.format(cal);
        //logger.warn("system Date After formate : "+ss);
        Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
		//List status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate='"+compareWithDateStr+"'and corpID='"+corpId+"'");
        List status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and  noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate=date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"'");
        //status.size();
		return status;
	}

	private java.util.Date getcurrentDate() {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = dfm.format(new Date());
		Date currentDate = null;
		try {
			currentDate = dfm.parse(dateStr);
			
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return currentDate;
	}
//	 For OverDue Note
	public List<Notes> findByStatus1(String reminderStatus,String corpId,String createdBy) {		
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr =dfm.format(filterdate);
		Company company = companyManager.findByCorpID(corpId).get(0);
		Date cal = new Date();
        //logger.warn("system Date : "+cal);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
        String ss =formatter.format(cal);
        logger.warn("system Date After formate : "+ss);
		List status = new ArrayList();
		/*if(createdBy!=null && !createdBy.equalsIgnoreCase("") && !createdBy.equalsIgnoreCase("UNASSIGNED") ){
			status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"' and corpID='"+corpId+"' and followUpFor='"+createdBy+"'");
		}else if(createdBy!=null && createdBy.equalsIgnoreCase("UNASSIGNED")){
			//String temp="select * from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"' and (followUpFor='' or followUpFor is null )";
			status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"' and (followUpFor='' or followUpFor is null ) " );
		}else{
			status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"'");	
		}*/
		Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
		if(createdBy!=null && !createdBy.equalsIgnoreCase("") && !createdBy.equalsIgnoreCase("UNASSIGNED") ){
			status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and followUpFor='"+createdBy+"' and updatedOn>'2017-01-01' ");
		}else if(createdBy!=null && createdBy.equalsIgnoreCase("UNASSIGNED")){
			//String temp="select * from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"' and (followUpFor='' or followUpFor is null )";
			status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and (followUpFor='' or followUpFor is null ) and updatedOn>'2017-01-01' " );
		}else{
			status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and updatedOn>'2017-01-01' ");	
		}
		//List status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"' and corpID='"+corpId+"' and (createdBy='"+createdBy+"' or forwardToUser='"+createdBy+"' or followUpFor='"+createdBy+"' or updatedBy='"+createdBy+"')");
		//status.size();
		//System.out.println("\n\n List Size-->>"+status.size());
		return status;
	}
//	 For Due This Week Note
	public List<Notes> findByStatus2(String reminderStatus,String corpId,String createdBy) {
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr =dfm.format(filterdate);
		Company company = companyManager.findByCorpID(corpId).get(0);
		Date cal = new Date();
        //logger.warn("system Date : "+cal);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
        String ss =formatter.format(cal);
        //logger.warn("system Date After formate : "+ss);
		List status = new ArrayList();
		/*if(createdBy!=null && !createdBy.equalsIgnoreCase("") && !createdBy.equalsIgnoreCase("UNASSIGNED")){
			status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate>'"+compareWithDateStr+"' and corpID='"+corpId+"' and followUpFor='"+createdBy+"'");
		}else if(createdBy!=null && createdBy.equalsIgnoreCase("UNASSIGNED")){
			status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate>'"+compareWithDateStr+"'and corpID='"+corpId+"' and (followUpFor='' or followUpFor is null )");
		}else{
			status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate>'"+compareWithDateStr+"'and corpID='"+corpId+"'");
		}*/
		Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
		if(createdBy!=null && !createdBy.equalsIgnoreCase("") && !createdBy.equalsIgnoreCase("UNASSIGNED")){
			status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate>date_format('"+ss+"','%y-%m-%d')  and corpID='"+corpId+"' and followUpFor='"+createdBy+"'");
		}else if(createdBy!=null && createdBy.equalsIgnoreCase("UNASSIGNED")){
			status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP')  AND reminderStatus='"+reminderStatus+"' and forwarddate>date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and (followUpFor='' or followUpFor is null )");
		}else{
			status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP')  AND reminderStatus='"+reminderStatus+"' and forwarddate>date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"'");
		}
		//List status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate>'"+compareWithDateStr+"' and corpID='"+corpId+"' and (createdBy='"+createdBy+"' or forwardToUser='"+createdBy+"' or followUpFor='"+createdBy+"'  or updatedBy='"+createdBy+"')");
		//status.size();
		return status;
	}
	
	// For OverDue Note by CorpID
	public List<Notes> findByStatus1(String reminderStatus,String corpId) {
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr =dfm.format(filterdate);
		Company company = companyManager.findByCorpID(corpId).get(0);
		Date cal = new Date();
        //logger.warn("system Date : "+cal);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
        String ss =formatter.format(cal);
        Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
        //logger.warn("system Date After formate 1: "+ss);
		/*List status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"' and corpID='"+corpId+"'");*/
        List status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<date_format('"+ss+"','%y-%m-%d')  and corpID='"+corpId+"'  and updatedOn>'2017-01-01'");
		return status;
	}
	
	//	 For Due This Week Note by CorpID
	public List<Notes> findByStatus2(String reminderStatus,String corpId) {
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr =dfm.format(filterdate);
		Company company = companyManager.findByCorpID(corpId).get(0);
		Date cal = new Date();
        //logger.warn("system Date : "+cal);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
        String ss =formatter.format(cal);
        Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
        //logger.warn("system Date After formate 2: "+ss);
		/*List status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate>'"+compareWithDateStr+"' and corpID='"+corpId+"'");*/
        List status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND  reminderStatus='"+reminderStatus+"' and forwarddate>date_format('"+ss+"','%y-%m-%d')  and corpID='"+corpId+"'");
		return status;
	}

	public List<ToDoResult> findByduration(String corpID) {
		 //return getHibernateTemplate().find("from ToDoResult where durationAddSub>'0' and corpID='"+corpID+"' group by id");
		List resultList = new ArrayList();
		String query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid,if(n.note='',n.subject,n.note),tr.billToName,tr.ruleNumber" +
		"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.notetype = 'Activity') and n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"' group by tr.id,tr.ruleNumber";
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
			if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
			if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
			if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
			if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
			if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
			if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
			if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
			if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
			if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
			if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
			if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
			if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
			if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
			if(row[22] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[22]);}
			if(row[23] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[23]);}
			resultList.add(toDoRuleDTO);
		  }
		return resultList;
	}

	public List findByDurationComingUp(String sessionCorpID){
		List resultList = new ArrayList();
		String query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.billToName ,tr.ruleNumber" +
		"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.notetype = 'Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"'";
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
			if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
			if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
			if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
			if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
			if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
			if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
			if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
			if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
			if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
			if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
			if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
			if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
			if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
			if(row[22] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[22]);}
			if(row[23] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[23]);}
			resultList.add(toDoRuleDTO);
		  }
		return resultList;
	}
	
	public List<ToDoResult> findBydurationdueweek(String corpID) {
		return getHibernateTemplate().find("from ToDoResult where durationAddSub<'0' and corpID='"+corpID+"' group by id");
	}

	public List<ToDoResult> findBydurationdtoday(String corpID) {
		//return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' group by id");
		List resultList = new ArrayList();
		String query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.billToName ,tr.ruleNumber" +
		"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement' or n.noteSubtype = 'AgentNotes' ) and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"' group by tr.id,tr.ruleNumber";
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
			if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
			if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
			if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
			if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
			if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
			if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
			if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
			if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
			if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
			if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
			if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
			if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
			if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
			if(row[22] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[22]);}
			if(row[23] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[23]);}
			resultList.add(toDoRuleDTO);
		  }
		return resultList;
	}

	public void deleteAllResults(String sessionCorpID) {
			//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			//User user = (User)auth.getPrincipal();
		 getHibernateTemplate().bulkUpdate("delete from ToDoResult where corpID='"+sessionCorpID+"'");
		
	}

	public void deleteResultsForRecord(String entity, Long recordID) {
		 getHibernateTemplate().bulkUpdate("delete from ToDoResult where resultRecordType = '" + entity + "' and resultRecordId = " + recordID);
		
	}
	
	public void changestatus(String status, Long id) {
		
		getHibernateTemplate().bulkUpdate("update ToDoRule set status='"+status+"' where id=?",id);
		
	}

	public List<DataCatalog> selectfield(String entity, String corpID) {
		return getHibernateTemplate().find("select rulesFiledName from DataCatalog where defineByToDoRule='true' and tableName='"+entity+"' and corpID in ('TSFT','"+corpID+"')");
	}

	public List<DataCatalog> selectDatefield(String corpID) {
		
		return getHibernateTemplate().find("select rulesFiledName from DataCatalog where isdateField='true' and defineByToDoRule='true' and corpID in ('TSFT','" + corpID + "') group by rulesFiledName order by rulesFiledName");
		 
	}

	public List<ToDoResult> findNumberOfResult(String corpID) {
		
		  return getHibernateTemplate().find("select count(*) from ToDoResult where corpID='"+corpID+"'");
		 
	}

	public List findUserForSupervisor(String supervisor, String corpID) {
	
		return getHibernateTemplate().find("select concat('''',username,'''') from User where supervisor='"+supervisor+"' and corpID='"+corpID+"'");
	
	}

	//For Today's Note for Corp_ID & Supervisor
	public List<Notes> findByStatusForSupervisor(String reminderStatus, String corpId, String users) {
		//String user = users.replaceAll("''", "'");
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr = dfm.format(filterdate);
		List status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate='"+compareWithDateStr+"'and corpID='"+corpId+"' and createdBy in ("+users+")");
		//status.size();
		return status;
	}

	//For OverDue Note by CorpID & Supervisor
	public List<Notes> findByStatusForSupervisor1(String reminderStatus, String corpId, String users) {
		//String user = users.replaceAll("'", "");
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr = dfm.format(filterdate);
		List status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"' and createdBy in ("+users+")");
		//status.size();
		return status;
	}
	
	//For Due This Week Note by CorpID & Supervisor
	public List<Notes> findByStatusForSupervisor2(String reminderStatus, String corpId, String users) {
		//String user = users.replaceAll("'", "");
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr = dfm.format(filterdate);
		List status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate>'"+compareWithDateStr+"'and corpID='"+corpId+"' and createdBy in ("+users+")");
		//status.size();
		return status;
	}

	public List<ToDoResult> findBydurationUser(String corpID, String owner, String messageDisplayed, String ruleId, String shipper,User appUser) {
		if(messageDisplayed==null){	messageDisplayed="";}
		if(ruleId==null){ruleId="";}
		if(shipper==null){shipper="";}
	    //return getHibernateTemplate().find("from ToDoResult where durationAddSub>'0' and corpID='"+corpID+"' and owner like '"+owner+"%' and messagedisplayed like '"+messageDisplayed+"%' and todoRuleId like '"+ruleId+"%' group by id");
		List resultList = new ArrayList();
		String query="";
		if(appUser.getUserType().equalsIgnoreCase("AGENT")){
			List partnerCorpIDList = new ArrayList(); 
			List list = new ArrayList();
	        String partnerCodePopup = "";
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal(); 
			Map filterMap=user.getFilterMap();
			if(filterMap.containsKey("agentFilter")){
		    	 HashSet valueSet=	(HashSet)filterMap.get("agentFilter");	
		    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
		    	Iterator iterator = valueSet.iterator();
		    	while (iterator.hasNext()) {
		    		String  mapkey = (String)iterator.next(); 
				    if(filterMap.containsKey(mapkey)){	
					
					HashMap valueMap=	(HashMap)filterMap.get(mapkey);
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					}
				    }}}
					
	    	Iterator listIterator= partnerCorpIDList.iterator();
	    	while (listIterator.hasNext()) {
	    		String code=listIterator.next().toString();
	    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
	    			list.add("'" + code + "'");	
	    		
	    		} 	
	    	} 
	    	//companydivisionSetCode= list.toString().replace("[","").replace("]", "");
	    	} 
			partnerCodePopup = list.toString().replace("[","").replace("]", ""); 
			String parentCode = appUser.getParentAgent();
			if(ruleId.equalsIgnoreCase("")){
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity' ) and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT')  and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'";
					if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
						query=query+"and (tr.owner='' or tr.owner is null) " ;	
					}else{
						query=query+"and tr.rolelist in ('OriginAgent','DestinationAgent','OriginSubAgent','DestinationSubAgent') " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.agentCode in("+partnerCodePopup+") group by tr.id,tr.ruleNumber";	
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity' ) and  n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'" +
							"and tr.rolelist in ('OriginAgent','DestinationAgent','OriginSubAgent','DestinationSubAgent') and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  and tr.agentCode in("+partnerCodePopup+") group by tr.id,tr.ruleNumber";	
				}
			
			}else{
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"' " ;
					if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
						query=query+"and (tr.owner='' or tr.owner is null) " ;	
					}else{
						query=query+"and tr.rolelist in ('OriginAgent','DestinationAgent','OriginSubAgent','DestinationSubAgent') " ;		
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber = '"+ruleId+"%'  and tr.agentCode in("+partnerCodePopup+") group by tr.id,tr.ruleNumber";	
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'" ;
					if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
						query=query+"and (tr.owner='' or tr.owner is null) " ;	
					}else{
						query=query+"and tr.rolelist in ('OriginAgent','DestinationAgent','OriginSubAgent','DestinationSubAgent') " ;		
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber = '"+ruleId+"%'  and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  and tr.agentCode in("+partnerCodePopup+") group by tr.id,tr.ruleNumber";
					}
				}
		}else if(appUser.getUserType().equalsIgnoreCase("PARTNER")){

			String parentCode = appUser.getParentAgent();
			if(ruleId.equalsIgnoreCase("")){
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity' ) and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'" ;
					if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
						query=query+"and (tr.owner='' or tr.owner is null) " ;	
					}else{
						query=query+"and tr.owner ='PartnerPortal' " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.agentCode ='"+parentCode+"' group by tr.id,tr.ruleNumber";	
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity' ) and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'" +
							"and tr.owner ='PartnerPortal' and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  and tr.agentCode ='"+parentCode+"' group by tr.id,tr.ruleNumber";	
				}
			
			}else{
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'" ;
					if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
						query=query+"and (tr.owner='' or tr.owner is null) " ;	
					}else{
						query=query+"and tr.owner ='PartnerPortal' " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber = '"+ruleId+"%'  and tr.agentCode ='"+parentCode+"' group by tr.id,tr.ruleNumber";	
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"' " ;
					if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
						query=query+"and (tr.owner='' or tr.owner is null) " ;	
					}else{
						query=query+"and tr.owner ='PartnerPortal' " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber = '"+ruleId+"%'  and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  and tr.agentCode ='"+parentCode+"' group by tr.id,tr.ruleNumber";
					}
				}
					
		}else{
			//plesse check this one
		if(ruleId.equalsIgnoreCase("")){
			if(shipper.equalsIgnoreCase("")){
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'" ;
				if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
					query=query+"and (tr.owner='' or tr.owner is null) " ;	
				}else if(owner!=null && !owner.equals("")){
					query=query+"and tr.owner = '"+owner+"' and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%'  group by tr.id,tr.ruleNumber";	
			}else{
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"' " ;
				if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
					query=query+"and (tr.owner='' or tr.owner is null) " ;	
				}else if(owner!=null && !owner.equals("")){
					query=query+"and tr.owner = '"+owner+"' and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";	
			}
		
		}else{
			if(shipper.equalsIgnoreCase("")){
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"' " ;
				if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
					query=query+"and (tr.owner='' or tr.owner is null) " ;	
				}else if(owner!=null && !owner.equals("")){
					query=query+"and tr.owner = '"+owner+"' and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+" and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber = '"+ruleId+"%'  group by tr.id,tr.ruleNumber  ";	
			}else{
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'";
				if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
					query=query+"and (tr.owner='' or tr.owner is null) " ;	
				}else if(owner!=null && !owner.equals("")){
					query=query+"and tr.owner = '"+owner+"' and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber = '"+ruleId+"%'  and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";
				}
			}
		}
		
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
			if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
			if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
			if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
			if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
			if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
			if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
			if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
			if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
			if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
			if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
			if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
			if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
			if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
			if(row[22] == null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[22]);}
			if(row[23] == null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);}
			if(row[24] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[24]);}
			if(row[25] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[25]);}
			resultList.add(toDoRuleDTO);
		  }
		return resultList;
	}
 
	public List<ToDoResult> findBydurationdtodayUser(String corpID, String owner, String messageDisplayed, String ruleId, String shipper,User appUser) {
		if(messageDisplayed==null){	messageDisplayed="";}
		if(ruleId==null){ruleId="";}
		if(shipper==null){shipper="";}
		//return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' and owner like '"+owner+"%' and messagedisplayed like '"+messageDisplayed+"%' and todoRuleId like '"+ruleId+"%' group by id");
		List resultList = new ArrayList();
		String query="";
		if(appUser.getUserType().equalsIgnoreCase("AGENT")){
			List partnerCorpIDList = new ArrayList(); 
			List list = new ArrayList();
	        String partnerCodePopup = "";
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal(); 
			Map filterMap=user.getFilterMap();
			if(filterMap.containsKey("agentFilter")){
		    	 HashSet valueSet=	(HashSet)filterMap.get("agentFilter");	
		    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
		    	Iterator iterator = valueSet.iterator();
		    	while (iterator.hasNext()) {
		    		String  mapkey = (String)iterator.next(); 
				    if(filterMap.containsKey(mapkey)){	
					
					HashMap valueMap=	(HashMap)filterMap.get(mapkey);
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					}
				    }}}
					
	    	Iterator listIterator= partnerCorpIDList.iterator();
	    	while (listIterator.hasNext()) {
	    		String code=listIterator.next().toString();
	    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
	    			list.add("'" + code + "'");	
	    		
	    		} 	
	    	} 
	    	//companydivisionSetCode= list.toString().replace("[","").replace("]", "");
	    	} 
			partnerCodePopup = list.toString().replace("[","").replace("]", "");
			String parentCode = appUser.getParentAgent();
			if(ruleId.equalsIgnoreCase("")){
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"'  and tr.rolelist in ('OriginAgent','DestinationAgent','OriginSubAgent','DestinationSubAgent') and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.agentCode in("+partnerCodePopup+") group by tr.id,tr.ruleNumber";
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"' and tr.rolelist in ('OriginAgent','DestinationAgent','OriginSubAgent','DestinationSubAgent') and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%' and tr.agentCode in("+partnerCodePopup+") group by tr.id,tr.ruleNumber";
				}
				
			}else{
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"' and tr.rolelist in ('OriginAgent','DestinationAgent','OriginSubAgent','DestinationSubAgent') and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' and tr.agentCode in("+partnerCodePopup+") group by tr.id,tr.ruleNumber";		
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"' and tr.rolelist in ('OriginAgent','DestinationAgent','OriginSubAgent','DestinationSubAgent') and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%' and tr.agentCode in("+partnerCodePopup+") group by tr.id,tr.ruleNumber";	
					}
				}
		}else if(appUser.getUserType().equalsIgnoreCase("PARTNER")){
			

			String parentCode = appUser.getParentAgent();
			if(ruleId.equalsIgnoreCase("")){
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"'  and tr.owner = 'PartnerPortal' and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.agentCode ='"+parentCode+"' group by tr.id,tr.ruleNumber";
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"' and tr.owner like 'PartnerPortal' and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%' and tr.agentCode ='"+parentCode+"' group by tr.id,tr.ruleNumber";
				}
				
			}else{
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"' and tr.owner like 'PartnerPortal' and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' and tr.agentCode ='"+parentCode+"' group by tr.id,tr.ruleNumber";		
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"' and tr.owner like 'PartnerPortal' and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%' and tr.agentCode ='"+parentCode+"' group by tr.id,tr.ruleNumber";	
					}
				}
					
		}else{
		if(ruleId.equalsIgnoreCase("")){
			if(shipper.equalsIgnoreCase("")){
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"' " ;
				if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
					query=query+"and (tr.owner='' or tr.owner is null) " ;	
				}else if(owner!=null && !owner.equals("")){
					query=query+"and tr.owner = '"+owner+"' and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%'  group by tr.id,tr.ruleNumber";
			}else{
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName ,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"' " ;
				if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
					query=query+"and (tr.owner='' or tr.owner is null) " ;	
				}else if(owner!=null && !owner.equals("")){
					query=query+"and tr.owner = '"+owner+"' and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";
			}
			
		}else{
			if(shipper.equalsIgnoreCase("")){
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"' " ;
				if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
					query=query+"and (tr.owner='' or tr.owner is null) " ;	
				}else if(owner!=null && !owner.equals("")){
					query=query+"and tr.owner = '"+owner+"' and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%'  group by tr.id,tr.ruleNumber";		
			}else{
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"' " ;
				if(owner!=null && owner.equalsIgnoreCase("UNASSIGNED")){
					query=query+"and (tr.owner='' or tr.owner is null) " ;	
				}else if(owner!=null && !owner.equals("")){
					query=query+"and tr.owner = '"+owner+"' and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
				    query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+" and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";	
			}
			
			}
		}
		
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
			if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
			if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
			if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
			if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
			if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
			if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
			if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
			if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
			if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
			if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
			if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
			if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
			if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
			if(row[22] == null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[22]);}
			if(row[23] == null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);}
			if(row[24] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[24]);}
			if(row[25] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[25]);}
			resultList.add(toDoRuleDTO);
		  }
		return resultList;
	}

    public List findByDurationComingUpUser(String sessionCorpID, String ownerName, String messageDisplayed, String ruleId, String shipperName, User appUser){
    	if(messageDisplayed==null){	messageDisplayed="";}
		if(ruleId==null){ruleId="";}
		if(shipperName==null){shipperName="";}
		//return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' and owner like '"+owner+"%' and messagedisplayed like '"+messageDisplayed+"%' and todoRuleId like '"+ruleId+"%' group by id");
		List resultList = new ArrayList();
		String query="";
		if(appUser.getUserType().equalsIgnoreCase("AGENT")){
			List partnerCorpIDList = new ArrayList(); 
			List list = new ArrayList();
	        String partnerCodePopup = "";
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal(); 
			Map filterMap=user.getFilterMap();
			if(filterMap.containsKey("agentFilter")){
		    	 HashSet valueSet=	(HashSet)filterMap.get("agentFilter");	
		    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
		    	Iterator iterator = valueSet.iterator();
		    	while (iterator.hasNext()) {
		    		String  mapkey = (String)iterator.next(); 
				    if(filterMap.containsKey(mapkey)){	
					
					HashMap valueMap=	(HashMap)filterMap.get(mapkey);
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					}
				    }}}
					
	    	Iterator listIterator= partnerCorpIDList.iterator();
	    	while (listIterator.hasNext()) {
	    		String code=listIterator.next().toString();
	    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
	    			list.add("'" + code + "'");	
	    		
	    		} 	
	    	} 
	    	//companydivisionSetCode= list.toString().replace("[","").replace("]", "");
	    	} 
			partnerCodePopup = list.toString().replace("[","").replace("]", "");
			String parentCode = appUser.getParentAgent();
			if(ruleId.equalsIgnoreCase("")){
				if(shipperName.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"' and tr.rolelist in ('OriginAgent','DestinationAgent','OriginSubAgent','DestinationSubAgent') and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.agentCode in("+partnerCodePopup+")group by tr.id" ;
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"' and tr.rolelist in ('OriginAgent','DestinationAgent','OriginSubAgent','DestinationSubAgent') and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipperName.replaceAll("'", "\\\\'")+"%' and tr.agentCode in("+partnerCodePopup+") group by tr.id";
				}
				
			}else{
				if(shipperName.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"' and tr.rolelist in ('OriginAgent','DestinationAgent','OriginSubAgent','DestinationSubAgent') and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' and tr.agentCode in("+partnerCodePopup+") group by tr.id";		
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"' and tr.rolelist in ('OriginAgent','DestinationAgent','OriginSubAgent','DestinationSubAgent') and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' and shipper like '"+shipperName.replaceAll("'", "\\\\'")+"%' and tr.agentCode in("+partnerCodePopup+") group by tr.id";	
				}
				
			}
		}else if(appUser.getUserType().equalsIgnoreCase("PARTNER")){

			String parentCode = appUser.getParentAgent();
			if(ruleId.equalsIgnoreCase("")){
				if(shipperName.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"' and tr.owner = 'PartnerPortal' and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.agentCode ='"+parentCode+"' group by tr.id" ;
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"' and tr.owner = 'PartnerPortal' and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipperName.replaceAll("'", "\\\\'")+"%' and tr.agentCode ='"+parentCode+"' group by tr.id";
				}
				
			}else{
				if(shipperName.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"' and tr.owner = 'PartnerPortal' and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' and tr.agentCode ='"+parentCode+"' group by tr.id";		
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"' and tr.owner = 'PartnerPortal' and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' and shipper like '"+shipperName.replaceAll("'", "\\\\'")+"%' and tr.agentCode ='"+parentCode+"' group by tr.id";	
				}
				
			}
					
		}else{
		if(ruleId.equalsIgnoreCase("")){
			if(shipperName.equalsIgnoreCase("")){
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"' and tr.owner like '"+ownerName+"%' and tr.messagedisplayed like '"+messageDisplayed+"%' group by tr.id" ;
			}else{
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"' and tr.owner like '"+ownerName+"%' and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipperName.replaceAll("'", "\\\\'")+"%' group by tr.id";
			}
			
		}else{
			if(shipperName.equalsIgnoreCase("")){
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"' and tr.owner like '"+ownerName+"%' and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' group by tr.id";		
			}else{
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') where tr.durationAddSub<'0' and tr.corpID='"+sessionCorpID+"' and tr.owner like '"+ownerName+"%' and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' and shipper like '"+shipperName.replaceAll("'", "\\\\'")+"%' group by tr.id";	
			}
			
			}
		}
		
		
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
			if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
			if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
			if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
			if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
			if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
			if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
			if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
			if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
			if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
			if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
			if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
			if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
			if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
			if(row[22] == null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[22]);}
			if(row[23] == null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);}
			if(row[24] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[24]);}
			if(row[25] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[25]);}
			resultList.add(toDoRuleDTO);
		  }
		return resultList;
	}
	
	public List<ToDoResult> findBydurationdueweekUser(String corpID, String owner) {
		return getHibernateTemplate().find("from ToDoResult where durationAddSub<'0' and corpID='"+corpID+"' and owner ='"+owner+"' group by id");
	}

	public List<ToDoResult> findBydurationForSupervisor(String corpID, String owner) {
		return getHibernateTemplate().find("from ToDoResult where durationAddSub>'0' and corpID='"+corpID+"' and owner in ("+owner+") group by id");
	}

	public List<ToDoResult> findBydurationdtodayForSupervisor(String corpID, String owner) {
		return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' and owner in ("+owner+") group by id");
	}

	public List<ToDoResult> findBydurationdueweekForSupervisor(String corpID, String owner) {
		return getHibernateTemplate().find("from ToDoResult where durationAddSub<'0' and corpID='"+corpID+"' and owner in ("+owner+") group by id");
	}

	public List checkSupervisor(String username) {
		return getHibernateTemplate().find("select username from User where supervisor='"+username+"'");
	}

	public void updateCompanyDivisionLastRunDate(String corpID) {
		getHibernateTemplate().bulkUpdate("update SystemDefault set lastRunDate=now() where corpID='"+corpID+"'");
		
	}
	public String getNetworkAgentType(String networkPartnerCode,String sessionCorpID){
		String chk = "" ;
		List l1  = this.getSession().createSQLQuery("select if(ugwwNetworkGroup,'T','F') from partner where partnercode='"+networkPartnerCode+"' and corpid in ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"')  ").list();
		if(l1!=null && !l1.isEmpty() && l1.get(0)!=null){
			chk = l1.get(0).toString().trim();
		}
		return chk;
	}
	public List getTimeToExecute(String corpID) {
		return getHibernateTemplate().find("select toDoRuleExecutionTime from SystemDefault where corpid='"+corpID+"' and toDoRuleExecutionTime between CURTIME() and ADDTIME(CURTIME(),'00:20')");
	}
	public List getFilterName(String sessionCorpID ,Long userId){
		String queryfilter="";
		queryfilter="select dss.name from datasecurityset dss,datasecuritypermission dsp,datasecurityfilter dsf,userdatasecurity uds,app_user ap where dss.id=dsp.datasecurityset_id and dsf.id=dsp.datasecurityfilter_id and uds.userdatasecurity_id=dss.id and ap.id = uds.user_id  and ap.corpID='"+sessionCorpID+"' and ap.id='"+userId+"'";
	List tempuserfilter = this.getSession().createSQLQuery(queryfilter).list();
	return 	tempuserfilter;
	}
	public List getOwnersList(String sessionCorpID ,String username,Long userId,String userFirstName,String userType) {
		List userList=new ArrayList();
		List tempuserfilter=new ArrayList();
		if(userType.equalsIgnoreCase("USER")){
			String queryfilter="";
			queryfilter="select dss.name from datasecurityset dss,datasecuritypermission dsp,datasecurityfilter dsf,userdatasecurity uds,app_user ap where dss.id=dsp.datasecurityset_id and dsf.id=dsp.datasecurityfilter_id and uds.userdatasecurity_id=dss.id and ap.id = uds.user_id  and ap.corpID='"+sessionCorpID+"' and ap.id='"+userId+"'";
		 tempuserfilter = this.getSession().createSQLQuery(queryfilter).list();
		 if( tempuserfilter!=null && !tempuserfilter.isEmpty()){
			 userList.add(userFirstName);
		}else{
			String query="";
			if(sessionCorpID.equals("TSFT")){
				query = "select distinct t.owner from todoresult t ,app_user a where t.corpID='"+sessionCorpID+"' and t.agentCode=a.parentAgent and a.username='"+username+"' and t.owner<>'' and t.owner is not null group by t.owner ";
			}else{
				query = "select distinct owner from todoresult where corpID='"+sessionCorpID+"' and owner<>'' and  owner is not null union select distinct UPPER(owner) from checklistresult where corpID='"+sessionCorpID+"' and owner<>'' union select distinct followUpFor as owner from notes where id>(select id from notes_start_id) and corpID='"+sessionCorpID+"' and (noteStatus in('NEW','FWD','PEND') or noteStatus is null) and followUpFor is not null and followUpFor<>'' and reminderstatus = 'NEW' and forwardDate is not null group by owner order by owner";
			}
			List tempuserList = this.getSession().createSQLQuery(query).list();
			userList.add("UNASSIGNED");
			Iterator it=tempuserList.iterator();
			while(it.hasNext()){
			Object value=it.next();
		    userList.add(value);
			}
			}	
		}else{
		String query="";
		if(sessionCorpID.equals("TSFT")){
			query = "select distinct t.owner from todoresult t ,app_user a where t.corpID='"+sessionCorpID+"' and t.agentCode=a.parentAgent and a.username='"+username+"' and t.owner<>'' and t.owner is not null  group by t.owner ";
		}else{
			if(userType.equalsIgnoreCase("PARTNER")){
			query = "select distinct t.owner from todoresult t ,app_user a where t.corpID='"+sessionCorpID+"' and t.agentCode=a.parentAgent and a.username='"+username+"' and t.owner<>'' and t.owner is not null  group by t.owner ";				
			}else{
			query = "select distinct owner from todoresult where corpID='"+sessionCorpID+"' and owner<>'' and owner is not null union select distinct followUpFor as owner from notes where corpID='"+sessionCorpID+"' and (noteStatus in('NEW','FWD','PEND') or noteStatus is null) and followUpFor is not null and followUpFor<>'' and reminderstatus = 'NEW' union select distinct alertUser as owner from history where corpID='"+sessionCorpID+"' and alertUser!='' and alertUser is not null and isViewed is false group by owner order by owner";
			}
		}
		List tempuserList = this.getSession().createSQLQuery(query).list();
		userList.add("UNASSIGNED");
		Iterator it=tempuserList.iterator();
		while(it.hasNext()){
		Object value=it.next();
	    userList.add(value);
		}
		}
		return 	userList;
		//return getHibernateTemplate().find(query);
	}
   public void setViewFieldOfHistory(Long historyId){
 		 getHibernateTemplate().bulkUpdate("update AuditTrail set isViewed=true where id='"+historyId+"'");	   
   }
	public void bulkUpdateViewFieldOfHistory(String idList){
		getHibernateTemplate().bulkUpdate("update AuditTrail set isViewed=true where id in ("+idList+")");
	}
	public List getOwnerResult(String owner, String sessionCorpID) {
		return getHibernateTemplate().find("from ToDoResult where owner='"+owner+"' and corpID='"+sessionCorpID+"' order by durationAddSub");
	}

	public List findResultByOwnerOverDue(String sessionCorpID, String owner) {
		return getHibernateTemplate().find("");
	}

	public void deleteThisResults(Long id, String sessionCorpID,String role) {
		String corpID=sessionCorpID;
		if(role.equalsIgnoreCase("DestinationAgent") || role.equalsIgnoreCase("DestinationSubAgent") || role.equalsIgnoreCase("OriginAgent") || role.equalsIgnoreCase("OriginSubAgent") ){
			corpID="TSFT";
		}
		Query query = getSession().createSQLQuery("delete from todoresult where corpID='"+corpID+"' and todoRuleId='"+id+"'");	
		query.executeUpdate();
		//getHibernateTemplate().bulkUpdate("delete from ToDoResult where corpID='"+corpID+"' and todoRuleId='"+id+"'");	
	}

	public List<ToDoRule> getThisRule(Long id) {
		return getHibernateTemplate().find("from ToDoRule where id='"+id+"'");
	}

	public List findNumberOfResultEntered(String sessionCorpID, Long id, String role){
		String corpID=sessionCorpID;
		if(role.equalsIgnoreCase("DestinationAgent") || role.equalsIgnoreCase("DestinationSubAgent") || role.equalsIgnoreCase("OriginAgent") || role.equalsIgnoreCase("OriginSubAgent") ){
			corpID="TSFT";
		}
		return this.getSession().createSQLQuery("select count(*) from todoresult where corpID='"+corpID+"' and todoRuleId='"+id+"'").list();
	}

	public void updateUserCheck(String recordId, String userName, String fieldToValidate) {
		getHibernateTemplate().bulkUpdate("update ToDoResult set checkedUserName=UPPER('"+userName+"') where resultRecordId='"+recordId+"' and fieldToValidate1='"+fieldToValidate+"'");	
	}
	
	public void updateUserCheckForCheck(String recordId, String userName){
		getHibernateTemplate().bulkUpdate("update CheckListResult set checkedUserName=UPPER('"+userName+"') where resultId='"+recordId+"' ");
	}

	public List  getByCorpID(String sessionCorpID, Boolean isAgentTdr) {
		if(isAgentTdr){
		return getHibernateTemplate().find("from ToDoRule where corpID in ('TSFT','"+sessionCorpID+"') and (docType is null or docType='') and publishRule=true") ;
		}else{
			return getHibernateTemplate().find("from ToDoRule where corpID in ('TSFT','"+sessionCorpID+"') and (docType is null or docType='') and publishRule=false") ;	
		}
		}


	public List  getDocCheckListByCorpID(String sessionCorpID){
		return getHibernateTemplate().find("from ToDoRule where corpID in ('TSFT','"+sessionCorpID+"') and docType is not null and docType!='' ") ;
	}
	
	public List getDescription(String tableName, String fieldname, String sessionCorpID) {
		return getHibernateTemplate().find("select description from DataCatalog where tableName='"+tableName+"' and fieldName='"+fieldname+"' and corpID='"+sessionCorpID+"'");
	}

	public List getTimeToExecute2(String corpID) {
		return getHibernateTemplate().find("select toDoRuleExecutionTime2 from SystemDefault where corpid='"+corpID+"' and toDoRuleExecutionTime2 between CURTIME() and ADDTIME(CURTIME(),'00:20')");
	}

	public List getMessageList(String sessionCorpID, String userName,Long userId,String userType) {
		List ls = new ArrayList();
		List lsCheckList= new ArrayList();
		List tempuserfilter=new ArrayList();
		if(userType.equalsIgnoreCase("USER")){
			String queryfilter="";
			queryfilter="select dss.name from datasecurityset dss,datasecuritypermission dsp,datasecurityfilter dsf,userdatasecurity uds,app_user ap where dss.id=dsp.datasecurityset_id and dsf.id=dsp.datasecurityfilter_id and uds.userdatasecurity_id=dss.id and ap.id = uds.user_id  and ap.corpID='"+sessionCorpID+"' and ap.id='"+userId+"'";
			 tempuserfilter = this.getSession().createSQLQuery(queryfilter).list();
			 if( tempuserfilter!=null && !tempuserfilter.isEmpty()){
				 ls=this.getSession().createSQLQuery("select distinct messagedisplayed from todoresult where corpID='"+sessionCorpID+"' and owner='"+userName+"' union select distinct messagedisplayed from checklistresult where corpID='"+sessionCorpID+"' and owner='"+userName+"' group by messagedisplayed").list();
				}else{
					if(sessionCorpID.equalsIgnoreCase("TSFT")){
						ls=this.getSession().createSQLQuery("select t.messagedisplayed as messagedisplayed from todoresult t,app_user a where t.corpID='"+sessionCorpID+"' and t.agentCode=a.parentAgent and a.username='"+userName+"' group by messagedisplayed").list();
					}else{
						ls=this.getSession().createSQLQuery("select distinct messagedisplayed from todoresult where corpID='"+sessionCorpID+"' union select distinct messagedisplayed from checklistresult where corpID='"+sessionCorpID+"' group by messagedisplayed").list();
						/*lsCheckList=getHibernateTemplate().find("select messageDisplayed from CheckListResult where corpID='"+sessionCorpID+"' group by messagedisplayed");
					   ls.addAll(lsCheckList);*/
					}	
				}
		}else{
		if(sessionCorpID.equalsIgnoreCase("TSFT")){
			ls=this.getSession().createSQLQuery("select t.messagedisplayed as messagedisplayed from todoresult t,app_user a where t.corpID='"+sessionCorpID+"' and t.agentCode=a.parentAgent and a.username='"+userName+"' group by messagedisplayed").list();
		}else{
			if(userType.equalsIgnoreCase("PARTNER")){
			ls=this.getSession().createSQLQuery("select t.messagedisplayed as messagedisplayed from todoresult t,app_user a where t.corpID='"+sessionCorpID+"' and t.agentCode=a.parentAgent and a.username='"+userName+"' group by messagedisplayed").list();				
			}else{
			ls= getHibernateTemplate().find("select messagedisplayed from ToDoResult where corpID='"+sessionCorpID+"' group by messagedisplayed");
			/*lsCheckList=getHibernateTemplate().find("select messageDisplayed from CheckListResult where corpID='"+sessionCorpID+"' group by messagedisplayed");
			ls.addAll(lsCheckList);*/
			}
		}
		}
		return ls;
	}

	public List findMessageOverDue(String sessionCorpID, String messageDisplayed, String userName) {
		return getHibernateTemplate().find("from ToDoResult where durationAddSub>'0' and corpID='"+sessionCorpID+"' and messagedisplayed='"+messageDisplayed+"'");
	}

	public List findMessageToday(String sessionCorpID, String messageDisplayed, String userName) {
		return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+sessionCorpID+"' and messagedisplayed='"+messageDisplayed+"'");
	}

	public List getRuleIdList(String sessionCorpID ,String username,Long userId,String userType) {
		List ls = new ArrayList();
		List tempuserfilter=new ArrayList();
		if(userType.equalsIgnoreCase("USER")){
			String queryfilter="";
			queryfilter="select dss.name from datasecurityset dss,datasecuritypermission dsp,datasecurityfilter dsf,userdatasecurity uds,app_user ap where dss.id=dsp.datasecurityset_id and dsf.id=dsp.datasecurityfilter_id and uds.userdatasecurity_id=dss.id and ap.id = uds.user_id  and ap.corpID='"+sessionCorpID+"' and ap.id='"+userId+"'";
		 tempuserfilter = this.getSession().createSQLQuery(queryfilter).list();
		 if( tempuserfilter!=null && !tempuserfilter.isEmpty()){
			 ls=this.getSession().createSQLQuery("select ruleNumber from todoresult where corpID='"+sessionCorpID+"' and owner='"+username+"' group by ruleNumber union select Cast(ruleNumber as DECIMAL) from checklistresult where corpID='"+sessionCorpID+"' and owner='"+username+"' group by ruleNumber order by ruleNumber").list();
			}else{
				if(sessionCorpID.equalsIgnoreCase("TSFT")){
					ls=this.getSession().createSQLQuery("select t.ruleNumber as ruleNumber from todoresult t,app_user a where t.corpID='"+sessionCorpID+"' and t.agentCode=a.parentAgent and a.username='"+username+"' group by ruleNumber").list();
				}else{
					ls=this.getSession().createSQLQuery("select ruleNumber from todoresult where corpID='"+sessionCorpID+"' group by ruleNumber union select Cast(ruleNumber as DECIMAL) from checklistresult where corpID='"+sessionCorpID+"' group by ruleNumber order by ruleNumber").list();
				}
			}
		}else{
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
				ls=this.getSession().createSQLQuery("select t.ruleNumber as ruleNumber from todoresult t,app_user a where t.corpID='"+sessionCorpID+"' and t.agentCode=a.parentAgent and a.username='"+username+"' group by ruleNumber").list();
			}else{
				if(userType.equalsIgnoreCase("PARTNER")){
					ls=this.getSession().createSQLQuery("select t.ruleNumber as ruleNumber from todoresult t,app_user a where t.corpID='"+sessionCorpID+"' and t.agentCode=a.parentAgent and a.username='"+username+"' group by ruleNumber").list();	
				}else{
					ls=this.getSession().createSQLQuery("select ruleNumber from todoresult where corpID='"+sessionCorpID+"' group by ruleNumber union select Cast(ruleNumber as DECIMAL) from checklistresult where corpID='"+sessionCorpID+"' group by ruleNumber order by ruleNumber").list();
				}
			}
		}
		return ls;
	}

	public List findIDOverDue(String sessionCorpID, String ruleId, String username) {
		return getHibernateTemplate().find("from ToDoResult where durationAddSub>'0' and corpID='"+sessionCorpID+"' and todoRuleId='"+ruleId+"'");
	}
	public List <ServiceOrder> getServiceOrderByCFId(Long cid){
		return  getHibernateTemplate().find("from ServiceOrder where customerFileId='"+cid+"'");
	}

	public List findIdToday(String sessionCorpID, String ruleId, String username) {
		return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+sessionCorpID+"' and todoRuleId='"+ruleId+"'");
	}
	
	public  class ToDoRuleDTO
	{
		private Object id;
		private Object todoRuleId;
		private Object resultRecordId;
		private Object resultRecordType;
		private Object owner;
		
		private Object rolelist;
		private Object messagedisplayed;
		private Object testdate;
		private Object fieldToValidate1;
		private Object fieldToValidate2;
		private Object durationAddSub;
		private Object duration;
		private Object corpID;	
		private Object url;
		private Object shipper;
		private Object checkedUserName;
		private Object supportingId;
		private Object fieldDisplay;
		private Object fileNumber;
		private Object isNotesAdded;
		
		private Object noteType;
		private Object noteSubType;
		
		private Object note;
		private Object noteId;
		
		private Object current;
		private Object overDue;
		private Object total;
		
		private Object ruleNumber;
		private Object emailNotification;
		private Object billToName;
		private Object AgentName;
		//private Object emailAddress;
		
		public Object getCheckedUserName() {
			return checkedUserName;
		}
		public void setCheckedUserName(Object checkedUserName) {
			this.checkedUserName = checkedUserName;
		}
		public Object getCorpID() {
			return corpID;
		}
		public void setCorpID(Object corpID) {
			this.corpID = corpID;
		}
		public Object getDuration() {
			return duration;
		}
		public void setDuration(Object duration) {
			this.duration = duration;
		}
		public Object getDurationAddSub() {
			return durationAddSub;
		}
		public void setDurationAddSub(Object durationAddSub) {
			this.durationAddSub = durationAddSub;
		}
		public Object getFieldDisplay() {
			return fieldDisplay;
		}
		public void setFieldDisplay(Object fieldDisplay) {
			this.fieldDisplay = fieldDisplay;
		}
		public Object getFieldToValidate1() {
			return fieldToValidate1;
		}
		public void setFieldToValidate1(Object fieldToValidate1) {
			this.fieldToValidate1 = fieldToValidate1;
		}
		public Object getFileNumber() {
			return fileNumber;
		}
		public void setFileNumber(Object fileNumber) {
			this.fileNumber = fileNumber;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getIsNotesAdded() {
			return isNotesAdded;
		}
		public void setIsNotesAdded(Object isNotesAdded) {
			this.isNotesAdded = isNotesAdded;
		}
		public Object getMessagedisplayed() {
			return messagedisplayed;
		}
		public void setMessagedisplayed(Object messagedisplayed) {
			this.messagedisplayed = messagedisplayed;
		}
		public Object getNote() {
			return note;
		}
		public void setNote(Object note) {
			this.note = note;
		}
		public Object getNoteId() {
			return noteId;
		}
		public void setNoteId(Object noteId) {
			this.noteId = noteId;
		}
		public Object getNoteSubType() {
			return noteSubType;
		}
		public void setNoteSubType(Object noteSubType) {
			this.noteSubType = noteSubType;
		}
		public Object getNoteType() {
			return noteType;
		}
		public void setNoteType(Object noteType) {
			this.noteType = noteType;
		}
		public Object getOwner() {
			return owner;
		}
		public void setOwner(Object owner) {
			this.owner = owner;
		}
		public Object getResultRecordId() {
			return resultRecordId;
		}
		public void setResultRecordId(Object resultRecordId) {
			this.resultRecordId = resultRecordId;
		}
		public Object getResultRecordType() {
			return resultRecordType;
		}
		public void setResultRecordType(Object resultRecordType) {
			this.resultRecordType = resultRecordType;
		}
		public Object getRolelist() {
			return rolelist;
		}
		public void setRolelist(Object rolelist) {
			this.rolelist = rolelist;
		}
		public Object getShipper() {
			return shipper;
		}
		public void setShipper(Object shipper) {
			this.shipper = shipper;
		}
		public Object getSupportingId() {
			return supportingId;
		}
		public void setSupportingId(Object supportingId) {
			this.supportingId = supportingId;
		}
		public Object getTestdate() {
			return testdate;
		}
		public void setTestdate(Object testdate) {
			this.testdate = testdate;
		}
		public Object getTodoRuleId() {
			return todoRuleId;
		}
		public void setTodoRuleId(Object todoRuleId) {
			this.todoRuleId = todoRuleId;
		}
		public Object getUrl() {
			return url;
		}
		public void setUrl(Object url) {
			this.url = url;
		}
		public Object getFieldToValidate2() {
			return fieldToValidate2;
		}
		public void setFieldToValidate2(Object fieldToValidate2) {
			this.fieldToValidate2 = fieldToValidate2;
		}
		public Object getCurrent() {
			return current;
		}
		public void setCurrent(Object current) {
			this.current = current;
		}
		public Object getOverDue() {
			return overDue;
		}
		public void setOverDue(Object overDue) {
			this.overDue = overDue;
		}
		public Object getTotal() {
			return total;
		}
		public void setTotal(Object total) {
			this.total = total;
		}
		public Object getRuleNumber() {
			return ruleNumber;
		}
		public void setRuleNumber(Object ruleNumber) {
			this.ruleNumber = ruleNumber;
		}
		public Object getEmailNotification() {
			return emailNotification;
		}
		public void setEmailNotification(Object emailNotification) {
			this.emailNotification = emailNotification;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime
					* result
					+ ((checkedUserName == null) ? 0 : checkedUserName
							.hashCode());
			result = prime
					* result
					+ ((durationAddSub == null) ? 0 : durationAddSub.hashCode());
			result = prime * result
					+ ((fieldDisplay == null) ? 0 : fieldDisplay.hashCode());
			result = prime * result
					+ ((fileNumber == null) ? 0 : fileNumber.hashCode());
			result = prime
					* result
					+ ((messagedisplayed == null) ? 0 : messagedisplayed
							.hashCode());
			result = prime * result + ((note == null) ? 0 : note.hashCode());
			result = prime * result + ((owner == null) ? 0 : owner.hashCode());
			result = prime * result
					+ ((rolelist == null) ? 0 : rolelist.hashCode());
			result = prime * result
					+ ((ruleNumber == null) ? 0 : ruleNumber.hashCode());
			result = prime * result
					+ ((shipper == null) ? 0 : shipper.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ToDoRuleDTO other = (ToDoRuleDTO) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (checkedUserName == null) {
				if (other.checkedUserName != null)
					return false;
			} else if (!checkedUserName.equals(other.checkedUserName))
				return false;
			if (durationAddSub == null) {
				if (other.durationAddSub != null)
					return false;
			} else if (!durationAddSub.equals(other.durationAddSub))
				return false;
			if (fieldDisplay == null) {
				if (other.fieldDisplay != null)
					return false;
			} else if (!fieldDisplay.equals(other.fieldDisplay))
				return false;
			if (fileNumber == null) {
				if (other.fileNumber != null)
					return false;
			} else if (!fileNumber.equals(other.fileNumber))
				return false;
			if (messagedisplayed == null) {
				if (other.messagedisplayed != null)
					return false;
			} else if (!messagedisplayed.equals(other.messagedisplayed))
				return false;
			if (note == null) {
				if (other.note != null)
					return false;
			} else if (!note.equals(other.note))
				return false;
			if (owner == null) {
				if (other.owner != null)
					return false;
			} else if (!owner.equals(other.owner))
				return false;
			if (rolelist == null) {
				if (other.rolelist != null)
					return false;
			} else if (!rolelist.equals(other.rolelist))
				return false;
			if (ruleNumber == null) {
				if (other.ruleNumber != null)
					return false;
			} else if (!ruleNumber.equals(other.ruleNumber))
				return false;
			if (shipper == null) {
				if (other.shipper != null)
					return false;
			} else if (!shipper.equals(other.shipper))
				return false;
			return true;
		}
		private ToDoRuleDaoHibernate getOuterType() {
			return ToDoRuleDaoHibernate.this;
		}
		public Object getBillToName() {
			return billToName;
		}
		public void setBillToName(Object billToName) {
			this.billToName = billToName;
		}
		public Object getAgentName() {
			return AgentName;
		}
		public void setAgentName(Object agentName) {
			AgentName = agentName;
		}
	}

	public List getToDoRulesPersonSummary(String sessionCorpID,String username,Long userId,String userType) {
		
		List resultList = new ArrayList();
		if(userType.equalsIgnoreCase("USER")){
			String queryfilter="";
			queryfilter="select dss.name from datasecurityset dss,datasecuritypermission dsp,datasecurityfilter dsf,userdatasecurity uds,app_user ap where dss.id=dsp.datasecurityset_id and dsf.id=dsp.datasecurityfilter_id and uds.userdatasecurity_id=dss.id and ap.id = uds.user_id  and ap.corpID='"+sessionCorpID+"' and ap.id='"+userId+"'";
		    List tempuserfilter = this.getSession().createSQLQuery(queryfilter).list();
		    if( tempuserfilter!=null && !tempuserfilter.isEmpty()){
		    	//resultList.add("");
			}else{
				String query="";
				if(sessionCorpID.equals("TSFT")){
					query="select t.owner, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
					"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total' ,t.messagedisplayed " +
					"from todoresult t ,app_user a where t.corpID = '"+sessionCorpID+"' and a.username='"+username+"' and t.agentCode=a.parentAgent group by t.owner order by 4 desc ";
				}
				else{
				query="select owner, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
						"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total' ,messagedisplayed " +
						"from todoresult where corpID = '"+sessionCorpID+"' group by owner order by 4 desc ";
				}
				List list= this.getSession().createSQLQuery(query).list();
				Iterator it=list.iterator();
				while(it.hasNext())
				  {
					Object [] row=(Object[])it.next(); 
					ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
					if(row[0] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[0]);}
					if(row[1] == null){toDoRuleDTO.setCurrent("");}else{toDoRuleDTO.setCurrent(row[1]);}
					if(row[2] == null){toDoRuleDTO.setOverDue("");}else{toDoRuleDTO.setOverDue(row[2]);}
					if(row[3] == null){toDoRuleDTO.setTotal("");}else{toDoRuleDTO.setTotal(row[3]);}
					if(row[4] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[4]);}
					resultList.add(toDoRuleDTO);
				  }		
			}
		   }else{
			String query="";
			if(sessionCorpID.equals("TSFT")){
				query="select t.owner, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
				"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total' ,t.messagedisplayed " +
				"from todoresult t ,app_user a where t.corpID = '"+sessionCorpID+"' and a.username='"+username+"' and t.agentCode=a.parentAgent group by t.owner order by 4 desc ";
			}
			else{
				if(userType.equalsIgnoreCase("PARTNER")){
					query="select t.owner, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
					"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total' ,t.messagedisplayed " +
					"from todoresult t ,app_user a where t.corpID = '"+sessionCorpID+"' and a.username='"+username+"' and t.agentCode=a.parentAgent group by t.owner order by 4 desc ";
					
				}else{
					query="select owner, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
					"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total' ,messagedisplayed " +
					"from todoresult where corpID = '"+sessionCorpID+"' group by owner order by 4 desc ";
				}
			}
			List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext())
			  {
				Object [] row=(Object[])it.next(); 
				ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
				if(row[0] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[0]);}
				if(row[1] == null){toDoRuleDTO.setCurrent("");}else{toDoRuleDTO.setCurrent(row[1]);}
				if(row[2] == null){toDoRuleDTO.setOverDue("");}else{toDoRuleDTO.setOverDue(row[2]);}
				if(row[3] == null){toDoRuleDTO.setTotal("");}else{toDoRuleDTO.setTotal(row[3]);}
				if(row[4] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[4]);}
				resultList.add(toDoRuleDTO);
			  }	
		}
		return resultList;
	}

	public List getToDoRulesSummary(String sessionCorpID,String username,Long userId,String userType) {
		List resultList = new ArrayList();
		if(userType.equalsIgnoreCase("USER")){
			String queryfilter="";
			queryfilter="select dss.name from datasecurityset dss,datasecuritypermission dsp,datasecurityfilter dsf,userdatasecurity uds,app_user ap where dss.id=dsp.datasecurityset_id and dsf.id=dsp.datasecurityfilter_id and uds.userdatasecurity_id=dss.id and ap.id = uds.user_id  and ap.corpID='"+sessionCorpID+"' and ap.id='"+userId+"'";
		    List tempuserfilter = this.getSession().createSQLQuery(queryfilter).list();
		    if( tempuserfilter!=null && !tempuserfilter.isEmpty()){
		    	//resultList.add("");
			}else{
				String query="";
				if(sessionCorpID.equals("TSFT")){
					query="select t.todoRuleId, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
					"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',t.messagedisplayed, t.ruleNumber  " +
					"from todoresult t ,app_user a where t.corpID = '"+sessionCorpID+"' and t.owner is not null and a.username='"+username+"' and t.agentCode=a.parentAgent " +
					" group by t.todoRuleId order by 4 desc ";
				}else{
						query="select todoRuleId, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
						"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',messagedisplayed, ruleNumber  " +
						"from todoresult where corpID = '"+sessionCorpID+"' and owner is not null group by todoRuleId order by 4 desc ";
				}
				List list= this.getSession().createSQLQuery(query).list();
				Iterator it=list.iterator();
				while(it.hasNext())
				  {
					Object [] row=(Object[])it.next(); 
					ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
					if(row[0] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[0]);}
					if(row[1] == null){toDoRuleDTO.setCurrent("");}else{toDoRuleDTO.setCurrent(row[1]);}
					if(row[2] == null){toDoRuleDTO.setOverDue("");}else{toDoRuleDTO.setOverDue(row[2]);}
					if(row[3] == null){toDoRuleDTO.setTotal("");}else{toDoRuleDTO.setTotal(row[3]);}
					if(row[4] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[4]);}
					if(row[5] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[5]);}
					resultList.add(toDoRuleDTO);
				  }	
			}
		    }else{
			String query="";
			if(sessionCorpID.equals("TSFT")){
				query="select t.todoRuleId, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
				"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',t.messagedisplayed, t.ruleNumber  " +
				"from todoresult t ,app_user a where t.corpID = '"+sessionCorpID+"' and t.owner is not null and a.username='"+username+"' and t.agentCode=a.parentAgent " +
				" group by t.todoRuleId order by 4 desc ";
			}else{
				if(userType.equalsIgnoreCase("PARTNER")){
					query="select t.todoRuleId, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
					"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',t.messagedisplayed, t.ruleNumber  " +
					"from todoresult t ,app_user a where t.corpID = '"+sessionCorpID+"' and t.owner is not null and a.username='"+username+"' and t.agentCode=a.parentAgent " +
					" group by t.todoRuleId order by 4 desc ";					
				}else{
					query="select todoRuleId, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
					"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',messagedisplayed, ruleNumber  " +
					"from todoresult where corpID = '"+sessionCorpID+"' and owner is not null group by todoRuleId order by 4 desc ";
				}
			}
			List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext())
			  {
				Object [] row=(Object[])it.next(); 
				ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
				if(row[0] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[0]);}
				if(row[1] == null){toDoRuleDTO.setCurrent("");}else{toDoRuleDTO.setCurrent(row[1]);}
				if(row[2] == null){toDoRuleDTO.setOverDue("");}else{toDoRuleDTO.setOverDue(row[2]);}
				if(row[3] == null){toDoRuleDTO.setTotal("");}else{toDoRuleDTO.setTotal(row[3]);}
				if(row[4] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[4]);}
				if(row[5] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[5]);}
				resultList.add(toDoRuleDTO);
			  }
		}
		
		return resultList;
	}

	public List getToDoRulesShipperSummary(String sessionCorpID ,String username,Long userId,String userType) {
		List resultList = new ArrayList();
		if(userType.equalsIgnoreCase("USER")){
			String queryfilter="";
			queryfilter="select dss.name from datasecurityset dss,datasecuritypermission dsp,datasecurityfilter dsf,userdatasecurity uds,app_user ap where dss.id=dsp.datasecurityset_id and dsf.id=dsp.datasecurityfilter_id and uds.userdatasecurity_id=dss.id and ap.id = uds.user_id  and ap.corpID='"+sessionCorpID+"' and ap.id='"+userId+"'";
		    List tempuserfilter = this.getSession().createSQLQuery(queryfilter).list();
		    if( tempuserfilter!=null && !tempuserfilter.isEmpty()){
		    	//resultList.add("");
			}else{
				String query="";
				if(sessionCorpID.equals("TSFT")){
					query="select t.todoRuleId, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
					"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',t.messagedisplayed, t.ruleNumber,t.shipper, t.id  " +
					"from todoresult t ,app_user a where t.corpID = '"+sessionCorpID+"' and t.shipper is not null and a.username='"+username+"' and t.agentCode=a.parentAgent " +
							" group by t.shipper order by trim(t.shipper)  ";
				}else{
					query="select todoRuleId, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
						"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',messagedisplayed, ruleNumber,shipper, id  " +
						"from todoresult where corpID = '"+sessionCorpID+"' and shipper is not null group by shipper order by trim(shipper)  ";
				}
				List list= this.getSession().createSQLQuery(query).list();
				Iterator it=list.iterator();
				while(it.hasNext())
				  {
					Object [] row=(Object[])it.next(); 
					ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
					if(row[0] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[0]);}
					if(row[1] == null){toDoRuleDTO.setCurrent("");}else{toDoRuleDTO.setCurrent(row[1]);}
					if(row[2] == null){toDoRuleDTO.setOverDue("");}else{toDoRuleDTO.setOverDue(row[2]);}
					if(row[3] == null){toDoRuleDTO.setTotal("");}else{toDoRuleDTO.setTotal(row[3]);}
					if(row[4] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[4]);}
					if(row[5] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[5]);}
					if(row[6] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[6]);}
					if(row[7] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[7]);}
					resultList.add(toDoRuleDTO);
				  }	
			}
		}else{
			String query="";
			if(sessionCorpID.equals("TSFT")){
				query="select t.todoRuleId, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
				"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',t.messagedisplayed, t.ruleNumber,t.shipper, t.id  " +
				"from todoresult t ,app_user a where t.corpID = '"+sessionCorpID+"' and t.shipper is not null and a.username='"+username+"' and t.agentCode=a.parentAgent " +
						" group by t.shipper order by trim(t.shipper)  ";
			}else{
				if(userType.equalsIgnoreCase("PARTNER")){
					query="select t.todoRuleId, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
					"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',t.messagedisplayed, t.ruleNumber,t.shipper, t.id  " +
					"from todoresult t ,app_user a where t.corpID = '"+sessionCorpID+"' and t.shipper is not null and a.username='"+username+"' and t.agentCode=a.parentAgent " +
							" group by t.shipper order by trim(t.shipper)  ";					
				}else{
				query="select todoRuleId, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
					"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',messagedisplayed, ruleNumber,shipper, id  " +
					"from todoresult where corpID = '"+sessionCorpID+"' and shipper is not null group by shipper order by trim(shipper)  ";
				}
			}
			List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext())
			  {
				Object [] row=(Object[])it.next(); 
				ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
				if(row[0] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[0]);}
				if(row[1] == null){toDoRuleDTO.setCurrent("");}else{toDoRuleDTO.setCurrent(row[1]);}
				if(row[2] == null){toDoRuleDTO.setOverDue("");}else{toDoRuleDTO.setOverDue(row[2]);}
				if(row[3] == null){toDoRuleDTO.setTotal("");}else{toDoRuleDTO.setTotal(row[3]);}
				if(row[4] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[4]);}
				if(row[5] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[5]);}
				if(row[6] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[6]);}
				if(row[7] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[7]);}
				resultList.add(toDoRuleDTO);
			  }
		}
		
		return resultList;
	}
	
	public List findMaximum(String sessionCorpID) {
		return getHibernateTemplate().find("select max(ruleNumber) from ToDoRule where corpID='"+sessionCorpID+"'");
	}
	
	public List getToDoResults(String sessionCorpID, String ruleNo, String email, String eMailType,String entity){
		if(eMailType.equalsIgnoreCase("manual")){
			if(ruleNo.equalsIgnoreCase("ALL")){
				if(entity.equals("OriginAgent") || entity.equals("DestinationAgent") || entity.equals("OriginSubAgent") || entity.equals("DestinationSubAgent") )
					toDoResults = this.getSession().createSQLQuery("select id from todoresult where manualEmail is not null and manualEmail!='' and emailOn < now() and manualEmail='"+email+"' and corpID='TSFT' ").list();
				else
					toDoResults = getHibernateTemplate().find("select id from ToDoResult where manualEmail is not null and manualEmail!='' and emailOn < now() and manualEmail='"+email+"' and corpID='"+sessionCorpID+"' ");
			}else{
				if(entity.equals("OriginAgent") || entity.equals("DestinationAgent") || entity.equals("OriginSubAgent") || entity.equals("DestinationSubAgent") )
					toDoResults = this.getSession().createSQLQuery("select id  from todoresult where manualEmail is not null and manualEmail!='' and emailOn < now()and manualEmail='"+email+"' and corpID='TSFT' and ruleNumber='"+ruleNo+"' ").list();
				else
					toDoResults = getHibernateTemplate().find("select id from ToDoResult where manualEmail is not null and manualEmail!='' and emailOn < now()and manualEmail='"+email+"' and corpID='"+sessionCorpID+"' and ruleNumber='"+ruleNo+"' ");
			}
		}else{
			if(ruleNo.equalsIgnoreCase("ALL")){
				if(entity.equals("OriginAgent") || entity.equals("DestinationAgent") || entity.equals("OriginSubAgent") || entity.equals("DestinationSubAgent") )
					toDoResults = this.getSession().createSQLQuery("select id from todoresult where emailNotification is not null and emailNotification!='' and emailOn < now() and emailNotification='"+email+"' and corpID='TSFT' ").list();
				else
					toDoResults = getHibernateTemplate().find("select id from ToDoResult where emailNotification is not null and emailNotification!='' and emailOn < now() and emailNotification='"+email+"' and corpID='"+sessionCorpID+"' ");
			}else{
				if(entity.equals("OriginAgent") || entity.equals("DestinationAgent") || entity.equals("OriginSubAgent") || entity.equals("DestinationSubAgent") )
					toDoResults = this.getSession().createSQLQuery("select id from todoresult where emailNotification is not null and emailNotification!='' and emailOn < now()and emailNotification='"+email+"' and corpID='TSFT' and ruleNumber='"+ruleNo+"' ").list();
				else
					toDoResults = getHibernateTemplate().find("select id from ToDoResult where emailNotification is not null and emailNotification!='' and emailOn < now()and emailNotification='"+email+"' and corpID='"+sessionCorpID+"' and ruleNumber='"+ruleNo+"' ");
			}
		}
		return toDoResults;
	}
	
	public List getToDoResultEmailList(String sessionCorpID, String ruleNo, String eMailType, String entity){
		 List list =new ArrayList();
		 if(eMailType.equalsIgnoreCase("manual")){
			 	/*if(entity.equals("OriginAgent") || entity.equals("DestinationAgent") || entity.equals("OriginSubAgent") || entity.equals("DestinationSubAgent") )
			 		list= this.getSession().createSQLQuery("select distinct manualEmail from todoresult where manualEmail is not null and manualEmail!='' and emailOn < now() and corpID='TSFT' and ruleNumber='"+ruleNo+"' ").list();
			 	else*/
			 		list= this.getSession().createSQLQuery("select distinct manualEmail from todoresult where manualEmail is not null and manualEmail!='' and emailOn < now() and corpID='"+sessionCorpID+"' and ruleNumber='"+ruleNo+"' ").list();
		 }else{
			 /*if(entity.equals("OriginAgent") || entity.equals("DestinationAgent") || entity.equals("OriginSubAgent") || entity.equals("DestinationSubAgent") )
				 	list= this.getSession().createSQLQuery("select distinct emailNotification from todoresult where emailNotification is not null and emailNotification!='' and emailOn < now() and corpID='TSFT' and ruleNumber='"+ruleNo+"' ").list();
			 	else*/
			 		list= this.getSession().createSQLQuery("select distinct emailNotification from todoresult where emailNotification is not null and emailNotification!='' and emailOn < now() and corpID='"+sessionCorpID+"' and ruleNumber='"+ruleNo+"' ").list();
			}
		return list;
	}
	
	public List getToDoResultEmailList(String sessionCorpID, String ruleNumber){
		List list =new ArrayList();
		if(ruleNumber!=null && !ruleNumber.equalsIgnoreCase("")){
			list= this.getSession().createSQLQuery("select distinct manualEmail from todoresult where manualEmail is not null and manualEmail!='' and emailOn < now() and corpID='"+sessionCorpID+"' and ruleNumber='"+ruleNumber+"' UNION select distinct emailNotification from todoresult where emailNotification is not null and emailNotification!='' and emailOn < now() and corpID='"+sessionCorpID+"' and ruleNumber='"+ruleNumber+"' ").list();
		}else{
			list= this.getSession().createSQLQuery("select distinct manualEmail from todoresult where manualEmail is not null and manualEmail!='' and emailOn < now() and corpID='"+sessionCorpID+"' union select distinct emailNotification from todoresult where emailNotification is not null and emailNotification!='' and emailOn < now() and corpID='"+sessionCorpID+"' ").list();
		}
		return list;
	}
	
	public List getDocCheckListResultEmailList(String sessionCorpID, String ruleNumber){
		List list =new ArrayList();
		if(ruleNumber!=null && !ruleNumber.equalsIgnoreCase("")){
			list= this.getSession().createSQLQuery("select distinct manualEmail from checklistresult where manualEmail is not null and manualEmail!='' and emailOn < now() and corpID='"+sessionCorpID+"' and ruleNumber='"+ruleNumber+"' UNION select distinct emailNotification from checklistresult where emailNotification is not null and emailNotification!='' and emailOn < now() and corpID='"+sessionCorpID+"' and ruleNumber='"+ruleNumber+"' ").list();
		}else{
			list= this.getSession().createSQLQuery("select distinct manualEmail from checklistresult where manualEmail is not null and manualEmail!='' and emailOn < now() and corpID='"+sessionCorpID+"' union select distinct emailNotification from checklistresult where emailNotification is not null and emailNotification!='' and emailOn < now() and corpID='"+sessionCorpID+"' ").list();
		}
		return list;
	}
	
	public List<ToDoResult> getToDoResults(String sessionCorpID, String email, String ruleNumber){
		List<ToDoResult> toDoResults1 = new ArrayList<ToDoResult>();
		if(ruleNumber!=null && !ruleNumber.equalsIgnoreCase("")){
			toDoResults = getHibernateTemplate().find("from ToDoResult where emailNotification is not null and emailNotification!='' and emailOn < now() and emailNotification='"+email+"' and ruleNumber='"+ruleNumber+"' and corpID='"+sessionCorpID+"' ");
			toDoResults1 = getHibernateTemplate().find("from ToDoResult where manualEmail is not null and manualEmail!='' and emailOn < now() and manualEmail='"+email+"' and ruleNumber='"+ruleNumber+"' and corpID='"+sessionCorpID+"' ");
		}else{			
			toDoResults = getHibernateTemplate().find("from ToDoResult where emailNotification is not null and emailNotification!='' and emailOn < now() and emailNotification='"+email+"' and corpID='"+sessionCorpID+"' ");
			toDoResults1 = getHibernateTemplate().find("from ToDoResult where manualEmail is not null and manualEmail!='' and emailOn < now() and manualEmail='"+email+"' and corpID='"+sessionCorpID+"' ");
		}
		toDoResults.addAll(toDoResults1);
		return toDoResults;
	}
	
	
	public List<CheckListResult> getCheckListResult(String sessionCorpID, String email, String ruleNumber){ 
		List<CheckListResult> checkListResult = new ArrayList<CheckListResult>();
		List<CheckListResult> checkListResult1 = new ArrayList<CheckListResult>();
		if(ruleNumber!=null && !ruleNumber.equalsIgnoreCase("")){
			checkListResult = getHibernateTemplate().find("from CheckListResult where emailNotification is not null and emailNotification!='' and emailOn < now() and emailNotification='"+email+"' and ruleNumber='"+ruleNumber+"' and corpID='"+sessionCorpID+"' ");
			checkListResult1 = getHibernateTemplate().find("from CheckListResult where manualEmail is not null and manualEmail!='' and emailOn < now() and manualEmail='"+email+"' and ruleNumber='"+ruleNumber+"' and corpID='"+sessionCorpID+"' ");
		}else{			
			checkListResult = getHibernateTemplate().find("from CheckListResult where emailNotification is not null and emailNotification!='' and emailOn < now() and emailNotification='"+email+"' and corpID='"+sessionCorpID+"' ");
			checkListResult1 = getHibernateTemplate().find("from CheckListResult where manualEmail is not null and manualEmail!='' and emailOn < now() and manualEmail='"+email+"' and corpID='"+sessionCorpID+"' ");
		}
		checkListResult.addAll(checkListResult1);
		return checkListResult;
	}
	public List getComingUpList(String ShipNumber,String sessionCorpID,String relatedTask){
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr =dfm.format(filterdate);
		Company company = companyManager.findByCorpID(sessionCorpID).get(0);
		Date cal = new Date();
        logger.warn("system Date : "+cal);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
        String ss =formatter.format(cal);
        logger.warn("system Date After formate : "+ss);
		List status=new ArrayList();
		/*if(relatedTask.equalsIgnoreCase("relatedTask")){
			 status = getHibernateTemplate().find("from Notes where reminderStatus='NEW' and forwarddate>'"+compareWithDateStr+"' and customerNumber like '%"+ShipNumber.replaceAll("'", "''") +"%' and corpID='"+sessionCorpID+"'");	
		}else{
			 status = getHibernateTemplate().find("from Notes where reminderStatus='NEW' and forwarddate>'"+compareWithDateStr+"' and notesId='"+ShipNumber+"' and corpID='"+sessionCorpID+"'");
		}*/
		if(relatedTask.equalsIgnoreCase("relatedTask")){
			 status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='NEW' and forwarddate>date_format('"+ss+"','%y-%m-%d')  and customerNumber like '%"+ShipNumber.replaceAll("'", "''") +"%' and corpID='"+sessionCorpID+"'");	
		}else{
			 status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='NEW' and forwarddate>date_format('"+ss+"','%y-%m-%d')  and notesId='"+ShipNumber+"' and corpID='"+sessionCorpID+"'");
		}
		
		return status;
		
	}
	public List getDueToday(String ShipNumber,String sessionCorpID,String relatedTask){
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr = dfm.format(filterdate);
		List status=new ArrayList();
		if(relatedTask.equalsIgnoreCase("relatedTask")){
			 status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='NEW' and forwarddate='"+compareWithDateStr+"' and customerNumber like '%"+ShipNumber.replaceAll("'", "''") +"%' and corpID='"+sessionCorpID+"'");	
		}else{
			 status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='NEW' and forwarddate='"+compareWithDateStr+"' and notesId='"+ShipNumber+"' and corpID='"+sessionCorpID+"'");
		}
		
		return status;
	}
	public List getOverDue(String ShipNumber,String sessionCorpID,String relatedTask){
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr =dfm.format(filterdate);
		Company company = companyManager.findByCorpID(sessionCorpID).get(0);
		Date cal = new Date();
        logger.warn("system Date : "+cal);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
        String ss =formatter.format(cal);
        logger.warn("system Date After formate : "+ss);
		List status=new ArrayList();
		/*if(relatedTask.equalsIgnoreCase("relatedTask")){
		 status = getHibernateTemplate().find("from Notes where reminderStatus='NEW' and forwarddate<'"+compareWithDateStr+"' and customerNumber like '%"+ShipNumber.replaceAll("'", "''") +"%' and corpID='"+sessionCorpID+"'");	
		}else{
		 status = getHibernateTemplate().find("from Notes where reminderStatus='NEW' and forwarddate<'"+compareWithDateStr+"' and notesId='"+ShipNumber+"' and corpID='"+sessionCorpID+"'");
		}*/
		if(relatedTask.equalsIgnoreCase("relatedTask")){
			 status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='NEW' and forwarddate<date_format('"+ss+"','%y-%m-%d')  and customerNumber like '%"+ShipNumber.replaceAll("'", "''") +"%' and corpID='"+sessionCorpID+"'");	
			}else{
			 status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='NEW' and forwarddate<date_format('"+ss+"','%y-%m-%d')  and notesId='"+ShipNumber+"' and corpID='"+sessionCorpID+"'");
			}
		
		return status;
		
	}
	public List getResultToday(String ShipNumber,Long id,String sessionCorpID,String relatedTask){
		List resultList = new ArrayList();
		String query="";
		if(relatedTask.equalsIgnoreCase("relatedTask")){
			 query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.billToName,tr.ruleNumber" +
			"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement' or n.noteSubtype = 'AgentNotes' ) and n.corpID in ('"+sessionCorpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null)  where tr.durationAddSub='0' and tr.fileNumber like '%"+ShipNumber.replaceAll("'", "''") +"%'  and tr.corpID='"+sessionCorpID+"' group by tr.id,tr.ruleNumber ";	
		}else{
			 query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.billToName,tr.ruleNumber" +
			"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement' or n.noteSubtype = 'AgentNotes' ) and n.corpID in ('"+sessionCorpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.fileNumber='"+ShipNumber+"'  and tr.corpID='"+sessionCorpID+"' group by tr.id,tr.ruleNumber ";
		}
		
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
			if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
			if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
			if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
			if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
			if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
			if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
			if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
			if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
			if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
			if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
			if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
			if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
			if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
			if(row[22] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[22]);}
			if(row[23] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[23]);}
			resultList.add(toDoRuleDTO);
		  }
		return resultList;	
	}
	public List getToDoOverDue(String ShipNumber,Long id,String sessionCorpID,String relatedTask){
		List resultList = new ArrayList();
		String query="";
		if(relatedTask.equalsIgnoreCase("relatedTask")){
			query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.billToName ,tr.ruleNumber" +
			"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.notetype = 'Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+sessionCorpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.fileNumber like '%"+ShipNumber.replaceAll("'", "''") +"%'   and tr.corpID='"+sessionCorpID+"' group by tr.id,tr.ruleNumber ";
		}else{
			query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.billToName ,tr.ruleNumber" +
			"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.notetype = 'Activity') and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+sessionCorpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.fileNumber='"+ShipNumber+"'  and tr.corpID='"+sessionCorpID+"' group by tr.id,tr.ruleNumber ";
		}
		
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
			if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
			if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
			if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
			if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
			if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
			if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
			if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
			if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
			if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
			if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
			if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
			if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
			if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
			if(row[22] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[22]);}
			if(row[23] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[23]);}
			resultList.add(toDoRuleDTO);
		  }
		return resultList;
	
	}
	public List getResultListDueToday(String ShipNumber,Long id,String sessionCorpID,String owner, String duration,String relatedTask){
		if(owner==null){owner="";}
		String owners = owner.replaceAll("'", "");
		List result= new ArrayList();
		if(relatedTask.equalsIgnoreCase("relatedTask")){
			if(duration.equals("today")){
				result=getHibernateTemplate().find("from CheckListResult where resultNumber like '%"+ShipNumber.replaceAll("'", "''") +"%' and corpID='"+sessionCorpID+"' and duration=0");
		
				}
			if(duration.equals("overDue")){
			   result=getHibernateTemplate().find("from CheckListResult where resultNumber like '%"+ShipNumber.replaceAll("'", "''") +"%' and corpID='"+sessionCorpID+"' and duration>0");	
			
			}
		}else{
			if(duration.equals("today")){
				result=getHibernateTemplate().find("from CheckListResult where resultid='"+id+"' and resultNumber='"+ShipNumber+"' and corpID='"+sessionCorpID+"' and duration=0");
		
				}
			if(duration.equals("overDue")){
			   result=getHibernateTemplate().find("from CheckListResult where resultid='"+id+"' and resultNumber='"+ShipNumber+"' and corpID='"+sessionCorpID+"' and duration>0");	
			
			}
		}
		
		return result;
	}
	
	public String getUserLastFirstName(String coordinator){
		String name="";
		try {
			List l =  this
					.getSession()
					.createSQLQuery(
							"select if(first_name is not null and first_name!='',concat(first_name,',',last_name),last_name) from app_user where username='"
									+ coordinator + "' ").list();
			if(l!=null && !l.isEmpty() && l.get(0)!=null){
				name = l.get(0).toString();
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return name;
	}
	
	public List getBillToCode(String companydivisionCode,String sessionCorpID){
		List companyCode=new ArrayList();
		companyCode=this.getSession().createSQLQuery("SELECT bookingAgentCode,description FROM companydivision  where companycode='"+companydivisionCode+"' and corpid='"+sessionCorpID+"'" ).list(); 
		if(companyCode==null && (!companyCode.isEmpty())){
		companyCode=this.getSession().createSQLQuery("SELECT companyCode,company FROM systemdefault  where corpid='"+sessionCorpID+"'").list();
		}
		return companyCode;
		
	}

	

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	public Map<String, List<String>> getNetworkDataFields(){
		Map<String, List<String>> networkFields = new HashMap<String, List<String>>();
		List l1 = this.getSession().createSQLQuery("select  distinct modelName from networkdatafields where modelName <>'AccountLine' ").list();
		Iterator itr = l1.iterator();
		while(itr.hasNext()){
			String ModelName = itr.next().toString();
			List<String> fieldsList = this.getSession().createSQLQuery("select distinct fieldName from networkdatafields where modelName='"+ModelName+"' and transactionType='Update' and (type is null or type='') and fieldName not in ('updatedOn','updatedBy') ").list();
			networkFields.put(ModelName, fieldsList);
		}
		return networkFields;
	}
	public int updateCompanyRulesRunning(String corpId,Boolean useDistAmt){
		return getHibernateTemplate().bulkUpdate("update Company set rulesRunning="+useDistAmt+" where corpID='"+corpId+"'");
	}
	public <ToDoResult> List getAllToDoResult(){
		return this.getSession().createSQLQuery("select * from todoresult").list();
	}
	public <ToDoResult> List getToDoResultByRuleNumber(String ruleNumber)
	{
	List<ToDoResult>  list =  (List<ToDoResult>)getHibernateTemplate().find("from ToDoResult where ruleNumber='"+ruleNumber+"' ");
	return list;
	}
	
	public <TaskCheckList> List getTaskCheckListByRuleNumber(String ruleNumber)
	{
	List<TaskCheckList>  list =  (List<TaskCheckList>)getHibernateTemplate().find("from TaskCheckList where ruleNumber='"+ruleNumber+"' ");
	return list;
	}
	
	public void delAndUpdateResult(String corpId) {
		int i = this.getSession().createSQLQuery("delete from todoresult where recordToDel='0' and corpid='"+corpId+"'").executeUpdate();
		System.out.println("Records Deleted From ToDoResult : "+i);
		
		 i = this.getSession().createSQLQuery("update  todoresult set recordToDel='0' where recordToDel='1' and corpid='"+corpId+"'").executeUpdate();
		System.out.println("Records Updated in ToDoResult : "+i);
		
	}
	public void updateToDoResult(String toDoResultId,String toDoResultUrl,String toDoRecordId,String roleForTransfer,String roleType,String shipnumber){
		getHibernateTemplate().bulkUpdate("update ToDoResult set reassignedOwner=UPPER('"+roleForTransfer+"'),owner=UPPER('"+roleForTransfer+"') where id='"+toDoResultId+"' ");
		/*if(roleType.equals("Coordinator")){
			getHibernateTemplate().bulkUpdate("update ServiceOrder set coordinator=UPPER('"+roleForTransfer+"') where shipNumber='"+shipnumber+"' ");
		}
		if(roleType.equals("Consultant")){
			getHibernateTemplate().bulkUpdate("update CustomerFile set estimator=UPPER('"+roleForTransfer+"') where sequenceNumber='"+shipnumber+"' ");
		}
		if(roleType.equals("Billing")){
			getHibernateTemplate().bulkUpdate("update Billing set personBilling=UPPER('"+roleForTransfer+"') where shipNumber='"+shipnumber+"' ");
		}*/
	}

	public int findCheckListEnterd(String corpID, Long id){
		int count =0;
		List l = new ArrayList();
		if(id>0L){
		 l= this.getSession().createSQLQuery("select count(*) from checklistresult where checkListId ='"+id+"' and corpID ='"+corpID+"' ").list();
		}else{
		 l= this.getSession().createSQLQuery("select count(*) from checklistresult where corpID ='"+corpID+"' ").list();
		}
		count = Integer.parseInt(l.get(0).toString());
		return count;
	}
	
	public String getCordinfo(ToDoResult tdr) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
    public void deleteUpdateByRuleNumber(Long ruleNumber,String corpId){
    	int i = this.getSession().createSQLQuery("delete from todoresult where recordToDel='0' and ruleNumber='"+ruleNumber+"' and corpid='"+corpId+"' ").executeUpdate();
		System.out.println("Records Deleted From ToDoResult : "+i);
		i = this.getSession().createSQLQuery("update  todoresult set recordToDel='0' where recordToDel ='1' and ruleNumber='"+ruleNumber+"' and corpid='"+corpId+"' ").executeUpdate();
		System.out.println("Records Updated in ToDoResult : "+i);
	}
    public <CheckListResult> List getCheckListResultByRuleNumber(String ruleNumber)
	{
	List<CheckListResult>  list =  (List<CheckListResult>)getHibernateTemplate().find("from CheckListResult where ruleNumber='"+ruleNumber+"' ");
	return list;
	}
    public List findByAgentTdrEmail(String corpID,String owner) {
		//return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' group by id");
		List resultList = new ArrayList();
		String query="";
		if(owner!=null && !owner.equals("") ){
			query="select tr.id,tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed,tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType,tr.noteSubType,tr.billToName ,tr.ruleNumber, tr.agentName,tr.emailNotification,tr.fieldToValidate2 " +
			"  from todoresult tr  where  lpad(tr.fileNumber,4,'')='"+corpID+"' and tr.corpID='TSFT' and owner ='"+owner+"' ";
		}else{
			query="select tr.id,tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed,tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType,tr.noteSubType,tr.billToName ,tr.ruleNumber,tr.agentName,tr.emailNotification,tr.fieldToValidate2 " +
			"  from todoresult tr  where  lpad(tr.fileNumber,4,'')='"+corpID+"' and tr.corpID='TSFT'";
		}
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
			if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
			if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
			if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
			if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
			if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
			if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
			if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
			if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
			if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
			if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
			if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
			/*if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
			if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}*/
			if(row[20] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[20]);}
			if(row[21] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[21]);}
			if(row[22]== null){toDoRuleDTO.setAgentName("");}else{toDoRuleDTO.setAgentName(row[22]);}
			if(row[23]== null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);
			if(row[24]== null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[24]);}
			
			}
			resultList.add(toDoRuleDTO);
		  }
		return resultList;
	}
    public List findByAgentTdrEmail(String ShipNumber,String corpID,String owner) {

		//return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' group by id");
		List resultList = new ArrayList();
		String query="";
		if(owner!=null && !owner.equals("") ){
			query="select tr.id,tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed,tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType,tr.noteSubType,tr.billToName ,tr.ruleNumber, tr.agentName,tr.emailNotification,tr.fieldToValidate2 " +
			"  from todoresult tr  where  lpad(tr.fileNumber,4,'')='"+corpID+"' and tr.corpID='TSFT' and fileNumber='"+ShipNumber+"' and owner ='"+owner+"' ";
		}else{
			query="select tr.id,tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed,tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType,tr.noteSubType,tr.billToName ,tr.ruleNumber,tr.agentName,tr.emailNotification,tr.fieldToValidate2 " +
			"  from todoresult tr  where  lpad(tr.fileNumber,4,'')='"+corpID+"' and fileNumber='"+ShipNumber+"' and tr.corpID='TSFT'" ;
		}
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
			if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
			if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
			if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
			if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
			if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
			if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
			if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
			if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
			if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
			if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
			if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
			/*if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
			if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}*/
			if(row[20] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[20]);}
			if(row[21] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[21]);}
			if(row[22]== null){toDoRuleDTO.setAgentName("");}else{toDoRuleDTO.setAgentName(row[22]);}
			if(row[23]== null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);
			if(row[24]== null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[24]);}
			
			}
			resultList.add(toDoRuleDTO);
		  }
		return resultList;
	}
    
    public <RuleActions> List getToDoActionsByRuleId(Long ruleId, String corpId)
    {
    List<RuleActions>  list =  (List<RuleActions>)getHibernateTemplate().find("from RuleActions  where ruleId='"+ruleId+"' and corpID='"+corpId+"' and status=true");
    return list;
    }
    public List getOwnersListExtCordinator(String sessionCorpID ,String username,Long userId,String userFirstName,String userType,String bookingAgentPopup,String billToCodePopup) {
		List userList=new ArrayList();
		List tempuserfilter=new ArrayList();
		String billtocodecondition="";
		String bookingAgentcondition="";
		String acc="";
		if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
		{
			
			billtocodecondition= "and billToCode in ("+billToCodePopup+")";
		}
	
		if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
		{
			bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
        }
		if(userType.equalsIgnoreCase("ACCOUNT")){
			acc=" and isAccount is true ";
		}
		
			String query="";
			
		    query = "select distinct owner from todoresult where corpID='"+sessionCorpID+"' and owner<>'' and owner in ("+username+") and  owner is not null "+billtocodecondition+""+bookingAgentcondition+" "+acc+" union select distinct UPPER(owner) from checklistresult where corpID='"+sessionCorpID+"' and   owner in ("+username+") and owner<>'' "+billtocodecondition+""+bookingAgentcondition+" "+acc+" union select distinct followUpFor as owner from notes where id>(select id from notes_start_id) and corpID='"+sessionCorpID+"' and (noteStatus in('NEW','FWD','PEND') or noteStatus is null) and followUpFor is not null and followUpFor<>'' and   followUpFor in ("+username+")  and reminderstatus = 'NEW' and forwardDate is not null "+billtocodecondition+""+bookingAgentcondition+" group by owner order by owner";
		    List tempuserList = this.getSession().createSQLQuery(query).list();
			
			Iterator it=tempuserList.iterator();
			while(it.hasNext()){
			Object value=it.next();
		    userList.add(value);
			
			}	
		
		return 	userList;
		//return getHibernateTemplate().find(query);
	}
    
    public List getMessageListExtCordinator(String sessionCorpID, String userName,Long userId,String userType,String bookingAgentPopup,String billToCodePopup) {
		List ls = new ArrayList();
		List lsCheckList= new ArrayList();
		List tempuserfilter=new ArrayList();
		String billtocodecondition="";
		String bookingAgentcondition="";
		String acc="";
		if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
		{
			
			billtocodecondition= "and billToCode in ("+billToCodePopup+")";
		}
	
		if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
		{
			bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
        }
		if(userType.equalsIgnoreCase("ACCOUNT")){
			acc=" and isAccount is true ";
		}
			String queryfilter="";
						ls=this.getSession().createSQLQuery("select distinct messagedisplayed from todoresult where  owner in ("+userName+") and corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+" "+acc+" union select distinct messagedisplayed from checklistresult where corpID='"+sessionCorpID+"' and  owner in ("+userName+") "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by messagedisplayed").list();
				
		
		return ls;
	}
    
    public List getRuleIdListExtCordinator(String sessionCorpID ,String username,Long userId,String userType,String bookingAgentPopup,String billToCodePopup) {
		List ls = new ArrayList();
		List tempuserfilter=new ArrayList();
		String billtocodecondition="";
		String bookingAgentcondition="";
		String acc="";
		if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
		{
			
			billtocodecondition= "and billToCode in ("+billToCodePopup+")";
		}
	
		if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
		{
			bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
        }
		if(userType.equalsIgnoreCase("ACCOUNT")){
			acc=" and isAccount is true ";
		}
					ls=this.getSession().createSQLQuery("select ruleNumber from todoresult where owner in("+username+") and corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by ruleNumber union select Cast(ruleNumber as DECIMAL) from checklistresult where corpID='"+sessionCorpID+"' and  owner in("+username+") "+billtocodecondition+""+bookingAgentcondition+"  "+acc+"  group by ruleNumber order by ruleNumber").list();
			
		
		return ls;
	}
    public List<ToDoResult> findBydurationUserExtCordinator(String corpID, String owner, String messageDisplayed, String ruleId, String shipper,User appUser,String bookingAgentPopup,String billToCodePopup) {
		if(messageDisplayed==null){	messageDisplayed="";}
		if(ruleId==null){ruleId="";}
		if(shipper==null){shipper="";}
	    //return getHibernateTemplate().find("from ToDoResult where durationAddSub>'0' and corpID='"+corpID+"' and owner like '"+owner+"%' and messagedisplayed like '"+messageDisplayed+"%' and todoRuleId like '"+ruleId+"%' group by id");
		List resultList = new ArrayList();
		String query="";
		String billtocodecondition="";
		String bookingAgentcondition="";
		String user = owner.replaceAll("'", "");
		String acc="";
		if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
		{
			
			billtocodecondition= " and tr.billToCode in ("+billToCodePopup+")";
		}
	
		if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
		{
			bookingAgentcondition= "and tr.bookingAgentCode in ("+bookingAgentPopup+")";
        }
		if(appUser.getUserType().equalsIgnoreCase("ACCOUNT")){
			acc=" and tr.isAccount is true ";
		}
			//plesse check this one
		if(ruleId.equalsIgnoreCase("")){
			if(shipper.equalsIgnoreCase("")){
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+"  "+acc+" "  ;
				 if(user!=null && !user.equals("")){
					query=query+"and tr.owner in ("+owner+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%'  group by tr.id,tr.ruleNumber";	
			}else{
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+"  "+acc+"  " ;
				 if(user!=null && !user.equals("")){
					query=query+"and tr.owner in ("+owner+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";	
			}
		
		}else{
			if(shipper.equalsIgnoreCase("")){
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+"  "+acc+"  " ;
				 if(user!=null && !user.equals("")){
					query=query+"and tr.owner in ("+owner+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+" and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber = '"+ruleId+"%'  group by tr.id,tr.ruleNumber  ";	
			}else{
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'  "+billtocodecondition+""+bookingAgentcondition+"  "+acc+" ";
				 if(user!=null && !user.equals("")){
					query=query+"and tr.owner in ("+owner+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber = '"+ruleId+"%'  and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";
				}
			}
		
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
			if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
			if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
			if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
			if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
			if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
			if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
			if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
			if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
			if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
			if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
			if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
			if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
			if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
			if(row[22] == null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[22]);}
			if(row[23] == null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);}
			if(row[24] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[24]);}
			if(row[25] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[25]);}
			resultList.add(toDoRuleDTO);
		  }
		return resultList;
	}
	public List<ToDoResult> findBydurationdtodayUserExtCordinator(String corpID, String owner, String messageDisplayed, String ruleId, String shipper,User appUser,String bookingAgentPopup,String billToCodePopup) {
		if(messageDisplayed==null){	messageDisplayed="";}
		if(ruleId==null){ruleId="";}
		if(shipper==null){shipper="";}
		//return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' and owner like '"+owner+"%' and messagedisplayed like '"+messageDisplayed+"%' and todoRuleId like '"+ruleId+"%' group by id");
		List resultList = new ArrayList();
		String query="";
		String billtocodecondition="";
		String bookingAgentcondition="";
		String user = owner.replaceAll("'", "");	
		String acc="";
		if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
		{
			
			billtocodecondition= "and tr.billToCode in ("+billToCodePopup+")";
		}
	
		if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
		{
			bookingAgentcondition= "and tr.bookingAgentCode in ("+bookingAgentPopup+")";
        }
		if(appUser.getUserType().equalsIgnoreCase("ACCOUNT")){
			acc=" and tr.isAccount is true ";
		}
		
		if(ruleId.equalsIgnoreCase("")){
			if(shipper.equalsIgnoreCase("")){
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+" "+acc+" " ;
				 if(user!=null && !user.equals("")){
					query=query+"and tr.owner in ("+owner+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%'  group by tr.id,tr.ruleNumber";
			}else{
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName ,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+" "+acc+" " ;
				 if(user!=null && !user.equals("")){
					query=query+"and tr.owner in ("+owner+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";
			}
			
		}else{
			if(shipper.equalsIgnoreCase("")){
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+" "+acc+" " ;
				 if(user!=null && !user.equals("")){
					query=query+"and tr.owner in ("+owner+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
					query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%'  group by tr.id,tr.ruleNumber";		
			}else{
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+"  "+acc+" " ;
				 if(user!=null && !user.equals("")){
					query=query+"and tr.owner in ("+owner+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}else{
				    query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
				}
				query=query+" and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";	
			}
			
			}
		
		
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
			if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
			if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
			if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
			if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
			if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
			if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
			if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
			if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
			if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
			if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
			if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
			if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
			if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
			if(row[22] == null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[22]);}
			if(row[23] == null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);}
			if(row[24] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[24]);}
			if(row[25] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[25]);}
			resultList.add(toDoRuleDTO);
		  }
		return resultList;
	}
	 public List findByAgentTdrEmailExtCordinator(String corpID,String owner,String bookingAgentPopup,String billToCodePopup,String userType) {
			//return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' group by id");
			List resultList = new ArrayList();
			String query="";
			String billtocodecondition="";
			String bookingAgentcondition="";
			String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and tr.billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and tr.bookingAgentCode in ("+bookingAgentPopup+")";
	        }
			if(userType.equalsIgnoreCase("ACCOUNT")){
				acc=" and tr.isAccount is true ";
			}
			if(owner!=null && !owner.equals("") ){
				query="select tr.id,tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed,tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType,tr.noteSubType,tr.billToName ,tr.ruleNumber, tr.agentName,tr.emailNotification,tr.fieldToValidate2 " +
				"  from todoresult tr  where  lpad(tr.fileNumber,4,'')='"+corpID+"' and tr.corpID='TSFT' and owner in ("+owner+")   "+billtocodecondition+""+bookingAgentcondition+" "+acc+" ";
			}else{
				query="select tr.id,tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed,tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType,tr.noteSubType,tr.billToName ,tr.ruleNumber,tr.agentName,tr.emailNotification,tr.fieldToValidate2 " +
				"  from todoresult tr  where  lpad(tr.fileNumber,4,'')='"+corpID+"' and tr.corpID='TSFT'    "+billtocodecondition+""+bookingAgentcondition+" "+acc+" ";
			}
			List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext())
			  {
				Object [] row=(Object[])it.next(); 
				ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
				if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
				if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
				if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
				if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
				if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
				if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
				if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
				if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
				if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
				if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
				if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
				if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
				if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
				if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
				if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
				if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
				if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
				if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
				if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
				if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
				/*if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
				if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}*/
				if(row[20] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[20]);}
				if(row[21] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[21]);}
				if(row[22]== null){toDoRuleDTO.setAgentName("");}else{toDoRuleDTO.setAgentName(row[22]);}
				if(row[23]== null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);
				if(row[24]== null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[24]);}
				
				}
				resultList.add(toDoRuleDTO);
			  }
			return resultList;
		}
	 public List<ToDoResult> findBydurationForSupervisorExtCordinator(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String userType) {
			String billtocodecondition="";
			String bookingAgentcondition="";
			String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }
			if(userType.equalsIgnoreCase("ACCOUNT")){
				acc=" and isAccount is true ";
			}
			return getHibernateTemplate().find("from ToDoResult where durationAddSub>'0' and corpID='"+corpID+"' and owner in ("+owner+") "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by id");
	 }
	 
	 public List<ToDoResult> findBydurationdtodayForSupervisorExtCordinator(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String userType) {
			String billtocodecondition="";
			String bookingAgentcondition="";
			String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }	
			if(userType.equalsIgnoreCase("ACCOUNT")){
				acc=" and isAccount is true ";
			}
		 return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' and owner in ("+owner+") "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by id");
		}
	 public List<ToDoResult> findBydurationdueweekForSupervisorExtCordinator(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String userType) {
		 String billtocodecondition="";
			String bookingAgentcondition="";
			String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }		
			if(userType.equalsIgnoreCase("ACCOUNT")){
				acc=" and isAccount is true ";
			}
		 return getHibernateTemplate().find("from ToDoResult where durationAddSub<'0' and corpID='"+corpID+"' and owner in ("+owner+") "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by id");
		}
	 public List<Notes> findByStatusExtCordinator(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup) {
		 String user = createdBy.replaceAll("'", "");	
		 Date filterdate = DateUtils.addDays(getcurrentDate(),0);
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String compareWithDateStr =dfm.format(filterdate);
			Company company = companyManager.findByCorpID(corpId).get(0);
			Date cal = new Date();
	        //logger.warn("system Date : "+cal);
	        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
	        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
	        String ss =formatter.format(cal);
	        String billtocodecondition="";
			String bookingAgentcondition="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }	        //logger.warn("system Date After formate : "+ss);
			//List status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate='"+compareWithDateStr+"'and corpID='"+corpId+"' and (createdBy='"+createdBy+"' or forwardToUser='"+createdBy+"' or followUpFor='"+createdBy+"' or updatedBy='"+createdBy+"')");
	        Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
	        List status = new ArrayList();
			if(user!=null && !user.equalsIgnoreCase("") ){
				status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate=date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and followUpFor in ("+createdBy+") "+billtocodecondition+""+bookingAgentcondition+"");
			}else{
				status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and  noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate=date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' "+billtocodecondition+""+bookingAgentcondition+" ");
			}
			//status.size();
			return status;
		}
	 public List<Notes> findByStatus1ExtCordinator(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup) {		
		String user = createdBy.replaceAll("'", "");
		 Date filterdate = DateUtils.addDays(getcurrentDate(),0);
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String compareWithDateStr =dfm.format(filterdate);
			Company company = companyManager.findByCorpID(corpId).get(0);
			Date cal = new Date();
	        //logger.warn("system Date : "+cal);
	        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
	        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
	        String ss =formatter.format(cal);
	        logger.warn("system Date After formate : "+ss);
			List status = new ArrayList();
			/*if(createdBy!=null && !createdBy.equalsIgnoreCase("") && !createdBy.equalsIgnoreCase("UNASSIGNED") ){
				status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"' and corpID='"+corpId+"' and followUpFor='"+createdBy+"'");
			}else if(createdBy!=null && createdBy.equalsIgnoreCase("UNASSIGNED")){
				//String temp="select * from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"' and (followUpFor='' or followUpFor is null )";
				status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"' and (followUpFor='' or followUpFor is null ) " );
			}else{
				status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"'");	
			}*/
			
			String billtocodecondition="";
			String bookingAgentcondition="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }	
			Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
			if(user!=null && !user.equalsIgnoreCase("")  ){
				status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and followUpFor in ("+createdBy+") and updatedOn>'2017-01-01' "+billtocodecondition+""+bookingAgentcondition+"");
			}else{
				status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and updatedOn>'2017-01-01' "+billtocodecondition+""+bookingAgentcondition+"");	
			}
			//List status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"' and corpID='"+corpId+"' and (createdBy='"+createdBy+"' or forwardToUser='"+createdBy+"' or followUpFor='"+createdBy+"' or updatedBy='"+createdBy+"')");
			//status.size();
			//System.out.println("\n\n List Size-->>"+status.size());
			return status;
		}
	 
	 public List<Notes> findByStatus2ExtCordinator(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup) {
			Date filterdate = DateUtils.addDays(getcurrentDate(),0);
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String compareWithDateStr =dfm.format(filterdate);
			Company company = companyManager.findByCorpID(corpId).get(0);
			Date cal = new Date();
	        //logger.warn("system Date : "+cal);
	        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
	        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
	        String ss =formatter.format(cal);
	        Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
	        String billtocodecondition="";
			String bookingAgentcondition="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }	
	        //logger.warn("system Date After formate 2: "+ss);
			/*List status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate>'"+compareWithDateStr+"' and corpID='"+corpId+"'");*/
			List status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND  reminderStatus='"+reminderStatus+"' and forwarddate>date_format('"+ss+"','%y-%m-%d')  and corpID='"+corpId+"' and followUpFor in ("+createdBy+") "+billtocodecondition+""+bookingAgentcondition+"  ");
			return status;
		}
	 
	 public List<Notes> findByStatusForSupervisorExtCordinator(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup) {
			//String user = users.replaceAll("''", "'");String billtocodecondition="";
			String bookingAgentcondition="";
			String billtocodecondition=""; 
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }	
			Date filterdate = DateUtils.addDays(getcurrentDate(),0);
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String compareWithDateStr = dfm.format(filterdate);
			List status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate='"+compareWithDateStr+"'and corpID='"+corpId+"' and followUpFor in ("+users+") "+billtocodecondition+""+bookingAgentcondition+" ");
			//status.size();
			return status;
		}
	 public List<Notes> findByStatusForSupervisor1ExtCordinator(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup) {
			//String user = users.replaceAll("'", "");
		 String bookingAgentcondition="";
			String billtocodecondition=""; 
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }
			Date filterdate = DateUtils.addDays(getcurrentDate(),0);
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String compareWithDateStr = dfm.format(filterdate);
			List status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"' and followUpFor in ("+users+") "+billtocodecondition+""+bookingAgentcondition+"");
			//status.size();
			return status;
		}
	 
	 public List<Notes> findByStatusForSupervisor2ExtCordinator(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup) {
			//String user = users.replaceAll("'", "");
		 String bookingAgentcondition="";
			String billtocodecondition=""; 
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }
			Date filterdate = DateUtils.addDays(getcurrentDate(),0);
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String compareWithDateStr = dfm.format(filterdate);
			List status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate>'"+compareWithDateStr+"'and corpID='"+corpId+"' and followUpFor in ("+users+") "+billtocodecondition+""+bookingAgentcondition+"");
			//status.size();
			return status;
		}
	 
	 public List<Notes> findByStatusForSupervisorExtCordinatorForTeam(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup,String username,String allradio) {
			//String user = users.replaceAll("''", "'");String billtocodecondition="";
			String bookingAgentcondition="";
			String billtocodecondition=""; 
			List status;
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }	
			Date filterdate = DateUtils.addDays(getcurrentDate(),0);
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String compareWithDateStr = dfm.format(filterdate);
			String userName="'"+username+"'";
			String finaluser=userName+","+users;
			if(allradio.equalsIgnoreCase("Team"))
			{			
			 status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate='"+compareWithDateStr+"' and corpID='"+corpId+"' and followUpFor in ("+finaluser+")   "+billtocodecondition+""+bookingAgentcondition+"");
			//status.size();
			}
			else
			{			 status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate='"+compareWithDateStr+"' and corpID='"+corpId+"'    "+billtocodecondition+""+bookingAgentcondition+"");

				
			}
			return status;
		}
	 
	 public List<Notes> findByStatusForSupervisor1ExtCordinatorForTeam(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup,String username,String allradio) {
		 //String user = users.replaceAll("'", "");
		Date filterdate = DateUtils.addDays(getcurrentDate(),0);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr = dfm.format(filterdate);
		String bookingAgentcondition="";
		String billtocodecondition=""; 
		List status;
		if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
		{
			
			billtocodecondition= "and billToCode in ("+billToCodePopup+")";
		}
	
		if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
		{
			bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
        }	
		
		String userName="'"+username+"'";
		String finaluser=userName+","+users;
		if(allradio.equalsIgnoreCase("Team"))
		{
		 status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"' and followUpFor in ("+finaluser+")   "+billtocodecondition+""+bookingAgentcondition+"");
		
		//status.size();
		}
		else
		{
			 status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"' and corpID='"+corpId+"'    "+billtocodecondition+""+bookingAgentcondition+"");

		}
		return status;
		
	 
	 }
	 public List<Notes> findByStatusForSupervisor2ExtCordinatorForTeam(String reminderStatus, String corpId, String users,String bookingAgentPopup,String billToCodePopup,String username,String allradio) {
		 Date filterdate = DateUtils.addDays(getcurrentDate(),0);
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String compareWithDateStr = dfm.format(filterdate);
			String bookingAgentcondition="";
			String billtocodecondition=""; 
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }	
			List status;
			String userName="'"+username+"'";
			String finaluser=userName+","+users;
		 
		 
		 //String user = users.replaceAll("'", "");
			if(allradio.equalsIgnoreCase("Team"))
			{
			 status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate>'"+compareWithDateStr+"' and corpID='"+corpId+"' and followUpFor in ("+finaluser+")   "+billtocodecondition+""+bookingAgentcondition+"");
			}
			else
			{
				 status = getHibernateTemplate().find("from Notes where noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate>'"+compareWithDateStr+"' and corpID='"+corpId+"'  "+billtocodecondition+""+bookingAgentcondition+"");

			}
			//status.size();
			return status;
		}
	 
	
	 public List<ToDoResult> findBydurationForSupervisorExtCordinatorForTeam(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType) {
		 String bookingAgentcondition="";
			String billtocodecondition=""; 
			String query="";
			List resultList = new ArrayList();
			String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and tr.billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and tr.bookingAgentCode in ("+bookingAgentPopup+")";
	        }	
			if(userType.equalsIgnoreCase("ACCOUNT")){
				acc=" and isAccount is true ";
			}
			String userName="'"+username+"'";
			String finaluser=userName+","+owner;
			if(allradio.equalsIgnoreCase("Team"))
			{
				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) "+
				"where durationAddSub>'0' and tr.corpID='"+corpID+"' and owner in ("+finaluser+")  "+billtocodecondition+""+bookingAgentcondition+"  "+acc+" group by id";
				List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext())
			  {
				Object [] row=(Object[])it.next(); 
				ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
				if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
				if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
				if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
				if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
				if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
				if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
				if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
				if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
				if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
				if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
				if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
				if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
				if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
				if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
				if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
				if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
				if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
				if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
				if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
				if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
				if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
				if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
				if(row[22] == null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[22]);}
				if(row[23] == null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);}
				if(row[24] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[24]);}
				if(row[25] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[25]);}
				resultList.add(toDoRuleDTO);
			  }
			return resultList;
			}
			else
			{

				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) "+
				"where durationAddSub>'0' and tr.corpID='"+corpID+"'  "+billtocodecondition+""+bookingAgentcondition+"  "+acc+" group by id";
				List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext())
			  {
				Object [] row=(Object[])it.next(); 
				ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
				if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
				if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
				if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
				if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
				if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
				if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
				if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
				if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
				if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
				if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
				if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
				if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
				if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
				if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
				if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
				if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
				if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
				if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
				if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
				if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
				if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
				if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
				if(row[22] == null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[22]);}
				if(row[23] == null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);}
				if(row[24] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[24]);}
				if(row[25] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[25]);}
				resultList.add(toDoRuleDTO);
			  }
			return resultList;
			
			}
			}
	 public List<ToDoResult> findBydurationdtodayForSupervisorExtCordinatorForTeam(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType) {
			String billtocodecondition="";
			String bookingAgentcondition="";
			String query="";
			List resultList = new ArrayList();
			String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and tr.billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and tr.bookingAgentCode in ("+bookingAgentPopup+")";
	        }	
			if(userType.equalsIgnoreCase("ACCOUNT")){
				acc=" and isAccount is true ";
			}
			String userName="'"+username+"'";
			String finaluser=userName+","+owner;
			if(allradio.equalsIgnoreCase("Team"))
			{
		     //return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' and owner in ("+finaluser+") "+billtocodecondition+""+bookingAgentcondition+" group by id");

				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) "+
				"where durationAddSub='0' and tr.corpID='"+corpID+"' and owner in ("+finaluser+")  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by id";
				List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext())
			  {
				Object [] row=(Object[])it.next(); 
				ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
				if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
				if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
				if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
				if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
				if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
				if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
				if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
				if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
				if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
				if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
				if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
				if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
				if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
				if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
				if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
				if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
				if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
				if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
				if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
				if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
				if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
				if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
				if(row[22] == null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[22]);}
				if(row[23] == null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);}
				if(row[24] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[24]);}
				if(row[25] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[25]);}
				resultList.add(toDoRuleDTO);
			  }
			return resultList;
			
			}
			else
			{
				
				// return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' "+billtocodecondition+""+bookingAgentcondition+" group by id");


				query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
				"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) "+
				"where durationAddSub='0' and tr.corpID='"+corpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by id";
				List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext())
			  {
				Object [] row=(Object[])it.next(); 
				ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
				if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
				if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
				if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
				if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
				if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
				if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
				if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
				if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
				if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
				if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
				if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
				if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
				if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
				if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
				if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
				if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
				if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
				if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
				if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
				if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
				if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
				if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
				if(row[22] == null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[22]);}
				if(row[23] == null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);}
				if(row[24] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[24]);}
				if(row[25] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[25]);}
				resultList.add(toDoRuleDTO);
			  }
			return resultList;
			
			
			}
			}
	 
	 
	 public List<ToDoResult> findBydurationdueweekForSupervisorExtCordinatorForTeam(String corpID, String owner,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType) {
		 String billtocodecondition="";
			String bookingAgentcondition="";
			String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }	
			if(userType.equalsIgnoreCase("ACCOUNT")){
				acc=" and isAccount is true ";
			}
			String userName="'"+username+"'";
			String finaluser=userName+","+owner;
			if(allradio.equalsIgnoreCase("Team"))
			{
		 return getHibernateTemplate().find("from ToDoResult where durationAddSub<'0' and corpID='"+corpID+"' and owner in ("+finaluser+") "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by id");
			}
			else
			{
				 return getHibernateTemplate().find("from ToDoResult where durationAddSub<'0' and corpID='"+corpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by id");

			}
			}
		public List findByAgentTdrEmailExtCordinatorForTeam(String corpID,String owner,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType ){
			//return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' group by id");
			List resultList = new ArrayList();
			String query="";
			String billtocodecondition="";
			String bookingAgentcondition="";
			String userName="'"+username+"'";
			String finaluser=userName+","+owner;
			String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and tr.billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and tr.bookingAgentCode in ("+bookingAgentPopup+")";
	        }
			if(userType.equalsIgnoreCase("ACCOUNT")){
				acc=" and isAccount is true ";
			}
			if(allradio.equalsIgnoreCase("Team"))
			{
			
			if(owner!=null && !owner.equals("") ){
				query="select tr.id,tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed,tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType,tr.noteSubType,tr.billToName ,tr.ruleNumber, tr.agentName,tr.emailNotification,tr.fieldToValidate2 " +
				"  from todoresult tr  where  lpad(tr.fileNumber,4,'')='"+corpID+"' and tr.corpID='TSFT' and owner in ("+finaluser+")   "+billtocodecondition+""+bookingAgentcondition+" "+acc+" ";
			}else{
				query="select tr.id,tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed,tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType,tr.noteSubType,tr.billToName ,tr.ruleNumber,tr.agentName,tr.emailNotification,tr.fieldToValidate2 " +
				"  from todoresult tr  where  lpad(tr.fileNumber,4,'')='"+corpID+"' and tr.corpID='TSFT'    "+billtocodecondition+""+bookingAgentcondition+" "+acc+" ";
			}}
	        else
	        {
	        	if(owner!=null && !owner.equals("")){
					query="select tr.id,tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed,tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType,tr.noteSubType,tr.billToName ,tr.ruleNumber, tr.agentName,tr.emailNotification,tr.fieldToValidate2 " +
					"  from todoresult tr  where  lpad(tr.fileNumber,4,'')='"+corpID+"' and tr.corpID='TSFT'    "+billtocodecondition+""+bookingAgentcondition+" "+acc+" ";
				}else{
					query="select tr.id,tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed,tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType,tr.noteSubType,tr.billToName ,tr.ruleNumber,tr.agentName,tr.emailNotification,tr.fieldToValidate2 " +
					"  from todoresult tr  where  lpad(tr.fileNumber,4,'')='"+corpID+"' and tr.corpID='TSFT'    "+billtocodecondition+""+bookingAgentcondition+"  "+acc+" ";
				}	
	        	
	        }
			List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext())
			  {
				Object [] row=(Object[])it.next(); 
				ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
				if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
				if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
				if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
				if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
				if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
				if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
				if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
				if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
				if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
				if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
				if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
				if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
				if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
				if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
				if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
				if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
				if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
				if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
				if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
				if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
				/*if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
				if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}*/
				if(row[20] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[20]);}
				if(row[21] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[21]);}
				if(row[22]== null){toDoRuleDTO.setAgentName("");}else{toDoRuleDTO.setAgentName(row[22]);}
				if(row[23]== null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);
				if(row[24]== null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[24]);}
				
				}
				resultList.add(toDoRuleDTO);
			  }
			return resultList;
		}
	 public List getOwnersListExtCordinatorForTeam(String sessionCorpID ,String username,Long userId,String userFirstName,String userType,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) {
			List userList=new ArrayList();
			List tempuserfilter=new ArrayList();
			String billtocodecondition="";
			String bookingAgentcondition="";
			String userName="'"+loggedInusername+"'";
			String finaluser=userName+","+username;
			String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }
			if(userType.equalsIgnoreCase("ACCOUNT")){
				acc=" and isAccount is true ";
			}
				String query="";
				if(allradio.equalsIgnoreCase("Team"))
				{

			    query = "select distinct owner from todoresult where corpID='"+sessionCorpID+"'   and owner<>'' and owner in ("+finaluser+") and  owner is not null "+billtocodecondition+""+bookingAgentcondition+" "+acc+" union select distinct UPPER(owner) from checklistresult where corpID='"+sessionCorpID+"'  and owner<>'' and owner in ("+finaluser+") and  owner is not null   "+billtocodecondition+""+bookingAgentcondition+" "+acc+" union select distinct followUpFor as owner from notes where id>(select id from notes_start_id) and corpID='"+sessionCorpID+"' and (noteStatus in('NEW','FWD','PEND') or noteStatus is null) and followUpFor is not null and followUpFor<>'' and reminderstatus = 'NEW' and forwardDate is not null and followUpFor<>'' and followUpFor in ("+finaluser+") and  followUpFor is not null "+billtocodecondition+""+bookingAgentcondition+" group by owner order by owner";
				}
				else
				{
					
				    query = "select distinct owner from todoresult where corpID='"+sessionCorpID+"' and  owner is not null  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" union select distinct UPPER(owner) from checklistresult where corpID='"+sessionCorpID+"'  and  owner is not null   "+billtocodecondition+""+bookingAgentcondition+" "+acc+"  union select distinct followUpFor as owner from notes where id>(select id from notes_start_id) and corpID='"+sessionCorpID+"' and (noteStatus in('NEW','FWD','PEND') or noteStatus is null) and followUpFor is not null and followUpFor<>'' and reminderstatus = 'NEW' and forwardDate is not null "+billtocodecondition+""+bookingAgentcondition+" group by owner order by owner";
				}
				
				List tempuserList = this.getSession().createSQLQuery(query).list();
				
				Iterator it=tempuserList.iterator();
				while(it.hasNext()){
				Object value=it.next();
			    userList.add(value);
				
				}	
			
			return 	userList;
			//return getHibernateTemplate().find(query);
		}
	 
	  public List getMessageListExtCordinatorForTeam(String sessionCorpID, String userName,Long userId,String userType,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) {
			List ls = new ArrayList();
			List lsCheckList= new ArrayList();
			List tempuserfilter=new ArrayList();
			String billtocodecondition="";
			String bookingAgentcondition="";
			String username="'"+loggedInusername+"'";
			String finaluser=username+","+userName;
			String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }
			if(userType.equalsIgnoreCase("ACCOUNT")){
				acc=" and isAccount is true ";
			}
				String queryfilter="";
				if(allradio.equalsIgnoreCase("Team"))
				{
							ls=this.getSession().createSQLQuery("select distinct messagedisplayed from todoresult where  owner in ("+finaluser+") and corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+" "+acc+" union select distinct messagedisplayed from checklistresult where corpID='"+sessionCorpID+"' and  owner in ("+finaluser+") "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by messagedisplayed").list();
				}
				else
				{
					ls=this.getSession().createSQLQuery("select distinct messagedisplayed from todoresult where   corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+" "+acc+" union select distinct messagedisplayed from checklistresult where corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by messagedisplayed").list();

					
					
				}
			
			return ls;
		}
	    
	  public List getRuleIdListExtCordinatorForTeam(String sessionCorpID ,String username,Long userId,String userType,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) {
			List ls = new ArrayList();
			List tempuserfilter=new ArrayList();
			String billtocodecondition="";
			String bookingAgentcondition="";
			String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }
			if(userType.equalsIgnoreCase("ACCOUNT")){
				acc=" and isAccount is true ";
			}
			String userName="'"+loggedInusername+"'";
			String finaluser=userName+","+username;
			if(allradio.equalsIgnoreCase("Team"))
			{
						ls=this.getSession().createSQLQuery("select ruleNumber from todoresult where owner in("+finaluser+") and corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by ruleNumber union select Cast(ruleNumber as DECIMAL) from checklistresult where corpID='"+sessionCorpID+"' and  owner in("+finaluser+") "+billtocodecondition+""+bookingAgentcondition+" "+acc+"  group by ruleNumber order by ruleNumber").list();
			}
			else
				
			{
				ls=this.getSession().createSQLQuery("select ruleNumber from todoresult where  corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by ruleNumber union select Cast(ruleNumber as DECIMAL) from checklistresult where corpID='"+sessionCorpID+"'"+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by ruleNumber order by ruleNumber").list();

			}
			
			return ls;
		}
	  
	  public List getSupervisorForTeam(String username, String corpID) {
			return getHibernateTemplate().find("select concat('''',supervisor,'''') from User where username='"+username+"' and corpID='"+corpID+"'");
		}
	  
	  
	  public List<Notes> findByStatus1ExtCordinatorForTeam(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) {		
			String user = createdBy.replaceAll("'", "");
			 Date filterdate = DateUtils.addDays(getcurrentDate(),0);
				DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
				String compareWithDateStr =dfm.format(filterdate);
				Company company = companyManager.findByCorpID(corpId).get(0);
				Date cal = new Date();
		        //logger.warn("system Date : "+cal);
		        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
		        String ss =formatter.format(cal);
		        logger.warn("system Date After formate : "+ss);
				List status = new ArrayList();
				String username="'"+loggedInusername+"'";
				String finaluser=username+","+createdBy;
				/*if(createdBy!=null && !createdBy.equalsIgnoreCase("") && !createdBy.equalsIgnoreCase("UNASSIGNED") ){
					status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"' and corpID='"+corpId+"' and followUpFor='"+createdBy+"'");
				}else if(createdBy!=null && createdBy.equalsIgnoreCase("UNASSIGNED")){
					//String temp="select * from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"' and (followUpFor='' or followUpFor is null )";
					status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"' and (followUpFor='' or followUpFor is null ) " );
				}else{
					status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"'and corpID='"+corpId+"'");	
				}*/
				
				String billtocodecondition="";
				String bookingAgentcondition="";
				if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
				{
					
					billtocodecondition= "and billToCode in ("+billToCodePopup+")";
				}
			
				if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
				{
					bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
		        }	
				Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
				if(allradio.equalsIgnoreCase("Team"))
				{
				if(user!=null && !user.equalsIgnoreCase("")){
					status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and followUpFor in ("+finaluser+") and updatedOn>'2017-01-01' "+billtocodecondition+""+bookingAgentcondition+"");
				}else{
					status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and updatedOn>'2017-01-01' "+billtocodecondition+""+bookingAgentcondition+"");	
				}
				}
				else
				{
					if(user!=null && !user.equalsIgnoreCase("")  ){
						status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and followUpFor in ("+finaluser+") and updatedOn>'2017-01-01' "+billtocodecondition+""+bookingAgentcondition+"");
					}else{
						status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate<date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and updatedOn>'2017-01-01' "+billtocodecondition+""+bookingAgentcondition+"");	
					}
					
				}
				//List status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate<'"+compareWithDateStr+"' and corpID='"+corpId+"' and (createdBy='"+createdBy+"' or forwardToUser='"+createdBy+"' or followUpFor='"+createdBy+"' or updatedBy='"+createdBy+"')");
				//status.size();
				//System.out.println("\n\n List Size-->>"+status.size());
				return status;
			}
	  public List<Notes> findByStatus2ExtCordinatorForTeam(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) {
			Date filterdate = DateUtils.addDays(getcurrentDate(),0);
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String compareWithDateStr =dfm.format(filterdate);
			Company company = companyManager.findByCorpID(corpId).get(0);
			Date cal = new Date();
	        //logger.warn("system Date : "+cal);
	        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
	        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
	        String ss =formatter.format(cal);
	        Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
	        String billtocodecondition="";
			String bookingAgentcondition="";
			String username="'"+loggedInusername+"'";
			String finaluser=username+","+createdBy;
			List status ;
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= "and billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
	        }	
	        //logger.warn("system Date After formate 2: "+ss);
			/*List status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate>'"+compareWithDateStr+"' and corpID='"+corpId+"'");*/
			if(allradio.equalsIgnoreCase("Team"))
			{
			 status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND  reminderStatus='"+reminderStatus+"' and forwarddate>date_format('"+ss+"','%y-%m-%d')  and corpID='"+corpId+"' and followUpFor in ("+finaluser+") "+billtocodecondition+""+bookingAgentcondition+"");
			
			}
			else
			{
				 status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND  reminderStatus='"+reminderStatus+"' and forwarddate>date_format('"+ss+"','%y-%m-%d')  and corpID='"+corpId+"' "+billtocodecondition+""+bookingAgentcondition+"");

				
			}
			return status;
		}
	  public List<ToDoResult> findBydurationUserExtCordinatorForTeam(String corpID, String owner, String messageDisplayed, String ruleId, String shipper,User appUser,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) {
			if(messageDisplayed==null){	messageDisplayed="";}
			if(ruleId==null){ruleId="";}
			if(shipper==null){shipper="";}
		    //return getHibernateTemplate().find("from ToDoResult where durationAddSub>'0' and corpID='"+corpID+"' and owner like '"+owner+"%' and messagedisplayed like '"+messageDisplayed+"%' and todoRuleId like '"+ruleId+"%' group by id");
			List resultList = new ArrayList();
			String query="";
			String billtocodecondition="";
			String bookingAgentcondition="";
			String username="'"+loggedInusername+"'";
			String finaluser=username+","+owner;
			String owners = owner.replaceAll("'", "");
			String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= " and tr.billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= "and tr.bookingAgentCode in ("+bookingAgentPopup+")";
	        }
			if(appUser.getUserType().equalsIgnoreCase("ACCOUNT")){
				acc=" and isAccount is true ";
			}
			if(allradio.equalsIgnoreCase("Team"))
			{
				//plesse check this one
			if(ruleId.equalsIgnoreCase("")){
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+" "+acc+" "  ;
					 if(owners!=null && !owners.equals("")){
						query=query+"and tr.owner in ("+finaluser+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}else{
						query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%'  group by tr.id,tr.ruleNumber";	
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+" "+acc+" " ;
					 if(owners!=null && !owners.equals("")){
						query=query+"and tr.owner in ("+finaluser+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}else{
						query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";	
				}
			
			}else{
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+" "+acc+"  " ;
					 if(owners!=null && !owners.equals("")){
						query=query+"and tr.owner in ("+finaluser+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}else{
						query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}
					query=query+" and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber = '"+ruleId+"%'  group by tr.id,tr.ruleNumber  ";	
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" ";
					if(owners!=null && !owners.equals("")){
						query=query+"and tr.owner in ("+finaluser+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}else{
						query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber = '"+ruleId+"%'  and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";
					}
				}}
			else
			{

				//plesse check this one
			if(ruleId.equalsIgnoreCase("")){
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+"  "+acc+" "  ;
					if(owners!=null && !owners.equals("")){
						query=query+" tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}else{
						query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%'  group by tr.id,tr.ruleNumber";	
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+" "+acc+"  " ;
					 if(owners!=null && !owners.equals("")){
						query=query+" and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}else{
						query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";	
				}
			
			}else{
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+"  "+acc+" " ;
					
						query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					
					query=query+" and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber = '"+ruleId+"%'  group by tr.id,tr.ruleNumber  ";	
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note),tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and  n.noteStatus not in('CNCL','CMP') and (tr.noteSubType = n.noteSubType or n.noteSubType='ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub>'0' and tr.corpID='"+corpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" ";
					 if(owners!=null && !owners.equals("")){
						query=query+"and and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}else{
						query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber = '"+ruleId+"%'  and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";
					}
				}	
				
				
				
			}
			
			List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext())
			  {
				Object [] row=(Object[])it.next(); 
				ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
				if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
				if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
				if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
				if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
				if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
				if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
				if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
				if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
				if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
				if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
				if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
				if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
				if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
				if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
				if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
				if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
				if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
				if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
				if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
				if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
				if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
				if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
				if(row[22] == null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[22]);}
				if(row[23] == null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);}
				if(row[24] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[24]);}
				if(row[25] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[25]);}
				resultList.add(toDoRuleDTO);
			  }
			return resultList;
		}
	  
	  public List<ToDoResult> findBydurationdtodayUserExtCordinatorForTeam(String corpID, String owner, String messageDisplayed, String ruleId, String shipper,User appUser,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String allradio) {
			if(messageDisplayed==null){	messageDisplayed="";}
			if(ruleId==null){ruleId="";}
			if(shipper==null){shipper="";}
			//return getHibernateTemplate().find("from ToDoResult where durationAddSub='0' and corpID='"+corpID+"' and owner like '"+owner+"%' and messagedisplayed like '"+messageDisplayed+"%' and todoRuleId like '"+ruleId+"%' group by id");
			List resultList = new ArrayList();
			String query="";
			String billtocodecondition="";
			String bookingAgentcondition="";
			String username="'"+loggedInusername+"'";
			String finaluser=username+","+owner;
			 String user = owner.replaceAll("'", "");
			 String acc="";
			if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
			{
				
				billtocodecondition= " and tr.billToCode in ("+billToCodePopup+")";
			}
		
			if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
			{
				bookingAgentcondition= " and tr.bookingAgentCode in ("+bookingAgentPopup+")";
	        }
			if(appUser.getUserType().equalsIgnoreCase("ACCOUNT")){
				acc=" and isAccount is true ";
			}
			if(ruleId.equalsIgnoreCase("")){
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+" "+acc+" " ;
					 if(user!=null && !user.equals("")){
						query=query+"and tr.owner in ("+finaluser+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}else{
						query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%'  group by tr.id,tr.ruleNumber";
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName ,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+" "+acc+" " ;
					 if(user!=null && !user.equals("")){
						query=query+"and tr.owner in ("+finaluser+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}else{
						query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";
				}
				
			}else{
				if(shipper.equalsIgnoreCase("")){
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+" "+acc+" " ;
					 if(user!=null && !user.equals("")){
						query=query+"and tr.owner in ("+finaluser+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}else{
						query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}
					query=query+"and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%'  group by tr.id,tr.ruleNumber";		
				}else{
					query="select tr.id, tr.owner, tr.url,tr.corpID, tr.durationAddSub, tr.fieldToValidate1, tr.messagedisplayed, tr.testdate, tr.resultRecordId, tr.resultRecordType, tr.todoRuleId, tr.shipper, tr.rolelist, tr.checkedUserName, tr.supportingId, tr.fieldDisplay, tr.fileNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType, n.id as nid, if(n.note='',n.subject,n.note) ,tr.fieldToValidate2, tr.emailNotification,tr.billToName,tr.ruleNumber" +
					"  from todoresult tr left outer join notes n on tr.resultRecordId = n.notesKeyId and (tr.noteType = n.noteType or n.noteType='Activity') and n.noteStatus not in('CNCL','CMP')  and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement') and n.corpID in ('"+corpID+"' ,'TSFT') and (tr.todoRuleId=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) where tr.durationAddSub='0' and tr.corpID='"+corpID+"'   "+billtocodecondition+""+bookingAgentcondition+" "+acc+" " ;
					if(user!=null && !user.equals("")){
						query=query+" and tr.owner in ("+finaluser+") and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}else{
					    query=query+"and tr.owner !='AgentPortal' and tr.owner !='PartnerPortal' " ;	
					}
					query=query+" and tr.messagedisplayed like '"+messageDisplayed+"%' and tr.ruleNumber= '"+ruleId+"%' and shipper like '"+shipper.replaceAll("'", "\\\\'")+"%'  group by tr.id,tr.ruleNumber";	
				}
				
				}
			
			
			List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext())
			  {
				Object [] row=(Object[])it.next(); 
				ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
				if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
				if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
				if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
				if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
				if(row[4] == null){toDoRuleDTO.setDurationAddSub("");}else{toDoRuleDTO.setDurationAddSub(row[4]);}
				if(row[5] == null){toDoRuleDTO.setFieldToValidate1("");}else{toDoRuleDTO.setFieldToValidate1(row[5]);}
				if(row[6] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[6]);}
				if(row[7] == null){toDoRuleDTO.setTestdate("");}else{toDoRuleDTO.setTestdate(row[7]);}
				if(row[8] == null){toDoRuleDTO.setResultRecordId("");}else{toDoRuleDTO.setResultRecordId(row[8]);}
				if(row[9] == null){toDoRuleDTO.setResultRecordType("");}else{toDoRuleDTO.setResultRecordType(row[9]);}
				if(row[10] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[10]);}
				if(row[11] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[11]);}
				if(row[12] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[12]);}
				if(row[13] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[13]);}
				if(row[14] == null){toDoRuleDTO.setSupportingId("");}else{toDoRuleDTO.setSupportingId(row[14]);}
				if(row[15] == null){toDoRuleDTO.setFieldDisplay("");}else{toDoRuleDTO.setFieldDisplay(row[15]);}
				if(row[16] == null){toDoRuleDTO.setFileNumber("");}else{toDoRuleDTO.setFileNumber(row[16]);}
				if(row[17] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
				if(row[18] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[18]);}
				if(row[19] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[19]);}
				if(row[20] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[20]);}
				if(row[21] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[21]);}
				if(row[22] == null){toDoRuleDTO.setFieldToValidate2("");}else{toDoRuleDTO.setFieldToValidate2(row[22]);}
				if(row[23] == null){toDoRuleDTO.setEmailNotification("");}else{toDoRuleDTO.setEmailNotification(row[23]);}
				if(row[24] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[24]);}
				if(row[25] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[25]);}
				resultList.add(toDoRuleDTO);
			  }
			return resultList;
		}
	  
	  public List<Notes> findByStatusExtCordinatorForTeam(String reminderStatus,String corpId,String createdBy,String bookingAgentPopup,String billToCodePopup,String allradio) {
			 String user = createdBy.replaceAll("'", "");	
			 Date filterdate = DateUtils.addDays(getcurrentDate(),0);
				DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
				String compareWithDateStr =dfm.format(filterdate);
				Company company = companyManager.findByCorpID(corpId).get(0);
				Date cal = new Date();
		        //logger.warn("system Date : "+cal);
		        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
		        String ss =formatter.format(cal);
		        String billtocodecondition="";
		        List status = new ArrayList();
				String bookingAgentcondition="";
				if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
				{
					
					billtocodecondition= " and billToCode in ("+billToCodePopup+")";
				}
			
				if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
				{
					bookingAgentcondition= " and bookingAgentCode in ("+bookingAgentPopup+")";
		        }	        //logger.warn("system Date After formate : "+ss);
				//List status = getHibernateTemplate().find("from Notes where reminderStatus='"+reminderStatus+"' and forwarddate='"+compareWithDateStr+"'and corpID='"+corpId+"' and (createdBy='"+createdBy+"' or forwardToUser='"+createdBy+"' or followUpFor='"+createdBy+"' or updatedBy='"+createdBy+"')");
				if(allradio.equalsIgnoreCase("Team"))
				{
				
				Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
				 status = new ArrayList();
				if(user!=null && !user.equalsIgnoreCase("")){
					status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate=date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' and followUpFor in ("+createdBy+") "+billtocodecondition+""+bookingAgentcondition+"");
				}else{
					status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and  noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate=date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' "+billtocodecondition+""+bookingAgentcondition+" ");
				}}
				else
				{

					
					Long noteStartId = Long.parseLong(this.getSession().createSQLQuery("select id from notes_start_id").list().get(0).toString());
			         status = new ArrayList();
					if(user!=null && !user.equalsIgnoreCase("")){
						status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate=date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"'  "+billtocodecondition+""+bookingAgentcondition+"");
					}else{
						status = getHibernateTemplate().find("from Notes where id>"+noteStartId+" and  noteStatus not in('CNCL','CMP') AND reminderStatus='"+reminderStatus+"' and forwarddate=date_format('"+ss+"','%y-%m-%d') and corpID='"+corpId+"' "+billtocodecondition+""+bookingAgentcondition+" ");
					}
					
				}
				//status.size();
				return status;
			}
	  public List getToDoRulesSummaryForGroupByRules(String sessionCorpID,String userLogged,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String userType) {
			List resultList = new ArrayList();
			String userName="'"+loggedInusername+"'";
			String finaluser="";
			if(userName!="''" && userName.equalsIgnoreCase(""))
			{
			 finaluser=userName+","+userLogged;
			}
			else
			{
				 finaluser=userLogged;
			}
				String query="";
				String billtocodecondition="";
				String bookingAgentcondition="";
				String acc="";
				if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
				{
					
					billtocodecondition= "and billToCode in ("+billToCodePopup+")";
				}
			
				if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
				{
					bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
		        }
				if(userType.equalsIgnoreCase("ACCOUNT")){
					acc=" and isAccount is true ";
				}
					query="select t.todoRuleId, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
					"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',t.messagedisplayed, t.ruleNumber  " +
					"from todoresult t where t.corpID = '"+sessionCorpID+"' and t.owner is not null and t.owner in ("+finaluser+")  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" " +
					" group by t.todoRuleId order by 4 desc ";
				
				List list= this.getSession().createSQLQuery(query).list();
				Iterator it=list.iterator();
				while(it.hasNext())
				  {
					Object [] row=(Object[])it.next(); 
					ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
					if(row[0] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[0]);}
					if(row[1] == null){toDoRuleDTO.setCurrent("");}else{toDoRuleDTO.setCurrent(row[1]);}
					if(row[2] == null){toDoRuleDTO.setOverDue("");}else{toDoRuleDTO.setOverDue(row[2]);}
					if(row[3] == null){toDoRuleDTO.setTotal("");}else{toDoRuleDTO.setTotal(row[3]);}
					if(row[4] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[4]);}
					if(row[5] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[5]);}
					resultList.add(toDoRuleDTO);
				  }
				return resultList;
	  }
			
	  public List getToDoRulesPersonSummaryForGroupByPerson(String sessionCorpID,String userLogged,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String userType) {
			List resultList = new ArrayList();
			
				String query="";
				String billtocodecondition="";
				String bookingAgentcondition="";
				String acc="";
				String finaluser="";
				String userName="'"+loggedInusername+"'";
				if(userName!="''" && userName.equalsIgnoreCase(""))
				{
				 finaluser=userName+","+userLogged;
				}
				else
				{
					 finaluser=userLogged;
				}
				if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
				{
					
					billtocodecondition= "and billToCode in ("+billToCodePopup+")";
				}
			
				if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
				{
					bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
		        }
				if(userType.equalsIgnoreCase("ACCOUNT")){
					acc=" and isAccount is true ";
				}
			
					query="select t.owner, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
					"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',t.messagedisplayed  " +
					"from todoresult t  where t.corpID = '"+sessionCorpID+"' and t.owner is not null and t.owner in ("+finaluser+")  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" " +
					" group by t.owner order by 4 desc ";
				
				List list= this.getSession().createSQLQuery(query).list();
				Iterator it=list.iterator();
				while(it.hasNext())
				  {
					Object [] row=(Object[])it.next(); 
					ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
					if(row[0] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[0]);}
					if(row[1] == null){toDoRuleDTO.setCurrent("");}else{toDoRuleDTO.setCurrent(row[1]);}
					if(row[2] == null){toDoRuleDTO.setOverDue("");}else{toDoRuleDTO.setOverDue(row[2]);}
					if(row[3] == null){toDoRuleDTO.setTotal("");}else{toDoRuleDTO.setTotal(row[3]);}
					if(row[4] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[4]);}
					
					resultList.add(toDoRuleDTO);
				  }
			
			
			return resultList;
		}
	  public List getToDoRulesShipperSummaryForGroupByShipper(String sessionCorpID,String userLogged,String bookingAgentPopup,String billToCodePopup,String loggedInusername,String userType) {

		  List resultList = new ArrayList();
			
				String query="";
				String billtocodecondition="";
				String bookingAgentcondition="";
				String acc="";
				String finaluser="";
				String userName="'"+loggedInusername+"'";
				if(userName!="''" && userName.equalsIgnoreCase(""))
				{
				 finaluser=userName+","+userLogged;
				}
				else
				{
					 finaluser=userLogged;
				}
				if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
				{
					
					billtocodecondition= "and billToCode in ("+billToCodePopup+")";
				}
			
				if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
				{
					bookingAgentcondition= "and bookingAgentCode in ("+bookingAgentPopup+")";
		        }
				if(userType.equalsIgnoreCase("ACCOUNT")){
					acc=" and isAccount is true ";
				}
			
					query="select t.todoRuleId, sum(if(durationaddsub = 0, 1, 0)) 'Current' ," +
					"sum(if(durationaddsub = 0, 0, 1)) 'Overdue', count(*) 'Total',t.messagedisplayed, t.ruleNumber,t.shipper, t.id  " +
					"from todoresult t  where t.corpID = '"+sessionCorpID+"' and t.owner is not null and t.owner in ("+finaluser+")  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" " +
					" and shipper is not null group by shipper order by trim(shipper) ";
				
				List list= this.getSession().createSQLQuery(query).list();
				Iterator it=list.iterator();
				while(it.hasNext())
				  {
					Object [] row=(Object[])it.next(); 
					ToDoRuleDTO  toDoRuleDTO= new ToDoRuleDTO();
					if(row[0] == null){toDoRuleDTO.setTodoRuleId("");}else{toDoRuleDTO.setTodoRuleId(row[0]);}
					if(row[1] == null){toDoRuleDTO.setCurrent("");}else{toDoRuleDTO.setCurrent(row[1]);}
					if(row[2] == null){toDoRuleDTO.setOverDue("");}else{toDoRuleDTO.setOverDue(row[2]);}
					if(row[3] == null){toDoRuleDTO.setTotal("");}else{toDoRuleDTO.setTotal(row[3]);}
					if(row[4] == null){toDoRuleDTO.setMessagedisplayed("");}else{toDoRuleDTO.setMessagedisplayed(row[4]);}
					if(row[5] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[5]);}
					if(row[6] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[6]);}
					if(row[7] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[7]);}
					resultList.add(toDoRuleDTO);
				  }
			
			
			return resultList;
		}
	 
	  
	  
}


 