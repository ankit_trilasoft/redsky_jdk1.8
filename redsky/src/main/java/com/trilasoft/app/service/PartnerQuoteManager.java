
package com.trilasoft.app.service;

import java.util.List;
import java.math.BigDecimal;  

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.PartnerQuote;

public interface PartnerQuoteManager extends GenericManager<PartnerQuote, Long> {   
	public List findMaximumId();
	public int updateQuoteStatus(String mail, String reqSO );
	public int updateQuoteReminder(String mail );
	public List findPartnerCode(String partnerCode);
	
	public List <PartnerQuote> findByStatus(String quoteStatus,String sessionCorpID, String vendorCodeSet );
	public List <PartnerQuote> findByStatusSubmitted(String quoteStatus,String searchByCode,String searchByName,String shipNumber);
	//public List findByTicket(String itemNumber, String locationId, String jobNumber);
	//public List findByTicket(String itemNumber, String jobNumber);
	public List findByVendorEmail(String email, String seqNum);
	
// 	@@@@@@@@@@@@@@@@@@@@@ By Madhu - For Quation Management @@@@@@@@@@@@@@@@@@@@@@@@
	public List findByQuoteType(String quoteType,String sequenceNumber);
	public List findBySequenceNumber(String sequenceNumber);
// 	@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	public List getGeneralListVendor(String searchByCode,String searchByName,String searchByCountryCode,String searchByState,String searchByCity);
	
	public List  findServiceOrders(String requestedSO,String sessionCorpID);
	public List vendorNamenEmail(String partnerCode);
	public List findSoList(String sequenceNumber, String corpId);
	public List findSoDetail(String shipNumber, String corpId);
}

