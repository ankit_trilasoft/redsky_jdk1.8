package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@Entity
@Table(name="pricingfreight")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class PricingFreight {
	
	private Long id;
	private String corpID;
	private Long pricingControlID;
	private Long reffreightId;
	private Boolean freightSelection;
	private BigDecimal freightPrice;
	private String originPortCode;
	private String destinationPortCode;
	private BigDecimal freightMarkup;
	
	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("pricingControlID", pricingControlID).append(
				"reffreightId", reffreightId).append("freightSelection",
				freightSelection).append("freightPrice", freightPrice).append(
				"originPortCode", originPortCode).append("destinationPortCode",
				destinationPortCode).append("freightMarkup", freightMarkup)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PricingFreight))
			return false;
		PricingFreight castOther = (PricingFreight) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(pricingControlID,
				castOther.pricingControlID).append(reffreightId,
				castOther.reffreightId).append(freightSelection,
				castOther.freightSelection).append(freightPrice,
				castOther.freightPrice).append(originPortCode,
				castOther.originPortCode).append(destinationPortCode,
				castOther.destinationPortCode).append(freightMarkup,
				castOther.freightMarkup).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				pricingControlID).append(reffreightId).append(freightSelection)
				.append(freightPrice).append(originPortCode).append(
						destinationPortCode).append(freightMarkup).append(
						createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).toHashCode();
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Boolean getFreightSelection() {
		return freightSelection;
	}
	public void setFreightSelection(Boolean freightSelection) {
		this.freightSelection = freightSelection;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Long getPricingControlID() {
		return pricingControlID;
	}
	public void setPricingControlID(Long pricingControlID) {
		this.pricingControlID = pricingControlID;
	}
	@Column
	public Long getReffreightId() {
		return reffreightId;
	}
	public void setReffreightId(Long reffreightId) {
		this.reffreightId = reffreightId;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public BigDecimal getFreightPrice() {
		return freightPrice;
	}
	public void setFreightPrice(BigDecimal freightPrice) {
		this.freightPrice = freightPrice;
	}
	public String getDestinationPortCode() {
		return destinationPortCode;
	}
	public void setDestinationPortCode(String destinationPortCode) {
		this.destinationPortCode = destinationPortCode;
	}
	public String getOriginPortCode() {
		return originPortCode;
	}
	public void setOriginPortCode(String originPortCode) {
		this.originPortCode = originPortCode;
	}
	public BigDecimal getFreightMarkup() {
		return freightMarkup;
	}
	public void setFreightMarkup(BigDecimal freightMarkup) {
		this.freightMarkup = freightMarkup;
	}
	
}
