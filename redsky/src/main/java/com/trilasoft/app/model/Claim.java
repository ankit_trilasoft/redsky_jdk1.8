/**
 * This class represents the basic "Claim" object in Redsky that allows for Claim management.
 * @Class Name	Claim
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */
package com.trilasoft.app.model;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "claim")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class Claim extends BaseObject implements ListLinkData { //Extended to implement ListLinkData interface
	private String corpID;
	private int claimNumber1;
	private Long claimNumber;           
	private String lastName;               
	private String firstName;       
	private String shipNumber;
	private String sequenceNumber;
	private String claimCertificateNumber;   
	private Date closeDate;
	private Date dateOfInspection;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String city;
	private String cityCode;
	private String state;
	private String zip;
	private String phone;
	private String fax;
	private Date formSent;
	private Date formRecived;
	private Date inspecteDate;
	private Date insurerNotification;  
	private Date insurerAcknowledgement;           
	private Date notifyShipper;            
	private String country;
	private String countryCode;
	private String shipperWorkPhone;
	private BigDecimal premium;     
	private String insurerCode;
	private String insurer;
	private String paidByName;
	private String registrationNumber;
	private Date cancelled;
	private String crossReference;
	private String email; 
	private String email2;
	private Date requestReimbursement;
	private Date noProcess;
	private BigDecimal reimbursement;
	private BigDecimal reciveReimbursement;
	/* Ticket Number: 6054*/
	private String reciveReimbursementCurrency;
	private String reimbursementCurrency;
	/* Closed */
	/* Ticket Number: 6006 */
	private String claimPerson;
	/* Closed */
	private Boolean insuranceUser; /* Field Added by kunal sharma for ticket number: 6053*/ 
	private String idNumber; // Add for linking and sync 
	private Date requestForm;
	private Date uvlRecived;
	private BigDecimal uvlAmount;
	private Date toVendor;
	private Long id;
	private Long serviceOrderId;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private Date insuranceCompanyPaid;
	private Boolean cportalAccess;
	//private Boolean declaredValue1;
	//private Boolean declaredValue2;
	//private Boolean declaredValue3;
	private String fullValueProtection;
	private Boolean signature;
	private Date signatutreDate;
	private String remarks;
	private String submitFlag;
	private String status;
	private Date submitDate;
	private Date confirmedDeliveryDateByCustomer;
	private String deductible;
	private String bankName;
	private String bankAccountNo;
	private String bankAddress;
	private String numberIBAN;
	private String swiftCode;
	private Date docCompleted;
	private Date acceptanceSent;
	private Date acceptanceRcvd;
	private Date settlementForm;
	private Date supplier1pay;
	private Date supplier2pay;
	private String deductibleInsurer;	
	private String deductibleAmtCurrency;
	private String deductibleInsurerCurrency;
	private Integer claimBy;
	private Integer noOfDaysForSettelment;
	
	// Added field based on claim integration
	
	private BigInteger clmsClaimNbr;
	private BigDecimal origClmsAmt;
	private BigDecimal pmntAmt;
	private Boolean assistanceRequired;
	
	/* Ticket Number: 10825 */
	private String claimType;
	/* Ticket Number: 11240 */
	private String branchandsortcode;
	private String accountname;
	
	private String additionalbankinformation;
	private String claimantCountry;
	private String claimantCity;
	
	//	 Mapping Variables
	private ServiceOrder serviceOrder; //One To Many RelationShip With ServiceOrder
	private Set<Loss> losss; //One To Many RelationShip With Loss
	private String responsibleAgentCode;
	private String location;
	private String factor;
	private String occurence;
	private String avoidable;
	private String culpable;
	private Date remediationClosingDate;
	private String actiontoPreventRecurrence;
	private String remediationComment;
	private String responsibleAgentName;
	private Date claimEmailSent;
	private BigDecimal propertyDamageAmount= new BigDecimal(0.00);
	private BigDecimal customerSvcAmount= new BigDecimal(0.00);
	private BigDecimal lostDamageAmount= new BigDecimal(0.00);
	private BigDecimal totalPaidToCustomerAmount= new BigDecimal(0.00);
	private BigDecimal goodWillPayment= new BigDecimal(0.00);
	private String claimUID;
	private String goodWillCurrency;
	/*
	 * Bug 14852 - 8061- New Todo item for claims
	 */
	private String moveCloudStatus;

	/*
	 * End
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("corpID", corpID)
				.append("claimNumber1", claimNumber1)
				.append("claimNumber", claimNumber)
				.append("lastName", lastName)
				.append("firstName", firstName)
				.append("shipNumber", shipNumber)
				.append("sequenceNumber", sequenceNumber)
				.append("claimCertificateNumber", claimCertificateNumber)
				.append("closeDate", closeDate)
				.append("dateOfInspection", dateOfInspection)
				.append("addressLine1", addressLine1)
				.append("addressLine2", addressLine2)
				.append("addressLine3", addressLine3)
				.append("city", city)
				.append("cityCode", cityCode)
				.append("state", state)
				.append("zip", zip)
				.append("phone", phone)
				.append("fax", fax)
				.append("formSent", formSent)
				.append("formRecived", formRecived)
				.append("inspecteDate", inspecteDate)
				.append("insurerNotification", insurerNotification)
				.append("insurerAcknowledgement", insurerAcknowledgement)
				.append("notifyShipper", notifyShipper)
				.append("country", country)
				.append("countryCode", countryCode)
				.append("shipperWorkPhone", shipperWorkPhone)
				.append("premium", premium)
				.append("insurerCode", insurerCode)
				.append("insurer", insurer)
				.append("paidByName", paidByName)
				.append("registrationNumber", registrationNumber)
				.append("cancelled", cancelled)
				.append("crossReference", crossReference)
				.append("email", email)
				.append("email2", email2)
				.append("requestReimbursement", requestReimbursement)
				.append("noProcess", noProcess)
				.append("reimbursement", reimbursement)
				.append("reciveReimbursement", reciveReimbursement)
				.append("reciveReimbursementCurrency",
						reciveReimbursementCurrency)
				.append("reimbursementCurrency", reimbursementCurrency)
				.append("claimPerson", claimPerson)
				.append("insuranceUser", insuranceUser)
				.append("idNumber", idNumber)
				.append("requestForm", requestForm)
				.append("uvlRecived", uvlRecived)
				.append("uvlAmount", uvlAmount)
				.append("toVendor", toVendor)
				.append("id", id)
				.append("serviceOrderId", serviceOrderId)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("insuranceCompanyPaid", insuranceCompanyPaid)
				.append("cportalAccess", cportalAccess)
				.append("fullValueProtection", fullValueProtection)
				.append("signature", signature)
				.append("signatutreDate", signatutreDate)
				.append("remarks", remarks)
				.append("submitFlag", submitFlag)
				.append("status", status)
				.append("submitDate", submitDate)
				.append("confirmedDeliveryDateByCustomer",
						confirmedDeliveryDateByCustomer)
				.append("deductible", deductible).append("bankName", bankName)
				.append("bankAccountNo", bankAccountNo)
				.append("bankAddress", bankAddress)
				.append("numberIBAN", numberIBAN)
				.append("swiftCode", swiftCode)
				.append("docCompleted", docCompleted)
				.append("acceptanceSent", acceptanceSent)
				.append("acceptanceRcvd", acceptanceRcvd)
				.append("settlementForm", settlementForm)
				.append("supplier1pay", supplier1pay)
				.append("supplier2pay", supplier2pay)
				.append("deductibleInsurer", deductibleInsurer)
				.append("deductibleAmtCurrency", deductibleAmtCurrency)
				.append("deductibleInsurerCurrency", deductibleInsurerCurrency)
				.append("claimBy", claimBy)
				.append("noOfDaysForSettelment", noOfDaysForSettelment)
				.append("clmsClaimNbr", clmsClaimNbr)
				.append("origClmsAmt", origClmsAmt).append("pmntAmt", pmntAmt)
				.append("assistanceRequired", assistanceRequired)
				.append("claimType", claimType)
				.append("branchandsortcode", branchandsortcode)
				.append("accountname", accountname).append("additionalbankinformation",additionalbankinformation)
				.append("claimantCountry",claimantCountry)
				.append("claimantCity",claimantCity)
				.append("responsibleAgentCode", responsibleAgentCode)
				.append("location",location)
				.append("factor", factor)
				.append("occurence", occurence)
				.append("avoidable", avoidable)
				.append("culpable", culpable)
				.append("remediationClosingDate", remediationClosingDate)
				.append("actiontoPreventRecurrence", actiontoPreventRecurrence)
				.append("remediationComment", remediationComment)
				.append("responsibleAgentName", responsibleAgentName)
				.append("claimEmailSent", claimEmailSent)
				.append("propertyDamageAmount", propertyDamageAmount)
				.append("customerSvcAmount", customerSvcAmount)
				.append("lostDamageAmount", lostDamageAmount)
				.append("totalPaidToCustomerAmount", totalPaidToCustomerAmount)
				.append("goodWillPayment", goodWillPayment)
				.append("claimUID", claimUID)
				.append("goodWillCurrency",goodWillCurrency)
				.append("moveCloudStatus",moveCloudStatus)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Claim))
			return false;
		Claim castOther = (Claim) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID)
				.append(claimNumber1, castOther.claimNumber1)
				.append(claimNumber, castOther.claimNumber)
				.append(lastName, castOther.lastName)
				.append(firstName, castOther.firstName)
				.append(shipNumber, castOther.shipNumber)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(claimCertificateNumber,
						castOther.claimCertificateNumber)
				.append(closeDate, castOther.closeDate)
				.append(dateOfInspection, castOther.dateOfInspection)
				.append(addressLine1, castOther.addressLine1)
				.append(addressLine2, castOther.addressLine2)
				.append(addressLine3, castOther.addressLine3)
				.append(city, castOther.city)
				.append(cityCode, castOther.cityCode)
				.append(state, castOther.state)
				.append(zip, castOther.zip)
				.append(phone, castOther.phone)
				.append(fax, castOther.fax)
				.append(formSent, castOther.formSent)
				.append(formRecived, castOther.formRecived)
				.append(inspecteDate, castOther.inspecteDate)
				.append(insurerNotification, castOther.insurerNotification)
				.append(insurerAcknowledgement,
						castOther.insurerAcknowledgement)
				.append(notifyShipper, castOther.notifyShipper)
				.append(country, castOther.country)
				.append(countryCode, castOther.countryCode)
				.append(shipperWorkPhone, castOther.shipperWorkPhone)
				.append(premium, castOther.premium)
				.append(insurerCode, castOther.insurerCode)
				.append(insurer, castOther.insurer)
				.append(paidByName, castOther.paidByName)
				.append(registrationNumber, castOther.registrationNumber)
				.append(cancelled, castOther.cancelled)
				.append(crossReference, castOther.crossReference)
				.append(email, castOther.email)
				.append(email2, castOther.email2)
				.append(requestReimbursement, castOther.requestReimbursement)
				.append(noProcess, castOther.noProcess)
				.append(reimbursement, castOther.reimbursement)
				.append(reciveReimbursement, castOther.reciveReimbursement)
				.append(reciveReimbursementCurrency,
						castOther.reciveReimbursementCurrency)
				.append(reimbursementCurrency, castOther.reimbursementCurrency)
				.append(claimPerson, castOther.claimPerson)
				.append(insuranceUser, castOther.insuranceUser)
				.append(idNumber, castOther.idNumber)
				.append(requestForm, castOther.requestForm)
				.append(uvlRecived, castOther.uvlRecived)
				.append(uvlAmount, castOther.uvlAmount)
				.append(toVendor, castOther.toVendor)
				.append(id, castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(insuranceCompanyPaid, castOther.insuranceCompanyPaid)
				.append(cportalAccess, castOther.cportalAccess)
				.append(fullValueProtection, castOther.fullValueProtection)
				.append(signature, castOther.signature)
				.append(signatutreDate, castOther.signatutreDate)
				.append(remarks, castOther.remarks)
				.append(submitFlag, castOther.submitFlag)
				.append(status, castOther.status)
				.append(submitDate, castOther.submitDate)
				.append(confirmedDeliveryDateByCustomer,
						castOther.confirmedDeliveryDateByCustomer)
				.append(deductible, castOther.deductible)
				.append(bankName, castOther.bankName)
				.append(bankAccountNo, castOther.bankAccountNo)
				.append(bankAddress, castOther.bankAddress)
				.append(numberIBAN, castOther.numberIBAN)
				.append(swiftCode, castOther.swiftCode)
				.append(docCompleted, castOther.docCompleted)
				.append(acceptanceSent, castOther.acceptanceSent)
				.append(acceptanceRcvd, castOther.acceptanceRcvd)
				.append(settlementForm, castOther.settlementForm)
				.append(supplier1pay, castOther.supplier1pay)
				.append(supplier2pay, castOther.supplier2pay)
				.append(deductibleInsurer, castOther.deductibleInsurer)
				.append(deductibleAmtCurrency, castOther.deductibleAmtCurrency)
				.append(deductibleInsurerCurrency,
						castOther.deductibleInsurerCurrency)
				.append(claimBy, castOther.claimBy)
				.append(noOfDaysForSettelment, castOther.noOfDaysForSettelment)
				.append(clmsClaimNbr, castOther.clmsClaimNbr)
				.append(origClmsAmt, castOther.origClmsAmt)
				.append(claimType, castOther.claimType)
				.append(pmntAmt, castOther.pmntAmt).append(assistanceRequired, castOther.assistanceRequired)
				.append(branchandsortcode, castOther.branchandsortcode)
				.append(accountname, castOther.accountname)
				.append(additionalbankinformation, castOther.additionalbankinformation)
				.append(claimantCountry, castOther.claimantCountry)
				.append(claimantCity, castOther.claimantCity)
				.append(responsibleAgentCode, castOther.responsibleAgentCode)
				.append(location, castOther.location)
				.append(factor, castOther.factor)
				.append(occurence, castOther.occurence)
				.append(avoidable, castOther.avoidable)
				.append(culpable, castOther.culpable)
				.append(remediationClosingDate, castOther.remediationClosingDate)
				.append(actiontoPreventRecurrence, castOther.actiontoPreventRecurrence)
				.append(remediationComment, castOther.remediationComment)
				.append(responsibleAgentName, castOther.responsibleAgentName)
				.append(claimEmailSent, castOther.claimEmailSent)
				.append(propertyDamageAmount, castOther.propertyDamageAmount)
				.append(customerSvcAmount, castOther.customerSvcAmount)
				.append(lostDamageAmount, castOther.lostDamageAmount)
				.append(totalPaidToCustomerAmount, castOther.totalPaidToCustomerAmount)
				.append(goodWillPayment, castOther.goodWillPayment)
				.append(claimUID, castOther.claimUID)
				.append(goodWillCurrency,castOther.goodWillCurrency)
				.append(moveCloudStatus,castOther.moveCloudStatus)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(claimNumber1)
				.append(claimNumber).append(lastName).append(firstName)
				.append(shipNumber).append(sequenceNumber)
				.append(claimCertificateNumber).append(closeDate)
				.append(dateOfInspection).append(addressLine1)
				.append(addressLine2).append(addressLine3).append(city)
				.append(cityCode).append(state).append(zip).append(phone)
				.append(fax).append(formSent).append(formRecived)
				.append(inspecteDate).append(insurerNotification)
				.append(insurerAcknowledgement).append(notifyShipper)
				.append(country).append(countryCode).append(shipperWorkPhone)
				.append(premium).append(insurerCode).append(insurer)
				.append(paidByName).append(registrationNumber)
				.append(cancelled).append(crossReference).append(email)
				.append(email2).append(requestReimbursement).append(noProcess)
				.append(reimbursement).append(reciveReimbursement)
				.append(reciveReimbursementCurrency)
				.append(reimbursementCurrency).append(claimPerson)
				.append(insuranceUser).append(idNumber).append(requestForm)
				.append(uvlRecived).append(uvlAmount).append(toVendor)
				.append(id).append(serviceOrderId).append(createdBy)
				.append(createdOn).append(updatedBy).append(updatedOn)
				.append(insuranceCompanyPaid).append(cportalAccess)
				.append(fullValueProtection).append(signature)
				.append(signatutreDate).append(remarks).append(submitFlag)
				.append(status).append(submitDate)
				.append(confirmedDeliveryDateByCustomer).append(deductible)
				.append(bankName).append(bankAccountNo).append(bankAddress)
				.append(numberIBAN).append(swiftCode).append(docCompleted)
				.append(acceptanceSent).append(acceptanceRcvd)
				.append(settlementForm).append(supplier1pay)
				.append(supplier2pay).append(deductibleInsurer)
				.append(deductibleAmtCurrency)
				.append(deductibleInsurerCurrency).append(claimBy)
				.append(noOfDaysForSettelment).append(clmsClaimNbr)
				.append(origClmsAmt).append(pmntAmt).append(assistanceRequired).append(claimType)
				.append(branchandsortcode)
				.append(accountname)
				.append(additionalbankinformation)
				.append(claimantCountry)
				.append(claimantCity)
				.append(responsibleAgentCode)
				.append(location)
				.append(factor)
				.append(occurence)
				.append(avoidable)
				.append(culpable)
				.append(remediationClosingDate)
				.append(actiontoPreventRecurrence)
				.append(remediationComment)
				.append(responsibleAgentName)
				.append(claimEmailSent).append(propertyDamageAmount).append(customerSvcAmount).append(lostDamageAmount).append(totalPaidToCustomerAmount)
				.append(goodWillPayment).append(claimUID).append(goodWillCurrency).append(moveCloudStatus)
				.toHashCode();
	}
	@ManyToOne
	@JoinColumn(name="serviceOrderId", nullable=false, updatable=false,
	referencedColumnName="id")
	public ServiceOrder getServiceOrder() {
	        return serviceOrder;
	    }
	    
		 public void setServiceOrder(ServiceOrder serviceOrder) {
				this.serviceOrder = serviceOrder;
		} 

	@OneToMany(mappedBy="claim", cascade = CascadeType.ALL, fetch = FetchType.LAZY )
			public Set<Loss> getLosss() {
				return losss;
			}
			public void setLosss(Set<Loss> losss) {
				this.losss = losss;
			}
		
	@Column(length=100)
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	@Column(length=100)
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	@Column(length=100)
	public String getAddressLine3() {
		return addressLine3;
	}
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}
	@Column
	public Date getCancelled() {
		return cancelled;
	}
	public void setCancelled(Date cancelled) {
		this.cancelled = cancelled;
	}
	@Column(length=30)
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Column(length=15)
	public String getClaimCertificateNumber() {
		return claimCertificateNumber;
	}
	public void setClaimCertificateNumber(String claimCertificateNumber) {
		this.claimCertificateNumber = claimCertificateNumber;
	}
	@Column(length=20)
	public Long getClaimNumber() {
		return claimNumber;
	}
	public void setClaimNumber(Long claimNumber) {
		this.claimNumber = claimNumber;
	}
	@Column(length=10)
	public int getClaimNumber1() {
		return claimNumber1;
	}
	public void setClaimNumber1(int claimNumber1) {
		this.claimNumber1 = claimNumber1;
	}
	@Column
	public Date getCloseDate() {
		return closeDate;
	}
	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=45)
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=20)
	public String getCrossReference() {
		return crossReference;
	}
	public void setCrossReference(String crossReference) {
		this.crossReference = crossReference;
	}
	@Column(length=65)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Column(length=65)
	public String getEmail2() {
		return email2;
	}
	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	@Column(length=20)
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	@Column(length=80)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Column
	public Date getFormRecived() {
		return formRecived;
	}
	public void setFormRecived(Date formRecived) {
		this.formRecived = formRecived;
	}
	@Column
	public Date getFormSent() {
		return formSent;
	}
	public void setFormSent(Date formSent) {
		this.formSent = formSent;
	}
	@Column
	public Date getInspecteDate() {
		return inspecteDate;
	}
	public void setInspecteDate(Date inspecteDate) {
		this.inspecteDate = inspecteDate;
	}
	@Column(length=55)
	public String getInsurer() {
		return insurer;
	}
	public void setInsurer(String insurer) {
		this.insurer = insurer;
	}
	@Column
	public Date getInsurerAcknowledgement() {
		return insurerAcknowledgement;
	}
	public void setInsurerAcknowledgement(Date insurerAcknowledgement) {
		this.insurerAcknowledgement = insurerAcknowledgement;
	}
	@Column(length=8)
	public String getInsurerCode() {
		return insurerCode;
	}
	public void setInsurerCode(String insurerCode) {
		this.insurerCode = insurerCode;
	}
	@Column
	public Date getInsurerNotification() {
		return insurerNotification;
	}
	public void setInsurerNotification(Date insurerNotification) {
		this.insurerNotification = insurerNotification;
	}
	@Column(length=80)
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Column
	public Date getNoProcess() {
		return noProcess;
	}
	public void setNoProcess(Date noProcess) {
		this.noProcess = noProcess;
	}
	@Column
	public Date getNotifyShipper() {
		return notifyShipper;
	}
	public void setNotifyShipper(Date notifyShipper) {
		this.notifyShipper = notifyShipper;
	}
	@Column(length=55)
	public String getPaidByName() {
		return paidByName;
	}
	public void setPaidByName(String paidByName) {
		this.paidByName = paidByName;
	}
	@Column(length=20)
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Column(length=20)
	public BigDecimal getPremium() {
		return premium;
	}
	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}
	@Column(length=20)
	public BigDecimal getReciveReimbursement() {
		return reciveReimbursement;
	}
	public void setReciveReimbursement(BigDecimal reciveReimbursement) {
		this.reciveReimbursement = reciveReimbursement;
	}
	@Column(length=20)
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	@Column(length=20)
	public BigDecimal getReimbursement() {
		return reimbursement;
	}
	public void setReimbursement(BigDecimal reimbursement) {
		this.reimbursement = reimbursement;
	}
	@Column
	public Boolean getInsuranceUser() {
		return insuranceUser;
	}
	public void setInsuranceUser(Boolean insuranceUser) {
		this.insuranceUser = insuranceUser;
	}
	@Column
	public Date getRequestForm() {
		return requestForm;
	}
	public void setRequestForm(Date requestForm) {
		this.requestForm = requestForm;
	}
	@Column
	public Date getRequestReimbursement() {
		return requestReimbursement;
	}
	public void setRequestReimbursement(Date requestReimbursement) {
		this.requestReimbursement = requestReimbursement;
	}
	public String getReciveReimbursementCurrency() {
		return reciveReimbursementCurrency;
	}
	public void setReciveReimbursementCurrency(String reciveReimbursementCurrency) {
		this.reciveReimbursementCurrency = reciveReimbursementCurrency;
	}
	public String getReimbursementCurrency() {
		return reimbursementCurrency;
	}
	public void setReimbursementCurrency(String reimbursementCurrency) {
		this.reimbursementCurrency = reimbursementCurrency;
	}
	@Column
	public String getClaimPerson() {
		return claimPerson;
	}
	public void setClaimPerson(String claimPerson) {		
			this.claimPerson = claimPerson;
	}
	@Column(length=15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column(length=26)
	public String getShipperWorkPhone() {
		return shipperWorkPhone;
	}
	public void setShipperWorkPhone(String shipperWorkPhone) {
		this.shipperWorkPhone = shipperWorkPhone;
	}
	@Column(length=2)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column
	public Date getToVendor() {
		return toVendor;
	}
	public void setToVendor(Date toVendor) {
		this.toVendor = toVendor;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=20)
	public BigDecimal getUvlAmount() {
		return uvlAmount;
	}
	public void setUvlAmount(BigDecimal uvlAmount) {
		this.uvlAmount = uvlAmount;
	}
	@Column
	public Date getUvlRecived() {
		return uvlRecived;
	}
	public void setUvlRecived(Date uvlRecived) {
		this.uvlRecived = uvlRecived;
	}
	@Column(length=10)
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Transient
    public String getListCode() {       
        return insurerCode;  //return the code that needs to be set on the parent page
    }

    @Transient
    public String getListDescription() {
        return this.insurer; //return the description that needs to be set on the parent page
    } 
	
	@Transient
	public String getListSecondDescription() {
		return null;
	}
	
	@Transient
	public String getListThirdDescription() {
		return null;
	}
	
	@Transient
	public String getListFourthDescription() {
		return null;
	}
	
	@Transient
	public String getListFifthDescription() {
		return null;
	}
	
	@Transient
	public String getListSixthDescription() {
		return null;
	}
	@Column(length=15)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	@Column(length=3)
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	@Transient
	public String getListSeventhDescription() {
		return null;
	}
	@Column(length=20, insertable=false, updatable=false)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Transient
	public String getListEigthDescription() {
		return null;
	}
	@Transient
	public String getListNinthDescription() {
		return null;
	}
	@Transient
	public String getListTenthDescription() {
		return null;
	}
	@Column
	public Date getDateOfInspection() {
		return dateOfInspection;
	}
	public void setDateOfInspection(Date dateOfInspection) {
		this.dateOfInspection = dateOfInspection;
	}
	
	@Column
	public Date getInsuranceCompanyPaid() {
		return insuranceCompanyPaid;
	}
	public void setInsuranceCompanyPaid(Date insuranceCompanyPaid) {
		this.insuranceCompanyPaid = insuranceCompanyPaid;
	}
	/**
	 * @return the cportalAccess
	 */
	@Column
	public Boolean getCportalAccess() {
		return cportalAccess;
	}
	/**
	 * @param cportalAccess the cportalAccess to set
	 */
	public void setCportalAccess(Boolean cportalAccess) {
		this.cportalAccess = cportalAccess;
	}
	
	/**
	 * @return the fullValueProtection
	 */
	@Column(length=30)
	public String getFullValueProtection() {
		return fullValueProtection;
	}
	/**
	 * @param fullValueProtection the fullValueProtection to set
	 */
	public void setFullValueProtection(String fullValueProtection) {
		this.fullValueProtection = fullValueProtection;
	}
	/**
	 * @return the remarks
	 */
	@Column(length=1000)
	public String getRemarks() {
		return remarks;
	}
	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	/**
	 * @return the signature
	 */
	@Column
	public Boolean getSignature() {
		return signature;
	}
	/**
	 * @param signature the signature to set
	 */
	public void setSignature(Boolean signature) {
		this.signature = signature;
	}
	/**
	 * @return the signatutreDate
	 */
	@Column
	public Date getSignatutreDate() {
		return signatutreDate;
	}
	/**
	 * @param signatutreDate the signatutreDate to set
	 */
	public void setSignatutreDate(Date signatutreDate) {
		this.signatutreDate = signatutreDate;
	}
	/**
	 * @return the submitFlag
	 */
	@Column(length=10)
	public String getSubmitFlag() {
		return submitFlag;
	}
	/**
	 * @param submitFlag the submitFlag to set
	 */
	public void setSubmitFlag(String submitFlag) {
		this.submitFlag = submitFlag;
	}
	@Column(length=20)
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column
	public Date getSubmitDate() {
		return submitDate;
	}
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}
	@Column
	public Date getConfirmedDeliveryDateByCustomer() {
		return confirmedDeliveryDateByCustomer;
	}
	public void setConfirmedDeliveryDateByCustomer(
			Date confirmedDeliveryDateByCustomer) {
		this.confirmedDeliveryDateByCustomer = confirmedDeliveryDateByCustomer;
	}
	@Column(length=50)
	public String getDeductible() {
		return deductible;
	}
	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}
	@Column
	public String getBankAccountNo() {
		return bankAccountNo;
	}
	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}
	@Column(length=200)
	public String getBankAddress() {
		return bankAddress;
	}
	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}
	@Column(length=100)
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	@Column(length=50)
	public String getNumberIBAN() {
		return numberIBAN;
	}
	public void setNumberIBAN(String numberIBAN) {
		this.numberIBAN = numberIBAN;
	}
	@Column(length=50)
	public String getSwiftCode() {
		return swiftCode;
	}
	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}
	@Column
	public Date getDocCompleted() {
		return docCompleted;
	}
	public void setDocCompleted(Date docCompleted) {
		this.docCompleted = docCompleted;
	}
	@Column
	public Date getAcceptanceSent() {
		return acceptanceSent;
	}
	public void setAcceptanceSent(Date acceptanceSent) {
		this.acceptanceSent = acceptanceSent;
	}
	@Column
	public Date getAcceptanceRcvd() {
		return acceptanceRcvd;
	}
	public void setAcceptanceRcvd(Date acceptanceRcvd) {
		this.acceptanceRcvd = acceptanceRcvd;
	}
	@Column
	public Date getSettlementForm() {
		return settlementForm;
	}
	public void setSettlementForm(Date settlementForm) {
		this.settlementForm = settlementForm;
	}
	@Column
	public Date getSupplier1pay() {
		return supplier1pay;
	}
	public void setSupplier1pay(Date supplier1pay) {
		this.supplier1pay = supplier1pay;
	}
	@Column
	public Date getSupplier2pay() {
		return supplier2pay;
	}
	public void setSupplier2pay(Date supplier2pay) {
		this.supplier2pay = supplier2pay;
	}
	@Column
	public String getDeductibleInsurer() {
		return deductibleInsurer;
	}
	public void setDeductibleInsurer(String deductibleInsurer) {
		this.deductibleInsurer = deductibleInsurer;
	}
	
	@Column
	public String getDeductibleAmtCurrency() {
		return deductibleAmtCurrency;
	}
	public void setDeductibleAmtCurrency(String deductibleAmtCurrency) {
		this.deductibleAmtCurrency = deductibleAmtCurrency;
	}
	@Column
	public String getDeductibleInsurerCurrency() {
		return deductibleInsurerCurrency;
	}
	public void setDeductibleInsurerCurrency(String deductibleInsurerCurrency) {
		this.deductibleInsurerCurrency = deductibleInsurerCurrency;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}	
	public Integer getClaimBy() {
		return claimBy;
	}
	public void setClaimBy(Integer claimBy) {
		this.claimBy = claimBy;
	}
	public Integer getNoOfDaysForSettelment() {
		return noOfDaysForSettelment;
	}
	public void setNoOfDaysForSettelment(Integer noOfDaysForSettelment) {
		this.noOfDaysForSettelment = noOfDaysForSettelment;
	}
	public BigInteger getClmsClaimNbr() {
		return clmsClaimNbr;
	}
	public void setClmsClaimNbr(BigInteger clmsClaimNbr) {
		this.clmsClaimNbr = clmsClaimNbr;
	}
	public BigDecimal getOrigClmsAmt() {
		return origClmsAmt;
	}
	public void setOrigClmsAmt(BigDecimal origClmsAmt) {
		this.origClmsAmt = origClmsAmt;
	}
	public BigDecimal getPmntAmt() {
		return pmntAmt;
	}
	public void setPmntAmt(BigDecimal pmntAmt) {
		this.pmntAmt = pmntAmt;
	}
	public Boolean getAssistanceRequired() {
		return assistanceRequired;
	}
	public void setAssistanceRequired(Boolean assistanceRequired) {
		this.assistanceRequired = assistanceRequired;
	}
	@Column
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	@Column
	public String getBranchandsortcode() {
		return branchandsortcode;
	}
	public void setBranchandsortcode(String branchandsortcode) {
		this.branchandsortcode = branchandsortcode;
	}
	@Column
	public String getAccountname() {
		return accountname;
	}
	public void setAccountname(String accountname) {
		this.accountname = accountname;
	}
	@Column
	public String getAdditionalbankinformation() {
		return additionalbankinformation;
	}
	public void setAdditionalbankinformation(String additionalbankinformation) {
		this.additionalbankinformation = additionalbankinformation;
	}
	@Column
	public String getClaimantCountry() {
		return claimantCountry;
	}
	public void setClaimantCountry(String claimantCountry) {
		this.claimantCountry = claimantCountry;
	}
	@Column
	public String getClaimantCity() {
		return claimantCity;
	}
	public void setClaimantCity(String claimantCity) {
		this.claimantCity = claimantCity;
	}
	@Column
	public String getLocation() {
		return location;
	}
	@Column
	public String getResponsibleAgentCode() {
		return responsibleAgentCode;
	}
	public void setResponsibleAgentCode(String responsibleAgentCode) {
		this.responsibleAgentCode = responsibleAgentCode;
	}
	@Column
	public String getResponsibleAgentName() {
		return responsibleAgentName;
	}
	public void setResponsibleAgentName(String responsibleAgentName) {
		this.responsibleAgentName = responsibleAgentName;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	@Column
	public String getFactor() {
		return factor;
	}
	public void setFactor(String factor) {
		this.factor = factor;
	}
	@Column
	public String getOccurence() {
		return occurence;
	}
	public void setOccurence(String occurence) {
		this.occurence = occurence;
	}
	@Column
	public String getAvoidable() {
		return avoidable;
	}
	public void setAvoidable(String avoidable) {
		this.avoidable = avoidable;
	}
	@Column
	public String getCulpable() {
		return culpable;
	}
	public void setCulpable(String culpable) {
		this.culpable = culpable;
	}
	@Column
	public Date getRemediationClosingDate() {
		return remediationClosingDate;
	}
	public void setRemediationClosingDate(Date remediationClosingDate) {
		this.remediationClosingDate = remediationClosingDate;
	}
	@Column
	public String getActiontoPreventRecurrence() {
		return actiontoPreventRecurrence;
	}
	public void setActiontoPreventRecurrence(String actiontoPreventRecurrence) {
		this.actiontoPreventRecurrence = actiontoPreventRecurrence;
	}
	@Column
	public String getRemediationComment() {
		return remediationComment;
	}
	public void setRemediationComment(String remediationComment) {
		this.remediationComment = remediationComment;
	}
	@Column
	public Date getClaimEmailSent() {
		return claimEmailSent;
	}
	public void setClaimEmailSent(Date claimEmailSent) {
		this.claimEmailSent = claimEmailSent;
	}
	public BigDecimal getPropertyDamageAmount() {
		return propertyDamageAmount;
	}
	public void setPropertyDamageAmount(BigDecimal propertyDamageAmount) {
		this.propertyDamageAmount = propertyDamageAmount;
	}
	public BigDecimal getCustomerSvcAmount() {
		return customerSvcAmount;
	}
	public void setCustomerSvcAmount(BigDecimal customerSvcAmount) {
		this.customerSvcAmount = customerSvcAmount;
	}
	public BigDecimal getLostDamageAmount() {
		return lostDamageAmount;
	}
	public void setLostDamageAmount(BigDecimal lostDamageAmount) {
		this.lostDamageAmount = lostDamageAmount;
	}
	public BigDecimal getTotalPaidToCustomerAmount() {
		return totalPaidToCustomerAmount;
	}
	public void setTotalPaidToCustomerAmount(BigDecimal totalPaidToCustomerAmount) {
		this.totalPaidToCustomerAmount = totalPaidToCustomerAmount;
	}
	@Column
	public BigDecimal getGoodWillPayment() {
		return goodWillPayment;
	}
	public void setGoodWillPayment(BigDecimal goodWillPayment) {
		this.goodWillPayment = goodWillPayment;
	}
	@Column
	public String getClaimUID() {
		return claimUID;
	}
	public void setClaimUID(String claimUID) {
		this.claimUID = claimUID;
	}
	@Column
	public String getGoodWillCurrency() {
		return goodWillCurrency;
	}
	public void setGoodWillCurrency(String goodWillCurrency) {
		this.goodWillCurrency = goodWillCurrency;
	}
	/**
	 * @return the moveCloudStatus
	 */
	@Column
	public String getMoveCloudStatus() {
		return moveCloudStatus;
	}
	/**
	 * @param moveCloudStatus the moveCloudStatus to set
	 */
	public void setMoveCloudStatus(String moveCloudStatus) {
		this.moveCloudStatus = moveCloudStatus;
	}
}


	
	