package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ResourceGridDao;
import com.trilasoft.app.model.ResourceGrid;
import com.trilasoft.app.service.ResourceGridManager;



public class ResourceGridManagerImpl extends GenericManagerImpl<ResourceGrid, Long> implements ResourceGridManager {
	ResourceGridDao resourceGridDao;
	public ResourceGridManagerImpl(ResourceGridDao resourceGridDao) {
		super(resourceGridDao);
		this.resourceGridDao = resourceGridDao;
	}

	public List findResource(String hub, Date workDate,String sessionCorpID){
		return resourceGridDao.findResource(hub,workDate,sessionCorpID);
	}
	public List getListById(Long id, String sessionCorpID){
		return resourceGridDao.getListById(id, sessionCorpID);
	}
}
