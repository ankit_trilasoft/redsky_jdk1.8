<%@ include file="/common/taglibs.jsp"%> 
<head>   
    <title><fmt:message key="partnerDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerDetail.heading'/>"/>  
    
<style type="text/css">
	h2 {background-color: #FBBFFF}
	.dspchar2 {
		padding-left:0;
	}		
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>

</head>

<s:form id="partnerAddForm" name="partnerAddForm" action="saveAgentDetails" method="post" validate="true">

<div id="Layer1" style="width:100%">
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="id" value="<%= request.getParameter("id")%>" />
<s:hidden name="id" value="<%= request.getParameter("id")%>" />
<s:hidden name="partner.id"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="popupval" value="${papam.popup}"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" /> 

<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
		
<!--<s:hidden name="formStatus" value=""/>

--><c:if test="${validateFormNav == 'OK' }">
	<c:choose>
	<c:when test="${gotoPageString == 'gototab.agentdtl' }">
		<c:redirect url="/editPartnerAddForm.html?partnerType=${partnerType}&id=${partner.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.vendordtl' }">
		<c:redirect url="/editPartnerAddForm.html?partnerType=${partnerType }&id=${partner.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.carrierdtl' }">
		<c:redirect url="/editPartnerAddForm.html?partnerType=${partnerType }&id=${partner.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.base' }">
		<c:redirect url="/baseList.html?id=${partner.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.ratematrix' }">
		<c:redirect url="/partnerRateGrids.html?partnerId=${partner.id}"/>
	</c:when>
		
		<c:when test="${gotoPageString == 'gototab.partnerlist' }">
			<c:if test="${paramValue == 'View'}">
				<c:redirect url="/searchPartnerView.html"/>
			</c:if>
			<c:if test="${paramValue != 'View'}">
				<c:redirect url="/searchPartnerAdmin.html?partnerType=${partnerType}"/>
			</c:if>
		</c:when>
	<c:otherwise>
	</c:otherwise>
	</c:choose>
</c:if>

<div id="newmnav">
		  <ul>
		  	<c:if test="${partnerType == 'AG'}">
		  	<li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.agentdtl');return checkdate('none');"><span>Agent Details</span></a></li>
		    </c:if>
		     
		    <c:if test="${partnerType == 'VN'}">
		    <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.vendordtl');return checkdate('none');"><span>Vendor Details</span></a></li>
		    </c:if>
		    
		  	<c:if test="${partnerType == 'AG'}">
		  	<li><a href="findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=${partnerType}&id=${partner.id}"><span>Agent Profile</span></a></li>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Agent Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
		    <li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
		    </c:if>
		    <c:if test="${partnerType == 'VN'}">
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Vendor Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
		    </c:if>
		   
		    <c:if test="${partnerType == 'CR'}">
		    <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.carrierdtl');return checkdate('none');"><span>Carrier Details</span></a></li>
		    </c:if>
		    <c:if test="${partnerType == 'AG'}">
		     <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.base');return checkdate('none');"><span>Base</span></a></li>
			 <%-- <c:if test='${partner.latitude == ""  || partner.longitude == "" || partner.latitude == null  || partner.longitude == null}'>
			     <li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
			  </c:if>
			  <c:if test='${partner.latitude != "" && partner.longitude != "" && partner.latitude != null && partner.longitude != null}'>
			       <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.ratematrix');return checkdate('none');"><span>Rate Matrix</span></a></li>
			 </c:if> --%>
		     </c:if>
		     <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.partnerlist');return checkdate('none');"><span>Partner List</span></a></li>
		     <c:if test="${partner.partnerPortalActive == true}">
  			<c:if test="${partnerType == 'AG'}">
				<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Users & contacts</span></a></li>
			</c:if>
			</c:if>
  </ul>
		</div><div class="spn">&nbsp;</div>
	
<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%">
	<tbody>
  <tr>
  	<td align="left" class="listwhitetext" > 
  	<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;padding: 0px;">
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Participates as
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
  
  	<table class="detailTabLabel" border="0">
		  
		   	<!--  <table>
			<tbody>
			<tr>
				<td align="left" class="listwhitetext">Name<font color="red" size="2">*</font></td>
				<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode'/><font color="red" size="2">*</font></td>
			</tr>
			<tr>
				<s:hidden key="partner.firstName" value=""/>
				<td align="left" class="listwhitetext"> <s:textfield key="partner.lastName" required="true" cssClass="input-text"
				size="25" maxlength="80"  onkeydown="return onlyCharsAllowed(event)"readonly="true"/> </td>
				<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerCode"
				required="true" cssClass="input-textUpper" maxlength="8" size="15" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true"/> </td>
			</tr>
			
			</tbody>
			</table>
			-->
			
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>
					
	  		
					
					
					<s:hidden name="partner.isBroker" value="false" />
					<s:hidden name="partner.qc" value="false" />
					
					<tr><td colspan="7">
					<table style="padding-bottom: 0px; margin-bottom: -3px" width="95%">
					<tr><td align="right" class="listwhitetext" style="width: 132px; !width:135px"><fmt:message key='partner.partnerCode'/><font color="red" size="2">*</font></td>
		
				
				<td align="left" class="listwhitetext" colspan=""> <s:textfield key="partner.partnerCode"
				required="true" cssClass="input-textUpper" maxlength="8" size="8" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true"/> </td>
					
				<td align="right" class="listwhitetext" width="">Name<font color="red" size="2">*</font></td>
				<s:hidden key="partner.firstName"/>
				<td align="left" class="listwhitetext" colspan=""> <s:textfield key="partner.lastName" required="true" cssClass="input-textUpper"
				size="74" maxlength="80"  onkeydown="return onlyCharsAllowed(event)"readonly="true"/> </td>
				</tr>
				</table>
				</td>
				</tr>
					<tr>
						
						<td align="right" class="listwhitetext"><fmt:message key='partner.effectiveDate'/></td>						
						<td align="left" colspan="3" class="listwhitetext">
						<table border="0" class="detailTabLabel" cellspacing="2" cellpadding="0">
						<tr>
						<c:if test="${not empty partner.effectiveDate}">
						<s:text id="partnerEffectiveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.effectiveDate"/></s:text>
						<td width="50px"><s:textfield cssClass="input-text" id="effectiveDate" readonly="true" name="partner.effectiveDate" value="%{partnerEffectiveDateFormattedValue}" size="25" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
						<td align="left" style="padding-top:1px;"><img id="effectiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty partner.effectiveDate}">
					<td width="50px"><s:textfield cssClass="input-text" id="effectiveDate" readonly="true" name="partner.effectiveDate" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
					<td align="left" style="padding-top:1px;"><img id="effectiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<!--<td align="right" class="listwhitetext"><fmt:message key='partner.zone'/></td><td><s:textfield cssClass="input-text" name="partner.zone"  cssStyle="width:115px" maxlength="2"/></td>
					-->
					</tr>
					</table>
					</td>					
					</tr> 
						<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.accountingDefault'/></td>
						<td  class="listwhitetext" width="8px" colspan="2"><s:checkbox key="partner.accountingDefault" onclick="changeStatus();" /></td>
					
						<td align="right" class="listwhitetext"><fmt:message key='partner.acctDefaultJobType'/></td>
						<td><s:textfield cssClass="input-text" name="partner.acctDefaultJobType"  size="35" maxlength="50"  tabindex="4"/></td>
						
						</tr>
					<tr> 
						<s:hidden name="partner.rank"/> 
						<td align="right" class="listwhitetext"><fmt:message key='partner.billPayType'/></td>
						<td colspan="2"><s:select name="partner.billPayType" list="%{paytype}" cssClass="list-menu" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="2"/></td>
					
						
						<td align="right" class="listwhitetext"><fmt:message key='partner.billPayOption'/></td>
						<td colspan="3"><s:select name="partner.billPayOption"  list="%{payopt}" cssClass="list-menu" cssStyle="width:205px"   onchange="changeStatus();" tabindex="3"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.abbreviation'/></td>
						<td colspan="2"><s:textfield cssClass="input-text" name="partner.abbreviation"  size="25" maxlength="15"/></td>
						<td ></td>
						<td ></td>
						</tr> 
					<tr>
					<td align="right" class="listwhitetext"><fmt:message key='partner.billingInstructionCode'/></td>
					<td colspan="7" align="left"><s:select cssClass="list-menu" id="billingInstructionCode" name="partner.billingInstruction"  list="%{billinst}" cssStyle="width:510px;!width:520px;"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="6"/></td>
					<td ></td>
					<td ></td>
					</tr> 
				
					<tr>
						<s:hidden  name="partner.client5"/> 
					<td align="right" class="listwhitetext"><fmt:message key='partner.agentParent'/></td>
						<td colspan="3"><s:textfield cssClass="input-text" name="partner.agentParent"  size="7" maxlength="8" onchange = "return checkVendorName();" tabindex="8"/>
						<img class="openpopup" width="17" height="20" align="top" onclick="javascript:openWindow('venderPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=partner.agentParent');" src="<c:url value='/images/open-popup.gif'/>" /></td>
					
					</tr>  
						
					
					
					
					<tr>
						<c:set var="ischeckeddddd" value="false"/>
						<c:if test="${partner.invoiceUploadCheck}">
						<c:set var="ischeckeddddd" value="true"/>
						</c:if>
						<td colspan="4" style="margin: 0px;padding: 0px;">
						<table border="0" valign="top" class="detailTabLabel" style="margin: 0px;padding: 0px;">
						<tr>
						<s:hidden  name="partner.client3"  /> 
						<td align="right" class="listwhitetext">Don't send to invoice upload</td>
						<td  class="listwhitetext" width="8px" ><s:checkbox key="partner.invoiceUploadCheck" onclick="changeStatus();"  value="${ischeckeddddd}" fieldValue="true" tabindex="18"/></td>
					    <c:set var="ischeckedddd" value="false"/>
						<c:if test="${partner.payableUploadCheck}">
						<c:set var="ischeckedddd" value="true"/>
						</c:if>
						
						<td align="right" class="listwhitetext">Don't send to payable upload</td>
						<td  class="listwhitetext" width="8px" ><s:checkbox key="partner.payableUploadCheck" onclick="changeStatus();"  value="${ischeckedddd}" fieldValue="true" tabindex="18"/></td>
						</tr>
						<tr> 
			              <td align="right" class="listwhitetext" width="">Don't generate unauthorized invoice</td>
		                  <td  class="listwhitetext" width="" ><s:checkbox key="partner.stopNotAuthorizedInvoices" onclick="changeStatus();"  value="${partner.stopNotAuthorizedInvoices}" fieldValue="true" tabindex="18"/></td>
			              <td align="right" class="listwhitetext" width="">Don't copy auth # from previous S/O#</td>
		                  <td  class="listwhitetext" width="" ><s:checkbox key="partner.doNotCopyAuthorizationSO" onclick="changeStatus();"  value="${partner.doNotCopyAuthorizationSO}" fieldValue="true" tabindex="18"/></td>
			            </tr>
						</table>
						
						
						</td>
					<configByCorp:fieldVisibility componentId="component.field.partner.partnerCreditTermsStar">	
					<tr>
						<td align="right" class="listwhitetext" style="padding-right: 2px;"><fmt:message key='accountInfo.creditTerms'/></td>
				        <td class="listwhitetext"  colspan="3" width=""><s:select cssClass="list-menu"  name="partner.creditTerms" list="%{creditTerms}" cssStyle="width:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="10"/></td>
					</tr>
					</configByCorp:fieldVisibility>
					<configByCorp:fieldVisibility componentId="component.field.partner.partnerCreditTermsSscw">	
					<s:hidden name="partner.creditTerms"></s:hidden>
					</configByCorp:fieldVisibility>
					</tr>
					<!--<tr> 
						
						<td></td>
						<td></td>
						<td></td>
						<s:hidden  name="partner.client3"  /> 
						<td align="right" class="listwhitetext">Don't send to invoice upload</td>
						<td  class="listwhitetext" width="8px" colspan="2"><s:checkbox key="partner.invoiceUploadCheck" onclick="changeStatus();"  value="${ischeckeddddd}" fieldValue="true" tabindex="18"/></td>
					    <c:set var="ischeckedddd" value="false"/>
						<c:if test="${partner.payableUploadCheck}">
						<c:set var="ischeckedddd" value="true"/>
						</c:if>
						
						<td align="right" class="listwhitetext">Don't send to payable upload</td>
						<td  class="listwhitetext" width="8px" ><s:checkbox key="partner.payableUploadCheck" onclick="changeStatus();"  value="${ischeckedddd}" fieldValue="true" tabindex="18"/></td>
					
					
					</tr> 
					--></table>
					</td>
					</tr>
					<tr><td height="5px"></td></tr>
					<tr>
							<td align="center" colspan="15" class="vertlinedata"></td>
						</tr>
					
					<tr>
					<tr><td height="5px"></td></tr>
					<tr>
					<td>
					<table style="width:98%; margin-bottom: 8px;" >	
					<tr>
					
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isPrivateParty}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="right" class="listwhitetext" >Private party<s:checkbox key="partner.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>				
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isAccount}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="right" class="listwhitetext" style="padding-right:5px"><fmt:message key='partner.accounts'/><s:checkbox key="partner.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();"tabindex="14"/></td>
						
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isAgent}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="right" class="listwhitetext" style="padding-right:15px"><fmt:message key='partner.agents'/><s:checkbox key="partner.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="15"/></td>
													
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isVendor}">
						<c:set var="ischecked" value="true"/>
						</c:if>					
						<td align="center" class="listwhitetext" ><fmt:message key='partner.vendors'/><s:checkbox key="partner.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
														
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isCarrier}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="center" class="listwhitetext" ><fmt:message key='partner.carriers'/><s:checkbox key="partner.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="17"/></td>
								
						<c:set var="ischecked" value="false"/>
						<c:if test="${partner.isOwnerOp}">
						<c:set var="ischecked" value="true"/>
						</c:if>						
						<td align="center" class="listwhitetext" ><fmt:message key='partner.owner'/><s:checkbox key="partner.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="18"/></td>
					</tr>
				
				</table>
				</td>
				</tr>
				<tr>
							<td height="10" align="left" class="listwhitetext" >
							 <div  onClick="javascript:animatedcollapse.toggle('partnerportal')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Partner Portal Detail
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
							
							<div  id="partnerportal">
						<table width="695" cellpadding="1" cellspacing="0" class="detailTabLabel">
							<tr>
								<c:set var="isActiveFlag1" value="false"/>
								<c:if test="${partner.partnerPortalActive}">
								<c:set var="isActiveFlag1" value="true"/>
								</c:if>
								<c:set var="isActiveFlag" value="false"/>
								<c:if test="${partner.viewChild}">
								<c:set var="isActiveFlag" value="true"/>
								</c:if>
							<td width="129" align="right" valign="middle" class="listwhitetext" >Partner Portal:</td>
						    <td width="1px" ><s:checkbox key="partner.partnerPortalActive" value="${isActiveFlag1}" fieldValue="true" onclick="changeStatus()" tabindex="84"/></td>
							<td width="180" align="left"><fmt:message key='customerFile.portalIdActive'/></td>
							</tr>
							<tr>
							
							<td  align="right"  class="listwhitetext" >Associated Agents</td>
							<td colspan="2"><s:textfield cssClass="input-text" name="partner.associatedAgents"   size="30" maxlength="65"/></td>
					 		
					 		<td  align="right" valign="middle" class="listwhitetext" width="200px">Allow access to child Agent Records:</td>
						    <td width="16" ><s:checkbox key="partner.viewChild" value="${isActiveFlag}" fieldValue="true" tabindex="84"/></td>
							<td><fmt:message key='customerFile.portalIdActive'/></td>
							<c:if test="${not empty partner.id}">
							<td  align="left" ><input type="button" value="Child Agent" class="cssbuttonB" onclick="window.open('getChildAgents.html?id=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true','','width=750,height=400,scrollbars=yes');""/></td>
					 		</c:if>
					  </tr>
					  </table> 
 					</div>
				</td>
					</tr>
					<tr>
							<td height="15"></td></tr>
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<table style="width:700px">

					<tr>
						<td align="right" class="listwhitetext" style="width:70px"><fmt:message key='accountProfile.createdOn'/></td>
						<c:if test="${not empty partner.createdOn}">
							<s:text id="createdOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.createdOn"/></s:text>
							<td><s:hidden cssClass="input-text" id="createdOn" name="partner.createdOn" value="%{createdOnFormattedValue}" /></td>
						</c:if>
						<c:if test="${empty partner.createdOn}">
							<td><s:hidden cssClass="input-text" id="createdOn" name="partner.createdOn" /></td>
						</c:if>
						<td style="width:130px"><fmt:formatDate value="${partner.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:70px"><fmt:message key='partner.createdBy' /></td>
						<c:if test="${not empty partner.id}">
								<s:hidden name="partner.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{partner.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="dataCatalogcreatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:85px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						
											
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.updatedOn'/></td>
							<s:text id="updatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.updatedOn"/></s:text>
							<td><s:hidden cssClass="input-text" id="updatedOn" name="partner.updatedOn" value="%{updatedOnFormattedValue}" /></td>
						<td style="width:130px"><fmt:formatDate value="${partner.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.updatedBy' /></td>
						
						<c:if test="${not empty partner.id}">
								<s:hidden name="partner.updatedBy"/>
								<td><s:label name="updatedBy" value="%{partner.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="partner.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						
						
					</tr>
					

</table>

	<table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" key="button.save" onmouseover="return chkSelect();" tabindex="18"/>  
        		</td>
       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset" tabindex="19"/> 
        		</td>
        		
        		
       	  	</tr>		  	
		  </tbody>
		  </table>
	
	
	
	</div>
<s:hidden name="partner.status"/>
<s:hidden name="partner.nationalAccount"/>
<s:hidden name="partner.accountHolder"/>
<s:hidden name="partner.paymentMethod"/>
<s:hidden name="partner.payOption"/>
<s:hidden name="partner.billingInstructionMessage"/>	
<s:hidden name="partner.storageBillingGroup"/>
<s:hidden name="partner.typeOfVendor"/>
<s:hidden name="partner.remarks1"  />
<s:hidden name="partner.partnerSuffix"/>
<s:hidden name="partner.partnerPrefix"/>
<s:hidden name="partner.middleInitial"/>
<s:hidden name="partner.mailingAddress1"/>
<s:hidden name="partner.mailingCity"/>
<s:hidden name="partner.mailingFax"/>
<s:hidden name="partner.mailingAddress2"/>
<s:hidden name="partner.mailingState" />
<s:hidden name="partner.mailingPhone"/>
<s:hidden name="partner.mailingAddress3"/>
<s:hidden name="partner.mailingZip"/>
<s:hidden name="partner.mailingTelex"/>
<s:hidden name="partner.mailingAddress4"/>
<s:hidden name="partner.mailingCountryCode"/>
<s:hidden name="partner.mailingCountry"/>
<s:hidden name="partner.mailingEmail" />
<s:hidden name="partner.terminalAddress1"/>
<s:hidden name="partner.terminalCity"/>
<s:hidden name="partner.terminalFax"/>
<s:hidden name="partner.terminalAddress2"/>
<s:hidden name="partner.terminalState" />
<s:hidden name="partner.terminalPhone"/>
<s:hidden name="partner.terminalAddress3"/>
<s:hidden name="partner.terminalZip"/>
<s:hidden name="partner.terminalTelex"/>
<s:hidden name="partner.terminalAddress4"/>
<s:hidden name="partner.terminalCountryCode"/>
<s:hidden name="partner.terminalCountry"/>
<s:hidden name="partner.terminalEmail" />
<s:hidden name="partner.billingAddress1"/>
<s:hidden name="partner.billingCity"/>
<s:hidden name="partner.billingFax"/>
<s:hidden name="partner.billingAddress2"/>
<s:hidden name="partner.billingState" />
<s:hidden name="partner.billingPhone"/>
<s:hidden name="partner.billingAddress3"/>
<s:hidden name="partner.billingZip"/>
<s:hidden name="partner.billingTelex"/>
<s:hidden name="partner.billingAddress4"/>
<s:hidden name="partner.billingCountryCode"/>
<s:hidden name="partner.billingCountry"/>
<s:hidden name="partner.billingEmail" />
<s:hidden name="partner.bases"></s:hidden>


<s:hidden name="partner.pricingUser" />
<s:hidden name="partner.billingUser" />
<s:hidden name="partner.payableUser" />

<s:hidden name="partner.creditAuthorizedBy" />

<s:hidden name="partner.validNationalCode" />

<s:hidden name="partner.multiAuthorization" />
<s:hidden name="partner.companyDivision" />

<s:hidden name="partner.sea"/>
<s:hidden name="partner.surface"/>
<s:hidden name="partner.air"/>

<s:hidden name="partner.driverAgency"/>

<s:hidden name="partner.longPercentage"/>

<s:hidden name="partner.client1"/>
<s:hidden name="partner.salesMan"/>
<s:hidden name="partner.location1" />
<s:hidden name="partner.location2" />
<s:hidden name="partner.location3" />
<s:hidden name="partner.location4" />
<s:hidden name="partner.url" />
<s:hidden name="partner.latitude" />
<s:hidden name="partner.longitude" />

<s:hidden name="partner.companyProfile" />
<s:hidden name="partner.yearEstablished" />
<s:hidden name="partner.companyFacilities" />
<s:hidden name="partner.companyCapabilities" />
<s:hidden name="partner.companyDestiantionProfile" />
<s:hidden name="partner.serviceRangeKms" />
<s:hidden name="partner.serviceRangeMiles" />
<s:hidden name="partner.fidiNumber" />
<s:hidden name="partner.OMNINumber" />
<s:hidden name="partner.IAMNumber" />
<s:hidden name="partner.AMSANumber" />
<s:hidden name="partner.WERCNumber" />
<s:hidden name="partner.facilitySizeSQFT" />
<s:hidden name="partner.facilitySizeSQMT" />
<s:hidden name="partner.qualityCertifications" />
<s:hidden name="partner.vanLineAffiliation" />
<s:hidden name="partner.serviceLines" />
</div>
</s:form>

<%-- Script Shifted from Top to Botton on 10-Sep-2012 By Kunal --%>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

<script type="text/javascript">
animatedcollapse.addDiv('partnerportal', 'fade=1,hide=1')
animatedcollapse.init()
function onlyNumsAllowed(evt, strList, bAllow){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)||(keyCode==110); 
}
function isFloat(targetElement){   
	var i;
	var s = targetElement.value;
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))){
	        if (c == ".") {
	        
	        }else{
		        alert("Only numbers are allowed here");
		        //alert(targetElement.id);
		        document.getElementById(targetElement.id).value='';
		        document.getElementById(targetElement.id).select();
		        return false;
	        }
    	}
    }
    return true;
}

var r={
 'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\,'&'\`'&'\=']/g,
 'quotes':/['\''&'\"']/g,
 'notnumbers':/[^\d]/g
};

function valid(targetElement,w){
 	targetElement.value = targetElement.value.replace(r[w],'');
}
function chkSelect(){}

function getChildAgents(){
	var partnerCode=document.forms['partnerAddForm'].elements['partner.partnerCode'].value;
}
	
function checkVendorName(){
    var vendorId = document.forms['partnerAddForm'].elements['partner.agentParent'].value;
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
}

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse2(){
    if (http2.readyState == 4){
        var results = http2.responseText
        results = results.trim();
        if(results.length>=1){
       		 return true;
        }else{
            alert("Invalid Agent Parent, Please select another");
			document.forms['partnerAddForm'].elements['partner.agentParent'].value='';  
			return false;                 
       }
    }
}

function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
</script>
	
<script>
function checkdate(clickType){
	
	if(!(clickType == 'save')){
	if ('${autoSavePrompt}' == 'No'){
		var id1 = document.forms['partnerAddForm'].elements['partner.id'].value;
		var partnerType = document.forms['partnerAddForm'].elements['partnerType'].value;
		var noSaveAction;
		if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.agentdtl'){
				noSaveAction = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.vendordtl'){
				noSaveAction = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.carrierdtl'){
				noSaveAction = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.base'){
				noSaveAction = 'baseList.html?id='+id1;
				}	
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.ratematrix'){
				noSaveAction = 'partnerRateGrids.html?partnerId='+id1;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
					noSaveAction = 'searchPartnerView.html';
				}else{
					noSaveAction = 'searchPartnerAdmin.html?id='+id1+'&partnerType='+partnerType;
				}
			}
		processAutoSave(document.forms['partnerAddForm'],'saveAgentDetails!saveOnTabChange.html', noSaveAction );
	
	}else{
	var id1 = document.forms['partnerAddForm'].elements['partner.id'].value;
	var partnerType = document.forms['partnerAddForm'].elements['partnerType'].value;
	
	if (document.forms['partnerAddForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='partnerDetail.heading'/>");
		if(agree){
			document.forms['partnerAddForm'].action = 'saveAgentDetails!saveOnTabChange.html';
			document.forms['partnerAddForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.agentdtl'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.vendordtl'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.carrierdtl'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.base'){
				location.href = 'baseList.html?id='+id1;
				}	
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.ratematrix'){
				location.href = 'partnerRateGrids.html?partnerId='+id1;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
					location.href = 'searchPartnerView.html';
				}else{
					location.href = 'searchPartnerAdmin.html?id='+id1+'&partnerType='+partnerType;
				}
				}
		}
		}
	}else{
	if(id1 != ''){
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.agentdtl'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.vendordtl'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.carrierdtl'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.base'){
				location.href = 'baseList.html?id='+id1;
				}	
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.ratematrix'){
				location.href = 'partnerRateGrids.html?partnerId='+id1;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
					location.href = 'searchPartnerView.html';
				}else{
					location.href = 'searchPartnerAdmin.html?id='+id1+'&partnerType='+partnerType;
				}
				}
	}
	}
}
}
}
function changeStatus(){
	document.forms['partnerAddForm'].elements['formStatus'].value = '1';
}
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>