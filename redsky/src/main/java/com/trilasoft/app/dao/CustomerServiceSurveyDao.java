package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CustomerServiceSurvey;
import com.trilasoft.app.model.Miscellaneous;

public interface CustomerServiceSurveyDao extends GenericDao<CustomerServiceSurvey, Long>{
	public Map<String,String> getCustomerServiceList(Long id);
}
