<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="accountLineList.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountLineList.heading'/>"/> 
<style>
 span.pagelinks {display:block;font-size:0.85em;margin-bottom:5px;margin-top:-18px;padding:2px 0;text-align:right;width:106%;}
.tdheight-footer {border: 1px solid #e0e0e0;padding: 0;}
form .table{font-size:1em;}
p{margin:0px;}
#mainPopup {padding:10px;}
.fx-left{float: left; padding-top: 7px;}
.fx-right{float: right;}

#overlay { 
	filter:alpha(opacity=70); -moz-opacity:0.7; 
	-khtml-opacity: 0.7; opacity: 0.7; 
	position:fixed; width:100%; height:100%;left:0px;top:0px; 
	z-index:999; margin:0px auto -1px auto;
	background:url(images/over-load.png);
}
</style>
<style> 
<%@ include file="/common/calenderStyle.css"%> 
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<%@page import="com.trilasoft.app.model.Company" %>
 </style>
<% 
    Company company = (Company)request.getAttribute("company");
	Calendar calc = Calendar.getInstance();
	
	calc.setTime(new Date());
	Date today=calc.getTime();
	calc.add(Calendar.DATE, 14);

	SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yy");
	format1.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
	String todayDate=format1.format(today);
	
	Date currentDate = new Date();
	SimpleDateFormat format11 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	format11.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
	String currentValueDate=format11.format(currentDate);
	
	format11 = new SimpleDateFormat("yyyy-MM-dd"); 
	currentDate =  format11.parse(currentValueDate);
	currentValueDate=format11.format(currentDate);
	%>
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
</head>

<s:form name="fxUpdateForm">
<c:if test="${showValueDate eq true}">
	<table class="" cellspacing="1" cellpadding="2" border="0" style="margin:0px;padding:0px;position:absolute;top:5px;">		
		<tr>		  		
				<td align="left" class="listwhitetext" >Value Date</td>		  
		  	  	<td align="left"> 	 
			  		<s:textfield cssClass="input-text" id="fxValueDate" name="fxValueDate" value="<%= todayDate %>" size="8" maxlength="11"  readonly="true" /> 
			  		<img id="fxValueDate_trigger" align="top" src="${pageContext.request.contextPath}/images/calender.png"HEIGHT=20 WIDTH=20 />
			  	</td>
			  	</tr>
	</table>
</c:if>
 <c:if test="${contractType eq true}">
 <style>
 .s-wid-fx{width:35%;}
 .s-wid-col-fx{width:2%;}
 </style>
 </c:if>
<s:hidden name="pageName" value="<%=request.getParameter("pageName") %>"/>
<c:set var="pageName"  value="<%=request.getParameter("pageName") %>"/>

<table cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
	<tr>
	<td class="s-wid-fx"></td>
	<td align="right" class="listwhitetext"><b>All&nbsp;Est&nbsp;Exp</b></td>
	<td>
		<input type="checkbox" id="selectAllEstimateExpense" onclick="selectAllFxUpdate(this,'estimateExpense');"/>
	</td>
	<c:if test="${multiCurrency=='Y'}">
	<td class="s-wid-col-fx"></td>
	<td align="right" class="listwhitetext"><b>All&nbsp;Est&nbsp;Rev</b></td>
	<td>
		<input type="checkbox" id="selectAllEstimateRevenue" onclick="selectAllFxUpdate(this,'estimateRevenue');"/>
	</td>
	</c:if>
	<c:if test="${discountUserFlag.pricingRevision && pageName=='SO'}">
		<td class="s-wid-col-fx"></td>
		<td align="right" class="listwhitetext"><b>All&nbsp;Rev&nbsp;Exp</b></td>
		<td>
			<input type="checkbox" id="selectAllRevisionExpense" onclick="selectAllFxUpdate(this,'revisionExpense');"/>
		</td>
		<c:if test="${multiCurrency=='Y'}">
		<td class="s-wid-col-fx"></td>
		<td align="right" class="listwhitetext"><b>All&nbsp;Rev&nbsp;Rev</b></td>
		<td>
			<input type="checkbox" id="selectAllRevisionRevenue" onclick="selectAllFxUpdate(this,'revisionRevenue');"/>
		</td>
		</c:if>
	</c:if>
	
	<c:if test="${discountUserFlag.pricingActual && pageName=='SO'}">
		<td class="s-wid-col-fx"></td>
		<td align="right" class="listwhitetext"><b>All&nbsp;Act&nbsp;Exp</b></td>
		<td>
			<input type="checkbox" id="selectAllActualExpense" onclick="selectAllFxUpdate(this,'actualExpense');"/>
		</td>
		<c:if test="${multiCurrency=='Y'}">
		<td class="s-wid-col-fx"></td>
		<td align="right" class="listwhitetext"><b>All&nbsp;Act&nbsp;Rev</b></td>
		<td>
			<input type="checkbox" id="selectAllActualRevenue" onclick="selectAllFxUpdate(this,'actualRevenue');"/>
		</td>
		</c:if>
	</c:if>
	
	<c:if test="${pageName=='AC'}">
		<td class="s-wid-col-fx"></td>
		<td align="right" class="listwhitetext"><b>All&nbsp;Rev&nbsp;Exp</b></td>
		<td>
			<input type="checkbox" id="selectAllRevisionExpense" onclick="selectAllFxUpdate(this,'revisionExpense');"/>
		</td>
		<c:if test="${multiCurrency=='Y'}">
		<td class="s-wid-col-fx"></td>
		<td align="right" class="listwhitetext"><b>All&nbsp;Rev&nbsp;Rev</b></td>
		<td>
			<input type="checkbox" id="selectAllRevisionRevenue" onclick="selectAllFxUpdate(this,'revisionRevenue');"/>
		</td>
		</c:if>
	</c:if>
	
	<c:if test="${pageName=='AC'}">
		<td class="s-wid-col-fx"></td>
		<td align="right" class="listwhitetext"><b>All&nbsp;Act&nbsp;Exp</b></td>
		<td>
			<input type="checkbox" id="selectAllActualExpense" onclick="selectAllFxUpdate(this,'actualExpense');"/>
		</td>
		<c:if test="${multiCurrency=='Y'}">
		<td class="s-wid-col-fx"></td>
		<td align="right" class="listwhitetext"><b>All&nbsp;Act&nbsp;Rev</b></td>
		<td>
			<input type="checkbox" id="selectAllActualRevenue" onclick="selectAllFxUpdate(this,'actualRevenue');"/>
		</td>
		</c:if>
	</c:if>
	
	</tr>
</table>
<display:table name="fxUpdateAccountLineList" class="table" requestURI="" id="accountLineList" style="width:100%;margin-top:1px;" defaultsort="1" pagesize="100" >   
     <display:column sortable="true" sortProperty="accountLineNumber" titleKey="accountLine.accountLineNumber"  style="width:5%;padding:0px;"><c:out value="${accountLineList.accountLineNumber}" /></display:column>
     <display:column title="Charge Code" style="width:15%;padding:0px;"> <c:out value="${accountLineList.chargeCode}" /></display:column>
     <c:if test="${contractType eq true}">
	     <display:column  sortable="true" title="FX For Currency" style="width:10%;border-right:medium solid #d1d1d1; text-align:left;padding-right:0em;!important" >
		     	Contract<br>
		     	Billing
	     </display:column>
     </c:if>
     <display:column title="<div class='fx-left'>FX</div><div class='fx-right'>Estimate<br>Expense</div>" style="width:10%;border-right:medium solid #d1d1d1; text-align:left;padding-right:0em;!important" >
            <div style="float:left;"> 
	                    <c:if test="${contractType eq true}">
	              <c:choose>
	                <c:when  test="${((accountLineList.estimatePayableContractCurrency)==(accountLineList.estCurrency)) && ((accountLineList.estimatePayableContractExchangeRate)!=(accountLineList.estExchangeRate))||((accountLineList.estimatePayableContractCurrency)!=(accountLineList.estCurrency)) && ((accountLineList.estimatePayableContractExchangeRate)==(accountLineList.estExchangeRate))}">
                    <div style="background-color:Tomato;" > 
                    <c:out value="${accountLineList.estimatePayableContractCurrency}" />
                    <c:out value="${accountLineList.estimatePayableContractExchangeRate}" /><br>
		     		</div>
		     		</c:when>
		     		<c:otherwise>
		     		<div style="float:left;"> 
		     		<c:out value="${accountLineList.estimatePayableContractCurrency}" />
                    <c:out value="${accountLineList.estimatePayableContractExchangeRate}" />
		     		</div><br>
		     		</c:otherwise>
		     		</c:choose>
		     	</c:if>
		     	 <c:choose>
	                <c:when  test="${((accountLineList.estimatePayableContractCurrency)==(accountLineList.estCurrency)) && ((accountLineList.estimatePayableContractExchangeRate)!=(accountLineList.estExchangeRate))||((accountLineList.estimatePayableContractCurrency)!=(accountLineList.estCurrency)) && ((accountLineList.estimatePayableContractExchangeRate)==(accountLineList.estExchangeRate))}">
                     <c:if test="${contractType eq true}">
                    <div style="background-color:Tomato;" > 
                   <c:out value="${accountLineList.estCurrency}" />
		           <c:out value="${accountLineList.estExchangeRate}" />
		     		</div>
		     		</c:if>
		     		 <c:if test="${contractType eq not true}">
                    <div> 
		     	<c:out value="${accountLineList.estCurrency}" />
		     	 <c:out value="${accountLineList.estExchangeRate}" />
		     		</div>
		     		</c:if>
		     		</c:when>
		     		<c:otherwise>
 	               <c:out value="${accountLineList.estCurrency}" />
		     	  <c:out value="${accountLineList.estExchangeRate}" /><br>
		     		</c:otherwise>
		     		</c:choose>
		     	
	     	</div>
            <div style="float:right;"> 
		           <input type="checkbox" id="estimateExpense${accountLineList.id}" name="estimateExpense"  onclick=""/>
		    </div> 
	           
     </display:column>
     <c:if test="${multiCurrency=='Y'}">
	     <display:column title="<div class='fx-left'>FX</div><div class='fx-right'>Estimate<br>Revenue</div>" style="width:10%;border-right:medium solid #d1d1d1; text-align:left;padding-right:0em;!important" >
	            <div style="float:left;">
		             <c:if test="${contractType eq true}">
		                   <c:choose>
		           <c:when  test="${((accountLineList.estimateContractCurrency)==(accountLineList.estSellCurrency)) && ((accountLineList.estimateContractExchangeRate)!=(accountLineList.estSellExchangeRate))||((accountLineList.estimateContractCurrency)!=(accountLineList.estSellCurrency)) && ((accountLineList.estimateContractExchangeRate)==(accountLineList.estSellExchangeRate))}">
	               <div style="background-color:Tomato;" > 
                 <c:out value="${accountLineList.estimateContractCurrency}" />
		     	<c:out value="${accountLineList.estimateContractExchangeRate}" /><br>
		     		</div>
		     		</c:when>
		     		<c:otherwise>
                  <c:out value="${accountLineList.estimateContractCurrency}" />
		     	<c:out value="${accountLineList.estimateContractExchangeRate}" /><br>
		     		</c:otherwise>
		     		</c:choose>
		     		</c:if>
		     		
		     		<c:choose>
	                <c:when  test="${((accountLineList.estimateContractCurrency)==(accountLineList.estSellCurrency)) && ((accountLineList.estimateContractExchangeRate)!=(accountLineList.estSellExchangeRate))||((accountLineList.estimateContractCurrency)!=(accountLineList.estSellCurrency)) && ((accountLineList.estimateContractExchangeRate)==(accountLineList.estSellExchangeRate))}">
	               <c:if test="${contractType eq true}">
                    <div style="background-color:Tomato;" > 
                	<c:out value="${accountLineList.estSellCurrency}" />
		     	<c:out value="${accountLineList.estSellExchangeRate}" /><br>
		     		</div>
		     		</c:if>
		     		 <c:if test="${contractType eq not true}">
                    <div> 
		     	<c:out value="${accountLineList.estSellCurrency}" />
		     	<c:out value="${accountLineList.estSellExchangeRate}" /><br>
		     		</div>
		     		</c:if>
		     		</c:when>
		     		<c:otherwise>
 	             	<c:out value="${accountLineList.estSellCurrency}" />
		     	<c:out value="${accountLineList.estSellExchangeRate}" /><br>
		     		</c:otherwise>
		     		</c:choose>
	     		</div> 
			    <div style="float:right;"> 
			             	<input type="checkbox" id="estimateRevenue${accountLineList.id}" name="estimateRevenue"  onclick=""/> 
			    </div> 
	     </display:column>
     </c:if>
     <c:if test="${discountUserFlag.pricingRevision && pageName=='SO'}">
     	<display:column title="<div class='fx-left'>FX</div><div class='fx-right'>Revision<br>Expense</div>" style="width:10%;border-right:medium solid #d1d1d1; text-align:left;padding-right:0em;!important">
            	<div style="float:left;">
	            <c:if test="${contractType eq true}">
	            	     <c:choose>
                       <c:when  test="${((accountLineList.revisionPayableContractCurrency)==(accountLineList.revisionCurrency)) && ((accountLineList.revisionPayableContractExchangeRate)!=(accountLineList.revisionExchangeRate))||((accountLineList.revisionPayableContractCurrency)!=(accountLineList.revisionCurrency)) && ((accountLineList.revisionPayableContractExchangeRate)==(accountLineList.revisionExchangeRate))}">
                             <div style="background-color:Tomato;" > 
                            <c:out value="${accountLineList.revisionPayableContractCurrency}" />
		     				<c:out value="${accountLineList.revisionPayableContractExchangeRate}" /><br>
		     				</div>
		     			</c:when>
		     			<c:otherwise>
		     			    <c:out value="${accountLineList.revisionPayableContractCurrency}" />
		     				<c:out value="${accountLineList.revisionPayableContractExchangeRate}" /><br>
		     			</c:otherwise>
		     			</c:choose>
		     		</c:if>
		     		    <c:choose>
		     		        <c:when  test="${((accountLineList.revisionPayableContractCurrency)==(accountLineList.revisionCurrency)) && ((accountLineList.revisionPayableContractExchangeRate)!=(accountLineList.revisionExchangeRate))||((accountLineList.revisionPayableContractCurrency)!=(accountLineList.revisionCurrency)) && ((accountLineList.revisionPayableContractExchangeRate)==(accountLineList.revisionExchangeRate))}">
                             <c:if test="${contractType eq true}">
                             <div style="background-color:Tomato;" > 
                         		<c:out value="${accountLineList.revisionCurrency}" />
		     				<c:out value="${accountLineList.revisionExchangeRate}" /><br>
		     				</div>
		     				</c:if>
		     				   <c:if test="${contractType eq not true}">
                             <div style="float:left;" > 
                         		<c:out value="${accountLineList.revisionCurrency}" />
		     				<c:out value="${accountLineList.revisionExchangeRate}" /><br>
		     				</div>
		     				</c:if>
		     			</c:when>
		     			<c:otherwise>
		     				<c:out value="${accountLineList.revisionCurrency}" />
		     				<c:out value="${accountLineList.revisionExchangeRate}" /><br>
		     			</c:otherwise>
		     			</c:choose>
	     		</div>
                <div style="float:right;"> 
	            		<input type="checkbox" id="revisionExpense${accountLineList.id}" name="revisionExpense"  onclick=""/>
	            </div> 
	            
      </display:column>
      <c:if test="${multiCurrency=='Y'}">
	      <display:column title="<div class='fx-left'>FX</div><div class='fx-right'>Revision<br>Revenue</div>" style="width:10%;border-right:medium solid #d1d1d1; text-align:left;padding-right:0em;!important">
	            <div style="float:left;">
            <c:if test="${contractType eq true}">
		             	     <c:choose>
                       <c:when  test="${((accountLineList.revisionContractCurrency)==(accountLineList.revisionSellCurrency)) && ((accountLineList.revisionContractExchangeRate)!=(accountLineList.revisionSellExchangeRate))||((accountLineList.revisionContractCurrency)!=(accountLineList.revisionSellCurrency)) && ((accountLineList.revisionContractExchangeRate)==(accountLineList.revisionSellExchangeRate))}">
	               <div style="background-color:Tomato;" > 
                           <c:out value="${accountLineList.revisionContractCurrency}" />
		     			 <c:out value="${accountLineList.revisionContractExchangeRate}" /><br>
		     				</div>
		     			</c:when>
		     			<c:otherwise>
		     		<c:out value="${accountLineList.revisionContractCurrency}" />
		     			 <c:out value="${accountLineList.revisionContractExchangeRate}" /><br>
		     			</c:otherwise>
		     			</c:choose>
		     		</c:if>
		     
		     	<c:choose>
                       <c:when  test="${((accountLineList.revisionContractCurrency)==(accountLineList.revisionSellCurrency)) && ((accountLineList.revisionContractExchangeRate)!=(accountLineList.revisionSellExchangeRate))||((accountLineList.revisionContractCurrency)!=(accountLineList.revisionSellCurrency)) && ((accountLineList.revisionContractExchangeRate)==(accountLineList.revisionSellExchangeRate))}">
	                            <c:if test="${contractType eq true}">
	                <div style="background-color:Tomato;" > 
                         		<c:out value="${accountLineList.revisionSellCurrency}" />
		     			 <c:out value="${accountLineList.revisionSellExchangeRate}" /><br>
		     				</div>
		     				</c:if>
		     				      <c:if test="${contractType eq not true}">
	                              <div> 
                         		<c:out value="${accountLineList.revisionSellCurrency}" />
		     			 <c:out value="${accountLineList.revisionSellExchangeRate}" /><br>
		     				</div>
		     				</c:if>
		     			</c:when>
		     			<c:otherwise>
		     				<c:out value="${accountLineList.revisionSellCurrency}" />
		     			 <c:out value="${accountLineList.revisionSellExchangeRate}" /><br>
		     			</c:otherwise>
		     			</c:choose>
	     		</div>
	            <div style="float:right;"> 
		            		<input type="checkbox" id="revisionRevenue${accountLineList.id}" name="revisionRevenue"  onclick=""/>
		        <div style="float:right;"> 
	      </display:column>
      </c:if>
     </c:if>
     <c:if test="${discountUserFlag.pricingActual && pageName=='SO'}">
            <display:column title="<div class='fx-left'>FX</div><div class='fx-right'>Actual<br>Expense</div>" style="width:10%;border-right:medium solid #d1d1d1;text-align:left;padding-right:0em; !important">
	         <div style="float:left;">
	            <c:if test="${contractType eq true}">
	            <c:set var = "exchangeRateVal" value = "${accountLineList.exchangeRate}"/>
	            <c:set var = "salary" scope = "session" value = "${accountLineList.payableContractExchangeRate}"/>
	                 <c:choose>
                       <c:when test="${(fn:contains(accountLineList.payableContractExchangeRate, accountLineList.exchangeRate) && !fn:contains(accountLineList.country, accountLineList.payableContractCurrency)) || (!fn:contains(accountLineList.payableContractExchangeRate, accountLineList.exchangeRate) && fn:contains(accountLineList.country, accountLineList.payableContractCurrency))}">
	                   <div style="background-color:Tomato;" > 
	                   <c:out value="${accountLineList.payableContractCurrency}" />
		     		   <c:out value="${accountLineList.payableContractExchangeRate}" />
		     			</div>
		     			</c:when>
		     			<c:otherwise>
	                    <div > 
	                    <c:out value="${accountLineList.payableContractCurrency}" />
		     			<c:out value="${accountLineList.payableContractExchangeRate}" /><br>
		     			</div>
		     			</c:otherwise>
		     			</c:choose>
		     	</c:if>
      			<c:set var = "exchangeRateDecimalVal" value = "${fn1:substringAfter(exchangeRateVal, '.')}" />
      			<c:if test="${exchangeRateDecimalVal == '0'}">
      				<c:set var = "exchangeRateFinalVal" value = "${fn1:replace(exchangeRateVal, '0', '0000')}" />
      				  <c:choose>
      				  <c:when test="${(fn:contains(accountLineList.payableContractExchangeRate,exchangeRateFinalVal) && !fn:contains(accountLineList.country, accountLineList.payableContractCurrency)) || (!fn:contains(accountLineList.payableContractExchangeRate, accountLineList.exchangeRate) && fn:contains(accountLineList.country, accountLineList.payableContractCurrency))}">
      				   <c:if test="${contractType eq true}">
      				 <div style="background-color:Tomato;" > 
      				  <c:out value="${accountLineList.country}" />
      				  <c:out value="${exchangeRateFinalVal}" /> 
      				  </div>
      				  </c:if>
      				  <c:if test="${contractType eq not true}">
      				 <div> 
      				  <c:out value="${accountLineList.country}" />
      				  <c:out value="${exchangeRateFinalVal}" /> 
      				  </div>
      				  </c:if>
      				  </c:when>
      				   <c:otherwise>
      				   <div > 
      				  <c:out value="${accountLineList.country}" />
      				  <c:out value="${exchangeRateFinalVal}" /> 
      				  </div>
      				   </c:otherwise>
      				 </c:choose>
      			</c:if>
      			<c:if test="${exchangeRateDecimalVal != '0'}">
      			 <c:choose>
      			  <c:when test="${(fn:contains(accountLineList.payableContractExchangeRate, accountLineList.exchangeRate) && !fn:contains(accountLineList.country, accountLineList.payableContractCurrency)) || (!fn:contains(accountLineList.payableContractExchangeRate, accountLineList.exchangeRate) && fn:contains(accountLineList.country, accountLineList.payableContractCurrency))}">
      			     <c:if test="${contractType eq true}">
      			   <div style="background-color:Tomato;" > 
      			    <c:out value="${accountLineList.country}" />
      				<c:out value="${accountLineList.exchangeRate}" />
      				</div>
      				</c:if>
      				<c:if test="${contractType eq not true}">
      			   <div> 
      			    <c:out value="${accountLineList.country}" />
      				<c:out value="${accountLineList.exchangeRate}" />
      				</div>
      				</c:if>
      				</c:when>
      				 <c:otherwise>
      				<div >
      				 <c:out value="${accountLineList.country}" />
      				<c:out value="${accountLineList.exchangeRate}" />
      				</div>
      				 </c:otherwise>
      				</c:choose>
      			</c:if>
		     	<%-- <c:out value="${accountLineList.exchangeRate}" /> --%>
		     </div> 
          <c:choose>
            <c:when test="${((!trackingStatus.soNetworkGroup && networkAgent && billingCMMContractType) 
            || (!trackingStatus.soNetworkGroup && ((freezeAccValue!='Invoicing' && not empty accountLineList.payAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.invoiceNumber!='' || not empty accountLineList.invoiceNumber)))) 
            || (trackingStatus.soNetworkGroup && (!(trackingStatus.accNetworkGroup)) && networkAgent && billingDMMContractType && ((freezeAccValue!='Invoicing' && not empty accountLineList.payAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.invoiceNumber!='' || not empty accountLineList.invoiceNumber)))) 
            || (trackingStatus.soNetworkGroup && ((trackingStatus.accNetworkGroup)) && !networkAgent && (billingDMMContractType || billingCMMContractType)  &&  ((freezeAccValue!='Invoicing' && not empty accountLineList.payAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.invoiceNumber!='' || not empty accountLineList.invoiceNumber))))) 
               }">
            <div style="float:right;">
            	<input type="checkbox" id="actualExpense${accountLineList.id}" name="actualExpense"  disabled="true"/>
            </div>
            </c:when>
             <c:otherwise>
             	<div style="float:right;">
            		<input type="checkbox" id="actualExpense${accountLineList.id}" name="actualExpense"  />
            	</div> 
            </c:otherwise>
            </c:choose>   
    </display:column>
    <c:if test="${multiCurrency=='Y'}">    
	    <display:column title="<div class='fx-left'>FX</div><div class='fx-right'>Actual<br>Revenue</div>" style="width:10%;text-align:left;padding-right:0em; !important"> 
		        <div style="float:left;">
		         <c:if test="${contractType eq true}">
				  <c:choose>
              <c:when  test="${((accountLineList.contractCurrency)==(accountLineList.recRateCurrency)) && ((accountLineList.contractExchangeRate)!=(accountLineList.recRateExchange))||((accountLineList.contractCurrency)!=(accountLineList.recRateCurrency)) && ((accountLineList.contractExchangeRate)==(accountLineList.recRateExchange))}">
	                <div style="background-color:Tomato;" >  
	            	<c:out value="${accountLineList.contractCurrency}" />
			     	<c:out value="${accountLineList.contractExchangeRate}" /><br>
	                  </div>
	             </c:when> 
	             <c:otherwise>
	             <c:out value="${accountLineList.contractCurrency}" />
			     	<c:out value="${accountLineList.contractExchangeRate}" /><br>
	             </c:otherwise>
	             </c:choose>
			     </c:if>
			      <c:choose>
               <c:when  test="${((accountLineList.contractCurrency)==(accountLineList.recRateCurrency)) && ((accountLineList.contractExchangeRate)!=(accountLineList.recRateExchange))||((accountLineList.contractCurrency)!=(accountLineList.recRateCurrency)) && ((accountLineList.contractExchangeRate)==(accountLineList.recRateExchange))}">
	            <c:if test="${contractType eq true}">
	          <div style="background-color:Tomato;" > 
	            	<c:out value="${accountLineList.recRateCurrency}" />
			     	<c:out value="${accountLineList.recRateExchange}" /><br>
	                  </div>
	                  </c:if>
	                <c:if test="${contractType eq not true}">
	                <div > 
	            	<c:out value="${accountLineList.recRateCurrency}" />
			     	<c:out value="${accountLineList.recRateExchange}" /><br>
	                  </div>
	                  </c:if>
	             </c:when> 
	             <c:otherwise>
	                <c:out value="${accountLineList.recRateCurrency}" />
			     	<c:out value="${accountLineList.recRateExchange}" /><br>
	             </c:otherwise>
	             </c:choose>
			    </div> 
	            <c:choose>
	            <c:when test="${(!trackingStatus.soNetworkGroup &&  ((freezeAccValue!='Invoicing' && not empty accountLineList.recAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.recInvoiceNumber!='' || not empty accountLineList.recInvoiceNumber)))) 
	            || (trackingStatus.soNetworkGroup && (!(trackingStatus.accNetworkGroup)) && networkAgent && billingDMMContractType && ((freezeAccValue!='Invoicing' && not empty accountLineList.recAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.recInvoiceNumber!='' || not empty accountLineList.recInvoiceNumber)))) 
	            || (trackingStatus.soNetworkGroup && ((trackingStatus.accNetworkGroup)) && !networkAgent && (billingDMMContractType || billingCMMContractType)  &&  ((freezeAccValue!='Invoicing' && not empty accountLineList.recAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.recInvoiceNumber!='' || not empty accountLineList.recInvoiceNumber)) || accountLineList.utsiRecAccDateFlag || accountLineList.utsiPayAccDateFlag))}">
	            <div style="float:right;">
	            	<input type="checkbox" id="actualRevenue${accountLineList.id}" name="actualRevenue"  disabled="true"/>
	            </div>
	            </c:when>
	             <c:otherwise>
	             	<div style="float:right;">
	            		<input type="checkbox" id="actualRevenue${accountLineList.id}" name="actualRevenue"  />
	            	</div>
	            </c:otherwise>
	            </c:choose> 
	    </display:column> 
    </c:if>
 </c:if>
 
      <c:if test="${pageName=='AC'}">
     	<display:column title="<div class='fx-left'>FX</div><div class='fx-right'>Revision<br>Expense</div>" style="width:10%;border-right:medium solid #d1d1d1; text-align:left;padding-right:0em;!important">
            	<div style="float:left;">
	           <c:if test="${contractType eq true}">
	            	     <c:choose>
                       <c:when  test="${((accountLineList.revisionPayableContractCurrency)==(accountLineList.revisionCurrency)) && ((accountLineList.revisionPayableContractExchangeRate)!=(accountLineList.revisionExchangeRate))||((accountLineList.revisionPayableContractCurrency)!=(accountLineList.revisionCurrency)) && ((accountLineList.revisionPayableContractExchangeRate)==(accountLineList.revisionExchangeRate))}">
                       <div style="background-color:Tomato;" >  
                            <c:out value="${accountLineList.revisionPayableContractCurrency}" />
		     				<c:out value="${accountLineList.revisionPayableContractExchangeRate}" /><br>
		     				</div>
		     			</c:when>
		     			<c:otherwise>
		     			    <c:out value="${accountLineList.revisionPayableContractCurrency}" />
		     				<c:out value="${accountLineList.revisionPayableContractExchangeRate}" /><br>
		     			</c:otherwise>
		     			</c:choose>
		     		</c:if>
		     		    <c:choose>
		     		        <c:when  test="${((accountLineList.revisionPayableContractCurrency)==(accountLineList.revisionCurrency)) && ((accountLineList.revisionPayableContractExchangeRate)!=(accountLineList.revisionExchangeRate))||((accountLineList.revisionPayableContractCurrency)!=(accountLineList.revisionCurrency)) && ((accountLineList.revisionPayableContractExchangeRate)==(accountLineList.revisionExchangeRate))}">
                               <c:if test="${contractType eq true}">
                            <div style="background-color:Tomato;" > 
                         		<c:out value="${accountLineList.revisionCurrency}" />
		     				<c:out value="${accountLineList.revisionExchangeRate}" /><br>
		     				</div>
		     				</c:if>
		     				     <c:if test="${contractType eq not true}">
                             <div> 
                         		<c:out value="${accountLineList.revisionCurrency}" />
		     				<c:out value="${accountLineList.revisionExchangeRate}" /><br>
		     				</div>
		     				</c:if>
		     			</c:when>
		     			<c:otherwise>
		     				<c:out value="${accountLineList.revisionCurrency}" />
		     				<c:out value="${accountLineList.revisionExchangeRate}" /><br>
		     			</c:otherwise>
		     			</c:choose>
	     		</div>
                <div style="float:right;"> 
	            		<input type="checkbox" id="revisionExpense${accountLineList.id}" name="revisionExpense"  onclick=""/>
	            </div> 
	            
      </display:column>
      <c:if test="${multiCurrency=='Y'}">
	      <display:column title="<div class='fx-left'>FX</div><div class='fx-right'>Revision<br>Revenue</div>" style="width:10%;border-right:medium solid #d1d1d1; text-align:left;padding-right:0em;!important">
	            <div style="float:left;">
		               <c:if test="${contractType eq true}">
		             	     <c:choose>
                       <c:when  test="${((accountLineList.revisionContractCurrency)==(accountLineList.revisionSellCurrency)) && ((accountLineList.revisionContractExchangeRate)!=(accountLineList.revisionSellExchangeRate))||((accountLineList.revisionContractCurrency)!=(accountLineList.revisionSellCurrency)) && ((accountLineList.revisionContractExchangeRate)==(accountLineList.revisionSellExchangeRate))}">
	                           <div style="background-color:Tomato;" > 
                           <c:out value="${accountLineList.revisionContractCurrency}" />
		     			 <c:out value="${accountLineList.revisionContractExchangeRate}" /><br>
		     				</div>
		     			</c:when>
		     			<c:otherwise>
		     		     <c:out value="${accountLineList.revisionContractCurrency}" />
		     			 <c:out value="${accountLineList.revisionContractExchangeRate}" /><br>
		     			</c:otherwise>
		     			</c:choose>
		     		</c:if>
		     
		     	<c:choose>
                       <c:when  test="${((accountLineList.revisionContractCurrency)==(accountLineList.revisionSellCurrency)) && ((accountLineList.revisionContractExchangeRate)!=(accountLineList.revisionSellExchangeRate))||((accountLineList.revisionContractCurrency)!=(accountLineList.revisionSellCurrency)) && ((accountLineList.revisionContractExchangeRate)==(accountLineList.revisionSellExchangeRate))}">
	                         <c:if test="${contractType eq true}">
	                       <div style="background-color:Tomato;" > 
                         		<c:out value="${accountLineList.revisionSellCurrency}" />   
		     			 <c:out value="${accountLineList.revisionSellExchangeRate}" /><br>
		     				</div>
		     				</c:if>
		     				 <c:if test="${contractType eq not true}">
	                       <div> 
                         		<c:out value="${accountLineList.revisionSellCurrency}" />
		     			 <c:out value="${accountLineList.revisionSellExchangeRate}" /><br>
		     				</div>
		     				</c:if>
		     			</c:when>
		     			<c:otherwise>
		     				<c:out value="${accountLineList.revisionSellCurrency}" />
		     			 <c:out value="${accountLineList.revisionSellExchangeRate}" /><br>
		     			</c:otherwise>
		     			</c:choose>
	     		</div>
	            <div style="float:right;"> 
		            		<input type="checkbox" id="revisionRevenue${accountLineList.id}" name="revisionRevenue"  onclick=""/>
		        <div style="float:right;"> 
	      </display:column>
      </c:if>
     </c:if>
     
     <c:if test="${pageName=='AC'}">
            <display:column title="<div class='fx-left'>FX</div><div class='fx-right'>Actual<br>Expense</div>" style="width:10%;border-right:medium solid #d1d1d1;text-align:left;padding-right:0em; !important">
	         <div style="float:left;">
	            <c:if test="${contractType eq true}">
	            <c:set var = "exchangeRateVal" value = "${accountLineList.exchangeRate}"/>
	            <c:set var = "salary" scope = "session" value = "${accountLineList.payableContractExchangeRate}"/>
	                 <c:choose>
                       <c:when test="${(fn:contains(accountLineList.payableContractExchangeRate, accountLineList.exchangeRate) && !fn:contains(accountLineList.country, accountLineList.payableContractCurrency)) || (!fn:contains(accountLineList.payableContractExchangeRate, accountLineList.exchangeRate) && fn:contains(accountLineList.country, accountLineList.payableContractCurrency))}">
	                   <div style="background-color:Tomato;" > 
	                   <c:out value="${accountLineList.payableContractCurrency}" />
		     		   <c:out value="${accountLineList.payableContractExchangeRate}" />
		     			</div>
		     			</c:when>
		     			<c:otherwise>
	                    <div> 
	                    <c:out value="${accountLineList.payableContractCurrency}" />
		     			<c:out value="${accountLineList.payableContractExchangeRate}" /><br>
		     			</div>
		     			</c:otherwise>
		     			</c:choose>
		     	</c:if>
      			<c:set var = "exchangeRateDecimalVal" value = "${fn1:substringAfter(exchangeRateVal, '.')}" />
      			<c:if test="${exchangeRateDecimalVal == '0'}">
      				<c:set var = "exchangeRateFinalVal" value = "${fn1:replace(exchangeRateVal, '0', '0000')}" />
      				  <c:choose>
      				  <c:when test="${(fn:contains(accountLineList.payableContractExchangeRate,exchangeRateFinalVal) && !fn:contains(accountLineList.country, accountLineList.payableContractCurrency)) || (!fn:contains(accountLineList.payableContractExchangeRate, accountLineList.exchangeRate) && fn:contains(accountLineList.country, accountLineList.payableContractCurrency))}">
      		              <c:if test="${contractType eq true}">
      		           <div style="background-color:Tomato;"> 
      				  <c:out value="${accountLineList.country}" />
      				  <c:out value="${exchangeRateFinalVal}" /> 
      				  </div>
      				  </c:if>
      				  <c:if test="${contractType eq not true}">
      		           <div> 
      				  <c:out value="${accountLineList.country}" />
      				  <c:out value="${exchangeRateFinalVal}" /> 
      				  </div>
      				  </c:if>
      				  </c:when>
      				   <c:otherwise>
      				   <div > 
      				  <c:out value="${accountLineList.country}" />
      				  <c:out value="${exchangeRateFinalVal}" /> 
      				  </div>
      				   </c:otherwise>
      				 </c:choose>
      			</c:if>
      			<c:if test="${exchangeRateDecimalVal != '0'}">
      			 <c:choose>
      			  <c:when test="${(fn:contains(accountLineList.payableContractExchangeRate, accountLineList.exchangeRate) && !fn:contains(accountLineList.country, accountLineList.payableContractCurrency)) || (!fn:contains(accountLineList.payableContractExchangeRate, accountLineList.exchangeRate) && fn:contains(accountLineList.country, accountLineList.payableContractCurrency))}">
      	                   <c:if test="${contractType eq true}">
      	             <div style="background-color:Tomato;"> 
      			    <c:out value="${accountLineList.country}" />
      				<c:out value="${accountLineList.exchangeRate}"/>
      				</div>
      				</c:if>
      				      <c:if test="${contractType eq not true}">
      	             <div"> 
      			    <c:out value="${accountLineList.country}" />
      				<c:out value="${accountLineList.exchangeRate}"/>
      				</div>
      				</c:if>
      				</c:when>
      				 <c:otherwise>
      				<div >
      				 <c:out value="${accountLineList.country}" />
      				<c:out value="${accountLineList.exchangeRate}" />
      				</div>
      				 </c:otherwise>
      				</c:choose>
      			</c:if>
		     	<%-- <c:out value="${accountLineList.exchangeRate}" /> --%>
		     </div> 
            <c:choose>
            <c:when test="${((!trackingStatus.soNetworkGroup && networkAgent && billingCMMContractType) 
            || (!trackingStatus.soNetworkGroup && ((freezeAccValue!='Invoicing' && not empty accountLineList.payAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.invoiceNumber!='' || not empty accountLineList.invoiceNumber)))) 
            || (trackingStatus.soNetworkGroup && (!(trackingStatus.accNetworkGroup)) && networkAgent && billingDMMContractType && ((freezeAccValue!='Invoicing' && not empty accountLineList.payAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.invoiceNumber!='' || not empty accountLineList.invoiceNumber)))) 
            || (trackingStatus.soNetworkGroup && ((trackingStatus.accNetworkGroup)) && !networkAgent && (billingDMMContractType || billingCMMContractType)  &&  ((freezeAccValue!='Invoicing' && not empty accountLineList.payAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.invoiceNumber!='' || not empty accountLineList.invoiceNumber))))) 
               }">
            <div style="float:right;">
            	<input type="checkbox" id="actualExpense${accountLineList.id}" name="actualExpense"  disabled="true"/>
            </div>
            </c:when>
             <c:otherwise>
             	<div style="float:right;">
            		<input type="checkbox" id="actualExpense${accountLineList.id}" name="actualExpense"  />
            	</div> 
            </c:otherwise>
            </c:choose>  
    </display:column>
    <c:if test="${multiCurrency=='Y'}">    
	    <display:column title="<div class='fx-left'>FX</div><div class='fx-right'>Actual<br>Revenue</div>" style="width:10%;text-align:left;padding-right:0em; !important"> 
		        <div style="float:left;">
		           <c:if test="${contractType eq true}">
				  <c:choose>
              <c:when  test="${((accountLineList.contractCurrency)==(accountLineList.recRateCurrency)) && ((accountLineList.contractExchangeRate)!=(accountLineList.recRateExchange))||((accountLineList.contractCurrency)!=(accountLineList.recRateCurrency)) && ((accountLineList.contractExchangeRate)==(accountLineList.recRateExchange))}">
	               <div style="background-color:Tomato;" > 
	                <c:out value="${accountLineList.contractCurrency}" />
	            	<c:out value="${accountLineList.contractExchangeRate}" />
	                  </div>
	             </c:when> 
	             <c:otherwise>
	                   <c:out value="${accountLineList.contractCurrency}" />
	            	<c:out value="${accountLineList.contractExchangeRate}" /><br>
	             </c:otherwise>
	             </c:choose>
			     </c:if>
			      <c:choose>
              <c:when  test="${((accountLineList.contractCurrency)==(accountLineList.recRateCurrency)) && ((accountLineList.contractExchangeRate)!=(accountLineList.recRateExchange))||((accountLineList.contractCurrency)!=(accountLineList.recRateCurrency)) && ((accountLineList.contractExchangeRate)==(accountLineList.recRateExchange))}">
	            <c:if test="${contractType eq true}">
	          <div style="background-color:Tomato;" > 
	                 <c:out value="${accountLineList.recRateCurrency}" />
	              <c:out value="${accountLineList.recRateExchange}" /><br>
	                  </div>
	                  </c:if>
	                <c:if test="${contractType eq not true}">
	                <div> 
	            	<c:out value="${accountLineList.recRateCurrency}" />
	              <c:out value="${accountLineList.recRateExchange}" /><br>
	                  </div>
	                  </c:if>
	             </c:when> 
	             <c:otherwise>
	              <c:out value="${accountLineList.recRateCurrency}" />
	              <c:out value="${accountLineList.recRateExchange}" /><br>
	             </c:otherwise>
	             </c:choose>
			    </div> 
	            <c:choose>
	            <c:when test="${(!trackingStatus.soNetworkGroup &&  ((freezeAccValue!='Invoicing' && not empty accountLineList.recAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.recInvoiceNumber!='' || not empty accountLineList.recInvoiceNumber)))) 
	            || (trackingStatus.soNetworkGroup && (!(trackingStatus.accNetworkGroup)) && networkAgent && billingDMMContractType && ((freezeAccValue!='Invoicing' && not empty accountLineList.recAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.recInvoiceNumber!='' || not empty accountLineList.recInvoiceNumber)))) 
	            || (trackingStatus.soNetworkGroup && ((trackingStatus.accNetworkGroup)) && !networkAgent && (billingDMMContractType || billingCMMContractType)  &&  ((freezeAccValue!='Invoicing' && not empty accountLineList.recAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.recInvoiceNumber!='' || not empty accountLineList.recInvoiceNumber)) || accountLineList.utsiRecAccDateFlag || accountLineList.utsiPayAccDateFlag))}">
	            <div style="float:right;">
	            	<input type="checkbox" id="actualRevenue${accountLineList.id}" name="actualRevenue"  disabled="true"/>
	            </div>
	            </c:when>
	             <c:otherwise>
	             	<div style="float:right;">
	            		<input type="checkbox" id="actualRevenue${accountLineList.id}" name="actualRevenue"  />
	            	</div>
	            </c:otherwise>
	            </c:choose> 
	    </display:column> 
    </c:if>
 </c:if>
 
</display:table> 
        
    <div id="overlay">
	   <table cellspacing="0" cellpadding="0" border="0" width="100%" >
		   <tr>
			   <td align="center">
				   <table cellspacing="0" cellpadding="3" align="center">
				   		<tr>
				   			<td height="200px"></td>
				   		</tr>
				   		<tr>
					        <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
					            <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait...</font>
					        </td>
				        </tr>
				       <tr>
					      	<td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
					           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
					       </td>
				       </tr>
				    </table>
			    </td>
		    </tr>
	    </table>
   </div>
</s:form>
     <c:if test="${fxUpdateAccountLineList!='[]'}">    
     <input type="button" class="cssbuttonA" style="width:85px;"  name="Update FX"  value="Update FX" onclick="calculateFxUpdate('${pageName}')" />
     </c:if>
     <c:if test="${fxUpdateAccountLineList=='[]'}">
      <input type="button" class="cssbuttonA" style="width:85px;"  name="Update FX"  value="Update FX" disabled="disabled" />
     </c:if>  
     <input type="button" class="cssbuttonA" style="width:120px;"  value="Clear check boxes" onclick="checkAll(this)" />
     <input type="button" class="cssbuttonA" style="width:100px;"  value="Close Screen" onclick="javascript:window.close()" />    
     
     <script type="text/javascript">
     
     function calculateFxUpdate(pageName){
         var fxUpdateValue='';
         var estimateExpense ='';
         var estimateRevenue = '';
         var revisionExpense = '';
         var revisionRevenue = '';
         var actualExpense = '';
         var actualRevenue = '';
         <c:forEach var="entry" items="${fxUpdateAccountLineList}">
         var id="${entry.id}";
         var sid="${serviceOrder.id}";
         var showValueDate = "${showValueDate}";
         var fxValueDate = null;
         <c:if test="${showValueDate eq true}">
         	fxValueDate = document.forms['fxUpdateForm'].elements['fxValueDate'].value;
         	
         	var day = '';
   		   	var month = '';
   		   	var year = '';
   		   	var finalDate ='';
   	    	if(fxValueDate!='' && fxValueDate.indexOf('-')>-1){
   				var mySplitResult = fxValueDate.split("-");
   		  	    day = mySplitResult[0];
   		  	    month = mySplitResult[1];
   		  	    year = mySplitResult[2];
   		  	  if(month == 'Jan')  { month = "01";
   		  	   } else if(month == 'Feb') { month = "02";
   		  	   } else if(month == 'Mar') { month = "03"
   		  	   } else if(month == 'Apr') { month = "04"
   		  	   } else if(month == 'May') { month = "05"
   		  	   } else if(month == 'Jun') { month = "06"
   		  	   } else if(month == 'Jul') { month = "07"
   		  	   } else if(month == 'Aug') {month = "08"
   		  	   } else if(month == 'Sep') {month = "09"
   		  	   } else if(month == 'Oct') {month = "10"
   		  	   } else if(month == 'Nov') { month = "11"
   		  	   } else if(month == 'Dec') {month = "12";
   		  	   }
   	    	    finalDate = month+"-"+day+"-"+year;
   	    	}
   	    	var currentD = '<%= currentValueDate %>';
   		    var date3SplitResult = currentD.split("-");
   	 		var day3 = date3SplitResult[2];
   	 		var month3 = date3SplitResult[1];
   	 		var year3 = date3SplitResult[0];
   	 		var currentDate = new Date(month3+"/"+day3+"/"+year3);
	   	 	var diff =0;
	        if(finalDate!='' && finalDate.indexOf('-')>-1){
	          var actArvDate = finalDate.split("-");
			  var actArvDD = actArvDate[0];
			  var actArvMM = actArvDate[1];
			  var actArvYY = actArvDate[2];
			  var yr="20";
			  var actArvFinalDate = actArvDD+"/"+actArvMM+"/"+yr+actArvYY;
			  var date5 = new Date(actArvFinalDate)
			  var diff = Math.round((date5-currentDate)/86400000);
	        }
	        if(diff > 0){
	    		alert("System cannot accept future date.Please edit Value date.")
	    		return false;
			}	
     	 </c:if>
         estimateExpense = document.getElementById('estimateExpense'+id).checked;
         estimateRevenue=false;
         <c:if test="${multiCurrency=='Y'}">
         	estimateRevenue = document.getElementById('estimateRevenue'+id).checked;
         </c:if>
         <c:if test="${(pageName=='AC') || (discountUserFlag.pricingRevision && pageName=='SO')}">
         revisionExpense = document.getElementById('revisionExpense'+id).checked;
         revisionRevenue=false;
         <c:if test="${multiCurrency=='Y'}">
         	revisionRevenue = document.getElementById('revisionRevenue'+id).checked;
         </c:if>
         </c:if>
         <c:if test="${(pageName=='AC') || (discountUserFlag.pricingActual && pageName=='SO')}">
         actualExpense = document.getElementById('actualExpense'+id).checked;
         actualRevenue=false;
         <c:if test="${multiCurrency=='Y'}">
         	actualRevenue = document.getElementById('actualRevenue'+id).checked; 
         </c:if>
         </c:if>
         if(estimateExpense == true || estimateRevenue == true || revisionExpense == true || revisionRevenue == true || actualExpense == true || actualRevenue == true ){
        	 if(fxUpdateValue != ''){
             fxUpdateValue = fxUpdateValue+"~";    
         }
        	 
         
          if(estimateExpense == true){
             if(fxUpdateValue == ''){
                 fxUpdateValue ="fxEstimateExpense"
             }else{
                 fxUpdateValue=fxUpdateValue+"@"+"fxEstimateExpense"
             }
         }else{
             if(fxUpdateValue == ''){
                 fxUpdateValue =" "
             }else{
                 fxUpdateValue=fxUpdateValue+"@"+" "
             }
         }
         
         if(estimateRevenue == true){
             if(fxUpdateValue == ''){
                 fxUpdateValue ="fxEstimateRevenue"
             }else{
                 fxUpdateValue=fxUpdateValue+"@"+"fxEstimateRevenue"
             }
         } else {
             if(fxUpdateValue == ''){
                 fxUpdateValue =" "
             }else{
                 fxUpdateValue=fxUpdateValue+"@"+" "
             }
         }
         
         if(revisionExpense == true){
             if(fxUpdateValue == ''){
                 fxUpdateValue ="fxRevisionExpense"
             }else{
                 fxUpdateValue=fxUpdateValue+"@"+"fxRevisionExpense"
             }
         } else {
             if(fxUpdateValue == ''){
                 fxUpdateValue =" "
             }else{
                 fxUpdateValue=fxUpdateValue+"@"+" "
             }
         }
         
         if(revisionRevenue == true){
             if(fxUpdateValue == ''){
                 fxUpdateValue ="fxRevisionRevenue"
             }else{
                 fxUpdateValue=fxUpdateValue+"@"+"fxRevisionRevenue"
             }
         } else {
             if(fxUpdateValue == ''){
                 fxUpdateValue =" "
             }else{
                 fxUpdateValue=fxUpdateValue+"@"+" "
             }
         }
         
         if(actualExpense == true){
             if(fxUpdateValue == ''){
                 fxUpdateValue ="fxActualExpense"
             }else{
                 fxUpdateValue=fxUpdateValue+"@"+"fxActualExpense"
             }   
         } else {
             if(fxUpdateValue == ''){
                 fxUpdateValue =" "
             }else{
                 fxUpdateValue=fxUpdateValue+"@"+" "
             }
         }
         
         if(actualRevenue == true){
             if(fxUpdateValue == ''){
                 fxUpdateValue ="fxActualRevenue"
             }else{
                 fxUpdateValue=fxUpdateValue+"@"+"fxActualRevenue"
             }   
         } else {
             if(fxUpdateValue == ''){
                 fxUpdateValue =" "
             }else{
                 fxUpdateValue=fxUpdateValue+"@"+" "
             }
         }
         
         if(fxUpdateValue != ''){             
             fxUpdateValue = fxUpdateValue+"@"+id;   
         }
         }
         </c:forEach>
         if(fxUpdateValue != ''){ 
         showOrHide(1);
        var pageName = '${pageName}';
     	$.ajax({
            type: "POST",
            url: "fxUpdateCalulationAjax.html?ajax=1&decorator=simple&popup=true",
            data: {fxUpdateValue:fxUpdateValue,sid:sid,showValueDate:showValueDate,fxValueDate:fxValueDate},
            success: function (data, textStatus, jqXHR) {
           	if(pageName=="SO"){
           	 window.opener.findSoAllPricing();
           	  window.close();
           	}else{
           	 refreshAndClose();
           	}
            },
          error: function (data, textStatus, jqXHR) {
                      showOrHide(0);
                  }
          }); 
         }else{
        	 alert("Please select any check box. ")
         }    
   }
     
     function checkAll(ele) {
         var checkboxes = document.getElementsByTagName('input');
         if (ele.checked) {
             for (var i = 0; i < checkboxes.length; i++) {
                 if (checkboxes[i].type == 'checkbox') {
                     checkboxes[i].checked = true;
                 }
             }
         } else {
             for (var i = 0; i < checkboxes.length; i++) {
                 console.log(i)
                 if (checkboxes[i].type == 'checkbox') {
                     checkboxes[i].checked = false;
                 }
             }
         }
     }
     
     function refreshAndClose() {
         window.opener.location.reload(true);
         window.close();
     }
     
     
     function showOrHide(value) {
            if (value == 0) {
                if (document.layers)
                   document.layers["overlay"].visibility='hide';
                else
                   document.getElementById("overlay").style.visibility='hidden';
            }else if (value == 1) {
                if (document.layers)
                  document.layers["overlay"].visibility='show';
                else
                  document.getElementById("overlay").style.visibility='visible';
            }
     }
     function selectAllFxUpdate(target,type){
                var fxUpdateValue='';
                var estimateExpense ='';
                var estimateRevenue = '';
                var revisionExpense = '';
                var revisionRevenue = '';
                var actualExpense = '';
                var actualRevenue = '';
                
                <c:forEach var="entry" items="${fxUpdateAccountLineList}">
                var id="${entry.id}";
                var sid="${serviceOrder.id}";
                var showValueDate = "${showValueDate}";
                var fxValueDate = null;
                <c:if test="${showValueDate eq true}">
                	fxValueDate = document.forms['fxUpdateForm'].elements['fxValueDate'].value;
                	
                	var day = '';
          		   	var month = '';
          		   	var year = '';
          		   	var finalDate ='';
          	    	if(fxValueDate!='' && fxValueDate.indexOf('-')>-1){
          				var mySplitResult = fxValueDate.split("-");
          		  	    day = mySplitResult[0];
          		  	    month = mySplitResult[1];
          		  	    year = mySplitResult[2];
          		  	  if(month == 'Jan')  { month = "01";
          		  	   } else if(month == 'Feb') { month = "02";
          		  	   } else if(month == 'Mar') { month = "03"
          		  	   } else if(month == 'Apr') { month = "04"
          		  	   } else if(month == 'May') { month = "05"
          		  	   } else if(month == 'Jun') { month = "06"
          		  	   } else if(month == 'Jul') { month = "07"
          		  	   } else if(month == 'Aug') {month = "08"
          		  	   } else if(month == 'Sep') {month = "09"
          		  	   } else if(month == 'Oct') {month = "10"
          		  	   } else if(month == 'Nov') { month = "11"
          		  	   } else if(month == 'Dec') {month = "12";
          		  	   }
          	    	    finalDate = month+"-"+day+"-"+year;
          	    	}
          	    	var currentD = '<%= currentValueDate %>';
          		    var date3SplitResult = currentD.split("-");
          	 		var day3 = date3SplitResult[2];
          	 		var month3 = date3SplitResult[1];
          	 		var year3 = date3SplitResult[0];
          	 		var currentDate = new Date(month3+"/"+day3+"/"+year3);
       	   	 	var diff =0;
       	        if(finalDate!='' && finalDate.indexOf('-')>-1){
       	          var actArvDate = finalDate.split("-");
       			  var actArvDD = actArvDate[0];
       			  var actArvMM = actArvDate[1];
       			  var actArvYY = actArvDate[2];
       			  var yr="20";
       			  var actArvFinalDate = actArvDD+"/"+actArvMM+"/"+yr+actArvYY;
       			  var date5 = new Date(actArvFinalDate)
       			  var diff = Math.round((date5-currentDate)/86400000);
       	        }
       	        if(diff > 0){
       	    		alert("System cannot accept future date.Please edit Value date.")
       	    		return false;
       			}	
            	 </c:if>
                if(type=='estimateExpense'){
 			 	    if(target.checked==true){
 			 	    	estimateExpense = document.getElementById('estimateExpense'+id).checked=true;
 			 	    }else if(target.checked==false){
 			 	    	estimateExpense = document.getElementById('estimateExpense'+id).checked=false;
 			 	    }
            	}
                estimateRevenue=false;
                if(type=='estimateRevenue'){
	                <c:if test="${multiCurrency=='Y'}">
	 			 	    if(target.checked==true){
	 			 	    	estimateRevenue = document.getElementById('estimateRevenue'+id).checked=true;
	 			 	    }else if(target.checked==false){
	 			 	    	estimateRevenue = document.getElementById('estimateRevenue'+id).checked=false;
	 			 	    }
	                </c:if>
                }
                if(type=='revisionExpense'){
 			 	    if(target.checked==true){
 			 	    	revisionExpense = document.getElementById('revisionExpense'+id).checked=true;
 			 	    }else if(target.checked==false){
 			 	    	revisionExpense = document.getElementById('revisionExpense'+id).checked=false;
 			 	    }
                }
                revisionRevenue=false;
                if(type=='revisionRevenue'){
	                <c:if test="${multiCurrency=='Y'}">
	 			 	    if(target.checked==true){
	 			 	    	revisionRevenue = document.getElementById('revisionRevenue'+id).checked=true;
	 			 	    }else if(target.checked==false){
	 			 	    	revisionRevenue = document.getElementById('revisionRevenue'+id).checked=false;
	 			 	    }
	                </c:if>
                }
                if(type=='actualExpense'){
 			 	    if(target.checked==true){
 			 	    	<c:choose>
 			            <c:when test="${((!trackingStatus.soNetworkGroup && networkAgent && billingCMMContractType) || (!trackingStatus.soNetworkGroup && ((freezeAccValue!='Invoicing' && not empty entry.payAccDate) || (freezeAccValue=='Invoicing' && (entry.invoiceNumber!='' || not empty entry.invoiceNumber)))) || (trackingStatus.soNetworkGroup && (!(trackingStatus.accNetworkGroup)) && networkAgent && billingDMMContractType && ((freezeAccValue!='Invoicing' && not empty entry.payAccDate) || (freezeAccValue=='Invoicing' && (entry.invoiceNumber!='' || not empty entry.invoiceNumber)))) || (trackingStatus.soNetworkGroup && ((trackingStatus.accNetworkGroup)) && !networkAgent && (billingDMMContractType || billingCMMContractType)  &&  ((freezeAccValue!='Invoicing' && not empty entry.payAccDate) || (freezeAccValue=='Invoicing' && (entry.invoiceNumber!='' || not empty entry.invoiceNumber)))))}">
 			            	actualExpense = document.getElementById('actualExpense'+id).checked=false;
 			            </c:when>
 			            <c:otherwise>
 			            	actualExpense = document.getElementById('actualExpense'+id).checked=true;
 			            </c:otherwise>
 			            </c:choose>
 			 	    }else if(target.checked==false){
 			 	    	actualExpense = document.getElementById('actualExpense'+id).checked=false;
 			 	    }
                }
                actualRevenue=false;
                if(type=='actualRevenue'){
	                <c:if test="${multiCurrency=='Y'}">
	 			 	    if(target.checked==true){
	 			 	    	<c:choose>
	 			            <c:when test="${(!trackingStatus.soNetworkGroup &&  ((freezeAccValue!='Invoicing' && not empty entry.recAccDate) || (freezeAccValue=='Invoicing' && (entry.recInvoiceNumber!='' || not empty entry.recInvoiceNumber)))) || (trackingStatus.soNetworkGroup && (!(trackingStatus.accNetworkGroup)) && networkAgent && billingDMMContractType && ((freezeAccValue!='Invoicing' && not empty entry.recAccDate) || (freezeAccValue=='Invoicing' && (entry.recInvoiceNumber!='' || not empty entry.recInvoiceNumber)))) || (trackingStatus.soNetworkGroup && ((trackingStatus.accNetworkGroup)) && !networkAgent && (billingDMMContractType || billingCMMContractType)  &&  ((freezeAccValue!='Invoicing' && not empty entry.recAccDate) || (freezeAccValue=='Invoicing' && (entry.recInvoiceNumber!='' || not empty entry.recInvoiceNumber)) || entry.utsiRecAccDateFlag || entry.utsiPayAccDateFlag))}">
	 			            	actualRevenue = document.getElementById('actualRevenue'+id).checked=false;
	 			            </c:when>
	 			            <c:otherwise>
	 			            	actualRevenue = document.getElementById('actualRevenue'+id).checked=true;
	 			            </c:otherwise>
	 			            </c:choose> 
	 			 	    }else if(target.checked==false){
	 			 	    	actualRevenue = document.getElementById('actualRevenue'+id).checked=false;
	 			 	    }
	                </c:if>
                }
                if(estimateExpense == true || estimateRevenue == true || revisionExpense == true || revisionRevenue == true || actualExpense == true || actualRevenue == true ){
               	 if(fxUpdateValue != ''){
                    fxUpdateValue = fxUpdateValue+"~";    
                }
               	 
                
                 if(estimateExpense == true){
                    if(fxUpdateValue == ''){
                        fxUpdateValue ="fxEstimateExpense"
                    }else{
                        fxUpdateValue=fxUpdateValue+"@"+"fxEstimateExpense"
                    }
                }else{
                    if(fxUpdateValue == ''){
                        fxUpdateValue =" "
                    }else{
                        fxUpdateValue=fxUpdateValue+"@"+" "
                    }
                }
                
                if(estimateRevenue == true){
                    if(fxUpdateValue == ''){
                        fxUpdateValue ="fxEstimateRevenue"
                    }else{
                        fxUpdateValue=fxUpdateValue+"@"+"fxEstimateRevenue"
                    }
                } else {
                    if(fxUpdateValue == ''){
                        fxUpdateValue =" "
                    }else{
                        fxUpdateValue=fxUpdateValue+"@"+" "
                    }
                }
                
                if(revisionExpense == true){
                    if(fxUpdateValue == ''){
                        fxUpdateValue ="fxRevisionExpense"
                    }else{
                        fxUpdateValue=fxUpdateValue+"@"+"fxRevisionExpense"
                    }
                } else {
                    if(fxUpdateValue == ''){
                        fxUpdateValue =" "
                    }else{
                        fxUpdateValue=fxUpdateValue+"@"+" "
                    }
                }
                
                if(revisionRevenue == true){
                    if(fxUpdateValue == ''){
                        fxUpdateValue ="fxRevisionRevenue"
                    }else{
                        fxUpdateValue=fxUpdateValue+"@"+"fxRevisionRevenue"
                    }
                } else {
                    if(fxUpdateValue == ''){
                        fxUpdateValue =" "
                    }else{
                        fxUpdateValue=fxUpdateValue+"@"+" "
                    }
                }
                
                if(actualExpense == true){
                    if(fxUpdateValue == ''){
                        fxUpdateValue ="fxActualExpense"
                    }else{
                        fxUpdateValue=fxUpdateValue+"@"+"fxActualExpense"
                    }   
                } else {
                    if(fxUpdateValue == ''){
                        fxUpdateValue =" "
                    }else{
                        fxUpdateValue=fxUpdateValue+"@"+" "
                    }
                }
                
                if(actualRevenue == true){
                    if(fxUpdateValue == ''){
                        fxUpdateValue ="fxActualRevenue"
                    }else{
                        fxUpdateValue=fxUpdateValue+"@"+"fxActualRevenue"
                    }   
                } else {
                    if(fxUpdateValue == ''){
                        fxUpdateValue =" "
                    }else{
                        fxUpdateValue=fxUpdateValue+"@"+" "
                    }
                }
                
                if(fxUpdateValue != ''){             
                    fxUpdateValue = fxUpdateValue+"@"+id;   
                }
                }
                </c:forEach>
          }
     
</script>

<script type="text/javascript">
 showOrHide(0);
 setOnSelectBasedMethods();
setCalendarFunctionality();
</script>