<%@ include file="/common/taglibs.jsp"%> 

<head>
<meta name="heading" content="<fmt:message key='otProcessForm.heading'/>"/> 
<title><fmt:message key="otProcessForm.title"/></title> 

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->
<style>
div.error, span.error, li.error, div.message {text-align:left !important;width:600px !important;}
</style>
<script language="javascript" type="text/javascript">

function clear_fields(){
			document.forms['otProcessForm'].elements['wareHse'].value = "";
			document.forms['otProcessForm'].elements['beginDt'].value = "";
			document.forms['otProcessForm'].elements['endDt'].value = "";
			document.forms['otProcessForm'].elements['beginDateDay'].value = "";
			document.forms['otProcessForm'].elements['endDateDay'].value = "";
}
function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) ; 
	}
function checkDatOfWeek(targetElement){
		dt = (targetElement.value).split('-');
		var month = dt[1];
		if(month == "Jan")month=1;
		if(month == "Feb")month=2;
		if(month == "Mar")month=3;
		if(month == "Apr")month=4;
		if(month == "May")month=5;
		if(month == "Jun")month=6;
		if(month == "Jul")month=7;
		if(month == "Aug")month=8;
		if(month == "Sep")month=9;
		if(month == "Oct")month=10;
		if(month == "Nov")month=11;
		if(month == "Dec")month=12;
		today=new Date('20'+dt[2],month-1,dt[0]);
		var myDays=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
		thisDay=today.getDay();
		thisDay=myDays[thisDay];
		if(targetElement.name == 'beginDt'){
			if(document.forms['otProcessForm'].elements['beginDt'].value != ''){
				document.forms['otProcessForm'].elements['beginDateDay'].value = thisDay;
			}
		}else{
			if(targetElement.name == 'endDt'){
				if(document.forms['otProcessForm'].elements['endDt'].value != ''){
					document.forms['otProcessForm'].elements['endDateDay'].value = thisDay;
				}
			}
		}
		
}
function checkDayOfWeek(){
   var date1 = document.forms['otProcessForm'].elements['beginDt'].value;	 
   var date2 = document.forms['otProcessForm'].elements['endDt'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  
  
		if(document.forms['otProcessForm'].elements['wareHse'].value == ''){
		alert("Select a warehouse to continue....");
		return false;
	}else{
	if(document.forms['otProcessForm'].elements['beginDateDay'].value != 'Friday' && document.forms['otProcessForm'].elements['endDateDay'].value != 'Thursday'){
		alert("Begin date should be Friday and End date should be Thursday for processing OverTime");
		return false;
	}else{
	if(document.forms['otProcessForm'].elements['beginDateDay'].value != 'Friday'){
		alert("Begin date should be Friday to process OverTime");
		return false;
	}
	if(document.forms['otProcessForm'].elements['endDateDay'].value != 'Thursday'){
		alert("End date should be Thursday to process OverTime");
		return false;
	}
	}
	}
	if((daysApart<0) && (document.forms['otProcessForm'].elements['beginDateDay'].value == 'Friday' && document.forms['otProcessForm'].elements['endDateDay'].value == 'Thursday'))
  {
    alert("End Payroll Date should be greater than Beginning Payroll Date");
    document.forms['otProcessForm'].elements['endDt'].value='';
    document.forms['otProcessForm'].elements['endDateDay'].value ='';
    return false;
  }
}



/*********************Global start**************************/
var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && (typeof date == "string" || date instanceof String) && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date();
		if (isNaN(date)) throw new SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};

/*************************Global End************/

function fillEndDate()
{

 var date1 = document.forms['otProcessForm'].elements['beginDt'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   
  date1 = finalDate.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
 
  sDate.setDate(sDate.getDate() + 6);
  var df1 = sDate.format("dd-mmm-yy");
  document.forms['otProcessForm'].elements['endDt'].value=df1;
  var myDays=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday", "Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
 document.forms['otProcessForm'].elements['endDateDay'].value=myDays[sDate.getDay()+6];
  
}
</script>


</head>

<s:form name="otProcessForm" action="processOTCharges">

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden id="formStatus" name="formStatus"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.processOTPreview">
<c:set var="processOTPreviewFlag" value="Y" />
</configByCorp:fieldVisibility>
<div style="width:100%">
<div id="newmnav">
	  <ul>
	  	  		<c:if test="${processOTPreviewFlag!='Y'}">
	  	  		<li><a><span>Overtime and Payroll completion Processing</span></a></li>
	  	  		</c:if> 
	  	  		<c:if test="${processOTPreviewFlag=='Y'}"> 	
				<li id="newmnav1" style="background:#FFF"><a class="current"><span>Overtime and Payroll completion Processing</span></a></li>	
				<li><a href="processOTPreViewForm.html"><span>Preview on Overtime Processing</span></a></li>				  	
				</c:if>
	  </ul>
</div>
<div class="spnblk">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top: -5px;"><span></span></div>
   <div class="center-content">

<table style="width:700px" border="0" cellspacing="0" cellpadding="0" >
<tr><td align="center">
<table class="" style="width:550px;margin:0px;padding:0px;" align="left" cellpadding="4" cellspacing="0" border="0">
<thead>

<tr>
	<td align="right" class="listwhitetext" >Warehouse<font color="red">*</font></td>
	<td align="left" style="width:250px" colspan="2"><s:select name="wareHse" list="%{wareHouse}" cssStyle="width:250px" cssClass="list-menu" headerKey="" headerValue=""/></td>
</tr>
<tr>
	<td align="right" class="listwhitetext">Beginning&nbsp;Payroll&nbsp;Date</td>
	<td align="left" style="width:110px"><s:textfield cssClass="input-text" id="beginDt" name="beginDt" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"  onselect="fillEndDate();"/><img id="beginDt_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	<td style="width:100px" align="left"><input type="text" name="beginDateDay" id="beginDateDay" style="width:102px;border:0;color:#003366;text-align:left;-webkit-user-select:none;-moz-user-input:none;"/> </td>
</tr>
<tr>
	<td align="right" class="listwhitetext" >Ending&nbsp;Payroll&nbsp;Date</td>
	<td align="left" style="width:110px"><s:textfield cssClass="input-text" id="endDt" name="endDt" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" /><img id="endDt_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['otProcessForm'].endDt.select();  return false;" /></td>
	<td style="width:270px" align="left"><input type="text" name="endDateDay" id="endDateDay" style="width:102px;border:0;color:#003366;text-align:left;-webkit-user-select:none;-moz-user-input:none;" /> </td>
</tr>
<tr><td height="3px"></td></tr>
<tr>
	<td align="left" style="padding-left:162px;" colspan="3">
	<s:submit cssClass="cssbutton" cssStyle="width:58px; height:25px" key="button.process" onclick="return checkDayOfWeek();"/>
	<input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/>
	</td>
				
</tr>
</thead>
</table>
</td>
</tr>

</table>
</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>


	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Overtime Results</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<s:set name="overTimeProcessList" value="overTimeProcessList" scope="request"/>
		<display:table name="overTimeProcessList" class="table" requestURI="" id="overTimeProcessList"  defaultsort="1" export="true" pagesize="500" style="width: 100%;"> 
		<display:column property="crewName" sortable="true" title="CrewName"/> 
		<display:column property="workDate" sortable="true" title="WorkDate" format="{0,date,dd-MMM-yyyy}"/> 
		<display:column property="warehouse" sortable="true" title="Whse"/> 
		<display:column property="ticket" sortable="true" title="Ticket"/> 
		<display:column property="shipper" sortable="true" title="Shipper" style="width:100px"/> 
		<display:column property="action" sortable="true" title="Action"/> 
		<display:column property="crewType" sortable="true" title="Crew"/> 
		<display:column property="calculationCode" sortable="true" title="Calc Code"/> 
		<display:column property="distributionCode" sortable="true" title="Dist Code"/> 
		<display:column property="beginHours" sortable="true" title="Begin Hrs"/> 
		<display:column property="endHours" sortable="true" title="End Hrs"/> 
		<display:column  sortable="true" title="Pd Rev Sh" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="true" value="${overTimeProcessList.paidRevenueShare}" /></display:column>
		<display:column property="regularPayPerHour" sortable="true" title="Reg Pay Hrs"/>
		<display:column sortable="true" title="Reg Hrs"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="true" value="${overTimeProcessList.regularHours}" /></display:column>
		<display:column sortable="true" title="OT"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="true" value="${overTimeProcessList.overTime}" /></display:column>
		<display:column property="doubleOverTime" sortable="true" title="DOT"/>
		
		<display:setProperty name="export.excel.filename" value="OT List.xls"/>   
    	<display:setProperty name="export.csv.filename" value="OT List.csv"/>   
    	<display:setProperty name="export.pdf.filename" value="OT List.pdf"/> 
</display:table>  
</div>
</s:form>
<script>
try{
checkDatOfWeek(document.forms['otProcessForm'].elements['beginDt']);
}
catch(e){}
try{
checkDatOfWeek(document.forms['otProcessForm'].elements['endDt']);
}
catch(e){}
</script>

<script type="text/javascript">
	setOnSelectBasedMethods(["checkDatOfWeek(document.getElementById('beginDt')),checkDatOfWeek(document.getElementById('endDt'))"]); 
	setCalendarFunctionality();
</script>