package com.trilasoft.app.webapp.action;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.ServletActionContext;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.google.gson.Gson;
import com.opensymphony.xwork2.Preparable;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;
import com.trilasoft.app.dao.hibernate.MyFileDaoHibernate.SDTO;
import com.trilasoft.app.dao.hibernate.RedskyBillingDaoHibernate.DTO;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TimeSheet;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ContainerManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.ToDoResultManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.WorkTicketManager;

public class DashBoardAction extends BaseAction implements Preparable {
	private Long sid;
	private Long corpID;
	private Long id;
	private ServiceOrder serviceOrder;
	private Billing billing;
	private String accountName;
	private CustomerFile customerFile;
	private TrackingStatus trackingStatus;
	private Miscellaneous miscellaneous;
	private String sessionCorpID;
	private WorkTicket workTicket;
	private List workTicketsList;
	private List accountLineList;
	private List totalrevenueList;
	private Set workTickets;
	private String workticketStatus;
	private AccountLine accountLine;
	private List taskList;
	private String filenumber;
	private List documentCategoryList;
	private List documentList;
	private MyFile myfile;
	private String shipNumber;
	private List<SystemDefault> sysDefaultDetail;
	private String baseCurrency;
	private String usertype;
	private Company company;
	private Container container;
	private String voxmeIntergartionFlag;
	private String oiJobList;
	private List transptDetails;
	private String noteOriginCount;
	private String noteForwardCount;
	private String noteDestCount;
	private List noteCountList;
	private List containerList;
	private String notetype;
	private Notes notes;
	private String noteFor;
	private String shipSize;
	private String minShip;
	private String countShip;
	private String subType;
	private ServicePartner servicePartner;
	private AccountLineManager accountLineManager;
	private ServiceOrderManager serviceOrderManager;
	private CustomerFileManager customerFileManager;
	private TrackingStatusManager trackingStatusManager;
	private MiscellaneousManager miscellaneousManager;
	private ToDoResultManager toDoResultManager;
	private WorkTicketManager workTicketManager;
	private BillingManager billingManager;
	private CompanyManager companyManager;
	private MyFileManager myFileManager;
	private ServicePartnerManager servicePartnerManager;
	private NotesManager notesManager;
	private ContainerManager containerManager;
	public DashBoardAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		this.usertype = user.getUserType();
	}

	public void prepare() {
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {

				if (systemDefault.getBaseCurrency() != null) {
					baseCurrency = systemDefault.getBaseCurrency();
				}
			}
			company = companyManager.findByCorpID(sessionCorpID).get(0);
			if (company != null) {
				{
					if (company.getVoxmeIntegration() != null) {
						voxmeIntergartionFlag = company.getVoxmeIntegration().toString();
					}
				}
			}
			if (company != null && company.getOiJob() != null) {
				oiJobList = company.getOiJob();
			}
		}
	}
	public String edit() {

		if (sid != null) {
			serviceOrder = serviceOrderManager.get(sid);
			customerFile = serviceOrder.getCustomerFile();
			trackingStatus = trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid);
			billing=billingManager.get(sid);
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getId());
			totalrevenueList = accountLineManager.getTotalrevenueList(serviceOrder.getId());
			transptDetails = servicePartnerManager.findTransptDetailList(serviceOrder.getShipNumber(), sessionCorpID);
		    workTicketsList = serviceOrderManager.getWorkTickestList(serviceOrder.getId(),sessionCorpID);
			taskList = toDoResultManager.getTaskList(serviceOrder.getShipNumber(), sessionCorpID,serviceOrder.getId());
			documentCategoryList = myFileManager.getListByDesc(serviceOrder.getShipNumber(),sessionCorpID);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			documentList = myFileManager.getDocumentCategoryList(serviceOrder.getShipNumber(), documentCategory,sessionCorpID);
			containerList = containerManager.containerList(serviceOrder.getId());
			Iterator it = containerList.iterator();
			while(it.hasNext())
			{
				Container container=(Container)it.next();

				noteForwardCount = notesManager.getNoteForwardCount(sessionCorpID, container.getId());
			
		
			}
				noteOriginCount = notesManager.getNotesCount(sessionCorpID, serviceOrder.getShipNumber(),serviceOrder.getId());
				noteDestCount = notesManager.getnoteDestCount(sessionCorpID, serviceOrder.getShipNumber(),serviceOrder.getId());
				
				
				shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
				minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
				countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
			
		}
		return SUCCESS;
	}

	String documentCategory;
	String documentJson;

	public String ajaxSearch() {
		try {
			if (sid != null) {
				serviceOrder = serviceOrderManager.get(sid);
				documentList = myFileManager.getDocumentCategoryList(serviceOrder.getShipNumber(), documentCategory,sessionCorpID);
				documentJson = new Gson().toJson(documentList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public Long getCorpID() {
		return corpID;
	}

	public void setCorpID(Long corpID) {
		this.corpID = corpID;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public List getWorkTicketsList() {
		return workTicketsList;
	}

	public void setWorkTicketsList(List workTicketsList) {
		this.workTicketsList = workTicketsList;
	}

	public String getWorkticketStatus() {
		return workticketStatus;
	}

	public void setWorkticketStatus(String workticketStatus) {
		this.workticketStatus = workticketStatus;
	}

	public WorkTicket getWorkTicket() {
		return workTicket;
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}

	public Set getWorkTickets() {
		return workTickets;
	}

	public void setWorkTickets(Set workTickets) {
		this.workTickets = workTickets;
	}

	public AccountLine getAccountLine() {
		return accountLine;
	}

	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;
	}

	public List getAccountLineList() {
		return accountLineList;
	}

	public void setAccountLineList(List accountLineList) {
		this.accountLineList = accountLineList;
	}

	public List getTotalrevenueList() {
		return totalrevenueList;
	}

	public void setTotalrevenueList(List totalrevenueList) {
		this.totalrevenueList = totalrevenueList;
	}

	public List getTaskList() {
		return taskList;
	}

	public void setTaskList(List taskList) {
		this.taskList = taskList;
	}

	public String getFilenumber() {
		return filenumber;
	}

	public void setFilenumber(String filenumber) {
		this.filenumber = filenumber;
	}

	public MyFile getMyfile() {
		return myfile;
	}

	public void setMyfile(MyFile myfile) {
		this.myfile = myfile;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}

	public String getDocumentCategory() {
		return documentCategory;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	public ServicePartner getServicePartner() {
		return servicePartner;
	}

	public void setServicePartner(ServicePartner servicePartner) {
		this.servicePartner = servicePartner;
	}

	public List getTransptDetails() {
		return transptDetails;
	}

	public void setTransptDetails(List transptDetails) {
		this.transptDetails = transptDetails;
	}

	public List getDocumentCategoryList() {
		return documentCategoryList;
	}

	public void setDocumentCategoryList(List documentCategoryList) {
		this.documentCategoryList = documentCategoryList;
	}

	public List getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List documentList) {
		this.documentList = documentList;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setToDoResultManager(ToDoResultManager toDoResultManager) {
		this.toDoResultManager = toDoResultManager;
	}
	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}

	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public String getDocumentJson() {
		return documentJson;
	}

	public void setDocumentJson(String documentJson) {
		this.documentJson = documentJson;
	}

	public List getNoteCountList() {
		return noteCountList;
	}

	public void setNoteCountList(List noteCountList) {
		this.noteCountList = noteCountList;
	}

	public Notes getNotes() {
		return notes;
	}

	public void setNotes(Notes notes) {
		this.notes = notes;
	}

	public String getNoteOriginCount() {
		return noteOriginCount;
	}

	public void setNoteOriginCount(String noteOriginCount) {
		this.noteOriginCount = noteOriginCount;
	}

	public String getNoteForwardCount() {
		return noteForwardCount;
	}

	public void setNoteForwardCount(String noteForwardCount) {
		this.noteForwardCount = noteForwardCount;
	}

	public String getNoteDestCount() {
		return noteDestCount;
	}

	public void setNoteDestCount(String noteDestCount) {
		this.noteDestCount = noteDestCount;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public void setContainerManager(ContainerManager containerManager) {
		this.containerManager = containerManager;
	}

	public List getContainerList() {
		return containerList;
	}

	public void setContainerList(List containerList) {
		this.containerList = containerList;
	}

	public String getNoteFor() {
		return noteFor;
	}

	public void setNoteFor(String noteFor) {
		this.noteFor = noteFor;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getShipSize() {
		return shipSize;
	}

	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}

	public String getMinShip() {
		return minShip;
	}

	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}

	public String getCountShip() {
		return countShip;
	}

	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}

}
