<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<head>   
    <title><fmt:message key="myFileList.title"/></title>   
    <meta name="heading" content="<fmt:message key='myFileList.heading'/>"/> 
    <style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}

#loader {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
#overlayPopUp { filter:alpha(opacity=70); -moz-opacity:0.7; 
-khtml-opacity: 0.7; opacity: 0.7; 
position:fixed; width:100%; height:100%;left:0px;top:0px; 
z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
<style>  
.table thead th, .tableHeaderTable td {
background:#BCD2EF url(images/bg_listheader.png) repeat-x scroll 0 0;
border-color:#3dafcb #3dafcb #3dafcb -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(images/bg_listheader.png) repeat scroll 0%;
border-color:#3dafcb rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table th.sorted {
    background-color: #BCD2EF;
    color: #15428B;
}

.table tr.odd {
color:#000d47;
}

.table tr.even {
color:#000000;
background-color:{#e4e6f2}
}

.table tr:hover {
color:#0b0148;
background-color:{#fffcd6}
}
div#content {
padding:0px 0px; 
min-height:50px; 
margin-left:0px;
}
</style> 
  <script type="text/javascript">
  
  function linkInvoice()
  {
	  var rowId = valButton(document.forms['accountFileList'].elements['DD']);   
	    if (rowId == null) 
		   {
	          alert("Please select any radio button"); 
	       	  return false;
	       } 
	       else 
	       {
	    		 var sid = document.forms['accountFileList'].elements['id'].value;
	    		 var accId = document.forms['accountFileList'].elements['accId'].value;
	    		 var agentCorpId=document.forms['accountFileList'].elements['agentCorpId'].value;
	    		 var fieldVal1 = document.forms['accountFileList'].elements['customerFile.sequenceNumber'].value;

	    	   var url="checkInvoice.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&agentCorpId="+agentCorpId+"&rowId="+rowId;
	    	   httpCheck.open("GET", url, true);
	    	   httpCheck.onreadystatechange =function(){ selectiveResponse(rowId);}; ;
	    	   httpCheck.send(null);

         }
  }
  function valButton(btn) {
	    var cnt = -1; 
	    var len = btn.length; 
	    if(len >1)
	    {
	    for (var i=btn.length-1; i > -1; i--) {
		        if (btn[i].checked) {cnt = i; i = -1;}
		    }
		    if (cnt > -1) return btn[cnt].value;
		    else return null;
	    	
	    }
	    else
	    { 
	    	return document.forms['accountFileList'].elements['DD'].value; 
	    } 
	  }
  function conbinedDoc(){
		var checkBoxId = document.forms['accountFileList'].elements['userCheck'].value;
		var checkBoxFile = document.forms['accountFileList'].elements['userCheckFile'].value;
		var seqNo = document.forms['accountFileList'].elements['customerFile.sequenceNumber'].value;
		if(seqNo == '' ){
			seqNo = document.forms['accountFileList'].elements['serviceOrder.sequenceNumber'].value;
		}
		
		var name = document.forms['accountFileList'].elements['customerFile.firstName'].value;
		name +=	' '+document.forms['accountFileList'].elements['customerFile.lastName'].value;
		
		if(checkBoxFile != ''){
			checkBoxFile = checkBoxFile.trim();
			if (checkBoxFile.indexOf(",") == 0) {
				checkBoxFile = checkBoxFile.substring(1);
			}
			if (checkBoxFile.lastIndexOf(",") == checkBoxFile.length - 1) {
				checkBoxFile = checkBoxFile.substring(0, checkBoxFile.length - 1);
			}
			
			var arrayid = checkBoxFile.split(",");
			var arrayLength = arrayid.length;
			
			for (var i = 0; i < arrayLength; i++) {
				var fName = arrayid[i];
				if(fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'pdf'  && fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'PDF'){
					alert('Please select only PDF Files.');
					return false;
				}
			}
		}
		
		if(checkBoxId =='' || checkBoxId ==','){
			alert('Please select one or more document to merge.');
		}else{
			var url = 'conbinedDoc.html?rId='+checkBoxId+'&seqNum='+seqNo+'&name='+name;
			location.href=url;
		}
	}
  
  function userStatusCheck(target){
		var targetElement = target;
		var ids=target.value;	
		
		if(targetElement.checked){
    		var userCheckStatus = document.forms['accountFileList'].elements['userCheck'].value;
    		if(userCheckStatus == ''){
	  			document.forms['accountFileList'].elements['userCheck'].value = ids;
    		}else{
    			var userCheckStatus=	document.forms['accountFileList'].elements['userCheck'].value = userCheckStatus + ',' + ids;
    			document.forms['accountFileList'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
    		}
    	}
	 	if(targetElement.checked==false){
	 	var userCheckStatus = document.forms['accountFileList'].elements['userCheck'].value;
   		userCheckStatus = userCheckStatus.replace( ids , '' );
   		userCheckStatus = userCheckStatus.replace( ',,' , ',' );
   		var len = userCheckStatus.length-1;
   	    if(len==userCheckStatus.lastIndexOf(",")){
   	    	userCheckStatus=userCheckStatus.substring(0,len);
   	      }
   	      if(userCheckStatus.indexOf(",")==0){
   	    	 userCheckStatus=userCheckStatus.substring(1,userCheckStatus.length);
   	      }
   		document.forms['accountFileList'].elements['userCheck'].value = userCheckStatus;
	 	}
 	}

  </script>  
</head>
<div id="Layer5" style="width:100%">
<s:form id="accountFileList"  name="accountFileList" action="" method="post" >
<s:hidden name="accountlinelist1" id="accountlinelist1" value="<%=request.getParameter("accountlinelist") %>"/>
<s:hidden name="accountlineship1" id="accountlineship1" value="<%=request.getParameter("accountlineship") %>"/>
<s:hidden name="accountlineFile" id="accountlineFile" value=""/>
<s:hidden name="agentCorpId" value="<%=request.getParameter("agentCorpId") %>"/>
<s:hidden name="rowId" id="rowId" value=""/>
<s:hidden name="amount"  value="<%=request.getParameter("amount") %>" />
<c:set var="invoiceDate" value="<%=request.getParameter("invoiceDate") %>"/>
<s:hidden name="invoiceDate"  value="<%=request.getParameter("invoiceDate") %>" />
<c:set var="amount" value="<%=request.getParameter("amount") %>"/>
<s:hidden name="currency"  value="<%=request.getParameter("currency") %>" />
<c:set var="currency" value="<%=request.getParameter("currency") %>"/>
<s:hidden name="vatamount"  value="<%=request.getParameter("vatamount") %>" />
<c:set var="vatamount" value="<%=request.getParameter("vatamount") %>"/>
<c:set var="vendorCode" value="<%=request.getParameter("vendorCode") %>"/>
<s:hidden name="vendorCode"  value="<%=request.getParameter("vendorCode") %>" />
<c:set var="vendorInvoice" value="<%=request.getParameter("vendorInvoice") %>"/>
<s:hidden name="vendorInvoice"  value="<%=request.getParameter("vendorInvoice") %>" />
<s:hidden name="accId" value="<%=request.getParameter("accId") %>"/>
<c:set var="fileType" value="Invoice Vendor"/>
<s:hidden name="fileType"  value="Invoice Vendor" />
<c:set var="flagUloadInv" value="<%=request.getParameter("flagUloadInv") %>"/>
<s:hidden name="flagUloadInv"  value="<%=request.getParameter("flagUloadInv") %>" />
<s:hidden name="id" value="<%=request.getParameter("sid") %>"/>
<s:hidden name="cid" value="<%=request.getParameter("cid") %>"/>
<s:hidden name="customerFile.id"/>
<s:hidden name="userCheck"/> 
<s:hidden name="userCheckFile"/>
<s:hidden name="customerFile.sequenceNumber"/>
<s:hidden name="customerFile.firstName"/>
<s:hidden name="customerFile.lastName"/>

<c:set var="id"  value="<%=request.getParameter("sid") %>"/>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session" /> 
			    <c:set var="noteID"	value="${serviceOrder.shipNumber}" scope="session" /> 
			    <c:set var="noteFor" value="ServiceOrder" scope="session" /> 
			    <c:if test="${empty serviceOrder.id}">
				<c:set var="isTrue" value="false" scope="request" />
			    </c:if> 
			    <c:if test="${not empty serviceOrder.id}">
				<c:set var="isTrue" value="true" scope="request" />
			    </c:if>
<c:set var="buttons">  
<c:choose><c:when test="${flagUloadInv}">
       <c:if test="${empty cid}">
	  <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15"
         onclick="window.open('uploadAgentsFile!start.html?id=${id}&soCorpId=${serviceOrder.corpID}&vendorInvoice=${vendorInvoice}&vendorCode=${vendorCode}&invoiceDate=${invoiceDate}&fileType=${fileType}&vatamount=${vatamount}&currency=${currency}&flagUloadInv=true&amount=${amount}&myFileFor=SO&decorator=popup&popup=true','','width=750,height=570');"
         value="<fmt:message key="button.upload"/>"/>
        </c:if>
        <c:if test="${not empty cid}">
        
         <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15"
         onclick="window.open('uploadAgentsFile!start.html?id=${cid}&soCorpId=${serviceOrder.corpID}&vendorInvoice=${vendorInvoice}&vendorCode=${vendorCode}&fileType=${fileType}&vatamount=${vatamount}&currency=${currency}&flagUloadInv=true&amount=${amount}&myFileFor=CF&decorator=popup&popup=true','','width=750,height=570');"
         value="<fmt:message key="button.upload"/>"/>
         </c:if>
         <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:75px; font-size: 15" name="combBtn"  value="Link Invoice" onclick="return linkInvoice();"/>
         </c:when>
         <c:otherwise>
      <c:if test="${empty cid}">
	    <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15"
         onclick="window.open('uploadAgentsFile!start.html?id=${id}&soCorpId=${serviceOrder.corpID}&myFileFor=SO&decorator=popup&popup=true','','width=750,height=570');"
         value="<fmt:message key="button.upload"/>"/>
        </c:if>
        <c:if test="${not empty cid}">
          <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15"
         onclick="window.open('uploadAgentsFile!start.html?id=${cid}&soCorpId=${serviceOrder.corpID}&myFileFor=CF&decorator=popup&popup=true','','width=750,height=570');"
         value="<fmt:message key="button.upload"/>"/>
         </c:if>
         <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:70px; font-size: 15" name="combBtn"  value="Merge PDF" onclick="return conbinedDoc();"/>
        </c:otherwise>
       </c:choose>
        </c:set> 
		<div id="newmnav">
		  <ul>
		  <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
	        <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
	        <c:set var="relocationServicesKey" value="" />
	        <c:set var="relocationServicesValue" value="" /> 
	        <c:forEach var="entry" items="${relocationServices}">
                <c:if test="${relocationServicesKey==''}">
                <c:if test="${entry.key==serviceOrder.serviceType}">
	               <c:set var="relocationServicesKey" value="${entry.key}" />
	               <c:set var="relocationServicesValue" value="${entry.value}" /> 
                </c:if>
                </c:if> 
            </c:forEach>
            <c:choose><c:when test="${flagUloadInv}">
            <li id="newmnav1" style="background:#FFF "><a class="current"><span>Doc List</span></a></li>
            </c:when>
            <c:otherwise>
             <c:if test="${empty cid}">
            <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
		  	<li><a href="editServiceOrderUpdate.html?id=${id}"><span>S/O Details</span></a></li>
		  	</sec-auth:authComponent>
		  	</c:if>
		  	 <c:if test="${not empty cid}">
		  	<sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id} "><span>Service Orders</span></a></li>
			</sec-auth:authComponent>
			</c:if>
		  	<c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
		      <sec-auth:authComponent componentId="module.accountingPortalTab.serviceorder.operationResourceTab">
		      <li><a href="operationResourceFromAcPortal.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
		      </sec-auth:authComponent>
		  	</c:if>
		  	<sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
		  	<li><a href="costingDetail.html?sid=${id}"><span>Costing</span></a></li>
		  	</sec-auth:authComponent>
		  	<sec-auth:authComponent componentId="module.tab.document.forwardingTab">
		  	<c:if test="${userType!='ACCOUNT'}">
		  	<c:if test="${serviceOrder.job !='RLO'}"> 
		  	<li><a href="containers.html?id=${id}"><span>Forwarding</span></a></li>
		  	</c:if>
		  	</c:if>
		  	  <c:if test="${userType=='ACCOUNT' && serviceOrder.job !='RLO'}">
		  	  <%--  <c:if test="${serviceOrder.corpID!='CWMS' || (serviceOrder.job !='OFF' && serviceOrder.corpID=='CWMS')}"> --%>
		  	   <c:if test="${serviceOrder.corpID!='CWMS' || (fn:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}"> 
              <li><a href="servicePartnerss.html?id=${id}"><span>Forwarding</span></a></li>
              </c:if>
             </c:if>
             </sec-auth:authComponent>
		  	<sec-auth:authComponent componentId="module.tab.document.statusTab">
		  	<c:if test="${serviceOrder.job =='RLO'}">  
                <li><a href="editDspDetails.html?id=${id}"><span>Status</span></a></li>
             </c:if>
             <c:if test="${serviceOrder.job !='RLO'}"> 
		  	  <li><a href="editTrackingStatus.html?id=${id}"><span>Status</span></a></li>
		  	 </c:if>
		  	 </sec-auth:authComponent>
		  	 <sec-auth:authComponent componentId="module.tab.summary.summaryTab">		    
		  	<%--  <c:if test="${serviceOrder.corpID!='CWMS' || (serviceOrder.job !='OFF' && serviceOrder.corpID=='CWMS')}"> --%>
		  	<c:if test="${serviceOrder.corpID!='CWMS' || (fn:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}"> 
		  	 	<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
				</c:if>
			 </sec-auth:authComponent>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
   			 <c:if test="${serviceOrder.job!='RLO'}">
   			 <c:if test="${empty cid}">
   			 <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
  	  		 <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
             </sec-auth:authComponent>
             </c:if>
             </c:if>
             </configByCorp:fieldVisibility>
			 <sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">
			  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.document.reportTab">
			  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Claims&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			  </sec-auth:authComponent>
             <li id="newmnav1" style="background:#FFF "><a class="current"><span>Doc List</span></a></li>
            </c:otherwise>
            		  </c:choose>	
		  	
		  </ul>
		</div>
	<div class="spn" >&nbsp;</div>		
<div style="!margin-top:7px;">
<display:table name="myFiles" class="table" requestURI="" id="myFileList" pagesize="25" defaultsort="2" style="width:100%;margin-top:2px;" >
   <c:choose>
   <c:when test="${flagUloadInv}">
   <display:column title=" " style="width:2px;"><input type="radio" id="checkboxId" name="DD" value="${myFileList.id}:${myFileList.description}" onclick="userStatusCheck(this);"/></display:column>
   </c:when>
  <c:otherwise>
  <display:column title=" " style="width:2px;"><input type="checkbox" id="checkboxId" name="DD" value="${myFileList.id}" onclick="userStatusCheck(this)"/></display:column>
   <c:if test="${empty cid}">
   <display:column sortable="true" title="S/O #" style="width:20px;">
   <c:if test="${fn:substring(myFileList.fileId,11,14)!=''}">
   <c:out value="${fn:substring(myFileList.fileId,11,14)}"/>
   </c:if>
    <c:if test="${fn:substring(myFileList.fileId,11,14)==''}">
    <c:out value="00"/>
    </c:if>
  </display:column>
  </c:if>
  </c:otherwise></c:choose>
   <display:column property="fileType" sortable="true" titleKey="myFile.fileType" style="width:108px;"/>
   <display:column property="documentCategory" sortable="true" title="Document&nbsp;Category" style="width:108px;"/>
   <display:column property="description" sortable="true" title="Description" style="width:128px;"/>
   <display:column titleKey="myFile.fileFileName"  maxLength="50" style="width:155px;">
   <%-- <a onclick="javascript:openWindow('ImageServletAction.html?id=${myFileList.id}&decorator=popup&popup=true',900,600);">
   <c:out value="${myFileList.fileFileName}" escapeXml="false"/>
   </a> --%>
   <a onclick="downloadSelectedFile('${myFileList.id}');">
		<c:out value="${myFileList.fileFileName}" escapeXml="false"/>
	</a>
   </display:column>
</display:table>
</div>
<c:out value="${buttons}" escapeXml="false" />
</s:form>
</div>
<script type="text/javascript">
function downloadSelectedFile(id){
	var fieldVal = "AP";
	var sid = document.forms['accountFileList'].elements['id'].value;
	var fieldVal1 = document.forms['accountFileList'].elements['customerFile.sequenceNumber'].value;
	var myFileJspName = "accountFiles";
	var url ="";
	url="ImageServletAction.html?id="+id+"&sid="+sid+"&myFileForVal="+fieldVal+"&sequenceNumber="+fieldVal1+"&myFileJspName="+myFileJspName+"";
	location.href=url;
}
var fileVal = '${resultType}';
if ((fileVal!=null && fileVal!='') && (fileVal=='errorNoFile')){
	alert('This File is Temporarily Unavailable.')
	
	var fieldVal = document.forms['accountFileList'].elements['customerFile.sequenceNumber'].value;	
	var sid = document.forms['accountFileList'].elements['id'].value
	var replaceURL = "accountFiles.html?sid="+sid+"&seqNum="+fieldVal+"";
	window.location.replace(replaceURL);
}

var  httpCheck= getHTTPObject78();

function getHTTPObject78() {
   var xmlhttp;
   if(window.XMLHttpRequest)  {
       xmlhttp = new XMLHttpRequest();
   }
   else if (window.ActiveXObject)  {
       xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
       if (!xmlhttp)  {
           xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
       } }
   return xmlhttp;
}
var rowId;
function selectiveResponse(rowId){
	  if (httpCheck.readyState == 4)
   {
      var results = httpCheck.responseText
      results = results.trim();
		 if(results>0)
				 {
			  var agree = confirm("Some Lines are already linked to a Invoice Document , do you want to use this document for all the lines");
	            if(agree)
	             {
	             //alert("ok in agree");
	             
	                  window.opener.document.forms['payableProcessing'].elements['rowId'].value= rowId; 
	                   parent.window.opener.agentDetailSubmit();
	                   window.close();
	    		      }
	            else
	            	{
	            	   window.opener.document.forms['payableProcessing'].elements['rowId'].value=""; 
		                  parent.window.opener.agentDetailSubmit();
		                  window.close();
		    		     }
		 }
		 else
			 { 
			   window.opener.document.forms['payableProcessing'].elements['rowId'].value= rowId; 
             parent.window.opener.agentDetailSubmit();
             window.close();
		       }
   }
	  progressBarAutoSave('0');
}

function progressBarAutoSave(tar){
	showOrHideAutoSave(tar);
	}

		function showOrHideAutoSave(value) {
		    if (value==0) {
		        if (document.layers)
		           document.layers["overlayPopUp"].visibility='hide';
		        else
		           document.getElementById("overlayPopUp").style.visibility='hidden';
		   }
		   else if (value==1) {
		       if (document.layers)
		          document.layers["overlayPopUp"].visibility='show';
		       else
		          document.getElementById("overlayPopUp").style.visibility='visible';
		   }
		}
		try{
			progressBarAutoSave('0');
		}catch(e){}
		
		
		function submitAfterUpload()
		{
			 window.opener.document.forms['payableProcessing'].elements['rowId'].value= document.forms['accountFileList'].elements['rowId'].value; 
             parent.window.opener.agentDetailSubmit();
			 window.close();
			
			
		}
	       
</script>