package com.trilasoft.app.service.impl;

import com.trilasoft.app.dao.QuotesFileDao;   
import com.trilasoft.app.model.QuotesFile;   
import com.trilasoft.app.service.QuotesFileManager;   
import org.appfuse.service.impl.GenericManagerImpl;   
  
import java.util.List;   
  
public class QuotesFileManagerImpl extends GenericManagerImpl<QuotesFile, Long> implements QuotesFileManager {   
    QuotesFileDao customerFileDao;   
  
    public QuotesFileManagerImpl(QuotesFileDao customerFileDao) {   
        super(customerFileDao);   
        this.customerFileDao = customerFileDao;   
    }   
  
    public List<QuotesFile> findByLastName(String lastName) {   
        return customerFileDao.findByLastName(lastName);   
    }  
}