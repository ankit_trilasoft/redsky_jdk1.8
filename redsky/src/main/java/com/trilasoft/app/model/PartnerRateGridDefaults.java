package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="partnerrategriddefaults")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class PartnerRateGridDefaults extends BaseObject{
	private Long id;
	private String corpID;
	private String standardInclusionsOrigin;
	private String standardInclusionsDestination;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String rateGridRules;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("standardInclusionsOrigin",
				standardInclusionsOrigin).append(
				"standardInclusionsDestination", standardInclusionsDestination)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("rateGridRules", rateGridRules).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerRateGridDefaults))
			return false;
		PartnerRateGridDefaults castOther = (PartnerRateGridDefaults) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(standardInclusionsOrigin,
				castOther.standardInclusionsOrigin).append(
				standardInclusionsDestination,
				castOther.standardInclusionsDestination).append(createdBy,
				castOther.createdBy).append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(rateGridRules,
						castOther.rateGridRules).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				standardInclusionsOrigin).append(standardInclusionsDestination)
				.append(createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).append(rateGridRules).toHashCode();
	}
	@Column(length=10)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	} 
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column(length=5000)
	public String getStandardInclusionsDestination() {
		return standardInclusionsDestination;
	}
	public void setStandardInclusionsDestination(
			String standardInclusionsDestination) {
		this.standardInclusionsDestination = standardInclusionsDestination;
	}
	
	@Column(length=5000)
	public String getStandardInclusionsOrigin() {
		return standardInclusionsOrigin;
	}
	public void setStandardInclusionsOrigin(String standardInclusionsOrigin) {
		this.standardInclusionsOrigin = standardInclusionsOrigin;
	}
	@Column(length=5000)
	public String getRateGridRules() {
		return rateGridRules;
	}
	public void setRateGridRules(String rateGridRules) {
		this.rateGridRules = rateGridRules;
	}

}
