package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table ( name="calendarfile")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class CalendarFile extends BaseObject{

	private Long id;
	private String corpID;
	private String userName;
	private String fromTime;
	private String toTime;
	private String state;
	private String city;
	private String activityName;
	private String billTo;
	private Boolean fullDay;
	private Date surveyDate;
	private Date surveyTillDate;
	private int surveyDays;
	
	private Date createdOn;
	private Date updatedOn;
	private String createdBy;
	private String updatedBy;
	private String eventType;
	private String category;
	private String personId;
	private String firstName;
	private String lastName;
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CalendarFile))
			return false;
		CalendarFile castOther = (CalendarFile) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(userName, castOther.userName).append(
				fromTime, castOther.fromTime).append(toTime, castOther.toTime)
				.append(state, castOther.state).append(city, castOther.city)
				.append(activityName, castOther.activityName).append(billTo,
						castOther.billTo).append(fullDay, castOther.fullDay)
				.append(surveyDate, castOther.surveyDate).append(
						surveyTillDate, castOther.surveyTillDate).append(
						surveyDays, castOther.surveyDays).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(eventType,
						castOther.eventType).append(personId,
								castOther.personId).append(firstName,
										castOther.firstName).append(lastName,
												castOther.lastName).append(category,castOther.category).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(userName)
				.append(fromTime).append(toTime).append(state).append(city)
				.append(activityName).append(billTo).append(fullDay).append(
						surveyDate).append(surveyTillDate).append(surveyDays)
				.append(createdOn).append(updatedOn).append(createdBy).append(
						updatedBy).append(eventType).append(personId).append(firstName).append(lastName).append(category).toHashCode();
	}
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("userName", userName).append("fromTime",
				fromTime).append("toTime", toTime).append("state", state)
				.append("city", city).append("activityName", activityName)
				.append("billTo", billTo).append("fullDay", fullDay).append(
						"surveyDate", surveyDate).append("surveyTillDate",
						surveyTillDate).append("surveyDays", surveyDays)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("eventType", eventType).append("personId", personId).append("firstName", firstName).append("lastName", lastName).append("category",category).toString();
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	@Column( length=50 )
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	@Column( length=12 )
	public String getBillTo() {
		return billTo;
	}
	public void setBillTo(String billTo) {
		this.billTo = billTo;
	}
	@Column( length=30 )
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Column( length=12 )
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column( length=5 )
	public String getFromTime() {
		return fromTime;
	}
	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column( length=30 )
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column
	public Date getSurveyDate() {
		return surveyDate;
	}
	public void setSurveyDate(Date surveyDate) {
		this.surveyDate = surveyDate;
	}
	@Column( length=5 )
	public String getToTime() {
		return toTime;
	}
	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
	@Column( length=82 )
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Column
	public Boolean getFullDay() {
		return fullDay;
	}
	public void setFullDay(Boolean fullDay) {
		this.fullDay = fullDay;
	}
	@Column( length=82 )
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column( length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Date getSurveyTillDate() {
		return surveyTillDate;
	}
	public void setSurveyTillDate(Date surveyTillDate) {
		this.surveyTillDate = surveyTillDate;
	}
	@Column(length=5)
	public int getSurveyDays() {
		return surveyDays;
	}
	public void setSurveyDays(int surveyDays) {
		this.surveyDays = surveyDays;
	}
	@Column(length=25)
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@Column(length=25)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Column(length=25)
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Column(length=20)
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	
}
