package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.AdAddressesDetails;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.VatIdentification;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.VatIdentificationManager;

public class VatIdentificationAction extends BaseAction implements Preparable {
private Long id;
private String sessionCorpID;
private RefMasterManager refMasterManager;
private VatIdentification vatIdentification;
private VatIdentificationManager vatIdentificationManager;
private Map<String, String> country;
private List vatList;
private String flage;
private String vatCountryCode;
public String validVatCode;
public String vatNumber;
public String vatId;

public VatIdentificationAction(){
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User) auth.getPrincipal();
	this.sessionCorpID = user.getCorpID();
}
public void prepare() throws Exception {
	
}
public String getComboList(String corpID){ 

 	country = refMasterManager.findWithDescriptionWithBucket2("COUNTRY",corpID);

 	 return SUCCESS;
}


@SkipValidation 
public String searchByCountryCode() {		    	
	getComboList(sessionCorpID);
	boolean currencyCode = (vatCountryCode == null);
	if(!currencyCode)
	{
		vatList=vatIdentificationManager.searchAll(vatCountryCode);
	}	    		   	
	
    return SUCCESS;   
}
public String list(){
	getComboList(sessionCorpID);
	vatList=vatIdentificationManager.findAllVat();
	return SUCCESS;
	
}


public Boolean startWithCheck(String startWith,VatIdentification vatIdentification){
	Boolean chk = false;
	if(vatIdentification.getStartWith().equalsIgnoreCase(startWith)){
		chk=true;
	}else{
		chk=false;
	}
	return chk;
}

public Boolean endWithCheck(String endWith,VatIdentification vatIdentification){
	Boolean chk = false;
	if(vatIdentification.getEndWith().equalsIgnoreCase(endWith)){
		chk=true;
	}else{
		chk=false;
	}
	return chk;
}

public Boolean blockCheck(String[] tempBlock,VatIdentification vatIdentification,String blockValue){
	Boolean chk = false;
	String valuePerBlock ="";
	int numberOfBlock = tempBlock.length;
	String[] dbBlock = vatIdentification.getNumberOfBlock().split(",");
	String[] dbBlockValue=  vatIdentification.getBlockValue().split("/");
	for(int i=0 ; i<dbBlock.length ; i++){
		if(numberOfBlock == Integer.parseInt(dbBlock[i])){
			valuePerBlock = dbBlockValue[i];
			break;
		}
	}
	String[] valuePerBlockArray = valuePerBlock.split(",");
	if(dbBlock.length== 1 && valuePerBlockArray.length == 1){
		if(blockValue.length()== (Integer.parseInt(vatIdentification.getBlockValue()))){
			chk = true; 
		}
	}else{
		for(int i=0;i<dbBlock.length ; i++){
			if(numberOfBlock==(Integer.parseInt( dbBlock[i]))){
				for(int j=0 ;j<tempBlock.length ; j++){
					if(tempBlock[j].length()== (Integer.parseInt(valuePerBlockArray[j]))){
						chk=true;
					}else{
						chk=false;
						break;
					}
				}
			}
		}
	}
	return chk;
}

@SkipValidation
public String validateVatCode(){
	// vatNumber and vatCountryCode is comming from the partnerPrivateform.jsp in method  getAgentCountryCode of java script
	vatId = vatIdentificationManager.getIdVatCode(vatCountryCode);
	boolean chkStartWith = false;
	boolean chkEndWith = false;
	boolean chkBlock= false;
	String chkBool="N"; // variable representing a boolean value true("Y") and false("N")
	String blockValue = vatNumber.substring(2, vatNumber.length());
	String startWith = vatNumber.substring(2, 3);
	String endWith= vatNumber.substring(vatNumber.length()-1, vatNumber.length());
	String[] tempBlock = blockValue.split(" ");
			
	if(!vatId.equals("")){
		id=Long.parseLong(vatId);
		vatIdentification=vatIdentificationManager.get(id);
		if(vatIdentification.getStartWith()!=null && !(vatIdentification.getStartWith().equalsIgnoreCase(""))){
				chkStartWith = startWithCheck(startWith,vatIdentification);
		}else{
			chkStartWith = true;
		}
		if(vatIdentification.getEndWith()!=null && !(vatIdentification.getEndWith().equalsIgnoreCase(""))){
				chkEndWith = endWithCheck(endWith,vatIdentification);
		}else{
			chkEndWith = true;
		}
		if(vatIdentification.getBlockValue()!=null && !(vatIdentification.getBlockValue().equalsIgnoreCase("")) ){
				chkBlock = blockCheck(tempBlock,vatIdentification,blockValue);
		}else{
			chkBlock=true;
		}
		
		if(!(chkStartWith && chkEndWith && chkBlock)){
			validVatCode = chkBool+"^"+vatIdentification.getMessage();
		}else{
			if(vatCountryCode.equals("NL")){
				char tempChar = blockValue.charAt(9);
				if(tempChar == 'B'){
					validVatCode = "Y^"+vatIdentification.getMessage();
				}else{
					validVatCode = chkBool+"^"+vatIdentification.getMessage();
				}
			}else if(vatCountryCode.equals("IE")){
				validVatCode = "Y^"+vatIdentification.getMessage();
			}else{
				validVatCode = "Y^"+vatIdentification.getMessage();
			}
		}
	}else{
		validVatCode= "Y^No message.";
	}
	return SUCCESS;	
}

public String edit() {
	getComboList(sessionCorpID);
	if (id != null) {
		vatIdentification = vatIdentificationManager.get(id);
	} else {
		vatIdentification = new VatIdentification(); 
		vatIdentification.setCreatedOn(new Date());
		vatIdentification.setUpdatedOn(new Date());
		vatIdentification.setUpdatedBy(getRequest().getRemoteUser());
		vatIdentification.setCreatedBy(getRequest().getRemoteUser());
	} 
	return SUCCESS;
}
public String save() throws Exception{
	getComboList(sessionCorpID);
	if(id!=null){
		vatIdentification.setUpdatedOn(new Date());	
		vatIdentification.setUpdatedBy(getRequest().getRemoteUser());
	}
	boolean isNew = (vatIdentification.getId() == null);
	List checkCountry=vatIdentificationManager.isCountryExisted(vatIdentification.getCountryCode(),sessionCorpID);
	if (!checkCountry.isEmpty() && isNew) { 
		errorMessage("Country Code already exist in Vat");
		vatIdentification.setCreatedOn(new Date());
		return SUCCESS;
    } 
	if (isNew) { 
		vatIdentification.setCreatedOn(new Date());
    } 
	
	vatIdentification.setCorpID(sessionCorpID);
	vatIdentification.setCreatedBy(getRequest().getRemoteUser());
	vatIdentification.setUpdatedBy(getRequest().getRemoteUser()); 
	vatIdentificationManager.save(vatIdentification);  
    String key = (isNew) ?"VAT details have been saved." :"VAT details has been updated successfully." ;
    saveMessage(getText(key));
    flage="1";
	return SUCCESS;	
}
public String vatDelete(){
	vatIdentificationManager.remove(id);
	String key ="VAT deleted successfully";
  	saveMessage(getText(key));
	 return SUCCESS;
}
public RefMasterManager getRefMasterManager() {
	return refMasterManager;
}
public void setRefMasterManager(RefMasterManager refMasterManager) {
	this.refMasterManager = refMasterManager;
}
public String getSessionCorpID() {
	return sessionCorpID;
}
public void setSessionCorpID(String sessionCorpID) {
	this.sessionCorpID = sessionCorpID;
}
public Map<String, String> getCountry() {
	return country;
}
public void setCountry(Map<String, String> country) {
	this.country = country;
}
public VatIdentification getVatIdentification() {
	return vatIdentification;
}
public void setVatIdentification(VatIdentification vatIdentification) {
	this.vatIdentification = vatIdentification;
}

public void setVatIdentificationManager(
		VatIdentificationManager vatIdentificationManager) {
	this.vatIdentificationManager = vatIdentificationManager;
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public List getVatList() {
	return vatList;
}
public void setVatList(List vatList) {
	this.vatList = vatList;
}
public String getFlage() {
	return flage;
}
public void setFlage(String flage) {
	this.flage = flage;
}
public String getVatCountryCode() {
	return vatCountryCode;
}
public void setVatCountryCode(String vatCountryCode) {
	this.vatCountryCode = vatCountryCode;
}
public String getValidVatCode() {
	return validVatCode;
}
public void setValidVatCode(String validVatCode) {
	this.validVatCode = validVatCode;
}
public String getVatNumber() {
	return vatNumber;
}
public void setVatNumber(String vatNumber) {
	this.vatNumber = vatNumber;
}
public String getVatId() {
	return vatId;
}
public void setVatId(String vatId) {
	this.vatId = vatId;
}
}
