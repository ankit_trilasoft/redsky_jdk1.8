<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="upload.title" /></title>
<meta name="heading" content="<fmt:message key='upload.heading'/>" />

<script>
function formSubmit(){
	if(document.forms['uploadForm'].elements['fileId'].value!='' && document.forms['uploadForm'].elements['SOName'].value!=''){
		 //alert('Submiting')
		 document.uploadForm.submit();
		 //alert('Submited')
	}
}
function validat(){
	if(document.forms['uploadForm'].elements['fileType'].value==''){
		alert('Please select file type.');
		document.forms['uploadForm'].elements['fileType'].focus;
		return false;
	}
	if(document.forms['uploadForm'].elements['description'].value==''){
		alert('Please fill the description.');
		document.forms['uploadForm'].elements['description'].focus;
		return false;
	}
	if(document.forms['uploadForm'].elements['fileId'].value=='' ){
		alert('Please fill the customr file / service order number.');
		document.forms['uploadForm'].elements['fileId'].focus;
		return false;
	}
	if(document.forms['uploadForm'].elements['fileId'].value!=''){
		document.forms['uploadForm'].elements['SOName'].focus();
		setTimeout('formSubmit()', 1000)
		return false;
	}
	
}

function setDescriptionDuplicate(targetElement){
	document.forms['uploadForm'].elements['description'].value = targetElement.options[targetElement.selectedIndex].text;
}

function setFlagValue(){
	document.forms['uploadForm'].elements['hitFlag'].value = '1';
}

function strippedDoc(){
	var cover = '${myFile.coverPageStripped}';
	var fileID = document.forms['uploadForm'].elements['fileId'].value; 
	
	if(document.forms['uploadForm'].elements['fileType'].value==''){
		alert('Please select file type.');
		document.forms['uploadForm'].elements['fileType'].focus;
		return false;
	}
	if(document.forms['uploadForm'].elements['description'].value==''){
		alert('Please fill the description.');
		document.forms['uploadForm'].elements['description'].focus;
		return false;
	}else if(document.forms['uploadForm'].elements['fileId'].value=='' ){
		alert('Please fill the customr file / service order number.');
		document.forms['uploadForm'].elements['fileId'].focus;
		return false;
	}
	
	if(cover == 'true'){
		alert('Cover page already stripped.');
		return false;
	}else{
		var checkBoxId = '${myFile.id}';
		var agree = confirm("The first page will be deleted, do you want to continue?");
		if (agree){
			var url = "strippedFileOpen.html?rId="+checkBoxId;
			document.forms['uploadForm'].action = url;
			document.forms['uploadForm'].submit();
		}else{
			return false;
		}
	}
}

function validateSO(){
	var fileID = document.forms['uploadForm'].elements['fileId'].value; 
    if(fileID==''){
    	document.forms['uploadForm'].elements['SOName'].value = '';
	}
    if(fileID != ''){
     	var url="serviceOrderName.html?ajax=1&decorator=simple&popup=true&fileId=" + encodeURI(fileID);
     	http2.open("GET", url, true);
     	http2.onreadystatechange = handleHttpResponse2;
    	http2.send(null);
    }
}

function handleHttpResponse2(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                if(results.length >= 1){ 
           			document.forms['uploadForm'].elements['SOName'].value = results;
 					document.forms['uploadForm'].elements['fileId'].select();
		       }else{
	            alert("Customer File / Service order number is not valid");    
				document.forms['uploadForm'].elements['SOName'].value = '';
				document.forms['uploadForm'].elements['fileId'].value = '';
				document.forms['uploadForm'].elements['fileId'].select();
				return false;
			   }
           }
}

var http2 = getHTTPObject();

function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function docSplit(){
	if(document.forms['uploadForm'].elements['fileId'].value == '' && document.forms['uploadForm'].elements['SOName'].value == ''){
		alert('Please fill the Customer File / Service Order Number and Save Document.');
		return false;
	}	
	if('${fileId}' == ''){
		alert('Please Save Document before split.');
		return false;
	}
	if(document.forms['uploadForm'].elements['fileId'].value != '' && document.forms['uploadForm'].elements['SOName'].value != ''){
		location.href = 'docSplit.html?fileId=${myFile.id}';
	}
}

function documentMap(targetElement){
   	var fileType = targetElement.value;
   	var url="docMapFolder.html?ajax=1&decorator=simple&popup=true&fileType=" + encodeURI(fileType);
   	http2.open("GET", url, true);
   	http2.onreadystatechange = handleHttpResponseMap;
   	http2.send(null);
}

function handleHttpResponseMap(){
      if(http2.readyState == 4){
		var results = http2.responseText
        results = results.trim();
        var res = results.substring(1,results.length-1);
		document.forms['uploadForm'].elements['mapFolder'].options[0].text = res; 
		document.forms['uploadForm'].elements['mapFolder'].options[0].value = res;
		document.forms['uploadForm'].elements['mapFolder'].options[0].selected=true;					     	 
	}
}

</script>
</head>
<s:form action="uploadMyFileDoc.html?id=${myFile.id}" method="post" validate="true" id="uploadForm" name="uploadForm">

<c:set var="fileForId" value="<%=request.getParameter("fileForId") %>"/>
<s:hidden name="fileForId" value="<%=request.getParameter("fileForId") %>"/>

<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFrom") %>"/>
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFrom") %>"/>

<s:hidden name="myFile.id"/>
<c:set var="description" value="${myFile.description}"/>
<div id="Layer1" style="width: 100%;">
	<div id="newmnav">
		<ul>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Upload Document<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<!--  li><a href="docSplit.html?fileId=${myFile.id}"><span>Split Document</span></a></li> -->
			<c:if test="${myFile.fileContentType == 'application/pdf'}">
				<li><a onclick="return docSplit();"><span>Split Document</span></a></li>
			</c:if>	
			<li><a href="docProcess.html"><span>Document List</span></a></li>
		</ul>
	</div>	
	
	<div class="spn">&nbsp;</div>
	

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="detailTabLabel" cellspacing="2" cellpadding="1" border="0">
		<tbody>
			<c:if test="${myFile.fileContentType == 'application/pdf'}">
				<tr>
					<td valign="top">
						<table cellpadding="1" cellspacing="0">
							<tbody>
								
								<tr>
									<td align="right" class="listwhitetext" style="">Document Type</td>
									<td width="120px" colspan="2"><s:select name="fileType" cssClass="list-menu" list="%{docsList}" headerKey="" headerValue="" onchange="setDescriptionDuplicate(this),documentMap(this);" /></td>
									<td align="right" class="listwhitetext" width="100px">Map&nbsp;&nbsp;<s:select name="mapFolder" cssClass="list-menu" list="%{mapList}" headerKey="" headerValue="" cssStyle="width:70px" /></td>
									<td align="right" class="listwhitetext" style="" width="155px">File Name</td>
									<td colspan="3"><s:textfield name="myFile.fileFileName" cssClass="input-textUpper" size="47" readonly="true" /></td>
								</tr>
								<tr>	
									<td align="right" class="listwhitetext" style="">Portal Access : </td>
									<td colspan="7">
										<table class="detailTabLabel">
											<tr>							
												<c:set var="ischecked" value="false"/>
													<c:if test="${myFile.isCportal}">
														<c:set var="ischecked" value="true"/>
													</c:if>	
												<td align="left" class="listwhitetext" width="100px"><s:checkbox name="isCportal" key="myFile.isCportal" value="${ischecked}" fieldValue="true"/>&nbsp;Customer</td>
												
												<c:set var="ischeckedACC" value="false"/>
													<c:if test="${myFile.isAccportal}">
														<c:set var="ischeckedACC" value="true"/>
													</c:if>
												<td class="listwhitetext" width="100px"><s:checkbox name="isAccportal" key="myFile.isAccportal" value="${ischeckedACC}" fieldValue="true"/> &nbsp;Account</td>
												
												<c:set var="ischeckedPP" value="false"/>
													<c:if test="${myFile.isPartnerPortal}">
														<c:set var="ischeckedPP" value="true"/>
													</c:if>
												<td class="listwhitetext" width="100px"><s:checkbox name="isPPortal" key="myFile.isPartnerPortal" value="${ischeckedPP}" fieldValue="true"/> &nbsp;Partner</td>
												<td align="right" class="listwhitetext" style="">Customer / Service Order No.</td>
												<td><s:textfield name="fileId" value="${fileId}" cssClass="input-text" size="20" maxLength="15" onchange="return validateSO();"/></td>
												<td><s:textfield name="SOName" cssClass="input-text" size="20" readonly="true"/></td>
											</tr>
										</table>
									</td>
								</tr>
								
								<tr>
									<td align="right" valign="top" class="listwhitetext" style="">Description</td>
									<td colspan="7" valign="top"><s:textarea name="description" value="${description}"  cols="135" rows="1"  cssClass="textarea"/></td>
								</tr>
								
								
								<tr ><td colspan="8" style="border-bottom: 2px solid;border-bottom-color:#c1c1c1; "></td></tr>
							</tbody>
						</table>
					</td>
				</tr>	
				<tr>	
					<td valign="top">
							<%@ include file="/common/enable-dojo-imagedocviewer.jsp"%>
					</td>
				</tr>
			</c:if>
			
			<c:if test="${myFile.fileContentType != 'application/pdf'}">
				<tr>
					<td align="right" class="listwhitetext" style="width:165px">Document Type</td>
					<td width="120px" colspan=""><s:select name="fileType" cssClass="list-menu" list="%{docsList}" headerKey="" headerValue="" onchange="setDescriptionDuplicate(this),documentMap(this);" /></td>
					<td align="right" class="listwhitetext" width="100px">Map&nbsp;&nbsp;<s:select name="mapFolder" cssClass="list-menu" list="%{mapList}" headerKey="" headerValue="" cssStyle="width:70px" /></td>
				</tr>
				<tr>	
					<td align="right" class="listwhitetext" style="">Portal Access : </td>
					<td colspan="3">
					<table class="detailTabLabel" cellpadding="1" cellspacing="1" style="margin:0px; ">
					<tr>							
					<c:set var="ischecked" value="false"/>
						<c:if test="${myFile.isCportal}">
							<c:set var="ischecked" value="true"/>
						</c:if>	
					<td align="left" class="listwhitetext" width="100px"><s:checkbox name="isCportal" key="myFile.isCportal" value="${ischecked}" fieldValue="true"/>&nbsp;Customer</td>
					
					<c:set var="ischeckedACC" value="false"/>
						<c:if test="${myFile.isAccportal}">
							<c:set var="ischeckedACC" value="true"/>
						</c:if>
					<td class="listwhitetext" width="100px"><s:checkbox name="isAccportal" key="myFile.isAccportal" value="${ischeckedACC}" fieldValue="true"/> &nbsp;Account</td>
					
					<c:set var="ischeckedPP" value="false"/>
						<c:if test="${myFile.isPartnerPortal}">
							<c:set var="ischeckedPP" value="true"/>
						</c:if>
					<td class="listwhitetext" width="100px"><s:checkbox name="isPPortal" key="myFile.isPartnerPortal" value="${ischeckedPP}" fieldValue="true"/> &nbsp;Partner</td>
					</tr>
					</table>
					</td>
				</tr>
				
				<tr>
					<td align="right" class="listwhitetext" style="">Description</td>
					<td colspan="3"><s:textarea name="description" value="${description}"  cols="66" rows="4" cssClass="textarea"/></td>
				</tr>
				
				<tr>
					<td align="right" class="listwhitetext" style="">File Name</td>
					<td colspan="3"><s:textfield name="myFile.fileFileName" cssClass="input-textUpper" size="70" readonly="true" /></td>
				</tr>
				
				<tr>
					<td align="right" class="listwhitetext" style="">Customer File / Service Order No.</td>
					<td><s:textfield name="fileId" value="${fileId}" cssClass="input-text" size="32" maxLength="15" onchange="return validateSO();"/></td>
					<td><s:textfield name="SOName" cssClass="input-text" size="30" readonly="true"/></td>
				</tr>
				<tr><td></td></tr>
			</c:if>
			<tr height="20px"></tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<table border="0">
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.createdOn' /></b></td>

				
				<td style="width:130px"><fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${myFile.createdOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="myFile.createdOn" value="${customerFileCreatedOnFormattedValue}" /> <fmt:formatDate value="${myFile.createdOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="right" class="listwhitetext" style="width:65px"><b><fmt:message key='billing.createdBy' /></b></td>
				<c:if test="${not empty myFile.id}">
					<s:hidden name="myFile.createdBy" />
					<td style="width:65px"><s:label name="createdBy" value="%{myFile.createdBy}" /></td>
				</c:if>
				<c:if test="${empty myFile.id}">
					<s:hidden name="myFile.createdBy" value="${pageContext.request.remoteUser}" />
					<td style="width:65px"><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
				</c:if>				
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.updatedOn' /></b></td>
				<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${myFile.updatedOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="myFile.updatedOn" value="${customerFileupdatedOnFormattedValue}" />
				<td style="width:120px"><fmt:formatDate value="${myFile.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.updatedBy' /></b></td>
				<c:if test="${not empty myFile.id}">
					<s:hidden name="myFile.updatedBy"/>
					<td style="width:85px"><s:label name="updatedBy" value="%{myFile.updatedBy}"/></td>
				</c:if>
				<c:if test="${empty myFile.id}">
					<s:hidden name="myFile.updatedBy" value="${pageContext.request.remoteUser}"/>
					<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				</c:if>
			</tr>
		</tbody>
	</table>
		<tr>
			<td><s:submit value="Save Without Stripping" cssClass="cssbutton" cssStyle="margin-right: 5px;height: 25px;width:174px; font-size: 15" onclick="return validat();"/></td>
				
				<td align="right">
					<input type="button" class="cssbutton" style="margin-right: 5px;height: 25px;width:145px; font-size: 15" name="stripped"  value="Save & Strip Cover Page" onclick="javascript: return strippedDoc();"/>
					<input type="button" class="cssbutton" style="margin-right: 5px;height: 25px;width:100px; font-size: 15" name="stripped"  value="View Document" onclick="javascript:window.open('ImageServletAction.html?id=${myFile.id}&decorator=popup&popup=true','document','height=900,width=475,top=0, left=610, scrollbars=0, resizable=yes');"/>
				</td>
		</tr>
		
</s:form>

<script type="text/javascript">
try{
<c:if test="${hitFlag == 1}" >
	<c:redirect url="/docProcess.html"/>
</c:if>
}
catch(e){}
try{
<c:if test="${hitFlag == 0}" >
	alert('Customer File / Service order number is not valid');
</c:if>
}
catch(e){}
try{
<c:if test="${hitFlag == null || hitFlag != 0}" >
	validateSO();
</c:if>
}
catch(e){}
</script>

