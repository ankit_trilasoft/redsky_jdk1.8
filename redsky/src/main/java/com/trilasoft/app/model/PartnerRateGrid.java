package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="partnerrategrid")
@FilterDef(name = "partnerCorpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "partnerCorpID", condition = "corpID in (:forCorpID)")
public class PartnerRateGrid extends BaseObject {
	private Long id;
	private String corpID;
	private String partnerCode;
	private String tariffApplicability;
	private String metroCity;
	private String terminalCountry;
	private BigDecimal serviceRangeMiles;
	private BigDecimal serviceRangeKms;
	private Date effectiveDate;
	private Date endDate;
	private String currency;
	private boolean tarriffFlagAir;
	private boolean tariffFlagL20;
	private boolean tariffFlagL40;
	private boolean tariffFlagLCL;
	private boolean tariffFlagFCL;
	private boolean tariffFalgGRP;
	private String status;
	private String auto_hhg_toDoor;
	private String auto_lcl_toDoor;
	private String auto_hhg_whse;
	private String auto_lcl_whse;
	private String sit_storage_period;
	private String sit_storage_basis;
	private String sit_storage_rate;
	private String sit_whse_loose_basis;
	private String sit_whse_loose_rate;
	private String sit_whse_liftvan_basis;
	private String sit_whse_liftvan_rate;
	private String state;
	private String standardInclusions;
	private String tariffNotes;
	private BigDecimal serviceRangeMiles2;
	private BigDecimal serviceRangeMiles3;
	private BigDecimal serviceRangeMiles4;
	private BigDecimal serviceRangeMiles5;
	private BigDecimal serviceRangeKms2;
	private BigDecimal serviceRangeKms3;
	private BigDecimal serviceRangeKms4;
	private BigDecimal serviceRangeKms5;
	private BigDecimal serviceRangeExtra2;
	private BigDecimal serviceRangeExtra3;
	private BigDecimal serviceRangeExtra4;
	private BigDecimal serviceRangeExtra5;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String userConfirming;
	private Date confirmationDate;
	private boolean publishTariff;
	private String partnerName;
	private String tariffScope;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("partnerCode", partnerCode).append(
				"tariffApplicability", tariffApplicability).append("metroCity",
				metroCity).append("terminalCountry", terminalCountry).append(
				"serviceRangeMiles", serviceRangeMiles).append(
				"serviceRangeKms", serviceRangeKms).append("effectiveDate",
				effectiveDate).append("endDate", endDate).append("currency",
				currency).append("tarriffFlagAir", tarriffFlagAir).append(
				"tariffFlagL20", tariffFlagL20).append("tariffFlagL40",
				tariffFlagL40).append("tariffFlagLCL", tariffFlagLCL).append(
				"tariffFlagFCL", tariffFlagFCL).append("tariffFalgGRP",
				tariffFalgGRP).append("status", status).append(
				"auto_hhg_toDoor", auto_hhg_toDoor).append("auto_lcl_toDoor",
				auto_lcl_toDoor).append("auto_hhg_whse", auto_hhg_whse).append(
				"auto_lcl_whse", auto_lcl_whse).append("sit_storage_period",
				sit_storage_period).append("sit_storage_basis",
				sit_storage_basis).append("sit_storage_rate", sit_storage_rate)
				.append("sit_whse_loose_basis", sit_whse_loose_basis).append(
						"sit_whse_loose_rate", sit_whse_loose_rate).append(
						"sit_whse_liftvan_basis", sit_whse_liftvan_basis)
				.append("sit_whse_liftvan_rate", sit_whse_liftvan_rate).append(
						"state", state).append("standardInclusions",
						standardInclusions).append("tariffNotes", tariffNotes)
				.append("serviceRangeMiles2", serviceRangeMiles2).append(
						"serviceRangeMiles3", serviceRangeMiles3).append(
						"serviceRangeMiles4", serviceRangeMiles4).append(
						"serviceRangeMiles5", serviceRangeMiles5).append(
						"serviceRangeKms2", serviceRangeKms2).append(
						"serviceRangeKms3", serviceRangeKms3).append(
						"serviceRangeKms4", serviceRangeKms4).append(
						"serviceRangeKms5", serviceRangeKms5).append(
						"serviceRangeExtra2", serviceRangeExtra2).append(
						"serviceRangeExtra3", serviceRangeExtra3).append(
						"serviceRangeExtra4", serviceRangeExtra4).append(
						"serviceRangeExtra5", serviceRangeExtra5).append(
						"createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("userConfirming", userConfirming).append(
						"confirmationDate", confirmationDate).append(
						"publishTariff", publishTariff).append("partnerName",
						partnerName).append("tariffScope", tariffScope)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerRateGrid))
			return false;
		PartnerRateGrid castOther = (PartnerRateGrid) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(partnerCode, castOther.partnerCode)
				.append(tariffApplicability, castOther.tariffApplicability)
				.append(metroCity, castOther.metroCity).append(terminalCountry,
						castOther.terminalCountry).append(serviceRangeMiles,
						castOther.serviceRangeMiles).append(serviceRangeKms,
						castOther.serviceRangeKms).append(effectiveDate,
						castOther.effectiveDate).append(endDate,
						castOther.endDate).append(currency, castOther.currency)
				.append(tarriffFlagAir, castOther.tarriffFlagAir).append(
						tariffFlagL20, castOther.tariffFlagL20).append(
						tariffFlagL40, castOther.tariffFlagL40).append(
						tariffFlagLCL, castOther.tariffFlagLCL).append(
						tariffFlagFCL, castOther.tariffFlagFCL).append(
						tariffFalgGRP, castOther.tariffFalgGRP).append(status,
						castOther.status).append(auto_hhg_toDoor,
						castOther.auto_hhg_toDoor).append(auto_lcl_toDoor,
						castOther.auto_lcl_toDoor).append(auto_hhg_whse,
						castOther.auto_hhg_whse).append(auto_lcl_whse,
						castOther.auto_lcl_whse).append(sit_storage_period,
						castOther.sit_storage_period).append(sit_storage_basis,
						castOther.sit_storage_basis).append(sit_storage_rate,
						castOther.sit_storage_rate).append(
						sit_whse_loose_basis, castOther.sit_whse_loose_basis)
				.append(sit_whse_loose_rate, castOther.sit_whse_loose_rate)
				.append(sit_whse_liftvan_basis,
						castOther.sit_whse_liftvan_basis).append(
						sit_whse_liftvan_rate, castOther.sit_whse_liftvan_rate)
				.append(state, castOther.state).append(standardInclusions,
						castOther.standardInclusions).append(tariffNotes,
						castOther.tariffNotes).append(serviceRangeMiles2,
						castOther.serviceRangeMiles2).append(
						serviceRangeMiles3, castOther.serviceRangeMiles3)
				.append(serviceRangeMiles4, castOther.serviceRangeMiles4)
				.append(serviceRangeMiles5, castOther.serviceRangeMiles5)
				.append(serviceRangeKms2, castOther.serviceRangeKms2).append(
						serviceRangeKms3, castOther.serviceRangeKms3).append(
						serviceRangeKms4, castOther.serviceRangeKms4).append(
						serviceRangeKms5, castOther.serviceRangeKms5).append(
						serviceRangeExtra2, castOther.serviceRangeExtra2)
				.append(serviceRangeExtra3, castOther.serviceRangeExtra3)
				.append(serviceRangeExtra4, castOther.serviceRangeExtra4)
				.append(serviceRangeExtra5, castOther.serviceRangeExtra5)
				.append(createdBy, castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(userConfirming,
						castOther.userConfirming).append(confirmationDate,
						castOther.confirmationDate).append(publishTariff,
						castOther.publishTariff).append(partnerName,
						castOther.partnerName).append(tariffScope,
						castOther.tariffScope).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				partnerCode).append(tariffApplicability).append(metroCity)
				.append(terminalCountry).append(serviceRangeMiles).append(
						serviceRangeKms).append(effectiveDate).append(endDate)
				.append(currency).append(tarriffFlagAir).append(tariffFlagL20)
				.append(tariffFlagL40).append(tariffFlagLCL).append(
						tariffFlagFCL).append(tariffFalgGRP).append(status)
				.append(auto_hhg_toDoor).append(auto_lcl_toDoor).append(
						auto_hhg_whse).append(auto_lcl_whse).append(
						sit_storage_period).append(sit_storage_basis).append(
						sit_storage_rate).append(sit_whse_loose_basis).append(
						sit_whse_loose_rate).append(sit_whse_liftvan_basis)
				.append(sit_whse_liftvan_rate).append(state).append(
						standardInclusions).append(tariffNotes).append(
						serviceRangeMiles2).append(serviceRangeMiles3).append(
						serviceRangeMiles4).append(serviceRangeMiles5).append(
						serviceRangeKms2).append(serviceRangeKms3).append(
						serviceRangeKms4).append(serviceRangeKms5).append(
						serviceRangeExtra2).append(serviceRangeExtra3).append(
						serviceRangeExtra4).append(serviceRangeExtra5).append(
						createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).append(userConfirming).append(
						confirmationDate).append(publishTariff).append(
						partnerName).append(tariffScope).toHashCode();
	}
	@Column(length=10)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=3)
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@Column
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	@Column
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(length=50)
	public String getMetroCity() {
		return metroCity;
	}
	public void setMetroCity(String metroCity) {
		this.metroCity = metroCity;
	}
	
	@Column(length=8)
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	
	@Column(length=12)
	public BigDecimal getServiceRangeKms() {
		return serviceRangeKms;
	}
	public void setServiceRangeKms(BigDecimal serviceRangeKms) {
		this.serviceRangeKms = serviceRangeKms;
	}
	
	@Column(length=12)
	public BigDecimal getServiceRangeMiles() {
		return serviceRangeMiles;
	}
	public void setServiceRangeMiles(BigDecimal serviceRangeMiles) {
		this.serviceRangeMiles = serviceRangeMiles;
	}
	
	@Column(length=30)
	public String getTariffApplicability() {
		return tariffApplicability;
	}
	public void setTariffApplicability(String tariffApplicability) {
		this.tariffApplicability = tariffApplicability;
	}
	
	@Column
	public boolean isTariffFalgGRP() {
		return tariffFalgGRP;
	}
	public void setTariffFalgGRP(boolean tariffFalgGRP) {
		this.tariffFalgGRP = tariffFalgGRP;
	}
	
	@Column
	public boolean isTariffFlagFCL() {
		return tariffFlagFCL;
	}
	public void setTariffFlagFCL(boolean tariffFlagFCL) {
		this.tariffFlagFCL = tariffFlagFCL;
	}
	
	@Column
	public boolean isTariffFlagL20() {
		return tariffFlagL20;
	}
	public void setTariffFlagL20(boolean tariffFlagL20) {
		this.tariffFlagL20 = tariffFlagL20;
	}
	
	@Column
	public boolean isTariffFlagL40() {
		return tariffFlagL40;
	}
	public void setTariffFlagL40(boolean tariffFlagL40) {
		this.tariffFlagL40 = tariffFlagL40;
	}
	
	@Column
	public boolean isTariffFlagLCL() {
		return tariffFlagLCL;
	}
	public void setTariffFlagLCL(boolean tariffFlagLCL) {
		this.tariffFlagLCL = tariffFlagLCL;
	} 
	
	@Column
	public boolean isTarriffFlagAir() {
		return tarriffFlagAir;
	}
	public void setTarriffFlagAir(boolean tarriffFlagAir) {
		this.tarriffFlagAir = tarriffFlagAir;
	}
	
	@Column(length =3)
	public String getTerminalCountry() {
		return terminalCountry;
	}
	public void setTerminalCountry(String terminalCountry) {
		this.terminalCountry = terminalCountry;
	}
	
	@Column(length = 20)
	public String getAuto_hhg_toDoor() {
		return auto_hhg_toDoor;
	}
	public void setAuto_hhg_toDoor(String auto_hhg_toDoor) {
		this.auto_hhg_toDoor = auto_hhg_toDoor;
	}
	
	@Column(length = 20)
	public String getAuto_hhg_whse() {
		return auto_hhg_whse;
	}
	public void setAuto_hhg_whse(String auto_hhg_whse) {
		this.auto_hhg_whse = auto_hhg_whse;
	}
	
	@Column(length = 20)
	public String getAuto_lcl_toDoor() {
		return auto_lcl_toDoor;
	}
	public void setAuto_lcl_toDoor(String auto_lcl_toDoor) {
		this.auto_lcl_toDoor = auto_lcl_toDoor;
	}
	
	@Column(length = 20)
	public String getAuto_lcl_whse() {
		return auto_lcl_whse;
	}
	public void setAuto_lcl_whse(String auto_lcl_whse) {
		this.auto_lcl_whse = auto_lcl_whse;
	}
	
	@Column(length = 20)
	public String getSit_storage_basis() {
		return sit_storage_basis;
	}
	public void setSit_storage_basis(String sit_storage_basis) {
		this.sit_storage_basis = sit_storage_basis;
	}
	
	@Column(length = 20)
	public String getSit_storage_period() {
		return sit_storage_period;
	}
	public void setSit_storage_period(String sit_storage_period) {
		this.sit_storage_period = sit_storage_period;
	}
	
	@Column(length = 20)
	public String getSit_storage_rate() {
		return sit_storage_rate;
	}
	public void setSit_storage_rate(String sit_storage_rate) {
		this.sit_storage_rate = sit_storage_rate;
	}
	
	@Column(length = 20)
	public String getSit_whse_liftvan_basis() {
		return sit_whse_liftvan_basis;
	}
	public void setSit_whse_liftvan_basis(String sit_whse_liftvan_basis) {
		this.sit_whse_liftvan_basis = sit_whse_liftvan_basis;
	}
	
	@Column(length = 20)
	public String getSit_whse_liftvan_rate() {
		return sit_whse_liftvan_rate;
	}
	public void setSit_whse_liftvan_rate(String sit_whse_liftvan_rate) {
		this.sit_whse_liftvan_rate = sit_whse_liftvan_rate;
	}
	
	@Column(length = 20)
	public String getSit_whse_loose_basis() {
		return sit_whse_loose_basis;
	}
	public void setSit_whse_loose_basis(String sit_whse_loose_basis) {
		this.sit_whse_loose_basis = sit_whse_loose_basis;
	}
	
	@Column(length = 20)
	public String getSit_whse_loose_rate() {
		return sit_whse_loose_rate;
	}
	public void setSit_whse_loose_rate(String sit_whse_loose_rate) {
		this.sit_whse_loose_rate = sit_whse_loose_rate;
	}
	@Column(length = 20)
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(length=3)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column(length=5000)
	public String getStandardInclusions() {
		return standardInclusions;
	}
	public void setStandardInclusions(String standardInclusions) {
		this.standardInclusions = standardInclusions;
	}
	@Column(length=5000)
	public String getTariffNotes() {
		return tariffNotes;
	}
	public void setTariffNotes(String tariffNotes) {
		this.tariffNotes = tariffNotes;
	}
	
	@Column(length=20)
	public BigDecimal getServiceRangeExtra2() {
		return serviceRangeExtra2;
	}
	public void setServiceRangeExtra2(BigDecimal serviceRangeExtra2) {
		this.serviceRangeExtra2 = serviceRangeExtra2;
	}
	
	@Column(length=20)
	public BigDecimal getServiceRangeExtra3() {
		return serviceRangeExtra3;
	}
	public void setServiceRangeExtra3(BigDecimal serviceRangeExtra3) {
		this.serviceRangeExtra3 = serviceRangeExtra3;
	}
	
	@Column(length=20)
	public BigDecimal getServiceRangeExtra4() {
		return serviceRangeExtra4;
	} 
	public void setServiceRangeExtra4(BigDecimal serviceRangeExtra4) {
		this.serviceRangeExtra4 = serviceRangeExtra4;
	}
	
	@Column(length=20)
	public BigDecimal getServiceRangeExtra5() {
		return serviceRangeExtra5;
	} 
	public void setServiceRangeExtra5(BigDecimal serviceRangeExtra5) {
		this.serviceRangeExtra5 = serviceRangeExtra5;
	}
	
	@Column(length=6)
	public BigDecimal getServiceRangeKms2() {
		return serviceRangeKms2;
	}
	public void setServiceRangeKms2(BigDecimal serviceRangeKms2) {
		this.serviceRangeKms2 = serviceRangeKms2;
	}
	
	@Column(length=6)
	public BigDecimal getServiceRangeKms3() {
		return serviceRangeKms3;
	}
	public void setServiceRangeKms3(BigDecimal serviceRangeKms3) {
		this.serviceRangeKms3 = serviceRangeKms3;
	}
	
	@Column(length=6)
	public BigDecimal getServiceRangeKms4() {
		return serviceRangeKms4;
	}
	public void setServiceRangeKms4(BigDecimal serviceRangeKms4) {
		this.serviceRangeKms4 = serviceRangeKms4;
	}
	
	@Column(length=6)
	public BigDecimal getServiceRangeKms5() {
		return serviceRangeKms5;
	}
	public void setServiceRangeKms5(BigDecimal serviceRangeKms5) {
		this.serviceRangeKms5 = serviceRangeKms5;
	}
	
	@Column(length=6)
	public BigDecimal getServiceRangeMiles2() {
		return serviceRangeMiles2;
	}
	public void setServiceRangeMiles2(BigDecimal serviceRangeMiles2) {
		this.serviceRangeMiles2 = serviceRangeMiles2;
	}
	
	@Column(length=6)
	public BigDecimal getServiceRangeMiles3() {
		return serviceRangeMiles3;
	}
	public void setServiceRangeMiles3(BigDecimal serviceRangeMiles3) {
		this.serviceRangeMiles3 = serviceRangeMiles3;
	}
	
	@Column(length=6)
	public BigDecimal getServiceRangeMiles4() {
		return serviceRangeMiles4;
	}
	public void setServiceRangeMiles4(BigDecimal serviceRangeMiles4) {
		this.serviceRangeMiles4 = serviceRangeMiles4;
	}
	
	@Column(length=6)
	public BigDecimal getServiceRangeMiles5() {
		return serviceRangeMiles5;
	}
	public void setServiceRangeMiles5(BigDecimal serviceRangeMiles5) {
		this.serviceRangeMiles5 = serviceRangeMiles5;
	}
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column
	public Date getConfirmationDate() {
		return confirmationDate;
	}
	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}
	
	@Column(length=25)
	public String getUserConfirming() {
		return userConfirming;
	}
	public void setUserConfirming(String userConfirming) {
		this.userConfirming = userConfirming;
	}
	
	@Column
	public boolean isPublishTariff() {
		return publishTariff;
	}
	public void setPublishTariff(boolean publishTariff) {
		this.publishTariff = publishTariff;
	}
	
	@Column(length=225)
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	@Column(length=225)
	public String getTariffScope() {
		return tariffScope;
	}
	public void setTariffScope(String tariffScope) {
		this.tariffScope = tariffScope;
	}

}
