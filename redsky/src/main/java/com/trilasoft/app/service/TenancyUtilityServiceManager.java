package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.TenancyUtilityService;


public interface TenancyUtilityServiceManager extends GenericManager<TenancyUtilityService, Long>{

}
