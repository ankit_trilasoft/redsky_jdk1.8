package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;


import com.trilasoft.app.dao.PartnerRateGridContractsDao;

import com.trilasoft.app.model.PartnerRateGridContracts;

import com.trilasoft.app.service.PartnerRateGridContractsManager;

public class PartnerRateGridContractsManagerImpl extends GenericManagerImpl<PartnerRateGridContracts, Long> implements PartnerRateGridContractsManager {
	PartnerRateGridContractsDao partnerRateGridContractsDao;
	
	public PartnerRateGridContractsManagerImpl(PartnerRateGridContractsDao partnerRateGridContractsDao) {
		super(partnerRateGridContractsDao); 
        this.partnerRateGridContractsDao = partnerRateGridContractsDao; 
	}

	public List getRateGridContractsList(String sessionCorpID, Long partnerRateGridID) {
	  return partnerRateGridContractsDao.getRateGridContractsList(sessionCorpID, partnerRateGridID);
	}

	public List getdDiscountCompanyList() {
		return partnerRateGridContractsDao.getdDiscountCompanyList();
	}

	public List getContractList(String sessionCorpID) {
		return partnerRateGridContractsDao.getContractList(sessionCorpID);
	}

	public int findUniqueCount(Long partnerRateGridID,String sessionCorpID, String discountCompany, String contract, String effectiveDate) {
		return partnerRateGridContractsDao.findUniqueCount(partnerRateGridID,sessionCorpID, discountCompany, contract, effectiveDate);
	}

	public List findMaxEffectiveDate(String sessionCorpID, Long partnerRateGridID) {
		return partnerRateGridContractsDao.findMaxEffectiveDate(sessionCorpID, partnerRateGridID);
	}

	public int findUniqueCount(Long partnerRateGridID,String sessionCorpID, String discountCompany, String contract, String effectiveDate, Long id) {
		return partnerRateGridContractsDao.findUniqueCount(partnerRateGridID,sessionCorpID, discountCompany, contract, effectiveDate,id);
	}  
}
