package com.trilasoft.app.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.OperationsIntelligence;


public interface OperationsIntelligenceDao extends GenericDao<OperationsIntelligence, Long>{
	public void updateOperationsIntelligence(Long id,String fieldName,String fieldValue,String tableName ,String shipNumber,String updatedBy);
	public List findAllResourcesAjax(String shipNumber,String sessionCorpID,String resouceTemplate, String resourceListForOIStatus);
	public void getDeleteWorkOrder(String shipNumber,String sessionCorpID);
	public String getSellRateBuyRate(String type,String shipNumber,String descript,String sessionCorpID);
	public void deletefromOIResorcesList(long id,String shipNumber,String usertype);
	public void deletefromWorkTicketResorcesList(long id,String shipNumber,String ticket, String sessionCorpID);
	public List distinctWorkOrderForWT(String shipNumber,String sessionCorpID);
	public List getResourceFromOI(String sessionCorpID ,String shipNumber, String workorder);
	 public List distinctWorkOrderForThirdParty(String shipNumber,String sessionCorpID);
	 public List getResourceFromOIForTP(String sessionCorpID ,String shipNumber, String workorder ,String tpId);
	 public void updateOperationsIntelligenceTicketNo(Long ticket,String workorder,String shipNumber,String sessionCorpID,String tpId,String date, Long ticketId, String usertype,String targetActual);
	 public void updateOperationsIntelligenceForScope(String workorder,String tempScopeOfWo,String shipNumber,Long ticket);
	 public void saveScopeInOperationsIntelligence(String workorder,String tempScopeOfWo,String shipNumber,Long ticket,String numberOfComputer,String numberOfEmployee,Double salesTax,String consumables,String revisionScopeValue,boolean consumablePercentageCheck,String aB5Surcharge,boolean editAB5Surcharge ,BigDecimal consumableFromPP);
	 public String findScopeOfWorkOrder(Long id,String shipNumber);
	 public String checkForResourceTransFerWorkTicket(String sessionCorpID ,String shipNumber, String workorder);
	 public void updateOperationsIntelligenceItemsJbkEquipId(Long id ,Long itemsJbkEquipId,String shipNumber,String sessionCorpID);
	 public List scopeOfWorkOrder(String shipNumber,String sessionCorpID,String resouceTemplate);
	 public String checkForTicketBasedOnWorkOrder(String sessionCorpID ,String shipNumber, String workorder);
	 public List distinctWorkOrderForOI(String shipNumber,String sessionCorpID,String flag );
	 public List findResourcesByWorkOrder(String workorder,String shipNumber);
	 public String scopeByWorkOrder(String workorder ,String shipNumber);
	 public void updateCalculationOfOI(String calculateOI,String shipNumber);
	 public void updateSalesTax(Double salesTaxForOI, String shipNumber,String salesTaxFieldName);
	 public List showDistinctWorkOrderForCopy(String shipNumber,String sessionCorpID);
	 public List copyWorkOrderToResource(String distinctWorkOrder,String shipNumber);
	 public String FindMaxWorkOrder(String shipNumber);
	public void changeStatusOfWorkOrder(String workOrderGroup, String shipNumber,String updatedBy,String itemsJbkEquipId);
	public void changeStatusOfWorkTicketFromDel(Long id , String shipNumber,String updatedBy,Long itemsJbkEquipId);
	public void changeStatusOfWorkOrderForThirdParty(String workorder,String shipNumber,String updatedBy, Long ticket);
	public void updateWorkOrder(String workorder,String shipNumber,String usertype);
	public List distinctWOForInsert(String action,String shipNumber,String distinctWorkOrder);
	public void updateOIDate(Long ticket,String shipNumber,String date1,String updatedBy,String targetActual);
	public void updateOIResourceTicketAndDate(Long ticket,String shipNumber,String updatedBy);
	public List taskDetailsListForOI(String sessionCorpID, Long ticket);
	public List distinctWOForOI(String shipNumber,String sessionCorpID,String flag,String wo);
	public String scopeByWorkOrderInUserPortal(String workorder, String shipNumber);
	public String getScopeWithValues(String shipNumber, String workorder,String type);
	public void getScopeWithValues(String shipNumber, String workorder, String type, Long ticket);
	public List FindValueFromOI(String shipNumber);
	public String valueOfCommentForRevised(Long id, String shipNumber, String commentField);
	public void saveCmtForRevised(Long id, String shipNumber,String value, String commentField);
	public List crewListBYTicket(Long ticket, String sessionCorpID);
	public List addWorkOrderToResource(String shipNumber);
	public List findDataForCopyEstimate(String shipNumber,String sessionCorpID);
	public List updateSelectiveInvoice(Long id, boolean oIStatus, String sessionCorpID,Long itemsJbkEquipId,String shipNumber,String ticket,String workorder,boolean estConsumCheck,BigDecimal estConsumableFromPP,boolean revConsumCheck,BigDecimal revConsumableFromPP,BigDecimal aB5Surcharge,boolean editAB5Surcharge,BigDecimal revisionScopeOfAB5Surcharge,boolean revisionaeditAB5Surcharge);
	public String findRevisionScopeOfWorkOrder(Long id,String shipNumber);
	public String findInvoicedWorkTicketNumber(String shipNumber);
	public void updateOperationIntelligenceRevisionSalesTax(String shipNumber);
	public void updateExpenseRevenueConsumables(String shipNumber);
	public List findEstimateRevisionExpenseRevenueSum(String shipNumber);
	public void updateEstimateAndRevisionConsumables(String shipNumber,String workOrder);
	public String findDistinctWorkOrder(String shipNumber,String sessionCorpID);
	public void updateEstimateAndRevisionConsumablesWithParnerPercentage(String shipNumber,String workOrder,String fieldName,BigDecimal consumableFromPP);
	public Boolean findConsumableChecked(String shipNumber,String workOrder,String fieldName);
	public void updateEstimateAndRevisionConsumablesWithOutParnerPercentage(String shipNumber,String workOrder,String fieldName);
	/*
	 * Bug 14390 - O&I and Ops work flow -- Stage One
	 */
	public String getPresetValue(String contract, String charge, String sessionCorpID,String descript);
	public void updateExpenseRevenueAB5Surcharge(String shipNumber);
	public void updateEstimateAndRevisionAB5SurchargeWithPercentage(String shipNumber,String workOrder,String fieldName,BigDecimal consumableFromPP);
	/**
	 * End
	 */
	public void updateAccountLineId(String shipNumber, Long serviceOrderId, String operationsIntelligenceIds,Long accountLineId);
	public double getHoursWithValues(String shipNumber, String workorder,String type);
}
