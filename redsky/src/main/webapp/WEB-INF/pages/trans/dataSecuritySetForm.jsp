<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title><fmt:message key="dataSecuritySetForm.title"/></title> 
<meta name="heading" content="<fmt:message key='dataSecuritySetForm.heading'/>"/>
<script type="text/javascript">

function onFormSubmit(theForm) {
	selectAll('filters');
	var name=document.forms['dataSecuritySetForm'].elements['dataSecuritySet.name'].value;
	
	if(name.trim()=='')
	{
		alert('Please enter the dataSecuritySet name');
		document.forms['dataSecuritySetForm'].elements['dataSecuritySet.name'].focus;
		return false;
	}
}


function copyToName()
{
	var name=document.forms['dataSecuritySetForm'].elements['dataSecuritySet.name'].value;
	var name1=document.forms['dataSecuritySetForm'].elements['tableName'].value;
	genName='DATA_SECURITY_SET_' +name1+'_';
	document.forms['dataSecuritySetForm'].elements['dataSecuritySet.name'].value=genName;
}
</script>
<style type="text/css">

table.pickList td select
{
width:600px;
}

table.pickList td button {
width: 45px;
</style>
 <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>
</head>

 <s:form id="dataSecuritySetForm" name="dataSecuritySetForm" action="saveDataSecuritySetForm" method="post" validate="true">
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <s:hidden name="dataSecuritySet.id"/>
 
 <div id="Layer1" style="width:100%"> 

		<div id="newmnav">
		  <ul>
		  	<li><a href="dataSecuritySetList.html"><span>DataSecurity List</span></a></li>
		  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>DataSecurity Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${dataSecuritySet.id}&tableName=datasecurityset,datasecuritypermission&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:2px; "><span></span></div>
   <div class="center-content">
		
<table class="" cellspacing="1" cellpadding="1" border="0" style="width:650px">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right">Select Filter</td>
		  		<td align="left"><s:select cssClass="list-menu" id="tableName" name="tableName" list="{'AGENT','SO_MODE','SO_ROUTING','SO_JOB','CF_BILLTOCODE','SO_BILLTOCODE','SO_BILLTOVENDORCODE','SO_INLANDCODE','NOTES_ACCOUNTPORTAL','NOTES_PARTNERPORTAL','SO_NETWORKSO','SO_COMPANYDIVISION','SO_RLOVENDORCODE','SO_SALESPORTAL','SO_BOOKINGAGENTCODE','CF_ORIGINDESTINATIONCOMPANYCODE'}" headerKey="" headerValue="" cssStyle="width:392px" onchange="copyToName();"/>
		  		<td align="right"><fmt:message key="dataSecuritySet.name"/><font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield name="dataSecuritySet.name"  maxlength="100" size="70" cssClass="input-text" readonly="false"/></td>
		  	</tr>
		  	
				<tr>
				  	<td align="right" valign="top"><fmt:message key="dataSecuritySet.description"/></td>
				 
		  			<td align="left" colspan="3"><s:textarea cssClass="textarea" name="dataSecuritySet.description"  rows="4" cols="115" readonly="false" cssStyle="width:804px;" /></td>
		  		</tr>
	  	
		</tbody>
	</table>
		  
		    </td>
       	  </tr>	
       	  
       	  <tr>
       	  <td align="left" class="listwhitetext">
       	 
			        <fieldset >
			            <legend>Assign Filters</legend>
			            <table class="pickList" >
			                <tr>
			                    <th class="pickLabel">
			                        <label class="required">Filter</label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required">Set</label>
			                    </th>
			                </tr>
			                <c:set var="leftList" value="${dataSecurityFilterList}" scope="request"/>
			                <s:set name="rightList" value="dataSecuritySet.filterList" scope="request"/>
			                <c:import url="/WEB-INF/pages/pickList.jsp">
			                    <c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="dataSecurityFilterList && !filters"/>
			                    <c:param name="rightId" value="filters"/>
			                </c:import>
			            </table>
			        </fieldset>
			  
			   
			    	
			    </td>
			</tr>  	
	 </tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="dataSecuritySetCreatedOnFormattedValue" value="${dataSecuritySet.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="dataSecuritySet.createdOn" value="${dataSecuritySetCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${dataSecuritySet.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty dataSecuritySet.id}">
								<s:hidden name="dataSecuritySet.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{dataSecuritySet.createdBy}"/></td>
							</c:if>
							<c:if test="${empty dataSecuritySet.id}">
								<s:hidden name="dataSecuritySet.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="dataSecuritySetupdatedOnFormattedValue" value="${dataSecuritySet.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="dataSecuritySet.updatedOn" value="${dataSecuritySetupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${dataSecuritySet.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty dataSecuritySet.id}">
								<s:hidden name="dataSecuritySet.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{dataSecuritySet.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty dataSecuritySet.id}">
								<s:hidden name="dataSecuritySet.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>



		  <table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save" onclick="return onFormSubmit(this.form)"/>  
        		</td>
       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
        		 		
        		<td align="right">
        		<c:if test="${not empty dataSecuritySet.id}">
		       <input type="button" class="cssbutton1" value="Add" onclick="location.href='<c:url value="/dataSecuritySetForm.html"/>'" />   
	            </c:if>
	            </td>
       	  	</tr>		  	
		  </tbody>
		  </table></td></tr></tbody></table></div>

</s:form>