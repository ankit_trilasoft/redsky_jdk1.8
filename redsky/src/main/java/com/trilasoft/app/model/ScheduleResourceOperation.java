package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@Entity
@Table ( name="scheduleresourceoperation")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class ScheduleResourceOperation extends BaseObject {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String corpID;
	private String crewName;
	private String ticket;
	private Date workDate;
	private String shipper;
	private Date beginDate;
	private String estimatedWeight;
	private String service;
	private String warehouse;
	private String description;
	private String localtruckNumber;
	private String timesheetId;
	private String truckingOpsId;
	private Long workTicketId;
	private String shipNumber;
    private String estimatedCubicFeet;
    private String crewType;
    private String originCity;
	private String destinationCity;
	private String actualWeight;
	private String actualVolume;
	private Boolean isDriverUsed;
	private String unit1;
	private String unit2;
	private String integrationTool;
	private String assignedCrewForTruck;
	/**
	 * Bug 13381 - Missing create/update fields for table "scheduleresourceoperation"
	 */
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("crewName", crewName)
				.append("ticket", ticket).append("workDate", workDate)
				.append("shipper", shipper).append("beginDate", beginDate)
				.append("estimatedWeight", estimatedWeight)
				.append("service", service).append("warehouse", warehouse)
				.append("description", description)
				.append("localtruckNumber", localtruckNumber)
				.append("timesheetId", timesheetId)
				.append("truckingOpsId", truckingOpsId)
				.append("workTicketId", workTicketId)
				.append("shipNumber", shipNumber)
				.append("estimatedCubicFeet", estimatedCubicFeet)
				.append("crewType", crewType).append("originCity", originCity)
				.append("destinationCity", destinationCity)
				.append("actualWeight", actualWeight)
				.append("actualVolume", actualVolume)
				.append("isDriverUsed", isDriverUsed).append("unit1", unit1)
				.append("unit2", unit2)
				.append("integrationTool", integrationTool)
				.append("assignedCrewForTruck", assignedCrewForTruck)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ScheduleResourceOperation))
			return false;
		ScheduleResourceOperation castOther = (ScheduleResourceOperation) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(crewName, castOther.crewName)
				.append(ticket, castOther.ticket)
				.append(workDate, castOther.workDate)
				.append(shipper, castOther.shipper)
				.append(beginDate, castOther.beginDate)
				.append(estimatedWeight, castOther.estimatedWeight)
				.append(service, castOther.service)
				.append(warehouse, castOther.warehouse)
				.append(description, castOther.description)
				.append(localtruckNumber, castOther.localtruckNumber)
				.append(timesheetId, castOther.timesheetId)
				.append(truckingOpsId, castOther.truckingOpsId)
				.append(workTicketId, castOther.workTicketId)
				.append(shipNumber, castOther.shipNumber)
				.append(estimatedCubicFeet, castOther.estimatedCubicFeet)
				.append(crewType, castOther.crewType)
				.append(originCity, castOther.originCity)
				.append(destinationCity, castOther.destinationCity)
				.append(actualWeight, castOther.actualWeight)
				.append(actualVolume, castOther.actualVolume)
				.append(isDriverUsed, castOther.isDriverUsed)
				.append(unit1, castOther.unit1).append(unit2, castOther.unit2)
				.append(integrationTool, castOther.integrationTool)
				.append(assignedCrewForTruck, castOther.assignedCrewForTruck)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(crewName)
				.append(ticket).append(workDate).append(shipper)
				.append(beginDate).append(estimatedWeight).append(service)
				.append(warehouse).append(description).append(localtruckNumber)
				.append(timesheetId).append(truckingOpsId).append(workTicketId)
				.append(shipNumber).append(estimatedCubicFeet).append(crewType)
				.append(originCity).append(destinationCity)
				.append(actualWeight).append(actualVolume).append(isDriverUsed)
				.append(unit1).append(unit2).append(integrationTool)
				.append(assignedCrewForTruck)
				.append(createdBy)
				.append(createdOn)
				.append(updatedBy)
				.append(updatedOn)
				.toHashCode();
	}
	@Id  @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column( length = 20 )
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column( length = 35 )
	public String getCrewName() {
		return crewName;
	}
	public void setCrewName(String crewName) {
		this.crewName = crewName;
	}
	@Column( length = 30 )
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	@Column
	public Date getWorkDate() {
		return workDate;
	}
	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}
	@Column( length = 30 )
	public String getShipper() {
		return shipper;
	}
	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	@Column
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	
	@Column( length = 30 )
	public String getEstimatedWeight() {
		return estimatedWeight;
	}
	public void setEstimatedWeight(String estimatedWeight) {
		this.estimatedWeight = estimatedWeight;
	}
	
	@Column( length = 30 )
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	
	@Column( length = 30 )
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	@Column( length = 30 )
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column( length = 20 )
	public String getLocaltruckNumber() {
		return localtruckNumber;
	}
	public void setLocaltruckNumber(String localtruckNumber) {
		this.localtruckNumber = localtruckNumber;
	}
	@Column( length = 20 )
	public String getTimesheetId() {
		return timesheetId;
	}
	public void setTimesheetId(String timesheetId) {
		this.timesheetId = timesheetId;
	}
	@Column( length = 20 )
	public String getTruckingOpsId() {
		return truckingOpsId;
	}
	public void setTruckingOpsId(String truckingOpsId) {
		this.truckingOpsId = truckingOpsId;
	}
	public String getEstimatedCubicFeet() {
		return estimatedCubicFeet;
	}
	public void setEstimatedCubicFeet(String estimatedCubicFeet) {
		this.estimatedCubicFeet = estimatedCubicFeet;
	}
	@Column( length = 2 )
	public String getCrewType() {
		return crewType;
	}
	public void setCrewType(String crewType) {
		this.crewType = crewType;
	}
	@Column( length = 20 )
	public Long getWorkTicketId() {
		return workTicketId;
	}
	public void setWorkTicketId(Long workTicketId) {
		this.workTicketId = workTicketId;
	}
	@Column( length = 50 )
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	
	@Column( length = 100 )
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	@Column( length = 100 )
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	public String getActualWeight() {
		return actualWeight;
	}
	public void setActualWeight(String actualWeight) {
		this.actualWeight = actualWeight;
	}
	public String getActualVolume() {
		return actualVolume;
	}
	public void setActualVolume(String actualVolume) {
		this.actualVolume = actualVolume;
	}
	public Boolean getIsDriverUsed() {
		return isDriverUsed;
	}
	public void setIsDriverUsed(Boolean isDriverUsed) {
		this.isDriverUsed = isDriverUsed;
	}
	public String getUnit1() {
		return unit1;
	}
	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}
	public String getUnit2() {
		return unit2;
	}
	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}
	public String getIntegrationTool() {
		return integrationTool;
	}
	public void setIntegrationTool(String integrationTool) {
		this.integrationTool = integrationTool;
	}
	public String getAssignedCrewForTruck() {
		return assignedCrewForTruck;
	}
	public void setAssignedCrewForTruck(String assignedCrewForTruck) {
		this.assignedCrewForTruck = assignedCrewForTruck;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
		
}
