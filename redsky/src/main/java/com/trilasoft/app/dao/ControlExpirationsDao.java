package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ControlExpirations;



public interface ControlExpirationsDao extends GenericDao<ControlExpirations, Long> {
	public List getDriverValidation(String driverCode,String corpId,String parentId);
}
