<%@ include file="/common/taglibs.jsp"%> 
<head>
	<title>SO/Partner Extracts</title>   
    <meta name="heading" content="SO/Partner Extracts"/> 
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	<script language="javascript" type="text/javascript">
	function goForExtract(){
		document.forms['utsiNavisionForm'].action = 'financePartnerDataExtractsList.html';
      	document.forms['utsiNavisionForm'].submit();
	}
	function soExtract(){
		if(document.forms['utsiNavisionForm'].elements['beginDate'].value==''){
	 		alert("Please enter the begin date"); 
	 		return false;
	 	}
	 	if(document.forms['utsiNavisionForm'].elements['endDate'].value==''){
	 		alert("Please enter the end date "); 
	 		return false;
	 	}
		document.forms['utsiNavisionForm'].action = 'serviceOrderDataExtractsAction.html';
      	document.forms['utsiNavisionForm'].submit();
	}
	function partnerExtract(){
		if(document.forms['utsiNavisionForm'].elements['pbeginDate'].value==''){
	 		alert("Please enter the begin date"); 
	 		return false;
	 	}
	 	if(document.forms['utsiNavisionForm'].elements['pendDate'].value==''){
	 		alert("Please enter the end date "); 
	 		return false;
	 	}
		document.forms['utsiNavisionForm'].action = 'financePartnerDataExtracts.html';
      	document.forms['utsiNavisionForm'].submit();
	}
</script>
	 
</head>
<body>
<s:form id="utsiNavisionForm" name="utsiNavisionForm" action="" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:95% " >
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top: 10px;!margin-top: -2px"><span></span></div>
   <div class="center-content">
   <table cellspacing="1" cellpadding="1" border="0" > 
<tr><td height="10px"></td></tr>
<tr>
<td width="10px;"></td>
<td class="bgblue" >SO Data Extracts</td>
</tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px" border="0">
<tr> 
  			
				<td class="listwhitetext" align="right">Begin&nbsp;Date<font color="red" size="2">*</font></td>
					<c:if test="${not empty beginDate}">
						<s:text id="beginDate" name="${FormDateValue}"><s:param name="value" value="beginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" value="%{beginDate}" size="8" maxlength="11" readonly="true" /> <img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty beginDate}">
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" required="true" size="8" maxlength="11" readonly="true" /> <img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				      <td width=""></td>	
				<td class="listwhitetext" align="right">End Date<font color="red" size="2">*</font></td>
		 			<c:if test="${not empty endDate}">
						<s:text id="endDate" name="${FormDateValue}"> <s:param name="value" value="endDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" size="8" maxlength="11" readonly="true" /><img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty endDate}" >
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" required="true" size="8" maxlength="11" readonly="true" /> <img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>	
				
			</tr>
<tr>
<td width="20px"></td>  
<td>
<input type="button" class="cssbutton" style="width:100px;" 
        			onclick="soExtract();"  
        			value="SO Extract"/> 
        			</td>
<td width="10px"></td>
 </tr> 
</table>
</td>

</tr></table>
<table cellspacing="1" cellpadding="1" border="0" > 
<tr><td height="10px"></td></tr>
<tr>
<td width="10px;"></td>
<td class="bgblue" >Partner Extracts</td>
</tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px" border="0">
<tr> 
  			
				<td class="listwhitetext" align="right">Begin&nbsp;Date<font color="red" size="2">*</font></td>
					<c:if test="${not empty pbeginDate}">
						<s:text id="pbeginDate" name="${FormDateValue}"><s:param name="value" value="pbeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="pbeginDate" name="pbeginDate" value="%{pbeginDate}" size="8" maxlength="11" readonly="true" /> <img id="pbeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty pbeginDate}">
						<td><s:textfield cssClass="input-text" id="pbeginDate" name="pbeginDate" required="true" size="8" maxlength="11" readonly="true" /> <img id="pbeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				      <td width=""></td>	
				<td class="listwhitetext" align="right">End Date<font color="red" size="2">*</font></td>
		 			<c:if test="${not empty pendDate}">
						<s:text id="pendDate" name="${FormDateValue}"> <s:param name="value" value="pendDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="pendDate" name="pendDate" value="%{pendDate}" size="8" maxlength="11" readonly="true" /><img id="pendDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty pendDate}" >
						<td><s:textfield cssClass="input-text" id="pendDate" name="pendDate" required="true" size="8" maxlength="11" readonly="true" /> <img id="pendDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>	
				
			</tr>
<tr>
<td width="20px"></td>  
<td>
<input type="button" class="cssbutton" style="width:100px;" 
        			onclick="partnerExtract();"  
        			value="Partner Extract"/> 
        			</td>

<td width="10px"></td>
   </tr> 
<!--<tr>     			
<td width="20px"></td>
<td><input type="button" class="cssbutton" style="width:165px; height:25px" 
        			onclick="location.href='<c:url value="/utsiPayableNavisionProcessing.html"/>'"  
        			value="Purchase Invoice Extract"/></td>
</tr>
--></table>
</td>

</tr></table>
</div>
<div class="bottom-header"><span></span></div> 

</div></div></div>
</s:form>
<script language="javascript" type="text/javascript">
if('${soextsize}'=='0'){
	alert("No Record Found To Extract");
}

</script>
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
</body> 