package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ExtractColumnMgmtDao;
import com.trilasoft.app.model.ExtractColumnMgmt;
import com.trilasoft.app.service.ExtractColumnMgmtManager;

public class ExtractColumnMgmtManagerImpl extends GenericManagerImpl<ExtractColumnMgmt, Long> implements ExtractColumnMgmtManager{
	ExtractColumnMgmtDao extractColumnMgmtDao;
	
	public ExtractColumnMgmtManagerImpl(ExtractColumnMgmtDao extractColumnMgmtDao) { 
        super(extractColumnMgmtDao); 
        this.extractColumnMgmtDao = extractColumnMgmtDao; 
    }
	public List getExtractColum(String reportName, String sessionCorpID) {
		
		return extractColumnMgmtDao.getExtractColum(reportName, sessionCorpID);
	}
	public List getSelectCondition(String selectCondition,String reportName,String sessionCorpID) { 
		return extractColumnMgmtDao.getSelectCondition(selectCondition,reportName,sessionCorpID);
	}
	
	public List getReportName(String corpid){
		return extractColumnMgmtDao.getReportName(corpid);
	}
	
	public List getTableName(String corpid){
		return extractColumnMgmtDao.getTableName(corpid);
	}
	
	public List getFieldNameList(String corpid){
		return extractColumnMgmtDao.getFieldNameList(corpid);
	}
	
	public List getDisplayNameList(String corpid){
		return extractColumnMgmtDao.getDisplayNameList(corpid);
	}
	
	public List getTableNameByReportName(String reportName, String corpid){
		return extractColumnMgmtDao.getTableNameByReportName(reportName, corpid);
	}
	
	public boolean checkExtractColumnMgmtRecord(String reportName, String tableName, String fieldName, String corpid){
		return extractColumnMgmtDao.checkExtractColumnMgmtRecord(reportName, tableName, fieldName, corpid);
	}
	
	public String checkExtractColumnMgmtRecordId(String reportName, String tableName, String fieldName, String corpid){
		return extractColumnMgmtDao.checkExtractColumnMgmtRecordId(reportName, tableName, fieldName, corpid);
	}
	
	public Long getExtractColMgmtSeqNo(String corpId){
		return extractColumnMgmtDao.getExtractColMgmtSeqNo(corpId);
	}
	
	public List searchFilterList(String reportName, String tableName, String fieldNameSearch, String displayName, String corpId, boolean tsftflag){
		return extractColumnMgmtDao.searchFilterList(reportName, tableName, fieldNameSearch, displayName, corpId, tsftflag);
	}
	
	public List searchDefaultList(String corpId){
		return extractColumnMgmtDao.searchDefaultList(corpId);
	}
	
	public List getCorpIdList(){
		return extractColumnMgmtDao.getCorpIdList();
	}
}
