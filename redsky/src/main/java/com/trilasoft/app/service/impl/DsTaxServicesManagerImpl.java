package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DsTaxServicesDao; 
import com.trilasoft.app.model.DsTaxServices;
import com.trilasoft.app.service.DsTaxServicesManager;

public class DsTaxServicesManagerImpl extends GenericManagerImpl<DsTaxServices, Long> implements DsTaxServicesManager {
	DsTaxServicesDao dsTaxServicesDao;
	public DsTaxServicesManagerImpl(DsTaxServicesDao dsTaxServicesDao) {
		super(dsTaxServicesDao); 
        this.dsTaxServicesDao = dsTaxServicesDao; 
	}
	public List getListById(Long id) {
		return dsTaxServicesDao.getListById(id);
	}

	

}
