<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>


<script language="JavaScript">
	function enableDevice(target){
		if(target.value=="MM"){
			document.getElementById("mmDeviceDiv").style.display = 'block';
		}else{
			document.getElementById("mmDeviceDiv").style.display = 'none';
		}
	}
	function check(clickType){
	
	if(document.forms['payrollform'].elements['payroll.userName'].value=="")
	{
		alert("User name is the required field");
		return false;
	}
	else{
		autoSave(clickType);
	}
	
	
}
	function checkNumber()
	{   
		var i;
		var s = document.forms['payrollform'].elements['payroll.paidSickLeave'].value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        
	        if ((c < '0') || (c > '9')) 
	        {
	        	alert('Please enter integer value for Paid Sick Leave!');
		        document.forms['payrollform'].elements['payroll.paidSickLeave'].value='';
		        document.forms['payrollform'].elements['payroll.paidSickLeave'].focus();
		        return false;
	        }
	    }
	    return true;
	}

function chkSelect()
    {
        	if (checkFloat('payrollform','payroll.sickHours','Invalid data in Sick Hours') == false)
           {
              document.forms['payrollform'].elements['payroll.sickHours'].value='0';
              return false
           }
           if (checkFloat('payrollform','payroll.sickUsed','Invalid data in Sick Used') == false)
           {
              document.forms['payrollform'].elements['payroll.sickUsed'].value='0';
              return false
           }
           
           if (checkFloat('payrollform','payroll.vacationHrs','Invalid data in Vacation Hrs') == false)
           {
              document.forms['payrollform'].elements['payroll.vacationHrs'].value='0';
              return false
           }
           
           if (checkFloat('payrollform','payroll.vacationUsed','Invalid data in Vacation Used') == false)
           {
              document.forms['payrollform'].elements['payroll.vacationUsed'].value='0';
              return false
           }
           if (checkFloat('payrollform','payroll.personalDayUsed','Invalid data in personal Day Used') == false)
           {
              document.forms['payrollform'].elements['payroll.personalDayUsed'].value='0';
              return false
           }
           if (checkNumber('payrollform','payroll.paidSickLeave','Please enter integer value for Paid Sick Leave!') == false)
           {
              document.forms['payrollform'].elements['payroll.paidSickLeave'].value='';
              document.forms['payrollform'].elements['payroll.paidSickLeave'].focus();
              return false
           }
                     
 }

	function changeStatus(){
	document.forms['payrollform'].elements['formStatus'].value = '1';
     }
     
	function autoSave(clickType){
	
	if(!(clickType == 'save')){
	
	if ('${autoSavePrompt}' == 'No'){

			var noSaveAction = '<c:out value="${payroll.id}"/>';

			var id1 = document.forms['payrollform'].elements['payroll.id'].value;

			if(document.forms['payrollform'].elements['gotoPageString'].value == 'gototab.payrollList'){

			noSaveAction = 'payrolls.html';

			}
			
		processAutoSave(document.forms['payrollform'], 'savepayroll!saveOnTabChange.html', noSaveAction);

		}else{
	
	var id1 = document.forms['payrollform'].elements['payroll.id'].value;
	
	if (document.forms['payrollform'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the Payroll Form");
		if(agree){
			document.forms['payrollform'].action = 'savepayroll!saveOnTabChange.html';
			document.forms['payrollform'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['payrollform'].elements['gotoPageString'].value == 'gototab.payrollList'){
				location.href = 'payrolls.html';
				}
			
					
		}
		}
	}else{
	if(id1 != ''){
		if(document.forms['payrollform'].elements['gotoPageString'].value == 'gototab.payrollList'){
				location.href = 'payrolls.html';
				}
			
			
	}
	}
}
} 
} 

function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;

	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36); 
	}

function notExists(){
	alert("The Payroll information has not been saved yet, please save Payroll information to continue");
}  

function onlyFloat(targetElement)
{   
	var i;
	var s = targetElement.value.trim();
	var count = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if (((c < '0') || (c > '9')) && (c != '.') || (count>'1')) 
        {
        	alert("Please enter an appropriate value for Pay/Hour!");
	        document.getElementById(targetElement.id).value=0.0;
            document.getElementById(targetElement.id).select();
	        return false;
        }
    }
	document.forms['payrollform'].elements['payroll.payhour'].value=document.forms['payrollform'].elements['payroll.payhour'].value.trim();
    return true;
}
function findDriverFromPartner(){
	 openWindow('crewDriversList.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=payroll.partnerName&fld_code=payroll.partnerCode&fld_ninthDescription=ninthDescription&fld_eigthDescription=eightDescription');
	 }
function checkDriverId(){
	var driverCode=document.forms['payrollform'].elements['payroll.partnerCode'].value;
	if(driverCode!=''){
		//var driverCode=target.value;
		driverCode=driverCode.trim();
        var url="checkDriverIdAjax.html?ajax=1&decorator=simple&popup=true&driverCode="+driverCode;
	    http1987.open("GET", url, true);
	    http1987.onreadystatechange = handleHttpDriverCode;
	    http1987.send(null); 

	}else{
		document.forms['payrollform'].elements['payroll.partnerName'].value="";
	}
}
function handleHttpDriverCode(){
	if (http1987.readyState == 4){
		var results = http1987.responseText
	     results = results.trim(); 
	     if(results!=''){
	    	 document.forms['payrollform'].elements['payroll.partnerName'].value=results;
	     }else{
		     alert("Please Enter Valid Partner Code.");
		     document.forms['payrollform'].elements['payroll.partnerCode'].value="";
		     document.forms['payrollform'].elements['payroll.partnerName'].value="";
	     }
	}
}
var http1987 = getHTTPObject();


function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
	}

	function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
		lastName=lastName.replace("~","'");
		document.getElementById(partnerNameId).value=lastName;
		document.getElementById(paertnerCodeId).value=partnercode;
		document.getElementById(autocompleteDivId).style.display = "none";	
		if(paertnerCodeId=='payrollPartnerCodeId'){
			checkDriverId();
		}
	}
</script>



	<meta name="heading" content="<fmt:message key='payrollForm.title'/>"/> 
	<title><fmt:message key="payrollForm.title"/></title>	
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>	
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
<!-- Modification closed here -->
	
	<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
	<script type="text/javascript">

animatedcollapse.addDiv('leave', 'fade=1,show=1')
animatedcollapse.init()

</script>
	
</head>

<body>
<s:form cssClass="form_magn" id="payrollform" name="payrollform" action="savepayroll" onsubmit="return  checkNumber();" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="payroll.id"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.payrollList' }">
    <c:redirect url="/payrolls.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>



<c:set var="idOfWhom" value="${payroll.id}" scope="session"/>
<c:set var="noteID" value="${payroll.id}" scope="session"/>
<c:set var="noteFor" value="Crew" scope="session"/>
<c:set var="idOfTasks" value="" scope="session"/>
<c:if test="${empty payroll.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty payroll.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>

<s:hidden id="countPayNotes" name="countPayNotes" value="<%=request.getParameter("countPayNotes") %>"/>
<c:set var="countPayNotes" value="<%=request.getParameter("countPayNotes") %>" />

<div id="Layer1" style="width:100%" onkeydown="changeStatus();">
<div id="newmnav" class="nav_tabs"><!-- sandeep -->
		  <ul>   
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Personnel Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		   <li><a onclick="setReturnString('gototab.payrollList');return check('none');" onmouseover="return chkSelect();" ><span>Personnel List</span></a></li>
		 <li><a onclick="window.open('auditList.html?id=${payroll.id}&tableName=crew&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')">
              <span>Audit</span></a></li>
		 </ul>
		</div><div class="spn">&nbsp;</div>
	
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" ><span></span></div>
   <div class="center-content">
  	<table class="listwhitetext" border="0" width="100%" >
		  <tbody>  	
		  	<tr><td align="left" height="3px"></td></tr>
		  	
		  		<tr>
			  		<td align="right"><fmt:message key="payrollform.userName"/><font color="red" size="2">*</font></td>
			  		<td align="left" colspan="2"><s:textfield name="payroll.userName"  maxlength="25" size="16"  cssClass="input-text" readonly="false" tabindex="1" cssStyle="width:100px;"/></td>
			  		 <configByCorp:fieldVisibility componentId="component.field.Alternative.CrewType">
			  		<td ><s:radio name="payroll.crewGroup" list="'HHG'" onchange="changeStatus();"/>
		  	        <s:radio name="payroll.crewGroup"  list="'Commercial'" onchange="changeStatus();"/></td>	  		
			  		</configByCorp:fieldVisibility>
			  		<td align="right"><fmt:message key="payrollform.companyDivision"/></td>
		  			<td align="left" colspan="2"><s:select cssClass="list-menu"   name="payroll.companyDivision" headerKey=" " headerValue=" " list="%{compDevision}" cssStyle="width:200px;" onchange="changeStatus();" tabindex="2"/></td> 
			  		<td></td>			  		
			  		<td align="left" colspan="3" style="padding-left:35px;">
			  		<table class="detailTabLabel" border="0">
			  		<tr>
			  		<td><s:checkbox key="payroll.active" fieldValue="true" onchange="changeStatus();" tabindex="3"/></td>
			  		<td><fmt:message key="payrollform.active"/></td>
			  		<td><s:checkbox key="payroll.ignoreForTimeSheet" fieldValue="true" onchange="changeStatus();" tabindex="3"/></td>
			  		<td>Ignore&nbsp;For&nbsp;TimeSheet</td>		
			  		<td><s:checkbox key="payroll.contractor" fieldValue="true" onchange="changeStatus();" tabindex="3"/></td>
                    <td>Contractor</td>			  			  		
			  		</tr>
			  		</table>
			  		</td>
	  				<c:if test="${empty payroll.id}">
					<td align="right" width="25"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
					</c:if>
					<c:if test="${not empty payroll.id}">
					<c:choose>
					<c:when test="${countPayNotes == '0' || countPayNotes == '' || countPayNotes == null}">
					<td align="right" width="25" class="listwhitetext" ><img id="countPayNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${payroll.id}&notesId=${payroll.id}&noteFor=Crew&subType=Crew&imageId=countPayNotesImage&fieldId=countPayNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${payroll.id}&notesId=${payroll.id}&noteFor=Crew&subType=Crew&imageId=countPayNotesImage&fieldId=countPayNotes&decorator=popup&popup=true',800,600);" ></a></td>
					</c:when>
					<c:otherwise>
					<td align="right" width="25" class="listwhitetext" ><img id="countPayNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${payroll.id}&notesId=${payroll.id}&noteFor=Crew&subType=Crew&imageId=countPayNotesImage&fieldId=countPayNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${payroll.id}&notesId=${payroll.id}&noteFor=Crew&subType=Crew&imageId=countPayNotesImage&fieldId=countPayNotes&decorator=popup&popup=true',800,600);" ></a></td>
					</c:otherwise>
					</c:choose> 
					</c:if>
					
		   </tr>
		  	
		  	<tr>
		  		<td align="right"><fmt:message key="payrollform.firstName"/></td>
		  		<td align="left" colspan="3"><s:textfield name="payroll.firstName"  maxlength="25" size="16"  onkeydown="return onlyCharsAllowed(event)" cssClass="input-text" readonly="false" tabindex="4" cssStyle="width:100px;"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="payrollform.initial"/>&nbsp;
		  		
		  		<s:textfield name="payroll.initial"  maxlength="10" size="2" onkeydown="return onlyCharsAllowed(event)" cssClass="input-text" readonly="false" tabindex="5"/>
		  		 <configByCorp:fieldVisibility componentId="component.field.Alternative.CrewType">
		  		&nbsp;&nbsp;Total ManPower
                <s:textfield cssStyle="text-align:left; width:63px" name="payroll.totalManPower" size="12" cssClass="input-text" maxlength="7" onchange="onlyNumeric(this);"  />
              	</configByCorp:fieldVisibility>
              	<td align="right"><fmt:message key="payrollform.lastName"/></td>
		  		<td align="left" colspan="2"><s:textfield name="payroll.lastName"  maxlength="25" size="29" onkeydown="return onlyCharsAllowed(event)"  cssClass="input-text" readonly="false" tabindex="6" cssStyle="width:196px;"/></td>		  		 
		  		 <td></td>
		  		<td align="right"><fmt:message key="payrollform.employeeId"/></td>
		  		<td align="left" colspan="2"><s:textfield name="payroll.employeeId"  maxlength="25" size="16"  cssClass="input-text"  readonly="false" tabindex="7" cssStyle="width:104px;"/></td>
		  		
		  	</tr>
		  	    
		  		<tr>		  		
		  		<td align="right" ><fmt:message key="payrollform.title"/></td>
		  		<td align="left" colspan="3"><s:textfield name="payroll.title"   maxlength="50" size="16" onkeydown="return onlyCharsAllowed(event)" cssClass="input-text" readonly="false" tabindex="8" cssStyle="width:100px;"/></td>
		  		<td align="right"><fmt:message key="payrollform.typeofWork"/></td>
		  		<td align="left" colspan="2"><s:select cssClass="list-menu"  list="%{typeofWork}" headerKey=" " headerValue=" " name="payroll.typeofWork" cssStyle="width:200px;" onchange="changeStatus();" tabindex="9"/></td> 		  		
		  		
		  		 <td></td>
		  		<td align="right"><fmt:message key="payrollform.classificationEffective"/></td>
		  		<c:if test="${not empty payroll.classificationEffective}">
				<s:text id="classificationEffectiveFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.classificationEffective"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="classificationEffective" name="payroll.classificationEffective" value="%{classificationEffectiveFormattedValue}"  maxlength="11" size="7"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true" cssStyle="width:80px;"/></td>
		  		</c:if>
		  		<c:if test="${empty payroll.classificationEffective}">
				<s:text id="payroll.classificationEffectiveFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.classificationEffective"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="classificationEffective" name="payroll.classificationEffective"  maxlength="11" size="12"  cssClass="input-text"  onkeydown="return onlyDel(event, this)" readonly="true"/></td>
		  		</c:if>
		  		<td align="left" width="150"><img id="classificationEffective_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  	</tr>
		  	
		  	    
		  	   
		  	    <tr>
		  	   
		  		<td align="right"><fmt:message key="payrollform.rank"/></td>
		  		<td align="left" colspan="2"><s:textfield cssStyle="text-align:right; width:100px;" name="payroll.rank"  maxlength="5"size="16" onkeydown="return onlyNumsAllowed(event)" cssClass="input-text" readonly="false" tabindex="10" /></td>
		  		 <td></td>
		  		<td align="right"><fmt:message key="payrollform.warehouse"/></td>
		  		<td align="left" colspan="2"><s:select cssClass="list-menu"   name="payroll.warehouse" headerKey=" " headerValue=" " list="%{house}" cssStyle="width:200px;" onchange="changeStatus();" tabindex="11"/></td> 
		  		 <td></td>
		  		<td align="right"><fmt:message key="payrollform.payhour"/></td>
		  		<td align="left" colspan="2"><s:textfield name="payroll.payhour" cssStyle="text-align:right; width:104px;"  maxlength="10" size="16"  onchange="onlyFloat(this);" cssClass="input-text" readonly="false" tabindex="12"/></td>
		  		
		  	</tr> 
		  	
			<tr>
		  		<td align="right"><fmt:message key="payrollform.info"/></td>
		  		<td align="left" colspan="2"><s:textfield name="payroll.info"  maxlength="100" size="16" onkeydown="return onlyCharsAllowed(event)" cssClass="input-text" readonly="false" tabindex="13" cssStyle="width:100px;"/></td>
		  		 <td></td>
		  		<td align="right"><fmt:message key="payrollform.branch"/></td>
		  		<td align="left" colspan="2"><s:textfield name="payroll.branch"  maxlength="25" size="16" onkeydown="return onlyCharsAllowed(event)" cssClass="input-text" readonly="false" tabindex="14" cssStyle="width:105px;"/></td>
		  		<td></td>
		  		<td align="right"><fmt:message key="payrollform.department"/></td>
		  		<td align="left" colspan="2"><s:select cssClass="list-menu"   name="payroll.department" headerKey=" " headerValue=" " list="%{dept}" cssStyle="width:108px;" onchange="changeStatus();" tabindex="15"/></td>
		  		
		  	</tr> 
             <tr>
		  		<td align="right"><fmt:message key="payrollform.officePhone"/></td>
		  		<td align="left" colspan="2"><s:textfield name="payroll.officePhone"  maxlength="50" size="16" onkeydown="return onlyNumsAllowed(event)" cssClass="input-text" readonly="false" tabindex="16" cssStyle="width:100px;"/></td>		  		 
		  		 <td></td>
		  		<td align="right"><fmt:message key="payrollform.officeFax"/></td>
		  		<td align="left" colspan="3">
		  		<table style="margin:0px;padding:0px;" cellspacing="0">
		  		<tr>
		  		<td align="left" colspan="2"><s:textfield name="payroll.officeFax"  maxlength="25" size="16" onkeydown="return onlyNumsAllowed(event)" cssClass="input-text" readonly="false" tabindex="17" cssStyle="width:104px;"/></td>
		  		<td align="right" width="35" style="padding-left:27px;">Email&nbsp;</td>
		  		<td align="left"><s:textfield name="payroll.crewEmail"  maxlength="82" size="30" cssClass="input-text" readonly="false" tabindex="17"/></td>
		  		</tr>
		  		</table>
		  		</td>		  		
		  		 <configByCorp:fieldVisibility componentId="component.field.Alternative.LicenceNumber">
		  		<td align="right">License Class</td>
		  		<td align="left" colspan="2"><s:textfield name="payroll.licenceNumber"  maxlength="25" size="16" onkeydown="" cssClass="input-text" readonly="false" tabindex="17"/></td> 
		  		</configByCorp:fieldVisibility>	
		  	</tr>
		  	
		  	<tr>
		  	 <td align="right"><fmt:message key="payrollform.overtimetype"/></td>
		  		<td align="left" colspan="2"><s:select cssClass="list-menu"  list="%{labor}" name="payroll.overtimetype" cssStyle="width:104px;" headerKey="" headerValue="" onchange="changeStatus();getContract(this);" tabindex="18"/></td> 
		  		<td></td>
		  		<td align="right">VL Code</td>
		  		<td align="left" colspan="2"><s:textfield name="payroll.vlCode" size="16" maxlength="100" cssClass="input-text"  tabindex="19" cssStyle="width:104px;"/></td>
		  		<td></td>
		  		<td align="right"><fmt:message key="payrollform.drivingClass"/></td>
		  		<td align="left" colspan="2"><s:select cssClass="list-menu"  list="%{drvr_cls}"  headerKey=" " headerValue=" " name="payroll.drivingClass" cssStyle="width:108px;" onchange="changeStatus();" tabindex="19"/></td> 
		  	 </tr>
		  	 
		  	  <tr>
		  		<td align="right" width="70px"><s:checkbox key="payroll.usCitizen" fieldValue="true" onchange="changeStatus();" tabindex="20"/></td>
		  		<td align="left" colspan="2" ><fmt:message key="payrollform.usCitizen"/></td>
		  		<td></td>
		  		<td align="right"><fmt:message key="payrollform.hired"/></td>
		  		<c:if test="${not empty payroll.hired}">
				<s:text id="hiredFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.hired"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="hired" name="payroll.hired" value="%{hiredFormattedValue}"  maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true" cssStyle="width:80px;"/></td>
		  		</c:if>
		  		<c:if test="${empty payroll.hired}">
				<s:text id="payroll.hiredFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.hired"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="hired" name="payroll.hired"  maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true"/></td>
		  		</c:if>
             
		  		<td  align="left" width="190">		  		
		  		<table class="detailTabLabel" cellspacing="0" cellpadding="0">
		  		<tr>
		  		<td>
		  		<img id="hired_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
		  		<td>
		  		<td style="padding-left:23px; padding-right:6px">Tenure</td>
		  		<td><s:textfield name="payroll.tenure"  maxlength="25" size="8"  cssClass="input-textUpper" cssStyle="text-align:right" readonly="true" tabindex="22"/>
		  		</td>
		  		</tr>
		  		</table>
		  		</td>  	
		  	    <td align="right" colspan="2" ><fmt:message key="payrollform.cdl"/></td>
		  		<td align="left" colspan="3"><s:textfield name="payroll.cdl"  maxlength="25" size="16"  cssClass="input-text" readonly="false" tabindex="23" cssStyle="width:104px;"/></td>
		  	
		  	</tr>
		  	    
		  	  <tr> 	 
				<td align="right" class="listwhitetext">Partner&nbsp;Code</td>
				<td align="left" colspan="3">
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
				<tr>
				<td align="left" class="listwhitetext" valign="top" width="60px"><s:textfield id="payrollPartnerCodeId" name="payroll.partnerCode" maxlength="8" required="true" cssClass="input-text" size="13" tabindex="24" onchange="checkDriverId();" cssStyle="width:83px;"/></td>
				<td align="left" width="4px"><img class="openpopup" width="17" height="20" onclick="findDriverFromPartner();" src="<c:url value='/images/open-popup.gif'/>" /></td>
				<td align="right" class="listwhitetext">&nbsp;Partner&nbsp;Name&nbsp;</td> 
				<td align="left" class="listwhitetext" valign="top"><s:textfield id="payrollPartnerNameId" name="payroll.partnerName" maxlength="30"   onkeyup="findPartnerDetails('payrollPartnerNameId','payrollPartnerCodeId','payrollPartnerNameDivId',' and (isOwnerOp is true OR isVendor is true OR isCarrier is true)','',event);"  cssClass="input-text" size="17" />
				<div id="payrollPartnerNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</td>
				</tr>
				</table>
		  		</td> 
		  		<td align="right"><fmt:message key="payrollform.unionName"/></td>
		  		<c:if test="${not empty payroll.unionName}">
				<s:text id="unionNameFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.unionName"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="unionName" name="payroll.unionName" value="%{unionNameFormattedValue}"   maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true" cssStyle="width:80px;"/></td>
		  		</c:if>
		  		<c:if test="${empty payroll.unionName}">
				<s:text id="payroll.unionNameFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.unionName"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="unionName" name="payroll.unionName"  maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true"/></td>
		  		</c:if>
		  		<td  align="left">		  		
		  		<table class="detailTabLabel" cellspacing="0" cellpadding="0">
		  		<tr>		  		
		  		<td align="left"><img id="unionName_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
		  		</td>
		  		<td style="padding-left:8px;padding-right:2px;">Last Anniv</td>
		  		<c:if test="${not empty payroll.lastAnniversaryDate}">
		  		<td>
		  		<s:text id="lastAnniversaryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.lastAnniversaryDate"/></s:text>
		  		<s:textfield id="lastAnniversaryDate" name="payroll.lastAnniversaryDate" value="%{lastAnniversaryDateFormattedValue}" cssClass="input-textUpper" cssStyle="text-align:right" size="8"  readonly="true" /> 
		  		</td>
		  		</c:if>
		  		<c:if test="${empty payroll.lastAnniversaryDate}">
		  		<td>
		  		<s:text id="lastAnniversaryDate" name="${FormDateValue}"><s:param name="value" value="payroll.lastAnniversaryDate"/></s:text>
		  		<s:textfield id="lastAnniversaryDate" name="payroll.lastAnniversaryDate" cssClass="input-textUpper" tabindex="25" cssStyle="text-align:right" size="8" readonly="true" /> 
		  		</td>
		  		</c:if>
		  		</tr>
		  		</table>
		  		</td>
		  		
		  		<td align="right" colspan="2"><fmt:message key="payrollform.cdlExpires"/></td>
		  		<c:if test="${not empty payroll.cdlExpires}">
				<s:text id="cdlExpiresFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.cdlExpires"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="cdlExpires" name="payroll.cdlExpires" value="%{cdlExpiresFormattedValue}"  maxlength="10" size="7"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true" cssStyle="width:80px;"/></td>
		  		</c:if>
		  		<c:if test="${empty payroll.cdlExpires}">
				<s:text id="payroll.cdlExpiresFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.cdlExpires"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="cdlExpires" name="payroll.cdlExpires"  maxlength="10" size="12"  cssClass="input-text"  onkeydown="return onlyDel(event, this)" readonly="true"/></td>
		  		</c:if>

		  		<td align="left"><img id="cdlExpires_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td> 
		  		
		  	</tr>
		  		<tr>	
		  		<td align="right"><fmt:message key="payrollform.socialSecurityNumber"/></td>
		  		<td align="left" colspan="2"><s:textfield name="payroll.socialSecurityNumber" cssStyle="text-align:right; width:100px;"  maxlength="25" size="16" onkeydown="return onlyNumsAllowed(event)" cssClass="input-text" readonly="false" tabindex="27"/></td>
		  	      <td></td>
		  	    <td align="right"><fmt:message key="payrollform.beginHealthWelf"/></td>
		  		<c:if test="${not empty payroll.beginHealthWelf}">
				<s:text id="beginHealthWelfFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.beginHealthWelf"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="beginHealthWelf" name="payroll.beginHealthWelf" value="%{beginHealthWelfFormattedValue}"  maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)"  readonly="true" cssStyle="width:80px;"/></td>
		  		</c:if>
		  		<c:if test="${empty payroll.beginHealthWelf}">
				<s:text id="payroll.beginHealthWelfFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.beginHealthWelf"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="beginHealthWelf" name="payroll.beginHealthWelf"  maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)"  readonly="true"/></td>
		  		</c:if>
		  		<td align="left"><img id="beginHealthWelf_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td> 	   
		  	    	  
		  	  <td></td>
		  	  
		  		<td align="right"><fmt:message key="payrollform.suspended"/></td>
		  		<c:if test="${not empty payroll.suspended}">
				<s:text id="suspendedFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.suspended"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="suspended" name="payroll.suspended" value="%{suspendedFormattedValue}"  maxlength="11" size="7"   cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true" cssStyle="width:80px;"/></td>
		  		</c:if>
		  		<c:if test="${empty payroll.suspended}">
				<s:text id="payroll.suspendedFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.suspended"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="suspended" name="payroll.suspended"  maxlength="11" size="12"  cssClass="input-text"  onkeydown="return onlyDel(event, this)" readonly="true"/></td>
		  		</c:if>
		  		<td align="left"><img id="suspended_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>	  		
		  		
		  	</tr> 
		  
		  	 <tr>
		  	 
		  		<td align="right"><fmt:message key="payrollform.greenCard"/></td>
		  		<td align="left" colspan="2"><s:textfield name="payroll.greenCard"   maxlength="25" size="16"  cssClass="input-text" readonly="false" tabindex="30" cssStyle="width:100px;"/></td>
		  		 <td></td>
		  		<td align="right"><fmt:message key="payrollform.terminatedDate"/></td>
		  		<c:if test="${not empty payroll.terminatedDate}">
				<s:text id="terminatedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.terminatedDate"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="terminatedDate" name="payroll.terminatedDate" value="%{terminatedDateFormattedValue}"  maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true" cssStyle="width:80px;"/></td>
		  		</c:if>
		  		<c:if test="${empty payroll.terminatedDate}">
				<s:text id="payroll.terminatedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.terminatedDate"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="terminatedDate" name="payroll.terminatedDate"  maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true"/></td>
		  		</c:if>

		  		<td align="left"><img id="terminatedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		<td></td>
		  		<td align="right"><fmt:message key="payrollform.doTPhysical"/></td>
		  		<c:if test="${not empty payroll.doTPhysical}">
				<s:text id="doTPhysicalFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.doTPhysical"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="doTPhysical" name="payroll.doTPhysical" value="%{doTPhysicalFormattedValue}"  maxlength="10" size="7"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true" cssStyle="width:80px;"/></td>
		  		</c:if>
		  		<c:if test="${empty payroll.doTPhysical}">
				<s:text id="payroll.doTPhysicalFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.doTPhysical"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="doTPhysical" name="payroll.doTPhysical"  maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true"/></td>
		  		</c:if>

		  		<td align="left"><img id="doTPhysical_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		
		  	</tr> 
		  	
		  	<tr>
		  		
		  		<td align="right"><fmt:message key="payrollform.expiration"/></td>
		  		<c:if test="${not empty payroll.expiration}">
					<s:text id="expirationFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.expiration"/></s:text>
		  			<td align="left" width="20px"><s:textfield id="expiration" name="payroll.expiration" value="%{expirationFormattedValue}"  maxlength="10" size="16"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true"/></td>
		  		</c:if>
		  		<c:if test="${empty payroll.expiration}">
					<s:text id="payroll.expirationFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.expiration"/></s:text>
		  			<td align="left" width="20px"><s:textfield id="expiration" name="payroll.expiration"  maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true" cssStyle="width:76px;"/></td>
		  		</c:if>
		  		<td align="left" width="150"><img id="expiration_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td> 
		  		<td></td>
		  		<td align="right"><fmt:message key="payrollform.begginingPension"/></td>
		  		<c:if test="${not empty payroll.begginingPension}">
				<s:text id="begginingPensionFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.begginingPension"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="begginingPension" name="payroll.begginingPension"  value="%{begginingPensionFormattedValue}"  maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true" cssStyle="width:80px;"/></td>
		  		</c:if>
		  		<c:if test="${empty payroll.begginingPension}">
				<s:text id="payroll.begginingPensionFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.begginingPension"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="begginingPension" name="payroll.begginingPension"  maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true"/></td>
		  		</c:if>

		  		<td align="left"><img id="begginingPension_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		<td></td>
		  		<td align="right"><fmt:message key="payrollform.licenceReview"/></td>
		  		<c:if test="${not empty payroll.licenceReview}">
				<s:text id="licenceReviewFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.licenceReview"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="licenceReview" name="payroll.licenceReview"  value="%{licenceReviewFormattedValue}"  maxlength="10" size="7"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true" cssStyle="width:80px;"/></td>
		  		</c:if>
		  		<c:if test="${empty payroll.licenceReview}">
				<s:text id="payroll.licenceReviewFormattedValue" name="${FormDateValue}"><s:param name="value" value="payroll.licenceReview"/></s:text>
		  		<td align="left" width="20px"><s:textfield id="licenceReview" name="payroll.licenceReview"  maxlength="10" size="12"  cssClass="input-text" onkeydown="return onlyDel(event, this)" readonly="true"/></td>
		  		</c:if>

		  		<td align="left"><img id="licenceReview_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  		
		  </tr> 
		  <tr>
		   <configByCorp:fieldVisibility componentId="component.field.Alternative.Supervisor">
		  		<td align="right">Supervisor</td>
		  		<td align="left" colspan="2"><s:select cssClass="list-menu"  list="%{supervisorList}"  headerKey=" " headerValue=" " name="payroll.supervisorName" cssStyle="width:105px;" onchange="changeStatus();" tabindex="31"/></td>		  		
		  	</configByCorp:fieldVisibility>	
		  </tr>
		  <c:if test="${not empty integrationToolMap}">
		  <tr> 
		  	<td align="right">Integration&nbsp;Tool</td> 
		  	<td align="left" colspan="3"><s:select cssClass="list-menu"   name="payroll.integrationTool" headerKey=" " headerValue=" " list="%{integrationToolMap}" cssStyle="width:104px;" onchange="changeStatus();enableDevice(this)" tabindex="32"/></td> 
	  		<td align="right">Password</td>
		  		<td align="left" colspan="2"><s:textfield name="payroll.integrationPassword"  maxlength="45" size="16"  cssClass="input-text" tabindex="33" cssStyle="width:104px;"/></td>
	  		<td colspan="4">
	  		<table id = "mmDeviceDiv" style="padding-left:177px; margin:0px;">
	  		<tr>
	  		<td align="right">MM Device</td>
	  		<td align="left" ><s:select cssClass="list-menu"   name="payroll.deviceNumber" headerKey=" " headerValue=" " list="%{mmDevice}" cssStyle="width:100px;" onchange="changeStatus();" tabindex="34"/></td> 
		 	</tr>
		 	</table>
		 	</td>
		  </tr>	
		  </c:if>
		  <tr>
			<td height="10" align="left" class="listwhitetext" colspan="15">
			 <div  onClick="javascript:animatedcollapse.toggle('leave')" style="margin: 0px"  >
				<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
				<tr>
				<td class="headtab_left">
				</td>
				<td class="headtab_center" >&nbsp;Leave Management
				</td>
				<td width="28" valign="top" class="headtab_bg"></td>
				<td class="headtab_bg_special" >&nbsp;
				</td>
				<td class="headtab_right">
				</td>
				</tr>
				</table>
				</div>		
							
		<div  id="leave">
		<table width="100%" cellpadding="1" cellspacing="1" class="detailTabLabel" border="0">
	      <tr height="5px"></tr>
	      <tr>
	       <td align="right">Paid Sick Leave</td>
		   <td align="left"><s:textfield name="payroll.paidSickLeave" cssStyle="text-align:right"  maxlength="5" size="4" onkeydown="return onlyNumsAllowed(event)"  cssClass="input-text" readonly="false" tabindex="36"/></td>
	      </tr>
	      <tr>
	      <td align="right">Sick Carried Over</td>
		  	<td align="left"><s:textfield name="payroll.sickCarriedOver" cssStyle="text-align:right"  maxlength="5" size="4" onchange="onlyFloat(this);" cssClass="input-text" readonly="false" tabindex="37"/></td>
		     <td></td>
		     <td></td>
		     <td></td>
		     <td></td>
		  <td align="right">Carry Over Allowed</td>
		  <td align="left"><s:textfield name="payroll.carryOverAllowed" cssStyle="text-align:right"  maxlength="5" size="4" onchange="onlyFloat(this);" cssClass="input-text" readonly="false" tabindex="38"/></td>
		     	
		    <td></td>
		    <td></td>
	      </tr>
	      
		  <tr>
		  <td align="right" width="12%"><fmt:message key="payrollform.sickHours"/></td>
		  <td align="left" colspan="2" width="18%"><s:textfield name="payroll.sickHours" cssStyle="text-align:right"   maxlength="5" size="4" onchange="onlyFloat(this);" onblur="numKey()" cssClass="input-text" readonly="false" tabindex="39"/></td>
		  <td></td>
		  <td colspan="2" align="left">
		  <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin:0px;padding:0px;" >
			<tr>
			<td width="40">
			</td>
			<td >Allowed Personal Hours 8
			</td>
			</tr>
			</table>
		  </td>
		  	<td align="right" width="15%"><fmt:message key="payrollform.vacationHrs"/></td>
		  	<td align="left"><s:textfield name="payroll.vacationHrs" cssStyle="text-align:right"  maxlength="5" size="4" onchange="onlyFloat(this);" cssClass="input-text" readonly="false" tabindex="40"/></td>
		  	<td></td>
		  	<td></td>
		  </tr>
		  
		   <tr>
		  <td colspan="10" height="1px" align="center">
		  </td>
		  </tr>
		  <tr>
		  <td colspan="10" height="15px" align="center" class="vertlinedata" style="padding-bottom:2px;">
		  </td>
		  </tr>
		  
		  <tr>
		  <td align="right"><fmt:message key="payrollform.sickUsed"/></td>
		  		<td align="left" colspan="2"><s:textfield name="payroll.sickUsed"   maxlength="5" size="4" cssStyle="text-align:right" onchange="onlyFloat(this);" cssClass="input-textUpper" readonly="true" tabindex="41"/></td>
		   <td></td>
		   <td align="right" width="12%"><fmt:message key="payrollform.personalDayUsed"/></td>
		  <td align="left"><s:textfield name="payroll.personalDayUsed" cssStyle="text-align:right"  maxlength="5" size="4"  cssClass="input-textUpper" readonly="true" onchange="onlyFloat(this);" tabindex="42"/></td> 
		  <td align="right"><fmt:message key="payrollform.vacationUsed"/></td>
		  <td align="left"><s:textfield name="payroll.vacationUsed" cssStyle="text-align:right"  maxlength="5" size="4" onchange="onlyFloat(this);" cssClass="input-textUpper" readonly="true" tabindex="43"/></td>
		  	
		  </tr>
		  <tr>
		  <td align="right"></td>
		  		<td align="left" colspan="2">
		  		</td>
		   <td></td>
		   <td align="right" width="12%"></td>
		  <td align="left"></td> 
		  <td align="right">Unscheduled leave</td>
		  <td align="left"><s:textfield name="payroll.unscheduledLeave" cssStyle="text-align:right"  maxlength="5" size="4" onchange="onlyFloat(this);" cssClass="input-text" readonly="false" tabindex="44"/></td>
		  	
		  </tr>
		  
		  <tr> <td></td> </tr>		  
		  </table>
		  </div>
		  </td>
		  </tr>
		  <tr>
		  		<td align="left" height="13px"></td>
		  	</tr>
		  	  	</tbody>
		  	  	</table>   
 	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

 		  <table class="detailTabLabel" border="0">
				<tbody>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='payrollform.createdOn'/></b></td>
							<td style="width:200px">
							<fmt:formatDate var="payrollCreatedOnFormattedValue" value="${payroll.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="payroll.createdOn" value="${payrollCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${payroll.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='payrollform.createdBy' /></b></td>
							<c:if test="${not empty payroll.id}">
								<s:hidden name="payroll.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{payroll.createdBy}"/></td>
							</c:if>
							<c:if test="${empty payroll.id}">
								<s:hidden name="payroll.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:65px"><b><fmt:message key='payroll.updatedOn'/></b></td>
							<fmt:formatDate var="payrollupdatedOnFormattedValue" value="${payroll.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="payroll.updatedOn" value="${payrollupdatedOnFormattedValue}"/>
							<td style="width:200px"><fmt:formatDate value="${payroll.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='payroll.updatedBy' /></b></td>
							<c:if test="${not empty payroll.id}">
								<s:hidden name="payroll.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{payroll.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty payroll.id}">
								<s:hidden name="payroll.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
						</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table>	

		 	<table>
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save"  onclick= "return check('save');"/>  
        		</td>      	
        		<td align="right">
        		<c:if test="${not empty payroll.id}">
        		<input type="button" class="cssbutton1" style="width:55px;"  value="Add" onclick="location.href='<c:url value="/payrollForm.html"/>'" /> 
        		 </c:if>	           
        		</td>        		
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
       	  	</tr>		  	
	</table>
</div>

 <s:hidden name="description" />
<s:hidden name="secondDescription" />    
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="seventhDescription" />
<s:hidden name="eightDescription" />
<s:hidden name="tenthDescription" /><s:hidden name="ninthDescription" />

</s:form>

<script type="text/javascript">
try{
	if(document.forms['payrollform'].elements['payroll.integrationTool'].value=="MM"){
		document.getElementById('mmDeviceDiv').style.display = 'block';
	}else{
		document.getElementById('mmDeviceDiv').style.display = 'none';
	}
}catch(e){}
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
		