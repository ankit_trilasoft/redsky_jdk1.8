<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="partnerRateMatrixList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerRateMatrixList.heading'/>"/>  
    
<style>
.tab{
border:1px solid #74B3DC;}

span.pagelinks {
display:block;
font-size:0.9em;
margin:-11px 0 0 0;
text-align:right;
}

</style> 
<script language="javascript" type="text/javascript">
function clear_fields(){ 
			document.forms['partnerRateMatrixList'].elements['partnerRateGrid.partnerCode'].value = "";
			document.forms['partnerRateMatrixList'].elements['partnerRateGrid.partnerName'].value = "";
			document.forms['partnerRateMatrixList'].elements['partnerRateGrid.terminalCountry'].value = "";
			document.forms['partnerRateMatrixList'].elements['partnerRateGrid.tariffApplicability'].value = "";
			document.forms['partnerRateMatrixList'].elements['partnerRateGrid.status'].value = "";
			document.forms['partnerRateMatrixList'].elements['partnerRateGrid.publishTariff'].checked = false;
			
}
</script>
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="" key="button.search"/> 
	 <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
 </c:set> 
<s:form name="partnerRateMatrixList" id="partnerRateMatrixList" action="searchPartnerRateMatrix" method="post" validate="true"> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="newmnav">
		 <ul> 
		<!--<li id="newmnav1" style="background:#FFF "><a class="current"><span>Rate Matrix<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		--></ul>
		</div>
		<div style="width:100%;">
		<div class="spn">&nbsp;</div>	
 </div>
<div id="Layer4" style="width:100%;">
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:70px; height:25px" 
       /onclick="location.href='<c:url value=""/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  

<table  width="100%" cellspacing="1" cellpadding="0" border="0" width="100%">
	<tbody>
		<tr>
			<td >
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
   <table class="table" border="0" style="width:100%;">
   <thead>
   <tr>
	<th>Partner Code</th>
	<th>Name</th>
	<th>Country Code</th>
	<th>Tariff-Applicability</th>
	<th>Matrix-Status</th> 
   </tr>
  </thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partnerRateGrid.partnerCode" size="20" cssClass="input-text"/>
			</td>
			<td>
			    <s:textfield name="partnerRateGrid.partnerName" size="35" cssClass="input-text" />
			</td>
			<td>
			    <s:textfield name="partnerRateGrid.terminalCountry" size="15" cssClass="input-text"/>
			</td>
	        <td>
				<s:textfield name="partnerRateGrid.tariffApplicability" size="24" cssClass="input-text"/>
		   </td>
		   <td>
			   <s:textfield name="partnerRateGrid.status" size="24" cssClass="input-text"/>
		   </td> 
		</tr>
		<tr>
			<td colspan="3"></td>
			<c:if test="${partnerRateGrid.publishTariff}">
			<c:set var="ischecked" value="true"/>
			</c:if>	
			<td align="right" class="listwhitetext" width="100px" style="border-left: hidden;text-align:right;">
			<s:checkbox key="partnerRateGrid.publishTariff" value="${ischecked}" fieldValue="true" onclick="changeStatus();" cssStyle="vertical-align:middle;"/>Publish Flag</td>				
			<td width="130px" style="border-left: hidden;">
			 <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>		
		</tbody>
	</table>
		</div>
		<div style="!height:2px;"></div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<s:set name="partnerRatesGrids" value="partnerRatesGrids" scope="request"/>  
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>List</span></a></li>
		  </ul>
</div>
<div class="spnblk">&nbsp;</div>
<display:table name="partnerRatesGrids" class="table" requestURI="" id="partnerRateGridList" export="true" defaultsort="1" defaultorder="descending" pagesize="10" >   
	<display:column sortable="true" title="Tariff #" style="width: 100px;" sortProperty="id">
    	<a href="#" onclick="location.href='<c:url value="/editPartnerRateGrid.html?partnerId=${partnerPublic.id}&id=${partnerRateGridList.id}" />'"/>
			<c:out value="${partnerRateGridList.id}" /></a>
    </display:column>
    <display:column sortable="true" title="Partner Code" style="width: 100px;">
    	<c:out value="${partnerRateGridList.partnerCode}" /></a>
    </display:column>
     <display:column sortable="true" title="Name" property="partnerName" /> 
  	<display:column sortable="true" title="Effective From Date" property="effectiveDate" style="width: 110px;" format="{0,date,dd-MMM-yyyy}"/>
    <display:column sortable="true" title="Tariff Applicability" property="tariffApplicability" style="width: 100px;"/>
    <display:column sortable="true" title="Metro City" property="metroCity" />
    <display:column sortable="true" title="Country" property="terminalCountry" /> 
    <display:column sortable="true" title="Status" property="status" /> 
    <display:setProperty name="paging.banner.item_name" value="partnerRates"/>   
    <display:setProperty name="paging.banner.items_name" value="partnerRatesGrids"/>  
    <display:setProperty name="export.excel.filename" value="PartnerRates List.xls"/>   
    <display:setProperty name="export.csv.filename" value="PartnerRates List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="PartnerRates List.pdf"/>   
</display:table>  
</td>
</tr>
</tbody>
</table> 
<table>

<tr><td style="height:60px; !height:100px"></td></tr></table>
</div>
</s:form>
<script type="text/javascript">   
   
</script>