<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
  
<head>   
    <title><fmt:message key="menuItemDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='menuItemDetail.heading'/>"/>
    <style type="text/css">h2 {background-color: #FBBFFF}</style>
    <style><%@ include file="/common/calenderStyle.css"%></style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>

<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="javascript" type="text/javascript"> var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
</script>

<style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

</style>

<script>
function chooseCom(){
  ///alert("aaaaaaaaaaaaaa");
	document.forms['menuItemForm'].elements['corp'].value = document.forms['menuItemForm'].elements['menuItem.corpID'].value;
	////alert(document.forms['menuItemForm'].elements['corp'].value);
	}
function choosepar(){
 ////alert("aaaaaaaaaaaaaa");
	document.forms['menuItemForm'].elements['parent'].value = document.forms['menuItemForm'].elements['menuItem.parentName'].value;
	////alert(document.forms['menuItemForm'].elements['corp'].value);
	}
function checkMandatory(){
	if(document.forms['menuItemForm'].elements['menuItem.url'].value == "" || document.forms['menuItemForm'].elements['menuItem.title'].value == ""  || document.forms['menuItemForm'].elements['menuItem.sequenceNum'].value == "" || document.forms['menuItemForm'].elements['menuItem.name'].value == "" || document.forms['menuItemForm'].elements['menuItem.description'].value == "" || document.forms['menuItemForm'].elements['menuItem.parentName'].value == ""){
 		alert("Enter Mandatory fields to continue.....");
 		return false;
 		}else{
 		return true;
 	}
}

function findParByCorp(){
////alert("aaaaaaaaaaaaaaaaaaaaaaaa");
	var corp= document.forms['menuItemForm'].elements['menuItem.corpID'].value;
	var url="findParByCorp.html?ajax=1&decorator=simple&popup=true&corp="+encodeURI(corp);
	///alert(url);
	http7.open("GET", url, true);
    http7.onreadystatechange = handleHttpResponse55555;
    http7.send(null);
}

function handleHttpResponse55555()        {
		   if (http7.readyState == 4){
                var results = http7.responseText
                results = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split("@");
               //// alert(res);
                targetElement = document.forms['menuItemForm'].elements['menuItem.parentName'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['menuItemForm'].elements['menuItem.parentName'].options[i].text = '';
						document.forms['menuItemForm'].elements['menuItem.parentName'].options[i].value = '';
					}else{
						document.forms['menuItemForm'].elements['menuItem.parentName'].options[i].text =res[i];
						document.forms['menuItemForm'].elements['menuItem.parentName'].options[i].value =res[i];
					}
				}
         }
        }
	

function findSeqNumList(){
	var corp= document.forms['menuItemForm'].elements['menuItem.corpID'].value;
	var parent= document.forms['menuItemForm'].elements['menuItem.parentName'].value;
	var url="findSeqNumList.html?ajax=1&decorator=simple&popup=true&corp="+encodeURI(corp)+"&parent="+encodeURI(parent);
	http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponse4444;
    http6.send(null);
}
	
function handleHttpResponse4444()
        {
		    if (http6.readyState == 4)
             {
                var results = http6.responseText
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.trim();
               /// alert(res);
            	document.forms['menuItemForm'].elements['menuItem.sequenceNum'].value = res[0];
			}	
        }


function checkTitleAvailability() {
	var corp= document.forms['menuItemForm'].elements['menuItem.corpID'].value;
	var parent= document.forms['menuItemForm'].elements['menuItem.parentName'].value;
	var titleN= document.forms['menuItemForm'].elements['menuItem.title'].value;
	var url="checkTitleAvailability.html?ajax=1&decorator=simple&popup=true&corp="+encodeURI(corp)+"&parent="+encodeURI(parent)+"&titleN="+encodeURI(titleN);
	http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponse5;
    http4.send(null);
}
	
function handleHttpResponse5()
        {
		  if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                var res = results.split("@");
                if(res.length-1 >= 1){
                alert("Title is already in use for this Company &  Top Menu");
                document.forms['menuItemForm'].elements['menuItem.title'].value ='';
                } else {
                
                }
			}	
        }
        
	
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

	var http6 = getHTTPObject();
	var http4 = getHTTPObject(); 
	var http7 = getHTTPObject(); 



</script>
</head>
<s:form id="menuItemForm" action="saveMenuItem" method="post" validate="true">   
<s:hidden name="menuItem.id" value="%{menuItem.id}"/>
<s:hidden name="corp" value="%{menuItem.corpID}" />
<s:set name="corp" value="corp" scope="session"/>
<s:hidden name="parent" value="%{menuItem.parentName}" />
<s:set name="parent" value="parent" scope="session"/>


<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<div id="Layer1" style="width:850px;">
<div id="newmnav">
		  <ul>
		  <li><a  href="allMenuItems.html"><span>Menu Item List</span></a></li>
  		  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Menu Item</span></a></li>
		  </ul>
	</div><div class="spn">&nbsp;</div>
<div style="padding-bottom:0px;"></div>
	<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
   
<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%">
<tbody>
<tr>
<td>
<table class="" cellspacing="0" cellpadding="3" border="0">
<tbody>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="menuItem.corpID"/><font color="red" size="2">*</font></td>
<c:if test="${empty menuItem.id}">
<td align="left"><s:select  cssClass="list-menu" name="menuItem.corpID" list="%{corpList}" headerKey="" headerValue="" onchange="chooseCom();findParByCorp();" required="true" /></td>
</c:if>
<c:if test="${not empty menuItem.id}">
<td align="left"><s:textfield cssClass="input-text" name="menuItem.corpID" size="20" readonly="true"/></td>
</c:if>
<td align="right" class="listwhitetext"><fmt:message key="menuItem.parentName"/><font color="red" size="2">*</font></td>
<td align="left"><s:select name="menuItem.parentName" cssClass="list-menu" list="%{findParByCorp}" onchange="choosepar();findSeqNumList();" required="true" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="menuItem.name"/><font color="red" size="2">*</font></td>
<td align="left"><s:textfield cssClass="input-text" name="menuItem.name" size="40" maxlength="30" required="true" /></td>
<td align="right" class="listwhitetext"><fmt:message key="menuItem.url"/><font color="red" size="2">*</font></td>
<td align="left"><s:textfield cssClass="input-text" name="menuItem.url" size="40" maxlength="255" required="true" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="menuItem.title"/><font color="red" size="2">*</font></td>
<td align="left"><s:textfield cssClass="input-text" name="menuItem.title" onchange="checkTitleAvailability();" size="40" maxlength="30" required="true" /></td>
<td align="right" class="listwhitetext"><fmt:message key="menuItem.description"/><font color="red" size="2">*</font></td>
<td align="left"><s:textfield cssClass="input-text" name="menuItem.description" size="40" maxlength="30" required="true" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext">
<a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=menuItem.sequenceNum',this);return false" onmouseout="ajax_hideTooltip()">
<fmt:message key="menuItem.sequenceNum"/><font color="red" size="2">*</font></td>
<td align="left"><s:textfield cssClass="input-text" name="menuItem.sequenceNum" size="40" maxlength="7" onchange="onlyFloat(this);" required="true" /></td>
</tr>
</tbody>
</table>
</td>
</tr>	
</tbody>
</table>
</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>

			<!--<table>
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='menuItem.createdOn'/></b></td>
							<s:hidden name="menuItem.createdOn" />
							<td style="width:120px"><s:date name="menuItem.createdOn" format="dd-MMM-yyyy HH:mm"/></td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='menuItem.createdBy' /></b></td>
							<c:if test="${not empty menuItem.id}">
								<s:hidden name="menuItem.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{menuItem.createdBy}"/></td>
							</c:if>
							<c:if test="${empty menuItem.id}">
								<s:hidden name="menuItem.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='menuItem.updatedOn'/></b></td>
							<s:hidden name="menuItem.updatedOn"/>
							<td style="width:120px"><s:date name="menuItem.updatedOn" format="dd-MMM-yyyy HH:mm"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='menuItem.updatedBy' /></b></td>
							<s:hidden name="menuItem.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:85px"><s:label name="menuItem.updatedBy" value="${pageContext.request.remoteUser}" /></td>
						</tr>
					</tbody>
				</table> 
-->
<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='menuItem.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="menuItemCreatedOnFormattedValue" value="${menuItem.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="menuItem.createdOn" value="${menuItemCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${menuItem.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='menuItem.createdBy' /></b></td>
							<c:if test="${not empty menuItem.id}">
								<s:hidden name="menuItem.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{menuItem.createdBy}"/></td>
							</c:if>
							<c:if test="${empty menuItem.id}">
								<s:hidden name="menuItem.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='menuItem.updatedOn'/></b></td>
							<fmt:formatDate var="menuItemupdatedOnFormattedValue" value="${menuItem.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="menuItem.updatedOn" value="${menuItemupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${menuItem.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='menuItem.updatedBy' /></b></td>
							<c:if test="${not empty menuItem.id}">
								<s:hidden name="menuItem.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{menuItem.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty menuItem.id}">
								<s:hidden name="menuItem.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>

<s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:60px; height:25px" onclick="return checkMandatory();"/>  
<c:if test="${not empty menuItem.id}">
<input type="button" class="cssbutton" onclick="location.href='<c:url value="/editMenuItem.html"/>'"  
value="<fmt:message key="button.add"/>" style="width:60px; height:25px"/>
</c:if>  
<s:reset cssClass="cssbutton" key="Reset" cssStyle="width:60px; height:25px "/>

    
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
</s:form>
<c:set var="hitFlag" value="<%=request.getParameter("hitFlag") %>" />
<s:hidden name="hitFlag" />
<c:if test="${hitFlag == 1}" >
	<c:redirect url="/allMenuItems.html"/>
</c:if>
<script type="text/javascript">   
try{
chooseCom();
}
catch(e){}
try{
choosepar();
}
catch(e){}
///findSeqNumList();
 ///findParByCorp();
 /////   Form.focusFirstElement($("menuItemForm"));
</script>  		