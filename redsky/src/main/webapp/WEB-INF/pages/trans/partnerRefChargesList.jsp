<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="additionalCharges.title"/></title>   
    <meta name="heading" content="<fmt:message key='additionalCharges.heading'/>"/>  
    
<style>
.tab{
border:1px solid #74B3DC;

}
span.pagelinks {
display:block;
font-size:0.9em;
margin-top:-10px;
!margin-top:0px;
text-align:right;
}
</style> 
<script language="javascript" type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Base SCAC?");
	var did = targetElement;
	if (agree){
		location.href="deleteBaseSCAC.html?baseId=${agentBase.id}&id="+did;
	}else{
		return false;
	}
}
    function confirmSubmit1(targetElement){
	var agree=confirm("Are you sure you wish to remove this Partner RefCharges Item?");
	var did = targetElement;
	if (agree){
		location.href="deletePartnerRefCharges.html?id="+did+"";
	}
	else{
		return false;
	}
}
  function clear_fields(){  		    	

	document.getElementById('value').value= "";
   	document.getElementById('tariffApplicability').value ="";
    document.getElementById('parameter').value = "";
    document.getElementById('countryCode').value ="";
  document.getElementById('grid').value ="";
  document.getElementById('basis').value ="";
  document.getElementById('label').value ="";
}
function onlyFloat(targetElement)
{   var i;
	var s = targetElement.value;
	var count = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if (((c < '0') || (c > '9')) && (c != '.') || (count>'1')) 
        {
        	alert("Enter valid Number");
	        document.getElementById(targetElement.id).value='';
            document.getElementById(targetElement.id).select();
	        return false;
        }
    }
    return true;
}		
</script>
</head>
<s:form id="additionalChargesListForm" action="searchPartnerRefCharges" method="post" validate="true">	
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton1" cssStyle="width:58px;" align="top"  key="button.search" />   
    <input type="button" class="cssbutton1" value="Clear" style="width:58px;" onclick="clear_fields();"/> 
</c:set>
<div id="otabs"><ul><li><a class="current"><span>Search</span></a></li></ul></div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 11px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
	<table class="table" style="width:99%;"  >
	<thead>
<tr>
<th>Value</th>
<th>Tariff Applicability</th>
<th>Parameter</th>
<th>Country Code</th>
<th>Grid</th>
<th>Basis</th>
<th>Label</th>
</tr></thead>	
		<tbody>
		<tr>	
			<td width="">
			    <s:textfield name="partnerRefCharges.value" id="value" size="9" required="true" cssClass="input-text" onchange="onlyFloat(this)"/>
			</td>
			<td width="">
			   <s:select name="partnerRefCharges.tariffApplicability" cssClass="list-menu" id="tariffApplicability" list="{'Origin','Destination'}" headerKey="" headerValue="" cssStyle="width:120px"/>
			</td>
			<td width="">
			    <s:textfield name="partnerRefCharges.parameter" id="parameter" size="9" required="true" cssClass="input-text"/>
			</td>
			<td width="">
			<s:select name="partnerRefCharges.countryCode" cssClass="list-menu" list="%{country}" id="countryCode" headerKey="" headerValue="" cssStyle="width:120px"/></td></td>
			</td>
			<td width="">
			   <s:select name="partnerRefCharges.grid" cssClass="list-menu"  list="%{grid}" id="grid" headerKey="" headerValue="" cssStyle="width:120px"/></td>
			</td>
			<td width="">
			    <s:textfield name="partnerRefCharges.basis" id="basis" size="9" required="true" cssClass="input-text"/>
			</td>
			<td width="">
			    <s:textfield name="partnerRefCharges.label" id="label" size="9" required="true" cssClass="input-text"/>
			</td>
			</tr>
			<tr>
			
			<td colspan="7" style="text-align:right"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		
		</tr>
		</tbody>
	</table>
	</div>
	<c:out value="${searchresults}" escapeXml="false" /> 
<div class="bottom-header" style="!margin-top:45px"><span ></span></div>
</div>
</div>
<div id="newmnav">
	 <ul>
	 	<li id="newmnav1" style="background:#FFF;"><a class="current" style="!margin-bottom:-15px;"><span>Additional Charges List</span></a></li>
	 </ul>
</div>
<div style="width: 100%" >
<div class="spn">&nbsp;</div>
</div>
<div id="Layer1" style="width: 100%;">
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:60px; height:25px" onclick="location.href='<c:url value="/editAdditionalCharges.html"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>  

<table  width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td >				
				<s:set name="partnerRefChargesList" value="partnerRefChargesList" scope="request"/>  
				<display:table name="partnerRefChargesList" class="table" requestURI="" id="partnerRefChargesLists" export="true" defaultsort="1" pagesize="10" style="width: 100%; " >   
					<display:column property="countryCode" sortable="true" titleKey="partnerRefCharges.countryCode" href="editAdditionalCharges.html" paramId="id" paramProperty="id" />
					<display:column property="tariffApplicability" sortable="true" titleKey="partnerRefCharges.tariffApplicability"/>
					<display:column property="label" sortable="true" titleKey="partnerRefCharges.label"/>
				    <display:column property="parameter" sortable="true" titleKey="partnerRefCharges.parameter"/>
					<display:column property="grid" sortable="true" titleKey="partnerRefCharges.grid"/>
					<display:column property="value" sortable="true" headerClass="containeralign" style="text-align:right" titleKey="partnerRefCharges.value" />
					<display:column property="basis" sortable="true" titleKey="partnerRefCharges.basis"/>
					<display:column title="Remove" style="width:45px;">
				    <a><img align="middle" onclick="confirmSubmit1(${partnerRefChargesLists.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
					</display:column>

				    <display:setProperty name="paging.banner.item_name" value="Additional Charges"/>   
				    <display:setProperty name="paging.banner.items_name" value="Additional Charges"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Additional Charges List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Additional Charges List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Additional Charges List.pdf"/>   
				</display:table>  
			</td>
		</tr>
	</tbody>
</table> 
<table> 
<c:out value="${buttons}" escapeXml="false" /> 
	<tr>
		<td style="height:70px; !height:100px"></td>
	</tr>
</table>
</div>
</s:form> 
