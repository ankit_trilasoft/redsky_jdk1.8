package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AuditTrailDao;
import com.trilasoft.app.model.AuditTrail;
import com.trilasoft.app.service.AuditTrailManager;

public class AuditTrailManagerImpl extends GenericManagerImpl<AuditTrail, Long> implements AuditTrailManager {
	AuditTrailDao auditTrailDao;

	public AuditTrailManagerImpl(AuditTrailDao auditTrailDao) {
		super(auditTrailDao);
		this.auditTrailDao = auditTrailDao;
	}

	public List getAuditTrailList(Long xtranId, String tableName, String corpID,String from,String secondID) {
		return auditTrailDao.getAuditTrailList(xtranId, tableName, corpID,from,secondID);
	}
	public List getAuditTrailListNew(Long xtranId, String tableName, String corpID) {
		return auditTrailDao.getAuditTrailListNew(xtranId, tableName, corpID);
	}	
	public List  getAlertHistoryList(String corpID,int daystoManageAlerts){
		return auditTrailDao.getAlertHistoryList(corpID,daystoManageAlerts);
	}
	public List findByAlertListFilter(String sessionCorpID,String alertUser,int daystoManageAlerts){
		return auditTrailDao.findByAlertListFilter(sessionCorpID, alertUser,daystoManageAlerts);
	}
	public List getAlertTaskList(Long xtranId,String tableName,String sessionCorpID,String relatedTask,String sequenceNumber){
		return auditTrailDao.getAlertTaskList(xtranId,tableName,sessionCorpID,relatedTask, sequenceNumber);		
	}
	public List findContainerCartonVehicleRoutingConsigneeList(Long sid,String tableName,String sessionCorpID){
		return auditTrailDao.findContainerCartonVehicleRoutingConsigneeList(sid,tableName,sessionCorpID);	
	}
	public List getAuditAccountContactList(String corpID,String partnerCode,String tableName){
		return auditTrailDao.getAuditAccountContactList(corpID, partnerCode,tableName);
	}
	public List findByAlertListFilterForExtCoordinator(String sessionCorpID,String alertUser,int daystoManageAlerts,String bookingAgentPopup,String billToCodePopup)
	{
		return auditTrailDao.findByAlertListFilterForExtCoordinator(sessionCorpID, alertUser,daystoManageAlerts,bookingAgentPopup,billToCodePopup);
		
	}
}
