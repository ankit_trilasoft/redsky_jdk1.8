<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ include file="/common/taglibs.jsp"%>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>

<head>
<title> Query Information </title>
<meta name="heading" content="Query Information" />
<script language="javascript" type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Query?");
	var did = targetElement;
	if (agree){
		location.href="deleteQuery.html?id="+did;
	}else{
		return false;
	}
}

function call(tarVal,tarid){ 
var tarIdd=tarid;
var extractVal=tarVal;
var tar=tarVal;
location.href="showOldDataExtract.html?reportName="+extractVal+"&id="+tarIdd ;

}

function  editQuery(tarVal,tarid){
var tarIdd=tarid;
var extractVal=tarVal;
var tar=tarVal;
location.href="editOldDataExtract.html?reportName="+extractVal+"&id="+tarIdd+"&columnButton=yes" ;

}

function dataExtractMail(tarVal,tarid){ 
var tarIdd=tarid;
var extractVal=tarVal;
var tar=tarVal;
openWindow('dataExtractSendMail.html?id='+tarIdd+'&decorator=popup&popup=true',400,200);

}
function findInvoiceTotal(position,id) {
  var id=id;  
  openWindow('findDataExtractMailInfo.html?&id='+ encodeURI(id)+'&decorator=popup&popup=true',900,300);
  	
  }		
</script>
<script language="javascript" type="text/javascript">

function clear_fields(){
	
			
					document.forms['extractQueryFile'].elements['queryName'].value = "";
					document.forms['extractQueryFile'].elements['modifiedby'].value = "";
					document.forms['extractQueryFile'].elements['publicPrivateFlag'].value = "";
}
</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:0;
margin-top:-15px;
padding:2px 0;
text-align:right;
width:100%;
}
</style>
</head>
        
  
<s:form id="extractQueryFile" cssClass="form_magn" name="extractQueryFile" action="searchExtractQueryFiles" validate="true" method="post"> 
<c:set var="userName" value="${pageContext.request.remoteUser}" />
<s:hidden name="userName" value="${pageContext.request.remoteUser}"/> 
  <div id="Layer1" style="width: 100%">

 	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width: 100%;">
<thead>
	<tr>
		<th>Query Name</th>
		<th>Modified By</th>
		<th>Flag</th>
		<th></th>
	</tr>
</thead> 
<tbody>
	  <tr>
		   <td>
		       <s:textfield name="queryName" required="true" cssClass="input-text" size="30" maxlength="30"/>
		   </td>
		   <td>
		       <s:textfield name="modifiedby" required="true" cssClass="input-text" size="30" maxlength="25"/>
		   </td>
		   <td>
		       <s:textfield name="publicPrivateFlag" required="true" cssClass="input-text" size="30" maxlength="20"/>
		   </td>   
          
          <td width="430px" style="border-left: hidden;">
               <s:submit cssClass="cssbutton1" cssStyle="width:55px;!margin-bottom: 10px;" method="" key="button.search"/>  
               <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
          </td>
     </tr>
  </tbody>
 </table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Query Information List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<s:set name="queryList" value="queryList" scope="request"/> 
<c:if test="${queryList!='[]'}"> 
	<display:table name="queryList" class="table" requestURI="" id="queryList" defaultsort="1" defaultorder="ascending" export="true" pagesize="10" style="width:100%"> 
	<display:column property="queryName" sortable="true" title="Query Name"/> 
	<display:column   title="Modified On" sortable="true" sortProperty="modifiedon"  property="modifiedon" format="{0,date,dd-MMM-yyyy}">
		</display:column>
	<display:column property="modifiedby" sortable="true" title="Modified By"></display:column>
	<display:column property="publicPrivateFlag" sortable="true" title="Flag"></display:column>
	<display:column title="Scheduler">
	<c:if test="${queryList.autoGenerateReoprt}">
		 <img id="target" title="Scheduled" alt="Scheduled" src="${pageContext.request.contextPath}/images/schedulerClockNew.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
			 <c:if test="${queryList.queryScheduler=='D'}">
				 daily 
			 </c:if>
			 <c:if test="${queryList.queryScheduler=='W'}"> 
				 Weekly -
				 <c:if test="${queryList.perWeekScheduler=='1'}" >
					  Sun
				 </c:if>
				 <c:if test="${queryList.perWeekScheduler=='2'}" >
					  Mon
				 </c:if>
				 <c:if test="${queryList.perWeekScheduler=='3'}" >
					  Tus
				 </c:if>
				 <c:if test="${queryList.perWeekScheduler=='4'}" >
					  Wed
				 </c:if>
				 <c:if test="${queryList.perWeekScheduler=='5'}" >
					  Thu
				 </c:if>
				 <c:if test="${queryList.perWeekScheduler=='6'}" >
					  Fri
				 </c:if>
				 <c:if test="${queryList.perWeekScheduler=='7'}" >
					  Sat
				 </c:if> 
			 </c:if>
			 <c:if test="${queryList.queryScheduler=='M'}">
				 Monthly -  <c:out value="${queryList.perMonthScheduler}" />
			 </c:if>
		  	 <c:if test="${queryList.queryScheduler=='Q'}">
				 Quarterly
		  	 </c:if>
	</c:if>
	<c:if test="${queryList.autoGenerateReoprt=='false'}">
		<img id="target"   title="Not Scheduled" alt="Not Scheduled" src="${pageContext.request.contextPath}/images/nonschedulerClocknew.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	</c:if>
	</display:column>
	<c:if test="${queryList.autoGenerateReoprt}">
	<display:column   title="Email" style="width:70px; "> 
		<input type="button" class="cssbutton" style="width:70px; " align="top"  value="Send Mail" onclick="return dataExtractMail('${queryList.extractType}','${queryList.id}');"/>
	</display:column>
	</c:if>
	<c:if test="${queryList.autoGenerateReoprt=='false'}">
		<display:column   title="Email" style="width:70px; "> 
	<input type="button" class="cssbutton" style="width:70px; " align="top"  value="Send Mail" disabled="disabled"/>
	</display:column>
	</c:if>
	<display:column   title="Extract" style="width:90px; "> 
	<input type="button" class="cssbutton" style="width:90px; " align="top"  value="Show Reports" onclick="return call('${queryList.extractType}','${queryList.id}');"/>
	</display:column>
<c:if test="${queryList!='[]'}"> 
<c:if test="${queryList.createdby==userName}">
<display:column   title="Edit" style="width:70px; " > 
<input type="button" class="cssbutton" style="width:70px; " align="top"  value="Edit Query" onclick="return editQuery('${queryList.extractType}','${queryList.id}');"/>
</display:column>
</c:if>

<c:if test="${queryList.createdby!=userName}">
<display:column   title="Edit" style="width:70px; " > 
<input type="button" class="cssbutton" style="width:70px; " align="top"  value="Edit Query" onclick="javascript:alert('This query has created by ${queryList.createdby}, You cannot edit the query.')"/>
</display:column>
</c:if>
<display:column title="Remove" style="width: 10px;">
<c:if test="${queryList.createdby==userName}">
	<a><img align="middle" title="Delete Query" onclick="confirmSubmit('${queryList.id}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
</c:if>
</display:column> 
</c:if>
<display:column title="Mail&nbsp;Info" style="width:10px;text-align:center;">
<c:if test="${queryList.autoGenerateReoprt}">
	<a><img class="openpopup" src="${pageContext.request.contextPath}/images/information-frame.png" title="Send Email Information" onclick="findInvoiceTotal(this,${queryList.id});"/></a>
</c:if> 
</display:column> 
</display:table>
</c:if>
<c:if test="${queryList=='[]'}"> 
<display:table name="queryList" class="table" requestURI="" id="queryList" style="width:970px"> 
<display:column property="queryName" title="Query Name"/> 
<display:column title="Modified On" property="modifiedon" format="{0,date,dd-MMM-yyyy}"/>
<display:column property="modifiedby" title="Modified By"/>
<display:column property="publicPrivateFlag" title="Flag"/>
<display:column title="Scheduler"/>
<display:column title="Email" style="width:70px; "/>
<display:column title="Extract" style="width:90px; "/>
<display:column title="Edit" style="width:70px; " />
<display:column title="Remove" style="width: 10px;"/>
<display:column title="Mail&nbsp;Info" style="width: 10px;"/>
</display:table>
</c:if>
<s:submit action="dynamicDataExtracts" cssStyle="width:55px;" value="Back" cssClass="cssbutton"></s:submit> 
</s:form>