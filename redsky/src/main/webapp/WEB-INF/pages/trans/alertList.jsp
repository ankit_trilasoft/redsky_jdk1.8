<%@ taglib prefix="fn1" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head> 
    <title>Alert List</title> 
    <meta name="heading" content="Alert List"/> 
<style type="text/css">
div#page {
margin:15px 0px 0px 10px;
padding:0pt;
text-align:center;
}
#mainPopup {
padding-left:0px;
padding-right:0px;
}
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-18px;
padding:2px 0;
text-align:right;
width:100%;
}
</style>
<script type="text/javascript">
/*function showFilePage(id)
{
window.open('findPartnerProfileList.html?from=view&code='+code+'&partnerType=AG&id='+id, '_blank');
}*/
</script>
</head>
<s:form id="searchForm" name="searchForm" action="" method="post" validate="true">  

<s:set name="reportss" value="reportss" scope="request"/>

<div id="Layer1" align="left" style="width:100%;border:1px solid #fff;">
<div id="otabs" style="margin-left:40px;">
		<ul>
			<li><a class="current"><span>Alert List</span></a></li>
		</ul>
	</div>
	<div class="spnblk"></div>	
<div style="padding-bottom:0px;!padding-bottom:10px;"></div>
<display:table name="auditTrailList" class="table" requestURI="" id="auditTrailList" defaultsort="0" pagesize="100" style="width:100%;margin-top:-7px;!margin-top:0px; ">
<display:column sortable="true" title="File Number" >
				    <a onclick="showFilePage(${auditTrailList.id});" ><c:out value="${auditTrailList.alertFileNumber}" /></a>
					</display:column>
<display:column property="alertShipperName" sortable="true" title="Shipper Name"/>
<display:column property="fieldName" sortable="true" title="Field Name"/>
<display:column property="description" sortable="true" title="Description"/>
<display:column  sortable="true" title="Old Value">

    <c:if test="${fn1:contains(auditTrailList.oldValue,'00:00')}">
       		<c:out value= "${fn1:substringBefore(auditTrailList.oldValue,'00:00')}"></c:out>   
       </c:if>
       <c:if test="${!fn1:contains(auditTrailList.oldValue,'00:00')}">
       		<c:out value="${auditTrailList.oldValue }"></c:out>
       </c:if>
       </display:column>
    <display:column  sortable="true" title="New Value">
    
       <c:if test="${fn1:contains(auditTrailList.newValue,'00:00')}">
       		<c:out value= "${fn1:substringBefore(auditTrailList.newValue,'00:00')}"></c:out>   
       </c:if>
       <c:if test="${!fn1:contains(auditTrailList.newValue,'00:00')}">
       		<c:out value="${auditTrailList.newValue }"></c:out>
       </c:if>
    </display:column>
    <display:column property="user" sortable="true" title="User"/>
    <display:column title="Modify Date" style="width:125px"><fmt:formatDate value="${auditTrailList.dated}" pattern="${displayDateTimeFormat}"/></display:column>
    <display:setProperty name="export.excel.filename" value="Reports List.xls"/> 
    <display:setProperty name="export.csv.filename" value="Reports List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="Reports List.pdf"/> 

</display:table>
</div> 
</s:form>
 
