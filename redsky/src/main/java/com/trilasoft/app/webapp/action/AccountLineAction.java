package com.trilasoft.app.webapp.action;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.StringTokenizer;
import java.util.TreeMap; 
import com.google.gson.Gson; 
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse; 
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction; 
import com.opensymphony.xwork2.Preparable; 
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTO;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DtoBillingWizard;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DtoTotalInvoice;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.ChargeTpDetailDTO;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.DTOUserDataSecurity;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Commission;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.ExtractedFileLog;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.VanLineCommissionType;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.CommissionManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.ExchangeRateManager;
import com.trilasoft.app.service.ExpressionManager;
import com.trilasoft.app.service.ExtractedFileLogManager;
import com.trilasoft.app.service.ItemsJbkEquipManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.webapp.util.GridDataProcessor;
public class AccountLineAction extends BaseAction implements Preparable{
	private static Map pageFieldVisibilityMap = AppInitServlet.pageFieldVisibilityComponentMap;
	private String vanLineAccountView;
    private Long sid;
    private Long networkAgentId; 
	private ItemsJbkEquipManager itemsJbkEquipManager;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private ServiceOrder serviceOrderToRecods;
	private TrackingStatus trackingStatus;
	private TrackingStatus trackingStatusToRecods;
	private TrackingStatusManager trackingStatusManager;
	private ServicePartner servicePartner;
	private ServicePartnerManager servicePartnerManager;
	private PartnerManager partnerManager;
	public CommissionManager commissionManager;
	private Commission commission;
	private AccountLine accountLine;
	private AccountLine synchedAccountLine;
	private AccountLine accountLineReverse;
	private  ExchangeRateManager exchangeRateManager;
	private double buildFormulaActualRevenue=0;
	private List accExchangeRateList;
	private List accountLineList;
	private List findOurInviceList;
	private List findMilitaryBaseList;
	private List findVendorInviceList;	
	private List accountLineEstimateList;
	private ServiceOrder serviceOrderCombo;
	private Date newSysDate;
	/* Added By kunal for ticket number: 6926 */
    private Date beginDate;
    private Date endDate;
	/* Modification ends here */
	private List accountLineListBillToCode;// for bill to codes
	private List partners;
	private List systemDefaultList;
	private  List getCommissionList;
	private  List getGlTypeList;
	private List BillingWizardList;
	private List nonInvoiceBillingList;
	private AccountLineManager accountLineManager;
	private Long id;
	public int billingFlag;
	public String billingContractFlag;
	private CustomerFile customerFile;
	private CustomerFile customerFileToRecods;
	private CustomerFileManager customerFileManager;
	private  Miscellaneous  miscellaneous;
	private MiscellaneousManager miscellaneousManager ;
	private String sessionCorpID;
	private String accCorpID;
	private List maxLineNumber;
	private String accountLineNumber;
	private Long autoLineNumber;
	private Long i;
	private int rowAffected;
	private String countInvoice;
	private Long newInvoiceNumber;
	private Long IdMax;
	private Billing billing;
	private Billing billingRecods;
	private BillingManager billingManager;
	private Charges charges;
	private ChargesManager chargesManager;
	private ChargesAction chargeAction; 
	private BigDecimal totalEstimateExpenseCwt;
	private BigDecimal totalRevisionExpenseCwt;
	private BigDecimal totalEstimateExpenseKg;
	private BigDecimal totalRevisionExpenseKg;
	private BigDecimal totalEstimateExpenseCft;
	private BigDecimal totalRevisionExpenseCft;
	private BigDecimal totalEstimateExpenseCbm;
	private BigDecimal totalRevisionExpenseCbm;
	private BigDecimal totalEstimateExpenseEach;
	private BigDecimal totalRevisionExpenseEach;
	private BigDecimal totalEstimateExpenseFlat;
	private BigDecimal totalRevisionExpenseFlat;
	private BigDecimal totalEstimateExpenseHour;
	private BigDecimal totalRevisionExpenseHour;
	private  BigDecimal actualRevenue;
	private String shipNumber;
	private String recInvoiceNumber;
	private String invoiceToDelete;
	private String recInvNumBilling;
	private String job;
	private NotesManager notesManager;
	/* Form change status verifier */
	private String formStatus = "0";
	/* Modification done */
	private String countCategoryTypeDetailNotes;
	private String countEstimateDetailNotes;
	private String countPayableDetailNotes;
	private String countRevesionDetailNotes;
	private String countEntitleDetailOrderNotes;
	private String invoiceCheck;
	private String accountIdCheck;
	private String countReceivableDetailNotes;
	private String idCheck;
	private String oldRecRateCurrency;
	private BigDecimal estimateExp;
	private BigDecimal estimateRevenueAmt;
	private BigDecimal gmPercent;
	private BigDecimal grossMrgn;
	private BigDecimal revisedTotalRev;
	private BigDecimal revisedTotal;
	private BigDecimal revisedGrossMarginPer;
	private BigDecimal revisedGrossMar;
	private BigDecimal entitledTotal;
	private BigDecimal actualExpenseTotal;
	private BigDecimal actualRevenueTotal;
	private BigDecimal actualGrossMar;
	private BigDecimal actualGrossMarginPer;
	private List revisedChargeList;
	private List revisedEntitleChargeList;
	private List revisedEstimateChargeList;
	private List revisedChargeListRadio;
	private List systemDefaultValueList;
	private List paidInvoiceList;
	private String billingContract;
	private String accountCompanyDivision;
	private String charge;
	private String shipNum;
	private String baseCode;
	private String description;
	private String terminalState;
	private String SCACCode;
	private String recPostDate;
	private String newDefaultDate;
	private List quantity;
	private ExpressionManager expressionManager;
	private String gotoPageString;
	private String validateFormNav;
	private String glType;
	private String actualRevanue;
	private String branchCode;
	private String chargeCode;
	private String contract;
	private String desc;
	private String notPaidInvoiceNumber;
	private String notPaidBillToCode;
	private String notPaidBillToName;
	private String systemDefaultmiscVl;
	private String systemDefaultCommissionJob;
	private String systemDefaultstorage;
	private List authorizationNoList =new ArrayList();
	private String glcomm;
	private String distAmt;
	private String actualAmt;
	private String checkCropId;
	private VanLineCommissionType vanLineCommissionType;
	private String accountInterface;
	private Boolean costElementFlag;
	private String companyDivisionAcctgCodeUnique;
	private String multiCurrency;
	private String baseCurrency;
	private boolean contractType=false;
	private boolean selectiveInvoice ;
	private boolean billingCMMContractType=false;
	boolean billingDMMContractType=false;
	private boolean networkAgent=false;
	private boolean utsiRecAccDateFlag=false;
	private boolean utsiPayAccDateFlag=false;
	private boolean automaticReconcile=false;
	private boolean ownbillingVanline=false;
	private boolean singleCompanyDivisionInvoicing=false;
	String bookingAgentCodeDMM="";
	private String baseCurrencyCompanyDivision="";	
	private String salesCommisionRate;
	private String grossMarginThreshold;
	private List reverseInvoiceList;
	private List invoiceToDeleteList;
	private List<DtoTotalInvoice> totalInvoiceList = new ArrayList<DtoTotalInvoice>();
	private List totalVendorInvoiceList;
	private List actualSenttoDatesList;
	private List invoiceDetailList;
	private List nonInvoiceList;
	private List vanPayChargesList; 
	private List domCommChargesList;
	private String recInvoiceCheck;
	private BigDecimal actRevSumBilling;
	private BigDecimal revRevAmtSumBilling;
	private BigDecimal estRevAmtSumBilling;
	private BigDecimal distAmtSumBilling;
	private String listData;
	private String listFieldNames;
    private String listFieldEditability;   
	private String listFieldTypes; 
	private String listIdField;
	private String dateUpdate;
	private String companyDivision; 
	private String destinationCountryCode; 
	private String originCountryCode;
	private String countryCodeEUResult;
	private String billToCode;
	private String vendorCode;
	private Date fromDate; 
	private Date toDate; 
	private List emailInvoiceList; 
	private String hitflag;
	private String accSaveHitflag;
	private String checkLHF; 
	private List creditInvoiceList=new ArrayList(); 
	private List companyDivis = new ArrayList(); 
	private Integer checkRecInvoiceForHVY;
	private String systemDefaultVatCalculation;
	private Company company; 
	private CompanyManager companyManager;
    private String estVatFlag;
    private String contractTypeValue = ""; 
    private String partnerCode;
    DecimalFormat decimalFormat = new DecimalFormat("#.####");
	Date currentdate = new Date();
	private boolean payablesXferWithApprovalOnly = false;
	private boolean networkAgentInvoiceFlag = false; 
	private boolean accNonEditFlag=false; 
	private boolean adminInvoiceAmountEdit=false;
	static final Logger logger = Logger.getLogger(AccountLineAction.class);
	private BigDecimal accountLineDistribution;
	private String driverCommission;
	private String automaticDriverReversal;
	private Boolean driverCommissionSetUp=false;
	private BigDecimal accountEstimateRevenueAmount;
	private String voxmeIntergartionFlag;
	private String jobName;
	private String quotesToValidate;
	private String commissionJobName;
	private String billToCodeLst;
	private  int billToLength = 0;
	private int currencyLength =0;
	private List compDivForBookingAgentList;
	private List validateChargeVatExcludeList;
    private String accountLineStatus;
    private String previewLine;
	private List allPreviewLine;
	//private String isSOExtract;
	private String networkPartnerCode;
	private String networkBillToCode;
	private String partnerNameAutoCopmlete;
	private String autocompleteCondition;
	private Partner partner;
	private String checkZeroforInvoice;
	private String invoiceBillToCode;
	private boolean checkDescriptionFieldVisibility=false;
	private String oiJobList;
	/*public String getIsSOExtract() {
		return isSOExtract;
	}
	public void setIsSOExtract(String isSOExtract) {
		this.isSOExtract = isSOExtract;
	}*/
	private Map<String, String> accEstmateStatus;
	private BigDecimal accountRevisionRevenueAmount;
	private EmailSetupManager emailSetupManager;
	private List selectiveInvoiceList;
	private Boolean acctDisplayEntitled = new Boolean(true);
    private Boolean acctDisplayEstimate = new Boolean(true);
    private Boolean acctDisplayRevision = new Boolean(true);
    private boolean accountLineAccountPortalFlag=false; 
    private String checkContractChargesMandatory= "0";
    private ErrorLogManager errorLogManager;
    private String emptyList;
    private Boolean oAdAWeightVolumeValidationForInv=false;
    private Boolean subOADAValidationForInv=false;
    private Boolean ownBookingAgentFlag=false;
    private String compDivFlag;
    private String updatedUserName;
    private String oldAccountLineBillToCode;
    private List partnerDetailsAutoComplete;
    private String partnerNameId;
    private String paertnerCodeId;
    private String autocompleteDivId;
	private String partnerDetailsAutoCompleteGsonData;
	private List closeCompanyDivision;
    private boolean cmmDmmFlag=false;
    private String acctRefVisiable="";
    private List findInvoiceList;
    private String usertype;
    private String dashBoardHideJobsList;
    private boolean allowAgentInvoiceUpload;           
    private boolean autoPayablePosting;
    private String accId;
    private String notesStatus;
	private String vatBillimngGroupCode;
	private PartnerPrivateManager partnerPrivateManager;
	private String payVatDesc;
	private String recVatDescr;
	private String payVatDescr;
	private  Map<String,String>flex1RecCodeList;
	private  Map<String,String>flex2PayCodeList;
	public String getPayVatDescr() {
		return payVatDescr;
	}

	public void setPayVatDescr(String payVatDescr) {
		this.payVatDescr = payVatDescr;
	}
	private  Map<String, String> vatBillingGroups = new LinkedHashMap<String, String>();
	public String getRecVatDescr() {
		return recVatDescr;
	}

	public void setRecVatDescr(String recVatDescr) {
		this.recVatDescr = recVatDescr;
	}

	public String getPayVatDesc() {
		return payVatDesc;
	}

	public void setPayVatDesc(String payVatDesc) {
		this.payVatDesc = payVatDesc;
	}

	public PartnerPrivateManager getPartnerPrivateManager() {
		return partnerPrivateManager;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public String getVatBillimngGroupCode() {
		return vatBillimngGroupCode;
	}

	public void setVatBillimngGroupCode(String vatBillimngGroupCode) {
		this.vatBillimngGroupCode = vatBillimngGroupCode;
	}
	private String flex1;
	public String getFlex1() {
		return flex1;
	}

	public void setFlex1(String flex1) {
		this.flex1 = flex1;
	}
	private String flex2;
	public String getFlex2() {
		return flex2;
	}

	public void setFlex2(String flex2) {
		this.flex2 = flex2;
	}
	private List vatDescription;
	public List getVatDescription() {
		return vatDescription;
	}

	public void setVatDescription(List vatDescription) {
		this.vatDescription = vatDescription;
	}

	public String getAccId() {
	return accId;
    }

      public void setAccId(String accId) {
	this.accId = accId;
     }

	public ChargesAction getChargeAction() {
		return chargeAction;
	}
	
	public void setChargeAction(ChargesAction chargeAction) {
		this.chargeAction = chargeAction;
	}
	private Set<Role> roles;
	public AccountLineAction(){
	   Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        usertype=user.getUserType();
        this.sessionCorpID = user.getCorpID();
        if(user.getAcctDisplayEntitled()!=null){
        	acctDisplayEntitled=user.getAcctDisplayEntitled();
        }
        if(user.getAcctDisplayEstimate()!=null){
        	acctDisplayEstimate=user.getAcctDisplayEstimate();
        }
        if(user.getAcctDisplayRevision()!=null){
        	acctDisplayRevision=user.getAcctDisplayRevision();
        }
         
        roles=user.getRoles();
        
	}
	
	public void prepare() throws Exception {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			accCorpID=sessionCorpID;
			updatedUserName=getRequest().getRemoteUser();
			logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
			if(getRequest().getParameter("ajax") == null){ 
			 getComboList(sessionCorpID);
			 closeCompanyDivision=serviceOrderManager.findClosedCompanyDivision(sessionCorpID);
			 accEstmateStatus  = refMasterManager.findByParameter(sessionCorpID, "AccEstmateStatus");
			 //accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString(); 
			 //costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
			 companyDivisionAcctgCodeUnique="";
			 /*List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
			 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
				 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
			 }*/
			 currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
			 //multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString();
			 getGlTypeList=new ArrayList(); 
			 getCommissionList=new ArrayList();  
			 //systemDefaultmiscVl=serviceOrderManager.findmiscVlSystemDefault(sessionCorpID).get(0).toString();
			 //systemDefaultstorage=serviceOrderManager.findStorageSystemDefault(sessionCorpID).get(0).toString();
			 //systemDefaultCommissionJob=accountLineManager.findCommissionJobSystemDefault(sessionCorpID).get(0).toString();
			 //systemDefaultVatCalculation =accountLineManager.findVatCalculation(sessionCorpID).get(0).toString();
			 company=companyManager.findByCorpID(sessionCorpID).get(0);
			 if(company!=null && company.getOiJob()!=null){
					oiJobList=company.getOiJob();  
				  }
			 if(company!=null){
				 if(company.getEstimateVATFlag()!=null){
						estVatFlag=company.getEstimateVATFlag();
					} 
				 if(company.getUTSI()!=null){
					 networkAgent=company.getUTSI();
					}else{
						networkAgent=false;
					}
				 if(company.getPayablesXferWithApprovalOnly()!=null){
					 payablesXferWithApprovalOnly =company.getPayablesXferWithApprovalOnly();
				 }else{
					 payablesXferWithApprovalOnly =false;
				 }
				 if(company.getVoxmeIntegration()!=null){
						voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
					}
				 if(company.getSingleCompanyDivisionInvoicing()!=null && company.getSingleCompanyDivisionInvoicing()){
					 singleCompanyDivisionInvoicing=company.getSingleCompanyDivisionInvoicing();
				 }
				 if(company.getAccountLineNonEditable()!=null && company.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
					 accNonEditFlag =true;
				 }
				 if(company.getoAdAWeightVolumeMandatoryValidation()!=null){
					 oAdAWeightVolumeValidationForInv=company.getoAdAWeightVolumeMandatoryValidation();
					}
				 if(company.getsubOADASoftValidation()!=null){
					 subOADAValidationForInv=company.getsubOADASoftValidation();
					}
				 compDivFlag = company.getCompanyDivisionFlag();
				 if((company.getCmmdmmAgent() !=null && company.getCmmdmmAgent())){
				  		cmmDmmFlag = true;
				 }
				 if (company != null && company.getDashBoardHideJobs() != null) {
							dashBoardHideJobsList = company.getDashBoardHideJobs();	 
			    } 
				if(company.getMultiCurrency()!=null){
					multiCurrency =company.getMultiCurrency();
				}
				 if(company.getAllowAgentInvoiceUpload()!=null && company.getAllowAgentInvoiceUpload()){
					 allowAgentInvoiceUpload = company.getAllowAgentInvoiceUpload();
				}else{
					allowAgentInvoiceUpload = false;
				}
				
				}
			 try {
			 String permKey = sessionCorpID +"-"+"module.accountLine.section.adminInvoiceAmountEdit.edit"; 
			 boolean visibilityForReceivableDetail=AppInitServlet.roleBasedComponentPerms.containsKey(permKey);
			  if(roles!=null && visibilityForReceivableDetail){
				  for (Role role : roles){				
						if(role.getName().toString().equals("ROLE_ADMIN")){
				            adminInvoiceAmountEdit=true;
						}
				  }
			  }
			 }
			  catch (Exception e) {
				  e.printStackTrace();
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	
				}
			
	
			 systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(sessionCorpID).get(0);
	
						if(systemDefault.getCostingDetailShow()){
							costingDetailShowFlag="Y";
						}
						if(systemDefault.getAutomaticReconcile()!=null && systemDefault.getAutomaticReconcile().booleanValue()){
							automaticReconcile=	systemDefault.getAutomaticReconcile();
						}
						if(systemDefault.getOwnbillingVanline()!=null && systemDefault.getOwnbillingVanline().booleanValue()){
							ownbillingVanline=	systemDefault.getOwnbillingVanline();
						}
						
						driverCommissionSetUp=systemDefault.getDriverCommissionSetUp();
						if(systemDefault.getAccountLineAccountPortalFlag() !=null ){
							accountLineAccountPortalFlag = 	systemDefault.getAccountLineAccountPortalFlag();
						}
						checkContractChargesMandatory=systemDefault.getContractChargesMandatory();
						if(systemDefault.getBaseCurrency() !=null){
						baseCurrency = systemDefault.getBaseCurrency();
						}
						if(systemDefault.getAccountingInterface()!=null){
							accountInterface =systemDefault.getAccountingInterface();
						}
						if(systemDefault.getCostElement()!=null && systemDefault.getCostElement()){
							costElementFlag = systemDefault.getCostElement();
						}else{
							costElementFlag = false;
						}
						if(systemDefault.getCompanyDivisionAcctgCodeUnique()!=null){
							companyDivisionAcctgCodeUnique = systemDefault.getCompanyDivisionAcctgCodeUnique();
						}
						if(systemDefault.getMiscVl()!=null){
							systemDefaultmiscVl=systemDefault.getMiscVl();
						}
						if(systemDefault.getStorage() !=null){
							systemDefaultstorage = systemDefault.getStorage();
						}
						if(systemDefault.getCommissionJob() !=null){
							systemDefaultCommissionJob =systemDefault.getCommissionJob();
						}
						systemDefaultVatCalculation="false";
						if(systemDefault.getVatCalculation()!=null && systemDefault.getVatCalculation()){
							systemDefaultVatCalculation="true";
						}
						
					}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
	    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public  AccountLine getAccountLine() {
		return accountLine;
	}
	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;
		
	}
	
	
	public void setAccountLineManager(
			AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public Billing getBilling() {
		return( billing!=null )?billing:billingManager.get(sid);
	}
	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	
	public void setNotesManager(NotesManager notesManager) {
        this.notesManager = notesManager;
    }
	public String simpleDate()
	{
		Date now= new Date();
		String dateString = now.toString();
		SimpleDateFormat df= new SimpleDateFormat("mm/dd/yyyy");
		String dated=df.format(df);
		return dated;
	}
	@SkipValidation
	public String revisedChargeRadio()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			revisedChargeListRadio=chargesManager.getChargeRadio(charge, billingContract,sessionCorpID);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}

		return SUCCESS;	
	}
	
	@SkipValidation
	public String findRadioValueInternalCost(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			revisedChargeListRadio=chargesManager.findRadioValueInternalCost(charge, accountCompanyDivision,sessionCorpID);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		return SUCCESS;	
	}
	
	@SkipValidation
	public String systemDefaultValue()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			systemDefaultValueList=accountLineManager.findFromSysDefault(sessionCorpID);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}

		return SUCCESS;	
	}
	@SkipValidation
	public String  findInternalCostVendorCode(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			List InternalCostVendorCodeList=accountLineManager.findInternalCostVendorCode(sessionCorpID,companyDivision);
			systemDefaultValueList=new ArrayList();
			systemDefaultValueList.add(InternalCostVendorCodeList.get(0));
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	private String revisedChargeValue;
	
	@SkipValidation
	public String revisedCharge()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			revisedChargeList=chargesManager.getCharge(charge, billingContract,sessionCorpID);
			String wording="";
			if((revisedChargeList!=null && !revisedChargeList.isEmpty()) && !(revisedChargeList.get(0) == null)){
				String newfieldName="";
				String qty;
				String QuantityBuy 	;
				String totalRevenue="";
				String buyRate="0";
				billing=billingManager.get(sid);
				serviceOrder=serviceOrderManager.get(sid);
				String distanceInKmMiles="";
				if(serviceOrder.getDistanceInKmMiles()!=null){
				distanceInKmMiles =serviceOrder.getDistanceInKmMiles();
				}
				double distance=0.00;
				BigDecimal soDistance=new BigDecimal(0.00); 
				if(serviceOrder.getDistance()!=null){
					soDistance=serviceOrder.getDistance();
				}
				distance=Double.parseDouble(soDistance.toString());
				String[] str1 = revisedChargeList.get(0).toString().split("#");
				newfieldName= str1[7];
				String minimum=str1[13];
				String twoDGridUnit="";
				if(str1[15]!=null && (!(str1[15].toString().trim().equals("")))){
				twoDGridUnit =str1[15];
				}else{
				twoDGridUnit="ND";	
				}
				String multquantity="";
				if(str1[16]!=null && (!(str1[16].toString().trim().equals("")))){
				multquantity =str1[16];
				}else{
				multquantity ="P";	
				}
				String destinationCountryCode=serviceOrder.getDestinationCountryCode();
				String originCountryCode=serviceOrder.getOriginCountryCode();
				String chargedeviation="";
				if(str1[17]!=null && (!(str1[17].toString().trim().equals("")))){
					chargedeviation =str1[17];
				}else{
					chargedeviation ="NCT";	
				}
				String buyDependSell="N";
				if(str1[18]!=null && (!(str1[18].toString().trim().equals("")))){
					buyDependSell =str1[18];
				}else{
					buyDependSell ="N";	
				}
				String VATExclude="N";
				if(str1[19]!=null && (!(str1[19].toString().trim().equals("")))){
					VATExclude =str1[19];
				}else{
					VATExclude ="N";	
				}
				String contractCurrency="";
				try{
				if(str1[20]!=null && (!(str1[20].toString().trim().equals("")))){
					contractCurrency =str1[20];
				}else{
					contractCurrency ="";	
				}}catch(Exception e){
					 e.printStackTrace();
				}
				String payableContractCurrency="";
				try{
				if(str1[21]!=null && (!(str1[21].toString().trim().equals("")))){
					payableContractCurrency =str1[21];
				}else{
					payableContractCurrency ="";	
				}}catch(Exception e){
					 e.printStackTrace();
				}
				try{
					
			   		if(str1[22]!=null && (!(str1[22].toString().trim().equals("")))){
			   			buyRate =str1[22];
			   		}else{
			   			buyRate ="0";	
			   		}}catch(Exception e){
			   			buyRate ="0";	e.printStackTrace();
			   		}
				
				if(newfieldName.trim().equalsIgnoreCase("")){				
				}
				else
				{
					quantity=chargesManager.getQuantity(newfieldName, shipNum);
				}
				
				if(str1[8].equalsIgnoreCase("Preset2")){
					qty = str1[0];
				}else{
					if(newfieldName.trim().equalsIgnoreCase("")){
						qty="";	
					}else{
						qty = quantity.get(0).toString();	
					}
				}
				QuantityBuy=qty;
			List chargeid=chargesManager.findChargeId(charge, billingContract);		
			String rg="";
			String rg1="";
			List buyRateList=null;
			if(str1[9].equalsIgnoreCase("RateGrid")){
				try{
					if((twoDGridUnit.equalsIgnoreCase("DMI")) && distanceInKmMiles.equalsIgnoreCase("Km"))	{
						distance=distance/1.609;
					}
					if((twoDGridUnit.equalsIgnoreCase("DKM")) && distanceInKmMiles.equalsIgnoreCase("Mile"))	{
						distance=distance*1.609;
					}
					String stringDistance=""+distance; 
			List rate = chargesManager.findRateFromRateGrid(qty,chargeid.get(0).toString(),multquantity,stringDistance,twoDGridUnit);
			List checkBreakPoints=chargesManager.getBreakPoints(chargeid.get(0).toString(), sessionCorpID);	
			if(checkBreakPoints.get(0).toString().equals("false") && (!(multquantity.equals("N"))) && (!(multquantity.equals("B"))))
				{
				
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				List firstQuantity=chargesManager.getFirstQuantity(chargeid.get(0).toString(), sessionCorpID,multquantity,stringDistance,qty);
				
				if(Double.parseDouble(qty) < Double.parseDouble(firstQuantity.get(0).toString()))
				{
					qty=rate.get(2).toString();
				}
			
				}
				else
				{
				}
			}
			else{
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				qty=rate.get(2).toString();
				}
				else
				{
				}	
			}
			
			buyRateList = chargesManager.findBuyRateFromRateGrid(QuantityBuy,chargeid.get(0).toString(),multquantity,stringDistance,twoDGridUnit);
			if(buyRateList!=null && !buyRateList.isEmpty())
			{
				buyRate=buyRateList.get(0).toString();
			}
			}catch(Exception ex)
			{
				rg="0";
				rg1="";
				qty="0";
				ex.printStackTrace();
			}		
			}
			else{
				rg=str1[1];			
			}
			try{
				if(str1[23].equalsIgnoreCase("Preset")){ 
			   		if(str1[22]!=null && (!(str1[22].toString().trim().equals("")))){
			   			buyRate =str1[22];
			   		}else{
			   			buyRate ="0";	
			   		}
				}else{
					if(str1[23].equalsIgnoreCase("BuildFormula")){
						String expression=str1[24];
						String replacement=str1[7];
						String newEpression=expression;
						if(expression.trim().contains("FromField")){
						newEpression=expression.replaceAll("FromField", replacement);
						}
						Map values = new HashMap();			
						values.put(replacement, new BigDecimal(qty));
						 values.put("charges.minimum",minimum);
						 values.put("billing.onHand",billing.getOnHand());
						 values.put("billing.cycle",billing.getCycle());
						 values.put("billing.postGrate",billing.getPostGrate());
						 values.put("billing.insuranceRate",billing.getInsuranceRate());
						 values.put("billing.insuranceValueActual",billing.getInsuranceValueActual());
						 values.put("billing.baseInsuranceValue",billing.getBaseInsuranceValue());
						 values.put("billing.vendorStoragePerMonth", billing.getVendorStoragePerMonth());
					     values.put("billing.storagePerMonth", billing.getStoragePerMonth());
					     values.put("billing.totalInsrVal", billing.getTotalInsrVal());
					     values.put("billing.insurancePerMonth", billing.getInsurancePerMonth());
					     values.put("billing.insuranceValueEntitle", billing.getInsuranceValueEntitle());
					     values.put("billing.payableRate", billing.getPayableRate());
					     values.put("billing.storageMeasurement", billing.getStorageMeasurement());
					     values.put("accountLine.actualRevenue", buildFormulaActualRevenue);
					     values.put("serviceOrder.estimatedTotalRevenue", serviceOrder.getEstimatedTotalRevenue()); 
					     Object totRev = expressionManager.executeExpression(newEpression, values);
						buyRate=totRev.toString();			
					}
				}
				}catch(Exception e){
					buyRate ="0";
					e.printStackTrace();
				}
			if(str1[9].equalsIgnoreCase("BuildFormula")){
				try{
				String expression=str1[10];
				String replacement=str1[7];
				String newEpression=expression;
				if(expression.trim().contains("FromField")){
				newEpression=expression.replaceAll("FromField", replacement);
				}
				Map values = new HashMap();
				values.put(replacement, new BigDecimal(qty));
				 values.put("charges.minimum",minimum);
				 values.put("billing.onHand",billing.getOnHand());
				 values.put("billing.cycle",billing.getCycle());
				 values.put("billing.postGrate",billing.getPostGrate());
				 values.put("billing.insuranceRate",billing.getInsuranceRate());
				 values.put("billing.insuranceValueActual",billing.getInsuranceValueActual());
				 values.put("billing.baseInsuranceValue",billing.getBaseInsuranceValue()); 
				 values.put("billing.vendorStoragePerMonth", billing.getVendorStoragePerMonth());
				 values.put("billing.storagePerMonth", billing.getStoragePerMonth());
				 values.put("billing.totalInsrVal", billing.getTotalInsrVal());
				 values.put("billing.insurancePerMonth", billing.getInsurancePerMonth());
				 values.put("billing.insuranceValueEntitle", billing.getInsuranceValueEntitle());
				 values.put("billing.payableRate", billing.getPayableRate());
				 values.put("billing.storageMeasurement", billing.getStorageMeasurement());
				 values.put("accountLine.actualRevenue", buildFormulaActualRevenue);
				 values.put("serviceOrder.estimatedTotalRevenue", serviceOrder.getEstimatedTotalRevenue());
				Object totRev = expressionManager.executeExpression(newEpression, values);
				totalRevenue=totRev.toString();			
			}
			catch(Exception e){
				totalRevenue ="0";
				e.printStackTrace();
			}
			}
			
			String sellDeviation="100";
			String buyDeviation="100";
			if(!chargedeviation.equalsIgnoreCase("NCT")){
				if(chargedeviation.equalsIgnoreCase("OCT")){
					sellDeviation=chargesManager.findDeviation(chargeid.get(0).toString(),originCountryCode,"N",sessionCorpID);	
					buyDeviation=chargesManager.findDeviation(chargeid.get(0).toString(),originCountryCode,"Y",sessionCorpID);
				}
				if(chargedeviation.equalsIgnoreCase("DCT")){
					sellDeviation=chargesManager.findDeviation(chargeid.get(0).toString(),destinationCountryCode,"N",sessionCorpID);	
					buyDeviation=chargesManager.findDeviation(chargeid.get(0).toString(),destinationCountryCode,"Y",sessionCorpID);
				}
				if(chargedeviation.equalsIgnoreCase("MCT")){
					sellDeviation=chargesManager.findMaxDeviation(chargeid.get(0).toString(),originCountryCode,destinationCountryCode,"N",sessionCorpID);	
					buyDeviation=chargesManager.findMaxDeviation(chargeid.get(0).toString(),originCountryCode,destinationCountryCode,"Y",sessionCorpID);
				}
				if(chargedeviation.equalsIgnoreCase("SCT")){
					sellDeviation=chargesManager.findSumDeviation(chargeid.get(0).toString(),originCountryCode,destinationCountryCode,"N",sessionCorpID); 
					buyDeviation=chargesManager.findSumDeviation(chargeid.get(0).toString(),originCountryCode,destinationCountryCode,"Y",sessionCorpID); 
				}
			}
			
				Charges charge1=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
				Miscellaneous miscellaneous=miscellaneousManager.get(sid);
				  try {

						Map wordingValues = new HashMap();
						
						if (billing.getOnHand() != null) {
							wordingValues.put("billing.onHand", new BigDecimal(billing.getOnHand().toString()));
						} else {
							wordingValues.put("billing.onHand", 0.00);
						}
						if (charge1.getPerValue() != null) {
							wordingValues.put("charges.perValue", new BigDecimal(charge1.getPerValue().toString()));
						} else {
							wordingValues.put("charges.perValue", 0.00);
						}
						if (billing.getCycle() != 0 ) {
							String cy=billing.getCycle()+"";
							wordingValues.put("billing.cycle", new BigDecimal(cy));
						} else {
							wordingValues.put("billing.cycle", 0.00);
						}
						if (billing.getInsuranceRate() != null) {
							wordingValues.put("billing.insuranceRate", (BigDecimal) billing.getInsuranceRate());
						} else {
							wordingValues.put("billing.insuranceRate", 0.00);
						}
						if (billing.getInsuranceValueActual() != null) {
							wordingValues.put("billing.insuranceValueActual", (BigDecimal) billing.getInsuranceValueActual());
						} else {
							wordingValues.put("billing.insuranceValueActual", 0.00);
						}
						if (billing.getBaseInsuranceValue() != null) {
							wordingValues.put("billing.baseInsuranceValue", (BigDecimal) billing.getBaseInsuranceValue());
						} else {
							wordingValues.put("billing.baseInsuranceValue", 0.00);
						}
						if (charge1.getMinimum() != null) {
							wordingValues.put("charges.minimum", (BigDecimal) charge1.getMinimum());
						} else {
							wordingValues.put("charges.minimum", 0.00);
						}
						if (billing.getPostGrate() != null) {
							wordingValues.put("billing.postGrate", (BigDecimal) billing.getPostGrate().setScale(2,BigDecimal.ROUND_HALF_UP));
						} else {
							wordingValues.put("billing.postGrate", 0.00000);
						}
						if (charge1.getDescription() != null) {
							wordingValues.put("charges.description", charge1.getDescription().toString());
						} else {
							wordingValues.put("charges.description", "");
						}
						if (charge1.getUseDiscount() != null) {
							wordingValues.put("charges.useDiscount", charge1.getUseDiscount().toString());
						} else {
							wordingValues.put("charges.useDiscount", "");
						}
						if (charge1.getPricePre() != 0.00) {
							wordingValues.put("charges.pricepre", charge1.getPricePre());
						} else {
							wordingValues.put("charges.pricepre", "");
						}
						if (charge1.getPerItem() != null) {
							wordingValues.put("form.qty", charge1.getPerItem().toString());
						} else {
							wordingValues.put("form.qty", 0.0);
						}
						if(miscellaneous.getActualNetWeight()!=null){
							wordingValues.put("miscellaneous.actualNetWeight", new BigDecimal(miscellaneous.getActualNetWeight().toString()));
						}else{
							wordingValues.put("miscellaneous.actualNetWeight", 0.00);								
						}
						if(miscellaneous.getActualGrossWeight()!=null){
							wordingValues.put("miscellaneous.actualGrossWeight", new BigDecimal(miscellaneous.getActualGrossWeight().toString()));								
						}else{
							wordingValues.put("miscellaneous.actualGrossWeight", 0.00);
						}
						if(miscellaneous.getEstimateGrossWeight()!=null){
							wordingValues.put("miscellaneous.estimateGrossWeight", new BigDecimal(miscellaneous.getEstimateGrossWeight().toString()));								
						}else{
							wordingValues.put("miscellaneous.estimateGrossWeight", 0.00);
						}
						if(miscellaneous.getEstimatedNetWeight()!=null){
							wordingValues.put("miscellaneous.estimatedNetWeight", new BigDecimal(miscellaneous.getEstimatedNetWeight().toString()));								
						}else{
							wordingValues.put("miscellaneous.estimatedNetWeight", 0.00);
						}
						if(miscellaneous.getEstimateGrossWeightKilo()!=null){
							wordingValues.put("miscellaneous.estimateGrossWeightKilo", new BigDecimal(miscellaneous.getEstimateGrossWeightKilo().toString()));								
						}else{
							wordingValues.put("miscellaneous.estimateGrossWeightKilo", 0.00);
						}
						if(miscellaneous.getEstimatedNetWeightKilo()!=null){
							wordingValues.put("miscellaneous.estimatedNetWeightKilo", new BigDecimal(miscellaneous.getEstimatedNetWeightKilo().toString()));								
						}else{
							wordingValues.put("miscellaneous.estimatedNetWeightKilo", 0.00);
						}
						if(miscellaneous.getChargeableGrossWeight()!=null){
							wordingValues.put("miscellaneous.chargeableGrossWeight", new BigDecimal(miscellaneous.getChargeableGrossWeight().toString()));								
						}else{
							wordingValues.put("miscellaneous.chargeableGrossWeight", 0.00);
						}
						if(miscellaneous.getChargeableNetWeight()!=null){
							wordingValues.put("miscellaneous.chargeableNetWeight", new BigDecimal(miscellaneous.getChargeableNetWeight().toString()));								
						}else{
							wordingValues.put("miscellaneous.chargeableNetWeight", 0.00);
						}
						if(miscellaneous.getActualCubicFeet()!=null){
							wordingValues.put("miscellaneous.actualCubicFeet", new BigDecimal(miscellaneous.getActualCubicFeet().toString()));								
						}else{
							wordingValues.put("miscellaneous.actualCubicFeet", 0.00);
						}
						  if(miscellaneous.getNetActualCubicFeet()!=null){
								wordingValues.put("miscellaneous.netActualCubicFeet", new BigDecimal(miscellaneous.getNetActualCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.netActualCubicFeet", 0.00);
							}
						
						if(miscellaneous.getActualNetWeightKilo()!=null){
							wordingValues.put("miscellaneous.actualNetWeightKilo", new BigDecimal(miscellaneous.getActualNetWeightKilo().toString()));								
						}else{
							wordingValues.put("miscellaneous.actualNetWeightKilo", 0.00);
						}
						if(miscellaneous.getActualGrossWeightKilo()!=null){
							wordingValues.put("miscellaneous.actualGrossWeightKilo", new BigDecimal(miscellaneous.getActualGrossWeightKilo().toString()));								
						}else{
							wordingValues.put("miscellaneous.actualGrossWeightKilo", 0.00);
						}
						if(miscellaneous.getActualCubicMtr()!=null){
							wordingValues.put("miscellaneous.actualCubicMtr", new BigDecimal(miscellaneous.getActualCubicMtr().toString()));								
						}else{
							wordingValues.put("miscellaneous.actualCubicMtr", 0.00);
						}
						if(miscellaneous.getNetActualCubicMtr()!=null){
							wordingValues.put("miscellaneous.netActualCubicMtr", new BigDecimal(miscellaneous.getNetActualCubicMtr().toString()));								
						}else{
							wordingValues.put("miscellaneous.netActualCubicMtr", 0.00);
						}
						
						if(miscellaneous.getEstimateCubicFeet()!=null){
							wordingValues.put("miscellaneous.estimateCubicFeet", new BigDecimal(miscellaneous.getEstimateCubicFeet().toString()));								
						}else{
							wordingValues.put("miscellaneous.estimateCubicFeet", 0.00);
						}
						if(miscellaneous.getNetEstimateCubicFeet()!=null){
							wordingValues.put("miscellaneous.netEstimateCubicFeet", new BigDecimal(miscellaneous.getNetEstimateCubicFeet().toString()));								
						}else{
							wordingValues.put("miscellaneous.netEstimateCubicFeet", 0.00);
						}
						if(miscellaneous.getEstimateCubicMtr()!=null){
							wordingValues.put("miscellaneous.estimateCubicMtr", new BigDecimal(miscellaneous.getEstimateCubicMtr().toString()));								
						}else{
							wordingValues.put("miscellaneous.estimateCubicMtr", 0.00);
						}
						if(miscellaneous.getNetEstimateCubicMtr()!=null){
							wordingValues.put("miscellaneous.netEstimateCubicMtr", new BigDecimal(miscellaneous.getNetEstimateCubicMtr().toString()));								
						}else{
							wordingValues.put("miscellaneous.netEstimateCubicMtr", 0.00);
						}
						if(miscellaneous.getChargeableCubicFeet()!=null){
							wordingValues.put("miscellaneous.chargeableCubicFeet", new BigDecimal(miscellaneous.getChargeableCubicFeet().toString()));								
						}else{
							wordingValues.put("miscellaneous.chargeableCubicFeet", 0.00);
						}
						if(miscellaneous.getChargeableNetCubicFeet()!=null){
							wordingValues.put("miscellaneous.chargeableNetCubicFeet", new BigDecimal(miscellaneous.getChargeableNetCubicFeet().toString()));								
						}else{
							wordingValues.put("miscellaneous.chargeableNetCubicFeet", 0.00);
						}
						if(miscellaneous.getChargeableCubicMtr()!=null){
							wordingValues.put("miscellaneous.chargeableCubicMtr", new BigDecimal(miscellaneous.getChargeableCubicMtr().toString()));								
						}else{
							wordingValues.put("miscellaneous.chargeableCubicMtr", 0.00);
						}
						if(miscellaneous.getChargeableNetCubicMtr()!=null){
							wordingValues.put("miscellaneous.chargeableNetCubicMtr", new BigDecimal(miscellaneous.getChargeableNetCubicMtr().toString()));								
						}else{
							wordingValues.put("miscellaneous.chargeableNetCubicMtr", 0.00);
						}
						
 						if(miscellaneous.getActualAutoWeight()!=null){
 							wordingValues.put("miscellaneous.actualAutoWeight", new BigDecimal(miscellaneous.getActualAutoWeight().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.actualAutoWeight", 0.00);
 						}	
 						if(miscellaneous.getActualAutoWeight()!=null){
 							wordingValues.put("miscellaneous.actualAutoWeight", new BigDecimal(miscellaneous.getActualAutoWeight().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.actualAutoWeight", 0.00);
 						}	
 						if(billing.getPackDiscount()!=null){
 							wordingValues.put("billing.packDiscount", new BigDecimal(billing.getPackDiscount().toString()));								
 						}else{
 							wordingValues.put("billing.packDiscount", 0.00);
 						}
 						if(billing.getStorageMeasurement()!=null){
 							wordingValues.put("billing.storageMeasurement", billing.getStorageMeasurement().toString());								
 						}else{
 							wordingValues.put("billing.storageMeasurement", "");
 						}
 						if(miscellaneous.getChargeableGrossWeightKilo()!=null){
 							wordingValues.put("miscellaneous.chargeableGrossWeightKilo", new BigDecimal(miscellaneous.getChargeableGrossWeightKilo().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.chargeableGrossWeightKilo", 0.00);
 						}
 						if(miscellaneous.getEntitleGrossWeight()!=null){
 							wordingValues.put("miscellaneous.entitleGrossWeight", new BigDecimal(miscellaneous.getEntitleGrossWeight().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.entitleGrossWeight", 0.00);
 						}
 						if(miscellaneous.getEntitleTareWeight()!=null){
 							wordingValues.put("miscellaneous.entitleTareWeight", new BigDecimal(miscellaneous.getEntitleTareWeight().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.entitleTareWeight", 0.00);
 						}
 						if(miscellaneous.getEstimateTareWeight()!=null){
 							wordingValues.put("miscellaneous.estimateTareWeight", new BigDecimal(miscellaneous.getEstimateTareWeight().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.estimateTareWeight", 0.00);
 						}
 						if(miscellaneous.getActualTareWeight()!=null){
 							wordingValues.put("miscellaneous.actualTareWeight", new BigDecimal(miscellaneous.getActualTareWeight().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.actualTareWeight", 0.00);
 						}
 						if(miscellaneous.getChargeableTareWeight()!=null){
 							wordingValues.put("miscellaneous.chargeableTareWeight", new BigDecimal(miscellaneous.getChargeableTareWeight().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.chargeableTareWeight", 0.00);
 						}
 						if(miscellaneous.getEntitleNetWeight()!=null){
 							wordingValues.put("miscellaneous.entitleNetWeight", new BigDecimal(miscellaneous.getEntitleNetWeight().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.entitleNetWeight", 0.00);
 						}
 						if(miscellaneous.getEntitleCubicFeet()!=null){
 							wordingValues.put("miscellaneous.entitleCubicFeet", new BigDecimal(miscellaneous.getEntitleCubicFeet().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.entitleCubicFeet", 0.00);
 						}
 						if(miscellaneous.getNetEntitleCubicFeet()!=null){
 							wordingValues.put("miscellaneous.netEntitleCubicFeet", new BigDecimal(miscellaneous.getNetEntitleCubicFeet().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.netEntitleCubicFeet", 0.00);
 						}
 						if(miscellaneous.getRwghGross()!=null){
 							wordingValues.put("miscellaneous.rwghGross", new BigDecimal(miscellaneous.getRwghGross().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.rwghGross", 0.00);
 						}
 						if(miscellaneous.getRwghTare()!=null){
 							wordingValues.put("miscellaneous.rwghTare", new BigDecimal(miscellaneous.getRwghTare().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.rwghTare", 0.00);
 						}
 						if(miscellaneous.getRwghNet()!=null){
 							wordingValues.put("miscellaneous.rwghNet", new BigDecimal(miscellaneous.getRwghNet().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.rwghNet", 0.00);
 						}
 						if(miscellaneous.getRwghCubicFeet()!=null){
 							wordingValues.put("miscellaneous.rwghCubicFeet", new BigDecimal(miscellaneous.getRwghCubicFeet().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.rwghCubicFeet", 0.00);
 						}
 						if(miscellaneous.getRwghNetCubicFeet()!=null){
 							wordingValues.put("miscellaneous.rwghNetCubicFeet", new BigDecimal(miscellaneous.getRwghNetCubicFeet().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.rwghNetCubicFeet", 0.00);
 						}
 						if(miscellaneous.getEntitleGrossWeightKilo()!=null){
 							wordingValues.put("miscellaneous.entitleGrossWeightKilo", new BigDecimal(miscellaneous.getEntitleGrossWeightKilo().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.entitleGrossWeightKilo", 0.00);
 						}
 						if(miscellaneous.getEntitleTareWeightKilo()!=null){
 							wordingValues.put("miscellaneous.entitleTareWeightKilo", new BigDecimal(miscellaneous.getEntitleTareWeightKilo().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.entitleTareWeightKilo", 0.00);
 						}
 						if(miscellaneous.getEstimateTareWeightKilo()!=null){
 							wordingValues.put("miscellaneous.estimateTareWeightKilo", new BigDecimal(miscellaneous.getEstimateTareWeightKilo().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.estimateTareWeightKilo", 0.00);
 						}
 						if(miscellaneous.getActualTareWeightKilo()!=null){
 							wordingValues.put("miscellaneous.actualTareWeightKilo", new BigDecimal(miscellaneous.getActualTareWeightKilo().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.actualTareWeightKilo", 0.00);
 						}
 						if(miscellaneous.getChargeableTareWeightKilo()!=null){
 							wordingValues.put("miscellaneous.chargeableTareWeightKilo", new BigDecimal(miscellaneous.getChargeableTareWeightKilo().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.chargeableTareWeightKilo", 0.00);
 						}
 						if(miscellaneous.getEntitleNetWeightKilo()!=null){
 							wordingValues.put("miscellaneous.entitleNetWeightKilo", new BigDecimal(miscellaneous.getEntitleNetWeightKilo().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.entitleNetWeightKilo", 0.00);
 						}
 						if(miscellaneous.getEntitleCubicMtr()!=null){
 							wordingValues.put("miscellaneous.entitleCubicMtr", new BigDecimal(miscellaneous.getEntitleCubicMtr().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.entitleCubicMtr", 0.00);
 						}
 						if(miscellaneous.getNetEntitleCubicMtr()!=null){
 							wordingValues.put("miscellaneous.netEntitleCubicMtr", new BigDecimal(miscellaneous.getNetEntitleCubicMtr().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.netEntitleCubicMtr", 0.00);
 						}
 						if(miscellaneous.getRwghGrossKilo()!=null){
 							wordingValues.put("miscellaneous.rwghGrossKilo", new BigDecimal(miscellaneous.getRwghGrossKilo().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.rwghGrossKilo", 0.00);
 						}
 						if(miscellaneous.getRwghTareKilo()!=null){
 							wordingValues.put("miscellaneous.rwghTareKilo", new BigDecimal(miscellaneous.getRwghTareKilo().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.rwghTareKilo", 0.00);
 						}
 						if(miscellaneous.getRwghNetKilo()!=null){
 							wordingValues.put("miscellaneous.rwghNetKilo", new BigDecimal(miscellaneous.getRwghNetKilo().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.rwghNetKilo", 0.00);
 						}
 						if(miscellaneous.getRwghCubicMtr()!=null){
 							wordingValues.put("miscellaneous.rwghCubicMtr", new BigDecimal(miscellaneous.getRwghCubicMtr().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.rwghCubicMtr", 0.00);
 						}
 						if(miscellaneous.getRwghNetCubicMtr()!=null){
 							wordingValues.put("miscellaneous.rwghNetCubicMtr", new BigDecimal(miscellaneous.getRwghNetCubicMtr().toString()));								
 						}else{
 							wordingValues.put("miscellaneous.rwghNetCubicMtr", 0.00);
 						}
 						if(miscellaneous.getChargeableNetWeightKilo()!=null){
 							wordingValues.put("miscellaneous.chargeableNetWeightKilo", new BigDecimal(miscellaneous.getChargeableNetWeightKilo().toString()));
 							}else{
 							wordingValues.put("miscellaneous.chargeableNetWeightKilo", 0.00);
 							}
 						
   			
 						wordingValues.put("form.price", 0);
 						wordingValues.put("form.extra", 0);
 						wordingValues.put("nprice", 0);
 						wordingValues.put("form.perqty", 0);
						String chargeWordingTemp=charge1.getWording().toString();
						while(chargeWordingTemp.indexOf('$')>-1)
							chargeWordingTemp=chargeWordingTemp.replace('$', '~');
						wording = expressionManager.executeExpression(chargeWordingTemp, wordingValues).toString();
						while(wording.indexOf('~')>-1)
							wording=wording.replace('~', '$');
					} catch (Exception ex) {
					}
				
			
			revisedChargeValue=str1[0]+"#"+str1[1]+"#"+str1[2]+"#"+str1[3]+"#"+wording+"#"+str1[5]+"#"+str1[6]+"#"+qty+"#"+rg+"#"+str1[8]+"#"+str1[9]+"#"+totalRevenue+"#"+rg1+"#"+str1[11]+"#"+str1[12]+"#"+buyDependSell+"#"+sellDeviation+"#"+buyDeviation+"#"+chargedeviation+"#"+VATExclude+"#"+buyRate+"#"+contractCurrency+"#"+payableContractCurrency;
			
			
				
				}
			else{
			    }
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String payableCharge(){

		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			revisedChargeList=chargesManager.getCharge(charge, billingContract,sessionCorpID);
			String newfieldName="";
			String qty;
			String QuantityBuy;
			String totalRevenue="";
			String buyRate="0";
			billing=billingManager.get(sid);
			serviceOrder=serviceOrderManager.get(sid);
			String distanceInKmMiles="";
			if(serviceOrder.getDistanceInKmMiles()!=null){
			distanceInKmMiles =serviceOrder.getDistanceInKmMiles();
			}
			double distance=0.00;
			BigDecimal soDistance=new BigDecimal(0.00); 
			if(serviceOrder.getDistance()!=null){
				soDistance=serviceOrder.getDistance();
			}
			distance=Double.parseDouble(soDistance.toString());
			if(!(revisedChargeList.get(0) == null)){
				String[] str1 = revisedChargeList.get(0).toString().split("#");
				newfieldName= str1[7];
				String minimum=str1[13];
				String twoDGridUnit="";
				if(str1[15]!=null && (!(str1[15].toString().trim().equals("")))){
				twoDGridUnit =str1[15];
				}else{
				twoDGridUnit="ND";	
				}
				String multquantity="";
				if(str1[16]!=null && (!(str1[16].toString().trim().equals("")))){
				multquantity =str1[16];
				}else{
				multquantity ="P";	
				}
				String destinationCountryCode=serviceOrder.getDestinationCountryCode();
				String originCountryCode=serviceOrder.getOriginCountryCode();
				String chargedeviation="";
				if(str1[17]!=null && (!(str1[17].toString().trim().equals("")))){
					chargedeviation =str1[17];
				}else{
					chargedeviation ="NCT";	
				}
				String buyDependSell="N";
				if(str1[18]!=null && (!(str1[18].toString().trim().equals("")))){
					buyDependSell =str1[18];
				}else{
					buyDependSell ="N";	
				}
				String VATExclude="N";
				if(str1[19]!=null && (!(str1[19].toString().trim().equals("")))){
					VATExclude =str1[19];
				}else{
					VATExclude ="N";	
				}
				String contractCurrency="";
				try{
				if(str1[20]!=null && (!(str1[20].toString().trim().equals("")))){
					contractCurrency =str1[20];
				}else{
					contractCurrency ="";	
				}}catch(Exception e){
					 e.printStackTrace();
				}
				String payableContractCurrency="";
				try{
				if(str1[21]!=null && (!(str1[21].toString().trim().equals("")))){
					payableContractCurrency =str1[21];
				}else{
					payableContractCurrency ="";	
				}}catch(Exception e){
					 e.printStackTrace();
				}
				try{
			   		if(str1[22]!=null && (!(str1[22].toString().trim().equals("")))){
			   			buyRate =str1[22];
			   		}else{
			   			buyRate ="0";	
			   		}}catch(Exception e){
			   			buyRate ="0";
			   			e.printStackTrace();
			   		}
				if(newfieldName.trim().equalsIgnoreCase("")){
					
				}
				else
				{
					quantity=chargesManager.getQuantity(newfieldName, shipNum);
				}
				
				if(str1[8].equalsIgnoreCase("Preset2")){
					qty = str1[0];
				}else{
					if(newfieldName.trim().equalsIgnoreCase("")){
						qty="";	
					}else{
						qty = quantity.get(0).toString();	
					}
				}
				QuantityBuy=qty;
			
			List chargeid=chargesManager.findChargeId(charge, billingContract);		
			String rg="";
			String rg1="";
			List buyRateList=null;
			if(str1[9].equalsIgnoreCase("RateGrid")){
				try{
					if((twoDGridUnit.equalsIgnoreCase("DMI")) && distanceInKmMiles.equalsIgnoreCase("Km"))	{
						distance=distance/1.609;
					}
					if((twoDGridUnit.equalsIgnoreCase("DKM")) && distanceInKmMiles.equalsIgnoreCase("Mile"))	{
						distance=distance*1.609;
					}
					String stringDistance=""+distance; 
			List rate = chargesManager.findRateFromRateGrid(qty,chargeid.get(0).toString(),multquantity,stringDistance,twoDGridUnit);
			List checkBreakPoints=chargesManager.getBreakPoints(chargeid.get(0).toString(), sessionCorpID);	
			if(checkBreakPoints.get(0).toString().equals("false") && (!(multquantity.equals("N"))) && (!(multquantity.equals("B"))))
				{
				
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				List firstQuantity=chargesManager.getFirstQuantity(chargeid.get(0).toString(), sessionCorpID,multquantity,stringDistance,qty);
				
				if(Double.parseDouble(qty) < Double.parseDouble(firstQuantity.get(0).toString()))
				{
					qty=rate.get(2).toString();
				}
			
				}
				else
				{
				}
			}
			else{
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				qty=rate.get(2).toString();
				}
				else
				{
				}	
			}
			
			buyRateList = chargesManager.findBuyRateFromRateGrid(QuantityBuy,chargeid.get(0).toString(),multquantity,stringDistance,twoDGridUnit);
			if(buyRateList!=null && !buyRateList.isEmpty())
			{
				buyRate=buyRateList.get(0).toString();
			}
			}catch(Exception ex)
			{
				rg="0";
				rg1="";
				qty="0";
				ex.printStackTrace();
			}
			}
			else{
				rg=str1[1];			
			}
			try{
				if(str1[23].equalsIgnoreCase("Preset")){ 
			   		if(str1[22]!=null && (!(str1[22].toString().trim().equals("")))){
			   			buyRate =str1[22];
			   		}else{
			   			buyRate ="0";	
			   		}
				}else{
					if(str1[23].equalsIgnoreCase("BuildFormula")){
						String expression=str1[24];
						String replacement=str1[7];
						String newEpression=expression;
						if(expression.trim().contains("FromField")){
						newEpression=expression.replaceAll("FromField", replacement);
						}
						Map values = new HashMap();			
						values.put(replacement, new BigDecimal(qty));
						 values.put("charges.minimum",minimum);
						 values.put("billing.onHand",billing.getOnHand());
						 values.put("billing.cycle",billing.getCycle());
						 values.put("billing.postGrate",billing.getPostGrate());
						 values.put("billing.insuranceRate",billing.getInsuranceRate());
						 values.put("billing.insuranceValueActual",billing.getInsuranceValueActual());
						 values.put("billing.baseInsuranceValue",billing.getBaseInsuranceValue()); 
						 values.put("billing.vendorStoragePerMonth", billing.getVendorStoragePerMonth());
					     values.put("billing.storagePerMonth", billing.getStoragePerMonth());
					     values.put("billing.totalInsrVal", billing.getTotalInsrVal());
					     values.put("billing.insurancePerMonth", billing.getInsurancePerMonth());
					     values.put("billing.insuranceValueEntitle", billing.getInsuranceValueEntitle());
					     values.put("billing.payableRate", billing.getPayableRate());
					     values.put("billing.storageMeasurement", billing.getStorageMeasurement());
					     values.put("accountLine.actualRevenue", buildFormulaActualRevenue);
					     values.put("serviceOrder.estimatedTotalRevenue", serviceOrder.getEstimatedTotalRevenue());
						Object totRev = expressionManager.executeExpression(newEpression, values);
						buyRate=totRev.toString();			
					}
				}}catch(Exception e){
					buyRate ="0";	
					e.printStackTrace();
				}
			//}
			if(str1[9].equalsIgnoreCase("BuildFormula")){
				String expression=str1[10];
				String replacement=str1[7];
				String newEpression=expression;
				if(expression.trim().contains("FromField")){
				newEpression=expression.replaceAll("FromField", replacement);
				}
				Map values = new HashMap();
				values.put(replacement, new BigDecimal(qty));
				 values.put("charges.minimum",minimum);
				 values.put("billing.onHand",billing.getOnHand());
				 values.put("billing.cycle",billing.getCycle());
				 values.put("billing.postGrate",billing.getPostGrate());
				 values.put("billing.insuranceRate",billing.getInsuranceRate());
				 values.put("billing.insuranceValueActual",billing.getInsuranceValueActual());
				 values.put("billing.baseInsuranceValue",billing.getBaseInsuranceValue()); 
				 values.put("billing.vendorStoragePerMonth", billing.getVendorStoragePerMonth());
				 values.put("billing.storagePerMonth", billing.getStoragePerMonth());
				 values.put("billing.totalInsrVal", billing.getTotalInsrVal());
				 values.put("billing.insurancePerMonth", billing.getInsurancePerMonth());
				 values.put("billing.insuranceValueEntitle", billing.getInsuranceValueEntitle());
				 values.put("billing.payableRate", billing.getPayableRate());
				 values.put("billing.storageMeasurement", billing.getStorageMeasurement());
				 values.put("accountLine.actualRevenue", buildFormulaActualRevenue);
				 values.put("serviceOrder.estimatedTotalRevenue", serviceOrder.getEstimatedTotalRevenue());
				Object totRev = expressionManager.executeExpression(newEpression, values);
				totalRevenue=totRev.toString();
			}
			String sellDeviation="100";
			String buyDeviation="100";
			if(!chargedeviation.equalsIgnoreCase("NCT")){
				if(chargedeviation.equalsIgnoreCase("OCT")){
					sellDeviation=chargesManager.findDeviation(chargeid.get(0).toString(),originCountryCode,"N",sessionCorpID);	
					buyDeviation=chargesManager.findDeviation(chargeid.get(0).toString(),originCountryCode,"Y",sessionCorpID);
				}
				if(chargedeviation.equalsIgnoreCase("DCT")){
					sellDeviation=chargesManager.findDeviation(chargeid.get(0).toString(),destinationCountryCode,"N",sessionCorpID);	
					buyDeviation=chargesManager.findDeviation(chargeid.get(0).toString(),destinationCountryCode,"Y",sessionCorpID);
				}
				if(chargedeviation.equalsIgnoreCase("MCT")){
					sellDeviation=chargesManager.findMaxDeviation(chargeid.get(0).toString(),originCountryCode,destinationCountryCode,"N",sessionCorpID);	
					buyDeviation=chargesManager.findMaxDeviation(chargeid.get(0).toString(),originCountryCode,destinationCountryCode,"Y",sessionCorpID);
				}
				if(chargedeviation.equalsIgnoreCase("SCT")){
					sellDeviation=chargesManager.findSumDeviation(chargeid.get(0).toString(),originCountryCode,destinationCountryCode,"N",sessionCorpID); 
					buyDeviation=chargesManager.findSumDeviation(chargeid.get(0).toString(),originCountryCode,destinationCountryCode,"Y",sessionCorpID); 
				}
			}
			revisedChargeValue=str1[0]+"#"+str1[1]+"#"+str1[2]+"#"+str1[3]+"#"+str1[4]+"#"+str1[5]+"#"+str1[6]+"#"+qty+"#"+rg+"#"+str1[8]+"#"+str1[9]+"#"+totalRevenue+"#"+rg1+"#"+str1[11]+"#"+str1[12]+"#"+buyDependSell+"#"+sellDeviation+"#"+buyDeviation+"#"+chargedeviation+"#"+VATExclude+"#"+buyRate+"#"+contractCurrency+"#"+payableContractCurrency;
   }
			else{
			    }
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String internalCostsRevisedCharge(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			revisedChargeList=chargesManager.getInternalCostsCharge(charge, accountCompanyDivision,sessionCorpID);
			String newfieldName="";
			String qty;
			String totalRevenue="";
			billing=billingManager.get(sid);
			serviceOrder=serviceOrderManager.get(sid);
			String distanceInKmMiles="";
			if(serviceOrder.getDistanceInKmMiles()!=null){
			distanceInKmMiles =serviceOrder.getDistanceInKmMiles();
			}
			double distance=0.00;
			BigDecimal soDistance=new BigDecimal(0.00); 
			if(serviceOrder.getDistance()!=null){
				soDistance=serviceOrder.getDistance();
			}
			distance=Double.parseDouble(soDistance.toString());
			if((!(revisedChargeList.get(0) == null)) && (!(revisedChargeList.isEmpty())) && revisedChargeList.get(0)!=null && (!(revisedChargeList.get(0).toString().trim().equals("")))){
				String[] str1 = revisedChargeList.get(0).toString().split("#");
				newfieldName= str1[7];
				String minimum=str1[13];
				String twoDGridUnit="";
				if(str1[15]!=null && (!(str1[15].toString().trim().equals("")))){
				twoDGridUnit =str1[15];
				}else{
				twoDGridUnit="ND";	
				}
				String multquantity="";
				if(str1[16]!=null && (!(str1[16].toString().trim().equals("")))){
				multquantity =str1[16];
				}else{
				multquantity ="P";	
				}
				String VATExclude="N";
				if(str1[17]!=null && (!(str1[17].toString().trim().equals("")))){
					VATExclude =str1[17];
				}else{
					VATExclude ="N";	
				}
				String contractCurrency="";
				try{
				if(str1[18]!=null && (!(str1[18].toString().trim().equals("")))){
					contractCurrency =str1[18];
				}else{
					contractCurrency ="";	
				}}catch(Exception e){
					 e.printStackTrace();
				}
				if(newfieldName.trim().equalsIgnoreCase("")){
					
				}
				else
				{
					quantity=chargesManager.getQuantity(newfieldName, shipNum);
				}
				
				if(str1[8].equalsIgnoreCase("Preset2")){
					qty = str1[0];
				}else{
					if(newfieldName.trim().equalsIgnoreCase("")){
						qty="";	
					}else{
						qty = quantity.get(0).toString();	
					}
				}
			
			List chargeid=chargesManager.findInternalCostsChargeId(charge, accountCompanyDivision,sessionCorpID);
			String rg="";
			String rg1="";

			if(str1[9].equalsIgnoreCase("RateGrid")){
				try{
					if((twoDGridUnit.equalsIgnoreCase("DMI")) && distanceInKmMiles.equalsIgnoreCase("Km"))	{
						distance=distance/1.609;
					}
					if((twoDGridUnit.equalsIgnoreCase("DKM")) && distanceInKmMiles.equalsIgnoreCase("Mile"))	{
						distance=distance*1.609;
					}
					String stringDistance=""+distance; 
			List rate = chargesManager.findRateFromRateGrid(qty,chargeid.get(0).toString(),multquantity,stringDistance,twoDGridUnit);
			List checkBreakPoints=chargesManager.getBreakPoints(chargeid.get(0).toString(), sessionCorpID);	
			if(checkBreakPoints.get(0).toString().equals("false") && (!(multquantity.equals("N"))) && (!(multquantity.equals("B"))))
				{
				
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				List firstQuantity=chargesManager.getFirstQuantity(chargeid.get(0).toString(), sessionCorpID,multquantity,stringDistance,qty);
				
				if(Double.parseDouble(qty) < Double.parseDouble(firstQuantity.get(0).toString()))
				{
					qty=rate.get(2).toString();
				}
			
				}
				else
				{
				}
			}
			else{
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				qty=rate.get(2).toString();
				}
				else
				{
				}

			}
			}catch(Exception ex)
			{
				rg="0";
				rg1="";
				qty="0";
				ex.printStackTrace();
			}
			}
			else{
				rg=str1[1];
				
			}
			if(str1[9].equalsIgnoreCase("BuildFormula")){
				String expression=str1[10];
				String replacement=str1[7];
				String newEpression=expression;
				if(expression.trim().contains("FromField")){
				newEpression=expression.replaceAll("FromField", replacement);
				}
				Map values = new HashMap();
				values.put(replacement, new BigDecimal(qty));
				 values.put("charges.minimum",minimum);
				 values.put("billing.onHand",billing.getOnHand());
				 values.put("billing.cycle",billing.getCycle());
				 values.put("billing.postGrate",billing.getPostGrate());
				 values.put("billing.insuranceRate",billing.getInsuranceRate());
				 values.put("billing.insuranceValueActual",billing.getInsuranceValueActual());
				 values.put("billing.baseInsuranceValue",billing.getBaseInsuranceValue()); 
				 values.put("billing.vendorStoragePerMonth", billing.getVendorStoragePerMonth());
				 values.put("billing.storagePerMonth", billing.getStoragePerMonth());
				 values.put("billing.totalInsrVal", billing.getTotalInsrVal());
				 values.put("billing.insurancePerMonth", billing.getInsurancePerMonth());
				 values.put("billing.insuranceValueEntitle", billing.getInsuranceValueEntitle());
				 values.put("billing.payableRate", billing.getPayableRate());
				 values.put("billing.storageMeasurement", billing.getStorageMeasurement());
				 values.put("accountLine.actualRevenue", buildFormulaActualRevenue);
				 values.put("serviceOrder.estimatedTotalRevenue", serviceOrder.getEstimatedTotalRevenue());
				Object totRev = expressionManager.executeExpression(newEpression, values);
				totalRevenue=totRev.toString();
			}
			revisedChargeValue=str1[0]+"#"+str1[1]+"#"+str1[2]+"#"+str1[3]+"#"+str1[4]+"#"+str1[5]+"#"+str1[6]+"#"+qty+"#"+rg+"#"+str1[8]+"#"+str1[9]+"#"+totalRevenue+"#"+rg1+"#"+str1[11]+"#"+str1[12]+"#"+VATExclude+"#"+contractCurrency;
   }
			else{
			    }
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	@SkipValidation
	public String revisedEntitleCharge()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			revisedEntitleChargeList=chargesManager.getChargeEntitle(charge, billingContract);
			String[] str1 = revisedEntitleChargeList.get(0).toString().split("#");
			String newfieldName = str1[1];
			if(newfieldName.trim().equalsIgnoreCase("")){
				
			}else{
				quantity=chargesManager.getQuantity(newfieldName, shipNum);
			}
			
			String qty;
			if(str1[2].equalsIgnoreCase("Preset0")){
				qty = str1[0];
			}else{
				if(newfieldName.trim().equalsIgnoreCase("")){
					qty="";
				}
				else{
				qty = quantity.get(0).toString();
				}
			} 
			String twoDGridUnit="";
			if(str1[8]!=null && (!(str1[8].toString().trim().equals("")))){
			twoDGridUnit =str1[8];
			}else{
			twoDGridUnit="ND";	
			}
			String multquantity="";
			if(str1[9]!=null && (!(str1[9].toString().trim().equals("")))){
			multquantity =str1[9];
			}else{
			multquantity ="P";	
			}
			String VATExclude="N";
			if(str1[10]!=null && (!(str1[10].toString().trim().equals("")))){
				VATExclude =str1[10];
			}else{
				VATExclude ="N";	
			}
			serviceOrder=serviceOrderManager.get(sid);
			String distanceInKmMiles="";
			if(serviceOrder.getDistanceInKmMiles()!=null){
			distanceInKmMiles =serviceOrder.getDistanceInKmMiles();
			}
			double distance=0.00;
			BigDecimal soDistance=new BigDecimal(0.00); 
			if(serviceOrder.getDistance()!=null){
				soDistance=serviceOrder.getDistance();
			}
			distance=Double.parseDouble(soDistance.toString());

			List chargeid=chargesManager.findChargeId(charge, billingContract);
			
			String rg="";
			String rg1="";
			if(str1[4].equalsIgnoreCase("RateGrid")){
				try{
					if((twoDGridUnit.equalsIgnoreCase("DMI")) && distanceInKmMiles.equalsIgnoreCase("Km"))	{
						distance=distance/1.609;
					}
					if((twoDGridUnit.equalsIgnoreCase("DKM")) && distanceInKmMiles.equalsIgnoreCase("Mile"))	{
						distance=distance*1.609;
					}
					String stringDistance=""+distance;  
			List rate = chargesManager.findRateFromRateGrid(qty,chargeid.get(0).toString(),multquantity,stringDistance,twoDGridUnit);
			List checkBreakPoints=chargesManager.getBreakPoints(chargeid.get(0).toString(), sessionCorpID);	
			if(checkBreakPoints.get(0).toString().equals("false") && (!(multquantity.equals("N"))) && (!(multquantity.equals("B"))))
				{
				
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				List firstQuantity=chargesManager.getFirstQuantity(chargeid.get(0).toString(), sessionCorpID,multquantity,stringDistance,qty);
				
				if(Double.parseDouble(qty) < Double.parseDouble(firstQuantity.get(0).toString()))
				{
					qty=rate.get(2).toString();
				}
			
				}
				else
				{
				}
			}
			else{
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				qty=rate.get(2).toString();
				}
				else
				{
				}

			}
			}catch(Exception ex)
			{
				rg="0";
				rg1="";
				qty="0";
				ex.printStackTrace();
			}
			}else{
				rg=str1[6];
			}
				revisedChargeValue=str1[0]+"#"+qty+"#"+str1[1]+"#"+str1[2]+"#"+str1[3]+"#"+str1[4]+"#"+str1[5]+"#"+str1[6]+"#"+rg+"#"+rg1+"#"+str1[7]+"#"+VATExclude;
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String internalCostsRevisedEntitleCharge(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			revisedEntitleChargeList=chargesManager.getInternalCostsChargeEntitle(charge, accountCompanyDivision,sessionCorpID);
			String[] str1 = revisedEntitleChargeList.get(0).toString().split("#");
			String newfieldName = str1[1];
			if(newfieldName.trim().equalsIgnoreCase("")){
				
			}else{
				quantity=chargesManager.getQuantity(newfieldName, shipNum);
			}
			
			String qty;
			if(str1[2].equalsIgnoreCase("Preset0")){
				qty = str1[0];
			}else{
				if(newfieldName.trim().equalsIgnoreCase("")){
					qty="";
				}
				else{
				qty = quantity.get(0).toString();
				}
			} 
			String twoDGridUnit="";
			if(str1[8]!=null && (!(str1[8].toString().trim().equals("")))){
			twoDGridUnit =str1[8];
			}else{
			twoDGridUnit="ND";	
			}
			String multquantity="";
			if(str1[9]!=null && (!(str1[9].toString().trim().equals("")))){
			multquantity =str1[9];
			}else{
			multquantity ="P";	
			}
			String VATExclude="N";
			if(str1[10]!=null && (!(str1[10].toString().trim().equals("")))){
				VATExclude =str1[10];
			}else{
				VATExclude ="N";	
			}
			serviceOrder=serviceOrderManager.get(sid);
			String distanceInKmMiles="";
			if(serviceOrder.getDistanceInKmMiles()!=null){
			distanceInKmMiles =serviceOrder.getDistanceInKmMiles();
			}
			double distance=0.00;
			BigDecimal soDistance=new BigDecimal(0.00); 
			if(serviceOrder.getDistance()!=null){
				soDistance=serviceOrder.getDistance();
			}
			distance=Double.parseDouble(soDistance.toString());


			List chargeid=chargesManager.findInternalCostsChargeId(charge, accountCompanyDivision,sessionCorpID);
			
			String rg="";
			String rg1="";
			if(str1[4].equalsIgnoreCase("RateGrid")){
				try{
					if((twoDGridUnit.equalsIgnoreCase("DMI")) && distanceInKmMiles.equalsIgnoreCase("Km"))	{
						distance=distance/1.609;
					}
					if((twoDGridUnit.equalsIgnoreCase("DKM")) && distanceInKmMiles.equalsIgnoreCase("Mile"))	{
						distance=distance*1.609;
					}
					String stringDistance=""+distance; 
			List rate = chargesManager.findRateFromRateGrid(qty,chargeid.get(0).toString(),multquantity,stringDistance,twoDGridUnit);
			List checkBreakPoints=chargesManager.getBreakPoints(chargeid.get(0).toString(), sessionCorpID);	
			if(checkBreakPoints.get(0).toString().equals("false") && (!(multquantity.equals("N"))) && (!(multquantity.equals("B"))))
				{
				
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				List firstQuantity=chargesManager.getFirstQuantity(chargeid.get(0).toString(), sessionCorpID,multquantity,stringDistance,qty);
				
				if(Double.parseDouble(qty) < Double.parseDouble(firstQuantity.get(0).toString()))
				{
					qty=rate.get(2).toString();
				}
			
				}
				else
				{
				}
			}
			else{
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				qty=rate.get(2).toString();
				}
				else
				{
				}

			}
			}catch(Exception ex)
			{
				rg="0";
				rg1="";
				qty="0";
				ex.printStackTrace();
			}
			}else{
				rg=str1[6];
			}
				revisedChargeValue=str1[0]+"#"+qty+"#"+str1[1]+"#"+str1[2]+"#"+str1[3]+"#"+str1[4]+"#"+str1[5]+"#"+str1[6]+"#"+rg+"#"+rg1+"#"+str1[7]+"#"+VATExclude;
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
	
		
	}
	
	@SkipValidation
	public String getVanPayCharges(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			vanPayChargesList=accountLineManager.getVanPayCharges(sid,sessionCorpID);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	} 
	@SkipValidation
	public String getDomCommCharges(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			systemDefaultmiscVl=serviceOrderManager.findmiscVlSystemDefault(sessionCorpID).get(0).toString();
			domCommChargesList=accountLineManager.getDomCommCharges(sid,sessionCorpID,job,systemDefaultmiscVl);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	} 
	private String vatExcludedFlag; 
	private String vatExclude;
	public String getVatExclude() {
		return vatExclude;
	}

	public void setVatExclude(String vatExclude) {
		this.vatExclude = vatExclude;
	}

	public String getVatExcludedFlag() {
		return vatExcludedFlag;
	}

	public void setVatExcludedFlag(String vatExcludedFlag) {
		this.vatExcludedFlag = vatExcludedFlag;
	}

	@SkipValidation
	public String checkVatExcluded()
	{
		vatExcludedFlag=accountLineManager.findChargeVatCalculation(sessionCorpID,contract,chargeCode).get(0).toString();
		return SUCCESS;
	}
	
	@SkipValidation
	public String revisedEstimateCharge()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			revisedEstimateChargeList=chargesManager.getChargeEstimate(charge, billingContract,sessionCorpID);
			String totalRevenue="";
			if(!(revisedEstimateChargeList.get(0) == null)){
				String[] str1 = revisedEstimateChargeList.get(0).toString().split("#");
			String newfieldName = str1[1];
			String minimum=str1[10]; 
			String twoDGridUnit="";
			String buyRate="0";
			if(str1[11]!=null && (!(str1[11].toString().trim().equals("")))){
			twoDGridUnit =str1[11];
			}else{
			twoDGridUnit="ND";	
			}
			String multquantity="";
			if(str1[12]!=null && (!(str1[12].toString().trim().equals("")))){
			multquantity =str1[12];
			}else{
			multquantity ="P";	
			}
      if(newfieldName.trim().equalsIgnoreCase("")){
				
			}else{
				quantity=chargesManager.getQuantity(newfieldName, shipNum);
			}
				billing=billingManager.get(sid);
				serviceOrder=serviceOrderManager.get(sid);
				String distanceInKmMiles="";
				if(serviceOrder.getDistanceInKmMiles()!=null){
				distanceInKmMiles =serviceOrder.getDistanceInKmMiles();
				}
				double distance=0.00;
				BigDecimal soDistance=new BigDecimal(0.00); 
				if(serviceOrder.getDistance()!=null){
					soDistance=serviceOrder.getDistance();
				}
				distance=Double.parseDouble(soDistance.toString());
				String destinationCountryCode=serviceOrder.getDestinationCountryCode();
				String originCountryCode=serviceOrder.getOriginCountryCode();
				String chargedeviation="";
				if(str1[13]!=null && (!(str1[13].toString().trim().equals("")))){
					chargedeviation =str1[13];
				}else{
					chargedeviation ="NCT";	
				}
				String buyDependSell="N";
				if(str1[14]!=null && (!(str1[14].toString().trim().equals("")))){
					buyDependSell =str1[14];
				}else{
					buyDependSell ="N";	
				}
				String VATExclude="N";
				if(str1[15]!=null && (!(str1[15].toString().trim().equals("")))){
					VATExclude =str1[15];
				}else{
					VATExclude ="N";	
				}
				String contractCurrency="";
				try{
				if(str1[16]!=null && (!(str1[16].toString().trim().equals("")))){
					contractCurrency =str1[16];
				}else{
					contractCurrency ="";	
				}}catch(Exception e){
					contractCurrency ="";	
					e.printStackTrace();
				}
				String payableContractCurrency="";
				try{
				if(str1[17]!=null && (!(str1[17].toString().trim().equals("")))){
					payableContractCurrency =str1[17];
				}else{
					payableContractCurrency ="";	
				}}catch(Exception e){
					payableContractCurrency ="";
					e.printStackTrace();
				}
				try{
					if(str1.length>18){
			   		if(str1[18]!=null && (!(str1[18].toString().trim().equals("")))){
			   			buyRate =str1[18];
			   		}else{
			   			buyRate ="0";	
			   		}}else{
			   			buyRate ="0";	
			   		}
					}catch(Exception e){
			   			buyRate ="0";	
			   			e.printStackTrace();
			   		}
			String qty;
			String QuantityBuy;
			if(str1[2].equalsIgnoreCase("Preset1")){
				qty = str1[0];
			}else{
				if(newfieldName.trim().equalsIgnoreCase("")) {
					qty="";
				}
				else{
				qty = quantity.get(0).toString();
				}		
			}
			QuantityBuy=qty;
			List chargeid=chargesManager.findChargeId(charge, billingContract);
			
			String rg="";
			String rg1="";
			List buyRateList=null;
			if(str1[3].equalsIgnoreCase("RateGrid")){
				if((twoDGridUnit.equalsIgnoreCase("DMI")) && distanceInKmMiles.equalsIgnoreCase("Km"))	{
					distance=distance/1.609;
				}
				if((twoDGridUnit.equalsIgnoreCase("DKM")) && distanceInKmMiles.equalsIgnoreCase("Mile"))	{
					distance=distance*1.609;
				}
				String stringDistance=""+distance;
			List rate = chargesManager.findRateFromRateGrid(qty,chargeid.get(0).toString(),multquantity,stringDistance,twoDGridUnit); 
			List checkBreakPoints=chargesManager.getBreakPoints(chargeid.get(0).toString(), sessionCorpID);	
			if(checkBreakPoints.get(0).toString().equals("false") && (!(multquantity.equals("N"))) && (!(multquantity.equals("B"))))
				{
				
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				List firstQuantity=chargesManager.getFirstQuantity(chargeid.get(0).toString(), sessionCorpID,multquantity,stringDistance,qty);
				if(firstQuantity != null && firstQuantity.size() > 0 && Double.parseDouble(qty) < Double.parseDouble(firstQuantity.get(0).toString()))
				{
					qty=rate.get(2).toString();
				}
				}
				else
				{
				}
			}
			else{
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				qty=rate.get(2).toString();
				}
				else
				{
				}
			}
			
			
			buyRateList = chargesManager.findBuyRateFromRateGrid(QuantityBuy,chargeid.get(0).toString(),multquantity,stringDistance,twoDGridUnit);
			if(buyRateList!=null && !buyRateList.isEmpty())
			{
				buyRate=buyRateList.get(0).toString();
			}
			}else{
				rg=str1[4];
			}		
			try{
				if(str1.length>19){
				if(str1[19].equalsIgnoreCase("Preset")){ 
			   		if(str1[18]!=null && (!(str1[18].toString().trim().equals("")))){
			   			buyRate =str1[18];
			   		}else{
			   			buyRate ="0";	
			   		}
				}else{
					if(str1[19].equalsIgnoreCase("BuildFormula")){
						String expression=str1[20];
						String replacement=str1[1];
						String newEpression=expression;
						if(expression.trim().contains("FromField")){
						newEpression=expression.replaceAll("FromField", replacement);
						}
						Map values = new HashMap();			
						values.put(replacement, new BigDecimal(qty));
						 values.put("charges.minimum",minimum);
						 values.put("billing.onHand",billing.getOnHand());
						 values.put("billing.cycle",billing.getCycle());
						 values.put("billing.postGrate",billing.getPostGrate());
						 values.put("billing.insuranceRate",billing.getInsuranceRate());
						 values.put("billing.insuranceValueActual",billing.getInsuranceValueActual());
						 values.put("billing.baseInsuranceValue",billing.getBaseInsuranceValue()); 
						 values.put("billing.vendorStoragePerMonth", billing.getVendorStoragePerMonth());
					     values.put("billing.storagePerMonth", billing.getStoragePerMonth());
					     values.put("billing.totalInsrVal", billing.getTotalInsrVal());
					     values.put("billing.insurancePerMonth", billing.getInsurancePerMonth());
					     values.put("billing.insuranceValueEntitle", billing.getInsuranceValueEntitle());
					     values.put("billing.payableRate", billing.getPayableRate());
					     values.put("billing.storageMeasurement", billing.getStorageMeasurement());
					     values.put("accountLine.actualRevenue", buildFormulaActualRevenue);
					     values.put("serviceOrder.estimatedTotalRevenue", serviceOrder.getEstimatedTotalRevenue());
						Object totRev = expressionManager.executeExpression(newEpression, values);
						buyRate=totRev.toString();			
					}
				}}else{
		   			buyRate ="0";	
		   		}
				}catch(Exception e){
					buyRate ="0";	
					e.printStackTrace();
				}
			//}
			if(str1[3].equalsIgnoreCase("BuildFormula")){
				try{
				String expression=str1[6];
				String replacement=str1[1];
				String newEpression=expression;
				if(expression.trim().contains("FromField")){
					newEpression=expression.replaceAll("FromField", replacement);
					}  
				Map values = new HashMap();
					
					values.put(replacement, new BigDecimal(qty));
				//}
				 values.put("charges.minimum",minimum);
				 values.put("billing.onHand",billing.getOnHand());
				 values.put("billing.cycle",billing.getCycle());
				 values.put("billing.postGrate",billing.getPostGrate());
				 values.put("billing.insuranceRate",billing.getInsuranceRate());
				 values.put("billing.insuranceValueActual",billing.getInsuranceValueActual());
				 values.put("billing.baseInsuranceValue",billing.getBaseInsuranceValue()); 
				 values.put("billing.vendorStoragePerMonth", billing.getVendorStoragePerMonth());
				 values.put("billing.storagePerMonth", billing.getStoragePerMonth());
				 values.put("billing.totalInsrVal", billing.getTotalInsrVal());
				 values.put("billing.insurancePerMonth", billing.getInsurancePerMonth());
				 values.put("billing.insuranceValueEntitle", billing.getInsuranceValueEntitle());
				 values.put("billing.payableRate", billing.getPayableRate());
				 values.put("billing.storageMeasurement", billing.getStorageMeasurement());
				 values.put("accountLine.actualRevenue", buildFormulaActualRevenue);
				 values.put("serviceOrder.estimatedTotalRevenue", serviceOrder.getEstimatedTotalRevenue());
				Object totRev = expressionManager.executeExpression(newEpression, values);
				if(totRev != null){
					totalRevenue=totRev.toString();
				}
				}catch(NullPointerException nullExp){
					totalRevenue ="0";
					nullExp.printStackTrace();
				}
				catch(Exception exp){
					totalRevenue ="0";
					exp.printStackTrace();
				}
			}
			String sellDeviation="100";
			String buyDeviation="100";
			if(!chargedeviation.equalsIgnoreCase("NCT")){
				if(chargedeviation.equalsIgnoreCase("OCT")){
					sellDeviation=chargesManager.findDeviation(chargeid.get(0).toString(),originCountryCode,"N",sessionCorpID);	
					buyDeviation=chargesManager.findDeviation(chargeid.get(0).toString(),originCountryCode,"Y",sessionCorpID);
				}
				if(chargedeviation.equalsIgnoreCase("DCT")){
					sellDeviation=chargesManager.findDeviation(chargeid.get(0).toString(),destinationCountryCode,"N",sessionCorpID);	
					buyDeviation=chargesManager.findDeviation(chargeid.get(0).toString(),destinationCountryCode,"Y",sessionCorpID);
				}
				if(chargedeviation.equalsIgnoreCase("MCT")){
					sellDeviation=chargesManager.findMaxDeviation(chargeid.get(0).toString(),originCountryCode,destinationCountryCode,"N",sessionCorpID);	
					buyDeviation=chargesManager.findMaxDeviation(chargeid.get(0).toString(),originCountryCode,destinationCountryCode,"Y",sessionCorpID);
				}
				if(chargedeviation.equalsIgnoreCase("SCT")){
					sellDeviation=chargesManager.findSumDeviation(chargeid.get(0).toString(),originCountryCode,destinationCountryCode,"N",sessionCorpID); 
					buyDeviation=chargesManager.findSumDeviation(chargeid.get(0).toString(),originCountryCode,destinationCountryCode,"Y",sessionCorpID); 
				}
			}
			
			revisedChargeValue=str1[0]+"#"+qty+"#"+str1[1]+"#"+str1[2]+"#"+rg+"#"+str1[3]+"#"+totalRevenue +"#"+rg1+"#"+str1[7]+"#"+str1[8]+"#"+str1[9]+"#"+buyDependSell+"#"+sellDeviation+"#"+buyDeviation+"#"+multquantity+"#"+chargedeviation+"#"+VATExclude+"#"+buyRate+"#"+contractCurrency+"#"+payableContractCurrency;
			}
			else{
				
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String internalCostsRevisedEstimateCharge(){

		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			revisedEstimateChargeList=chargesManager.getInternalCostChargeEstimate(charge, accountCompanyDivision,sessionCorpID);
			String totalRevenue="";
			if(!(revisedEstimateChargeList.get(0) == null)){
				String[] str1 = revisedEstimateChargeList.get(0).toString().split("#");
			String newfieldName = str1[1];
			String minimum=str1[10]; 
			String twoDGridUnit="";
			if(str1[11]!=null && (!(str1[11].toString().trim().equals("")))){
			twoDGridUnit =str1[11];
			}else{
			twoDGridUnit="ND";	
			}
			String multquantity="";
			if(str1[12]!=null && (!(str1[12].toString().trim().equals("")))){
			multquantity =str1[12];
			}else{
			multquantity ="P";	
			}
			String VATExclude="N";
			if(str1[13]!=null && (!(str1[13].toString().trim().equals("")))){
				VATExclude =str1[13];
			}else{
				VATExclude ="N";	
			}
      if(newfieldName.trim().equalsIgnoreCase("")){
				
			}else{
				quantity=chargesManager.getQuantity(newfieldName, shipNum);
			}
				billing=billingManager.get(sid);
				serviceOrder=serviceOrderManager.get(sid);
				String distanceInKmMiles="";
				if(serviceOrder.getDistanceInKmMiles()!=null){
				distanceInKmMiles =serviceOrder.getDistanceInKmMiles();
				}
				double distance=0.00;
				BigDecimal soDistance=new BigDecimal(0.00); 
				if(serviceOrder.getDistance()!=null){
					soDistance=serviceOrder.getDistance();
				}
				distance=Double.parseDouble(soDistance.toString());

			String qty;
			if(str1[2].equalsIgnoreCase("Preset1")){
				qty = str1[0];
			}else{
				if(newfieldName.trim().equalsIgnoreCase("")) {
					qty="";
				}
				else{
				qty = quantity.get(0).toString();
				}		
			}
			
			List chargeid=chargesManager.findInternalCostsChargeId(charge, accountCompanyDivision,sessionCorpID);
			
			String rg="";
			String rg1="";

			if(str1[3].equalsIgnoreCase("RateGrid")){
				if((twoDGridUnit.equalsIgnoreCase("DMI")) && distanceInKmMiles.equalsIgnoreCase("Km"))	{
					distance=distance/1.609;
				}
				if((twoDGridUnit.equalsIgnoreCase("DKM")) && distanceInKmMiles.equalsIgnoreCase("Mile"))	{
					distance=distance*1.609;
				}
				String stringDistance=""+distance; 
			List rate = chargesManager.findRateFromRateGrid(qty,chargeid.get(0).toString(),multquantity,stringDistance,twoDGridUnit);
				
			List checkBreakPoints=chargesManager.getBreakPoints(chargeid.get(0).toString(), sessionCorpID);	
			if(checkBreakPoints.get(0).toString().equals("false") && (!(multquantity.equals("N"))) && (!(multquantity.equals("B"))))
				{
				
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				List firstQuantity=chargesManager.getFirstQuantity(chargeid.get(0).toString(), sessionCorpID,multquantity,stringDistance,qty);
				if(Double.parseDouble(qty) < Double.parseDouble(firstQuantity.get(0).toString()))
				{
					qty=rate.get(2).toString();
				}
				}
				else
				{
				}
			}
			else{
				if(!rate.isEmpty())
				{
				rg=rate.get(0).toString();
				rg1=rate.get(1).toString();
				qty=rate.get(2).toString();
				}
				else
				{
				}
			}
			}else{
				rg=str1[4];
			}
			if(str1[3].equalsIgnoreCase("BuildFormula")){
				try{
				String expression=str1[6];
				String replacement=str1[1];
				String newEpression=expression;
				if(expression.trim().contains("FromField")){
					newEpression=expression.replaceAll("FromField", replacement);
					}  
				Map values = new HashMap();
				values.put(replacement, new BigDecimal(qty));
				 values.put("charges.minimum",minimum);
				 values.put("billing.onHand",billing.getOnHand());
				 values.put("billing.cycle",billing.getCycle());
				 values.put("billing.postGrate",billing.getPostGrate());
				 values.put("billing.insuranceRate",billing.getInsuranceRate());
				 values.put("billing.insuranceValueActual",billing.getInsuranceValueActual());
				 values.put("billing.baseInsuranceValue",billing.getBaseInsuranceValue()); 
				 values.put("billing.vendorStoragePerMonth", billing.getVendorStoragePerMonth());
				 values.put("billing.storagePerMonth", billing.getStoragePerMonth());
				 values.put("billing.totalInsrVal", billing.getTotalInsrVal());
				 values.put("billing.insurancePerMonth", billing.getInsurancePerMonth());
				 values.put("billing.insuranceValueEntitle", billing.getInsuranceValueEntitle());
				 values.put("billing.payableRate", billing.getPayableRate());
				 values.put("billing.storageMeasurement", billing.getStorageMeasurement());
				 values.put("accountLine.actualRevenue", buildFormulaActualRevenue);
				 values.put("serviceOrder.estimatedTotalRevenue", serviceOrder.getEstimatedTotalRevenue());
				Object totRev = expressionManager.executeExpression(newEpression, values);
				totalRevenue=totRev.toString();
					
			
			}catch(NullPointerException nullExp){
				totalRevenue="0";
				nullExp.printStackTrace();
			}catch(Exception exp){
				totalRevenue="0";
				exp.printStackTrace();
			}
				}
			revisedChargeValue=str1[0]+"#"+qty+"#"+str1[1]+"#"+str1[2]+"#"+rg+"#"+str1[3]+"#"+totalRevenue +"#"+rg1+"#"+str1[7]+"#"+str1[8]+"#"+str1[9]+"#"+VATExclude;
			}
			else{
				
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		
	}
	
	private String dateStatus;
	@SkipValidation
	public String sysDefDate () throws Exception
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			systemDefaultList=serviceOrderManager.findBySysDefault(sessionCorpID);
			
			if(!systemDefaultList.isEmpty())
			{
				newDefaultDate = systemDefaultList.get(0).toString();
				newDefaultDate=newDefaultDate.replace(".0", "");
			}
			Date dtTmp = new SimpleDateFormat("yyyy-MM-dd").parse(newDefaultDate);
			if( ! (recPostDate.equals(null) || recPostDate.equals("") ) )
			{
				Date dtTmpNew = new SimpleDateFormat("dd-MMM-yy").parse(recPostDate);
				
			if(dtTmp.after(dtTmpNew))
			{
			 dateStatus = "less";
			}else{
				dateStatus = "more";
			}
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
	}
	private String vendorCodeNew;
	private String invoiceNumberNew;
	private String invoiceDateNew;	
	private String currencyNew;
	private String idss;

	@SkipValidation
	public String getAllAccListAjax(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			purchaseInvoiceFlag=accountLineManager.getListOfAccList(sessionCorpID,vendorCodeNew,invoiceNumberNew,invoiceDateNew,currencyNew,idss);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String checkNotesStatusAcc(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			 
			notesStatus=accountLineManager.checkNotesStatusAcc(sessionCorpID,vendorCodeNew,invoiceNumberNew,sidNum);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String vendorInvoiceEnhancementAjax(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			purchaseInvoiceFlag=accountLineManager.vendorInvoiceEnhancementList(sessionCorpID,vendorCodeNew,invoiceNumberNew,idss);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}	

	
	private String driverCode; 
    @SkipValidation
    	public String checkDriverIdAjax() {  
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			purchaseInvoiceFlag = partnerManager.checkDriverIdAjax(driverCode,sessionCorpID);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
      	return SUCCESS;   
      } 
	private String purchaseInvoiceFlag;
	private List driverList;
	@SkipValidation
	public String checkPurchasePostingAjax(){
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
		driverList=accountLineManager.checkPurchasePostingMethod(sessionCorpID,shipNumber);
		}
		catch(Exception e)
		{
			 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String driverCount;
	public String getDriverCount() {
		return driverCount;
	}

	public void setDriverCount(String driverCount) {
		this.driverCount = driverCount;
	}

	@SkipValidation
	public String checkDriverForMoreThanOneAjax(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
		driverCount=accountLineManager.checkDriverForMoreThanOneAjax(sessionCorpID,shipNumber);
		}catch(Exception e)
		{
			 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	
	public List getDriverList() {
		return driverList;
	}

	public void setDriverList(List driverList) {
		this.driverList = driverList;
	}  
	private String costingDetailShowFlag="N";
	private List<SystemDefault> sysDefaultDetail;
	private SystemDefault systemDefault;
	private CompanyDivisionManager companyDivisionManager;
    private String divisionFlag;
    private String disableALL;
    private Map<String, String> myfileDocumentList=new HashMap<String, String>();

	public String edit() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String str=getRequest().getParameter("toDoAlert");
			  if((str!=null)&&(str.equalsIgnoreCase("YES"))){
			   sid = accountLineManager.get(id).getServiceOrderId();
			  }
			trackingStatus=trackingStatusManager.get(sid);
			serviceOrder=serviceOrderManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			billing =billingManager.get(sid);
			customerFile = serviceOrder.getCustomerFile();
			//getComboList(sessionCorpID); 
			checkCropId=sessionCorpID;  
			salesCommisionRate=serviceOrderManager.findSalesCommisionRate(sessionCorpID).get(0).toString();
			grossMarginThreshold=serviceOrderManager.findGrossMarginThreshold(sessionCorpID).get(0).toString();
			baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID);
			companyDivis = customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(serviceOrder.getCorpID(), "",serviceOrder.getCompanyDivision());
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
			if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
				contractType=true;	
			}
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			  	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
			  }
			if(billingDMMContractType){
				try{
				if(trackingStatus.getUtsiNetworkGroup()){ 
			    	if((!(trackingStatus.getAccNetworkGroup())) && serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getBookingAgentExSO()!=null && (!(trackingStatus.getBookingAgentExSO().equals(""))))	{ 
			    	serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(trackingStatus.getBookingAgentExSO()));
			    	customerFileToRecods=customerFileManager.getForOtherCorpid(serviceOrderToRecods.getCustomerFileId());
			    	bookingAgentCodeDMM=customerFileToRecods.getAccountCode();
			    	}
			    }else{
				if((!(trackingStatus.getAccNetworkGroup())) && serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && serviceOrder.getBookingAgentShipNumber()!=null && (!(serviceOrder.getBookingAgentShipNumber().equals(""))))	{ 
				serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(serviceOrder.getBookingAgentShipNumber()));
				customerFileToRecods=customerFileManager.getForOtherCorpid(serviceOrderToRecods.getCustomerFileId());
				bookingAgentCodeDMM=customerFileToRecods.getAccountCode();
				}
				}
				}catch(Exception e){
					 e.printStackTrace();
				}
			}
			BigDecimal multiplyer=new BigDecimal(2.2046);
			BigDecimal divisor=new BigDecimal(2.2046);
			BigDecimal cuMultiplyer=new BigDecimal(35.314);
			BigDecimal cuDivisor=new BigDecimal(35.314);  
           if (id != null)
			 { 
				accountLine = accountLineManager.get(id); 
				serviceOrder = accountLine.getServiceOrder();
				accountLine.setCorpID(sessionCorpID);
				billing=billingManager.get(sid);
				
				try{
				if(accountLine.getCompanyDivision()!=null && (!(accountLine.getCompanyDivision().equals("")))){	
				if(companyDivis.contains(accountLine.getCompanyDivision())){
					
				}else{
					companyDivis.add(accountLine.getCompanyDivision());	
				}
				}
				}catch(Exception e){
					 e.printStackTrace();
				}
				if(accountLine.getVendorCode()!=null && !accountLine.getVendorCode().equals(""))
				{
					myfileDocumentList=accountLineManager.getDocumentList(sessionCorpID,accountLine.getVendorCode(),accountLine.getShipNumber());
				}
				if(accountLine.getBillToCode()!=null && (!accountLine.getBillToCode().equals(""))){
				creditInvoiceList=accountLineManager.getCreditInvoice(sessionCorpID,sid,accountLine.getBillToCode());
				}
				//accountLine.setContract(billing.getContract());
				if((billingManager.checkById(sid)).isEmpty())
				{ 
					billingFlag=0; 
				}
				else
				{ 
					billingFlag=1; 
				}
				 getGlTypeList=new ArrayList(accountLineManager.findGlTypeList(accountLine.getContract(), accountLine.getChargeCode()));  
				 if(getGlTypeList.isEmpty()||getGlTypeList==null)
				 {
					 getGlTypeList=new ArrayList();
					 
				 }
				 getCommissionList =new ArrayList(accountLineManager.findCommissionList(accountLine.getContract(), accountLine.getChargeCode()));  
			 	 if(getCommissionList.isEmpty()||getCommissionList==null)
			 	 {
			 		getCommissionList=new ArrayList(); 	
			 	 }
				
				 if((accountLine.getEstimateQuantity().toString()).equals((new BigDecimal("0.00").toString()))  || accountLine.getEstimateQuantity()== null){
				     if(serviceOrder.getMode()== null || serviceOrder.getMode().equalsIgnoreCase("AIR"))
				      {  
						 if(miscellaneous.getEstimateGrossWeight()==null || (miscellaneous.getEstimateGrossWeight().toString()).equals(new BigDecimal("0.00").toString())) {  
			 				setTotalEstimateExpenseCwt(new BigDecimal("0"));  
						 }else{
						    setTotalEstimateExpenseCwt(miscellaneous.getEstimateGrossWeight()); 
						 }
						 if(miscellaneous.getEstimateGrossWeightKilo()==null || (miscellaneous.getEstimateGrossWeightKilo().toString()).equals(new BigDecimal("0.00").toString())){
							 setTotalEstimateExpenseKg(new BigDecimal("0"));
						 }else{
							 setTotalEstimateExpenseKg((miscellaneous.getEstimateGrossWeightKilo()));
						 }
						 if(miscellaneous.getEstimateCubicFeet()==null || (miscellaneous.getEstimateCubicFeet().toString()).equals(new BigDecimal("0.00").toString())){
							 setTotalEstimateExpenseCft(new BigDecimal("0"));
			    			
						 }else{
							 setTotalEstimateExpenseCft(miscellaneous.getEstimateCubicFeet());
							 
						 }
						 if(miscellaneous.getEstimateCubicMtr()==null || (miscellaneous.getEstimateCubicMtr().toString()).equals(new BigDecimal("0.00").toString())){
							 setTotalEstimateExpenseCbm(new BigDecimal("0"));
						 }else{
							 setTotalEstimateExpenseCbm(miscellaneous.getEstimateCubicMtr());
						 }
					}
				
				 else{ 
				 
					 if(miscellaneous.getEstimatedNetWeight()==null || (miscellaneous.getEstimatedNetWeight().toString()).equals(new BigDecimal("0.00").toString())) {  
			 				 setTotalEstimateExpenseCwt(new BigDecimal("0")); 
					 }else{ 
						     setTotalEstimateExpenseCwt((miscellaneous.getEstimatedNetWeight()));  
					 } 
					 if(miscellaneous.getEstimatedNetWeightKilo()==null || (miscellaneous.getEstimatedNetWeightKilo().toString()).equals(new BigDecimal("0.00").toString())){
						 setTotalEstimateExpenseKg(new BigDecimal("0")); 
					 }else{
						 setTotalEstimateExpenseKg((miscellaneous.getEstimatedNetWeightKilo())); 
					 }
					 if(miscellaneous.getNetEstimateCubicFeet()==null || (miscellaneous.getNetEstimateCubicFeet().toString()).equals(new BigDecimal("0.00").toString())){
						 setTotalEstimateExpenseCft(new BigDecimal("0"));
					 }else{
						 setTotalEstimateExpenseCft(miscellaneous.getNetEstimateCubicFeet());
					 }
					 if(miscellaneous.getNetEstimateCubicMtr()==null || (miscellaneous.getNetEstimateCubicMtr().toString()).equals(new BigDecimal("0.00").toString())){
						 setTotalEstimateExpenseCbm(new BigDecimal("0"));
					 }else{
						 setTotalEstimateExpenseCbm(miscellaneous.getNetEstimateCubicMtr());
					 }
				 } 
				if(accountLine.getBasis() !=null){    
				 if(accountLine.getBasis().equals("each"))
				 {
						 setTotalEstimateExpenseEach(accountLine.getEstimateQuantity()); 
				 }
				 if(accountLine.getBasis().equals("flat"))
				 {
						 setTotalEstimateExpenseFlat(accountLine.getEstimateQuantity()); 
				 }
				 if(accountLine.getBasis().equals("hour"))
				 {
						 setTotalEstimateExpenseHour(accountLine.getEstimateQuantity()); 
				 }
				 }
				 }
				 else{ 
					 if(accountLine.getBasis() !=null){
					 	 if(accountLine.getBasis().equalsIgnoreCase("cwt")){
							 setTotalEstimateExpenseCwt(accountLine.getEstimateQuantity());
							 setTotalEstimateExpenseKg(accountLine.getEstimateQuantity().divide(divisor, 3, 0));
						 }
						 else{
							 if(accountLine.getBasis().equalsIgnoreCase("kg")){
								 setTotalEstimateExpenseCwt(accountLine.getEstimateQuantity().multiply(multiplyer));
								 setTotalEstimateExpenseKg(accountLine.getEstimateQuantity());
							 }else{ 
									 if(serviceOrder.getMode()== null || serviceOrder.getMode().equalsIgnoreCase("AIR")){
										 if(miscellaneous.getEstimateGrossWeight()==null || (miscellaneous.getEstimateGrossWeight().toString()).equals(new BigDecimal("0.00").toString())) {  
							     				setTotalEstimateExpenseCwt(new BigDecimal("0")); 
										 }else{
										     setTotalEstimateExpenseCwt((miscellaneous.getEstimateGrossWeight())); 
										 }
										 if(miscellaneous.getEstimateGrossWeightKilo()==null || (miscellaneous.getEstimateGrossWeightKilo().toString()).equals(new BigDecimal("0.00").toString())){
											 setTotalEstimateExpenseKg(new BigDecimal("0")); 
										 }else{
											 setTotalEstimateExpenseKg((miscellaneous.getEstimateGrossWeightKilo()));
										 }
									 }else{
										 if(miscellaneous.getEstimatedNetWeight()==null || (miscellaneous.getEstimatedNetWeight().toString()).equals(new BigDecimal("0.00").toString())) {  
							     				setTotalEstimateExpenseCwt(new BigDecimal("0")); 
										 }else{
										     setTotalEstimateExpenseCwt((miscellaneous.getEstimatedNetWeight())); 
										 }
										 if(miscellaneous.getEstimatedNetWeightKilo()==null || (miscellaneous.getEstimatedNetWeightKilo().toString()).equals(new BigDecimal("0.00").toString())){
											 setTotalEstimateExpenseKg(new BigDecimal("0"));
										 }else{
											 setTotalEstimateExpenseKg((miscellaneous.getEstimatedNetWeightKilo())); 
										 }
								 } 
							 }
						} 
						 if(accountLine.getBasis().equalsIgnoreCase("Cft")){
							 setTotalEstimateExpenseCft(accountLine.getEstimateQuantity());
							 setTotalEstimateExpenseCbm(accountLine.getEstimateQuantity().divide(cuDivisor, 3, 0));
						 }else{
							 if(accountLine.getBasis().equalsIgnoreCase("Cbm")){
								 setTotalEstimateExpenseCft(accountLine.getEstimateQuantity().multiply(cuMultiplyer));
								 setTotalEstimateExpenseCbm(accountLine.getEstimateQuantity()); 
							 }else{							 
									 if(serviceOrder.getMode()== null || serviceOrder.getMode().equalsIgnoreCase("AIR")){
										 if(miscellaneous.getEstimateCubicFeet()==null || (miscellaneous.getEstimateCubicFeet().toString().toString()).equals(new BigDecimal("0.00").toString())){
						    				   setTotalEstimateExpenseCft(new BigDecimal("0")); 
										 }else{
										      setTotalEstimateExpenseCft(miscellaneous.getEstimateCubicFeet()); 
						    			 }
										 if(miscellaneous.getEstimateCubicMtr()==null || (miscellaneous.getEstimateCubicMtr().toString().toString()).equals(new BigDecimal("0.00").toString())){
											 setTotalEstimateExpenseCbm(new BigDecimal("0"));
										 }else{
											 setTotalEstimateExpenseCbm(miscellaneous.getEstimateCubicMtr());  
										 }
									 }else{
										 if(miscellaneous.getNetEstimateCubicFeet()==null || (miscellaneous.getNetEstimateCubicFeet().toString()).equals(new BigDecimal("0.00").toString())){
						      				   setTotalEstimateExpenseCft(new BigDecimal("0")); 
						      			   }else{
											  setTotalEstimateExpenseCft(miscellaneous.getNetEstimateCubicFeet()); 
											 
										 }if(miscellaneous.getNetEstimateCubicMtr()==null || (miscellaneous.getNetEstimateCubicMtr().toString()).equals(new BigDecimal("0.00").toString())){
											 setTotalEstimateExpenseCbm(new BigDecimal("0"));
										 }else{
											 setTotalEstimateExpenseCbm(miscellaneous.getNetEstimateCubicMtr()); 
										 }
									 } 
							 }
						 }
					 setTotalEstimateExpenseEach(accountLine.getEstimateQuantity());
					 setTotalEstimateExpenseFlat(accountLine.getEstimateQuantity());
					 setTotalEstimateExpenseHour(accountLine.getEstimateQuantity());
			      }
			 }
				
if(accountLine.getRevisionQuantity() == null || (accountLine.getRevisionQuantity().toString()).equals(new BigDecimal("0.00").toString()) ){ 
			 if(serviceOrder.getMode()== null || serviceOrder.getMode().equalsIgnoreCase("AIR"))
			 {  
				 if(miscellaneous.getActualGrossWeight()==null || (miscellaneous.getActualGrossWeight().toString()).equals(new BigDecimal("0.00").toString())) {  
						 setTotalRevisionExpenseCwt(new BigDecimal("0")); 
			    }else{
					 setTotalRevisionExpenseCwt((miscellaneous.getActualGrossWeight())); 
					 }
				if(miscellaneous.getActualGrossWeightKilo()==null || (miscellaneous.getActualGrossWeightKilo().toString()).equals(new BigDecimal("0.00").toString())){
					setTotalRevisionExpenseKg(new BigDecimal("0"));
				}else{
					setTotalRevisionExpenseKg((miscellaneous.getActualGrossWeightKilo()));
				}
				} 
			
			 else{ 
				 if(miscellaneous.getActualNetWeight()==null || (miscellaneous.getActualNetWeight().toString()).equals(new BigDecimal("0.00").toString())) {  
					 setTotalRevisionExpenseCwt(new BigDecimal("0"));
					
				 }else{
				    setTotalRevisionExpenseCwt((miscellaneous.getActualNetWeight())); 
				 }
				 if(miscellaneous.getActualNetWeightKilo()==null || (miscellaneous.getActualNetWeightKilo().toString()).equals(new BigDecimal("0.00").toString())){
					 setTotalRevisionExpenseKg(new BigDecimal("0"));
				 }else{
					 setTotalRevisionExpenseKg((miscellaneous.getActualNetWeightKilo())); 
				 } 
			 } 
			 if(serviceOrder.getMode()== null || serviceOrder.getMode().equalsIgnoreCase("AIR"))
			 {
			 
				 if(miscellaneous.getActualCubicFeet()==null || (miscellaneous.getActualCubicFeet().toString()).equals(new BigDecimal("0.00").toString())){
					    setTotalRevisionExpenseCft(new BigDecimal("0")); 
				   }else{
				       setTotalRevisionExpenseCft(miscellaneous.getActualCubicFeet()); 
			     } 
				 if(miscellaneous.getActualCubicFeet()==null || (miscellaneous.getActualCubicFeet().toString()).equals(new BigDecimal("0.00").toString())){
					 setTotalRevisionExpenseCbm(new BigDecimal("0"));
				 }else{
					 setTotalRevisionExpenseCbm(miscellaneous.getActualCubicMtr());
				 }
				 
			}
			 else{
				  if(miscellaneous.getNetActualCubicFeet()==null || (miscellaneous.getNetActualCubicFeet().toString()).equals(new BigDecimal("0.00").toString())){
						setTotalRevisionExpenseCft(new BigDecimal("0")); 
				   }else{ 
					   setTotalRevisionExpenseCft(miscellaneous.getNetActualCubicFeet()); 
				 }
				 if(miscellaneous.getNetActualCubicMtr()==null || (miscellaneous.getNetActualCubicMtr().toString()).equals(new BigDecimal("0.00").toString())){
					 setTotalRevisionExpenseCbm(new BigDecimal("0"));
				 }else{
					 setTotalRevisionExpenseCbm(miscellaneous.getNetActualCubicMtr());
				 }
				
			 }
			 if(accountLine.getBasis() !=null){ 
			 if(accountLine.getBasis().equals("each"))
			 {
					 setTotalRevisionExpenseEach(accountLine.getRevisionQuantity()); 
			 }
			 if(accountLine.getBasis().equals("flat"))
			 {
					 setTotalRevisionExpenseFlat(accountLine.getRevisionQuantity()); 
			 }
			 if(accountLine.getBasis().equals("hour"))
			 {
					 setTotalRevisionExpenseHour(accountLine.getRevisionQuantity()); 
			 }
			 }
			 }
  else{
			 if(accountLine.getBasis() !=null){
			  if(accountLine.getBasis().equalsIgnoreCase("cwt")){
				     setTotalRevisionExpenseCwt(accountLine.getRevisionQuantity());
				     setTotalRevisionExpenseKg(accountLine.getRevisionQuantity().divide(divisor, 3, 0));
				 }else{
					 if(accountLine.getBasis().equalsIgnoreCase("kg")){
						 setTotalRevisionExpenseCwt(accountLine.getRevisionQuantity().multiply(multiplyer));
						 setTotalRevisionExpenseKg(accountLine.getRevisionQuantity());
					 }else{ 
							 if(serviceOrder.getMode()== null || serviceOrder.getMode().equalsIgnoreCase("AIR")){
								 if(miscellaneous.getActualGrossWeight()==null || (miscellaneous.getActualGrossWeight().toString()).equals(new BigDecimal("0.00").toString())) {  
									 setTotalRevisionExpenseCwt(new BigDecimal("0")); 
								 }else{
								     setTotalRevisionExpenseCwt((miscellaneous.getActualGrossWeight())); 
								 }
								 if(miscellaneous.getActualGrossWeightKilo()==null || (miscellaneous.getActualGrossWeightKilo().toString()).equals(new BigDecimal("0.00").toString())){
									 setTotalRevisionExpenseKg(new BigDecimal("0"));
								 }else{
									 setTotalRevisionExpenseKg((miscellaneous.getActualGrossWeightKilo())); 
								 }
							 }else{
								 if(miscellaneous.getActualNetWeight()==null || (miscellaneous.getActualNetWeight().toString()).equals(new BigDecimal("0.00").toString())) {  
									    setTotalRevisionExpenseCwt(new BigDecimal("0")); 
								 }else{
								     setTotalRevisionExpenseCwt((miscellaneous.getActualNetWeight())); 
								 }
								 if(miscellaneous.getActualNetWeightKilo()==null || (miscellaneous.getActualNetWeightKilo().toString()).equals(new BigDecimal("0.00").toString())){
									 setTotalRevisionExpenseKg(new BigDecimal("0"));
								 }else{
									 setTotalRevisionExpenseKg((miscellaneous.getActualNetWeightKilo()));
								 }
							 } 
					}
				} 
				 if(accountLine.getBasis().equalsIgnoreCase("Cft")){
					 setTotalRevisionExpenseCft(accountLine.getRevisionQuantity());
					 setTotalRevisionExpenseCbm(accountLine.getRevisionQuantity().divide(cuDivisor, 3, 0));
				 }else{
					 if(accountLine.getBasis().equalsIgnoreCase("Cbm")){
						 setTotalRevisionExpenseCft(accountLine.getRevisionQuantity().multiply(cuMultiplyer));
						 setTotalRevisionExpenseCbm(accountLine.getRevisionQuantity()); 
					 }else{ 
							 if(serviceOrder.getMode()== null || serviceOrder.getMode().equalsIgnoreCase("AIR")){
								 if(miscellaneous.getActualCubicFeet()==null || (miscellaneous.getActualCubicFeet().toString().toString()).equals(new BigDecimal("0.00").toString())){
									 setTotalRevisionExpenseCft(new BigDecimal("0")); 
								   }else{
								     setTotalRevisionExpenseCft(miscellaneous.getActualCubicFeet()); 
								   }
								 if(miscellaneous.getActualCubicMtr()==null || (miscellaneous.getActualCubicMtr().toString().toString().equals(new BigDecimal("0.00").toString()))){
									 setTotalRevisionExpenseCbm(new BigDecimal("0"));
								 }else{
									 setTotalRevisionExpenseCbm(miscellaneous.getActualCubicMtr());
								 }
								 
							 }else{
								 if(miscellaneous.getNetActualCubicFeet()==null || (miscellaneous.getNetActualCubicFeet().toString().equals(new BigDecimal("0.00").toString()))){
									 setTotalRevisionExpenseCft(new BigDecimal("0")); 
					 			   }else{
									 setTotalRevisionExpenseCft(miscellaneous.getNetActualCubicFeet()); 
								 }
								 if(miscellaneous.getNetActualCubicMtr()==null || (miscellaneous.getNetActualCubicMtr().toString().equals(new BigDecimal("0.00").toString()))){
									 setTotalRevisionExpenseCbm(new BigDecimal("0"));
								 }else{
									 setTotalRevisionExpenseCbm(miscellaneous.getNetActualCubicMtr());
								 }
							 }
						
						 }
				 }
			     setTotalRevisionExpenseEach(accountLine.getRevisionQuantity());
				 setTotalRevisionExpenseFlat(accountLine.getRevisionQuantity());
				 setTotalRevisionExpenseHour(accountLine.getRevisionQuantity());
			  }
			}


maxAccountLine = Long.parseLong(accountLineManager.findMaximumAccountLine(serviceOrder.getShipNumber()).get(0).toString());
   minAccountLine=  Long.parseLong(accountLineManager.findMinimumAccountLine(serviceOrder.getShipNumber()).get(0).toString());
   countAccountLine =  accountLineManager.findCountAccountLine(serviceOrder.getShipNumber()).get(0).toString();
   try{ 
   if(trackingStatus.getSoNetworkGroup() && billingCMMContractType && accountLine.getCreatedBy()!=null && accountLine.getCreatedBy().trim().equalsIgnoreCase("Networking")){
			if(trackingStatus.getUtsiNetworkGroup()){  

					
			}else{

			}
   }
   }catch(Exception e){
	   e.printStackTrace();		
   }
   try{
			  
			  if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			  	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
			  }
			  
			    if(trackingStatus.getSoNetworkGroup() && billingDMMContractType && accountLine.getCreatedBy()!=null && accountLine.getCreatedBy().trim().equalsIgnoreCase("Networking")){
			}
			}catch(Exception e){
				 e.printStackTrace();	
			}
			try{
			  if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup())	{
				   String networkSynchedId=	accountLineManager.getNetworkSynchedId(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
			      	if(!(networkSynchedId.equals(""))){
			      		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
			      		Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
			      		boolean accNonEditable=false;
						 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
							 accNonEditable =true;
						 }
						 if(accNonEditable){
							 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
				      			 utsiRecAccDateFlag=true; 
				      		}
							 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
				      			 utsiPayAccDateFlag=true;
				      		}
						 }else{
			      		if(synchedAccountLine.getRecAccDate()!=null && accountLine.getRecAccDate()== null){
			      			 utsiRecAccDateFlag=true; 
			      		}
			      		if(synchedAccountLine.getPayAccDate()!=null && accountLine.getRecAccDate()== null){
			      			 utsiPayAccDateFlag=true;
			      		}
						 }
			      		
			      	}  
			  }
				
			}catch(Exception e){
				e.printStackTrace();
			}
			String permKey = sessionCorpID +"-"+"component.partner.vatBillingGroup.edit";
			boolean visibilityForCorpId=false;
			 visibilityForCorpId=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;	
		 if(visibilityForCorpId){
		    String flag="";
			String vatBillimngGroupDes="";
			String partnerVatDec="";
			if(accountLine.getBillToCode()!=null && !accountLine.getBillToCode().equals("")){
				String vatBillimngGroupCode="";
			List vatBillimngGroupCode1=partnerPrivateManager.findVatBillimngGroup(sessionCorpID, accountLine.getBillToCode());
			Iterator listIterator= vatBillimngGroupCode1.iterator();
			while (listIterator.hasNext()) {
			 Object [] row=(Object[])listIterator.next();
				if(row[0]!=null) {
				 vatBillimngGroupCode=row[0].toString();  
				 }
				 if(row[1]!=null){
				partnerVatDec = row[1].toString();  
				}
				}
			if(vatBillingGroupList.containsKey(vatBillimngGroupCode)){
				vatBillimngGroupDes =vatBillingGroupList.get(vatBillimngGroupCode);
			}
				 flex1  = refMasterManager.findPartnerVatBillingCode(vatBillimngGroupDes, sessionCorpID,accountLine.getCompanyDivision());
				if(vatBillimngGroupCode!=null && !vatBillimngGroupCode.equals("") && flex1!=null && (!(flex1.trim().equals("")))){
					  euVatList=refMasterManager.findVatDescriptionList(sessionCorpID, "EUVAT", flex1, accountLine.getRecVatDescr(),partnerVatDec,"Y");
				}
			}
			
			if(accountLine.getVendorCode()!=null && !accountLine.getVendorCode().equals("")){
				String vatBillimngGroupCode="";
				List vatBillimngGroupCode1=partnerPrivateManager.findVatBillimngGroup(sessionCorpID, accountLine.getVendorCode());
				Iterator listIterator= vatBillimngGroupCode1.iterator();
				while (listIterator.hasNext()) {
				 Object [] row=(Object[])listIterator.next();
					if(row[0]!=null) {
					 vatBillimngGroupCode=row[0].toString();  
					 }
					 if(row[1]!=null){
					partnerVatDec = row[1].toString();  
					}
					}
				if(vatBillingGroupList.containsKey(vatBillimngGroupCode)){
					vatBillimngGroupDes =vatBillingGroupList.get(vatBillimngGroupCode);	
				}
				   flex2 = refMasterManager.findAccountLinePayVatBillingCode(vatBillimngGroupDes, sessionCorpID, accountLine.getCompanyDivision());
					if(vatBillimngGroupCode!=null && !vatBillimngGroupCode.equals("") && flex2!=null && (!(flex2.trim().equals("")))){
						 payVatList=refMasterManager.findPayVatDescriptionList(sessionCorpID, "PAYVATDESC", flex2, accountLine.getPayVatDescr(), "Y");
					}
				}
		 }
            }	    	
			    	else
			    	{ 
			    		accountLine = new AccountLine(); 
			    		accountLine.setActivateAccPortal(true);
			    		boolean activateAccPortal =true;
			    		try{
			    		if(accountLineAccountPortalFlag){	
			    		activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
			    		accountLine.setActivateAccPortal(activateAccPortal);
			    		}
			    		}catch(Exception e){
			    			 e.printStackTrace();
			    		}
			    		try {
						myfileDocumentList=accountLineManager.getDocumentList(sessionCorpID,accountLine.getVendorCode(),serviceOrder.getShipNumber());
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
			    		///Code for AccountLine division
						if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
								
						String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
							try{  
								String roles[] = agentRoleValue.split("~");
						          if(roles[3].toString().equals("hauler")){
							           if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
							        	   accountLine.setDivision("01");
							           }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
							        	   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
							        		   accountLine.setDivision("01");
							        	   }else{ 
								        	   String divisionTemp="";
												if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
												divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
												}
												if(!divisionTemp.equalsIgnoreCase("")){
													accountLine.setDivision(divisionTemp);
												}else{
													accountLine.setDivision(null);
												}
								             
							        	   }
							           }else{
							        	   String divisionTemp="";
											if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
											divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
											}
											if(!divisionTemp.equalsIgnoreCase("")){
												accountLine.setDivision(divisionTemp);
											}else{
												accountLine.setDivision(null);
											}
							           }
						          }else{
						        	  String divisionTemp="";
										if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
										divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
										}
										if(!divisionTemp.equalsIgnoreCase("")){
											accountLine.setDivision(divisionTemp);
										}else{
											accountLine.setDivision(null);
										}
						          }
							}catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
				        getGlTypeList=new ArrayList();
				        getCommissionList=new ArrayList(); 	
				        
			    		if((miscellaneousManager.checkById(sid)).isEmpty()){
			        		miscellaneous = new Miscellaneous(); 
			        	}else{
			        		   miscellaneous = miscellaneousManager.get(sid);
			        		if(serviceOrder.getMode()== null || serviceOrder.getMode().equalsIgnoreCase("AIR"))
			        		{
			        		   
			        			   if(miscellaneous.getEstimateGrossWeight()==null || (miscellaneous.getEstimateGrossWeight().toString()).equals(new BigDecimal("0.00").toString())) {  
				        				setTotalEstimateExpenseCwt(new BigDecimal("0")); 
			        			   }
			        			   else{
				        			   setTotalEstimateExpenseCwt((miscellaneous.getEstimateGrossWeight()));  
				        			}
			        			   if(miscellaneous.getEstimateGrossWeightKilo()==null || (miscellaneous.getEstimateGrossWeightKilo().toString()).equals(new BigDecimal("0.00").toString())){
			        				   setTotalEstimateExpenseKg(new BigDecimal("0"));
			        			   }else{
			        				   setTotalEstimateExpenseKg((miscellaneous.getEstimateGrossWeightKilo()));
			        			   }
			        			   if(miscellaneous.getActualGrossWeight()==null || (miscellaneous.getActualGrossWeight().toString()).equals(new BigDecimal("0.00").toString())){
				        				setTotalRevisionExpenseCwt(new BigDecimal("0")); 
				        			}else{
				        		       setTotalRevisionExpenseCwt((miscellaneous.getActualGrossWeight())); 
				        		   }
			        			   if(miscellaneous.getActualGrossWeightKilo()==null || (miscellaneous.getActualGrossWeightKilo().toString()).equals(new BigDecimal("0.00").toString())){
			        				   setTotalRevisionExpenseKg(new BigDecimal("0"));
			        			   }else{
			        				   setTotalRevisionExpenseKg((miscellaneous.getActualGrossWeightKilo()));
			        			   } 
			        				
			        		}  else
			        		   {   
			        				
			        			if(miscellaneous.getEstimatedNetWeight()==null || (miscellaneous.getEstimatedNetWeight().toString()).equals(new BigDecimal("0.00").toString())) {  
			        				setTotalEstimateExpenseCwt(new BigDecimal("0")); 
			        			}else{
			        			   setTotalEstimateExpenseCwt((miscellaneous.getEstimatedNetWeight())); 
			        			}
			        			if(miscellaneous.getEstimatedNetWeightKilo()==null || (miscellaneous.getEstimatedNetWeightKilo().toString()).equals(new BigDecimal("0.00").toString())){
			        				setTotalEstimateExpenseKg(new BigDecimal("0"));
			        			}else{
			        				setTotalEstimateExpenseKg((miscellaneous.getEstimatedNetWeightKilo()));
			        			}
			        			if(miscellaneous.getActualNetWeight()==null || (miscellaneous.getActualNetWeight().toString()).equals(new BigDecimal("0.00").toString())){
			        				setTotalRevisionExpenseCwt(new BigDecimal("0")); 
			        			}else{
			        		       setTotalRevisionExpenseCwt((miscellaneous.getActualNetWeight())); 
			        		   }
			        		   if(miscellaneous.getActualNetWeightKilo()==null || (miscellaneous.getActualNetWeightKilo().toString()).equals(new BigDecimal("0.00").toString())){
			        			   setTotalRevisionExpenseKg(new BigDecimal("0"));
			        		   }else{
			        			   setTotalRevisionExpenseKg((miscellaneous.getActualNetWeightKilo()));
			        		   }
				        			
				    	}      
			        		   if(serviceOrder.getMode()== null || serviceOrder.getMode().equalsIgnoreCase("AIR"))
						 {
			        		   
			        			   if(miscellaneous.getEstimateCubicFeet()==null || (miscellaneous.getEstimateCubicFeet().toString().equals(new BigDecimal("0.00").toString()))){
			        				   setTotalEstimateExpenseCft(new BigDecimal("0")); 
			        			   }
			        			   else{
			        				   setTotalEstimateExpenseCft(miscellaneous.getEstimateCubicFeet()); 
			        			   }
			        			   if(miscellaneous.getEstimateCubicMtr()==null || (miscellaneous.getEstimateCubicMtr().toString().equals(new BigDecimal("0.00").toString()))){
			        				   setTotalEstimateExpenseCbm(new BigDecimal("0"));
			        			   }else{
			        				   setTotalEstimateExpenseCbm(miscellaneous.getEstimateCubicMtr()); 
			        			   }
			        			   if(miscellaneous.getActualCubicFeet()==null || (miscellaneous.getActualCubicFeet().toString().equals(new BigDecimal("0.00").toString()))){
			        				   setTotalRevisionExpenseCft(new BigDecimal("0"));  
			        			   }
			        			   else{
			        				   setTotalRevisionExpenseCft(miscellaneous.getActualCubicFeet()); 
			        			   }
			        			   if(miscellaneous.getActualCubicMtr()==null || (miscellaneous.getActualCubicMtr().toString().equals(new BigDecimal("0.00").toString()))){
			        				   setTotalRevisionExpenseCbm(new BigDecimal("0"));
			        			   }else{
			        				   setTotalRevisionExpenseCbm(miscellaneous.getActualCubicMtr()); 
			        			   } 
			        		   		        	
			        	}else
			        	{
			        		
			        			   if(miscellaneous.getNetEstimateCubicFeet()==null || (miscellaneous.getNetEstimateCubicFeet().toString().equals(new BigDecimal("0.00").toString()))){
			        				   setTotalEstimateExpenseCft(new BigDecimal("0")); 
			        			   }
			        			   else{
			        				   setTotalEstimateExpenseCft(miscellaneous.getNetEstimateCubicFeet()); 
			        			   }
			        			   if(miscellaneous.getNetEstimateCubicMtr()==null || (miscellaneous.getNetEstimateCubicMtr().toString().equals(new BigDecimal("0.00").toString()))){
			        				   setTotalEstimateExpenseCbm(new BigDecimal("0"));
			        			   }else{
			        				   setTotalEstimateExpenseCbm(miscellaneous.getNetEstimateCubicMtr());
			        			   }
			        			   if(miscellaneous.getNetActualCubicFeet()==null || (miscellaneous.getNetActualCubicFeet().toString().equals(new BigDecimal("0.00").toString()))){
			        				   setTotalRevisionExpenseCft(new BigDecimal("0"));  
			        			   }
			        			   else{
			        				   setTotalRevisionExpenseCft(miscellaneous.getNetActualCubicFeet()); 
			        			   }
			        			   if(miscellaneous.getNetActualCubicMtr()==null || (miscellaneous.getNetActualCubicMtr().toString().equals(new BigDecimal("0.00").toString()))){
			        				   setTotalRevisionExpenseCbm(new BigDecimal("0"));
			        			   }else{
			        				   setTotalRevisionExpenseCbm(miscellaneous.getNetActualCubicMtr());
			        			   } 
			        		  
			        	} 
			        	} 
			    		billing=billingManager.get(sid); 
			    		
			    		accountLine.setContract(billing.getContract());
			    		accountLine.setBillToCode(billing.getBillToCode());
			    		if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
			    		accountLine.setRecVatDescr(billing.getPrimaryVatCode());
			    		if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
			    		String recVatPercent="0";
			    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){ 
					    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode()); 
			    		}
			    		String recVatGL="";
			    		if(euvatRecGLMap.containsKey(billing.getPrimaryVatCode())){
			    			recVatGL =euvatRecGLMap.get(billing.getPrimaryVatCode());
			    			accountLine.setRecVatGl(recVatGL);
			    		}
			    		if(qstEuvatRecGLMap.containsKey(billing.getPrimaryVatCode())){
			    			recVatGL =qstEuvatRecGLMap.get(billing.getPrimaryVatCode());
			    			accountLine.setQstRecVatGl(recVatGL);
			    		}
			    		
						accountLine.setRecVatPercent(recVatPercent);
			    		}
			    		}
			    		accountLine.setBillToName(billing.getBillToName());
			    		accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
			    		accountLine.setNetworkBillToName(billing.getNetworkBillToName());
			    		accountLine.setExternalReference(billing.getBillToReference());
			    		accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
			    		try{
			    		if((!(trackingStatus.getSoNetworkGroup())) && billingCMMContractType && networkAgent && (!(trackingStatus.getAccNetworkGroup()))){
			    			String actCode="";
			    			accountLine.setVendorCode(serviceOrder.getBookingAgentCode());  
			    			accountLine.setEstimateVendorName(serviceOrder.getBookingAgentName());
					         if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
					 			actCode=partnerManager.getAccountCrossReference(serviceOrder.getBookingAgentCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
					 		}else{
					 			actCode=partnerManager.getAccountCrossReferenceUnique(serviceOrder.getBookingAgentCode(),sessionCorpID);
					 		}
					         accountLine.setActgCode(actCode);	
			    		}else{}
			    		}catch(Exception e){
			    			 e.printStackTrace();
			    		}
			    		if(systemDefaultCommissionJob.contains(serviceOrder.getJob())){
			    			if(miscellaneous.getCarrier()!=null){
			    				accountLine.setTruckNumber(miscellaneous.getCarrier()); 
			    				try{
			    					String vendorCodeData=accountLineManager.getOwnerPayTo(sessionCorpID,miscellaneous.getCarrier());
									String[] vendorCodearrayData=vendorCodeData.split("~"); 
									String vendorCode=vendorCodearrayData[0];
									String vendorName=vendorCodearrayData[1];
									if(!(vendorCode.equalsIgnoreCase("NO"))){
										accountLine.setVendorCode(vendorCode);
									}
									if(!(vendorName.equalsIgnoreCase("NO"))){
										accountLine.setEstimateVendorName(vendorName);
									}
								}catch(Exception e){
									 e.printStackTrace();	
								}
									
			    			}
								
			    			
			    		}
			    		if((billingManager.checkById(sid)).isEmpty())
			        	{ 	
			        		billingFlag=0; 
			        	}
			        	else
			        	{ 	
			        		billingFlag=1; 
			        	}
			    		accountLine.setCorpID(sessionCorpID);
			    		maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
			            if ( maxLineNumber.get(0) == null ) {          
			             	accountLineNumber = "001";
			             }else {
			             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
			                 if((autoLineNumber.toString()).length() == 2) {
			                 	accountLineNumber = "0"+(autoLineNumber.toString());
			                 }
			                 else if((autoLineNumber.toString()).length() == 1) {
			                 	accountLineNumber = "00"+(autoLineNumber.toString());
			                 } 
			                 else {
			                 	accountLineNumber=autoLineNumber.toString();
			                 }
			             }
			            accountLine.setAccountLineNumber(accountLineNumber); 
			    		accountLine.setShipNumber(serviceOrder.getShipNumber());
			    		accountLine.setCreatedOn(new Date());
			    		accountLine.setUpdatedOn(new Date()); 
			    		accountLine.setStatus(true);
			    		if(sessionCorpID !=null && (!(sessionCorpID.equalsIgnoreCase("UTSI")))){
			    		accountLine.setPaymentStatus("Not paid");
			    		}
			    		if(multiCurrency.equalsIgnoreCase("Y")){
			    			if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals(""))
			    			{
			    				String billingCurrency="";
			    				if(contractType){
			    				billingCurrency=serviceOrderManager.getBillingCurrencyValue(billing.getBillToCode(),sessionCorpID);
			    				}
					    		if(!billingCurrency.equalsIgnoreCase("")&& contractType)
					    		{
					    			accountLine.setRecRateCurrency(billingCurrency);
					    			accountLine.setEstSellCurrency(billingCurrency);
					    			accountLine.setRevisionSellCurrency(billingCurrency);
					    			accountLine.setRacValueDate(new Date());
					    			accountLine.setEstSellValueDate(new Date());
					    			accountLine.setRevisionSellValueDate(new Date());

					    			accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,billingCurrency);
					    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
					    				try{
					    				accountLine.setRecRateExchange(new BigDecimal(accExchangeRateList.get(0).toString()));
					    				accountLine.setEstSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
					    				accountLine.setRevisionSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
					    				}catch(Exception e){
					    				accountLine.setRecRateExchange(new BigDecimal(1.0000));
					    				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
					    				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
					    				e.printStackTrace();
					    				}
					    			}else{
					    				accountLine.setRecRateExchange(new BigDecimal(1.0000));
					    				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
					    				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
					    			}
					    		}else{
					    			
					    			accountLine.setRecRateCurrency(baseCurrency);
					    			accountLine.setEstSellCurrency(baseCurrency);
					    			accountLine.setRevisionSellCurrency(baseCurrency);
					    			accountLine.setRacValueDate(new Date());
					    			accountLine.setRecRateExchange(new BigDecimal(1.0000));
					    			accountLine.setEstSellValueDate(new Date());
					    			accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
					    			accountLine.setRevisionSellValueDate(new Date());
					    			accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
			    			}
			    			}
			    			else
			    			{
			    				String baseCurrencyCompanyDivision1=""; 
			    				String billingCurrency="";
			    				if(contractType){
			    				billingCurrency=serviceOrderManager.getBillingCurrencyValue(billing.getBillToCode(),sessionCorpID);
			    				}
					    		if(!billingCurrency.equalsIgnoreCase("")&& contractType)
					    		{
					    			accountLine.setRecRateCurrency(billingCurrency);
					    			accountLine.setEstSellCurrency(billingCurrency);
					    			accountLine.setRevisionSellCurrency(billingCurrency);
					    			baseCurrencyCompanyDivision1=billingCurrency;
					    		}else{
					    			accountLine.setRecRateCurrency(baseCurrencyCompanyDivision);
					    			accountLine.setEstSellCurrency(baseCurrencyCompanyDivision);
					    			accountLine.setRevisionSellCurrency(baseCurrencyCompanyDivision);
					    			baseCurrencyCompanyDivision1=baseCurrencyCompanyDivision;
					    		}
			    			accountLine.setRacValueDate(new Date());
			    			accountLine.setEstSellValueDate(new Date());
			    			accountLine.setRevisionSellValueDate(new Date());
			    			accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision1);
			    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
			    				try{
			    				accountLine.setRecRateExchange(new BigDecimal(accExchangeRateList.get(0).toString()));
			    				accountLine.setEstSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
			    				accountLine.setRevisionSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
			    				}catch(Exception e){
			    				accountLine.setRecRateExchange(new BigDecimal(1.0000));
			    				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
			    				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
			    				e.printStackTrace();
			    				}
			    			}else{
			    				accountLine.setRecRateExchange(new BigDecimal(1.0000));
			    				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
			    				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
			    			}
			    			}
			    			
			    		}
			    		if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
			    		{
			    		accountLine.setEstCurrency(baseCurrency);
						accountLine.setRevisionCurrency(baseCurrency);	
						accountLine.setCountry(baseCurrency);
						accountLine.setEstValueDate(new Date());
						accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setRevisionValueDate(new Date());
						accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setValueDate(new Date());
						accountLine.setExchangeRate(new Double(1.0));
			    		}
			    		else
			    		{
			    		accountLine.setEstCurrency(baseCurrencyCompanyDivision);
						accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
						accountLine.setCountry(baseCurrencyCompanyDivision);
						accountLine.setEstValueDate(new Date());
						accountLine.setRevisionValueDate(new Date());
						accountLine.setValueDate(new Date());
						//accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision); 
						accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
						if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
							try{
							accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
							accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
							accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
							}catch(Exception e){
								accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
			    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
			    				accountLine.setExchangeRate(new Double(1.0));	
			    				e.printStackTrace();
							}
						}else{
							accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
							accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
							accountLine.setExchangeRate(new Double(1.0));
						}
			    		}
						
						accountLine.setLocalAmount(new BigDecimal("0.00"));
			    		if(serviceOrder.getJob().equalsIgnoreCase("UVL")||serviceOrder.getJob().equalsIgnoreCase("DOM")||serviceOrder.getJob().equalsIgnoreCase("MVL")){
			    		List branchCodeList=accountLineManager.getBranchCode(serviceOrder.getCompanyDivision(),sessionCorpID);
			    	    if(branchCodeList!=null && (!branchCodeList.isEmpty())){
			    		String branchCode=branchCodeList.get(0).toString(); 
			    		accountLine.setBranchCode(branchCode);
			    	    }
			    	}
			    		String permKey = sessionCorpID +"-"+"component.partner.vatBillingGroup.edit";
						boolean visibilityForCorpId=false;
						 visibilityForCorpId=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;	
					 if(visibilityForCorpId){
			    		String flag="";
			    		String recVatDescr="";
			    		String payVatDescr="";
						String vatBillimngGroupDes="";
						String partnerVatDec="";
						if(billing.getBillToCode()!=null && !billing.getBillToCode().equals("")){
							String vatBillimngGroupCode="";
						List vatBillimngGroupCode1=partnerPrivateManager.findVatBillimngGroup(sessionCorpID, billing.getBillToCode());
						Iterator listIterator= vatBillimngGroupCode1.iterator();
						while (listIterator.hasNext()) {
						 Object [] row=(Object[])listIterator.next();
							if(row[0]!=null) {
							 vatBillimngGroupCode=row[0].toString();  
							 }
							 if(row[1]!=null){
							partnerVatDec = row[1].toString();  
							}
							}
						if(vatBillingGroupList.containsKey(vatBillimngGroupCode)){
							vatBillimngGroupDes =vatBillingGroupList.get(vatBillimngGroupCode);	
						}
							 flex1  = refMasterManager.findPartnerVatBillingCode(vatBillimngGroupDes, sessionCorpID,serviceOrder.getCompanyDivision());
							if(vatBillimngGroupCode!=null && !vatBillimngGroupCode.equals("") && flex1!=null && (!(flex1.trim().equals("")))){
								  euVatList=refMasterManager.findVatDescriptionList(sessionCorpID, "EUVAT", flex1, recVatDescr,partnerVatDec,"Y");
							}
							
						}
					 }

			    		accountLine.setVATExclude(false);
			    		if(systemDefault!=null){
					         try{
							        if(systemDefault.getPayableVat()!=null && !systemDefault.getPayableVat().equalsIgnoreCase("")){
							         accountLine.setPayVatDescr(systemDefault.getPayableVat());
							         if(payVatPercentList.containsKey(systemDefault.getPayableVat())){
							        	 accountLine.setPayVatPercent(payVatPercentList.get(systemDefault.getPayableVat()))	; 
							         }
							        }
							         if(systemDefault.getReceivableVat()!=null && (!(systemDefault.getReceivableVat().equals("")))){	 
							         if(accountLine.getRecVatDescr() ==null || accountLine.getRecVatDescr().toString().trim().equals("")){
							          accountLine.setRecVatDescr(systemDefault.getReceivableVat());
							         
							         accountLine.setRevisionVatDescr(systemDefault.getReceivableVat());
							         accountLine.setEstVatDescr(systemDefault.getReceivableVat()); 
										
							         if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
							        	 accountLine.setRecVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()))	;
							         }
							         }
					                 }
							         }catch(Exception e){
							        	 e.printStackTrace();	 
							         }
							         }
			    	}
shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
   countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
   
getNotesForIconChange();

try {
	if (id != null){
	        if(billingDMMContractType && ("DMMFEE".equals(accountLine.getChargeCode()) || "DMMFXFEE".equals(accountLine.getChargeCode()))){
				disableALL = "YES";
			}else if(trackingStatus.getSoNetworkGroup()  && (billingCMMContractType || billingDMMContractType) && ("Networking".equals(accountLine.getCreatedBy()) || ((accountLine.getCreatedBy().indexOf("Stg Bill")>=0 && accountLine.getNetworkSynchedId() !=null )  &&  !(trackingStatus.getAccNetworkGroup()) ))){
				disableALL = "YES";
			}else if(trackingStatus.getSoNetworkGroup()  && billingCMMContractType && "MGMTFEE".equals(accountLine.getChargeCode())){
				disableALL = "YES";
			}else if(trackingStatus.getSoNetworkGroup()  && billingDMMContractType && ("DMMFEE".equals(accountLine.getChargeCode()) || "DMMFXFEE".equals(accountLine.getChargeCode()) )){
				disableALL = "YES";
			}else if("MGMTFEE".equals(accountLine.getChargeCode())){
				disableALL = "YES";
			}else if(billingDMMContractType && ("DMMFEE".equals(accountLine.getChargeCode()) || "DMMFXFEE".equals(accountLine.getChargeCode())) ){
				disableALL = "YES";
			}else if(trackingStatus.getSoNetworkGroup()  && (billingCMMContractType || billingDMMContractType) && ("Networking".equals(accountLine.getCreatedBy()))){
				disableALL = "YES";
			}
	}
} catch (Exception e) {
			e.printStackTrace();
}
		if(myFileForVal!=null && !myFileForVal.equalsIgnoreCase("") && myFileForVal.equalsIgnoreCase("AC")){
			resultType="errorNoFile";
		}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    
	    	 return "errorlog";
		}
	
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    return SUCCESS;
    }
	private String myFileForVal;
	private String resultType;
	@SkipValidation 
	  public String myfileDocument(){	
		try{
		myfileDocumentList=accountLineManager.getDocumentList(sessionCorpID,partnerCode,shipNumber);	 
		}	catch(Exception e)
		{
			 e.printStackTrace();
		}
	  return SUCCESS;
	  }
	@SkipValidation 	
	public String  setCurrencyCompanyDivision()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString();
			baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
		
			baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(companyDivision,sessionCorpID);
			if(baseCurrencyCompanyDivision.equals("")||baseCurrencyCompanyDivision==null)
			{
				baseCurrencyCompanyDivision=baseCurrency;
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation 
	public String  updateAcc()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid); 
			billing=billingManager.get(sid);
			accountLineManager.updateAcc(billing.getBillToCode(),billing.getBillToName(),sid); 
			String key = "AccountLine Has been Successfully Updated";  
			saveMessage(getText(key));
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			
			if("category".equals(vanLineAccountView)){
			   vanLineAccCategoryURL = "?sid="+serviceOrder.getId()+"&vanLineAccountView="+vanLineAccountView;
			   return "category"; 
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
        
		return SUCCESS;
	}
	@SkipValidation 
	public String countInvoice()
	{try{
		countInvoice=accountLineManager.countInvoice(sid);
	}
	catch(Exception e)
	{
		 e.printStackTrace();
	}
		return SUCCESS;
	}
	
	@SkipValidation 
	public String  findReverseInvoiceList()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			shipNumber=serviceOrder.getShipNumber(); 
			reverseInvoiceList =accountLineManager.getReverseInvoiceList(serviceOrder.getShipNumber(),sessionCorpID);
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

	    	 return "errorlog";
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation 
	public String  findCopyInvoiceList()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			shipNumber=serviceOrder.getShipNumber(); 
			reverseInvoiceList =accountLineManager.getCopyInvoiceList(serviceOrder.getShipNumber(),sessionCorpID);
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation 
	public String  findInvoiceToDelete()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			shipNumber=serviceOrder.getShipNumber(); 
			invoiceToDeleteList =accountLineManager.findInvoiceToDelete(serviceOrder.getShipNumber(),sessionCorpID);
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation 
	public String getInvoiceToDeleteDitail()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			shipNumber=serviceOrder.getShipNumber(); 
			idCheck="";
			invoiceDetailList=accountLineManager.getInvoiceToDeleteDitail(serviceOrder.getShipNumber(),invoiceToDelete,sessionCorpID);
			findInvoiceToDelete();
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String updateInvoiceToDelete(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			shipNumber=serviceOrder.getShipNumber();  
			if(idCheck.equals("")||idCheck.equals(","))
			  {
			  }
			 else
			  {
				 idCheck=idCheck.trim();
			    if(idCheck.indexOf(",")==0)
				 {
			    	idCheck=idCheck.substring(1);
				 }
				if(idCheck.lastIndexOf(",")==idCheck.length()-1)
				 {
					idCheck=idCheck.substring(0, idCheck.length()-1);
				 }
			    String[] arrayId=idCheck.split(",");
			    int arrayLength = arrayId.length;
				for(int i=0;i<arrayLength;i++)
				 {	
					accountLine = accountLineManager.get(Long.parseLong(arrayId[i]));
				    accountLine.setRecInvoiceNumber("");
				    accountLine.setReceivedInvoiceDate(null);
				    accountLine.setServiceOrder(serviceOrder);
			        accountLine.setUpdatedOn(new Date());
			        accountLine.setUpdatedBy(getRequest().getRemoteUser());
			        accountLineManager.save(accountLine); 
			     }
			    }
			
			getInvoiceToDeleteDitail();
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}

		return SUCCESS;	
	}
	
	@SkipValidation
	public String findTotalInvoiceList()
	{		
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			shipNumber=serviceOrder.getShipNumber(); 
			systemDefaultmiscVl=serviceOrderManager.findmiscVlSystemDefault(sessionCorpID).get(0).toString();
			multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString();
			if(multiCurrency.equalsIgnoreCase("Y")) {
				if(serviceOrder.getJob().equals("HVY")){
					BigDecimal sum = new BigDecimal(( accountLineManager.getProjectedActualRevenueSumHVY(serviceOrder.getShipNumber())).get(0).toString());
					totalInvoiceList = accountLineManager.getInvoiceCurrencyList(serviceOrder.getShipNumber(),sessionCorpID,divisionFlag); 
				   if(!totalInvoiceList.isEmpty()||totalInvoiceList.get(0)!=null){
					   totalInvoiceList.get(0).setActualRevenueSum(new BigDecimal(totalInvoiceList.get(0).getActualRevenueSum().toString()).add(sum));
				    }	
				}else{
			           totalInvoiceList =accountLineManager.getInvoiceCurrencyList(serviceOrder.getShipNumber(),sessionCorpID,divisionFlag); }	
			 	}
			else { 
				if(serviceOrder.getJob().equals("HVY")){
					BigDecimal sum = new BigDecimal(( accountLineManager.getProjectedActualRevenueSumHVY(serviceOrder.getShipNumber())).get(0).toString());
					totalInvoiceList = accountLineManager.getTotalInvoiceList(serviceOrder.getShipNumber(),sessionCorpID,divisionFlag); 
				   if(!totalInvoiceList.isEmpty()||totalInvoiceList.get(0)!=null){
					   totalInvoiceList.get(0).setActualRevenueSum(new BigDecimal(totalInvoiceList.get(0).getActualRevenueSum().toString()).add(sum));
				    }	
				}else{
					if(systemDefaultmiscVl.contains(serviceOrder.getJob())){
				     totalInvoiceList =accountLineManager.getTotalDistributionInvoiceList(serviceOrder.getShipNumber(),sessionCorpID,divisionFlag); 
				    }else{
				     totalInvoiceList =accountLineManager.getTotalInvoiceList(serviceOrder.getShipNumber(),sessionCorpID,divisionFlag); 
				    }
			    }
			}
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

		return SUCCESS;
	}
	@SkipValidation
	public String  findVendorInvoiceList(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			shipNumber=serviceOrder.getShipNumber(); 
			totalVendorInvoiceList =accountLineManager.getTotalVendorInvList(serviceOrder.getShipNumber(),sessionCorpID);
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation 
	public String  resetSendtoDates()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		serviceOrder = serviceOrderManager.get(sid); 
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		shipNumber=serviceOrder.getShipNumber(); 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

		return SUCCESS;
	}
	
	@SkipValidation
	public String resetAllEstimateDates()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			shipNumber=serviceOrder.getShipNumber();
			int i =accountLineManager.updateAllEstimateDate(shipNumber,sessionCorpID); 
			if(i>0)
			{
				String key = "Estimate Sent to client date have been updated for "+i + " account line";   
			    saveMessage(getText(key));
			}
			if(i==0)
			{
				String key = "There are no Estimate Sent to client date for reset";   
			    saveMessage(getText(key));
				
			}
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String findActualSenttoDates()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			shipNumber=serviceOrder.getShipNumber(); 
			actualSenttoDatesList =accountLineManager.findActualSenttoDates(serviceOrder.getShipNumber(),sessionCorpID);
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String updateActualDate()
	{  
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid);
			int count=0;
			if(recInvoiceCheck.equals("")||recInvoiceCheck.equals(","))
			  {
			  }
			 else
			  {
				 recInvoiceCheck=recInvoiceCheck.trim();
			    if(recInvoiceCheck.indexOf(",")==0)
				 {
			    	recInvoiceCheck=recInvoiceCheck.substring(1);
				 }
				if(recInvoiceCheck.lastIndexOf(",")==recInvoiceCheck.length()-1)
				 {
					recInvoiceCheck=recInvoiceCheck.substring(0, recInvoiceCheck.length()-1);
				 }
			    String[] arrayRecInvoice=recInvoiceCheck.split(",");
			    int arrayLength = arrayRecInvoice.length;
			    
				for(int i=0;i<arrayLength;i++)
				 {	
				   String invoiceActualSent=arrayRecInvoice[i]; 
			       
			      int updateLineNumber=accountLineManager.updateActualSenttoDates(serviceOrder.getShipNumber(),invoiceActualSent,sessionCorpID);
			      count=count+updateLineNumber;
				 }
			 }
			String key = "Actual Sent to client date have been updated for "+count + " account line";  
			saveMessage(getText(key));
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
    	//findActualSenttoDates();
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    
	}
	@SkipValidation 
	public String  openBillingWizard()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			trackingStatus=trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid); 
			customerFile = serviceOrder.getCustomerFile();
			shipNumber=serviceOrder.getShipNumber(); 
			BillingWizardList =accountLineManager.getBillingWizardList(serviceOrder.getShipNumber(),sessionCorpID);
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation 
	public String  invoiceDetailBilling()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			trackingStatus=trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid); 
			customerFile = serviceOrder.getCustomerFile();
			shipNumber=serviceOrder.getShipNumber();
			BillingWizardList =accountLineManager.getBillingWizardList(serviceOrder.getShipNumber(),sessionCorpID);  
			invoiceDetailList =accountLineManager.getInvoiceDetailBilling(serviceOrder.getShipNumber(),recInvNumBilling,sessionCorpID);  
			allSumAmountBilling(serviceOrder.getShipNumber(),recInvNumBilling);
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation 
	public String  allSumAmountBilling(String shipnumberBilling ,String recInvNumberBilling)
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			systemDefaultmiscVl=serviceOrderManager.findmiscVlSystemDefault(sessionCorpID).get(0).toString();
			accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString();
			List allSnmAmount=accountLineManager.getAllSumAmountBilling(shipnumberBilling,recInvNumberBilling,sessionCorpID);
			Iterator its=allSnmAmount.iterator();
			while(its.hasNext())
			{
				Object amount=its.next();
				if(((DtoBillingWizard)amount).getActualRevenueSum()!=null ) 
				{
					actRevSumBilling=(BigDecimal)((DtoBillingWizard)amount).getActualRevenueSum();
				}
				else  {
					actRevSumBilling= new BigDecimal(0);
				}
				if(((DtoBillingWizard)amount).getRevisionRevenueAmountSum()!=null ) 
				{
					revRevAmtSumBilling=(BigDecimal)((DtoBillingWizard)amount).getRevisionRevenueAmountSum();
				}
				else  {
					revRevAmtSumBilling= new BigDecimal(0);
				}	
				if(((DtoBillingWizard)amount).getEstimateRevenueAmountSum()!=null ) 
				{
					estRevAmtSumBilling=(BigDecimal)((DtoBillingWizard)amount).getEstimateRevenueAmountSum();
				}
				else  {
					estRevAmtSumBilling= new BigDecimal(0);
				}	
				if(((DtoBillingWizard)amount).getDistributionAmountSum()!=null ) 
				{
					distAmtSumBilling=(BigDecimal)((DtoBillingWizard)amount).getDistributionAmountSum();
				}
				else  {
					distAmtSumBilling= new BigDecimal(0);
				}
			}
			List idList=accountLineManager.getidListBilling(shipnumberBilling,recInvNumberBilling,sessionCorpID);
			//id =Long.parseLong(idList.get(0).toString());
			accountLine = accountLineManager.get(Long.parseLong(idList.get(0).toString()));
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	private List bilingbasis;
	private List billingcheckNew;
	@SkipValidation
	public String nonInvListWizard()
	{    
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			//getComboList(sessionCorpID);
			systemDefaultmiscVl=serviceOrderManager.findmiscVlSystemDefault(sessionCorpID).get(0).toString();
			serviceOrder = serviceOrderManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			trackingStatus=trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid); 
			customerFile = serviceOrder.getCustomerFile();
			bilingbasis = new ArrayList();
			billingcheckNew=new ArrayList();
			billingcheckNew.add("");
			billingcheckNew.add("Division");
			billingcheckNew.add("Multiply");
			Iterator mapIterator = basis.entrySet().iterator();
			while (mapIterator.hasNext()) {
				Map.Entry entry = (Map.Entry) mapIterator.next();
				String basisCode = (String) entry.getKey(); 
			    bilingbasis.add(basisCode);
			}
			
			//if(systemDefaultmiscVl.contains(serviceOrder.getJob()))
			//{
			 nonInvoiceBillingList=accountLineManager.getNonInvoiceList(serviceOrder.getShipNumber(),sessionCorpID);	
			//}
			//else {
				
			//}
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String findNonInvoiceList()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
		serviceOrder = serviceOrderManager.get(sid); 
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		 nonInvoiceList=accountLineManager.findListForInvoice(serviceOrder.getShipNumber(),sessionCorpID);
		 
		}
		catch(Exception e)
		{
			 e.printStackTrace();
		}
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String saveInvoiceBillingList()throws Exception{
	
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			List<Map> rowValues =  GridDataProcessor.getRowData(listData, listFieldNames, listFieldTypes, listFieldEditability, listIdField);
			for (Map row : rowValues) {
				Long id = (Long) row.get(listIdField);
				
				try{
					accountLine = accountLineManager.get(id); 
						for (Object key : row.keySet()) {
							Object value = row.get(key);
							
							if(key.toString().trim().equals("distributionAmount"))
								value=new BigDecimal( value.toString()); 
							if(key.toString().trim().equals("recQuantity"))
								value=new BigDecimal( value.toString());
							if(key.toString().trim().equals("actualRevenue"))
								value=new BigDecimal( value.toString());
							if(key.toString().trim().equals("revisionRevenueAmount"))
								value=new BigDecimal( value.toString());
							if(key.toString().trim().equals("estimateRevenueAmount"))
								value=new BigDecimal( value.toString());
							if(key.toString().trim().equals("recRate"))
								value=new BigDecimal( value.toString()); 
							PropertyUtils.setProperty(accountLine, key.toString(), value);
						}
						accountLineManager.save(accountLine);
				}
				catch(Exception ex)
				{
					accountLine=new AccountLine();
					for (Object key : row.keySet()) {
						Object value = row.get(key);
						if(key.toString().trim().equals("distributionAmount"))
							value=new BigDecimal( value.toString()); 
						if(key.toString().trim().equals("recQuantity"))
							value=new BigDecimal( value.toString());
						if(key.toString().trim().equals("actualRevenue"))
							value=new BigDecimal( value.toString());
						if(key.toString().trim().equals("revisionRevenueAmount"))
							value=new BigDecimal( value.toString());
						if(key.toString().trim().equals("estimateRevenueAmount"))
							value=new BigDecimal( value.toString());
						if(key.toString().trim().equals("recRate"))
							value=new BigDecimal( value.toString()); 
						PropertyUtils.setProperty(accountLine, key.toString(), value);
					}
					accountLineManager.save(accountLine);
					ex.printStackTrace();
				}
			}
			serviceOrder = serviceOrderManager.get(sid); 
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			updateAccountLine(serviceOrder);
			getRequest().getSession().setAttribute("messages", null);
			saveMessage(getText("accountLine.updated")); 
			nonInvListWizard();
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation 
	public String createReverseInvoices()
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString();
			List idList=accountLineManager.findReverseInvoiceId(shipNumber,recInvoiceNumber); 
			serviceOrder=serviceOrderManager.get(sid);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			billing =billingManager.get(sid);
			trackingStatus=trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid);
			contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
			if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
				contractType=true;	
			}
			if(trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup() && serviceOrder.getIsNetworkRecord()){
				if(trackingStatus.getAgentNetworkGroup()){
					serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(serviceOrder.getBookingAgentShipNumber()));	
				}else{
				serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(trackingStatus.getNetworkAgentExSO()));
				}
			}
			Iterator it=idList.iterator();
			String userName = getRequest().getRemoteUser();
			 while(it.hasNext()){
				  Long id=(Long)it.next(); 
				  accountLineReverse = accountLineManager.get(id);
				  maxLineNumber = serviceOrderManager.findMaximumLineNumber(shipNumber); 
			        if ( maxLineNumber.get(0) == null ) {          
			         	accountLineNumber = "001";
			         }else {
			         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
			             if((autoLineNumber.toString()).length() == 2) {
			             	accountLineNumber = "0"+(autoLineNumber.toString());
			             }
			             else if((autoLineNumber.toString()).length() == 1) {
			             	accountLineNumber = "00"+(autoLineNumber.toString());
			             } 
			             else {
			             	accountLineNumber=autoLineNumber.toString();
			             }
			         } 
			        accountLine = new AccountLine(); 
			        accountLine.setActivateAccPortal(accountLineReverse.getActivateAccPortal());
			        accountLine.setAccountLineNumber(accountLineNumber); 
			        if(accountLineReverse.getCompanyDivision()!=null)
			        {
					accountLine.setCompanyDivision(accountLineReverse.getCompanyDivision());
			        }else{
			        	accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
			        }
			        accountLine.setRecInvoiceNumber("");
			        accountLine.setAuthorization(accountLineReverse.getAuthorization());
			        BigDecimal multiplyValue=new BigDecimal(-1.0);
			        BigDecimal trueValue=new BigDecimal(0.0);
			        if(accountLineReverse.getRecRate()!=null)
			        {
			        accountLine.setRecRate(accountLineReverse.getRecRate().multiply(multiplyValue));
			        }
			        else if(accountLineReverse.getRecRate()==null)
			        {
			        accountLine.setRecRate(trueValue.multiply(multiplyValue));
			        }
			        if(accountLineReverse.getActualRevenue()!=null)
			        {
			        accountLine.setActualRevenue(accountLineReverse.getActualRevenue().multiply(multiplyValue)); 
			        }
			        else if(accountLineReverse.getActualRevenue()==null)
			        {
			         accountLine.setActualRevenue(trueValue.multiply(multiplyValue));
			        }
			        accountLine.setRecVatDescr(accountLineReverse.getRecVatDescr());
			        accountLine.setRecVatPercent(accountLineReverse.getRecVatPercent());
			        if(accountLineReverse.getRecVatAmt()!=null)
			        {
			        accountLine.setRecVatAmt(accountLineReverse.getRecVatAmt().multiply(multiplyValue)); 
			        }
			        else if(accountLineReverse.getRecVatAmt()==null)
			        {
			         accountLine.setRecVatAmt(trueValue.multiply(multiplyValue));
			        } 				
					///Code for AccountLine division 
					if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
						String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
						try{  
							String roles[] = agentRoleValue.split("~");
						  if(roles[3].toString().equals("hauler")){
							   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
								   accountLine.setDivision("01");
							   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
								   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
									   accountLine.setDivision("01");
								   }else{ 
									   String divisionTemp="";
											if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
											divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
											}
											if(!divisionTemp.equalsIgnoreCase("")){
												accountLine.setDivision(divisionTemp);
											}else{
												accountLine.setDivision(null);
											}
								     
								   }
							   }else{
								   String divisionTemp="";
										if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
										divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
										}
										if(!divisionTemp.equalsIgnoreCase("")){
											accountLine.setDivision(divisionTemp);
										}else{
											accountLine.setDivision(null);
										}
							   }
						  }else{
							  String divisionTemp="";
									if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
									}
									if(!divisionTemp.equalsIgnoreCase("")){
										accountLine.setDivision(divisionTemp);
									}else{
										accountLine.setDivision(null);
									}
						  }
						}catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					///Code for AccountLine division 
			        accountLine.setCreatedOn(new Date());
			        accountLine.setUpdatedOn(new Date());
			        accountLine.setCreatedBy(userName);
			        accountLine.setUpdatedBy(userName);
			        accountLine.setCategory(accountLineReverse.getCategory());
			        accountLine.setStatus(true); 
			        accountLine.setBasis(accountLineReverse.getBasis());
			        accountLine.setEstimateQuantity(accountLineReverse.getEstimateQuantity());
			        if(accountLineReverse.getEstimateSellRate()!=null)
			        {
			        accountLine.setEstimateSellRate(accountLineReverse.getEstimateSellRate().multiply(multiplyValue));
			        }
			        if(accountLineReverse.getEstimateRevenueAmount()!=null)
			        {
			        accountLine.setEstimateRevenueAmount(accountLineReverse.getEstimateRevenueAmount().multiply(multiplyValue));
			        }
			        accountLine.setRevisionQuantity(accountLineReverse.getRevisionQuantity());
			        if(accountLineReverse.getRevisionSellRate()!=null)
			        {
			        accountLine.setRevisionSellRate(accountLineReverse.getRevisionSellRate().multiply(multiplyValue));
			        }
			        if(accountLineReverse.getRevisionRevenueAmount()!=null)
			        {
			        accountLine.setRevisionRevenueAmount(accountLineReverse.getRevisionRevenueAmount().multiply(multiplyValue));
			        }
			        accountLine.setBillToCode(accountLineReverse.getBillToCode()); 
			        accountLine.setBillToName(accountLineReverse.getBillToName());
			        accountLine.setNetworkBillToCode(accountLineReverse.getNetworkBillToCode());
		    		accountLine.setNetworkBillToName(accountLineReverse.getNetworkBillToName());
			        accountLine.setRecQuantity(accountLineReverse.getRecQuantity());
			        accountLine.setItemNew(accountLineReverse.getItemNew());
			        accountLine.setCorpID(sessionCorpID); 
			        accountLine.setCheckNew(accountLineReverse.getCheckNew());
			        accountLine.setBasisNew(accountLineReverse.getBasisNew());
			        accountLine.setBasisNewType(accountLineReverse.getBasisNewType());
			        accountLine.setDescription(accountLineReverse.getDescription()); 
			        accountLine.setStorageDateRangeFrom(accountLineReverse.getStorageDateRangeFrom());
			        accountLine.setStorageDateRangeTo(accountLineReverse.getStorageDateRangeTo());
			        accountLine.setStorageDays(accountLineReverse.getStorageDays());  
			        accountLine.setChargeCode(accountLineReverse.getChargeCode());
			        accountLine.setIgnoreForBilling(accountLineReverse.isIgnoreForBilling());
			        accountLine.setPaymentStatus(accountLineReverse.getPaymentStatus());
					try {
						String ss=chargesManager.findPopulateCostElementData(accountLineReverse.getChargeCode(), accountLineReverse.getContract(),sessionCorpID);
						if(!ss.equalsIgnoreCase("")){
							accountLine.setAccountLineCostElement(ss.split("~")[0]);
							accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
						}
						if(ss==null || ss.equals("")){
							accountLine.setAccountLineCostElement(accountLineReverse.getAccountLineCostElement());
							accountLine.setAccountLineScostElementDescription(accountLineReverse.getAccountLineScostElementDescription());
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
			        
			        accountLine.setVATExclude(accountLineReverse.getVATExclude());
			        accountLine.setContract(accountLineReverse.getContract());
			        accountLine.setRecGl(accountLineReverse.getRecGl());
			        accountLine.setPayGl(accountLineReverse.getPayGl());
			        accountLine.setQstRecVatGl(accountLineReverse.getQstRecVatGl());
			        
			        if(accountLineReverse.getQstRecVatAmt()!=null)
			        {
			        accountLine.setQstRecVatAmt(accountLineReverse.getQstRecVatAmt().multiply(multiplyValue));
			        }
			        else if(accountLineReverse.getQstRecVatAmt()==null)
			        {
			         accountLine.setQstRecVatAmt(trueValue.multiply(multiplyValue));	
			        }
			        
			        if(accountLineReverse.getDistributionAmount()!=null)
			        {
			        accountLine.setDistributionAmount(accountLineReverse.getDistributionAmount().multiply(multiplyValue));
			        }
			        else if(accountLineReverse.getDistributionAmount()==null)
			        {
			         accountLine.setDistributionAmount(trueValue.multiply(multiplyValue));	
			        }
			        if(systemDefault!=null && systemDefault.getVatCalculation())
			         {
			        if(accountLineReverse.getEstVatAmt()!=null){
			            accountLine.setEstVatAmt(accountLineReverse.getEstVatAmt().multiply(multiplyValue));
			            }
			            else if(accountLineReverse.getEstVatAmt()==null){
			            accountLine.setEstVatAmt(trueValue.multiply(multiplyValue));
			            }
			        if(accountLineReverse.getRevisionVatAmt()!=null){
			            accountLine.setRevisionVatAmt(accountLineReverse.getRevisionVatAmt().multiply(multiplyValue));
			            }
			            else if(accountLineReverse.getRevisionVatAmt()==null){
			            accountLine.setRevisionVatAmt(trueValue.multiply(multiplyValue));
			            }


			        
			         }	            
			        if(multiCurrency.trim().equalsIgnoreCase("Y")){
			        	accountLine.setRecRateCurrency(accountLineReverse.getRecRateCurrency());
			        	accountLine.setRacValueDate(accountLineReverse.getRacValueDate());
			        	if(accountLineReverse.getRecCurrencyRate()!=null){
			        		accountLine.setRecCurrencyRate(accountLineReverse.getRecCurrencyRate().multiply(multiplyValue));
			        	}
			        	else if(accountLineReverse.getRecCurrencyRate()==null){
			        		accountLine.setRecCurrencyRate(trueValue.multiply(multiplyValue));
			        	}
			        	accountLine.setRecRateExchange(accountLineReverse.getRecRateExchange());
			        	if(accountLineReverse.getActualRevenueForeign()!=null){
			        		accountLine.setActualRevenueForeign(accountLineReverse.getActualRevenueForeign().multiply(multiplyValue));
			        	}
			        	else if(accountLineReverse.getActualRevenueForeign()==null){
			        		accountLine.setActualRevenueForeign(trueValue.multiply(multiplyValue));
			        	}
			        	accountLine.setEstSellCurrency(accountLineReverse.getEstSellCurrency());
			        	accountLine.setEstSellValueDate(accountLineReverse.getEstSellValueDate());
			        	accountLine.setEstSellExchangeRate(accountLineReverse.getEstSellExchangeRate());
			        	if(accountLineReverse.getEstSellLocalRate()!=null){
			        		accountLine.setEstSellLocalRate(accountLineReverse.getEstSellLocalRate().multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getEstSellLocalRate()==null){
			        		accountLine.setEstSellLocalRate(trueValue.multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getEstSellLocalAmount()!=null){
			        		accountLine.setEstSellLocalAmount(accountLineReverse.getEstSellLocalAmount().multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getEstSellLocalAmount()==null){
			        		accountLine.setEstSellLocalAmount(trueValue.multiply(multiplyValue));
			        	}
			        	accountLine.setRevisionSellCurrency(accountLineReverse.getRevisionSellCurrency());
			        	accountLine.setRevisionSellValueDate(accountLineReverse.getRevisionSellValueDate());
			        	accountLine.setRevisionSellExchangeRate(accountLineReverse.getRevisionSellExchangeRate());
			        	if(accountLineReverse.getRevisionSellLocalRate()!=null){
			        		accountLine.setRevisionSellLocalRate(accountLineReverse.getRevisionSellLocalRate().multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getRevisionSellLocalRate()==null){
			        		accountLine.setRevisionSellLocalRate(trueValue.multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getRevisionSellLocalAmount()!=null){
			        		accountLine.setRevisionSellLocalAmount(accountLineReverse.getRevisionSellLocalAmount().multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getRevisionSellLocalAmount()==null){
			        		accountLine.setRevisionSellLocalAmount(trueValue.multiply(multiplyValue));
			        	}
			        }
			        if(contractType){
			        	accountLine.setEstimateSellQuantity(accountLineReverse.getEstimateSellQuantity());
			        	accountLine.setRevisionSellQuantity(accountLineReverse.getRevisionSellQuantity());
			        	accountLine.setContractCurrency(accountLineReverse.getContractCurrency());
			        	accountLine.setContractValueDate(accountLineReverse.getContractValueDate());
			        	accountLine.setContractExchangeRate(accountLineReverse.getContractExchangeRate());
			        	if(accountLineReverse.getContractRate()!=null){
			        		accountLine.setContractRate(accountLineReverse.getContractRate().multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getContractRate()==null){
			        		accountLine.setContractRate(trueValue.multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getContractRateAmmount()!=null){
			        		accountLine.setContractRateAmmount(accountLineReverse.getContractRateAmmount().multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getContractRateAmmount()==null){
			        		accountLine.setContractRateAmmount(trueValue.multiply(multiplyValue));
			        	}
			        	accountLine.setEstimateContractCurrency(accountLineReverse.getEstimateContractCurrency());
			        	accountLine.setEstimateContractValueDate(accountLineReverse.getEstimateContractValueDate());
			        	accountLine.setEstimateContractExchangeRate(accountLineReverse.getEstimateContractExchangeRate());
			        	if(accountLineReverse.getEstimateContractRate()!=null){
			        		accountLine.setEstimateContractRate(accountLineReverse.getEstimateContractRate().multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getEstimateContractRate()==null){
			        		accountLine.setEstimateContractRate(trueValue.multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getEstimateContractRateAmmount()!=null){
			        		accountLine.setEstimateContractRateAmmount(accountLineReverse.getEstimateContractRateAmmount().multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getEstimateContractRateAmmount()==null){
			        		accountLine.setEstimateContractRateAmmount(trueValue.multiply(multiplyValue));
			        	}
			        	accountLine.setRevisionContractCurrency(accountLineReverse.getRevisionContractCurrency());
			        	accountLine.setRevisionContractValueDate(accountLineReverse.getRevisionContractValueDate());
			        	accountLine.setRevisionContractExchangeRate(accountLineReverse.getRevisionContractExchangeRate());
			        	if(accountLineReverse.getRevisionContractRate()!=null){
			        		accountLine.setRevisionContractRate(accountLineReverse.getRevisionContractRate().multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getRevisionContractRate()==null){
			        		accountLine.setRevisionContractRate(trueValue.multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getRevisionContractRateAmmount()!=null){
			        		accountLine.setRevisionContractRateAmmount(accountLineReverse.getRevisionContractRateAmmount().multiply(multiplyValue));
			        	}
			        	if(accountLineReverse.getRevisionContractRateAmmount()==null){
			        		accountLine.setRevisionContractRateAmmount(trueValue.multiply(multiplyValue));
			        	}
			        	accountLine.setPayableContractCurrency(accountLineReverse.getPayableContractCurrency());
			        	accountLine.setPayableContractValueDate(accountLineReverse.getPayableContractValueDate());
			        	accountLine.setPayableContractExchangeRate(accountLineReverse.getPayableContractExchangeRate());
			        	accountLine.setEstimatePayableContractCurrency(accountLineReverse.getEstimatePayableContractCurrency());
			        	accountLine.setEstimatePayableContractValueDate(accountLineReverse.getEstimatePayableContractValueDate());
			        	accountLine.setEstimatePayableContractExchangeRate(accountLineReverse.getEstimatePayableContractExchangeRate());
			        	accountLine.setRevisionPayableContractCurrency(accountLineReverse.getRevisionPayableContractCurrency());
			        	accountLine.setRevisionPayableContractValueDate(accountLineReverse.getRevisionPayableContractValueDate());
			        	accountLine.setRevisionPayableContractExchangeRate(accountLineReverse.getRevisionPayableContractExchangeRate());
			        	baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
			    		baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(accountLine.getCompanyDivision(),sessionCorpID);
			        	if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
			    		{
			    		accountLine.setEstCurrency(baseCurrency);
						accountLine.setRevisionCurrency(baseCurrency);	
						accountLine.setCountry(baseCurrency);
						accountLine.setEstValueDate(new Date());
						accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setRevisionValueDate(new Date());
						accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setValueDate(new Date());
						accountLine.setExchangeRate(new Double(1.0));
			    		}
			    		else
			    		{
			    		accountLine.setEstCurrency(baseCurrencyCompanyDivision);
						accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
						accountLine.setCountry(baseCurrencyCompanyDivision);
						accountLine.setEstValueDate(new Date());
						accountLine.setRevisionValueDate(new Date());
						accountLine.setValueDate(new Date());
						//accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision); 
						accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
						if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
							try{
							accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
							accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
							accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
							}catch(Exception e){
								accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
			    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
			    				accountLine.setExchangeRate(new Double(1.0));
			    				e.printStackTrace();
							}
						}else{
							accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
							accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
							accountLine.setExchangeRate(new Double(1.0));
						}
			    		}
			        	accountLine.setVendorCode(accountLineReverse.getVendorCode());
			          	accountLine.setEstimateVendorName(accountLineReverse.getEstimateVendorName());
			          	accountLine.setActgCode(accountLineReverse.getActgCode());
			            accountLine.setPayGl(accountLineReverse.getPayGl());
			            accountLine.setPayVatDescr(accountLineReverse.getPayVatDescr());
			            accountLine.setPayVatPercent(accountLineReverse.getPayVatPercent());
			        	
			        	
			        }
			        
			        
			        
			        
			        
			        accountLine.setServiceOrder(serviceOrder);
					accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
					accountLine.setShipNumber(serviceOrder.getShipNumber());
					accountLine.setServiceOrderId(serviceOrder.getId());
					if((rollUpInvoiceFlag!=null && !rollUpInvoiceFlag.equalsIgnoreCase("")) && rollUpInvoiceFlag.equalsIgnoreCase("True")){
						accountLine.setRollUpInInvoice(accountLineReverse.getRollUpInInvoice());
					}			       					
					accountLine=accountLineManager.save(accountLine);
			        if(trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup() && serviceOrder.getIsNetworkRecord()){
			        //serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(serviceOrder.getShipNumber()));
			        String networkSynchedId=	accountLineManager.getNetworkSynchedId(accountLineReverse.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
			    	if(!(networkSynchedId.equals(""))){
			    		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
			    		createCMMDMMReverseInvoices(serviceOrderToRecods, synchedAccountLine,accountLine);
			    	}
			        } 
			        
			 }
			 if(trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup() && serviceOrder.getIsNetworkRecord()){
				 updateExternalAccountLine(serviceOrderToRecods,serviceOrderToRecods.getCorpID());
			 }
			 updateAccountLine(serviceOrder);
			 estimateAccountList(); 
			 int i=accountLineManager.updateReverseAuthNo(shipNumber,recInvoiceNumber); 
			findReverseInvoiceList();
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String rollUpInvoiceFlag;
	@SkipValidation
	public String createCopyInvoices(){

		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString();
			List idList=accountLineManager.findReverseInvoiceId(shipNumber,recInvoiceNumber); 
			serviceOrder=serviceOrderManager.get(sid);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			billing =billingManager.get(sid);
			trackingStatus=trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid);
			contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
			if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
				contractType=true;	
			}
			if(trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup() && serviceOrder.getIsNetworkRecord()){
				if(trackingStatus.getAgentNetworkGroup()){
					serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(serviceOrder.getBookingAgentShipNumber()));	
				}else{
				serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(trackingStatus.getNetworkAgentExSO()));
				}
			}
			Iterator it=idList.iterator();
			String userName = getRequest().getRemoteUser();
			 while(it.hasNext()){
				  Long id=(Long)it.next(); 
				  accountLineReverse = accountLineManager.get(id);
				  maxLineNumber = serviceOrderManager.findMaximumLineNumber(shipNumber); 
			        if ( maxLineNumber.get(0) == null ) {          
			         	accountLineNumber = "001";
			         }else {
			         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
			             if((autoLineNumber.toString()).length() == 2) {
			             	accountLineNumber = "0"+(autoLineNumber.toString());
			             }
			             else if((autoLineNumber.toString()).length() == 1) {
			             	accountLineNumber = "00"+(autoLineNumber.toString());
			             } 
			             else {
			             	accountLineNumber=autoLineNumber.toString();
			             }
			         } 
			        accountLine = new AccountLine(); 
			        accountLine.setActivateAccPortal(accountLineReverse.getActivateAccPortal());
					 
			        accountLine.setAccountLineNumber(accountLineNumber); 
			        if(accountLineReverse.getCompanyDivision()!=null)
			        {
					accountLine.setCompanyDivision(accountLineReverse.getCompanyDivision());
			        }else{
			        	accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
			        }
			        accountLine.setRecInvoiceNumber("");
			        accountLine.setAuthorization(accountLineReverse.getAuthorization());
			        accountLine.setRecRate(accountLineReverse.getRecRate());
			        accountLine.setActualRevenue(accountLineReverse.getActualRevenue()); 
			        accountLine.setRecVatDescr(accountLineReverse.getRecVatDescr());
			        accountLine.setRecVatPercent(accountLineReverse.getRecVatPercent());
			        accountLine.setRecVatAmt(accountLineReverse.getRecVatAmt()); 
			        accountLine.setQstRecVatAmt(accountLineReverse.getQstRecVatAmt());
			        accountLine.setCreatedOn(new Date());
			        accountLine.setUpdatedOn(new Date());
			        accountLine.setCreatedBy(userName);
			        accountLine.setUpdatedBy(userName);
			        accountLine.setCategory(accountLineReverse.getCategory());
			        accountLine.setStatus(true); 
			        accountLine.setBasis(accountLineReverse.getBasis());
			        accountLine.setEstimateQuantity(accountLineReverse.getEstimateQuantity());
			        accountLine.setEstimateSellRate(accountLineReverse.getEstimateSellRate());
			        accountLine.setEstimateRevenueAmount(accountLineReverse.getEstimateRevenueAmount());
			        accountLine.setRevisionQuantity(accountLineReverse.getRevisionQuantity());
			        accountLine.setRevisionSellRate(accountLineReverse.getRevisionSellRate());
			        accountLine.setRevisionRevenueAmount(accountLineReverse.getRevisionRevenueAmount());
			        accountLine.setBillToCode(accountLineReverse.getBillToCode()); 
			        accountLine.setBillToName(accountLineReverse.getBillToName());
			        accountLine.setNetworkBillToCode(accountLineReverse.getNetworkBillToCode());
		    		accountLine.setNetworkBillToName(accountLineReverse.getNetworkBillToName());
			        accountLine.setRecQuantity(accountLineReverse.getRecQuantity());
			        accountLine.setItemNew(accountLineReverse.getItemNew());
			        accountLine.setCorpID(sessionCorpID); 
			        accountLine.setCheckNew(accountLineReverse.getCheckNew());
			        accountLine.setBasisNew(accountLineReverse.getBasisNew());
			        accountLine.setBasisNewType(accountLineReverse.getBasisNewType());
			        accountLine.setDescription(accountLineReverse.getDescription());
			        accountLine.setQstRecVatGl(accountLineReverse.getQstRecVatGl());
			        accountLine.setPaymentStatus(accountLineReverse.getPaymentStatus());


					///Code for AccountLine division 
					if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
						String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
						try{  
							String roles[] = agentRoleValue.split("~");
						  if(roles[3].toString().equals("hauler")){
							   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
								   accountLine.setDivision("01");
							   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
								   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
									   accountLine.setDivision("01");
								   }else{ 
									   String divisionTemp="";
											if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
											divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
											}
											if(!divisionTemp.equalsIgnoreCase("")){
												accountLine.setDivision(divisionTemp);
											}else{
												accountLine.setDivision(null);
											}
								     
								   }
							   }else{
								   String divisionTemp="";
										if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
										divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
										}
										if(!divisionTemp.equalsIgnoreCase("")){
											accountLine.setDivision(divisionTemp);
										}else{
											accountLine.setDivision(null);
										}
							   }
						  }else{
							  String divisionTemp="";
									if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
									}
									if(!divisionTemp.equalsIgnoreCase("")){
										accountLine.setDivision(divisionTemp);
									}else{
										accountLine.setDivision(null);
									}
						  }
						}catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					///Code for AccountLine division 
			        accountLine.setStorageDateRangeFrom(accountLineReverse.getStorageDateRangeFrom());
			        accountLine.setStorageDateRangeTo(accountLineReverse.getStorageDateRangeTo());
			        accountLine.setStorageDays(accountLineReverse.getStorageDays());  
			        accountLine.setChargeCode(accountLineReverse.getChargeCode());
			        accountLine.setIgnoreForBilling(accountLineReverse.isIgnoreForBilling());
					try {
						String ss=chargesManager.findPopulateCostElementData(accountLineReverse.getChargeCode(), accountLineReverse.getContract(),sessionCorpID);
						if(!ss.equalsIgnoreCase("")){
							accountLine.setAccountLineCostElement(ss.split("~")[0]);
							accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
						}
						if(ss==null || ss.equalsIgnoreCase("")){
							accountLine.setAccountLineCostElement(accountLineReverse.getAccountLineCostElement());
							accountLine.setAccountLineScostElementDescription(accountLineReverse.getAccountLineScostElementDescription());
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
			        
			        accountLine.setContract(accountLineReverse.getContract());
			        accountLine.setVATExclude(accountLineReverse.getVATExclude());
			        accountLine.setRecGl(accountLineReverse.getRecGl());
			        accountLine.setPayGl(accountLineReverse.getPayGl());
			        accountLine.setDistributionAmount(accountLineReverse.getDistributionAmount());
			        if(systemDefault!=null && systemDefault.getVatCalculation())
			         {

			           accountLine.setEstVatAmt(accountLineReverse.getEstVatAmt());
			    	   accountLine.setRevisionVatAmt(accountLineReverse.getRevisionVatAmt());
			         }	            
			        
			        if(multiCurrency.trim().equalsIgnoreCase("Y")){
			        	accountLine.setRecRateCurrency(accountLineReverse.getRecRateCurrency());
			        	accountLine.setRacValueDate(accountLineReverse.getRacValueDate());
			        	accountLine.setRecCurrencyRate(accountLineReverse.getRecCurrencyRate());
			        	accountLine.setRecRateExchange(accountLineReverse.getRecRateExchange());
			        	accountLine.setActualRevenueForeign(accountLineReverse.getActualRevenueForeign());
			        	accountLine.setEstSellCurrency(accountLineReverse.getEstSellCurrency());
			        	accountLine.setEstSellValueDate(accountLineReverse.getEstSellValueDate());
			        	accountLine.setEstSellExchangeRate(accountLineReverse.getEstSellExchangeRate());
			        	accountLine.setEstSellLocalRate(accountLineReverse.getEstSellLocalRate());
			        	accountLine.setEstSellLocalAmount(accountLineReverse.getEstSellLocalAmount());
			        	accountLine.setRevisionSellCurrency(accountLineReverse.getRevisionSellCurrency());
			        	accountLine.setRevisionSellValueDate(accountLineReverse.getRevisionSellValueDate());
			        	accountLine.setRevisionSellExchangeRate(accountLineReverse.getRevisionSellExchangeRate());
			        	accountLine.setRevisionSellLocalRate(accountLineReverse.getRevisionSellLocalRate());
			        	accountLine.setRevisionSellLocalAmount(accountLineReverse.getRevisionSellLocalAmount());
			        }
			        baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
					baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(accountLine.getCompanyDivision(),sessionCorpID);
			    	if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
					{
					accountLine.setEstCurrency(baseCurrency);
					accountLine.setRevisionCurrency(baseCurrency);	
					accountLine.setCountry(baseCurrency);
					accountLine.setEstValueDate(new Date());
					accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
					accountLine.setRevisionValueDate(new Date());
					accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
					accountLine.setValueDate(new Date());
					accountLine.setExchangeRate(new Double(1.0));
					}
					else
					{
					accountLine.setEstCurrency(baseCurrencyCompanyDivision);
					accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
					accountLine.setCountry(baseCurrencyCompanyDivision);
					accountLine.setEstValueDate(new Date());
					accountLine.setRevisionValueDate(new Date());
					accountLine.setValueDate(new Date());
					accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
					if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
						try{
						accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
						accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
						accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
						}catch(Exception e){
							accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
							accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
							accountLine.setExchangeRate(new Double(1.0));
							e.printStackTrace();
						}
					}else{
						accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
						accountLine.setExchangeRate(new Double(1.0));
					}
					}
			        if(contractType){
			        	accountLine.setEstimateSellQuantity(accountLineReverse.getEstimateSellQuantity());
			        	accountLine.setRevisionSellQuantity(accountLineReverse.getRevisionSellQuantity());
			        	accountLine.setContractCurrency(accountLineReverse.getContractCurrency());
			        	accountLine.setContractValueDate(accountLineReverse.getContractValueDate());
			        	accountLine.setContractExchangeRate(accountLineReverse.getContractExchangeRate());
			        	accountLine.setContractRate(accountLineReverse.getContractRate());
			        	accountLine.setContractRateAmmount(accountLineReverse.getContractRateAmmount());
			        	accountLine.setEstimateContractCurrency(accountLineReverse.getEstimateContractCurrency());
			        	accountLine.setEstimateContractValueDate(accountLineReverse.getEstimateContractValueDate());
			        	accountLine.setEstimateContractExchangeRate(accountLineReverse.getEstimateContractExchangeRate());
			        	accountLine.setEstimateContractRate(accountLineReverse.getEstimateContractRate());
			        	accountLine.setEstimateContractRateAmmount(accountLineReverse.getEstimateContractRateAmmount());
			        	accountLine.setRevisionContractCurrency(accountLineReverse.getRevisionContractCurrency());
			        	accountLine.setRevisionContractValueDate(accountLineReverse.getRevisionContractValueDate());
			        	accountLine.setRevisionContractExchangeRate(accountLineReverse.getRevisionContractExchangeRate());
			        	accountLine.setRevisionContractRate(accountLineReverse.getRevisionContractRate());
			        	accountLine.setRevisionContractRateAmmount(accountLineReverse.getRevisionContractRateAmmount());
			        	accountLine.setPayableContractCurrency(accountLineReverse.getPayableContractCurrency());
			        	accountLine.setPayableContractValueDate(accountLineReverse.getPayableContractValueDate());
			        	accountLine.setPayableContractExchangeRate(accountLineReverse.getPayableContractExchangeRate());
			        	accountLine.setEstimatePayableContractCurrency(accountLineReverse.getEstimatePayableContractCurrency());
			        	accountLine.setEstimatePayableContractValueDate(accountLineReverse.getEstimatePayableContractValueDate());
			        	accountLine.setEstimatePayableContractExchangeRate(accountLineReverse.getEstimatePayableContractExchangeRate());
			        	accountLine.setRevisionPayableContractCurrency(accountLineReverse.getRevisionPayableContractCurrency());
			        	accountLine.setRevisionPayableContractValueDate(accountLineReverse.getRevisionPayableContractValueDate());
			        	accountLine.setRevisionPayableContractExchangeRate(accountLineReverse.getRevisionPayableContractExchangeRate());
			        	baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
			    		baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(accountLine.getCompanyDivision(),sessionCorpID);
			        	if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
			    		{
			    		accountLine.setEstCurrency(baseCurrency);
						accountLine.setRevisionCurrency(baseCurrency);	
						accountLine.setCountry(baseCurrency);
						accountLine.setEstValueDate(new Date());
						accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setRevisionValueDate(new Date());
						accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setValueDate(new Date());
						accountLine.setExchangeRate(new Double(1.0));
			    		}
			    		else
			    		{
			    		accountLine.setEstCurrency(baseCurrencyCompanyDivision);
						accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
						accountLine.setCountry(baseCurrencyCompanyDivision);
						accountLine.setEstValueDate(new Date());
						accountLine.setRevisionValueDate(new Date());
						accountLine.setValueDate(new Date());
						accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
						if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
							try{
							accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
							accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
							accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
							}catch(Exception e){
								accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
			    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
			    				accountLine.setExchangeRate(new Double(1.0));	
			    				e.printStackTrace();
							}
						}else{
							accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
							accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
							accountLine.setExchangeRate(new Double(1.0));
						}
			    		}
			        	accountLine.setVendorCode(accountLineReverse.getVendorCode());
			        	accountLine.setEstimateVendorName(accountLineReverse.getEstimateVendorName());
			          	accountLine.setActgCode(accountLineReverse.getActgCode());
			            accountLine.setPayGl(accountLineReverse.getPayGl());
			            accountLine.setQstPayVatGl(accountLineReverse.getQstPayVatGl());
			            accountLine.setPayVatDescr(accountLineReverse.getPayVatDescr());
			            accountLine.setPayVatPercent(accountLineReverse.getPayVatPercent());	
			        } 
			        accountLine.setServiceOrder(serviceOrder);
					accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
					accountLine.setShipNumber(serviceOrder.getShipNumber());
					accountLine.setServiceOrderId(serviceOrder.getId());
					if((rollUpInvoiceFlag!=null && !rollUpInvoiceFlag.equalsIgnoreCase("")) && rollUpInvoiceFlag.equalsIgnoreCase("True")){
						accountLine.setRollUpInInvoice(accountLineReverse.getRollUpInInvoice());
					}
			        					
					accountLine=accountLineManager.save(accountLine);
			 }
			 estimateAccountList(); 
			 int i=accountLineManager.updateReverseAuthNo(shipNumber,recInvoiceNumber); 
			findCopyInvoiceList();
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
			
	}
	@SkipValidation
    public void createCMMDMMReverseInvoices(ServiceOrder serviceOrderToRecods, AccountLine accountLineReverse,AccountLine agentAccountLine){
		try {
			maxLineNumber = serviceOrderManager.findExternalMaximumLineNumber(serviceOrderToRecods.getShipNumber(),serviceOrderToRecods.getCorpID()); 
			if ( maxLineNumber.get(0) == null ) {          
			 	accountLineNumber = "001";
			 }else {
			 	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
			     if((autoLineNumber.toString()).length() == 2) {
			     	accountLineNumber = "0"+(autoLineNumber.toString());
			     }
			     else if((autoLineNumber.toString()).length() == 1) {
			     	accountLineNumber = "00"+(autoLineNumber.toString());
			     } 
			     else {
			     	accountLineNumber=autoLineNumber.toString();
			     }
			 }
			  accountLine = new AccountLine(); 
			  accountLine.setActivateAccPortal(accountLineReverse.getActivateAccPortal());
			  accountLine.setAccountLineNumber(accountLineNumber); 
			  accountLine.setNetworkSynchedId(agentAccountLine.getId());
			  accountLine.setRecInvoiceNumber("");
			  accountLine.setAuthorization(accountLineReverse.getAuthorization());
			  BigDecimal multiplyValue=new BigDecimal(-1.0);
			  BigDecimal trueValue=new BigDecimal(0.0);
			  if(accountLineReverse.getRecRate()!=null)
			  {
			  accountLine.setRecRate(accountLineReverse.getRecRate().multiply(multiplyValue));
			  }
			  else if(accountLineReverse.getRecRate()==null)
			  {
			  accountLine.setRecRate(trueValue.multiply(multiplyValue));
			  }
			  if(accountLineReverse.getActualRevenue()!=null)
			  {
			  accountLine.setActualRevenue(accountLineReverse.getActualRevenue().multiply(multiplyValue)); 
			  }
			  else if(accountLineReverse.getActualRevenue()==null)
			  {
			   accountLine.setActualRevenue(trueValue.multiply(multiplyValue));
			  }
			  accountLine.setRecVatDescr(accountLineReverse.getRecVatDescr());
			  accountLine.setRecVatPercent(accountLineReverse.getRecVatPercent());
			  if(accountLineReverse.getRecVatAmt()!=null)
			  {
			  accountLine.setRecVatAmt(accountLineReverse.getRecVatAmt().multiply(multiplyValue)); 
			  }
			  else if(accountLineReverse.getRecVatAmt()==null)
			  {
			   accountLine.setRecVatAmt(trueValue.multiply(multiplyValue));
			  }
			  
			  if(accountLineReverse.getEstVatAmt()!=null){
		            accountLine.setEstVatAmt(accountLineReverse.getEstVatAmt().multiply(multiplyValue));
		       }
		      else if(accountLineReverse.getEstVatAmt()==null){
		            accountLine.setEstVatAmt(trueValue.multiply(multiplyValue));
		      }
			  if(accountLineReverse.getRevisionVatAmt()!=null){
		            accountLine.setRevisionVatAmt(accountLineReverse.getRevisionVatAmt().multiply(multiplyValue));
		            }
		      else if(accountLineReverse.getRevisionVatAmt()==null){
		            accountLine.setRevisionVatAmt(trueValue.multiply(multiplyValue));
		      }
			  accountLine.setCreatedOn(new Date());
			  if(accountLineReverse.getCompanyDivision()!=null)
			  {
				accountLine.setCompanyDivision(accountLineReverse.getCompanyDivision());
			  }else{
				  accountLine.setCompanyDivision(serviceOrderToRecods.getCompanyDivision());
			  }
			  accountLine.setUpdatedOn(new Date());
			  accountLine.setCreatedBy("Networking");
			  accountLine.setUpdatedBy("Networking");
			  accountLine.setCategory(accountLineReverse.getCategory());
			  accountLine.setStatus(true); 
			  accountLine.setBasis(accountLineReverse.getBasis());
			  accountLine.setEstimateQuantity(accountLineReverse.getEstimateQuantity());
			  accountLine.setEstimateSellQuantity(accountLineReverse.getEstimateSellQuantity());
			  if(accountLineReverse.getEstimateSellRate()!=null)
			  {
			  accountLine.setEstimateSellRate(accountLineReverse.getEstimateSellRate().multiply(multiplyValue));
			  }
			  if(accountLineReverse.getEstimateRevenueAmount()!=null)
			  {
			  accountLine.setEstimateRevenueAmount(accountLineReverse.getEstimateRevenueAmount().multiply(multiplyValue));
			  }
			  accountLine.setRevisionQuantity(accountLineReverse.getRevisionQuantity());
			  accountLine.setRevisionSellQuantity(accountLineReverse.getRevisionSellQuantity());
			  if(accountLineReverse.getRevisionSellRate()!=null)
			  {
			  accountLine.setRevisionSellRate(accountLineReverse.getRevisionSellRate().multiply(multiplyValue));
			  }
			  if(accountLineReverse.getRevisionRevenueAmount()!=null)
			  {
			  accountLine.setRevisionRevenueAmount(accountLineReverse.getRevisionRevenueAmount().multiply(multiplyValue));
			  }
			  accountLine.setBillToCode(accountLineReverse.getBillToCode()); 
			  accountLine.setBillToName(accountLineReverse.getBillToName());
			  accountLine.setNetworkBillToCode(accountLineReverse.getNetworkBillToCode());
	    	  accountLine.setNetworkBillToName(accountLineReverse.getNetworkBillToName());
			  //accountLine.setReceivedInvoiceDate(accountLineReverse.getReceivedInvoiceDate());
			  accountLine.setRecQuantity(accountLineReverse.getRecQuantity());
			  accountLine.setItemNew(accountLineReverse.getItemNew());
			  accountLine.setCorpID(accountLineReverse.getCorpID()); 
			  accountLine.setCheckNew(accountLineReverse.getCheckNew());
			  accountLine.setBasisNew(accountLineReverse.getBasisNew());
			  accountLine.setBasisNewType(accountLineReverse.getBasisNewType());
			  accountLine.setDescription(accountLineReverse.getDescription()); 
			  accountLine.setExternalReference(accountLineReverse.getExternalReference());
			  accountLine.setPaymentStatus(accountLineReverse.getPaymentStatus());
			  accountLine.setStatusDate(accountLineReverse.getStatusDate());
			  accountLine.setStorageDateRangeFrom(accountLineReverse.getStorageDateRangeFrom());
			  accountLine.setStorageDateRangeTo(accountLineReverse.getStorageDateRangeTo());
			  accountLine.setStorageDays(accountLineReverse.getStorageDays());  
			  accountLine.setChargeCode(accountLineReverse.getChargeCode());
			  accountLine.setIgnoreForBilling(accountLineReverse.isIgnoreForBilling());
				try {
					String ss=chargesManager.findPopulateCostElementData(accountLineReverse.getChargeCode(), accountLineReverse.getContract(),sessionCorpID);
					if(!ss.equalsIgnoreCase("")){
						accountLine.setAccountLineCostElement(ss.split("~")[0]);
						accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
					}
					if(ss==null || ss.equals("")){
						accountLine.setAccountLineCostElement(accountLineReverse.getAccountLineCostElement());
						accountLine.setAccountLineScostElementDescription(accountLineReverse.getAccountLineScostElementDescription());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			  
			  accountLine.setContract(accountLineReverse.getContract());
			  accountLine.setRecGl(accountLineReverse.getRecGl());
			  accountLine.setVATExclude(accountLineReverse.getVATExclude());
			  if(accountLineReverse.getDistributionAmount()!=null)
			  {
			  accountLine.setDistributionAmount(accountLineReverse.getDistributionAmount().multiply(multiplyValue));
			  }
			  else if(accountLineReverse.getDistributionAmount()==null)
			  {
			   accountLine.setDistributionAmount(trueValue.multiply(multiplyValue));	
			  }
			  
			  	accountLine.setRecRateCurrency(accountLineReverse.getRecRateCurrency());
			  	accountLine.setRacValueDate(accountLineReverse.getRacValueDate());
			  	if(accountLineReverse.getRecCurrencyRate()!=null){
			  		accountLine.setRecCurrencyRate(accountLineReverse.getRecCurrencyRate().multiply(multiplyValue));
			  	}
			  	else if(accountLineReverse.getRecCurrencyRate()==null){
			  		accountLine.setRecCurrencyRate(trueValue.multiply(multiplyValue));
			  	}
			  	accountLine.setRecRateExchange(accountLineReverse.getRecRateExchange());
			  	if(accountLineReverse.getActualRevenueForeign()!=null){
			  		accountLine.setActualRevenueForeign(accountLineReverse.getActualRevenueForeign().multiply(multiplyValue));
			  	}
			  	else if(accountLineReverse.getActualRevenueForeign()==null){
			  		accountLine.setActualRevenueForeign(trueValue.multiply(multiplyValue));
			  	}
			  	accountLine.setEstSellCurrency(accountLineReverse.getEstSellCurrency());
			  	accountLine.setEstSellValueDate(accountLineReverse.getEstSellValueDate());
			  	accountLine.setEstSellExchangeRate(accountLineReverse.getEstSellExchangeRate());
			  	if(accountLineReverse.getEstSellLocalRate()!=null){
			  		accountLine.setEstSellLocalRate(accountLineReverse.getEstSellLocalRate().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstSellLocalRate()==null){
			  		accountLine.setEstSellLocalRate(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstSellLocalAmount()!=null){
			  		accountLine.setEstSellLocalAmount(accountLineReverse.getEstSellLocalAmount().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstSellLocalAmount()==null){
			  		accountLine.setEstSellLocalAmount(trueValue.multiply(multiplyValue));
			  	}
			  	accountLine.setRevisionSellCurrency(accountLineReverse.getRevisionSellCurrency());
			  	accountLine.setRevisionSellValueDate(accountLineReverse.getRevisionSellValueDate());
			  	accountLine.setRevisionSellExchangeRate(accountLineReverse.getRevisionSellExchangeRate());
			  	if(accountLineReverse.getRevisionSellLocalRate()!=null){
			  		accountLine.setRevisionSellLocalRate(accountLineReverse.getRevisionSellLocalRate().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionSellLocalRate()==null){
			  		accountLine.setRevisionSellLocalRate(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionSellLocalAmount()!=null){
			  		accountLine.setRevisionSellLocalAmount(accountLineReverse.getRevisionSellLocalAmount().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionSellLocalAmount()==null){
			  		accountLine.setRevisionSellLocalAmount(trueValue.multiply(multiplyValue));
			  	}
			   
			   
			  	accountLine.setContractCurrency(accountLineReverse.getContractCurrency());
			  	accountLine.setContractValueDate(accountLineReverse.getContractValueDate());
			  	accountLine.setContractExchangeRate(accountLineReverse.getContractExchangeRate());
			  	if(accountLineReverse.getContractRate()!=null){
			  		accountLine.setContractRate(accountLineReverse.getContractRate().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getContractRate()==null){
			  		accountLine.setContractRate(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getContractRateAmmount()!=null){
			  		accountLine.setContractRateAmmount(accountLineReverse.getContractRateAmmount().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getContractRateAmmount()==null){
			  		accountLine.setContractRateAmmount(trueValue.multiply(multiplyValue));
			  	}
			  	accountLine.setEstimateContractCurrency(accountLineReverse.getEstimateContractCurrency());
			  	accountLine.setEstimateContractValueDate(accountLineReverse.getEstimateContractValueDate());
			  	accountLine.setEstimateContractExchangeRate(accountLineReverse.getEstimateContractExchangeRate());
			  	if(accountLineReverse.getEstimateContractRate()!=null){
			  		accountLine.setEstimateContractRate(accountLineReverse.getEstimateContractRate().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstimateContractRate()==null){
			  		accountLine.setEstimateContractRate(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstimateContractRateAmmount()!=null){
			  		accountLine.setEstimateContractRateAmmount(accountLineReverse.getEstimateContractRateAmmount().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstimateContractRateAmmount()==null){
			  		accountLine.setEstimateContractRateAmmount(trueValue.multiply(multiplyValue));
			  	}
			  	accountLine.setRevisionContractCurrency(accountLineReverse.getRevisionContractCurrency());
			  	accountLine.setRevisionContractValueDate(accountLineReverse.getRevisionContractValueDate());
			  	accountLine.setRevisionContractExchangeRate(accountLineReverse.getRevisionContractExchangeRate());
			  	if(accountLineReverse.getRevisionContractRate()!=null){
			  		accountLine.setRevisionContractRate(accountLineReverse.getRevisionContractRate().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionContractRate()==null){
			  		accountLine.setRevisionContractRate(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionContractRateAmmount()!=null){
			  		accountLine.setRevisionContractRateAmmount(accountLineReverse.getRevisionContractRateAmmount().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionContractRateAmmount()==null){
			  		accountLine.setRevisionContractRateAmmount(trueValue.multiply(multiplyValue));
			  	}
			  	accountLine.setPayableContractCurrency(accountLineReverse.getPayableContractCurrency());
			  	accountLine.setPayableContractValueDate(accountLineReverse.getPayableContractValueDate());
			  	accountLine.setPayableContractExchangeRate(accountLineReverse.getPayableContractExchangeRate());
			  	accountLine.setEstimatePayableContractCurrency(accountLineReverse.getEstimatePayableContractCurrency());
			  	accountLine.setEstimatePayableContractValueDate(accountLineReverse.getEstimatePayableContractValueDate());
			  	accountLine.setEstimatePayableContractExchangeRate(accountLineReverse.getEstimatePayableContractExchangeRate());
			  	accountLine.setRevisionPayableContractCurrency(accountLineReverse.getRevisionPayableContractCurrency());
			  	accountLine.setRevisionPayableContractValueDate(accountLineReverse.getRevisionPayableContractValueDate());
			  	accountLine.setRevisionPayableContractExchangeRate(accountLineReverse.getRevisionPayableContractExchangeRate());
			    accountLine.setCountry(accountLineReverse.getCountry());
			  	accountLine.setValueDate(accountLineReverse.getValueDate());
			  	accountLine.setExchangeRate(accountLineReverse.getExchangeRate());
			  	accountLine.setEstCurrency(accountLineReverse.getEstCurrency());
			  	accountLine.setEstValueDate(accountLineReverse.getEstValueDate());
			  	accountLine.setEstExchangeRate(accountLineReverse.getEstExchangeRate());
			  	accountLine.setRevisionCurrency(accountLineReverse.getRevisionCurrency());
			  	accountLine.setRevisionValueDate(accountLineReverse.getRevisionValueDate());
			  	accountLine.setRevisionExchangeRate(accountLineReverse.getRevisionExchangeRate());
			  	if(accountLineReverse.getEstimatePayableContractRate()==null){
			  		accountLine.setEstimatePayableContractRate(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstimatePayableContractRate()!=null){
			  		accountLine.setEstimatePayableContractRate(accountLineReverse.getEstimatePayableContractRate().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstimatePayableContractRateAmmount()==null){
			  		accountLine.setEstimatePayableContractRateAmmount(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstimatePayableContractRateAmmount()!=null){
			  		accountLine.setEstimatePayableContractRateAmmount(accountLineReverse.getEstimatePayableContractRateAmmount().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstLocalRate()==null){
			  		accountLine.setEstLocalRate(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstLocalRate()!=null){
			  		accountLine.setEstLocalRate(accountLineReverse.getEstLocalRate().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstLocalAmount()==null){
			  		accountLine.setEstLocalAmount(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstLocalAmount()!=null){
			  		accountLine.setEstLocalAmount(accountLineReverse.getEstLocalAmount().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstimateRate()==null){
			  		accountLine.setEstimateRate(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstimateRate()!=null){
			  		accountLine.setEstimateRate(accountLineReverse.getEstimateRate().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstimateExpense()==null){
			  		accountLine.setEstimateExpense(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getEstimateExpense()!=null){
			  		accountLine.setEstimateExpense(accountLineReverse.getEstimateExpense().multiply(multiplyValue));
			  	}
			  	accountLine.setEstimatePassPercentage(accountLineReverse.getEstimatePassPercentage());
			  	if(accountLineReverse.getRevisionPayableContractRate()==null){
			  		accountLine.setRevisionPayableContractRate(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionPayableContractRate()!=null){
			  		accountLine.setRevisionPayableContractRate(accountLineReverse.getRevisionPayableContractRate().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionPayableContractRateAmmount()==null){
			  		accountLine.setRevisionPayableContractRateAmmount(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionPayableContractRateAmmount()!=null){
			  		accountLine.setRevisionPayableContractRateAmmount(accountLineReverse.getRevisionPayableContractRateAmmount().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionLocalRate()==null){
			  		accountLine.setRevisionLocalRate(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionLocalRate()!=null){
			  		accountLine.setRevisionLocalRate(accountLineReverse.getRevisionLocalRate().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionLocalAmount()==null){
			  		accountLine.setRevisionLocalAmount(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionLocalAmount()!=null){
			  		accountLine.setRevisionLocalAmount(accountLineReverse.getRevisionLocalAmount().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionRate()==null){
			  		accountLine.setRevisionRate(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionRate()!=null){
			  		accountLine.setRevisionRate(accountLineReverse.getRevisionRate().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionExpense()==null){
			  		accountLine.setRevisionExpense(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getRevisionExpense()!=null){
			  		accountLine.setRevisionExpense(accountLineReverse.getRevisionExpense().multiply(multiplyValue));
			  	}
			  	accountLine.setRevisionPassPercentage(accountLineReverse.getRevisionPassPercentage());
			  	accountLine.setVendorCode(accountLineReverse.getVendorCode());
			  	accountLine.setEstimateVendorName(accountLineReverse.getEstimateVendorName());
			  	accountLine.setActgCode(accountLineReverse.getActgCode());
			  	if(accountLineReverse.getPayableContractRateAmmount()==null){
			  		accountLine.setPayableContractRateAmmount(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getPayableContractRateAmmount()!=null){
			  		accountLine.setPayableContractRateAmmount(accountLineReverse.getPayableContractRateAmmount().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getLocalAmount()==null){
			  		accountLine.setLocalAmount(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getLocalAmount()!=null){
			  		accountLine.setLocalAmount(accountLineReverse.getLocalAmount().multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getActualExpense()==null){
			  		accountLine.setActualExpense(trueValue.multiply(multiplyValue));
			  	}
			  	if(accountLineReverse.getActualExpense()!=null){
			  		accountLine.setActualExpense(accountLineReverse.getActualExpense().multiply(multiplyValue));
			  	} 
			  	accountLine.setPayVatDescr(accountLineReverse.getPayVatDescr());
			    accountLine.setPayVatPercent(accountLineReverse.getPayVatPercent());
			    if(accountLineReverse.getPayVatAmt()!=null)
			    {
			    accountLine.setPayVatAmt(accountLineReverse.getPayVatAmt().multiply(multiplyValue)); 
			    }
			    else if(accountLineReverse.getPayVatAmt()==null)
			    {
			     accountLine.setPayVatAmt(trueValue.multiply(multiplyValue));
			    } 
			    
			    if(accountLineReverse.getEstExpVatAmt()!=null){
		            accountLine.setEstExpVatAmt(accountLineReverse.getEstExpVatAmt().multiply(multiplyValue));
		            }
		        else if(accountLineReverse.getEstExpVatAmt()==null){
		            accountLine.setEstExpVatAmt(trueValue.multiply(multiplyValue));
		        }
			    if(accountLineReverse.getRevisionExpVatAmt()!=null){
		            accountLine.setRevisionExpVatAmt(accountLineReverse.getRevisionExpVatAmt().multiply(multiplyValue));
		            }
		        else if(accountLineReverse.getRevisionExpVatAmt()==null){
		            accountLine.setRevisionExpVatAmt(trueValue.multiply(multiplyValue));
		        }
			  	accountLine.setVendorCode(accountLineReverse.getVendorCode());
			  	accountLine.setEstimateVendorName(accountLineReverse.getEstimateVendorName());
			  	accountLine.setActgCode(accountLineReverse.getActgCode());
			    accountLine.setPayGl(accountLineReverse.getPayGl()); 
			    accountLine.setServiceOrder(serviceOrderToRecods);
				accountLine.setSequenceNumber(serviceOrderToRecods.getSequenceNumber());
				accountLine.setShipNumber(serviceOrderToRecods.getShipNumber());
				accountLine.setServiceOrderId(serviceOrderToRecods.getId());
				
				accountLine= accountLineManager.save(accountLine);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		} 
		
	}
	private String msgClicked;
	private BigDecimal grossMargin = new BigDecimal("0.00"); 
	private BigDecimal commissionableGrossMargin = new BigDecimal("0.00");
	 @SkipValidation
	    public String estimateAccountList() {
			try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				jobName="";
				commissionJobName="No";
				
				
				
				
				
				serviceOrder = serviceOrderManager.get(sid); 
				getRequest().setAttribute("soLastName",serviceOrder.getLastName());

				miscellaneous = miscellaneousManager.get(sid);
				trackingStatus=trackingStatusManager.get(sid);
				billing= billingManager.get(sid);
				compDivForBookingAgentList = companyDivisionManager.checkBookingAgentCode(serviceOrder.getBookingAgentCode(), sessionCorpID);
				 if(compDivForBookingAgentList == null || compDivForBookingAgentList.isEmpty()){
					    ownBookingAgentFlag=true;  	
					    }
				accountLineManager.updateSelectiveInvoice(serviceOrder.getId(),sessionCorpID);
				if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
					contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
					if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
						contractType=true;	
					}
				} 
				companyDivis = customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(serviceOrder.getCorpID(), "",serviceOrder.getCompanyDivision());
				if(contractType){
				 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				 billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
				 } 
				 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				 	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
				 } 
				 if(trackingStatus.getAccNetworkGroup() && billingCMMContractType ){
					Set<String> billToSet = new HashSet<String>();
					Set<String> currencySet=new HashSet<String>();
					
					List billToCodeNtList = accountLineManager.getBillToCode(serviceOrder.getShipNumber(),serviceOrder.getId(),trackingStatus.getNetworkPartnerCode());
					if(billToCodeNtList != null && billToCodeNtList.size() > 0){
						for (int i = 0; i < billToCodeNtList.size(); i++) {
							String partnertype=  partnerManager.checkPartnerType(billToCodeNtList.get(i).toString());
							if("".equals(partnertype.trim())){
								billToSet.add(billToCodeNtList.get(i).toString());
								currencySet = accountLineManager.getAccountLineForInvoice(serviceOrder.getId(),billToCodeNtList.get(i).toString());
							}
						}
						billToCodeLst = billToSet.toString().replace("[", "").replace("]", "").replaceAll(" ", "");
						billToLength = billToSet.size();
						currencyLength = currencySet.size();
					}
				}
				}
				customerFile = serviceOrder.getCustomerFile();
				checkRecInvoiceForHVY =accountLineManager.checkRecInvoiceForHVY(serviceOrder.getShipNumber());
				List tempCommissionJob = serviceOrderManager.getCommissionableJobs(sessionCorpID);
				if(tempCommissionJob!=null && !tempCommissionJob.isEmpty() && tempCommissionJob.get(0)!= null){
					jobName = tempCommissionJob.get(0).toString();
					// # 8022 - Commission able gross margin% Start    	
					try{
				    	List <AccountLine> chargeDetailList = serviceOrderManager.getCommissionableChargeGrossMarginPersentage(sid,sessionCorpID);
				    	BigDecimal rev1 = new BigDecimal("0.00");
				 		BigDecimal exp1 = new BigDecimal("0.00");
				 		BigDecimal cGrossMargin = new BigDecimal("0.00");
				 		if(chargeDetailList!=null){
				     		for(AccountLine accObj : chargeDetailList){
				     			rev1=rev1.add(accObj.getActualRevenue());
				     			exp1=exp1.add(((accObj.getActualExpense().doubleValue()!=0)?accObj.getActualExpense():accObj.getRevisionExpense()));
				     		}
				 		}
				    	if(rev1.doubleValue()!=0){	    		
				    		cGrossMargin = BigDecimal.valueOf((((rev1.doubleValue()-exp1.doubleValue())/rev1.doubleValue())*100));
				    	}else{
				    		cGrossMargin = new BigDecimal("0");
				    	}
				    	
				    	
				    	grossMargin=rev1.subtract(exp1);
				    	commissionableGrossMargin=cGrossMargin;
					}catch(Exception e){ e.printStackTrace();}
					// # 8022 - Commission able gross margin% End    
				}
				if(jobName.contains(serviceOrder.getJob())){
					commissionJobName = "Yes";
				}
				//checkCommision = accountLineManager.getCommissionCount(sid,serviceOrder.getJob());
				List quotesToValidateList=customerFileManager.getQuotesToValidate(sessionCorpID, serviceOrder.getJob(), serviceOrder.getCompanyDivision());
				if(quotesToValidateList!=null && !quotesToValidateList.isEmpty() && !quotesToValidateList.contains(null)){
				quotesToValidate=quotesToValidateList.get(0).toString();
				}else{
					quotesToValidate="";
				}
				if(billing.getContract()==null || billing.getContract().trim().equals(""))
				{ 
					billingContractFlag="N";
				}
				else 
				{ 
					billingContractFlag="Y";
				}
				if(billing == null)
				{
				  billingFlag=0;
				}
				else
				{
					billingFlag=1;
				}
				if(accountLineStatus==null||accountLineStatus.equals(""))
				{
					accountLineStatus="true";
				}
				if(!"category".equals(vanLineAccountView)){
					accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
					getSession().setAttribute("accountLines", accountLineList);
				}
				if(accountLineList == null || accountLineList.isEmpty()){
					
					emptyList="true";
				}
				if(serviceOrder.getCompanyDivision()!=null && (!(serviceOrder.getCompanyDivision().toString().equals(""))) && ((serviceOrder.getCompanyDivision().toString().equals("FFG"))|| (serviceOrder.getCompanyDivision().toString().equals("FMG")) )){
				allPreviewLine=accountLineManager.getAllPreviewLine(sessionCorpID, sid);
				if(allPreviewLine == null || allPreviewLine.isEmpty()){
					previewLine="true";
				}
				}
				updateAccountLine(serviceOrder);
				shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
				minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
				countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");  
				return SUCCESS;
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getCause().getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.getCause().printStackTrace();
		    	 return "errorlog";
			}   
	    }
	 
	 @SkipValidation
	    public String updateSelectiveInvoice() 
	    {
		 try {
			selectiveInvoiceList = accountLineManager.updateSelectiveInvoice(sid,id,selectiveInvoice,sessionCorpID);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
	    	 return SUCCESS; 	
	    }
	 @SkipValidation
	 public String checkNetworkAgentInvoice() 
	    {
		 try {
			networkAgentInvoiceFlag = accountLineManager.findNetworkAgentInvoiceFlag(networkAgentId);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
    	 return SUCCESS; 	
	    }
	 
	@SkipValidation 
    public String getNotesForIconChange() {  
		
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			List m = notesManager.countForCategoryTypeNotes(accountLine.getId());
			List mo = notesManager.countForEntitleDetailNotes(accountLine.getId());
			List md = notesManager.countForEstimateDetailNotes(accountLine.getId());
			List ms = notesManager.countForRevisionDetailNotes(accountLine.getId());
			List mso = notesManager.countForPayableDetailNotes(accountLine.getId());
			List msdo = notesManager.countForReceivableDetailNotes(accountLine.getId()); 
			if( m.isEmpty() ){
				countCategoryTypeDetailNotes = "0";
			}else {
				countCategoryTypeDetailNotes = ((m).get(0)).toString() ;
			}
			
			if( md.isEmpty() ){
				countEstimateDetailNotes = "0";
			}else {
				countEstimateDetailNotes = ((md).get(0)).toString() ;
			}
			
			if( ms.isEmpty() ){
				countRevesionDetailNotes = "0";
			}else {
				countRevesionDetailNotes = ((ms).get(0)).toString() ;
			}
			if( mo.isEmpty() ){
				countEntitleDetailOrderNotes = "0";
			}else {
				countEntitleDetailOrderNotes = ((mo).get(0)).toString() ;
			} 
			if( mso.isEmpty() ){
				countPayableDetailNotes = "0";
			}else {
				countPayableDetailNotes = ((mso).get(0)).toString() ;
			}
			if( msdo.isEmpty() ){
				countReceivableDetailNotes = "0";
			}else {
				countReceivableDetailNotes = ((msdo).get(0)).toString() ;
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS;   
    }
	
	public String editAfterSubmitPartnerType() throws Exception 
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if ( save != null ) 
		{
			getGlTypeList();
	        getCommissionList();
			return save();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return "Submit";
	}
	
	public String chargeCode()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		chargeAction.list();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

		return SUCCESS;
	} 
	private Long maxId;
	public String newSysDefaultDate=new String("");
    private String chargeVatCalculation;
	public String getChargeVatCalculation() {
		return chargeVatCalculation;
	}

	public void setChargeVatCalculation(String chargeVatCalculation) {
		this.chargeVatCalculation = chargeVatCalculation;
	}
	
	private String setRegistrationNumber;
	public String save() throws Exception {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			billing =billingManager.get(sid);
			trackingStatus=trackingStatusManager.get(sid);
			serviceOrder=serviceOrderManager.get(sid);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			customerFile = serviceOrder.getCustomerFile();
			//getComboList(sessionCorpID);
			companyDivis = customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(serviceOrder.getCorpID(), "",serviceOrder.getCompanyDivision());  
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){ 
				contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}
				
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			    }
			    if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			      	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
			      }
			    if(billingDMMContractType){
			    	try{
			    		if(trackingStatus.getUtsiNetworkGroup()){ 
			              if((!(trackingStatus.getAccNetworkGroup())) && serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getBookingAgentExSO()!=null && (!(trackingStatus.getBookingAgentExSO().equals(""))))	{ 
						    	serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(trackingStatus.getBookingAgentExSO()));
						    	customerFileToRecods=customerFileManager.getForOtherCorpid(serviceOrderToRecods.getCustomerFileId());
						    	bookingAgentCodeDMM=customerFileToRecods.getAccountCode();
					      } 		
			    		}else{
			    	if((!(trackingStatus.getAccNetworkGroup())) && serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && serviceOrder.getBookingAgentShipNumber()!=null && (!(serviceOrder.getBookingAgentShipNumber().equals(""))))	{ 
				    	serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(serviceOrder.getBookingAgentShipNumber()));
				    	customerFileToRecods=customerFileManager.getForOtherCorpid(serviceOrderToRecods.getCustomerFileId());
				    	bookingAgentCodeDMM=customerFileToRecods.getAccountCode();
				    	}
			    		}
				    	}catch(Exception e){
				    		 e.printStackTrace();	
				    	}
				    	}
			    try{ 
			        if(trackingStatus.getSoNetworkGroup() && billingCMMContractType && accountLine.getCreatedBy()!=null && accountLine.getCreatedBy().trim().equalsIgnoreCase("Networking")){
			        	if(trackingStatus.getUtsiNetworkGroup()){ 
	  
			        	}else{

			        	}
			        }
			        }catch(Exception e){
			        	 e.printStackTrace();
			        }
			         
			            try{
			            	if(accountLine.getId()!=null){
			                if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup())	{
			              	   String networkSynchedId=	accountLineManager.getNetworkSynchedId(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
			                    	if(!(networkSynchedId.equals(""))){
			                    		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
			                    		Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
			    			      		boolean accNonEditable=false;
			    						 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
			    							 accNonEditable =true;
			    						 }
			    						 if(accNonEditable){
			    							 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
			    				      			 utsiRecAccDateFlag=true; 
			    				      		}	 
			    						 }else{
			                    		if(synchedAccountLine.getRecAccDate()!=null && accountLine.getRecAccDate()== null){
			                    			 utsiRecAccDateFlag=true; 
			                    		}
			    						 }
			                    		if(synchedAccountLine.getPayAccDate()!=null && accountLine.getRecAccDate()== null){
			                    			 utsiPayAccDateFlag=true;
			                    		}
			                    	}  
			                }
			            	}
			              }catch(Exception e){
			            	  e.printStackTrace();	
			              }
			        	String permKeys = sessionCorpID +"-"+"component.partner.vatBillingGroup.edit";
						boolean visibilityForCorpId=false;
						 visibilityForCorpId=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKeys) ;	
					 if(visibilityForCorpId){
					    String flag="";
						String vatBillimngGroupDes="";
						String partnerVatDec="";
						if(accountLine.getBillToCode()!=null && !accountLine.getBillToCode().equals("")){
							String vatBillimngGroupCode="";
						List vatBillimngGroupCode1=partnerPrivateManager.findVatBillimngGroup(sessionCorpID, accountLine.getBillToCode());
						Iterator listIterator= vatBillimngGroupCode1.iterator();
						while (listIterator.hasNext()) {
						 Object [] row=(Object[])listIterator.next();
							if(row[0]!=null) {
							 vatBillimngGroupCode=row[0].toString();  
							 }
							 if(row[1]!=null){
							partnerVatDec = row[1].toString();  
							}
							}
						if(vatBillingGroupList.containsKey(vatBillimngGroupCode)){
							vatBillimngGroupDes =vatBillingGroupList.get(vatBillimngGroupCode);
						}
							 flex1  = refMasterManager.findPartnerVatBillingCode(vatBillimngGroupDes, sessionCorpID,accountLine.getCompanyDivision());
							if(vatBillimngGroupCode!=null && !vatBillimngGroupCode.equals("") && flex1!=null && (!(flex1.trim().equals("")))){
								  euVatList=refMasterManager.findVatDescriptionList(sessionCorpID, "EUVAT", flex1, accountLine.getRecVatDescr(),partnerVatDec,"Y");
							}
						}
						
						if(accountLine.getVendorCode()!=null && !accountLine.getVendorCode().equals("")){
							String vatBillimngGroupCode="";
							List vatBillimngGroupCode1=partnerPrivateManager.findVatBillimngGroup(sessionCorpID, accountLine.getVendorCode());
							Iterator listIterator= vatBillimngGroupCode1.iterator();
							while (listIterator.hasNext()) {
							 Object [] row=(Object[])listIterator.next();
								if(row[0]!=null) {
								 vatBillimngGroupCode=row[0].toString();  
								 }
								 if(row[1]!=null){
								partnerVatDec = row[1].toString();  
								}
								}
							if(vatBillingGroupList.containsKey(vatBillimngGroupCode)){
								vatBillimngGroupDes =vatBillingGroupList.get(vatBillimngGroupCode);	
							}
							   flex2 = refMasterManager.findAccountLinePayVatBillingCode(vatBillimngGroupDes, sessionCorpID, accountLine.getCompanyDivision());
								if(vatBillimngGroupCode!=null && !vatBillimngGroupCode.equals("") && flex2!=null && (!(flex2.trim().equals("")))){
									 payVatList=refMasterManager.findPayVatDescriptionList(sessionCorpID, "PAYVATDESC", flex2, accountLine.getPayVatDescr(), "Y");
								}
							}
					 }  
			if(accountLine.getBillToCode()!=null && (!accountLine.getBillToCode().equals(""))){
			creditInvoiceList=accountLineManager.getCreditInvoice(sessionCorpID,sid,accountLine.getBillToCode());
			}
			try
			{
			if(!serviceOrderManager.findBySysDefault(sessionCorpID).isEmpty())
			{    
				 newSysDefaultDate = serviceOrderManager.findBySysDefault(sessionCorpID).get(0).toString();
				 newSysDefaultDate=newSysDefaultDate.replace(".0", ""); 
			}
			else
			{
				newSysDefaultDate=new String("");//newSysDefaultDate.replace(".0", "");
			}
			Date dtTmp = new SimpleDateFormat("yyyy-MM-dd").parse(newSysDefaultDate); 
			if( ! (accountLine.getRecPostDate() == null) ){ 
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			if((billingManager.checkById(sid)).isEmpty())
			{ 	
				billingFlag=0; 
			}
			else
			{ 	
				billingFlag=1; 
			}
			boolean isNew =(accountLine.getId() == null);
			/**if(isNew){
				accountLineManager.updateSOExtFalg(serviceOrder.getId());
			}else if(!isNew){
				if(isSOExtract!=null && isSOExtract.equals("yes")){
				accountLineManager.updateSOExtFalg(serviceOrder.getId());
				}
			}*/
			accountLine.setServiceOrder(serviceOrder);
			accountLine.setUpdatedOn(new Date());
			accountLine.setUpdatedBy(getRequest().getRemoteUser());
			accountLine.setCorpID(sessionCorpID);
			
			getGlTypeList();
			getCommissionList();
			if(accountLine.getSendActualToClient()!=null && dateUpdate.trim().equalsIgnoreCase("Y"))
			{
				SimpleDateFormat sendActualToClient = new SimpleDateFormat("yyyy-MM-dd");
			    StringBuilder sendActualToClientDates = new StringBuilder(sendActualToClient.format(accountLine.getSendActualToClient())); 
			    int i=accountLineManager.updateInvActSentToDate(serviceOrder.getShipNumber(),sessionCorpID,accountLine.getRecInvoiceNumber(),sendActualToClientDates.toString(),getRequest().getRemoteUser(),accountLine.getId());
			}
		
			if(isNew){
				accountLine.setCreatedOn(new Date());
			}
			autoLineNumber=Long.parseLong(accountLine.getAccountLineNumber());
			if((autoLineNumber.toString()).length() == 2) {
			    accountLineNumber = "0"+(autoLineNumber.toString());
			 }
			else if((autoLineNumber.toString()).length() == 1) {
			    accountLineNumber = "00"+(autoLineNumber.toString());
			 } 
			else {
			    accountLineNumber=autoLineNumber.toString();
			 }  
			accountLine.setAccountLineNumber(accountLineNumber);  
			if((accountInterface.trim().equalsIgnoreCase("Y"))&&(systemDefaultmiscVl.contains(serviceOrder.getJob()))&&(accountLine.getPayingStatus().equals("A"))&&(accountLine.getActgCode().trim().equalsIgnoreCase("TEMP"))&&(accountLine.getInvoiceDate()==null||accountLine.getInvoiceDate().toString().equals("")))
			{
				accountLine.setInvoiceDate(new Date());
			}
			if(billingCMMContractType && trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup() && (accountLine.getRecInvoiceNumber() ==null || accountLine.getRecInvoiceNumber().trim().equals(""))){
				String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
				if(!(networkSynchedId.equals(""))){
					synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
					if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().trim().equals("")))){
						String key1 = "This is a CMM order and Harmony already generated invoice for this account line. System reset all your data. Please re-enter your data again."; 
						setFormStatus("1");
						 errorMessage(getText(key1));
					     validateFormNav = "networkAgentInvoiced";
					     	return INPUT; 
					}
				}
			}
			if(accountLine.getCategory()==null || accountLine.getCategory().trim().equalsIgnoreCase("") ){
				String key1 = "Category is a required field."; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     	return INPUT; 
			}
			if(compDivFlag !=null && compDivFlag.trim().equalsIgnoreCase("yes") && (accountLine.getCompanyDivision()==null || accountLine.getCompanyDivision().trim().equalsIgnoreCase("")) ){
				String key1 = "Company Division is a required field."; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     	return INPUT; 
			}
			///Code for AccountLine division 
			if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))&&((accountLine.getDivision()==null)||(accountLine.getDivision().equalsIgnoreCase("")))){
				String key1 = "Division is a required field."; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     	return INPUT;  
			}
			///Charge Code for UTSI
			
			if((checkContractChargesMandatory!=null && !checkContractChargesMandatory.equals("")) && checkContractChargesMandatory.equals("1") && (accountLine.getChargeCode()==null || accountLine.getChargeCode().equalsIgnoreCase("") )){
				String key1 = "Charge Code is a required field."; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     	return INPUT; 
			}
			
			///Code for AccountLine division 
			if(contractType){
				boolean returnflag=false;
			if(accountLine.getEstimatePayableContractCurrency()==null || accountLine.getEstimatePayableContractCurrency().equals("")||accountLine.getEstimateContractCurrency()==null || accountLine.getEstimateContractCurrency().equals("")){
				String key1 = "Contract Currency in the Estimate Detail Section is a required field "; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     returnflag=true;
			     //return INPUT;   	
			}
			if(accountLine.getRevisionPayableContractCurrency()==null || accountLine.getRevisionPayableContractCurrency().equals("")||accountLine.getRevisionContractCurrency()==null || accountLine.getRevisionContractCurrency().equals("")){
				String key1 = "Contract Currency in the Revision Detail Section is a required field "; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     returnflag=true;
			     //return INPUT;   	
			}
			if(accountLine.getContractCurrency()==null || accountLine.getContractCurrency().equals("")){
				String key1 = "Contract Currency in the Receivable Details Section is a required field "; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     returnflag=true;
			     //return INPUT;   	
			}
			if(accountLine.getPayableContractCurrency()==null || accountLine.getPayableContractCurrency().equals("")){
				String key1 = "Contract Currency in the Payable Detail Section is a required field "; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     returnflag=true;
			     //return INPUT;   	
			}
			if(accountLine.getEstCurrency()==null || accountLine.getEstCurrency().equals("")){
				String key1 = "Currency in the Estimate Detail Section is a required field "; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     returnflag=true;
			     //return INPUT;   	
			}
			if(accountLine.getEstSellCurrency()==null || accountLine.getEstSellCurrency().equals("")){
				String key1 = "Billing Currency in the Estimate Detail Section is a required field "; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     returnflag=true;
			     //return INPUT;   	
			}
			if(accountLine.getRevisionCurrency()==null || accountLine.getRevisionCurrency().equals("")){
				String key1 = "Currency in the Revision Detail Section is a required field "; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     returnflag=true;
			     //return INPUT;   	
			}
			if(accountLine.getRevisionSellCurrency()==null || accountLine.getRevisionSellCurrency().equals("")){
				String key1 = "Billing Currency in the Revision Detail Section is a required field "; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     returnflag=true;
			     //return INPUT;   	
			}
			if(accountLine.getRecRateCurrency()==null || accountLine.getRecRateCurrency().equals("")){
				String key1 = "Billing Currency in the Receivable Detail Section is a required field "; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     returnflag=true;
			    	
			}
			if(accountLine.getCountry()==null || accountLine.getCountry().equals("")){
				String key1 = "Currency in the Payable Detail Section is a required field "; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     returnflag=true;
			       	
			}
			if((accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) && trackingStatus.getSoNetworkGroup() && (accountLine.getChargeCode().equalsIgnoreCase("MGMTFEE") || accountLine.getChargeCode().equalsIgnoreCase("DMMFEE") || accountLine.getChargeCode().equalsIgnoreCase("DMMFXFEE"))){
				String key1 = "Please choose other charge code."; 
				setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     returnflag=true;
			}
			if(returnflag){
			return INPUT;  
			}
			}
			if(((accountLine.getRevisionExpense().doubleValue()!= 0)) && ((accountLine.getVendorCode() == null) || (accountLine.getVendorCode().trim().equals("")))){
			   
				 String key1 = "Revision Expense cannot be entered as no Partner has been selected. Please select a partner before entering revision expense. ";
				 setFormStatus("1");
				 errorMessage(getText(key1));
			     validateFormNav = "NOTOK";
			     return INPUT;   
			}
			
			try{
				String permKey = sessionCorpID +"-"+"component.tab.accountline.blankDescription";
				checkDescriptionFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
				}catch(Exception e){
					e.printStackTrace();
				}
			try{
			
			// New Changes 12145
				if(accountLine.getChargeCode()!=null  && (!(accountLine.getChargeCode().toString().equals("")))){
					List al=chargesManager.findChargeId(accountLine.getChargeCode(), accountLine.getContract());
					if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().equalsIgnoreCase(""))){
					Charges charge=chargesManager.get(Long.parseLong(al.get(0).toString()));
				if((accountLine.getDescription()==null) || (accountLine.getDescription().toString().trim().equals("")) || (accountLine.getNote()==null) || (accountLine.getNote().toString().trim().equals("")) || (accountLine.getQuoteDescription()==null) || (accountLine.getQuoteDescription().toString().trim().equals("")))
				{
			 
				  if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().equalsIgnoreCase(""))){
					 
					  if(!checkDescriptionFieldVisibility){
					  if((accountLine.getDescription()==null || accountLine.getDescription().toString().trim().equals(""))){
						  accountLine.setDescription(charge.getDescription());
						  }
					  if((accountLine.getQuoteDescription()==null || accountLine.getQuoteDescription().toString().trim().equals(""))){
						  accountLine.setQuoteDescription(charge.getDescription());
						  }
					  }
					  if((accountLine.getNote()==null || accountLine.getNote().toString().trim().equals(""))){
						  accountLine.setNote(charge.getDescription());
						  }
					 
				  }}
				// 12173 
				 if(accountLine.getAccountLineCostElement()==null || (accountLine.getAccountLineCostElement().equals(""))){
					 if(charge.getCostElement()!=null && (!charge.getCostElement().equals(""))){
					 accountLine.setAccountLineCostElement(charge.getCostElement());
					 }
				  }
				 if(accountLine.getAccountLineScostElementDescription()==null || (accountLine.getAccountLineScostElementDescription().equals(""))){
					 if(charge.getScostElementDescription()!=null && (!charge.getScostElementDescription().equals(""))){
					 accountLine.setAccountLineScostElementDescription(charge.getScostElementDescription());
					 }
				  }
					}
			}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		
			if((systemDefaultVatCalculation.trim().equalsIgnoreCase("true")) &&(!accountLine.getVATExclude()) && (accountLine.getActualExpense().doubleValue()!=0 ) && (accountLine.getPayVatDescr()==null || accountLine.getPayVatDescr().equals("")) ){
				String key1 = "Please fill VAT details in Payable details section."; 
				setFormStatus("1");
			    errorMessage(getText(key1));
			    validateFormNav = "NOTOK";
			    return INPUT;
			}
			if((systemDefaultVatCalculation.trim().equalsIgnoreCase("true")) &&(!accountLine.getVATExclude()) && (accountLine.getActualRevenue().doubleValue()!=0 ) && (accountLine.getRecVatDescr()==null || accountLine.getRecVatDescr().equals("")) ){
				String key1 = "Please fill VAT details in Receivable details section."; 
				setFormStatus("1");
			    errorMessage(getText(key1));
			    validateFormNav = "NOTOK";
			    return INPUT;
			}
			if(accountLine.getId()!=null && ((accountLine.getRecInvoiceNumber()==null || accountLine.getRecInvoiceNumber().trim().equals(""))) ){
				AccountLine	dbAccObj = accountLineManager.get(accountLine.getId());
				if((dbAccObj.getRecInvoiceNumber()!=null && (!(dbAccObj.getRecInvoiceNumber().trim().equals(""))))){
					String key1 = "Please fill the data again as Invoice is already generated by other user."; 
					accountLine = accountLineManager.get(accountLine.getId());
					setFormStatus("1");
				    errorMessage(getText(key1));
				    validateFormNav = "NOTOK";
				    return INPUT;
				}
			}
			if((accountInterface.trim().equalsIgnoreCase("Y")) && (accountLine.getActualExpense().doubleValue()!=0 ) && ((accountLine.getActgCode()==null)||(accountLine.getActgCode().trim().equalsIgnoreCase(""))||(accountLine.getActgCode().trim().equalsIgnoreCase("null"))))
			{
				 String key1 = "Invalid Actg Code, Please correct it."; 
				 setFormStatus("1");
				 errorMessage(getText(key1));
				 validateFormNav = "NOTOK";
				 return INPUT;
			}
			if((!(accountInterface.trim().equalsIgnoreCase("Y")))&&((accountLine.getActualExpense().doubleValue()!= 0)) && (accountLine.getVendorCode() == null || (accountLine.getVendorCode().trim().equals("")))){
			   
				 String key1 = "You can not remove Partner Code as there is value in the payable details section ";
				 setFormStatus("1");
				 errorMessage(getText(key1));
				 validateFormNav = "NOTOK";
				 return INPUT;   
			} 
			if((accountLine.getVendorCode() != null &&  (!(accountLine.getVendorCode().trim().equals("")))) && ( (accountLine.getEstimateVendorName() ==null) || (accountLine.getEstimateVendorName().trim().equals("")) )){
				   
				 String key1 = "Vendor Name is not valid ";
				 setFormStatus("1");
				 errorMessage(getText(key1));
				 validateFormNav = "NOTOK";
				 return INPUT;   
			} 
			if((accountLine.getEstimateVendorName() != null &&  (!(accountLine.getEstimateVendorName().trim().equals("")))) && ( (accountLine.getVendorCode() ==null) || (accountLine.getVendorCode().trim().equals("")) )){
				   
				 String key1 = "Vendor Code is not valid ";
				 setFormStatus("1");
				 errorMessage(getText(key1));
				 validateFormNav = "NOTOK";
				 return INPUT;   
			} 
      
				if((accountInterface.trim().equalsIgnoreCase("Y")) && (accountLine.getActualRevenue().doubleValue()!=0) && (accountLine.getRecGl()==null || (accountLine.getRecGl().equals("")) || (accountLine.getRecGl().equalsIgnoreCase("undefind"))) ){
					String key1 = "GL Code in Receivable Section is not correct. ";
				 	setFormStatus("1");
				 	errorMessage(getText(key1));
				 	validateFormNav = "NOTOK";
				 	return INPUT; 
				}
				if((accountInterface.trim().equalsIgnoreCase("Y")) && (accountLine.getActualExpense().doubleValue()!=0) && (accountLine.getPayGl()==null || (accountLine.getPayGl().equals("")) || (accountLine.getPayGl().equalsIgnoreCase("undefind"))) ){
					String key1 = "GL Code in Payable Section is not correct. ";
				 	setFormStatus("1");
				 	errorMessage(getText(key1));
				 	validateFormNav = "NOTOK";
				 	return INPUT; 
				}
				
				
			 
			if(((accountLine.getActualRevenue().doubleValue()!= 0)) && (accountLine.getChargeCode() == null || (accountLine.getChargeCode().trim().equals("")))){
			   
				 String key1 = "You can not remove Charge Code as there is value in the Receivable details section "; 
				 setFormStatus("1");
				 errorMessage(getText(key1));
				 validateFormNav = "NOTOK";
				 return INPUT;   
			} 
			else {
				Boolean temp=false;
				try {
					if(pageFieldVisibilityMap.containsKey(sessionCorpID +"-component.field.PayableInvoiceNumber.SetServiceOrderId")) {
						Integer i= (Integer) pageFieldVisibilityMap.get(sessionCorpID +"-component.field.PayableInvoiceNumber.SetServiceOrderId") ;
						if(i>=2){
						temp=true;
						}
					} 
					} catch (Exception e) {
						log.error("Error in CorpComponentPermission the  Field Visibility cache",e);
					}				
				if(accountLine.getVendorCode()!= null && (!(accountLine.getVendorCode().toString().trim().equals(""))) && accountLine.getActualExpense()!=null && accountLine.getActualExpense().doubleValue()!=0 && (accountLine.getInvoiceNumber() == null || accountLine.getInvoiceNumber().toString().trim().equals("")) && accountLine.getActgCode()!=null &&  (!(accountLine.getActgCode().trim().equals("")))){
					boolean isowneroperator =false;
					isowneroperator=accountLineManager.findOwneroperator(accountLine.getVendorCode());
					Boolean autuPopPostDate=false;
			    	List<SystemDefault> sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
					if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
						for (SystemDefault systemDefault : sysDefaultDetail) {
							autuPopPostDate=systemDefault.getAutoPayablePosting();
						}
					}
					if(isowneroperator){						
						if(autuPopPostDate){
							String invoiceNumber="A"+sid+accountLine.getVendorCode();
							accountLine.setInvoiceNumber(invoiceNumber);
							accountLine.setReceivedDate(new Date());
							accountLine.setPayingStatus("A");
							accountLine.setInvoiceDate(new Date());
						}else if(temp){
							String invoiceNumber=sid+accountLine.getVendorCode();
							accountLine.setInvoiceNumber(invoiceNumber);
							accountLine.setReceivedDate(new Date());
							accountLine.setPayingStatus("A");
							accountLine.setInvoiceDate(new Date());							
						}
					}else{
						if(setRegistrationNumber!=null &&(!setRegistrationNumber.equalsIgnoreCase("")) && (accountLine.getActualExpense()!=null && accountLine.getActualExpense().doubleValue()!=0)){
							if(serviceOrder.getRegistrationNumber()!=null && (!(serviceOrder.getRegistrationNumber().equalsIgnoreCase("")))){
				        				accountLine.setInvoiceNumber(serviceOrder.getRegistrationNumber().trim());
								}else{
										accountLine.setInvoiceNumber(serviceOrder.getShipNumber());
									}
								if(accountLine.getInvoiceDate()!=null && (!accountLine.getInvoiceDate().equals(""))){
										accountLine.setInvoiceDate(accountLine.getInvoiceDate());
								}else{
									accountLine.setInvoiceDate(new Date());
								}
									accountLine.setReceivedDate(new Date());
							}
						}
					}
				if(setRegistrationNumber!=null &&(!setRegistrationNumber.equalsIgnoreCase("")) && (accountLine.getActualExpense()!=null && accountLine.getActualExpense().doubleValue()!=0) && (accountLine.getInvoiceNumber()!= null && (!accountLine.getInvoiceNumber().toString().trim().equals("")))){
					if(accountLine.getInvoiceDate()!=null && (!accountLine.getInvoiceDate().equals(""))){
						accountLine.setInvoiceDate(accountLine.getInvoiceDate());
					}else{
						accountLine.setInvoiceDate(new Date());
					}
					if(accountLine.getReceivedDate()!=null){
					}else{
						accountLine.setReceivedDate(new Date());
					}
				}
				if(setRegistrationNumber!=null &&(!setRegistrationNumber.equalsIgnoreCase("")) && (accountLine.getActualExpense()==null || accountLine.getActualExpense().doubleValue()==0) && (accountLine.getInvoiceNumber()!= null && (!accountLine.getInvoiceNumber().toString().trim().equals(""))) && (accountLine.getPayingStatus()!=null)&&(accountLine.getPayingStatus().equalsIgnoreCase("A"))){
					accountLine.setInvoiceNumber("");
				
					accountLine.setPayingStatus("");
			
				}
				if((accountLine.getActualExpense()==null || accountLine.getActualExpense().doubleValue()==0  || accountLine.getActgCode()==null || (accountLine.getActgCode().trim().equals(""))) && (accountLine.getInvoiceNumber()!= null && (!accountLine.getInvoiceNumber().toString().trim().equals(""))) && (accountLine.getPayingStatus()!=null)&&(accountLine.getPayingStatus().equalsIgnoreCase("A"))){
					accountLine.setInvoiceNumber("");
				
					accountLine.setPayingStatus("");
									
				}
				if((accountLine.getPayingStatus()!=null)&&(accountLine.getPayingStatus().equalsIgnoreCase("A"))&&(accountLine.getLocalAmount()!=null)&&(accountLine.getLocalAmount().floatValue()!=0)&& (accountLine.getActualExpense()!=null && accountLine.getActualExpense().doubleValue()!=0)){
			    	Boolean autuPopPostDate=false;
			    	Date payDate=null;
			    	List<SystemDefault> sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
					if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
						for (SystemDefault systemDefault : sysDefaultDetail) {
							autuPopPostDate=systemDefault.getAutoPayablePosting();
							payDate=systemDefault.getPostDate1();
						}
					}
					if(autuPopPostDate){
						//change for posting date flexiblity Start	
						try{
							company=companyManager.findByCorpID(sessionCorpID).get(0);
				    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
					    		Date dt1=new Date();
					    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					    		String dt11=sdfDestination.format(dt1);
					    		dt1=sdfDestination.parse(dt11);
					    		Date dt2 =null;
					    		try{
					    		dt2 = new Date();
					    		}catch(Exception e){
					    			e.printStackTrace();
					    		}				    		
					    		
					    		dt2=payDate;
					    		if(dt2!=null){
						    		if(dt1.compareTo(dt2)>0){
						    			payDate=dt2;
						    		}else{
						    			payDate=dt1;
						    		}
					    		}
				    		}
					}catch(Exception e){e.printStackTrace();}			
					//change for posting date flexiblity End		
						if(payDate!=null){
							if((accountLine.getPayPostDate()==null)&&(accountLine.getPayAccDate()==null)){
								accountLine.setPayPostDate(payDate);
							}
						}
						 
					}
				}
				if(accountLine!=null && (accountLine.getNetworkBillToCode() ==null || accountLine.getNetworkBillToCode().trim().equals(""))){
					accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
		    		accountLine.setNetworkBillToName(billing.getNetworkBillToName());
				}
				if(accountLine.getId()!=null && ((accountLine.getPayPostDate() !=null && accountLine.getPayAccDate()==null) || (accountLine.getRecPostDate() !=null && accountLine.getRecAccDate()==null))){
					AccountLine	dbAccObj = accountLineManager.get(accountLine.getId());
					if(dbAccObj.getPayAccDate() !=null && accountLine.getPayAccDate()==null){
						accountLine.setPayAccDate(dbAccObj.getPayAccDate());
						accountLine.setPayXfer(dbAccObj.getPayXfer());
						accountLine.setPayXferUser(dbAccObj.getPayXferUser());
					}
					
					if(dbAccObj.getRecAccDate() !=null && accountLine.getRecAccDate()==null){
						accountLine.setRecAccDate(dbAccObj.getRecAccDate());
						accountLine.setRecXfer(dbAccObj.getRecXfer());
						accountLine.setRecXferUser(dbAccObj.getRecXferUser());
					}
				}
				accountLine=accountLineManager.save(accountLine);  
				if(accountLine.getVendorCode()!=null && !accountLine.getVendorCode().equals("") && accountLine.getMyFileFileName()!=null && !accountLine.getMyFileFileName().equals("") ){
					String myfileId[]=accountLine.getMyFileFileName().split(":");
					if(!myfileId[0].equals("")){
						String AccPayingStatus="";
						if(accountLine.getPayingStatus().equals("A")){
							AccPayingStatus="Approved";
						}else if(accountLine.getPayingStatus().equals("I")){
							AccPayingStatus="Internal Cost";
						}else if(accountLine.getPayingStatus().equals("N")){
							AccPayingStatus="Not Authorized";
						}else if(accountLine.getPayingStatus().equals("P")){
							AccPayingStatus="Pending with note";
						}else if(accountLine.getPayingStatus().equals("S")){
							AccPayingStatus="Short pay with notes";
						}
						accountLineManager.updateMyfileVender(accountLine.getVendorCode(),AccPayingStatus,myfileId[0],sessionCorpID);	
					}        		
				}
				if(accountLine.isStatus()){}else{
			  		try{
			  			String loginUser = getRequest().getRemoteUser();	
				  		accountLineManager.updateCommisionLockingTable(accountLine.getId()+"",sessionCorpID,loginUser);
				  		}catch (Exception e) {
				  			e.printStackTrace();
						}
				}        	
				accSaveHitflag="1";
				if(saveCopyFlag.equalsIgnoreCase("YES")){
					addCopyAccountLine=accountLine;
				}
				try {
					if(accountLine.getCategoryResourceName()!=null && (!"".equals(accountLine.getCategoryResourceName().trim()))){
						if((accountLine.getRevisionExpense() != null && accountLine.getRevisionExpense().doubleValue() > 0 && (!"".equals(accountLine.getRevisionExpense().toString().trim())))
								||(accountLine.getRevisionRevenueAmount() != null && accountLine.getRevisionRevenueAmount().doubleValue() > 0 && (!"".equals(accountLine.getRevisionRevenueAmount().toString().trim()))
								|| (accountLine.getActualRevenue() != null && accountLine.getActualRevenue().doubleValue() > 0 && (!"".equals(accountLine.getActualRevenue().toString().trim())))
								|| (accountLine.getActualExpense() != null && accountLine.getActualExpense().doubleValue() > 0 && (!"".equals(accountLine.getActualExpense().toString().trim())))
								|| (accountLine.getDistributionAmount() != null && accountLine.getDistributionAmount().doubleValue() > 0 && (!"".equals(accountLine.getDistributionAmount().toString().trim())))
								|| (accountLine.getRecInvoiceNumber() != null && (!"".equals(accountLine.getRecInvoiceNumber().trim()))))){
							String categoryResource[] = accountLine.getCategoryResourceName().split("-@-");
							String category = categoryResource[0];
							String resource = categoryResource[1];
							itemsJbkEquipManager.updateAccountLineForCreatePricing(accountLine.getShipNumber(),category,resource,accountLine.getCategoryResource());
						}
					}
				} catch (Exception e) {
					 e.printStackTrace();
				}
			
			
			if(multiCurrency.trim().equalsIgnoreCase("Y")&& (!(accountLine.getRecInvoiceNumber().toString().equals(""))) && (!(oldRecRateCurrency.equals("")))){
				SimpleDateFormat racValueDate = new SimpleDateFormat("yyyy-MM-dd");
			    StringBuilder racValueDates = new StringBuilder(racValueDate.format(accountLine.getRacValueDate()));  
				int i=accountLineManager.updateCurrencyToInvoice(serviceOrder.getShipNumber(),sessionCorpID,accountLine.getRecInvoiceNumber(),accountLine.getRecRateExchange(),accountLine.getRecRateCurrency(),accountLine.getId(),racValueDates.toString(),accountLine.getBillToCode(),accountLine.getBillToName(),getRequest().getRemoteUser(),contractType);
			}
			if(multiCurrency.trim().equalsIgnoreCase("Y")&& (!(accountLine.getRecInvoiceNumber().toString().equals(""))) && accountLine.getReceivedInvoiceDate()!=null){
				SimpleDateFormat receivedInvoiceDate = new SimpleDateFormat("yyyy-MM-dd");
			    StringBuilder receivedInvoiceDates = new StringBuilder(receivedInvoiceDate.format(accountLine.getReceivedInvoiceDate()));  
			    int i=accountLineManager.updateReceivedInvoiceDate(serviceOrder.getShipNumber(),sessionCorpID,accountLine.getRecInvoiceNumber(),receivedInvoiceDates.toString());
			}
			serviceOrder=serviceOrderManager.get(sid);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			trackingStatus=trackingStatusManager.get(sid);
			
			
			if(billingCMMContractType){
			if(isNew){
			    if(trackingStatus.getAccNetworkGroup())	{
			      if(serviceOrder.getIsNetworkRecord()){
			    	  String partnertype="";
			    	  if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
			    		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
			    	  }
			    	  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (!(partnertype.equals("")))){
			    	  //int j=accountLineManager.updateNetworkSynchedId(accountLine.getId());  
						List linkedShipNumber=findLinkerShipNumber(serviceOrder);
						List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
						createAccountLine(serviceOrderRecords,serviceOrder,accountLine); 
			      }}
			    }
			    }
			if(!isNew){
				if(serviceOrder.getIsNetworkRecord()){
				if(trackingStatus.getAccNetworkGroup())	{	
				String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
				if(!(networkSynchedId.equals(""))){
					synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
					synchornizeAccountLine(synchedAccountLine,accountLine); 
				}else{
					 if(trackingStatus.getAccNetworkGroup())	{
			             if(serviceOrder.getIsNetworkRecord()){
			              String partnertype="";
			           	  if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
			           		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
			           	  }
			           	  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (!(partnertype.equals("")))  && (accountLine.getRecInvoiceNumber() ==null || accountLine.getRecInvoiceNumber().toString().trim().equals(""))){
			           	  //int j=accountLineManager.updateNetworkSynchedId(accountLine.getId());  
			   				List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			   				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
			   				createAccountLine(serviceOrderRecords,serviceOrder,accountLine); 
			             }}
			           }
					
				}
				}
				}
			} 
			}
			
			
			if(billingDMMContractType){
				  if(isNew){
			          if(trackingStatus.getAccNetworkGroup())	{
			            if(serviceOrder.getIsNetworkRecord()){
			            
			            	boolean billtotype=false;
			             	  if(billing.getNetworkBillToCode()!=null && (!(billing.getNetworkBillToCode().toString().equals(""))) && accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals(""))) && accountLine.getNetworkBillToCode()!=null && (!(accountLine.getNetworkBillToCode().toString().equals("")))){
			             		
			             		  billtotype =accountLine.getBillToCode().trim().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().trim());
			             	  }
			            	if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (billtotype)){	
			            	 int j=accountLineManager.updateNetworkSynchedId(accountLine.getId());  
			  				List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			  				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
			  				createDMMAccountLine(serviceOrderRecords,serviceOrder,accountLine,billing); 
			            }}
			          }
			          }
				  if(!isNew){
			      	if(serviceOrder.getIsNetworkRecord()){
			       if(trackingStatus.getAccNetworkGroup())	{	
			      	String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
			      	if(!(networkSynchedId.equals(""))){
			      		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
			      		synchornizeDMMAccountLine(synchedAccountLine,accountLine,trackingStatus); 
			      	} else{
			      	   if(trackingStatus.getAccNetworkGroup())	{
			               if(serviceOrder.getIsNetworkRecord()){
			            	  
			            	   boolean billtotype=false;
			              	  if(billing.getNetworkBillToCode()!=null && (!(billing.getNetworkBillToCode().toString().equals("")))  && accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals(""))) && accountLine.getNetworkBillToCode()!=null && (!(accountLine.getNetworkBillToCode().toString().equals("")))){
			              		  //partnertype=  partnerManager.checkPartnerType(customerFile.getAccountCode());
			              		  billtotype =accountLine.getBillToCode().trim().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().trim());
			              	  }	   
			               	if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (billtotype)  && (accountLine.getRecInvoiceNumber() ==null || accountLine.getRecInvoiceNumber().toString().trim().equals(""))){	
			               	    int j=accountLineManager.updateNetworkSynchedId(accountLine.getId());  
			     				List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			     				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
			     				createDMMAccountLine(serviceOrderRecords,serviceOrder,accountLine,billing); 
			               }}
			             }
			      		
			      	}
			      	}else{ 
			          	
			      	}
			       if(accountLine.getNetworkSynchedId() !=null && billingDMMContractType && (trackingStatus.getSoNetworkGroup()!= null && ((trackingStatus.getSoNetworkGroup()))) && (trackingStatus.getAccNetworkGroup()== null || (!(trackingStatus.getAccNetworkGroup()))) && ((contractType)) && ((networkAgent)) && oldAccountLineBillToCode !=null && accountLine.getBillToCode()!=null && (!(oldAccountLineBillToCode.equalsIgnoreCase(accountLine.getBillToCode())))){
			    	   int i=billingManager.updateAccNetworkBilltocode(accountLine.getNetworkSynchedId().toString(),accountLine.getBillToCode(),accountLine.getBillToName(),oldAccountLineBillToCode,"");     
			       }
			      }
				  }
			}
			accountLineList = new ArrayList(serviceOrder.getAccountLines());
			getSession().setAttribute("accountLines", accountLineList);
			setFormStatus("0");
			String key = (isNew) ? "accountLine.added" : "accountLine.updated";  
			updateAccountLine(serviceOrder); 
      //   #7470 - International Driver Reversal
			
			if(automaticDriverReversal!=null && automaticDriverReversal.equals("YES")){
			  if((serviceOrder.getJob().equals("INT")) && (accountLine.getCategory().equals("Driver"))  && (accountLine.getActualExpense().compareTo(new BigDecimal("0.00"))!= 0) && (accountLine.getCreatedBy().indexOf("INTDRV")<0)) {
				  Long accId = accountLineManager.getDriverReversalLine("Driver",serviceOrder.getShipNumber(),accountLine.getId(),sessionCorpID); 
				  AccountLine driverAccObj ;
				  if(accId!=null && accId != 0L ){
					  driverAccObj = accountLineManager.get(accId);
					 
			    		 }else{
			    			 driverAccObj = new AccountLine(); 
			    			boolean activateAccPortal =true;
				    		try{
				    	    if(accountLineAccountPortalFlag){	
				    		activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
				    		driverAccObj.setActivateAccPortal(activateAccPortal);
				    	    }
				    		}catch(Exception e){
				    			 e.printStackTrace();
				    		}
			    			driverAccObj.setCreatedBy("INTDRV:"+getRequest().getRemoteUser());
			       		  	driverAccObj.setCreatedOn(new Date());
			       		  	driverAccObj.setServiceOrder(serviceOrder);
			       		  	driverAccObj.setShipNumber(serviceOrder.getShipNumber());
			       		  	driverAccObj.setServiceOrderId(serviceOrder.getId());
			       		  	driverAccObj.setCorpID(sessionCorpID);
			       		  	maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber());
			       		  	if (maxLineNumber.get(0) == null) {
							 accountLineNumber = "001";
			       		  	} else {
						     autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
				             if((autoLineNumber.toString()).length() == 2) {
				             	accountLineNumber = "0"+(autoLineNumber.toString());
				             }else if((autoLineNumber.toString()).length() == 1) {
				             	accountLineNumber = "00"+(autoLineNumber.toString());
				             } else {
				             	accountLineNumber=autoLineNumber.toString();
				             } 
						 }
			    	     driverAccObj.setAccountLineNumber(accountLineNumber);
			    	     driverAccObj.setInvoiceNumber(accountLine.getShipNumber().toString()+"-"+accountLine.getId().toString());
			    	     driverAccObj.setInvoiceDate(new Date());
			    	     
			    	     driverAccObj.setCategory("Driver");
			   		  
			   		  driverAccObj.setCompanyDivision(accountLine.getCompanyDivision());
			   		  driverAccObj.setSequenceNumber(serviceOrder.getSequenceNumber());
			   		 
			   		  driverAccObj.setVendorCode("T17917");
			   		  String vendorName="";
			   		  List listPartnerName = serviceOrderManager.getPartnerDetails("T17917",sessionCorpID);
						  if(!listPartnerName.isEmpty()){
								vendorName=listPartnerName.get(0).toString();
						  }
						  if(!vendorName.equalsIgnoreCase("")){
							  String [] vendorDetail = vendorName.split("~");
							  driverAccObj.setEstimateVendorName(vendorDetail[0]);
						  }else{
							 driverAccObj.setEstimateVendorName("");
						  }

					     driverAccObj.setDivision("02");
					     driverAccObj.setContract(accountLine.getContract());
					     driverAccObj.setChargeCode(accountLine.getChargeCode());
						try {
							String ss=chargesManager.findPopulateCostElementData(accountLine.getChargeCode(), accountLine.getContract(),sessionCorpID);
							if(!ss.equalsIgnoreCase("")){
								driverAccObj.setAccountLineCostElement(ss.split("~")[0]);
								driverAccObj.setAccountLineScostElementDescription(ss.split("~")[1]);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					     String desc =  accountLineManager.getChargeCodeDesc(accountLine.getChargeCode(),accountLine.getContract(),sessionCorpID); 
					     driverAccObj.setNote(desc);
					     driverAccObj.setPayingStatus("A");
					     driverAccObj.setStatus(true);
					     driverAccObj.setBasis("");
					     driverAccObj.setReceivedDate(new Date());
			    	}
				  // Set Actg code A/c to Vendor code
					 String actCode="";
					     String  companyDivisionAcctgCodeUnique1="";
					 	 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
					 	 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
					 		companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
					 	 }
					     if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
					 		actCode=partnerManager.getAccountCrossReference(driverAccObj.getVendorCode(),accountLine.getCompanyDivision(),sessionCorpID);
					 	 }else{
					 		actCode=partnerManager.getAccountCrossReferenceUnique(driverAccObj.getVendorCode(),sessionCorpID);
					 	 }
					     driverAccObj.setActgCode(actCode);
			      // Set Rec & Pay Gl code A/c to Charge code				     
			         List  agentGLList=accountLineManager.getGLList(driverAccObj.getChargeCode(),driverAccObj.getContract(),sessionCorpID);
						if(agentGLList!=null && (!(agentGLList.isEmpty())) && agentGLList.get(0)!=null){
							try{
								String[] GLarrayData=agentGLList.get(0).toString().split("#"); 
								String recGl=GLarrayData[0];
								String payGl=GLarrayData[1];
								if(!(recGl.equalsIgnoreCase("NO"))){
									driverAccObj.setRecGl(recGl);
								}
								if(!(payGl.equalsIgnoreCase("NO"))){
									driverAccObj.setPayGl(payGl);
								}						
							}catch(Exception e){e.printStackTrace();}
						}
					driverAccObj.setCompanyDivision(accountLine.getCompanyDivision());	
				    driverAccObj.setCountry(accountLine.getCountry());
					driverAccObj.setExchangeRate(accountLine.getExchangeRate());     		  
				    driverAccObj.setEstCurrency(accountLine.getEstCurrency());
					driverAccObj.setEstValueDate(accountLine.getEstValueDate());
					driverAccObj.setEstExchangeRate(accountLine.getEstExchangeRate());
					driverAccObj.setRevisionCurrency(accountLine.getRevisionCurrency());
					driverAccObj.setRevisionValueDate(accountLine.getRevisionValueDate());
					driverAccObj.setRevisionExchangeRate(accountLine.getRevisionExchangeRate()); 			    
				  	driverAccObj.setValueDate(new Date());
				  	BigDecimal actualExp = new BigDecimal("-1").multiply(accountLine.getActualExpense());
				  	driverAccObj.setActualExpense(actualExp);
				  	BigDecimal localAmt = new BigDecimal("-1").multiply(accountLine.getLocalAmount());
			    	driverAccObj.setLocalAmount(localAmt);
			    	baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
			    	driverAccObj.setUpdatedBy("INTDRV:"+getRequest().getRemoteUser());
			    	driverAccObj.setUpdatedOn(new Date());
			    	if(accountLine.getPayPostDate()!=null){
			    		driverAccObj.setPayPostDate(accountLine.getPayPostDate());
			    	}else{
			    		driverAccObj.setPayPostDate(new Date());
			    	}
			    	accountLineManager.save(driverAccObj);
			 }
			}        
			
      if(accountLine.getPayingStatus().equalsIgnoreCase("A")){
			Boolean autoPopPostDate=false;
			Date payDate1=null;
			List<SystemDefault> sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						autoPopPostDate=systemDefault.getAutoPayablePosting();
						payDate1=systemDefault.getPostDate1();
					}
				}
				if(autoPopPostDate){
					//change for posting date flexiblity Start	
					try{
						company=companyManager.findByCorpID(sessionCorpID).get(0);
			    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
				    		Date dt1=new Date();
				    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				    		String dt11=sdfDestination.format(dt1);
				    		dt1=sdfDestination.parse(dt11);
				    		Date dt2 =null;
				    		try{
				    		dt2 = new Date();
				    		}catch(Exception e){
				    			 e.printStackTrace();
				    		}				    		
				    		
				    		dt2=payDate1;
				    		if(dt2!=null){
					    		if(dt1.compareTo(dt2)>0){
					    			payDate1=dt2;
					    		}else{
					    			payDate1=dt1;
					    		}
				    		}
			    		}
				}catch(Exception e){e.printStackTrace();}	
				}
			if(autoPopPostDate && payDate1!=null ){
			serviceOrder=serviceOrderManager.get(sid);
			String jobName="";
			  List tempCommissionJob = serviceOrderManager.getCommissionableJobs(sessionCorpID);
			    if(tempCommissionJob!=null && !tempCommissionJob.isEmpty() && tempCommissionJob.get(0)!= null){
			    	jobName = tempCommissionJob.get(0).toString();
			    }
			    if(jobName.contains(serviceOrder.getJob()) ){
			    	try{
			    	ServiceOrderAction soAction = new ServiceOrderAction();
					soAction.setSid(serviceOrder.getId());
					soAction.setServiceOrderManager(serviceOrderManager);
					soAction.setBillingManager(billingManager);
					soAction.setAccountLineManager(accountLineManager);
					soAction.setCompanyManager(companyManager);
					soAction.setRefMasterManager(refMasterManager);
					soAction.setPartnerManager(partnerManager);
					soAction.setCommission(commission);
					soAction.setCommissionManager(commissionManager);
					soAction.setChargesManager(chargesManager);
			    	soAction.calculateCommisssionMethod();
					//calculateCommisssionMethod();
			    	}catch(Exception e){e.printStackTrace();}
			    } 
			}
			}      
      
		/*if(isNew){
			//accountLineManager.updateSOExtFalg(serviceOrder.getId());
		}else if(!isNew){
			if(isSOExtract!=null && isSOExtract.equals("yes")){
			//accountLineManager.updateSOExtFalg(serviceOrder.getId());
			}
		}*/
      logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
      if("category".equals(vanLineAccountView)){
			   vanLineAccCategoryURL = "?sid="+serviceOrder.getId()+"&vanLineAccountView="+vanLineAccountView;
			   return "category"; 
      }
			return SUCCESS; 
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
    }
	public String vanLineAccCategoryURL;
	
	//*********************************************** commission calculation **********************************
	
	public String isPayableRequired;
	public List accListBycompDiv;
	public String commLine1Id;
     public String commLine2Id;
 	Map <String,String > offGridSelected=new HashMap<String, String>();
     
     
	@SkipValidation
	public String calculateCommisssionMethod(){
		try {
			int invCount =0;
			serviceOrder = serviceOrderManager.get(sid);
			trackingStatus=trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid);
			Map<String,String> commissionMap=new LinkedHashMap<String, String>();
			String aidList="";
			if((serviceOrder.getJob()!=null)&&(serviceOrder.getJob().trim().equalsIgnoreCase("INT"))){
				commissionMap=serviceOrderManager.checkShipEntryInCommission(sessionCorpID,"COMMISSION",serviceOrder.getShipNumber());
				String aidListTemp=serviceOrderManager.findAccIdNeedToBlock(sid,sessionCorpID);
				for(String str:aidListTemp.split(",")){
					if(commissionMap.containsKey(str)){
						if(aidList.equalsIgnoreCase("")){
							aidList="'"+str+"'";
						}else{
							aidList=aidList+",'"+str+"'";
						}
					}
				}
			}
			
			Map <String,BigDecimal > compDivComm  = new   HashMap<String,BigDecimal >();
			compDivComm.put(serviceOrder.getCompanyDivision(),new BigDecimal("0.00"));
			Map <String,BigDecimal > compDivRev = new   HashMap<String,BigDecimal >();
			compDivRev.put(serviceOrder.getCompanyDivision(),new BigDecimal("0.00"));
			Map <String,String > compDivAccId = new   HashMap<String,String >();
			compDivAccId.put(serviceOrder.getCompanyDivision(),"");
			billing = billingManager.get(sid);
			baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
			
			List accountIdList = serviceOrderManager.findAccountlines(sid);
			if((commissionMap!=null)&&(!commissionMap.isEmpty())){
				if(accountIdList!=null && !accountIdList.isEmpty()){
					List al=new ArrayList();
					Iterator itr1 =accountIdList.iterator();
					while(itr1.hasNext()){
						String s=itr1.next()+"".trim().toString();
						if(!commissionMap.containsKey(s)){
							al.add(s);
						}
					}
					accountIdList.clear();
					accountIdList.addAll(al);
				}						
			}			
			
			if(accountIdList!=null && !accountIdList.isEmpty()){
				Iterator itr1 =accountIdList.iterator();
			while(itr1.hasNext()){
				accountLine = accountLineManager.get(Long.parseLong(itr1.next().toString()));
				if(accountLine.getRecInvoiceNumber()!=null && !(accountLine.getRecInvoiceNumber().toString().trim().equals(""))){
					invCount++;
					break;
				}
			}
			}
			if(invCount > 0){
			List compDivList = serviceOrderManager.findAccountLineCompDiv(sid,sessionCorpID);
			
			if(compDivList!=null && !compDivList.isEmpty()){
				BigDecimal totalOffRev1 = new BigDecimal(0);
				BigDecimal totalOffExp1 = new BigDecimal(0);
				offGridSelected = new HashMap<String,String>();
				
			Iterator itrCompDiv =compDivList.iterator();
				while(itrCompDiv.hasNext()){
						BigDecimal temp =  new BigDecimal("00.00");
						String tempCompDiv = itrCompDiv.next().toString();
						compDivComm.put(tempCompDiv, temp);
						compDivAccId.put(tempCompDiv,"");
					}
			
			BigDecimal rev1 = new BigDecimal(0);
			BigDecimal exp1 = new BigDecimal(0);
			String chargeCode1 = "";
			
			List chargeTpDetailList1 = serviceOrderManager.getTPChargeDetail(sid,serviceOrder.getJob(),"","",sessionCorpID,aidList);  
			Iterator it1 = chargeTpDetailList1.iterator();
			while (it1.hasNext()) {
				Object chargeItr1 = (Object) it1.next();
				if (((ChargeTpDetailDTO) chargeItr1).getRevenue() != null && (!((ChargeTpDetailDTO) chargeItr1).getRevenue().toString().trim().equals(""))) {
					rev1 = new BigDecimal(((ChargeTpDetailDTO) chargeItr1).getRevenue().toString());
				}
				if (((ChargeTpDetailDTO) chargeItr1).getExpense() != null && (!((ChargeTpDetailDTO) chargeItr1).getExpense().toString().trim().equals(""))) {
					exp1 = new BigDecimal(((ChargeTpDetailDTO) chargeItr1).getExpense().toString());
				}
				if (((ChargeTpDetailDTO) chargeItr1).getCharge() != null && (!((ChargeTpDetailDTO) chargeItr1).getCharge().toString().trim().equals(""))) {
					chargeCode1 = ((ChargeTpDetailDTO) chargeItr1).getCharge().toString();
				}
				
				// logic for reverse invoice
				List <AccountLine> reverseInvoiceIdsList = serviceOrderManager.getReverseInvoiceIds(chargeCode1,sessionCorpID,sid,aidList); 
				if(reverseInvoiceIdsList!=null && !reverseInvoiceIdsList.isEmpty()){
				String tempId1="";
				for(AccountLine accObj1 : reverseInvoiceIdsList){
					if(tempId1.equals(""))
						tempId1 = accObj1.getId().toString();
					else
						tempId1 = tempId1+","+accObj1.getId().toString();
				}
				List<AccountLine> accidList = serviceOrderManager.getAllTPAccId(chargeCode1,sessionCorpID,sid,aidList);
				String  compDiv ="";
					//String tempId = "";
					for(AccountLine accObj : accidList){
						if(accObj.getActualRevenue().compareTo(new BigDecimal(0))!=0){
							if(compDiv.equalsIgnoreCase("")){
								compDiv = accObj.getCompanyDivision();
							}else if(!compDiv.equalsIgnoreCase(accObj.getCompanyDivision())){
								 compDiv = serviceOrder.getCompanyDivision();
								break;
							}else{
								
							}
						}
					
					}
					String tempAccid = compDivAccId.get(compDiv).toString();
					
					if(tempAccid.equalsIgnoreCase(""))
						tempAccid =  tempId1 ;
					else
						tempAccid = tempAccid + "," +tempId1;
					
					compDivAccId.put(compDiv,tempAccid);
					
			}
			
			if(rev1.compareTo(new BigDecimal(0))!= 0 && exp1.compareTo(new BigDecimal(0))!= 0){
				itrCompDiv =compDivList.iterator();
				
				while(itrCompDiv.hasNext()){
				
					BigDecimal totalActualCommRevenue = new BigDecimal(0);
					BigDecimal totalActualCommExpence = new BigDecimal(0);
					BigDecimal commissionableProfitPer = new BigDecimal(0);
					BigDecimal commissionableProfitPer1 = new BigDecimal(0);
					BigDecimal commissionableRatePer = new BigDecimal(0);
					BigDecimal commissionForTP = new BigDecimal(0);
					int countRec = 0;
					int countPay = 0;
					String compDiv ="";
					compDiv =itrCompDiv.next().toString();
						
			List chargeTpDetailList = serviceOrderManager.getTPChargeDetail(sid,serviceOrder.getJob(),compDiv,chargeCode1,sessionCorpID,aidList);  
			Iterator it = chargeTpDetailList.iterator();
			while (it.hasNext()) {
			List<AccountLine> accidList = serviceOrderManager.getAllTPAccId(chargeCode1,sessionCorpID,sid,aidList);
				 //compDiv ="";
				String tempId = "";
				
				if(compDiv.equalsIgnoreCase("")){
					compDiv=serviceOrder.getCompanyDivision();
				}
				
				
				 totalOffRev1 = new BigDecimal(0);
				 totalOffExp1 = new BigDecimal(0);
				
				for(AccountLine accObj : accidList){
					if(tempId.equals("")){
						tempId = accObj.getId().toString();
					}else{
						tempId = tempId+","+accObj.getId().toString();
					}
					totalOffRev1=totalOffRev1.add(accObj.getActualRevenue());
					totalOffExp1=totalOffExp1.add((accObj.getActualExpense()!=null && accObj.getActualExpense().doubleValue()!=0)?accObj.getActualExpense():accObj.getRevisionExpense());
				}
				
				Double OffRevExDif=totalOffRev1.doubleValue()-totalOffExp1.doubleValue();
				if(totalOffRev1.doubleValue()!=0){
					OffRevExDif=(OffRevExDif/totalOffRev1.doubleValue())*100;	
				}else{
					OffRevExDif=0.00;
				}
				
				BigDecimal temp = new BigDecimal(0);
				String tempAccid = compDivAccId.get(compDiv).toString();
				temp = (BigDecimal) compDivComm.get(compDiv);
				Object chargeItr = (Object) it.next();
				BigDecimal rev = new BigDecimal(0);
				BigDecimal exp = new BigDecimal(0);
				BigDecimal commission = new BigDecimal(0);
				String ChargeCode = "";
				if (((ChargeTpDetailDTO) chargeItr).getRevenue() != null && (!((ChargeTpDetailDTO) chargeItr).getRevenue().toString().trim().equals(""))) {
					rev = new BigDecimal(((ChargeTpDetailDTO) chargeItr).getRevenue().toString());
				}
				if (((ChargeTpDetailDTO) chargeItr).getExpense() != null && (!((ChargeTpDetailDTO) chargeItr).getExpense().toString().trim().equals(""))) {
					exp = new BigDecimal(((ChargeTpDetailDTO) chargeItr).getExpense().toString());
				}
				if (((ChargeTpDetailDTO) chargeItr).getCommission() != null && (!((ChargeTpDetailDTO) chargeItr).getCommission().toString().trim().equals(""))) {
					commission = new BigDecimal(((ChargeTpDetailDTO) chargeItr).getCommission().toString());
				}
				if (((ChargeTpDetailDTO) chargeItr).getCharge() != null && (!((ChargeTpDetailDTO) chargeItr).getCharge().toString().trim().equals(""))) {
					ChargeCode = ((ChargeTpDetailDTO) chargeItr).getCharge().toString();
				}
				
				try{
					BigDecimal tempComm = new BigDecimal(0);
					if(serviceOrder.getJob()!=null && !serviceOrder.getJob().equalsIgnoreCase("OFF")){
					tempComm = rev.subtract(exp);
					tempComm = tempComm.multiply(commission);
					tempComm= tempComm.divide(new BigDecimal(100),4);
					}
					try{
						 //11050 - TP commission for OFF for jobs  Start
						if(serviceOrder.getJob()!=null && serviceOrder.getJob().equalsIgnoreCase("OFF")){
							String param="'OFFCOMMISSION'";
							List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(sessionCorpID,param);
							for (RefMasterDTO refObj : allParamValue) {
								if(OffRevExDif>=Double.parseDouble(refObj.getFlex1())&&OffRevExDif<=Double.parseDouble(refObj.getFlex2())){
									if(Double.parseDouble(refObj.getBucket2())!=0){
										if(refObj.getDescription()!=null && refObj.getDescription().equalsIgnoreCase("Revenue")){
											tempComm=totalOffRev1.multiply(new BigDecimal(refObj.getBucket2()));
											tempComm= tempComm.divide(new BigDecimal(100),4);
											offGridSelected.put(ChargeCode, "Revenue"+"~"+refObj.getBucket2());
										}else if(refObj.getDescription()!=null && refObj.getDescription().equalsIgnoreCase("Margin")){
											tempComm=totalOffRev1.subtract(totalOffExp1);
											tempComm=tempComm.multiply(new BigDecimal(refObj.getBucket2()));
											tempComm= tempComm.divide(new BigDecimal(100),4);
											offGridSelected.put(ChargeCode, "Margin"+"~"+refObj.getBucket2());
										}
									}else{
										tempComm = new BigDecimal(0);
									}
								}							
							}
						}
						//11050 - TP commission for OFF for jobs  End
					}catch(Exception e){e.printStackTrace();}
					commissionForTP = commissionForTP.add(tempComm); 
					temp = temp.add(commissionForTP);
					compDivComm.put(compDiv, temp);
					
					if(tempAccid.equalsIgnoreCase(""))
						tempAccid =  tempId ;
					else
						tempAccid = tempAccid + "," +tempId;
					
					compDivAccId.put(compDiv,tempAccid);
					
					serviceOrderManager.updateSetteledDateOfToCharge(ChargeCode,sid); 
				
				}catch(Exception e){
					 e.printStackTrace();
				}
				
			
			}
			}
			}

			}
			
						itrCompDiv =compDivList.iterator();
						while(itrCompDiv.hasNext()){
							
							BigDecimal totalActualCommRevenue = new BigDecimal(0);
							BigDecimal totalActualCommExpence = new BigDecimal(0);
							BigDecimal commissionableProfitPer = new BigDecimal(0);
							BigDecimal commissionableProfitPer1 = new BigDecimal(0);
							BigDecimal commissionableRatePer = new BigDecimal(0);
							BigDecimal commissionForTP = new BigDecimal(0);
							int countRec = 0;
							int countPay = 0;
							String compDiv ="";
							commLine1Id="";
							commLine2Id="";
							compDiv =itrCompDiv.next().toString();
							
							
							accListBycompDiv = null;
							accListBycompDiv = serviceOrderManager.findAccountLineListByCompDiv(sid,sessionCorpID,compDiv,aidList);
							if(accListBycompDiv!=null && !accListBycompDiv.isEmpty()){
								Iterator itr = accListBycompDiv.iterator(); 
					
						while(itr.hasNext()){
							accountLine = accountLineManager.get(Long.parseLong(itr.next().toString()));
							String contact = accountLine.getContract();
							String chargeCode = accountLine.getChargeCode();
							List tempList =  serviceOrderManager.getCommissionableValue(contact,chargeCode,sessionCorpID,serviceOrder.getJob()); 
							if(tempList != null && !(tempList.isEmpty())){
								charges = (Charges) tempList.get(0);
							}else{
								charges = null ;
							}
							if(charges != null && charges.getCommissionable() == true){
								if(charges.getCommission()!=null && !(charges.getCommission().equalsIgnoreCase("")) ){
									if(!charges.getBucket().toString().trim().equalsIgnoreCase("TP") ){
										BigDecimal tempComm = new BigDecimal(0);
										BigDecimal temp = new BigDecimal(0);
										temp = (BigDecimal) compDivComm.get(compDiv);
										tempComm = accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense():accountLine.getRevisionExpense()));
										try{
											double tempDoubleVal = tempComm.doubleValue();
											double tempCommVal =  Double.parseDouble(charges.getCommission());
											tempDoubleVal = ((tempDoubleVal*100)*(tempCommVal/100))/100;
											tempComm = new BigDecimal(tempDoubleVal);
										}catch(Exception e){
											e.printStackTrace();
										}
									
										temp = temp.add(tempComm);
										compDivComm.put(compDiv,temp);
										accountLine.setSettledDate(new Date());
										accountLine = accountLineManager.save(accountLine);
										
										String tempId = compDivAccId.get(compDiv).toString();
										if(tempId.equalsIgnoreCase(""))
											tempId = accountLine.getId().toString();
										else
											tempId = tempId+","+accountLine.getId().toString();
										
										compDivAccId.put(compDiv,tempId);
									}
								}else{
									BigDecimal temp = new BigDecimal(0);
									temp = (BigDecimal) compDivComm.get(compDiv);
									String chargeCodeCom1 = "SACOM"+serviceOrder.getJob();
									List contractNameList = serviceOrderManager.getInternalCostContract(compDiv,sessionCorpID,serviceOrder.getJob(),chargeCodeCom1); 
									String contractName="";
									if(contractNameList!=null && !(contractNameList.isEmpty())){
										contractName = contractNameList.get(0).toString();
									}
									
									if(accountLine.getActualRevenue().compareTo(new BigDecimal(0))!= 0 ){
										commissionableProfitPer=accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense():accountLine.getRevisionExpense()));
										commissionableProfitPer = commissionableProfitPer.divide(accountLine.getActualRevenue(),3);
										commissionableProfitPer = commissionableProfitPer.multiply(new BigDecimal(100));
										List <RateGrid> rateList =  serviceOrderManager.getRateGirdForComm(chargeCodeCom1,contractName,sessionCorpID);
										if((rateList!=null)&&(!rateList.isEmpty()))	{
								    		for (RateGrid rateGrid : rateList) {
								    			if(commissionableProfitPer.compareTo(new BigDecimal(rateGrid.getQuantity1()))== 0 || commissionableProfitPer.compareTo(new BigDecimal(rateGrid.getQuantity1()))==-1 ){
								    				commPercentage = rateGrid.getRate1().toString();
								    				break;
								    			}
								    		}
								    	}
										BigDecimal tempComm = new BigDecimal(0.00);
										try{
											 //11050 - TP commission for OFF for jobs  Start
											if(serviceOrder.getJob()!=null && serviceOrder.getJob().equalsIgnoreCase("OFF") && charges.getBucket().toString().trim().equalsIgnoreCase("TP") ){/*
												BigDecimal marginValue = new BigDecimal(0);
												BigDecimal marginPersentageValue = new BigDecimal(0);
												marginValue=accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense().setScale(4,BigDecimal.ROUND_HALF_UP):accountLine.getRevisionExpense().setScale(4,BigDecimal.ROUND_HALF_UP)));
												marginPersentageValue=marginValue.divide(accountLine.getActualRevenue(),4);
												marginPersentageValue=marginPersentageValue.multiply(new BigDecimal(100));
												String param="'OFFCOMMISSION'";
												List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(sessionCorpID,param);
												for (RefMasterDTO refObj : allParamValue) {
													if(marginPersentageValue.doubleValue()>=Double.parseDouble(refObj.getFlex1())&&marginPersentageValue.doubleValue()<=Double.parseDouble(refObj.getFlex2())){
														if(Double.parseDouble(refObj.getBucket2())!=0){
															if(refObj.getDescription()!=null && refObj.getDescription().equalsIgnoreCase("Revenue")){
																tempComm=accountLine.getActualRevenue().multiply(new BigDecimal(refObj.getBucket2()));
																tempComm= tempComm.divide(new BigDecimal(100),4);										
															}else if(refObj.getDescription()!=null && refObj.getDescription().equalsIgnoreCase("Margin")){
																tempComm=accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense().setScale(4,BigDecimal.ROUND_HALF_UP):accountLine.getRevisionExpense().setScale(4,BigDecimal.ROUND_HALF_UP)));
																tempComm=tempComm.multiply(new BigDecimal(refObj.getBucket2()));
																tempComm= tempComm.divide(new BigDecimal(100),4);										
															}
														}else{
															tempComm = new BigDecimal(0);
														}
													}							
												}
											
											//11050 - TP commission for OFF for jobs  End
											
										*/}else{
										if(commPercentage!=null && (new BigDecimal(commPercentage)).compareTo(new BigDecimal(0))!=0){
											try{
												double tempDoubleVal = accountLine.getActualRevenue().doubleValue();
												double tempCommVal =  Double.parseDouble(commPercentage) ;
												tempDoubleVal = ((tempDoubleVal*100)*(tempCommVal/100))/100;
												tempComm = new BigDecimal(tempDoubleVal);
											}catch(Exception e){
												e.printStackTrace();
											}
										}
										}
										}catch(Exception e){e.printStackTrace();}
											temp = temp.add(tempComm);
											compDivComm.put(compDiv,temp);
											accountLine.setSettledDate(new Date());
											accountLine = accountLineManager.save(accountLine);
										
										totalActualCommRevenue = totalActualCommRevenue.add(accountLine.getActualRevenue());
										compDivRev.put(compDiv, totalActualCommRevenue);
										

										String tempId = compDivAccId.get(compDiv).toString();
										if(tempId.equalsIgnoreCase(""))
											tempId = accountLine.getId().toString();
										else
											tempId = tempId+","+accountLine.getId().toString();
										
										compDivAccId.put(compDiv,tempId);
									}
							
									accountLine.setSettledDate(new Date());
									accountLine = accountLineManager.save(accountLine);
								}
								
							}
						}
					}	
				}
		 
			}	
			
			
			
			if(compDivComm!=null && !compDivComm.isEmpty()){
				for (Map.Entry<String, BigDecimal> entry : compDivComm.entrySet()) {
					commLine1Id="";
					commLine2Id="";
				String vendorCode1="";
				String vendorName1="";
				String compDiv = entry.getKey().toString();
				List listUser1 = serviceOrderManager.getParentAgentCodeFormUser(serviceOrder.getSalesMan());
				
							
				if(listUser1!=null && !listUser1.isEmpty()){
					vendorCode1 = listUser1.get(0).toString();
				}
					if(vendorCode1!=null && !(vendorCode1.trim().equals(""))){
						AccountLine commAccountLine=null;
						String chargeCodeCom1 = "SACOM"+serviceOrder.getJob();
						List accIdList = serviceOrderManager.findAccountLineByCharge(chargeCodeCom1,sessionCorpID,serviceOrder.getShipNumber(),compDiv);
						if(accIdList!=null && !(accIdList.isEmpty()) ){
							String tempid= accIdList.get(0).toString();
							 commAccountLine =accountLineManager.get(Long.parseLong(tempid));
							 commAccountLine.setAccountLineNumber(commAccountLine.getAccountLineNumber());
							 accountLineNumber=commAccountLine.getAccountLineNumber();
						}else{
							
							
							 commAccountLine = new AccountLine();
							 boolean activateAccPortal =true;
					    		try{
					    		if(accountLineAccountPortalFlag){	
					    		activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
					    		commAccountLine.setActivateAccPortal(activateAccPortal);
					    		}
					    		}catch(Exception e){
					    			e.printStackTrace();
					    		}
							commAccountLine.setCreatedBy("COMM:"+getRequest().getRemoteUser());
							commAccountLine.setCreatedOn(new Date());
							maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber());
							if (maxLineNumber.get(0) == null) {
								accountLineNumber = "001";
							} else {
				             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
				                 if((autoLineNumber.toString()).length() == 2) {
				                 	accountLineNumber = "0"+(autoLineNumber.toString());
				                 }else if((autoLineNumber.toString()).length() == 1) {
				                 	accountLineNumber = "00"+(autoLineNumber.toString());
				                 } else {
				                 	accountLineNumber=autoLineNumber.toString();
				                 } 
							}
							commAccountLine.setAccountLineNumber(accountLineNumber);
							  ///Code for AccountLine division 
								String divisionTemp="";
								if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
								}
								if(!divisionTemp.equalsIgnoreCase("")){
									commAccountLine.setDivision(divisionTemp);
								}else{
									commAccountLine.setDivision(null);
								}
							///Code for AccountLine division 			
						}
						BigDecimal totalCommisionExp1 = new BigDecimal(0);
						try{
						 totalCommisionExp1 = serviceOrderManager.findTotalExpenseOfChargeACC(chargeCodeCom1,sessionCorpID,serviceOrder.getShipNumber(),compDiv);  
						}catch(Exception e){e.printStackTrace();}
						commAccountLine.setUpdatedBy("COMM:"+getRequest().getRemoteUser());
						commAccountLine.setUpdatedOn(new Date());
						commAccountLine.setServiceOrderId(sid);
						commAccountLine.setCorpID(sessionCorpID);
						commAccountLine.setBasis("");
						//commAccountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
						commAccountLine.setCompanyDivision(compDiv);
						commAccountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
						commAccountLine.setShipNumber(serviceOrder.getShipNumber());
						commAccountLine.setCategory("Broker");
						commAccountLine.setNote("Commission");
						commAccountLine.setSettledDate(new Date());
						List contractNameList = serviceOrderManager.getInternalCostContract(compDiv,sessionCorpID,serviceOrder.getJob(),chargeCodeCom1); 
						String contractName="";
						if(contractNameList!=null && !(contractNameList.isEmpty())){
							contractName = contractNameList.get(0).toString();
						}
						commAccountLine.setContract(contractName);
						commAccountLine.setChargeCode(chargeCodeCom1);
						try {
							String ss=chargesManager.findPopulateCostElementData(chargeCodeCom1, contractName,sessionCorpID);
							if(!ss.equalsIgnoreCase("")){
								commAccountLine.setAccountLineCostElement(ss.split("~")[0]);
								commAccountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
							}
						} catch (Exception e1) {
							e1.printStackTrace();
						}

						
						String payRecGl = serviceOrderManager.findPayRecgl(chargeCodeCom1,sessionCorpID,contractName);
				    	String recGl="";
				    	String payGl="";
						if(payRecGl!=null && !(payRecGl.equalsIgnoreCase("")))	{
				    		String [] glcode = payRecGl.split("~");
				    		recGl=glcode[0];
				    		payGl=glcode[1];
				    	}
						commAccountLine.setRecGl(recGl);
						commAccountLine.setPayGl(payGl);
						String accountLineNumberU="";
						accountLineNumberU=accountLineNumber;
						accountLineNumberU=accountLineNumberU.trim();
						while(accountLineNumberU.indexOf("0")==0){
							accountLineNumberU=accountLineNumberU.replace("0", "");	
						}
						accountLineNumberU=accountLineNumberU.trim();
						String invoiceNumber="COMM"+accountLineNumberU.trim()+sid;
						commAccountLine.setInvoiceNumber(invoiceNumber);
						commAccountLine.setInvoiceDate(new Date());
						commAccountLine.setValueDate(new Date());
						commAccountLine.setPayingStatus("A");
						commAccountLine.setServiceOrder(serviceOrder);
						commAccountLine.setCountry(baseCurrency);
						commAccountLine.setStatus(true);
						
						
						BigDecimal comm1ActualExp = new BigDecimal(0);
						if(!aidList.equalsIgnoreCase("")){
							BigDecimal tempamt = commAccountLine.getActualExpense()!=null && !commAccountLine.getActualExpense().toString().equals("")? commAccountLine.getActualExpense() : new BigDecimal("0.00");
							comm1ActualExp =new BigDecimal( entry.getValue().toString());
							comm1ActualExp = comm1ActualExp.add(tempamt);
						}else{
							comm1ActualExp =new BigDecimal( entry.getValue().toString());
							comm1ActualExp = comm1ActualExp.subtract(totalCommisionExp1); 
						}
						totalCommisionExp1 = new BigDecimal(0);
						commAccountLine.setActualExpense(comm1ActualExp);
						commAccountLine.setLocalAmount(comm1ActualExp);
						
						
						
						commAccountLine.setVendorCode(vendorCode1);
						
						if(!vendorCode1.equalsIgnoreCase("")){
							List listPartnerName = serviceOrderManager.getPartnerDetails(vendorCode1,sessionCorpID);
							if(!listPartnerName.isEmpty()){
								vendorName1=listPartnerName.get(0).toString();
							}
						}
						if(!vendorName1.equalsIgnoreCase("")){
							String [] vendorDetail = vendorName1.split("~");
							commAccountLine.setEstimateVendorName(vendorDetail[0]);
						}else{
							commAccountLine.setEstimateVendorName("");
						}
						 String actCode="";
				          String  companyDivisionAcctgCodeUnique1="";
				 		 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
				 		 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
				 			 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
				 		 }
				         if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
				 			actCode=partnerManager.getAccountCrossReference(vendorCode1,compDiv,sessionCorpID);
				 		}else{
				 			actCode=partnerManager.getAccountCrossReferenceUnique(vendorCode1,sessionCorpID);
				 		}
				         commAccountLine.setActgCode(actCode);
				     	
							try{
								company=companyManager.findByCorpID(sessionCorpID).get(0);
					    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
						    		Date dt1 =null;
						    		try{
						    		dt1 = new Date();
						    		}catch(Exception e){
						    			 e.printStackTrace();
						    		}					    		
						    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						    		String dt11=sdfDestination.format(dt1);
						    		dt1=sdfDestination.parse(dt11);

						    		Date dt2 =null;
						    		try{
						    		dt2 = new Date();
						    		}catch(Exception e){
						    			 e.printStackTrace();
						    		}
						    		
						    		List systemDefaultList = serviceOrderManager.findSysDefault(sessionCorpID);
						        	for (Object object : systemDefaultList) {
										SystemDefault systemDefault = (SystemDefault)object;
										dt2=systemDefault.getPostDate1();
									}
						    		if(dt1.compareTo(dt2)>0){
						    			commAccountLine.setPayPostDate(dt2);
						    		}else{
						    			commAccountLine.setPayPostDate(dt1);
						    		}
					    		}else{
						        	List systemDefaultList = serviceOrderManager.findSysDefault(sessionCorpID);
						        	for (Object object : systemDefaultList) {
										SystemDefault systemDefault = (SystemDefault)object;
										commAccountLine.setPayPostDate(systemDefault.getPostDate1());
									}
					    		}
						}catch(Exception e){
				        	List systemDefaultList = serviceOrderManager.findSysDefault(sessionCorpID);
				        	for (Object object : systemDefaultList) {
								SystemDefault systemDefault = (SystemDefault)object;
								commAccountLine.setPayPostDate(systemDefault.getPostDate1());
							}
						}			
						        	
				
				         if(contractNameList!=null && !(contractNameList.isEmpty()) && comm1ActualExp.compareTo(new BigDecimal(0))!=0 ){
				        	// commLine1Id = commAccountLine.getId().toString();
				        	 commAccountLine = accountLineManager.save(commAccountLine);
				        	 commLine1Id = commAccountLine.getId().toString();
				         }
				      
				}else{
					String chargeCodeCom1 = "SACOM"+serviceOrder.getJob();
					List accIdList = serviceOrderManager.findAccountLineByCharge(chargeCodeCom1,sessionCorpID,serviceOrder.getShipNumber(),compDiv);
					if(accIdList!=null && !(accIdList.isEmpty())){
						String tempid= accIdList.get(0).toString();
						AccountLine commAccountLine =accountLineManager.get(Long.parseLong(tempid));
						commAccountLine.setStatus(false);
						 accountLineManager.save(commAccountLine);
					}
				}
					
							String vendorCode="";
							String vendorName="";
							List listUser= serviceOrderManager.getParentAgentCodeFormUser(serviceOrder.getEstimator()); 
							if(!listUser.isEmpty()){
								vendorCode = listUser.get(0).toString();
							}
							
								if(vendorCode!=null && !(vendorCode.trim().equals(""))){
									BigDecimal totalCommisionExp1 = new BigDecimal(0);
									try{
									 totalCommisionExp1 = serviceOrderManager.findTotalExpenseOfChargeACC("SACOM2",sessionCorpID,serviceOrder.getShipNumber(),compDiv); 
									}catch(Exception e){ e.printStackTrace();         }
								AccountLine commAccountLine1=null;
								List accIdList1 = serviceOrderManager.findAccountLineByCharge("SACOM2",sessionCorpID,serviceOrder.getShipNumber(),compDiv);
								if(accIdList1!=null && !(accIdList1.isEmpty())){
									String tempid= accIdList1.get(0).toString();
									commAccountLine1 =accountLineManager.get(Long.parseLong(tempid));
									commAccountLine1.setAccountLineNumber(commAccountLine1.getAccountLineNumber());
									accountLineNumber=commAccountLine1.getAccountLineNumber();
								}else{
									commAccountLine1 = new AccountLine();
									boolean activateAccPortal =true;
						    		try{
						    		if(accountLineAccountPortalFlag){	
						    		activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
						    		commAccountLine1.setActivateAccPortal(activateAccPortal);
						    		}
						    		}catch(Exception e){
						    			e.printStackTrace();
						    		}
									commAccountLine1.setCreatedBy("COMM:"+getRequest().getRemoteUser());
									commAccountLine1.setCreatedOn(new Date());
									maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber());
									if (maxLineNumber.get(0) == null) {
										accountLineNumber = "001";
									} else {
						             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
						                 if((autoLineNumber.toString()).length() == 2) {
						                 	accountLineNumber = "0"+(autoLineNumber.toString());
						                 }else if((autoLineNumber.toString()).length() == 1) {
						                 	accountLineNumber = "00"+(autoLineNumber.toString());
						                 } else {
						                 	accountLineNumber=autoLineNumber.toString();
						                 } 
									}
									commAccountLine1.setAccountLineNumber(accountLineNumber);
									  ///Code for AccountLine division 
									String divisionTemp="";
									if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
									}
									if(!divisionTemp.equalsIgnoreCase("")){
										commAccountLine1.setDivision(divisionTemp);
									}else{
										commAccountLine1.setDivision(null);
									}
									  ///Code for AccountLine division 
								}
								
								commAccountLine1.setUpdatedBy("COMM:"+getRequest().getRemoteUser());
								commAccountLine1.setUpdatedOn(new Date());
								commAccountLine1.setBasis("");
								commAccountLine1.setCorpID(sessionCorpID);
								commAccountLine1.setServiceOrder(serviceOrder);
								commAccountLine1.setServiceOrderId(sid);
								commAccountLine1.setSequenceNumber(serviceOrder.getSequenceNumber());
								commAccountLine1.setShipNumber(serviceOrder.getShipNumber());
								commAccountLine1.setCategory("Broker");
								commAccountLine1.setChargeCode("SACOM2");
								try {
									String ss=chargesManager.findPopulateCostElementData("SACOM2", billing.getContract(),sessionCorpID);
									if(!ss.equalsIgnoreCase("")){
										commAccountLine1.setAccountLineCostElement(ss.split("~")[0]);
										commAccountLine1.setAccountLineScostElementDescription(ss.split("~")[1]);
									}
								} catch (Exception e1) {
									e1.printStackTrace();
								}
								
								commAccountLine1.setNote("Commission");
								commAccountLine1.setSettledDate(new Date());
								String accountLineNumberU2="";
								accountLineNumberU2=accountLineNumber;
								accountLineNumberU2=accountLineNumberU2.trim();
								while(accountLineNumberU2.indexOf("0")==0){
									accountLineNumberU2=accountLineNumberU2.replace("0", "");	
								}
								accountLineNumberU2=accountLineNumberU2.trim(); 
								String invoiceNumber2="COMM"+accountLineNumberU2.trim()+sid;
								commAccountLine1.setInvoiceNumber(invoiceNumber2); 
								commAccountLine1.setInvoiceDate(new Date());
								commAccountLine1.setValueDate(new Date());
								commAccountLine1.setPayingStatus("A");
								commAccountLine1.setCountry(baseCurrency);
								//commAccountLine1.setCompanyDivision(serviceOrder.getCompanyDivision());
								commAccountLine1.setCompanyDivision(compDiv);
								
								String payRecGl1 = serviceOrderManager.findPayRecgl("SACOM2",sessionCorpID,billing.getContract());
						    	String recGl1="";
						    	String payGl1="";
								if(payRecGl1!=null && !(payRecGl1.equalsIgnoreCase("")))	{
						    		String [] glcode = payRecGl1.split("~");
						    		recGl1=glcode[0];
						    		payGl1=glcode[1];
						    	}
								commAccountLine1.setRecGl(recGl1);
								commAccountLine1.setPayGl(payGl1);
								
								
								commAccountLine1.setVendorCode(vendorCode);
								
								if(!vendorCode.equalsIgnoreCase("")){
									List listPartnerName = serviceOrderManager.getPartnerDetails(vendorCode,sessionCorpID);
									if(!listPartnerName.isEmpty()){
										vendorName=listPartnerName.get(0).toString();
									}
								}
								BigDecimal comm2ActualExp = new BigDecimal(0);
								try{
									if(!vendorName.equalsIgnoreCase("")){
										String [] vendorDetail = vendorName.split("~");
										commAccountLine1.setEstimateVendorName(vendorDetail[0]);
										BigDecimal totalActualCommRevenue = new BigDecimal(compDivRev.get(compDiv).toString().trim());
										
										BigDecimal tempComm = new BigDecimal(0.00);
										try{
											double tempDoubleVal = totalActualCommRevenue.doubleValue();
											double tempCommVal =  Double.parseDouble(vendorDetail[1].toString()) ;
											tempDoubleVal = ((tempDoubleVal*100)*(tempCommVal/100))/100;
											comm2ActualExp = new BigDecimal(tempDoubleVal);
										}catch(Exception e){
											e.printStackTrace();
										}
										
										
										
									}
								}catch(Exception e){
									 e.printStackTrace();
								}
								if(!aidList.equalsIgnoreCase("")){
									BigDecimal tempamt = commAccountLine1.getActualExpense()!=null && !commAccountLine1.getActualExpense().toString().equals("")? commAccountLine1.getActualExpense() : new BigDecimal("0.00");
									
									comm2ActualExp = comm2ActualExp.add(tempamt);
								}else{
									
									comm2ActualExp = comm2ActualExp.subtract(totalCommisionExp1); 
								}
								totalCommisionExp1 = new BigDecimal(0);
								commAccountLine1.setActualExpense(comm2ActualExp);
								commAccountLine1.setLocalAmount(comm2ActualExp);
								commAccountLine1.setStatus(true);
								commAccountLine1.setContract(billing.getContract());
								 String actCode1="";
						          String  companyDivisionAcctgCodeUnique2="";
						 		 List companyDivisionAcctgCodeUniqueList1=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
						 		 if(companyDivisionAcctgCodeUniqueList1!=null && (!(companyDivisionAcctgCodeUniqueList1.isEmpty())) && companyDivisionAcctgCodeUniqueList1.get(0)!=null){
						 			 companyDivisionAcctgCodeUnique2 =companyDivisionAcctgCodeUniqueList1.get(0).toString();
						 		 }
						         if(companyDivisionAcctgCodeUnique2.equalsIgnoreCase("Y")){
						 			actCode1=partnerManager.getAccountCrossReference(vendorCode,compDiv,sessionCorpID);
						 		}else{
						 			actCode1=partnerManager.getAccountCrossReferenceUnique(vendorCode,sessionCorpID);
						 		}
						        commAccountLine1.setActgCode(actCode1);
						        
									try{
										company=companyManager.findByCorpID(sessionCorpID).get(0);
							    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
								    		Date dt1 =null;
								    		try{
								    		dt1 = new Date();
								    		}catch(Exception e){
								    			 e.printStackTrace();
								    		}
								    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
								    		String dt11=sdfDestination.format(dt1);
								    		dt1=sdfDestination.parse(dt11);
								    		Date dt2 =null;
								    		try{
								    		dt2 = new Date();
								    		}catch(Exception e){
								    			 e.printStackTrace();
								    		}
								    		List systemDefaultList = serviceOrderManager.findSysDefault(sessionCorpID);
								        	for (Object object : systemDefaultList) {
												SystemDefault systemDefault = (SystemDefault)object;
												dt2=systemDefault.getPostDate1();
											}
								    		if(dt1.compareTo(dt2)>0){
								    			commAccountLine1.setPayPostDate(dt2);
								    		}else{
								    			commAccountLine1.setPayPostDate(dt1);
								    		}
							    		}else{
								        	List systemDefaultList = serviceOrderManager.findSysDefault(sessionCorpID);
								        	for (Object object : systemDefaultList) {
												SystemDefault systemDefault = (SystemDefault)object;
												commAccountLine1.setPayPostDate(systemDefault.getPostDate1());
											}
							    		}
								}catch(Exception e){
						        	List systemDefaultList = serviceOrderManager.findSysDefault(sessionCorpID);
						        	for (Object object : systemDefaultList) {
										SystemDefault systemDefault = (SystemDefault)object;
										commAccountLine1.setPayPostDate(systemDefault.getPostDate1());
									}
						        	e.printStackTrace();
								}			
											        	
						        if(comm2ActualExp.compareTo(new BigDecimal(0))!=0 ){
						        	//commLine2Id = commAccountLine1.getId().toString();
						        	commAccountLine1 = accountLineManager.save(commAccountLine1);
						        	commLine2Id = commAccountLine1.getId().toString();
						        }
							}else{
								
									AccountLine commAccountLine1=null;
									List accIdList1 = serviceOrderManager.findAccountLineByCharge("SACOM2",sessionCorpID,serviceOrder.getShipNumber(),compDiv);  
									if(accIdList1!=null && !(accIdList1.isEmpty())){
										String tempid= accIdList1.get(0).toString();
										commAccountLine1 =accountLineManager.get(Long.parseLong(tempid));
										commAccountLine1.setStatus(false);
										accountLineManager.save(commAccountLine1);
									}
									
							    }
								
								String tempAccId = compDivAccId.get(compDiv);
								if(!commLine1Id.equals("") || !commLine2Id.equals("") ){
									
								}
						
								if(tempAccId!=null && tempAccId.length()>0 )
									perLineCommissionEntry(sid,commLine1Id,commLine2Id,invCount,tempAccId,aidList);	   
						}
					}
					
				}
								
			int i = serviceOrderManager.disableCommissionZeroEntry(sid,sessionCorpID);	
			
				
					
			int k =accountLineManager.updateSacom2ComDiv(sessionCorpID,sid,"");
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

		}
		
		
		
		return SUCCESS;
	}
	
	
	public String commPercentage ;
	public String commPercentageLine2;
	private String commType;
	private String salesPersonParentCode;
	private String estimatorParentCode;
	public void perLineCommissionEntry(Long soid,String commLine1Id,String commLine2Id, int invCount, String tempAccId,String aidList){
		Long StartTime = System.currentTimeMillis();
		if(invCount > 0){
			try{
			BigDecimal commissionableProfitPer = new BigDecimal(0);
			BigDecimal commissionableRatePer = new BigDecimal(0);
			BigDecimal totalActualCommExpence = new BigDecimal(0);
			
			serviceOrder = serviceOrderManager.get(soid);
			String [] accIdArray = tempAccId.split(",");
			try{
			for(int i = 0 ; i< accIdArray.length ; i++){
		
				BigDecimal tempComm = new BigDecimal(0);
				BigDecimal tempComm2 = new BigDecimal(0);
				commType="";
				commPercentage="";
				commPercentageLine2="";
				salesPersonParentCode = "";
				estimatorParentCode ="";
				accountLine = accountLineManager.get(Long.parseLong(accIdArray[i].toString()));
				if(accountLine.isStatus()){
				List tempList =  serviceOrderManager.getCommissionableValue(accountLine.getContract(),accountLine.getChargeCode(),sessionCorpID,serviceOrder.getJob()); 
				if(tempList != null && !(tempList.isEmpty())){
					charges = (Charges) tempList.get(0);
				}else{
					charges = null ;
				}
				
				String vendorCode1="";
				String vendorName1="";
				List listUser1 = serviceOrderManager.getParentAgentCodeFormUser(serviceOrder.getSalesMan());
				if(listUser1!=null && !listUser1.isEmpty()){
					vendorCode1 = listUser1.get(0).toString();
				}
				if(vendorCode1!=null && !(vendorCode1.trim().equals(""))){
					salesPersonParentCode = vendorCode1;
					
					if(charges != null && charges.getCommissionable() == true){
						try{
							if(serviceOrder.getJob()!=null && serviceOrder.getJob().equalsIgnoreCase("OFF") && charges.getBucket().toString().trim().equalsIgnoreCase("TP")){
								try{
									 //11050 - TP commission for OFF for jobs  Start
									if(serviceOrder.getJob()!=null && serviceOrder.getJob().equalsIgnoreCase("OFF") && offGridSelected!=null && !offGridSelected.isEmpty() && offGridSelected.get(accountLine.getChargeCode())!=null && !offGridSelected.get(accountLine.getChargeCode()).equalsIgnoreCase("")){
										String typeArr[]=offGridSelected.get(accountLine.getChargeCode()).split("~");
										String type=typeArr[0].toString();
										String per=typeArr[1].toString();
										if(type.equalsIgnoreCase("Revenue")){
											tempComm=accountLine.getActualRevenue().multiply(new BigDecimal(per));
											tempComm= tempComm.divide(new BigDecimal(100),4);										
										}else if(type.equalsIgnoreCase("Margin")){
											tempComm=accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense().setScale(4,BigDecimal.ROUND_HALF_UP):accountLine.getRevisionExpense().setScale(4,BigDecimal.ROUND_HALF_UP)));
											tempComm=tempComm.multiply(new BigDecimal(per));
											tempComm= tempComm.divide(new BigDecimal(100),4);										
										}		
										}
								
									}catch(Exception e){e.printStackTrace();}
									commType="TP";
							}else{
							
						if(charges.getCommission()!=null && !(charges.getCommission().equalsIgnoreCase("")) ){
							if(!charges.getBucket().toString().trim().equalsIgnoreCase("TP") ){
								tempComm = new BigDecimal(0);
								tempComm = accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense():accountLine.getRevisionExpense()));
							
								try{
									double tempDoubleVal = tempComm.doubleValue();
									double tempCommVal =  Double.parseDouble(charges.getCommission());
									tempDoubleVal = ((tempDoubleVal*100)*(tempCommVal/100))/100;
									tempComm = new BigDecimal(tempDoubleVal).setScale(4, RoundingMode.HALF_UP);;
								}catch(Exception e){
									e.printStackTrace();
								}
								commPercentage = charges.getCommission().toString();
								commType="FL";
							}else{
								
								if(accountLine.getActualRevenue().doubleValue()!=0.00 && accountLine.getRecInvoiceNumber()!=null && !accountLine.getRecInvoiceNumber().toString().equals("")){
									tempComm = accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense():accountLine.getRevisionExpense()));
								}else if((accountLine.getActualExpense().doubleValue()!=0.00)||(accountLine.getRevisionExpense().doubleValue()!=0)){
									tempComm = accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense():accountLine.getRevisionExpense()));
								}
								try{
									double tempDoubleVal = tempComm.doubleValue();
									double tempCommVal =  Double.parseDouble(charges.getCommission());
									tempDoubleVal = ((tempDoubleVal*100)*(tempCommVal/100))/100;
									tempComm = new BigDecimal(tempDoubleVal).setScale(4, RoundingMode.HALF_UP);;
								}catch(Exception e){
									e.printStackTrace();
								}
								commPercentage = charges.getCommission().toString();
								commType="TP";
						}
						}else{
							if(commLine1Id !=null && !commLine1Id.equals("") ){
							AccountLine acComLine1 = accountLineManager.get(new Long(commLine1Id));
							if(accountLine.getActualRevenue().compareTo(new BigDecimal(0))!= 0 ){
								commissionableProfitPer=accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense():accountLine.getRevisionExpense()));
								commissionableProfitPer = commissionableProfitPer.divide(accountLine.getActualRevenue(),3);
								commissionableProfitPer = commissionableProfitPer.multiply(new BigDecimal(100));
								List <RateGrid> rateList =  serviceOrderManager.getRateGirdForComm(acComLine1.getChargeCode(),acComLine1.getContract(),sessionCorpID);
								if((rateList!=null)&&(!rateList.isEmpty()))	{
						    		for (RateGrid rateGrid : rateList) {
						    			if(commissionableProfitPer.compareTo(new BigDecimal(rateGrid.getQuantity1()))== 0 || commissionableProfitPer.compareTo(new BigDecimal(rateGrid.getQuantity1()))==-1 ){
						    				commPercentage = rateGrid.getRate1().toString();
						    				break;
						    			}
						    		}
						    	}
								if(commPercentage!=null && (new BigDecimal(commPercentage)).compareTo(new BigDecimal(0))!=0){
								
									try{
										double tempDoubleVal = accountLine.getActualRevenue().doubleValue();
										double tempCommVal =  Double.parseDouble(commPercentage) ;
										tempDoubleVal = ((tempDoubleVal*100)*(tempCommVal/100))/100;
										tempComm = new BigDecimal(tempDoubleVal).setScale(4, RoundingMode.HALF_UP);;
										
									}catch(Exception e){
										e.printStackTrace();
									}
								}
								commType="VA";
							}
						}
						}
							}
						
						}catch(Exception e){e.printStackTrace();}

					}
				}
				
			
				if(charges != null && charges.getCommissionable() == true){
					if(charges.getCommission()!=null && !(charges.getCommission().equalsIgnoreCase("")) ){
					
					}else{
							String vendorCode="";
							String vendorName="";
							List listUser= serviceOrderManager.getParentAgentCodeFormUser(serviceOrder.getEstimator()); 
							if(!listUser.isEmpty()){
									vendorCode = listUser.get(0).toString();
							}
							if(vendorCode!=null && !(vendorCode.trim().equals(""))){
								estimatorParentCode = vendorCode;
								List listPartnerName = serviceOrderManager.getPartnerDetails(vendorCode,sessionCorpID);
								if(!listPartnerName.isEmpty() && listPartnerName.get(0)!=null){
									vendorName=listPartnerName.get(0).toString();
								}
								if(!vendorName.equalsIgnoreCase("")){
									String [] vendorDetail = vendorName.split("~");
									try{
										commPercentageLine2 = vendorDetail[1].toString();
										
										try{
											double tempDoubleVal = accountLine.getActualRevenue().doubleValue();
											double tempCommVal =  Double.parseDouble(commPercentageLine2) ;
											tempDoubleVal = ((tempDoubleVal*100)*(tempCommVal/100))/100;
											tempComm2 = new BigDecimal(tempDoubleVal).setScale(4, RoundingMode.HALF_UP);;
										}catch(Exception e){
											e.printStackTrace();
										}
										
										
									}catch(Exception e){
										 e.printStackTrace();
									}
								}
							}
					}
				}
					
				if(tempComm2.compareTo(new BigDecimal("0.00"))!=0 && (commType==null || commType.equals("")) )
					commType="Comm2";
				
				if(!accountLine.getChargeCode().contains("SACOM") && charges != null && charges.getCommissionable() == true)
					commissionEnrty(accountLine.getId(),tempComm,tempComm2,aidList);	
			}
			}
			}catch(ArrayIndexOutOfBoundsException arr){
				arr.printStackTrace();;
			}
		}catch(Exception e){
			 e.printStackTrace();
			logger.error(e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		}
		
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken to Entry in Commission table: "+timeTaken+"\n\n");
		
	}
	

	
	public void commissionEnrty(Long accId,BigDecimal line1CommAmount , BigDecimal line2CommAmount,String aidList){
		try{
			
			if(commType!=null && !commType.equalsIgnoreCase("") && accId!=null){
				AccountLine tempAccObj = accountLineManager.get(accId);
				if(!tempAccObj.getCompanyDivision().equalsIgnoreCase("CAJ")){				
				List l1  = serviceOrderManager.getCommissionById(accId, sessionCorpID,commLine1Id,commLine2Id);
				String commissionId="";
				if(l1!=null && !l1.isEmpty() && l1.get(0)!=null && !l1.get(0).toString().equals("")){
				//if(count>0){
					commission = commissionManager.get(new Long(l1.get(0).toString()));
					commissionId= commission.getId().toString();
			}else{
				commission= new Commission();
				commission.setShipNumber(serviceOrder.getShipNumber());
				commission.setCorpId(sessionCorpID);
				commission.setCreatedBy(getRequest().getRemoteUser());
				commission.setCreatedOn(new Date());
			}
				commission.setAid(accId);
				commission.setSalesPerson(salesPersonParentCode);
				commission.setConsultant(estimatorParentCode);
				
				if(!commType.equalsIgnoreCase("Comm2"))
					commission.setCommissionType(commType);
				
			if(commLine1Id== null)
				commLine1Id="";
			
			if(commLine2Id== null)
				commLine2Id="";
			
			//logger.warn("\n\n Sales person commission amount  value............  \n\n");
			
			BigDecimal tempComm1Amount = new BigDecimal(0.00);
			try{
			if(!commLine1Id.equals("")){
			
				List commissionAmountList = serviceOrderManager.getCommissionAmountTotalValue(sessionCorpID,"salesPersonAmount",accId,commissionId); 
				
				if(commissionAmountList!=null && !commissionAmountList.isEmpty() && commissionAmountList.get(0)!=null && !commissionAmountList.get(0).toString().equals("") )
					tempComm1Amount = new BigDecimal(commissionAmountList.get(0).toString());
				
				if(tempComm1Amount.compareTo(new BigDecimal(0.00))!=0)
					line1CommAmount = line1CommAmount.subtract(tempComm1Amount) ;
				
 				if(!aidList.equalsIgnoreCase("")){
 					commission.setSalesPersonAmount(commission.getSalesPersonAmount().add(line1CommAmount));
 				}else{
 					commission.setSalesPersonAmount(line1CommAmount);
 				}

				commission.setSalesPersonPercentage(new BigDecimal(commPercentage));
			
				
				}
			}catch(Exception e){
				 e.printStackTrace();
			}
			
			
			BigDecimal tempComm2Amount = new BigDecimal(0.00);
			try{
			if(!commLine2Id.equals("")){
				
				List commissionAmoutList = serviceOrderManager.getCommissionAmountTotalValue(sessionCorpID,"consultantAmount",accId,commissionId); 
				
				if(commissionAmoutList!=null && !commissionAmoutList.isEmpty() && commissionAmoutList.get(0)!=null && !commissionAmoutList.get(0).toString().equals("") )
					tempComm2Amount = new BigDecimal(commissionAmoutList.get(0).toString());
				
				if(tempComm2Amount.compareTo(new BigDecimal(0.00))!=0)
					line2CommAmount = line2CommAmount.subtract(tempComm2Amount) ;
			
 				if(!aidList.equalsIgnoreCase("")){
 					commission.setConsultantAmount(commission.getConsultantAmount().add(line2CommAmount));
 				}else{
 					commission.setConsultantAmount(line2CommAmount);
 				}

				commission.setConsultantPercentage(new BigDecimal(commPercentageLine2));
			
					}
			}catch(Exception e){
				 e.printStackTrace();
			}
			commission.setUpdatedBy(getRequest().getRemoteUser());
			commission.setUpdatedOn(new Date());
			
			if(!commLine1Id.equals("") && line1CommAmount.compareTo(new BigDecimal(0.00))!=0 )
				commission.setCommissionLine1Id(commLine1Id);
			if(!commLine2Id.equals("") && line2CommAmount.compareTo(new BigDecimal(0.00))!=0)
				commission.setCommissionLine2Id(commLine2Id);
			
			commission.setSettledDate(new Date());
			commission.setCompanyDivision(tempAccObj.getCompanyDivision());
			commission.setActualExpense((tempAccObj.getActualExpense().doubleValue()!=0?tempAccObj.getActualExpense():tempAccObj.getRevisionExpense()));
			commission.setActualRevenue(tempAccObj.getActualRevenue());
			commission.setChargeCode(tempAccObj.getChargeCode());
			
			
						
			
			if(!commLine1Id.equals("")  && line1CommAmount.compareTo(new BigDecimal(0.00))!=0 ){
				commissionManager.save(commission);
			}else{
				if(!commLine2Id.equals("") && line2CommAmount.compareTo(new BigDecimal(0.00))!=0 ){
					commissionManager.save(commission);
				}
		 }
		}
			}
		}catch(Exception e){
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}}

	@SkipValidation
    private List findLinkerShipNumber(ServiceOrder serviceOrder) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	List linkedShipNumberList= new ArrayList();
    	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
    	}else{
    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return linkedShipNumberList;
    }
	@SkipValidation
    private List<Object> findServiceOrderRecords(List linkedShipNumber,	ServiceOrder serviceOrderLocal) {
    	List<Object> recordList= new ArrayList();
    	try{
    	Iterator it =linkedShipNumber.iterator();
    	while(it.hasNext()){
    		String shipNumber= it.next().toString();
    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
    		recordList.add(serviceOrderRemote);
    	}
    	}catch(Exception e)
		{
			 e.printStackTrace();
		}
    	return recordList;
    	}
	@SkipValidation
    public void createAccountLine(List<Object> records, ServiceOrder  serviceOrder,AccountLine accountLine) {
		Iterator  it=records.iterator();
    	while(it.hasNext()){
    		  serviceOrderToRecods=(ServiceOrder)it.next();
    		try{
    		trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
    		 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
        	if(trackingStatusToRecods.getSoNetworkGroup() && !trackingStatusToRecods.getAccNetworkGroup()){
        		trackingStatus=trackingStatusManager.get(serviceOrder.getId()); 
        		String externalCorpID = trackingStatusToRecods.getCorpID();
        		String externalBaseCurrency=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
        		//String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getNetworkPartnerCode()); 
        		//Boolean extCostElement =  accountLineManager.getExternalCostElement(externalCorpID); 
        		UTSIeuVatPercentList=refMasterManager.findVatPercentList(externalCorpID, "EUVAT");
        		UTSIpayVatPercentList = refMasterManager.findVatPercentList(externalCorpID, "PAYVATDESC");
        		List fieldToSync=customerFileManager.findAccFieldToSyncCreate("AccountLine","CMM");
        		AccountLine accountLineNew= new AccountLine();
        		try{
    				Iterator it1=fieldToSync.iterator();
    				while(it1.hasNext()){
    				String field=it1.next().toString();
    				String[] splitField=field.split("~");
    				String fieldFrom=splitField[0];
    				String fieldTo=splitField[1];
    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
					if(fieldFrom!=null && fieldTo!=null && fieldFrom.trim().equalsIgnoreCase("recRateExchange") && fieldTo.trim().equalsIgnoreCase("exchangeRate"))	{
						BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();
						if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
						payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
						accountLineNew.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
						} 
					}else{
					beanUtilsBean.setProperty(accountLineNew,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
					}
					}catch(Exception ex){
						logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
					}
    				
    				}
        		}catch(Exception e){
        			e.printStackTrace();
        		}
        	
	            accountLineNew.setNetworkSynchedId(accountLine.getId());
	            accountLineNew.setAccountLineNumber(accountLine.getAccountLineNumber()); 
	            accountLineNew.setCompanyDivision(serviceOrderToRecods.getCompanyDivision());
	            accountLineNew.setVendorCode(serviceOrderToRecods.getBookingAgentCode());
	            accountLineNew.setEstimateVendorName(serviceOrderToRecods.getBookingAgentName());
	             String actCode="";
	             String  companyDivisionAcctgCodeUnique1="";
	    		 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(externalCorpID);
	    		 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
	    			 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
	    		 }
	            if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
	    			actCode=partnerManager.getAccountCrossReference(serviceOrderToRecods.getBookingAgentCode(),serviceOrderToRecods.getCompanyDivision(),externalCorpID);
	    		}else{
	    			actCode=partnerManager.getAccountCrossReferenceUnique(serviceOrderToRecods.getBookingAgentCode(),externalCorpID);
	    		}
	            accountLineNew.setActgCode(actCode);
	            if(accountLineNew.getEstimateRevenueAmount() !=null && accountLineNew.getEstimateRevenueAmount().doubleValue()>0){
	            accountLineNew.setEstimatePassPercentage(100);
	            }
	            if(accountLineNew.getRevisionRevenueAmount() !=null && accountLineNew.getRevisionRevenueAmount().doubleValue()>0){
	            accountLineNew.setRevisionPassPercentage(100);
	            }
	            
        		String chargeStr="";
				List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),accountLineNew.getChargeCode(),externalCorpID,serviceOrderToRecods.getJob(),serviceOrderToRecods.getRouting(),serviceOrderToRecods.getCompanyDivision());
				if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
					  chargeStr= glList.get(0).toString();
				  }
				   if(!chargeStr.equalsIgnoreCase("")){
					  String [] chrageDetailArr = chargeStr.split("#");
					  accountLineNew.setRecGl(chrageDetailArr[1]);
					  accountLineNew.setPayGl(chrageDetailArr[2]);
					}else{
						accountLineNew.setRecGl("");
						accountLineNew.setPayGl("");
				  }
	            accountLineNew.setId(null);
	            accountLineNew.setCorpID(externalCorpID);
	            accountLineNew.setCreatedBy("Networking");
	            accountLineNew.setUpdatedBy("Networking");
	            accountLineNew.setCreatedOn(new Date());
	            accountLineNew.setUpdatedOn(new Date()); 
	            
	            
				accountLineNew.setRacValueDate(new Date());
				
				accountLineNew.setContractValueDate(new Date());
			    
				accountLineNew.setValueDate(new Date());
			    
				BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();
				if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
				payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
				accountLineNew.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
				}
				
				try{
		
				}catch(Exception e){
					e.printStackTrace();
	    		}
				if(externalBaseCurrency !=null && baseCurrency!=null && (!(baseCurrency.trim().equalsIgnoreCase(externalBaseCurrency.trim())))){
					DecimalFormat decimalFormatExchangeRate = new DecimalFormat("#.####");
					String estExchangeRate="1"; 
		            BigDecimal estExchangeRateBig=new BigDecimal("1.0000");
		            List estRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstSellCurrency());
					 if((estRate!=null)&&(!estRate.isEmpty())&& estRate.get(0)!=null && (!(estRate.get(0).toString().equals(""))))
					 {
						 estExchangeRate=estRate.get(0).toString();
					 
					 } 
					estExchangeRateBig=new BigDecimal(estExchangeRate);	
					accountLineNew.setEstSellExchangeRate(estExchangeRateBig); 
					accountLineNew.setEstExchangeRate(estExchangeRateBig);
					if(accountLine.getEstSellLocalRate()!=null && accountLine.getEstSellLocalRate().doubleValue()!=0){
					accountLineNew.setEstimateSellRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
					accountLineNew.setEstimateRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
					}
					if(accountLine.getEstSellLocalAmount()!=null && accountLine.getEstSellLocalAmount().doubleValue()!=0){
					accountLineNew.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
					accountLineNew.setEstimateExpense(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
					} 
					BigDecimal estcontractExchangeRateBig=new BigDecimal("1");
					try{ 	
						estcontractExchangeRateBig=((new BigDecimal(decimalFormatExchangeRate.format((accountLineNew.getEstimateContractRate().doubleValue())/(accountLineNew.getEstimateSellRate().doubleValue()))))); 
					}catch(Exception es){
						es.printStackTrace();
					} 
					accountLineNew.setEstimateContractExchangeRate(estcontractExchangeRateBig);
					accountLineNew.setEstimatePayableContractExchangeRate(estcontractExchangeRateBig);
					
					
					String revisionExchangeRate="1"; 
		            BigDecimal revisionExchangeRateBig=new BigDecimal("1.0000");
		            List revisionRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionSellCurrency());
					 if((revisionRate!=null)&&(!revisionRate.isEmpty())&& revisionRate.get(0)!=null && (!(revisionRate.get(0).toString().equals(""))))
					 {
						 revisionExchangeRate=revisionRate.get(0).toString();
					 
					 } 
					 revisionExchangeRateBig=new BigDecimal(revisionExchangeRate);	
					accountLineNew.setRevisionSellExchangeRate(revisionExchangeRateBig);
					accountLineNew.setRevisionExchangeRate(revisionExchangeRateBig);
					if(accountLine.getRevisionSellLocalRate()!=null && accountLine.getRevisionSellLocalRate().doubleValue()!=0){
					accountLineNew.setRevisionSellRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
					accountLineNew.setRevisionRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
					}
					if(accountLine.getRevisionSellLocalAmount()!=null && accountLine.getRevisionSellLocalAmount().doubleValue()!=0){
					accountLineNew.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
					accountLineNew.setRevisionExpense(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
					}
					
					
					
					BigDecimal revisioncontractExchangeRateBig=new BigDecimal("1");	
					try{ 
						revisioncontractExchangeRateBig=((new BigDecimal(decimalFormatExchangeRate.format((accountLineNew.getRevisionContractRate().doubleValue())/(accountLineNew.getRevisionSellRate().doubleValue())))));
						
					}catch(Exception es){
						es.printStackTrace();
					}
					accountLineNew.setRevisionContractExchangeRate(revisioncontractExchangeRateBig);
					accountLineNew.setRevisionPayableContractExchangeRate(revisioncontractExchangeRateBig);
					 
					
					
					
					String recExchangeRate="1"; 
		            BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
		            List recRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRecRateCurrency());
					 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
					 {
						 recExchangeRate=recRate.get(0).toString();
					 
					 } 
					recExchangeRateBig=new BigDecimal(recExchangeRate);	
					accountLineNew.setRecRateExchange(recExchangeRateBig);
					payExchangeRateBig=accountLineNew.getRecRateExchange();
					if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
						payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
						accountLineNew.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
					}
					if(accountLine.getRecCurrencyRate()!=null && accountLine.getRecCurrencyRate().doubleValue()!=0){
					accountLineNew.setRecRate((new BigDecimal(decimalFormatExchangeRate.format((accountLine.getRecCurrencyRate().doubleValue())/(recExchangeRateBig.doubleValue())))));
					}
					if(accountLine.getActualRevenueForeign()!=null && accountLine.getActualRevenueForeign().doubleValue()!=0){
					accountLineNew.setActualRevenue((new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue())/(recExchangeRateBig.doubleValue())))));
					try{
						accountLineNew.setActualExpense((new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue())/(recExchangeRateBig.doubleValue())))));
						}catch(Exception e){
							e.printStackTrace();
					}
					
					
		    		}
					BigDecimal contractExchangeRateBig=new BigDecimal("1");	
					try{
						
						contractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((accountLineNew.getContractRate().doubleValue())/(accountLineNew.getRecRate().doubleValue()))));
						
					}catch(Exception es){
						es.printStackTrace();
					}
					accountLineNew.setContractExchangeRate(contractExchangeRateBig);
					accountLineNew.setPayableContractExchangeRate(contractExchangeRateBig); 
				}

				if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){
				if(UTSIeuVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){	
				accountLineNew.setRecVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
				}else{
				accountLineNew.setRecVatDescr("");
				accountLineNew.setRecVatPercent("0");
				accountLineNew.setRecVatAmt(new BigDecimal(0));
				accountLineNew.setEstVatAmt(new BigDecimal(0));
				accountLineNew.setRevisionVatAmt(new BigDecimal(0));
				}
				if(UTSIpayVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){ 
				accountLineNew.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
				}else{
				accountLineNew.setPayVatDescr("");
				accountLineNew.setPayVatPercent("0");
				accountLineNew.setPayVatAmt(new BigDecimal(0));
				accountLineNew.setEstExpVatAmt(new BigDecimal(0)); 
				accountLineNew.setRevisionExpVatAmt(new BigDecimal(0));
				}
				}
	            accountLineNew.setServiceOrderId(serviceOrderToRecods.getId());
	            accountLineNew.setSequenceNumber(serviceOrderToRecods.getSequenceNumber());
	            accountLineNew.setShipNumber(serviceOrderToRecods.getShipNumber());
	            accountLineNew.setServiceOrder(serviceOrderToRecods);
	            accountLineNew=accountLineManager.save(accountLineNew);
	            updateExternalAccountLine(serviceOrderToRecods,externalCorpID);
        	}
    		}catch(Exception e){
    	    	 e.printStackTrace();
    			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	
    		}
    	}
	}
	
	@SkipValidation
	public void createDMMAccountLine(List<Object> records, ServiceOrder  serviceOrder,AccountLine accountLine,Billing billing){

		Iterator  it=records.iterator();
    	while(it.hasNext()){
    		  serviceOrderToRecods=(ServiceOrder)it.next();
    		try{
    		trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
    		 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
        	if(trackingStatusToRecods.getSoNetworkGroup() && !trackingStatusToRecods.getAccNetworkGroup()){
        		trackingStatus=trackingStatusManager.get(serviceOrder.getId()); 
        		String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getNetworkPartnerCode()); 
        		UTSIpayVatList=	refMasterManager.findByParameterUTSIPayVat (sessionCorpID, "PAYVATDESC",externalCorpID);
        		UTSIpayVatPercentList = refMasterManager.findVatPercentList(externalCorpID, "PAYVATDESC");
        		UTSIeuVatPercentList=refMasterManager.findVatPercentList(externalCorpID, "EUVAT");
        		List fieldToSync=customerFileManager.findAccFieldToSyncCreate("AccountLine","DMM");
        		AccountLine accountLineNew= new AccountLine();
        		try{
    				Iterator it1=fieldToSync.iterator();
    				while(it1.hasNext()){
    				String field=it1.next().toString();
    				String[] splitField=field.split("~");
    				String fieldFrom=splitField[0];
    				String fieldTo=splitField[1];
    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
					beanUtilsBean.setProperty(accountLineNew,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
					}catch(Exception ex){
						logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
					}
    				
    				}
        		}catch(Exception e){
        			e.printStackTrace();
        		}
        		try{
        			if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){ 
    			        String payVatDescr="";
    			       String companyDivisionUTSI=accountLineManager.getUTSICompanyDivision(externalCorpID,accountLine.getBillToCode()) ;
    			       accountLineNew.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
    			    	if(UTSIpayVatList.containsKey(accountLineNew.getPayVatDescr()+"~"+companyDivisionUTSI)){
    			    		payVatDescr=UTSIpayVatList.get(accountLineNew.getPayVatDescr()+"~"+companyDivisionUTSI);
    			    		accountLineNew.setPayVatDescr(payVatDescr);
    			    		String payVatPercent="0";
    			    		if(UTSIpayVatPercentList.containsKey(payVatDescr)){ 
    	    				payVatPercent=UTSIpayVatPercentList.get(payVatDescr);
    			    		}
    	    				accountLineNew.setPayVatPercent(payVatPercent);
    			    	}else{
    			    		if(UTSIpayVatList.containsKey(accountLineNew.getPayVatDescr()+"~NO")){
    				    		payVatDescr=UTSIpayVatList.get(accountLineNew.getPayVatDescr()+"~NO");
    				    		accountLineNew.setPayVatDescr(payVatDescr);
    				    		String payVatPercent="0";
    				    		if(UTSIpayVatPercentList.containsKey(payVatDescr)){ 
    		    				payVatPercent=UTSIpayVatPercentList.get(payVatDescr);
    				    		}
    		    				accountLineNew.setPayVatPercent(payVatPercent);
    				    	}else{
    				    		accountLineNew.setPayVatDescr("");
    				    		accountLineNew.setPayVatPercent("0");
    				    	}
    			    	}
    			    	}
        		}catch(Exception e){
        			e.printStackTrace();
        		}
        		
        		  
			    accountLineNew.setPayableContractCurrency(accountLine.getRecRateCurrency());
			    accountLineNew.setCountry(accountLine.getRecRateCurrency());
			    accountLineNew.setEstimatePayableContractCurrency(accountLine.getEstSellCurrency());
			    accountLineNew.setEstCurrency(accountLine.getEstSellCurrency());
			    accountLineNew.setRevisionPayableContractCurrency(accountLine.getRevisionSellCurrency());
			    accountLineNew.setRevisionCurrency(accountLine.getRevisionSellCurrency());
			    String billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLineNew.getBillToCode(),externalCorpID);
	    		if(!billingCurrency.equalsIgnoreCase(""))
	    		{
	    			accountLineNew.setRecRateCurrency(billingCurrency);
	    			accountLineNew.setEstSellCurrency(billingCurrency);
	    			accountLineNew.setRevisionSellCurrency(billingCurrency);
	    		}else{ 
	    			String baseCurrencyExternalCorpID="";
	    			String baseCurrencyCompanyDivisionExternalCorpID=""; 
	    			baseCurrencyExternalCorpID=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
	    			baseCurrencyCompanyDivisionExternalCorpID=	 companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrderToRecods.getCompanyDivision(),externalCorpID);
	    			if(baseCurrencyCompanyDivisionExternalCorpID==null ||baseCurrencyCompanyDivisionExternalCorpID.equals(""))
	    			{
	    				accountLineNew.setRecRateCurrency(baseCurrencyExternalCorpID);
	    				accountLineNew.setEstSellCurrency(baseCurrencyExternalCorpID);
	    				accountLineNew.setRevisionSellCurrency(baseCurrencyExternalCorpID);
	    			}else{
	    				accountLineNew.setRecRateCurrency(baseCurrencyCompanyDivisionExternalCorpID);
	    				accountLineNew.setEstSellCurrency(baseCurrencyCompanyDivisionExternalCorpID);
	    				accountLineNew.setRevisionSellCurrency(baseCurrencyCompanyDivisionExternalCorpID);
	    			}
	    		}
			   
	            accountLineNew.setNetworkSynchedId(accountLine.getId());
	            
	          
	            try{
	            if(accountLineNew.getBillToCode()!=null && (!(accountLineNew.getBillToCode().toString().equals(""))) && billingRecods.getBillToCode()!=null && (!(billingRecods.getBillToCode().toString().equals(""))) && accountLineNew.getBillToCode().toString().equals(billingRecods.getBillToCode().toString()) )	{
                 if(!accountLineNew.getVATExclude()){ 
	            	accountLineNew.setRecVatDescr(billingRecods.getPrimaryVatCode());
		    		if(billingRecods.getPrimaryVatCode()!=null && (!(billingRecods.getPrimaryVatCode().toString().equals("")))){
			    		String recVatPercent="0";
			    		if(UTSIeuVatPercentList.containsKey(billingRecods.getPrimaryVatCode())){
				    		recVatPercent=UTSIeuVatPercentList.get(billingRecods.getPrimaryVatCode());
				    		}
			    		accountLineNew.setRecVatPercent(recVatPercent);
			    		}
                 }	
	            }
	            }catch(Exception e){
	            	e.printStackTrace();
	            }
	            //}
	            accountLineNew.setAccountLineNumber(accountLine.getAccountLineNumber()); 
	            accountLineNew.setCompanyDivision(serviceOrderToRecods.getCompanyDivision());
	            List bookingAgentCodeList=companyDivisionManager.getBookingAgentCode(accountLine.getCompanyDivision(),sessionCorpID);
	            if(bookingAgentCodeList!=null && (!(bookingAgentCodeList.isEmpty())) && bookingAgentCodeList.get(0)!=null && (!(bookingAgentCodeList.get(0).toString().trim().equals(""))))
	            {
	            	accountLineNew.setVendorCode(bookingAgentCodeList.get(0).toString());
	            	List bookingAgentNamelist=	serviceOrderManager.findByBooking(bookingAgentCodeList.get(0).toString(), sessionCorpID);
	                
	            	if(bookingAgentNamelist!=null && (!(bookingAgentNamelist.isEmpty())) && bookingAgentNamelist.get(0)!=null && (!(bookingAgentNamelist.get(0).toString().trim().equals("")))){
		            accountLineNew.setEstimateVendorName(bookingAgentNamelist.get(0).toString());
	                }
	            	
	            }
	            //accountLineNew.setVendorCode(serviceOrderToRecods.getBookingAgentCode());
	            //accountLineNew.setEstimateVendorName(serviceOrderToRecods.getBookingAgentName());
	             String actCode="";
	             String  companyDivisionAcctgCodeUnique1="";
	    		 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(externalCorpID);
	    		 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
	    			 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
	    		 }
	            if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
	    			actCode=partnerManager.getAccountCrossReference(accountLineNew.getVendorCode(),serviceOrderToRecods.getCompanyDivision(),externalCorpID);
	    		}else{
	    			actCode=partnerManager.getAccountCrossReferenceUnique(accountLineNew.getVendorCode(),externalCorpID);
	    		}
	            accountLineNew.setActgCode(actCode);
	            Boolean extCostElement =  accountLineManager.getExternalCostElement(externalCorpID);
	            if(!extCostElement){
	            List  agentGLList=accountLineManager.getGLList(accountLineNew.getChargeCode(),billingRecods.getContract(),externalCorpID);
				if(agentGLList!=null && (!(agentGLList.isEmpty())) && agentGLList.get(0)!=null){
					try{
					String[] GLarrayData=agentGLList.get(0).toString().split("#"); 
					String recGl=GLarrayData[0];
					String payGl=GLarrayData[1];
					if(!(recGl.equalsIgnoreCase("NO"))){
						accountLineNew.setRecGl(recGl);
					}
					if(!(payGl.equalsIgnoreCase("NO"))){
						accountLineNew.setPayGl(payGl);
					}
					String VATExclude=GLarrayData[2];
					if(VATExclude.equalsIgnoreCase("Y")){
						accountLineNew.setVATExclude(true);
					}else{
						accountLineNew.setVATExclude(false);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				}
        	}else{
        		String chargeStr="";
				List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),accountLineNew.getChargeCode(),externalCorpID,serviceOrderToRecods.getJob(),serviceOrderToRecods.getRouting(),serviceOrderToRecods.getCompanyDivision());
				if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
					  chargeStr= glList.get(0).toString();
				  }
				   if(!chargeStr.equalsIgnoreCase("")){
					  String [] chrageDetailArr = chargeStr.split("#");
					  accountLineNew.setRecGl(chrageDetailArr[1]);
					  accountLineNew.setPayGl(chrageDetailArr[2]);
					}else{
						accountLineNew.setRecGl("");
						accountLineNew.setPayGl("");
				  }
        	}
	            accountLineNew.setId(null);
	            accountLineNew.setCorpID(externalCorpID);
	            accountLineNew.setCreatedBy("Networking");
	            accountLineNew.setUpdatedBy("Networking");
	            accountLineNew.setCreatedOn(new Date());
	            accountLineNew.setUpdatedOn(new Date()); 
	            String estimatePayableContractExchangeRate="1"; 
	            BigDecimal estimatePayableContractExchangeRateBig=new BigDecimal("1.0000");
	            List estimatePayableContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstimatePayableContractCurrency ());
				 if((estimatePayableContractRate!=null)&&(!estimatePayableContractRate.isEmpty())&& estimatePayableContractRate.get(0)!=null && (!(estimatePayableContractRate.get(0).toString().equals(""))))
				 {
					 estimatePayableContractExchangeRate=estimatePayableContractRate.get(0).toString();
				 
				 }
				 estimatePayableContractExchangeRateBig=new BigDecimal(estimatePayableContractExchangeRate);	
				 accountLineNew.setEstimatePayableContractExchangeRate(estimatePayableContractExchangeRateBig);
				 accountLineNew.setEstimatePayableContractValueDate(new Date());
				 accountLineNew.setEstExchangeRate(estimatePayableContractExchangeRateBig);
				 accountLineNew.setEstValueDate(new Date());
				 if(accountLine.getEstimateSellDeviation()!=null && accountLine.getEstSellLocalAmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLine.getEstSellLocalAmount().doubleValue()>0){
				  accountLineNew.setEstimatePayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
				 }else{
				 accountLineNew.setEstimatePayableContractRateAmmount(accountLine.getEstSellLocalAmount());
				 }
				 accountLineNew.setEstimatePayableContractRate(accountLine.getEstSellLocalRate());
				 if(accountLine.getEstimateSellDeviation()!=null && accountLine.getEstSellLocalAmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLine.getEstSellLocalAmount().doubleValue()>0){
					 accountLineNew.setEstLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
				 }else{
				 accountLineNew.setEstLocalAmount(accountLine.getEstSellLocalAmount());
				 }
				 accountLineNew.setEstLocalRate(accountLine.getEstSellLocalRate());
				 accountLineNew.setEstimateRate(new BigDecimal(decimalFormat.format(accountLine.getEstSellLocalRate().doubleValue()/accountLineNew.getEstimatePayableContractExchangeRate().doubleValue())));
				 accountLineNew.setEstimateExpense(new BigDecimal(decimalFormat.format(accountLineNew.getEstimatePayableContractRateAmmount().doubleValue()/accountLineNew.getEstimatePayableContractExchangeRate().doubleValue())));
				
				 
				 
				 String estimateContractExchangeRate="1"; 
	            BigDecimal estimateContractExchangeRateBig=new BigDecimal("1.0000");
	            List estimateContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstimateContractCurrency ());
				 if((estimateContractRate!=null)&&(!estimateContractRate.isEmpty())&& estimateContractRate.get(0)!=null && (!(estimateContractRate.get(0).toString().equals(""))))
				 {
					 estimateContractExchangeRate=estimateContractRate.get(0).toString();
				 
				 } 
				 estimateContractExchangeRateBig=new BigDecimal(estimateContractExchangeRate);	
				 accountLineNew.setEstimateContractExchangeRate(estimateContractExchangeRateBig);
				 accountLineNew.setEstimateContractValueDate(new Date()); 
				 accountLineNew.setEstimateSellRate(new BigDecimal(decimalFormat.format(accountLineNew.getEstimateContractRate().doubleValue()/accountLineNew.getEstimateContractExchangeRate().doubleValue())));
				 if(accountLine.getEstimateSellDeviation()!=null && accountLineNew.getEstimateContractRateAmmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLineNew.getEstimateContractRateAmmount().doubleValue()>0){
					  accountLineNew.setEstimateContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateContractRateAmmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
					 }else{
						 
					 }
				 accountLineNew.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format(accountLineNew.getEstimateContractRateAmmount().doubleValue()/ accountLineNew.getEstimateContractExchangeRate().doubleValue())));
				 
				 
				String estSellExchangeRate="1"; 
		        BigDecimal estSellExchangeRateBig=new BigDecimal("1.0000");
	            List estSellRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstSellCurrency());
				 if((estSellRate!=null)&&(!estSellRate.isEmpty())&& estSellRate.get(0)!=null && (!(estSellRate.get(0).toString().equals(""))))
				 {
					 estSellExchangeRate=estSellRate.get(0).toString();
				 }
				 estSellExchangeRateBig=new BigDecimal(estSellExchangeRate);
				accountLineNew.setEstSellExchangeRate(estSellExchangeRateBig);
				accountLineNew.setEstSellValueDate(new Date());
				
				accountLineNew.setEstSellLocalRate(accountLineNew.getEstimateSellRate().multiply(estSellExchangeRateBig));
				accountLineNew.setEstSellLocalAmount(accountLineNew.getEstimateRevenueAmount().multiply(estSellExchangeRateBig));
				 
				 
				
				
				String revisionPayableContractExchangeRate="1"; 
	            BigDecimal revisionPayableContractExchangeRateBig=new BigDecimal("1.0000");
	            List revisionPayableContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionPayableContractCurrency ());
				 if((revisionPayableContractRate!=null)&&(!revisionPayableContractRate.isEmpty())&& revisionPayableContractRate.get(0)!=null && (!(revisionPayableContractRate.get(0).toString().equals(""))))
				 {
					 revisionPayableContractExchangeRate=revisionPayableContractRate.get(0).toString();
				 
				 }
				 revisionPayableContractExchangeRateBig=new BigDecimal(revisionPayableContractExchangeRate);	
				 accountLineNew.setRevisionPayableContractExchangeRate(revisionPayableContractExchangeRateBig);
				 accountLineNew.setRevisionPayableContractValueDate(new Date());
				 accountLineNew.setRevisionExchangeRate(revisionPayableContractExchangeRateBig);
				 accountLineNew.setRevisionValueDate(new Date());
				 if(accountLine.getRevisionSellDeviation()!=null && accountLine.getRevisionSellLocalAmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLine.getRevisionSellLocalAmount().doubleValue()>0){
				  accountLineNew.setRevisionPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue()))	 );	 
				 }else{
				 accountLineNew.setRevisionPayableContractRateAmmount(accountLine.getRevisionSellLocalAmount());
				 }
				 accountLineNew.setRevisionPayableContractRate(accountLine.getRevisionSellLocalRate());
				 if(accountLine.getRevisionSellDeviation()!=null && accountLine.getRevisionSellLocalAmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLine.getRevisionSellLocalAmount().doubleValue()>0){
					 accountLineNew.setRevisionLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue()))	 );	 
				 }else{
				 accountLineNew.setRevisionLocalAmount(accountLine.getRevisionSellLocalAmount());
				 }
				 accountLineNew.setRevisionLocalRate(accountLine.getRevisionSellLocalRate());
				 accountLineNew.setRevisionRate(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionPayableContractRate().doubleValue()/ accountLineNew.getRevisionPayableContractExchangeRate().doubleValue())));
				 accountLineNew.setRevisionExpense(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionPayableContractRateAmmount().doubleValue()/ accountLineNew.getRevisionPayableContractExchangeRate().doubleValue())));
				
				String revisionContractExchangeRate="1"; 
	            BigDecimal revisionContractExchangeRateBig=new BigDecimal("1.0000");
	            List revisionContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionContractCurrency ());
				 if((revisionContractRate!=null)&&(!revisionContractRate.isEmpty())&& revisionContractRate.get(0)!=null && (!(revisionContractRate.get(0).toString().equals(""))))
				 {
					 revisionContractExchangeRate=revisionContractRate.get(0).toString();
				 
				 } 
				 revisionContractExchangeRateBig=new BigDecimal(revisionContractExchangeRate);	
				 accountLineNew.setRevisionContractExchangeRate(revisionContractExchangeRateBig);
				 accountLineNew.setRevisionContractValueDate(new Date());
				 accountLineNew.setRevisionSellRate(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionContractRate().doubleValue()/  accountLineNew.getRevisionContractExchangeRate().doubleValue())));
				 if(accountLine.getRevisionSellDeviation()!=null && accountLineNew.getRevisionContractRateAmmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLineNew.getRevisionContractRateAmmount().doubleValue()>0){
					  accountLineNew.setRevisionContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionContractRateAmmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue())));	 
					 }else{
						 
					 }
				 accountLineNew.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionContractRateAmmount().doubleValue()/  accountLineNew.getRevisionContractExchangeRate().doubleValue())));
				 
				 
				String revisionSellExchangeRate="1"; 
		        BigDecimal revisionSellExchangeRateBig=new BigDecimal("1.0000");
	            List revisionSellRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionSellCurrency());
				 if((revisionSellRate!=null)&&(!revisionSellRate.isEmpty())&& revisionSellRate.get(0)!=null && (!(revisionSellRate.get(0).toString().equals(""))))
				 {
					 revisionSellExchangeRate=estSellRate.get(0).toString();
				 }
				 revisionSellExchangeRateBig=new BigDecimal(revisionSellExchangeRate);
				 accountLineNew.setRevisionSellExchangeRate(revisionSellExchangeRateBig);
				 accountLineNew.setRevisionSellValueDate(new Date());
				 
				 accountLineNew.setRevisionSellLocalRate(accountLineNew.getRevisionSellRate().multiply(revisionSellExchangeRateBig));
				 accountLineNew.setRevisionSellLocalAmount(accountLineNew.getRevisionRevenueAmount().multiply(revisionSellExchangeRateBig));
				
				 
				  
				String contractPayableExchangeRate="1"; 
		        BigDecimal contractPayableExchangeRateBig=new BigDecimal("1.0000");
		        List contractPayableRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getPayableContractCurrency ());
				if((contractPayableRate!=null)&&(!contractPayableRate.isEmpty())&& contractPayableRate.get(0)!=null && (!(contractPayableRate.get(0).toString().equals(""))))
					 {
					contractPayableExchangeRate=contractPayableRate.get(0).toString();
					 
					 } 
				contractPayableExchangeRateBig=new BigDecimal(contractPayableExchangeRate);
				accountLineNew.setPayableContractExchangeRate(contractPayableExchangeRateBig);
				accountLineNew.setPayableContractValueDate(new Date()); 
				accountLineNew.setValueDate(new Date());
			    double payExchangeRate=0; 
	            //BigDecimal payExchangeRateBig=new BigDecimal("1.0000");  
				payExchangeRate=Double.parseDouble(contractPayableExchangeRate);
				accountLineNew.setExchangeRate(payExchangeRate);
				if(accountLine.getReceivableSellDeviation()!=null && accountLine.getActualRevenueForeign()!=null &&  accountLine.getReceivableSellDeviation().doubleValue()>0 && accountLine.getActualRevenueForeign().doubleValue()>0){
					accountLineNew.setPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));
					accountLineNew.setLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));	
				}else{
				accountLineNew.setPayableContractRateAmmount(accountLine.getActualRevenueForeign());
				accountLineNew.setLocalAmount(accountLine.getActualRevenueForeign());
				}
				try{
					accountLineNew.setActualExpense(new BigDecimal(decimalFormat.format(accountLineNew.getPayableContractRateAmmount().doubleValue()/accountLineNew.getPayableContractExchangeRate().doubleValue())));
				}catch(Exception e){
					e.printStackTrace();	
		    	}
					//payExchangeRateBig=new BigDecimal(payExchangeRate);
				 
				 
			    String contractExchangeRate="1"; 
	            BigDecimal contractExchangeRateBig=new BigDecimal("1.0000");
	            List contractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getContractCurrency ());
				 if((contractRate!=null)&&(!contractRate.isEmpty())&& contractRate.get(0)!=null && (!(contractRate.get(0).toString().equals(""))))
				 {
					 contractExchangeRate=contractRate.get(0).toString();
				 
				 } 
				contractExchangeRateBig=new BigDecimal(contractExchangeRate);	
				accountLineNew.setContractExchangeRate(contractExchangeRateBig);
				accountLineNew.setContractValueDate(new Date()); 
				accountLineNew.setRecRate(new BigDecimal(decimalFormat.format(accountLineNew.getContractRate().doubleValue()/accountLineNew.getContractExchangeRate().doubleValue())));
				 if(accountLine.getReceivableSellDeviation()!=null && accountLineNew.getContractRateAmmount()!=null &&  accountLine.getReceivableSellDeviation().doubleValue()>0 && accountLineNew.getContractRateAmmount().doubleValue()>0){
					  accountLineNew.setContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getContractRateAmmount().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));	 
					 }else{
						 
					 }
				accountLineNew.setActualRevenue(new BigDecimal(decimalFormat.format(accountLineNew.getContractRateAmmount().doubleValue()/accountLineNew.getContractExchangeRate().doubleValue())));
				accountLineNew.setRacValueDate(new Date());
				String recExchangeRate="1"; 
	            BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
	            List recRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRecRateCurrency());
				 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
				 {
					 recExchangeRate=recRate.get(0).toString();
				 }
				recExchangeRateBig=new BigDecimal(recExchangeRate);
				accountLineNew.setRecRateExchange(recExchangeRateBig);
				accountLineNew.setRacValueDate(new Date());
				accountLineNew.setRecCurrencyRate(accountLineNew.getRecRate().multiply(recExchangeRateBig));
				accountLineNew.setActualRevenueForeign(accountLineNew.getActualRevenue().multiply(recExchangeRateBig));
				try{
					if(accountLineNew.getEstimateSellDeviation()!=null && accountLineNew.getEstimateSellDeviation().doubleValue()>0 ){
						if(accountLineNew.getEstimateRevenueAmount()!=null && accountLineNew.getEstimateRevenueAmount().doubleValue()>0)
							accountLineNew.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateRevenueAmount().doubleValue())*((accountLineNew.getEstimateSellDeviation().doubleValue())/100))));
						if(accountLineNew.getEstSellLocalAmount()!=null && accountLineNew.getEstSellLocalAmount().doubleValue()>0)
							accountLineNew.setEstSellLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstSellLocalAmount().doubleValue())*((accountLineNew.getEstimateSellDeviation().doubleValue())/100))));
						if(accountLineNew.getEstimateContractRateAmmount()!=null && accountLineNew.getEstimateContractRateAmmount().doubleValue()>0)
							accountLineNew.setEstimateContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateContractRateAmmount().doubleValue())*((accountLineNew.getEstimateSellDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getEstimateDeviation()!=null && accountLineNew.getEstimateDeviation().doubleValue()>0 ){
						if(accountLineNew.getEstimateExpense()!=null && accountLineNew.getEstimateExpense().doubleValue()>0)
							accountLineNew.setEstimateExpense(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateExpense().doubleValue())*((accountLineNew.getEstimateDeviation().doubleValue())/100))));
						if(accountLineNew.getEstLocalAmount()!=null && accountLineNew.getEstLocalAmount().doubleValue()>0)
							accountLineNew.setEstLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstLocalAmount().doubleValue())*((accountLineNew.getEstimateDeviation().doubleValue())/100))));
						if(accountLineNew.getEstimatePayableContractRateAmmount()!=null && accountLineNew.getEstimatePayableContractRateAmmount().doubleValue()>0)
							accountLineNew.setEstimatePayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimatePayableContractRateAmmount().doubleValue())*((accountLineNew.getEstimateDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getRevisionSellDeviation()!=null && accountLineNew.getRevisionSellDeviation().doubleValue()>0 ){
						if(accountLineNew.getRevisionRevenueAmount()!=null && accountLineNew.getRevisionRevenueAmount().doubleValue()>0)
							accountLineNew.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionRevenueAmount().doubleValue())*((accountLineNew.getRevisionSellDeviation().doubleValue())/100))));
						if(accountLineNew.getRevisionSellLocalAmount()!=null && accountLineNew.getRevisionSellLocalAmount().doubleValue()>0)
							accountLineNew.setRevisionSellLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionSellLocalAmount().doubleValue())*((accountLineNew.getRevisionSellDeviation().doubleValue())/100))));
						if(accountLineNew.getRevisionContractRateAmmount()!=null && accountLineNew.getRevisionContractRateAmmount().doubleValue()>0)
							accountLineNew.setRevisionContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionContractRateAmmount().doubleValue())*((accountLineNew.getRevisionSellDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getRevisionDeviation()!=null && accountLineNew.getRevisionDeviation().doubleValue()>0 ){
						if(accountLineNew.getRevisionExpense()!=null && accountLineNew.getRevisionExpense().doubleValue()>0)
							accountLineNew.setRevisionExpense(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionExpense().doubleValue())*((accountLineNew.getRevisionDeviation().doubleValue())/100))));
						if(accountLineNew.getRevisionLocalAmount()!=null && accountLineNew.getRevisionLocalAmount().doubleValue()>0)
							accountLineNew.setRevisionLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionLocalAmount().doubleValue())*((accountLineNew.getRevisionDeviation().doubleValue())/100))));
						if(accountLineNew.getRevisionPayableContractRateAmmount()!=null && accountLineNew.getRevisionPayableContractRateAmmount().doubleValue()>0)
							accountLineNew.setRevisionPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionPayableContractRateAmmount().doubleValue())*((accountLineNew.getRevisionDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getReceivableSellDeviation()!=null && accountLineNew.getReceivableSellDeviation().doubleValue()>0 ){
						if(accountLineNew.getActualRevenue()!=null && accountLineNew.getActualRevenue().doubleValue()>0)
							accountLineNew.setActualRevenue(new BigDecimal(decimalFormat.format((accountLineNew.getActualRevenue().doubleValue())*((accountLineNew.getReceivableSellDeviation().doubleValue())/100))));
						if(accountLineNew.getActualRevenueForeign()!=null && accountLineNew.getActualRevenueForeign().doubleValue()>0)
							accountLineNew.setActualRevenueForeign(new BigDecimal(decimalFormat.format((accountLineNew.getActualRevenueForeign().doubleValue())*((accountLineNew.getReceivableSellDeviation().doubleValue())/100))));
						if(accountLineNew.getContractRateAmmount()!=null && accountLineNew.getContractRateAmmount().doubleValue()>0)
							accountLineNew.setContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getContractRateAmmount().doubleValue())*((accountLineNew.getReceivableSellDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getPayDeviation()!=null && accountLineNew.getPayDeviation().doubleValue()>0 ){
						if(accountLineNew.getActualExpense()!=null && accountLineNew.getActualExpense().doubleValue()>0)
							accountLineNew.setActualExpense(new BigDecimal(decimalFormat.format((accountLineNew.getActualExpense().doubleValue())*((accountLineNew.getPayDeviation().doubleValue())/100))));
						if(accountLineNew.getLocalAmount()!=null && accountLineNew.getLocalAmount().doubleValue()>0)
							accountLineNew.setLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getLocalAmount().doubleValue())*((accountLineNew.getPayDeviation().doubleValue())/100))));
						if(accountLineNew.getPayableContractRateAmmount()!=null && accountLineNew.getPayableContractRateAmmount().doubleValue()>0)
							accountLineNew.setPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getPayableContractRateAmmount().doubleValue())*((accountLineNew.getPayDeviation().doubleValue())/100))));	
					} 
				}catch(Exception e){
					e.printStackTrace();
	    		}	
				
				
				  try{
			    		if(accountLineNew.getEstimateRevenueAmount() !=null && accountLineNew.getEstimateRevenueAmount().doubleValue()>0 && accountLineNew.getEstimateExpense()!=null && accountLineNew.getEstimateExpense().doubleValue()>0){
			    			Double estimatePassPercentageValue=(accountLineNew.getEstimateRevenueAmount().doubleValue()/accountLineNew.getEstimateExpense().doubleValue())*100;
			    			Integer estimatePassPercentage=estimatePassPercentageValue.intValue();
			    			accountLineNew.setEstimatePassPercentage(estimatePassPercentage);
				            }
				            if(accountLineNew.getRevisionRevenueAmount() !=null && accountLineNew.getRevisionRevenueAmount().doubleValue()>0 && accountLineNew.getRevisionExpense()!=null &&  accountLineNew.getRevisionExpense().doubleValue()>0){
				            	Double revisionPassPercentageValue=(accountLineNew.getRevisionRevenueAmount().doubleValue()/accountLineNew.getRevisionExpense().doubleValue())*100;
				            	Integer revisionPassPercentage=revisionPassPercentageValue.intValue();
				            	accountLineNew.setRevisionPassPercentage(revisionPassPercentage);
				            }
			    		}catch(Exception e){
			    			e.printStackTrace();
			    		}	
				 
			    try{
			    	
			    	
			    String payVatPercent=accountLineNew.getPayVatPercent();
			    BigDecimal payVatPercentBig=new BigDecimal("0.00");
			    BigDecimal payVatAmmountBig=new BigDecimal("0.00");
			    BigDecimal estPayVatAmmountBig=new BigDecimal("0.00");
				BigDecimal revisionpayVatAmmountBig=new BigDecimal("0.00");
			    if(payVatPercent!=null && (!(payVatPercent.trim().equals("")))){
			    	payVatPercentBig=new BigDecimal(payVatPercent);
			    }
			    payVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
			    try{
					    estPayVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getEstLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
					    revisionpayVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getRevisionLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
					    
					  }catch(Exception e){
							logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					    }
			    accountLineNew.setPayVatAmt(payVatAmmountBig);
			    accountLineNew.setEstExpVatAmt(estPayVatAmmountBig);
			    accountLineNew.setRevisionExpVatAmt(revisionpayVatAmmountBig);
			  
			    }catch(Exception e){
			    	e.printStackTrace();
			    }
	            accountLineNew.setServiceOrderId(serviceOrderToRecods.getId());
	            accountLineNew.setSequenceNumber(serviceOrderToRecods.getSequenceNumber());
	            accountLineNew.setShipNumber(serviceOrderToRecods.getShipNumber());
	            accountLineNew.setServiceOrder(serviceOrderToRecods);
	            
	            try{
	            	String recVatPercent="0";
	            	recVatPercent=accountLineNew.getRecVatPercent();
				    BigDecimal recVatPercentBig=new BigDecimal("0.00");
				    BigDecimal recVatAmmountBig=new BigDecimal("0.00");
				    BigDecimal revisionVatAmmountBig=new BigDecimal("0.00");
				    BigDecimal estVatAmmountBig=new BigDecimal("0.00");
				    if(recVatPercent!=null && (!(recVatPercent.trim().equals("")))){
				    	recVatPercentBig=new BigDecimal(recVatPercent);
				    }
				    DecimalFormat decimalFormat = new DecimalFormat("#.####");
				    recVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getActualRevenueForeign().multiply(recVatPercentBig)).doubleValue())/100)));
				    try{
				    estVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getEstSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
				    revisionVatAmmountBig = (new BigDecimal(decimalFormat.format(((accountLineNew.getRevisionSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
				    }catch(Exception e){
				    	e.printStackTrace();
				    }
				    accountLineNew.setRecVatAmt(recVatAmmountBig); 
				    accountLineNew.setEstVatAmt(estVatAmmountBig); 
				    accountLineNew.setRevisionVatAmt(revisionVatAmmountBig);
				    }catch(Exception e){
				    	e.printStackTrace();
				    }
	            
	            accountLineNew=accountLineManager.save(accountLineNew);
	            updateExternalAccountLine(serviceOrderToRecods,externalCorpID);
        	}
    		}catch(Exception e){
    	    	 e.printStackTrace();
    			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
    		}
    	} 
	}
	
	@SkipValidation
    public void updateExternalAccountLine( ServiceOrder externalServiceOrder,String externalCorpID){ 
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			BigDecimal cuDivisormar=new BigDecimal(100);
			 
			externalServiceOrder.setEstimatedTotalRevenue(new BigDecimal((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			externalServiceOrder.setDistributedTotalAmount(new BigDecimal((accountLineManager.getExternaldistributedAmountSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			externalServiceOrder.setProjectedDistributedTotalAmount(new BigDecimal((accountLineManager.getExternalProjectedDistributedTotalAmountSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			externalServiceOrder.setProjectedActualExpense(new BigDecimal((accountLineManager.getExternalProjectedActualExpenseSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			externalServiceOrder.setProjectedActualRevenue(new BigDecimal((accountLineManager.getExternalProjectedActualRevenueSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			BigDecimal divisormarEst=new BigDecimal(((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())); 
			if(externalServiceOrder.getEstimatedTotalRevenue()==null || externalServiceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0"))||externalServiceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.00")) || externalServiceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.0000"))){
				externalServiceOrder.setEstimatedGrossMarginPercentage(new BigDecimal("0"));
			} 
			else 
			{
				externalServiceOrder.setEstimatedGrossMarginPercentage((((new BigDecimal((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalEstimateExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())))).multiply(cuDivisormar)).divide(divisormarEst,2));
			} 
			externalServiceOrder.setEstimatedGrossMargin((new BigDecimal((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalEstimateExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()))));
			externalServiceOrder.setRevisedGrossMargin((new BigDecimal((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalRevisionExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()))));
			externalServiceOrder.setRevisedTotalRevenue(new BigDecimal((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			BigDecimal divisormarEstPer=new BigDecimal(((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			if(externalServiceOrder.getRevisedTotalRevenue() == null || externalServiceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0"))||externalServiceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.00")) || externalServiceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.0000")) ){
				externalServiceOrder.setRevisedGrossMarginPercentage(new BigDecimal("0"));
			}
			else{
				externalServiceOrder.setRevisedGrossMarginPercentage((((new BigDecimal((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalRevisionExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())))).multiply(cuDivisormar)).divide(divisormarEstPer,2));
			} 
			/*# 7784 - Always show actual gross margin in accountline overview Start*/
			try{
				externalServiceOrder.setProjectedGrossMargin(accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"EXP")));
			if(accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"REV").doubleValue()!=0){
				externalServiceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((externalServiceOrder.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"REV")).doubleValue())*100));
			}else{
				externalServiceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
			}
			}catch(Exception e){e.printStackTrace();}
			/*# 7784 - Always show actual gross margin in accountline overview End*/

			externalServiceOrder.setRevisedTotalExpense(new BigDecimal((accountLineManager.getExternalRevisionExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			externalServiceOrder.setEstimatedTotalExpense(new BigDecimal((accountLineManager.getExternalEstimateExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			externalServiceOrder.setEntitledTotalAmount(new BigDecimal((accountLineManager.getExternalEntitleSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			externalServiceOrder.setActualExpense(new BigDecimal((accountLineManager.getExternalActualExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			externalServiceOrder.setActualRevenue(new BigDecimal((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			externalServiceOrder.setActualGrossMargin((new BigDecimal((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalActualExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()))));
			BigDecimal divisormar=new BigDecimal(((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
			if(externalServiceOrder.getActualRevenue()== null || externalServiceOrder.getActualRevenue().equals(new BigDecimal("0"))||externalServiceOrder.getActualRevenue().equals(new BigDecimal("0.00")) || externalServiceOrder.getActualRevenue().equals(new BigDecimal("0.0000"))){
				externalServiceOrder.setActualGrossMarginPercentage(new BigDecimal("0"));
			}
			else{
				externalServiceOrder.setActualGrossMarginPercentage((((new BigDecimal((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalActualExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())))).multiply(cuDivisormar)).divide(divisormar,2));
			}
			serviceOrderManager.updateFromAccountLine(externalServiceOrder); 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}  
    } 
	
	@SkipValidation
    public void synchornizeAccountLine(AccountLine synchedAccountLine, AccountLine accountLine) {
		
		try{
		Date todayDate=new Date();
		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
		String dt11=sdfDestination.format(todayDate);
	    todayDate=sdfDestination.parse(dt11);
		Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
  		boolean accEditable=true;
  		boolean accPayEditable=true;
		 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
			 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
				 accEditable=false;
			 }
			 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
				 accPayEditable=false;
			 }
		 }else{ 
  		 if(synchedAccountLine.getRecAccDate()!=null ){
  			accEditable=false;
  		 }
  		if(synchedAccountLine.getPayAccDate()!=null ){
  			accPayEditable=false;
  		 }
		 }
		if(accEditable && accPayEditable){
			Boolean fXRateOnActualizationDate=false;
			try{
			fXRateOnActualizationDate=billingManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId()).getfXRateOnActualizationDate();
			}catch(Exception e){e.printStackTrace();}
		boolean chargeCodeFlag=false;
		if((!(accountLine.getChargeCode().equals(synchedAccountLine.getChargeCode())))){
	    	   chargeCodeFlag=true;  
	       }
		boolean EstSellCurrencyFlag=false;
		BigDecimal estExchangeRateBig=synchedAccountLine.getEstSellExchangeRate();
		if((synchedAccountLine.getEstSellCurrency()!=null && accountLine.getEstSellCurrency()!=null && (!(synchedAccountLine.getEstSellCurrency().toString().trim().equalsIgnoreCase(accountLine.getEstSellCurrency().toString().trim())))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getEstSellLocalRate() !=null && synchedAccountLine.getEstSellLocalRate()!=null &&   (accountLine.getEstSellLocalRate().doubleValue() != synchedAccountLine.getEstSellLocalRate().doubleValue()))  || (accountLine.getEstSellValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getEstSellValueDate())))==0) ){
			EstSellCurrencyFlag=true;
		}
		boolean revisionSellCurrencyFlag=false;
		BigDecimal revisionExchangeRateBig=synchedAccountLine.getRevisionSellExchangeRate();
		if((synchedAccountLine.getRevisionSellCurrency()!=null && accountLine.getRevisionSellCurrency()!=null && (!(synchedAccountLine.getRevisionSellCurrency().toString().trim().equalsIgnoreCase(accountLine.getRevisionSellCurrency().toString().trim())))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getRevisionSellLocalRate() !=null && synchedAccountLine.getRevisionSellLocalRate()!=null &&   (accountLine.getRevisionSellLocalRate().doubleValue() != synchedAccountLine.getRevisionSellLocalRate().doubleValue())) || (accountLine.getRevisionSellValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRevisionSellValueDate())))==0)){
			revisionSellCurrencyFlag=true;
		}
		boolean recRateCurrencyFlag=false;
		BigDecimal recExchangeRateBig=synchedAccountLine.getRecRateExchange();
		if((synchedAccountLine.getRecRateCurrency()!=null && accountLine.getRecRateCurrency()!=null && (!(synchedAccountLine.getRecRateCurrency().toString().trim().equalsIgnoreCase(accountLine.getRecRateCurrency().toString().trim())))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getRecCurrencyRate() !=null && synchedAccountLine.getRecCurrencyRate()!=null &&   (accountLine.getRecCurrencyRate().doubleValue() != synchedAccountLine.getRecCurrencyRate().doubleValue())) || (accountLine.getRacValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRacValueDate())))==0)){
			 recRateCurrencyFlag=true;
		}
		String externalCorpID = synchedAccountLine.getCorpID();
		UTSIeuVatPercentList=refMasterManager.findVatPercentList(externalCorpID, "EUVAT");
		UTSIpayVatPercentList = refMasterManager.findVatPercentList(externalCorpID, "PAYVATDESC");
		String externalBaseCurrency=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
		List fieldToSync=customerFileManager.findAccFieldToSync("AccountLine","CMM"); 
    		Iterator it1=fieldToSync.iterator();
    		while(it1.hasNext()){
    			String field=it1.next().toString();
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
    			try{
    	
    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
    				try{
    					if(fieldFrom!=null && fieldTo!=null && fieldFrom.trim().equalsIgnoreCase("recRateExchange") && fieldTo.trim().equalsIgnoreCase("exchangeRate"))	{
    						BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();  
    		            	if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
    		            		payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
    		            		synchedAccountLine.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
    							} 
    					}else{		
    				        beanUtilsBean.setProperty(synchedAccountLine,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
    					}
    				}catch(Exception ex){
					
						 ex.printStackTrace();
					}
    				
    			}catch(Exception ex){
    			
    				ex.printStackTrace();
    			}
    			
    		}
    		try{
        	    if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
        	    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
        	    	synchedAccountLine.setUpdatedOn(new Date());
        	    }}catch( Exception e){
        	    	
        	    }
    		try{
    			if(chargeCodeFlag){
    		   ServiceOrder  soObj = serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
    			billingRecods =billingManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
    
        			String chargeStr="";
    				List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),synchedAccountLine.getChargeCode(),synchedAccountLine.getCorpID(),soObj.getJob(),soObj.getRouting(),soObj.getCompanyDivision());
    				if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
    					  chargeStr= glList.get(0).toString();
    				  }
    				   if(!chargeStr.equalsIgnoreCase("")){
    					  String [] chrageDetailArr = chargeStr.split("#");
    					  synchedAccountLine.setRecGl(chrageDetailArr[1]);
    					  synchedAccountLine.setPayGl(chrageDetailArr[2]);
    					}else{
    						synchedAccountLine.setRecGl("");
    						synchedAccountLine.setPayGl("");
    				 }
        		}
    			}
				//}
		catch(Exception e){
			e.printStackTrace();
				}
    		
    		if(synchedAccountLine.getEstimateRevenueAmount() !=null && synchedAccountLine.getEstimateRevenueAmount().doubleValue()>0){
    			synchedAccountLine.setEstimatePassPercentage(100);
	            }
	            if(synchedAccountLine.getRevisionRevenueAmount() !=null && synchedAccountLine.getRevisionRevenueAmount().doubleValue()>0){
	            	synchedAccountLine.setRevisionPassPercentage(100);
	            }
              
	           
		            try{
		            	BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();  
		            	if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
		            		payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
		            		synchedAccountLine.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
							}
		            	//synchedAccountLine.setActualExpense(synchedAccountLine.getLocalAmount().divide(accountLine.getRecRateExchange(),2));	
		            }catch(Exception e){
		            	e.printStackTrace();
		            }
		            
		            if(externalBaseCurrency !=null && baseCurrency!=null && (!(baseCurrency.trim().equalsIgnoreCase(externalBaseCurrency.trim())))){
		            	DecimalFormat decimalFormatExchangeRate = new DecimalFormat("#.####");
		            	if(EstSellCurrencyFlag){
						String estExchangeRate="1"; 
			            
			            List estRate=exchangeRateManager.findAccExchangeRate(externalCorpID,synchedAccountLine.getEstSellCurrency());
						 if((estRate!=null)&&(!estRate.isEmpty())&& estRate.get(0)!=null && (!(estRate.get(0).toString().equals(""))))
						 {
							 estExchangeRate=estRate.get(0).toString();
						 
						 } 
						estExchangeRateBig=new BigDecimal(estExchangeRate);	
		            	}
		            	synchedAccountLine.setEstSellExchangeRate(estExchangeRateBig);
		            	synchedAccountLine.setEstExchangeRate(estExchangeRateBig);
		            	if(accountLine.getEstSellLocalRate() !=null && accountLine.getEstSellLocalRate().doubleValue()!=0){
		            	synchedAccountLine.setEstimateSellRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
		            	synchedAccountLine.setEstimateRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
		            	}
		            	if(accountLine.getEstSellLocalAmount() !=null && accountLine.getEstSellLocalAmount().doubleValue() !=0){
		            	synchedAccountLine.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
		            	synchedAccountLine.setEstimateExpense(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
		            	} 
		            	
		            	BigDecimal estcontractExchangeRateBig=new BigDecimal("1");	
		            	try{ 
							estcontractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((synchedAccountLine.getEstimateContractRate().doubleValue())/(synchedAccountLine.getEstimateSellRate().doubleValue())))); 
						}catch(Exception es){
							es.printStackTrace();
						} 
		            	synchedAccountLine.setEstimateContractExchangeRate(estcontractExchangeRateBig);
						synchedAccountLine.setEstimatePayableContractExchangeRate(estcontractExchangeRateBig);
						
						if(revisionSellCurrencyFlag){
						String revisionExchangeRate="1"; 
			             
			            List revisionRate=exchangeRateManager.findAccExchangeRate(externalCorpID,synchedAccountLine.getRevisionSellCurrency());
						 if((revisionRate!=null)&&(!revisionRate.isEmpty())&& revisionRate.get(0)!=null && (!(revisionRate.get(0).toString().equals(""))))
						 {
							 revisionExchangeRate=revisionRate.get(0).toString();
						 
						 } 
						 revisionExchangeRateBig=new BigDecimal(revisionExchangeRate);	
						}
						synchedAccountLine.setRevisionSellExchangeRate(revisionExchangeRateBig);
						synchedAccountLine.setRevisionExchangeRate(revisionExchangeRateBig);
						if(accountLine.getRevisionSellLocalRate() !=null && accountLine.getRevisionSellLocalRate().doubleValue()!=0){
						synchedAccountLine.setRevisionSellRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
						synchedAccountLine.setRevisionRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
						}
						if(accountLine.getRevisionSellLocalAmount() !=null && accountLine.getRevisionSellLocalAmount().doubleValue()!=0){
						synchedAccountLine.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
						synchedAccountLine.setRevisionExpense(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
						} 
						BigDecimal revisioncontractExchangeRateBig=new BigDecimal("1");	
						try{ 
							revisioncontractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((synchedAccountLine.getRevisionContractRate().doubleValue())/(synchedAccountLine.getRevisionSellRate().doubleValue())))); 
						}catch(Exception es){
							es.printStackTrace();
						}
						synchedAccountLine.setRevisionContractExchangeRate(revisioncontractExchangeRateBig);
						synchedAccountLine.setRevisionPayableContractExchangeRate(revisioncontractExchangeRateBig);  
						if(recRateCurrencyFlag){
						String recExchangeRate="1";  
			            List recRate=exchangeRateManager.findAccExchangeRate(externalCorpID,synchedAccountLine.getRecRateCurrency());
						 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
						 {
							 recExchangeRate=recRate.get(0).toString();
						 
						 } 
						recExchangeRateBig=new BigDecimal(recExchangeRate);	
						}
						synchedAccountLine.setRecRateExchange(recExchangeRateBig);
						BigDecimal payExchangeRateBig=synchedAccountLine.getRecRateExchange();
						if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
							payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
							synchedAccountLine.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
						}
						if(accountLine.getRecCurrencyRate() !=null && accountLine.getRecCurrencyRate().doubleValue()!=0){
						synchedAccountLine.setRecRate(new BigDecimal(decimalFormatExchangeRate.format((accountLine.getRecCurrencyRate().doubleValue())/(recExchangeRateBig.doubleValue()))));
						}
						if(accountLine.getActualRevenueForeign() !=null && accountLine.getActualRevenueForeign().doubleValue()!=0){
						synchedAccountLine.setActualRevenue(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue())/(recExchangeRateBig.doubleValue()))));
						try{
							synchedAccountLine.setActualExpense(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue())/(recExchangeRateBig.doubleValue()))));
						}catch(Exception e){
							e.printStackTrace();
			    		}
						} 
						
						BigDecimal contractExchangeRateBig=new BigDecimal("1");	
						try{ 
							contractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((synchedAccountLine.getContractRate().doubleValue())/(synchedAccountLine.getRecRate().doubleValue())))); 
						}catch(Exception es){
							es.printStackTrace();
						}
						synchedAccountLine.setContractExchangeRate(contractExchangeRateBig);
						synchedAccountLine.setPayableContractExchangeRate(contractExchangeRateBig);

					}
    		
    		try{
    	    if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
    	    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
    	    	synchedAccountLine.setUpdatedOn(new Date());
    	    }}catch( Exception e){
    	    	
    	    } 
    	    try{
    	    String partnertype="";
    	    if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
      		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
      	    }
    	    if(partnertype !=null && (!(partnertype.toString().trim().equals("")))){
    	    		
    	    }else{
    	    	synchedAccountLine.setStatus(false)	;
    	    }
    	    }catch(Exception e){
    	    	e.printStackTrace();
    	    }
    	    if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){
    	    if(UTSIeuVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){	
    	    	synchedAccountLine.setRecVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
    	    }
    	    else{
    	    	synchedAccountLine.setRecVatDescr("");
    	    	synchedAccountLine.setRecVatPercent("0");
    	    	synchedAccountLine.setRecVatAmt(new BigDecimal(0));
    	    	synchedAccountLine.setEstVatAmt(new BigDecimal(0));
    	    	synchedAccountLine.setRevisionVatAmt(new BigDecimal(0));
				}
		    if(UTSIpayVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){ 
    	    synchedAccountLine.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
		    }
		    else{
		    	synchedAccountLine.setPayVatDescr("");
		    	synchedAccountLine.setPayVatPercent("0");
		    	synchedAccountLine.setPayVatAmt(new BigDecimal(0));
		    	synchedAccountLine.setEstExpVatAmt(new BigDecimal(0)); 
		    	synchedAccountLine.setRevisionExpVatAmt(new BigDecimal(0));
				}
    	    }
    	    synchedAccountLine=accountLineManager.save(synchedAccountLine);
    	    serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
    	    updateExternalAccountLine(serviceOrderToRecods,synchedAccountLine.getCorpID());
		}
		
		else{

			Boolean fXRateOnActualizationDate=false;
			try{
			fXRateOnActualizationDate=billingManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId()).getfXRateOnActualizationDate();
			}catch(Exception e){e.printStackTrace();}
		boolean chargeCodeFlag=false;
		if((!(accountLine.getChargeCode().equals(synchedAccountLine.getChargeCode())))){
	    	   chargeCodeFlag=true;  
	       }
		boolean EstSellCurrencyFlag=false;
		BigDecimal estExchangeRateBig=synchedAccountLine.getEstSellExchangeRate();
		if((synchedAccountLine.getEstSellCurrency()!=null && accountLine.getEstSellCurrency()!=null && (!(synchedAccountLine.getEstSellCurrency().toString().trim().equalsIgnoreCase(accountLine.getEstSellCurrency().toString().trim())))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getEstSellLocalRate() !=null && synchedAccountLine.getEstSellLocalRate()!=null &&   (accountLine.getEstSellLocalRate().doubleValue() != synchedAccountLine.getEstSellLocalRate().doubleValue()))  || (accountLine.getEstSellValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getEstSellValueDate())))==0) ){
			EstSellCurrencyFlag=true;
		}
		boolean revisionSellCurrencyFlag=false;
		BigDecimal revisionExchangeRateBig=synchedAccountLine.getRevisionSellExchangeRate();
		if((synchedAccountLine.getRevisionSellCurrency()!=null && accountLine.getRevisionSellCurrency()!=null && (!(synchedAccountLine.getRevisionSellCurrency().toString().trim().equalsIgnoreCase(accountLine.getRevisionSellCurrency().toString().trim())))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getRevisionSellLocalRate() !=null && synchedAccountLine.getRevisionSellLocalRate()!=null &&   (accountLine.getRevisionSellLocalRate().doubleValue() != synchedAccountLine.getRevisionSellLocalRate().doubleValue())) || (accountLine.getRevisionSellValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRevisionSellValueDate())))==0)){
			revisionSellCurrencyFlag=true;
		}
		boolean recRateCurrencyFlag=false;
		BigDecimal recExchangeRateBig=synchedAccountLine.getRecRateExchange();
		if((synchedAccountLine.getRecRateCurrency()!=null && accountLine.getRecRateCurrency()!=null && (!(synchedAccountLine.getRecRateCurrency().toString().trim().equalsIgnoreCase(accountLine.getRecRateCurrency().toString().trim())))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getRecCurrencyRate() !=null && synchedAccountLine.getRecCurrencyRate()!=null &&   (accountLine.getRecCurrencyRate().doubleValue() != synchedAccountLine.getRecCurrencyRate().doubleValue())) || (accountLine.getRacValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRacValueDate())))==0)){
			 recRateCurrencyFlag=true;
		}
		String externalCorpID = synchedAccountLine.getCorpID();
		UTSIeuVatPercentList=refMasterManager.findVatPercentList(externalCorpID, "EUVAT");
		UTSIpayVatPercentList = refMasterManager.findVatPercentList(externalCorpID, "PAYVATDESC");
		String externalBaseCurrency=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
		List fieldToSync=customerFileManager.findAccEstimateRevisionFieldToSync("AccountLine","CMM"); 
    		Iterator it1=fieldToSync.iterator();
    		while(it1.hasNext()){
    			String field=it1.next().toString();
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
    			try{ 
    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
    				try{
    				beanUtilsBean.setProperty(synchedAccountLine,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
    				}catch(Exception ex){
    					ex.printStackTrace();
					}
    				
    			}catch(Exception ex){
    			
    				ex.printStackTrace();
    			}
    			
    		}
    		try{
        	    if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
        	    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
        	    	synchedAccountLine.setUpdatedOn(new Date());
        	    }}catch( Exception e){
        	    	
        	    } 
    		
    		if(synchedAccountLine.getEstimateRevenueAmount() !=null && synchedAccountLine.getEstimateRevenueAmount().doubleValue()>0){
    			synchedAccountLine.setEstimatePassPercentage(100);
	            }
	            if(synchedAccountLine.getRevisionRevenueAmount() !=null && synchedAccountLine.getRevisionRevenueAmount().doubleValue()>0){
	            	synchedAccountLine.setRevisionPassPercentage(100);
	            }  
		            try{
		            	BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();  
		            	if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
		            		payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
		            		synchedAccountLine.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
							} 
		            }catch(Exception e){
		            	e.printStackTrace();
		            }
		            
		            if(externalBaseCurrency !=null && baseCurrency!=null && (!(baseCurrency.trim().equalsIgnoreCase(externalBaseCurrency.trim())))){
		            	DecimalFormat decimalFormatExchangeRate = new DecimalFormat("#.####");
		            	if(EstSellCurrencyFlag){
						String estExchangeRate="1"; 
			            
			            List estRate=exchangeRateManager.findAccExchangeRate(externalCorpID,synchedAccountLine.getEstSellCurrency());
						 if((estRate!=null)&&(!estRate.isEmpty())&& estRate.get(0)!=null && (!(estRate.get(0).toString().equals(""))))
						 {
							 estExchangeRate=estRate.get(0).toString();
						 
						 } 
						estExchangeRateBig=new BigDecimal(estExchangeRate);	
		            	}
		            	synchedAccountLine.setEstSellExchangeRate(estExchangeRateBig);
		            	synchedAccountLine.setEstExchangeRate(estExchangeRateBig);
		            	if(accountLine.getEstSellLocalRate() !=null && accountLine.getEstSellLocalRate().doubleValue()!=0){
		            	synchedAccountLine.setEstimateSellRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
		            	synchedAccountLine.setEstimateRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
		            	}
		            	if(accountLine.getEstSellLocalAmount() !=null && accountLine.getEstSellLocalAmount().doubleValue() !=0){
		            	synchedAccountLine.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
		            	synchedAccountLine.setEstimateExpense(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
		            	} 
		            	
		            	BigDecimal estcontractExchangeRateBig=new BigDecimal("1");	
		            	try{ 
							estcontractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((synchedAccountLine.getEstimateContractRate().doubleValue())/(synchedAccountLine.getEstimateSellRate().doubleValue())))); 
						}catch(Exception es){
							es.printStackTrace();
						} 
		            	synchedAccountLine.setEstimateContractExchangeRate(estcontractExchangeRateBig);
						synchedAccountLine.setEstimatePayableContractExchangeRate(estcontractExchangeRateBig);
						
						if(revisionSellCurrencyFlag){
						String revisionExchangeRate="1"; 
			             
			            List revisionRate=exchangeRateManager.findAccExchangeRate(externalCorpID,synchedAccountLine.getRevisionSellCurrency());
						 if((revisionRate!=null)&&(!revisionRate.isEmpty())&& revisionRate.get(0)!=null && (!(revisionRate.get(0).toString().equals(""))))
						 {
							 revisionExchangeRate=revisionRate.get(0).toString();
						 
						 } 
						 revisionExchangeRateBig=new BigDecimal(revisionExchangeRate);	
						}
						synchedAccountLine.setRevisionSellExchangeRate(revisionExchangeRateBig);
						synchedAccountLine.setRevisionExchangeRate(revisionExchangeRateBig);
						if(accountLine.getRevisionSellLocalRate() !=null && accountLine.getRevisionSellLocalRate().doubleValue()!=0){
						synchedAccountLine.setRevisionSellRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
						synchedAccountLine.setRevisionRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
						}
						if(accountLine.getRevisionSellLocalAmount() !=null && accountLine.getRevisionSellLocalAmount().doubleValue()!=0){
						synchedAccountLine.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
						synchedAccountLine.setRevisionExpense(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
						} 
						BigDecimal revisioncontractExchangeRateBig=new BigDecimal("1");	
						try{ 
							revisioncontractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((synchedAccountLine.getRevisionContractRate().doubleValue())/(synchedAccountLine.getRevisionSellRate().doubleValue())))); 
						}catch(Exception es){
							es.printStackTrace();
						}
						synchedAccountLine.setRevisionContractExchangeRate(revisioncontractExchangeRateBig);
						synchedAccountLine.setRevisionPayableContractExchangeRate(revisioncontractExchangeRateBig);  

					}
    		
    		try{
    	    if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
    	    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
    	    	synchedAccountLine.setUpdatedOn(new Date());
    	    }}catch( Exception e){
    	    	
    	    } 
    	    try{
    	    String partnertype="";
    	    if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
      		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
      	    }
    	    if(partnertype !=null && (!(partnertype.toString().trim().equals("")))){
    	    		
    	    }else{
    	    	synchedAccountLine.setStatus(false)	;
    	    }
    	    }catch(Exception e){
    	    	e.printStackTrace();
    	    }
    	 
    	    synchedAccountLine=accountLineManager.save(synchedAccountLine);
    	    serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
    	    updateExternalAccountLine(serviceOrderToRecods,synchedAccountLine.getCorpID());
		
		}
		}catch(Exception e){
	    	 e.printStackTrace();
		}
    	    }
	
	@SkipValidation
    private void synchornizeDMMNetWorkAccountLine(AccountLine synchedAccountLine, AccountLine accountLine) {
       
       try {
		boolean estimateContractCurrencyFlag=false;
		   boolean estimateBillingCurrencyFlag=false;
		   boolean revisionContractCurrencyFlag=false;
		   boolean revisionBillingCurrencyFlag=false;
		   boolean contractCurrencyFlag=false;
		   boolean  billingCurrencyFlag=false;
		   boolean chargeCodeFlag=false;
		   if((!(accountLine.getEstimateContractCurrency().equals(synchedAccountLine.getEstimateContractCurrency()))) || (!(accountLine.getEstimateContractValueDate().equals(synchedAccountLine.getEstimateContractValueDate())))){
			   estimateContractCurrencyFlag=true;
		   }
		   if((!(accountLine.getEstSellCurrency().equals(synchedAccountLine.getEstSellCurrency()))) || (!(accountLine.getEstSellValueDate().equals(synchedAccountLine.getEstSellValueDate())))){
			   estimateBillingCurrencyFlag=true;
		   }
		   if((!(accountLine.getRevisionContractCurrency().equals(synchedAccountLine.getRevisionContractCurrency()))) || (!(accountLine.getRevisionContractValueDate().equals(synchedAccountLine.getRevisionContractValueDate())))){
			   revisionContractCurrencyFlag=true;
		   }
		   if((!(accountLine.getRevisionSellCurrency().equals(synchedAccountLine.getRevisionSellCurrency()))) || (!(accountLine.getRevisionSellValueDate().equals(synchedAccountLine.getRevisionSellValueDate())))){
			   revisionBillingCurrencyFlag=true;
		   }
		   if((!(accountLine.getContractCurrency().equals(synchedAccountLine.getContractCurrency()))) || (!(accountLine.getContractValueDate().equals(synchedAccountLine.getContractValueDate())))){
			   contractCurrencyFlag=true;
		   }
		   if((!(accountLine.getRecRateCurrency().equals(synchedAccountLine.getRecRateCurrency()))) || (!(accountLine.getRacValueDate().equals(synchedAccountLine.getRacValueDate())))){
			   billingCurrencyFlag=true;
		   }
		   if((!(accountLine.getChargeCode().equals(synchedAccountLine.getChargeCode())))){
			   chargeCodeFlag=true;  
		   }

			List fieldToSync=customerFileManager.findAccDMMNetWorkFieldToSync("AccountLine","DMM"); 
				Iterator it1=fieldToSync.iterator();
				while(it1.hasNext()){
					String field=it1.next().toString();
					String[] splitField=field.split("~");
					String fieldFrom=splitField[0];
					String fieldTo=splitField[1];
					try{
						//BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
						//beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
						PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
						try{
						beanUtilsBean.setProperty(synchedAccountLine,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
						}catch(Exception ex){
							 ex.printStackTrace();
						}
						
					}catch(Exception ex){
						
						ex.printStackTrace();
					}
					
				}
				try{
					if(chargeCodeFlag){
					billingRecods =billingManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
					ServiceOrder soObj = serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
					 Boolean extCostElement =  accountLineManager.getExternalCostElement(synchedAccountLine.getCorpID());
					if(!extCostElement){
				  List  agentGLList=accountLineManager.getGLList(synchedAccountLine.getChargeCode(),billingRecods.getContract(),synchedAccountLine.getCorpID());
					if(agentGLList!=null && (!(agentGLList.isEmpty())) && agentGLList.get(0)!=null){
						
						String[] GLarrayData=agentGLList.get(0).toString().split("#"); 
						String recGl=GLarrayData[0];
						String payGl=GLarrayData[1];
						if(!(recGl.equalsIgnoreCase("NO"))){
							synchedAccountLine.setRecGl(recGl);
						}
						if(!(payGl.equalsIgnoreCase("NO"))){
							synchedAccountLine.setPayGl(payGl);
						}
						String VATExclude=GLarrayData[2];
						if(VATExclude.equalsIgnoreCase("Y")){
							synchedAccountLine.setVATExclude(true);
						}else{
							synchedAccountLine.setVATExclude(false);
						}
					}
					}else{
						String chargeStr="";
						List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),synchedAccountLine.getChargeCode(),synchedAccountLine.getCorpID(),soObj.getJob(),soObj.getRouting(),soObj.getCompanyDivision());
						if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
							  chargeStr= glList.get(0).toString();
						  }
						   if(!chargeStr.equalsIgnoreCase("")){
							  String [] chrageDetailArr = chargeStr.split("#");
							  synchedAccountLine.setRecGl(chrageDetailArr[1]);
							  synchedAccountLine.setPayGl(chrageDetailArr[2]);
							}else{
								synchedAccountLine.setRecGl("");
								synchedAccountLine.setPayGl("");
						 }
		    		}
					}
					}catch(Exception e){
						e.printStackTrace();
					}
				   BigDecimal estimateContractExchangeRateBig=synchedAccountLine.getEstimateContractExchangeRate();
		            if(estimateContractCurrencyFlag){
		            	synchedAccountLine.setEstimateContractValueDate(accountLine.getEstimateContractValueDate()); 
		            	 String estimateContractExchangeRate="1"; 
		 	            List estimateContractRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID() ,synchedAccountLine.getEstimateContractCurrency ());
		 				 if((estimateContractRate!=null)&&(!estimateContractRate.isEmpty())&& estimateContractRate.get(0)!=null && (!(estimateContractRate.get(0).toString().equals(""))))
		 				 {
		 					estimateContractExchangeRate=estimateContractRate.get(0).toString();
		 				 
		 				 } 
		 				estimateContractExchangeRateBig=new BigDecimal(estimateContractExchangeRate);	
		 				synchedAccountLine.setEstimateContractExchangeRate(estimateContractExchangeRateBig);
		            } 
		            synchedAccountLine.setEstimateSellRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getEstimateContractRate().doubleValue()/synchedAccountLine.getEstimateContractExchangeRate().doubleValue())));
		            synchedAccountLine.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format(synchedAccountLine.getEstimateContractRateAmmount().doubleValue()/synchedAccountLine.getEstimateContractExchangeRate().doubleValue())));
		           
		            BigDecimal estSellExchangeRateBig=synchedAccountLine.getEstSellExchangeRate();
		            
		            if(estimateBillingCurrencyFlag){
						synchedAccountLine.setEstSellValueDate(new Date());
					  String estSellExchangeRate="1"; 
		            
		            List estSellRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getEstSellCurrency());
					 if((estSellRate!=null)&&(!estSellRate.isEmpty())&& estSellRate.get(0)!=null && (!(estSellRate.get(0).toString().equals(""))))
					 {
						estSellExchangeRate=estSellRate.get(0).toString();
					 }
					estSellExchangeRateBig=new BigDecimal(estSellExchangeRate);
					synchedAccountLine.setEstSellExchangeRate(estSellExchangeRateBig);
					 	
					}
					synchedAccountLine.setEstSellLocalRate(synchedAccountLine.getEstimateSellRate().multiply(estSellExchangeRateBig));
					synchedAccountLine.setEstSellLocalAmount(synchedAccountLine.getEstimateRevenueAmount().multiply(estSellExchangeRateBig));
		            BigDecimal revisionContractExchangeRateBig=synchedAccountLine.getRevisionContractExchangeRate();
		            if(revisionContractCurrencyFlag){
		            	synchedAccountLine.setRevisionContractValueDate(accountLine.getRevisionContractValueDate()); 
		            	 String revisionContractExchangeRate="1"; 
		 	            List revisionContractRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID() ,synchedAccountLine.getRevisionContractCurrency ());
		 				 if((revisionContractRate!=null)&&(!revisionContractRate.isEmpty())&& revisionContractRate.get(0)!=null && (!(revisionContractRate.get(0).toString().equals(""))))
		 				 {
		 					revisionContractExchangeRate=revisionContractRate.get(0).toString();
		 				 
		 				 } 
		 				revisionContractExchangeRateBig=new BigDecimal(revisionContractExchangeRate);	
		 				synchedAccountLine.setRevisionContractExchangeRate(revisionContractExchangeRateBig);
		            } 
		            synchedAccountLine.setRevisionSellRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getRevisionContractRate().doubleValue()/synchedAccountLine.getRevisionContractExchangeRate().doubleValue())));
		            synchedAccountLine.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format(synchedAccountLine.getRevisionContractRateAmmount().doubleValue()/synchedAccountLine.getRevisionContractExchangeRate().doubleValue())));
		           
		            BigDecimal revisionSellExchangeRateBig=synchedAccountLine.getRevisionSellExchangeRate();
		            
		            if(revisionBillingCurrencyFlag){
						synchedAccountLine.setRevisionSellValueDate(new Date());
					  String revisionSellExchangeRate="1"; 
		            
		            List revisionSellRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getRevisionSellCurrency());
					 if((revisionSellRate!=null)&&(!revisionSellRate.isEmpty())&& revisionSellRate.get(0)!=null && (!(revisionSellRate.get(0).toString().equals(""))))
					 {
						revisionSellExchangeRate=revisionSellRate.get(0).toString();
					 }
					revisionSellExchangeRateBig=new BigDecimal(revisionSellExchangeRate);
					synchedAccountLine.setRevisionSellExchangeRate(revisionSellExchangeRateBig);
					 	
					}
					synchedAccountLine.setRevisionSellLocalRate(synchedAccountLine.getRevisionSellRate().multiply(revisionSellExchangeRateBig));
					synchedAccountLine.setRevisionSellLocalAmount(synchedAccountLine.getRevisionRevenueAmount().multiply(revisionSellExchangeRateBig));
					
					try{
				if(synchedAccountLine.getEstimateRevenueAmount() !=null && synchedAccountLine.getEstimateRevenueAmount().doubleValue()>0 && synchedAccountLine.getEstimateExpense()!=null && synchedAccountLine.getEstimateExpense().doubleValue()>0){
					Double estimatePassPercentageValue=(synchedAccountLine.getEstimateRevenueAmount().doubleValue()/synchedAccountLine.getEstimateExpense().doubleValue())*100;
					Integer estimatePassPercentage=estimatePassPercentageValue.intValue();
					synchedAccountLine.setEstimatePassPercentage(estimatePassPercentage);
		            }
		            if(synchedAccountLine.getRevisionRevenueAmount() !=null && synchedAccountLine.getRevisionRevenueAmount().doubleValue()>0 && synchedAccountLine.getRevisionExpense()!=null &&  synchedAccountLine.getRevisionExpense().doubleValue()>0){
		            	Double revisionPassPercentageValue=(synchedAccountLine.getRevisionRevenueAmount().doubleValue()/synchedAccountLine.getRevisionExpense().doubleValue())*100;
		            	Integer revisionPassPercentage=revisionPassPercentageValue.intValue();
		            	synchedAccountLine.setRevisionPassPercentage(revisionPassPercentage);
		            }
				}catch(Exception e){
					e.printStackTrace();
				}
		          
			            
		              BigDecimal contractExchangeRateBig=synchedAccountLine.getContractExchangeRate();
		            if(contractCurrencyFlag){
		            	synchedAccountLine.setContractValueDate(accountLine.getContractValueDate()); 
		            	 String contractExchangeRate="1"; 
		 	            List contractRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID() ,synchedAccountLine.getContractCurrency ());
		 				 if((contractRate!=null)&&(!contractRate.isEmpty())&& contractRate.get(0)!=null && (!(contractRate.get(0).toString().equals(""))))
		 				 {
		 					 contractExchangeRate=contractRate.get(0).toString();
		 				 
		 				 } 
		 				contractExchangeRateBig=new BigDecimal(contractExchangeRate);	
		 				synchedAccountLine.setContractExchangeRate(contractExchangeRateBig);
		            }
		 				//accountLineNew.setContractRate(accountLine.getContractRate().divide(contractExchangeRateBig,2)); 
		 				synchedAccountLine.setRecRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getContractRate().doubleValue()/synchedAccountLine.getContractExchangeRate().doubleValue())));
		 				synchedAccountLine.setActualRevenue(new BigDecimal(decimalFormat.format(synchedAccountLine.getContractRateAmmount().doubleValue()/synchedAccountLine.getContractExchangeRate().doubleValue())));
		 				BigDecimal recExchangeRateBig=synchedAccountLine.getRecRateExchange();
		 				BigDecimal payExchangeRateBig=synchedAccountLine.getRecRateExchange();
		 				if(billingCurrencyFlag){
		 					synchedAccountLine.setRacValueDate(new Date());
		 				  String recExchangeRate="1"; 
		 	            
		 	            List recRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getRecRateCurrency());
		 				 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
		 				 {
		 					 recExchangeRate=recRate.get(0).toString();
		 				 }
		 				recExchangeRateBig=new BigDecimal(recExchangeRate);
		 				synchedAccountLine.setRecRateExchange(recExchangeRateBig);
		 				
		 				double payExchangeRate=0; 
		 				payExchangeRate=Double.parseDouble(recExchangeRate);
		 			
		 				
						payExchangeRateBig=new BigDecimal(payExchangeRate);	
		 				}
		 				synchedAccountLine.setRecCurrencyRate(synchedAccountLine.getRecRate().multiply(recExchangeRateBig));
		 				synchedAccountLine.setActualRevenueForeign(synchedAccountLine.getActualRevenue().multiply(recExchangeRateBig));
		 			
		 				try{
							 
		 					String RecVatPercent=  synchedAccountLine.getRecVatPercent( );
							 BigDecimal   recVatAmt=new BigDecimal(0.0);
							 Double f1= (synchedAccountLine.getActualRevenueForeign().doubleValue() *(Double.parseDouble(synchedAccountLine.getRecVatPercent())));
				      		 f1=f1/100;
							 DecimalFormat df = new DecimalFormat("#.####");
							 String revAmt = df.format(f1);
							 synchedAccountLine.setRecVatAmt(new BigDecimal(revAmt));
							 }catch (Exception e) {
								 
							} 
		 				try{
		 			  
		 					}catch(Exception e){
		 						e.printStackTrace();
		 		    		} 
				try{
			    if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
			    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
			    	synchedAccountLine.setUpdatedOn(new Date());
			    }}catch( Exception e){
			    	
			    }  
			    accountLineManager.save(synchedAccountLine);
	} catch (Exception e) {
   	 e.printStackTrace();
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	}
    	    
		
	
	}
    	     
	@SkipValidation
    public void synchornizeDMMAccountLine(AccountLine synchedAccountLine, AccountLine accountLine,TrackingStatus trackingStatus) {

		   try {
			    Date todayDate=new Date();
	    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
	    		String dt11=sdfDestination.format(todayDate);
	    	    todayDate=sdfDestination.parse(dt11);
	    		
			   Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
		  		boolean accEditable=true;
		  		boolean accPayEditable=true;
				 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
					 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
						 accEditable=false;
					 }
					 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
						 accPayEditable=false;
					 }
				 }else{ 
		  		 if(synchedAccountLine.getRecAccDate()!=null ){
		  			accEditable=false;
		  		 }
		  		if(synchedAccountLine.getPayAccDate()!=null ){
		  			accPayEditable=false;
		  		 }
				 }   
			if((accEditable || accPayEditable) && synchedAccountLine.getChargeCode()!=null && (!(synchedAccountLine.getChargeCode().trim().equalsIgnoreCase("DMMFXFEE"))) && (!(synchedAccountLine.getChargeCode().trim().equalsIgnoreCase("DMMFEE")))){
				String billtocode=synchedAccountLine.getBillToCode();
				Boolean fXRateOnActualizationDate=false;
				try{
				fXRateOnActualizationDate=billingManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId()).getfXRateOnActualizationDate();
				}catch(Exception e){e.printStackTrace();}
			boolean estimateContractCurrencyFlag=false;
			   boolean estimateBillingCurrencyFlag=false;
			   boolean revisionContractCurrencyFlag=false;
			   boolean revisionBillingCurrencyFlag=false;
			   boolean contractCurrencyFlag=false;
			   boolean  billingCurrencyFlag=false;
			   boolean  chargeCodeFlag=false;
			   if((!(accountLine.getEstimateContractCurrency().equals(synchedAccountLine.getEstimateContractCurrency()))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getEstimateContractRate() !=null && synchedAccountLine.getEstimateContractRate()!=null &&   (accountLine.getEstimateContractRate().doubleValue() != synchedAccountLine.getEstimateContractRate().doubleValue())) || (accountLine.getEstimateContractValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getEstimateContractValueDate())))==0)){
				   estimateContractCurrencyFlag=true;
			   }
			   if((!(accountLine.getEstSellCurrency().equals(synchedAccountLine.getEstimatePayableContractCurrency()))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getEstimateContractRate() !=null && synchedAccountLine.getEstimateContractRate()!=null &&  ( (accountLine.getEstimateContractRate().doubleValue() != synchedAccountLine.getEstimateContractRate().doubleValue()) || (!(accountLine.getEstimateContractCurrency().equals(synchedAccountLine.getEstimateContractCurrency()))) ) ) || (accountLine.getEstSellValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getEstSellValueDate())))==0)){
				   estimateBillingCurrencyFlag=true;
			   }
			   if((!(accountLine.getRevisionContractCurrency().equals(synchedAccountLine.getRevisionContractCurrency()))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getRevisionContractRate()!=null && synchedAccountLine.getRevisionContractRate()!=null &&  (accountLine.getRevisionContractRate().doubleValue() != synchedAccountLine.getRevisionContractRate().doubleValue())) || (accountLine.getRevisionContractValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRevisionContractValueDate())))==0)){
				   revisionContractCurrencyFlag=true;
			   }
			   if((!(accountLine.getRevisionSellCurrency().equals(synchedAccountLine.getRevisionPayableContractCurrency()))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getRevisionContractRate()!=null && synchedAccountLine.getRevisionContractRate()!=null &&  ( (accountLine.getRevisionContractRate().doubleValue() != synchedAccountLine.getRevisionContractRate().doubleValue()) || (!(accountLine.getRevisionContractCurrency().equals(synchedAccountLine.getRevisionContractCurrency()))) )) || (accountLine.getRevisionSellValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRevisionSellValueDate())))==0)){
				   revisionBillingCurrencyFlag=true;
			   }
			   if((!(accountLine.getContractCurrency().equals(synchedAccountLine.getContractCurrency()))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getContractRate() !=null && synchedAccountLine.getContractRate() !=null &&  (accountLine.getContractRate().doubleValue() != synchedAccountLine.getContractRate().doubleValue()))  || (accountLine.getContractValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getContractValueDate())))==0)){
				   contractCurrencyFlag=true;
			   }
			   if((!(accountLine.getRecRateCurrency().equals(synchedAccountLine.getPayableContractCurrency()))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getContractRate() !=null && synchedAccountLine.getContractRate() !=null && ( (accountLine.getContractRate().doubleValue() != synchedAccountLine.getContractRate().doubleValue()) || (!(accountLine.getContractCurrency().equals(synchedAccountLine.getContractCurrency())))  ) ) || (accountLine.getRacValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRacValueDate())))==0)){
				   billingCurrencyFlag=true;
			   }
			   if((!(accountLine.getChargeCode().equals(synchedAccountLine.getChargeCode())))){
				   chargeCodeFlag=true;  
			   }
			List fieldToSync=customerFileManager.findAccFieldToSync("AccountLine","DMM"); 
			String companyDivisionUTSI=accountLineManager.getUTSICompanyDivision(synchedAccountLine.getCorpID(),accountLine.getBillToCode()) ;
			UTSIpayVatList=	refMasterManager.findByParameterUTSIPayVat (sessionCorpID, "PAYVATDESC",synchedAccountLine.getCorpID());
			UTSIpayVatPercentList = refMasterManager.findVatPercentList(synchedAccountLine.getCorpID(), "PAYVATDESC");
				Iterator it1=fieldToSync.iterator();
				while(it1.hasNext()){
					String field=it1.next().toString();
					String[] splitField=field.split("~");
					String fieldFrom=splitField[0];
					String fieldTo=splitField[1];
					try{
					
						PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
						try{
						beanUtilsBean.setProperty(synchedAccountLine,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
						}catch(Exception ex){
							ex.printStackTrace(); 
						}
						
					}catch(Exception ex){
				
						ex.printStackTrace();
					}
					
				} 
				try{
					
						
						if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){ 
							synchedAccountLine.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
							}
						String payVatDescr="";
				    	if(UTSIpayVatList.containsKey(synchedAccountLine.getPayVatDescr()+"~"+companyDivisionUTSI)){
				    		payVatDescr=UTSIpayVatList.get(synchedAccountLine.getPayVatDescr()+"~"+companyDivisionUTSI);
				    		synchedAccountLine.setPayVatDescr(payVatDescr);
				    		String payVatPercent="0";
				    		if(UTSIpayVatPercentList.containsKey(payVatDescr)){ 
		    				payVatPercent=UTSIpayVatPercentList.get(payVatDescr);
				    		}
				    		synchedAccountLine.setPayVatPercent(payVatPercent);
				    	} else{
				    		if(UTSIpayVatList.containsKey(synchedAccountLine.getPayVatDescr()+"~NO")){
	 				    		payVatDescr=UTSIpayVatList.get(synchedAccountLine.getPayVatDescr()+"~NO");
	 				    		synchedAccountLine.setPayVatDescr(payVatDescr);
	 				    		String payVatPercent="0";
	 				    		if(UTSIpayVatPercentList.containsKey(payVatDescr)){ 
	 		    				payVatPercent=UTSIpayVatPercentList.get(payVatDescr);
	 				    		}
	 				    		synchedAccountLine.setPayVatPercent(payVatPercent);
	 				    	}else{
	 				    		synchedAccountLine.setPayVatDescr("");
	 				    		synchedAccountLine.setPayVatPercent("0");
	 				    	}
				    	}
				    	if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
					    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
					    	synchedAccountLine.setUpdatedOn(new Date());
					    }
				}catch(Exception e){
					e.printStackTrace();
				}
				if(billtocode!=null &&   (!(billtocode.equalsIgnoreCase(synchedAccountLine.getBillToCode())))){		
					String billingCurrency=serviceOrderManager.getBillingCurrencyValue(synchedAccountLine.getBillToCode(),synchedAccountLine.getCorpID());
					if(!billingCurrency.equalsIgnoreCase(""))
					{
						synchedAccountLine.setRecRateCurrency(billingCurrency);
						synchedAccountLine.setEstSellCurrency(billingCurrency);
						synchedAccountLine.setRevisionSellCurrency(billingCurrency); 
						String recExchangeRate="1"; 
			            BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
			            List recRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getRecRateCurrency());
						 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
						 {
							 recExchangeRate=recRate.get(0).toString();
						 }
						recExchangeRateBig=new BigDecimal(recExchangeRate);
						synchedAccountLine.setRecRateExchange(recExchangeRateBig);
						synchedAccountLine.setRacValueDate(new Date()); 
						synchedAccountLine.setEstSellExchangeRate(recExchangeRateBig);
						synchedAccountLine.setEstSellValueDate(new Date()); 
						synchedAccountLine.setRevisionSellExchangeRate(recExchangeRateBig);
						synchedAccountLine.setRevisionSellValueDate(new Date());  
						if(synchedAccountLine.getRecRateCurrency().equalsIgnoreCase(synchedAccountLine.getContractCurrency())){
							synchedAccountLine.setContractExchangeRate(recExchangeRateBig);
							synchedAccountLine.setContractValueDate(new Date());
						}
						if(synchedAccountLine.getEstSellCurrency().equalsIgnoreCase(synchedAccountLine.getEstimateContractCurrency())){
							synchedAccountLine.setEstimateContractExchangeRate(recExchangeRateBig);
							synchedAccountLine.setEstimateContractValueDate(new Date());
						}
						if(synchedAccountLine.getRevisionSellCurrency().equalsIgnoreCase(synchedAccountLine.getRevisionContractCurrency())){
							synchedAccountLine.setRevisionContractExchangeRate(recExchangeRateBig);
							synchedAccountLine.setRevisionContractValueDate(new Date());
						}
					}
				} 
				

				try{
					if(chargeCodeFlag){
						ServiceOrder soObj =  serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
					billingRecods =billingManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
					 Boolean extCostElement =  accountLineManager.getExternalCostElement(synchedAccountLine.getCorpID());
					if(!extCostElement){
				  List  agentGLList=accountLineManager.getGLList(synchedAccountLine.getChargeCode(),billingRecods.getContract(),synchedAccountLine.getCorpID());
					if(agentGLList!=null && (!(agentGLList.isEmpty())) && agentGLList.get(0)!=null){
						
						String[] GLarrayData=agentGLList.get(0).toString().split("#"); 
						String recGl=GLarrayData[0];
						String payGl=GLarrayData[1];
						if(!(recGl.equalsIgnoreCase("NO"))){
							synchedAccountLine.setRecGl(recGl);
						}
						if(!(payGl.equalsIgnoreCase("NO"))){
							synchedAccountLine.setPayGl(payGl);
						}
						String VATExclude=GLarrayData[2];
						if(VATExclude.equalsIgnoreCase("Y")){
							synchedAccountLine.setVATExclude(true);
						}else{
							synchedAccountLine.setVATExclude(false);
						}
					}
					}else{
						String chargeStr="";
						List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),synchedAccountLine.getChargeCode(),synchedAccountLine.getCorpID(),soObj.getJob(),soObj.getRouting(),soObj.getCompanyDivision());
						if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
							  chargeStr= glList.get(0).toString();
						  }
						   if(!chargeStr.equalsIgnoreCase("")){
							  String [] chrageDetailArr = chargeStr.split("#");
							  synchedAccountLine.setRecGl(chrageDetailArr[1]);
							  synchedAccountLine.setPayGl(chrageDetailArr[2]);
							}else{
								synchedAccountLine.setRecGl("");
								synchedAccountLine.setPayGl("");
						 }
					}
					}
					}catch(Exception e){
						e.printStackTrace();
					}
				/*if(synchedAccountLine.getEstimateRevenueAmount() !=null && synchedAccountLine.getEstimateRevenueAmount().doubleValue()>0){
					synchedAccountLine.setEstimatePassPercentage(100);
			        }
			        if(synchedAccountLine.getRevisionRevenueAmount() !=null && synchedAccountLine.getRevisionRevenueAmount().doubleValue()>0){
			        	synchedAccountLine.setRevisionPassPercentage(100);
			        }*/
			      /* try{
			        BigDecimal estExchangeRateBig=synchedAccountLine.getEstExchangeRate();
			        synchedAccountLine.setEstLocalRate(synchedAccountLine.getEstimateRate().multiply(estExchangeRateBig));
			        synchedAccountLine.setEstLocalAmount(synchedAccountLine.getEstimateExpense().multiply(estExchangeRateBig));
			        }catch(Exception e){
			        	
			        }


			           try{
			            BigDecimal revisionExchangeRateBig=synchedAccountLine.getRevisionExchangeRate();
			            synchedAccountLine.setRevisionLocalRate(synchedAccountLine.getRevisionRate().multiply(revisionExchangeRateBig));
			            synchedAccountLine.setRevisionLocalAmount(synchedAccountLine.getRevisionExpense().multiply(revisionExchangeRateBig));
			            }catch(Exception e){
			            	
			            }*/
			            
					List bookingAgentCodeList=companyDivisionManager.getBookingAgentCode(accountLine.getCompanyDivision(),sessionCorpID);
			        if(bookingAgentCodeList!=null && (!(bookingAgentCodeList.isEmpty())) && bookingAgentCodeList.get(0)!=null && (!(bookingAgentCodeList.get(0).toString().trim().equals(""))))
			        {
			        	synchedAccountLine.setVendorCode(bookingAgentCodeList.get(0).toString());
			        	List bookingAgentNamelist=	serviceOrderManager.findByBooking(bookingAgentCodeList.get(0).toString(), sessionCorpID);
			            
			        	if(bookingAgentNamelist!=null && (!(bookingAgentNamelist.isEmpty())) && bookingAgentNamelist.get(0)!=null && (!(bookingAgentNamelist.get(0).toString().trim().equals("")))){
			        		synchedAccountLine.setEstimateVendorName(bookingAgentNamelist.get(0).toString());
			            }
			        	
			        }
			        
			         String actCode="";
			         String  companyDivisionAcctgCodeUnique1="";
					 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(synchedAccountLine.getCorpID());
					 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
						 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
					 }
			        if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
						actCode=partnerManager.getAccountCrossReference(synchedAccountLine.getVendorCode(),synchedAccountLine.getCompanyDivision(),synchedAccountLine.getCorpID());
					}else{
						actCode=partnerManager.getAccountCrossReferenceUnique(synchedAccountLine.getVendorCode(),synchedAccountLine.getCorpID());
					}
			        synchedAccountLine.setActgCode(actCode);
			        String estimateContractExchangeRate="1"; 
			        BigDecimal estimateContractExchangeRateBig=synchedAccountLine.getEstimateContractExchangeRate();
			        if(estimateContractCurrencyFlag){
			        List estimateContractRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getEstimateContractCurrency ());
					 if((estimateContractRate!=null)&&(!estimateContractRate.isEmpty())&& estimateContractRate.get(0)!=null && (!(estimateContractRate.get(0).toString().equals(""))))
					 {
						 estimateContractExchangeRate=estimateContractRate.get(0).toString();
					 
					 } 
					 estimateContractExchangeRateBig=new BigDecimal(estimateContractExchangeRate);	
					 synchedAccountLine.setEstimateContractExchangeRate(estimateContractExchangeRateBig); 
			        
                       //if(fXRateOnActualizationDate!=null && fXRateOnActualizationDate){
                    	 String estSellExchangeRate="1"; 
                    	 List estSellRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getEstSellCurrency());
						 if((estSellRate!=null)&&(!estSellRate.isEmpty())&& estSellRate.get(0)!=null && (!(estSellRate.get(0).toString().equals(""))))
						 {
							 estSellExchangeRate=estSellRate.get(0).toString();
						 }
						 BigDecimal estSellExchangeRateBig=new BigDecimal(estSellExchangeRate); 
						 synchedAccountLine.setEstSellExchangeRate(estSellExchangeRateBig);
						 synchedAccountLine.setEstSellValueDate(new Date());
				        
			        //}
			        
			        }
					 synchedAccountLine.setEstimateSellRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getEstimateContractRate().doubleValue()/synchedAccountLine.getEstimateContractExchangeRate().doubleValue())));
					 if(accountLine.getEstimateSellDeviation()!=null && synchedAccountLine.getEstimateContractRateAmmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && synchedAccountLine.getEstimateContractRateAmmount().doubleValue()>0){
						 synchedAccountLine.setEstimateContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstimateContractRateAmmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
						 }else{
							 
						 }
					 synchedAccountLine.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format(synchedAccountLine.getEstimateContractRateAmmount().doubleValue()/synchedAccountLine.getEstimateContractExchangeRate().doubleValue())));
					 
					String estSellExchangeRate="1"; 
			        BigDecimal estSellExchangeRateBig=synchedAccountLine.getEstimatePayableContractExchangeRate();
			        
			        if(estimateBillingCurrencyFlag){
			        List estSellRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),accountLine.getEstSellCurrency());
					 if((estSellRate!=null)&&(!estSellRate.isEmpty())&& estSellRate.get(0)!=null && (!(estSellRate.get(0).toString().equals(""))))
					 {
						 estSellExchangeRate=estSellRate.get(0).toString();
					 }
					 estSellExchangeRateBig=new BigDecimal(estSellExchangeRate);
					 //synchedAccountLine.setEstSellExchangeRate(estSellExchangeRateBig);
					 synchedAccountLine.setEstExchangeRate(estSellExchangeRateBig);
					 synchedAccountLine.setEstimatePayableContractExchangeRate(estSellExchangeRateBig);
			        }
			         synchedAccountLine.setEstimatePayableContractRate(accountLine.getEstSellLocalRate());
			         if(accountLine.getEstimateSellDeviation()!=null && accountLine.getEstSellLocalAmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLine.getEstSellLocalAmount().doubleValue()>0){
			            	synchedAccountLine.setEstimatePayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
					 }else{
			         synchedAccountLine.setEstimatePayableContractRateAmmount(accountLine.getEstSellLocalAmount());
					 }
			         synchedAccountLine.setEstLocalRate(accountLine.getEstSellLocalRate());
			         if(accountLine.getEstimateSellDeviation()!=null && accountLine.getEstSellLocalAmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLine.getEstSellLocalAmount().doubleValue()>0){
			         synchedAccountLine.setEstLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
					 }else{
					 synchedAccountLine.setEstLocalAmount(accountLine.getEstSellLocalAmount());
					 }
					 synchedAccountLine.setEstSellLocalRate(synchedAccountLine.getEstimateSellRate().multiply(synchedAccountLine.getEstSellExchangeRate()));
					 synchedAccountLine.setEstSellLocalAmount(synchedAccountLine.getEstimateRevenueAmount().multiply(synchedAccountLine.getEstSellExchangeRate()));
					 synchedAccountLine.setEstimateRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getEstimatePayableContractRate().doubleValue()/synchedAccountLine.getEstimatePayableContractExchangeRate().doubleValue())));
					 synchedAccountLine.setEstimateExpense(new BigDecimal(decimalFormat.format(synchedAccountLine.getEstimatePayableContractRateAmmount().doubleValue()/synchedAccountLine.getEstimatePayableContractExchangeRate().doubleValue())));
					 
					 
					 String revisionContractExchangeRate="1";
					 
					 BigDecimal revisionContractExchangeRateBig=synchedAccountLine.getRevisionContractExchangeRate();
			         if(revisionContractCurrencyFlag){
			         List revisionContractRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getRevisionContractCurrency ());
					 if((revisionContractRate!=null)&&(!revisionContractRate.isEmpty())&& revisionContractRate.get(0)!=null && (!(revisionContractRate.get(0).toString().equals(""))))
					 {
						 revisionContractExchangeRate=revisionContractRate.get(0).toString();
					 
					 } 
					 revisionContractExchangeRateBig=new BigDecimal(revisionContractExchangeRate);	
					 synchedAccountLine.setRevisionContractExchangeRate(revisionContractExchangeRateBig);
					 //if(fXRateOnActualizationDate!=null && fXRateOnActualizationDate ){
						 String revisionSellExchangeRate="1";
						 List revisionSellRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getRevisionSellCurrency());
						 if((revisionSellRate!=null)&&(!revisionSellRate.isEmpty())&& revisionSellRate.get(0)!=null && (!(revisionSellRate.get(0).toString().equals(""))))
						 {
							 revisionSellExchangeRate=revisionSellRate.get(0).toString();
						 }
						 BigDecimal revisionSellExchangeRateBig=new BigDecimal(revisionSellExchangeRate);
						 synchedAccountLine.setRevisionSellExchangeRate(revisionSellExchangeRateBig);
						 synchedAccountLine.setRevisionSellValueDate(new Date());
						  
					 //}
					 
			         }
					 synchedAccountLine.setRevisionSellRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getRevisionContractRate().doubleValue()/synchedAccountLine.getRevisionContractExchangeRate().doubleValue())));
					 if(accountLine.getRevisionSellDeviation()!=null && synchedAccountLine.getRevisionContractRateAmmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && synchedAccountLine.getRevisionContractRateAmmount().doubleValue()>0){
						 synchedAccountLine.setRevisionContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionContractRateAmmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue())));	 
						 }else{
							 
						 }
					 synchedAccountLine.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format(synchedAccountLine.getRevisionContractRateAmmount().doubleValue()/synchedAccountLine.getRevisionContractExchangeRate().doubleValue())));
					 
					 
					String revisionSellExchangeRate="1"; 
			        BigDecimal revisionSellExchangeRateBig=synchedAccountLine.getRevisionPayableContractExchangeRate();
			        
			        if(revisionBillingCurrencyFlag){
			        List revisionSellRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),accountLine.getRevisionSellCurrency());
					 if((revisionSellRate!=null)&&(!revisionSellRate.isEmpty())&& revisionSellRate.get(0)!=null && (!(revisionSellRate.get(0).toString().equals(""))))
					 {
						 revisionSellExchangeRate=revisionSellRate.get(0).toString();
					 }
					 revisionSellExchangeRateBig=new BigDecimal(revisionSellExchangeRate);
					 //synchedAccountLine.setRevisionSellExchangeRate(revisionSellExchangeRateBig);
					 synchedAccountLine.setRevisionExchangeRate(revisionSellExchangeRateBig);
					 synchedAccountLine.setRevisionPayableContractExchangeRate(revisionSellExchangeRateBig);
			        } 
			         synchedAccountLine.setRevisionPayableContractRate(accountLine.getRevisionSellLocalRate());
			         if(accountLine.getRevisionSellDeviation()!=null && accountLine.getRevisionSellLocalAmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLine.getRevisionSellLocalAmount().doubleValue()>0){
			        	 synchedAccountLine.setRevisionPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue()))	 );	 
					 }else{
			         synchedAccountLine.setRevisionPayableContractRateAmmount(accountLine.getRevisionSellLocalAmount());
						 }
			         synchedAccountLine.setRevisionLocalRate(accountLine.getRevisionSellLocalRate());
			         if(accountLine.getRevisionSellDeviation()!=null && accountLine.getRevisionSellLocalAmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLine.getRevisionSellLocalAmount().doubleValue()>0){
			         synchedAccountLine.setRevisionLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue()))	 );	 
					 }else{
					 synchedAccountLine.setRevisionLocalAmount(accountLine.getRevisionSellLocalAmount());
					 } 
					 synchedAccountLine.setRevisionRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getRevisionPayableContractRate().doubleValue()/synchedAccountLine.getRevisionPayableContractExchangeRate().doubleValue())));
					 synchedAccountLine.setRevisionExpense(new BigDecimal(decimalFormat.format(synchedAccountLine.getRevisionPayableContractRateAmmount().doubleValue()/synchedAccountLine.getRevisionPayableContractExchangeRate().doubleValue())));
					 synchedAccountLine.setRevisionSellLocalRate(synchedAccountLine.getRevisionSellRate().multiply(synchedAccountLine.getRevisionSellExchangeRate()));
					 synchedAccountLine.setRevisionSellLocalAmount(synchedAccountLine.getRevisionRevenueAmount().multiply(synchedAccountLine.getRevisionSellExchangeRate()));
					  
			          BigDecimal contractExchangeRateBig=synchedAccountLine.getContractExchangeRate();
			        if(contractCurrencyFlag){
			        	synchedAccountLine.setContractValueDate(accountLine.getContractValueDate()); 
			        	 String contractExchangeRate="1"; 
			            List contractRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID() ,synchedAccountLine.getContractCurrency ());
						 if((contractRate!=null)&&(!contractRate.isEmpty())&& contractRate.get(0)!=null && (!(contractRate.get(0).toString().equals(""))))
						 {
							 contractExchangeRate=contractRate.get(0).toString();
						 
						 } 
						contractExchangeRateBig=new BigDecimal(contractExchangeRate);	
						//if(fXRateOnActualizationDate!=null && fXRateOnActualizationDate ){
							
							String recExchangeRate="1";  
				            List recRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getRecRateCurrency());
							 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
							 {
								 recExchangeRate=recRate.get(0).toString();
							 }
							 BigDecimal recExchangeRateBig=new BigDecimal(recExchangeRate);
							synchedAccountLine.setRecRateExchange(recExchangeRateBig);
							synchedAccountLine.setRacValueDate(new Date());
							 
						//}
						
			         }
			            synchedAccountLine.setContractExchangeRate(contractExchangeRateBig); 
						
						synchedAccountLine.setRecRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getContractRate().doubleValue()/synchedAccountLine.getContractExchangeRate().doubleValue())));
						if(accountLine.getReceivableSellDeviation()!=null && synchedAccountLine.getContractRateAmmount()!=null &&  accountLine.getReceivableSellDeviation().doubleValue()>0 && synchedAccountLine.getContractRateAmmount().doubleValue()>0){
							synchedAccountLine.setContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getContractRateAmmount().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));	 
							 }else{
								 
							 }
						synchedAccountLine.setActualRevenue(new BigDecimal(decimalFormat.format(synchedAccountLine.getContractRateAmmount().doubleValue()/synchedAccountLine.getContractExchangeRate().doubleValue())));
						BigDecimal recExchangeRateBig=synchedAccountLine.getPayableContractExchangeRate();
				
						if(billingCurrencyFlag){
						  String recExchangeRate="1"; 
			            
			            List recRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),accountLine.getRecRateCurrency());
						 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
						 {
							 recExchangeRate=recRate.get(0).toString();
						 }
						recExchangeRateBig=new BigDecimal(recExchangeRate);
						//synchedAccountLine.setRecRateExchange(recExchangeRateBig);
						synchedAccountLine.setPayableContractExchangeRate(recExchangeRateBig); 
						synchedAccountLine.setValueDate(new Date());
						double payExchangeRate=0; 
						payExchangeRate=Double.parseDouble(recExchangeRate);
						synchedAccountLine.setExchangeRate(payExchangeRate);
						
						//payExchangeRateBig=new BigDecimal(payExchangeRate);	
						}
						synchedAccountLine.setRecCurrencyRate(synchedAccountLine.getRecRate().multiply(synchedAccountLine.getRecRateExchange()));
						synchedAccountLine.setActualRevenueForeign(synchedAccountLine.getActualRevenue().multiply(synchedAccountLine.getRecRateExchange()));
						if(accountLine.getReceivableSellDeviation()!=null && accountLine.getActualRevenueForeign()!=null &&  accountLine.getReceivableSellDeviation().doubleValue()>0 && accountLine.getActualRevenueForeign().doubleValue()>0){
							synchedAccountLine.setPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));
							synchedAccountLine.setLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));	
						}else{
						synchedAccountLine.setLocalAmount(accountLine.getActualRevenueForeign());
						synchedAccountLine.setPayableContractRateAmmount(accountLine.getActualRevenueForeign());
						}
						 try{
					    synchedAccountLine.setActualExpense(new BigDecimal(decimalFormat.format(synchedAccountLine.getPayableContractRateAmmount().doubleValue()/synchedAccountLine.getPayableContractExchangeRate().doubleValue())));
							}catch(Exception e){
								e.printStackTrace();
				    		}
							try{
								if(synchedAccountLine.getEstimateSellDeviation()!=null && synchedAccountLine.getEstimateSellDeviation().doubleValue()>0 ){
									if(synchedAccountLine.getEstimateRevenueAmount()!=null && synchedAccountLine.getEstimateRevenueAmount().doubleValue()>0)
										synchedAccountLine.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstimateRevenueAmount().doubleValue())*((synchedAccountLine.getEstimateSellDeviation().doubleValue())/100))));
									if(synchedAccountLine.getEstSellLocalAmount()!=null && synchedAccountLine.getEstSellLocalAmount().doubleValue()>0)
										synchedAccountLine.setEstSellLocalAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstSellLocalAmount().doubleValue())*((synchedAccountLine.getEstimateSellDeviation().doubleValue())/100))));
									if(synchedAccountLine.getEstimateContractRateAmmount()!=null && synchedAccountLine.getEstimateContractRateAmmount().doubleValue()>0)
										synchedAccountLine.setEstimateContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstimateContractRateAmmount().doubleValue())*((synchedAccountLine.getEstimateSellDeviation().doubleValue())/100))));	
								}
								if(synchedAccountLine.getEstimateDeviation()!=null && synchedAccountLine.getEstimateDeviation().doubleValue()>0 ){
									if(synchedAccountLine.getEstimateExpense()!=null && synchedAccountLine.getEstimateExpense().doubleValue()>0)
										synchedAccountLine.setEstimateExpense(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstimateExpense().doubleValue())*((synchedAccountLine.getEstimateDeviation().doubleValue())/100))));
									if(synchedAccountLine.getEstLocalAmount()!=null && synchedAccountLine.getEstLocalAmount().doubleValue()>0)
										synchedAccountLine.setEstLocalAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstLocalAmount().doubleValue())*((synchedAccountLine.getEstimateDeviation().doubleValue())/100))));
									if(synchedAccountLine.getEstimatePayableContractRateAmmount()!=null && synchedAccountLine.getEstimatePayableContractRateAmmount().doubleValue()>0)
										synchedAccountLine.setEstimatePayableContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstimatePayableContractRateAmmount().doubleValue())*((synchedAccountLine.getEstimateDeviation().doubleValue())/100))));	
								}
								if(synchedAccountLine.getRevisionSellDeviation()!=null && synchedAccountLine.getRevisionSellDeviation().doubleValue()>0 ){
									if(synchedAccountLine.getRevisionRevenueAmount()!=null && synchedAccountLine.getRevisionRevenueAmount().doubleValue()>0)
										synchedAccountLine.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionRevenueAmount().doubleValue())*((synchedAccountLine.getRevisionSellDeviation().doubleValue())/100))));
									if(synchedAccountLine.getRevisionSellLocalAmount()!=null && synchedAccountLine.getRevisionSellLocalAmount().doubleValue()>0)
										synchedAccountLine.setRevisionSellLocalAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionSellLocalAmount().doubleValue())*((synchedAccountLine.getRevisionSellDeviation().doubleValue())/100))));
									if(synchedAccountLine.getRevisionContractRateAmmount()!=null && synchedAccountLine.getRevisionContractRateAmmount().doubleValue()>0)
										synchedAccountLine.setRevisionContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionContractRateAmmount().doubleValue())*((synchedAccountLine.getRevisionSellDeviation().doubleValue())/100))));	
								}
								if(synchedAccountLine.getRevisionDeviation()!=null && synchedAccountLine.getRevisionDeviation().doubleValue()>0 ){
									if(synchedAccountLine.getRevisionExpense()!=null && synchedAccountLine.getRevisionExpense().doubleValue()>0)
										synchedAccountLine.setRevisionExpense(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionExpense().doubleValue())*((synchedAccountLine.getRevisionDeviation().doubleValue())/100))));
									if(synchedAccountLine.getRevisionLocalAmount()!=null && synchedAccountLine.getRevisionLocalAmount().doubleValue()>0)
										synchedAccountLine.setRevisionLocalAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionLocalAmount().doubleValue())*((synchedAccountLine.getRevisionDeviation().doubleValue())/100))));
									if(synchedAccountLine.getRevisionPayableContractRateAmmount()!=null && synchedAccountLine.getRevisionPayableContractRateAmmount().doubleValue()>0)
										synchedAccountLine.setRevisionPayableContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionPayableContractRateAmmount().doubleValue())*((synchedAccountLine.getRevisionDeviation().doubleValue())/100))));	
								}
								if(synchedAccountLine.getReceivableSellDeviation()!=null && synchedAccountLine.getReceivableSellDeviation().doubleValue()>0 ){
									if(synchedAccountLine.getActualRevenue()!=null && synchedAccountLine.getActualRevenue().doubleValue()>0)
										synchedAccountLine.setActualRevenue(new BigDecimal(decimalFormat.format((synchedAccountLine.getActualRevenue().doubleValue())*((synchedAccountLine.getReceivableSellDeviation().doubleValue())/100))));
									if(synchedAccountLine.getActualRevenueForeign()!=null && synchedAccountLine.getActualRevenueForeign().doubleValue()>0)
										synchedAccountLine.setActualRevenueForeign(new BigDecimal(decimalFormat.format((synchedAccountLine.getActualRevenueForeign().doubleValue())*((synchedAccountLine.getReceivableSellDeviation().doubleValue())/100))));
									if(synchedAccountLine.getContractRateAmmount()!=null && synchedAccountLine.getContractRateAmmount().doubleValue()>0)
										synchedAccountLine.setContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getContractRateAmmount().doubleValue())*((synchedAccountLine.getReceivableSellDeviation().doubleValue())/100))));	
								}
								if(synchedAccountLine.getPayDeviation()!=null && synchedAccountLine.getPayDeviation().doubleValue()>0 ){
									if(synchedAccountLine.getActualExpense()!=null && synchedAccountLine.getActualExpense().doubleValue()>0)
										synchedAccountLine.setActualExpense(new BigDecimal(decimalFormat.format((synchedAccountLine.getActualExpense().doubleValue())*((synchedAccountLine.getPayDeviation().doubleValue())/100))));
									if(synchedAccountLine.getLocalAmount()!=null && synchedAccountLine.getLocalAmount().doubleValue()>0)
										synchedAccountLine.setLocalAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getLocalAmount().doubleValue())*((synchedAccountLine.getPayDeviation().doubleValue())/100))));
									if(synchedAccountLine.getPayableContractRateAmmount()!=null && synchedAccountLine.getPayableContractRateAmmount().doubleValue()>0)
										synchedAccountLine.setPayableContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getPayableContractRateAmmount().doubleValue())*((synchedAccountLine.getPayDeviation().doubleValue())/100))));	
								} 
							}catch(Exception e){
								e.printStackTrace();
				    		}	
							 try{
								
							    String payVatPercent=synchedAccountLine.getPayVatPercent();
							    BigDecimal payVatPercentBig=new BigDecimal("0.00");
							    BigDecimal payVatAmmountBig=new BigDecimal("0.00");
							    BigDecimal estPayVatAmmountBig=new BigDecimal("0.00");
								BigDecimal revisionpayVatAmmountBig=new BigDecimal("0.00");
							    if(payVatPercent!=null && (!(payVatPercent.trim().equals("")))){
							    	payVatPercentBig=new BigDecimal(payVatPercent);
							    }
							    payVatAmmountBig= (new BigDecimal(decimalFormat.format(((synchedAccountLine.getLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
							    try{
								    estPayVatAmmountBig= (new BigDecimal(decimalFormat.format(((synchedAccountLine.getEstLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
								    revisionpayVatAmmountBig= (new BigDecimal(decimalFormat.format(((synchedAccountLine.getRevisionLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
								    
								  }catch(Exception e){
										logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								    }
							    synchedAccountLine.setPayVatAmt(payVatAmmountBig);
							    synchedAccountLine.setEstExpVatAmt(estPayVatAmmountBig);
							    synchedAccountLine.setRevisionExpVatAmt(revisionpayVatAmmountBig);
							    }catch(Exception e){
							    	e.printStackTrace();
							    }
							 try{
								    if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
								    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
								    	synchedAccountLine.setUpdatedOn(new Date());
								    }
								    
								    
								    
									}catch( Exception e){
								    	
								    }
							   try{
								    String recVatPercent=synchedAccountLine.getRecVatPercent();
								    BigDecimal recVatPercentBig=new BigDecimal("0.00");
								    BigDecimal recVatAmmountBig=new BigDecimal("0.00");
								    BigDecimal revisionVatAmmountBig=new BigDecimal("0.00");
								    BigDecimal estVatAmmountBig=new BigDecimal("0.00");
								    if(recVatPercent!=null && (!(recVatPercent.trim().equals("")))){
								    	recVatPercentBig=new BigDecimal(recVatPercent);
								    }
								   recVatAmmountBig= (new BigDecimal(decimalFormat.format(((synchedAccountLine.getActualRevenueForeign().multiply(recVatPercentBig)).doubleValue())/100)));
								   try{
								   estVatAmmountBig= (new BigDecimal(decimalFormat.format(((synchedAccountLine.getEstSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
								   revisionVatAmmountBig = (new BigDecimal(decimalFormat.format(((synchedAccountLine.getRevisionSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
								   }catch(Exception e){
									   e.printStackTrace();
								    }
								   synchedAccountLine.setRecVatAmt(recVatAmmountBig);
								   synchedAccountLine.setEstVatAmt(estVatAmmountBig); 
								   synchedAccountLine.setRevisionVatAmt(revisionVatAmmountBig);
								  
								    }catch(Exception e){
								    	e.printStackTrace();
								    }			    
				try{
			    if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
			    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
			    	synchedAccountLine.setUpdatedOn(new Date());
			    }
			    
			    
			    
				}catch( Exception e){
			    	
			    } 
				try{
					if(synchedAccountLine.getEstimateRevenueAmount() !=null && synchedAccountLine.getEstimateRevenueAmount().doubleValue()>0 && synchedAccountLine.getEstimateExpense()!=null && synchedAccountLine.getEstimateExpense().doubleValue()>0){
						Double estimatePassPercentageValue=(synchedAccountLine.getEstimateRevenueAmount().doubleValue()/synchedAccountLine.getEstimateExpense().doubleValue())*100;
						Integer estimatePassPercentage=estimatePassPercentageValue.intValue();
						synchedAccountLine.setEstimatePassPercentage(estimatePassPercentage);
			            }
			            if(synchedAccountLine.getRevisionRevenueAmount() !=null && synchedAccountLine.getRevisionRevenueAmount().doubleValue()>0 && synchedAccountLine.getRevisionExpense()!=null &&  synchedAccountLine.getRevisionExpense().doubleValue()>0){
			            	Double revisionPassPercentageValue=(synchedAccountLine.getRevisionRevenueAmount().doubleValue()/synchedAccountLine.getRevisionExpense().doubleValue())*100;
			            	Integer revisionPassPercentage=revisionPassPercentageValue.intValue();
			            	synchedAccountLine.setRevisionPassPercentage(revisionPassPercentage);
			            }
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						boolean billtotype=false; 
						billtotype =accountLine.getBillToCode().trim().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().trim());	
						if(billtotype){
							
						}else{
							synchedAccountLine.setStatus(false)	;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					synchedAccountLine= accountLineManager.save(synchedAccountLine);
			    serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
			    updateExternalAccountLine(serviceOrderToRecods,synchedAccountLine.getCorpID());
			}
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		
	}
    		
    	 
   
    @SkipValidation
    public String updateAccountLine( ServiceOrder serviceOrder){ 
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			BigDecimal cuDivisormar=new BigDecimal(100);
			if((billingManager.checkById(sid)).isEmpty())
			{ 
				billingFlag=0; 
			}
			else
			{ 
				billingFlag=1; 
			}
			serviceOrder.setEstimatedTotalRevenue(new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()));
			serviceOrder.setDistributedTotalAmount(new BigDecimal((accountLineManager.getdistributedAmountSum(serviceOrder.getShipNumber())).get(0).toString()));
			serviceOrder.setProjectedDistributedTotalAmount(new BigDecimal((accountLineManager.getProjectedDistributedTotalAmountSum(serviceOrder.getShipNumber())).get(0).toString()));
			serviceOrder.setProjectedActualExpense(new BigDecimal((accountLineManager.getProjectedActualExpenseSum(serviceOrder.getShipNumber())).get(0).toString()));
				if(serviceOrder.getJob().equals("HVY")){
				BigDecimal sum = new BigDecimal((accountLineManager.getProjectedActualRevenueSum(serviceOrder.getShipNumber())).get(0).toString());
				sum=sum.add(new BigDecimal((accountLineManager.getProjectedActualRevenueSumHVY(serviceOrder.getShipNumber())).get(0).toString()));
				serviceOrder.setProjectedActualRevenue(sum);
				
				}else{
					serviceOrder.setProjectedActualRevenue(new BigDecimal((accountLineManager.getProjectedActualRevenueSum(serviceOrder.getShipNumber())).get(0).toString()));
				}
				BigDecimal divisormarEst=new BigDecimal(((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString())); 
			if(serviceOrder.getEstimatedTotalRevenue()==null || serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0"))||serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.0000")) ){
				serviceOrder.setEstimatedGrossMarginPercentage(new BigDecimal("0"));
			} 
			else 
			{
			serviceOrder.setEstimatedGrossMarginPercentage((((new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract (new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(divisormarEst,2));
			}
			/*# 7784 - Always show actual gross margin in accountline overview Start*/
			try{
			serviceOrder.setProjectedGrossMargin(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"EXP")));
			if(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").doubleValue()!=0){
				serviceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((serviceOrder.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV")).doubleValue())*100));
			}else{
				serviceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
			}
			}catch(Exception e){e.printStackTrace();}
			/*# 7784 - Always show actual gross margin in accountline overview End*/
			
			serviceOrder.setEstimatedGrossMargin((new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract (new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
			serviceOrder.setRevisedGrossMargin((new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract (new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
			serviceOrder.setRevisedTotalRevenue(new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()));
			BigDecimal divisormarEstPer=new BigDecimal(((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()));
			if(serviceOrder.getRevisedTotalRevenue() == null || serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0"))||serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.0000"))){
				serviceOrder.setRevisedGrossMarginPercentage(new BigDecimal("0"));
			}
			else{
			serviceOrder.setRevisedGrossMarginPercentage((((new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract (new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(divisormarEstPer,2));
			} 
			serviceOrder.setRevisedTotalExpense(new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString()));
			serviceOrder.setEstimatedTotalExpense(new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString()));
			serviceOrder.setEntitledTotalAmount(new BigDecimal((accountLineManager.getEntitleSum(serviceOrder.getShipNumber())).get(0).toString()));
			serviceOrder.setActualExpense(new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString()));
if(serviceOrder.getJob().equals("HVY")){
			BigDecimal sum =(new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()));
			if(sum !=null && sum.doubleValue()>0){
			sum=sum.add(new BigDecimal((accountLineManager.getProjectedActualRevenueSumHVY(serviceOrder.getShipNumber())).get(0).toString()));
			serviceOrder.setActualRevenue(sum);
			}else{
				serviceOrder.setActualRevenue(new BigDecimal(0.00));	
			}
}else{
			serviceOrder.setActualRevenue(new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()));
			 }
			serviceOrder.setActualGrossMargin((new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract (new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
			BigDecimal divisormar=new BigDecimal(((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()));
			if(serviceOrder.getActualRevenue()== null || serviceOrder.getActualRevenue().equals(new BigDecimal("0"))||serviceOrder.getActualRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getActualRevenue().equals(new BigDecimal("0.0000"))){
				serviceOrder.setActualGrossMarginPercentage(new BigDecimal("0"));
			}
			else{
				serviceOrder.setActualGrossMarginPercentage((((new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract (new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(divisormar,2));
				}
				try{
				serviceOrderManager.updateFromAccountLine(serviceOrder);
				}catch(Exception e){
					e.printStackTrace();
					
				}
				serviceOrder = serviceOrderManager.get(sid);
				if(accountLineStatus==null||accountLineStatus.equals(""))
				{
					accountLineStatus="true";
				}
				if(!"category".equals(vanLineAccountView)){
			    	accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			    	getSession().setAttribute("accountLines", accountLineList);
				}
				
				if("category".equals(vanLineAccountView)){
			    	   vanLineAccCategoryURL = "?sid="+serviceOrder.getId()+"&vanLineAccountView="+vanLineAccountView;
			    	   return "category"; 
			    }
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.getCause().printStackTrace();
	    	 return "errorlog";
		}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    		return SUCCESS;
	    
    } 
	
   
    
    //for service manager List
    
    public String list() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
    	serviceOrder=serviceOrderManager.get(sid); 
    	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
    	accountLineList =accountLineManager.getAll();
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }
    @SkipValidation
    public String findOurInviceList() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }
    
    @SkipValidation
    public String findMilitaryBaseList() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }
    
    @SkipValidation
    public String findVendorInviceList() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }
    @SkipValidation
    public String findAgentPayInviceList() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }
    @SkipValidation
    public String getAuthorizationNo()
    {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	authorizationNoList=accountLineManager.findAuthorizationNo(shipNumber); 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
    
    @SkipValidation
    public String searchOurInviceList() { 
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String companydivisionSetCode="";
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User)auth.getPrincipal();
			//List dataSecuritySet= customerFileManager.getFilterData(userOpenOrderInitiationBillToCode.getId(), sessionCorpID); 
			
			Map filterMap=user.getFilterMap(); 
			List list2 = new ArrayList();
			List partnerCorpIDList = new ArrayList(); 
			if(filterMap.containsKey("serviceOrderCompanyDivisionFilter")){
					HashMap valueMap=	(HashMap)filterMap.get("serviceOrderCompanyDivisionFilter");
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					}
					
			Iterator listIterator= partnerCorpIDList.iterator();
			while (listIterator.hasNext()) {
				list2.add("'" + listIterator.next().toString() + "'");	
			}
			companydivisionSetCode= list2.toString().replace("[","").replace("]", "");
			}
			String invoiceNum=(accountLine.getRecInvoiceNumber() == null? "":accountLine.getRecInvoiceNumber());
			String billTo=(accountLine.getBillToCode() == null? "":accountLine.getBillToCode());
			String billToName=(accountLine.getBillToName() == null? "":accountLine.getBillToName());
			String shipNum=(accountLine.getShipNumber() == null? "":accountLine.getShipNumber());
			findOurInviceList=accountLineManager.searchOurInvoice(invoiceNum, billTo, shipNum,sessionCorpID,billToName,companydivisionSetCode);
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		} 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }
    private List totalOfInvoiceList;
    @SkipValidation    
    public String findTotalOfInvoice(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	try{
		totalOfInvoiceList=accountLineManager.findTotalOfInvoice(recInvoiceNumber,shipNumber,sessionCorpID);
    	}catch(Exception e)
		{
			 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
    
    @SkipValidation
    public String searchMilitaryBaseList() { 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	
    	try{
    	findMilitaryBaseList=accountLineManager.searchMilitaryBase(baseCode, description, terminalState,SCACCode,sessionCorpID); 
    	}catch(Exception e)
		{
			 e.printStackTrace();
		}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }
    
    @SkipValidation
    public String searchVendorInviceList() {
    	
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String companydivisionSetCode="";
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User)auth.getPrincipal();
			
			Map filterMap=user.getFilterMap(); 
			List list2 = new ArrayList();
			List partnerCorpIDList = new ArrayList(); 
			if(filterMap.containsKey("serviceOrderCompanyDivisionFilter")){
					HashMap valueMap=	(HashMap)filterMap.get("serviceOrderCompanyDivisionFilter");
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					}
					
			Iterator listIterator= partnerCorpIDList.iterator();
			while (listIterator.hasNext()) {
				list2.add("'" + listIterator.next().toString() + "'");	
			}
			companydivisionSetCode= list2.toString().replace("[","").replace("]", "");
			}
			String invoiceNum=(accountLine.getInvoiceNumber() == null? "":accountLine.getInvoiceNumber());
			String name=(accountLine.getEstimateVendorName() == null? "":accountLine.getEstimateVendorName());
			String shipNum=(accountLine.getShipNumber() == null? "":accountLine.getShipNumber());
			findVendorInviceList=accountLineManager.searchVendorInvoice(invoiceNum, name, shipNum,sessionCorpID,companydivisionSetCode,accountLine.getInvoiceDate());
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		} 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }

    @SkipValidation
	public String findAgentInvoice(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String partnerCodePopup;
    @SkipValidation
    public String searchAgentPayInviceList() {
       	try {
    			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    			User user = (User) auth.getPrincipal();  
    			Map filterMap=user.getFilterMap(); 
    	    	List list = new ArrayList();
    	    	Set partnerSet = new HashSet();
    	    	Set allPartnerSet = new HashSet();
    	    	Set finalPartnerSet = new HashSet();
    	    	List partnerCorpIDList = new ArrayList(); 
    	    	if(filterMap.containsKey("agentFilter")){
    		    	 HashSet valueSet=	(HashSet)filterMap.get("agentFilter");	
    		    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
    		    	Iterator iterator = valueSet.iterator();
    		    	while (iterator.hasNext()) {
    		    		String  mapkey = (String)iterator.next(); 
    				    if(filterMap.containsKey(mapkey)){	
    					
    					HashMap valueMap=	(HashMap)filterMap.get(mapkey);
    					if(valueMap!=null){
    						Iterator mapIterator = valueMap.entrySet().iterator();
    						while (mapIterator.hasNext()) {
    							Map.Entry entry = (Map.Entry) mapIterator.next();
    							String key = (String) entry.getKey(); 
    						    partnerCorpIDList= (List)entry.getValue(); 
    						}
    					}
    				    }}}
    					
    	    	Iterator listIterator= partnerCorpIDList.iterator();
    	    	while (listIterator.hasNext()) {
    	    		String code=listIterator.next().toString();
    	    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
    	    			list.add("'" + code + "'");	
    	    		
    	    		} 	
    	    	} 
    	    	//companydivisionSetCode= list.toString().replace("[","").replace("]", "");
    	    	} 
    			partnerCodePopup = list.toString().replace("[","").replace("]", "");  
    			String invoiceNum=(accountLine.getInvoiceNumber() == null? "":accountLine.getInvoiceNumber());
    			String name=(accountLine.getEstimateVendorName() == null? "":accountLine.getEstimateVendorName());
    			String shipNum=(accountLine.getShipNumber() == null? "":accountLine.getShipNumber());
    			findVendorInviceList=accountLineManager.agentPayInviceList(invoiceNum, name, shipNum,sessionCorpID,partnerCodePopup,accountLine.getInvoiceDate());
    		} catch (Exception e) {
    	    	 e.printStackTrace();
    			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
    	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
    	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
    	    	 return "errorlog";
    		} 
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;
	   
    }
	
	@SkipValidation
	public String searchAgentInviceList(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String invoiceNum=(accountLine.getInvoiceNumber() == null? "":accountLine.getInvoiceNumber());
			String shipNum=(accountLine.getShipNumber() == null? "":accountLine.getShipNumber());
			String regNum=(serviceOrder.getRegistrationNumber()==null?"":serviceOrder.getRegistrationNumber());
			try{
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				User user = (User)auth.getPrincipal();
				SortedMap userDataSecurityMap = new TreeMap();
				String key = new String("");
				String samekey = new String("");
				StringBuffer allFilterValues=new StringBuffer();
				StringBuffer securityQueryBuffer=new StringBuffer("  and ");
				String securityQuery=new String ();
				String dataSetName="";
				List userDataSecurityList=serviceOrderManager.getUserDateSecurity(user.getId(), sessionCorpID);
				Iterator userDataSecurityIterator = userDataSecurityList.iterator();
				
				while (userDataSecurityIterator.hasNext()) {
					Object DataSecurityObject = userDataSecurityIterator.next();
					String tableName = (String) ((DTOUserDataSecurity) DataSecurityObject).getTableName();
					String fieldName = (String) ((DTOUserDataSecurity) DataSecurityObject).getFieldName();
					String filterValues = (String) ((DTOUserDataSecurity) DataSecurityObject).getFilterValues();
					
					dataSetName=(String) ((DTOUserDataSecurity) DataSecurityObject).getDataSetName();
					if (!(dataSetName.startsWith("DATA_SECURITY_SET_SO_BILLTOCODE_"))) {
					key = tableName.trim().charAt(0)+"." +fieldName.trim(); 
					if (key.equals(samekey)) {
						allFilterValues=allFilterValues.append("'");
						allFilterValues=allFilterValues.append((String)(filterValues));
						allFilterValues=allFilterValues.append("',"); 
					} else if (!key.equals(samekey)) {
						allFilterValues= new StringBuffer();
						allFilterValues=allFilterValues.append("'");
						allFilterValues=allFilterValues.append((String)(filterValues));
						allFilterValues=allFilterValues.append("',"); 
					} 
					
					samekey = tableName.trim().charAt(0)+"." +fieldName.trim(); 
					try { 
						String userDataSecurityMapValue = allFilterValues.toString() ; 
						userDataSecurityMap.put(key, userDataSecurityMapValue); 
					} catch (Exception e) {
						e.printStackTrace();
					} 
				}else{
					Map filterMap=user.getFilterMap(); 
					if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
					 
						StringBuffer allFilterValues1=new StringBuffer();
			    		List partnerCorpIDList = new ArrayList(); 
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									
								    partnerCorpIDList= (List)entry.getValue(); 
								}
							}
							
			    	Iterator listIterator= partnerCorpIDList.iterator();
			    	while (listIterator.hasNext()) {
			    		String code=listIterator.next().toString();
			    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
			    			allFilterValues1=allFilterValues1.append("'");
			    			allFilterValues1=allFilterValues1.append((String)(code));
			    			allFilterValues1=allFilterValues1.append("',"); 
			    		}
			    	} 
			    	key ="s.billToCode";
			    	String userDataSecurityMapValue = allFilterValues1.toString() ; 
			    	userDataSecurityMap.put(key, userDataSecurityMapValue); 
			    	userDataSecurityMap.put("a.billToCode", userDataSecurityMapValue); 
			    	}
				}
				}
				Iterator mapIterator = userDataSecurityMap.entrySet().iterator();
				while (mapIterator.hasNext()) {
					Map.Entry entry = (Map.Entry) mapIterator.next();
					String filtertable = (String) entry.getKey();
					String filterValue = (String) entry.getValue(); 
					if (!filterValue.equals("")) {
						filterValue = filterValue.substring(0, filterValue.length() - 1);
					} else if (filterValue.equals("")) {
						filterValue = new String("''");
					}
						if(mapIterator.hasNext()){
						securityQueryBuffer=securityQueryBuffer.append("  "+filtertable +" in  ("+filterValue+ ") and");
						}
						else if(!(mapIterator.hasNext())){
							securityQueryBuffer=securityQueryBuffer.append("  "+filtertable +" in  ("+filterValue+ ") ");	
						}
					
				}
				securityQuery=securityQueryBuffer.toString();
		
			findVendorInviceList=accountLineManager.searchAgentInviceList(invoiceNum, shipNum,sessionCorpID, securityQuery, regNum);
			if(findVendorInviceList.size()==1){
			    detailPage="true";
			}
			}catch(Exception ex)
			{ex.printStackTrace();
				
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

	    	 return "errorlog";
		}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
	}
	private String detailPage;
	public String estimateAccountLineList()
	{   
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		try{
		accountLineEstimateList = accountLineManager.getAll(); 
		}catch(Exception e)
		{
			 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS; 
	}

	
	@SkipValidation
	public String accountLineBillToCode() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID); 
    	serviceOrder = serviceOrderManager.get(id); 
    	partners = new ArrayList(partnerManager.findByBillToCode(serviceOrder.getShipNumber())); 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    }
	
	@SkipValidation
	public String findNetworkBillToCodeList(){

		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID); 
    	try{
    	partners = new ArrayList(partnerManager.findByNetworkPartnerCode(networkPartnerCode,sessionCorpID,"","","","","","","","")); 
    	}catch(Exception e)
		{
			 e.printStackTrace();
		}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    
	}
	@SkipValidation
	public String findNetworkBillToCodeSearchList(){

		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String lastName="";
		String partnerCode="";
		String billingCountryCode="";
		String billingCountry="";
		String billingStateCode="";
		String extReference="";
		String aliasName="";
		String vanlineCode="";
		 
		if(partner !=null){
		lastName = partner.getLastName() == null ? "" : partner.getLastName().trim();
		partnerCode = partner.getPartnerCode() == null ? "" : partner.getPartnerCode().trim();
		aliasName=partner.getAliasName()==null ? "" :partner.getAliasName().trim();
			if (partner.getBillingCountryCode() != null && (!"".equals(partner.getBillingCountryCode()))) {
				billingCountryCode = partner.getBillingCountryCode().trim();
			}else if(partner.getTerminalCountryCode() != null && (!"".equals(partner.getTerminalCountryCode()))){
				billingCountryCode = partner.getTerminalCountryCode().trim();
			}
			else {
				billingCountryCode = "";
			} 
			if (partner.getBillingState() != null) {
				billingStateCode = partner.getBillingState().trim();
			}else if(partner.getTerminalState() != null){
				billingStateCode = partner.getTerminalState().trim();
			}else{
				billingStateCode = "";
			}
			
			if (partner.getBillingCountry() != null) {
				billingCountry = partner.getBillingCountry().trim();
			}else if(partner.getTerminalCountry() != null){
				billingCountry = partner.getTerminalCountry().trim();
			}else{
				billingCountry = "";
			}

			if ((partner.getExtReference() == null)) {
				extReference = "";
			} else {
				extReference = partner.getExtReference().trim();
			}
		}
    	
    	partners = new ArrayList(partnerManager.findByNetworkPartnerCode(networkPartnerCode,sessionCorpID,lastName,partnerCode,billingCountryCode,billingCountry,billingStateCode,extReference,aliasName,vanlineCode)); 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    
	}
	
	
	@SkipValidation
	  public String networkPartnerDetailsAutocompleteAjax(){   
		try { 
			if(autocompleteDivId!=null && (autocompleteDivId.indexOf("accountestimateVendorDiv")>=0 || autocompleteDivId.indexOf("estimateVendorNameDiv")>=0 || autocompleteDivId.contains("estimateVendorNameDiv"))){
				acctRefVisiable="Yes";
			}
			
			if((autocompleteCondition.lastIndexOf("'") == autocompleteCondition.length() - 1)){
		    	autocompleteCondition = autocompleteCondition.substring(0, autocompleteCondition.length() - 1);
		    }
		   if((autocompleteCondition.indexOf("'") == 0)){
		    	autocompleteCondition = autocompleteCondition.substring(1, autocompleteCondition.length());
		    }
		    partnerDetailsAutoComplete=partnerManager.findNetworkPartnerDetailsForAutoComplete(partnerNameAutoCopmlete,autocompleteCondition,sessionCorpID);
			partnerDetailsAutoCompleteGsonData = new Gson().toJson(partnerDetailsAutoComplete);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    
	    	 return CANCEL;
		}
			return SUCCESS; 
	  }
	
	@SkipValidation
	public String findNetworkBillToName(){

		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");

		try{
		accountLineList = new ArrayList(partnerManager.findByNetworkBillToName(networkPartnerCode,sessionCorpID,networkBillToCode)); 
		}catch(Exception e)
		{
			 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	
	}
	
    public String editAfterSubmit () throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if ( save != null ) { 
    		return save();
    	} 
    		//generateInvoiceNumber(); 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return "invoice";
    	
    }   
// for lookup and combobox

    private List refMasters;
	private RefMasterManager refMasterManager;
	private  Map<String, String> category;
	private Map<String, String> relocationServices;
	private  Map<String,String>basis;
	private  Map<String,String>country;
	private  Map<String,String>recRateCurrency;
	private  Map<String,String>currencyExchangeRate;
	private  Map<String,String>payingStatus;
	private  Map<String,String>payingStatusPopUpList;
	private  Map<String,String>paymentStatus;
	private  Map<String,String>euVatList;
	private  Map<String,String>vatBillingGroupList;
	private  Map<String,String>payVatList;
	private  Map<String,String>UTSIpayVatList;
	private  Map<String,String>euVatPercentList;
	private  Map<String,String>payVatPercentList;
	private  Map<String,String>UTSIpayVatPercentList;
	private  Map<String,String>UTSIeuVatPercentList;
	private Map<String, String> euvatRecGLMap = new LinkedHashMap<String, String>();
	private Map<String, String> payVatPayGLMap = new LinkedHashMap<String, String>();
	private Map<String, String> qstEuvatRecGLMap = new LinkedHashMap<String, String>();
	private Map<String, String> qstPayVatPayGLMap = new LinkedHashMap<String, String>();
	private Map<String, String> qstEuvatRecGLAmtMap = new LinkedHashMap<String, String>();
	private Map<String, String> qstPayVatPayGLAmtMap = new LinkedHashMap<String, String>();
	private  String payingStatusValue;
	private Map<String,String> division;
	private Map<String,String> divisionTemp; 
	
    public String getpayingStatusValue() {
		return payingStatusValue;
	}

	public void setpayingStatusValue(String payingStatusValue) {
		this.payingStatusValue = payingStatusValue;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
        this.refMasterManager = refMasterManager;
    }
    
    public List getRefMasters(){
    	return refMasters;
    }
    


    public String getComboList(String corpID){
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String parameters="'PAYVATDESC','ACC_BASIS','CURRENCY','ACC_STATUS','PAY_STATUS','EUVAT','ACC_CATEGORY','VATBILLINGGROUP'";
			LinkedHashMap <String,String> tempParemeterMap = new LinkedHashMap<String, String>();
			 payVatList= new LinkedHashMap<String, String>();
			 category= new LinkedHashMap<String, String>(); 
			 basis= new LinkedHashMap<String, String>();
			 country=new TreeMap<String, String>();
			 paymentStatus= new LinkedHashMap<String, String>();
			 payingStatus=new LinkedHashMap<String, String>();
			 payingStatusPopUpList=new LinkedHashMap<String, String>();
			 euVatList=new LinkedHashMap<String,String>();
			 vatBillingGroupList = new LinkedHashMap<String,String>();
			 List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpID,parameters);
		        for (RefMasterDTO refObj : allParamValue) {
			 	if(refObj.getParameter().trim().equalsIgnoreCase("PAYVATDESC")){
			 		payVatList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
			 	}else if(refObj.getParameter().trim().equals("ACC_CATEGORY")){
			 		category.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
			 	}
			 	else if(refObj.getParameter().trim().equals("ACC_BASIS"))
			 	{
			 	basis.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 }
		 else if(refObj.getParameter().trim().equals("CURRENCY"))
		 {  
           country.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getCode().trim());
		 }
		 else if(refObj.getParameter().trim().equals("ACC_STATUS"))
		 {
			 
				payingStatus.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
				payingStatusPopUpList.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 
		 }
		 else if(refObj.getParameter().trim().equals("PAY_STATUS"))
		 {
			 
				paymentStatus.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 
		 }
		
		 else if(refObj.getParameter().trim().equals("EUVAT"))
		 {
			 
			 euVatList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 
		 }
		 else if(refObj.getParameter().trim().equals("VATBILLINGGROUP"))
		 {
			 
			 vatBillingGroupList.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 
		 }	 	
		 }
	 
			relocationServices=refMasterManager.findByRelocationServices(corpID, "RELOCATIONSERVICES");
     // euVatList = refMasterManager.findByParameterWithoutParent(corpID, "EUVAT");
      //payVatList = refMasterManager.findByParameterWithoutParent (corpID, "PAYVATDESC");
      euVatPercentList = refMasterManager.findVatPercentList(corpID, "EUVAT");
      payVatPercentList = refMasterManager.findVatPercentList(corpID, "PAYVATDESC");
      euvatRecGLMap = refMasterManager.findByParameterVatRecGL(sessionCorpID, "EUVAT");
      payVatPayGLMap = refMasterManager.findByParameterVatPayGL(sessionCorpID, "PAYVATDESC");
      flex1RecCodeList=refMasterManager.findRecVatDescriptionData(sessionCorpID, "vatBillingGroup");
	  flex2PayCodeList=refMasterManager.findPayVatDescriptionData(sessionCorpID, "vatBillingGroup");
      Map<String, Map<String, String>> valueMap=refMasterManager.findByParameterQstVatGL(sessionCorpID);
      qstEuvatRecGLMap = valueMap.get("EUVAT");
      qstPayVatPayGLMap = valueMap.get("PAYVATDESC");
      valueMap=refMasterManager.findByParameterQstVatAmtGL(sessionCorpID);
      qstEuvatRecGLAmtMap = valueMap.get("EUVAT");
      qstPayVatPayGLAmtMap = valueMap.get("PAYVATDESC");
     

   recRateCurrency=refMasterManager.findCodeOnleByParameter(corpID, "BILLING_CURRENCY");
   currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);

   try {
		if(serviceOrderCombo!=null){
		}else{
		try {
			if(getRequest().getParameter("sid")!=null){
				sid = new Long(getRequest().getParameter("sid").toString());
			 if(sid!=null){
				 serviceOrderCombo = serviceOrderManager.get(sid);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		} 
   }catch (Exception e) {
		e.printStackTrace();
	}
   if(serviceOrderCombo!=null && serviceOrderCombo.getCorpID()!=null && (!(serviceOrderCombo.getCorpID().trim().equals(""))) && serviceOrderCombo.getCompanyDivision() !=null && (!(serviceOrderCombo.getCompanyDivision().trim().equals("")))  ){
	  companyDivis = customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(serviceOrderCombo.getCorpID(), "",serviceOrderCombo.getCompanyDivision());
   
   }else{
   companyDivis=customerFileManager.findCompanyDivision(corpID);    
   } 
   division=refMasterManager.findMapList(corpID,"JOB");
   divisionTemp=refMasterManager.findCountryIso2(corpID,"JOB");
		} catch (Exception e) {
		 	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	   
		}
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS;
    }
    
    @SkipValidation
    public String getGlTypeList(){
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			getGlTypeList=accountLineManager.findGlTypeList(billingContract, charge); 
			 if(getGlTypeList.isEmpty() || getGlTypeList==null){
				 getGlTypeList=new ArrayList();  
			 }
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    
		} 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;	
    }
	@SkipValidation
    public String getCommissionList(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			getCommissionList =accountLineManager.findCommissionList(billingContract, charge);
			
			 if(getCommissionList.isEmpty() || getCommissionList==null){
				getCommissionList=new ArrayList(); 	
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		} 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;	
    }
	
	@SkipValidation
    public String getDistributedAmt(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
			serviceOrder=serviceOrderManager.get(sid);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			miscellaneous = miscellaneousManager.get(sid);
			billing= billingManager.get(sid);
			String driverAgencyCode = "";
			List aa = accountLineManager.findDriverAgency(sid);
			if(!aa.isEmpty()){
				driverAgencyCode = aa.get(0).toString();
			}
	
			Map values = new HashMap();
			values.put("partner.driverAgency", driverAgencyCode);
			values.put("accountline.branchCode", branchCode);
			values.put("accountline.chargeCode", chargeCode);
			if(billing.getDiscount()==null){
				values.put("billing.discount",0.00 );
			}else{
				values.put("billing.discount",billing.getDiscount());
			}
			if(billing.getOtherDiscount()==null){
				values.put("billing.otherDiscount",0.00 );
			}else{
				values.put("billing.otherDiscount",billing.getOtherDiscount());
			}
			values.put("miscellaneous.actualNetWeight", miscellaneous.getActualNetWeight());
			values.put("accountline.actualRevenue", new BigDecimal(actualRevanue));
			values.put("accountline.description", desc);
			String expression = accountLineManager.findExpression(glType);	
			Object totRev = expressionManager.executeExpression(expression, values);
			totalRevenue = totRev.toString();
		}catch(Exception e){
			 e.printStackTrace();
			totalRevenue = "0.00";
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
			
	}
	
	@SkipValidation
    public String getCommAmt(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
			serviceOrder=serviceOrderManager.get(sid);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			vanLineCommissionType = (VanLineCommissionType)accountLineManager.findCommissionExpression(glcomm).get(0);
			Map values = new HashMap();
			values.put("serviceorder.bookingAgentCode",serviceOrder.getBookingAgentCode());
			values.put("accountline.distributionAmount", new BigDecimal(distAmt));
			values.put("accountline.actualRevenue", new BigDecimal(actualAmt));
			values.put("vanLineCommissionType.pc", vanLineCommissionType.getPc());  
			String expression = vanLineCommissionType.getCalculation();
			Object totRev = expressionManager.executeExpression(expression, values);
		
			totalRevenue = new BigDecimal(totRev.toString()).toString();
		}catch(Exception e){
			 e.printStackTrace();
			totalRevenue = "0.00";
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;
			
	}
	
	@SkipValidation
	public String findCountryCodeEU(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
		countryCodeEUResult=refMasterManager.findCountryCodeEU(originCountryCode,destinationCountryCode,sessionCorpID);
		}catch(Exception e)
		{
			 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	@SkipValidation
	public String findBilltoCodeVatNumber(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
		countryCodeEUResult=accountLineManager.findBilltoCodeVatNumber(billToCode,sessionCorpID);
		}catch(Exception e)
		{
			 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	private String currencyByBillToCode;
	@SkipValidation
	public String fillCurrencyByBillToCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
		currencyByBillToCode=accountLineManager.fillCurrencyByBillToCode(billToCode,sessionCorpID);
		}catch(Exception e)
		{
			 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	private String currencyByChargeCode;
	@SkipValidation
	public String fillCurrencyByChargeCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
		currencyByChargeCode=accountLineManager.fillCurrencyByChargeCode(vendorCode,sessionCorpID);
		}catch(Exception e)
		{
			 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	@SkipValidation
	public String findbillingCountryCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
		countryCodeEUResult=accountLineManager.findbillingCountryCode(billToCode,sessionCorpID);
		}catch(Exception e)
		{
			 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	@SkipValidation
	 public String calculateDistribution(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			 Object totDistAmt = new Object();
			 Object totRev = new Object();
			 serviceOrder=serviceOrderManager.get(sid);
			 accountLineList = accountLineManager.getAccountLinesID(serviceOrder.getShipNumber());
			 Iterator it=accountLineList.listIterator();
			 while(it.hasNext()){
				  Long row=(Long)it.next();
				  accountLine = accountLineManager.get(row);
				  try{
						miscellaneous = miscellaneousManager.get(sid);
						billing= billingManager.get(sid);
						String driverAgencyCode = "";
						List aa = accountLineManager.findDriverAgency(sid);
						if(!aa.isEmpty()){
							driverAgencyCode = aa.get(0).toString();
						}
						
						Map values = new HashMap();
						values.put("partner.driverAgency", driverAgencyCode);
						values.put("accountline.branchCode", accountLine.getBranchCode());
						values.put("accountline.chargeCode", accountLine.getChargeCode());
						if(billing.getDiscount()==null){
							values.put("billing.discount",0.00 );
						}else{
							values.put("billing.discount",billing.getDiscount());
						}
						if(billing.getOtherDiscount()==null){
							values.put("billing.otherDiscount",0.00 );
						}else{
							values.put("billing.otherDiscount",billing.getOtherDiscount());
						}
						values.put("miscellaneous.actualNetWeight", miscellaneous.getActualNetWeight());
						values.put("accountline.actualRevenue", accountLine.getActualRevenue());
						values.put("accountline.description", accountLine.getDescription());
						String expression = accountLineManager.findExpression(accountLine.getGlType());	
						totDistAmt = expressionManager.executeExpression(expression, values);
						accountLine.setDistributionAmount(new BigDecimal(totDistAmt.toString()));
						totalRevenue = totDistAmt.toString();
						accountLineManager.save(accountLine);
					}catch(Exception e){
						accountLine.setDistributionAmount(new BigDecimal("0.00"));
						totalRevenue = "0.00";
						e.printStackTrace();
					}
					
					try{
						vanLineCommissionType = (VanLineCommissionType)accountLineManager.findCommissionExpression(accountLine.getCommission()).get(0);
						Map values = new HashMap();
						values.put("serviceorder.bookingAgentCode",serviceOrder.getBookingAgentCode());
						values.put("accountline.distributionAmount", new BigDecimal(totDistAmt.toString()));
						values.put("vanLineCommissionType.pc", vanLineCommissionType.getPc());
						
						String expression = vanLineCommissionType.getCalculation();
						totRev = expressionManager.executeExpression(expression, values);
						accountLine.setActualExpense(new BigDecimal(totRev.toString()));
						totalRevenue = new BigDecimal(totRev.toString()).toString();
						accountLineManager.save(accountLine);
					}catch(Exception e){
						accountLine.setActualExpense(new BigDecimal(totRev.toString()));
						totalRevenue = "0.00";
						e.printStackTrace();
					}
			  }
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
	    	 return "errorlog";
		}
	     logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		 return SUCCESS;
	 }
	
	private BigDecimal buyRateEstimate;
	private BigDecimal sellRateEstimate;
	private BigDecimal buyRateRevision;
	private BigDecimal sellRateRevision;
	private BigDecimal localAmount;
	private BigDecimal payVatAmt;
	private BigDecimal estVatAmt;
	private BigDecimal revVatAmt;
	private Integer estimatePassPercentage;
	private Integer revisionPassPercentage;
	private BigDecimal actualExpense;
	private String masterId;
	private List<ServiceOrder> serviceOrdersList;
	private String weights;
	private AccountLine groupAccountLine;
	private ServiceOrder groupServiceOrder;
	private List maxAccLineNumber;
	private double setRate;
	private List listOfGroupAccount=new ArrayList();
	private BigDecimal estLocalRate;
	private BigDecimal estLocalAmount;
	private BigDecimal revisionLocalRate;
	private BigDecimal revisionLocalAmount;
	
	
	public String distributeOrdersAmount(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Long maxLine=new Long(0);
		try{
			getGlTypeList();
	        getCommissionList();
			String str= save();
			 buyRateEstimate=accountLine.getEstimateRate();
			 sellRateEstimate=accountLine.getEstimateSellRate();
			 buyRateRevision=accountLine.getRevisionRate();
			 sellRateRevision=accountLine.getRevisionSellRate();
			 localAmount=accountLine.getLocalAmount();
			 payVatAmt=accountLine.getPayVatAmt();
			 estVatAmt=accountLine.getEstVatAmt();
			 revVatAmt=accountLine.getRevisionVatAmt();			 
			 estimatePassPercentage=accountLine.getEstimatePassPercentage();
			 revisionPassPercentage=accountLine.getRevisionPassPercentage();
			 actualExpense=accountLine.getActualExpense();
			 estLocalRate=accountLine.getEstLocalRate();
			 estLocalAmount=accountLine.getEstLocalAmount();
			 revisionLocalRate=accountLine.getRevisionLocalRate();
			 revisionLocalAmount=accountLine.getRevisionLocalAmount();
			 
		if(str.equalsIgnoreCase("SUCCESS")){			 
		serviceOrdersList=serviceOrderManager.findServiceOrderByGrpId(masterId, sessionCorpID);
		
		if(serviceOrdersList!=null && !(serviceOrdersList.isEmpty())){
			Iterator it=serviceOrdersList.iterator();
			while(it.hasNext()){
				try{
					groupServiceOrder=(ServiceOrder)it.next();
					weights=miscellaneousManager.weightForAmountDistribution(groupServiceOrder.getId(),masterId, sessionCorpID);
					listOfGroupAccount=accountLineManager.checkForGroupAccount(groupServiceOrder.getShipNumber(),accountLine.getId(),accountLine.getChargeCode());
					
//FOR OLD ACCOUNTLINE 					
		if((listOfGroupAccount!=null)&&(!listOfGroupAccount.isEmpty())){
						Iterator it1 = listOfGroupAccount.iterator();
						while(it1.hasNext())
						{
							groupAccountLine=(AccountLine)it1.next();
							groupAccountLine.setUpdatedBy(getRequest().getRemoteUser());
							groupAccountLine.setUpdatedOn(new Date());
							groupAccountLine.setServiceOrderId(groupServiceOrder.getId());
							groupAccountLine.setSequenceNumber(groupServiceOrder.getSequenceNumber());
							groupAccountLine.setShipNumber(groupServiceOrder.getShipNumber());
							groupAccountLine.setServiceOrder(groupServiceOrder);
							if((sellRateEstimate!=null) && !(buyRateEstimate.toString().equalsIgnoreCase("")) && !(buyRateEstimate.toString().equalsIgnoreCase("0.00")) && !(buyRateEstimate.toString().equalsIgnoreCase("0.0")) && !(buyRateEstimate.toString().equalsIgnoreCase("0"))){
								if(weights!=null && !(weights.equalsIgnoreCase(""))){
									String[] splitWeight=weights.split("#");
									if(Double.parseDouble(splitWeight[1])>0){
										setRate=(Double.parseDouble(buyRateEstimate.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
										groupAccountLine.setEstimateRate(new BigDecimal(setRate));
									}
									else{
									
									}
								}
								else{
								
								}
							}	
							else{
							
							}
						
							if((sellRateEstimate!=null) && (!sellRateEstimate.toString().equalsIgnoreCase("")) && ((Double.parseDouble(sellRateEstimate.toString())>0)||(Double.parseDouble(sellRateEstimate.toString())<0))){
								if(weights!=null && !(weights.equalsIgnoreCase(""))){
									String[] splitWeight=weights.split("#");
									if(Double.parseDouble(splitWeight[1])>0){
										setRate=(Double.parseDouble(sellRateEstimate.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
										groupAccountLine.setEstimateSellRate(new BigDecimal(setRate));
									}
									else{
								
									}
								}
								else{
							
								}
							}
							else{
								
							}
						
							if((buyRateRevision!=null) && (!buyRateRevision.toString().equalsIgnoreCase("")) &&((Double.parseDouble(buyRateRevision.toString())>0)||(Double.parseDouble(buyRateRevision.toString())<0))){
								if(weights!=null && !(weights.equalsIgnoreCase(""))){
									String[] splitWeight=weights.split("#");
									if(Double.parseDouble(splitWeight[1])>0){
										setRate=(Double.parseDouble(buyRateRevision.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
										groupAccountLine.setRevisionRate(new BigDecimal(setRate));
									}
									else{
									//	groupAccountLine.setRevisionRate(new BigDecimal(0));
									}
								}
								else{
							//		groupAccountLine.setRevisionRate(new BigDecimal(0));
								}
							}
							else{
						//		groupAccountLine.setRevisionRate(new BigDecimal(0));
							}
							if((sellRateRevision!=null) && (!sellRateRevision.toString().equalsIgnoreCase("")) &&((Double.parseDouble(sellRateRevision.toString())>0)||(Double.parseDouble(sellRateRevision.toString())<0))){
								if(weights!=null && !(weights.equalsIgnoreCase(""))){
									String[] splitWeight=weights.split("#");
									if(Double.parseDouble(splitWeight[1])>0){
										setRate=(Double.parseDouble(sellRateRevision.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
										groupAccountLine.setRevisionSellRate(new BigDecimal(setRate));
									}
									else{
							//			groupAccountLine.setRevisionSellRate(new BigDecimal(0));
									}
								}
								else{
						//			groupAccountLine.setRevisionSellRate(new BigDecimal(0));
								}
							}
							else{
						//		groupAccountLine.setRevisionSellRate(new BigDecimal(0));
							}
							if((localAmount!=null) && !(localAmount.toString().equalsIgnoreCase("")) && !(localAmount.toString().equalsIgnoreCase("0.00")) && !(localAmount.toString().equalsIgnoreCase("0.0")) && !(localAmount.toString().equalsIgnoreCase("0"))){
								if(weights!=null && !(weights.equalsIgnoreCase(""))){
									String[] splitWeight=weights.split("#");
									if(Double.parseDouble(splitWeight[1])>0){
										setRate=(Double.parseDouble(localAmount.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
										groupAccountLine.setLocalAmount(new BigDecimal(setRate));
									}
									else{
		//								groupAccountLine.setLocalAmount(new BigDecimal(0));
									}
								}
								else{
		//							groupAccountLine.setLocalAmount(new BigDecimal(0));
								}
							}
							else{
		//						groupAccountLine.setLocalAmount(new BigDecimal(0));
							}														
							
							if((accountLine.getInvoiceNumber()!=null)&&(!accountLine.getInvoiceNumber().equalsIgnoreCase(""))&&(!accountLine.getInvoiceNumber().equalsIgnoreCase("0")))	{	groupAccountLine.setInvoiceNumber(accountLine.getInvoiceNumber());							}
							if((accountLine.getInvoiceDate()!=null)&&(!accountLine.getInvoiceDate().equals("")))							{							groupAccountLine.setInvoiceDate(accountLine.getInvoiceDate());							}
							if((accountLine.getCountry()!=null)&&(!accountLine.getCountry().equalsIgnoreCase(""))&&(!accountLine.getCountry().equalsIgnoreCase("0"))){								groupAccountLine.setCountry(accountLine.getCountry());							}
							if((accountLine.getValueDate()!=null)&&(!accountLine.getValueDate().equals(""))){ 											groupAccountLine.setValueDate(accountLine.getValueDate());							}
							if((accountLine.getReceivedDate()!=null)&&(!accountLine.getReceivedDate().equals(""))){		 							groupAccountLine.setReceivedDate(accountLine.getReceivedDate());							}
							if((accountLine.getPayingStatus()!=null)&&(!accountLine.getPayingStatus().equals(""))){ 	 							groupAccountLine.setPayingStatus(accountLine.getPayingStatus());							}
							if((accountLine.getPayVatDescr()!=null)&&(!accountLine.getPayVatDescr().equals(""))&&(!accountLine.getPayVatDescr().equals("0"))){								groupAccountLine.setPayVatDescr(accountLine.getPayVatDescr());							}
							if((accountLine.getPayVatPercent()!=null)&&(!accountLine.getPayVatPercent().equals(""))&&(!accountLine.getPayVatPercent().equals("0"))){							groupAccountLine.setPayVatPercent(accountLine.getPayVatPercent());							}
							if((accountLine.getEstVatDescr()!=null)&&(!accountLine.getEstVatDescr().equals(""))&&(!accountLine.getEstVatDescr().equals("0"))){								groupAccountLine.setEstVatDescr(accountLine.getEstVatDescr());							}
							if((accountLine.getEstVatPercent()!=null)&&(!accountLine.getEstVatPercent().equals(""))&&(!accountLine.getEstVatPercent().equals("0"))){							groupAccountLine.setEstVatPercent(accountLine.getEstVatPercent());							}
							if((accountLine.getRevisionVatDescr()!=null)&&(!accountLine.getRevisionVatDescr().equals(""))&&(!accountLine.getRevisionVatDescr().equals("0"))){								groupAccountLine.setRevisionVatDescr(accountLine.getRevisionVatDescr());							}
							if((accountLine.getRevisionVatPercent()!=null)&&(!accountLine.getRevisionVatPercent().equals(""))&&(!accountLine.getRevisionVatPercent().equals("0"))){							groupAccountLine.setRevisionVatPercent(accountLine.getRevisionVatPercent());							}
							if((accountLine.getPayPayableDate()!=null)&&(!accountLine.getPayPayableDate().equals(""))){							groupAccountLine.setPayPayableDate(accountLine.getPayPayableDate());							}
							if((accountLine.getPayGl()!=null)&&(!accountLine.getPayGl().equals(""))){							groupAccountLine.setPayGl(accountLine.getPayGl());						}
							if((accountLine.getPayPostDate()!=null)&&(!accountLine.getPayPostDate().equals(""))){							groupAccountLine.setPayPostDate(accountLine.getPayPostDate());						}
							if((accountLine.getAccruePayable()!=null)&&(!accountLine.getAccruePayable().equals(""))){							groupAccountLine.setAccruePayable(accountLine.getAccruePayable());							}
							if((accountLine.getPayAccDate()!=null)&&(!accountLine.getPayAccDate().equals(""))){							groupAccountLine.setPayAccDate(accountLine.getPayAccDate());							}
							if((accountLine.getCategory()!=null)&&(!accountLine.getCategory().equals(""))){							groupAccountLine.setCategory(accountLine.getCategory());							}
							if((accountLine.getCompanyDivision()!=null)&&(!accountLine.getCompanyDivision().equals(""))){							groupAccountLine.setCompanyDivision(accountLine.getCompanyDivision());							}
							if((accountLine.getVendorCode()!=null)&&(!accountLine.getVendorCode().equals(""))){							groupAccountLine.setVendorCode(accountLine.getVendorCode());							}
							if((accountLine.getEstimateVendorName()!=null)&&(!accountLine.getEstimateVendorName().equals(""))){							groupAccountLine.setEstimateVendorName(accountLine.getEstimateVendorName());							}
							if((accountLine.getActgCode()!=null)&&(!accountLine.getActgCode().equals(""))){							groupAccountLine.setActgCode(accountLine.getActgCode());							}
							if((accountLine.getChargeCode()!=null)&&(!accountLine.getChargeCode().equals(""))){							groupAccountLine.setChargeCode(accountLine.getChargeCode()); groupAccountLine.setVATExclude(accountLine.getVATExclude());							}
							if((accountLine.getEstimateDate()!=null)&&(!accountLine.getEstimateDate().equals(""))){							groupAccountLine.setEstimateDate(accountLine.getEstimateDate());							}
							if((accountLine.getEstCurrency()!=null)&&(!accountLine.getEstCurrency().equals(""))){						groupAccountLine.setEstCurrency(accountLine.getEstCurrency());							}
							if((accountLine.getEstValueDate()!=null)&&(!accountLine.getEstValueDate().equals(""))){							groupAccountLine.setEstValueDate(accountLine.getEstValueDate());							}
							if((accountLine.getBasis()!=null)&&(!accountLine.getBasis().equals(""))){							groupAccountLine.setBasis(accountLine.getBasis());						}
							if((accountLine.getRevisionCurrency()!=null)&&(!accountLine.getRevisionCurrency().equals(""))){							groupAccountLine.setRevisionCurrency(accountLine.getRevisionCurrency());							}
							if((accountLine.getRevisionValueDate()!=null)&&(!accountLine.getRevisionValueDate().equals(""))){							groupAccountLine.setRevisionValueDate(accountLine.getRevisionValueDate());							}
							if((accountLine.getEstimateQuantity()!=null)&&(!accountLine.getEstimateQuantity().equals(""))&&((Double.parseDouble(accountLine.getEstimateQuantity().toString())>0)||(Double.parseDouble(accountLine.getEstimateQuantity().toString())<0))){							groupAccountLine.setEstimateQuantity(accountLine.getEstimateQuantity());							}
							if((accountLine.getRevisionQuantity()!=null)&&(!accountLine.getRevisionQuantity().equals(""))&&((Double.parseDouble(accountLine.getRevisionQuantity().toString())>0)||(Double.parseDouble(accountLine.getRevisionQuantity().toString())<0))){							groupAccountLine.setRevisionQuantity(accountLine.getRevisionQuantity());						}
							if((accountLine.getReference()!=null)&&(!accountLine.getReference().equals(""))){							groupAccountLine.setReference(accountLine.getReference());							}
							if((accountLine.getEstLocalRate()!=null)&&(!accountLine.getEstLocalRate().equals(""))&&((Double.parseDouble(accountLine.getEstLocalRate().toString())>0)||(Double.parseDouble(accountLine.getEstLocalRate().toString())<0))){							groupAccountLine.setEstLocalRate(accountLine.getEstLocalRate());						}
							if((accountLine.getEstLocalAmount()!=null)&&(!accountLine.getEstLocalAmount().equals(""))&&((Double.parseDouble(accountLine.getEstLocalAmount().toString())>0)||(Double.parseDouble(accountLine.getEstLocalAmount().toString())<0))){							groupAccountLine.setEstLocalAmount(accountLine.getEstLocalAmount());						}
							if((accountLine.getRevisionLocalAmount()!=null)&&(!accountLine.getRevisionLocalAmount().equals(""))&&((Double.parseDouble(accountLine.getRevisionLocalAmount().toString())>0)||(Double.parseDouble(accountLine.getRevisionLocalAmount().toString())<0))){							groupAccountLine.setRevisionLocalAmount(accountLine.getRevisionLocalAmount());						}
							if((accountLine.getRevisionLocalRate()!=null)&&(!accountLine.getRevisionLocalRate().equals(""))&&((Double.parseDouble(accountLine.getRevisionLocalRate().toString())>0)||(Double.parseDouble(accountLine.getRevisionLocalRate().toString())<0))){								groupAccountLine.setRevisionLocalRate(accountLine.getRevisionLocalRate());									}
							if((accountLine.getNote()!=null)&&(!accountLine.getNote().equals(""))){							groupAccountLine.setNote(accountLine.getNote());						}
							if(accountLine.getExchangeRate()>0){							groupAccountLine.setExchangeRate(accountLine.getExchangeRate());						}
							if((accountLine.getEstExchangeRate()!=null)&&(!accountLine.getEstExchangeRate().equals(""))&&(!accountLine.getEstExchangeRate().equals("0"))){						groupAccountLine.setEstExchangeRate(accountLine.getEstExchangeRate());							}
							groupAccountLine.setIgnoreForBilling(accountLine.isIgnoreForBilling());
							if((accountLine.getRevisionExchangeRate()!=null)&&(!accountLine.getRevisionExchangeRate().equals(""))&&(!accountLine.getRevisionExchangeRate().equals("0"))){							groupAccountLine.setRevisionExchangeRate(accountLine.getRevisionExchangeRate());							}
							//		groupAccountLine.setActualExpense(accountLine.getActualExpense());
							if((sellRateEstimate!=null) && !(buyRateEstimate.toString().equalsIgnoreCase("")) && !(buyRateEstimate.toString().equalsIgnoreCase("0.00")) && !(buyRateEstimate.toString().equalsIgnoreCase("0.0")) && !(buyRateEstimate.toString().equalsIgnoreCase("0"))){
								if(weights!=null && !(weights.equalsIgnoreCase(""))){
									String[] splitWeight=weights.split("#");
									if(Double.parseDouble(splitWeight[1])>0){
										setRate=(Double.parseDouble(buyRateEstimate.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
										BigDecimal qty=accountLine.getEstimateQuantity();
										if(qty!=null && qty!=new BigDecimal(0)){
											Double result=Double.parseDouble(qty.toString())*setRate;
											groupAccountLine.setEstimateExpense(new BigDecimal(result));
										}
										else{
											groupAccountLine.setEstimateExpense(new BigDecimal(setRate));
										}
									}
									else{
//										groupAccountLine.setEstimateExpense(new BigDecimal(0));
									}
								}
								else{
	//								groupAccountLine.setEstimateExpense(new BigDecimal(0));
								}
							}
							else{
		//						groupAccountLine.setEstimateExpense(new BigDecimal(0));
							}
							if((sellRateEstimate!=null) && !(sellRateEstimate.toString().equalsIgnoreCase("")) && !(sellRateEstimate.toString().equalsIgnoreCase("0.00")) && !(sellRateEstimate.toString().equalsIgnoreCase("0.0")) && !(sellRateEstimate.toString().equalsIgnoreCase("0"))){
								if(weights!=null && !(weights.equalsIgnoreCase(""))){
									String[] splitWeight=weights.split("#");
									if(Double.parseDouble(splitWeight[1])>0){
										setRate=(Double.parseDouble(sellRateEstimate.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
										BigDecimal qty=accountLine.getEstimateQuantity();
										if(qty!=null && qty!=new BigDecimal(0)){
											Double result=Double.parseDouble(qty.toString())*setRate;
											groupAccountLine.setEstimateRevenueAmount(new BigDecimal(result));
										}
										else{
											groupAccountLine.setEstimateRevenueAmount(new BigDecimal(setRate));
										}
									}
									else{
			//							groupAccountLine.setEstimateRevenueAmount(new BigDecimal(0));
									}
								}
								else{
				//					groupAccountLine.setEstimateRevenueAmount(new BigDecimal(0));
								}
							}
							else{
					//			groupAccountLine.setEstimateRevenueAmount(new BigDecimal(0));
							}

//calculation for EstVatAmt
							if((groupAccountLine.getEstimateRevenueAmount()!=null)&&(!groupAccountLine.getEstimateRevenueAmount().equals(""))&&((Double.parseDouble(groupAccountLine.getEstimateRevenueAmount().toString())>0)||(Double.parseDouble(groupAccountLine.getEstimateRevenueAmount().toString())<0))&&(groupAccountLine.getEstVatPercent()!=null)&&(!groupAccountLine.getEstVatPercent().equals(""))&&((Double.parseDouble(groupAccountLine.getEstVatPercent().toString())>0)||(Double.parseDouble(groupAccountLine.getEstVatPercent().toString())<0)))
							{							 	
							 	setRate = ((Double.parseDouble(groupAccountLine.getEstimateRevenueAmount().toString())*Double.parseDouble(groupAccountLine.getEstVatPercent().toString()))/100);
							 	DecimalFormat df = new DecimalFormat("#.####");
							 	String setRate1 = df.format(setRate);
							 	groupAccountLine.setEstVatAmt(new BigDecimal(setRate1));
							}else{
								groupAccountLine.setEstVatAmt(new BigDecimal(0));
							}
								
							
							if((buyRateRevision!=null) && (!buyRateRevision.toString().equalsIgnoreCase("")) && ((Double.parseDouble(buyRateRevision.toString())>0)||(Double.parseDouble(buyRateRevision.toString())<0))){
								if(weights!=null && !(weights.equalsIgnoreCase(""))){
									String[] splitWeight=weights.split("#");
									if(Double.parseDouble(splitWeight[1])>0){
										setRate=(Double.parseDouble(buyRateRevision.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
										BigDecimal qty=accountLine.getRevisionQuantity();
										if(qty!=null && qty!=new BigDecimal(0)){
											Double result=Double.parseDouble(qty.toString())*setRate;
											groupAccountLine.setRevisionExpense(new BigDecimal(result));
										}
										else{
											groupAccountLine.setRevisionExpense(new BigDecimal(setRate));
										}
									}
									else{
						//				groupAccountLine.setRevisionExpense(new BigDecimal(0));
									}
								}
								else{
					//				groupAccountLine.setRevisionExpense(new BigDecimal(0));
								}
							}
							else{
					//			groupAccountLine.setRevisionExpense(new BigDecimal(0));
							}

							if((sellRateRevision!=null) && (!sellRateRevision.toString().equalsIgnoreCase("")) && ((Double.parseDouble(sellRateRevision.toString())>0)||(Double.parseDouble(sellRateRevision.toString())<0))){
								if(weights!=null && !(weights.equalsIgnoreCase(""))){
									String[] splitWeight=weights.split("#");
									if(Double.parseDouble(splitWeight[1])>0){
										setRate=(Double.parseDouble(sellRateRevision.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
										BigDecimal qty=accountLine.getRevisionQuantity();
										if(qty!=null && qty!=new BigDecimal(0)){
											Double result=Double.parseDouble(qty.toString())*setRate;
											groupAccountLine.setRevisionRevenueAmount(new BigDecimal(result));
										}
										else{
											groupAccountLine.setRevisionRevenueAmount(new BigDecimal(setRate));
										}
									}
									else{
				//						groupAccountLine.setRevisionRevenueAmount(new BigDecimal(0));
									}
								}
								else{
					//				groupAccountLine.setRevisionRevenueAmount(new BigDecimal(0));
								}
							}
							else{
			//					groupAccountLine.setRevisionRevenueAmount(new BigDecimal(0));
							}
//Calculation of revisionVatAmt
		
							if((groupAccountLine.getRevisionRevenueAmount()!=null)&&(!groupAccountLine.getRevisionRevenueAmount().equals(""))&&((Double.parseDouble(groupAccountLine.getRevisionRevenueAmount().toString())>0)||(Double.parseDouble(groupAccountLine.getRevisionRevenueAmount().toString())<0))&&(groupAccountLine.getRevisionVatPercent()!=null)&&(!groupAccountLine.getRevisionVatPercent().equals(""))&&((Double.parseDouble(groupAccountLine.getRevisionVatPercent().toString())>0)||(Double.parseDouble(groupAccountLine.getRevisionVatPercent().toString())<0)))
							{							 	
							 	setRate = ((Double.parseDouble(groupAccountLine.getRevisionRevenueAmount().toString())*Double.parseDouble(groupAccountLine.getRevisionVatPercent().toString()))/100);
							 	DecimalFormat df = new DecimalFormat("#.####");
							 	String setRate1 = df.format(setRate);
							 	groupAccountLine.setRevisionVatAmt(new BigDecimal(setRate1));
							}else{
								groupAccountLine.setRevisionVatAmt(new BigDecimal(0));
							}
							
							if((groupAccountLine.getEstimateExpense()!=null)&&(!groupAccountLine.getEstimateExpense().equals(""))&&((Double.parseDouble(groupAccountLine.getEstimateExpense().toString())>0)||(Double.parseDouble(groupAccountLine.getEstimateExpense().toString())<0))&&(groupAccountLine.getEstimateRevenueAmount()!=null)&&(!groupAccountLine.getEstimateRevenueAmount().equals(""))&&((Double.parseDouble(groupAccountLine.getEstimateRevenueAmount().toString())>0)||(Double.parseDouble(groupAccountLine.getEstimateRevenueAmount().toString())<0)))
							{                                                                                                       
							 	setRate=(Double.parseDouble(groupAccountLine.getEstimateRevenueAmount().toString())/Double.parseDouble(groupAccountLine.getEstimateExpense().toString()))*100;					
							 	int result=(int)Math.round(setRate);
							 	groupAccountLine.setEstimatePassPercentage(result);
							}else{
								groupAccountLine.setEstimatePassPercentage(0);
							}

							if((groupAccountLine.getRevisionExpense()!=null)&&(!groupAccountLine.getRevisionExpense().equals(""))&&((Double.parseDouble(groupAccountLine.getRevisionExpense().toString())>0)||(Double.parseDouble(groupAccountLine.getRevisionExpense().toString())<0))&&(groupAccountLine.getRevisionRevenueAmount()!=null)&&(!groupAccountLine.getRevisionRevenueAmount().equals(""))&&((Double.parseDouble(groupAccountLine.getRevisionRevenueAmount().toString())>0)||(Double.parseDouble(groupAccountLine.getRevisionRevenueAmount().toString())<0)))								
							{
							 	setRate=(Double.parseDouble(groupAccountLine.getRevisionRevenueAmount().toString())/Double.parseDouble(groupAccountLine.getRevisionExpense().toString()))*100;					
							 	int result=(int)Math.round(setRate);
							 	groupAccountLine.setRevisionPassPercentage(result);
							}else{
								groupAccountLine.setRevisionPassPercentage(0);
							}
					
					
							if((actualExpense!=null) && !(actualExpense.toString().equalsIgnoreCase("")) && !(actualExpense.toString().equalsIgnoreCase("0.00")) && !(actualExpense.toString().equalsIgnoreCase("0.0")) && !(actualExpense.toString().equalsIgnoreCase("0"))){
								if(weights!=null && !(weights.equalsIgnoreCase(""))){
									String[] splitWeight=weights.split("#");
									if(Double.parseDouble(splitWeight[1])>0){
										setRate=(Double.parseDouble(actualExpense.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
										groupAccountLine.setActualExpense(new BigDecimal(setRate));
									}
									else{
//										groupAccountLine.setActualExpense(new BigDecimal(0));
									}
								}
								else{
	//								groupAccountLine.setActualExpense(new BigDecimal(0));
								}
							}
							else{
		//						groupAccountLine.setActualExpense(new BigDecimal(0));
							}
//Calculation of payVatAmt	

							if((groupAccountLine.getActualExpense()!=null)&&(!groupAccountLine.getActualExpense().equals(""))&&((Double.parseDouble(groupAccountLine.getActualExpense().toString())>0)||(Double.parseDouble(groupAccountLine.getActualExpense().toString())<0))&&(groupAccountLine.getPayVatPercent()!=null)&&(!groupAccountLine.getPayVatPercent().equals(""))&&((Double.parseDouble(groupAccountLine.getPayVatPercent().toString())>0)||(Double.parseDouble(groupAccountLine.getPayVatPercent().toString())<0)))
							{							 	
							 	setRate = ((Double.parseDouble(groupAccountLine.getActualExpense().toString())*Double.parseDouble(groupAccountLine.getPayVatPercent().toString()))/100);
							 	DecimalFormat df = new DecimalFormat("#.####");
							 	String setRate1 = df.format(setRate);
							 	groupAccountLine.setPayVatAmt(new BigDecimal(setRate1));
							}else{
								groupAccountLine.setPayVatAmt(new BigDecimal(0));
							}

							if((estLocalRate!=null) && !(estLocalRate.toString().equalsIgnoreCase("")) && !(estLocalRate.toString().equalsIgnoreCase("0.00")) && !(estLocalRate.toString().equalsIgnoreCase("0.0")) && !(estLocalRate.toString().equalsIgnoreCase("0"))){
								if(weights!=null && !(weights.equalsIgnoreCase(""))){
									String[] splitWeight=weights.split("#");
									if(Double.parseDouble(splitWeight[1])>0){
										setRate=(Double.parseDouble(estLocalRate.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
										groupAccountLine.setEstLocalRate(new BigDecimal(setRate));
									}
									else{
			//							groupAccountLine.setEstLocalRate(new BigDecimal(0));
									}
								}
								else{
				//					groupAccountLine.setEstLocalRate(new BigDecimal(0));
								}
							}
							else{
					//			groupAccountLine.setEstLocalRate(new BigDecimal(0));
							}
								if((estLocalAmount!=null) && !(estLocalAmount.toString().equalsIgnoreCase("")) && !(estLocalAmount.toString().equalsIgnoreCase("0.00")) && !(estLocalAmount.toString().equalsIgnoreCase("0.0")) && !(estLocalAmount.toString().equalsIgnoreCase("0"))){
									if(weights!=null && !(weights.equalsIgnoreCase(""))){
										String[] splitWeight=weights.split("#");
										if(Double.parseDouble(splitWeight[1])>0){
											setRate=(Double.parseDouble(estLocalAmount.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
											groupAccountLine.setEstLocalAmount(new BigDecimal(setRate));
										}	
										else{
				//							groupAccountLine.setEstLocalAmount(new BigDecimal(0));
										}
									}
									else{
			//							groupAccountLine.setEstLocalAmount(new BigDecimal(0));
									}
								}
								else{
			//						groupAccountLine.setEstLocalAmount(new BigDecimal(0));
								}
								if((revisionLocalRate!=null) && !(revisionLocalRate.toString().equalsIgnoreCase("")) && !(revisionLocalRate.toString().equalsIgnoreCase("0.00")) && !(revisionLocalRate.toString().equalsIgnoreCase("0.0")) && !(revisionLocalRate.toString().equalsIgnoreCase("0"))){
									if(weights!=null && !(weights.equalsIgnoreCase(""))){
										String[] splitWeight=weights.split("#");
										if(Double.parseDouble(splitWeight[1])>0){
											setRate=(Double.parseDouble(revisionLocalRate.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
											groupAccountLine.setRevisionLocalRate(new BigDecimal(setRate));
										}
										else{
				//							groupAccountLine.setRevisionLocalRate(new BigDecimal(0));
										}	
									}
									else{
			//							groupAccountLine.setRevisionLocalRate(new BigDecimal(0));
									}
								}
								else{
			//						groupAccountLine.setRevisionLocalRate(new BigDecimal(0));
								}
								if((revisionLocalAmount!=null) && !(revisionLocalAmount.toString().equalsIgnoreCase("")) && !(revisionLocalAmount.toString().equalsIgnoreCase("0.00")) && !(revisionLocalAmount.toString().equalsIgnoreCase("0.0")) && !(revisionLocalAmount.toString().equalsIgnoreCase("0"))){
									if(weights!=null && !(weights.equalsIgnoreCase(""))){
										String[] splitWeight=weights.split("#");
										if(Double.parseDouble(splitWeight[1])>0){
											setRate=(Double.parseDouble(revisionLocalAmount.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
											groupAccountLine.setRevisionLocalAmount(new BigDecimal(setRate));
										}
										else{
		//									groupAccountLine.setRevisionLocalAmount(new BigDecimal(0));
										}
									}
									else{
		//								groupAccountLine.setRevisionLocalAmount(new BigDecimal(0));
									}
									
								}
								else{
	//								groupAccountLine.setRevisionLocalAmount(new BigDecimal(0));
								}
								
								AccountLine accLine=accountLineManager.save(groupAccountLine);
							}					
 			
		}else{
			//FOR NEW ACCOUNTLINE			
						groupAccountLine=new AccountLine();
						boolean activateAccPortal =true;
			    		try{
			    		if(accountLineAccountPortalFlag){	
			    		activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
			    		groupAccountLine.setActivateAccPortal(activateAccPortal);
			    		}
			    		}catch(Exception e){
			    			e.printStackTrace();
			    		}
						groupAccountLine.setCreatedBy(getRequest().getRemoteUser());
						groupAccountLine.setCreatedOn(new Date());
						groupAccountLine.setCorpID(groupServiceOrder.getCorpID());
						groupAccountLine.setBasis("each");
						groupAccountLine.setStatus(true);
						groupAccountLine.setGroupAccount(accountLine.getId());
						maxAccLineNumber=accountLineManager.findMaxLineNumber(groupServiceOrder.getShipNumber());
						if(maxAccLineNumber!=null && !(maxAccLineNumber.isEmpty())){
							maxLine=Long.parseLong(maxAccLineNumber.get(0).toString());
							maxLine++;
							if(maxLine.toString().length()==1){
								groupAccountLine.setAccountLineNumber("00"+maxLine);
							}
							else if(maxLine.toString().length()==2){
								groupAccountLine.setAccountLineNumber("0"+maxLine);
							}
							else{
								groupAccountLine.setAccountLineNumber(maxLine.toString());
							}
						}
						else{
							groupAccountLine.setAccountLineNumber("001");
						}
						groupAccountLine.setUpdatedBy(getRequest().getRemoteUser());
						groupAccountLine.setUpdatedOn(new Date());
						groupAccountLine.setServiceOrderId(groupServiceOrder.getId());
						groupAccountLine.setSequenceNumber(groupServiceOrder.getSequenceNumber());
						groupAccountLine.setShipNumber(groupServiceOrder.getShipNumber());
						groupAccountLine.setServiceOrder(groupServiceOrder);
						if((sellRateEstimate!=null) && !(buyRateEstimate.toString().equalsIgnoreCase("")) && !(buyRateEstimate.toString().equalsIgnoreCase("0.00")) && !(buyRateEstimate.toString().equalsIgnoreCase("0.0")) && !(buyRateEstimate.toString().equalsIgnoreCase("0"))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(buyRateEstimate.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									groupAccountLine.setEstimateRate(new BigDecimal(setRate));
								}
								else{
									groupAccountLine.setEstimateRate(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setEstimateRate(new BigDecimal(0));
							}
						}	
						else{
							groupAccountLine.setEstimateRate(new BigDecimal(0));
						}
						// 	Code Start for distribution of sell rate
						if((sellRateEstimate!=null) && !(sellRateEstimate.toString().equalsIgnoreCase("")) && !(sellRateEstimate.toString().equalsIgnoreCase("0.00")) && !(sellRateEstimate.toString().equalsIgnoreCase("0.0")) && !(sellRateEstimate.toString().equalsIgnoreCase("0"))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(sellRateEstimate.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									groupAccountLine.setEstimateSellRate(new BigDecimal(setRate));
								}
								else{
									groupAccountLine.setEstimateSellRate(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setEstimateSellRate(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setEstimateSellRate(new BigDecimal(0));
						}
						// 	code end for distribution of sell rate
						if((buyRateRevision!=null) && !(buyRateRevision.toString().equalsIgnoreCase("") && !(buyRateRevision.toString().equalsIgnoreCase("0.00")) && !(buyRateRevision.toString().equalsIgnoreCase("0.0")) && !(buyRateRevision.toString().equalsIgnoreCase("0")))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(buyRateRevision.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									groupAccountLine.setRevisionRate(new BigDecimal(setRate));
								}
								else{
									groupAccountLine.setRevisionRate(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setRevisionRate(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setRevisionRate(new BigDecimal(0));
						}
						if((sellRateRevision!=null) && !(sellRateRevision.toString().equalsIgnoreCase("") && !(sellRateRevision.toString().equalsIgnoreCase("0.00")) && !(sellRateRevision.toString().equalsIgnoreCase("0.0")) && !(sellRateRevision.toString().equalsIgnoreCase("0")))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(sellRateRevision.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									groupAccountLine.setRevisionSellRate(new BigDecimal(setRate));
								}
								else{
									groupAccountLine.setRevisionSellRate(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setRevisionSellRate(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setRevisionSellRate(new BigDecimal(0));
						}
						if((localAmount!=null) && !(localAmount.toString().equalsIgnoreCase("")) && !(localAmount.toString().equalsIgnoreCase("0.00")) && !(localAmount.toString().equalsIgnoreCase("0.0")) && !(localAmount.toString().equalsIgnoreCase("0"))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(localAmount.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									groupAccountLine.setLocalAmount(new BigDecimal(setRate));
								}
								else{
									groupAccountLine.setLocalAmount(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setLocalAmount(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setLocalAmount(new BigDecimal(0));
						}

							
						
						groupAccountLine.setInvoiceNumber(accountLine.getInvoiceNumber());
						groupAccountLine.setInvoiceDate(accountLine.getInvoiceDate());
						groupAccountLine.setCountry(accountLine.getCountry());
						groupAccountLine.setValueDate(accountLine.getValueDate());
						groupAccountLine.setReceivedDate(accountLine.getReceivedDate());
						groupAccountLine.setPayingStatus(accountLine.getPayingStatus());
						if(accountLine.getPayVatDescr()!=null){
							groupAccountLine.setPayVatDescr(accountLine.getPayVatDescr());							
						}else{
							groupAccountLine.setPayVatDescr("");
						}							
						if(accountLine.getPayVatPercent()!=null){
							groupAccountLine.setPayVatPercent(accountLine.getPayVatPercent());							
						}else{
							groupAccountLine.setPayVatPercent("0");
						}						
						if(accountLine.getEstVatDescr()!=null){
							groupAccountLine.setEstVatDescr(accountLine.getEstVatDescr());							
						}else{
							groupAccountLine.setEstVatDescr("");
						}							
						if(accountLine.getEstVatPercent()!=null){
							groupAccountLine.setEstVatPercent(accountLine.getEstVatPercent());							
						}else{
							groupAccountLine.setEstVatPercent("0");
						}							
						if(accountLine.getRevisionVatDescr()!=null){
							groupAccountLine.setRevisionVatDescr(accountLine.getRevisionVatDescr());							
						}else{
							groupAccountLine.setRevisionVatDescr("");
						}							
						if(accountLine.getRevisionVatPercent()!=null){
							groupAccountLine.setRevisionVatPercent(accountLine.getRevisionVatPercent());							
						}else{
							groupAccountLine.setRevisionVatPercent("0");
						}							
						groupAccountLine.setPayPayableDate(accountLine.getPayPayableDate());
						groupAccountLine.setPayGl(accountLine.getPayGl());
						groupAccountLine.setPayPostDate(accountLine.getPayPostDate());
						groupAccountLine.setAccruePayable(accountLine.getAccruePayable());
						groupAccountLine.setPayAccDate(accountLine.getPayAccDate());
						groupAccountLine.setCategory(accountLine.getCategory());
						groupAccountLine.setCompanyDivision(accountLine.getCompanyDivision());
						groupAccountLine.setVendorCode(accountLine.getVendorCode());
						groupAccountLine.setEstimateVendorName(accountLine.getEstimateVendorName());
						groupAccountLine.setActgCode(accountLine.getActgCode());
						groupAccountLine.setChargeCode(accountLine.getChargeCode());
						groupAccountLine.setIgnoreForBilling(accountLine.isIgnoreForBilling());
						groupAccountLine.setVATExclude(accountLine.getVATExclude());
						groupAccountLine.setEstimateDate(accountLine.getEstimateDate());
						groupAccountLine.setEstCurrency(accountLine.getEstCurrency());
						groupAccountLine.setEstValueDate(accountLine.getEstValueDate());
						groupAccountLine.setBasis(accountLine.getBasis());
						groupAccountLine.setRevisionCurrency(accountLine.getRevisionCurrency());
						groupAccountLine.setRevisionValueDate(accountLine.getRevisionValueDate());
						groupAccountLine.setEstimateQuantity(accountLine.getEstimateQuantity());
						groupAccountLine.setRevisionQuantity(accountLine.getRevisionQuantity());
						groupAccountLine.setReference(accountLine.getReference());
						groupAccountLine.setEstLocalRate(accountLine.getEstLocalRate());
						groupAccountLine.setEstLocalAmount(accountLine.getEstLocalAmount());
						groupAccountLine.setRevisionLocalAmount(accountLine.getRevisionLocalAmount());
						groupAccountLine.setRevisionLocalRate(accountLine.getRevisionLocalRate());
						groupAccountLine.setNote(accountLine.getNote());
						groupAccountLine.setExchangeRate(accountLine.getExchangeRate());
						groupAccountLine.setEstExchangeRate(accountLine.getEstExchangeRate());
						groupAccountLine.setRevisionExchangeRate(accountLine.getRevisionExchangeRate());
						//		groupAccountLine.setActualExpense(accountLine.getActualExpense());
						if((sellRateEstimate!=null) && !(buyRateEstimate.toString().equalsIgnoreCase("")) && !(buyRateEstimate.toString().equalsIgnoreCase("0.00")) && !(buyRateEstimate.toString().equalsIgnoreCase("0.0")) && !(buyRateEstimate.toString().equalsIgnoreCase("0"))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(buyRateEstimate.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									BigDecimal qty=accountLine.getEstimateQuantity();
									if(qty!=null && qty!=new BigDecimal(0)){
										Double result=Double.parseDouble(qty.toString())*setRate;
										groupAccountLine.setEstimateExpense(new BigDecimal(result));
									}
									else{
										groupAccountLine.setEstimateExpense(new BigDecimal(setRate));
									}
								}
								else{
									groupAccountLine.setEstimateExpense(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setEstimateExpense(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setEstimateExpense(new BigDecimal(0));
						}
						if((sellRateEstimate!=null) && !(sellRateEstimate.toString().equalsIgnoreCase("")) && !(sellRateEstimate.toString().equalsIgnoreCase("0.00")) && !(sellRateEstimate.toString().equalsIgnoreCase("0.0")) && !(sellRateEstimate.toString().equalsIgnoreCase("0"))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(sellRateEstimate.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									BigDecimal qty=accountLine.getEstimateQuantity();
									if(qty!=null && qty!=new BigDecimal(0)){
										Double result=Double.parseDouble(qty.toString())*setRate;
										groupAccountLine.setEstimateRevenueAmount(new BigDecimal(result));
									}
									else{
										groupAccountLine.setEstimateRevenueAmount(new BigDecimal(setRate));
									}
								}
								else{
									groupAccountLine.setEstimateRevenueAmount(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setEstimateRevenueAmount(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setEstimateRevenueAmount(new BigDecimal(0));
						}
						if((groupAccountLine.getEstimateRevenueAmount()!=null)&&(!groupAccountLine.getEstimateRevenueAmount().equals(""))&&((Double.parseDouble(groupAccountLine.getEstimateRevenueAmount().toString())>0)||(Double.parseDouble(groupAccountLine.getEstimateRevenueAmount().toString())<0))&&(groupAccountLine.getEstVatPercent()!=null)&&(!groupAccountLine.getEstVatPercent().equals(""))&&((Double.parseDouble(groupAccountLine.getEstVatPercent().toString())>0)||(Double.parseDouble(groupAccountLine.getEstVatPercent().toString())<0)))
						{							 	
						 	setRate = ((Double.parseDouble(groupAccountLine.getEstimateRevenueAmount().toString())*Double.parseDouble(groupAccountLine.getEstVatPercent().toString()))/100);
						 	DecimalFormat df = new DecimalFormat("#.####");
						 	String setRate1 = df.format(setRate);
						 	groupAccountLine.setEstVatAmt(new BigDecimal(setRate1));
						}else{
							groupAccountLine.setEstVatAmt(new BigDecimal(0));
						}						
						if((buyRateRevision!=null) && !(buyRateRevision.toString().equalsIgnoreCase("") && !(buyRateRevision.toString().equalsIgnoreCase("0.00")) && !(buyRateRevision.toString().equalsIgnoreCase("0.0")) && !(buyRateRevision.toString().equalsIgnoreCase("0")))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(buyRateRevision.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									BigDecimal qty=accountLine.getRevisionQuantity();
									if(qty!=null && qty!=new BigDecimal(0)){
										Double result=Double.parseDouble(qty.toString())*setRate;
										groupAccountLine.setRevisionExpense(new BigDecimal(result));
									}
									else{
										groupAccountLine.setRevisionExpense(new BigDecimal(setRate));
									}
								}
								else{
									groupAccountLine.setRevisionExpense(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setRevisionExpense(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setRevisionExpense(new BigDecimal(0));
						}
						if((sellRateRevision!=null) && !(sellRateRevision.toString().equalsIgnoreCase("") && !(sellRateRevision.toString().equalsIgnoreCase("0.00")) && !(sellRateRevision.toString().equalsIgnoreCase("0.0")) && !(sellRateRevision.toString().equalsIgnoreCase("0")))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(sellRateRevision.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									BigDecimal qty=accountLine.getRevisionQuantity();
									if(qty!=null && qty!=new BigDecimal(0)){
										Double result=Double.parseDouble(qty.toString())*setRate;
										groupAccountLine.setRevisionRevenueAmount(new BigDecimal(result));
									}
									else{
										groupAccountLine.setRevisionRevenueAmount(new BigDecimal(setRate));
									}
								}
								else{
									groupAccountLine.setRevisionRevenueAmount(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setRevisionRevenueAmount(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setRevisionRevenueAmount(new BigDecimal(0));
						}
						if((groupAccountLine.getRevisionRevenueAmount()!=null)&&(!groupAccountLine.getRevisionRevenueAmount().equals(""))&&((Double.parseDouble(groupAccountLine.getRevisionRevenueAmount().toString())>0)||(Double.parseDouble(groupAccountLine.getRevisionRevenueAmount().toString())<0))&&(groupAccountLine.getRevisionVatPercent()!=null)&&(!groupAccountLine.getRevisionVatPercent().equals(""))&&((Double.parseDouble(groupAccountLine.getRevisionVatPercent().toString())>0)||(Double.parseDouble(groupAccountLine.getRevisionVatPercent().toString())<0)))
						{							 	
						 	setRate = ((Double.parseDouble(groupAccountLine.getRevisionRevenueAmount().toString())*Double.parseDouble(groupAccountLine.getRevisionVatPercent().toString()))/100);
						 	DecimalFormat df = new DecimalFormat("#.####");
						 	String setRate1 = df.format(setRate);
						 	groupAccountLine.setRevisionVatAmt(new BigDecimal(setRate1));
						}else{
							groupAccountLine.setRevisionVatAmt(new BigDecimal(0));
						}						
						if((groupAccountLine.getEstimateExpense()!=null)&&(!groupAccountLine.getEstimateExpense().equals(""))&&((Double.parseDouble(groupAccountLine.getEstimateExpense().toString())>0)||(Double.parseDouble(groupAccountLine.getEstimateExpense().toString())<0))&&(groupAccountLine.getEstimateRevenueAmount()!=null)&&(!groupAccountLine.getEstimateRevenueAmount().equals(""))&&((Double.parseDouble(groupAccountLine.getEstimateRevenueAmount().toString())>0)||(Double.parseDouble(groupAccountLine.getEstimateRevenueAmount().toString())<0)))
						{                                                                                                       
						 	setRate=(Double.parseDouble(groupAccountLine.getEstimateRevenueAmount().toString())/Double.parseDouble(groupAccountLine.getEstimateExpense().toString()))*100;					
						 	int result=(int)Math.round(setRate);
						 	groupAccountLine.setEstimatePassPercentage(result);
						}else{
							groupAccountLine.setEstimatePassPercentage(0);
						}	
						if((groupAccountLine.getRevisionExpense()!=null)&&(!groupAccountLine.getRevisionExpense().equals(""))&&((Double.parseDouble(groupAccountLine.getRevisionExpense().toString())>0)||(Double.parseDouble(groupAccountLine.getRevisionExpense().toString())<0))&&(groupAccountLine.getRevisionRevenueAmount()!=null)&&(!groupAccountLine.getRevisionRevenueAmount().equals(""))&&((Double.parseDouble(groupAccountLine.getRevisionRevenueAmount().toString())>0)||(Double.parseDouble(groupAccountLine.getRevisionRevenueAmount().toString())<0)))								
						{
						 	setRate=(Double.parseDouble(groupAccountLine.getRevisionRevenueAmount().toString())/Double.parseDouble(groupAccountLine.getRevisionExpense().toString()))*100;					
						 	int result=(int)Math.round(setRate);
						 	groupAccountLine.setRevisionPassPercentage(result);
						}else{
							groupAccountLine.setRevisionPassPercentage(0);
						}						
						if((actualExpense!=null) && !(actualExpense.toString().equalsIgnoreCase("")) && !(actualExpense.toString().equalsIgnoreCase("0.00")) && !(actualExpense.toString().equalsIgnoreCase("0.0")) && !(actualExpense.toString().equalsIgnoreCase("0"))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(actualExpense.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									groupAccountLine.setActualExpense(new BigDecimal(setRate));
								}
								else{
									groupAccountLine.setActualExpense(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setActualExpense(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setActualExpense(new BigDecimal(0));
						}
						if((groupAccountLine.getActualExpense()!=null)&&(!groupAccountLine.getActualExpense().equals(""))&&((Double.parseDouble(groupAccountLine.getActualExpense().toString())>0)||(Double.parseDouble(groupAccountLine.getActualExpense().toString())<0))&&(groupAccountLine.getPayVatPercent()!=null)&&(!groupAccountLine.getPayVatPercent().equals(""))&&((Double.parseDouble(groupAccountLine.getPayVatPercent().toString())>0)||(Double.parseDouble(groupAccountLine.getPayVatPercent().toString())<0)))
						{							 	
						 	setRate = ((Double.parseDouble(groupAccountLine.getActualExpense().toString())*Double.parseDouble(groupAccountLine.getPayVatPercent().toString()))/100);
						 	DecimalFormat df = new DecimalFormat("#.####");
						 	String setRate1 = df.format(setRate);
						 	groupAccountLine.setPayVatAmt(new BigDecimal(setRate1));
						}else{
							groupAccountLine.setPayVatAmt(new BigDecimal(0));
						}						
						
						if((estLocalRate!=null) && !(estLocalRate.toString().equalsIgnoreCase("")) && !(estLocalRate.toString().equalsIgnoreCase("0.00")) && !(estLocalRate.toString().equalsIgnoreCase("0.0")) && !(estLocalRate.toString().equalsIgnoreCase("0"))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(estLocalRate.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									groupAccountLine.setEstLocalRate(new BigDecimal(setRate));
								}
								else{
									groupAccountLine.setEstLocalRate(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setEstLocalRate(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setEstLocalRate(new BigDecimal(0));
						}
						if((estLocalAmount!=null) && !(estLocalAmount.toString().equalsIgnoreCase("")) && !(estLocalAmount.toString().equalsIgnoreCase("0.00")) && !(estLocalAmount.toString().equalsIgnoreCase("0.0")) && !(estLocalAmount.toString().equalsIgnoreCase("0"))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(estLocalAmount.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									groupAccountLine.setEstLocalAmount(new BigDecimal(setRate));
								}	
								else{
									groupAccountLine.setEstLocalAmount(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setEstLocalAmount(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setEstLocalAmount(new BigDecimal(0));
						}
						if((revisionLocalRate!=null) && !(revisionLocalRate.toString().equalsIgnoreCase("")) && !(revisionLocalRate.toString().equalsIgnoreCase("0.00")) && !(revisionLocalRate.toString().equalsIgnoreCase("0.0")) && !(revisionLocalRate.toString().equalsIgnoreCase("0"))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(revisionLocalRate.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									groupAccountLine.setRevisionLocalRate(new BigDecimal(setRate));
								}
								else{
									groupAccountLine.setRevisionLocalRate(new BigDecimal(0));
								}	
							}
							else{
								groupAccountLine.setRevisionLocalRate(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setRevisionLocalRate(new BigDecimal(0));
						}
						if((revisionLocalAmount!=null) && !(revisionLocalAmount.toString().equalsIgnoreCase("")) && !(revisionLocalAmount.toString().equalsIgnoreCase("0.00")) && !(revisionLocalAmount.toString().equalsIgnoreCase("0.0")) && !(revisionLocalAmount.toString().equalsIgnoreCase("0"))){
							if(weights!=null && !(weights.equalsIgnoreCase(""))){
								String[] splitWeight=weights.split("#");
								if(Double.parseDouble(splitWeight[1])>0){
									setRate=(Double.parseDouble(revisionLocalAmount.toString())*Double.parseDouble(splitWeight[0]))/(Double.parseDouble(splitWeight[1]));
									groupAccountLine.setRevisionLocalAmount(new BigDecimal(setRate));
								}
								else{
									groupAccountLine.setRevisionLocalAmount(new BigDecimal(0));
								}
							}
							else{
								groupAccountLine.setRevisionLocalAmount(new BigDecimal(0));
							}
						}
						else{
							groupAccountLine.setRevisionLocalAmount(new BigDecimal(0));
						}
						AccountLine accLine=accountLineManager.save(groupAccountLine);					
					}
				}catch (Exception e) {
					e.printStackTrace();
			}
			}
		}
	}}
		catch(Exception e){
			e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

		return SUCCESS;
	}
	private List invoicesForUnpostingList=new ArrayList();
	@SkipValidation 
	public String findVendorInvoicesForUnpostingList(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
			invoicesForUnpostingList = accountLineManager.getVendorInvoicesForUnposting(shipNumber,sessionCorpID);
		} catch (Exception e) {
		  	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	  
	    	 return "errorlog";
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	private String stringAid;
	@SkipValidation
	public void updateUnPostPayable() throws Exception{
		accountLineManager.updatePayaccAndPostdate(shipNumber,stringAid,getRequest().getRemoteUser());
	}
    
    public Map<String, String> getCategory() {
		return category;
	}
	
	public Map<String, String> getBasis() {
		return basis;
	}
	public  Map<String, String> getPayingStatus() {
		return payingStatus;
	}
	public Map<String, String> getCountry() {
		return country;
	}
	
	public void setSid(Long sid) {
		this.sid = sid;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public Miscellaneous getMiscellaneous() {
		return( miscellaneous !=null )?miscellaneous :miscellaneousManager.get(sid);
	}
	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}
	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}
	public List getAccountLineList() {
		return accountLineList;
	}
	public void setAccountLineList(List<AccountLine> accountLineList) {
		this.accountLineList = accountLineList;
	}
	public void setEstimateAccountList(List<AccountLine> accountLineEstimateList) {
		this.accountLineEstimateList = accountLineEstimateList;
	}
	public List getEstimateAccountList() {
		return accountLineEstimateList;
	}
	public Long getNewInvoiceNumber() {
		return newInvoiceNumber;
	}
	public void setNewInvoiceNumber(Long newInvoiceNumber) {
		this.newInvoiceNumber =newInvoiceNumber ;
	}
	public BigDecimal getTotalEstimateExpenseCbm() {
		return totalEstimateExpenseCbm;
	}
	public void setTotalEstimateExpenseCbm(BigDecimal totalEstimateExpenseCbm) {
		this.totalEstimateExpenseCbm = totalEstimateExpenseCbm;
	}
	public BigDecimal getTotalEstimateExpenseCft() {
		return totalEstimateExpenseCft;
	}
	public void setTotalEstimateExpenseCft(BigDecimal totalEstimateExpenseCft) {
		this.totalEstimateExpenseCft = totalEstimateExpenseCft;
	}
	public BigDecimal getTotalEstimateExpenseCwt() {
		return totalEstimateExpenseCwt;
	}
	public void setTotalEstimateExpenseCwt(BigDecimal totalEstimateExpenseCwt) {
		this.totalEstimateExpenseCwt = totalEstimateExpenseCwt;
	}
	public BigDecimal getTotalEstimateExpenseEach() {
		return totalEstimateExpenseEach;
	}
	public void setTotalEstimateExpenseEach(BigDecimal totalEstimateExpenseEach) {
		this.totalEstimateExpenseEach = totalEstimateExpenseEach;
	}
	public BigDecimal getTotalEstimateExpenseKg() {
		return totalEstimateExpenseKg;
	}
	public void setTotalEstimateExpenseKg(BigDecimal totalEstimateExpenseKg) {  
		this.totalEstimateExpenseKg = totalEstimateExpenseKg; 
	}
	public BigDecimal getTotalRevisionExpenseCbm() {
		return totalRevisionExpenseCbm;
	}
	public void setTotalRevisionExpenseCbm(BigDecimal totalRevisionExpenseCbm) {
		this.totalRevisionExpenseCbm = totalRevisionExpenseCbm;
	}
	public BigDecimal getTotalRevisionExpenseCft() {
		return totalRevisionExpenseCft;
	}
	public void setTotalRevisionExpenseCft(BigDecimal totalRevisionExpenseCft) {
		this.totalRevisionExpenseCft = totalRevisionExpenseCft;
	}
	public BigDecimal getTotalRevisionExpenseCwt() {
		return totalRevisionExpenseCwt;
	}
	public void setTotalRevisionExpenseCwt(BigDecimal totalRevisionExpenseCwt) {
		this.totalRevisionExpenseCwt = totalRevisionExpenseCwt;
	}
	public BigDecimal getTotalRevisionExpenseEach() {
		return totalRevisionExpenseEach;
	}
	public void setTotalRevisionExpenseEach(BigDecimal totalRevisionExpenseEach) {
		this.totalRevisionExpenseEach = totalRevisionExpenseEach;
	}
	public BigDecimal getTotalRevisionExpenseKg() {
		return totalRevisionExpenseKg;
	}
	public void setTotalRevisionExpenseKg(BigDecimal totalRevisionExpenseKg) {
		this.totalRevisionExpenseKg = totalRevisionExpenseKg;
	}
	public BigDecimal getTotalEstimateExpenseFlat() {
		return totalEstimateExpenseFlat;
	}
	public void setTotalEstimateExpenseFlat(BigDecimal totalEstimateExpenseFlat) {
		this.totalEstimateExpenseFlat = totalEstimateExpenseFlat;
	}
	public BigDecimal getTotalEstimateExpenseHour() {
		return totalEstimateExpenseHour;
	}
	public void setTotalEstimateExpenseHour(BigDecimal totalEstimateExpenseHour) {
		this.totalEstimateExpenseHour = totalEstimateExpenseHour;
	}
	public BigDecimal getTotalRevisionExpenseFlat() {
		return totalRevisionExpenseFlat;
	}
	public void setTotalRevisionExpenseFlat(BigDecimal totalRevisionExpenseFlat) {
		this.totalRevisionExpenseFlat = totalRevisionExpenseFlat;
	}
	public BigDecimal getTotalRevisionExpenseHour() {
		return totalRevisionExpenseHour;
	}
	public void setTotalRevisionExpenseHour(BigDecimal totalRevisionExpenseHour) {
		this.totalRevisionExpenseHour = totalRevisionExpenseHour;
	}
	public   BigDecimal getActualRevenue() {
		return actualRevenue;
	}
	public  void setActualRevenue(BigDecimal actualRevenue) {
		this.actualRevenue = actualRevenue;
	}
	public int getBillingFlag() {
		return billingFlag;
	}
	public void setBillingFlag(int billingFlag) {
		this.billingFlag = billingFlag;
	}
	public String getCountCategoryTypeDetailNotes() {
		return countCategoryTypeDetailNotes;
	}
	public String getCountEntitleDetailOrderNotes() {
		return countEntitleDetailOrderNotes;
	}
	public String getCountEstimateDetailNotes() {
		return countEstimateDetailNotes;
	}
	public String getCountPayableDetailNotes() {
		return countPayableDetailNotes;
	}
	public String getCountReceivableDetailNotes() {
		return countReceivableDetailNotes;
	}
	public String getCountRevesionDetailNotes() {
		return countRevesionDetailNotes;
	}
	public ServicePartner getServicePartner() {
		return servicePartner;
	}
	public void setServicePartner(ServicePartner servicePartner) {
		this.servicePartner = servicePartner;
	}
	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}
	public void setActualExpenseTotal(BigDecimal actualExpenseTotal) {
		this.actualExpenseTotal = actualExpenseTotal;
	}
	public void setActualGrossMar(BigDecimal actualGrossMar) {
		this.actualGrossMar = actualGrossMar;
	}
	public void setActualGrossMarginPer(BigDecimal actualGrossMarginPer) {
		this.actualGrossMarginPer = actualGrossMarginPer;
	}
	public void setActualRevenueTotal(BigDecimal actualRevenueTotal) {
		this.actualRevenueTotal = actualRevenueTotal;
	}
	public void setEntitledTotal(BigDecimal entitledTotal) {
		this.entitledTotal = entitledTotal;
	}
	public void setEstimateExp(BigDecimal estimateExp) {
		this.estimateExp = estimateExp;
	}
	public void setEstimateRevenueAmt(BigDecimal estimateRevenueAmt) {
		this.estimateRevenueAmt = estimateRevenueAmt;
	}
	public void setGmPercent(BigDecimal gmPercent) {
		this.gmPercent = gmPercent;
	}
	public void setGrossMrgn(BigDecimal grossMrgn) {
		this.grossMrgn = grossMrgn;
	}
	public void setRevisedGrossMar(BigDecimal revisedGrossMar) {
		this.revisedGrossMar = revisedGrossMar;
	}
	public void setRevisedGrossMarginPer(BigDecimal revisedGrossMarginPer) {
		this.revisedGrossMarginPer = revisedGrossMarginPer;
	}
	public void setRevisedTotal(BigDecimal revisedTotal) {
		this.revisedTotal = revisedTotal;
	}
	public void setRevisedTotalRev(BigDecimal revisedTotalRev) {
		this.revisedTotalRev = revisedTotalRev;
	}
	public List getAccountLineListBillToCode() {
		return accountLineListBillToCode;
	}
	public void setAccountLineListBillToCode(List accountLineListBillToCode) {
		this.accountLineListBillToCode = accountLineListBillToCode;
	}
	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}
	public List getPartners() {
		return partners;
	}
	public void setPartners(List partners) {
		this.partners = partners;
	}
	public List getRevisedChargeList() {
		return revisedChargeList;
	}
	public void setRevisedChargeList(List revisedChargeList) {
		this.revisedChargeList = revisedChargeList;
	}
	public void setChargesManager(ChargesManager chargesManager) {
		this.chargesManager = chargesManager;
	}
	public Charges getCharges() {
		return charges;
	}
	public void setCharges(Charges charges) {
		this.charges = charges;
	}
	
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	public SystemDefault getSystemDefault() {
		return systemDefault;
	}
	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}
	public List getSystemDefaultList() {
		return systemDefaultList;
	}
	public void setSystemDefaultList(List systemDefaultList) {
		this.systemDefaultList = systemDefaultList;
	}
	public List getQuantity() {
		return quantity;
	}
	public void setQuantity(List quantity) {
		this.quantity = quantity;
	}
	public String getShipNum() {
		return shipNum;
	}
	public void setShipNum(String shipNum) {
		this.shipNum = shipNum;
	}
	public String getRevisedChargeValue() {
		return revisedChargeValue;
	}
	public void setRevisedChargeValue(String revisedChargeValue) {
		this.revisedChargeValue = revisedChargeValue;
	}
	public List getRevisedChargeListRadio() {
		return revisedChargeListRadio;
	}
	public void setRevisedChargeListRadio(List revisedChargeListRadio) {
		this.revisedChargeListRadio = revisedChargeListRadio;
	}
	public List getRevisedEntitleChargeList() {
		return revisedEntitleChargeList;
	}
	public void setRevisedEntitleChargeList(List revisedEntitleChargeList) {
		this.revisedEntitleChargeList = revisedEntitleChargeList;
	}
	public List getRevisedEstimateChargeList() {
		return revisedEstimateChargeList;
	}
	public void setRevisedEstimateChargeList(List revisedEstimateChargeList) {
		this.revisedEstimateChargeList = revisedEstimateChargeList;
	}
	public void setExpressionManager(ExpressionManager expressionManager) {
		this.expressionManager = expressionManager;
	}
	private String totalRevenue;
	private String expressionFormula;
	private String fromField;
	@SkipValidation
	public String testFormula()	
	{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
				String qty="1000";
				String expression=expressionFormula;
				String replacement="miscellaneous.rwghCubicFeet";
				
				String newEpression=expression.replaceAll("FromField", replacement);
				Map values = new HashMap();
				values.put(replacement, new BigDecimal(qty));
				values.put("billing.insuranceBuyRate", 10);
				values.put("billing.onHand", 10); 
				values.put("billing.cycle", 3); 
				values.put("billing.postGrate", 3);
				values.put("billing.insuranceRate",10);
				values.put("billing.insuranceValueActual", 10);
				values.put("billing.baseInsuranceValue",10); 
				values.put("charges.minimum", 25);
				values.put("dateDifference", 31);
				values.put("standardDeductions.maxDeductAmt", 1000);
				values.put("standardDeductions.rate", 10);
				values.put("billing.vendorStoragePerMonth", 10);
				values.put("billing.storagePerMonth", 10);
				values.put("billing.totalInsrVal", 10);
				values.put("billing.insurancePerMonth", 10);
				values.put("billing.insuranceValueEntitle", 10);
				values.put("billing.payableRate", 10);
				values.put("billing.storageMeasurement", 0);
				values.put("serviceOrder.estimatedTotalRevenue", 0);
				values.put("accountLine.actualRevenue", 0);
				totalRevenue=expressionManager.executeTestExpression(newEpression, values);
		
				if(totalRevenue==null){
					totalRevenue="Formula is Correct";
				}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    
		}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	/* 
	 * method for Auto Save*/
	String selectedAccountId;
	public String saveOnTabChange() throws Exception 
	{
		validateFormNav="Ok"; 
	    String s = save(); 
	    return gotoPageString;
    }
	
	private String sequenceNumber;
	private String shipNumberForTickets;
	private List workTickets;
	
	@SkipValidation
	public String nonLinkedAccountLineUpload(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	
		return SUCCESS;
	}
	public String fileExt;
	
	@SkipValidation
	public void findNonLinkedAccountLine() throws Exception,EOFException{ 
		//FileInputStream fis = null;
        int record=0;
        String totRecords=null;
        HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file1=new File("nonLinkedAccountline"); 
		response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls");
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		outputStream.write("ServiceOrder Id \t".getBytes());
		outputStream.write("accountLine Id \t".getBytes());
        outputStream.write("shipnumber \t".getBytes());
		outputStream.write("AccountLine Number \t".getBytes());
		outputStream.write("Estimate Revenue \t".getBytes());
		outputStream.write("Revision Revenue \t".getBytes());
		outputStream.write("Actual Revenue \t".getBytes());
		outputStream.write("Invoice# \t".getBytes());
		outputStream.write("AccountLine Bill to code \t".getBytes());
		outputStream.write("Billing Bill to code \t".getBytes());
		outputStream.write("Billing Contract \t".getBytes());
		outputStream.write("link Contract(TrackingStatus) \t".getBytes());
		outputStream.write("Reason".getBytes());
		outputStream.write("\r\n".getBytes());
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder beginDates = new StringBuilder(formats.format( beginDate ));
        StringBuilder endDates = new StringBuilder(formats.format( endDate ));
		List nonLinkedAccountline=accountLineManager.findNonLinkedAccountline(beginDates.toString(), endDates.toString());
	    Iterator it=nonLinkedAccountline.iterator();
		while(it.hasNext()){
			Object invoice=it.next();		
			 if(((DTO)invoice).getId()!=null && (!((((DTO)invoice).getId()).toString().equals(""))) && ((DTO)invoice).getSid()!=null && (!((((DTO)invoice).getSid()).toString().equals("")))  )
			 {
				 id=Long.parseLong((((DTO)invoice).getId().toString()));
				 sid=Long.parseLong((((DTO)invoice).getSid().toString()));
				 String reason="";
				 try{
		            	
		            		billing =billingManager.getForOtherCorpid(sid);
		        			trackingStatus=trackingStatusManager.getForOtherCorpid(sid);
		        			serviceOrder=serviceOrderManager.getForOtherCorpid(sid); 
		        			customerFile = customerFileManager.getForOtherCorpid(serviceOrder.getCustomerFileId()); 
		        			accountLine=accountLineManager.getForOtherCorpid(id);
		        			
		        			String networkSynchedIdTrue=	accountLineManager.getNetworkSynchedId(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
					      	if(!(networkSynchedIdTrue.equals(""))){
					      		
					      	}else{
		        			
		        			
		        			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
		        				  billingCMMContractType = trackingStatusManager.getCMMContractTypeForOtherCorpid(accountLine.getCorpID() ,billing.getContract());
		        			}
		        			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
		        			      	billingDMMContractType = trackingStatusManager.getDMMContractTypeForOtherCorpid(accountLine.getCorpID() ,billing.getContract());
		        			}
		        			if(billingCMMContractType){ 
		        				    if(trackingStatus.getAccNetworkGroup())	{
		        				      if(serviceOrder.getIsNetworkRecord()){
		        				    	  String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),accountLine.getCorpID(), trackingStatus.getNetworkPartnerCode());
		              					  if(!(networkSynchedId.equals(""))){
		              						synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
		              						serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
		              						reason="accountline("+id+") already linked with "+serviceOrderToRecods.getShipNumber()+"and "+synchedAccountLine.getAccountLineNumber()+"Line # "+networkSynchedId+"Status of accountline "+synchedAccountLine.isStatus(); 
		              					   }else{ 
		              						 String partnertype="";
		             				    	  if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
		             				    		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
		             				    	  }else{
		             				    		  reason="accountline bill to code blank "; 
		             				    	  }
		             				    	  if(partnertype.equals("")){
		             				    		  reason="accountline bill to code not CMM/DMM type  ";  
		             				    	  }
		        				    	  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (!(partnertype.equals("")))){
		        				    	   //List linkedShipNumber=findLinkerShipNumber(serviceOrder);
		        				    		List  linkedShipNumber=new ArrayList();
		        				    		if(trackingStatus.getNetworkAgentExSO()!=null && (!(trackingStatus.getNetworkAgentExSO().toString().trim().equals(""))))
		        				    		{
		        				    		linkedShipNumber.add(trackingStatus.getNetworkAgentExSO());
		        				    		}
		        				    		if(serviceOrder.getBookingAgentShipNumber()!=null && (!(serviceOrder.getBookingAgentShipNumber().toString().trim().equals(""))))
		        				    		{
		        				    		linkedShipNumber.add(serviceOrder.getBookingAgentShipNumber());
		        				    		}
		        							List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
		        							Iterator  itss=serviceOrderRecords.iterator();
		        					    	while(itss.hasNext()){
		        					    		  serviceOrderToRecods=(ServiceOrder)itss.next();
		        					    		try{
		        					    		trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
		        					    		 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
		        					        	if(trackingStatusToRecods.getShipNumber()!=null && trackingStatusToRecods.getShipNumber().startsWith("UTSI")){
		        					    		 if(trackingStatusToRecods.getSoNetworkGroup()){
		        					        		
		        					        	}else{
		        					        	reason="tracking soNetworkGroup flag false in UTSI "; 	
		        					        	}
		        					    		 if(accountLine.getReceivedInvoiceDate() !=null && trackingStatusToRecods.getCreatedOn()!=null && trackingStatusToRecods.getCreatedOn().compareTo(accountLine.getReceivedInvoiceDate())>0 ){
		        					    			 reason="S/O link done ("+trackingStatusToRecods.getCreatedOn()+") after accountline Invoiced Date of Invoice:"+accountLine.getReceivedInvoiceDate(); 
		        					    		 }else{
		        					    			if(accountLine.getRecInvoiceNumber()!=null && (!(accountLine.getRecInvoiceNumber().equals(""))) ){
		        					    				reason="accountline should be link but invoiced ";
		        					    			}else{
		        					    				reason="accountline should be link";
		        					    			}
		        					    		 }
		        					        	}
		        					    		}catch (Exception e) {
		        			        	            e.printStackTrace();
		        				        	     }
		        					    	}
		        				      }else{
		        				    	  if(reason==null || reason.trim().equals("")){
		        				    	  reason="accountline RevenueAmount is zero  "; 
		        				    	  }
		        				      }
		              					   }}else{
		        				    	 reason="serviceOrder IsNetworkRecord flag false "; 
		        				     }
		        				    }else{
		        				    	reason="tracking AccNetworkGroup flag false ";	
		        				    }
		        				} 
		        			else if(billingDMMContractType){ 
		    				    if(trackingStatus.getAccNetworkGroup())	{
		      				      if(serviceOrder.getIsNetworkRecord()){
		      				    	  String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),accountLine.getCorpID(), trackingStatus.getNetworkPartnerCode());
		            					  if(!(networkSynchedId.equals(""))){
		            						synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
		            						serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
		            						reason="accountline("+id+") already linked with "+serviceOrderToRecods.getShipNumber()+"and "+synchedAccountLine.getAccountLineNumber()+"Line # "+networkSynchedId+"Status of accountline "+synchedAccountLine.isStatus();
		            					   }else{
		            						   String partnertype="";
		        				            	boolean billtotype=false;
		        				             	  if(customerFile.getAccountCode()!=null && (!(customerFile.getAccountCode().toString().equals(""))) ){
		        				             		  partnertype=  partnerManager.checkPartnerType(customerFile.getAccountCode());
		        				             		if(partnertype.equals("")){
		        	        				    		  reason="customerFile account not CMM/DMM type  ";  
		        	        				    	  }
		        				             		  
		        				             	  }else{
		        				             		reason="customerFile account code blank ";  
		        				             	  }
		        				             	 if(billing.getNetworkBillToCode()!=null && (!(billing.getNetworkBillToCode().toString().equals("")))){
			            							   String NetworkBillToCode=""; 
			            							   NetworkBillToCode=  partnerManager.checkPartnerType(billing.getNetworkBillToCode());
			        				             		if(NetworkBillToCode.equals("")){
			        	        				    		  reason="billing Network BillToCode not CMM/DMM type  ";  
			        	        				    	  }
			            						   }else{
			            							   reason="billing Network BillToCode blank ";
			            						   } 
		        				             	if(accountLine.getNetworkBillToCode()!=null && (!(accountLine.getNetworkBillToCode().toString().equals("")))){
			            							   String NetworkBillToCode=""; 
			            							   NetworkBillToCode=  partnerManager.checkPartnerType(accountLine.getNetworkBillToCode());
			        				             		if(NetworkBillToCode.equals("")){
			        	        				    		  reason="accountline Network BillToCode not CMM/DMM type  ";  
			        	        				    	  }
			            						   }else{
			            							   reason="accountline Network BillToCode blank ";
			            						   } 
		           				    	  if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
		           				    		  billtotype =accountLine.getBillToCode().trim().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().trim());
		           				    	  }else{
		           				    		  reason="accountline bill to code blank "; 
		           				    	  }
		           				    	  if(!billtotype){
		           				    		  reason="accountline bill to code is not same as  trackingstatus network code ";  
		           				    	  }
		      				    	 
		      				    	  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (!(partnertype.equals("")))&& (billtotype)){
		      				    	   //List linkedShipNumber=findLinkerShipNumber(serviceOrder);
		      				    		List  linkedShipNumber=new ArrayList();
		      				    		if(trackingStatus.getNetworkAgentExSO()!=null && (!(trackingStatus.getNetworkAgentExSO().toString().trim().equals(""))))
		      				    		{
		      				    		linkedShipNumber.add(trackingStatus.getNetworkAgentExSO());
		      				    		}
		      				    		if(serviceOrder.getBookingAgentShipNumber()!=null && (!(serviceOrder.getBookingAgentShipNumber().toString().trim().equals(""))))
		      				    		{
		      				    		linkedShipNumber.add(serviceOrder.getBookingAgentShipNumber());
		      				    		}
		      							List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
		      							Iterator  itss=serviceOrderRecords.iterator();
		      					    	while(itss.hasNext()){
		      					    		  serviceOrderToRecods=(ServiceOrder)itss.next();
		      					    		try{
		      					    		trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
		      					    		 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
		      					        	if(trackingStatusToRecods.getShipNumber()!=null && trackingStatusToRecods.getShipNumber().startsWith("UTSI")){
		      					    		 if(trackingStatusToRecods.getSoNetworkGroup() && !trackingStatusToRecods.getAccNetworkGroup()){
		      					        		
		      					        	}else{
		      					        	reason="tracking soNetworkGroup flag false in UTSI "; 	
		      					        	}
		      					    		if(accountLine.getReceivedInvoiceDate() !=null && trackingStatusToRecods.getCreatedOn()!=null && trackingStatusToRecods.getCreatedOn().compareTo(accountLine.getReceivedInvoiceDate())>0 ){
		   					    			 reason="S/O link done ("+trackingStatusToRecods.getCreatedOn()+") after accountline Invoiced Date of Invoice:"+accountLine.getReceivedInvoiceDate(); 
		   					    		    }else{
		   					    			if(accountLine.getRecInvoiceNumber()!=null && (!(accountLine.getRecInvoiceNumber().equals(""))) ){
		   					    				reason="accountline should be link but invoiced ";
		   					    			}else{
		   					    				reason="accountline should be link";
		   					    			}
		   					    		 }
		      					        	}
		      					    		}catch (Exception e) {
		      			        	            e.printStackTrace();
		      				        	     }
		      					    	}
		      				      }else{
		      				    	if(reason==null || reason.trim().equals("")){
		      				    	  reason="accountline RevenueAmount is zero  "; 
		      				    	}
		      				      }
		            					   }}else{
		      				    	 reason="serviceOrder IsNetworkRecord flag false "; 
		      				     }
		      				    }else{
		      				    	reason="tracking AccNetworkGroup flag false ";	
		      				    }
		      				}else{
		        					reason="selected billing contract is non CMM/DMM.";
		        				}
		        		 outputStream.write(((sid)+" \t").getBytes());	
		        		 outputStream.write(((id)+" \t").getBytes());	
		        		 outputStream.write(((accountLine.getShipNumber())+" \t").getBytes());
		       		     outputStream.write((accountLine.getAccountLineNumber() + " \t").getBytes());
		       		     outputStream.write((accountLine.getEstimateRevenueAmount() + " \t").getBytes());
		       		     outputStream.write((accountLine.getRevisionRevenueAmount() + " \t").getBytes());
		       		     outputStream.write((accountLine.getActualRevenue() + " \t").getBytes());
		       		     outputStream.write((accountLine.getRecInvoiceNumber() + " \t").getBytes());
		       		     outputStream.write((accountLine.getBillToCode() + " \t").getBytes());
		       		     outputStream.write((billing.getBillToCode() + " \t").getBytes());
		       		     String contract="";
		       		     if(billingDMMContractType){
		       		    	contract="DMM"; 
		       		     }
		       		     if(billingCMMContractType){
		     		    	contract="CMM"; 
		     		     }
		       		     outputStream.write((contract + " \t").getBytes());
		       		     outputStream.write((trackingStatus.getContractType() + " \t").getBytes());
		       		     outputStream.write((reason + "").getBytes());
		       		     outputStream.write("\r\n".getBytes());
		                 }
				 }
		                 catch (Exception e) {
		        	            e.printStackTrace();
		        	     } 
		    		     } 
		                
		            }  
		     }
		

	
	@SkipValidation
	public void processNonLinkedAccountLineUpload() throws Exception,EOFException{ 
		FileInputStream fis = null;
        int record=0;
        String totRecords=null;
        HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file1=new File("nonLinkedAccountline"); 
		response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls");
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
        outputStream.write("shipnumber \t".getBytes());
		outputStream.write("AccountLine Number \t".getBytes());
		outputStream.write("Estimate Revenue \t".getBytes());
		outputStream.write("Revision Revenue \t".getBytes());
		outputStream.write("Actual Revenue \t".getBytes());
		outputStream.write("Invoice# \t".getBytes());
		outputStream.write("AccountLine Bill to code \t".getBytes());
		outputStream.write("Billing Bill to code \t".getBytes());
		outputStream.write("Billing Contract \t".getBytes());
		outputStream.write("link Contract(TrackingStatus) \t".getBytes());
		outputStream.write("Reason".getBytes());
		outputStream.write("\r\n".getBytes());
        if(fileExt.equalsIgnoreCase("xls")){
            try{
            	List myList = new ArrayList(); 
    			fis = new FileInputStream(file);
                HSSFWorkbook workbook = new HSSFWorkbook(fis);
                HSSFSheet sheet = workbook.getSheetAt(0);
                Iterator rows = sheet.rowIterator();
                while (rows.hasNext()) {
                	try {
                	String 	reason="";
                    HSSFRow row = (HSSFRow) rows.next();
                    if(row.getRowNum()==0){
                	  continue;
                	}
                    Iterator cells = row.cellIterator();
                    myList.clear();
                    while (cells.hasNext()) {
                    	HSSFCell cell = (HSSFCell) cells.next();
                        int type = cell.getCellType();
                        int cellNum = cell.getCellNum();
                        
                        if (cellNum == 0) {
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getRichStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        }
                        else if (cellNum == 1) {
                        	if(type == HSSFCell.CELL_TYPE_STRING ){
                        		myList.add(cell.getRichStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        	myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        }  
                       
                                     
                    }
                    if(myList.get(0)!=null && (!(myList.get(0).toString().equals("")))){
            			sid=(Long.parseLong((myList.get(0).toString())));
            			}else{
            				sid=0L;	
            			}
            		if(myList.get(1)!=null && (!(myList.get(1).toString().equals("")))){
                			id=(Long.parseLong((myList.get(1).toString())));
                			}else{
                				id=0L;	
                			}
            		billing =billingManager.getForOtherCorpid(sid);
        			trackingStatus=trackingStatusManager.getForOtherCorpid(sid);
        			serviceOrder=serviceOrderManager.getForOtherCorpid(sid); 
        			customerFile = customerFileManager.getForOtherCorpid(serviceOrder.getCustomerFileId()); 
        			accountLine=accountLineManager.getForOtherCorpid(id);
        			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
        				  billingCMMContractType = trackingStatusManager.getCMMContractTypeForOtherCorpid(accountLine.getCorpID() ,billing.getContract());
        			}
        			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
        			      	billingDMMContractType = trackingStatusManager.getDMMContractTypeForOtherCorpid(accountLine.getCorpID() ,billing.getContract());
        			}
        			if(billingCMMContractType){ 
        				    if(trackingStatus.getAccNetworkGroup())	{
        				      if(serviceOrder.getIsNetworkRecord()){
        				    	  String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),accountLine.getCorpID(), trackingStatus.getNetworkPartnerCode());
              					  if(!(networkSynchedId.equals(""))){
              						synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
              						serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
              						reason="accountline("+id+") already linked with "+serviceOrderToRecods.getShipNumber()+"and "+synchedAccountLine.getAccountLineNumber()+"Line # "+networkSynchedId+"Status of accountline "+synchedAccountLine.isStatus(); 
              					   }else{ 
              						 String partnertype="";
             				    	  if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
             				    		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
             				    	  }else{
             				    		  reason="accountline bill to code blank "; 
             				    	  }
             				    	  if(partnertype.equals("")){
             				    		  reason="accountline bill to code not CMM/DMM type  ";  
             				    	  }
        				    	  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (!(partnertype.equals("")))){
        				    	   //List linkedShipNumber=findLinkerShipNumber(serviceOrder);
        				    		List  linkedShipNumber=new ArrayList();
        				    		if(trackingStatus.getNetworkAgentExSO()!=null && (!(trackingStatus.getNetworkAgentExSO().toString().trim().equals(""))))
        				    		{
        				    		linkedShipNumber.add(trackingStatus.getNetworkAgentExSO());
        				    		}
        				    		if(serviceOrder.getBookingAgentShipNumber()!=null && (!(serviceOrder.getBookingAgentShipNumber().toString().trim().equals(""))))
        				    		{
        				    		linkedShipNumber.add(serviceOrder.getBookingAgentShipNumber());
        				    		}
        							List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
        							Iterator  it=serviceOrderRecords.iterator();
        					    	while(it.hasNext()){
        					    		  serviceOrderToRecods=(ServiceOrder)it.next();
        					    		try{
        					    		trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
        					    		 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
        					        	if(trackingStatusToRecods.getShipNumber()!=null && trackingStatusToRecods.getShipNumber().startsWith("UTSI")){
        					    		 if(trackingStatusToRecods.getSoNetworkGroup()){
        					        		
        					        	}else{
        					        	reason="tracking soNetworkGroup flag false in UTSI "; 	
        					        	}
        					    		 if(accountLine.getReceivedInvoiceDate() !=null && trackingStatusToRecods.getCreatedOn()!=null && trackingStatusToRecods.getCreatedOn().compareTo(accountLine.getReceivedInvoiceDate())>0 ){
        					    			 reason="S/O link done ("+trackingStatusToRecods.getCreatedOn()+") after accountline Invoiced Date of Invoice:"+accountLine.getReceivedInvoiceDate(); 
        					    		 }else{
        					    			if(accountLine.getRecInvoiceNumber()!=null && (!(accountLine.getRecInvoiceNumber().equals(""))) ){
        					    				reason="accountline should be link but invoiced ";
        					    			}else{
        					    				reason="accountline should be link";
        					    			}
        					    		 }
        					        	}
        					    		}catch (Exception e) {
        			        	            e.printStackTrace();
        				        	     }
        					    	}
        				      }else{
        				    	  if(reason==null || reason.trim().equals("")){
        				    	  reason="accountline RevenueAmount is zero  ";
        				    	  }
        				      }
              					   }}else{
        				    	 reason="serviceOrder IsNetworkRecord flag false "; 
        				     }
        				    }else{
        				    	reason="tracking AccNetworkGroup flag false ";	
        				    }
        				} 
        			else if(billingDMMContractType){ 
    				    if(trackingStatus.getAccNetworkGroup())	{
      				      if(serviceOrder.getIsNetworkRecord()){
      				    	  String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),accountLine.getCorpID(), trackingStatus.getNetworkPartnerCode());
            					  if(!(networkSynchedId.equals(""))){
            						synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
            						serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
            						reason="accountline("+id+") already linked with "+serviceOrderToRecods.getShipNumber()+"and "+synchedAccountLine.getAccountLineNumber()+"Line # "+networkSynchedId+"Status of accountline "+synchedAccountLine.isStatus();
            					   }else{
            						   String partnertype="";
        				            	boolean billtotype=false;
        				             	  if(customerFile.getAccountCode()!=null && (!(customerFile.getAccountCode().toString().equals(""))) ){
        				             		  partnertype=  partnerManager.checkPartnerType(customerFile.getAccountCode());
        				             		if(partnertype.equals("")){
        	        				    		  reason="customerFile account not CMM/DMM type  ";  
        	        				    	  }
        				             		  
        				             	  }else{
        				             		reason="customerFile account code blank ";  
        				             	  }
        				             	 if(billing.getNetworkBillToCode()!=null && (!(billing.getNetworkBillToCode().toString().equals("")))){
        	                                   String NetworkBillToCode=""; 
        	                                   NetworkBillToCode=  partnerManager.checkPartnerType(billing.getNetworkBillToCode());
        	                                       if(NetworkBillToCode.equals("")){
        	                                          reason="billing Network BillToCode not CMM/DMM type  ";  
        	                                        }
        	                                 }else{
        	                                   reason="billing Network BillToCode blank ";
        	                                 } 
        	                                   if(accountLine.getNetworkBillToCode()!=null && (!(accountLine.getNetworkBillToCode().toString().equals("")))){
        	                                   String NetworkBillToCode=""; 
        	                                   NetworkBillToCode=  partnerManager.checkPartnerType(accountLine.getNetworkBillToCode());
        	                                       if(NetworkBillToCode.equals("")){
        	                                          reason="accountline Network BillToCode not CMM/DMM type  ";  
        	                                        }
        	                                 }else{
        	                                   reason="accountline Network BillToCode blank ";
        	                                 }  	  
           				    	  if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
           				    		  billtotype =accountLine.getBillToCode().trim().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().trim());
           				    	  }else{
           				    		  reason="accountline bill to code blank "; 
           				    	  }
           				    	  if(!billtotype){
           				    		  reason="accountline bill to code is not same as  trackingstatus network code ";  
           				    	  }
      				    	 
      				    	  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (!(partnertype.equals("")))&& (billtotype)){
      				    	   //List linkedShipNumber=findLinkerShipNumber(serviceOrder);
      				    		List  linkedShipNumber=new ArrayList();
      				    		if(trackingStatus.getNetworkAgentExSO()!=null && (!(trackingStatus.getNetworkAgentExSO().toString().trim().equals(""))))
      				    		{
      				    		linkedShipNumber.add(trackingStatus.getNetworkAgentExSO());
      				    		}
      				    		if(serviceOrder.getBookingAgentShipNumber()!=null && (!(serviceOrder.getBookingAgentShipNumber().toString().trim().equals(""))))
      				    		{
      				    		linkedShipNumber.add(serviceOrder.getBookingAgentShipNumber());
      				    		}
      							List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
      							Iterator  it=serviceOrderRecords.iterator();
      					    	while(it.hasNext()){
      					    		  serviceOrderToRecods=(ServiceOrder)it.next();
      					    		try{
      					    		trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
      					    		 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
      					        	if(trackingStatusToRecods.getShipNumber()!=null && trackingStatusToRecods.getShipNumber().startsWith("UTSI")){
      					    		 if(trackingStatusToRecods.getSoNetworkGroup() && !trackingStatusToRecods.getAccNetworkGroup()){
      					        		
      					        	}else{
      					        	reason="tracking soNetworkGroup flag false in UTSI "; 	
      					        	}
      					    		if(accountLine.getReceivedInvoiceDate() !=null && trackingStatusToRecods.getCreatedOn()!=null && trackingStatusToRecods.getCreatedOn().compareTo(accountLine.getReceivedInvoiceDate())>0 ){
   					    			 reason="S/O link done ("+trackingStatusToRecods.getCreatedOn()+") after accountline Invoiced Date of Invoice:"+accountLine.getReceivedInvoiceDate(); 
   					    		    }else{
   					    			if(accountLine.getRecInvoiceNumber()!=null && (!(accountLine.getRecInvoiceNumber().equals(""))) ){
   					    				reason="accountline should be link but invoiced ";
   					    			}else{
   					    				reason="accountline should be link";
   					    			}
   					    		 }
      					        	}
      					    		}catch (Exception e) {
      			        	            e.printStackTrace();
      				        	     }
      					    	}
      				      }else{
      				    	if(reason==null || reason.trim().equals("")){
      				    	  reason="accountline RevenueAmount is zero  ";  
      				    	}
      				      }
            					   }}else{
      				    	 reason="serviceOrder IsNetworkRecord flag false "; 
      				     }
      				    }else{
      				    	reason="tracking AccNetworkGroup flag false ";	
      				    }
      				}else{
        					reason="selected billing contract is non CMM/DMM.";
        				}
        			
        		 outputStream.write(((accountLine.getShipNumber())+" \t").getBytes());
       		     outputStream.write((accountLine.getAccountLineNumber() + " \t").getBytes());
       		     outputStream.write((accountLine.getEstimateRevenueAmount() + " \t").getBytes());
       		     outputStream.write((accountLine.getRevisionRevenueAmount() + " \t").getBytes());
       		     outputStream.write((accountLine.getActualRevenue() + " \t").getBytes());
       		     outputStream.write((accountLine.getRecInvoiceNumber() + " \t").getBytes());
       		     outputStream.write((accountLine.getBillToCode() + " \t").getBytes());
       		     outputStream.write((billing.getBillToCode() + " \t").getBytes());
       		     String contract="";
       		     if(billingDMMContractType){
       		    	contract="DMM"; 
       		     }
       		     if(billingCMMContractType){
     		    	contract="CMM"; 
     		     }
       		     outputStream.write((contract + " \t").getBytes());
       		     outputStream.write((trackingStatus.getContractType() + " \t").getBytes());
       		     outputStream.write((reason + "").getBytes());
       		     outputStream.write("\r\n".getBytes());
                 }
                 catch (Exception e) {
        	            e.printStackTrace();
        	     } 
    		     } 

               try {
    					fis.close();
    		   } catch (IOException e) {
    					e.printStackTrace();
    		   }
                
            }catch (Exception e) {
                e.printStackTrace();
            }
        } 
		
        if(fileExt.equalsIgnoreCase("csv")){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String unProcessedInvoiceNo=""; 
		int row = 0;
		String invalidRow="";
		try { 
	  	String processedInvoiceNo=""; 
		String columnData=new String();
		File newFile = new File(file); 
		BufferedReader bufRdr = new BufferedReader(new FileReader(newFile)); 
		String line = null; 
		int col = 0;
		int count=0; 
		while((line = bufRdr.readLine()) != null) 
		{ 
			String reason="";
			columnData=new String("");
		StringTokenizer st = new StringTokenizer(line,","); 
		try{
		while (st.hasMoreTokens()) 
		{ 	
			columnData=columnData+"~"+st.nextToken().replace('"', ' ').replace('"', ' ').trim(); 
		}
		columnData=columnData.trim();
  	    if(columnData.indexOf(",")==0)
  		 {
  	    	columnData=columnData.substring(1);
  		 }
  		if(columnData.lastIndexOf(",")==columnData.length()-1)
  		 {
  			columnData=columnData.substring(0, columnData.length()-1);
  		 }
  		 String[] arrayData=columnData.split("~");
  		 if(arrayData[1]!=null && (!(arrayData[1].toString().trim().equals("")))){
  		 sid=Long.parseLong(arrayData[1]);
  		 }else{
  			sid=0L; 
  		 }
  		 if(arrayData[2]!=null && (!(arrayData[2].toString().trim().equals("")))){
  	  		 id=Long.parseLong(arrayData[2]);
  	  	}else{
  	  			id=0L; 
  	  	} 
  		billing =billingManager.getForOtherCorpid(sid);
		trackingStatus=trackingStatusManager.getForOtherCorpid(sid);
		serviceOrder=serviceOrderManager.getForOtherCorpid(sid); 
		customerFile = customerFileManager.getForOtherCorpid(serviceOrder.getCustomerFileId()); 
		accountLine=accountLineManager.getForOtherCorpid(id);
		if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			  billingCMMContractType = trackingStatusManager.getCMMContractTypeForOtherCorpid(accountLine.getCorpID() ,billing.getContract());
		}
		if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
		      	billingDMMContractType = trackingStatusManager.getDMMContractTypeForOtherCorpid(accountLine.getCorpID() ,billing.getContract());
		}
		if(billingCMMContractType){ 
			    if(trackingStatus.getAccNetworkGroup())	{
			      if(serviceOrder.getIsNetworkRecord()){
			    	  String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),accountLine.getCorpID(), trackingStatus.getNetworkPartnerCode());
  					  if(!(networkSynchedId.equals(""))){
  						synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
  						serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
  						reason="accountline("+id+") already linked with "+serviceOrderToRecods.getShipNumber()+"and "+synchedAccountLine.getAccountLineNumber()+"Line # "+networkSynchedId+"Status of accountline "+synchedAccountLine.isStatus(); 
  					   }else{ 
  						 String partnertype="";
 				    	  if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
 				    		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
 				    	  }else{
 				    		  reason="accountline bill to code blank "; 
 				    	  }
 				    	  if(partnertype.equals("")){
 				    		  reason="accountline bill to code not CMM/DMM type  ";  
 				    	  }
			    	  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (!(partnertype.equals("")))){
			    	   //List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			    		List  linkedShipNumber=new ArrayList();
			    		if(trackingStatus.getNetworkAgentExSO()!=null && (!(trackingStatus.getNetworkAgentExSO().toString().trim().equals(""))))
			    		{
			    		linkedShipNumber.add(trackingStatus.getNetworkAgentExSO());
			    		}
			    		if(serviceOrder.getBookingAgentShipNumber()!=null && (!(serviceOrder.getBookingAgentShipNumber().toString().trim().equals(""))))
			    		{
			    		linkedShipNumber.add(serviceOrder.getBookingAgentShipNumber());
			    		}
						List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
						Iterator  it=serviceOrderRecords.iterator();
				    	while(it.hasNext()){
				    		  serviceOrderToRecods=(ServiceOrder)it.next();
				    		try{
				    		trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
				    		 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
				        	if(trackingStatusToRecods.getShipNumber()!=null && trackingStatusToRecods.getShipNumber().startsWith("UTSI")){
				    		 if(trackingStatusToRecods.getSoNetworkGroup()){
				        		
				        	}else{
				        	reason="tracking soNetworkGroup flag false in UTSI "; 	
				        	}
				    		 if(accountLine.getReceivedInvoiceDate() !=null && trackingStatusToRecods.getCreatedOn()!=null && trackingStatusToRecods.getCreatedOn().compareTo(accountLine.getReceivedInvoiceDate())>0 ){
				    			 reason="S/O link done ("+trackingStatusToRecods.getCreatedOn()+") after accountline Invoiced Date of Invoice:"+accountLine.getReceivedInvoiceDate(); 
				    		 }else{
				    			if(accountLine.getRecInvoiceNumber()!=null && (!(accountLine.getRecInvoiceNumber().equals(""))) ){
				    				reason="accountline should be link but invoiced ";
				    			}else{
				    				reason="accountline should be link";
				    			}
				    		 }
				        	}
				    		}catch (Exception e) {
		        	            e.printStackTrace();
			        	     }
				    	}
			      }else{
			    	  if(reason==null || reason.trim().equals("")){
			    	  reason="accountline RevenueAmount is zero  ";
			    	  }
			      }
  					   }}else{
			    	 reason="serviceOrder IsNetworkRecord flag false "; 
			     }
			    }else{
			    	reason="tracking AccNetworkGroup flag false ";	
			    }
			} 
		else if(billingDMMContractType){ 
		    if(trackingStatus.getAccNetworkGroup())	{
			      if(serviceOrder.getIsNetworkRecord()){
			    	  String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),accountLine.getCorpID(), trackingStatus.getNetworkPartnerCode());
					  if(!(networkSynchedId.equals(""))){
						synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
						serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
						reason="accountline("+id+") already linked with "+serviceOrderToRecods.getShipNumber()+"and "+synchedAccountLine.getAccountLineNumber()+"Line # "+networkSynchedId+"Status of accountline "+synchedAccountLine.isStatus(); 
					   }else{
						   String partnertype="";
			            	boolean billtotype=false;
			             	  if(customerFile.getAccountCode()!=null && (!(customerFile.getAccountCode().toString().equals(""))) ){
			             		  partnertype=  partnerManager.checkPartnerType(customerFile.getAccountCode());
			             		if(partnertype.equals("")){
        				    		  reason="customerFile account not CMM/DMM type  ";  
        				    	  }
			             		  
			             	  }else{
			             		reason="customerFile account code blank ";  
			             	  }
				    	  if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
				    		  billtotype =(accountLine.getBillToCode().trim()).equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().trim());
				    	  }else{
				    		  reason="accountline bill to code blank "; 
				    	  }
				    	  if(!billtotype){
				    		  reason="accountline bill to code is not same as  trackingstatus network code ";  
				    	  }
			    	 
			    	  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (!(partnertype.equals("")))&& (billtotype)){
			    	   //List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			    		List  linkedShipNumber=new ArrayList();
			    		if(trackingStatus.getNetworkAgentExSO()!=null && (!(trackingStatus.getNetworkAgentExSO().toString().trim().equals(""))))
			    		{
			    		linkedShipNumber.add(trackingStatus.getNetworkAgentExSO());
			    		}
			    		if(serviceOrder.getBookingAgentShipNumber()!=null && (!(serviceOrder.getBookingAgentShipNumber().toString().trim().equals(""))))
			    		{
			    		linkedShipNumber.add(serviceOrder.getBookingAgentShipNumber());
			    		}
						List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
						Iterator  it=serviceOrderRecords.iterator();
				    	while(it.hasNext()){
				    		  serviceOrderToRecods=(ServiceOrder)it.next();
				    		try{
				    		trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
				    		 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
				        	if(trackingStatusToRecods.getShipNumber()!=null && trackingStatusToRecods.getShipNumber().startsWith("UTSI")){
				    		 if(trackingStatusToRecods.getSoNetworkGroup() && !trackingStatusToRecods.getAccNetworkGroup()){
				        		
				        	}else{
				        	reason="tracking soNetworkGroup flag false in UTSI "; 	
				        	}
				    		 if(accountLine.getReceivedInvoiceDate() !=null && trackingStatusToRecods.getCreatedOn()!=null && trackingStatusToRecods.getCreatedOn().compareTo(accountLine.getReceivedInvoiceDate())>0 ){
				    			 reason="S/O link done ("+trackingStatusToRecods.getCreatedOn()+") after accountline Invoiced Date of Invoice:"+accountLine.getReceivedInvoiceDate(); 
				    		 }else{
				    			if(accountLine.getRecInvoiceNumber()!=null && (!(accountLine.getRecInvoiceNumber().equals(""))) ){
				    				reason="accountline should be link but invoiced ";
				    			}else{
				    				reason="accountline should be link";
				    			}
				    		 }
				        	}
				    		}catch (Exception e) {
		        	            e.printStackTrace();
			        	     }
				    	}
			      }else{
			    	  if(reason==null || reason.trim().equals("")){
			    	  reason="accountline RevenueAmount is zero  ";
			    	  }
			      }
					   }}else{
			    	 reason="serviceOrder IsNetworkRecord flag false "; 
			     }
			    }else{
			    	reason="tracking AccNetworkGroup flag false ";	
			    }
			}else{
				reason="selected billing contract is non CMM/DMM.";
			}
		
	     outputStream.write(((accountLine.getShipNumber())+" \t").getBytes());
	     outputStream.write((accountLine.getAccountLineNumber() + " \t").getBytes());
	     outputStream.write((accountLine.getEstimateRevenueAmount() + " \t").getBytes());
		 outputStream.write((accountLine.getRevisionRevenueAmount() + " \t").getBytes());
		 outputStream.write((accountLine.getActualRevenue() + " \t").getBytes());
		 outputStream.write((accountLine.getRecInvoiceNumber() + " \t").getBytes());
		 outputStream.write((accountLine.getBillToCode() + " \t").getBytes());
		 outputStream.write((billing.getBillToCode() + " \t").getBytes());
	     String contract="";
	     if(billingDMMContractType){
	    	contract="DMM"; 
	     }
	     if(billingCMMContractType){
	    	contract="CMM"; 
	     }
	     outputStream.write((contract + " \t").getBytes());
	     outputStream.write((trackingStatus.getContractType() + " \t").getBytes());
	     outputStream.write((reason + "").getBytes());
	     outputStream.write("\r\n".getBytes());
  		 
  		 
  		 
  		 
  		 
		}catch (Exception e) { 
			row=row+1; 
			invalidRow=invalidRow+", "+row; 
		}
		}  
		bufRdr.close();
		
		}catch (FileNotFoundException e) {
		  	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(accountLine.getCorpID(),new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	//return "errorlog";
        } catch (IOException e) {
         	 e.printStackTrace();
        	String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(accountLine.getCorpID(),new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

		  	//return "errorlog";
        }
        catch(ArrayIndexOutOfBoundsException ae){}
        catch (Exception e) { 
         	 e.printStackTrace();
        	String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(accountLine.getCorpID(),new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		
		  	//return "errorlog";
        } 
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		//return SUCCESS; 
    }
	}
	
	@SkipValidation
	public String solomonsPmtUpdate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
		return SUCCESS;
	}
	
	@SkipValidation
	public String greatPlainsPmtUpdate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
		return SUCCESS;
	}
	
	@SkipValidation
	public String processPayPayment(){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String unProcessedInvoiceNo=""; 
		int row = 0;
		String invalidRow="";
		try { 
	  	String processedInvoiceNo=""; 
		String columnData=new String();
		File newFile = new File(file); 
		BufferedReader bufRdr = new BufferedReader(new FileReader(newFile)); 
		String line = null; 
		int col = 0;
		int count=0;
  
	
		while((line = bufRdr.readLine()) != null) 
		{ 
			columnData=new String("");
		StringTokenizer st = new StringTokenizer(line,","); 
		try{
		while (st.hasMoreTokens()) 
		{ 	
			columnData=columnData+"~"+st.nextToken().replace('"', ' ').replace('"', ' ').trim(); 
		}
		columnData=columnData.trim();
	  	    if(columnData.indexOf(",")==0)
	  		 {
	  	    	columnData=columnData.substring(1);
	  		 }
	  		if(columnData.lastIndexOf(",")==columnData.length()-1)
	  		 {
	  			columnData=columnData.substring(0, columnData.length()-1);
	  		 }
	  		String[] arrayData=columnData.split("~");  
	  		String actgcode =arrayData[1];
	  		String vendorInvoice="";
	  		vendorInvoice=arrayData[2];
	  		if(vendorInvoice.length()>15){ 
	  			vendorInvoice=vendorInvoice.substring(0,15);
	  		} 
	  		String payPayablevia =arrayData[3];  
	  		String payPayableDate  =""; 
	  		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	        Date du = new Date();
	        du = df.parse(arrayData[4]);
	        df = new SimpleDateFormat("yyyy-MM-dd");
	        payPayableDate  = df.format(du); 
	        BigDecimal payPayableAmount=new BigDecimal(arrayData[5]);
	        String shipNumber=arrayData[6];
	        String paygl=arrayData[7];
	        String shipNumbernew=shipNumber.toString().substring(4,5);  
	        if(shipNumbernew.equals("0")||shipNumbernew.equals("1")||shipNumbernew.equals("2")||shipNumbernew.equals("3")||shipNumbernew.equals("4")||shipNumbernew.equals("5")||shipNumbernew.equals("6")||shipNumbernew.equals("7")||shipNumbernew.equals("8")||shipNumbernew.equals("9")){
	        count=accountLineManager.updatePayablesUpload(sessionCorpID ,actgcode,vendorInvoice,payPayablevia,payPayableDate,payPayableAmount,shipNumber,paygl);
  		    if(count==0){
			  unProcessedInvoiceNo = new StringBuilder(unProcessedInvoiceNo).append(vendorInvoice+", ").toString(); 
		    }else{
			  processedInvoiceNo = new StringBuilder(processedInvoiceNo).append(vendorInvoice+",").toString();
		    }
	        }
  		row++;
		}catch (Exception e) { 
			row=row+1; 
			invalidRow=invalidRow+", "+row; 
		}
		}  
		bufRdr.close();
		if(!(invalidRow.toString().equals(""))){
		String key1 ="Incorrect data format in row number "+invalidRow+" the uploaded file.";  
     	saveMessage(getText(key1));
		}
		if(unProcessedInvoiceNo != ""){
			String key ="\n\nThe following Invoice #s have not been processed: " +unProcessedInvoiceNo.subSequence(0, unProcessedInvoiceNo.length()-1).toString(); 
			saveMessage(getText(key)); 
		}else{
			String key ="Invoice processed successfully."; 
			saveMessage(getText(key)); 
		}
		}catch (FileNotFoundException e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	return "errorlog";
        } catch (IOException e) {
        	String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	return "errorlog";
        }
        catch(ArrayIndexOutOfBoundsException ae){}
        catch (Exception e) { 
        	if(unProcessedInvoiceNo != ""){
    			String key ="\n\nThe following Invoice #s have not been processed: " +unProcessedInvoiceNo.subSequence(0, unProcessedInvoiceNo.length()-1).toString(); 
    			saveMessage(getText(key)); 
    			}
        	else{
    			String key ="Invoice processed successfully."; 
    			saveMessage(getText(key)); 
    		}
        	String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	return "errorlog";
        } 
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}

	@SkipValidation
	public String processRecPayment(){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String unProcessedInvoiceNo=""; 
		int row = 0;
		String invalidRow="";
		try { 
	  	String processedInvoiceNo=""; 
		String columnData=new String();
		File newFile = new File(file); 
		BufferedReader bufRdr = new BufferedReader(new FileReader(newFile)); 
		String line = null; 
		int col = 0;
		int count=0;
//		read each line of text file  
	
		while((line = bufRdr.readLine()) != null) 
		{ 
			columnData=new String("");
		StringTokenizer st = new StringTokenizer(line,","); 
		try{
		while (st.hasMoreTokens()) 
		{ 	
			 String calData=st.nextToken().replace('"', ' ').replace('"', ' ').trim();
			 if(calData.toString().equals("")){
				 calData="NOData"	; 
			 }
			columnData=columnData+"~"+calData; 
		}
		columnData=columnData.trim();
	  	    if(columnData.indexOf(",")==0)
	  		 {
	  	    	columnData=columnData.substring(1);
	  		 }
	  		if(columnData.lastIndexOf(",")==columnData.length()-1)
	  		 {
	  			columnData=columnData.substring(0, columnData.length()-1);
	  		 }
	  		String[] arrayData=columnData.split("~");
	  		String invoiceNumber=arrayData[1].toString().replaceAll("NOData", "");;
	  		String recInvoiceNumber =arrayData[1].toString().replaceAll("NOData", "");;
	  		recInvoiceNumber=recInvoiceNumber.substring(1);
	  		String billToCode=arrayData[2].toString().replaceAll("NOData", "");;
	  		
	  		String statusDate  =""; 
	  		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	        Date du = new Date();
	        du = df.parse(arrayData[3].toString().replaceAll("NOData", ""));
	        df = new SimpleDateFormat("yyyy-MM-dd");
	        statusDate  = df.format(du); 
	        BigDecimal receivedAmount=new BigDecimal(arrayData[4].toString().replaceAll("NOData", ""));
	        String recGl=arrayData[5].toString().replaceAll("NOData", "");
	        String externalReference="";
	        try{
	        	externalReference=arrayData[6].toString().replaceAll("NOData", "");
	        }catch (Exception e) { 
	        	externalReference="";	
			}
	        count=accountLineManager.updateReceivableUpload(sessionCorpID ,recInvoiceNumber,billToCode,statusDate,receivedAmount,systemDefaultmiscVl,recGl,externalReference);
	        //count=accountLineManager.updatePayablesUpload(sessionCorpID ,actgcode,vendorInvoice,payPayablevia,payPayableDate,payPayableAmount,shipNumber,paygl);
  		    if(count==0){
			  unProcessedInvoiceNo = new StringBuilder(unProcessedInvoiceNo).append(invoiceNumber+", ").toString(); 
		    }else{
			  processedInvoiceNo = new StringBuilder(processedInvoiceNo).append(invoiceNumber+",").toString();
		    } 
  		row++;
		}catch (Exception e) { 
			row=row+1; 
			invalidRow=invalidRow+", "+row; 
		}
		}  
		bufRdr.close();
		if(!(invalidRow.toString().equals(""))){
		String key1 ="Incorrect data format in row number "+invalidRow+" the uploaded file.";  
     	saveMessage(getText(key1));
		}
		if(unProcessedInvoiceNo != ""){
			String key ="\n\nThe following Invoice #s have not been processed: " +unProcessedInvoiceNo.subSequence(0, unProcessedInvoiceNo.length()-1).toString(); 
			saveMessage(getText(key)); 
		}else{
			String key ="Invoice processed successfully."; 
			saveMessage(getText(key)); 
		}
		}catch (FileNotFoundException e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	return "errorlog";
        } catch (IOException e) {
        	String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
        	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
        	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
        	 e.printStackTrace();
        	return "errorlog";
        }
        catch(ArrayIndexOutOfBoundsException ae){}
        catch (Exception e) { 
        	if(unProcessedInvoiceNo != ""){
    			String key ="\n\nThe following Invoice #s have not been processed: " +unProcessedInvoiceNo.subSequence(0, unProcessedInvoiceNo.length()-1).toString(); 
    			saveMessage(getText(key)); 
    			}
        	else{
    			String key ="Invoice processed successfully."; 
    			saveMessage(getText(key)); 
    		}
        	String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
        	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
        	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
        	 e.printStackTrace();
        	return "errorlog";
        } 
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
       return SUCCESS; 
	}
	
	private ExtractedFileLogManager extractedFileLogManager;
	private ExtractedFileLog extractedFileLog;
	@SkipValidation
	public String gpProcessPayPayment(){  
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	String unProcessedInvoiceNo=""; 
	int row = 0;
	String invalidRow="";
	try { 
  	String processedInvoiceNo=""; 
	String columnData=new String();
	File newFile = new File(file); 
	BufferedReader bufRdr = new BufferedReader(new FileReader(newFile)); 
	String line = null; 
	int col = 0;
	int count=0;
//	read each line of text file  

	while((line = bufRdr.readLine()) != null) 
	{ 
		columnData=new String("");
	StringTokenizer st = new StringTokenizer(line,","); 
	try{
	while (st.hasMoreTokens()) 
	{ 	
		columnData=columnData+"~"+st.nextToken().replace('"', ' ').replace('"', ' ').trim(); 
	}
	columnData=columnData.trim();
  	    if(columnData.indexOf(",")==0)
  		 {
  	    	columnData=columnData.substring(1);
  		 }
  		if(columnData.lastIndexOf(",")==columnData.length()-1)
  		 {
  			columnData=columnData.substring(0, columnData.length()-1);
  		 }
  		if(columnData.indexOf("AmountPaid")<0){
  		String[] arrayData=columnData.split("~"); 
  		BigDecimal payPayableAmount=new BigDecimal(arrayData[1]);
  		String actgcode =arrayData[2];
  		String payPayablevia =arrayData[3]; 
  		String vendorInvoice="";
  		vendorInvoice=arrayData[4];
  		String shipNumber=arrayData[5];
  		String paygl=arrayData[6];
  		String[] payglData=paygl.split("-");
  		  paygl=payglData[2];
  		String companyDivision=payglData[0]; 
  		String payPayableDate  =""; 
  		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Date du = new Date();
        du = df.parse(arrayData[7]);
        df = new SimpleDateFormat("yyyy-MM-dd");
        payPayableDate  = df.format(du); 
        
        String shipNumbernew=shipNumber.toString().substring(4,5);  
        if(shipNumbernew.equals("0")||shipNumbernew.equals("1")||shipNumbernew.equals("2")||shipNumbernew.equals("3")||shipNumbernew.equals("4")||shipNumbernew.equals("5")||shipNumbernew.equals("6")||shipNumbernew.equals("7")||shipNumbernew.equals("8")||shipNumbernew.equals("9")){
        count=accountLineManager.updateGPPayablesUpload(sessionCorpID ,actgcode,vendorInvoice,payPayablevia,payPayableDate,payPayableAmount,shipNumber,paygl,companyDivision);
		    if(count==0){
		  unProcessedInvoiceNo = new StringBuilder(unProcessedInvoiceNo).append(vendorInvoice+", ").toString(); 
	    }else{
		  processedInvoiceNo = new StringBuilder(processedInvoiceNo).append(vendorInvoice+",").toString();
	    }
        }
		row++;
  		}
	}catch (Exception e) { 
		row=row+1; 
		invalidRow=invalidRow+", "+row; 
	}
	}  
	bufRdr.close();
	String key1="";
	if(!(invalidRow.toString().equals(""))){
	key1 ="Incorrect data format in row number "+invalidRow+" the uploaded file.";  
 	saveMessage(getText(key1));
	}
	String key="";
	if(unProcessedInvoiceNo != ""){
		key ="\n\nThe following Invoice #s have not been processed: " +unProcessedInvoiceNo.subSequence(0, unProcessedInvoiceNo.length()-1).toString(); 
		saveMessage(getText(key)); 
	}else{
		key ="Invoice processed successfully."; 
		saveMessage(getText(key)); 
	}
	String messageLogVal="";
	if(!key1.equalsIgnoreCase("") && !key.equalsIgnoreCase("")){
		messageLogVal = key1+"<BR>"+key;
	}else if(!key1.equalsIgnoreCase("")){
		messageLogVal = key1;
	}else if(!key.equalsIgnoreCase("")){
		messageLogVal = key;
	}
	extractedFileLog = new ExtractedFileLog();
	extractedFileLog.setCorpID(sessionCorpID);
	extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
	extractedFileLog.setCreatedOn(new Date());
	extractedFileLog.setLocation("");
	extractedFileLog.setFileName(fileFileName);
	extractedFileLog.setModule("Great Plains Pay Pmt Update");
	extractedFileLog.setMessageLog(messageLogVal);
	extractedFileLog=extractedFileLogManager.save(extractedFileLog);
	}catch (FileNotFoundException e) {
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	 e.printStackTrace();
  	return "errorlog";
    } catch (IOException e) {
    	String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	 e.printStackTrace();
  	return "errorlog";
    }
    catch(ArrayIndexOutOfBoundsException ae){}
    catch (Exception e) { 
    	if(unProcessedInvoiceNo != ""){
			String key ="\n\nThe following Invoice #s have not been processed: " +unProcessedInvoiceNo.subSequence(0, unProcessedInvoiceNo.length()-1).toString(); 
			saveMessage(getText(key)); 
			}
    	else{
			String key ="Invoice processed successfully."; 
			saveMessage(getText(key)); 
		}
    	String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	 e.printStackTrace();
  	return "errorlog";
    } 
   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS; 
}

@SkipValidation
public String gpProcessRecPayment(){ 
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	String unProcessedInvoiceNo=""; 
	int row = 0;
	String invalidRow="";
	try { 
  	String processedInvoiceNo=""; 
	String columnData=new String();
	File newFile = new File(file); 
	BufferedReader bufRdr = new BufferedReader(new FileReader(newFile)); 
	String line = null; 
	int col = 0;
	int count=0;
//	read each line of text file  

	while((line = bufRdr.readLine()) != null) 
	{ 
		columnData=new String("");
	StringTokenizer st = new StringTokenizer(line,","); 
	try{
	while (st.hasMoreTokens()) 
	{ 	
		 String calData=st.nextToken().replace('"', ' ').replace('"', ' ').trim();
		 if(calData.toString().equals("")){
			 calData="NOData"; 
		 }
		columnData=columnData+"~"+calData; 
	}
	columnData=columnData.trim();
	if(columnData.indexOf("InvoiceNumber")<0){
  	    if(columnData.indexOf(",")==0)
  		 {
  	    	columnData=columnData.substring(1);
  		 }
  		if(columnData.lastIndexOf(",")==columnData.length()-1)
  		 {
  			columnData=columnData.substring(0, columnData.length()-1);
  		 }
  		String[] arrayData=columnData.split("~");
  		String invoiceNumber=arrayData[1].toString().replaceAll("NOData", "");;
  		String recInvoiceNumber =arrayData[1].toString().replaceAll("NOData", "");;
  		recInvoiceNumber=recInvoiceNumber.substring(1);
  		String billToCode=arrayData[2].toString().replaceAll("NOData", "");;
  		
  		String statusDate  =""; 
  		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Date du = new Date();
        du = df.parse(arrayData[3].toString().replaceAll("NOData", ""));
        df = new SimpleDateFormat("yyyy-MM-dd");
        statusDate  = df.format(du); 
        BigDecimal receivedAmount=new BigDecimal(arrayData[4].toString().replaceAll("NOData", ""));
        BigDecimal receivedAmount1=new BigDecimal(arrayData[5].toString().replaceAll("NOData", ""));
        BigDecimal balance=new BigDecimal(arrayData[6].toString().replaceAll("NOData", ""));
        String payingStatus="";
        if(balance.doubleValue()==0){
        	receivedAmount=receivedAmount;
        	payingStatus="Fully Paid";
        }else{
        	receivedAmount=receivedAmount1;
        	payingStatus="Partially Paid";
        }
//        String recGl=arrayData[5].toString().replaceAll("NOData", "");
 //       String[] recGlData=recGl.split("-");
  //      recGl=recGlData[2];
   //     String jobtypeCode=recGlData[1];//its pick division
    //    String companyDivision=recGlData[0];
        String externalReference="";
        try{
        	externalReference=arrayData[7].toString().replaceAll("NOData", "");
        }catch (Exception e) { 
        	externalReference="";	
		}
        
        count=accountLineManager.updateGPReceivableUpload(sessionCorpID ,recInvoiceNumber,billToCode,statusDate,receivedAmount,systemDefaultmiscVl,payingStatus,externalReference,"","");
        //count=accountLineManager.updatePayablesUpload(sessionCorpID ,actgcode,vendorInvoice,payPayablevia,payPayableDate,payPayableAmount,shipNumber,paygl);
		    if(count==0){
		  unProcessedInvoiceNo = new StringBuilder(unProcessedInvoiceNo).append(invoiceNumber+", ").toString(); 
	    }else{
		  processedInvoiceNo = new StringBuilder(processedInvoiceNo).append(invoiceNumber+",").toString();
	    } 
		row++;
	}
	}catch (Exception e) { 
		row=row+1; 
		invalidRow=invalidRow+", "+row; 
	}
	}  
	bufRdr.close();
	String key1="";
	if(!(invalidRow.toString().equals(""))){
	key1 ="Incorrect data format in row number "+invalidRow+" the uploaded file.";  
 	saveMessage(getText(key1));
	}
	String key="";
	if(unProcessedInvoiceNo != ""){
		key ="\n\nThe following Invoice #s have not been processed: " +unProcessedInvoiceNo.subSequence(0, unProcessedInvoiceNo.length()-1).toString(); 
		saveMessage(getText(key)); 
	}else{
		key ="Invoice processed successfully."; 
		saveMessage(getText(key)); 
	}
	String messageLogVal="";
	if(!key1.equalsIgnoreCase("") && !key.equalsIgnoreCase("")){
		messageLogVal = key+"<BR>"+key1;
	}else if(!key1.equalsIgnoreCase("")){
		messageLogVal = key1;
	}else if(!key.equalsIgnoreCase("")){
		messageLogVal = key;
	}
	extractedFileLog = new ExtractedFileLog();
	extractedFileLog.setCorpID(sessionCorpID);
	extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
	extractedFileLog.setCreatedOn(new Date());
	extractedFileLog.setLocation("");
	extractedFileLog.setFileName(fileFileName);
	extractedFileLog.setModule("Great Plains Rec Pmt Update");
	extractedFileLog.setMessageLog(messageLogVal);
	extractedFileLog=extractedFileLogManager.save(extractedFileLog);
	}catch (FileNotFoundException e) {
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	 e.printStackTrace();
  	 return "errorlog";
    } catch (IOException e) {
    	String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	 e.printStackTrace();
  	 return "errorlog";
    }
    catch(ArrayIndexOutOfBoundsException ae){}
    catch (Exception e) { 
    	if(unProcessedInvoiceNo != ""){
			String key ="\n\nThe following Invoice #s have not been processed: " +unProcessedInvoiceNo.subSequence(0, unProcessedInvoiceNo.length()-1).toString(); 
			saveMessage(getText(key)); 
			}
    	else{
			String key ="Invoice processed successfully."; 
			saveMessage(getText(key)); 
		}
    	String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	 e.printStackTrace();
  	 return "errorlog";
    } 
   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
   return SUCCESS; 
}


	@SkipValidation
	public String clientPaymentUpload(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
		return SUCCESS;
	}
	private String uploadBillToCode;
	private Date processingDate; 
	private String file;
	private String fileNotExists;
	private String fileFileName;
	
	@SkipValidation
	public String processPayment(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String thisLine; 
			String processedInvoiceNo="";
			String unProcessedInvoiceNo="";
			      
			      File newFile = new File(file);
			      
			      if(newFile.length()==0){
			     	 String key ="File does not contain any invoice number.";  
			     	errorMessage(getText(key)); 
			 		 return SUCCESS;
			      }
			      
			      FileInputStream fis = null;
			      BufferedInputStream bis = null;
			      DataInputStream dis = null;
			      
			      try {
			           fis = new FileInputStream(newFile);
			           bis = new BufferedInputStream(fis);
			           dis = new DataInputStream(bis);

			           while (dis.available() != 0) {
			          	 while ((thisLine = dis.readLine()) != null){
								StringTokenizer st =new StringTokenizer(thisLine,",");
									while(st.hasMoreElements()){
										String invoiceNo = st.nextToken();
										invoiceNo = invoiceNo.trim();
										int temp = accountLineManager.updateProcessDate(processingDate, invoiceNo, uploadBillToCode);
										if(temp==0){
											unProcessedInvoiceNo = new StringBuilder(unProcessedInvoiceNo).append(invoiceNo+",").toString(); 
										}else{
											processedInvoiceNo = new StringBuilder(processedInvoiceNo).append(invoiceNo+",").toString();
										}
										System.err.println(invoiceNo+"\n");
									}
							}
			           }
			           fis.close();
			           bis.close();
			           dis.close();
			           if(unProcessedInvoiceNo != ""){
							String key ="Unprocessed Invoice Number is : "+unProcessedInvoiceNo.subSequence(0, unProcessedInvoiceNo.length()-1).toString(); 
							saveMessage(getText(key)); 
						}else{
							String key ="Invoice processed successfully."; 
							saveMessage(getText(key)); 
						}
			         } catch (FileNotFoundException e) {
			           e.printStackTrace();
			         } catch (IOException e) {
			           e.printStackTrace();
			         }
			         catch (Exception e) {
			        	 String key ="Incorrect data format in the uploaded file.";  
			        	 errorMessage(getText(key)); 
				         }
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
	    	 return "errorlog";
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    return SUCCESS;
	}
	
	
	@SkipValidation
	public String getNotPaidInvoiceList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		invoiceCheck = invoiceCheck;
		paidInvoiceList=accountLineManager.getNotPaidInvoiceList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	 
	
	@SkipValidation
	public String updatePaidInvoice(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			if(invoiceCheck.equals("")||invoiceCheck.equals(","))
 	  {
 	  }
 	 else
 	  {
			 String userName=getRequest().getRemoteUser();
			 invoiceCheck=invoiceCheck.trim();
			if(invoiceCheck.indexOf(",")==0)
			 {
				invoiceCheck=invoiceCheck.substring(1);
			 }
			if(invoiceCheck.lastIndexOf(",")==invoiceCheck.length()-1)
			 {
				invoiceCheck=invoiceCheck.substring(0, invoiceCheck.length()-1);
			 }
			String[] arrayInvoice=invoiceCheck.split(",");
			int arrayLength = arrayInvoice.length;
			for(int i=0;i<arrayLength;i++)
			 {	
			   String recInvoiceNumber=arrayInvoice[i];  
			      int payPost=accountLineManager.updatePaidInvoice(recInvoiceNumber,sessionCorpID,userName);
			 }
			String key = "Invoice number ("+invoiceCheck+ ") have been updated as Fully Paid in account line";   
			saveMessage(getText(key));
			invoiceCheck="";
 	 }
			paidInvoiceList=accountLineManager.getNotPaidInvoiceList(sessionCorpID);
		} catch (Exception e) {
		   	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

		return SUCCESS;	
	} 
	
	@SkipValidation
	public String searchPaidInvoiceList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
		paidInvoiceList=accountLineManager.searchNotPaidInvoice(notPaidInvoiceNumber,notPaidBillToCode,notPaidBillToName,sessionCorpID);
		}catch(Exception e)
		{
			 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

		return SUCCESS;	
	}
	
	private String activateAccPortal;
	@SkipValidation
	public String activateAccPortal(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		serviceOrder = serviceOrderManager.get(sid);
		trackingStatus =trackingStatusManager.get(sid);
		boolean soNetworkGroup=false;
		if(trackingStatus.getAccNetworkGroup()!=null){
			soNetworkGroup=trackingStatus.getAccNetworkGroup();
		}
		int value = accountLineManager.setActivateAccPortal(activateAccPortal, sessionCorpID,soNetworkGroup);
	    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String updateAccInactive(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(sid);
			trackingStatus =trackingStatusManager.get(sid);
			String loginUser = getRequest().getRemoteUser();	
			boolean soNetworkGroup=false;
			if(trackingStatus.getAccNetworkGroup()!=null){
				soNetworkGroup=trackingStatus.getAccNetworkGroup();
			}
			if(accountIdCheck.equals("")||accountIdCheck.equals(","))
			  {
			  }
			 else
			  {
				accountIdCheck=accountIdCheck.trim();
			    if(accountIdCheck.indexOf(",")==0)
				 {
			    	accountIdCheck=accountIdCheck.substring(1);
				 }
				if(accountIdCheck.lastIndexOf(",")==accountIdCheck.length()-1)
				 {
					accountIdCheck=accountIdCheck.substring(0, accountIdCheck.length()-1);
				 }
			    String[] arrayInvoice=accountIdCheck.split(",");
			    int arrayLength = arrayInvoice.length;
			    String accId="";
				for(int i=0;i<arrayLength;i++)
				 {	
				      if(accId.equals("")){
			        		accId	=arrayInvoice[i];
			        	}else{
			        		accId=accId+","+arrayInvoice[i];
			        	}
			          
			     }
				int payPost=accountLineManager.updateAccInactive(accId,sessionCorpID,soNetworkGroup,sid,loginUser);
				accountIdCheck="";
				try{
				accountLineManager.updateCommisionLockingTable(accId,sessionCorpID,loginUser);
				}catch (Exception e) {
					e.printStackTrace();
				}
			 }
			 
			updateAccountLine(serviceOrder);
			if(soNetworkGroup){

				List linkedShipNumber=findLinkerShipNumber(serviceOrder);
				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
				Iterator  it=serviceOrderRecords.iterator();
				while(it.hasNext()){
					try{
					serviceOrderToRecods=(ServiceOrder)it.next(); 
					trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
					if(trackingStatusToRecods.getSoNetworkGroup() && (!trackingStatusToRecods.getAccNetworkGroup())  && (!(serviceOrder.getShipNumber().toString().equalsIgnoreCase(serviceOrderToRecods.getShipNumber().toString())))){
						updateExternalAccountLine(serviceOrderToRecods,serviceOrderToRecods.getCorpID());		
			    	}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
			
			}
			estimateAccountList(); 
			
			if("category".equals(vanLineAccountView)){
				   vanLineAccCategoryURL = "?sid="+serviceOrder.getId()+"&vanLineAccountView="+vanLineAccountView;
				   return "category"; 
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
	    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
		}
	
	@SkipValidation
	public String emailStorageInvoice(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private List valuationList;
	private List transportationList;
	private List packingList;
	private List packingContList;
	private List packingUnpackList;
	private List otherMiscellaneousList;
	private List underLyingHaulList;
	private String packingSum;
	private String packContSum;
	private String packUnpackSum;
	private String transportSum;
	private String othersSum;
	private String valuationSum;
	@SkipValidation
	public String estimatePricingList(){
		try {
			serviceOrder = serviceOrderManager.get(sid);
			miscellaneous=miscellaneousManager.get(sid);
			valuationList=serviceOrderManager.findValuationList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			packingList=serviceOrderManager.findPackingList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			underLyingHaulList=serviceOrderManager.findUnderLyingHaulList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			otherMiscellaneousList=serviceOrderManager.findOtherMiscellaneousList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list1=serviceOrderManager.findPackingSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list2=serviceOrderManager.findUnpackingSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list3=serviceOrderManager.findContainerSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			if(!list1.contains(null)){
				packingSum=list1.get(0).toString();
			}else{
				packingSum="None";
			}
			if(!list3.contains(null)){
				packContSum=list3.get(0).toString();
			}else{
				packContSum="None";
			}
			if(!list2.contains(null)){
				packUnpackSum=list2.get(0).toString();
			}else{
				packUnpackSum="None";
			}
			List list4=serviceOrderManager.findValuationSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list5=serviceOrderManager.findTransportationSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			List list6=serviceOrderManager.findOthersSumList(serviceOrder.getSequenceNumber(),serviceOrder.getShipNumber());
			if(!list4.contains(null))
				{
				valuationSum=list4.get(0).toString();
				}else{
					valuationSum="None";
				}
			if(!list5.contains(null)){
				transportSum=list5.get(0).toString();
			}else{
				transportSum="None";
			}
			if(!list6.contains(null)){
				othersSum=list6.get(0).toString();
			}else{
				othersSum="None";
			}
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
         
		return SUCCESS;
	}
	
	public Long getAid() {
		return aid;
	}

	public void setAid(Long aid) {
		this.aid = aid;
	}

	public String getFieldNames() {
		return fieldNames;
	}

	public void setFieldNames(String fieldNames) {
		this.fieldNames = fieldNames;
	}

	private Long aid;
	private String fieldNames;
	private List tooltipList;
	public List getTooltipList() {
		return tooltipList;
	}

	public void setTooltipList(List tooltipList) {
		this.tooltipList = tooltipList;
	}

	@SkipValidation
	public String findToolTipForExpense(){
		try{
		tooltipList=accountLineManager.findToolTipForExpense(aid,fieldNames);
		}catch(Exception e){
	    	 e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String findToolTipForRevenue(){
		multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString();
		Date statusDate = accountLineManager.get(aid).getStatusDate();
		tooltipList=accountLineManager.findToolTipForRevenue(aid,fieldNames, multiCurrency, statusDate);
		return SUCCESS;
	}
	// variable for partner detail tool tip 
	private String lName;
	private String billingAdd1;
	private String billingAdd2;
	private String billingAdd3;
	private String billingAdd4;
	private String billingCity;
	private String billingState;
	private String billingCountry;
	private String billingPhone;
	private String billingFax;
	private String billingZip;
	
	@SkipValidation
	public String findToolTipPartnerDetail(){
		tooltipList=accountLineManager.findToolTipPartnerDetail(sessionCorpID,partnerCode); 
		try{
			if(!tooltipList.isEmpty())
			{
				ListIterator listIterator = tooltipList.listIterator();				
				while(listIterator.hasNext()) {
					Object []row= (Object [])listIterator.next();
					if(row[0]==null){
						lName="";
					}else {
						lName=(String)row[0].toString();
					}
					if(row[1]==null) {
						billingCountry="";
					} else {
						billingCountry=(String)row[1].toString();
					}
					
					if(row[2]==null) {
						billingCity="";
					}else {
						billingCity=(String)row[2].toString();
					}
			    	if(row[3]==null) {
			    		billingState="";
					}else {
						billingState=(String)row[3].toString();
					}
					if(row[4]==null) {
						billingAdd1="";
					}else {
						billingAdd1=(String)row[4].toString();
					}
					if(row[5]==null) {
						billingAdd2="";
					}else {
						billingAdd2=(String)row[5].toString();
					}
					if(row[6]==null) {
						billingAdd3="";
					}else {
						billingAdd3=(String)row[6].toString();
					}
					if(row[7]==null) {
						billingAdd4="";
					}else {
						billingAdd4=(String)row[7].toString();
					}
					if(row[8]==null) {
						billingZip="";
					}else {
						billingZip=(String)row[8].toString();
					}
					if(row[9]==null) {
						billingPhone="";
					}else {
						billingPhone=(String)row[9].toString();
					}
					if(row[10]==null) {
						billingFax="";
					}else {
						billingFax=(String)row[10].toString();
					}
					
		       }
			} else{
			}
			}
			catch(Exception e) {
		    	 e.printStackTrace();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	
		    	 return "errorlog";
				
			}
		return SUCCESS;
	}
	@SkipValidation
	public String emailInvoice(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			emailInvoiceList = accountLineManager.getEmailInvoiceList(fromDate, toDate, sessionCorpID);
			Iterator it = emailInvoiceList.iterator();
			while (it.hasNext()) {
				DTO  invoiceList = (DTO)it.next();
				String emailTo = invoiceList.getEmail().toString();
				String userID = invoiceList.getCustomerPortalId().toString();
				String emailFrom = invoiceList.getPersonBilling().toString();
				
				try {
					String subject = "Storage Invoice Information";
					
					String msgText1 ="";
					
					msgText1 = msgText1 + "Dear Customer,";
					msgText1 = msgText1 + "\n\nYour current storage invoice is now available for viewing at our customer portal, please access the portal by clicking on http://"+sessionCorpID.toLowerCase()+".skyrelo.com and accessing it by using your ID: " + userID;
					msgText1 = msgText1 + "\n\nWe had earlier sent you the password, in case you have forgotten the password, please click on the Forgot Password link on the portal and reset your password";
					msgText1 = msgText1 + "\n\nWe will be charging your credit card with the invoice amount before the 15th of the month.";
					msgText1 = msgText1 + "\n\nBest regards,";
					msgText1 = msgText1 + "\n\nFinance Department" ;
					msgText1 = msgText1 + "\nSecurity Storage Company";
					msgText1 = msgText1 + "\n1701 Florida Avenue, NW";
					msgText1 = msgText1 + "\nWashington, DC 20009";
					
					String tempRecipient="";
					String tempRecipientArr[]=emailTo.split(",");
					for(String str:tempRecipientArr){
						if(!userManager.doNotEmailFlag(str).equalsIgnoreCase("YES")){
							if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
							tempRecipient += str;
						}
					}
					emailTo=tempRecipient;
					emailSetupManager.globalEmailSetupProcess(emailFrom, emailTo, "", "", "", msgText1, subject, sessionCorpID,"",invoiceList.getShipNumber().toString(),"");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			hitflag = "1";
		} catch (Exception e) {
		  	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String findToolTipExpRevenue(){
		try{
		tooltipList=accountLineManager.findToolTipExpRevenue(aid,fieldNames);
		}catch(Exception e)
		{
			 e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/* Added By kunal for ticket number: 6926 (Missing Invoice)*/
	
	@SkipValidation
	public String   missingInvoiceExtracts()
	{  
		return SUCCESS;
	}
	
@SkipValidation
public void missingInvoiceExtract() throws Exception, EOFException{
	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
    StringBuilder beginDates = new StringBuilder(formats.format( beginDate ));
    StringBuilder endDates = new StringBuilder(formats.format( endDate ));

	String []columnList = new String[]{"Invoice #","Revenue","SO#"};
	SimpleDateFormat format = new SimpleDateFormat("MMddyyyy");
    StringBuilder systemDate = new StringBuilder(format.format(new Date())); 
    MissedInvoiceDTO missedInvoice = null;
    List<MissedInvoiceDTO> mainList = new ArrayList<MissedInvoiceDTO>();
    try{
	    	List groupList=accountLineManager.missingInvoices(beginDates.toString(), endDates.toString(), sessionCorpID);
	    	List missingInvoices = (List)groupList.get(1);
	    	List invoiceList = (List)groupList.get(0);
	    	int sequence = 0;
	    	for(int seq = 0; seq < missingInvoices.size(); seq++) {
	    		int min = Integer.parseInt(((Object[]) missingInvoices.get(seq))[0].toString());
	    		int max = (seq != missingInvoices.size()-1)?Integer.parseInt(((Object[]) missingInvoices.get(seq+1))[0].toString()):seq;
	    		for(int invoiceNumber = min; invoiceNumber <= max; invoiceNumber++){
		    		if(invoiceList.contains(String.valueOf(invoiceNumber))){
		    			Object [] row = (Object[]) missingInvoices.get(sequence);
		    			if(row[0].toString().trim().equals(String.valueOf(invoiceNumber))){
			    			missedInvoice = new MissedInvoiceDTO();
				 			missedInvoice.setInvoice((row[0] != null)?row[0].toString():"");
				 			missedInvoice.setRevenue((row[1] != null)?row[1].toString():"");
				 			missedInvoice.setSoNumber((row[2] != null)?row[2].toString():"");
				 			sequence++;
				 			mainList.add(missedInvoice);
		    			}
		    		}else{
		    			missedInvoice = new MissedInvoiceDTO(); 
			 			missedInvoice.setInvoice(String.valueOf(invoiceNumber));
			 			missedInvoice.setRevenue("Missed Invoice");
			 			missedInvoice.setSoNumber("");
			 			mainList.add(missedInvoice);
		    		}
	    		}
		}
	    	
	    Collections.sort(mainList);
	 	ExcelCreator ec = ExcelCreator.getExcelCreator();
	 	ec.setHeaderRowBackgroundColor(ExcelCreatorColor.AQUA);
	 	ec.setConditionalDataAndIndex("Missed Invoice",1);
	 	ec.rowBeforeHeader(new String[]{"From "+beginDates+" To "+endDates}, true, true);
	 	ec.mergeCells(0, 0, columnList.length-1);
	 	ec.createExcelForAttachment(getResponse(), "Missed_Invoice-From#"+beginDates+"#To#"+endDates,columnList , mainList, "Missed_Invoice", ",");
    } catch(Exception e){
   	 e.printStackTrace();	
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    
	}
 
}
	
	
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	private String saveCopyFlag="NO";
	private AccountLine addCopyAccountLine;
	public String addWithCopyAccountLine()throws Exception{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			saveCopyFlag="YES";
			String st=editAfterSubmitPartnerType();
					if(st.equalsIgnoreCase("SUCCESS") || st.equalsIgnoreCase("category")){
				getComboList(sessionCorpID);
				serviceOrder = serviceOrderManager.get(sid);
				billing = billingManager.get(sid);
			    trackingStatus = trackingStatusManager.get(sid);
				accountLine = new AccountLine();
				//addCopyAccountLine = (AccountLine)accountLineManager.get(id);
					try{
						BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
						beanUtils.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
						beanUtils.copyProperties(accountLine, addCopyAccountLine);
					}catch(Exception ex){ex.printStackTrace();}
					try{
					List chargeid=chargesManager.findChargeId(accountLine.getChargeCode(), billing.getContract());
				    if((chargeid!=null)&&(!chargeid.isEmpty())&&(chargeid.get(0)!=null)&&(!chargeid.get(0).toString().equalsIgnoreCase(""))){
				    	Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
				    	if(charge.getPrintOnInvoice()!=null && charge.getPrintOnInvoice()){
				    		accountLine.setIgnoreForBilling(true);
				    	}else{
				    		accountLine.setIgnoreForBilling(false);	
				    	}
				    	if(!costElementFlag){
							accountLine.setRecGl(charge.getGl());
							accountLine.setPayGl(charge.getExpGl());
						}else{
							String chargeStr="";
							List glList = accountLineManager.findChargeDetailFromSO(billing.getContract(),accountLine.getChargeCode(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
							if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
								  chargeStr= glList.get(0).toString();
							  }
							   if(!chargeStr.equalsIgnoreCase("")){
								  String [] chrageDetailArr = chargeStr.split("#");
								  accountLine.setRecGl(chrageDetailArr[1]);
								  accountLine.setPayGl(chrageDetailArr[2]);
								}else{
								  accountLine.setRecGl("");
								  accountLine.setPayGl("");
							  }
						}
				    }else{
				    	accountLine.setRecGl("");
						accountLine.setPayGl("");
						accountLine.setChargeCode("");
				    }
					}catch(Exception ex){ex.printStackTrace();}
					accountLine.setCreatedBy(getRequest().getRemoteUser());
					accountLine.setCreatedOn(new Date());
					accountLine.setUpdatedBy(getRequest().getRemoteUser());
					accountLine.setUpdatedOn(new Date());
					accountLine.setCorpID(sessionCorpID);
					
					//Code for Recievable Section
					accountLine.setInvoiceCreatedBy(""); 
					accountLine.setRecInvoiceNumber("");
					accountLine.setExternalReference("");
					accountLine.setPaymentStatus("");
					accountLine.setReceivedAmount(null);
					accountLine.setStatusDate(null);
					accountLine.setSendEstToClient(null);
					accountLine.setSendActualToClient(null);
					accountLine.setDoNotSendtoClient(null);
				//	accountLine.setRecGl("");
					accountLine.setAccrueRevenue(null);
					accountLine.setAccrueRevenueManual(false);
					accountLine.setAccrueRevenueReverse(null);
					accountLine.setRecPostDate(null);
					accountLine.setRecAccDate(null);
					accountLine.setRecXfer("");
					accountLine.setRecXferUser("");
					accountLine.setReceivedInvoiceDate(null);
					//Code for payable Section
					accountLine.setPayPayableStatus("");
					accountLine.setPayPayableDate(null);
					accountLine.setPayPayableVia("");
					accountLine.setPayPayableAmount(null);
					accountLine.setInvoiceNumber("");
				//	accountLine.setPayGl(null);
					accountLine.setPayPostDate(null);
					accountLine.setAccruePayable(null);
					accountLine.setAccruePayableManual(false);
					accountLine.setAccruePayableReverse(null);
					accountLine.setPayAccDate(null);
					accountLine.setPayXfer("");
					accountLine.setPayXferUser("");
					accountLine.setSettledDate(null);
					accountLine.setId(null);
					accountLine.setRecRate(new BigDecimal("0.00"));
					accountLine.setLocalAmount(new BigDecimal("0.00"));
					maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
			        if ( maxLineNumber.get(0) == null ) {          
			         	accountLineNumber = "001";
			         }else {
			         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
			             if((autoLineNumber.toString()).length() == 2) {
			             	accountLineNumber = "0"+(autoLineNumber.toString());
			             }
			             else if((autoLineNumber.toString()).length() == 1) {
			             	accountLineNumber = "00"+(autoLineNumber.toString());
			             } 
			             else {
			             	accountLineNumber=autoLineNumber.toString();
			             }
			         }
			        accountLine.setAccountLineNumber(accountLineNumber); 
					accountLine.setShipNumber(serviceOrder.getShipNumber());
					
					accountLine.setActualExpense(new BigDecimal("0.00"));
					accountLine.setActualRevenue(new BigDecimal("0.00"));
					if(accountLine.getVendorCode()!=null && !accountLine.getVendorCode().equals(""))
					{
						myfileDocumentList=accountLineManager.getDocumentList(sessionCorpID,accountLine.getVendorCode(),accountLine.getShipNumber());
					}
					
/*    		if(accountLine.getCountry() !=null && (!(accountLine.getCountry().toString().equals("")))){
						accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,accountLine.getCountry());	
						if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
							try{
								accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));		
							}catch(Exception e){
								accountLine.setExchangeRate(new Double(1.0));		
							}
						}else{
							accountLine.setExchangeRate(new Double(1.0));
						}
					}
					accountLine.setValueDate(new Date());
					if(accountLine.getLocalAmount()!=null && accountLine.getLocalAmount().doubleValue()!=0)	{
								try{ 					
								 Double f1= (accountLine.getLocalAmount().doubleValue() /((accountLine.getExchangeRate())));
					      		 f1=f1*100/100;
								 DecimalFormat df = new DecimalFormat("#.##");
								 String revAmt = df.format(f1);
								 accountLine.setActualExpense(new BigDecimal(revAmt));
								 }catch (Exception e) {						 
								} 
						}else{					
						}
						 
					if(accountLine.getEstCurrency() !=null && (!(accountLine.getEstCurrency().toString().equals("")))){
				    		accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,accountLine.getEstCurrency());	
				    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
				    				try{
				    					accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));		
				    				}catch(Exception e){
				    					accountLine.setEstExchangeRate(new BigDecimal(1.0));		
				    				}
				    			}else{
				    				accountLine.setEstExchangeRate(new BigDecimal(1.0));
				    			}
				    		}
				    accountLine.setEstValueDate(new Date());
				    if(accountLine.getEstLocalAmount()!=null && accountLine.getEstLocalAmount().doubleValue()!=0)	{
				    		try{ 					
								 Double f1= (accountLine.getEstLocalAmount().doubleValue() /((accountLine.getEstExchangeRate().doubleValue())));
					      		 f1=f1*100/100;
								 DecimalFormat df = new DecimalFormat("#.##");
								 String revAmt = df.format(f1);
								 accountLine.setEstimateExpense(new BigDecimal(revAmt));
								
								 if(accountLine.getEstimateRevenueAmount()!=null && accountLine.getEstimateRevenueAmount().doubleValue()!=0)	{
								 Double f2= ((accountLine.getEstimateRevenueAmount().doubleValue() /(Double.valueOf(revAmt.trim()).doubleValue())) * 100);
								 int result=(int)Math.round(f2);
								 accountLine.setEstimatePassPercentage(result);
								 }
								
								 if(accountLine.getEstLocalRate()!=null && accountLine.getEstLocalRate().doubleValue()!=0)	{
									 Double f2= (accountLine.getEstLocalRate().doubleValue() /((accountLine.getEstExchangeRate().doubleValue())));
									 DecimalFormat df3 = new DecimalFormat("#.##");
									 String revAmt1 = df3.format(f2);
									 accountLine.setEstimateRate(new BigDecimal(revAmt1));								 				
								    }
								 }catch (Exception e) {						 
								}
				    		}
					if(accountLine.getRevisionCurrency() !=null && (!(accountLine.getRevisionCurrency().toString().equals("")))){
							    		accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,accountLine.getRevisionCurrency());	
							    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
							    				try{
							    					accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));		
							    				}catch(Exception e){
							    					accountLine.setRevisionExchangeRate(new BigDecimal(1.0));		
							    				}
							    			}else{
							    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0));
							    			}
							    		}
				    accountLine.setRevisionValueDate(new Date());
							if(accountLine.getRevisionLocalAmount()!=null && accountLine.getRevisionLocalAmount().doubleValue()!=0)	{	    		
											try{ 					
											 Double f1= (accountLine.getRevisionLocalAmount().doubleValue() /((accountLine.getRevisionExchangeRate().doubleValue())));
								      		 f1=f1*100/100;
											 DecimalFormat df = new DecimalFormat("#.##");
											 String revAmt5 = df.format(f1);
											 accountLine.setRevisionExpense(new BigDecimal(revAmt5));
											 
							if(accountLine.getRevisionRevenueAmount()!=null && accountLine.getRevisionRevenueAmount().doubleValue()!=0)	{
											 Double f2= ((accountLine.getRevisionRevenueAmount().doubleValue() /(Double.valueOf(revAmt5.trim()).doubleValue())) * 100);
											 int result=(int)Math.round(f2);
											 accountLine.setRevisionPassPercentage(result);
												 }
							if(accountLine.getRevisionLocalRate()!=null && accountLine.getRevisionLocalRate().doubleValue()!=0)	{
											 Double f2= (accountLine.getRevisionLocalRate().doubleValue() /((accountLine.getRevisionExchangeRate().doubleValue())));
											 DecimalFormat df3 = new DecimalFormat("#.##");
											 String revAmt1 = df3.format(f2);
											 accountLine.setRevisionRate(new BigDecimal(revAmt1));
											 }
											 }catch (Exception e) {						 
											}
									}*/
				    
							logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
					}else{
						return st;
					}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
					return SUCCESS;
			}

	private String accEstimate;
	@SkipValidation
	public String updateAccountLineField(){
		accountLineManager.updateAccountLineField(aid,accEstimate,sessionCorpID);
		return SUCCESS;
	}
	private String field;
	private String type;
	private String checkDmm;
	@SkipValidation
	public String updateAccline(){
		try {
			accountLineManager.updateAccline(aid,field,type,sessionCorpID);
			if((checkDmm!=null)&&(checkDmm.equalsIgnoreCase("Y"))){
				AccountLine currentAccountLine=accountLineManager.get(aid);
				TrackingStatus currentTrackingStatus=trackingStatusManager.get(currentAccountLine.getServiceOrderId());
			 	String networkSynchedId =	accountLineManager.getNetworkSynchedIdCreate(currentAccountLine.getId(),sessionCorpID, currentTrackingStatus.getNetworkPartnerCode());
			  	if(!(networkSynchedId.equals(""))){
			  		AccountLine networkSynchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
			  		networkSynchornizeDMMAccountLine(networkSynchedAccountLine,currentAccountLine); 
			  	} 
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	@SkipValidation
    public void networkSynchornizeDMMAccountLine(AccountLine synchedAccountLine,AccountLine  accountLine ) {
		try {
			List bookingAgentCodeList=companyDivisionManager.getBookingAgentCode(accountLine.getCompanyDivision(),sessionCorpID);
			if(bookingAgentCodeList!=null && (!(bookingAgentCodeList.isEmpty())) && bookingAgentCodeList.get(0)!=null && (!(bookingAgentCodeList.get(0).toString().trim().equals(""))))
			{
				synchedAccountLine.setVendorCode(bookingAgentCodeList.get(0).toString());
				List bookingAgentNamelist=	serviceOrderManager.findByBooking(bookingAgentCodeList.get(0).toString(), sessionCorpID);
			    
				if(bookingAgentNamelist!=null && (!(bookingAgentNamelist.isEmpty())) && bookingAgentNamelist.get(0)!=null && (!(bookingAgentNamelist.get(0).toString().trim().equals("")))){
					synchedAccountLine.setEstimateVendorName(bookingAgentNamelist.get(0).toString());
			    }
				
			}
			String actCode="";
			String  companyDivisionAcctgCodeUnique1="";
			 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(synchedAccountLine.getCorpID());
			 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
				 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
			 }
      if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
				actCode=partnerManager.getAccountCrossReference(synchedAccountLine.getVendorCode(),synchedAccountLine.getCompanyDivision(),synchedAccountLine.getCorpID());
			}else{
				actCode=partnerManager.getAccountCrossReferenceUnique(synchedAccountLine.getVendorCode(),synchedAccountLine.getCorpID());
			}
      synchedAccountLine.setActgCode(actCode);
      synchedAccountLine.setAccountLineNumber(accountLine.getAccountLineNumber());
      accountLineManager.save(synchedAccountLine);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		}
	}

	@SkipValidation
	public String driverCommissionCalculation(){
		 try {
			billing=billingManager.get(sid);
			 String billingContract=billing.getContract();
			 Miscellaneous miscellaneousTemp=null;
			 BigDecimal actualLineHaul = new BigDecimal("0.00");
			try{ 
				miscellaneousTemp=miscellaneousManager.get(sid);
				if(miscellaneousTemp.getActualLineHaul()!=null){
					actualLineHaul=miscellaneousTemp.getActualLineHaul();
				}
			}catch(Exception e){
				miscellaneousTemp=null;
				e.printStackTrace();
			}
			List al=accountLineManager.getDriverCommission(actualRevenue,chargeCode,vendorCode,billingContract,accountLineDistribution,accountEstimateRevenueAmount,accountRevisionRevenueAmount,sessionCorpID,actualLineHaul);
			driverCommission="";
			if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
				driverCommission=al.get(0).toString();
			}
		} catch (Exception e) {
	    	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

	    	 return "errorlog";
		}
		return SUCCESS;
	}
	
	@SkipValidation 
	public String checkZeroforInvoice()
	{
		try{
		checkZeroforInvoice=accountLineManager.checkZeroforInvoice(sid,invoiceBillToCode);
		}
		catch(NullPointerException np){
			np.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	private String accountLineCategory = "";
	private String comptetive = "";
	private String bulkComputeValue;
	   
	   Double estimateQuantity=0.00;
	   Double estimateContractRate=0.00;
	   Double estimateContractRateAmmount=0.00;
	   Double typeConversion=0.00;
	   String estimateContractCurrency="";
	   String estimatePayableContractCurrency="";
	   Double estimateContractExchangeRate=1.000;
	   Double estimatePayableContractExchangeRate=1.000;
	   Double estExchangeRate=1.000;
	   String estSellCurrency="";
	   String estCurrency="";
	   Double estSellExchangeRate=1.000;
	   Double estimatePayableContractRateAmmount=0.00;
	   Double estimateRevenueAmount=0.00;
	   Double estimateSellRate=0.00;
	   Double estSellLocalAmount=0.00;
	   Double estSellLocalRate=0.00;
	   Double estimateRate=0.00;
	   Double estimatePayableContractRate=0.00;
	   Double estimateExpense=0.00;
	   String contractCurrency="";
	   String recRateCurrencyValue="";
	   Double contractExchangeRate=0.00;
	   Double recRateExchange=0.00;
	   Double recQuantity=0.00;
	   Double contractRate=0.00;
	   Double contractRateAmmount=0.00;
	   Double actualRevenueValue=0.00;
	   Double recRate=0.00;
	   Double actualRevenueForeign=0.00;
	   Double recCurrencyRate=0.00;
	   Double recVatAmt=0.00;
	   Double estVatAmtValue=0.00;
	   Double estExpVatAmt=0.00;
	   String payableContractCurrency="";
	   String countryValue="";
	   Double payableContractExchangeRate=0.00;
	   Double exchangeRate=0.00;
	   Double payableContractRateAmmount=0.00;
	   Double actualExpenseValue=0.00;
	   Double localAmountValue=0.00;
	   Double payVatAmtValue=0.00;
	   String revisionContractCurrency="";
	   String revisionPayableContractCurrency="";
	   String revisionSellCurrency="";
	   String revisionCurrency="";
	   Double revisionContractExchangeRate=0.00;
	   Double revisionSellExchangeRate=0.00;
	   Double revisionPayableContractExchangeRate=0.00;
	   Double revisionExchangeRate=0.00;
	   Double revisionQuantity=0.00;
	   Double revisionContractRate=0.00;
	   Double revisionContractRateAmmount=0.00;
	   Double revisionPayableContractRateAmmount=0.00;
	   Double revisionPayableContractRate=0.00;
	   Double revisionRevenueAmount=0.00;
	   Double revisionSellRate=0.00;
	   Double revisionSellLocalAmount=0.00;
	   Double revisionSellLocalRate=0.00;
	   Double revisionRate=0.00;
	   Double revisionExpense=0.00;
	   Double revisionVatAmtValue=0.00;
	   Double revisionExpVatAmt=0.00;
	   HashMap dynamicSetQuery = new HashMap();
	
	   @SkipValidation
	 public String bulkComputeCalulation(){
	  systemDefaultVatCalculation =accountLineManager.findVatCalculation(sessionCorpID).get(0).toString();
	  multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString();
	  currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
	  String strAcc[] = bulkComputeValue.split("~@");
	  billing= billingManager.get(sid);
	  shipNum = billing.getShipNumber();
	  serviceOrder=serviceOrderManager.get(sid);
	  billingContract = billing.getContract();
	  trackingStatus = trackingStatusManager.get(sid);
	  try{
	  if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
	   contractTypeValue= accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
	   if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
	    contractType=true; 
	   }
	  }
	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  for (String str : strAcc) {
	   String str1[] = str.split("@");
	   dynamicSetQuery = new HashMap();
	   accountLine = accountLineManager.get(Long.parseLong(str1[14].toString()));
	   estimateQuantity=0.00;
	   estimateContractRate=0.00;
	   estimateContractRateAmmount=0.00;
	   typeConversion=0.00;
	   estimateContractCurrency="";
	   estimatePayableContractCurrency="";
	   estimateContractExchangeRate=1.000;
	   estimatePayableContractExchangeRate=1.000;
	   estExchangeRate=1.000;
	   estSellCurrency="";
	   estCurrency="";
	   estSellExchangeRate=1.000;
	   estimatePayableContractRateAmmount=0.00;
	   estimateRevenueAmount=0.00;
	   estimateSellRate=0.00;
	   estSellLocalAmount=0.00;
	   estSellLocalRate=0.00;
	   estimateRate=0.00;
	   estimatePayableContractRate=0.00;
	   estimateExpense=0.00;
	   contractCurrency="";
	   recRateCurrencyValue="";
	   contractExchangeRate=0.00;
	   recRateExchange=0.00;
	   recQuantity=0.00;
	   contractRate=0.00;
	   contractRateAmmount=0.00;
	   actualRevenueValue=0.00;
	   recRate=0.00;
	   actualRevenueForeign=0.00;
	   recCurrencyRate=0.00;
	   recVatAmt=0.00;
	   estVatAmtValue=0.00;
	   estExpVatAmt=0.00;
	   payableContractCurrency="";
	   countryValue="";
	   payableContractExchangeRate=0.00;
	   exchangeRate=0.00;
	   payableContractRateAmmount=0.00;
	   actualExpenseValue=0.00;
	   localAmountValue=0.00;
	   payVatAmtValue=0.00;
	   revisionContractCurrency="";
	   revisionPayableContractCurrency="";
	   revisionSellCurrency="";
	   revisionCurrency="";
	   revisionContractExchangeRate=0.00;
	   revisionSellExchangeRate=0.00;
	   revisionPayableContractExchangeRate=0.00;
	   revisionExchangeRate=0.00;
	   revisionQuantity=0.00;
	   revisionContractRate=0.00;
	   revisionContractRateAmmount=0.00;
	   revisionPayableContractRateAmmount=0.00;
	   revisionPayableContractRate=0.00;
	   revisionRevenueAmount=0.00;
	   revisionSellRate=0.00;
	   revisionSellLocalAmount=0.00;
	   revisionSellLocalRate=0.00;
	   revisionRate=0.00;
	   revisionExpense=0.00;
	   revisionVatAmtValue=0.00;
	   revisionExpVatAmt=0.00;
	   if(str1[6] != null && str1[6] != ""){
		   charge = str1[6].toString();
		   }else{
		    charge="";
		   }
		   if(str1[8] != null && str1[8] != ""){
		   accountCompanyDivision = str1[8].toString();
		   }else{
		    
		   } 
		   if(str1[11] != null && str1[11] != ""){
		   comptetive = str1[11].toString();
		   }else{
		    
		   }
	   
	   
	   if(str1[0].toString().equalsIgnoreCase("computeEntitle")){
		   try{
			   if(accountLine.getCategory()==null || accountLine.getCategory().trim().equalsIgnoreCase("Internal Cost")){ 
				    
			   }else{
				   
				   revisedEntitleCharge();
			
				   double rateEstimate=0.00;
				   double entitlementAmount=0.00;
			     
			       String  res[] = revisedChargeValue.split("#");
			       if(res[3]!= null && (res[3].equalsIgnoreCase("AskUser1"))){ 
			       }
			       if(res[3]!= null && (res[3].equalsIgnoreCase("Preset0"))){
			    	   if(res[0] != null && (!(res[0].trim().equalsIgnoreCase("")))){	
			    		   entitlementAmount=Double.parseDouble(res[0].toString());
			    	   }
			       }
			       if(res[3]!= null && (res[3].equalsIgnoreCase("FromField0"))){
			    	   if(res[1] != null && (!(res[1].trim().equalsIgnoreCase("")))){	
			    		   entitlementAmount=Double.parseDouble(res[1].toString());
			    	   }
			       }
			       if(res[5]!= null && (res[5].equalsIgnoreCase("AskUser1"))){
			    		
			    	}else{
			    	if(res[10] != null && (!(res[10].trim().equalsIgnoreCase("")))){	
			    		rateEstimate = Double.parseDouble(res[10].toString()); 
			    	}
			    	}
			       if(res[5]!= null && (res[5].equalsIgnoreCase("RateGrid"))){
			    	   if(res[8] != null && (!(res[8].trim().equalsIgnoreCase("")))){	
				    		rateEstimate = Double.parseDouble(res[8].toString()); 
				    	} 
			       }
			       entitlementAmount=entitlementAmount*rateEstimate;
			       String type = res[4];
				   if(res[6]!=null &&  (!(res[6].toString().equalsIgnoreCase("")))) 	 {
		           typeConversion=Double.parseDouble(res[6].toString()); 
				   }
		           if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
		        	   if(typeConversion==null ||  typeConversion==0.00) 	 {
		        		  
		        	   }else{
		        		   entitlementAmount=entitlementAmount/typeConversion;
		        	   }
		           }
		           if(type!=null && type.toString().equalsIgnoreCase("Multiply"))  {
		        	   entitlementAmount=entitlementAmount*typeConversion; 
		           }
		           dynamicSetQuery.put("entitlementAmount",entitlementAmount); 
		           dynamicSetQuery.put("basis",res[7].toString());
		           if(dynamicSetQuery.containsKey("basis")){
		            	accountLine.setBasis(dynamicSetQuery.get("basis").toString()); 
		            }
		            if(dynamicSetQuery.containsKey("entitlementAmount")){
		            	accountLine.setEntitlementAmount(new BigDecimal(dynamicSetQuery.get("entitlementAmount").toString())); 
		            }
			   }
		   }catch(Exception e){
			   e.printStackTrace();	  
			  }
	   }
	   
	   if(str1[1].toString().equalsIgnoreCase("computeEstimate")){
		  try{
			  if(accountLine.getCategory()==null || accountLine.getCategory().trim().equalsIgnoreCase("Internal Cost")){ 
			    
		   }else{
			   revisedEstimateCharge();
			
		       String  res[] = revisedChargeValue.split("#");
		    if(contractType){
		    	estimateContractCurrency=accountLine.getEstimateContractCurrency();
		    	if(res[18]!=null && (!(res[18].toString().trim().equals("")))){
		    		estimateContractCurrency=res[18].toString().trim();
		    	}
		    	estimatePayableContractCurrency=accountLine.getEstimatePayableContractCurrency();
		    	if(res[19]!=null && (!(res[19].toString().trim().equals("")))){
		    		estimatePayableContractCurrency=res[19].toString().trim();
		    	}
		    	estimateContractExchangeRate= Double.parseDouble(accountLine.getEstimateContractExchangeRate().toString());
		    	estSellExchangeRate=Double.parseDouble(accountLine.getEstSellExchangeRate().toString());
		    	estimatePayableContractExchangeRate=Double.parseDouble(accountLine.getEstimatePayableContractExchangeRate().toString());
		    	estExchangeRate=Double.parseDouble(accountLine.getEstExchangeRate().toString());
		    	
		    	if(currencyExchangeRate.containsKey(estimateContractCurrency)){
    				estimateContractExchangeRate=Double.parseDouble(currencyExchangeRate.get(estimateContractCurrency)); 
    				dynamicSetQuery.put("estimateContractCurrency",estimateContractCurrency);
    				dynamicSetQuery.put("estimateContractExchangeRate",estimateContractExchangeRate);  
    				dynamicSetQuery.put("estimateContractValueDate",new Date());
    			}
    			estSellCurrency=accountLine.getEstSellCurrency();
    			if(currencyExchangeRate.containsKey(estSellCurrency)){
    				estSellExchangeRate=Double.parseDouble(currencyExchangeRate.get(estSellCurrency)); 
    				dynamicSetQuery.put("estSellCurrency",estSellCurrency);
    				dynamicSetQuery.put("estSellExchangeRate",estSellExchangeRate);  
    				dynamicSetQuery.put("estSellValueDate",new Date());
    			}
		    	if(currencyExchangeRate.containsKey(estimatePayableContractCurrency)){
		    			estimatePayableContractExchangeRate=Double.parseDouble(currencyExchangeRate.get(estimatePayableContractCurrency)); 
	    				dynamicSetQuery.put("estimatePayableContractCurrency",estimatePayableContractCurrency);
	    				dynamicSetQuery.put("estimatePayableContractExchangeRate",estimatePayableContractExchangeRate);  
	    				dynamicSetQuery.put("estimatePayableContractValueDate",new Date());
	    		}
		    	estCurrency=accountLine.getEstCurrency();
		    	if(currencyExchangeRate.containsKey(estCurrency)){
		    		estExchangeRate=Double.parseDouble(currencyExchangeRate.get(estCurrency)); 
    				dynamicSetQuery.put("estCurrency",estCurrency);
    				dynamicSetQuery.put("estExchangeRate",estExchangeRate);  
    				dynamicSetQuery.put("estValueDate",new Date());
    		   }
		    	if(res[3]!= null && (res[3].equalsIgnoreCase("AskUser1"))){
		    		
		    	}else{
		    	if(res[1] != null && (!(res[1].trim().equalsIgnoreCase("")))){	
		         estimateQuantity = Double.parseDouble(res[1].toString()); 
		    	}
		    	} 
		     if(res[1] == null || res[1].toString() == ""){
		      dynamicSetQuery.put("estimateQuantity", 0); 
		      dynamicSetQuery.put("estimateSellQuantity", 0); 
		     }else{
		      dynamicSetQuery.put("estimateQuantity",estimateQuantity);  
		      dynamicSetQuery.put("estimateSellQuantity", estimateQuantity); 
		     }
		     
		    
		     if(res[5]!=null && res[5].toString().equalsIgnoreCase("AskUser1")){
		     }else{
		      if(res[4] == null || res[4].toString().trim().equalsIgnoreCase("")){
		      dynamicSetQuery.put("estimateContractRate",0); 
		     }else{
		      estimateContractRate = Double.parseDouble(res[4].toString());
		      dynamicSetQuery.put("estimateContractRate",estimateContractRate);  
		     }
		      
		     } 
             if(res[5].toString().equalsIgnoreCase("BuildFormula")){
		      if(res[6] == null || res[6].toString().trim().equalsIgnoreCase("")){
		       dynamicSetQuery.put("estimateContractRate",0); 
		      }else{
		       estimateContractRate = Double.parseDouble(res[6].toString());
		       dynamicSetQuery.put("estimateContractRate",estimateContractRate);  
		      } 
		    } 
		   estimateContractRateAmmount = estimateContractRate*estimateQuantity;
		   
		   String type = res[8];
		   if(res[10]!=null &&  (!(res[10].toString().equalsIgnoreCase("")))) 	 {
           typeConversion=Double.parseDouble(res[10].toString()); 
		   }
           if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
        	   if(typeConversion==null ||  typeConversion==0.00) 	 {
        		  
        	   }else{
        		   estimateContractRateAmmount=estimateContractRateAmmount/typeConversion;
        	   }
           }
           if(type!=null && type.toString().equalsIgnoreCase("Multiply"))  {
        	   estimateContractRateAmmount=estimateContractRateAmmount*typeConversion; 
           } 
           dynamicSetQuery.put("estimateContractRateAmmount",estimateContractRateAmmount);
           dynamicSetQuery.put("basis",res[9].toString());
           String chargedeviation=res[15];
           if(accountLine.getEstimatePayableContractRateAmmount()!=null){
           estimatePayableContractRateAmmount=Double.parseDouble(accountLine.getEstimatePayableContractRateAmmount().toString());
           }
           if(chargedeviation!=null && (!(chargedeviation.trim().equals(""))) && (!(chargedeviation.trim().equalsIgnoreCase("NCT")))){
        	   dynamicSetQuery.put("buyDependSell",res[11]); 
               String  buyDependSellAccount=res[11];
               //double estimateRevenueAmount=0;
               double finalEstimateContractRate=0; 
               double finalEstimateExpenceAmount=0;
               double finalEstimateContractRateAmmount=0; 
               estimatePayableContractRateAmmount=0.00;
               double estimateSellRate=0; 
               double sellDeviation=0;
               double  estimatepasspercentage=0;
               double estimatepasspercentageRound=0;
               double buyDeviation=0;
               if(res[12]!=null && (!(res[12].toString().equals("")))){
               sellDeviation=Double.parseDouble(res[12].toString());
               }
               if(res[13]!=null && (!(res[13].toString().equals("")))){
               buyDeviation=Double.parseDouble(res[13].toString()); 
               }
        
               finalEstimateContractRateAmmount=Math.round((estimateContractRateAmmount*(sellDeviation/100))*10000)/10000;
               dynamicSetQuery.put("estimateContractRateAmmount",finalEstimateContractRateAmmount);
               finalEstimateContractRate=Math.round((estimateContractRate*(sellDeviation/100))*10000)/10000;
               dynamicSetQuery.put("estimateContractRate",finalEstimateContractRate);
                
               if(buyDependSellAccount=="Y"){
            	   estimatePayableContractRateAmmount= (finalEstimateContractRateAmmount*(buyDeviation/100));
            	   estimatePayableContractRate= (finalEstimateContractRate*(buyDeviation/100));
               }else{
            	   estimatePayableContractRateAmmount= (estimateContractRateAmmount*(buyDeviation/100));
            	   estimatePayableContractRate= (estimateContractRate*(buyDeviation/100));
               }
               
               dynamicSetQuery.put("estimateSellDeviation",sellDeviation);
               dynamicSetQuery.put("estimateDeviation",buyDeviation); 
              
               dynamicSetQuery.put("estimatePayableContractRateAmmount",estimatePayableContractRateAmmount);
               dynamicSetQuery.put("estimatePayableContractRate",estimatePayableContractRate);
               
        	   
           }else{

           dynamicSetQuery.put("estimateSellDeviation",0);
           dynamicSetQuery.put("estimateDeviation",0);
           
           double bayRate=0.00;
          if(res[17]!=null && (!(res[17].toString().trim().equals("")))){
           bayRate = Double.parseDouble(res[17].toString());
          }
           if(bayRate!=0.00 && bayRate !=0.0){
        	double estimateQuantityPay=0.00;
            estimateQuantityPay= estimateQuantity; 
            estimatePayableContractRateAmmount=estimateQuantityPay*bayRate;
           if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
        	   if(typeConversion==null ||  typeConversion==0.00) 	 {
        		  
        	   }else{
        		   estimatePayableContractRateAmmount=estimatePayableContractRateAmmount/typeConversion;
        	   }
           }
           if(type!=null && type.toString().equalsIgnoreCase("Multiply"))  {
        	   estimatePayableContractRateAmmount=estimatePayableContractRateAmmount*typeConversion; 
           }
           estimatePayableContractRate=bayRate;
           dynamicSetQuery.put("estimatePayableContractRate",bayRate);
           dynamicSetQuery.put("estimatePayableContractRateAmmount",estimatePayableContractRateAmmount); 
           }else{
        	 dynamicSetQuery.put("estimatePayableContractRate",bayRate);
             dynamicSetQuery.put("estimatePayableContractRateAmmount",estimatePayableContractRateAmmount);   
           }
           }
           
           estimateRevenueAmount=estimateContractRateAmmount/estimateContractExchangeRate;
           estimateSellRate = estimateContractRate/estimateContractExchangeRate;;
		   dynamicSetQuery.put("estimateRevenueAmount",estimateRevenueAmount); 
		   dynamicSetQuery.put("estimateSellRate",estimateSellRate);
		   
		   estSellLocalAmount = estimateRevenueAmount*estSellExchangeRate;
		   estSellLocalRate = estimateSellRate*estSellExchangeRate;
		   dynamicSetQuery.put("estSellLocalAmount",estSellLocalAmount); 
		   dynamicSetQuery.put("estSellLocalRate",estSellLocalRate);
		  
		   
		           
		            estimateRate = estimatePayableContractRate/estimatePayableContractExchangeRate; 
		            estimateExpense = estimatePayableContractRateAmmount/estimatePayableContractExchangeRate;
		            
		            dynamicSetQuery.put("estimateRate",estimateRate);
		            dynamicSetQuery.put("estimateExpense",estimateExpense);
		           
		            estLocalRate   = new BigDecimal(estimateRate*estExchangeRate);
		            estLocalAmount =  new BigDecimal(estimateExpense*estExchangeRate);
		            
		            dynamicSetQuery.put("estLocalRate",estLocalRate.setScale(2, BigDecimal.ROUND_HALF_UP)); 
		            dynamicSetQuery.put("estLocalAmount",estLocalAmount.setScale(2, BigDecimal.ROUND_HALF_UP));
		         
		            if(estimateExpense!=0.00){
		            Double estimatepasspercentage = ((estimateRevenueAmount/(estimateExpense))*100);
		            Integer estimatePassPercentageValue=estimatepasspercentage.intValue();
		            dynamicSetQuery.put("estimatepasspercentage",estimatePassPercentageValue);
		            }else{
		            	dynamicSetQuery.put("estimatepasspercentage",0);	
		            }
		            if(accountLine.getVATExclude()!=null && accountLine.getVATExclude()){
		            	
		            }else{
		            	if(systemDefaultVatCalculation.trim().equalsIgnoreCase("true")){
		            	Double recVatPercent=0.00;
						   if(accountLine.getRecVatPercent()!=null && (!(accountLine.getRecVatPercent().trim().equals("")))){
						   recVatPercent=Double.parseDouble(accountLine.getRecVatPercent());
						   estVatAmtValue=(estSellLocalAmount*recVatPercent)/100; 
						   }
						 Double payVatPercent=0.00;  
						 if(accountLine.getPayVatPercent()!=null && (!(accountLine.getPayVatPercent().trim().equals("")))){
							 payVatPercent=Double.parseDouble(accountLine.getPayVatPercent());
							 estExpVatAmt=((Double.parseDouble(estLocalAmount.toString()))*payVatPercent)/100; 
							   }
						 dynamicSetQuery.put("estVatAmt",estVatAmtValue);
				         dynamicSetQuery.put("estExpVatAmt",estExpVatAmt);
		            	}
		            }
		            
		            
		            if(dynamicSetQuery.containsKey("estimateContractCurrency")){
		            	accountLine.setEstimateContractCurrency(dynamicSetQuery.get("estimateContractCurrency").toString()); 
		            }
		            if(dynamicSetQuery.containsKey("estimateContractExchangeRate")){
		            	accountLine.setEstimateContractExchangeRate(new BigDecimal(dynamicSetQuery.get("estimateContractExchangeRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimateContractValueDate")){
		            	accountLine.setEstimateContractValueDate((Date)dynamicSetQuery.get("estimateContractValueDate")); 
		            }
		            if(dynamicSetQuery.containsKey("estSellCurrency")){
		            	accountLine.setEstSellCurrency(dynamicSetQuery.get("estSellCurrency").toString()); 
		            }
		            if(dynamicSetQuery.containsKey("estSellExchangeRate")){
		            	accountLine.setEstSellExchangeRate(new BigDecimal(dynamicSetQuery.get("estSellExchangeRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estSellValueDate")){
		            	accountLine.setEstSellValueDate((Date)dynamicSetQuery.get("estSellValueDate")); 
		            }
		            if(dynamicSetQuery.containsKey("estimatePayableContractCurrency")){
		            	accountLine.setEstimatePayableContractCurrency(dynamicSetQuery.get("estimatePayableContractCurrency").toString()); 
		            }
		            if(dynamicSetQuery.containsKey("estimatePayableContractExchangeRate")){
		            	accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(dynamicSetQuery.get("estimatePayableContractExchangeRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimatePayableContractValueDate")){
		            	accountLine.setEstimatePayableContractValueDate((Date)dynamicSetQuery.get("estimatePayableContractValueDate")); 
		            }
		            if(dynamicSetQuery.containsKey("estCurrency")){
		            	accountLine.setEstCurrency(dynamicSetQuery.get("estCurrency").toString()); 
		            }
		            if(dynamicSetQuery.containsKey("estExchangeRate")){
		            	accountLine.setEstExchangeRate(new BigDecimal(dynamicSetQuery.get("estExchangeRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estValueDate")){
		            	accountLine.setEstValueDate((Date)dynamicSetQuery.get("estValueDate")); 
		            }
		            if(dynamicSetQuery.containsKey("estimateQuantity")){
		            	accountLine.setEstimateQuantity(new BigDecimal(dynamicSetQuery.get("estimateQuantity").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimateSellQuantity")){
		            	accountLine.setEstimateSellQuantity(new BigDecimal(dynamicSetQuery.get("estimateSellQuantity").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimateContractRate")){
		            	accountLine.setEstimateContractRate(new BigDecimal(dynamicSetQuery.get("estimateContractRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimateContractRateAmmount")){
		            	accountLine.setEstimateContractRateAmmount(new BigDecimal(dynamicSetQuery.get("estimateContractRateAmmount").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("basis")){
		            	accountLine.setBasis(dynamicSetQuery.get("basis").toString()); 
		            }
		            if(dynamicSetQuery.containsKey("buyDependSell")){
		            	accountLine.setBuyDependSell(dynamicSetQuery.get("buyDependSell").toString()); 
		            }
		            if(dynamicSetQuery.containsKey("estimateSellDeviation")){
		            	accountLine.setEstimateSellDeviation(new BigDecimal(dynamicSetQuery.get("estimateSellDeviation").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimateDeviation")){
		            	accountLine.setEstimateDeviation(new BigDecimal(dynamicSetQuery.get("estimateDeviation").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimatePayableContractRateAmmount")){
		            	accountLine.setEstimatePayableContractRateAmmount(new BigDecimal(dynamicSetQuery.get("estimatePayableContractRateAmmount").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimatePayableContractRate")){
		            	accountLine.setEstimatePayableContractRate(new BigDecimal(dynamicSetQuery.get("estimatePayableContractRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimateRevenueAmount")){
		            	accountLine.setEstimateRevenueAmount(new BigDecimal(dynamicSetQuery.get("estimateRevenueAmount").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimateSellRate")){
		            	accountLine.setEstimateSellRate(new BigDecimal(dynamicSetQuery.get("estimateSellRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estSellLocalAmount")){
		            	accountLine.setEstSellLocalAmount(new BigDecimal(dynamicSetQuery.get("estSellLocalAmount").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estSellLocalRate")){
		            	accountLine.setEstSellLocalRate(new BigDecimal(dynamicSetQuery.get("estSellLocalRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimateRate")){
		            	accountLine.setEstimateRate(new BigDecimal(dynamicSetQuery.get("estimateRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimateExpense")){
		            	accountLine.setEstimateExpense(new BigDecimal(dynamicSetQuery.get("estimateExpense").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estLocalRate")){
		            	accountLine.setEstLocalRate(new BigDecimal(dynamicSetQuery.get("estLocalRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estLocalAmount")){
		            	accountLine.setEstLocalAmount(new BigDecimal(dynamicSetQuery.get("estLocalAmount").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estimatepasspercentage")){
		            	accountLine.setEstimatePassPercentage(Integer.parseInt((dynamicSetQuery.get("estimatepasspercentage").toString()))); 
		            }
		            if(dynamicSetQuery.containsKey("estVatAmt")){
		            	accountLine.setEstVatAmt(new BigDecimal(dynamicSetQuery.get("estVatAmt").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("estExpVatAmt")){
		            	accountLine.setEstExpVatAmt(new BigDecimal(dynamicSetQuery.get("estExpVatAmt").toString())); 
		            }
	   }else{

		   estimateSellRate=0.00;
		   estimateRate=0.00;
	    	estSellExchangeRate=Double.parseDouble(accountLine.getEstSellExchangeRate().toString());
	    	//estimatePayableContractExchangeRate=Double.parseDouble(accountLine.getEstimatePayableContractExchangeRate().toString());
	    	estExchangeRate=Double.parseDouble(accountLine.getEstExchangeRate().toString());
	    	
	    	
	    	if(res[3]!= null && (res[3].equalsIgnoreCase("AskUser1"))){
	    		
	    	}else{
	    	if(res[1] != null && (!(res[1].trim().equalsIgnoreCase("")))){	
	         estimateQuantity = Double.parseDouble(res[1].toString()); 
	    	}
	    	} 
	     if(res[1] == null || res[1].toString() == ""){
	      dynamicSetQuery.put("estimateQuantity", 0); 
	      
	     }else{
	      dynamicSetQuery.put("estimateQuantity",estimateQuantity);  
	   
	     }
	     
	    
	     if(res[5]!=null && res[5].toString().equalsIgnoreCase("AskUser1")){
	     }else{
	      if(res[4] == null || res[4].toString().trim().equalsIgnoreCase("")){
	      dynamicSetQuery.put("estimateSellRate",0); 
	     }else{
	    	 estimateSellRate = Double.parseDouble(res[4].toString());
	      dynamicSetQuery.put("estimateSellRate",estimateSellRate);  
	     }
	      
	     } 
        if(res[5].toString().equalsIgnoreCase("BuildFormula")){
	      if(res[6] == null || res[6].toString().trim().equalsIgnoreCase("")){
	       dynamicSetQuery.put("estimateSellRate",0); 
	      }else{
	    	  estimateSellRate = Double.parseDouble(res[6].toString());
	       dynamicSetQuery.put("estimateSellRate",estimateSellRate);  
	      } 
	    } 
        estimateRevenueAmount = estimateSellRate*estimateQuantity;
	   
	   String type = res[8];
	   if(res[10]!=null &&  (!(res[10].toString().equalsIgnoreCase("")))) 	 {
      typeConversion=Double.parseDouble(res[10].toString()); 
	   }
      if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
   	   if(typeConversion==null ||  typeConversion==0.00) 	 {
   		  
   	   }else{
   		estimateRevenueAmount=estimateRevenueAmount/typeConversion;
   	   }
      }
      if(type!=null && type.toString().equalsIgnoreCase("Multiply"))  {
    	  estimateRevenueAmount=estimateRevenueAmount*typeConversion; 
      } 
      dynamicSetQuery.put("estimateRevenueAmount",estimateRevenueAmount);
      dynamicSetQuery.put("basis",res[9].toString());
      String chargedeviation=res[15];
      if(accountLine.getEstimateExpense()!=null){
    	  estimateExpense=Double.parseDouble(accountLine.getEstimateExpense().toString());
      }
      if(chargedeviation!=null && (!(chargedeviation.trim().equals(""))) && (!(chargedeviation.trim().equalsIgnoreCase("NCT")))){
   	   dynamicSetQuery.put("buyDependSell",res[11]); 
          String  buyDependSellAccount=res[11];
          //double estimateRevenueAmount=0;
          double finalEstimateSellRate=0; 
          double finalEstimateExpenceAmount=0;
          double finalEstimateRevenueAmount=0; 
          estimateExpense=0.00;
          double estimateSellRate=0; 
          double sellDeviation=0;
          double  estimatepasspercentage=0;
          double estimatepasspercentageRound=0;
          double buyDeviation=0;
          if(res[12]!=null && (!(res[12].toString().equals("")))){
          sellDeviation=Double.parseDouble(res[12].toString());
          }
          if(res[13]!=null && (!(res[13].toString().equals("")))){
          buyDeviation=Double.parseDouble(res[13].toString()); 
          }
 
          finalEstimateRevenueAmount=Math.round((estimateRevenueAmount*(sellDeviation/100))*10000)/10000;
          dynamicSetQuery.put("estimateRevenueAmount",finalEstimateRevenueAmount);
          finalEstimateSellRate=Math.round((estimateSellRate*(sellDeviation/100))*10000)/10000;
          dynamicSetQuery.put("estimateSellRate",finalEstimateSellRate);
           
          if(buyDependSellAccount=="Y"){
        	  estimateExpense= (finalEstimateRevenueAmount*(buyDeviation/100));
        	  estimateRate= (finalEstimateSellRate*(buyDeviation/100));
          }else{
        	  estimateExpense= (estimateRevenueAmount*(buyDeviation/100));
        	  estimateRate= (estimateSellRate*(buyDeviation/100));
          }
          
          dynamicSetQuery.put("estimateSellDeviation",sellDeviation);
          dynamicSetQuery.put("estimateDeviation",buyDeviation); 
 
          dynamicSetQuery.put("estimateExpense",estimateExpense);
          dynamicSetQuery.put("estimateRate",estimateRate);
          
   	   
      }else{

      dynamicSetQuery.put("estimateSellDeviation",0);
      dynamicSetQuery.put("estimateDeviation",0);
      
      double bayRate=0.00;
     if(res[17]!=null && (!(res[17].toString().trim().equals("")))){
      bayRate = Double.parseDouble(res[17].toString());
     }
      if(bayRate!=0.00 && bayRate!=0.00){
   	double estimateQuantityPay=0.00;
       estimateQuantityPay= estimateQuantity; 
       estimateExpense=estimateQuantityPay*bayRate;
      if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
   	   if(typeConversion==null ||  typeConversion==0.00) 	 {
   		  
   	   }else{
   		estimateExpense=estimateExpense/typeConversion;
   	   }
      }
      if(type!=null && type.toString().equalsIgnoreCase("Multiply"))  {
    	  estimateExpense=estimateExpense*typeConversion; 
      }
      estimateRate=bayRate;
      dynamicSetQuery.put("estimateRate",estimateRate);
      dynamicSetQuery.put("estimateExpense",estimateExpense); 
      }else{
    	  dynamicSetQuery.put("estimateRate",estimateRate);
          dynamicSetQuery.put("estimateExpense",estimateExpense);  
      }
      }
      
     
      if(multiCurrency.equalsIgnoreCase("Y")) {
	   estSellLocalAmount = estimateRevenueAmount*estSellExchangeRate;
	   estSellLocalRate = estimateSellRate*estSellExchangeRate;
	   dynamicSetQuery.put("estSellLocalAmount",estSellLocalAmount); 
	   dynamicSetQuery.put("estSellLocalRate",estSellLocalRate);
      }
	   
	           
	            
	           
	            estLocalRate   = new BigDecimal(estimateRate*estExchangeRate);
	            estLocalAmount =  new BigDecimal(estimateExpense*estExchangeRate);
	            
	            dynamicSetQuery.put("estLocalRate",estLocalRate.setScale(2, BigDecimal.ROUND_HALF_UP)); 
	            dynamicSetQuery.put("estLocalAmount",estLocalAmount.setScale(2, BigDecimal.ROUND_HALF_UP));
	         
	            if(estimateExpense!=0.00){
	            Double estimatepasspercentage = ((estimateRevenueAmount/(estimateExpense))*100);
	            Integer estimatePassPercentageValue=estimatepasspercentage.intValue();
	            dynamicSetQuery.put("estimatepasspercentage",estimatePassPercentageValue);
	            }else{
	            dynamicSetQuery.put("estimatepasspercentage",0);
	            }
	            if(accountLine.getVATExclude()!=null && accountLine.getVATExclude()){
	            	
	            }else{
	            	if(systemDefaultVatCalculation.trim().equalsIgnoreCase("true")){
	            	Double recVatPercent=0.00;
					   if(accountLine.getRecVatPercent()!=null && (!(accountLine.getRecVatPercent().trim().equals("")))){
					   recVatPercent=Double.parseDouble(accountLine.getRecVatPercent());
					   estVatAmtValue=(estSellLocalAmount*recVatPercent)/100; 
					   }
					 Double payVatPercent=0.00;  
					 if(accountLine.getPayVatPercent()!=null && (!(accountLine.getPayVatPercent().trim().equals("")))){
						 payVatPercent=Double.parseDouble(accountLine.getPayVatPercent());
						 estExpVatAmt=((Double.parseDouble(estLocalAmount.toString()))*payVatPercent)/100; 
						   }
					 dynamicSetQuery.put("estVatAmt",estVatAmtValue);
			         dynamicSetQuery.put("estExpVatAmt",estExpVatAmt);
	            	}
	            }
	            
	            
	            if(dynamicSetQuery.containsKey("estimateContractCurrency")){
	            	accountLine.setEstimateContractCurrency(dynamicSetQuery.get("estimateContractCurrency").toString()); 
	            }
	            if(dynamicSetQuery.containsKey("estimateContractExchangeRate")){
	            	accountLine.setEstimateContractExchangeRate(new BigDecimal(dynamicSetQuery.get("estimateContractExchangeRate").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimateContractValueDate")){
	            	accountLine.setEstimateContractValueDate((Date)dynamicSetQuery.get("estimateContractValueDate")); 
	            }
	            if(dynamicSetQuery.containsKey("estSellCurrency")){
	            	accountLine.setEstSellCurrency(dynamicSetQuery.get("estSellCurrency").toString()); 
	            }
	            if(dynamicSetQuery.containsKey("estSellExchangeRate")){
	            	accountLine.setEstSellExchangeRate(new BigDecimal(dynamicSetQuery.get("estSellExchangeRate").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estSellValueDate")){
	            	accountLine.setEstSellValueDate((Date)dynamicSetQuery.get("estSellValueDate")); 
	            }
	            if(dynamicSetQuery.containsKey("estimatePayableContractCurrency")){
	            	accountLine.setEstimatePayableContractCurrency(dynamicSetQuery.get("estimatePayableContractCurrency").toString()); 
	            }
	            if(dynamicSetQuery.containsKey("estimatePayableContractExchangeRate")){
	            	accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(dynamicSetQuery.get("estimatePayableContractExchangeRate").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimatePayableContractValueDate")){
	            	accountLine.setEstimatePayableContractValueDate((Date)dynamicSetQuery.get("estimatePayableContractValueDate")); 
	            }
	            if(dynamicSetQuery.containsKey("estimateQuantity")){
	            	accountLine.setEstimateQuantity(new BigDecimal(dynamicSetQuery.get("estimateQuantity").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimateSellQuantity")){
	            	accountLine.setEstimateSellQuantity(new BigDecimal(dynamicSetQuery.get("estimateSellQuantity").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimateContractRate")){
	            	accountLine.setEstimateContractRate(new BigDecimal(dynamicSetQuery.get("estimateContractRate").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimateContractRateAmmount")){
	            	accountLine.setEstimateContractRateAmmount(new BigDecimal(dynamicSetQuery.get("estimateContractRateAmmount").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("basis")){
	            	accountLine.setBasis(dynamicSetQuery.get("basis").toString()); 
	            }
	            if(dynamicSetQuery.containsKey("buyDependSell")){
	            	accountLine.setBuyDependSell(dynamicSetQuery.get("buyDependSell").toString()); 
	            }
	            if(dynamicSetQuery.containsKey("estimateSellDeviation")){
	            	accountLine.setEstimateSellDeviation(new BigDecimal(dynamicSetQuery.get("estimateSellDeviation").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimateDeviation")){
	            	accountLine.setEstimateDeviation(new BigDecimal(dynamicSetQuery.get("estimateDeviation").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimatePayableContractRateAmmount")){
	            	accountLine.setEstimatePayableContractRateAmmount(new BigDecimal(dynamicSetQuery.get("estimatePayableContractRateAmmount").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimatePayableContractRate")){
	            	accountLine.setEstimatePayableContractRate(new BigDecimal(dynamicSetQuery.get("estimatePayableContractRate").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimateRevenueAmount")){
	            	accountLine.setEstimateRevenueAmount(new BigDecimal(dynamicSetQuery.get("estimateRevenueAmount").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimateSellRate")){
	            	accountLine.setEstimateSellRate(new BigDecimal(dynamicSetQuery.get("estimateSellRate").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estSellLocalAmount")){
	            	accountLine.setEstSellLocalAmount(new BigDecimal(dynamicSetQuery.get("estSellLocalAmount").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estSellLocalRate")){
	            	accountLine.setEstSellLocalRate(new BigDecimal(dynamicSetQuery.get("estSellLocalRate").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimateRate")){
	            	accountLine.setEstimateRate(new BigDecimal(dynamicSetQuery.get("estimateRate").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimateExpense")){
	            	accountLine.setEstimateExpense(new BigDecimal(dynamicSetQuery.get("estimateExpense").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estLocalRate")){
	            	accountLine.setEstLocalRate(new BigDecimal(dynamicSetQuery.get("estLocalRate").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estLocalAmount")){
	            	accountLine.setEstLocalAmount(new BigDecimal(dynamicSetQuery.get("estLocalAmount").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estimatepasspercentage")){
	            	accountLine.setEstimatePassPercentage(Integer.parseInt((dynamicSetQuery.get("estimatepasspercentage").toString()))); 
	            }
	            if(dynamicSetQuery.containsKey("estVatAmt")){
	            	accountLine.setEstVatAmt(new BigDecimal(dynamicSetQuery.get("estVatAmt").toString())); 
	            }
	            if(dynamicSetQuery.containsKey("estExpVatAmt")){
	            	accountLine.setEstExpVatAmt(new BigDecimal(dynamicSetQuery.get("estExpVatAmt").toString())); 
	            }
   
	   }
		   }   
		  }catch(Exception e){
			  e.printStackTrace();  
		  }
		  }
	  
	   if(str1[2].toString().equalsIgnoreCase("computeRevision")){

			  try{
				  if(accountLine.getCategory()==null || accountLine.getCategory().trim().equalsIgnoreCase("Internal Cost")){ 
				    
			   }else{
				  revisedCharge();
				
			       String  res[] = revisedChargeValue.split("#");
			    if(contractType){
			    	revisionContractRate=0.00;
			    	revisionPayableContractRate=0.00;
			    	revisionContractCurrency=accountLine.getRevisionContractCurrency();
			    	if(res[21]!=null && (!(res[21].toString().trim().equals("")))){
			    		revisionContractCurrency=res[21].toString().trim();
			    	}
			    	revisionPayableContractCurrency=accountLine.getRevisionPayableContractCurrency();
			    	if(res[22]!=null && (!(res[22].toString().trim().equals("")))){
			    		revisionPayableContractCurrency=res[22].toString().trim();
			    	}
			    	revisionContractExchangeRate= Double.parseDouble(accountLine.getRevisionContractExchangeRate().toString());
			    	revisionSellExchangeRate=Double.parseDouble(accountLine.getRevisionSellExchangeRate().toString());
			    	revisionPayableContractExchangeRate=Double.parseDouble(accountLine.getRevisionPayableContractExchangeRate().toString());
			    	revisionExchangeRate=Double.parseDouble(accountLine.getRevisionExchangeRate().toString());
			    	
			    	if(currencyExchangeRate.containsKey(revisionContractCurrency)){
	    				revisionContractExchangeRate=Double.parseDouble(currencyExchangeRate.get(revisionContractCurrency)); 
	    				dynamicSetQuery.put("revisionContractCurrency",revisionContractCurrency);
	    				dynamicSetQuery.put("revisionContractExchangeRate",revisionContractExchangeRate);  
	    				dynamicSetQuery.put("revisionContractValueDate",new Date());
	    			}
	    			revisionSellCurrency=accountLine.getRevisionSellCurrency();
	    			if(currencyExchangeRate.containsKey(revisionSellCurrency)){
	    				revisionSellExchangeRate=Double.parseDouble(currencyExchangeRate.get(revisionSellCurrency)); 
	    				dynamicSetQuery.put("revisionSellCurrency",revisionSellCurrency);
	    				dynamicSetQuery.put("revisionSellExchangeRate",revisionSellExchangeRate);  
	    				dynamicSetQuery.put("revisionSellValueDate",new Date());
	    			}
			        if(currencyExchangeRate.containsKey(revisionPayableContractCurrency)){
			    			revisionPayableContractExchangeRate=Double.parseDouble(currencyExchangeRate.get(revisionPayableContractCurrency)); 
		    				dynamicSetQuery.put("revisionPayableContractCurrency",revisionPayableContractCurrency);
		    				dynamicSetQuery.put("revisionPayableContractExchangeRate",revisionPayableContractExchangeRate);  
		    				dynamicSetQuery.put("revisionPayableContractValueDate",new Date());
		    		}
			        revisionCurrency=accountLine.getRevisionCurrency();
			        if(currencyExchangeRate.containsKey(revisionCurrency)){
			        	revisionExchangeRate=Double.parseDouble(currencyExchangeRate.get(revisionCurrency)); 
	    				dynamicSetQuery.put("revisionCurrency",revisionCurrency);
	    				dynamicSetQuery.put("revisionExchangeRate",revisionExchangeRate);  
	    				dynamicSetQuery.put("revisionValueDate",new Date());
	    		}
			    	
			    	if(res[9]!= null && (res[9].equalsIgnoreCase("AskUser1"))){
			    		
			    	}else{
			    	if(res[7] != null && (!(res[7].trim().equalsIgnoreCase("")))){	
			    		revisionQuantity = Double.parseDouble(res[7].toString()); 
			    	}
			    	} 
			     if(res[7] == null || res[7].toString() == ""){
			      dynamicSetQuery.put("revisionQuantity", 0); 
			      dynamicSetQuery.put("revisionSellQuantity", 0); 
			     }else{
			      dynamicSetQuery.put("revisionQuantity",revisionQuantity);  
			      dynamicSetQuery.put("revisionSellQuantity", revisionQuantity); 
			     }
			     
			    
			     if(res[10]!=null && res[10].toString().equalsIgnoreCase("AskUser1")){
			     }else{
			      if(res[8] == null || res[8].toString().trim().equalsIgnoreCase("")){
			      dynamicSetQuery.put("revisionContractRate",0); 
			     }else{
			    	 revisionContractRate = Double.parseDouble(res[8].toString());
			      dynamicSetQuery.put("revisionContractRate",revisionContractRate);  
			     }
			      
			     } 
	             if(res[10].toString().equalsIgnoreCase("BuildFormula")){
			      if(res[11] == null || res[11].toString().trim().equalsIgnoreCase("")){
			       dynamicSetQuery.put("revisionContractRate",0); 
			      }else{
			    	  revisionContractRate = Double.parseDouble(res[11].toString());
			       dynamicSetQuery.put("revisionContractRate",revisionContractRate);  
			      } 
			    } 
	             revisionContractRateAmmount = revisionContractRate*revisionQuantity;
			   
			   String type = res[3];
			   if(res[5]!=null &&  (!(res[5].toString().equalsIgnoreCase("")))) 	 {
	           typeConversion=Double.parseDouble(res[5].toString()); 
			   }
	           if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
	        	   if(typeConversion==null ||  typeConversion==0.00) 	 {
	        		  
	        	   }else{
	        		   revisionContractRateAmmount=revisionContractRateAmmount/typeConversion;
	        	   }
	           }
	           if(type!=null && type.toString().equalsIgnoreCase("Multiply"))  {
	        	   revisionContractRateAmmount=revisionContractRateAmmount*typeConversion; 
	           } 
	           dynamicSetQuery.put("revisionContractRateAmmount",revisionContractRateAmmount);
	           dynamicSetQuery.put("basis",res[13].toString());
	           String chargedeviation=res[18];
	           if(accountLine.getRevisionPayableContractRateAmmount()!=null){
	        	   revisionPayableContractRateAmmount=Double.parseDouble(accountLine.getRevisionPayableContractRateAmmount().toString());
	           }
	           if(chargedeviation!=null && (!(chargedeviation.trim().equals(""))) && (!(chargedeviation.trim().equalsIgnoreCase("NCT")))){
	        	   dynamicSetQuery.put("buyDependSell",res[15]); 
	               String  buyDependSellAccount=res[15];
	               double revisionRevenueAmount=0;
	               double finalRevisionContractRate=0; 
	               double finalRevisionExpenceAmount=0;
	               double finalRevisionContractRateAmmount=0; 
	               revisionPayableContractRateAmmount=0.00;
	               double revisionSellRate=0; 
	               double sellDeviation=0;
	               double  revisionpasspercentage=0;
	               double revisionpasspercentageRound=0;
	               double buyDeviation=0;
	               if(res[16]!=null && (!(res[16].toString().equals("")))){
	               sellDeviation=Double.parseDouble(res[16].toString());
	               }
	               if(res[17]!=null && (!(res[17].toString().equals("")))){
	               buyDeviation=Double.parseDouble(res[17].toString()); 
	               }
	               
	               //revisionRevenueAmount=revisionContractRateAmmount/revisionContractExchangeRate;
	               //revisionRevenueAmount=Math.round(revisionRevenueAmount*100)/100;
	              
	               //finalRevisionRevenueAmount=Math.round((revisionRevenueAmount*(sellDeviation/100))*100)/100;
	               finalRevisionContractRateAmmount=Math.round((revisionContractRateAmmount*(sellDeviation/100))*10000)/10000;
	               finalRevisionContractRate=Math.round((revisionContractRate*(sellDeviation/100))*10000)/10000;
	               dynamicSetQuery.put("revisionContractRateAmmount",finalRevisionContractRateAmmount);
	               dynamicSetQuery.put("revisionContractRate",finalRevisionContractRate);
	                
	               if(buyDependSellAccount=="Y"){
	            	   revisionPayableContractRateAmmount= (finalRevisionContractRateAmmount*(buyDeviation/100));
	            	   revisionPayableContractRate= (finalRevisionContractRate*(buyDeviation/100));
	               }else{
	            	   revisionPayableContractRateAmmount= (revisionContractRateAmmount*(buyDeviation/100));
	            	   revisionPayableContractRate= (revisionContractRate*(buyDeviation/100));
	               }
	               
	               dynamicSetQuery.put("revisionSellDeviation",sellDeviation);
	               dynamicSetQuery.put("revisionDeviation",buyDeviation); 
	         
	               dynamicSetQuery.put("revisionPayableContractRateAmmount",revisionPayableContractRateAmmount);
	               dynamicSetQuery.put("revisionPayableContractRate",revisionPayableContractRate);
	               
	        	   
	           }else{

	           dynamicSetQuery.put("revisionSellDeviation",0);
	           dynamicSetQuery.put("revisionDeviation",0);
	           
	           double bayRate=0.00;
	          if(res[20]!=null && (!(res[20].toString().trim().equals("")))){
	           bayRate = Double.parseDouble(res[20].toString());
	          }
	           if(bayRate!=0.00 && bayRate!=0.0){
	        	double estimateQuantityPay=0.00;
	            estimateQuantityPay= revisionQuantity; 
	            revisionPayableContractRateAmmount=estimateQuantityPay*bayRate;
	           if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
	        	   if(typeConversion==null ||  typeConversion==0.00) 	 {
	        		  
	        	   }else{
	        		   revisionPayableContractRateAmmount=revisionPayableContractRateAmmount/typeConversion;
	        	   }
	           }
	           if(type!=null && type.toString().equalsIgnoreCase("Multiply"))  {
	        	   revisionPayableContractRateAmmount=revisionPayableContractRateAmmount*typeConversion; 
	           } 
	           revisionPayableContractRate=bayRate;
	           dynamicSetQuery.put("revisionPayableContractRate",bayRate);
	           dynamicSetQuery.put("revisionPayableContractRateAmmount",revisionPayableContractRateAmmount); 
	           }else{
	        	   dynamicSetQuery.put("revisionPayableContractRate",bayRate);
		           dynamicSetQuery.put("revisionPayableContractRateAmmount",revisionPayableContractRateAmmount);    
	           }
	           }
	          
	           revisionRevenueAmount=revisionContractRateAmmount/revisionContractExchangeRate;
	           revisionSellRate = revisionContractRate/revisionContractExchangeRate;;
			   dynamicSetQuery.put("revisionRevenueAmount",revisionRevenueAmount); 
			   dynamicSetQuery.put("revisionSellRate",revisionSellRate);
			   
			   revisionSellLocalAmount = revisionRevenueAmount*revisionSellExchangeRate;
			   revisionSellLocalRate = revisionSellRate*revisionSellExchangeRate;
			   dynamicSetQuery.put("revisionSellLocalAmount",revisionSellLocalAmount); 
			   dynamicSetQuery.put("revisionSellLocalRate",revisionSellLocalRate);
			  
			   
			           
			   revisionRate = revisionPayableContractRate/revisionPayableContractExchangeRate; 
			   revisionExpense = revisionPayableContractRateAmmount/revisionPayableContractExchangeRate;
			            
			            dynamicSetQuery.put("revisionRate",revisionRate);
			            dynamicSetQuery.put("revisionExpense",revisionExpense);
			           
			            revisionLocalRate   = new BigDecimal(revisionRate*revisionExchangeRate);
			            revisionLocalAmount =  new BigDecimal(revisionExpense*revisionExchangeRate);
			            
			            dynamicSetQuery.put("revisionLocalRate",revisionLocalRate.setScale(2, BigDecimal.ROUND_HALF_UP)); 
			            dynamicSetQuery.put("revisionLocalAmount",revisionLocalAmount.setScale(2, BigDecimal.ROUND_HALF_UP));
			         
			            if(revisionExpense!=0.00){
			            Double revisionpasspercentage = ((revisionRevenueAmount/(revisionExpense))*100);
			            Integer revisionPassPercentageValue=revisionpasspercentage.intValue();
			            dynamicSetQuery.put("revisionPassPercentage",revisionPassPercentageValue);
			            }else{
			            	dynamicSetQuery.put("revisionPassPercentage",0);	
			            }
			            if(accountLine.getVATExclude()!=null && accountLine.getVATExclude()){
			            	
			            }else{
			            	if(systemDefaultVatCalculation.trim().equalsIgnoreCase("true")){
			            	Double recVatPercent=0.00;
							   if(accountLine.getRecVatPercent()!=null && (!(accountLine.getRecVatPercent().trim().equals("")))){
							   recVatPercent=Double.parseDouble(accountLine.getRecVatPercent());
							   revisionVatAmtValue=(revisionSellLocalAmount*recVatPercent)/100; 
							   }
							 Double payVatPercent=0.00;  
							 if(accountLine.getPayVatPercent()!=null && (!(accountLine.getPayVatPercent().trim().equals("")))){
								 payVatPercent=Double.parseDouble(accountLine.getPayVatPercent());
								 revisionExpVatAmt=((Double.parseDouble(revisionLocalAmount.toString()))*payVatPercent)/100; 
								   }
							 dynamicSetQuery.put("revisionVatAmt",revisionVatAmtValue);
					         dynamicSetQuery.put("revisionExpVatAmt",revisionExpVatAmt);
			            }
			            }
			            
			            
			            if(dynamicSetQuery.containsKey("revisionContractCurrency")){
			            	accountLine.setRevisionContractCurrency(dynamicSetQuery.get("revisionContractCurrency").toString()); 
			            }
			            if(dynamicSetQuery.containsKey("revisionContractExchangeRate")){
			            	accountLine.setRevisionContractExchangeRate(new BigDecimal(dynamicSetQuery.get("revisionContractExchangeRate").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionContractValueDate")){
			            	accountLine.setRevisionContractValueDate((Date)dynamicSetQuery.get("revisionContractValueDate")); 
			            }
			            if(dynamicSetQuery.containsKey("revisionSellCurrency")){
			            	accountLine.setRevisionSellCurrency(dynamicSetQuery.get("revisionSellCurrency").toString()); 
			            }
			            if(dynamicSetQuery.containsKey("revisionSellExchangeRate")){
			            	accountLine.setRevisionSellExchangeRate(new BigDecimal(dynamicSetQuery.get("revisionSellExchangeRate").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionSellValueDate")){
			            	accountLine.setRevisionSellValueDate((Date)dynamicSetQuery.get("revisionSellValueDate")); 
			            }
			            if(dynamicSetQuery.containsKey("revisionPayableContractCurrency")){
			            	accountLine.setRevisionPayableContractCurrency(dynamicSetQuery.get("revisionPayableContractCurrency").toString()); 
			            }
			            if(dynamicSetQuery.containsKey("revisionPayableContractExchangeRate")){
			            	accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(dynamicSetQuery.get("revisionPayableContractExchangeRate").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionPayableContractValueDate")){
			            	accountLine.setRevisionPayableContractValueDate((Date)dynamicSetQuery.get("revisionPayableContractValueDate")); 
			            }
			            if(dynamicSetQuery.containsKey("revisionCurrency")){
			            	accountLine.setRevisionCurrency(dynamicSetQuery.get("revisionCurrency").toString()); 
			            }
			            if(dynamicSetQuery.containsKey("revisionExchangeRate")){
			            	accountLine.setRevisionExchangeRate(new BigDecimal(dynamicSetQuery.get("revisionExchangeRate").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionValueDate")){
			            	accountLine.setRevisionValueDate((Date)dynamicSetQuery.get("revisionValueDate")); 
			            }
			            if(dynamicSetQuery.containsKey("revisionQuantity")){
			            	accountLine.setRevisionQuantity(new BigDecimal(dynamicSetQuery.get("revisionQuantity").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionSellQuantity")){
			            	accountLine.setRevisionSellQuantity(new BigDecimal(dynamicSetQuery.get("revisionSellQuantity").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionContractRate")){
			            	accountLine.setRevisionContractRate(new BigDecimal(dynamicSetQuery.get("revisionContractRate").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionContractRateAmmount")){
			            	accountLine.setRevisionContractRateAmmount(new BigDecimal(dynamicSetQuery.get("revisionContractRateAmmount").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("basis")){
			            	accountLine.setBasis(dynamicSetQuery.get("basis").toString()); 
			            }
			            if(dynamicSetQuery.containsKey("buyDependSell")){
			            	accountLine.setBuyDependSell(dynamicSetQuery.get("buyDependSell").toString()); 
			            }
			            if(dynamicSetQuery.containsKey("revisionSellDeviation")){
			            	accountLine.setRevisionSellDeviation(new BigDecimal(dynamicSetQuery.get("revisionSellDeviation").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionDeviation")){
			            	accountLine.setRevisionDeviation(new BigDecimal(dynamicSetQuery.get("revisionDeviation").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionPayableContractRateAmmount")){
			            	accountLine.setRevisionPayableContractRateAmmount(new BigDecimal(dynamicSetQuery.get("revisionPayableContractRateAmmount").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionPayableContractRate")){
			            	accountLine.setRevisionPayableContractRate(new BigDecimal(dynamicSetQuery.get("revisionPayableContractRate").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionRevenueAmount")){
			            	accountLine.setRevisionRevenueAmount(new BigDecimal(dynamicSetQuery.get("revisionRevenueAmount").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionSellRate")){
			            	accountLine.setRevisionSellRate(new BigDecimal(dynamicSetQuery.get("revisionSellRate").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionSellLocalAmount")){
			            	accountLine.setRevisionSellLocalAmount(new BigDecimal(dynamicSetQuery.get("revisionSellLocalAmount").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionSellLocalRate")){
			            	accountLine.setRevisionSellLocalRate(new BigDecimal(dynamicSetQuery.get("revisionSellLocalRate").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionRate")){
			            	accountLine.setRevisionRate(new BigDecimal(dynamicSetQuery.get("revisionRate").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionExpense")){
			            	accountLine.setRevisionExpense(new BigDecimal(dynamicSetQuery.get("revisionExpense").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionLocalRate")){
			            	accountLine.setRevisionLocalRate(new BigDecimal(dynamicSetQuery.get("revisionLocalRate").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionLocalAmount")){
			            	accountLine.setRevisionLocalAmount(new BigDecimal(dynamicSetQuery.get("revisionLocalAmount").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionpasspercentage")){
			            	accountLine.setRevisionPassPercentage(Integer.parseInt((dynamicSetQuery.get("revisionpasspercentage").toString()))); 
			            }
			            if(dynamicSetQuery.containsKey("revisionVatAmt")){
			            	accountLine.setRevisionVatAmt(new BigDecimal(dynamicSetQuery.get("revisionVatAmt").toString())); 
			            }
			            if(dynamicSetQuery.containsKey("revisionExpVatAmt")){
			            	accountLine.setRevisionExpVatAmt(new BigDecimal(dynamicSetQuery.get("revisionExpVatAmt").toString())); 
			            }
		   }else{
			   revisionSellRate=0.00;
			   revisionRate=0.00;
		    	/*revisionContractCurrency=accountLine.getRevisionContractCurrency();
		    	if(res[21]!=null && (!(res[21].toString().trim().equals("")))){
		    		revisionContractCurrency=res[21].toString().trim();
		    	}
		    	revisionPayableContractCurrency=accountLine.getRevisionPayableContractCurrency();
		    	if(res[22]!=null && (!(res[22].toString().trim().equals("")))){
		    		revisionPayableContractCurrency=res[22].toString().trim();
		    	}*/
		    	//revisionContractExchangeRate= Double.parseDouble(accountLine.getRevisionContractExchangeRate().toString());
		    	revisionSellExchangeRate=Double.parseDouble(accountLine.getRevisionSellExchangeRate().toString());
		    	//revisionPayableContractExchangeRate=Double.parseDouble(accountLine.getRevisionPayableContractExchangeRate().toString());
		    	revisionExchangeRate=Double.parseDouble(accountLine.getRevisionExchangeRate().toString());
		    
		    	
		    	if(res[9]!= null && (res[9].equalsIgnoreCase("AskUser1"))){
		    		
		    	}else{
		    	if(res[7] != null && (!(res[7].trim().equalsIgnoreCase("")))){	
		    		revisionQuantity = Double.parseDouble(res[7].toString()); 
		    	}
		    	} 
		     if(res[7] == null || res[7].toString() == ""){
		      dynamicSetQuery.put("revisionQuantity", 0); 
		      //dynamicSetQuery.put("revisionSellQuantity", 0); 
		     }else{
		      dynamicSetQuery.put("revisionQuantity",revisionQuantity);  
		      //dynamicSetQuery.put("revisionSellQuantity", revisionQuantity); 
		     }
		     
		    
		     if(res[10]!=null && res[10].toString().equalsIgnoreCase("AskUser1")){
		     }else{
		      if(res[8] == null || res[8].toString().trim().equalsIgnoreCase("")){
		      dynamicSetQuery.put("revisionSellRate",0); 
		     }else{
		    	 revisionSellRate = Double.parseDouble(res[8].toString());
		      dynamicSetQuery.put("revisionSellRate",revisionSellRate);  
		     }
		      
		     } 
            if(res[10].toString().equalsIgnoreCase("BuildFormula")){
		      if(res[11] == null || res[11].toString().trim().equalsIgnoreCase("")){
		       dynamicSetQuery.put("revisionSellRate",0); 
		      }else{
		    	  revisionSellRate = Double.parseDouble(res[11].toString());
		       dynamicSetQuery.put("revisionSellRate",revisionSellRate);  
		      } 
		    } 
            revisionRevenueAmount = revisionSellRate*revisionQuantity;
		   
		   String type = res[3];
		   if(res[5]!=null &&  (!(res[5].toString().equalsIgnoreCase("")))) 	 {
          typeConversion=Double.parseDouble(res[5].toString()); 
		   }
          if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
       	   if(typeConversion==null ||  typeConversion==0.00) 	 {
       		  
       	   }else{
       		revisionRevenueAmount=revisionRevenueAmount/typeConversion;
       	   }
          }
          if(type!=null && type.toString().equalsIgnoreCase("Multiply"))  {
        	  revisionRevenueAmount=revisionRevenueAmount*typeConversion; 
          } 
          dynamicSetQuery.put("revisionRevenueAmount",revisionRevenueAmount);
          dynamicSetQuery.put("basis",res[13].toString());
          String chargedeviation=res[18];
          if(accountLine.getRevisionExpense()!=null){
        	  revisionExpense=Double.parseDouble(accountLine.getRevisionExpense().toString());
          }
          if(chargedeviation!=null && (!(chargedeviation.trim().equals(""))) && (!(chargedeviation.trim().equalsIgnoreCase("NCT")))){
       	   dynamicSetQuery.put("buyDependSell",res[15]); 
              String  buyDependSellAccount=res[15];
              double revisionRevenueAmount=0;
              double finalRevisionSellRate=0; 
              double finalRevisionExpenceAmount=0;
              double finalRevisionRevenueAmount=0; 
              revisionExpense=0.00;
              double revisionSellRate=0; 
              double sellDeviation=0;
              double  revisionpasspercentage=0;
              double revisionpasspercentageRound=0;
              double buyDeviation=0;
              if(res[16]!=null && (!(res[16].toString().equals("")))){
              sellDeviation=Double.parseDouble(res[16].toString());
              }
              if(res[17]!=null && (!(res[17].toString().equals("")))){
              buyDeviation=Double.parseDouble(res[17].toString()); 
              }
       
              finalRevisionRevenueAmount=Math.round((revisionRevenueAmount*(sellDeviation/100))*10000)/10000;
              finalRevisionSellRate=Math.round((revisionSellRate*(sellDeviation/100))*10000)/10000;
              dynamicSetQuery.put("revisionRevenueAmount",finalRevisionRevenueAmount);
              dynamicSetQuery.put("revisionSellRate",finalRevisionSellRate);
               
              if(buyDependSellAccount=="Y"){
            	  revisionExpense= (finalRevisionRevenueAmount*(buyDeviation/100));
            	  revisionRate= (finalRevisionSellRate*(buyDeviation/100));
              }else{
            	  revisionExpense= (revisionRevenueAmount*(buyDeviation/100));
            	  revisionRate= (revisionSellRate*(buyDeviation/100));
              }
              
              dynamicSetQuery.put("revisionSellDeviation",sellDeviation);
              dynamicSetQuery.put("revisionDeviation",buyDeviation); 
            
              dynamicSetQuery.put("revisionExpense",revisionExpense);
              dynamicSetQuery.put("revisionRate",revisionRate);
              
       	   
          }else{

          dynamicSetQuery.put("revisionSellDeviation",0);
          dynamicSetQuery.put("revisionDeviation",0);
          
          double bayRate=0.00;
         if(res[20]!=null && (!(res[20].toString().trim().equals("")))){
          bayRate = Double.parseDouble(res[20].toString());
         }
          if(bayRate!=0.00 && bayRate !=0.00){
       	double estimateQuantityPay=0.00;
           estimateQuantityPay= revisionQuantity; 
           revisionExpense=estimateQuantityPay*bayRate;
          if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
       	   if(typeConversion==null ||  typeConversion==0.00) 	 {
       		  
       	   }else{
       		revisionExpense=revisionExpense/typeConversion;
       	   }
          }
          if(type!=null && type.toString().equalsIgnoreCase("Multiply"))  {
        	  revisionExpense=revisionExpense*typeConversion; 
          } 
          revisionRate=bayRate;
          dynamicSetQuery.put("revisionRate",bayRate);
          dynamicSetQuery.put("revisionExpense",revisionExpense); 
          }else{
        	  dynamicSetQuery.put("revisionRate",bayRate);
              dynamicSetQuery.put("revisionExpense",revisionExpense);   
          }
          }
         
          
          if(multiCurrency.equalsIgnoreCase("Y")) {
		   revisionSellLocalAmount = revisionRevenueAmount*revisionSellExchangeRate;
		   revisionSellLocalRate = revisionSellRate*revisionSellExchangeRate;
		   dynamicSetQuery.put("revisionSellLocalAmount",revisionSellLocalAmount); 
		   dynamicSetQuery.put("revisionSellLocalRate",revisionSellLocalRate);
          }

		            revisionLocalRate   = new BigDecimal(revisionRate*revisionExchangeRate);
		            revisionLocalAmount =  new BigDecimal(revisionExpense*revisionExchangeRate);
		            
		            dynamicSetQuery.put("revisionLocalRate",revisionLocalRate.setScale(2, BigDecimal.ROUND_HALF_UP)); 
		            dynamicSetQuery.put("revisionLocalAmount",revisionLocalAmount.setScale(2, BigDecimal.ROUND_HALF_UP));
		         
		            if(revisionExpense!=0.00){
		            Double revisionpasspercentage = ((revisionRevenueAmount/(revisionExpense))*100);
		            Integer revisionPassPercentageValue=revisionpasspercentage.intValue();
		            dynamicSetQuery.put("revisionPassPercentage",revisionPassPercentageValue);
		            }else{
		            	dynamicSetQuery.put("revisionPassPercentage",0);
		            }
		            if(accountLine.getVATExclude()!=null && accountLine.getVATExclude()){
		            	
		            }else{
		            	if(systemDefaultVatCalculation.trim().equalsIgnoreCase("true")){
		            	Double recVatPercent=0.00;
						   if(accountLine.getRecVatPercent()!=null && (!(accountLine.getRecVatPercent().trim().equals("")))){
						   recVatPercent=Double.parseDouble(accountLine.getRecVatPercent());
						   revisionVatAmtValue=(revisionSellLocalAmount*recVatPercent)/100; 
						   }
						 Double payVatPercent=0.00;  
						 if(accountLine.getPayVatPercent()!=null && (!(accountLine.getPayVatPercent().trim().equals("")))){
							 payVatPercent=Double.parseDouble(accountLine.getPayVatPercent());
							 revisionExpVatAmt=((Double.parseDouble(revisionLocalAmount.toString()))*payVatPercent)/100; 
							   }
						 dynamicSetQuery.put("revisionVatAmt",revisionVatAmtValue);
				         dynamicSetQuery.put("revisionExpVatAmt",revisionExpVatAmt);
		            	}
		            }
		           
		            
		            if(dynamicSetQuery.containsKey("revisionContractCurrency")){
		            	accountLine.setRevisionContractCurrency(dynamicSetQuery.get("revisionContractCurrency").toString()); 
		            }
		            if(dynamicSetQuery.containsKey("revisionContractExchangeRate")){
		            	accountLine.setRevisionContractExchangeRate(new BigDecimal(dynamicSetQuery.get("revisionContractExchangeRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionContractValueDate")){
		            	accountLine.setRevisionContractValueDate((Date)dynamicSetQuery.get("revisionContractValueDate")); 
		            }
		            if(dynamicSetQuery.containsKey("revisionSellCurrency")){
		            	accountLine.setRevisionSellCurrency(dynamicSetQuery.get("revisionSellCurrency").toString()); 
		            }
		            if(dynamicSetQuery.containsKey("revisionSellExchangeRate")){
		            	accountLine.setRevisionSellExchangeRate(new BigDecimal(dynamicSetQuery.get("revisionSellExchangeRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionSellValueDate")){
		            	accountLine.setRevisionSellValueDate((Date)dynamicSetQuery.get("revisionSellValueDate")); 
		            }
		            if(dynamicSetQuery.containsKey("revisionPayableContractCurrency")){
		            	accountLine.setRevisionPayableContractCurrency(dynamicSetQuery.get("revisionPayableContractCurrency").toString()); 
		            }
		            if(dynamicSetQuery.containsKey("revisionPayableContractExchangeRate")){
		            	accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(dynamicSetQuery.get("revisionPayableContractExchangeRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionPayableContractValueDate")){
		            	accountLine.setRevisionPayableContractValueDate((Date)dynamicSetQuery.get("revisionPayableContractValueDate")); 
		            }
		            if(dynamicSetQuery.containsKey("revisionQuantity")){
		            	accountLine.setRevisionQuantity(new BigDecimal(dynamicSetQuery.get("revisionQuantity").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionSellQuantity")){
		            	accountLine.setRevisionSellQuantity(new BigDecimal(dynamicSetQuery.get("revisionSellQuantity").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionContractRate")){
		            	accountLine.setRevisionContractRate(new BigDecimal(dynamicSetQuery.get("revisionContractRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionContractRateAmmount")){
		            	accountLine.setRevisionContractRateAmmount(new BigDecimal(dynamicSetQuery.get("revisionContractRateAmmount").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("basis")){
		            	accountLine.setBasis(dynamicSetQuery.get("basis").toString()); 
		            }
		            if(dynamicSetQuery.containsKey("buyDependSell")){
		            	accountLine.setBuyDependSell(dynamicSetQuery.get("buyDependSell").toString()); 
		            }
		            if(dynamicSetQuery.containsKey("revisionSellDeviation")){
		            	accountLine.setRevisionSellDeviation(new BigDecimal(dynamicSetQuery.get("revisionSellDeviation").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionDeviation")){
		            	accountLine.setRevisionDeviation(new BigDecimal(dynamicSetQuery.get("revisionDeviation").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionPayableContractRateAmmount")){
		            	accountLine.setRevisionPayableContractRateAmmount(new BigDecimal(dynamicSetQuery.get("revisionPayableContractRateAmmount").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionPayableContractRate")){
		            	accountLine.setRevisionPayableContractRate(new BigDecimal(dynamicSetQuery.get("revisionPayableContractRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionRevenueAmount")){
		            	accountLine.setRevisionRevenueAmount(new BigDecimal(dynamicSetQuery.get("revisionRevenueAmount").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionSellRate")){
		            	accountLine.setRevisionSellRate(new BigDecimal(dynamicSetQuery.get("revisionSellRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionSellLocalAmount")){
		            	accountLine.setRevisionSellLocalAmount(new BigDecimal(dynamicSetQuery.get("revisionSellLocalAmount").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionSellLocalRate")){
		            	accountLine.setRevisionSellLocalRate(new BigDecimal(dynamicSetQuery.get("revisionSellLocalRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionRate")){
		            	accountLine.setRevisionRate(new BigDecimal(dynamicSetQuery.get("revisionRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionExpense")){
		            	accountLine.setRevisionExpense(new BigDecimal(dynamicSetQuery.get("revisionExpense").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionLocalRate")){
		            	accountLine.setRevisionLocalRate(new BigDecimal(dynamicSetQuery.get("revisionLocalRate").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionLocalAmount")){
		            	accountLine.setRevisionLocalAmount(new BigDecimal(dynamicSetQuery.get("revisionLocalAmount").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionpasspercentage")){
		            	accountLine.setRevisionPassPercentage(Integer.parseInt((dynamicSetQuery.get("revisionpasspercentage").toString()))); 
		            }
		            if(dynamicSetQuery.containsKey("revisionVatAmt")){
		            	accountLine.setRevisionVatAmt(new BigDecimal(dynamicSetQuery.get("revisionVatAmt").toString())); 
		            }
		            if(dynamicSetQuery.containsKey("revisionExpVatAmt")){
		            	accountLine.setRevisionExpVatAmt(new BigDecimal(dynamicSetQuery.get("revisionExpVatAmt").toString())); 
		            }
	   
		   }
			   }   
			  }catch(Exception e){
				  e.printStackTrace();  
			  }
			  
		   
	   }
	   
	   if(str1[3].toString().equalsIgnoreCase("computeExpense")){

		   try{
			   if(accountLine.getCategory()==null || accountLine.getCategory().trim().equalsIgnoreCase("Internal Cost")){ 
					    
			}else{
			 revisedCharge();
		
		     String  res[] = revisedChargeValue.split("#");
			 if(contractType){
				 if(accountLine.getNote()==null || accountLine.getNote().toString().trim().equals("")){
					 String description="";
					 if((res[4] != null && (!(res[4].trim().equalsIgnoreCase(""))))){	
						 description=res[4];
						 description=description.replace("\"","").replace("\"","");
					 }
					 if((res[12] != null && (!(res[12].trim().equalsIgnoreCase(""))))){
						 description=description+" ("+res[12]+")";
						 description=description.replace("\"","").replace("\"","");
				     }
					 dynamicSetQuery.put("description",description);
				 }
				 payableContractCurrency=accountLine.getPayableContractCurrency();
			     if(res[22]!=null && (!(res[22].toString().trim().equals("")))){
			    	 payableContractCurrency=res[22].toString().trim();
			      }
			     contractExchangeRate=Double.parseDouble(accountLine.getContractExchangeRate().toString());
			     recRateExchange=Double.parseDouble(accountLine.getRecRateExchange().toString());
			     payableContractExchangeRate = Double.parseDouble(accountLine.getPayableContractExchangeRate().toString());
			     exchangeRate=accountLine.getExchangeRate();
			     /*if(billing.getfXRateOnActualizationDate()!=null && billing.getfXRateOnActualizationDate()){
			    		if(accountLine.getContractCurrency()!=null && contractCurrency!=null && accountLine.getContractCurrency().equalsIgnoreCase(contractCurrency)){
			    			
			    		}else{
			    			if(currencyExchangeRate.containsKey(contractCurrency)){
			    				contractExchangeRate=Double.parseDouble(currencyExchangeRate.get(contractCurrency)); 
			    				dynamicSetQuery.put("contractCurrency",contractCurrency);
			    				dynamicSetQuery.put("contractExchangeRate",contractExchangeRate);  
			    				dynamicSetQuery.put("contractValueDate",new Date());
			    			}
			    			recRateCurrencyValue=accountLine.getRecRateCurrency();
			    			if(currencyExchangeRate.containsKey(recRateCurrencyValue)){
			    				recRateExchange=Double.parseDouble(currencyExchangeRate.get(recRateCurrencyValue)); 
			    				dynamicSetQuery.put("recRateCurrency",recRateCurrencyValue);
			    				dynamicSetQuery.put("recRateExchange",recRateExchange);  
			    				dynamicSetQuery.put("racValueDate",new Date());
			    			}
			    		}
			    	}else{
			    		if(currencyExchangeRate.containsKey(contractCurrency)){
	    				contractExchangeRate=Double.parseDouble(currencyExchangeRate.get(contractCurrency)); 
	    				dynamicSetQuery.put("contractCurrency",contractCurrency);
	    				dynamicSetQuery.put("contractExchangeRate",contractExchangeRate);  
	    				dynamicSetQuery.put("contractValueDate",new Date());
	    			}
			    	}*/
			  
			     if(currencyExchangeRate.containsKey(payableContractCurrency)){
			    		payableContractExchangeRate=Double.parseDouble(currencyExchangeRate.get(payableContractCurrency)); 
	    				dynamicSetQuery.put("payableContractCurrency",payableContractCurrency);
	    				dynamicSetQuery.put("payableContractExchangeRate",payableContractExchangeRate);  
	    				dynamicSetQuery.put("payableContractValueDate",new Date());
	    		}
			     countryValue=accountLine.getCountry();
			     if(currencyExchangeRate.containsKey(countryValue)){
			    	 exchangeRate=Double.parseDouble(currencyExchangeRate.get(countryValue)); 
	    				dynamicSetQuery.put("country",countryValue);
	    				dynamicSetQuery.put("exchangeRate",exchangeRate);  
	    				dynamicSetQuery.put("valueDate",new Date());
	    		}
			    	
			     if(res[9]!= null && (res[9].equalsIgnoreCase("AskUser1"))){
			    		
			    	}else{
			    	if(res[7] != null && (!(res[7].trim().equalsIgnoreCase("")))){	
			    		recQuantity = Double.parseDouble(res[7].toString()); 
			    	}
			    	}  
			     if(res[7] != null && (!(res[7].trim().equalsIgnoreCase("")))){	
				     // dynamicSetQuery.put("recQuantity", recQuantity); 
				     
				     }else{ 
				      //dynamicSetQuery.put("estimateSellQuantity", 0); 
				     } 
				     if(res[10]!=null && res[10].toString().equalsIgnoreCase("AskUser1")){
				     }else{
				      if(res[8] == null || res[8].toString().trim().equalsIgnoreCase("")){
				      //dynamicSetQuery.put("contractRate",0); 
				     }else{
				      contractRate = Double.parseDouble(res[8].toString());
				      //dynamicSetQuery.put("contractRate",contractRate);  
				     }
				     } 
				     if(res[10].toString().equalsIgnoreCase("BuildFormula")){
					      if(res[11] == null || res[11].toString().trim().equalsIgnoreCase("")){
					       //dynamicSetQuery.put("contractRate",0); 
					      }else{
					       contractRate = Double.parseDouble(res[11].toString());
					       //dynamicSetQuery.put("contractRate",contractRate);  
					      } 
					    }
				     
				     contractRateAmmount = contractRate*recQuantity;
					   
					   String type = res[3];
					   if(res[5]!=null &&  (!(res[5].toString().equalsIgnoreCase("")))) 	 {
			           typeConversion=Double.parseDouble(res[5].toString()); 
					   }
			           if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
			        	   if(typeConversion==null ||  typeConversion==0.00) 	 {
			        		  
			        	   }else{
			        		   contractRateAmmount=contractRateAmmount/typeConversion;
			        	   }
			           }
			           if(type!=null && type.toString().trim().equalsIgnoreCase("Multiply"))  {
			        	   contractRateAmmount=contractRateAmmount*typeConversion; 
			           } 
			           //dynamicSetQuery.put("contractRateAmmount",contractRateAmmount);
			           dynamicSetQuery.put("basis",res[13].toString());
			           if(res[19] != null && (!(res[19].trim().equalsIgnoreCase(""))) && (res[19].trim().equalsIgnoreCase("Y"))){
			        	   dynamicSetQuery.put("VATExclude",true); 
		                }else{
		                	dynamicSetQuery.put("VATExclude",false);
		                }
		                 //setVatExcluded('1') ;
			           String chargedeviation=res[18];
			           if(chargedeviation!=null && (!(chargedeviation.trim().equals(""))) && (!(chargedeviation.trim().equalsIgnoreCase("NCT")))){
			        	   dynamicSetQuery.put("deviation",chargedeviation); 
			        	   double sellDeviation=0.00;
			        	   double buyDeviation=0.00;
			        	   double finalContractRateAmmount=0.00;
			        	   double finalActualExpenceAmount=0.00;
			        	   String buyDependSellAccount="";
			        	   if(res[15]!=null &&  (!(res[15].toString().equalsIgnoreCase("")))){
			        		   buyDependSellAccount=res[15].toString();
			        	   }
			        	   if(res[16]!=null &&  (!(res[16].toString().equalsIgnoreCase("")))){
			        		   sellDeviation=Double.parseDouble(res[16].toString());
			        	   }
			        	   if(res[17]!=null &&  (!(res[17].toString().equalsIgnoreCase("")))){
			        		   buyDeviation=Double.parseDouble(res[17].toString());
			        	   }
			        	   
			        	   dynamicSetQuery.put("buyDependSell",buyDependSellAccount); 
			        	   if(sellDeviation==0.00){
			        	   finalContractRateAmmount=Math.round((contractRateAmmount*(sellDeviation/100))*10000)/10000;
			        	   }
			        	   if(buyDependSellAccount!=null && buyDependSellAccount.trim().equalsIgnoreCase("Y")){
			        		   finalActualExpenceAmount= Math.round((finalContractRateAmmount*(buyDeviation/100))*10000)/10000;   
			        	   }else{
			                   finalActualExpenceAmount= Math.round((contractRateAmmount*(buyDeviation/100))*10000)/10000;
			               }
			          
			               dynamicSetQuery.put("payDeviation",buyDeviation); 
			               dynamicSetQuery.put("payableContractRateAmmount",finalActualExpenceAmount); 
			           }
			           else {
			        	     
			               dynamicSetQuery.put("payDeviation",0); 
			        	   double bayRate=0.00;
			        	   if(res[20]!=null && (!(res[20].toString().trim().equals("")))){
			                   bayRate = Double.parseDouble(res[20].toString());
			               } 
			               if(bayRate!=0){
			               if(accountLine.getRecQuantity()!=null && (!(accountLine.getRecQuantity().toString().trim().equals(""))))
			            	   recQuantity=Double.parseDouble(accountLine.getRecQuantity().toString());
			               }else{
			            	   recQuantity=1.00;
			               } 
			               payableContractRateAmmount=recQuantity* bayRate;
			               type = res[3];
						   if(res[5]!=null &&  (!(res[5].toString().equalsIgnoreCase("")))) 	 {
				           typeConversion=Double.parseDouble(res[5].toString()); 
						   }
				           if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
				        	   if(typeConversion==null ||  typeConversion==0.00) 	 {
				        		  
				        	   }else{
				        		   payableContractRateAmmount=payableContractRateAmmount/typeConversion;
				        	   }
				           }
				           if(type!=null && type.toString().trim().equalsIgnoreCase("Multiply"))  {
				        	   payableContractRateAmmount=payableContractRateAmmount*typeConversion; 
				           } 
				           dynamicSetQuery.put("payableContractRateAmmount",payableContractRateAmmount); 
			               } 
			          
			           actualExpenseValue=payableContractRateAmmount/payableContractExchangeRate; 
					   dynamicSetQuery.put("actualExpense",actualExpenseValue); 
					   localAmountValue = actualExpenseValue*exchangeRate;
					   
					   dynamicSetQuery.put("localAmount",localAmountValue);
					  if(systemDefaultVatCalculation.trim().equalsIgnoreCase("true")){
					   Double payVatPercent=0.00;
					   if(accountLine.getPayVatPercent()!=null && (!(accountLine.getPayVatPercent().trim().equals("")))){
						   payVatPercent=Double.parseDouble(accountLine.getPayVatPercent());
					   }
					   if(res[19] != null && (!(res[19].trim().equalsIgnoreCase(""))) && (res[19].trim().equalsIgnoreCase("Y"))){
						   
					   }else{
						   payVatAmtValue=(localAmountValue*payVatPercent)/100; 
						   dynamicSetQuery.put("payVatAmt",payVatAmtValue); 
					   }
					  }
					   if(dynamicSetQuery.containsKey("payableContractCurrency")){
			            	accountLine.setPayableContractCurrency(dynamicSetQuery.get("payableContractCurrency").toString()); 
			            }
					   if(dynamicSetQuery.containsKey("payableContractExchangeRate")){
			            	accountLine.setPayableContractExchangeRate(new BigDecimal(dynamicSetQuery.get("payableContractExchangeRate").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("payableContractValueDate")){
			            	accountLine.setPayableContractValueDate((Date)dynamicSetQuery.get("payableContractValueDate")); 
			            }
					   if(dynamicSetQuery.containsKey("country")){
			            	accountLine.setCountry(dynamicSetQuery.get("country").toString()); 
			            }
					   if(dynamicSetQuery.containsKey("exchangeRate")){
			            	accountLine.setExchangeRate(Double.parseDouble(dynamicSetQuery.get("exchangeRate").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("valueDate")){
			            	accountLine.setValueDate((Date)dynamicSetQuery.get("valueDate")); 
			            }
					   if(dynamicSetQuery.containsKey("basis")){
			            	accountLine.setBasis(dynamicSetQuery.get("basis").toString()); 
			            }
					   if(dynamicSetQuery.containsKey("VATExclude")){
			            	accountLine.setVATExclude((Boolean)dynamicSetQuery.get("VATExclude")); 
			            } 
					   if(dynamicSetQuery.containsKey("deviation")){
						   accountLine.setDeviation(dynamicSetQuery.get("deviation").toString()); 
			            }
					   if(dynamicSetQuery.containsKey("buyDependSell")){
						   accountLine.setBuyDependSell(dynamicSetQuery.get("buyDependSell").toString()); 
			            } 
					   if(dynamicSetQuery.containsKey("payDeviation")){
			            	accountLine.setPayDeviation(new BigDecimal(dynamicSetQuery.get("payDeviation").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("payableContractRateAmmount")){
			            	accountLine.setPayableContractRateAmmount(new BigDecimal(dynamicSetQuery.get("payableContractRateAmmount").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("actualExpense")){
			            	accountLine.setActualExpense(new BigDecimal(dynamicSetQuery.get("actualExpense").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("localAmount")){
			            	accountLine.setLocalAmount(new BigDecimal(dynamicSetQuery.get("localAmount").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("payVatAmt")){
			            	accountLine.setPayVatAmt(new BigDecimal(dynamicSetQuery.get("payVatAmt").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("description")){
			            	accountLine.setNote(dynamicSetQuery.get("description").toString()); 
			            } 
			 }else{

				 if(accountLine.getNote()==null || accountLine.getNote().toString().trim().equals("")){
					 String description="";
					 if((res[4] != null && (!(res[4].trim().equalsIgnoreCase(""))))){	
						 description=res[4];
						 description=description.replace("\"","").replace("\"","");
					 }
					 if((res[12] != null && (!(res[12].trim().equalsIgnoreCase(""))))){
						 description=description+" ("+res[12]+")";
						 description=description.replace("\"","").replace("\"","");
				     }
					 dynamicSetQuery.put("description",description);
				 }
				 
			     recRateExchange=Double.parseDouble(accountLine.getRecRateExchange().toString());
			     //payableContractExchangeRate = Double.parseDouble(accountLine.getPayableContractExchangeRate().toString());
			     exchangeRate=accountLine.getExchangeRate();
			     
			  
			     
			    	
			     if(res[9]!= null && (res[9].equalsIgnoreCase("AskUser1"))){
			    		
			    	}else{
			    	if(res[7] != null && (!(res[7].trim().equalsIgnoreCase("")))){	
			    		recQuantity = Double.parseDouble(res[7].toString()); 
			    	}
			    	}  
			     if(res[7] != null && (!(res[7].trim().equalsIgnoreCase("")))){	
				    
				     
				     }else{ 
				       
				     } 
				     if(res[10]!=null && res[10].toString().equalsIgnoreCase("AskUser1")){
				     }else{
				      if(res[8] == null || res[8].toString().trim().equalsIgnoreCase("")){
				      
				     }else{
				    	 recRate = Double.parseDouble(res[8].toString());
				       
				     }
				     } 
				     if(res[10].toString().equalsIgnoreCase("BuildFormula")){
					      if(res[11] == null || res[11].toString().trim().equalsIgnoreCase("")){
					       //dynamicSetQuery.put("contractRate",0); 
					      }else{
					    	  recRate = Double.parseDouble(res[11].toString());
					       //dynamicSetQuery.put("contractRate",contractRate);  
					      } 
					    }
				     
				     actualRevenueValue = recRate*recQuantity;
					   
					   String type = res[3];
					   if(res[5]!=null &&  (!(res[5].toString().equalsIgnoreCase("")))) 	 {
			           typeConversion=Double.parseDouble(res[5].toString()); 
					   }
			           if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
			        	   if(typeConversion==null ||  typeConversion==0.00) 	 {
			        		  
			        	   }else{
			        		   actualRevenueValue=actualRevenueValue/typeConversion;
			        	   }
			           }
			           if(type!=null && type.toString().trim().equalsIgnoreCase("Multiply"))  {
			        	   actualRevenueValue=actualRevenueValue*typeConversion; 
			           } 
			           
			           dynamicSetQuery.put("basis",res[13].toString());
			           if(res[19] != null && (!(res[19].trim().equalsIgnoreCase(""))) && (res[19].trim().equalsIgnoreCase("Y"))){
			        	   dynamicSetQuery.put("VATExclude",true); 
		                }else{
		                	dynamicSetQuery.put("VATExclude",false);
		                }
		                 //setVatExcluded('1') ;
			           String chargedeviation=res[18];
			           if(chargedeviation!=null && (!(chargedeviation.trim().equals(""))) && (!(chargedeviation.trim().equalsIgnoreCase("NCT")))){
			        	   dynamicSetQuery.put("deviation",chargedeviation); 
			        	   double sellDeviation=0.00;
			        	   double buyDeviation=0.00;
			        	   double finalContractRateAmmount=0.00;
			        	   double finalActualExpenceAmount=0.00;
			        	   String buyDependSellAccount="";
			        	   if(res[15]!=null &&  (!(res[15].toString().equalsIgnoreCase("")))){
			        		   buyDependSellAccount=res[15].toString();
			        	   }
			        	   if(res[16]!=null &&  (!(res[16].toString().equalsIgnoreCase("")))){
			        		   sellDeviation=Double.parseDouble(res[16].toString());
			        	   }
			        	   if(res[17]!=null &&  (!(res[17].toString().equalsIgnoreCase("")))){
			        		   buyDeviation=Double.parseDouble(res[17].toString());
			        	   }
			        	   
			        	   dynamicSetQuery.put("buyDependSell",buyDependSellAccount); 
			        	   if(sellDeviation==0.00){
			        	   finalContractRateAmmount=Math.round((actualRevenueValue*(sellDeviation/100))*10000)/10000;
			        	   }
			        	   if(buyDependSellAccount!=null && buyDependSellAccount.trim().equalsIgnoreCase("Y")){
			        		   finalActualExpenceAmount= Math.round((finalContractRateAmmount*(buyDeviation/100))*10000)/10000;   
			        	   }else{
			                   finalActualExpenceAmount= Math.round((actualRevenueValue*(buyDeviation/100))*10000)/10000;
			               }
			               //dynamicSetQuery.put("contractRateAmmount",finalContractRateAmmount); 
			               //dynamicSetQuery.put("receivableSellDeviation",sellDeviation);  
			               dynamicSetQuery.put("payDeviation",buyDeviation); 
			               dynamicSetQuery.put("actualExpense",finalActualExpenceAmount); 
			           }
			           else {
			        	   //dynamicSetQuery.put("receivableSellDeviation",0);  
			               dynamicSetQuery.put("payDeviation",0); 
			        	   double bayRate=0.00;
			        	   if(res[20]!=null && (!(res[20].toString().trim().equals("")))){
			                   bayRate = Double.parseDouble(res[20].toString());
			               } 
			               if(bayRate!=0){
			               if(accountLine.getRecQuantity()!=null && (!(accountLine.getRecQuantity().toString().trim().equals(""))))
			            	   recQuantity=Double.parseDouble(accountLine.getRecQuantity().toString());
			               }else{
			            	   recQuantity=1.00;
			               } 
			               actualExpenseValue=recQuantity* bayRate;
			               type = res[3];
						   if(res[5]!=null &&  (!(res[5].toString().equalsIgnoreCase("")))) 	 {
				           typeConversion=Double.parseDouble(res[5].toString()); 
						   }
				           if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
				        	   if(typeConversion==null ||  typeConversion==0.00) 	 {
				        		  
				        	   }else{
				        		   actualExpenseValue=actualExpenseValue/typeConversion;
				        	   }
				           }
				           if(type!=null && type.toString().trim().equalsIgnoreCase("Multiply"))  {
				        	   actualExpenseValue=actualExpenseValue*typeConversion; 
				           } 
				           dynamicSetQuery.put("actualExpense",actualExpenseValue); 
			               } 
			          
			           //actualExpenseValue=payableContractRateAmmount/payableContractExchangeRate; 
					   //dynamicSetQuery.put("actualExpense",actualExpenseValue); 
					   localAmountValue = actualExpenseValue*exchangeRate;
					   
					   dynamicSetQuery.put("localAmount",localAmountValue);
					   if(systemDefaultVatCalculation.trim().equalsIgnoreCase("true")){
					   Double payVatPercent=0.00;
					   if(accountLine.getPayVatPercent()!=null && (!(accountLine.getPayVatPercent().trim().equals("")))){
						   payVatPercent=Double.parseDouble(accountLine.getPayVatPercent());
					   }
					   if(res[19] != null && (!(res[19].trim().equalsIgnoreCase(""))) && (res[19].trim().equalsIgnoreCase("Y"))){
						   
					   }else{
						   payVatAmtValue=(localAmountValue*payVatPercent)/100; 
						   dynamicSetQuery.put("payVatAmt",payVatAmtValue); 
					   }
					   }
					   
					   if(dynamicSetQuery.containsKey("payableContractCurrency")){
			            	accountLine.setPayableContractCurrency(dynamicSetQuery.get("payableContractCurrency").toString()); 
			            }
					   if(dynamicSetQuery.containsKey("payableContractExchangeRate")){
			            	accountLine.setPayableContractExchangeRate(new BigDecimal(dynamicSetQuery.get("payableContractExchangeRate").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("payableContractValueDate")){
			            	accountLine.setPayableContractValueDate((Date)dynamicSetQuery.get("payableContractValueDate")); 
			            }
					   if(dynamicSetQuery.containsKey("basis")){
			            	accountLine.setBasis(dynamicSetQuery.get("basis").toString()); 
			            }
					   if(dynamicSetQuery.containsKey("VATExclude")){
			            	accountLine.setVATExclude((Boolean)dynamicSetQuery.get("VATExclude")); 
			            } 
					   if(dynamicSetQuery.containsKey("deviation")){
						   accountLine.setDeviation(dynamicSetQuery.get("deviation").toString()); 
			            }
					   if(dynamicSetQuery.containsKey("buyDependSell")){
						   accountLine.setBuyDependSell(dynamicSetQuery.get("buyDependSell").toString()); 
			            } 
					   if(dynamicSetQuery.containsKey("payDeviation")){
			            	accountLine.setPayDeviation(new BigDecimal(dynamicSetQuery.get("payDeviation").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("payableContractRateAmmount")){
			            	accountLine.setPayableContractRateAmmount(new BigDecimal(dynamicSetQuery.get("payableContractRateAmmount").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("actualExpense")){
			            	accountLine.setActualExpense(new BigDecimal(dynamicSetQuery.get("actualExpense").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("localAmount")){
			            	accountLine.setLocalAmount(new BigDecimal(dynamicSetQuery.get("localAmount").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("payVatAmt")){
			            	accountLine.setPayVatAmt(new BigDecimal(dynamicSetQuery.get("payVatAmt").toString())); 
			            }
					   if(dynamicSetQuery.containsKey("description")){
			            	accountLine.setNote(dynamicSetQuery.get("description").toString()); 
			            } 
			 
			 }
			}
		   }catch(Exception e){
				e.printStackTrace(); 
				  }
		     
		   
	   }
	   
	   if(str1[4].toString().equalsIgnoreCase("computeRevenue")){
	   try{
		   if(accountLine.getCategory()==null || accountLine.getCategory().trim().equalsIgnoreCase("Internal Cost")){ 
				    
		}else{
		 revisedCharge();
		 
	     String  res[] = revisedChargeValue.split("#");
		 if(contractType){
			 if(accountLine.getDescription()==null || accountLine.getDescription().toString().trim().equals("")){
				 String description="";
				 if((res[4] != null && (!(res[4].trim().equalsIgnoreCase(""))))){	
					 description=res[4];
					 description=description.replace("\"","").replace("\"","");
				 }
				 if((res[12] != null && (!(res[12].trim().equalsIgnoreCase(""))))){
					 description=description+" ("+res[12]+")";
					 description=description.replace("\"","").replace("\"","");
			     }
				 dynamicSetQuery.put("description",description);
			 }
			 contractCurrency=accountLine.getContractCurrency();
		     if(res[21]!=null && (!(res[21].toString().trim().equals("")))){
		    	 contractCurrency=res[21].toString().trim();
		      }
		     contractExchangeRate=Double.parseDouble(accountLine.getContractExchangeRate().toString());
		     recRateExchange=Double.parseDouble(accountLine.getRecRateExchange().toString());
		    

 			if(currencyExchangeRate.containsKey(contractCurrency)){
 				contractExchangeRate=Double.parseDouble(currencyExchangeRate.get(contractCurrency)); 
 				dynamicSetQuery.put("contractCurrency",contractCurrency);
 				dynamicSetQuery.put("contractExchangeRate",contractExchangeRate);  
 				dynamicSetQuery.put("contractValueDate",new Date());
 			}
 			recRateCurrencyValue=accountLine.getRecRateCurrency();
 			if(currencyExchangeRate.containsKey(recRateCurrencyValue)){
 				recRateExchange=Double.parseDouble(currencyExchangeRate.get(recRateCurrencyValue)); 
 				dynamicSetQuery.put("recRateCurrency",recRateCurrencyValue);
 				dynamicSetQuery.put("recRateExchange",recRateExchange);  
 				dynamicSetQuery.put("racValueDate",new Date());
 			}
 		
		     if(res[9]!= null && (res[9].equalsIgnoreCase("AskUser1"))){
		    		
		    	}else{
		    	if(res[7] != null && (!(res[7].trim().equalsIgnoreCase("")))){	
		    		recQuantity = Double.parseDouble(res[7].toString()); 
		    	}
		    	}  
		     if(res[7] != null && (!(res[7].trim().equalsIgnoreCase("")))){	
			      dynamicSetQuery.put("recQuantity", recQuantity); 
			     
			     }else{ 
			      dynamicSetQuery.put("recQuantity", 0); 
			     } 
			     if(res[10]!=null && res[10].toString().equalsIgnoreCase("AskUser1")){
			     }else{
			      if(res[8] == null || res[8].toString().trim().equalsIgnoreCase("")){
			      dynamicSetQuery.put("contractRate",0); 
			     }else{
			      contractRate = Double.parseDouble(res[8].toString());
			      dynamicSetQuery.put("contractRate",contractRate);  
			     }
			     } 
			     if(res[10].toString().equalsIgnoreCase("BuildFormula")){
				      if(res[11] == null || res[11].toString().trim().equalsIgnoreCase("")){
				       dynamicSetQuery.put("contractRate",0); 
				      }else{
				       contractRate = Double.parseDouble(res[11].toString());
				       dynamicSetQuery.put("contractRate",contractRate);  
				      } 
				    }
			     
			     contractRateAmmount = contractRate*recQuantity;
				   
				   String type = res[3];
				   if(res[5]!=null &&  (!(res[5].toString().equalsIgnoreCase("")))) 	 {
		           typeConversion=Double.parseDouble(res[5].toString()); 
				   }
		           if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
		        	   if(typeConversion==null ||  typeConversion==0.00) 	 {
		        		  
		        	   }else{
		        		   contractRateAmmount=contractRateAmmount/typeConversion;
		        	   }
		           }
		           if(type!=null && type.toString().trim().equalsIgnoreCase("Multiply"))  {
		        	   contractRateAmmount=contractRateAmmount*typeConversion; 
		           } 
		           dynamicSetQuery.put("contractRateAmmount",contractRateAmmount);
		           dynamicSetQuery.put("basis",res[13].toString());
		           if(res[19] != null && (!(res[19].trim().equalsIgnoreCase(""))) && (res[19].trim().equalsIgnoreCase("Y"))){
		        	   dynamicSetQuery.put("VATExclude",true); 
	                }else{
	                	dynamicSetQuery.put("VATExclude",false);
	                }
	                 //setVatExcluded('1') ;
		           String chargedeviation=res[18];
		           if(chargedeviation!=null && (!(chargedeviation.trim().equals(""))) && (!(chargedeviation.trim().equalsIgnoreCase("NCT")))){
		        	   dynamicSetQuery.put("deviation",chargedeviation); 
		        	   double sellDeviation=0.00;
		        	   double finalContractRateAmmount=0.00;
		        	   double finalContractRate=0.00;
		        	   if(res[16]!=null &&  (!(res[16].toString().equalsIgnoreCase("")))){
		        		   sellDeviation=Double.parseDouble(res[16].toString());
		        	   }
		        	   dynamicSetQuery.put("buyDependSell",res[15]); 
		        	   if(sellDeviation==0.00){
		        	   finalContractRateAmmount=Math.round((contractRateAmmount*(sellDeviation/100))*10000)/10000;
		        	   finalContractRate=Math.round((contractRate*(sellDeviation/100))*10000)/10000;
		        	   }
		               dynamicSetQuery.put("contractRateAmmount",finalContractRateAmmount); 
		               dynamicSetQuery.put("contractRate",finalContractRate); 
		               dynamicSetQuery.put("receivableSellDeviation",sellDeviation);  
		           }
		           actualRevenueValue=contractRateAmmount/contractExchangeRate;
		           recRate = contractRate/contractExchangeRate;;
				   dynamicSetQuery.put("actualRevenue",actualRevenueValue); 
				   dynamicSetQuery.put("recRate",recRate);
				   
				   actualRevenueForeign = actualRevenueValue*recRateExchange;
				   recCurrencyRate = recRate*recRateExchange;
				   dynamicSetQuery.put("actualRevenueForeign",actualRevenueForeign); 
				   dynamicSetQuery.put("recCurrencyRate",recCurrencyRate);
				   if(systemDefaultVatCalculation.trim().equalsIgnoreCase("true")){
				   Double recVatPercent=0.00;
				   if(accountLine.getRecVatPercent()!=null && (!(accountLine.getRecVatPercent().trim().equals("")))){
				   recVatPercent=Double.parseDouble(accountLine.getRecVatPercent());
				   }
				   if(res[19] != null && (!(res[19].trim().equalsIgnoreCase(""))) && (res[19].trim().equalsIgnoreCase("Y"))){
					   
				   }else{
					   recVatAmt=(actualRevenueForeign*recVatPercent)/100; 
					   dynamicSetQuery.put("recVatAmt",recVatAmt); 
				   }
				   }
				   
				   
				   if(dynamicSetQuery.containsKey("contractCurrency")){
		            	accountLine.setContractCurrency(dynamicSetQuery.get("contractCurrency").toString()); 
		            }
				   if(dynamicSetQuery.containsKey("contractExchangeRate")){
		            	accountLine.setContractExchangeRate(new BigDecimal(dynamicSetQuery.get("contractExchangeRate").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("contractValueDate")){
		            	accountLine.setContractValueDate((Date)dynamicSetQuery.get("contractValueDate")); 
		            }
				   if(dynamicSetQuery.containsKey("recRateCurrency")){
		            	accountLine.setRecRateCurrency(dynamicSetQuery.get("recRateCurrency").toString()); 
		            }
				   if(dynamicSetQuery.containsKey("recRateExchange")){
		            	accountLine.setRecRateExchange(new BigDecimal(dynamicSetQuery.get("recRateExchange").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("racValueDate")){
		            	accountLine.setRacValueDate((Date)dynamicSetQuery.get("racValueDate")); 
		            }
				   if(dynamicSetQuery.containsKey("recQuantity")){
		            	accountLine.setRecQuantity(new BigDecimal(dynamicSetQuery.get("recQuantity").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("contractRate")){
		            	accountLine.setContractRate(new BigDecimal(dynamicSetQuery.get("contractRate").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("contractRateAmmount")){
		            	accountLine.setContractRateAmmount(new BigDecimal(dynamicSetQuery.get("contractRateAmmount").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("basis")){
		            	accountLine.setBasis(dynamicSetQuery.get("basis").toString()); 
		            }
				   if(dynamicSetQuery.containsKey("VATExclude")){
		            	accountLine.setVATExclude((Boolean)dynamicSetQuery.get("VATExclude")); 
		            }
				   if(dynamicSetQuery.containsKey("deviation")){
		            	accountLine.setDeviation(dynamicSetQuery.get("deviation").toString()); 
		            }
				   if(dynamicSetQuery.containsKey("buyDependSell")){
		            	accountLine.setBuyDependSell(dynamicSetQuery.get("buyDependSell").toString()); 
		            }
				   if(dynamicSetQuery.containsKey("receivableSellDeviation")){
		            	accountLine.setReceivableSellDeviation(new BigDecimal(dynamicSetQuery.get("receivableSellDeviation").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("actualRevenue")){
		            	accountLine.setActualRevenue(new BigDecimal(dynamicSetQuery.get("actualRevenue").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("recRate")){
		            	accountLine.setRecRate(new BigDecimal(dynamicSetQuery.get("recRate").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("actualRevenueForeign")){
		            	accountLine.setActualRevenueForeign(new BigDecimal(dynamicSetQuery.get("actualRevenueForeign").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("recCurrencyRate")){
		            	accountLine.setRecCurrencyRate(new BigDecimal(dynamicSetQuery.get("recCurrencyRate").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("recVatAmt")){
		            	accountLine.setRecVatAmt(new BigDecimal(dynamicSetQuery.get("recVatAmt").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("description")){
		            	accountLine.setDescription(dynamicSetQuery.get("description").toString()); 
		            } 
		 }else{

			 if(accountLine.getDescription()==null || accountLine.getDescription().toString().trim().equals("")){
				 String description="";
				 if((res[4] != null && (!(res[4].trim().equalsIgnoreCase(""))))){	
					 description=res[4];
					 description=description.replace("\"","").replace("\"","");
				 }
				 if((res[12] != null && (!(res[12].trim().equalsIgnoreCase(""))))){
					 description=description+" ("+res[12]+")";
					 description=description.replace("\"","").replace("\"","");
			     }
				 dynamicSetQuery.put("description",description);
			 }
			 
		     recRateExchange=Double.parseDouble(accountLine.getRecRateExchange().toString());
		  
		     if(res[9]!= null && (res[9].equalsIgnoreCase("AskUser1"))){
		    		
		    	}else{
		    	if(res[7] != null && (!(res[7].trim().equalsIgnoreCase("")))){	
		    		recQuantity = Double.parseDouble(res[7].toString()); 
		    	}
		    	}  
		     if(res[7] != null && (!(res[7].trim().equalsIgnoreCase("")))){	
			      dynamicSetQuery.put("recQuantity", recQuantity); 
			     
			     }else{ 
			      dynamicSetQuery.put("recQuantity", 0); 
			     } 
			     if(res[10]!=null && res[10].toString().equalsIgnoreCase("AskUser1")){
			     }else{
			      if(res[8] == null || res[8].toString().trim().equalsIgnoreCase("")){
			      dynamicSetQuery.put("recRate",0); 
			     }else{
			    	 recRate = Double.parseDouble(res[8].toString());
			      dynamicSetQuery.put("recRate",recRate);  
			     }
			     } 
			     if(res[10].toString().equalsIgnoreCase("BuildFormula")){
				      if(res[11] == null || res[11].toString().trim().equalsIgnoreCase("")){
				       dynamicSetQuery.put("recRate",0); 
				      }else{
				    	  recRate = Double.parseDouble(res[11].toString());
				       dynamicSetQuery.put("recRate",recRate);  
				      } 
				    }
			     
			     actualRevenueValue = recRate*recQuantity;
				   
				   String type = res[3];
				   if(res[5]!=null &&  (!(res[5].toString().equalsIgnoreCase("")))) 	 {
		           typeConversion=Double.parseDouble(res[5].toString()); 
				   }
		           if(type!=null && type.toString().trim().equalsIgnoreCase("Division"))   {
		        	   if(typeConversion==null ||  typeConversion==0.00) 	 {
		        		  
		        	   }else{
		        		   actualRevenueValue=actualRevenueValue/typeConversion;
		        	   }
		           }
		           if(type!=null && type.toString().trim().equalsIgnoreCase("Multiply"))  {
		        	   actualRevenueValue=actualRevenueValue*typeConversion; 
		           } 
		           dynamicSetQuery.put("actualRevenue",actualRevenueValue);
		           dynamicSetQuery.put("basis",res[13].toString());
		           if(res[19] != null && (!(res[19].trim().equalsIgnoreCase(""))) && (res[19].trim().equalsIgnoreCase("Y"))){
		        	   dynamicSetQuery.put("VATExclude",true); 
	                }else{
	                	dynamicSetQuery.put("VATExclude",false);
	                }
	                 //setVatExcluded('1') ;
		           String chargedeviation=res[18];
		           if(chargedeviation!=null && (!(chargedeviation.trim().equals(""))) && (!(chargedeviation.trim().equalsIgnoreCase("NCT")))){
		        	   dynamicSetQuery.put("deviation",chargedeviation); 
		        	   double sellDeviation=0.00;
		        	   double finalContractRateAmmount=0.00;
		        	   double finalRecRatet=0.00;
		        	   if(res[16]!=null &&  (!(res[16].toString().equalsIgnoreCase("")))){
		        		   sellDeviation=Double.parseDouble(res[16].toString());
		        	   }
		        	   dynamicSetQuery.put("buyDependSell",res[15]); 
		        	   if(sellDeviation==0.00){
		        	   finalContractRateAmmount=Math.round((actualRevenueValue*(sellDeviation/100))*10000)/10000;
		        	   finalRecRatet=Math.round((recRate*(sellDeviation/100))*10000)/10000;
		        	   }
		               dynamicSetQuery.put("actualRevenue",finalContractRateAmmount); 
		               dynamicSetQuery.put("recRate",finalRecRatet); 
		               dynamicSetQuery.put("receivableSellDeviation",sellDeviation);  
		           }
		           /*actualRevenueValue=contractRateAmmount/contractExchangeRate;
		           recRate = contractRate/contractExchangeRate;;
				   dynamicSetQuery.put("actualRevenue",actualRevenueValue); 
				   dynamicSetQuery.put("recRate",recRate);*/
		           if(multiCurrency.equalsIgnoreCase("Y")) {
				   actualRevenueForeign = actualRevenueValue*recRateExchange;
				   recCurrencyRate = recRate*recRateExchange;
				   dynamicSetQuery.put("actualRevenueForeign",actualRevenueForeign); 
				   dynamicSetQuery.put("recCurrencyRate",recCurrencyRate);
		           }
		           if(systemDefaultVatCalculation.trim().equalsIgnoreCase("true")){
		           Double recVatPercent=0.00;
				   if(accountLine.getRecVatPercent()!=null && (!(accountLine.getRecVatPercent().trim().equals("")))){
				   recVatPercent=Double.parseDouble(accountLine.getRecVatPercent());
				   }
				   if(res[19] != null && (!(res[19].trim().equalsIgnoreCase(""))) && (res[19].trim().equalsIgnoreCase("Y"))){
					   
				   }else{
					   recVatAmt=(actualRevenueForeign*recVatPercent)/100;
					   dynamicSetQuery.put("recVatAmt",recVatAmt);
				   }
		           }
				    
				   
				   if(dynamicSetQuery.containsKey("contractCurrency")){
		            	accountLine.setContractCurrency(dynamicSetQuery.get("contractCurrency").toString()); 
		            }
				   if(dynamicSetQuery.containsKey("contractExchangeRate")){
		            	accountLine.setContractExchangeRate(new BigDecimal(dynamicSetQuery.get("contractExchangeRate").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("contractValueDate")){
		            	accountLine.setContractValueDate((Date)dynamicSetQuery.get("contractValueDate")); 
		            }
				   if(dynamicSetQuery.containsKey("recRateCurrency")){
		            	accountLine.setRecRateCurrency(dynamicSetQuery.get("recRateCurrency").toString()); 
		            }
				   if(dynamicSetQuery.containsKey("recRateExchange")){
		            	accountLine.setRecRateExchange(new BigDecimal(dynamicSetQuery.get("recRateExchange").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("racValueDate")){
		            	accountLine.setRacValueDate((Date)dynamicSetQuery.get("racValueDate")); 
		            }
				   if(dynamicSetQuery.containsKey("recQuantity")){
		            	accountLine.setRecQuantity(new BigDecimal(dynamicSetQuery.get("recQuantity").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("contractRate")){
		            	accountLine.setContractRate(new BigDecimal(dynamicSetQuery.get("contractRate").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("contractRateAmmount")){
		            	accountLine.setContractRateAmmount(new BigDecimal(dynamicSetQuery.get("contractRateAmmount").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("basis")){
		            	accountLine.setBasis(dynamicSetQuery.get("basis").toString()); 
		            }
				   if(dynamicSetQuery.containsKey("VATExclude")){
		            	accountLine.setVATExclude((Boolean)dynamicSetQuery.get("VATExclude")); 
		            }
				   if(dynamicSetQuery.containsKey("deviation")){
		            	accountLine.setDeviation(dynamicSetQuery.get("deviation").toString()); 
		            }
				   if(dynamicSetQuery.containsKey("buyDependSell")){
		            	accountLine.setBuyDependSell(dynamicSetQuery.get("buyDependSell").toString()); 
		            }
				   if(dynamicSetQuery.containsKey("receivableSellDeviation")){
		            	accountLine.setReceivableSellDeviation(new BigDecimal(dynamicSetQuery.get("receivableSellDeviation").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("actualRevenue")){
		            	accountLine.setActualRevenue(new BigDecimal(dynamicSetQuery.get("actualRevenue").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("recRate")){
		            	accountLine.setRecRate(new BigDecimal(dynamicSetQuery.get("recRate").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("actualRevenueForeign")){
		            	accountLine.setActualRevenueForeign(new BigDecimal(dynamicSetQuery.get("actualRevenueForeign").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("recCurrencyRate")){
		            	accountLine.setRecCurrencyRate(new BigDecimal(dynamicSetQuery.get("recCurrencyRate").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("recVatAmt")){
		            	accountLine.setRecVatAmt(new BigDecimal(dynamicSetQuery.get("recVatAmt").toString())); 
		            }
				   if(dynamicSetQuery.containsKey("description")){
		            	accountLine.setDescription(dynamicSetQuery.get("description").toString()); 
		            } 
		 
		 }
		}
	   }catch(Exception e){
		   e.printStackTrace();  
			  }
	   }
	   accountLine.setUpdatedBy(getRequest().getRemoteUser());
       accountLine.setUpdatedOn(new Date());
	   accountLine=accountLineManager.save(accountLine);
	   if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
	         billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
	         } 
	         if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
	         billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
	         }			
		
		 if(billingCMMContractType){
			 if(serviceOrder.getIsNetworkRecord()){
					if(trackingStatus.getAccNetworkGroup())	{	
					String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
					if(!(networkSynchedId.equals(""))){
						synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
						synchornizeAccountLine(synchedAccountLine,accountLine); 
					}else{
						 if(trackingStatus.getAccNetworkGroup())	{
				             if(serviceOrder.getIsNetworkRecord()){
				              String partnertype="";
				           	  if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
				           		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
				           	  }
				           	  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (!(partnertype.equals("")))  && (accountLine.getRecInvoiceNumber() ==null || accountLine.getRecInvoiceNumber().toString().trim().equals(""))){
				           	  //int j=accountLineManager.updateNetworkSynchedId(accountLine.getId());  
				   				List linkedShipNumber=findLinkerShipNumber(serviceOrder);
				   				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
				   				createAccountLine(serviceOrderRecords,serviceOrder,accountLine); 
				             }}
				           }
						
					}
					}
					}
		 }
		 if(billingDMMContractType){ 
				 if(serviceOrder.getIsNetworkRecord()){
					 if(trackingStatus.getAccNetworkGroup())	{	
					      	String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
					      	if(!(networkSynchedId.equals(""))){
					      		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
					      		synchornizeDMMAccountLine(synchedAccountLine,accountLine,trackingStatus); 
					      	} else{
					      	   if(trackingStatus.getAccNetworkGroup())	{
					               if(serviceOrder.getIsNetworkRecord()){
					            	   //customerFile = serviceOrder.getCustomerFile();
					            	   //String partnertype="";
					            	   boolean billtotype=false;
					              	  if(billing.getNetworkBillToCode()!=null && (!(billing.getNetworkBillToCode().toString().equals("")))  && accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals(""))) && accountLine.getNetworkBillToCode()!=null && (!(accountLine.getNetworkBillToCode().toString().equals("")))){
					              		  //partnertype=  partnerManager.checkPartnerType(customerFile.getAccountCode());
					              		  billtotype =accountLine.getBillToCode().trim().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().trim());
					              	  }	   
					               	if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (billtotype)  && (accountLine.getRecInvoiceNumber() ==null || accountLine.getRecInvoiceNumber().toString().trim().equals(""))){	
					               	    int j=accountLineManager.updateNetworkSynchedId(accountLine.getId());  
					     				List linkedShipNumber=findLinkerShipNumber(serviceOrder);
					     				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					     				createDMMAccountLine(serviceOrderRecords,serviceOrder,accountLine,billing); 
					               }}
					             }
					      		
					      	}
					      	}
				 }
			
			 
		 }
	  }
	  
	  return SUCCESS;
	}
	   private String invoiceNum;
	   private String authortionNum;
	   @SkipValidation
	    public String findInviceList() {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			//String key = "Please enter your search criteria below";
			//saveMessage(getText(key)); 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS;   
	    }
	   @SkipValidation
	    public String searchInvoiceList() { 
	    	try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				
				findInvoiceList=accountLineManager.searchInvoice(invoiceNum,sessionCorpID);
				
				
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
		    	 return "errorlog";
			} 
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS;   
	    }
private String invoiceLineIdNew;
	   @SkipValidation
	    public String updateAuthorization() { 
	    	try {
	    		
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				
				String[] invoiceIdItems=invoiceLineIdNew.split("~");
			    String totalId = "";
				for(int i=0;i<invoiceIdItems.length;i++){	
				    
				   if(totalId!=null && !totalId.equalsIgnoreCase("")){
					   totalId =  totalId+","+"'"+invoiceIdItems[i]+"'";
				   }else{
					   totalId =  "'"+invoiceIdItems[i]+"'";
				   }
				  
				 } 
			
				int i=accountLineManager.updateAuthorizaton(totalId,authortionNum,  sessionCorpID) ;
			     if(i>0){
					
					String key = "Authorization Number Has been Updated Successfully.";  
					saveMessage(getText(key));
				}
		
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
		    	 return "errorlog";
			} 
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS;   
	    }
	   
	   
	   
	   @SkipValidation
	    public String approveRejecValueList() { 
			try {
				//
				accountLineList = accountLineManager.findByPayingStatusAccountLines(shipNumber);
			} catch (Exception e) {
			  	 e.printStackTrace();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  
		    	 return "errorlog";
			} 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;  
	    }
	  

		@SkipValidation
		public String  updatePayingStatus(){
			Date payDate=null;
			if((payingStatusValue.trim().equalsIgnoreCase("A"))) {
				Boolean autuPopPostDate=false;  
	    	List<SystemDefault> sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					autuPopPostDate=systemDefault.getAutoPayablePosting();
					payDate=systemDefault.getPostDate1();
				}
			}
			if(autuPopPostDate){
				//change for posting date flexiblity Start	
				try{
					company=companyManager.findByCorpID(sessionCorpID).get(0);
		    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
			    		Date dt1=new Date();
			    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			    		String dt11=sdfDestination.format(dt1);
			    		dt1=sdfDestination.parse(dt11);
			    		Date dt2 =null;
			    		try{
			    		dt2 = new Date();
			    		}catch(Exception e){
			    			e.printStackTrace();
			    		}				    		
			    		
			    		dt2=payDate;
			    		if(dt2!=null){
				    		if(dt1.compareTo(dt2)>0){
				    			payDate=dt2;
				    		}else{
				    			payDate=dt1;
				    		}
			    		}
		    		}
			}catch(Exception e)
			{
				e.printStackTrace();
			}			
			 
			}
		    } 	
			accountLineManager.updateAccountLinepayingStatusField(accId, payingStatusValue, sessionCorpID,payDate); 
		
		return SUCCESS; 
	}
	   
		
		
		@SkipValidation
		public void accExtract() {
			StringBuffer query= new StringBuffer();	
			StringBuffer fileName= new StringBuffer();	
			String header=new String();
        	serviceOrder=serviceOrderManager.get(sid);
			List accExtractList=accountLineManager.accExtract(sessionCorpID, serviceOrder.getShipNumber());
			SimpleDateFormat format = new SimpleDateFormat("MMddyyyy");
		    StringBuilder systemDate = new StringBuilder(format.format(new Date()));

			try {

				if (!accExtractList.isEmpty()) {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					File file1 = new File("AccExtract");
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls");
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					//            1                      2                    3                  4                  5                      6                 7                        8                      
					header = "SO#" + "\t" + "First Name"+ "\t" +"Last Name" + "\t" + "Job Type" + "\t" + " Bill to Code" + "\t" + " Bill to Name " + "\t"+" Acc#" + "\t"+ "Charge Code" + "\t" + " Estimate Expense" + "\t";
					//                   9
					header=header+ " Mk% Est" + "\t"; 
					//              	10
					header=header+ " Estimate Revenue" + "\t"; 
					//                   11
					header=header+ " Revision Expense " + "\t"; 
					//                 12
					header=header+ " Mk% Rev " + "\t" ;
					//                  13
					header=header+ "Revision Revenue " + "\t" ;
					//                  14
					header=header+ " Vendor Invoice# " + "\t";
					//                  15
					header=header+ " Vendor Name" + "\t" ;
					//                  16
					header=header+ " Exp Description " + "\t" ;
					//                   17
					header=header+ " Actual Expense" + "\t" ;
					
					header=header+ " P/S " + "\t" ;
					
					header=header+ " Actual Revenue " + "\t" ;
					//                   19
					header=header+ " Invoice# " + "\t";
					//                   20
					header=header+ " Div " + "\t" ;
					//                    21
					header=header+ " Invoice Date "+ "\t" ;
					//                     22
					header=header+ " Revenue Description" + "\t" ;
					//                  23
					header=header+ " Category" + "\t" ;
					//                 24
					header=header+ " Active Acc Portal \t" ;
					//                 25
					header=header+ " ACT \t" ;
					//                 25
		
					header=header+ " Inv \t" ;
					//                 25
					
			
		
					header=header+ "\n";
					outputStream.write(header.getBytes());

					Iterator it = accExtractList.iterator();  
					//state = refMasterManager.findStateCodeDescription(sessionCorpID, "STATE");
					while (it.hasNext()) {
						Object[] row = (Object[]) it.next();
						//String desc=state.get(row[47].toString()+"~"+row[52].toString());
						if (row[0] != null) {
							String shipNo = row[0].toString();
							outputStream.write(("`" + shipNo + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						
						if (row[1] != null) {
							if (row[1].toString().trim().equals("")) {
								outputStream.write(("" + "\t").getBytes());
							} else {
								outputStream.write((row[1].toString() + "\t").getBytes());
							}

						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[2] != null) {
							
							outputStream.write((row[2].toString() + "\t").getBytes());
						
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[3] != null) {
							
							outputStream.write((row[3].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[4] != null) {
							outputStream.write((row[4].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[5] != null) {
							outputStream.write((row[5].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[6] != null) {
							outputStream.write((row[6].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[7] != null) {
							outputStream.write((row[7] + "\t").getBytes());

						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[8] != null) {
							outputStream.write((row[8] + "\t").getBytes());

						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[9] != null) {
							outputStream.write((row[9] + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[10] != null) {
							outputStream.write(( row[10] + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[11] != null) {
							outputStream.write((row[11].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[12] != null) {
							outputStream.write((row[12].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[13] != null) {
							outputStream.write((row[13].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[14] != null) {
							outputStream.write((row[14].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}

						if (row[15] != null) {
							outputStream.write((row[15].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						

						if (row[16] != null) {
							outputStream.write((row[16].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[17] != null) {
							outputStream.write((row[17].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[18] != null && !row[18].equals("")) {
				       String payingStatusrow =row[18].toString();
							if((payingStatusrow.trim().equalsIgnoreCase("D"))){
								outputStream.write(("Awaiting approval"+ "\t").getBytes());
								}
							if((payingStatusrow.trim().equalsIgnoreCase("W"))){
								outputStream.write(("Awaiting approval (amount/currency changed)" + "\t").getBytes());
								}
							if((payingStatusrow.trim().equalsIgnoreCase("A"))){
									outputStream.write(("Approved"+ "\t").getBytes());
									}
							if((payingStatusrow.trim().equalsIgnoreCase("I"))){
								outputStream.write(("Internal Cost"+ "\t").getBytes());
								}
					
							if((payingStatusrow.trim().equalsIgnoreCase("N"))){
								outputStream.write(("Not Authorized"+ "\t").getBytes());
								}
							if((payingStatusrow.trim().equalsIgnoreCase("P"))){
								outputStream.write(("Pending with note"+ "\t").getBytes());
								}
							if((payingStatusrow.trim().equalsIgnoreCase("R"))){
								outputStream.write(("Rejected"+ "\t").getBytes());
								}
							if((payingStatusrow.trim().equalsIgnoreCase("S"))){
								outputStream.write(("Short pay with notes"+ "\t").getBytes());
								}
						
						} else {
							outputStream.write((" "+ "\t").getBytes());

						}
						if (row[19] != null) {
							outputStream.write((row[19].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[20] != null) {
							outputStream.write((row[20].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[21] != null) {
							outputStream.write((row[21].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[22] != null) {
							outputStream.write((row[22].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());

						}
						if (row[23] != null) {
							outputStream.write((row[23].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[24] != null) {
							outputStream.write((row[24].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[25] != null) {
							outputStream.write((row[25].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[26] != null) {
							outputStream.write((row[26].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[27] != null) {
							outputStream.write((row[27].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						outputStream.write("\n".getBytes());
			
						}
						

					

				}

				else {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					File file1 = new File("AccExtract");
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls");
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					header = "NO Record Found , thanks";
					outputStream.write(header.getBytes());
				}
			} catch (Exception e) {
				System.out.println("ERROR:" + e);
			}
		}
		
		 @SkipValidation
		    public String searchAccountInvoiceListajax() { 
		    	try {
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
					
					findInvoiceList=accountLineManager.searchInvoiceAjax(shipNumber, sessionCorpID);
					
					
				} catch (Exception e) {
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	 e.printStackTrace();
			    	 return "errorlog";
				} 
		    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		        return SUCCESS;   
		    }	

private  Map<String, String> vatDescription1 = new LinkedHashMap<String, String>();
	public Map<String, String> getVatDescription1() {
	return vatDescription1;
}

public void setVatDescription1(Map<String, String> vatDescription1) {
	this.vatDescription1 = vatDescription1;
}
@SkipValidation
public String  recVatAccountLine() {                   
String flagb="";
	String vatBillimngGroupCode="";
	String partnerVatDec="";
	String vatBillimngGroupDes="";
	vatBillingGroups = refMasterManager.findByParameter(sessionCorpID, "vatBillingGroup");
	List vatBillimngGroupCode1=partnerPrivateManager.findVatBillimngGroup(sessionCorpID, partnerCode);
	Iterator listIterator= vatBillimngGroupCode1.iterator();
	while (listIterator.hasNext()) {
	 Object [] row=(Object[])listIterator.next();
		if(row[0]!=null) {
		 vatBillimngGroupCode=row[0].toString();  
		 }
		 if(row[1]!=null){
		partnerVatDec = row[1].toString();  
		}
		}
	if(vatBillingGroups.containsKey(vatBillimngGroupCode)){
		vatBillimngGroupDes =vatBillingGroups.get(vatBillimngGroupCode);
	}
		 flex1  = refMasterManager.findPartnerVatBillingCode(vatBillimngGroupDes, sessionCorpID,companyDivision);
		if(vatBillimngGroupCode!=null && !vatBillimngGroupCode.equals("")){
		vatDescription=refMasterManager.findAccountLineRecVatCodeDescription(flex1, recVatDescr,partnerVatDec, sessionCorpID,"Y");
		}
		 else{
		vatDescription=refMasterManager.findAccountLineRecVatCodeDescription(flex1, recVatDescr,partnerVatDec, sessionCorpID,flagb);
		}
	   
	return SUCCESS;
			}
	@SkipValidation
	public String payVatAccountLine() {	
	String flag="";
	String vatBillimngGroupCode="";
	String vatBillimngGroupDes="";
	String partnerVatDec="";
	vatBillingGroups = refMasterManager.findByParameter(sessionCorpID, "vatBillingGroup");
	List vatBillimngGroupCode1=partnerPrivateManager.findVatBillimngGroup(sessionCorpID, partnerCode);
			Iterator listIterator= vatBillimngGroupCode1.iterator();
			while (listIterator.hasNext()) {
					 Object [] row=(Object[])listIterator.next();
					  if(row[0]!=null) {
						  vatBillimngGroupCode=row[0].toString();  
					  }
					 
				}
			if(vatBillingGroups.containsKey(vatBillimngGroupCode)){
				vatBillimngGroupDes =vatBillingGroups.get(vatBillimngGroupCode);
			}
			  flex2  = refMasterManager.findAccountLinePayVatBillingCode(vatBillimngGroupDes, sessionCorpID,companyDivision);
			  if(vatBillimngGroupCode!=null && !vatBillimngGroupCode.equals("")){
				  vatDescription=refMasterManager.findAccountLinePayVatCodeDescription(flex2, payVatDescr, sessionCorpID, "Y");
			  }
			  else{
				  vatDescription=refMasterManager.findAccountLinePayVatCodeDescription(flex2, payVatDescr, sessionCorpID, flag);
				  }

   
    	return SUCCESS;
    	}
			
	public String getTotalRevenue() {
		return totalRevenue;
	}
	public void setTotalRevenue(String totalRevenue) {
		this.totalRevenue = totalRevenue;
	}
	public String getExpressionFormula() {
		return expressionFormula;
	}
	public void setExpressionFormula(String expressionFormula) {
		this.expressionFormula = expressionFormula;
	}
	
	public String getRecPostDate() {
		return recPostDate;
	}
	public void setRecPostDate(String recPostDate) {
		this.recPostDate = recPostDate;
	}
	public String getDateStatus() {
		return dateStatus;
	}
	public void setDateStatus(String dateStatus) {
		this.dateStatus = dateStatus;
	}
	public List getFindOurInviceList() {
		return findOurInviceList;
	}
	public void setFindOurInviceList(List findOurInviceList) {
		this.findOurInviceList = findOurInviceList;
	}
	public String getBillToCode() {
		return billToCode;
	}
	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	public String getFromField() {
		return fromField;
	}
	public void setFromField(String fromField) {
		this.fromField = fromField;
	}
	public List getFindVendorInviceList() {
		return findVendorInviceList;
	}
	public void setFindVendorInviceList(List findVendorInviceList) {
		this.findVendorInviceList = findVendorInviceList;
	}
	public List getMaxLineNumber() {
		return maxLineNumber;
	}
	public void setMaxLineNumber(List maxLineNumber) {
		this.maxLineNumber = maxLineNumber;
	}
	public String getAccountLineNumber() {
		return accountLineNumber;
	}
	public void setAccountLineNumber(String accountLineNumber) {
		this.accountLineNumber = accountLineNumber;
	}
	public Long getAutoLineNumber() {
		return autoLineNumber;
	}
	public void setAutoLineNumber(Long autoLineNumber) {
		this.autoLineNumber = autoLineNumber;
	}
	public String getGotoPageString() {
		return gotoPageString;
	}
	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}
	public String getValidateFormNav() {
		return validateFormNav;
	}
	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public List getWorkTickets() {
		return workTickets;
	}
	public void setWorkTickets(List workTickets) {
		this.workTickets = workTickets;
	}
	public String getShipNumberForTickets() {
		return shipNumberForTickets;
	}
	public void setShipNumberForTickets(String shipNumberForTickets) {
		this.shipNumberForTickets = shipNumberForTickets;
	}
	public List getGetCommissionList() {
		return getCommissionList;
	}
	public void setGetCommissionList(List getCommissionList) {
		this.getCommissionList = getCommissionList;
	}
	public List getGetGlTypeList() {
		return getGlTypeList;
	}
	public void setGetGlTypeList(List getGlTypeList) {
		this.getGlTypeList = getGlTypeList;
	}
	public  Map<String, String> getPaymentStatus() {
		return paymentStatus;
	}
	public String getGlType() {
		return glType;
	}
	public void setGlType(String glType) {
		this.glType = glType;
	}
	public String getActualRevanue() {
		return actualRevanue;
	}
	public void setActualRevanue(String actualRevanue) {
		this.actualRevanue = actualRevanue;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getChargeCode() {
		return chargeCode;
	}
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}
	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public List getAuthorizationNoList() {
		return authorizationNoList;
	}
	public void setAuthorizationNoList(List authorizationNoList) {
		this.authorizationNoList = authorizationNoList;
	}
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	public String getDistAmt() {
		return distAmt;
	}
	public void setDistAmt(String distAmt) {
		this.distAmt = distAmt;
	}
	public String getGlcomm() {
		return glcomm;
	}
	public void setGlcomm(String glcomm) {
		this.glcomm = glcomm;
	}
	public VanLineCommissionType getVanLineCommissionType() {
		return vanLineCommissionType;
	}
	public void setVanLineCommissionType(VanLineCommissionType vanLineCommissionType) {
		this.vanLineCommissionType = vanLineCommissionType;
	}
	
	
	public String getCheckCropId() {
		return checkCropId;
	}
	public void setCheckCropId(String checkCropId) {
		this.checkCropId = checkCropId;
	}
	public TrackingStatus getTrackingStatus() { 
		return( trackingStatus!=null )?trackingStatus:trackingStatusManager.get(sid);
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}
	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public List getSystemDefaultValueList() {
		return systemDefaultValueList;
	}
	public void setSystemDefaultValueList(List systemDefaultValueList) {
		this.systemDefaultValueList = systemDefaultValueList;
	}

	
	public Date getProcessingDate() {
		return processingDate;
	}
	public void setProcessingDate(Date processingDate) {
		this.processingDate = processingDate;
	}
	public String getUploadBillToCode() {
		return uploadBillToCode;
	}
	public void setUploadBillToCode(String uploadBillToCode) {
		this.uploadBillToCode = uploadBillToCode;
	}
	public String getSalesCommisionRate() {
		return salesCommisionRate;
	}
	public void setSalesCommisionRate(String salesCommisionRate) {
		this.salesCommisionRate = salesCommisionRate;
	}
	public String getGrossMarginThreshold() {
		return grossMarginThreshold;
	}
	public void setGrossMarginThreshold(String grossMarginThreshold) {
		this.grossMarginThreshold = grossMarginThreshold;
	} 
	public String getFileFileName() {
		return fileFileName;
	}
	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public List getReverseInvoiceList() {
		return reverseInvoiceList;
	}
	public void setReverseInvoiceList(List reverseInvoiceList) {
		this.reverseInvoiceList = reverseInvoiceList;
	}
	public String getRecInvoiceNumber() {
		return recInvoiceNumber;
	}
	public void setRecInvoiceNumber(String recInvoiceNumber) {
		this.recInvoiceNumber = recInvoiceNumber;
	}
	public AccountLine getAccountLineReverse() {
		return accountLineReverse;
	}
	public void setAccountLineReverse(AccountLine accountLineReverse) {
		this.accountLineReverse = accountLineReverse;
	}
	
	/**
	 * @return the accountInterface
	 */
	public  String getAccountInterface() {
		return accountInterface;
	}
	/**
	 * @param accountInterface the accountInterface to set
	 */
	public  void setAccountInterface(String accountInterface) {
		this.accountInterface = accountInterface;
	}
	/**
	 * @return the systemDefaultmiscVl
	 */
	public  String getSystemDefaultmiscVl() {
		return systemDefaultmiscVl;
	}
	/**
	 * @param systemDefaultmiscVl the systemDefaultmiscVl to set
	 */
	public  void setSystemDefaultmiscVl(String systemDefaultmiscVl) {
		this.systemDefaultmiscVl = systemDefaultmiscVl;
	}
	/**
	 * @return the systemDefaultstorage
	 */
	public  String getSystemDefaultstorage() {
		return systemDefaultstorage;
	}
	/**
	 * @param systemDefaultstorage the systemDefaultstorage to set
	 */
	public  void setSystemDefaultstorage(String systemDefaultstorage) {
		this.systemDefaultstorage = systemDefaultstorage;
	}
	/**
	 * @return the billingContract
	 */
	public String getBillingContract() {
		return billingContract;
	}
	/**
	 * @param billingContract the billingContract to set
	 */
	public void setBillingContract(String billingContract) {
		this.billingContract = billingContract;
	}
	public List getActualSenttoDatesList() {
		return actualSenttoDatesList;
	}
	public void setActualSenttoDatesList(List actualSenttoDatesList) {
		this.actualSenttoDatesList = actualSenttoDatesList;
	}
	public String getRecInvoiceCheck() {
		return recInvoiceCheck;
	}
	public void setRecInvoiceCheck(String recInvoiceCheck) {
		this.recInvoiceCheck = recInvoiceCheck;
	}
	public List getBillingWizardList() {
		return BillingWizardList;
	}
	public void setBillingWizardList(List billingWizardList) {
		BillingWizardList = billingWizardList;
	}
	public List getNonInvoiceBillingList() {
		return nonInvoiceBillingList;
	}
	public void setNonInvoiceBillingList(List nonInvoiceBillingList) {
		this.nonInvoiceBillingList = nonInvoiceBillingList;
	}
	public String getRecInvNumBilling() {
		return recInvNumBilling;
	}
	public void setRecInvNumBilling(String recInvNumBilling) {
		this.recInvNumBilling = recInvNumBilling;
	}
	public List getInvoiceDetailList() {
		return invoiceDetailList;
	}
	public void setInvoiceDetailList(List invoiceDetailList) {
		this.invoiceDetailList = invoiceDetailList;
	}
	public BigDecimal getActRevSumBilling() {
		return actRevSumBilling;
	}
	public void setActRevSumBilling(BigDecimal actRevSumBilling) {
		this.actRevSumBilling = actRevSumBilling;
	}
	public BigDecimal getRevRevAmtSumBilling() {
		return revRevAmtSumBilling;
	}
	public void setRevRevAmtSumBilling(BigDecimal revRevAmtSumBilling) {
		this.revRevAmtSumBilling = revRevAmtSumBilling;
	}
	public BigDecimal getEstRevAmtSumBilling() {
		return estRevAmtSumBilling;
	}
	public void setEstRevAmtSumBilling(BigDecimal estRevAmtSumBilling) {
		this.estRevAmtSumBilling = estRevAmtSumBilling;
	}
	public BigDecimal getDistAmtSumBilling() {
		return distAmtSumBilling;
	}
	public void setDistAmtSumBilling(BigDecimal distAmtSumBilling) {
		this.distAmtSumBilling = distAmtSumBilling;
	}
	public String getListData() {
		return listData;
	}
	public void setListData(String listData) {
		this.listData = listData;
	}
	public String getListFieldNames() {
		return listFieldNames;
	}
	public void setListFieldNames(String listFieldNames) {
		this.listFieldNames = listFieldNames;
	}
	public String getListFieldEditability() {
		return listFieldEditability;
	}
	public void setListFieldEditability(String listFieldEditability) {
		this.listFieldEditability = listFieldEditability;
	}
	public String getListFieldTypes() {
		return listFieldTypes;
	}
	public void setListFieldTypes(String listFieldTypes) {
		this.listFieldTypes = listFieldTypes;
	}
	public String getListIdField() {
		return listIdField;
	}
	public void setListIdField(String listIdField) {
		this.listIdField = listIdField;
	}
	public List getBilingbasis() {
		return bilingbasis;
	}
	public void setBilingbasis(List bilingbasis) {
		this.bilingbasis = bilingbasis;
	}
	public List getBillingcheckNew() {
		return billingcheckNew;
	}
	public void setBillingcheckNew(List billingcheckNew) {
		this.billingcheckNew = billingcheckNew;
	}
	public List getNonInvoiceList() {
		return nonInvoiceList;
	}
	public void setNonInvoiceList(List nonInvoiceList) {
		this.nonInvoiceList = nonInvoiceList;
	}
	public String getBillingContractFlag() {
		return billingContractFlag;
	}
	public void setBillingContractFlag(String billingContractFlag) {
		this.billingContractFlag = billingContractFlag;
	}
	public String getDateUpdate() {
		return dateUpdate;
	}
	public void setDateUpdate(String dateUpdate) {
		this.dateUpdate = dateUpdate;
	}
	public String getMultiCurrency() {
		return multiCurrency;
	}
	public void setMultiCurrency(String multiCurrency) {
		this.multiCurrency = multiCurrency;
	}
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	public List getTotalInvoiceList() {
		return totalInvoiceList;
	}
	public void setTotalInvoiceList(List totalInvoiceList) {
		this.totalInvoiceList = totalInvoiceList;
	}
	public List getTotalVendorInvoiceList() {
		return totalVendorInvoiceList;
	}
	public void setTotalVendorInvoiceList(List totalVendorInvoiceList) {
		this.totalVendorInvoiceList = totalVendorInvoiceList;
	}
	public Map<String, String> getRecRateCurrency() {
		return recRateCurrency;
	}
	public void setRecRateCurrency(Map<String, String> recRateCurrency) {
		this.recRateCurrency = recRateCurrency;
	}
	public List getPaidInvoiceList() {
		return paidInvoiceList;
	}
	public void setPaidInvoiceList(List paidInvoiceList) {
		this.paidInvoiceList = paidInvoiceList;
	}
	
	public String getNotPaidInvoiceNumber() {
		return notPaidInvoiceNumber;
	}
	public void setNotPaidInvoiceNumber(String notPaidInvoiceNumber) {
		this.notPaidInvoiceNumber = notPaidInvoiceNumber;
	}
	public String getNotPaidBillToCode() {
		return notPaidBillToCode;
	}
	public void setNotPaidBillToCode(String notPaidBillToCode) {
		this.notPaidBillToCode = notPaidBillToCode;
	}
	public String getNotPaidBillToName() {
		return notPaidBillToName;
	}
	public void setNotPaidBillToName(String notPaidBillToName) {
		this.notPaidBillToName = notPaidBillToName;
	}
	public List getInvoiceToDeleteList() {
		return invoiceToDeleteList;
	}
	public void setInvoiceToDeleteList(List invoiceToDeleteList) {
		this.invoiceToDeleteList = invoiceToDeleteList;
	}
	public String getInvoiceToDelete() {
		return invoiceToDelete;
	}
	public void setInvoiceToDelete(String invoiceToDelete) {
		this.invoiceToDelete = invoiceToDelete;
	}
	public String getIdCheck() {
		return idCheck;
	}
	public void setIdCheck(String idCheck) {
		this.idCheck = idCheck;
	}
	public String getAccountIdCheck() {
		return accountIdCheck;
	}
	public void setAccountIdCheck(String accountIdCheck) {
		this.accountIdCheck = accountIdCheck;
	}
	public String getOldRecRateCurrency() {
		return oldRecRateCurrency;
	}
	public void setOldRecRateCurrency(String oldRecRateCurrency) {
		this.oldRecRateCurrency = oldRecRateCurrency;
	}
	
	
	private String shipSize;
	private String minShip;
	private String countShip;
	private Long maxAccountLine;
	private Long minAccountLine;
	private String countAccountLine;
	private Long soIdNum;
	private Long sidNum;
	private Long tempAccLine;
	private List accLinetSO;
	private String urlValue;
	@SkipValidation 
    public String editNextAccountLine(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		tempAccLine=Long.parseLong((accountLineManager.goNextSOChild(sidNum,sessionCorpID,soIdNum)).get(0).toString());
		urlValue = "sid="+sidNum+"&id="+tempAccLine;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    }
    @SkipValidation 
    public String editPrevAccountLine(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		tempAccLine=Long.parseLong((accountLineManager.goPrevSOChild(sidNum,sessionCorpID,soIdNum)).get(0).toString());
		urlValue = "sid="+sidNum+"&id="+tempAccLine;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

    	return SUCCESS;   
    } 
      
    @SkipValidation 
    public String accountLineOtherSO(){
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			accLinetSO=accountLineManager.goSOChild(sidNum,sessionCorpID,soIdNum);
		} catch (Exception e) {
		  	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

		  	return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    }
    private String agentParentList1;
    @SkipValidation 
    public String findRollUpInvoiceValFromChargeAjax(){
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			agentParentList1 = accountLineManager.findRollUpInvoiceValFromCharge(contract,chargeCode,sessionCorpID);	
    	} catch (Exception e) {
		  	 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

		  	return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
	public String getAgentParentList1() {
		return agentParentList1;
	}

	public void setAgentParentList1(String agentParentList1) {
		this.agentParentList1 = agentParentList1;
	}
	public Map<String, String> getQstEuvatRecGLMap() {
		return qstEuvatRecGLMap;
	}

	public void setQstEuvatRecGLMap(Map<String, String> qstEuvatRecGLMap) {
		this.qstEuvatRecGLMap = qstEuvatRecGLMap;
	}

	public Map<String, String> getQstPayVatPayGLMap() {
		return qstPayVatPayGLMap;
	}

	public void setQstPayVatPayGLMap(Map<String, String> qstPayVatPayGLMap) {
		this.qstPayVatPayGLMap = qstPayVatPayGLMap;
	}

	public Map<String, String> getQstEuvatRecGLAmtMap() {
		return qstEuvatRecGLAmtMap;
	}

	public void setQstEuvatRecGLAmtMap(Map<String, String> qstEuvatRecGLAmtMap) {
		this.qstEuvatRecGLAmtMap = qstEuvatRecGLAmtMap;
	}

	public Map<String, String> getQstPayVatPayGLAmtMap() {
		return qstPayVatPayGLAmtMap;
	}

	public void setQstPayVatPayGLAmtMap(Map<String, String> qstPayVatPayGLAmtMap) {
		this.qstPayVatPayGLAmtMap = qstPayVatPayGLAmtMap;
	}
	public List getAccLinetSO() {
		return accLinetSO;
	}
	public void setAccLinetSO(List accLinetSO) {
		this.accLinetSO = accLinetSO;
	}
	public Long getSidNum() {
		return sidNum;
	}
	public void setSidNum(Long sidNum) {
		this.sidNum = sidNum;
	}
	public Long getSoIdNum() {
		return soIdNum;
	}
	public void setSoIdNum(Long soIdNum) {
		this.soIdNum = soIdNum;
	}
	public String getCountAccountLine() {
		return countAccountLine;
	}
	public void setCountAccountLine(String countAccountLine) {
		this.countAccountLine = countAccountLine;
	}
	public String getCountShip() {
		return countShip;
	}
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}
	public String getMinShip() {
		return minShip;
	}
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}
	public String getShipSize() {
		return shipSize;
	}
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}
	public Long getMaxAccountLine() {
		return maxAccountLine;
	}
	public void setMaxAccountLine(Long maxAccountLine) {
		this.maxAccountLine = maxAccountLine;
	}
	public Long getMinAccountLine() {
		return minAccountLine;
	}
	public void setMinAccountLine(Long minAccountLine) {
		this.minAccountLine = minAccountLine;
	}
	public Long getTempAccLine() {
		return tempAccLine;
	}
	public void setTempAccLine(Long tempAccLine) {
		this.tempAccLine = tempAccLine;
	}
	public String getDetailPage() {
		return detailPage;
	}
	public void setDetailPage(String detailPage) {
		this.detailPage = detailPage;
	}
	public List getFindMilitaryBaseList() {
		return findMilitaryBaseList;
	}
	public void setFindMilitaryBaseList(List findMilitaryBaseList) {
		this.findMilitaryBaseList = findMilitaryBaseList;
	}
	public String getBaseCode() {
		return baseCode;
	}
	public void setBaseCode(String baseCode) {
		this.baseCode = baseCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTerminalState() {
		return terminalState;
	}
	public void setTerminalState(String terminalState) {
		this.terminalState = terminalState;
	}
	/**
	 * @return the sCACCode
	 */
	public String getSCACCode() {
		return SCACCode;
	}
	/**
	 * @param code the sCACCode to set
	 */
	public void setSCACCode(String code) {
		SCACCode = code;
	} 
	public List getCreditInvoiceList() {
		return creditInvoiceList;
	}
	public void setCreditInvoiceList(List creditInvoiceList) {
		this.creditInvoiceList = creditInvoiceList;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public List getEmailInvoiceList() {
		return emailInvoiceList;
	}
	public void setEmailInvoiceList(List emailInvoiceList) {
		this.emailInvoiceList = emailInvoiceList;
	}
	public String getHitflag() {
		return hitflag;
	}
	public void setHitflag(String hitflag) {
		this.hitflag = hitflag;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public List getVanPayChargesList() {
		return vanPayChargesList;
	}

	public void setVanPayChargesList(List vanPayChargesList) {
		this.vanPayChargesList = vanPayChargesList;
	}

	public List getDomCommChargesList() {
		return domCommChargesList;
	}

	public void setDomCommChargesList(List domCommChargesList) {
		this.domCommChargesList = domCommChargesList;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getSystemDefaultCommissionJob() {
		return systemDefaultCommissionJob;
	}

	public void setSystemDefaultCommissionJob(String systemDefaultCommissionJob) {
		this.systemDefaultCommissionJob = systemDefaultCommissionJob;
	}
    public String getActualAmt() {
		return actualAmt;
	}

	public void setActualAmt(String actualAmt) {
		this.actualAmt = actualAmt;
	}

	public String getCheckLHF() {
		return checkLHF;
	}

	public void setCheckLHF(String checkLHF) {
		this.checkLHF = checkLHF;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	} 

	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}

	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}

	public String getAccountCompanyDivision() {
		return accountCompanyDivision;
	}

	public void setAccountCompanyDivision(String accountCompanyDivision) {
		this.accountCompanyDivision = accountCompanyDivision;
	}

	public Map<String, String> getEuVatList() {
		return euVatList;
	}

	public void setEuVatList(Map<String, String> euVatList) {
		this.euVatList = euVatList;
	}

	public Map<String, String> getEuVatPercentList() {
		return euVatPercentList;
	}

	public void setEuVatPercentList(Map<String, String> euVatPercentList) {
		this.euVatPercentList = euVatPercentList;
	}

	public String getCompanyDivisionAcctgCodeUnique() {
		return companyDivisionAcctgCodeUnique;
	}

	public void setCompanyDivisionAcctgCodeUnique(
			String companyDivisionAcctgCodeUnique) {
		this.companyDivisionAcctgCodeUnique = companyDivisionAcctgCodeUnique;
	}

	public String getDestinationCountryCode() {
		return destinationCountryCode;
	}

	public void setDestinationCountryCode(String destinationCountryCode) {
		this.destinationCountryCode = destinationCountryCode;
	}

	public String getOriginCountryCode() {
		return originCountryCode;
	}

	public void setOriginCountryCode(String originCountryCode) {
		this.originCountryCode = originCountryCode;
	}

	public String getCountryCodeEUResult() {
		return countryCodeEUResult;
	}

	public void setCountryCodeEUResult(String countryCodeEUResult) {
		this.countryCodeEUResult = countryCodeEUResult;
	}

	public Map<String, String> getPayVatList() {
		return payVatList;
	}

	public void setPayVatList(Map<String, String> payVatList) {
		this.payVatList = payVatList;
	}

	public Map<String, String> getPayVatPercentList() {
		return payVatPercentList;
	}

	public void setPayVatPercentList(Map<String, String> payVatPercentList) {
		this.payVatPercentList = payVatPercentList;
	}

	/**
	 * @return the countInvoice
	 */
	public String getCountInvoice() {
		return countInvoice;
	}

	/**
	 * @param countInvoice the countInvoice to set
	 */
	public void setCountInvoice(String countInvoice) {
		this.countInvoice = countInvoice;
	} 

	public String getAccCorpID() {
		return accCorpID;
	}

	public void setAccCorpID(String accCorpID) {
		this.accCorpID = accCorpID;
	}

	public Integer getCheckRecInvoiceForHVY() {
		return checkRecInvoiceForHVY;
	}

	public void setCheckRecInvoiceForHVY(Integer checkRecInvoiceForHVY) {
		this.checkRecInvoiceForHVY = checkRecInvoiceForHVY;
	}

	/**
	 * @return the totalOfInvoiceList
	 */
	public List getTotalOfInvoiceList() {
		return totalOfInvoiceList;
	}

	/**
	 * @param totalOfInvoiceList the totalOfInvoiceList to set
	 */
	public void setTotalOfInvoiceList(List totalOfInvoiceList) {
		this.totalOfInvoiceList = totalOfInvoiceList;
	}

	public String getBaseCurrencyCompanyDivision() {
		return baseCurrencyCompanyDivision;
	}
	public void setBaseCurrencyCompanyDivision(String baseCurrencyCompanyDivision) {
		this.baseCurrencyCompanyDivision = baseCurrencyCompanyDivision;
	}

	/**
	 * @param companyDivisionManager the companyDivisionManager to set
	 */
	public void setCompanyDivisionManager(
			CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}

	public List getAccExchangeRateList() {
		return accExchangeRateList;
	}

	public void setAccExchangeRateList(List accExchangeRateList) {
		this.accExchangeRateList = accExchangeRateList;
	}

	public void setExchangeRateManager(ExchangeRateManager exchangeRateManager) {
		this.exchangeRateManager = exchangeRateManager;
	}

	public String getSystemDefaultVatCalculation() {
		return systemDefaultVatCalculation;
	}

	public void setSystemDefaultVatCalculation(String systemDefaultVatCalculation) {
		this.systemDefaultVatCalculation = systemDefaultVatCalculation;
	}

	public String getMasterId() {
		return masterId;
	}

	public void setMasterId(String masterId) {
		this.masterId = masterId;
	}

	public Boolean getCostElementFlag() {
		return costElementFlag;
	}

	public void setCostElementFlag(Boolean costElementFlag) {
		this.costElementFlag = costElementFlag;
	}

	public String getInvoiceCheck() {
		return invoiceCheck;
	}

	public void setInvoiceCheck(String invoiceCheck) {
		this.invoiceCheck = invoiceCheck;
	}

	public List getValuationList() {
		return valuationList;
	}

	public void setValuationList(List valuationList) {
		this.valuationList = valuationList;
	}

	public List getTransportationList() {
		return transportationList;
	}

	public void setTransportationList(List transportationList) {
		this.transportationList = transportationList;
	}

	public List getUnderLyingHaulList() {
		return underLyingHaulList;
	}

	public void setUnderLyingHaulList(List underLyingHaulList) {
		this.underLyingHaulList = underLyingHaulList;
	}

	public String getTransportSum() {
		return transportSum;
	}

	public void setTransportSum(String transportSum) {
		this.transportSum = transportSum;
	}

	public String getValuationSum() {
		return valuationSum;
	}

	public void setValuationSum(String valuationSum) {
		this.valuationSum = valuationSum;
	}

	public List getPackingList() {
		return packingList;
	}

	public void setPackingList(List packingList) {
		this.packingList = packingList;
	}

	public List getPackingContList() {
		return packingContList;
	}

	public void setPackingContList(List packingContList) {
		this.packingContList = packingContList;
	}

	public List getPackingUnpackList() {
		return packingUnpackList;
	}

	public void setPackingUnpackList(List packingUnpackList) {
		this.packingUnpackList = packingUnpackList;
	}

	public List getOtherMiscellaneousList() {
		return otherMiscellaneousList;
	}

	public void setOtherMiscellaneousList(List otherMiscellaneousList) {
		this.otherMiscellaneousList = otherMiscellaneousList;
	}

	public String getPackingSum() {
		return packingSum;
	}

	public void setPackingSum(String packingSum) {
		this.packingSum = packingSum;
	}

	public String getPackContSum() {
		return packContSum;
	}

	public void setPackContSum(String packContSum) {
		this.packContSum = packContSum;
	}

	public String getPackUnpackSum() {
		return packUnpackSum;
	}

	public void setPackUnpackSum(String packUnpackSum) {
		this.packUnpackSum = packUnpackSum;
	}

	public String getOthersSum() {
		return othersSum;
	}

	public void setOthersSum(String othersSum) {
		this.othersSum = othersSum;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getEstVatFlag() {
		return estVatFlag;
	}

	public void setEstVatFlag(String estVatFlag) {
		this.estVatFlag = estVatFlag;
	}

	public boolean isContractType() {
		return contractType;
	}

	public void setContractType(boolean contractType) {
		this.contractType = contractType;
	}

	public String getContractTypeValue() {
		return (contractTypeValue!=null && !contractTypeValue.equals(""))?contractTypeValue:accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
	}
	
	public void setContractTypeValue(String contractTypeValue) {
		this.contractTypeValue = contractTypeValue;
	}
	public ServiceOrder getServiceOrderToRecods() {
		return serviceOrderToRecods;
	}

	public void setServiceOrderToRecods(ServiceOrder serviceOrderToRecods) {
		this.serviceOrderToRecods = serviceOrderToRecods;
	}

	public AccountLine getSynchedAccountLine() {
		return synchedAccountLine;
	}

	public void setSynchedAccountLine(AccountLine synchedAccountLine) {
		this.synchedAccountLine = synchedAccountLine;
	}

	public TrackingStatus getTrackingStatusToRecods() {
		return trackingStatusToRecods;
	}

	public void setTrackingStatusToRecods(TrackingStatus trackingStatusToRecods) {
		this.trackingStatusToRecods = trackingStatusToRecods;
	}

	public Billing getBillingRecods() {
		return billingRecods;
	}

	public void setBillingRecods(Billing billingRecods) {
		this.billingRecods = billingRecods;
	}

	public boolean isBillingCMMContractType() {
		return billingCMMContractType;
	}

	public void setBillingCMMContractType(boolean billingCMMContractType) {
		this.billingCMMContractType = billingCMMContractType;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public boolean isBillingDMMContractType() {
		return billingDMMContractType;
	}

	public void setBillingDMMContractType(boolean billingDMMContractType) {
		this.billingDMMContractType = billingDMMContractType;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getBillingAdd1() {
		return billingAdd1;
	}

	public void setBillingAdd1(String billingAdd1) {
		this.billingAdd1 = billingAdd1;
	}

	public String getBillingAdd2() {
		return billingAdd2;
	}

	public void setBillingAdd2(String billingAdd2) {
		this.billingAdd2 = billingAdd2;
	}

	public String getBillingAdd3() {
		return billingAdd3;
	}

	public void setBillingAdd3(String billingAdd3) {
		this.billingAdd3 = billingAdd3;
	}

	public String getBillingAdd4() {
		return billingAdd4;
	}

	public void setBillingAdd4(String billingAdd4) {
		this.billingAdd4 = billingAdd4;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public String getBillingPhone() {
		return billingPhone;
	}

	public void setBillingPhone(String billingPhone) {
		this.billingPhone = billingPhone;
	}

	public String getBillingFax() {
		return billingFax;
	}

	public void setBillingFax(String billingFax) {
		this.billingFax = billingFax;
	}

	public String getBillingZip() {
		return billingZip;
	}

	public void setBillingZip(String billingZip) {
		this.billingZip = billingZip;
	}

	public String getCurrencyByBillToCode() {
		return currencyByBillToCode;
	}

	public void setCurrencyByBillToCode(String currencyByBillToCode) {
		this.currencyByBillToCode = currencyByBillToCode;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getCurrencyByChargeCode() {
		return currencyByChargeCode;
	}

	public void setCurrencyByChargeCode(String currencyByChargeCode) {
		this.currencyByChargeCode = currencyByChargeCode;
	} 

	public String getBookingAgentCodeDMM() {
		return bookingAgentCodeDMM;
	}

	public void setBookingAgentCodeDMM(String bookingAgentCodeDMM) {
		this.bookingAgentCodeDMM = bookingAgentCodeDMM;
	}

	public CustomerFile getCustomerFileToRecods() {
		return customerFileToRecods;
	}

	public void setCustomerFileToRecods(CustomerFile customerFileToRecods) {
		this.customerFileToRecods = customerFileToRecods;
	}

	public ItemsJbkEquipManager getItemsJbkEquipManager() {
		return itemsJbkEquipManager;
	}

	public void setItemsJbkEquipManager(ItemsJbkEquipManager itemsJbkEquipManager) {
		this.itemsJbkEquipManager = itemsJbkEquipManager;
	}

	public String getUrlValue() {
		return urlValue;
	}

	public void setUrlValue(String urlValue) {
		this.urlValue = urlValue;
	}

	public String getSelectedAccountId() {
		return selectedAccountId;
	}

	public void setSelectedAccountId(String selectedAccountId) {
		this.selectedAccountId = selectedAccountId;
	}

	public String getFormStatus() {
		return formStatus;
	}

	public void setFormStatus(String formStatus) {
		this.formStatus = formStatus;
	}

	public boolean isNetworkAgent() {
		return networkAgent;
	}

	public void setNetworkAgent(boolean networkAgent) {
		this.networkAgent = networkAgent;
	}

	public boolean isUtsiRecAccDateFlag() {
		return utsiRecAccDateFlag;
	}

	public void setUtsiRecAccDateFlag(boolean utsiRecAccDateFlag) {
		this.utsiRecAccDateFlag = utsiRecAccDateFlag;
	}

	public boolean isUtsiPayAccDateFlag() {
		return utsiPayAccDateFlag;
	}

	public void setUtsiPayAccDateFlag(boolean utsiPayAccDateFlag) {
		this.utsiPayAccDateFlag = utsiPayAccDateFlag;
	}

	public String getActivateAccPortal() {
		return activateAccPortal;
	}

	public void setActivateAccPortal(String activateAccPortal) {
		this.activateAccPortal = activateAccPortal;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public AccountLine getAddCopyAccountLine() {
		return addCopyAccountLine;
	}

	public void setAddCopyAccountLine(AccountLine addCopyAccountLine) {
		this.addCopyAccountLine = addCopyAccountLine;
	}

	public boolean isPayablesXferWithApprovalOnly() {
		return payablesXferWithApprovalOnly;
	}

	public void setPayablesXferWithApprovalOnly(boolean payablesXferWithApprovalOnly) {
		this.payablesXferWithApprovalOnly = payablesXferWithApprovalOnly;
	}

	public String getAccEstimate() {
		return accEstimate;
	}

	public void setAccEstimate(String accEstimate) {
		this.accEstimate = accEstimate;
	}

	public Map<String, String> getDivision() {
		return division;
	}

	public void setDivision(Map<String, String> division) {
		this.division = division;
	}

	public Map<String, String> getDivisionTemp() {
		return divisionTemp;
	}

	public void setDivisionTemp(Map<String, String> divisionTemp) {
		this.divisionTemp = divisionTemp;
	}

	public String getDivisionFlag() {
		return divisionFlag;
	}

	public void setDivisionFlag(String divisionFlag) {
		this.divisionFlag = divisionFlag;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public String getCostingDetailShowFlag() {
		return costingDetailShowFlag;
	}

	public void setCostingDetailShowFlag(String costingDetailShowFlag) {
		this.costingDetailShowFlag = costingDetailShowFlag;
	}

	public String getCurrencyNew() {
		return currencyNew;
	}

	public void setCurrencyNew(String currencyNew) {
		this.currencyNew = currencyNew;
	}
	public String getSetRegistrationNumber() {
		return setRegistrationNumber;
	}

	public void setSetRegistrationNumber(String setRegistrationNumber) {
		this.setRegistrationNumber = setRegistrationNumber;
	}

	public String getVendorCodeNew() {
		return vendorCodeNew;
	}

	public void setVendorCodeNew(String vendorCodeNew) {
		this.vendorCodeNew = vendorCodeNew;
	}

	public String getInvoiceNumberNew() {
		return invoiceNumberNew;
	}

	public void setInvoiceNumberNew(String invoiceNumberNew) {
		this.invoiceNumberNew = invoiceNumberNew;
	}

	public String getInvoiceDateNew() {
		return invoiceDateNew;
	}

	public void setInvoiceDateNew(String invoiceDateNew) {
		this.invoiceDateNew = invoiceDateNew;
	}

	public String getPurchaseInvoiceFlag() {
		return purchaseInvoiceFlag;
	}

	public void setPurchaseInvoiceFlag(String purchaseInvoiceFlag) {
		this.purchaseInvoiceFlag = purchaseInvoiceFlag;
	}

	public void setCommissionManager(CommissionManager commissionManager) {
		this.commissionManager = commissionManager;
	}

	public Commission getCommission() {
		return commission;
	}

	public void setCommission(Commission commission) {
		this.commission = commission;
	}

	public boolean isAutomaticReconcile() {
		return automaticReconcile;
	}

	public void setAutomaticReconcile(boolean automaticReconcile) {
		this.automaticReconcile = automaticReconcile;
	}

	public String getNewSysDefaultDate() {
		return newSysDefaultDate;
	}

	public void setNewSysDefaultDate(String newSysDefaultDate) {
		this.newSysDefaultDate = newSysDefaultDate;
	}

	public String getIsPayableRequired() {
		return isPayableRequired;
	}

	public void setIsPayableRequired(String isPayableRequired) {
		this.isPayableRequired = isPayableRequired;
	}

	public List getAccListBycompDiv() {
		return accListBycompDiv;
	}

	public void setAccListBycompDiv(List accListBycompDiv) {
		this.accListBycompDiv = accListBycompDiv;
	}

	public String getCommPercentage() {
		return commPercentage;
	}

	public void setCommPercentage(String commPercentage) {
		this.commPercentage = commPercentage;
	}

	public String getCommPercentageLine2() {
		return commPercentageLine2;
	}

	public void setCommPercentageLine2(String commPercentageLine2) {
		this.commPercentageLine2 = commPercentageLine2;
	}

	public String getCommLine1Id() {
		return commLine1Id;
	}

	public void setCommLine1Id(String commLine1Id) {
		this.commLine1Id = commLine1Id;
	}

	public String getCommLine2Id() {
		return commLine2Id;
	}

	public void setCommLine2Id(String commLine2Id) {
		this.commLine2Id = commLine2Id;
	}

	public String getDriverCode() {
		return driverCode;
	}

	public void setDriverCode(String driverCode) {
		this.driverCode = driverCode;
	}

	public String getIdss() {
		return idss;
	}

	public void setIdss(String idss) {
		this.idss = idss;
	}

	public String getVanLineAccountView() {
		return vanLineAccountView;
	}

	public void setVanLineAccountView(String vanLineAccountView) {
		this.vanLineAccountView = vanLineAccountView;
	}

	public String getVanLineAccCategoryURL() {
		return vanLineAccCategoryURL;
	}

	public void setVanLineAccCategoryURL(String vanLineAccCategoryURL) {
		this.vanLineAccCategoryURL = vanLineAccCategoryURL;
	}

	public Map<String, String> getUTSIpayVatList() {
		return UTSIpayVatList;
	}

	public void setUTSIpayVatList(Map<String, String> uTSIpayVatList) {
		UTSIpayVatList = uTSIpayVatList;
	}

	public Map<String, String> getUTSIpayVatPercentList() {
		return UTSIpayVatPercentList;
	}

	public void setUTSIpayVatPercentList(Map<String, String> uTSIpayVatPercentList) {
		UTSIpayVatPercentList = uTSIpayVatPercentList;
	}

	public Map<String, String> getUTSIeuVatPercentList() {
		return UTSIeuVatPercentList;
	}

	public void setUTSIeuVatPercentList(Map<String, String> uTSIeuVatPercentList) {
		UTSIeuVatPercentList = uTSIeuVatPercentList;
	}

	public BigDecimal getAccountLineDistribution() {
		return accountLineDistribution;
	}

	public void setAccountLineDistribution(BigDecimal accountLineDistribution) {
		this.accountLineDistribution = accountLineDistribution;
	}

	public String getDriverCommission() {
		return driverCommission;
	}

	public void setDriverCommission(String driverCommission) {
		this.driverCommission = driverCommission;
	}

	public String getAutomaticDriverReversal() {
		return automaticDriverReversal;
	}

	public void setAutomaticDriverReversal(String automaticDriverReversal) {
		this.automaticDriverReversal = automaticDriverReversal;
	}

	public Boolean getDriverCommissionSetUp() {
		return driverCommissionSetUp;
	}

	public void setDriverCommissionSetUp(Boolean driverCommissionSetUp) {
		this.driverCommissionSetUp = driverCommissionSetUp;
	}

	public String getDisableALL() {
		return disableALL;
	}

	public void setDisableALL(String disableALL) {
		this.disableALL = disableALL;
	}

	public BigDecimal getAccountEstimateRevenueAmount() {
		return accountEstimateRevenueAmount;
	}

	public void setAccountEstimateRevenueAmount(
			BigDecimal accountEstimateRevenueAmount) {
		this.accountEstimateRevenueAmount = accountEstimateRevenueAmount;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}

	public boolean isOwnbillingVanline() {
		return ownbillingVanline;
	}

	public void setOwnbillingVanline(boolean ownbillingVanline) {
		this.ownbillingVanline = ownbillingVanline;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getQuotesToValidate() {
		if((quotesToValidate==null || quotesToValidate.equals(""))){
			List quotesToValidateList=customerFileManager.getQuotesToValidate(sessionCorpID, serviceOrder.getJob(), serviceOrder.getCompanyDivision());
			if(quotesToValidateList!=null && !quotesToValidateList.isEmpty() && !quotesToValidateList.contains(null)){
			quotesToValidate=quotesToValidateList.get(0).toString();
			}else{
				quotesToValidate="";
			}
			}
			return quotesToValidate;
		}

	public void setQuotesToValidate(String quotesToValidate) {
		this.quotesToValidate = quotesToValidate;
	}

	public String getCommissionJobName() {
		return commissionJobName;
	}

	public void setCommissionJobName(String commissionJobName) {
		this.commissionJobName = commissionJobName;
	}

	public Long getSid() {
		return sid;
	}

	public String getBillToCodeLst() {
		return billToCodeLst;
	}

	public void setBillToCodeLst(String billToCodeLst) {
		this.billToCodeLst = billToCodeLst;
	}

	public int getBillToLength() {
		return billToLength;
	}

	public void setBillToLength(int billToLength) {
		this.billToLength = billToLength;
	}

	public int getCurrencyLength() {
		return currencyLength;
	}

	public void setCurrencyLength(int currencyLength) {
		this.currencyLength = currencyLength;
	}

	public List getCompDivForBookingAgentList() {
		return compDivForBookingAgentList;
	}

	public void setCompDivForBookingAgentList(List compDivForBookingAgentList) {
		this.compDivForBookingAgentList = compDivForBookingAgentList;
	}

	public List getValidateChargeVatExcludeList() {
		return validateChargeVatExcludeList;
	}

	public void setValidateChargeVatExcludeList(List validateChargeVatExcludeList) {
		this.validateChargeVatExcludeList = validateChargeVatExcludeList;
	}

	public String getAccountLineStatus() {
		return accountLineStatus;
	}

	public void setAccountLineStatus(String accountLineStatus) {
		this.accountLineStatus = accountLineStatus;
	}

	public String getPreviewLine() {
		return previewLine;
	}

	public void setPreviewLine(String previewLine) {
		this.previewLine = previewLine;
	}

	public List getAllPreviewLine() {
		return allPreviewLine;
	}

	public void setAllPreviewLine(List allPreviewLine) {
		this.allPreviewLine = allPreviewLine;
	}

	public String getEmptyList() {
		return emptyList;
	}

	public void setEmptyList(String emptyList) {
		this.emptyList = emptyList;
	}

	public BigDecimal getGrossMargin() {
		return grossMargin;
	}

	public void setGrossMargin(BigDecimal grossMargin) {
		this.grossMargin = grossMargin;
	}

	public BigDecimal getCommissionableGrossMargin() {
		return commissionableGrossMargin;
	}

	public void setCommissionableGrossMargin(BigDecimal commissionableGrossMargin) {
		this.commissionableGrossMargin = commissionableGrossMargin;
	}

	public String getAccSaveHitflag() {
		return accSaveHitflag;
	}

	public void setAccSaveHitflag(String accSaveHitflag) {
		this.accSaveHitflag = accSaveHitflag;
	}

	public Map<String, String> getAccEstmateStatus() {
		return accEstmateStatus;
	}

	public void setAccEstmateStatus(Map<String, String> accEstmateStatus) {
		this.accEstmateStatus = accEstmateStatus;
	}

	public BigDecimal getAccountRevisionRevenueAmount() {
		return accountRevisionRevenueAmount;
	}

	public void setAccountRevisionRevenueAmount(
			BigDecimal accountRevisionRevenueAmount) {
		this.accountRevisionRevenueAmount = accountRevisionRevenueAmount;
	}


	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}


	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public Map<String, String> getEuvatRecGLMap() {
		return euvatRecGLMap;
	}

	public void setEuvatRecGLMap(Map<String, String> euvatRecGLMap) {
		this.euvatRecGLMap = euvatRecGLMap;
	}

	public Map<String, String> getPayVatPayGLMap() {
		return payVatPayGLMap;
	}

	public void setPayVatPayGLMap(Map<String, String> payVatPayGLMap) {
		this.payVatPayGLMap = payVatPayGLMap;
	}

	public String getCheckDmm() {
		return checkDmm;
	}

	public void setCheckDmm(String checkDmm) {
		this.checkDmm = checkDmm;
	}

	public boolean isSelectiveInvoice() {
		return selectiveInvoice;
	}

	public void setSelectiveInvoice(boolean selectiveInvoice) {
		this.selectiveInvoice = selectiveInvoice;
	}

	public List getSelectiveInvoiceList() {
		return selectiveInvoiceList;
	}

	public void setSelectiveInvoiceList(List selectiveInvoiceList) {
		this.selectiveInvoiceList = selectiveInvoiceList;
	}

	public Boolean getAcctDisplayEntitled() {
		return acctDisplayEntitled;
	}

	public void setAcctDisplayEntitled(Boolean acctDisplayEntitled) {
		this.acctDisplayEntitled = acctDisplayEntitled;
	}

	public Boolean getAcctDisplayEstimate() {
		return acctDisplayEstimate;
	}

	public void setAcctDisplayEstimate(Boolean acctDisplayEstimate) {
		this.acctDisplayEstimate = acctDisplayEstimate;
	}

	public Boolean getAcctDisplayRevision() {
		return acctDisplayRevision;
	}

	public void setAcctDisplayRevision(Boolean acctDisplayRevision) {
		this.acctDisplayRevision = acctDisplayRevision;
	}

	public Map<String, String> getMyfileDocumentList() {
		return myfileDocumentList;
	}

	public void setMyfileDocumentList(Map<String, String> myfileDocumentList) {
		this.myfileDocumentList = myfileDocumentList;
	}
	public boolean isNetworkAgentInvoiceFlag() {
		return networkAgentInvoiceFlag;
	}

	public void setNetworkAgentInvoiceFlag(boolean networkAgentInvoiceFlag) {
		this.networkAgentInvoiceFlag = networkAgentInvoiceFlag;
	}
	public Long getNetworkAgentId() {
		return networkAgentId;
	}

	public void setNetworkAgentId(Long networkAgentId) {
		this.networkAgentId = networkAgentId;
	}

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}

	public boolean isSingleCompanyDivisionInvoicing() {
		return singleCompanyDivisionInvoicing;
	}

	public void setSingleCompanyDivisionInvoicing(
			boolean singleCompanyDivisionInvoicing) {
		this.singleCompanyDivisionInvoicing = singleCompanyDivisionInvoicing;
	}

	public boolean isAccNonEditFlag() {
		return accNonEditFlag;
	}

	public void setAccNonEditFlag(boolean accNonEditFlag) {
		this.accNonEditFlag = accNonEditFlag;
	}

	public Boolean getoAdAWeightVolumeValidationForInv() {
		return oAdAWeightVolumeValidationForInv;
	}

	public void setoAdAWeightVolumeValidationForInv(
			Boolean oAdAWeightVolumeValidationForInv) {
		this.oAdAWeightVolumeValidationForInv = oAdAWeightVolumeValidationForInv;
	}

	public Boolean getSubOADAValidationForInv() {
		return subOADAValidationForInv;
	}

	public void setSubOADAValidationForInv(Boolean subOADAValidationForInv) {
		this.subOADAValidationForInv = subOADAValidationForInv;
	}

	public Boolean getOwnBookingAgentFlag() {
		return ownBookingAgentFlag;
	}

	public void setOwnBookingAgentFlag(Boolean ownBookingAgentFlag) {
		this.ownBookingAgentFlag = ownBookingAgentFlag;
	}

	public String getRollUpInvoiceFlag() {
		return rollUpInvoiceFlag;
	}

	public void setRollUpInvoiceFlag(String rollUpInvoiceFlag) {
		this.rollUpInvoiceFlag = rollUpInvoiceFlag;
	}

	public Map<String, String> getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	public void setCurrencyExchangeRate(Map<String, String> currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public String getCompDivFlag() {
		return compDivFlag;
	}

	public void setCompDivFlag(String compDivFlag) {
		this.compDivFlag = compDivFlag;
	}

	public String getFileExt() {
		return fileExt;
	}

	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}

	public String getUpdatedUserName() {
		return updatedUserName;
	}

	public void setUpdatedUserName(String updatedUserName) {
		this.updatedUserName = updatedUserName;
	}

	public String getNetworkPartnerCode() {
		return networkPartnerCode;
	}

	public void setNetworkPartnerCode(String networkPartnerCode) {
		this.networkPartnerCode = networkPartnerCode;
	}

	public String getOldAccountLineBillToCode() {
		return oldAccountLineBillToCode;
	}

	public void setOldAccountLineBillToCode(String oldAccountLineBillToCode) {
		this.oldAccountLineBillToCode = oldAccountLineBillToCode;
	}

	public String getNetworkBillToCode() {
		return networkBillToCode;
	}

	public void setNetworkBillToCode(String networkBillToCode) {
		this.networkBillToCode = networkBillToCode;
	}

	public boolean isCmmDmmFlag() {
		return cmmDmmFlag;
	}

	public void setCmmDmmFlag(boolean cmmDmmFlag) {
		this.cmmDmmFlag = cmmDmmFlag;
	}
	public List getPartnerDetailsAutoComplete() {
		return partnerDetailsAutoComplete;
	}

	public void setPartnerDetailsAutoComplete(List partnerDetailsAutoComplete) {
		this.partnerDetailsAutoComplete = partnerDetailsAutoComplete;
	}

	public String getPartnerDetailsAutoCompleteGsonData() {
		return partnerDetailsAutoCompleteGsonData;
	}

	public void setPartnerDetailsAutoCompleteGsonData(
			String partnerDetailsAutoCompleteGsonData) {
		this.partnerDetailsAutoCompleteGsonData = partnerDetailsAutoCompleteGsonData;
	}

	public String getPartnerNameAutoCopmlete() {
		return partnerNameAutoCopmlete;
	}

	public void setPartnerNameAutoCopmlete(String partnerNameAutoCopmlete) {
		this.partnerNameAutoCopmlete = partnerNameAutoCopmlete;
	}

	public String getAutocompleteCondition() {
		return autocompleteCondition;
	}

	public void setAutocompleteCondition(String autocompleteCondition) {
		this.autocompleteCondition = autocompleteCondition;
	}

	public String getPartnerNameId() {
		return partnerNameId;
	}

	public void setPartnerNameId(String partnerNameId) {
		this.partnerNameId = partnerNameId;
	}

	public String getPaertnerCodeId() {
		return paertnerCodeId;
	}

	public void setPaertnerCodeId(String paertnerCodeId) {
		this.paertnerCodeId = paertnerCodeId;
	}

	public String getAutocompleteDivId() {
		return autocompleteDivId;
	}

	public void setAutocompleteDivId(String autocompleteDivId) {
		this.autocompleteDivId = autocompleteDivId;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public ServiceOrder getServiceOrderCombo() {
		return serviceOrderCombo;
	}

	public void setServiceOrderCombo(ServiceOrder serviceOrderCombo) {
		this.serviceOrderCombo = serviceOrderCombo;
	}

	public String getCheckContractChargesMandatory() {
		return checkContractChargesMandatory;
	}

	public void setCheckContractChargesMandatory(
			String checkContractChargesMandatory) {
		this.checkContractChargesMandatory = checkContractChargesMandatory;
	}

	public List getInvoicesForUnpostingList() {
		return invoicesForUnpostingList;
	}

	public void setInvoicesForUnpostingList(List invoicesForUnpostingList) {
		this.invoicesForUnpostingList = invoicesForUnpostingList;
	}

	public String getStringAid() {
		return stringAid;
	}

	public void setStringAid(String stringAid) {
		this.stringAid = stringAid;
	}

	public String getCheckZeroforInvoice() {
		return checkZeroforInvoice;
	}

	public void setCheckZeroforInvoice(String checkZeroforInvoice) {
		this.checkZeroforInvoice = checkZeroforInvoice;
	}

	public String getInvoiceBillToCode() {
		return invoiceBillToCode;
	}

	public void setInvoiceBillToCode(String invoiceBillToCode) {
		this.invoiceBillToCode = invoiceBillToCode;
	}

	public List getCloseCompanyDivision() {
		return closeCompanyDivision;
	}

	public void setCloseCompanyDivision(List closeCompanyDivision) {
		this.closeCompanyDivision = closeCompanyDivision;
	}

	public boolean isCheckDescriptionFieldVisibility() {
		return checkDescriptionFieldVisibility;
	}

	public void setCheckDescriptionFieldVisibility(
			boolean checkDescriptionFieldVisibility) {
		this.checkDescriptionFieldVisibility = checkDescriptionFieldVisibility;
	}

	public String getBulkComputeValue() {
		return bulkComputeValue;
	}

	public void setBulkComputeValue(String bulkComputeValue) {
		this.bulkComputeValue = bulkComputeValue;
	}

	public String getResultType() {
		return resultType;
	}

	public void setResultType(String resultType) {
		this.resultType = resultType;
	}

	public String getMyFileForVal() {
		return myFileForVal;
	}

	public void setMyFileForVal(String myFileForVal) {
		this.myFileForVal = myFileForVal;
	}

	public String getAcctRefVisiable() {
		return acctRefVisiable;
	}

	public void setAcctRefVisiable(String acctRefVisiable) {
		this.acctRefVisiable = acctRefVisiable;
	}

	public ExtractedFileLog getExtractedFileLog() {
		return extractedFileLog;
	}

	public void setExtractedFileLog(ExtractedFileLog extractedFileLog) {
		this.extractedFileLog = extractedFileLog;
	}

	public void setExtractedFileLogManager(
			ExtractedFileLogManager extractedFileLogManager) {
		this.extractedFileLogManager = extractedFileLogManager;
	}

	public String getMsgClicked() {
		return msgClicked;
	}

	public void setMsgClicked(String msgClicked) {
		this.msgClicked = msgClicked;
	}

	public double getBuildFormulaActualRevenue() {
		return buildFormulaActualRevenue;
	}

	public void setBuildFormulaActualRevenue(double buildFormulaActualRevenue) {
		this.buildFormulaActualRevenue = buildFormulaActualRevenue;
	}
	public List getFindInvoiceList() {
		return findInvoiceList;
	}

	public void setFindInvoiceList(List findInvoiceList) {
		this.findInvoiceList = findInvoiceList;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public String getAuthortionNum() {
		return authortionNum;
	}

	public void setAuthortionNum(String authortionNum) {
		this.authortionNum = authortionNum;
	}

	

	public String getInvoiceLineIdNew() {
		return invoiceLineIdNew;
	}

	public void setInvoiceLineIdNew(String invoiceLineIdNew) {
		this.invoiceLineIdNew = invoiceLineIdNew;
	}

	public boolean isAdminInvoiceAmountEdit() {
		return adminInvoiceAmountEdit;
	}

	public void setAdminInvoiceAmountEdit(boolean adminInvoiceAmountEdit) {
		this.adminInvoiceAmountEdit = adminInvoiceAmountEdit;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}
	public String getDashBoardHideJobsList() {
		return dashBoardHideJobsList;
	}
	public void setDashBoardHideJobsList(String dashBoardHideJobsList) {
		this.dashBoardHideJobsList = dashBoardHideJobsList;
	}

	public boolean isAutoPayablePosting() {
		return autoPayablePosting;
	}

	public void setAutoPayablePosting(boolean autoPayablePosting) {
		this.autoPayablePosting = autoPayablePosting;
	}

	public boolean isAllowAgentInvoiceUpload() {
		return allowAgentInvoiceUpload;
	}

	public void setAllowAgentInvoiceUpload(boolean allowAgentInvoiceUpload) {
		this.allowAgentInvoiceUpload = allowAgentInvoiceUpload;
	}

	public String getNotesStatus() {
		return notesStatus;
	}

	public void setNotesStatus(String notesStatus) {
		this.notesStatus = notesStatus;
	}
	public Map<String, String> getPayingStatusPopUpList() {
		return payingStatusPopUpList;
	}

	public void setPayingStatusPopUpList(Map<String, String> payingStatusPopUpList) {
		this.payingStatusPopUpList = payingStatusPopUpList;
	}

	public Map<String, String> getFlex1RecCodeList() {
		return flex1RecCodeList;
	}

	public void setFlex1RecCodeList(Map<String, String> flex1RecCodeList) {
		this.flex1RecCodeList = flex1RecCodeList;
	}

	public Map<String, String> getFlex2PayCodeList() {
		return flex2PayCodeList;
	}

	public void setFlex2PayCodeList(Map<String, String> flex2PayCodeList) {
		this.flex2PayCodeList = flex2PayCodeList;
	}

	public Map<String, String> getVatBillingGroupList() {
		return vatBillingGroupList;
	}

	public void setVatBillingGroupList(Map<String, String> vatBillingGroupList) {
		this.vatBillingGroupList = vatBillingGroupList;
	}
} 

class MissedInvoiceDTO implements Comparable<MissedInvoiceDTO>{
	private String invoice;
	private String revenue;
	private String soNumber;
	public String getInvoice() {
		return invoice;
	}
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}
	public String getRevenue() {
		return revenue;
	}
	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}
	public String getSoNumber() {
		return soNumber;
	}
	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}
	@Override
	public String toString() {
		return invoice + ", " + revenue + ", " + soNumber;
	}
	public int compareTo(MissedInvoiceDTO o) {
        int lastCmp = invoice.compareTo(o.invoice);
        return (lastCmp != 0 ? lastCmp : invoice.compareTo(o.invoice));
	}
	

}