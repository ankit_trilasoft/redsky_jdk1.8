package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.PartnerRefHaulingGridDao;
import com.trilasoft.app.model.PartnerRefHaulingGrid;
import com.trilasoft.app.service.PartnerRefHaulingGridManager;

public class PartnerRefHaulingGridManagerImpl extends GenericManagerImpl<PartnerRefHaulingGrid, Long> implements PartnerRefHaulingGridManager {
	PartnerRefHaulingGridDao partnerRefHaulingGridDao;

	public PartnerRefHaulingGridManagerImpl(PartnerRefHaulingGridDao partnerRefHaulingGridDao) {
		super(partnerRefHaulingGridDao);
		this.partnerRefHaulingGridDao = partnerRefHaulingGridDao;
	}

	public List getHaulingGridList(String lowDistance, String highDistance, String rateMile, String rateFlat, String grid, String unit, String countryCode,String corpID) {
		// TODO Auto-generated method stub
		return partnerRefHaulingGridDao.getHaulingGridList(lowDistance, highDistance, rateMile, rateFlat, grid, unit, countryCode,corpID);
	}
}
