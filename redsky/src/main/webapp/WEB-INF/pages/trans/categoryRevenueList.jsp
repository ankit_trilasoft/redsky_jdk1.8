<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>
<meta name="heading" content="<fmt:message key='categoryRevenueList.title'/>"/> 
<title><fmt:message key="categoryRevenueList.heading"/></title> 

<style>
span.pagelinks {
	display:block;
	font-size:0.95em;
	margin-bottom:3px;
	!margin-bottom:2px;
	margin-top:-22px;
	!margin-top:-20px;
	padding:2px 0px;
	text-align:right;
	width:99%;
}

form {
margin-top:-40px;
!margin-top:-10px;
}

div#main {
margin:-5px 0 0;

}
</style>
</head>

<s:form id="categoryRevenueList" name="categoryRevenueList" action="searchcategoryRevenueList"  validate="true">   
<div id="Layer1" style="width:100%"> 

<div id="otabs">
  <ul>
    <li><a class="current"><span>Search</span></a></li>
  </ul>
</div>
<div class="spnblk">&nbsp;</div>

<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editcategoryRevenueForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">	
<table class="table" style="width:99%"  >
<thead>
<tr>

<th><fmt:message key="categoryRevenue.rateType"/></th>
<th><fmt:message key="categoryRevenue.totalCount"/></th>
<th>Cat1/DR</th>
<th>Cat2/CD</th>
<th>Cat3/HE</th>
<th>Cat4/CH</th>
<th>&nbsp;</th>

</tr></thead>	
		<tbody>
		<tr>
		    <td align="left"><s:textfield name="categoryRevenue.secondSet" required="true" cssClass="input-text" size="10" onkeydown="return quotesNotAllowed(event);"/></td>
			<td  align="left"><s:textfield name="categoryRevenue.totalCrew" required="true" cssClass="input-text" size="10" onkeydown="return quotesNotAllowed(event);"/></td>
			
			<td  align="left"><s:textfield name="categoryRevenue.numberOfCrew1" required="true" cssClass="input-text" size="10" onkeydown="return quotesNotAllowed(event);"/></td>
			<td  align="left"><s:textfield name="categoryRevenue.numberOfCrew2" required="true" cssClass="input-text" size="10" onkeydown="return quotesNotAllowed(event);"/></td>
			<td  align="left"><s:textfield name="categoryRevenue.numberOfCrew3" required="true" cssClass="input-text" size="10" onkeydown="return quotesNotAllowed(event);"/></td>
			<td  align="left"><s:textfield name="categoryRevenue.numberOfCrew4" required="true" cssClass="input-text" size="10" onkeydown="return quotesNotAllowed(event);"/></td>
			<td  align="left">
       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;" method="" key="button.search"/>  
       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>       
   			</td>
			</tr>			
		</tbody>
	</table>
	</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>
	<c:out value="${searchresults}" escapeXml="false" /> 
	
	<div id="otabs" style="margin-top:-15px;">
		  <ul>
		    <li><a class="current"><span>Revenue List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		
		<display:table name="categoryRevenueSet" class="table" requestURI="" id="categoryRevenueSet" export="true" defaultsort="1" pagesize="10" style="width:99%;margin-top: 1px;" >   
		<display:column property="secondSet"  href="editcategoryRevenueForm.html" paramId="id" paramProperty="id" sortable="true" titleKey="categoryRevenue.rateType"/>
   		<display:column headerClass="containeralign" style="text-align:right" property="totalCrew"  paramId="id" paramProperty="id" sortable="true" titleKey="categoryRevenue.totalCount"/>
   		<display:column headerClass="containeralign" style="text-align:right" property="numberOfCrew1" sortable="true" titleKey="categoryRevenue.Category1" />
   		<display:column headerClass="containeralign" style="text-align:right" property="percentPerPerson1" sortable="true" titleKey="categoryRevenue.Categorypercent1" />
   		<display:column headerClass="containeralign" style="text-align:right" property="numberOfCrew2" sortable="true" titleKey="categoryRevenue.Category2" />
   		<display:column headerClass="containeralign" style="text-align:right" property="percentPerPerson2" sortable="true" titleKey="categoryRevenue.Categorypercent2" />
   		<display:column headerClass="containeralign" style="text-align:right" property="numberOfCrew3" sortable="true" titleKey="categoryRevenue.Category3" />
   		<display:column headerClass="containeralign" style="text-align:right" property="percentPerPerson3" sortable="true" titleKey="categoryRevenue.Categorypercent3" />
   		<display:column headerClass="containeralign" style="text-align:right" property="numberOfCrew4" sortable="true" titleKey="categoryRevenue.Category4" />
   		<display:column headerClass="containeralign" style="text-align:right" property="percentPerPerson4" sortable="true" titleKey="categoryRevenue.Categorypercent4" />
   		<display:column headerClass="containeralign" style="text-align:right" property="numberOfCrew5" sortable="true" titleKey="categoryRevenue.Category5" />
   		<display:column headerClass="containeralign" style="text-align:right" property="percentPerPerson5" sortable="true" titleKey="categoryRevenue.Categorypercent5" />
   		<display:column headerClass="containeralign" style="text-align:right" property="numberOfCrew6" sortable="true" titleKey="categoryRevenue.Category6" />
   		<display:column headerClass="containeralign" style="text-align:right" property="percentPerPerson6" sortable="true" titleKey="categoryRevenue.Categorypercent6" />
   		<display:column headerClass="containeralign" style="text-align:right" property="numberOfCrew7" sortable="true" titleKey="categoryRevenue.Category7" />
   		<display:column headerClass="containeralign" style="text-align:right" property="percentPerPerson7" sortable="true" titleKey="categoryRevenue.Categorypercent7" />
   		<display:column headerClass="containeralign" style="text-align:right" property="numberOfCrew8" sortable="true" titleKey="categoryRevenue.Category8" />
   		<display:column headerClass="containeralign" style="text-align:right" property="percentPerPerson8" sortable="true" titleKey="categoryRevenue.Categorypercent8" />
   		<display:column headerClass="containeralign" style="text-align:right" property="numberOfCrew9" sortable="true" titleKey="categoryRevenue.Category9" />
   		<display:column headerClass="containeralign" style="text-align:right" property="percentPerPerson9" sortable="true" titleKey="categoryRevenue.Categorypercent9" />
   		<display:column headerClass="containeralign" style="text-align:right" property="numberOfCrew10" sortable="true" titleKey="categoryRevenue.Category10" />
   		<display:column headerClass="containeralign" style="text-align:right" property="percentPerPerson10" sortable="true" titleKey="categoryRevenue.Categorypercent10" />
   		<display:setProperty name="export.excel.filename" value="Revenue List.xls"/>
   		<display:setProperty name="export.csv.filename" value="Revenue List.csv"/>
   		</display:table>  
   		
   		<c:out value="${buttons}" escapeXml="false" />   
		<c:set var="isTrue" value="false" scope="session"/>
		</div>
</s:form> 

<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script language="javascript" type="text/javascript">
function clear_fields(){
	var i;
	for(i=0;i<=5;i++){
		document.forms['categoryRevenueList'].elements[i].value = "";
	}
}

function quotesNotAllowed(evt){
  var keyCode = evt.which ? evt.which : evt.keyCode;
  if(keyCode==222){
  	return false;
  }
  else{
 	 return true;
  }
}
</script>
<%-- Shifting Closed Here --%>
<script type="text/javascript"> 
	highlightTableRows("categoryRevenueSet"); 
</script>