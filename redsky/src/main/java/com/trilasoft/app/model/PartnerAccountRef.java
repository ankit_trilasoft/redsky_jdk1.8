package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table ( name="partneraccountref")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class PartnerAccountRef  extends BaseObject {
	private Long id;
	private String corpID;
	private String partnerCode;
	private Date createdOn;
	private Date updatedOn;
	private String updatedBy;
	private String createdBy;
	private String companyDivision;
	private String accountCrossReference;
	private Partner partner;
	private String  refType;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("partnerCode", partnerCode).append("createdOn",
				createdOn).append("updatedOn", updatedOn).append("updatedBy",
				updatedBy).append("createdBy", createdBy).append(
				"companyDevision", companyDivision).append(
				"accountCrossReference", accountCrossReference).append("refType",refType).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerAccountRef))
			return false;
		PartnerAccountRef castOther = (PartnerAccountRef) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(partnerCode, castOther.partnerCode)
				.append(createdOn, castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(updatedBy,
						castOther.updatedBy).append(createdBy,
						castOther.createdBy).append(companyDivision,
						castOther.companyDivision).append(
						accountCrossReference, castOther.accountCrossReference).append(refType,castOther.refType)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				partnerCode).append(createdOn).append(updatedOn).append(
				updatedBy).append(createdBy).append(companyDivision).append(
				accountCrossReference).append(refType).toHashCode();
	}
	
	/**
	 * @return the accountCrossReference
	 */
	@Column( length=20 )
	public String getAccountCrossReference() {
		return accountCrossReference;
	}
	/**
	 * @param accountCrossReference the accountCrossReference to set
	 */
	public void setAccountCrossReference(String accountCrossReference) {
		this.accountCrossReference = accountCrossReference;
	}
	/**
	 * @return the companyDevision
	 */
	@Column( length=10)
	public String getCompanyDivision() {
		return companyDivision;
	}
	/**
	 * @param companyDevision the companyDevision to set
	 */
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	/**
	 * @return the corpID
	 */
	@Column( length=15 )
	public String getCorpID() {
		return corpID;
	}
	/**
	 * @param corpID the corpID to set
	 */
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	/**
	 * @return the createdBy
	 */
	@Column( length=82 )
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the partnerCode
	 */
	@Column( length=8)
	public String getPartnerCode() {
		return partnerCode;
	}
	/**
	 * @param partnerCode the partnerCode to set
	 */
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	/**
	 * @return the updatedBy
	 */
	@Column( length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the updatedOn
	 */
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@ManyToOne
	@JoinColumn(name="partnerCode", updatable=false, insertable=false, referencedColumnName="partnerCode")
	public Partner getPartner() {
		return partner;
	}
	public void setPartner(Partner partner) {
		this.partner = partner;
	}
	@Column(length=1)
	public String getRefType() {
		return refType;
	}
	public void setRefType(String refType) {
		this.refType = refType;
	}
	
	
}
