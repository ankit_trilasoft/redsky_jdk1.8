package com.trilasoft.app.webapp.action;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeMap;

import com.trilasoft.app.model.Claim;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.*;
import net.sf.jasperreports.engine.export.ooxml.*;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.RoleManager;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;
import com.opensymphony.xwork2.Preparable;
import com.sun.jmx.snmp.Timestamp;
import com.trilasoft.app.dao.hibernate.ReportsFieldSectionDaoHibernate.ReportFieldSectionDTO;
import com.trilasoft.app.dao.hibernate.dto.ReportEmailDTO;
import com.trilasoft.app.dao.hibernate.util.FileSizeUtil;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.EmailSetup;
import com.trilasoft.app.model.ExtractedFileLog;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PrintDailyPackage;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.Reports;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ExtractedFileLogManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.RefJobDocumentTypeManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ReportBookmarkManager;
import com.trilasoft.app.service.ReportsManager;
import com.trilasoft.app.service.SQLExtractManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.WorkTicketManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.model.ReportsFieldSection;
import com.trilasoft.app.service.ReportsFieldSectionManager;

public class ReportAction extends BaseAction implements Preparable{

	private  final String REPORT_INPUT_PARAMETER_PREFIX = "reportParameter_";

	private  final long serialVersionUID = -9208910183310010569L;

	private File file;
	private List reportfieldSectionList;
	private String fieldnameUpdated;
	private String fieldvalueUpdated;
	private String controlFieldNameUpdated;
	private String controlFieldValueUpdated;
	private String list;
	private int  reportfieldSectionListSize;
	private int  reportfieldSectionListControlSize;
	private String docFileType;
	private String checkAgent;
	private String fileFileName;
	private Boolean isBookingAgent;

	private Boolean isNetworkAgent;

	private Boolean isOriginAgent;

	private Boolean isSubOriginAgent;

	private Boolean isDestAgent;
	
	private Boolean isSubDestAgent;
	private List reportss;
	private ExtendedPaginatedList reportssExt;
	
	private PagingLookupManager pagingLookupManager;
	
	private PaginateListFactory paginateListFactory;
	
	private ReportsManager reportsManager;

	private String module;

	private Reports reports;

	private RefMaster refMaster;
	
	private String documentCategory;

	private RefMasterManager refMasterManager;
	
	private CompanyManager companyManager;
	private Company company;
	private MyFileManager myFileManager;

	private RefJobDocumentTypeManager refJobDocumentTypeManager;
	
	private Long id;
	
	private Long accID;

	private List<JRParameter> reportParameters;

	private String sessionCorpID;
	
	private String sessionUserName;
	
	private String reportSubModule;

	private String reportModule;
	
	private String jobType;
	
	private String companyDivision;
	
	private String graphSoNumber;
	
	private String graphSoWeight;
	
	private String graphSoDPort;
	
	private String formReportFlag;

	private  Map<String, String> docsList;
	
	private  Map<String, String> reportFlag;
	
	private Map<String, String> moduleReport;
	
	 private Map<String, String> subModuleReport;
	
	 private MyFile myFile;
	
	 private String jobNumber;
	
	 private String jobSearchPersistValue;
	
	 private String sequenceNumber;
	 
	 private String shipNumber;
	
	 private ServiceOrderManager serviceOrderManager;
	
	 private CustomerFileManager customerFileManager;
	 
	 private CustomerFile customerFile;
	
	 private  Map<String,String> house;
	
	 private UserManager userManager;
	 
	 private User user;
	 
	 private RoleManager roleManager;
	 
	 private Role role;
	 
	 private Long reportHits=new Long(0);
	 
	 private String formFrom = "";
	 
	 private ReportsFieldSectionManager reportsFieldSectionManager;
	 
	 private ReportsFieldSection reportsFieldSection;
	 
	 private String reportfieldsectionlist;
	 private Map<String, String> roleMap;
	 private Map<String, String> reportCategory;
	 private List agentGroup;
	 private Long id1;
	 private boolean extractFileAudit=false;
	 private ExtractedFileLogManager extractedFileLogManager;
	 private ExtractedFileLog extractedFileLog;
		private  Map<String, String> printOptions;
	public List getAgentGroup() {
		return agentGroup;
	}

	public void setAgentGroup(List agentGroup) {
		this.agentGroup = agentGroup;
	}

	public Map<String, String> getReportCategory() {
		if(reports !=null && reports.getId()!=null && reports.getMenu()!=null && !reports.getMenu().equals(""))
		{
		if(!reportCategory.containsValue(reports.getMenu()) && !reportCategory.containsKey(reports.getMenu()))
		{reportCategory.put(reports.getMenu(), reports.getMenu());
		}
		}
		return reportCategory;
	}

	public void setReportCategory(Map<String, String> reportCategory) {
		this.reportCategory = reportCategory;
	}

	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	
	private ServiceOrder serviceOrder;

	private List soList;
	
	private List esList;
	
	private List COMPANYCODE;
		
	private static Map<String, String> actionType;
	
	private  Map<String, String> payableStatusList;
	
	private Map<String, String> payingStatusList;
	
	private Map<String, String> OMNI;
	
	private Map<String, String> ROUTING;
	
	private Map<String, String> CURRENCY;
	
	private Map<String, String> BANK;

	public Map<String, String> getBANK() {
		return BANK;
	}

	public void setBANK(Map<String, String> bANK) {
		BANK = bANK;
	}

	private String reportValidationFlag;	
	
	private Map<String, String> COMMODITS;
	
	private Map<String, String> JOB;
	private Map<String, String> BaseScore;
	private  Map<String, String> tcktGrp;
	private List CREWNAME;
	private List CREWTYPE;
	private int retryCount;
	private List reportfieldSectionControlList;
	private String reportfieldSectionControllist;
	private List multiplAgentRolesType;
	private String recInvNumb;
	


	public List getCREWTYPE() {
		return CREWTYPE;
	}

	public void setCREWTYPE(List crewtype) {
		CREWTYPE = crewtype;
	}

	private String marked =new String("");
	
	private ReportBookmarkManager reportBookmarkManager;
	
	private SQLExtractManager sqlExtractManager;
	
	 private List availRoles;
	 private  Map<String, String> sale = new LinkedHashMap<String, String>();
	 private String rId;
	 
	 private static Map<String, String> mapList;
	 
	 private String mapFolder;
	 
	 private String regNumber;
	 
	 private String noteID;
	 
	 private String claimNumber;
	 
	 private String custID;
	 
	 private String invoiceNumber;
	 
	 private String bookNumber;
	 
	 private String fromDt;	 
	 private String toDt;
	 private String whouse;

	private String REPORT_LOCALE;
	private String BillTo; 
	private String usertype;
	private String userParentAgent;
	private String searchFlag;
	private Map<String, String> Commodities;
	private List redskyCustomer;
	private String redskyCorp;
	private String ticket;
	private Map<String, String>  relocationServices;
	private List compare;
	private List containerList = new ArrayList();
	private String preferredLanguage;
	private String checkfromfilecabinetinvoicereportflag;
	private String checkfilepath;
	private Map<String, String> routing;
	private List multiplRoleType;
	private String oiJobList;
	
	
	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}
	
	public List getContainerList() {
		return containerList;
	}

	public void setContainerList(List containerList) {
		this.containerList = containerList;
	}

	public List getCompare() {
		return compare;
	}

	public void setCompare(List compare) {
		this.compare = compare;
	}

	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}

	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}
	private String billToDefaultStationary;
	
	public String getBillToDefaultStationary() {
		return billToDefaultStationary;
	}
	public void setBillToDefaultStationary(String billToDefaultStationary) {
		this.billToDefaultStationary = billToDefaultStationary;
	}

	public String getRedskyCorp() {
		return redskyCorp;
	}

	public void setRedskyCorp(String redskyCorp) {
		this.redskyCorp = redskyCorp;
	}

	public List getRedskyCustomer() {
		return redskyCustomer;
	}

	public void setRedskyCustomer(List redskyCustomer) {
		this.redskyCustomer = redskyCustomer;
	}
	private String selectCompanyDivisions;
	public String getSelectCompanyDivisions() {
		return selectCompanyDivisions;
	}

	public void setSelectCompanyDivisions(String selectCompanyDivisions) {
		this.selectCompanyDivisions = selectCompanyDivisions;
	}

	private String selectedCommodities;
	public Map<String, String> getCommodities() {
		return Commodities;
	}
	public void setCommodities(Map<String, String> commodities) {
		Commodities = commodities;
	}
    public String getSelectedCommodities() {
		return selectedCommodities;
	 }
	 public void setSelectedCommodities(String selectedCommodities) {
		this.selectedCommodities = selectedCommodities;
	 }
	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}

	public String getCustID() {
		return custID;
	}

	public void setCustID(String custID) {
		this.custID = custID;
	}

	public String getNoteID() {
		return noteID;
	}

	public void setNoteID(String noteID) {
		this.noteID = noteID;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}


	public ServiceOrderManager getServiceOrderManager() {
		return serviceOrderManager;
	}


	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	private List<SystemDefault> sysDefaultDetail;
	public ReportAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		this.sessionUserName = user.getUsername();
		this.usertype=user.getUserType();
		this.userParentAgent=user.getParentAgent();
		roles = user.getRoles();
		
	}
	
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(ReportAction.class);
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		
		if(getRequest().getParameter("ajax") == null){  
			getComboList(sessionCorpID);
			try{
				List<User> user=userManager.findUserDetails(getRequest().getRemoteUser());
				if (!(user == null || user.isEmpty())) {
					for (User user1 : user) {
						this.reportSignature = user1.getEmail();
						this.reportSignaturePart=user1.getSignature();
					}
				}			
				
			if((reportSignature==null)||(reportSignature.trim().equalsIgnoreCase(""))){
				sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);			
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						this.reportSignature=systemDefault.getEmail();
					}
				}			
			}
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			if((reportSignaturePart==null)||(reportSignaturePart.trim().equalsIgnoreCase(""))){ 
					if(company!=null){
						this.reportSignaturePart=(((company.getCompanyName()!=null)&&(!company.getCompanyName().equalsIgnoreCase("")))?company.getCompanyName()+"\n":"")+""+(((company.getAddress1()!=null)&&(!company.getAddress1().equalsIgnoreCase("")))?company.getAddress1()+"\n":"")+""+(((company.getAddress2()!=null)&&(!company.getAddress2().equalsIgnoreCase("")))?company.getAddress2()+"\n":"")+""+(((company.getPostal()!=null)&&(!company.getPostal().equalsIgnoreCase("")))?company.getPostal()+" ":"")+""+(((company.getCity()!=null)&&(!company.getCity().equalsIgnoreCase("")))?company.getCity()+"\n":(((company.getPostal()==null)||(company.getPostal().equalsIgnoreCase("")))&&((company.getCity()==null || company.getCity().equalsIgnoreCase("")))?"":"\n"))+""+(((company.getContactNumber()!=null)&&(!company.getContactNumber().equalsIgnoreCase("")))?company.getContactNumber():"");
					}				
				}
			if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }
			}catch(Exception e){}
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call End: "+getRequest().getParameter("ajax")); 
	}

	List docFileList1= new ArrayList();
	List docFileList2= new ArrayList();
	List finaldocFileList= new ArrayList();
	private  Map<String, String> saleperson = new LinkedHashMap<String, String>();

	private Map<String, String> jobOwner;
	
	public Map<String, String> getJobOwner() {
		return jobOwner;
	}

	public void setJobOwner(Map<String, String> jobOwner) {
		this.jobOwner = jobOwner;
	}

	public Map<String, String> getSaleperson() {
		return saleperson;
	}

	@SkipValidation
	public String viewFormPrint() throws Exception {
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		return SUCCESS;
	}
	private Boolean visibilityForSalesRole=false;
	 private Boolean visibilityForSalesPortla=false;
	 private String reportName="";
	 private String printOptionsValue="";

	private String multipleJobs;

	private String multipleCompanyDivisions;

	private Map<String, String> CompanyDivisions;
	
	
	public Map<String, String> getCompanyDivisions() {
		return CompanyDivisions;
	}

	public void setCompanyDivisions(Map<String, String> companyDivisions) {
		CompanyDivisions = companyDivisions;
	}

	public String getMultipleCompanyDivisions() {
		return multipleCompanyDivisions;
	}

	public void setMultipleCompanyDivisions(String multipleCompanyDivisions) {
		this.multipleCompanyDivisions = multipleCompanyDivisions;
	}

	public String getMultipleJobs() {
		return multipleJobs;
	}

	public void setMultipleJobs(String multipleJobs) {
		this.multipleJobs = multipleJobs;
	}

	@SkipValidation
	public String viewReportParam() throws Exception {
		String permKey = sessionCorpID +"-"+"module.script.form.corpSalesScript";
		visibilityForSalesRole=AppInitServlet.roleBasedComponentPerms.containsKey(permKey);
		if(roles!=null && visibilityForSalesRole){
			for (Role role : roles){				
				if(role.getName().toString().equals("ROLE_SALES_PORTAL")){
				visibilityForSalesPortla=true;
				}
		}
		}
		if(jobNumber!=null && !jobNumber.equals("") && invoiceNumber!=null && !invoiceNumber.equals("") ){
			printOptionsValue=reportsManager.getDefaultStationary(jobNumber,invoiceNumber,sessionCorpID);
		}
		List reportValidationFlagList=reportsManager.getReportsValidationFlag(sessionCorpID);
		if(!reportValidationFlagList.isEmpty()){
			reportValidationFlag=reportValidationFlagList.get(0).toString();
		}
		reports = reportsManager.get(id);
		initializeRequiredParameters();
		if (reportParameters!=null && !reportParameters.isEmpty()) {
			Iterator<JRParameter> paramIt =  reportParameters.iterator();
			while(paramIt.hasNext()){
				JRParameter jrParam = (JRParameter)paramIt.next();
				String paramName = jrParam.getName();
				if(paramName.equalsIgnoreCase("Crew Name")){
					CREWNAME=reportsManager.findCrewName(sessionCorpID);	
				}
				if(paramName.equalsIgnoreCase("Crew Type")){
					CREWTYPE=reportsManager.findCrewType(sessionCorpID);	
				}
				else if(paramName.equalsIgnoreCase("Warehouse")){
					house=reportsManager.findByModuleReports(sessionCorpID);	
				}
				else if(paramName.equalsIgnoreCase("Service Group")){
					tcktGrp = refMasterManager.findByParameter(sessionCorpID, "TICKETGROUP");
					
				}
				else if(paramName.equalsIgnoreCase("UserName")){
					usrName=refMasterManager.getSuperVisor(sessionCorpID);	
				}
				else if(paramName.equalsIgnoreCase("Coordinator")){
					soList=reportsManager.findByModuleCoordinator(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Estimator")){
					esList=reportsManager.findByModuleEstimator(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Payment Status")){
					payableStatusList=reportsManager.findByPayableStatus();
				}
				else if(paramName.equalsIgnoreCase("SO Container")){
					if(jobNumber!=null ){
						containerList = reportsManager.findContainer(sessionCorpID,jobNumber);
						} else {
						containerList = reportsManager.findContainer(sessionCorpID,"");	
						}				
				}	
				else if(paramName.equalsIgnoreCase("Approval Status")){
					payingStatusList=reportsManager.findByPayingStatus();
				}
				else if(paramName.equalsIgnoreCase("Service_Contract")){
					OMNI=reportsManager.findByOmni();
				}
				else if(paramName.equalsIgnoreCase("Commodity")){
					COMMODITS=reportsManager.findByCommodity(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Commodities")){
					Commodities=reportsManager.findByCommodities(sessionCorpID);
					String CommoditiesAll=Commodities.keySet().toString();
					CommoditiesAll = (CommoditiesAll.replace("[", "").replace("]", ""));
					StringBuffer commoditiesBuffer = new StringBuffer("");
					if (CommoditiesAll == null || CommoditiesAll.equals("") || CommoditiesAll.equals(",")) {
					} else {
						if (CommoditiesAll.indexOf(",") == 0) {
							CommoditiesAll = CommoditiesAll.substring(1);
						}
						if (CommoditiesAll.lastIndexOf(",") == CommoditiesAll.length() - 1) {
							CommoditiesAll = CommoditiesAll.substring(0, CommoditiesAll.length() - 1);
						}
						String[] arrayCommodities = CommoditiesAll.split(",");
						int arrayLength = arrayCommodities.length;
						for (int i = 0; i < arrayLength; i++) {
							commoditiesBuffer.append("'");
							commoditiesBuffer.append(arrayCommodities[i].trim());
							commoditiesBuffer.append("'");
							commoditiesBuffer.append(",");
						}
					}
					multipleCommodity = new String(commoditiesBuffer);
					if (!multipleCommodity.equals("")) {
						multipleCommodity = multipleCommodity.substring(0, multipleCommodity.length() - 1);
						multipleCommodity=multipleCommodity.substring(1);
						multipleCommodity=multipleCommodity.substring(0,multipleCommodity.length()-1);
					} else if (multipleCommodity.equals("")) {
						multipleCommodity = new String("''");
					}
				}
				else if(paramName.equalsIgnoreCase("Company Divisions")){
					CompanyDivisions=reportsManager.findByCompanyDivisions(sessionCorpID);
					String CompanyDivisionsAll=CompanyDivisions.keySet().toString();
					CompanyDivisionsAll = (CompanyDivisionsAll.replace("[", "").replace("]", ""));
					StringBuffer companyDivisionsBuffer = new StringBuffer("");
					if (CompanyDivisionsAll == null || CompanyDivisionsAll.equals("") || CompanyDivisionsAll.equals(",")) {
					} else {
						if (CompanyDivisionsAll.indexOf(",") == 0) {
							CompanyDivisionsAll = CompanyDivisionsAll.substring(1);
						}
						if (CompanyDivisionsAll.lastIndexOf(",") == CompanyDivisionsAll.length() - 1) {
							CompanyDivisionsAll = CompanyDivisionsAll.substring(0, CompanyDivisionsAll.length() - 1);
						}
						String[] arrayCompanyDivisions = CompanyDivisionsAll.split(",");
						int arrayLength = arrayCompanyDivisions.length;
						for (int i = 0; i < arrayLength; i++) {
							companyDivisionsBuffer.append("'");
							companyDivisionsBuffer.append(arrayCompanyDivisions[i].trim());
							companyDivisionsBuffer.append("'");
							companyDivisionsBuffer.append(",");
						}
					}
					multipleCompanyDivisions = new String(companyDivisionsBuffer);
					if (!multipleCompanyDivisions.equals("")) {
						multipleCompanyDivisions=multipleCompanyDivisions.substring(0, multipleCompanyDivisions.length() - 1);
						multipleCompanyDivisions=multipleCompanyDivisions.substring(1);
						multipleCompanyDivisions=multipleCompanyDivisions.substring(0,multipleCompanyDivisions.length()-1);
					} else if (multipleCompanyDivisions.equals("")) {
						multipleCompanyDivisions = new String("''");
					}
				}
				else if(paramName.equalsIgnoreCase("Compare")){
					if(jobNumber!=null ){
						compare = reportsManager.findRelatedSO(sessionCorpID,jobNumber,jobNumber.substring(0, 11));
						} else {
						compare = reportsManager.findRelatedSO(sessionCorpID,jobNumber,jobNumber);	
						}				
				}				
				else if(paramName.equalsIgnoreCase("RedSky Customer")){
					redskyCustomer=reportsManager.findByRedskyCustomer();
				}				
				else if(paramName.equalsIgnoreCase("ROUTING")){
					ROUTING=reportsManager.findByRouting(sessionCorpID);	
				}
				else if(paramName.equalsIgnoreCase("Currency")){
					CURRENCY= refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
				}
				else if(paramName.equalsIgnoreCase("BANK")){
					BANK= reportsManager.findByBank(sessionCorpID);	
				}
				else if(paramName.equalsIgnoreCase("Job Type")){
					JOB=reportsManager.findByJob(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Company Division")){
					COMPANYCODE=reportsManager.findByCompanyCode(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Movement")){
					customsMovement= refMasterManager.findByParameter(sessionCorpID, "CustomsMovement");
				}
				else if(paramName.equalsIgnoreCase("Status")){
					customStatus= refMasterManager.findByParameter(sessionCorpID, "CUSTOM_STATUS"); 
				}
				else if(paramName.equalsIgnoreCase("Contract")){
					contract=reportsManager.findContract(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Origin Base")){
					BaseScore=reportsManager.findByBaseScore(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("order")){
					order = refMasterManager.findByParameter(sessionCorpID, "REPORT_ORDER");
				}
				else if(paramName.equalsIgnoreCase("Sales Person")){
					sale = refMasterManager.findUser(sessionCorpID, "ROLE_SALE");
				}
				else if(paramName.equalsIgnoreCase("Sales_Person")){
					saleperson = refMasterManager.findUserUGCORP("ROLE_SALE");
				}
				else if(paramName.equalsIgnoreCase("Job Owner")){
					jobOwner= reportsManager.findJobOwner(sessionCorpID, "ROLE_SALE");
				}
				else if(paramName.equalsIgnoreCase("Partner Name")){
					if((reportModule!=null)&&(reportModule.trim().equalsIgnoreCase("serviceOrder"))){
						List<ServiceOrder> serviceOrderList = serviceOrderManager.findShipNumber(jobNumber);
						ServiceOrder serviceOrder=new ServiceOrder();
					     if (!(serviceOrderList == null || serviceOrderList.isEmpty())) {
							for (ServiceOrder serviceOrderTemp : serviceOrderList) {
								serviceOrder=serviceOrderTemp;
							}
							if((trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerCode(),trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerName());
							if((trackingStatusManager.get(serviceOrder.getId()).getOriginAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getOriginAgentCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getOriginAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getOriginAgent());
							if((trackingStatusManager.get(serviceOrder.getId()).getDestinationAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getDestinationAgentCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getDestinationAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getDestinationAgent());
							if((trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgentCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgent());
							if((trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgentCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgent());
							if((trackingStatusManager.get(serviceOrder.getId()).getBrokerCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getBrokerCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getBrokerCode(),trackingStatusManager.get(serviceOrder.getId()).getBrokerName());
							if((trackingStatusManager.get(serviceOrder.getId()).getForwarderCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getForwarderCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getForwarderCode(),trackingStatusManager.get(serviceOrder.getId()).getForwarder());
							if((billingManager.get(serviceOrder.getId()).getBillToCode()!=null)&&(!billingManager.get(serviceOrder.getId()).getBillToCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(billingManager.get(serviceOrder.getId()).getBillToCode(),billingManager.get(serviceOrder.getId()).getBillToName());
							if((billingManager.get(serviceOrder.getId()).getBillTo2Code()!=null)&&(!billingManager.get(serviceOrder.getId()).getBillTo2Code().equalsIgnoreCase("")))
								partnerCodeArrayList.put(billingManager.get(serviceOrder.getId()).getBillTo2Code(),billingManager.get(serviceOrder.getId()).getBillTo2Name());
							if((serviceOrder!=null)&&(serviceOrder.getShipNumber()!=null)){
								List<AccountLine> accountLineListTemp = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),"TRUE");
								for(AccountLine acc:accountLineListTemp){
									if(acc.getBillToCode()!=null && !acc.getBillToCode().equalsIgnoreCase("")){
										partnerCodeArrayList.put(acc.getBillToCode(), acc.getBillToName());
									}
									if(acc.getVendorCode()!=null && !acc.getVendorCode().equalsIgnoreCase("")){
										partnerCodeArrayList.put(acc.getVendorCode(), acc.getEstimateVendorName());
									}
								}
							}							
						}
					}
				}
				else if(paramName.equalsIgnoreCase("Sales Person / Consultant") || paramName.equalsIgnoreCase("Relationship Owner")){
					owner = reportsManager.findOwner(sessionCorpID, "ROLE_SALE");
				}	
				else if(paramName.equalsIgnoreCase("Billing Group")){
					storageBillingGroup = refMasterManager.findByParameter(sessionCorpID, "BILLGRP");
				}
				else if(paramName.equalsIgnoreCase("Agent Group")){
					agentGroup = reportsManager.findAgentGroup(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Credit Terms")){
					crTerms = refMasterManager.findByCrTerms(sessionCorpID, jobNumber);
				}
				else if(paramName.equalsIgnoreCase("Predefined Text")){
					preDefTxt=refMasterManager.findCodeOnleByParameter(sessionCorpID, "PREDEFINEDTEXT");
				}
				else if(paramName.equalsIgnoreCase("Vanline Agency")){
					vanLineCodeList = reportsManager.getVanLineCodeCompDiv(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Relocation Services")){
					relocationServices = reportsManager.findRelocationServices(sessionCorpID);
				} else {}
			}
			return SUCCESS;
		}
		return INPUT;
	}
	private TrackingStatusManager trackingStatusManager;
	private BillingManager billingManager;
	private String partnerCode;
	private List userListName = new ArrayList();
	private PartnerManager partnerManager;
	private String tempRec="";
	private ReportEmailDTO reportEmail;
	private TreeMap<String, ReportEmailDTO> userNameSort; 
	@SkipValidation
	public String linkingUserList(){
		List list = partnerManager.findPartnerCode(partnerCode);
		Partner partner  = (!list.isEmpty())?(Partner)list.get(0):new Partner();
		if(partner !=null && partner.getBillingEmail()!=null && (!(partner.getBillingEmail().trim().equals("")))){
		reportEmail=new ReportEmailDTO();
		reportEmail.setId(partner.getId());
		reportEmail.setFirstName(partner.getFirstName());
		reportEmail.setLastName(partner.getLastName());
		reportEmail.setUserName("");
		reportEmail.setEmail(partner.getBillingEmail());
		}
		userListName = refMasterManager.findReportEmailViaCompanyDivision(sessionCorpID, partner.getPartnerCode());
		if((userListName==null)||(userListName.isEmpty())||(userListName.get(0)==null)){
			userListName = refMasterManager.findReportEmailList(partner.getPartnerCode(), sessionCorpID);
		}
		generatePartnerReportEmail();
		if(userListName!=null){
		if(reportEmail!=null){
			userListName.add(reportEmail);	
		}
		List <ReportEmailDTO>al=userListName;
		userNameSort=new TreeMap<String, ReportEmailDTO>();
		for(ReportEmailDTO rec:al){
			userNameSort.put((rec.getFirstName().toString().toUpperCase()+""+rec.getLastName().toString().toUpperCase()+""+rec.getEmail().toString().toUpperCase()).toString().trim(), rec);
		}
		userListName.clear();
		for(Map.Entry<String, ReportEmailDTO> ss:userNameSort.entrySet()){
			userListName.add(ss.getValue());
		}
		}
		return SUCCESS;
	}
	@SkipValidation
	public String defaultStationaryDetails(){
		printOptionsValue=reportsManager.getDefaultStationary("",partnerCode,sessionCorpID);
		return SUCCESS;	
	}
	private String strFilePath="";
	private String linkCostingPage="";
	public String getLinkCostingPage() {
		return linkCostingPage;
	}

	public void setLinkCostingPage(String linkCostingPage) {
		this.linkCostingPage = linkCostingPage;
	}

	@SkipValidation
	private void generatePartnerReportEmail(){
		String temp="";
		if((reportModule!=null)&&(reportModule.trim().equalsIgnoreCase("customerFile"))){
			List<CustomerFile> customerFileList = customerFileManager.findSequenceNumber(jobNumber);
			CustomerFile customerFile=new CustomerFile();
		     if (!(customerFileList == null || customerFileList.isEmpty())) {
				for (CustomerFile customerFileTemp : customerFileList) {
					customerFile=customerFileTemp;
				}

				if((customerFile.getOriginAgentCode()!=null)&&(!customerFile.getOriginAgentCode().equalsIgnoreCase("")))
					partnerCodeArrayList.put(customerFile.getOriginAgentCode(),customerFile.getOriginAgentName());

				if((customerFile.getBillToCode()!=null)&&(!customerFile.getBillToCode().equalsIgnoreCase("")))
					partnerCodeArrayList.put(customerFile.getBillToCode(),customerFile.getBillToName());
				
				if((customerFile.getAccountCode()!=null)&&(!customerFile.getAccountCode().equalsIgnoreCase("")))
					partnerCodeArrayList.put(customerFile.getAccountCode(),customerFile.getAccountName());
				
				reportSubject=formNameVal+": "+customerFile.getSequenceNumber()+": "+(((customerFile.getFirstName()!=null)&&(!customerFile.getFirstName().equalsIgnoreCase("")))?customerFile.getFirstName()+",":" ")+""+(((customerFile.getLastName()!=null)&&(!customerFile.getLastName().equalsIgnoreCase("")))?customerFile.getLastName():" ")+": "+(((customerFile.getOriginCity()!=null)&&(!customerFile.getOriginCity().equalsIgnoreCase("")))?customerFile.getOriginCity():" ")+"("+(((customerFile.getOriginCountryCode()!=null)&&(!customerFile.getOriginCountryCode().equalsIgnoreCase("")))?customerFile.getOriginCountryCode():" ")+")->"+(((customerFile.getDestinationCity()!=null)&&(!customerFile.getDestinationCity().equalsIgnoreCase("")))?customerFile.getDestinationCity():" ")+"("+(((customerFile.getDestinationCountryCode()!=null)&&(!customerFile.getDestinationCountryCode().equalsIgnoreCase("")))?customerFile.getDestinationCountryCode():" ")+")";
				if((customerFile.getEmail()!=null)&&(!customerFile.getEmail().equalsIgnoreCase(""))){
				reportEmail=new ReportEmailDTO();
				reportEmail.setId(customerFile.getId());
				reportEmail.setFirstName(customerFile.getFirstName());
				reportEmail.setLastName(customerFile.getLastName());
				reportEmail.setUserName("");
				reportEmail.setEmail(customerFile.getEmail());
				}
				
		     }
		     
		}
		if((reportModule!=null)&&(reportModule.trim().equalsIgnoreCase("quotation"))){
			List<CustomerFile> customerFileList = customerFileManager.findSequenceNumber(jobNumber);
			CustomerFile customerFile=new CustomerFile();
		     if (!(customerFileList == null || customerFileList.isEmpty())) {
				for (CustomerFile customerFileTemp : customerFileList) {
					customerFile=customerFileTemp;
				}
				if((customerFile.getBillToCode()!=null)&&(!customerFile.getBillToCode().equalsIgnoreCase(""))){

					partnerCodeArrayList.put(customerFile.getBillToCode(),customerFile.getBillToName());
				}
				reportSubject=formNameVal+": "+customerFile.getSequenceNumber()+": "+(((customerFile.getFirstName()!=null)&&(!customerFile.getFirstName().equalsIgnoreCase("")))?customerFile.getFirstName()+",":" ")+""+(((customerFile.getLastName()!=null)&&(!customerFile.getLastName().equalsIgnoreCase("")))?customerFile.getLastName():" ")+": "+(((customerFile.getOriginCity()!=null)&&(!customerFile.getOriginCity().equalsIgnoreCase("")))?customerFile.getOriginCity():" ")+"("+(((customerFile.getOriginCountryCode()!=null)&&(!customerFile.getOriginCountryCode().equalsIgnoreCase("")))?customerFile.getOriginCountryCode():" ")+")->"+(((customerFile.getDestinationCity()!=null)&&(!customerFile.getDestinationCity().equalsIgnoreCase("")))?customerFile.getDestinationCity():" ")+"("+(((customerFile.getDestinationCountryCode()!=null)&&(!customerFile.getDestinationCountryCode().equalsIgnoreCase("")))?customerFile.getDestinationCountryCode():" ")+")";
				if((customerFile.getEmail()!=null)&&(!customerFile.getEmail().equalsIgnoreCase(""))){
				reportEmail=new ReportEmailDTO();
				reportEmail.setId(customerFile.getId());
				reportEmail.setFirstName(customerFile.getFirstName());
				reportEmail.setLastName(customerFile.getLastName());
				reportEmail.setUserName("");
				reportEmail.setEmail(customerFile.getEmail());
				}
		     }
		}
		if((reportModule!=null)&&(reportModule.trim().equalsIgnoreCase("quote"))){
			try{
			List<CustomerFile> customerFileList = customerFileManager.findSequenceNumber(serviceOrderManager.get(Long.parseLong((serviceOrderManager.findIdByShipNumber(jobNumber)).get(0).toString())).getSequenceNumber());
			CustomerFile customerFile=new CustomerFile();
		     if (!(customerFileList == null || customerFileList.isEmpty())) {
				for (CustomerFile customerFileTemp : customerFileList) {
					customerFile=customerFileTemp;
				}
				if((customerFile.getBillToCode()!=null)&&(!customerFile.getBillToCode().equalsIgnoreCase(""))){

					partnerCodeArrayList.put(customerFile.getBillToCode(),customerFile.getBillToName());
				}
				reportSubject=formNameVal+": "+customerFile.getSequenceNumber()+": "+(((customerFile.getFirstName()!=null)&&(!customerFile.getFirstName().equalsIgnoreCase("")))?customerFile.getFirstName()+",":" ")+""+(((customerFile.getLastName()!=null)&&(!customerFile.getLastName().equalsIgnoreCase("")))?customerFile.getLastName():" ")+": "+(((customerFile.getOriginCity()!=null)&&(!customerFile.getOriginCity().equalsIgnoreCase("")))?customerFile.getOriginCity():" ")+"("+(((customerFile.getOriginCountryCode()!=null)&&(!customerFile.getOriginCountryCode().equalsIgnoreCase("")))?customerFile.getOriginCountryCode():" ")+")->"+(((customerFile.getDestinationCity()!=null)&&(!customerFile.getDestinationCity().equalsIgnoreCase("")))?customerFile.getDestinationCity():" ")+"("+(((customerFile.getDestinationCountryCode()!=null)&&(!customerFile.getDestinationCountryCode().equalsIgnoreCase("")))?customerFile.getDestinationCountryCode():" ")+")";
				if((customerFile.getEmail()!=null)&&(!customerFile.getEmail().equalsIgnoreCase(""))){
				reportEmail=new ReportEmailDTO();
				reportEmail.setId(customerFile.getId());
				reportEmail.setFirstName(customerFile.getFirstName());
				reportEmail.setLastName(customerFile.getLastName());
				reportEmail.setUserName("");
				reportEmail.setEmail(customerFile.getEmail());
				}
		     }}catch(Exception e){}
		}		
		if((reportModule!=null)&&(reportModule.trim().equalsIgnoreCase("serviceOrder"))){
			List<ServiceOrder> serviceOrderList = serviceOrderManager.findShipNumber(jobNumber);
			ServiceOrder serviceOrder=new ServiceOrder();
		     if (!(serviceOrderList == null || serviceOrderList.isEmpty())) {
				for (ServiceOrder serviceOrderTemp : serviceOrderList) {
					serviceOrder=serviceOrderTemp;
				}
				if(!linkCostingPage.equalsIgnoreCase("")){
					linkCostingPage=linkCostingPage+""+serviceOrder.getId();
				}
				if((trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerCode().equalsIgnoreCase("")))
					partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerCode(),trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerName());
				if((trackingStatusManager.get(serviceOrder.getId()).getOriginAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getOriginAgentCode().equalsIgnoreCase("")))
					partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getOriginAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getOriginAgent());
				if((trackingStatusManager.get(serviceOrder.getId()).getDestinationAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getDestinationAgentCode().equalsIgnoreCase("")))
					partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getDestinationAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getDestinationAgent());
				if((trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgentCode().equalsIgnoreCase("")))
					partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgent());
				if((trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgentCode().equalsIgnoreCase("")))
					partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgent());
				if((trackingStatusManager.get(serviceOrder.getId()).getBrokerCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getBrokerCode().equalsIgnoreCase("")))
					partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getBrokerCode(),trackingStatusManager.get(serviceOrder.getId()).getBrokerName());
				if((trackingStatusManager.get(serviceOrder.getId()).getForwarderCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getForwarderCode().equalsIgnoreCase("")))
					partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getForwarderCode(),trackingStatusManager.get(serviceOrder.getId()).getForwarder());
				if((billingManager.get(serviceOrder.getId()).getBillToCode()!=null)&&(!billingManager.get(serviceOrder.getId()).getBillToCode().equalsIgnoreCase("")))
					partnerCodeArrayList.put(billingManager.get(serviceOrder.getId()).getBillToCode(),billingManager.get(serviceOrder.getId()).getBillToName());
				if((billingManager.get(serviceOrder.getId()).getBillTo2Code()!=null)&&(!billingManager.get(serviceOrder.getId()).getBillTo2Code().equalsIgnoreCase("")))
					partnerCodeArrayList.put(billingManager.get(serviceOrder.getId()).getBillTo2Code(),billingManager.get(serviceOrder.getId()).getBillTo2Name());						
				if((serviceOrder!=null)&&(serviceOrder.getShipNumber()!=null)){
					List<AccountLine> accountLineListTemp = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),"TRUE");
					for(AccountLine acc:accountLineListTemp){
						if(acc.getBillToCode()!=null && !acc.getBillToCode().equalsIgnoreCase("")){
							partnerCodeArrayList.put(acc.getBillToCode(), acc.getBillToName());
						}
						if(acc.getVendorCode()!=null && !acc.getVendorCode().equalsIgnoreCase("")){
							partnerCodeArrayList.put(acc.getVendorCode(), acc.getEstimateVendorName());
						}
					}
				}				
				reportSubject=formNameVal+": "+serviceOrder.getShipNumber()+": "+(((serviceOrder.getFirstName()!=null)&&(!serviceOrder.getFirstName().equalsIgnoreCase("")))?serviceOrder.getFirstName()+",":" ")+""+(((serviceOrder.getLastName()!=null)&&(!serviceOrder.getLastName().equalsIgnoreCase("")))?serviceOrder.getLastName():" ")+": "+(((serviceOrder.getOriginCity()!=null)&&(!serviceOrder.getOriginCity().equalsIgnoreCase("")))?serviceOrder.getOriginCity():" ")+"("+(((serviceOrder.getOriginCountryCode()!=null)&&(!serviceOrder.getOriginCountryCode().equalsIgnoreCase("")))?serviceOrder.getOriginCountryCode():" ")+")->"+(((serviceOrder.getDestinationCity()!=null)&&(!serviceOrder.getDestinationCity().equalsIgnoreCase("")))?serviceOrder.getDestinationCity():" ")+"("+(((serviceOrder.getDestinationCountryCode()!=null)&&(!serviceOrder.getDestinationCountryCode().equalsIgnoreCase("")))?serviceOrder.getDestinationCountryCode():" ")+")";
				if((serviceOrder.getEmail()!=null)&&(!serviceOrder.getEmail().equalsIgnoreCase(""))){
					reportEmail=new ReportEmailDTO();
					reportEmail.setId(serviceOrder.getId());
					reportEmail.setFirstName(serviceOrder.getFirstName());
					reportEmail.setLastName(serviceOrder.getLastName());
					reportEmail.setUserName("");
					reportEmail.setEmail(serviceOrder.getEmail());
					}
				try{
					defaultBillingEmail=billingManager.get(serviceOrder.getId()).getBillingEmail();
					}catch(Exception e){
						e.printStackTrace();
					}
			}
		}

	}
	
	private Map<String, String> partnerCodeArrayList = new TreeMap<String, String>(); 
	private String formNameVal;
	private String reportSubject;
	private String reportBody;
	private String reportSignature;
	private String recipientTo;
	private String recipientCC;
	private String typedoc;
	private String reportSignaturePart;
	private String defaultBillingEmail;
	private List vanLineCodeList;
 
	public List getVanLineCodeList() {
		return vanLineCodeList;
	}

	public void setVanLineCodeList(List vanLineCodeList) {
		this.vanLineCodeList = vanLineCodeList;
	}

	@SkipValidation
	public String viewFormParamEmailSetup() throws Exception {
		//getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List reportValidationFlagList=reportsManager.getReportsValidationFlag(sessionCorpID);
		if(!reportValidationFlagList.isEmpty()){
			reportValidationFlag=reportValidationFlagList.get(0).toString();
		}
		if(jobNumber!=null && !jobNumber.equals("") && invoiceNumber!=null && !invoiceNumber.equals("") ){
			printOptionsValue=reportsManager.getDefaultStationary(jobNumber,invoiceNumber,sessionCorpID);
		}
		reports = reportsManager.get(id);
		reportBody=reports.getEmailBody();
		String directLink=getRequest().getRequestURL().toString().replaceAll(getRequest().getRequestURI().toString(), "/redsky")+reports.getAttachedURL();
		if((reports.getAttachedURL()!=null)&&(!reports.getAttachedURL().equalsIgnoreCase(""))){
		linkCostingPage=directLink;
		
		}else{
			linkCostingPage="";
		}
		initializeRequiredParameters();
		generatePartnerReportEmail();
		//viewReportParam();
		if (!reportParameters.isEmpty()) {
			Iterator<JRParameter> paramIt =  reportParameters.iterator();
			while(paramIt.hasNext()){
				JRParameter jrParam = (JRParameter)paramIt.next();
				String paramName = jrParam.getName();
				if(paramName.equalsIgnoreCase("Crew Name")){
					CREWNAME=reportsManager.findCrewName(sessionCorpID);	
				}
				if(paramName.equalsIgnoreCase("Crew Type")){
					CREWTYPE=reportsManager.findCrewType(sessionCorpID);	
				}
				else if(paramName.equalsIgnoreCase("Warehouse")){
					house=reportsManager.findByModuleReports(sessionCorpID);	
				}
				else if(paramName.equalsIgnoreCase("UserName")){
					usrName=refMasterManager.getSuperVisor(sessionCorpID);	
				}
				else if(paramName.equalsIgnoreCase("Coordinator")){
					soList=reportsManager.findByModuleCoordinator(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Estimator")){
					esList=reportsManager.findByModuleEstimator(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Payment Status")){
					payableStatusList=reportsManager.findByPayableStatus();
				}
				else if(paramName.equalsIgnoreCase("SO Container")){
					if(jobNumber!=null ){
						containerList = reportsManager.findContainer(sessionCorpID, jobNumber);
						} else {
						containerList = reportsManager.findContainer(sessionCorpID, "");	
						}				
				}
				else if(paramName.equalsIgnoreCase("Approval Status")){
					payingStatusList=reportsManager.findByPayingStatus();
				}
				else if(paramName.equalsIgnoreCase("Service_Contract")){
					OMNI=reportsManager.findByOmni();
				}
				else if(paramName.equalsIgnoreCase("Commodity")){
					COMMODITS=reportsManager.findByCommodity(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("ROUTING")){
					ROUTING=reportsManager.findByRouting(sessionCorpID);	
				}
				else if(paramName.equalsIgnoreCase("Currency")){
					CURRENCY= refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
				}
				else if(paramName.equalsIgnoreCase("BANK")){
					BANK= reportsManager.findByBank(sessionCorpID);	
				}
				else if(paramName.equalsIgnoreCase("Job Type")){
					JOB=reportsManager.findByJob(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Company Division")){
					COMPANYCODE=reportsManager.findByCompanyCode(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Movement")){
					customsMovement= refMasterManager.findByParameter(sessionCorpID, "CustomsMovement");
				}
				else if(paramName.equalsIgnoreCase("Status")){
					customStatus= refMasterManager.findByParameter(sessionCorpID, "CUSTOM_STATUS"); 
				}
				else if(paramName.equalsIgnoreCase("Contract")){
					contract=reportsManager.findContract(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Origin Base")){
					BaseScore=reportsManager.findByBaseScore(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("order")){
					order = refMasterManager.findByParameter(sessionCorpID, "REPORT_ORDER");
				}
				else if(paramName.equalsIgnoreCase("Sales Person")){
					sale = refMasterManager.findUser(sessionCorpID, "ROLE_SALE");
				}
				else if(paramName.equalsIgnoreCase("Sales_Person")){
					saleperson = refMasterManager.findUserUGCORP("ROLE_SALE");
				}
				else if(paramName.equalsIgnoreCase("Partner Name")){
					if((reportModule!=null)&&(reportModule.trim().equalsIgnoreCase("serviceOrder"))){
						List<ServiceOrder> serviceOrderList = serviceOrderManager.findShipNumber(jobNumber);
						ServiceOrder serviceOrder=new ServiceOrder();
					     if (!(serviceOrderList == null || serviceOrderList.isEmpty())) {
							for (ServiceOrder serviceOrderTemp : serviceOrderList) {
								serviceOrder=serviceOrderTemp;
							}
							if((trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerCode(),trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerName());
							if((trackingStatusManager.get(serviceOrder.getId()).getOriginAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getOriginAgentCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getOriginAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getOriginAgent());
							if((trackingStatusManager.get(serviceOrder.getId()).getDestinationAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getDestinationAgentCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getDestinationAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getDestinationAgent());
							if((trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgentCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgent());
							if((trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgentCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgent());
							if((trackingStatusManager.get(serviceOrder.getId()).getBrokerCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getBrokerCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getBrokerCode(),trackingStatusManager.get(serviceOrder.getId()).getBrokerName());
							if((trackingStatusManager.get(serviceOrder.getId()).getForwarderCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getForwarderCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(trackingStatusManager.get(serviceOrder.getId()).getForwarderCode(),trackingStatusManager.get(serviceOrder.getId()).getForwarder());
							if((billingManager.get(serviceOrder.getId()).getBillToCode()!=null)&&(!billingManager.get(serviceOrder.getId()).getBillToCode().equalsIgnoreCase("")))
								partnerCodeArrayList.put(billingManager.get(serviceOrder.getId()).getBillToCode(),billingManager.get(serviceOrder.getId()).getBillToName());
							if((billingManager.get(serviceOrder.getId()).getBillTo2Code()!=null)&&(!billingManager.get(serviceOrder.getId()).getBillTo2Code().equalsIgnoreCase("")))
								partnerCodeArrayList.put(billingManager.get(serviceOrder.getId()).getBillTo2Code(),billingManager.get(serviceOrder.getId()).getBillTo2Name());
							if((serviceOrder!=null)&&(serviceOrder.getShipNumber()!=null)){
								List<AccountLine> accountLineListTemp = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),"TRUE");
								for(AccountLine acc:accountLineListTemp){
									if(acc.getBillToCode()!=null && !acc.getBillToCode().equalsIgnoreCase("")){
										partnerCodeArrayList.put(acc.getBillToCode(), acc.getBillToName());
									}
									if(acc.getVendorCode()!=null && !acc.getVendorCode().equalsIgnoreCase("")){
										partnerCodeArrayList.put(acc.getVendorCode(), acc.getEstimateVendorName());
									}
								}
							}
							try{
							defaultBillingEmail=billingManager.get(serviceOrder.getId()).getBillingEmail();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
				}
				else if(paramName.equalsIgnoreCase("Job Owner") || paramName.equalsIgnoreCase("Sales Person / Consultant") || paramName.equalsIgnoreCase("Relationship Owner")){
					owner = reportsManager.findOwner(sessionCorpID, "ROLE_SALE");
				}	
				else if(paramName.equalsIgnoreCase("Billing Group")){
					storageBillingGroup = refMasterManager.findByParameter(sessionCorpID, "BILLGRP");
				}
				else if(paramName.equalsIgnoreCase("Agent Group")){
					agentGroup = reportsManager.findAgentGroup(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Credit Terms")){
					crTerms = refMasterManager.findByCrTerms(sessionCorpID, jobNumber);
				}
				else if(paramName.equalsIgnoreCase("Predefined Text")){
					preDefTxt=refMasterManager.findCodeOnleByParameter(sessionCorpID, "PREDEFINEDTEXT");
				}
				else if(paramName.equalsIgnoreCase("Print Options")){
					billToDefaultStationary = reportsManager.billToDefaultStationary(sessionCorpID, invoiceNumber);
				}
				else if(paramName.equalsIgnoreCase("Vanline Agency")){
					vanLineCodeList = reportsManager.getVanLineCodeCompDiv(sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Relocation Services")){
					relocationServices = reportsManager.findRelocationServices(sessionCorpID);
				} else {}
			}

			return SUCCESS;
		}
		//viewReport();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		return INPUT;
	}	
	@SkipValidation
	private void initializeRequiredParameters() {
		JasperReport reportTemplate = reportsManager.getReportTemplate(id);
		if(reportTemplate!=null){
		JRParameter[] allParameters = reportTemplate.getParameters();
		reportParameters = new ArrayList<JRParameter>();
		for (JRParameter parameter : allParameters) {
			if (!parameter.isSystemDefined()) {
				reportParameters.add(parameter);

			}
		}
		}
	}
	private String invoiceIncludeFlag;
@SkipValidation
public String emailViewReportWithParam() throws Exception {
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	//getComboList(sessionCorpID);
	byte[] output;
	Map parameters = new HashMap();
	if(formFrom.equalsIgnoreCase("list")){
		JasperReport reportTemplate = reportsManager.getReportTemplate(id);
		JRParameter[] allParameters = reportTemplate.getParameters();
		reportParameters = new ArrayList<JRParameter>();
		
		for (JRParameter parameter : allParameters) {
			if (!parameter.isSystemDefined()) {
				reportParameters.add(parameter);
			}
		}
		for (JRParameter parameter : reportParameters) {
			if (!parameter.getValueClassName().equals("java.util.Date")) {
				String paramName = parameter.getName();
				
				if(paramName.equalsIgnoreCase("Service Order Number")){
					parameters.put(parameter.getName(), jobNumber);
				}
				else if(paramName.equalsIgnoreCase("Regnum")){
					parameters.put(parameter.getName(), regNumber);
				}
				else if(paramName.equalsIgnoreCase("User Name")){
					parameters.put(parameter.getName(), getRequest().getRemoteUser());
				}
				else if(paramName.equalsIgnoreCase("Corporate ID")){
					parameters.put(parameter.getName(), sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Work Ticket Number")){
					parameters.put(parameter.getName(), noteID);
				}
				else if(paramName.equalsIgnoreCase("Customer File Number")){
					parameters.put(parameter.getName(), custID);
				}
				else if(paramName.equalsIgnoreCase("Claim Number")){
					parameters.put(parameter.getName(), claimNumber);
				}
				else if(paramName.equalsIgnoreCase("Book Number")){
					parameters.put(parameter.getName(), bookNumber);
				}
				else if(paramName.equalsIgnoreCase("Invoice Number")){
					parameters.put(parameter.getName(), invoiceNumber);
				}else if(paramName.equalsIgnoreCase("Quotation File Number")){
					parameters.put(parameter.getName(), jobNumber);
				}
				else{
					parameters.put(parameter.getName(), "");
				}
			}
		}
	}else{
		parameters = buildParameters();
	}
	
	JasperPrint jasperPrint = new JasperPrint();
	company= companyManager.findByCorpID(sessionCorpID).get(0);
	if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
	Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
	parameters.put(JRParameter.REPORT_LOCALE, locale);
	} else {
	Locale locale = new Locale("en", "US");
	parameters.put(JRParameter.REPORT_LOCALE, locale);
	}
	ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	reports = reportsManager.get(id);
	String invoiceAttachedLocation="";
	try{
	if(reports.getFormReportFlag()==null || reports.getFormReportFlag().equalsIgnoreCase("") || reports.getFormReportFlag().equalsIgnoreCase("F")){
		reportfieldSectionList=reportsFieldSectionManager.reportfieldSectionList(reports.getId(),sessionCorpID,"update");
		Iterator itr = reportfieldSectionList.iterator();
		while(itr.hasNext()){
		Object obj=itr.next();
			if(((ReportFieldSectionDTO)obj).getFieldname()!=null ){
				String modelName=(((((ReportFieldSectionDTO)obj).getFieldname()!=null) && (!((ReportFieldSectionDTO)obj).getFieldname().toString().equalsIgnoreCase(""))) ?((ReportFieldSectionDTO)obj).getFieldname().toString().substring(0, ((ReportFieldSectionDTO)obj).getFieldname().toString().indexOf(".")):"");
				String fieldName=(((((ReportFieldSectionDTO)obj).getFieldname()!=null) && (!((ReportFieldSectionDTO)obj).getFieldname().toString().equalsIgnoreCase(""))) ?((ReportFieldSectionDTO)obj).getFieldname().toString().substring(((ReportFieldSectionDTO)obj).getFieldname().toString().indexOf(".")+1, ((ReportFieldSectionDTO)obj).getFieldname().toString().length()):"");				                
				String value = (((ReportFieldSectionDTO)obj).getFieldvalue()!=null?((ReportFieldSectionDTO)obj).getFieldvalue().toString():"");				
				String condition = reports.getFormCondition();
				if((!modelName.equalsIgnoreCase(""))&&(!fieldName.equalsIgnoreCase(""))&&(!value.equalsIgnoreCase(""))&&(condition.trim().equalsIgnoreCase(""))){
					if(modelName.equalsIgnoreCase("serviceOrder")){
						Long sid=null;
						try{
							sid=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
						}catch (Exception e){
							sid=Long.parseLong(accountLineManager.findShipNumByInvoiceId(parameters.get("Invoice Number").toString()).get(0).toString());
						}
							reportsManager.updateTableStatus(modelName,fieldName,value,sid);
					}else if(modelName.equalsIgnoreCase("customerFile")){
						Long sid=customerFileManager.findRemoteCustomerFile(parameters.get("Customer File Number").toString());
						reportsManager.updateTableStatus(modelName,fieldName,value,sid);
					}else if(modelName.equalsIgnoreCase("workTicket")){
						Long sid=workTicketManager.findRemoteWorkTicket(parameters.get("Work Ticket Number").toString());
						reportsManager.updateTableStatus(modelName,fieldName,value,sid);
					}else if(modelName.equalsIgnoreCase("accountLine")){
						Long sid=Long.parseLong(accountLineManager.getAllRemoteAccountLine(serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString())).toString());
						reportsManager.updateTableStatus(modelName,fieldName,value,sid);
					}
				}else{
					if((!modelName.equalsIgnoreCase(""))&&(!fieldName.equalsIgnoreCase(""))&&(!value.equalsIgnoreCase(""))&&(!condition.equalsIgnoreCase(""))){
						if(modelName.equalsIgnoreCase("serviceOrder")){
							Long sid=null;
							try{
								sid=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
							}catch (Exception e){
								sid=Long.parseLong(accountLineManager.findShipNumByInvoiceId(parameters.get("Invoice Number").toString()).get(0).toString());
							}
								reportsManager.updateTableStatusWithCondition(modelName,fieldName,value,condition,sid);
						}else if(modelName.equalsIgnoreCase("customerFile")){
							Long sid=customerFileManager.findRemoteCustomerFile(parameters.get("Customer File Number").toString());
							reportsManager.updateTableStatusWithCondition(modelName,fieldName,value,condition,sid);
						}else if(modelName.equalsIgnoreCase("workTicket")){
							Long sid=workTicketManager.findRemoteWorkTicket(parameters.get("Work Ticket Number").toString());
							reportsManager.updateTableStatusWithCondition(modelName,fieldName,value,condition,sid);
						}else if(modelName.equalsIgnoreCase("accountline")){
							Long aa=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
							String sid=accountLineManager.getRemoteAccountLine(aa).toString();
							sid = sid.replace("[", "").replace("]", "").trim();
							reportsManager.updateTableStatusWithCondition1(modelName,fieldName,value,condition,sid);
						}
					}
				}				
			}
		}
		}
	}catch(Exception e){}
		
		/*String modelName=(((reports.getFormFieldName()!=null) && (!reports.getFormFieldName().equalsIgnoreCase(""))) ?reports.getFormFieldName().substring(0, reports.getFormFieldName().indexOf(".")):"");
		String fieldName=(((reports.getFormFieldName()!=null) && (!reports.getFormFieldName().equalsIgnoreCase(""))) ?reports.getFormFieldName().substring(reports.getFormFieldName().indexOf(".")+1,reports.getFormFieldName().length()):"");
		String value = reports.getFormValue();
		String condition = reports.getFormCondition();
		if((!modelName.equalsIgnoreCase(""))&&(!fieldName.equalsIgnoreCase(""))&&(!value.equalsIgnoreCase(""))&&(condition.trim().equalsIgnoreCase(""))){
			if(modelName.equalsIgnoreCase("serviceOrder")){
				Long sid=null;
				try{
					sid=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
				}catch (Exception e){
					sid=Long.parseLong(accountLineManager.findShipNumByInvoiceId(parameters.get("Invoice Number").toString()).get(0).toString());
				}
					reportsManager.updateTableStatus(modelName,fieldName,value,sid);
			}else if(modelName.equalsIgnoreCase("customerFile")){
				Long sid=customerFileManager.findRemoteCustomerFile(parameters.get("Customer File Number").toString());
				reportsManager.updateTableStatus(modelName,fieldName,value,sid);
			}else if(modelName.equalsIgnoreCase("workTicket")){
				Long sid=workTicketManager.findRemoteWorkTicket(parameters.get("Work Ticket Number").toString());
				reportsManager.updateTableStatus(modelName,fieldName,value,sid);
			}else if(modelName.equalsIgnoreCase("accountLine")){
				Long sid=Long.parseLong(accountLineManager.getAllRemoteAccountLine(serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString())).toString());
				reportsManager.updateTableStatus(modelName,fieldName,value,sid);
			}
		}else{
			if((!modelName.equalsIgnoreCase(""))&&(!fieldName.equalsIgnoreCase(""))&&(!value.equalsIgnoreCase(""))&&(!condition.equalsIgnoreCase(""))){
				if(modelName.equalsIgnoreCase("serviceOrder")){
					Long sid=null;
					try{
						sid=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
					}catch (Exception e){
						sid=Long.parseLong(accountLineManager.findShipNumByInvoiceId(parameters.get("Invoice Number").toString()).get(0).toString());
					}
						reportsManager.updateTableStatusWithCondition(modelName,fieldName,value,condition,sid);
				}else if(modelName.equalsIgnoreCase("customerFile")){
					Long sid=customerFileManager.findRemoteCustomerFile(parameters.get("Customer File Number").toString());
					reportsManager.updateTableStatusWithCondition(modelName,fieldName,value,condition,sid);
				}else if(modelName.equalsIgnoreCase("workTicket")){
					Long sid=workTicketManager.findRemoteWorkTicket(parameters.get("Work Ticket Number").toString());
					reportsManager.updateTableStatusWithCondition(modelName,fieldName,value,condition,sid);
				}else if(modelName.equalsIgnoreCase("accountline")){
					Long aa=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
					String sid=accountLineManager.getRemoteAccountLine(aa).toString();
					sid = sid.replace("[", "").replace("]", "").trim();
					reportsManager.updateTableStatusWithCondition1(modelName,fieldName,value,condition,sid);
				}
			}
		}*/
	
	//#8630 - Mark document as invoice attachement Start
	if((reportModule!=null)&&((reportModule.trim().equalsIgnoreCase("serviceOrder")) || (reportModule.trim().equalsIgnoreCase("customerFile")))){
			try{
				List <MyFile>al=myFileManager.getListByDocId(jobNumber, "true", "", "");
				if(docFileType==null){
					docFileType="";
				}
				for(MyFile rec:al){
					if(rec.getInvoiceAttachment()!=null && rec.getInvoiceAttachment()){
						if((invoiceIncludeFlag!=null)&&(invoiceIncludeFlag.equalsIgnoreCase("TRUE"))){
							if(rec.getFileType().equalsIgnoreCase(docFileType)){
								if(invoiceAttachedLocation.equalsIgnoreCase("")){
									invoiceAttachedLocation=rec.getLocation();
								}else{
									invoiceAttachedLocation=invoiceAttachedLocation+","+rec.getLocation();
								}
							}
						}
					}
				}}catch(Exception e){e.printStackTrace();}
	     
	}
	//#8630 - Mark document as invoice attachement End
	String temp = reports.getReportName();
	reports.setReportHits(reports.getReportHits()+1);
	String fileName = temp.substring(0, temp.indexOf("."));
	if (typedoc.length() != 0 && typedoc.equalsIgnoreCase("PDF")) {
		jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
		output = JasperExportManager.exportReportToPdf(jasperPrint);
		
        String uploadDir =
            ServletActionContext.getServletContext().getRealPath("/images") +
            "/" ;
        String rightUploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
        String strFilePath="";
        try{
		        File dirPath = new File(rightUploadDir);
		        if (!dirPath.exists()) {
		            dirPath.mkdirs();
		        }
		        Long tempId=null;
		    	if(emailSetupManager.getMaxId().get(0)==null){
		    		tempId=1L;
		    		fileName = (tempId+ "_" +fileName);
		    	}
		    	else{
		    		tempId=Long.parseLong(emailSetupManager.getMaxId().get(0).toString());
		    		tempId=tempId+1;
		    		fileName = (tempId+"_" +fileName);
		    	}		        
				strFilePath=rightUploadDir+""+fileName+".pdf";
		        FileOutputStream fos = new FileOutputStream(strFilePath);
		        fos.write(output);
		        fos.close();
        }catch(Exception e){}
        if(!invoiceAttachedLocation.equalsIgnoreCase("")){
        	invoiceAttachedLocation=strFilePath+","+invoiceAttachedLocation;
        }else{
        	invoiceAttachedLocation=strFilePath;
        }
        String ids=emailExecuteForReport(invoiceAttachedLocation);
        saveItInFileCabinate(strFilePath,ids,reportModule);
        hitFlag="1";
	}
	if (typedoc.length() != 0 && typedoc.equalsIgnoreCase("DOC")) {
		jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
          JRRtfExporter exporterRTF = new JRRtfExporter();
          exporterRTF.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
          exporterRTF.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
          output=byteArrayOutputStream.toByteArray();
          
          String uploadDir =
              ServletActionContext.getServletContext().getRealPath("/images") +
              "/" ;
          String rightUploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");          
          String strFilePath="";
          try{
  		        File dirPath = new File(rightUploadDir);
  		        if (!dirPath.exists()) {
  		            dirPath.mkdirs();
  		        }
		        Long tempId=null;
		    	if(emailSetupManager.getMaxId().get(0)==null){
		    		tempId=1L;
		    		fileName = (tempId+ "_" +fileName);
		    	}
		    	else{
		    		tempId=Long.parseLong(emailSetupManager.getMaxId().get(0).toString());
		    		tempId=tempId+1;
		    		fileName = (tempId+"_" +fileName);
		    	}	  		        
  				strFilePath=rightUploadDir+""+fileName+".doc";
  		        FileOutputStream fos = new FileOutputStream(strFilePath);
  		        fos.write(output);
  		        fos.close();
          }catch(Exception e){}
          if(!invoiceAttachedLocation.equalsIgnoreCase("")){
          	invoiceAttachedLocation=strFilePath+","+invoiceAttachedLocation;
          }else{
          	invoiceAttachedLocation=strFilePath;
          }
          String ids=emailExecuteForReport(invoiceAttachedLocation);
          saveItInFileCabinate(strFilePath,ids,reportModule);
          hitFlag="1";
	}
	if (typedoc.length() != 0 && typedoc.equalsIgnoreCase("XLS")) {
		jasperPrint = reportsManager.generateReport(id, parameters);
		JRXlsExporter exporterXLS = new JRXlsExporter();
		exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
		exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
		exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
		exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		output=byteArrayOutputStream.toByteArray();
		
        String uploadDir =
            ServletActionContext.getServletContext().getRealPath("/images") +
            "/" ;
        
        String rightUploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
        String strFilePath="";
        try{
		        File dirPath = new File(rightUploadDir);
		        if (!dirPath.exists()) {
		            dirPath.mkdirs();
		        }
		        Long tempId=null;
		    	if(emailSetupManager.getMaxId().get(0)==null){
		    		tempId=1L;
		    		fileName = (tempId+ "_" +fileName);
		    	}
		    	else{
		    		tempId=Long.parseLong(emailSetupManager.getMaxId().get(0).toString());
		    		tempId=tempId+1;
		    		fileName = (tempId+"_" +fileName);
		    	}			        
				strFilePath=rightUploadDir+""+fileName+".xls";
		        FileOutputStream fos = new FileOutputStream(strFilePath);
		        fos.write(output);
		        fos.close();
        }catch(Exception e){}
        if(!invoiceAttachedLocation.equalsIgnoreCase("")){
        	invoiceAttachedLocation=strFilePath+","+invoiceAttachedLocation;
        }else{
        	invoiceAttachedLocation=strFilePath;
        }
        String ids=emailExecuteForReport(invoiceAttachedLocation);
        saveItInFileCabinate(strFilePath,ids,reportModule);
        hitFlag="1";
	}
	if (typedoc.length() != 0 && typedoc.equalsIgnoreCase("CSV")) {
		jasperPrint = reportsManager.generateReport(id, parameters);
		JRExporter exporterCSV = new JRCsvExporter();
		exporterCSV.setParameter(JRCsvExporterParameter.JASPER_PRINT, jasperPrint);
		exporterCSV.setParameter(JRCsvExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
		output=byteArrayOutputStream.toByteArray();

        String uploadDir =
            ServletActionContext.getServletContext().getRealPath("/images") +
            "/" ;
        
        String rightUploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
        String strFilePath="";
        try{
		        File dirPath = new File(rightUploadDir);
		        if (!dirPath.exists()) {
		            dirPath.mkdirs();
		        }
		        Long tempId=null;
		    	if(emailSetupManager.getMaxId().get(0)==null){
		    		tempId=1L;
		    		fileName = (tempId+ "_" +fileName);
		    	}
		    	else{
		    		tempId=Long.parseLong(emailSetupManager.getMaxId().get(0).toString());
		    		tempId=tempId+1;
		    		fileName = (tempId+"_" +fileName);
		    	}			        
				strFilePath=rightUploadDir+""+fileName+".csv";
		        FileOutputStream fos = new FileOutputStream(strFilePath);
		        fos.write(output);
		        fos.close();
        }catch(Exception e){}
        if(!invoiceAttachedLocation.equalsIgnoreCase("")){
        	invoiceAttachedLocation=strFilePath+","+invoiceAttachedLocation;
        }else{
        	invoiceAttachedLocation=strFilePath;
        }
        String ids=emailExecuteForReport(invoiceAttachedLocation);
        saveItInFileCabinate(strFilePath,ids,reportModule);
        hitFlag="1";
	}	
	if (typedoc.length() != 0 && typedoc.equalsIgnoreCase("HTM")) {
		jasperPrint = reportsManager.generateReport(id, parameters);
		JRExporter exporterHTML = new JRHtmlExporter();
		exporterHTML.setParameter(JRHtmlExporterParameter.JASPER_PRINT, jasperPrint);
		exporterHTML.setParameter(JRHtmlExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
		output=byteArrayOutputStream.toByteArray();
		 String uploadDir =
	            ServletActionContext.getServletContext().getRealPath("/images") +
	            "/" ;
	        
	        String rightUploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");	        
	        String strFilePath="";
	        try{
			        File dirPath = new File(rightUploadDir);
			        if (!dirPath.exists()) {
			            dirPath.mkdirs();
			        }
			        Long tempId=null;
			    	if(emailSetupManager.getMaxId().get(0)==null){
			    		tempId=1L;
			    		fileName = (tempId+ "_" +fileName);
			    	}
			    	else{
			    		tempId=Long.parseLong(emailSetupManager.getMaxId().get(0).toString());
			    		tempId=tempId+1;
			    		fileName = (tempId+"_" +fileName);
			    	}				        
					strFilePath=rightUploadDir+""+fileName+".html";
			        FileOutputStream fos = new FileOutputStream(strFilePath);
			        fos.write(output);
			        fos.close();
	        }catch(Exception e){}
	        if(!invoiceAttachedLocation.equalsIgnoreCase("")){
	        	invoiceAttachedLocation=strFilePath+","+invoiceAttachedLocation;
	        }else{
	        	invoiceAttachedLocation=strFilePath;
	        }
	        String ids=emailExecuteForReport(invoiceAttachedLocation);
	        saveItInFileCabinate(strFilePath,ids,reportModule);
	        hitFlag="1";
	}
	if (typedoc.length() != 0 && typedoc.equalsIgnoreCase("ext")) {

		String header = new String();
		String query = reportsManager.generateReportExtract(id, parameters);
		query = query.replaceAll(":", "-");
		List extractList = sqlExtractManager.sqlDataExtract(query);
		try {
			if (!extractList.isEmpty()) {
				int starValue = query.indexOf("AS")+3;
				int endValue = query.indexOf("FROM");
				String subQuery = query.substring(starValue,endValue);
				subQuery = subQuery+",";
				subQuery = subQuery.replaceAll("\n", "");
				String [] tempString = null;
		       
				tempString = subQuery.split("AS ");
				int totalLength = tempString.length;
				String finalOP ="";
				for (int i = 0 ; i < totalLength ; i++) {
		     	   String tmp = tempString[i];
		     	   finalOP += tmp.substring(0,tmp.indexOf(","))+"\t";
				}
				header = finalOP.toUpperCase().substring(0,finalOP.length())+"\n";
				Iterator it = extractList.iterator();
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();

					for(int i=1; i<totalLength+1; i++){
						if (row[i] != null) {
							String data = row[i].toString();
							header=header+data+"\t";
						} else {
							header=header+"\t";
						}
					}
					header=header+"\n";
			    }
			}else{
				String header1 = "NO Record Found , thanks";
				header=header+""+header1;
			}
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
		}
		output=header.getBytes();
		 String uploadDir =
	            ServletActionContext.getServletContext().getRealPath("/images") +
	            "/" ;
	        String rightUploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
	        String strFilePath="";
	        try{
			        File dirPath = new File(rightUploadDir);
			        if (!dirPath.exists()) {
			            dirPath.mkdirs();
			        }
			        Long tempId=null;
			    	if(emailSetupManager.getMaxId().get(0)==null){
			    		tempId=1L;
			    		fileName = (tempId+ "_" +fileName);
			    	}
			    	else{
			    		tempId=Long.parseLong(emailSetupManager.getMaxId().get(0).toString());
			    		tempId=tempId+1;
			    		fileName = (tempId+"_" +fileName);
			    	}				        
					strFilePath=rightUploadDir+""+fileName+".txt";
			        FileOutputStream fos = new FileOutputStream(strFilePath);
			        fos.write(output);
			        fos.close();
	        }catch(Exception e){}
	        if(!invoiceAttachedLocation.equalsIgnoreCase("")){
	        	invoiceAttachedLocation=strFilePath+","+invoiceAttachedLocation;
	        }else{
	        	invoiceAttachedLocation=strFilePath;
	        }
	        String ids=emailExecuteForReport(invoiceAttachedLocation);
	        saveItInFileCabinate(strFilePath,ids,reportModule);
	        hitFlag="1";
	}	
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	return SUCCESS;
}
public void saveItInFileCabinate(String strFilePath,String ids,String reportModuleName){
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	Map parameters = buildParameters();
	String checkValue=getRequest().getParameter("checkBoxData");
	company= companyManager.findByCorpID(sessionCorpID).get(0);
	if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
	Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
	parameters.put(JRParameter.REPORT_LOCALE, locale);
	} else {
	Locale locale = new Locale("en", "US");
	parameters.put(JRParameter.REPORT_LOCALE, locale);
	}
	if(jobNumber.trim().length() > 0){
		List seqNo = myFileManager.getSequanceNum(jobNumber, sessionCorpID);
		myFile.setCustomerNumber(seqNo.get(0).toString());
		myFile.setFileId(jobNumber);
	}else {
		JasperReport reportTemplate = reportsManager.getReportTemplate(id);
		JRParameter[] allParameters = reportTemplate.getParameters();
		reportParameters = new ArrayList<JRParameter>();
		for (JRParameter parameter : allParameters) {
				if (!parameter.isSystemDefined()) {
					reportParameters.add(parameter);
				}
		}
		for (JRParameter parameter : reportParameters) {
			if (!parameter.getValueClassName().equals("java.util.Date")) {
				String paramName = parameter.getName();
				if(paramName.equalsIgnoreCase("Customer File Number")){
						Object paramValue = parameters.put(parameter.getName(), custID);
						List seqNo = myFileManager.getSequanceNum(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber(seqNo.get(0).toString());
						myFile.setFileId(paramValue.toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Customer File Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
				else if(paramName.equalsIgnoreCase("Service Order Number")){
					Object paramValue = parameters.put(parameter.getName(), jobNumber);
					List seqNo = myFileManager.getSequanceNum(paramValue.toString(), sessionCorpID);
					if(!seqNo.isEmpty()){
					myFile.setCustomerNumber(seqNo.get(0).toString());
					myFile.setFileId(paramValue.toString());
				} else if (seqNo.isEmpty()) { 
						String key = "There are no Service Order Number which You Entered";   
			            saveMessage(getText(key));
					}
				}
				else if(paramName.equalsIgnoreCase("Invoice Number")){
					Object paramValue = parameters.put(parameter.getName(), invoiceNumber);
					List seqNo = myFileManager.getSequanceNumByInvNum(paramValue.toString(), sessionCorpID);
					if(!seqNo.isEmpty()){
					myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
					myFile.setFileId(seqNo.get(0).toString());
					} else if (seqNo.isEmpty()) { 
						String key = "There are no Invoice Number which You Entered";   
			            saveMessage(getText(key));
					}
				}
				else if(paramName.equalsIgnoreCase("Claim Number")){
					Object paramValue = parameters.put(parameter.getName(), claimNumber);
					List seqNo = myFileManager.getSequanceNumByClm(paramValue.toString(), sessionCorpID);
					if(!seqNo.isEmpty()){
					myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
					myFile.setFileId(seqNo.get(0).toString());
					} else if (seqNo.isEmpty()) { 
						String key = "There are no Claim Number which You Entered";   
			            saveMessage(getText(key));
					}
				}
				else if(paramName.equalsIgnoreCase("Work Ticket Number")){
					Object paramValue = parameters.put(parameter.getName(), noteID);
					List seqNo = myFileManager.getSequanceNumByTkt(paramValue.toString(), sessionCorpID);
					if(!seqNo.isEmpty()){
					myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
					myFile.setFileId(seqNo.get(0).toString());
					} else if (seqNo.isEmpty()) { 
						String key = "There are no Work Ticket Number which You Entered";   
			            saveMessage(getText(key));
					}
				}else if(paramName.equalsIgnoreCase("PartnerCode")|| paramName.equalsIgnoreCase("Bill to Code")||paramName.equalsIgnoreCase("Vendor Code")){
					Object paramValue=parameters.put(parameter.getName(), BillTo);
					if(BillTo!=null && !BillTo.equals(" ")){
						myFile.setCustomerNumber(BillTo);
					    myFile.setFileId(BillTo);
						
					} else{ 
						String key = "There are no Partner Code which You Entered";   
			            saveMessage(getText(key));
					}
				}
				
			} else {
				myFile.setCustomerNumber("");
				myFile.setFileId("");
			}
		}
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
		Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
		Locale locale = new Locale("en", "US");
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
	}
	reports = reportsManager.get(id);
	String temp = reports.getReportName();
	String tempFileFileName = temp.substring(0, temp.indexOf("."));
	fileFileName = tempFileFileName+".pdf";
	
	if(myFileManager.getMaxId(sessionCorpID).get(0)==null){
		id=1L;
		fileFileName = (id+ "_" +fileFileName);
	}
	else{
		Long id=Long.parseLong(myFileManager.getMaxId(sessionCorpID).get(0).toString());
	    	id=id+1;
	    	fileFileName = (id+"_" +fileFileName );
	}
	File newFile = new File(strFilePath);
	String filePath="";
	filePath =(strFilePath);
	myFile.setEmailStatus(ids);
	myFile.setFileContentType("application/pdf");
    myFile.setFileFileName(fileFileName);
    myFile.setLocation(filePath);
    myFile.setFileType(docFileType);
    if(myFile.getDescription()!=null && !myFile.getDescription().equals("")){
    	
    }else{
    	myFile.setDescription(docFileType);
    }
    myFile.setCorpID(sessionCorpID);
    myFile.setActive(true);
    myFile.setCreatedBy(getRequest().getRemoteUser());
    myFile.setUpdatedBy(getRequest().getRemoteUser());
    myFile.setCreatedOn(new Date());
    myFile.setUpdatedOn(new Date());
    documentCategory = refMasterManager.findDocCategoryByDocType(sessionCorpID, docFileType);
	if(documentCategory!=null && !documentCategory.equals(""))
	{
		myFile.setDocumentCategory(documentCategory);
	}else{
		myFile.setDocumentCategory("General");
	}
    FileSizeUtil fileSizeUtil = new FileSizeUtil(); 
    myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));    
    myFile.setMapFolder(mapFolder);    
    if(reports.getDocsxfer() != null && reports.getDocsxfer().equalsIgnoreCase("Yes")){
	    if(reports.getSecureForm() != null && reports.getSecureForm().equalsIgnoreCase("Yes")){
		    myFile.setIsSecure(true);
	    }else{
		    myFile.setIsSecure(false);
	    }  
    }else{
	    myFile.setIsSecure(false);
    } 
    if(myFile.getFileType().equalsIgnoreCase("Credit Card Authorization")){
		   myFile.setIsSecure(true);  
	   }
   // # 8630 - Mark document as invoice attachement start
    logger.warn("USER AUDIT: ID: "+docFileType+" ## User: "+getRequest().getRemoteUser()+" Start");
    try{
        if((checkValue!=null)&&(!checkValue.equalsIgnoreCase(""))){
        	String st[]=checkValue.split("~");
        	
        	if((st[0]!=null)&&(st[0].toString().equalsIgnoreCase("T"))){
        		myFile.setIsCportal(true);
        	}else{
        		myFile.setIsCportal(false);
        	}
        	if((st[1]!=null)&&(st[1].toString().equalsIgnoreCase("T"))){
        		myFile.setIsAccportal(true);
        	}else{
        		myFile.setIsAccportal(false);
        	}
        	
        	if((st[2]!=null)&&(st[2].toString().equalsIgnoreCase("T"))){
        		myFile.setIsPartnerPortal(true);
        	}else{
        		myFile.setIsPartnerPortal(false);
        	}    	
        	if((st[3]!=null)&&(st[3].toString().equalsIgnoreCase("T"))){
        		myFile.setIsServiceProvider(true);
        	}else{
        		myFile.setIsServiceProvider(false);
        	}    
        	
        	if((st[4]!=null)&&(st[4].toString().equalsIgnoreCase("T"))){
        		myFile.setIsBookingAgent(true);
        	}else{
        		myFile.setIsBookingAgent(false);
        	}
        	if((st[5]!=null)&&(st[5].toString().equalsIgnoreCase("T"))){
        		myFile.setIsNetworkAgent(true);
        	}else{
        		myFile.setIsNetworkAgent(false);
        	}
        	
        	if((st[6]!=null)&&(st[6].toString().equalsIgnoreCase("T"))){
        		myFile.setIsOriginAgent(true);
        	}else{
        		myFile.setIsOriginAgent(false);
        	}    	
        	if((st[7]!=null)&&(st[7].toString().equalsIgnoreCase("T"))){
        		myFile.setIsSubOriginAgent(true);
        	}else{
        		myFile.setIsSubOriginAgent(false);
        	}     
        	if((st[8]!=null)&&(st[8].toString().equalsIgnoreCase("T"))){
        		myFile.setIsDestAgent(true);
        	}else{
        		myFile.setIsDestAgent(false);
        	}
        	if((st[9]!=null)&&(st[9].toString().equalsIgnoreCase("T"))){
        		myFile.setIsSubDestAgent(true);
        	}else{
        		myFile.setIsSubDestAgent(false);
        	}
        	
        	if((st[10]!=null)&&(st[10].toString().equalsIgnoreCase("T"))){
        		myFile.setInvoiceAttachment(true);
        	}else{
        		myFile.setInvoiceAttachment(false);
        	}    	
        	if((st[11]!=null)&&(st[11].toString().equalsIgnoreCase("T"))){
        		myFile.setAccountLinePayingStatus("New");
        	}        	
        }
        }catch (Exception e) {
        	e.printStackTrace();
    	}
    
   /* if((docFileType!=null)&&(!docFileType.trim().toString().equalsIgnoreCase(""))){
    List docControl = myFileManager.getDocAccessControl(docFileType, sessionCorpID);
    if((docControl!=null)&&(!docControl.isEmpty())&&(docControl.get(0)!=null)&&(!docControl.get(0).toString().trim().equalsIgnoreCase(""))){
    	String st=docControl.get(0).toString().trim();
    	if((st.split("#")[10]!=null)&&(st.split("#")[10].toString().equalsIgnoreCase("true"))){
    		myFile.setInvoiceAttachment(true);
    	}
    	
    	if((st.split("#")[0]!=null)&&(st.split("#")[0].toString().equalsIgnoreCase("true"))){
    		myFile.setIsCportal(true);
    	}
    	if((st.split("#")[1]!=null)&&(st.split("#")[1].toString().equalsIgnoreCase("true"))){
    		myFile.setIsAccportal(true);
    	}
    	if((st.split("#")[2]!=null)&&(st.split("#")[2].toString().equalsIgnoreCase("true"))){
    		myFile.setIsPartnerPortal(true);
    	}
    	if((st.split("#")[3]!=null)&&(st.split("#")[3].toString().equalsIgnoreCase("true"))){
    		isBookingAgent = true;
    	}
    	if((st.split("#")[4]!=null)&&(st.split("#")[4].toString().equalsIgnoreCase("true"))){
    		isNetworkAgent=true;
    	}
    	if((st.split("#")[5]!=null)&&(st.split("#")[5].toString().equalsIgnoreCase("true"))){
    		isOriginAgent = true;
    	}
    	if((st.split("#")[6]!=null)&&(st.split("#")[6].toString().equalsIgnoreCase("true"))){
    		isSubOriginAgent = true;
    	}
    	if((st.split("#")[7]!=null)&&(st.split("#")[7].toString().equalsIgnoreCase("true"))){
    		isDestAgent = true;
    	}
    	if((st.split("#")[8]!=null)&&(st.split("#")[8].toString().equalsIgnoreCase("true"))){
    		isSubDestAgent = true;
    	}
    	if((st.split("#")[9]!=null)&&(st.split("#")[9].toString().equalsIgnoreCase("true"))){
    		myFile.setIsServiceProvider(true);
    	}
    	if((st.split("#")[11]!=null)&&(st.split("#")[11].toString().equalsIgnoreCase("true"))){
    		myFile.setAccountLinePayingStatus("New");
    	}
    }else{
    	myFile.setIsBookingAgent(false);
    	myFile.setIsNetworkAgent(false);
    	myFile.setIsOriginAgent(false);
    	myFile.setIsSubOriginAgent(false);
    	myFile.setIsDestAgent(false);
    	myFile.setIsSubDestAgent(false);
    }
    }else{
    	myFile.setIsBookingAgent(false);
    	myFile.setIsNetworkAgent(false);
    	myFile.setIsOriginAgent(false);
    	myFile.setIsSubOriginAgent(false);
    	myFile.setIsDestAgent(false);
    	myFile.setIsSubDestAgent(false);
    }*/
    logger.warn("USER AUDIT: ID: "+docFileType+" ## User: "+getRequest().getRemoteUser()+" end");
    if (reportModuleName.equalsIgnoreCase("serviceOrder")) {
		List shipNumberList = myFileManager.getShipNumberList(myFile.getFileId());
		if(shipNumberList!=null && !shipNumberList.isEmpty()){
		Iterator it = shipNumberList.iterator();
         while (it.hasNext()) {
        	String agentFlag=(String)it.next();
        	if(agentFlag.equalsIgnoreCase("BA") && isBookingAgent!=null && isBookingAgent==true){
        		myFile.setIsBookingAgent(true);
			}else if(agentFlag.equalsIgnoreCase("NA") && isNetworkAgent!=null && isNetworkAgent==true){
				myFile.setIsNetworkAgent(true);
			}else if(agentFlag.equalsIgnoreCase("OA") && isOriginAgent!=null && isOriginAgent==true){
				myFile.setIsOriginAgent(true);
			}else if(agentFlag.equalsIgnoreCase("SOA") && isSubOriginAgent!=null && isSubOriginAgent==true){
				myFile.setIsSubOriginAgent(true);
			}else if(agentFlag.equalsIgnoreCase("DA") && isDestAgent!=null && isDestAgent==true){
				myFile.setIsDestAgent(true);
			}else if(agentFlag.equalsIgnoreCase("SDA") && isSubDestAgent!=null && isSubDestAgent==true){
				myFile.setIsSubDestAgent(true);
			} 
         }
	}
		}
    
    if (reportModuleName.equalsIgnoreCase("customerFile")) {
    	customerFile = customerFileManager.get(customerFileManager.findRemoteCustomerFile(myFile.getCustomerNumber()));
    	if(customerFile!=null){
		List shipNumberList = myFileManager.findShipNumberBySequenceNumer(myFile.getCustomerNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
		if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
			Iterator shipIt =  shipNumberList.iterator();
				while(shipIt.hasNext()){
					String shipNumber = (String) shipIt.next();
					List shipNumberListSo = myFileManager.getShipNumberList(shipNumber);
					if(shipNumberListSo!=null && !shipNumberListSo.isEmpty()){
					Iterator it = shipNumberListSo.iterator();
			         while (it.hasNext()) {
			        	String agentFlag=(String)it.next();
			        	if(agentFlag.equalsIgnoreCase("BA") && isBookingAgent!=null && isBookingAgent==true){
			        		myFile.setIsBookingAgent(true);
						}else if(agentFlag.equalsIgnoreCase("NA") && isNetworkAgent!=null && isNetworkAgent==true){
							myFile.setIsNetworkAgent(true);
						}else if(agentFlag.equalsIgnoreCase("OA") && isOriginAgent!=null && isOriginAgent==true){
							myFile.setIsOriginAgent(true);
						}else if(agentFlag.equalsIgnoreCase("SOA") && isSubOriginAgent!=null && isSubOriginAgent==true){
							myFile.setIsSubOriginAgent(true);
						}else if(agentFlag.equalsIgnoreCase("DA") && isDestAgent!=null && isDestAgent==true){
							myFile.setIsDestAgent(true);
						}else if(agentFlag.equalsIgnoreCase("SDA") && isSubDestAgent!=null && isSubDestAgent==true){
							myFile.setIsSubDestAgent(true);
						} 
			         }
				}
			}
		}else{
			myFile.setIsBookingAgent(false);
	    	myFile.setIsNetworkAgent(false);
	    	myFile.setIsOriginAgent(false);
	    	myFile.setIsSubOriginAgent(false);
	    	myFile.setIsDestAgent(false);
	    	myFile.setIsSubDestAgent(false);
		}
	}
    }
    
    // # 8630 - Mark document as invoice attachement End
    logger.warn("USER AUDIT: ID: "+myFile.getId()+" ## User: "+getRequest().getRemoteUser()+" save");
    myFile = myFileManager.save(myFile);
    
    logger.warn("USER AUDIT: ID: "+myFile.getId()+" ## User: "+getRequest().getRemoteUser()+" save end");
    
    if (reportModuleName.equalsIgnoreCase("customerFile")) {
		if((myFile.getIsBookingAgent()!=null && myFile.getIsBookingAgent()) || (myFile.getIsNetworkAgent()!=null && myFile.getIsNetworkAgent() )
				|| ( myFile.getIsOriginAgent()!=null && myFile.getIsOriginAgent()) || (myFile.getIsSubOriginAgent()!=null && myFile.getIsSubOriginAgent()) 
				|| (myFile.getIsDestAgent()!=null && myFile.getIsDestAgent()) || (myFile.getIsSubDestAgent()!=null && myFile.getIsSubDestAgent())){																
			customerFile = customerFileManager.get(customerFileManager.findRemoteCustomerFile(myFile.getCustomerNumber()));
			if(customerFile!=null)
			{
		String networkLinkId="";
		List previousSequenceNumberList = new ArrayList();
		List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
		if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
			Iterator shipIt =  shipNumberList.iterator();
				while(shipIt.hasNext()){
					List linkedShipNumberList= new ArrayList();	
					String shipNumberOld = (String) shipIt.next();
		linkedShipNumberList=myFileManager.getLinkedShipNumber(shipNumberOld, "Primary", myFile.getIsBookingAgent(), myFile.getIsNetworkAgent(), myFile.getIsOriginAgent(), myFile.getIsSubOriginAgent(), myFile.getIsDestAgent(), myFile.getIsSubDestAgent());
		Set set = new HashSet(linkedShipNumberList);
		List linkedSeqNum = new ArrayList(set);
		if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
			Iterator it = linkedSeqNum.iterator();
	         while (it.hasNext()) {
	        	 	 String shipNumber=(String)it.next();
		        	 String removeDatarow = shipNumber.substring(0, 6);
		        	 if(!(myFile.getFileId().toString()).equalsIgnoreCase(shipNumber.substring(0, shipNumber.length()-2)) && !removeDatarow.equals("remove")){
			        	 networkLinkId = sessionCorpID + (myFile.getId()).toString();
			        	 String sequenceNumber = shipNumber.substring(0, shipNumber.length()-2);
			        	 if(previousSequenceNumberList.isEmpty() || (!(previousSequenceNumberList.contains(sequenceNumber)))){
			        	 previousSequenceNumberList.add(sequenceNumber);
			     		 myFileManager.upDateNetworkLinkId(networkLinkId,myFile.getId());
			             MyFile myFileObj = new MyFile();
			        	 myFileObj.setFileId(shipNumber.substring(0, shipNumber.length()-2));
			        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
			        	 myFileObj.setActive(myFile.getActive());
			        	 myFileObj.setCustomerNumber(shipNumber.substring(0, shipNumber.length()-2));
			        	 myFileObj.setDescription(myFile.getDescription());
			        	 myFileObj.setFileContentType(myFile.getFileContentType());
			        	 myFileObj.setFileFileName(myFile.getFileFileName());
			        	 myFileObj.setFileSize(myFile.getFileSize());
			        	 myFileObj.setFileType(myFile.getFileType());
			        	 myFileObj.setIsCportal(myFile.getIsCportal());
			        	 myFileObj.setIsAccportal(myFile.getIsAccportal());
			        	 myFileObj.setIsPartnerPortal(myFile.getIsPartnerPortal());
			        	 myFileObj.setIsServiceProvider(myFile.getIsServiceProvider());
			        	 myFileObj.setInvoiceAttachment(myFile.getInvoiceAttachment());
			        	 myFileObj.setIsBookingAgent(myFile.getIsBookingAgent());
			        	 myFileObj.setIsNetworkAgent(myFile.getIsNetworkAgent());
			        	 myFileObj.setIsOriginAgent(myFile.getIsOriginAgent());
			        	 myFileObj.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
			        	 myFileObj.setIsDestAgent(myFile.getIsDestAgent());
			        	 myFileObj.setIsSubDestAgent(myFile.getIsSubDestAgent());
			        	 myFileObj.setIsSecure(myFile.getIsSecure());
			        	 myFileObj.setLocation(myFile.getLocation());
			        	 myFileObj.setMapFolder(myFile.getMapFolder());
			        	 myFileObj.setCoverPageStripped(myFile.getCoverPageStripped());
			        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
			        	 myFileObj.setCreatedOn(new Date());
			        	 myFileObj.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
			        	 myFileObj.setUpdatedOn(new Date());
			        	 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
			        	 myFileObj.setNetworkLinkId(networkLinkId);
			        	 myFileManager.save(myFileObj);
			        	 }
		        	 }
		          }
		       }
	        }	
		  }
		}
	}
    }
    
    
    if (reportModuleName.equalsIgnoreCase("serviceOrder")) {
		if((myFile.getIsBookingAgent()!=null && myFile.getIsBookingAgent()) || (myFile.getIsNetworkAgent()!=null && myFile.getIsNetworkAgent()) || (myFile.getIsOriginAgent()!=null && myFile.getIsOriginAgent()) || (myFile.getIsSubOriginAgent()!=null && myFile.getIsSubOriginAgent()) || (myFile.getIsDestAgent()!=null && myFile.getIsDestAgent()) || (myFile.getIsSubDestAgent()!=null && myFile.getIsSubDestAgent())){
		List linkedShipNumberList= new ArrayList();	
		String networkLinkId="";
		linkedShipNumberList=myFileManager.getLinkedShipNumber(myFile.getFileId(), "Primary", myFile.getIsBookingAgent(), myFile.getIsNetworkAgent(), myFile.getIsOriginAgent(), myFile.getIsSubOriginAgent(), myFile.getIsDestAgent(), myFile.getIsSubDestAgent());
		Set set = new HashSet(linkedShipNumberList);
		List linkedSeqNum = new ArrayList(set);
		if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
			Iterator it = linkedSeqNum.iterator();
	         while (it.hasNext()) {
	        	 	 String shipNumber=(String)it.next();
		        	 String removeDatarow = shipNumber.substring(0, 6);
		        	 if(!(myFile.getFileId().toString()).equalsIgnoreCase(shipNumber) && !removeDatarow.equals("remove")){
			        	 networkLinkId = sessionCorpID + (myFile.getId()).toString();
			     		 myFileManager.upDateNetworkLinkId(networkLinkId,myFile.getId());
			             MyFile myFileObj = new MyFile();
			        	 myFileObj.setFileId(shipNumber);
			        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
			        	 myFileObj.setActive(myFile.getActive());
			        	 myFileObj.setCustomerNumber(shipNumber);
			        	 myFileObj.setDescription(myFile.getDescription());
			        	 myFileObj.setFileContentType(myFile.getFileContentType());
			        	 myFileObj.setFileFileName(myFile.getFileFileName());
			        	 myFileObj.setFileSize(myFile.getFileSize());
			        	 myFileObj.setFileType(myFile.getFileType());
			        	 myFileObj.setIsCportal(myFile.getIsCportal());
			        	 myFileObj.setIsAccportal(myFile.getIsAccportal());
			        	 myFileObj.setIsPartnerPortal(myFile.getIsPartnerPortal());
			        	 myFileObj.setIsServiceProvider(myFile.getIsServiceProvider());
			        	 myFileObj.setInvoiceAttachment(myFile.getInvoiceAttachment());
			        	 myFileObj.setIsBookingAgent(myFile.getIsBookingAgent());
			        	 myFileObj.setIsNetworkAgent(myFile.getIsNetworkAgent());
			        	 myFileObj.setIsOriginAgent(myFile.getIsOriginAgent());
			        	 myFileObj.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
			        	 myFileObj.setIsDestAgent(myFile.getIsDestAgent());
			        	 myFileObj.setIsSubDestAgent(myFile.getIsSubDestAgent());
			        	 myFileObj.setIsSecure(myFile.getIsSecure());
			        	 myFileObj.setLocation(myFile.getLocation());
			        	 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
			        	 myFileObj.setMapFolder(myFile.getMapFolder());
			        	 myFileObj.setCoverPageStripped(myFile.getCoverPageStripped());
			        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
			        	 myFileObj.setCreatedOn(new Date());
			        	 myFileObj.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
			        	 myFileObj.setUpdatedOn(new Date());
			        	 myFileObj.setNetworkLinkId(networkLinkId);
			        	 myFileManager.save(myFileObj);
		        	 }
		          }
		       }
	         }
		}
    
    
    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end");   
}
private String hitFlag="0";
private String emailStatus="";
private String emailStatusMsg="";
private EmailSetupManager emailSetupManager;
private EmailSetup emailSetup;
public String emailExecuteForReport(String strFilePath){
	
	String tempRecipient="";
	String tempRecipientArr[]=recipientTo.split(",");
	for(String str1:tempRecipientArr){
		if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
			if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
			tempRecipient += str1;
		}
	}
	recipientTo=tempRecipient;	
	
	tempRecipient="";
	tempRecipientArr=recipientCC.split(",");
	for(String str1:tempRecipientArr){
		if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
			if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
			tempRecipient += str1;
		}
	}
	recipientCC=tempRecipient;
	
			emailSetup= new EmailSetup();
			emailSetup.setAttchedFileLocation(strFilePath);
			reportBody=reportBody.replaceAll("\r\n", "<br>");
			emailSetup.setBody(reportBody);
			reportSignaturePart = reportSignaturePart.replaceAll("\r\n", "<br>");
			emailSetup.setSignaturePart(reportSignaturePart);
			emailSetup.setCorpId(sessionCorpID);
			emailSetup.setCreatedBy(getRequest().getRemoteUser());
			emailSetup.setCreatedOn(new Date());
			emailSetup.setDateSent(new Date());
			emailSetup.setEmailStatus("SaveForEmail");
			emailSetup.setRecipientBcc("");
			emailSetup.setRecipientCc(recipientCC);
			emailSetup.setRecipientTo(recipientTo);
			emailSetup.setRetryCount(0);
			emailSetup.setSignature(reportSignature);
			emailSetup.setSubject(reportSubject);
			emailSetup.setUpdatedBy(getRequest().getRemoteUser());
			emailSetup.setUpdatedOn(new Date());
			emailSetup.setFileNumber(jobNumber);
			emailSetup=emailSetupManager.save(emailSetup);
			return emailSetup.getId()+"";
}
private String emailStatusVal;
private String emailFlag;
private Date emailSetupDateSent;
private String rSignature;
private String rBcc;
@SkipValidation
public String openMailView() {
	if((emailStatusVal!=null)&&(!emailStatusVal.trim().equalsIgnoreCase(""))){
		EmailSetup emailSetup=emailSetupManager.get(Long.parseLong(emailStatusVal));
		emailFlag=emailSetup.getEmailStatus();		
		recipientTo=emailSetup.getRecipientTo();
		recipientCC=emailSetup.getRecipientCc();
		rBcc=emailSetup.getRecipientBcc();
		reportSubject=emailSetup.getSubject();
		reportBody=emailSetup.getBody();
		reportSignature=emailSetup.getSignaturePart();
		rSignature=emailSetup.getSignature();
        //SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
        //emailSetupDateSent = df.format(emailSetup.getDateSent());
		emailSetupDateSent=emailSetup.getDateSent();
        retryCount=emailSetup.getRetryCount();
	}
	return SUCCESS;
}
private AccountLineManager accountLineManager;
private WorkTicketManager workTicketManager;
private Long sid;

private String multipleCommodity;

private List selectedJobs;
public List getSelectedJobs() {
	return selectedJobs;
}

public void setSelectedJobs(List selectedJobs) {
	this.selectedJobs = selectedJobs;
}

public String getMultipleCommodity() {
	return multipleCommodity;
}
public void setMultipleCommodity(String multipleCommodity) {
	this.multipleCommodity = multipleCommodity;
}

@SkipValidation
private String printReports() throws Exception {
	byte[] output;
	Map parameters = new HashMap();
	
	List<PrintDailyPackage> printList = reportsManager.getByCorpId(sessionCorpID);
	
	for (PrintDailyPackage printDailyPackage : printList) {
		JasperReport reportTemplate = null; //reportsManager.getReportTemplateForWT(printDailyPackage.getJrxmlName(), "workTicket", sessionCorpID);
		JRParameter[] allParameters = reportTemplate.getParameters();
	
		reportParameters = new ArrayList<JRParameter>();
		for (JRParameter parameter : allParameters) {
			if (!parameter.isSystemDefined()) {
				reportParameters.add(parameter);
			}
		}
		for (JRParameter parameter : reportParameters) {
			if (!parameter.getValueClassName().equals("java.util.Date")) {
				String paramName = parameter.getName();
				
				if(paramName.equalsIgnoreCase("Warehouse")){
					parameters.put(parameter.getName(), whouse);
				}
				else if(paramName.equalsIgnoreCase("Work Ticket Number")){
					parameters.put(parameter.getName(), noteID);
				}
				else if(paramName.equalsIgnoreCase("Service Order Number")){
					parameters.put(parameter.getName(), jobNumber);
				}
				else if(paramName.equalsIgnoreCase("Corporate ID")){
					parameters.put(parameter.getName(), sessionCorpID);
				}
				else{
					parameters.put(parameter.getName(), "");
				}
			}
		}
	
		HttpServletResponse response = getResponse();
		HttpServletRequest request = getRequest();
		JasperPrint jasperPrint = new JasperPrint();
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
			Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
			Locale locale = new Locale("en", "US");
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		ServletOutputStream ouputStream = response.getOutputStream();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		reports = reportsManager.get(id);
	
		try{
			String modelName=(((reports.getFormFieldName()!=null) && (!reports.getFormFieldName().equalsIgnoreCase(""))) ?reports.getFormFieldName().substring(0, reports.getFormFieldName().indexOf(".")):"");
			String fieldName=(((reports.getFormFieldName()!=null) && (!reports.getFormFieldName().equalsIgnoreCase(""))) ?reports.getFormFieldName().substring(reports.getFormFieldName().indexOf(".")+1,reports.getFormFieldName().length()):"");
			String value = reports.getFormValue();
			String condition = reports.getFormCondition();
			if((!modelName.equalsIgnoreCase(""))&&(!fieldName.equalsIgnoreCase(""))&&(!value.equalsIgnoreCase(""))&&(condition.trim().equalsIgnoreCase(""))){
				if(modelName.equalsIgnoreCase("serviceOrder")){
					Long sid=null;
					try{
						sid=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
					}catch (Exception e){
						sid=Long.parseLong(accountLineManager.findShipNumByInvoiceId(parameters.get("Invoice Number").toString()).get(0).toString());
					}
					reportsManager.updateTableStatus(modelName,fieldName,value,sid);
				}else if(modelName.equalsIgnoreCase("customerFile")){
					Long sid=customerFileManager.findRemoteCustomerFile(parameters.get("Customer File Number").toString());
					reportsManager.updateTableStatus(modelName,fieldName,value,sid);
				}else if(modelName.equalsIgnoreCase("workTicket")){
					Long sid=workTicketManager.findRemoteWorkTicket(parameters.get("Work Ticket Number").toString());
					reportsManager.updateTableStatus(modelName,fieldName,value,sid);
				}else if(modelName.equalsIgnoreCase("accountLine")){
					Long sid=Long.parseLong(accountLineManager.getAllRemoteAccountLine(serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString())).toString());
					reportsManager.updateTableStatus(modelName,fieldName,value,sid);
				}
			}else{
				if((!modelName.equalsIgnoreCase(""))&&(!fieldName.equalsIgnoreCase(""))&&(!value.equalsIgnoreCase(""))&&(!condition.equalsIgnoreCase(""))){
					if(modelName.equalsIgnoreCase("serviceOrder")){
						Long sid=null;
						try{
							sid=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
						}catch (Exception e){
							sid=Long.parseLong(accountLineManager.findShipNumByInvoiceId(parameters.get("Invoice Number").toString()).get(0).toString());
						}
						reportsManager.updateTableStatusWithCondition(modelName,fieldName,value,condition,sid);
					}else if(modelName.equalsIgnoreCase("customerFile")){
						Long sid=customerFileManager.findRemoteCustomerFile(parameters.get("Customer File Number").toString());
						reportsManager.updateTableStatusWithCondition(modelName,fieldName,value,condition,sid);
					}else if(modelName.equalsIgnoreCase("workTicket")){
						Long sid=workTicketManager.findRemoteWorkTicket(parameters.get("Work Ticket Number").toString());
						reportsManager.updateTableStatusWithCondition(modelName,fieldName,value,condition,sid);
					}else if(modelName.equalsIgnoreCase("accountline")){
						Long aa=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
						String sid=accountLineManager.getRemoteAccountLine(aa).toString();
						sid = sid.replace("[", "").replace("]", "").trim();
						reportsManager.updateTableStatusWithCondition1(modelName,fieldName,value,condition,sid);
					}
				}
			}
		}catch(Exception e){}
	
		String temp = reports.getReportName();
		reports.setReportHits(reports.getReportHits()+1);
		String fileName = temp.substring(0, temp.indexOf("."));
		String fileType = request.getParameter("fileType");
		if (fileType.length() != 0 && fileType.equalsIgnoreCase("PDF")) {
			jasperPrint = reportsManager.generateReport(id, parameters);
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
			output = JasperExportManager.exportReportToPdf(jasperPrint);
			ouputStream.write(output);
		}else if (fileType.length() != 0 && fileType.equalsIgnoreCase("RTF")) {
			jasperPrint = reportsManager.generateReport(id, parameters);
			response.setContentType("application/rtf");
	          response.setHeader("Content-Disposition", "attachment; filename="+ fileName +".rtf");
	          JRRtfExporter exporterRTF = new JRRtfExporter();
	          exporterRTF.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
	          exporterRTF.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
	          exporterRTF.exportReport();
	          ouputStream.write(byteArrayOutputStream.toByteArray());
		}
	
		ouputStream.flush();
		ouputStream.close();
	}
	return null;
}

@SkipValidation
public String viewReportWithParam() throws Exception {
	byte[] output;
	Map parameters = new HashMap();
	company=companyManager.findByCorpID(sessionCorpID).get(0);
    if(company!=null){
      if(company.getExtractedFileAudit() !=null && company.getExtractedFileAudit()){
    	  extractFileAudit = true;
  		}
    }
	if(formFrom.equalsIgnoreCase("list")){
		JasperReport reportTemplate = reportsManager.getReportTemplate(id,preferredLanguage);
		JRParameter[] allParameters = reportTemplate.getParameters();
		reportParameters = new ArrayList<JRParameter>();
		if(jobNumber!=null && !jobNumber.equals("") && invoiceNumber!=null && !invoiceNumber.equals("") ){
			printOptionsValue=reportsManager.getDefaultStationary(jobNumber,invoiceNumber,sessionCorpID);
		}
		for (JRParameter parameter : allParameters) {
			if (!parameter.isSystemDefined()) {
				reportParameters.add(parameter);
			}
		}
		for (JRParameter parameter : reportParameters) {
			if (!parameter.getValueClassName().equals("java.util.Date")) {
				String paramName = parameter.getName();
				
				if(paramName.equalsIgnoreCase("Service Order Number")){
					parameters.put(parameter.getName(), jobNumber);
				}
				else if(paramName.equalsIgnoreCase("Regnum")){
					parameters.put(parameter.getName(), regNumber);
				}
				else if(paramName.equalsIgnoreCase("User Name")){
					parameters.put(parameter.getName(), getRequest().getRemoteUser());
				}
				else if(paramName.equalsIgnoreCase("Corporate ID")){
					parameters.put(parameter.getName(), sessionCorpID);
				}
				else if(paramName.equalsIgnoreCase("Work Ticket Number")){
					parameters.put(parameter.getName(), noteID);
				}
				else if(paramName.equalsIgnoreCase("Customer File Number")){
					parameters.put(parameter.getName(), custID);
				}
				else if(paramName.equalsIgnoreCase("Claim Number")){
					parameters.put(parameter.getName(), claimNumber);
				}
				else if(paramName.equalsIgnoreCase("Book Number")){
					parameters.put(parameter.getName(), bookNumber);
				}
				else if(paramName.equalsIgnoreCase("Invoice Number")){
					parameters.put(parameter.getName(), invoiceNumber);                  						
				}else if(paramName.equalsIgnoreCase("Print Options") && printOptionsValue!=null && !printOptionsValue.equals("")){
					parameters.put(parameter.getName(),printOptionsValue);	
				}else if(paramName.equalsIgnoreCase("Quotation File Number")){
					parameters.put(parameter.getName(), jobNumber);
				}else{
					parameters.put(parameter.getName(), "");
				}
			}
		}
	}else{
		parameters = buildParameters();
	}
	
	HttpServletResponse response = getResponse();
	HttpServletRequest request = getRequest();
	JasperPrint jasperPrint = new JasperPrint();
	//company= companyManager.findByCorpID(sessionCorpID).get(0);
	if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
	Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
	parameters.put(JRParameter.REPORT_LOCALE, locale);
	} else {
	Locale locale = new Locale("en", "US");
	parameters.put(JRParameter.REPORT_LOCALE, locale);
	}
	ServletOutputStream ouputStream = response.getOutputStream();
	ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	reports = reportsManager.get(id);
	//JasperReport reportTemplate = reportsManager.getReportTemplate(id,preferredLanguage);
	
	try{
	String modelName=(((reports.getFormFieldName()!=null) && (!reports.getFormFieldName().equalsIgnoreCase(""))) ?reports.getFormFieldName().substring(0, reports.getFormFieldName().indexOf(".")):"");
	String fieldName=(((reports.getFormFieldName()!=null) && (!reports.getFormFieldName().equalsIgnoreCase(""))) ?reports.getFormFieldName().substring(reports.getFormFieldName().indexOf(".")+1,reports.getFormFieldName().length()):"");
	String value = reports.getFormValue();
	String condition = reports.getFormCondition();
	if((!modelName.equalsIgnoreCase(""))&&(!fieldName.equalsIgnoreCase(""))&&(!value.equalsIgnoreCase(""))&&(condition.trim().equalsIgnoreCase(""))){
		if(modelName.equalsIgnoreCase("serviceOrder")){
			Long sid=null;
			try{
				sid=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
			}catch (Exception e){
				sid=Long.parseLong(accountLineManager.findShipNumByInvoiceId(parameters.get("Invoice Number").toString()).get(0).toString());
			}
				reportsManager.updateTableStatus(modelName,fieldName,value,sid);
		}else if(modelName.equalsIgnoreCase("customerFile")){
			Long sid=customerFileManager.findRemoteCustomerFile(parameters.get("Customer File Number").toString());
			reportsManager.updateTableStatus(modelName,fieldName,value,sid);
		}else if(modelName.equalsIgnoreCase("workTicket")){
			Long sid=workTicketManager.findRemoteWorkTicket(parameters.get("Work Ticket Number").toString());
			reportsManager.updateTableStatus(modelName,fieldName,value,sid);
		}else if(modelName.equalsIgnoreCase("accountLine")){
			Long sid=Long.parseLong(accountLineManager.getAllRemoteAccountLine(serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString())).toString());
			reportsManager.updateTableStatus(modelName,fieldName,value,sid);
		}
	}else{
		if((!modelName.equalsIgnoreCase(""))&&(!fieldName.equalsIgnoreCase(""))&&(!value.equalsIgnoreCase(""))&&(!condition.equalsIgnoreCase(""))){
			if(modelName.equalsIgnoreCase("serviceOrder")){
				Long sid=null;
				try{
					sid=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
				}catch (Exception e){
					sid=Long.parseLong(accountLineManager.findShipNumByInvoiceId(parameters.get("Invoice Number").toString()).get(0).toString());
				}
					reportsManager.updateTableStatusWithCondition(modelName,fieldName,value,condition,sid);
			}else if(modelName.equalsIgnoreCase("customerFile")){
				Long sid=customerFileManager.findRemoteCustomerFile(parameters.get("Customer File Number").toString());
				reportsManager.updateTableStatusWithCondition(modelName,fieldName,value,condition,sid);
			}else if(modelName.equalsIgnoreCase("workTicket")){
				Long sid=workTicketManager.findRemoteWorkTicket(parameters.get("Work Ticket Number").toString());
				reportsManager.updateTableStatusWithCondition(modelName,fieldName,value,condition,sid);
			}else if(modelName.equalsIgnoreCase("accountline")){
				Long aa=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
				String sid=accountLineManager.getRemoteAccountLine(aa).toString();
				sid = sid.replace("[", "").replace("]", "").trim();
				reportsManager.updateTableStatusWithCondition1(modelName,fieldName,value,condition,sid);
			}
		}
	}
}catch(Exception e){}
	String myfileId="";
	String oldReportName="";
	String temp = reports.getReportName();
	reports.setReportHits(reports.getReportHits()+1);
	String fileName = temp.substring(0, temp.indexOf("."));
	String fileType = request.getParameter("fileType");
	reportName=fileName+"."+fileType.toLowerCase();
	if(checkfromfilecabinetinvoicereportflag!=null && checkfromfilecabinetinvoicereportflag.equals("Y"))
	{
		List myFileIdList = reportsManager.getIdfromMyfileforServiceOrder(jobNumber,reports.getCorpID(),invoiceNumber,reportName);
		if(myFileIdList==null)
		{
			myFileIdList=new ArrayList();
		}
		if(myFileIdList!=null && !(myFileIdList.isEmpty()) && myFileIdList.get(0)!=null)
			{
				myfileId=myFileIdList.get(0).toString();
				myFile = myFileManager.get(Long.parseLong(myfileId.toString()));
				if(myFile!=null)
				{
					checkfilepath=myFile.getLocation();
					oldReportName=myFile.getFileFileName();
				}
			}
	}
	if (fileType.length() != 0 && fileType.equalsIgnoreCase("PDF")) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ## Report Name: "+temp+" Start In PDF");
		if((checkfilepath!=null) && (checkfromfilecabinetinvoicereportflag!=null) && (checkfromfilecabinetinvoicereportflag.equals("Y")) && !(checkfilepath.equals("")))
		{
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
		    File nfsPDF = new File(checkfilepath);
		    FileInputStream fis = new FileInputStream(nfsPDF);
		    BufferedInputStream bis = new BufferedInputStream(fis);
		    byte[] buffer = new byte[2048];
		    while (true) {
		      int bytesRead = bis.read(buffer, 0, buffer.length);
		      if (bytesRead < 0) {
		        break;
		      }
		      ouputStream.write(buffer, 0, bytesRead);
		    } 
		    bis.close();
if(extractFileAudit){
				
				response.setContentType("application/pdf");
				//ServletOutputStream outputStream = response.getOutputStream();
				SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
				StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
				String currentyear=yearDate.toString();
				
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
					dirPath.mkdirs();
				}
				SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
				StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
				String currentDateTime=tempDate.toString().trim();
				fileName=oldReportName+currentDateTime+"."+fileType.toLowerCase();
				response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
				String filePath=uploadDir+""+fileName;
				File file1 = new File(filePath); 
				FileInputStream fis1 = new FileInputStream(file1);
			    BufferedInputStream bis1 = new BufferedInputStream(fis1);
			    byte[] buffer1 = new byte[2048];
			    while (true) {
			      int bytesRead = bis1.read(buffer1, 0, buffer1.length);
			      if (bytesRead < 0) {
			        break;
			      }
			      ouputStream.write(buffer1, 0, bytesRead); 
			    }
				
				
				extractedFileLog = new ExtractedFileLog();
				extractedFileLog.setCorpID(sessionCorpID);
				extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
				extractedFileLog.setCreatedOn(new Date());
				extractedFileLog.setLocation(filePath);
				extractedFileLog.setFileName(fileName);
				extractedFileLog.setModule("Reports");
				extractedFileLog=extractedFileLogManager.save(extractedFileLog);
				}
		}
		else
		{
		try{
		if(reports.getCorpID().equalsIgnoreCase("TSFT") && (reports.getReportName().equalsIgnoreCase("ClientBillingSummary.jrxml") || reports.getReportName().equalsIgnoreCase("ClientBillingDetails.jrxml"))){
		redskyCorp=reportsManager.findByRedskyCustomerCorp(parameters.get("RedSky Customer").toString()).get(0).toString();
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("MMM-yyyy");
		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(parameters.get("End Date")));
		String dateFrom = nowYYYYMMDD1.toString();
		jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=" + redskyCorp+"_" + fileName +"_"+dateFrom+ ".pdf");
		} else {
			jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
		}
		output = JasperExportManager.exportReportToPdf(jasperPrint);
		ouputStream.write(output);
		if(extractFileAudit){
			
			response.setContentType("application/pdf");
			//ServletOutputStream outputStream = response.getOutputStream();
			SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
			StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
			String currentyear=yearDate.toString();
			
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
			File dirPath = new File(uploadDir);
					if (!dirPath.exists()) {
				dirPath.mkdirs();
			}
			SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
			StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
			String currentDateTime=tempDate.toString().trim();
			fileName=fileName+currentDateTime+".pdf";
			//response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
			String filePath=uploadDir+""+fileName;
			File file1 = new File(filePath); 
			FileOutputStream outputStream1 = new FileOutputStream(file1);
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
			outputStream1.write(output); 
		   
			
			
			extractedFileLog = new ExtractedFileLog();
			extractedFileLog.setCorpID(sessionCorpID);
			extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
			extractedFileLog.setCreatedOn(new Date());
			extractedFileLog.setLocation(filePath);
			extractedFileLog.setFileName(fileName);
			extractedFileLog.setModule("Reports");
			extractedFileLog=extractedFileLogManager.save(extractedFileLog);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end from PDF"); 
	}

	if (fileType.length() != 0 && fileType.equalsIgnoreCase("XLS")) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ## Report Name: "+temp+" Start In XLS");
		if((checkfilepath!=null) && (checkfromfilecabinetinvoicereportflag!=null) && (checkfromfilecabinetinvoicereportflag.equals("Y")) && !(checkfilepath.equals("")))
		{
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
		    File nfsPDF = new File(checkfilepath);
		    FileInputStream fis = new FileInputStream(nfsPDF);
		    BufferedInputStream bis = new BufferedInputStream(fis);
		    byte[] buffer = new byte[2048];
		    while (true) {
		      int bytesRead = bis.read(buffer, 0, buffer.length);
		      if (bytesRead < 0) {
		        break;
		      }
		      ouputStream.write(buffer, 0, bytesRead);
		    }
		    bis.close();
            if(extractFileAudit){
				
            	response.setContentType("application/vnd.ms-excel");
				//ServletOutputStream outputStream = response.getOutputStream();
				SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
				StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
				String currentyear=yearDate.toString();
				
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
					dirPath.mkdirs();
				}
				SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
				StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
				String currentDateTime=tempDate.toString().trim();
				fileName=oldReportName+currentDateTime+"."+fileType.toLowerCase();
				response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
				String filePath=uploadDir+""+fileName;
				File file1 = new File(filePath); 
				FileInputStream fis1 = new FileInputStream(file1);
			    BufferedInputStream bis1 = new BufferedInputStream(fis1);
			    byte[] buffer1 = new byte[2048];
			    while (true) {
			      int bytesRead = bis1.read(buffer1, 0, buffer1.length);
			      if (bytesRead < 0) {
			        break;
			      }
			      ouputStream.write(buffer1, 0, bytesRead); 
			    }
				
				
				extractedFileLog = new ExtractedFileLog();
				extractedFileLog.setCorpID(sessionCorpID);
				extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
				extractedFileLog.setCreatedOn(new Date());
				extractedFileLog.setLocation(filePath);
				extractedFileLog.setFileName(fileName);
				extractedFileLog.setModule("Reports");
				extractedFileLog=extractedFileLogManager.save(extractedFileLog);
				}
		}
		else
		{
		try {
			if(reports.getCorpID().equalsIgnoreCase("TSFT") && (reports.getReportName().equalsIgnoreCase("ClientBillingSummary.jrxml") || reports.getReportName().equalsIgnoreCase("ClientBillingDetails.jrxml"))){
			redskyCorp=reportsManager.findByRedskyCustomerCorp(parameters.get("RedSky Customer").toString()).get(0).toString();
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("MMM-yyyy");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(parameters.get("End Date")));
			String dateFrom = nowYYYYMMDD1.toString();
			jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=" + redskyCorp+"_" + fileName +"_"+dateFrom+ ".xls");
			} else {
				jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xls");	
			}
			JRXlsExporter exporterXLS = new JRXlsExporter();
			exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporterXLS.exportReport();
			ouputStream.write(byteArrayOutputStream.toByteArray());
			if(extractFileAudit){
				
				response.setContentType("application/vnd.ms-excel");
				//ServletOutputStream outputStream = response.getOutputStream();
				SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
				StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
				String currentyear=yearDate.toString();
				
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
					dirPath.mkdirs();
				}
				SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
				StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
				String currentDateTime=tempDate.toString().trim();
				fileName=fileName+currentDateTime+".xls";
				//response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
				String filePath=uploadDir+""+fileName;
				File file1 = new File(filePath); 
				FileOutputStream outputStream1 = new FileOutputStream(file1);
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
				outputStream1.write(byteArrayOutputStream.toByteArray()); 
			   
				
				
				extractedFileLog = new ExtractedFileLog();
				extractedFileLog.setCorpID(sessionCorpID);
				extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
				extractedFileLog.setCreatedOn(new Date());
				extractedFileLog.setLocation(filePath);
				extractedFileLog.setFileName(fileName);
				extractedFileLog.setModule("Reports");
				extractedFileLog=extractedFileLogManager.save(extractedFileLog);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end from XLS"); 
	}
	
	if (fileType.length() != 0 && fileType.equalsIgnoreCase("RTF")) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ## Report Name: "+temp+" Start In RTF");
		if((checkfilepath!=null) && (checkfromfilecabinetinvoicereportflag!=null) && (checkfromfilecabinetinvoicereportflag.equals("Y")) && !(checkfilepath.equals("")))
		{
			response.setContentType("application/rtf");
			response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
		    File nfsPDF = new File(checkfilepath);
		    FileInputStream fis = new FileInputStream(nfsPDF);
		    BufferedInputStream bis = new BufferedInputStream(fis);
		    byte[] buffer = new byte[2048];
		    while (true) {
		      int bytesRead = bis.read(buffer, 0, buffer.length);
		      if (bytesRead < 0) {
		        break;
		      }
		      ouputStream.write(buffer, 0, bytesRead);
		    }
		    bis.close();
if(extractFileAudit){
				
	            response.setContentType("application/rtf");
				//ServletOutputStream outputStream = response.getOutputStream();
				SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
				StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
				String currentyear=yearDate.toString();
				
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
					dirPath.mkdirs();
				}
				SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
				StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
				String currentDateTime=tempDate.toString().trim();
				fileName=oldReportName+currentDateTime+"."+fileType.toLowerCase();
				response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
				String filePath=uploadDir+""+fileName;
				File file1 = new File(filePath); 
				FileInputStream fis1 = new FileInputStream(file1);
			    BufferedInputStream bis1 = new BufferedInputStream(fis1);
			    byte[] buffer1 = new byte[2048];
			    while (true) {
			      int bytesRead = bis1.read(buffer1, 0, buffer1.length);
			      if (bytesRead < 0) {
			        break;
			      }
			      ouputStream.write(buffer, 0, bytesRead); 
			    }
				
				
				extractedFileLog = new ExtractedFileLog();
				extractedFileLog.setCorpID(sessionCorpID);
				extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
				extractedFileLog.setCreatedOn(new Date());
				extractedFileLog.setLocation(filePath);
				extractedFileLog.setFileName(fileName);
				extractedFileLog.setModule("Reports");
				extractedFileLog=extractedFileLogManager.save(extractedFileLog);
				}
		}
		else
		{
		try {
			if(reports.getCorpID().equalsIgnoreCase("TSFT") && (reports.getReportName().equalsIgnoreCase("ClientBillingSummary.jrxml") || reports.getReportName().equalsIgnoreCase("ClientBillingDetails.jrxml"))){
			redskyCorp=reportsManager.findByRedskyCustomerCorp(parameters.get("RedSky Customer").toString()).get(0).toString();
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("MMM-yyyy");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(parameters.get("End Date")));
			String dateFrom = nowYYYYMMDD1.toString();
			jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
			response.setContentType("application/rtf");
			response.setHeader("Content-Disposition", "attachment; filename=" + redskyCorp+"_" + fileName +"_"+dateFrom+ ".rtf");
			} else {
			jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
			response.setContentType("application/rtf");
			response.setHeader("Content-Disposition", "attachment; filename="+ fileName +".rtf");	
			}
			  JRRtfExporter exporterRTF = new JRRtfExporter();
			  exporterRTF.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			  exporterRTF.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
			  exporterRTF.exportReport();
			  ouputStream.write(byteArrayOutputStream.toByteArray());
			  if(extractFileAudit){
					
				    response.setContentType("application/rtf");
					//ServletOutputStream outputStream = response.getOutputStream();
					SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
					StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
					String currentyear=yearDate.toString();
					
					String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
					uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
					File dirPath = new File(uploadDir);
							if (!dirPath.exists()) {
						dirPath.mkdirs();
					}
					SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
					StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
					String currentDateTime=tempDate.toString().trim();
					fileName=fileName+currentDateTime+".rtf";
					//response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
					String filePath=uploadDir+""+fileName;
					File file1 = new File(filePath); 
					FileOutputStream outputStream1 = new FileOutputStream(file1);
					response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
					outputStream1.write(byteArrayOutputStream.toByteArray()); 
				   
					
					
					extractedFileLog = new ExtractedFileLog();
					extractedFileLog.setCorpID(sessionCorpID);
					extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
					extractedFileLog.setCreatedOn(new Date());
					extractedFileLog.setLocation(filePath);
					extractedFileLog.setFileName(fileName);
					extractedFileLog.setModule("Reports");
					extractedFileLog=extractedFileLogManager.save(extractedFileLog);
					}
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end from RTF"); 
	}
	
	if (fileType.length() != 0 && fileType.equalsIgnoreCase("DOCX")) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ## Report Name: "+temp+" Start In DOCX");
		if((checkfilepath!=null) && (checkfromfilecabinetinvoicereportflag!=null) && (checkfromfilecabinetinvoicereportflag.equals("Y")) && !(checkfilepath.equals("")))
		{
			response.setContentType("application/docx");
			response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
		    File nfsPDF = new File(checkfilepath);
		    FileInputStream fis = new FileInputStream(nfsPDF);
		    BufferedInputStream bis = new BufferedInputStream(fis);
		    byte[] buffer = new byte[2048];
		    while (true) {
		      int bytesRead = bis.read(buffer, 0, buffer.length);
		      if (bytesRead < 0) {
		        break;
		      }
		      ouputStream.write(buffer, 0, bytesRead);
		    }
		    bis.close();
if(extractFileAudit){
				
	             response.setContentType("application/docx");
				//ServletOutputStream outputStream = response.getOutputStream();
				SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
				StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
				String currentyear=yearDate.toString();
				
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
					dirPath.mkdirs();
				}
				SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
				StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
				String currentDateTime=tempDate.toString().trim();
				fileName=oldReportName+currentDateTime+"."+fileType.toLowerCase();
				response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
				String filePath=uploadDir+""+fileName;
				File file1 = new File(filePath); 
				FileInputStream fis1 = new FileInputStream(file1);
			    BufferedInputStream bis1 = new BufferedInputStream(fis1);
			    byte[] buffer1 = new byte[2048];
			    while (true) {
			      int bytesRead = bis1.read(buffer1, 0, buffer1.length);
			      if (bytesRead < 0) {
			        break;
			      }
			      ouputStream.write(buffer, 0, bytesRead); 
			    }
				
				
				extractedFileLog = new ExtractedFileLog();
				extractedFileLog.setCorpID(sessionCorpID);
				extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
				extractedFileLog.setCreatedOn(new Date());
				extractedFileLog.setLocation(filePath);
				extractedFileLog.setFileName(fileName);
				extractedFileLog.setModule("Reports");
				extractedFileLog=extractedFileLogManager.save(extractedFileLog);
				}
		}
		else
		{
		try {
			if(reports.getCorpID().equalsIgnoreCase("TSFT") && (reports.getReportName().equalsIgnoreCase("ClientBillingSummary.jrxml") || reports.getReportName().equalsIgnoreCase("ClientBillingDetails.jrxml"))){
				redskyCorp=reportsManager.findByRedskyCustomerCorp(parameters.get("RedSky Customer").toString()).get(0).toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("MMM-yyyy");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(parameters.get("End Date")));
				String dateFrom = nowYYYYMMDD1.toString();
				jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
				response.setContentType("application/docx");
				response.setHeader("Content-Disposition", "attachment; filename=" + redskyCorp+"_" + fileName +"_"+dateFrom+ ".docx");
			} else {
				jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
				response.setContentType("application/docx");
			    response.setHeader("Content-Disposition", "attachment; filename="+ fileName +".docx");	
			}
			JRDocxExporter exporterDocx = new JRDocxExporter();
			exporterDocx.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporterDocx.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
			exporterDocx.setParameter(JRDocxExporterParameter.FLEXIBLE_ROW_HEIGHT, Boolean.TRUE);
			exporterDocx.exportReport();
			ouputStream.write(byteArrayOutputStream.toByteArray());
			if(extractFileAudit){
				
				response.setContentType("application/docx");
				//ServletOutputStream outputStream = response.getOutputStream();
				SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
				StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
				String currentyear=yearDate.toString();
				
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
					dirPath.mkdirs();
				}
				SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
				StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
				String currentDateTime=tempDate.toString().trim();
				fileName=fileName+currentDateTime+".docx";
				//response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
				String filePath=uploadDir+""+fileName;
				File file1 = new File(filePath); 
				FileOutputStream outputStream1 = new FileOutputStream(file1);
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
				outputStream1.write(byteArrayOutputStream.toByteArray()); 
			   
				
				
				extractedFileLog = new ExtractedFileLog();
				extractedFileLog.setCorpID(sessionCorpID);
				extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
				extractedFileLog.setCreatedOn(new Date());
				extractedFileLog.setLocation(filePath);
				extractedFileLog.setFileName(fileName);
				extractedFileLog.setModule("Reports");
				extractedFileLog=extractedFileLogManager.save(extractedFileLog);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end from DOCX"); 
	}

	if (fileType.length() != 0 && fileType.equalsIgnoreCase("CSV")) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ## Report Name: "+temp+" Start In CSV");
		if((checkfilepath!=null) && (checkfromfilecabinetinvoicereportflag!=null) && (checkfromfilecabinetinvoicereportflag.equals("Y")) && !(checkfilepath.equals("")))
		{
			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
		    File nfsPDF = new File(checkfilepath);
		    FileInputStream fis = new FileInputStream(nfsPDF);
		    BufferedInputStream bis = new BufferedInputStream(fis);
		    byte[] buffer = new byte[2048];
		    while (true) {
		      int bytesRead = bis.read(buffer, 0, buffer.length);
		      if (bytesRead < 0) {
		        break;
		      }
		      ouputStream.write(buffer, 0, bytesRead);
		    }
		    bis.close();
if(extractFileAudit){
				
	            response.setContentType("text/csv");
				//ServletOutputStream outputStream = response.getOutputStream();
				SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
				StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
				String currentyear=yearDate.toString();
				
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
					dirPath.mkdirs();
				}
				SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
				StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
				String currentDateTime=tempDate.toString().trim();
				fileName=oldReportName+currentDateTime+"."+fileType.toLowerCase();
				response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
				String filePath=uploadDir+""+fileName;
				File file1 = new File(filePath); 
				FileInputStream fis1 = new FileInputStream(file1);
			    BufferedInputStream bis1 = new BufferedInputStream(fis1);
			    byte[] buffer1 = new byte[2048];
			    while (true) {
			      int bytesRead = bis1.read(buffer1, 0, buffer1.length);
			      if (bytesRead < 0) {
			        break;
			      }
			      ouputStream.write(buffer1, 0, bytesRead); 
			    }
				
				
				extractedFileLog = new ExtractedFileLog();
				extractedFileLog.setCorpID(sessionCorpID);
				extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
				extractedFileLog.setCreatedOn(new Date());
				extractedFileLog.setLocation(filePath);
				extractedFileLog.setFileName(fileName);
				extractedFileLog.setModule("Reports");
				extractedFileLog=extractedFileLogManager.save(extractedFileLog);
				}
		}else{
		try {
			if(reports.getCorpID().equalsIgnoreCase("TSFT") && (reports.getReportName().equalsIgnoreCase("ClientBillingSummary.jrxml") || reports.getReportName().equalsIgnoreCase("ClientBillingDetails.jrxml"))){
			redskyCorp=reportsManager.findByRedskyCustomerCorp(parameters.get("RedSky Customer").toString()).get(0).toString();
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("MMM-yyyy");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(parameters.get("End Date")));
			String dateFrom = nowYYYYMMDD1.toString();
			jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "attachment; filename=" + redskyCorp+"_" + fileName +"_"+dateFrom+ ".csv");
			} else {
			jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".csv");	
			}
			JRExporter exporterCSV = new JRCsvExporter();
			exporterCSV.setParameter(JRCsvExporterParameter.JASPER_PRINT, jasperPrint);
			exporterCSV.setParameter(JRCsvExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
			exporterCSV.exportReport();
			ouputStream.write(byteArrayOutputStream.toByteArray());
if(extractFileAudit){
				
	             response.setContentType("text/csv");
				//ServletOutputStream outputStream = response.getOutputStream();
				SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
				StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
				String currentyear=yearDate.toString();
				
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
					dirPath.mkdirs();
				}
				SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
				StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
				String currentDateTime=tempDate.toString().trim();
				fileName=fileName+currentDateTime+".csv";
				//response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
				String filePath=uploadDir+""+fileName;
				File file1 = new File(filePath); 
				FileOutputStream outputStream1 = new FileOutputStream(file1);
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
				outputStream1.write(byteArrayOutputStream.toByteArray()); 
			   
				
				
				extractedFileLog = new ExtractedFileLog();
				extractedFileLog.setCorpID(sessionCorpID);
				extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
				extractedFileLog.setCreatedOn(new Date());
				extractedFileLog.setLocation(filePath);
				extractedFileLog.setFileName(fileName);
				extractedFileLog.setModule("Reports");
				extractedFileLog=extractedFileLogManager.save(extractedFileLog);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end from CSV"); 
	}
	
	if (fileType.length() != 0 && fileType.equalsIgnoreCase("HTML")) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ## Report Name: "+temp+" Start In HTML");
		try{
			jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
			response.setContentType("text/html");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".html");
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			HashMap imagesMap = new HashMap();
		    request.getSession().setAttribute("IMAGES_MAP", imagesMap);
			request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
		  	JRExporter exporterHTML = new JRHtmlExporter();
			exporterHTML.setParameter(JRHtmlExporterParameter.CHARACTER_ENCODING, "UTF-8");
			exporterHTML.setParameter(JRHtmlExporterParameter.IMAGES_MAP, imagesMap);
			exporterHTML.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+ "/redsky/servlet/image?image=");//"." + imageFolderName.substring(imageFolderName.lastIndexOf("\\", imageFolderName.length())) + "\\"); 
			exporterHTML.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.TRUE);
			exporterHTML.setParameter(JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR, Boolean.FALSE);
			exporterHTML.setParameter(JRHtmlExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			exporterHTML.setParameter(JRHtmlExporterParameter.JASPER_PRINT, jasperPrint);
			exporterHTML.setParameter(JRCsvExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
			exporterHTML.exportReport();
			ouputStream.write(byteArrayOutputStream.toByteArray());
if(extractFileAudit){
				
	           response.setContentType("text/html");
				//ServletOutputStream outputStream = response.getOutputStream();
				SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
				StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
				String currentyear=yearDate.toString();
				
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
					dirPath.mkdirs();
				}
				SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
				StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
				String currentDateTime=tempDate.toString().trim();
				fileName=fileName+currentDateTime+".html";
				//response.setHeader("Content-Disposition", "attachment; filename=" + oldReportName);
				String filePath=uploadDir+""+fileName;
				File file1 = new File(filePath); 
				FileOutputStream outputStream1 = new FileOutputStream(file1);
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
				outputStream1.write(byteArrayOutputStream.toByteArray()); 
			   
				
				
				extractedFileLog = new ExtractedFileLog();
				extractedFileLog.setCorpID(sessionCorpID);
				extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
				extractedFileLog.setCreatedOn(new Date());
				extractedFileLog.setLocation(filePath);
				extractedFileLog.setFileName(fileName);
				extractedFileLog.setModule("Reports");
				extractedFileLog=extractedFileLogManager.save(extractedFileLog);
				}
		}
		catch (Exception e) {
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end from HTML"); 
	}
	if (fileType.length() != 0 && fileType.equalsIgnoreCase("EXTRACT")) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ## Report Name: "+temp+" Start In EXTRACT");
		String header = new String();
		if(reports.getCorpID().equalsIgnoreCase("TSFT") && (reports.getReportName().equalsIgnoreCase("ClientBillingSummary.jrxml") || reports.getReportName().equalsIgnoreCase("ClientBillingDetails.jrxml"))){
		redskyCorp=reportsManager.findByRedskyCustomerCorp(parameters.get("RedSky Customer").toString()).get(0).toString();
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("MMM-yyyy");
		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(parameters.get("End Date")));
		String dateFrom = nowYYYYMMDD1.toString();
		File fileName1 = new File(redskyCorp+"_" + fileName +"_"+dateFrom);
		response.setContentType("application/vnd.ms-excel; charset=UTF-8");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName1 + ".xls");
		} else {
			response.setContentType("application/vnd.ms-excel; charset=UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xls");	
		}
		 String query = reportsManager.generateReportExtract(id, parameters);
		 query = query.replaceAll(":", "-");
	      query = query.replaceAll("https-", "https:");
	     query = query.replaceAll("http-", "http:");
	     query = query.replaceAll("00-00", "00:00");
	     query = query.replaceAll("01-00", "01:00");
	     query = query.replaceAll("02-00", "02:00");
	     query = query.replaceAll("08-00", "08:00");
	     query = query.replaceAll("%H-%i", "%H:%i");
	     List extractList = sqlExtractManager.sqlDataExtract(query);
		try {
			if (!extractList.isEmpty()) {
				
				int starValue = query.indexOf("AS")+3;
				int endValue = query.indexOf("FROM");
				String subQuery = query.substring(starValue,endValue);
				subQuery = subQuery+",";
				subQuery = subQuery.replaceAll("\n", "");
				String [] tempString = null;
		       
				tempString = subQuery.split("AS ");
				int totalLength = tempString.length;
				String finalOP ="";
				for (int i = 0 ; i < totalLength ; i++) {
		     	   String tmp = tempString[i];
		     	   finalOP += tmp.substring(0,tmp.indexOf(","))+"\t";
				}
			   if(reports.getExtractTheme().equals("") || reports.getExtractTheme().equals(null)) {
				header = finalOP.toUpperCase().substring(0,finalOP.length())+"\n";
				ouputStream.write(header.getBytes());
				Iterator it = extractList.iterator();
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();

					for(int i=1; i<totalLength+1; i++){
						if (row[i] != null) {
							String data = row[i].toString();
							ouputStream.write((data + "\t").getBytes());
						} else {
							ouputStream.write("\t".getBytes());
						}
					}
					ouputStream.write("\n".getBytes());
			    }
				if(extractFileAudit){ 
					response.setContentType("application/vnd.ms-excel; charset=UTF-8");
					//ServletOutputStream outputStream = response.getOutputStream();
					SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
					StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
					String currentyear=yearDate.toString();
					
					String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
					uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
					File dirPath = new File(uploadDir);
							if (!dirPath.exists()) {
						dirPath.mkdirs();
					}
					SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
					StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
					String currentDateTime=tempDate.toString().trim();
					fileName=redskyCorp+"_" + fileName +"_"+currentDateTime+".xls";
					
					String filePath=uploadDir+""+fileName;
					File file1 = new File(filePath); 
					FileOutputStream outputStream1 = new FileOutputStream(file1);
					response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + "");
					 if(reports.getExtractTheme().equals("") || reports.getExtractTheme().equals(null)) {
							header = finalOP.toUpperCase().substring(0,finalOP.length())+"\n";
							outputStream1.write(header.getBytes());
							Iterator its = extractList.iterator();
							while (its.hasNext()) {
								Object[] row = (Object[]) its.next();

								for(int i=1; i<totalLength+1; i++){
									if (row[i] != null) {
										String data = row[i].toString();
										outputStream1.write((data + "\t").getBytes());
									} else {
										outputStream1.write("\t".getBytes());
									}
								}
								outputStream1.write("\n".getBytes());
						    }
					extractedFileLog = new ExtractedFileLog();
					extractedFileLog.setCorpID(sessionCorpID);
					extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
					extractedFileLog.setCreatedOn(new Date());
					extractedFileLog.setLocation(filePath);
					extractedFileLog.setFileName(fileName);
					extractedFileLog.setModule("Reports");
					extractedFileLog=extractedFileLogManager.save(extractedFileLog);
					}
				}
				} else {
					
					Iterator it = extractList.iterator();
					String[] columnName = finalOP.replaceAll("'", "").split("\t");
					ExcelCreator.getExcelCreator().createExcelAttachment(getResponse(),fileName, columnName, extractList, fileName, "",false);				
				}
			}else{
				header = "NO Record Found , thanks";
				ouputStream.write(header.getBytes());
			}
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end from EXTRACT"); 
	}
	
	ouputStream.flush();
	ouputStream.close();
	return null;
}

	@SkipValidation
	private Map buildParameters() {
		Map parameters = new HashMap();
		initializeRequiredParameters();
		for (JRParameter parameter : reportParameters) {
			String parameterValue = getPrefixedParameterFromRequest(parameter.getName(), REPORT_INPUT_PARAMETER_PREFIX);
			parameters.put(parameter.getName(), getFormattedParameterValueFromRequest(parameterValue, parameter.getValueClassName()));
		}
		if(parameters.containsKey("Commodities") && !selectedCommodities.contains("'%%'")){
			parameters.put("Commodities", selectedCommodities.replace("' '", "'"));
			}
		if(parameters.containsKey("Commodities") && selectedCommodities.contains("'%%'")){
			Commodities=reportsManager.findByCommodities(sessionCorpID);
		    String CommoditiesAll=Commodities.keySet().toString();
			CommoditiesAll = (CommoditiesAll.replace("[", "").replace("]", ""));
			StringBuffer commoditiesBuffer = new StringBuffer("");
			if (CommoditiesAll == null || CommoditiesAll.equals("") || CommoditiesAll.equals(",")) {
			} else {
				if (CommoditiesAll.indexOf(",") == 0) {
					CommoditiesAll = CommoditiesAll.substring(1);
				}
				if (CommoditiesAll.lastIndexOf(",") == CommoditiesAll.length() - 1) {
					CommoditiesAll = CommoditiesAll.substring(0, CommoditiesAll.length() - 1);
				}
				String[] arrayCommodities = CommoditiesAll.split(",");
				int arrayLength = arrayCommodities.length;
				for (int i = 0; i < arrayLength; i++) {
					commoditiesBuffer.append("'");
					commoditiesBuffer.append(arrayCommodities[i].trim());
					commoditiesBuffer.append("'");
					commoditiesBuffer.append(",");
				}
			}
			String multipleCommodities = new String(commoditiesBuffer);
			if (!multipleCommodities.equals("")) {
				multipleCommodities = multipleCommodities.substring(0, multipleCommodities.length() - 1);
			} else if (multipleCommodities.equals("")) {
				multipleCommodities = new String("''");
			}
		    parameters.put("Commodities", multipleCommodities);
			}
		
		if(parameters.containsKey("Company Divisions") && !selectCompanyDivisions.contains("'%%'")){
			parameters.put("Company Divisions", selectCompanyDivisions.replace("' '", "'"));
			}
		if(parameters.containsKey("Company Divisions") && selectCompanyDivisions.contains("'%%'")){
			CompanyDivisions= reportsManager.findByCompanyDivisions(sessionCorpID);
		    String CompanyDivisionsAll=CompanyDivisions.keySet().toString();
			CompanyDivisionsAll = (CompanyDivisionsAll.replace("[", "").replace("]", ""));
			StringBuffer companyDivisionsBuffer = new StringBuffer("");
			if (CompanyDivisionsAll == null || CompanyDivisionsAll.equals("") || CompanyDivisionsAll.equals(",")) {
			} else {
				if (CompanyDivisionsAll.indexOf(",") == 0) {
					CompanyDivisionsAll = CompanyDivisionsAll.substring(1);
				}
				if (CompanyDivisionsAll.lastIndexOf(",") == CompanyDivisionsAll.length() - 1) {
					CompanyDivisionsAll = CompanyDivisionsAll.substring(0, CompanyDivisionsAll.length() - 1);
				}
				String[] arrayCompanyDivisions = CompanyDivisionsAll.split(",");
				int arrayLength = arrayCompanyDivisions.length;
				for (int i = 0; i < arrayLength; i++) {
					companyDivisionsBuffer.append("'");
					companyDivisionsBuffer.append(arrayCompanyDivisions[i].trim());
					companyDivisionsBuffer.append("'");
					companyDivisionsBuffer.append(",");
				}
			}
			String multipleCompanyDivisions = new String(companyDivisionsBuffer);
			if (!multipleCompanyDivisions.equals("")) {
				multipleCompanyDivisions = multipleCompanyDivisions.substring(0, multipleCompanyDivisions.length() - 1);
			} else if (multipleCompanyDivisions.equals("")) {
				multipleCompanyDivisions = new String("''");
			}
		    parameters.put("Company Divisions", multipleCompanyDivisions);
			}
		
		
		if(parameters.containsKey("Compare")){
			parameters.put("Compare", selectedJobs.toString().replace("[", "").replace("]", ""));
			}
		if(parameters.containsKey("Corporate ID")){
			parameters.put("Corporate ID", sessionCorpID);
		}
		return parameters;
	}

	@SkipValidation
	private String getPrefixedParameterFromRequest(String parameterName, String prefix) {
		return this.getRequest().getParameter(prefix + parameterName);

	}


	@SkipValidation
	private Object getFormattedParameterValueFromRequest(String parameterValue, String parameterClassType) {
		Object formattedObject = parameterValue;
		if (parameterClassType.equals("java.util.Date")) {
			try {
				formattedObject = DateUtils.parseDate(parameterValue, new String[] { "MM/dd/yyyy" });
			} catch (ParseException e) {
				e.printStackTrace();
			}

		} else if (parameterClassType.equals("java.util.Long")) {
			formattedObject = Long.parseLong(parameterValue);
		}
		return formattedObject;
	}

	@SkipValidation
	public String viewReport() throws Exception {
		byte[] output;
		JasperPrint jasperPrint = reportsManager.generateReport(id);
		HttpServletResponse response = getResponse();
		HttpServletRequest request = getRequest();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ServletOutputStream ouputStream = response.getOutputStream();
		reports = reportsManager.get(id);
		String temp = reports.getReportName();
		String fileName = temp.substring(0, temp.indexOf("."));
		String fileType = request.getParameter("fileType");

		if (fileType.length() != 0 && fileType.equalsIgnoreCase("PDF")) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ## Report Name: "+temp+" Start In PDF");
			try {
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
				output = JasperExportManager.exportReportToPdf(jasperPrint);
				ouputStream.write(output);
			} catch (Exception e) {
				e.printStackTrace();
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end from PDF"); 
		}

		if (fileType.length() != 0 && fileType.equalsIgnoreCase("XLS")) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ## Report Name: "+temp+" Start In XLS");
			try {
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xls");
				JRXlsExporter exporterXLS = new JRXlsExporter();
				exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
				exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
				exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
				exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
				exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
				exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
				exporterXLS.exportReport();
				ouputStream.write(byteArrayOutputStream.toByteArray());
			} catch (Exception e) {
				e.printStackTrace();
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end from XLS"); 
		}

		if (fileType.length() != 0 && fileType.equalsIgnoreCase("CSV")) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ## Report Name: "+temp+" Start In CSV");
			try {
				response.setContentType("text/csv");
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".csv");
				JRExporter exporterCSV = new JRCsvExporter();
				exporterCSV.setParameter(JRCsvExporterParameter.JASPER_PRINT, jasperPrint);
				exporterCSV.setParameter(JRCsvExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
				exporterCSV.exportReport();
				ouputStream.write(byteArrayOutputStream.toByteArray());
			} catch (Exception e) {
				e.printStackTrace();
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end from CSV"); 
		}

		if (fileType.length() != 0 && fileType.equalsIgnoreCase("HTML")) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ## Report Name: "+temp+" Start In HTML");
			try {
				response.setContentType("text/html");
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".html");
				JRExporter exporterHTML = new JRHtmlExporter();
				exporterHTML.setParameter(JRHtmlExporterParameter.JASPER_PRINT, jasperPrint);
				exporterHTML.setParameter(JRHtmlExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
				exporterHTML.exportReport();
				ouputStream.write(byteArrayOutputStream.toByteArray());
			} catch (Exception e) {
				e.printStackTrace();
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end from HTML"); 
		}
		
		if (fileType.length() != 0 && fileType.equalsIgnoreCase("RTF")) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ## Report Name: "+temp+" Start In RTF");
			try {
				response.setContentType("application/rtf");
				response.setHeader("Content-Disposition", "attachment; filename="+ fileName +".rtf");
				JRRtfExporter exporter = new JRRtfExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
				exporter.exportReport();
				ouputStream.write(byteArrayOutputStream.toByteArray());
			} catch (Exception e) {
				e.printStackTrace();
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" end from RTF"); 
		}

		ouputStream.flush();
		ouputStream.close();
		return null;
	}
	@SkipValidation
	public String reportByEnabled() {
		user = userManager.getUserByUsername(sessionUserName);
		roles = user.getRoles();
		String Module="";
		String Menu="";
		String Description="";
		String SubModule="";
		if(reports!=null)
		{
		
		if ((reports.getModule() == null)) {
			Module = "";
		} else {
			Module = reports.getModule().trim();
		}
		if ((reports.getMenu() == null)) {
			Menu = "";
		} else {
			Menu = reports.getMenu().trim();
		}
		if ((reports.getDescription() == null)) {
			Description = "";
		} else {
			Description = reports.getDescription().trim();
		}
		if ((reports.getSubModule() == null)) {
			SubModule = "";
		} else {
			SubModule = reports.getSubModule().trim();
		}

			getRequest().getSession().setAttribute("Module", reports.getModule());
			getRequest().getSession().setAttribute("SubModule", reports.getSubModule());
			getRequest().getSession().setAttribute("Menu", reports.getMenu());
			getRequest().getSession().setAttribute("Description", reports.getDescription());
		if(marked.equalsIgnoreCase("yes")){
			reportss = reportBookmarkManager.getBookMarkedReportList(sessionUserName, Module, Menu,  SubModule, Description,sessionCorpID,roles);
		}else{
			reportss = reportsManager.findByEnabled(Module, Menu,  SubModule, Description, sessionCorpID,sessionUserName,roles,user.getSalesPortalAccess());
	        }
		}else{
			try{
			String moduleSession = (String) getRequest().getSession().getAttribute("Module");
			String subModuleSession = (String) getRequest().getSession().getAttribute("SubModule");
			String menuSession = (String) getRequest().getSession().getAttribute("Menu");
			String descriptionSession = (String) getRequest().getSession().getAttribute("Description");
			if(marked.equalsIgnoreCase("yes")){
				reportss = reportBookmarkManager.getBookMarkedReportList(sessionUserName, moduleSession, menuSession,  subModuleSession, descriptionSession,sessionCorpID,roles);
			}else{
				reportss = reportsManager.findByEnabled(moduleSession, menuSession,  subModuleSession, descriptionSession, sessionCorpID,sessionUserName,roles,user.getSalesPortalAccess());
		        }
			
			}catch(Exception ex)
			{
				reportss = reportsManager.findByEnabled("", "",  "", "", sessionCorpID,sessionUserName,roles,user.getSalesPortalAccess());
			}
		}
		return SUCCESS;
	}
	private Set<Role> roles;
	@SkipValidation
	public String search() {
		user = userManager.getUserByUsername(sessionUserName);
		roles = user.getRoles();
		String Module="";
		String Menu="";
		String Description="";
		String SubModule="";
	/*	if(user.getSalesPortalAccess()!=null && user.getSalesPortalAccess().equals(true)){
		}else{*/
		if(reports!=null)
		{
		if ((reports.getModule() == null)) {
			Module = "";
		} else {
			Module = reports.getModule().trim();
		}
		if ((reports.getMenu() == null)) {
			Menu = "";
		} else {
			Menu = reports.getMenu().trim();
		}
		if ((reports.getDescription() == null)) {
			Description = "";
		} else {
			Description = reports.getDescription().trim();
		}
		if ((reports.getSubModule() == null)) {
			SubModule = "";
		} else {
			SubModule = reports.getSubModule().trim();
		}
		getRequest().getSession().setAttribute("Module", reports.getModule());
		getRequest().getSession().setAttribute("SubModule", reports.getSubModule());
		getRequest().getSession().setAttribute("Menu", reports.getMenu());
		getRequest().getSession().setAttribute("Description", reports.getDescription());
		if(marked.equalsIgnoreCase("yes")){
			reportss = reportBookmarkManager.getBookMarkedReportList(sessionUserName, Module, Menu,  SubModule, Description, sessionCorpID,roles);
		}else{
			reportss = reportsManager.findByEnabled(reports.getModule(), reports.getMenu(),  reports.getSubModule(), reports.getDescription(), sessionCorpID,sessionUserName,roles,user.getSalesPortalAccess());
	    }
		}
		
		else{
			try
			{
			String moduleSession = (String) getRequest().getSession().getAttribute("Module");
			String subModuleSession = (String) getRequest().getSession().getAttribute("SubModule");
			String menuSession = (String) getRequest().getSession().getAttribute("Menu");
			String descriptionSession = (String) getRequest().getSession().getAttribute("Description");
			if(marked.equalsIgnoreCase("yes")){
				reportss = reportBookmarkManager.getBookMarkedReportList(sessionUserName, moduleSession, menuSession,  subModuleSession, descriptionSession,sessionCorpID,roles);
			}else{
				reportss = reportsManager.findByEnabled(moduleSession, menuSession,  subModuleSession, descriptionSession, sessionCorpID,sessionUserName,roles,user.getSalesPortalAccess());
		        }
		}
		catch(Exception ex)
		{
			reportss = reportsManager.findByEnabled("", "",  "", "", sessionCorpID,sessionUserName,roles,user.getSalesPortalAccess());
		}
	}
		
		reportByEnabled();
		jobSearchPersistValue="Sunil";
		//}
		return SUCCESS;
	}
	public String bookMarkedSearch(){
		user = userManager.getUserByUsername(sessionUserName);
		roles = user.getRoles();
		reportss = reportBookmarkManager.getBookMarkedReportList(sessionUserName,reports.getModule(), reports.getMenu(),  reports.getSubModule(), reports.getDescription(),sessionCorpID,roles);
		
		if(reportss.isEmpty())
		{
			return INPUT;
		}
		else
		{
		marked = "yes";
		return SUCCESS;
		}
	}
	private String gotoPageString;
	private String validateFormNav;
	private String roleName;

	private List contract;

	private List usrName;

	private  Map<String, String> owner;

	private Map<String, String> order;

	private Map<String, String> storageBillingGroup;

	private List checkBillCodes;
	
	private String checkModes;
	
	private String crTerms;

	public String getCrTerms() {
		return crTerms;
	}

	public void setCrTerms(String crTerms) {
		this.crTerms = crTerms;
	}

	/**
	 * @return the checkModes
	 */
	@SkipValidation
	public String driverReportParam()throws Exception{
		List reportId=reportsManager.getReportId(sessionCorpID);
		if(reportId!=null && !reportId.isEmpty() && reportId.get(0)!=null && !reportId.get(0).toString().equals("")){
			id=Long.parseLong(reportId.get(0).toString());
		}
	   viewReportParam();
		return SUCCESS;
	}
	public String getCheckModes() {
	    return checkModes;
	}

	/**
	 * @param checkModes the checkModes to set
	 */
	public void setCheckModes(String checkModes) {
	    this.checkModes = checkModes;
	}

	/**
	 * @return the checkModes
	 */
	
	private String billCode;
	private String modes;
	/**
	 * @return the modes
	 */
	public String getModes() {
	    return modes;
	}

	/**
	 * @param modes the modes to set
	 */
	public void setModes(String modes) {
	    this.modes = modes;
	}

	private String billToCode;
	private String routingfilter;

	private Map<String, String> customsMovement;

	private Map<String, String> customStatus;

	private Map<String, String> preDefTxt;
	/**
	 * @return the preDefTxt
	 */
	public Map<String, String> getPreDefTxt() {
	    return preDefTxt;
	}

	/**
	 * @param preDefTxt the preDefTxt to set
	 */
	public void setPreDefTxt(Map<String, String> preDefTxt) {
	    this.preDefTxt = preDefTxt;
	}

	/**
	 * @return the customsMovement
	 */
	public Map<String, String> getCustomsMovement() {
	    return customsMovement;
	}

	/**
	 * @param customsMovement the customsMovement to set
	 */
	public void setCustomsMovement(Map<String, String> customsMovement) {
	    this.customsMovement = customsMovement;
	}

	/**
	 * @return the customStatus
	 */
	public Map<String, String> getCustomStatus() {
	    return customStatus;
	}

	/**
	 * @param customStatus the customStatus to set
	 */
	public void setCustomStatus(Map<String, String> customStatus) {
	    this.customStatus = customStatus;
	}

	public String saveOnTabChange() throws Exception {
		validateFormNav = "OK";
    	String s = save();
    	return gotoPageString;
    }

	
	@SkipValidation
	public String searchForms() {
		if(marked.equalsIgnoreCase("yes")){	
			reportss = reportBookmarkManager.getBookMarkedFormList(sessionUserName,reports.getModule(), reports.getMenu(),  reports.getSubModule(), reports.getDescription(), sessionCorpID);
		}else{
			reportss = reportsManager.findByFormFlag(sessionUserName,reports.getModule(), reports.getMenu(),  reports.getSubModule(), reports.getDescription(), sessionCorpID);
			
		}
		
		
		return SUCCESS;
	}
	
	@SkipValidation
	public String searchModuleReport() {
		reportss = reportsManager.findByModuleCriteria(reportSubModule, reportModule, reports.getMenu(), reports.getDescription(), sessionCorpID, "Yes",companyDivision, jobType, billToCode, modes,routingfilter);
		return SUCCESS;
	}
	
	@SkipValidation
	public String searchRelatedModuleReport() {
		reportss = reportsManager.findByRelatedModuleCriteria(reportSubModule, reportModule, reports.getMenu(), reports.getDescription(), sessionCorpID, "Yes");
		return SUCCESS;
	}

	@SkipValidation
	public String searchAdmin() {	
		reportss = reportsManager.findByCriteria(reports.getModule(), reports.getSubModule(), reports.getMenu(), reports.getReportName(), reports.getDescription(), sessionCorpID, reports.getFormReportFlag());
		return SUCCESS;
	}

	@SkipValidation
	public String customerlistNew() {
		customerFile=customerFileManager.get(id);
		reportss = reportsManager.findByModule("customerFile", sessionCorpID);
		return SUCCESS;
	}

	@SkipValidation
	public String serviceOrderlistNew() {
		serviceOrder=serviceOrderManager.get(id);
		reportss = reportsManager.findByModule("serviceOrder", sessionCorpID);
		return SUCCESS;
	}

	@SkipValidation
	public String workTicketlistNew() {
		serviceOrder=serviceOrderManager.get(id);
		reportss = reportsManager.findByModule("workTicket", sessionCorpID);
		return SUCCESS;
	}

	@SkipValidation
	public String claimlistNew() {
		serviceOrder=serviceOrderManager.get(id);
		reportss = reportsManager.findByModule("claim", sessionCorpID);
		return SUCCESS;
	}

	@SkipValidation
	public String reportBySubModule() {
		reportss = reportsManager.findBySubModules(reportModule, reportSubModule,sessionCorpID, "Yes");
		return SUCCESS;
	}
	
	@SkipValidation
	public String reportBySubModule1() {
		reportss = reportsManager.findBySubModules1(reportModule, reportSubModule,companyDivision, jobType, billToCode, modes, sessionCorpID, "Yes",id,jobNumber);
		return SUCCESS;
	}
	@SkipValidation
	public String reportBySubModuleAccountLineList() {
		reportss = reportsManager.findBySubModule(reportModule, reportSubModule, sessionCorpID, "Yes", companyDivision, jobType, billToCode, modes);
		return SUCCESS;
	}
	private String accountLinecompanyDivision;
	@SkipValidation
	public String reportBySubModuleAccountLineList1() {
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		if(company.getSingleCompanyDivisionInvoicing()){
		companyDivision=accountLinecompanyDivision;	
		}
		reportss = reportsManager.findBySubModule1(reportModule, reportSubModule, sessionCorpID, "Yes", companyDivision, jobType, billToCode, modes,recInvNumb,jobNumber);
		return SUCCESS;
	}
	@SkipValidation
	public String findQuotationFormsAtQuotesAjax() {		
		reportss = reportsManager.findQuotationFormList(sessionCorpID, companyDivision,jobType, billToCode, modes,jobNumber);
		return SUCCESS;
	}
	
	@SkipValidation
	public String relatedModuleForm() {
		reportss = reportsManager.findByModule(reportModule, sessionCorpID);
		return SUCCESS;
	}
	
	@SkipValidation
	public String reportList() {
		reportss = reportsManager.findByCorpID(sessionCorpID);
		return SUCCESS;
	}

	@SkipValidation
	public String reportUserList() {
		searchFlag="0";
		user = userManager.getUserByUsername(sessionUserName);
		roles = user.getRoles();
	/*	if(user.getSalesPortalAccess()!=null && user.getSalesPortalAccess().equals(true)){
		}else{*/
		reportss = reportsManager.findByEnabled("", "",  "", "", sessionCorpID,sessionUserName,roles,user.getSalesPortalAccess());
		/*}*/
		return SUCCESS;
	}

	@SkipValidation
	public String reportUserDocsList() {
		reportss = reportsManager.findByDocsxfer();
		return SUCCESS;
	}
	@SkipValidation
	public String reportBySubModuleInvoiceOurList() {
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		if(company.getSingleCompanyDivisionInvoicing()){
		companyDivision=accountLinecompanyDivision;	
		}
		reportss = reportsManager.findBySubModule1(reportModule, reportSubModule, sessionCorpID, "Yes", companyDivision, jobType, billToCode, modes, recInvNumb, jobNumber);
		return SUCCESS;
	}
	@SkipValidation
	public String formList() {
		reportss = reportsManager.findByFormFlag(sessionUserName,"" ,"", "", "", sessionCorpID);
		return SUCCESS;
	}
	 public List findUserRoles(){
	    	return reportsManager.findUserRoles(sessionCorpID);
	    }

	@SkipValidation
	public String editReport() {
		reportfieldSectionList =new ArrayList(); 
		if (id != null) {
			reports = reportsManager.get(id);
			
			reportfieldSectionList=reportsFieldSectionManager.reportfieldSectionList(id,sessionCorpID,"update");
			 if(reportfieldSectionList!=null && !reportfieldSectionList.isEmpty()){
				 reportfieldSectionListSize=reportfieldSectionList.size();  
			   	   }
			 //////////////////////////////////////////////////////////////////////////////////////////////
			 reportfieldSectionControlList = new ArrayList();
			 reportfieldSectionControlList=reportsFieldSectionManager.reportfieldSectionList(id,sessionCorpID, "controll");
			 if(reportfieldSectionControlList!=null && !reportfieldSectionControlList.isEmpty()){
				 reportfieldSectionListControlSize=reportfieldSectionControlList.size();  
			   	   }
			 multiplRoleType = new ArrayList();
			 String roleType = reportsManager.getAgentRoleList(id);
			 roleType = roleType.replace("[", "");
			 roleType = roleType.replace("]", "");
			 String[] roleTypes = roleType.split(",");
			 for(String rType:roleTypes){
				 multiplRoleType.add(rType.trim());
			 }
			 
			System.out.println("multiplRoleType is : "+multiplRoleType);
			 /////////////////////////////////////////////////////////////////////////////////////////////
		} else {
			reports = new Reports();
			reports.setUpdatedOn(new Date());
			reports.setCreatedOn(new Date());
		}
		reports.setCorpID(sessionCorpID);
	    availRoles = reportsManager.findUserRoles(sessionCorpID);
	    getSession().setAttribute("availRoles", availRoles);
		
		return SUCCESS;
	}

	@SkipValidation
	public String checkBillCodes(){
	    	 String billToCodes ="";
	   	 if(billCode==null || billCode.equals("")){
	   	 billToCodes="";
	   	 } else {
	   	 String[] result = billCode.trim().split("\\,");
	   	 List billToCodes1 = new ArrayList();
		 for (int x=0; x<result.length; x++){
		 billToCodes=result[x];
		 billToCodes1=reportsManager.checkBillCodes(billToCodes.trim(),sessionCorpID);
		 if(!billToCodes1.isEmpty()){
		 checkBillCodes = new ArrayList(Arrays.asList(billToCodes1.get(0).toString()));
		 }}}
	return SUCCESS; 
	}
	
	@SkipValidation
	public String checkModes(){
	    	 String soModes ="";
	   	 if(modes==null || modes.equals("")){
	   	 soModes="";
	   	 } else {
	   	 String[] result = modes.trim().split("\\,");
	   	 List soModes1 = new ArrayList();
		 for (int x=0; x<result.length; x++){
		 soModes=result[x];
		 soModes1=reportsManager.checkModes(soModes.trim(),sessionCorpID);
		 if(soModes1.isEmpty()){
	     checkModes = soModes;
		 }
		 }}
	return SUCCESS; 
	}
	
	
	public String save() throws Exception {
		boolean isNew = (reports.getId() == null);
		List RoleList= roleManager.getRoles(role);
	     if (getRequest().isUserInRole(Constants.ADMIN_ROLE)) {
		 String[] reportsRoles = getRequest().getParameterValues("reportsRoles");
	            for (int i = 0; reportsRoles != null && i < reportsRoles.length; i++) {
	                String roleName = reportsRoles[i];
	                reports.addRole(roleManager.getRole(roleName));
	            }
	     }
		reports.setCorpID(sessionCorpID);
		reports.setUpdatedOn(new Date());
		reports.setUpdatedBy(getRequest().getRemoteUser());
		if (isNew) {
			reports.setCreatedOn(new Date());
		}
		if(reports.getSecureForm() == null){
			reports.setSecureForm("No");
		}
		
		reportsManager.saveReport(reports);
		
		String checkLimit = reportsFieldSectionManager.updateReportsFieldSection(fieldnameUpdated,fieldvalueUpdated,sessionCorpID,"update");
		String checkLimit1 = reportsFieldSectionManager.updateReportsFieldSection(controlFieldNameUpdated,controlFieldValueUpdated,sessionCorpID,"controll");
		String[] str= new String[100];
		String[] strRes= new String[100];
		reportsFieldSection=new ReportsFieldSection();
		
		boolean isNew1 = (reportsFieldSection.getId() == null);
		if(isNew1){
			reportsFieldSection.setCreatedOn(new Date());
	        }
		String tempfieldname="";
		String tempfieldvalue="";
		if(reports.getId()!=null){
			if(reportfieldsectionlist!=null && (!(reportfieldsectionlist.trim().equals("")))){
				str = reportfieldsectionlist.split("~");
				for (String string : str) { 
					strRes=string.split(":");
					
					 tempfieldname = strRes[0].toString().trim();
					
					
					 tempfieldvalue = strRes[1].toString().trim();
					
					 tempfieldname = (strRes[0]==null)?"" : strRes[0].toString().trim();
					 tempfieldvalue = (strRes[1]==null)?"" : strRes[1].toString().trim();
					
					reportsFieldSection=new ReportsFieldSection();
					reportsFieldSection.setReportId(reports.getId());
					reportsFieldSection.setCreatedOn(new Date());
					reportsFieldSection.setCreatedBy(getRequest().getRemoteUser());
					reportsFieldSection.setCorpId(sessionCorpID);
					reportsFieldSection.setUpdatedBy(getRequest().getRemoteUser());
					reportsFieldSection.setFieldname(tempfieldname);
					reportsFieldSection.setFieldvalue(tempfieldvalue);
					reportsFieldSection.setUpdatedOn(new Date());
					reportsFieldSection.setType("update");
					reportsFieldSectionManager.save(reportsFieldSection);
					
				}
			}
			///////////////////////////////////////////////////////////////////////////////////////
			tempfieldname="";
			tempfieldvalue="";
			if(reportfieldSectionControllist!=null && (!(reportfieldSectionControllist.trim().equals("")))){
				str = reportfieldSectionControllist.split("~");
				for (String string : str) { 
					strRes=string.split(":");
					
					 tempfieldname = strRes[0].toString().trim();
					
					
					 tempfieldvalue = strRes[1].toString().trim();
					
					 tempfieldname = (strRes[0]==null)?"" : strRes[0].toString().trim();
					 tempfieldvalue = (strRes[1]==null)?"" : strRes[1].toString().trim();
					
					reportsFieldSection=new ReportsFieldSection();
					reportsFieldSection.setReportId(reports.getId());
					reportsFieldSection.setCreatedOn(new Date());
					reportsFieldSection.setCreatedBy(getRequest().getRemoteUser());
					reportsFieldSection.setCorpId(sessionCorpID);
					reportsFieldSection.setUpdatedBy(getRequest().getRemoteUser());
					reportsFieldSection.setFieldname(tempfieldname);
					reportsFieldSection.setFieldvalue(tempfieldvalue);
					reportsFieldSection.setUpdatedOn(new Date());
					reportsFieldSection.setType("controll");
					reportsFieldSectionManager.save(reportsFieldSection);
					
				}
			}
			////////////////////////////////////////////////////////////////////////////////////////
		}
		reportfieldSectionList=new ArrayList();
		reportfieldSectionList=reportsFieldSectionManager.reportfieldSectionList(reports.getId(),sessionCorpID, "update");
		 if(reportfieldSectionList!=null && !reportfieldSectionList.isEmpty()){
			 reportfieldSectionListSize=reportfieldSectionList.size();  
		   	   }
		 ///////////////////////////////////////////////////////////////////////////////////
		 reportfieldSectionControlList = new ArrayList();
		 reportfieldSectionControlList=reportsFieldSectionManager.reportfieldSectionList(reports.getId(),sessionCorpID, "control");
		 if(reportfieldSectionControlList!=null && !reportfieldSectionControlList.isEmpty()){
			 reportfieldSectionListControlSize=reportfieldSectionControlList.size();  
		   	   }
		 ///////////////////////////////////////////////////////////////////////////////////
		
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
			String key = (isNew) ? "reports.added" : "reports.updated";
			saveMessage(getText(key));
			}		
		return SUCCESS;
	     }

	
	@SkipValidation
	public String start() {
		reports = new Reports();
		docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
		return INPUT;
    }

	public  Map<String, String> getDocsList() {
		return docsList;
	}

	public  void setDocsList(Map<String, String> docsList) {
		this.docsList = docsList;
	}

	@SkipValidation
	public String delete() {
		reportsManager.remove(id);
		saveMessage(getText("reports.deleted"));
		return SUCCESS;
	}
	
	public String getComboList(String corpId) {
		availRoles = reportsManager.findUserRoles(sessionCorpID);
		List roles= roleManager.getRoles(role);
		reportFlag = refMasterManager.findByParameter(sessionCorpID, "REPORT_FLAG");
		docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");		
		moduleReport=refMasterManager.findByParameter(sessionCorpID, "MODULE_REPORT");
		subModuleReport=refMasterManager.findByParameter(sessionCorpID, "SUBMODULE_REPORT");
		actionType = new LinkedHashMap<String, String>();
		actionType.put("localNumber",  "TruckNumber");
		actionType.put("truckType", "truckType");
		actionType.put("warehouse",  "Warehouse");
		order = refMasterManager.findByParameter(sessionCorpID, "REPORT_ORDER");
	    mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
	    routing = refMasterManager.findByParameter(sessionCorpID, "ROUTING");
	    roleMap = refMasterManager.findByParameter(sessionCorpID, "ROLE_REPORTS");
	    reportCategory= refMasterManager.findByParameter(sessionCorpID, "REPORT_CATEGORY");
		printOptions = refMasterManager.findByParameter(sessionCorpID, "INVOICEFORMINPUTPARM");		

		        // soList=reportsManager.findByModuleCoordinator(sessionCorpID);
				// esList=reportsManager.findByModuleEstimator(sessionCorpID);
				// payableStatusList=reportsManager.findByPayableStatus();
				// payingStatusList=reportsManager.findByPayingStatus();
				// house=reportsManager.findByModuleReports(sessionCorpID);
				// OMNI=reportsManager.findByOmni();
				// ROUTING=reportsManager.findByRouting(sessionCorpID);
				// CURRENCY=  refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
				// COMPANYCODE=reportsManager.findByCompanyCode(sessionCorpID);
				// COMMODITS=reportsManager.findByCommodity(sessionCorpID);
				// JOB=reportsManager.findByJob(sessionCorpID);
				// BaseScore=reportsManager.findByBaseScore(sessionCorpID);
		        // sale = refMasterManager.findUser(sessionCorpID, "ROLE_SALE");
				// owner = reportsManager.findOwner(sessionCorpID, "ROLE_SALE");
				// contract=reportsManager.findContract(sessionCorpID);
				// usrName=refMasterManager.getSuperVisor(sessionCorpID);
				// storageBillingGroup = refMasterManager.findByParameter(sessionCorpID, "BILLGRP");
				// customsMovement= refMasterManager.findByParameter(corpId, "CustomsMovement");
			    // customStatus= refMasterManager.findByParameter(corpId, "CUSTOM_STATUS");
	            // preDefTxt=refMasterManager.findCodeOnleByParameter(sessionCorpID, "PREDEFINEDTEXT");		
		return SUCCESS;
	}

	@SkipValidation
	public String dataExtracts() {
		if (id != null) {
			reports = reportsManager.get(id);
			reports.setUpdatedOn(new Date());
		} else {
			reports = new Reports();
			reports.setUpdatedOn(new Date());
			reports.setCreatedOn(new Date());
		}
		reports.setCorpID(sessionCorpID);
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String upload() throws Exception{
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
		File dirPath = new File(uploadDir);
		byte[] output;
		Map parameters = buildParameters();
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
		Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
		Locale locale = new Locale("en", "US");
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		HttpServletResponse response = getResponse();
		JasperPrint jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
		ServletOutputStream ouputStream = response.getOutputStream();
		reports = reportsManager.get(id);
		String temp = reports.getReportName();
		String fileName = temp.substring(0, temp.indexOf("."));
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
		output = JasperExportManager.exportReportToPdf(jasperPrint);
		ouputStream.write(output);
		if (!dirPath.exists()) {
				dirPath.mkdirs();
		}
	
		reports = reportsManager.get(id);
		String tempFileFileName = temp.substring(0, temp.indexOf("."));
		fileFileName = tempFileFileName+".pdf";
		
		if(myFileManager.getMaxId(sessionCorpID).get(0)==null){
			id=1L;
			fileFileName = (id+ "_" +fileFileName);
		}
		else{
			Long id=Long.parseLong(myFileManager.getMaxId(sessionCorpID).get(0).toString());
		    	id=id+1;
		    	fileFileName = (id+"_" +fileFileName );
		}
		File newFile = new File(uploadDir + fileFileName);
		String filePath="";
		filePath =(uploadDir+fileFileName );
		if (newFile.exists()) {
			newFile.delete();
		}
		FileOutputStream fos = new FileOutputStream(newFile);
		DataOutputStream bos =  new DataOutputStream(fos);
		bos.write(output);
		bos.close();
		if(jobNumber.trim().length() > 0){
			List seqNo = myFileManager.getSequanceNum(jobNumber, sessionCorpID);
			myFile.setCustomerNumber(seqNo.get(0).toString());
			myFile.setFileId(jobNumber);
		}else {
			JasperReport reportTemplate = reportsManager.getReportTemplate(id);
			JRParameter[] allParameters = reportTemplate.getParameters();
			reportParameters = new ArrayList<JRParameter>();
			for (JRParameter parameter : allParameters) {
					if (!parameter.isSystemDefined()) {
						reportParameters.add(parameter);
					}
			}
			for (JRParameter parameter : reportParameters) {
				if (!parameter.getValueClassName().equals("java.util.Date")) {
					String paramName = parameter.getName();
					if(paramName.equalsIgnoreCase("Customer File Number")){
							Object paramValue = parameters.put(parameter.getName(), custID);
							List seqNo = myFileManager.getSequanceNum(paramValue.toString(), sessionCorpID);
							if(!seqNo.isEmpty()){
							myFile.setCustomerNumber(seqNo.get(0).toString());
							myFile.setFileId(paramValue.toString());
							} else if (seqNo.isEmpty()) { 
								String key = "There are no Customer File Number which You Entered";   
					            saveMessage(getText(key));
							}
						}
					else if(paramName.equalsIgnoreCase("Service Order Number")){
						Object paramValue = parameters.put(parameter.getName(), jobNumber);
						List seqNo = myFileManager.getSequanceNum(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber(seqNo.get(0).toString());
						myFile.setFileId(paramValue.toString());
					} else if (seqNo.isEmpty()) { 
							String key = "There are no Service Order Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
					else if(paramName.equalsIgnoreCase("Invoice Number")){
						Object paramValue = parameters.put(parameter.getName(), invoiceNumber);
						List seqNo = myFileManager.getSequanceNumByInvNum(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
						myFile.setFileId(seqNo.get(0).toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Invoice Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
					else if(paramName.equalsIgnoreCase("Claim Number")){
						Object paramValue = parameters.put(parameter.getName(), claimNumber);
						List seqNo = myFileManager.getSequanceNumByClm(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
						myFile.setFileId(seqNo.get(0).toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Claim Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
					else if(paramName.equalsIgnoreCase("Work Ticket Number")){
						Object paramValue = parameters.put(parameter.getName(), noteID);
						List seqNo = myFileManager.getSequanceNumByTkt(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
						myFile.setFileId(seqNo.get(0).toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Work Ticket Number which You Entered";   
				            saveMessage(getText(key));
						}
					}else if(paramName.equalsIgnoreCase("PartnerCode")|| paramName.equalsIgnoreCase("Bill to Code")||paramName.equalsIgnoreCase("Vendor Code")){
						Object paramValue=parameters.put(parameter.getName(), BillTo);
						if(BillTo!=null && !BillTo.equals(" ")){
							myFile.setCustomerNumber(BillTo);
						    myFile.setFileId(BillTo);
							
						} else{ 
							String key = "There are no Partner Code which You Entered";   
				            saveMessage(getText(key));
						}
					}
					
				} else {
					myFile.setCustomerNumber("");
					myFile.setFileId("");
				}
			}
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
			Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			} else {
			Locale locale = new Locale("en", "US");
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
		}
	    myFile.setFileContentType("application/pdf");
	    myFile.setFileFileName(fileFileName);
	    myFile.setLocation(filePath);
	    myFile.setFileType(docFileType);
	    myFile.setCorpID(sessionCorpID);	 
	    myFile.setActive(true);
	    if(myFile.getDescription()!=null && !myFile.getDescription().equals("")){
	    	
	    }else{
	    	myFile.setDescription(docFileType);
	    }
	    if(documentCategory!=null){
	    myFile.setDocumentCategory(documentCategory);
	    }
	    myFile.setCreatedBy(getRequest().getRemoteUser());
	    myFile.setUpdatedBy(getRequest().getRemoteUser());
	    myFile.setCreatedOn(new Date());
	    myFile.setUpdatedOn(new Date());
	    
	    FileSizeUtil fileSizeUtil = new FileSizeUtil(); 
	    myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));
	    
	    myFile.setMapFolder(mapFolder);
	    
	    if(reports.getDocsxfer() != null && reports.getDocsxfer().equalsIgnoreCase("Yes")){
		    if(reports.getSecureForm() != null && reports.getSecureForm().equalsIgnoreCase("Yes")){
			    myFile.setIsSecure(true);
		    }else{
			    myFile.setIsSecure(false);
		    }  
	    }else{
		    myFile.setIsSecure(false);
	    } 
	   if(myFile.getFileType().equalsIgnoreCase("Credit Card Authorization")){
		   myFile.setIsSecure(true);  
	   }
	    if((docFileType!=null)&&(!docFileType.trim().toString().equalsIgnoreCase(""))){
	        List docControl = myFileManager.getDocAccessControl(docFileType, sessionCorpID);
	        if((docControl!=null)&&(!docControl.isEmpty())&&(docControl.get(0)!=null)&&(!docControl.get(0).toString().trim().equalsIgnoreCase(""))){
	        	String st=docControl.get(0).toString().trim();
	        	if((st.split("#")[10]!=null)&&(st.split("#")[10].toString().equalsIgnoreCase("true"))){
	        		myFile.setInvoiceAttachment(true);
	        	}
	        	
	        	if((st.split("#")[0]!=null)&&(st.split("#")[0].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsCportal(true);
	        	}else{
	        		myFile.setIsCportal(false);
	        	}
	        	if((st.split("#")[1]!=null)&&(st.split("#")[1].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsAccportal(true);
	        	}
	        	else{
	        		myFile.setIsAccportal(false);
	        	}
	        	if((st.split("#")[2]!=null)&&(st.split("#")[2].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsPartnerPortal(true);
	        	}
	        	else{
	        		myFile.setIsPartnerPortal(false);
	        	}
	        	if((st.split("#")[3]!=null)&&(st.split("#")[3].toString().equalsIgnoreCase("true"))){
	        		isBookingAgent = true;
	        	}
	        	else{
	        		myFile.setIsBookingAgent(false);
	        	}
	        	if((st.split("#")[4]!=null)&&(st.split("#")[4].toString().equalsIgnoreCase("true"))){
	        		isNetworkAgent =true;
	        	}
	        	else{
	        		myFile.setIsNetworkAgent(false);
	        	}
	        	if((st.split("#")[5]!=null)&&(st.split("#")[5].toString().equalsIgnoreCase("true"))){
	        		isOriginAgent = true;
	        	}
	        	else{
	        		myFile.setIsOriginAgent(false);
	        	}
	        	if((st.split("#")[6]!=null)&&(st.split("#")[6].toString().equalsIgnoreCase("true"))){
	        		isSubOriginAgent=true;
	        	}
	        	else{
	        		myFile.setIsSubOriginAgent(false);
	        	}
	        	if((st.split("#")[7]!=null)&&(st.split("#")[7].toString().equalsIgnoreCase("true"))){
	        		isDestAgent=true;
	        	}else{
	        		myFile.setIsDestAgent(false);
	        	}
	        	if((st.split("#")[8]!=null)&&(st.split("#")[8].toString().equalsIgnoreCase("true"))){
	        		isSubDestAgent=true;
	        	}else{
	        		myFile.setIsSubDestAgent(false);
	        	}
	        	if((st.split("#")[9]!=null)&&(st.split("#")[9].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsServiceProvider(true);
	        	}
	        	else{
	        		myFile.setIsServiceProvider(false);
	        	}
	        	if((st.split("#")[11]!=null)&&(st.split("#")[11].toString().equalsIgnoreCase("true"))){
	        		myFile.setAccountLinePayingStatus("New");
	        	}
	        }else{
	        	myFile.setIsBookingAgent(false);
	        	myFile.setIsNetworkAgent(false);
	        	myFile.setIsOriginAgent(false);
	        	myFile.setIsSubOriginAgent(false);
	        	myFile.setIsDestAgent(false);
	        	myFile.setIsSubDestAgent(false);
	        }
	    }    
	    if (reportModule.equalsIgnoreCase("serviceOrder")) {
	    	List shipNumberList = myFileManager.getShipNumberList(myFile.getFileId());
			if(shipNumberList!=null && !shipNumberList.isEmpty()){
			Iterator it = shipNumberList.iterator();
	         while (it.hasNext()) {
	        	String agentFlag=(String)it.next();
	        	if(agentFlag.equalsIgnoreCase("BA") && isBookingAgent!=null && isBookingAgent==true){
	        		myFile.setIsBookingAgent(true);
				}else if(agentFlag.equalsIgnoreCase("NA") && isNetworkAgent!=null && isNetworkAgent==true){
					myFile.setIsNetworkAgent(true);
				}else if(agentFlag.equalsIgnoreCase("OA") && isOriginAgent!=null && isOriginAgent==true){
					myFile.setIsOriginAgent(true);
				}else if(agentFlag.equalsIgnoreCase("SOA") && isSubOriginAgent!=null && isSubOriginAgent==true){
					myFile.setIsSubOriginAgent(true);
				}else if(agentFlag.equalsIgnoreCase("DA") && isDestAgent!=null && isDestAgent==true){
					myFile.setIsDestAgent(true);
				}else if(agentFlag.equalsIgnoreCase("SDA") && isSubDestAgent!=null && isSubDestAgent==true){
					myFile.setIsSubDestAgent(true);
				} 
	         }
		}
			}
	    if (reportModule.equalsIgnoreCase("customerFile")) {
	    	customerFile = customerFileManager.get(customerFileManager.findRemoteCustomerFile(myFile.getCustomerNumber()));
	    	if(customerFile!=null){
			List shipNumberList = myFileManager.findShipNumberBySequenceNumer(myFile.getCustomerNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
			if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
				Iterator shipIt =  shipNumberList.iterator();
					while(shipIt.hasNext()){
						String shipNumber = (String) shipIt.next();
						List shipNumberListSo = myFileManager.getShipNumberList(shipNumber);
						if(shipNumberListSo!=null && !shipNumberListSo.isEmpty()){
						Iterator it = shipNumberListSo.iterator();
				         while (it.hasNext()) {
				        	String agentFlag=(String)it.next();
				        	if(agentFlag.equalsIgnoreCase("BA") && isBookingAgent!=null && isBookingAgent==true){
				        		myFile.setIsBookingAgent(true);
							}else if(agentFlag.equalsIgnoreCase("NA") && isNetworkAgent!=null && isNetworkAgent==true){
								myFile.setIsNetworkAgent(true);
							}else if(agentFlag.equalsIgnoreCase("OA") && isOriginAgent!=null && isOriginAgent==true){
								myFile.setIsOriginAgent(true);
							}else if(agentFlag.equalsIgnoreCase("SOA") && isSubOriginAgent!=null && isSubOriginAgent==true){
								myFile.setIsSubOriginAgent(true);
							}else if(agentFlag.equalsIgnoreCase("DA") && isDestAgent!=null && isDestAgent==true){
								myFile.setIsDestAgent(true);
							}else if(agentFlag.equalsIgnoreCase("SDA") && isSubDestAgent!=null && isSubDestAgent==true){
								myFile.setIsSubDestAgent(true);
							} 
				         }
					}
				}
			}else{
				myFile.setIsBookingAgent(false);
		    	myFile.setIsNetworkAgent(false);
		    	myFile.setIsOriginAgent(false);
		    	myFile.setIsSubOriginAgent(false);
		    	myFile.setIsDestAgent(false);
		    	myFile.setIsSubDestAgent(false);
			}
		}
	    }
	    myFile = myFileManager.save(myFile);
	    if (reportModule.equalsIgnoreCase("customerFile")) {
			if(myFile.getIsBookingAgent() || myFile.getIsNetworkAgent() 
					|| myFile.getIsOriginAgent() || myFile.getIsSubOriginAgent() 
					|| myFile.getIsDestAgent() || myFile.getIsSubDestAgent()){																
				customerFile = customerFileManager.get(customerFileManager.findRemoteCustomerFile(myFile.getCustomerNumber()));
				if(customerFile!=null)
				{
			String networkLinkId="";
			List previousSequenceNumberList = new ArrayList();
			List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
			if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
				Iterator shipIt =  shipNumberList.iterator();
					while(shipIt.hasNext()){
						List linkedShipNumberList= new ArrayList();	
						String shipNumberOld = (String) shipIt.next();
			linkedShipNumberList=myFileManager.getLinkedShipNumber(shipNumberOld, "Primary", myFile.getIsBookingAgent(), myFile.getIsNetworkAgent(), myFile.getIsOriginAgent(), myFile.getIsSubOriginAgent(), myFile.getIsDestAgent(), myFile.getIsSubDestAgent());
			Set set = new HashSet(linkedShipNumberList);
			List linkedSeqNum = new ArrayList(set);
			if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
				Iterator it = linkedSeqNum.iterator();
		         while (it.hasNext()) {
		        	 	 String shipNumber=(String)it.next();
			        	 String removeDatarow = shipNumber.substring(0, 6);
			        	 if(!(myFile.getFileId().toString()).equalsIgnoreCase(shipNumber.substring(0, shipNumber.length()-2)) && !removeDatarow.equals("remove")){
				        	 networkLinkId = sessionCorpID + (myFile.getId()).toString();
				        	 String sequenceNumber = shipNumber.substring(0, shipNumber.length()-2);
				        	 if(previousSequenceNumberList.isEmpty() || (!(previousSequenceNumberList.contains(sequenceNumber)))){
				        	 previousSequenceNumberList.add(sequenceNumber);
				     		 myFileManager.upDateNetworkLinkId(networkLinkId,myFile.getId());
				             MyFile myFileObj = new MyFile();
				        	 myFileObj.setFileId(shipNumber.substring(0, shipNumber.length()-2));
				        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
				        	 myFileObj.setActive(myFile.getActive());
				        	 myFileObj.setCustomerNumber(shipNumber.substring(0, shipNumber.length()-2));
				        	 myFileObj.setDescription(myFile.getDescription());
				        	 myFileObj.setFileContentType(myFile.getFileContentType());
				        	 myFileObj.setFileFileName(myFile.getFileFileName());
				        	 myFileObj.setFileSize(myFile.getFileSize());
				        	 myFileObj.setFileType(myFile.getFileType());
				        	 myFileObj.setIsCportal(myFile.getIsCportal());
				        	 myFileObj.setIsAccportal(myFile.getIsAccportal());
				        	 myFileObj.setIsPartnerPortal(myFile.getIsPartnerPortal());
				        	 myFileObj.setIsServiceProvider(myFile.getIsServiceProvider());
				        	 myFileObj.setInvoiceAttachment(myFile.getInvoiceAttachment());
				        	 myFileObj.setIsBookingAgent(myFile.getIsBookingAgent());
				        	 myFileObj.setIsNetworkAgent(myFile.getIsNetworkAgent());
				        	 myFileObj.setIsOriginAgent(myFile.getIsOriginAgent());
				        	 myFileObj.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
				        	 myFileObj.setIsDestAgent(myFile.getIsDestAgent());
				        	 myFileObj.setIsSubDestAgent(myFile.getIsSubDestAgent());
				        	 myFileObj.setIsSecure(myFile.getIsSecure());
				        	 myFileObj.setLocation(myFile.getLocation());
				        	 myFileObj.setMapFolder(myFile.getMapFolder());
				        	 myFileObj.setCoverPageStripped(myFile.getCoverPageStripped());
				        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
				        	 myFileObj.setCreatedOn(new Date());
				        	 myFileObj.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
				        	 myFileObj.setUpdatedOn(new Date());
				        	 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
				        	 myFileObj.setNetworkLinkId(networkLinkId);
				        	 myFileManager.save(myFileObj);
				        	 }
			        	 }
			          }
			       }
		        }	
			  }
			}
		}
	    }
	    
	    
	    if (reportModule.equalsIgnoreCase("serviceOrder")) {
	    	System.out.println("\n\n\n\n\n  In Srvice Order");
			if(myFile.getIsBookingAgent() || myFile.getIsNetworkAgent() || myFile.getIsOriginAgent() || myFile.getIsSubOriginAgent() || myFile.getIsDestAgent() || myFile.getIsSubDestAgent()){																
			List linkedShipNumberList= new ArrayList();	
			String networkLinkId="";
			linkedShipNumberList=myFileManager.getLinkedShipNumber(myFile.getFileId(), "Primary", myFile.getIsBookingAgent(), myFile.getIsNetworkAgent(), myFile.getIsOriginAgent(), myFile.getIsSubOriginAgent(), myFile.getIsDestAgent(), myFile.getIsSubDestAgent());
			System.out.println("\n\n\n\n\n  linkedShipNumberList "+linkedShipNumberList);
			Set set = new HashSet(linkedShipNumberList);
			List linkedSeqNum = new ArrayList(set);
			if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
				Iterator it = linkedSeqNum.iterator();
		         while (it.hasNext()) {
		        	 	 String shipNumber=(String)it.next();
		        	 	System.out.println("\n\n\n\n\n  linkedShipNumber in while loop shipnumber  "+shipNumber);
			        	 String removeDatarow = shipNumber.substring(0, 6);
			        	 System.out.println("\n\n\n\n\n  linkedShipNumber in while loop removeDatarow  "+removeDatarow + " file ID "+ myFile.getFileId().toString() );
			        	 if(!(myFile.getFileId().equals(shipNumber))){
			        		 System.out.println("\n\n\n\n\n  linkedShipNumber in while loop myFile.getFileId()  "+myFile.getFileId());
			        		 System.out.println("\n\n\n\n\n  linkedShipNumber in while loop myFile.getFileId()  "+myFile.getId() +"sessionCorpID>>>>"+ sessionCorpID);
				        	 networkLinkId = sessionCorpID+myFile.getId().toString();
				        	 System.out.println("\n\n\n\n\n  linkedShipNumber in while loop networkLinkId  "+networkLinkId);
				     		 myFileManager.upDateNetworkLinkId(networkLinkId,myFile.getId());
				     		 System.out.println("\n\n\n\n\n  linkedShipNumber in while loop update networkLinkId  "+networkLinkId);
				             MyFile myFileObj = new MyFile();
				             System.out.println("\n\n\n\n\n  linkedShipNumber in while loop  "+shipNumber);
				        	 myFileObj.setFileId(shipNumber);
				        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
				        	 myFileObj.setActive(myFile.getActive());
				        	 myFileObj.setCustomerNumber(shipNumber);
				        	 myFileObj.setDescription(myFile.getDescription());
				        	 myFileObj.setFileContentType(myFile.getFileContentType());
				        	 myFileObj.setFileFileName(myFile.getFileFileName());
				        	 myFileObj.setFileSize(myFile.getFileSize());
				        	 myFileObj.setFileType(myFile.getFileType());
				        	 myFileObj.setIsCportal(myFile.getIsCportal());
				        	 myFileObj.setIsAccportal(myFile.getIsAccportal());
				        	 myFileObj.setIsPartnerPortal(myFile.getIsPartnerPortal());
				        	 myFileObj.setIsServiceProvider(myFile.getIsServiceProvider());
				        	 myFileObj.setInvoiceAttachment(myFile.getInvoiceAttachment());
				        	 myFileObj.setIsBookingAgent(myFile.getIsBookingAgent());
				        	 myFileObj.setIsNetworkAgent(myFile.getIsNetworkAgent());
				        	 myFileObj.setIsOriginAgent(myFile.getIsOriginAgent());
				        	 myFileObj.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
				        	 myFileObj.setIsDestAgent(myFile.getIsDestAgent());
				        	 myFileObj.setIsSubDestAgent(myFile.getIsSubDestAgent());
				        	 myFileObj.setIsSecure(myFile.getIsSecure());
				        	 myFileObj.setLocation(myFile.getLocation());
				        	 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
				        	 myFileObj.setMapFolder(myFile.getMapFolder());
				        	 myFileObj.setCoverPageStripped(myFile.getCoverPageStripped());
				        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
				        	 myFileObj.setCreatedOn(new Date());
				        	 myFileObj.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
				        	 myFileObj.setUpdatedOn(new Date());
				        	 myFileObj.setNetworkLinkId(networkLinkId);
				        	 myFileManager.save(myFileObj);
				        	 
				        	 System.out.println("\n\n\n\n\n  linkedShipNumber in while loop data saved");
			        	 }
			          }
			       }
		         }
			}
	    
	    
	    viewReportParam();
	    buildParameters();
			
	    ouputStream.flush();
	    ouputStream.close();
	    return SUCCESS;
	}
	private String reportParameter_Service;
	private String reportParameter_Agent;
	private String MarketagentId;
	private String jasperName;
	@SkipValidation
	public String uploadPricePoint() throws Exception{
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
		File dirPath = new File(uploadDir);
		byte[] output;
		// Added for taking id from jasper name
		Reports reportLocal = reportsManager.getReportTemplateForWT(jasperName,"",sessionCorpID);
		// initializing id value by report object
		id=reportLocal.getId();
		Map parameters = buildParameters();
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
		Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
		Locale locale = new Locale("en", "US");
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		HttpServletResponse response = getResponse();
		
		JasperPrint jasperPrint = reportsManager.generateReport(id, parameters);
		ServletOutputStream ouputStream = response.getOutputStream();
		reports = reportsManager.get(id);
		String fileName = jobNumber+"_"+reportParameter_Service+"_"+MarketagentId+"_"+reportParameter_Agent ;
		//String temp = reports.getReportName();
		//String fileName = temp.substring(0, temp.indexOf("."));
		String QueoteService="";
		if(reportParameter_Service.equalsIgnoreCase("destination")){
			QueoteService="Destination";
		}else if(reportParameter_Service.equalsIgnoreCase("origin")){
			QueoteService="Origin";
		}
		String descriptionName=QueoteService+"_"+MarketagentId+"_"+reportParameter_Agent ;
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
		output = JasperExportManager.exportReportToPdf(jasperPrint);
		ouputStream.write(output);
		if (!dirPath.exists()) {
				dirPath.mkdirs();
		}
	
		reports = reportsManager.get(id);
		String tempFileFileName = fileName;
		fileFileName = tempFileFileName+".pdf";
		
		if(myFileManager.getMaxId(sessionCorpID).get(0)==null){
			id=1L;
			fileFileName = (id+ "_" +fileFileName);
		}
		else{
			Long id=Long.parseLong(myFileManager.getMaxId(sessionCorpID).get(0).toString());
		    	id=id+1;
		    	fileFileName = (id+"_" +fileFileName );
		}
		File newFile = new File(uploadDir + fileFileName);
		String filePath="";
		filePath =(uploadDir+fileFileName );
		if (newFile.exists()) {
			newFile.delete();
		}
		FileOutputStream fos = new FileOutputStream(newFile);
		DataOutputStream bos =  new DataOutputStream(fos);
		bos.write(output);
		bos.close();
		myFile=new MyFile();
		if(jobNumber.trim().length() > 0){
			List seqNo = myFileManager.getSequanceNum(jobNumber, sessionCorpID);
			myFile.setCustomerNumber(seqNo.get(0).toString());
			myFile.setFileId(jobNumber);
		}else {
			JasperReport reportTemplate = reportsManager.getReportTemplate(id);
			JRParameter[] allParameters = reportTemplate.getParameters();
			reportParameters = new ArrayList<JRParameter>();
			for (JRParameter parameter : allParameters) {
					if (!parameter.isSystemDefined()) {
						reportParameters.add(parameter);
					}
			}
			for (JRParameter parameter : reportParameters) {
				if (!parameter.getValueClassName().equals("java.util.Date")) {
					String paramName = parameter.getName();
					if(paramName.equalsIgnoreCase("Customer File Number")){
							Object paramValue = parameters.put(parameter.getName(), custID);
							List seqNo = myFileManager.getSequanceNum(paramValue.toString(), sessionCorpID);
							if(!seqNo.isEmpty()){
							myFile.setCustomerNumber(seqNo.get(0).toString());
							myFile.setFileId(paramValue.toString());
							} else if (seqNo.isEmpty()) { 
								String key = "There are no Customer File Number which You Entered";   
					            saveMessage(getText(key));
							}
						}
					else if(paramName.equalsIgnoreCase("Service Order Number")){
						Object paramValue = parameters.put(parameter.getName(), jobNumber);
						List seqNo = myFileManager.getSequanceNum(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber(seqNo.get(0).toString());
						myFile.setFileId(paramValue.toString());
					} else if (seqNo.isEmpty()) { 
							String key = "There are no Service Order Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
					else if(paramName.equalsIgnoreCase("Invoice Number")){
						Object paramValue = parameters.put(parameter.getName(), invoiceNumber);
						List seqNo = myFileManager.getSequanceNumByInvNum(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
						myFile.setFileId(seqNo.get(0).toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Invoice Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
					else if(paramName.equalsIgnoreCase("Claim Number")){
						Object paramValue = parameters.put(parameter.getName(), claimNumber);
						List seqNo = myFileManager.getSequanceNumByClm(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
						myFile.setFileId(seqNo.get(0).toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Claim Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
					else if(paramName.equalsIgnoreCase("Work Ticket Number")){
						Object paramValue = parameters.put(parameter.getName(), noteID);
						List seqNo = myFileManager.getSequanceNumByTkt(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
						myFile.setFileId(seqNo.get(0).toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Work Ticket Number which You Entered";   
				            saveMessage(getText(key));
						}
					}else if(paramName.equalsIgnoreCase("PartnerCode")|| paramName.equalsIgnoreCase("Bill to Code")||paramName.equalsIgnoreCase("Vendor Code")){
						Object paramValue=parameters.put(parameter.getName(), BillTo);
						if(BillTo!=null && !BillTo.equals(" ")){
							myFile.setCustomerNumber(BillTo);
						    myFile.setFileId(BillTo);
							
						} else{ 
							String key = "There are no Partner Code which You Entered";   
				            saveMessage(getText(key));
						}
					}
					
				} else {
					myFile.setCustomerNumber("");
					myFile.setFileId("");
				}
			}
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
			Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			} else {
			Locale locale = new Locale("en", "US");
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
		}

	    myFile.setFileContentType("application/pdf");
	    myFile.setFileFileName(fileFileName);
	    myFile.setLocation(filePath);
	    myFile.setFileType("Quotes");
	    myFile.setCorpID(sessionCorpID);	 
	    myFile.setActive(true);	  
	    myFile.setDocumentCategory("Quotes / Rates");
	    myFile.setDescription(descriptionName);
	    myFile.setCreatedBy(getRequest().getRemoteUser());
	    myFile.setUpdatedBy(getRequest().getRemoteUser());
	    myFile.setCreatedOn(new Date());
	    myFile.setUpdatedOn(new Date());
	    
	    FileSizeUtil fileSizeUtil = new FileSizeUtil(); 
	    myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));
	    
	    myFile.setMapFolder(mapFolder);
	    
	    if(reports.getDocsxfer() != null && reports.getDocsxfer().equalsIgnoreCase("Yes")){
		    if(reports.getSecureForm() != null && reports.getSecureForm().equalsIgnoreCase("Yes")){
			    myFile.setIsSecure(true);
		    }else{
			    myFile.setIsSecure(false);
		    }  
	    }else{
		    myFile.setIsSecure(false);
	    } 
	    
	   /* if((docFileType!=null)&&(!docFileType.trim().toString().equalsIgnoreCase(""))){
	        List docControl = myFileManager.getDocAccessControl(docFileType, sessionCorpID);
	        if((docControl!=null)&&(!docControl.isEmpty())&&(docControl.get(0)!=null)&&(!docControl.get(0).toString().trim().equalsIgnoreCase(""))){
	        	String st=docControl.get(0).toString().trim();
	        	if((st.split("#")[10]!=null)&&(st.split("#")[10].toString().equalsIgnoreCase("true"))){
	        		myFile.setInvoiceAttachment(true);
	        	}
	        	
	        	if((st.split("#")[0]!=null)&&(st.split("#")[0].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsCportal(true);
	        	}
	        	if((st.split("#")[1]!=null)&&(st.split("#")[1].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsAccportal(true);
	        	}
	        	if((st.split("#")[2]!=null)&&(st.split("#")[2].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsPartnerPortal(true);
	        	}
	        	if((st.split("#")[3]!=null)&&(st.split("#")[3].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsBookingAgent(true);
	        	}
	        	if((st.split("#")[4]!=null)&&(st.split("#")[4].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsNetworkAgent(true);
	        	}
	        	if((st.split("#")[5]!=null)&&(st.split("#")[5].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsOriginAgent(true);
	        	}
	        	if((st.split("#")[6]!=null)&&(st.split("#")[6].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsSubOriginAgent(true);
	        	}
	        	if((st.split("#")[7]!=null)&&(st.split("#")[7].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsDestAgent(true);
	        	}
	        	if((st.split("#")[8]!=null)&&(st.split("#")[8].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsSubDestAgent(true);
	        	}
	        	if((st.split("#")[9]!=null)&&(st.split("#")[9].toString().equalsIgnoreCase("true"))){
	        		myFile.setIsServiceProvider(true);
	        	}
	        }
	    }*/
	    	    
	    myFileManager.save(myFile);
	    
	    viewReportParam();
	    buildParameters();
			
	    ouputStream.flush();
	    ouputStream.close();
	    return SUCCESS;
	}
	@SkipValidation
	public void claimFormFileUploadLink(Claim claim1,String jrxmlName,String docType,Claim claim) throws Exception{
		try {
			SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + claim.getCorpID() + "/" + getRequest().getRemoteUser() + "/");
			String fileName = claim.getClaimNumber()+"_"+jrxmlName.replace(".jrxml", "")+"_"+fm.format((docType.equalsIgnoreCase("Claim Declaration Form")?claim.getDocCompleted():claim.getSettlementForm()));
			String tempFileFileName = fileName;
			fileFileName = tempFileFileName+".pdf";
			String loc=uploadDir + fileFileName;
			File newFile = new File(uploadDir + fileFileName);
			FileSizeUtil fileSizeUtil = new FileSizeUtil();
			Long fid=myFileManager.findByLocation(loc, claim1.getCorpID());
			if(fid!=null){
				myFile=myFileManager.getForOtherCorpid(fid);
				myFile.setActive(true);
			    myFile.setUpdatedBy(getRequest().getRemoteUser());
			    myFile.setUpdatedOn(new Date());
			    myFile.setLocation(loc);
			    myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));
			    myFileManager.save(myFile);
			}else{
				myFile=new MyFile();
				if(claim1.getShipNumber().trim().length() > 0){
					List seqNo = myFileManager.getSequanceNum(claim1.getShipNumber(), claim1.getCorpID());
					myFile.setCustomerNumber(seqNo.get(0).toString());
					myFile.setFileId(claim1.getShipNumber());
				}
			 	myFile.setFileContentType("application/pdf");
			    myFile.setFileFileName(fileFileName);
			    myFile.setLocation(loc);
			    myFile.setFileType(docType);
			    myFile.setCorpID(claim1.getCorpID());	 
			    myFile.setActive(true);	  
			    myFile.setDocumentCategory("Claims");
			    myFile.setDescription(docType);
			    myFile.setCreatedBy(getRequest().getRemoteUser());
			    myFile.setUpdatedBy(getRequest().getRemoteUser());
			    myFile.setCreatedOn(new Date());
			    myFile.setUpdatedOn(new Date());
			    myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));
			    myFile.setMapFolder("");
			    if(reports.getDocsxfer() != null && reports.getDocsxfer().equalsIgnoreCase("Yes")){
			    	if(reports.getSecureForm() != null && reports.getSecureForm().equalsIgnoreCase("Yes")){
			    myFile.setIsSecure(true);
			    	}else{
			    myFile.setIsSecure(false);
			    	}  
			    }else{
		    	myFile.setIsSecure(false);
			    }				    
			    List docControl = myFileManager.getDocAccessControl(docType, claim1.getCorpID());
			    if((docControl!=null)&&(!docControl.isEmpty())&&(docControl.get(0)!=null)&&(!docControl.get(0).toString().trim().equalsIgnoreCase(""))){
		        	String st=docControl.get(0).toString().trim();
		        	if((st.split("#")[10]!=null)&&(st.split("#")[10].toString().equalsIgnoreCase("true"))){
		        		myFile.setInvoiceAttachment(true);
		        	}
		        	if((st.split("#")[0]!=null)&&(st.split("#")[0].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsCportal(true);
		        	}
		        	if((st.split("#")[1]!=null)&&(st.split("#")[1].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsAccportal(true);
		        	}
		        	if((st.split("#")[2]!=null)&&(st.split("#")[2].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsPartnerPortal(true);
		        	}
		        	if((st.split("#")[3]!=null)&&(st.split("#")[3].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsBookingAgent(true);
		        	}else{
		        		myFile.setIsBookingAgent(false);
		        	}
		        	if((st.split("#")[4]!=null)&&(st.split("#")[4].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsNetworkAgent(true);
		        	}else{
		        		myFile.setIsNetworkAgent(false);
		        	}
		        	if((st.split("#")[5]!=null)&&(st.split("#")[5].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsOriginAgent(true);
		        	}else{
		        		myFile.setIsOriginAgent(false);
		        	}
		        	if((st.split("#")[6]!=null)&&(st.split("#")[6].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsSubOriginAgent(true);
		        	}else{
		        		myFile.setIsSubOriginAgent(false);
		        	}
		        	if((st.split("#")[7]!=null)&&(st.split("#")[7].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsDestAgent(true);
		        	}else{
		        		myFile.setIsDestAgent(false);
		        	}
		        	if((st.split("#")[8]!=null)&&(st.split("#")[8].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsSubDestAgent(true);
		        	}else{
		        		myFile.setIsSubDestAgent(false);
		        	}
		        	if((st.split("#")[9]!=null)&&(st.split("#")[9].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsServiceProvider(true);
		        	}
		        }else{
		        	myFile.setIsBookingAgent(false);
		        	myFile.setIsNetworkAgent(false);
		        	myFile.setIsOriginAgent(false);
		        	myFile.setIsSubOriginAgent(false);
		        	myFile.setIsDestAgent(false);
		        	myFile.setIsSubDestAgent(false);
		        }
			   myFileManager.save(myFile);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}		
	@SkipValidation
	public void claimFormFileUpload(String shipNumber,Long claimNo,String jrxmlName,Date date,String docType) throws Exception{
		try {
			SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
			File dirPath = new File(uploadDir);
			String fileName = claimNo+"_"+jrxmlName.replace(".jrxml", "")+"_"+fm.format(date);
			String tempFileFileName = fileName;
			fileFileName = tempFileFileName+".pdf";
			File newFile = new File(uploadDir + fileFileName);
			Boolean fileExist=false;
						if (newFile.exists()) {
							newFile.delete();
							fileExist=true;
						}
						byte[] output;
						Reports reportLocal = reportsManager.getReportTemplateForWT(jrxmlName,"",sessionCorpID);
						id=reportLocal.getId();
					    Map parameters = buildParameters();
						company= companyManager.findByCorpID(sessionCorpID).get(0);
						if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
							Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
							parameters.put(JRParameter.REPORT_LOCALE, locale);
							} else {
							Locale locale = new Locale("en", "US");
							parameters.put(JRParameter.REPORT_LOCALE, locale);
							}
						    parameters.put("Corporate ID", sessionCorpID);
						    parameters.put("Service Order Number", shipNumber);	
							parameters.put("Claim Number", claimNo);
							HttpServletResponse response = getResponse();
							JasperPrint jasperPrint = reportsManager.generateReport(id, parameters);
							reports = reportsManager.get(id);
										
							response.setContentType("application/pdf");
							response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
							output = JasperExportManager.exportReportToPdf(jasperPrint);
							if (!dirPath.exists()) {
									dirPath.mkdirs();
							}
							String filePath="";
							filePath =(uploadDir+fileFileName);
							
							FileOutputStream fos = new FileOutputStream(newFile);
							DataOutputStream bos =  new DataOutputStream(fos);
							bos.write(output);
							bos.close();
							FileSizeUtil fileSizeUtil = new FileSizeUtil();
							Long fid=myFileManager.findByLocation(filePath, sessionCorpID);
							if(fid!=null){
								myFile=myFileManager.get(fid);
								myFile.setActive(true);
							    myFile.setUpdatedBy(getRequest().getRemoteUser());
							    myFile.setUpdatedOn(new Date());
							    myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));
							    myFileManager.save(myFile);
							}else{
									myFile=new MyFile();
								if(shipNumber.trim().length() > 0){
									List seqNo = myFileManager.getSequanceNum(shipNumber, sessionCorpID);
									myFile.setCustomerNumber(seqNo.get(0).toString());
									myFile.setFileId(shipNumber);
								}
								 	myFile.setFileContentType("application/pdf");
								    myFile.setFileFileName(fileFileName);
								    myFile.setLocation(filePath);
								    myFile.setFileType(docType);
								    myFile.setCorpID(sessionCorpID);	 
								    myFile.setActive(true);	  
								    myFile.setDocumentCategory("Claims");
								    myFile.setDescription(docType);
								    myFile.setCreatedBy(getRequest().getRemoteUser());
								    myFile.setUpdatedBy(getRequest().getRemoteUser());
								    myFile.setCreatedOn(new Date());
								    myFile.setUpdatedOn(new Date());
								    myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));
								    myFile.setMapFolder("");
								    if(reports.getDocsxfer() != null && reports.getDocsxfer().equalsIgnoreCase("Yes")){
								    	if(reports.getSecureForm() != null && reports.getSecureForm().equalsIgnoreCase("Yes")){
								    myFile.setIsSecure(true);
								    	}else{
								    myFile.setIsSecure(false);
								    	}  
								    }else{
							    	myFile.setIsSecure(false);
								    }				    
								    List docControl = myFileManager.getDocAccessControl(docType, sessionCorpID);
								    if((docControl!=null)&&(!docControl.isEmpty())&&(docControl.get(0)!=null)&&(!docControl.get(0).toString().trim().equalsIgnoreCase(""))){
							        	String st=docControl.get(0).toString().trim();
							        	if((st.split("#")[10]!=null)&&(st.split("#")[10].toString().equalsIgnoreCase("true"))){
							        		myFile.setInvoiceAttachment(true);
							        	}
							        	if((st.split("#")[0]!=null)&&(st.split("#")[0].toString().equalsIgnoreCase("true"))){
							        		myFile.setIsCportal(true);
							        	}
							        	if((st.split("#")[1]!=null)&&(st.split("#")[1].toString().equalsIgnoreCase("true"))){
							        		myFile.setIsAccportal(true);
							        	}
							        	if((st.split("#")[2]!=null)&&(st.split("#")[2].toString().equalsIgnoreCase("true"))){
							        		myFile.setIsPartnerPortal(true);
							        	}
							        	if((st.split("#")[3]!=null)&&(st.split("#")[3].toString().equalsIgnoreCase("true"))){
							        		myFile.setIsBookingAgent(true);
							        	}else{
							        		myFile.setIsBookingAgent(false);
							        	}
							        	if((st.split("#")[4]!=null)&&(st.split("#")[4].toString().equalsIgnoreCase("true"))){
							        		myFile.setIsNetworkAgent(true);
							        	}else{
							        		myFile.setIsNetworkAgent(false);
							        	}
							        	if((st.split("#")[5]!=null)&&(st.split("#")[5].toString().equalsIgnoreCase("true"))){
							        		myFile.setIsOriginAgent(true);
							        	}else{
							        		myFile.setIsOriginAgent(false);
							        	}
							        	if((st.split("#")[6]!=null)&&(st.split("#")[6].toString().equalsIgnoreCase("true"))){
							        		myFile.setIsSubOriginAgent(true);
							        	}else{
							        		myFile.setIsSubOriginAgent(false);
							        	}
							        	if((st.split("#")[7]!=null)&&(st.split("#")[7].toString().equalsIgnoreCase("true"))){
							        		myFile.setIsDestAgent(true);
							        	}else{
							        		myFile.setIsDestAgent(false);
							        	}
							        	if((st.split("#")[8]!=null)&&(st.split("#")[8].toString().equalsIgnoreCase("true"))){
							        		myFile.setIsSubDestAgent(true);
							        	}else{
							        		myFile.setIsSubDestAgent(false);
							        	}
							        	if((st.split("#")[9]!=null)&&(st.split("#")[9].toString().equalsIgnoreCase("true"))){
							        		myFile.setIsServiceProvider(true);
							        	}
							        }else{
							        	myFile.setIsBookingAgent(false);
							        	myFile.setIsNetworkAgent(false);
							        	myFile.setIsOriginAgent(false);
							        	myFile.setIsSubOriginAgent(false);
							        	myFile.setIsDestAgent(false);
							        	myFile.setIsSubDestAgent(false);
							        }
								   myFileManager.save(myFile);
							}
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}	
	@SkipValidation
	public void orderInitiationFileUpload(String sequenceNumber,String jrxmlName) throws Exception{
		try {
			custID=sequenceNumber;
			formReportFlag="F";
			//fileType="pdf";
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
			File dirPath = new File(uploadDir);
			byte[] output;
			Reports reportLocal = reportsManager.getReportTemplateForWT(jrxmlName,"",sessionCorpID);
			id=reportLocal.getId();
		    Map parameters = buildParameters();
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
				Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
				parameters.put(JRParameter.REPORT_LOCALE, locale);
				} else {
				Locale locale = new Locale("en", "US");
				parameters.put(JRParameter.REPORT_LOCALE, locale);
				}
			    /* Locale locale = new Locale("en", "US");
			    parameters.put(JRParameter.REPORT_LOCALE, locale);*/
			    parameters.put("Corporate ID", sessionCorpID);
			    parameters.put("Customer File Number", sequenceNumber);		   
				HttpServletResponse response = getResponse();
				JasperPrint jasperPrint = reportsManager.generateReport(id, parameters);
				//ServletOutputStream ouputStream = response.getOutputStream();
				reports = reportsManager.get(id);
				String fileNameDetails="";
				if(jrxmlName.equalsIgnoreCase("OrderIntiation.jrxml")){
					fileNameDetails="Order Initiation";
				}else if(jrxmlName.equalsIgnoreCase("ASMLOrderIntiation.jrxml")){
					fileNameDetails="ASMLOrder Initiation";
				}else{
					fileNameDetails="Order Initiation";
				}
				String fileName = sequenceNumber+"_"+fileNameDetails ;
			
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
				output = JasperExportManager.exportReportToPdf(jasperPrint);
				//ouputStream.write(output);
				//ouputStream.flush();
				//ouputStream.close();
				if (!dirPath.exists()) {
						dirPath.mkdirs();
				}
				reports = reportsManager.get(id);
				String tempFileFileName = fileName;
				fileFileName = tempFileFileName+".pdf";
				
				/*if(myFileManager.getMaxId(sessionCorpID).get(0)==null){
					id=1L;
					fileFileName = (id+ "_" +fileFileName);
				}
				else{
					Long id=Long.parseLong(myFileManager.getMaxId(sessionCorpID).get(0).toString());
				    	id=id+1;
				    	fileFileName = (id+"_" +fileFileName );
				}*/
				File newFile = new File(uploadDir + fileFileName);
				String filePath="";
				filePath =(uploadDir+fileFileName );
				if (newFile.exists()) {
					newFile.delete();
				}
				FileOutputStream fos = new FileOutputStream(newFile);
				DataOutputStream bos =  new DataOutputStream(fos);
				bos.write(output);
				bos.close();
				myFile=new MyFile();
				if(sequenceNumber.trim().length() > 0){
					List seqNo = myFileManager.getSequanceNum(sequenceNumber, sessionCorpID);
					myFile.setCustomerNumber(seqNo.get(0).toString());
					myFile.setFileId(sequenceNumber);
				}
				 myFile.setFileContentType("application/pdf");
				    myFile.setFileFileName(fileFileName);
				    myFile.setLocation(filePath);
				    myFile.setFileType("Move Authorization");
				    myFile.setCorpID(sessionCorpID);	 
				    myFile.setActive(true);	  
				    myFile.setDocumentCategory("Move Authorization");
				    myFile.setDescription("Order Initiation");
				    myFile.setCreatedBy(getRequest().getRemoteUser());
				    myFile.setUpdatedBy(getRequest().getRemoteUser());
				    myFile.setCreatedOn(new Date());
				    myFile.setUpdatedOn(new Date());
				    List docAccessControlList = myFileManager.getDocAccessControl("Move Authorization", sessionCorpID);
				    if(docAccessControlList!=null && !docAccessControlList.isEmpty()){
				    	String accessControl=docAccessControlList.get(0).toString();
				    	String [] accessControls=accessControl.split("#");
				    	if(accessControls[0].toString().equals("true")){
				    		myFile.setIsCportal(true);
				    	}
				    	if(accessControls[1].toString().equals("true")){
				    		myFile.setIsAccportal(true);
				    	}
				    	if(accessControls[2].toString().equals("true")){
				    		myFile.setIsPartnerPortal(true);
				    	}
				    	if(accessControls[3].toString().equals("true")){
				    		myFile.setIsBookingAgent(true);
				    	}
				    	if(accessControls[4].toString().equals("true")){
				    		myFile.setIsNetworkAgent(true);
				    	}
				    	if(accessControls[5].toString().equals("true")){
				    		myFile.setIsOriginAgent(true);
				    	}
				    	if(accessControls[6].toString().equals("true")){
				    		myFile.setIsSubOriginAgent(true);
				    	}
				    	if(accessControls[7].toString().equals("true")){
				    		myFile.setIsDestAgent(true);
				    	}
				    	if(accessControls[8].toString().equals("true")){
				    		myFile.setIsSubDestAgent(true);
				    	}
				    	if(accessControls[9].toString().equals("true")){
				    		myFile.setIsServiceProvider(true);
				    	}
				    	if(accessControls[10].toString().equals("true")){
				    		myFile.setInvoiceAttachment(true);
				    	}
				    }
				    
				    FileSizeUtil fileSizeUtil = new FileSizeUtil(); 
				    myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));
				    
				    myFile.setMapFolder(mapFolder);
				    
				    if(reports.getDocsxfer() != null && reports.getDocsxfer().equalsIgnoreCase("Yes")){
					    if(reports.getSecureForm() != null && reports.getSecureForm().equalsIgnoreCase("Yes")){
						    myFile.setIsSecure(true);
					    }else{
						    myFile.setIsSecure(false);
					    }  
				    }else{
					    myFile.setIsSecure(false);
				    } 
				    myFileManager.save(myFile);
				  
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	private String saveInFileCabinet;
	private String jrxmlName;
	@SkipValidation
	public void operationIntilligenceFileUpload() throws Exception{
		Reports reportLocal = reportsManager.getReportTemplateForWT(jrxmlName,"",sessionCorpID);
		id=reportLocal.getId();
		byte[] output;
		Map parameters = buildParameters();
		Date updatedDate = null;
	    company= companyManager.findByCorpID(sessionCorpID).get(0);
		 Date date = new Date();
	     SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss");
	     sdf.setTimeZone(TimeZone.getTimeZone(company.getTimeZone()));
	     String date1 = sdf.format(date);
	     
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
		Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
		Locale locale = new Locale("en", "US");
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		jobNumber=parameters.get("Service Order Number").toString();
		
		try {
			if (myFileManager.getMaxId(sessionCorpID).get(0) == null) {
				id1 = 1L;
			}else{
				 id1 = Long.parseLong(myFileManager.getMaxId(sessionCorpID).get(0).toString());
				id1 = id1 + 1;	
			}
			}catch(Exception e){
				
			}
		String fileName = jobNumber+"_"+id1+"_"+"OI" ;
		String header = new String();
		StringBuffer sb= new StringBuffer();
		reports = reportsManager.get(id);
		/*String query = reportsManager.generateReportExtract(id, parameters);
		
		query = query.replaceAll(":", "-");
		List extractList = sqlExtractManager.sqlDataExtract(query);
		try {
			if (!extractList.isEmpty()) {
				
				int starValue = query.indexOf("AS")+3;
				int endValue = query.indexOf("FROM");
				String subQuery = query.substring(starValue,endValue);
				subQuery = subQuery+",";
				subQuery = subQuery.replaceAll("\n", "");
				String [] tempString = null;
		       
				tempString = subQuery.split("AS ");
				int totalLength = tempString.length;
				String finalOP ="";
				for (int i = 0 ; i < totalLength ; i++) {
		     	   String tmp = tempString[i];
		     	   finalOP += tmp.substring(0,tmp.indexOf(","))+"\t";
				}
				header = finalOP.toUpperCase().substring(0,finalOP.length())+"\n";
				sb.append(header);
				//ouputStream.write(header.getBytes());

				Iterator it = extractList.iterator();
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();

					for(int i=1; i<totalLength+1; i++){
						if (row[i] != null) {
							String data = row[i].toString();
							sb.append(data + "\t");
						} else {
							sb.append("\t");
						}
					}
					sb.append("\n");
			    }
			}else{
				header = "NO Record Found , thanks";
				sb.append(header);
			}
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
		}
*/		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
		File dirPath = new File(uploadDir);
		if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		
		strFilePath=uploadDir+""+fileName+".pdf";
		File newFile = new File(strFilePath);
        FileOutputStream fos = new FileOutputStream(strFilePath);
        fos.write(String.valueOf(sb).getBytes());
        fos.close();
        HttpServletResponse response = getResponse();
        JasperPrint jasperPrint = reportsManager.generateReport(id, parameters);
        ServletOutputStream ouputStream = response.getOutputStream();
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
		output = JasperExportManager.exportReportToPdf(jasperPrint);
		ouputStream.write(output);
		ouputStream.flush();
	    ouputStream.close();
	    if(saveInFileCabinet!=null && saveInFileCabinet.equalsIgnoreCase("true"))
	    {
	    fileFileName = fileName+".pdf";
		newFile = new File(uploadDir + fileFileName);
		String filePath="";
		filePath =(uploadDir+fileFileName );
		if (newFile.exists()) {
			newFile.delete();
		}
		FileOutputStream fos1 = new FileOutputStream(newFile);
		DataOutputStream bos =  new DataOutputStream(fos1);
		bos.write(output);
		bos.close();
		myFile=new MyFile();
		if(jobNumber.trim().length() > 0){
			List seqNo = myFileManager.getSequanceNum(jobNumber, sessionCorpID);
			myFile.setCustomerNumber(seqNo.get(0).toString());
			myFile.setFileId(jobNumber);
		}else {
			JasperReport reportTemplate = reportsManager.getReportTemplate(id);
			JRParameter[] allParameters = reportTemplate.getParameters();
			reportParameters = new ArrayList<JRParameter>();
			for (JRParameter parameter : allParameters) {
					if (!parameter.isSystemDefined()) {
						reportParameters.add(parameter);
					}
			}
			for (JRParameter parameter : reportParameters) {
				if (!parameter.getValueClassName().equals("java.util.Date")) {
					String paramName = parameter.getName();
					if(paramName.equalsIgnoreCase("Customer File Number")){
							Object paramValue = parameters.put(parameter.getName(), custID);
							List seqNo = myFileManager.getSequanceNum(paramValue.toString(), sessionCorpID);
							if(!seqNo.isEmpty()){
							myFile.setCustomerNumber(seqNo.get(0).toString());
							myFile.setFileId(paramValue.toString());
							} else if (seqNo.isEmpty()) { 
								String key = "There are no Customer File Number which You Entered";   
					            saveMessage(getText(key));
							}
						}
					else if(paramName.equalsIgnoreCase("Service Order Number")){
						Object paramValue = parameters.put(parameter.getName(), jobNumber);
						List seqNo = myFileManager.getSequanceNum(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber(seqNo.get(0).toString());
						myFile.setFileId(paramValue.toString());
					} else if (seqNo.isEmpty()) { 
							String key = "There are no Service Order Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
					else if(paramName.equalsIgnoreCase("Invoice Number")){
						Object paramValue = parameters.put(parameter.getName(), invoiceNumber);
						List seqNo = myFileManager.getSequanceNumByInvNum(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
						myFile.setFileId(seqNo.get(0).toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Invoice Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
					else if(paramName.equalsIgnoreCase("Claim Number")){
						Object paramValue = parameters.put(parameter.getName(), claimNumber);
						List seqNo = myFileManager.getSequanceNumByClm(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
						myFile.setFileId(seqNo.get(0).toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Claim Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
					else if(paramName.equalsIgnoreCase("Work Ticket Number")){
						Object paramValue = parameters.put(parameter.getName(), noteID);
						List seqNo = myFileManager.getSequanceNumByTkt(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
						myFile.setFileId(seqNo.get(0).toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Work Ticket Number which You Entered";   
				            saveMessage(getText(key));
						}
					}else if(paramName.equalsIgnoreCase("PartnerCode")|| paramName.equalsIgnoreCase("Bill to Code")||paramName.equalsIgnoreCase("Vendor Code")){
						Object paramValue=parameters.put(parameter.getName(), BillTo);
						if(BillTo!=null && !BillTo.equals(" ")){
							myFile.setCustomerNumber(BillTo);
						    myFile.setFileId(BillTo);
							
						} else{ 
							String key = "There are no Partner Code which You Entered";   
				            saveMessage(getText(key));
						}
					}
					
				} else {
					myFile.setCustomerNumber("");
					myFile.setFileId("");
				}
			}
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
			Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			} else {
			Locale locale = new Locale("en", "US");
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
		}
		
		 DateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm:ss"); 
		 sdf.setTimeZone(TimeZone.getTimeZone(company.getTimeZone()));
		 updatedDate = sdf.parse(date1);
	    myFile.setFileContentType("application/pdf");
	    myFile.setFileFileName(fileName+".pdf");
	    myFile.setLocation(filePath);
	    myFile.setFileType("Rate Quotation");
	    myFile.setCorpID(sessionCorpID);	 
	    myFile.setActive(true);	  
	    myFile.setDocumentCategory("Quotes / Rates");
	    myFile.setDescription("Rate Quotation "+date1);
	    myFile.setCreatedBy(getRequest().getRemoteUser());
	    myFile.setUpdatedBy(getRequest().getRemoteUser());
	    myFile.setCreatedOn(updatedDate);
	    myFile.setUpdatedOn(updatedDate);
	    
	    FileSizeUtil fileSizeUtil = new FileSizeUtil(); 
	    myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));
	    
	    myFile.setMapFolder(mapFolder);
	    
	    if(reports.getDocsxfer() != null && reports.getDocsxfer().equalsIgnoreCase("Yes")){
		    if(reports.getSecureForm() != null && reports.getSecureForm().equalsIgnoreCase("Yes")){
			    myFile.setIsSecure(true);
		    }else{
			    myFile.setIsSecure(false);
		    }  
	    }else{
		    myFile.setIsSecure(false);
	    } 
	    myFileManager.save(myFile);
	    }
		 //Code for Save Myfile  End
	}
	private String jrxmlNameRev;
	@SkipValidation
	public void operationRevisionFileUpload() throws Exception{
		Reports reportLocal = reportsManager.getReportTemplateForWT(jrxmlNameRev,"",sessionCorpID);
		id=reportLocal.getId();
		byte[] output;
		Map parameters = buildParameters();
		Date updatedDate = null;
	    company= companyManager.findByCorpID(sessionCorpID).get(0);
		 Date date = new Date();
	     SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss");
	     sdf.setTimeZone(TimeZone.getTimeZone(company.getTimeZone()));
	     String date1 = sdf.format(date);
	     
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
		Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
		Locale locale = new Locale("en", "US");
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		jobNumber=parameters.get("Service Order Number").toString();
		
		try {
			if (myFileManager.getMaxId(sessionCorpID).get(0) == null) {
				id1 = 1L;
			}else{
				 id1 = Long.parseLong(myFileManager.getMaxId(sessionCorpID).get(0).toString());
				id1 = id1 + 1;	
			}
			}catch(Exception e){
				
			}
		String fileName = jobNumber+"_"+id1+"_"+"OI" ;
		String header = new String();
		StringBuffer sb= new StringBuffer();
		reports = reportsManager.get(id);
		/*String query = reportsManager.generateReportExtract(id, parameters);
		
		query = query.replaceAll(":", "-");
		List extractList = sqlExtractManager.sqlDataExtract(query);
		try {
			if (!extractList.isEmpty()) {
				
				int starValue = query.indexOf("AS")+3;
				int endValue = query.indexOf("FROM");
				String subQuery = query.substring(starValue,endValue);
				subQuery = subQuery+",";
				subQuery = subQuery.replaceAll("\n", "");
				String [] tempString = null;
		       
				tempString = subQuery.split("AS ");
				int totalLength = tempString.length;
				String finalOP ="";
				for (int i = 0 ; i < totalLength ; i++) {
		     	   String tmp = tempString[i];
		     	   finalOP += tmp.substring(0,tmp.indexOf(","))+"\t";
				}
				header = finalOP.toUpperCase().substring(0,finalOP.length())+"\n";
				sb.append(header);
				//ouputStream.write(header.getBytes());

				Iterator it = extractList.iterator();
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();

					for(int i=1; i<totalLength+1; i++){
						if (row[i] != null) {
							String data = row[i].toString();
							sb.append(data + "\t");
						} else {
							sb.append("\t");
						}
					}
					sb.append("\n");
			    }
			}else{
				header = "NO Record Found , thanks";
				sb.append(header);
			}
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
		}
*/		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
		File dirPath = new File(uploadDir);
		if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		
		strFilePath=uploadDir+""+fileName+".pdf";
		File newFile = new File(strFilePath);
        FileOutputStream fos = new FileOutputStream(strFilePath);
        fos.write(String.valueOf(sb).getBytes());
        fos.close();
        HttpServletResponse response = getResponse();
        JasperPrint jasperPrint = reportsManager.generateReport(id, parameters);
        ServletOutputStream ouputStream = response.getOutputStream();
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
		output = JasperExportManager.exportReportToPdf(jasperPrint);
		ouputStream.write(output);
		ouputStream.flush();
	    ouputStream.close();
	    if(saveInFileCabinet!=null && saveInFileCabinet.equalsIgnoreCase("true"))
	    {
	    fileFileName = fileName+".pdf";
		newFile = new File(uploadDir + fileFileName);
		String filePath="";
		filePath =(uploadDir+fileFileName );
		if (newFile.exists()) {
			newFile.delete();
		}
		FileOutputStream fos1 = new FileOutputStream(newFile);
		DataOutputStream bos =  new DataOutputStream(fos1);
		bos.write(output);
		bos.close();
		myFile=new MyFile();
		if(jobNumber.trim().length() > 0){
			List seqNo = myFileManager.getSequanceNum(jobNumber, sessionCorpID);
			myFile.setCustomerNumber(seqNo.get(0).toString());
			myFile.setFileId(jobNumber);
		}else {
			JasperReport reportTemplate = reportsManager.getReportTemplate(id);
			JRParameter[] allParameters = reportTemplate.getParameters();
			reportParameters = new ArrayList<JRParameter>();
			for (JRParameter parameter : allParameters) {
					if (!parameter.isSystemDefined()) {
						reportParameters.add(parameter);
					}
			}
			for (JRParameter parameter : reportParameters) {
				if (!parameter.getValueClassName().equals("java.util.Date")) {
					String paramName = parameter.getName();
					if(paramName.equalsIgnoreCase("Customer File Number")){
							Object paramValue = parameters.put(parameter.getName(), custID);
							List seqNo = myFileManager.getSequanceNum(paramValue.toString(), sessionCorpID);
							if(!seqNo.isEmpty()){
							myFile.setCustomerNumber(seqNo.get(0).toString());
							myFile.setFileId(paramValue.toString());
							} else if (seqNo.isEmpty()) { 
								String key = "There are no Customer File Number which You Entered";   
					            saveMessage(getText(key));
							}
						}
					else if(paramName.equalsIgnoreCase("Service Order Number")){
						Object paramValue = parameters.put(parameter.getName(), jobNumber);
						List seqNo = myFileManager.getSequanceNum(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber(seqNo.get(0).toString());
						myFile.setFileId(paramValue.toString());
					} else if (seqNo.isEmpty()) { 
							String key = "There are no Service Order Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
					else if(paramName.equalsIgnoreCase("Invoice Number")){
						Object paramValue = parameters.put(parameter.getName(), invoiceNumber);
						List seqNo = myFileManager.getSequanceNumByInvNum(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
						myFile.setFileId(seqNo.get(0).toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Invoice Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
					else if(paramName.equalsIgnoreCase("Claim Number")){
						Object paramValue = parameters.put(parameter.getName(), claimNumber);
						List seqNo = myFileManager.getSequanceNumByClm(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
						myFile.setFileId(seqNo.get(0).toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Claim Number which You Entered";   
				            saveMessage(getText(key));
						}
					}
					else if(paramName.equalsIgnoreCase("Work Ticket Number")){
						Object paramValue = parameters.put(parameter.getName(), noteID);
						List seqNo = myFileManager.getSequanceNumByTkt(paramValue.toString(), sessionCorpID);
						if(!seqNo.isEmpty()){
						myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
						myFile.setFileId(seqNo.get(0).toString());
						} else if (seqNo.isEmpty()) { 
							String key = "There are no Work Ticket Number which You Entered";   
				            saveMessage(getText(key));
						}
					}else if(paramName.equalsIgnoreCase("PartnerCode")|| paramName.equalsIgnoreCase("Bill to Code")||paramName.equalsIgnoreCase("Vendor Code")){
						Object paramValue=parameters.put(parameter.getName(), BillTo);
						if(BillTo!=null && !BillTo.equals(" ")){
							myFile.setCustomerNumber(BillTo);
						    myFile.setFileId(BillTo);
							
						} else{ 
							String key = "There are no Partner Code which You Entered";   
				            saveMessage(getText(key));
						}
					}
					
				} else {
					myFile.setCustomerNumber("");
					myFile.setFileId("");
				}
			}
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
			Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			} else {
			Locale locale = new Locale("en", "US");
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
		}
		
		 DateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm:ss"); 
		 sdf.setTimeZone(TimeZone.getTimeZone(company.getTimeZone()));
		 updatedDate = sdf.parse(date1);
	    myFile.setFileContentType("application/pdf");
	    myFile.setFileFileName(fileName+".pdf");
	    myFile.setLocation(filePath);
	    myFile.setFileType("Rate Quotation");
	    myFile.setCorpID(sessionCorpID);	 
	    myFile.setActive(true);	  
	    myFile.setDocumentCategory("Quotes / Rates");
	    myFile.setDescription("Rate Quotation "+date1);
	    myFile.setCreatedBy(getRequest().getRemoteUser());
	    myFile.setUpdatedBy(getRequest().getRemoteUser());
	    myFile.setCreatedOn(updatedDate);
	    myFile.setUpdatedOn(updatedDate);
	    
	    FileSizeUtil fileSizeUtil = new FileSizeUtil(); 
	    myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));
	    
	    myFile.setMapFolder(mapFolder);
	    
	    if(reports.getDocsxfer() != null && reports.getDocsxfer().equalsIgnoreCase("Yes")){
		    if(reports.getSecureForm() != null && reports.getSecureForm().equalsIgnoreCase("Yes")){
			    myFile.setIsSecure(true);
		    }else{
			    myFile.setIsSecure(false);
		    }  
	    }else{
		    myFile.setIsSecure(false);
	    } 
	    myFileManager.save(myFile);
	    }
		 //Code for Save Myfile  End
	}

		
	@SkipValidation
	public void operationIntilligenceFileUpload1() throws Exception{
		try {
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
			File dirPath = new File(uploadDir);
			byte[] output = null;
			// Added for taking id from jasper name
			Reports reportLocal = reportsManager.getReportTemplateForWT(jrxmlName,"",sessionCorpID);
			// initializing id value by report object
			id=reportLocal.getId();
			Map parameters = buildParameters();
			parameters.put("Corporate ID", sessionCorpID);
		    parameters.put("Service Order Number", jobNumber);	
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
			Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			} else {
			Locale locale = new Locale("en", "US");
			parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
						
			HttpServletResponse response = getResponse();
			ServletOutputStream ouputStream = response.getOutputStream();
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			reports = reportsManager.get(id);
			String fileName = jobNumber+"_"+"OI" ;
			String QueoteService="";
			
			
				response.setContentType("application/vnd.ms-excel; charset=UTF-8");
				response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xls");
				String header = new String();
				String query = reportsManager.generateReportExtract(id, parameters);
				query = query.replaceAll(":", "-");
				List extractList = sqlExtractManager.sqlDataExtract(query);
				try {
					if (!extractList.isEmpty()) {
						
						int starValue = query.indexOf("AS")+3;
						int endValue = query.indexOf("FROM");
						String subQuery = query.substring(starValue,endValue);
						subQuery = subQuery+",";
						subQuery = subQuery.replaceAll("\n", "");
						String [] tempString = null;
				       
						tempString = subQuery.split("AS ");
						int totalLength = tempString.length;
						String finalOP ="";
						for (int i = 0 ; i < totalLength ; i++) {
				     	   String tmp = tempString[i];
				     	   finalOP += tmp.substring(0,tmp.indexOf(","))+"\t";
						}
						header = finalOP.toUpperCase().substring(0,finalOP.length())+"\n";
						ouputStream.write(header.getBytes());

						Iterator it = extractList.iterator();
						while (it.hasNext()) {
							Object[] row = (Object[]) it.next();

							for(int i=1; i<totalLength+1; i++){
								if (row[i] != null) {
									String data = row[i].toString();
									ouputStream.write((data + "\t").getBytes());
								} else {
									ouputStream.write("\t".getBytes());
								}
							}
							ouputStream.write("\n".getBytes());
					    }
					}else{
						header = "NO Record Found , thanks";
						ouputStream.write(header.getBytes());
					}
				} catch (Exception e) {
					System.out.println("ERROR:" + e);
				}
			reports = reportsManager.get(id);
			String tempFileFileName = fileName;
			fileFileName = tempFileFileName+".xls";
			
			if(myFileManager.getMaxId(sessionCorpID).get(0)==null){
				id=1L;
				fileFileName = (id+ "_" +fileFileName);
			}
			else{
				Long id=Long.parseLong(myFileManager.getMaxId(sessionCorpID).get(0).toString());
			    	id=id+1;
			    	fileFileName = (id+"_" +fileFileName );
			}
			File newFile = new File(uploadDir + fileFileName);
			String filePath="";
			filePath =(uploadDir+fileFileName );
			if (newFile.exists()) {
				newFile.delete();
			}
			FileOutputStream fos = new FileOutputStream(newFile);
			fos.write(ouputStream.toString().getBytes());
			//DataOutputStream bos =  new DataOutputStream(fos);
			//bos.write(ouputStream.toString().getBytes());
			//bos.close();
			MyFile myFile=new MyFile();
			if(jobNumber.trim().length() > 0){
				List seqNo = myFileManager.getSequanceNum(jobNumber, sessionCorpID);
				myFile.setCustomerNumber(seqNo.get(0).toString());
				myFile.setFileId(jobNumber);
			}else {
				JasperReport reportTemplate = reportsManager.getReportTemplate(id);
				JRParameter[] allParameters = reportTemplate.getParameters();
				reportParameters = new ArrayList<JRParameter>();
				for (JRParameter parameter : allParameters) {
						if (!parameter.isSystemDefined()) {
							reportParameters.add(parameter);
						}
				}
				for (JRParameter parameter : reportParameters) {
					if (!parameter.getValueClassName().equals("java.util.Date")) {
						String paramName = parameter.getName();
						if(paramName.equalsIgnoreCase("Customer File Number")){
								Object paramValue = parameters.put(parameter.getName(), custID);
								List seqNo = myFileManager.getSequanceNum(paramValue.toString(), sessionCorpID);
								if(!seqNo.isEmpty()){
								myFile.setCustomerNumber(seqNo.get(0).toString());
								myFile.setFileId(paramValue.toString());
								} else if (seqNo.isEmpty()) { 
									String key = "There are no Customer File Number which You Entered";   
						            saveMessage(getText(key));
								}
							}
						else if(paramName.equalsIgnoreCase("Service Order Number")){
							Object paramValue = parameters.put(parameter.getName(), jobNumber);
							List seqNo = myFileManager.getSequanceNum(paramValue.toString(), sessionCorpID);
							if(!seqNo.isEmpty()){
							myFile.setCustomerNumber(seqNo.get(0).toString());
							myFile.setFileId(paramValue.toString());
						} else if (seqNo.isEmpty()) { 
								String key = "There are no Service Order Number which You Entered";   
					            saveMessage(getText(key));
							}
						}
						else if(paramName.equalsIgnoreCase("Invoice Number")){
							Object paramValue = parameters.put(parameter.getName(), invoiceNumber);
							List seqNo = myFileManager.getSequanceNumByInvNum(paramValue.toString(), sessionCorpID);
							if(!seqNo.isEmpty()){
							myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
							myFile.setFileId(seqNo.get(0).toString());
							} else if (seqNo.isEmpty()) { 
								String key = "There are no Invoice Number which You Entered";   
					            saveMessage(getText(key));
							}
						}
						else if(paramName.equalsIgnoreCase("Claim Number")){
							Object paramValue = parameters.put(parameter.getName(), claimNumber);
							List seqNo = myFileManager.getSequanceNumByClm(paramValue.toString(), sessionCorpID);
							if(!seqNo.isEmpty()){
							myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
							myFile.setFileId(seqNo.get(0).toString());
							} else if (seqNo.isEmpty()) { 
								String key = "There are no Claim Number which You Entered";   
					            saveMessage(getText(key));
							}
						}
						else if(paramName.equalsIgnoreCase("Work Ticket Number")){
							Object paramValue = parameters.put(parameter.getName(), noteID);
							List seqNo = myFileManager.getSequanceNumByTkt(paramValue.toString(), sessionCorpID);
							if(!seqNo.isEmpty()){
							myFile.setCustomerNumber((seqNo.get(0).toString()).substring(0, (seqNo.get(0).toString()).length()-2));
							myFile.setFileId(seqNo.get(0).toString());
							} else if (seqNo.isEmpty()) { 
								String key = "There are no Work Ticket Number which You Entered";   
					            saveMessage(getText(key));
							}
						}else if(paramName.equalsIgnoreCase("PartnerCode")|| paramName.equalsIgnoreCase("Bill to Code")||paramName.equalsIgnoreCase("Vendor Code")){
							Object paramValue=parameters.put(parameter.getName(), BillTo);
							if(BillTo!=null && !BillTo.equals(" ")){
								myFile.setCustomerNumber(BillTo);
							    myFile.setFileId(BillTo);
								
							} else{ 
								String key = "There are no Partner Code which You Entered";   
					            saveMessage(getText(key));
							}
						}
						
					} else {
						myFile.setCustomerNumber("");
						myFile.setFileId("");
					}
				}
				company= companyManager.findByCorpID(sessionCorpID).get(0);
				if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
				Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
				parameters.put(JRParameter.REPORT_LOCALE, locale);
				} else {
				Locale locale = new Locale("en", "US");
				parameters.put(JRParameter.REPORT_LOCALE, locale);
				}
			}
		    myFile.setFileContentType("application/vnd.ms-excel; charset=UTF-8");
		    myFile.setFileFileName(fileFileName);
		    myFile.setLocation(filePath);
		    myFile.setFileType("Quotes");
		    myFile.setCorpID(sessionCorpID);	 
		    myFile.setActive(true);	  
		    myFile.setDocumentCategory("Quotes / Rates");
		    myFile.setDescription("descriptionName");
		    myFile.setCreatedBy(getRequest().getRemoteUser());
		    myFile.setUpdatedBy(getRequest().getRemoteUser());
		    myFile.setCreatedOn(new Date());
		    myFile.setUpdatedOn(new Date());
		    
		    FileSizeUtil fileSizeUtil = new FileSizeUtil(); 
		    myFile.setFileSize(fileSizeUtil.FindFileSize(newFile));
		    
		    myFile.setMapFolder(mapFolder);
		    
		    if(reports.getDocsxfer() != null && reports.getDocsxfer().equalsIgnoreCase("Yes")){
			    if(reports.getSecureForm() != null && reports.getSecureForm().equalsIgnoreCase("Yes")){
				    myFile.setIsSecure(true);
			    }else{
				    myFile.setIsSecure(false);
			    }  
		    }else{
			    myFile.setIsSecure(false);
		    } 
		    
		   /* if((docFileType!=null)&&(!docFileType.trim().toString().equalsIgnoreCase(""))){
		        List docControl = myFileManager.getDocAccessControl(docFileType, sessionCorpID);
		        if((docControl!=null)&&(!docControl.isEmpty())&&(docControl.get(0)!=null)&&(!docControl.get(0).toString().trim().equalsIgnoreCase(""))){
		        	String st=docControl.get(0).toString().trim();
		        	if((st.split("#")[10]!=null)&&(st.split("#")[10].toString().equalsIgnoreCase("true"))){
		        		myFile.setInvoiceAttachment(true);
		        	}
		        	
		        	if((st.split("#")[0]!=null)&&(st.split("#")[0].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsCportal(true);
		        	}
		        	if((st.split("#")[1]!=null)&&(st.split("#")[1].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsAccportal(true);
		        	}
		        	if((st.split("#")[2]!=null)&&(st.split("#")[2].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsPartnerPortal(true);
		        	}
		        	if((st.split("#")[3]!=null)&&(st.split("#")[3].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsBookingAgent(true);
		        	}
		        	if((st.split("#")[4]!=null)&&(st.split("#")[4].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsNetworkAgent(true);
		        	}
		        	if((st.split("#")[5]!=null)&&(st.split("#")[5].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsOriginAgent(true);
		        	}
		        	if((st.split("#")[6]!=null)&&(st.split("#")[6].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsSubOriginAgent(true);
		        	}
		        	if((st.split("#")[7]!=null)&&(st.split("#")[7].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsDestAgent(true);
		        	}
		        	if((st.split("#")[8]!=null)&&(st.split("#")[8].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsSubDestAgent(true);
		        	}
		        	if((st.split("#")[9]!=null)&&(st.split("#")[9].toString().equalsIgnoreCase("true"))){
		        		myFile.setIsServiceProvider(true);
		        	}
		        }
		    }*/
		    	    
		    myFileManager.save(myFile);
		    
		    /*viewReportParam();
		    buildParameters();
				
		    ouputStream.flush();
		    ouputStream.close();*/
		   
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	@SkipValidation
	public String pdfPreview() throws Exception{
		byte[] output;
		Reports reports = reportsManager.getReportTemplateForWT(jasperName,"",sessionCorpID);
		id=reports.getId();
		Map parameters = buildParameters();
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
		Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
		Locale locale = new Locale("en", "US");
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		reports.setReportHits(reports.getReportHits()+1);
		HttpServletResponse response = getResponse();
		JasperPrint jasperPrint = reportsManager.generateReport(id, parameters);
		ServletOutputStream ouputStream = response.getOutputStream();
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=" + (reports.getReportName()).substring(0, (reports.getReportName()).indexOf(".")) + ".pdf");
		output = JasperExportManager.exportReportToPdf(jasperPrint);
		ouputStream.write(output);
		ouputStream.flush();
	    ouputStream.close();
	    return null;
		}	
	
	
	@SkipValidation
	public void printForms(){
		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			JasperPrint jasperPrint = new JasperPrint();
			List<JasperPrint> jasperPrints = new ArrayList<JasperPrint>();
			HttpServletResponse response = getResponse();
			ServletOutputStream ouputStream = response.getOutputStream();
			
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(new Date()));
			String dateFrom = nowYYYYMMDD1.toString();
			
			String fileName = "";
			if(custID != null && custID.length()>0){
				fileName = custID + "_" + dateFrom;
			}
			else if(jobNumber != null && jobNumber.length()>0){
				fileName = jobNumber + "_" + dateFrom;
			}
			else{
				fileName = "Combined_Report_" + dateFrom;
			}
			
			rId = rId.trim();
			if (rId.indexOf(",") == 0) {
				rId = rId.substring(1);
			}
			if (rId.lastIndexOf(",") == rId.length() - 1) {
				rId = rId.substring(0, rId.length() - 1);
			}
			String[] arrayid = rId.split(",");
			int arrayLength = arrayid.length;
			for (int i = 0; i < arrayLength; i++) {
				id = Long.parseLong(arrayid[i]);
				
				Map parameters = new HashMap();
				        company= companyManager.findByCorpID(sessionCorpID).get(0);
				        if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
					Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
					parameters.put(JRParameter.REPORT_LOCALE, locale);
					} else {
					Locale locale = new Locale("en", "US");
					parameters.put(JRParameter.REPORT_LOCALE, locale);
					}
				JasperReport reportTemplate = reportsManager.getReportTemplate(id);
				JRParameter[] allParameters = reportTemplate.getParameters();
				reportParameters = new ArrayList<JRParameter>();
				
				for (JRParameter parameter : allParameters) {
					if (!parameter.isSystemDefined()) {
						reportParameters.add(parameter);

					}
				}
				for (JRParameter parameter : reportParameters) {
					if (!parameter.getValueClassName().equals("java.util.Date")) {
						String paramName = parameter.getName();
						
						if(paramName.equalsIgnoreCase("Service Order Number")){
							parameters.put(parameter.getName(), jobNumber);
						}
						else if(paramName.equalsIgnoreCase("Regnum")){
							parameters.put(parameter.getName(), regNumber);
						}
						else if(paramName.equalsIgnoreCase("User Name")){
							parameters.put(parameter.getName(), getRequest().getRemoteUser());
						}
						else if(paramName.equalsIgnoreCase("Corporate ID")){
							parameters.put(parameter.getName(), sessionCorpID);
						}
						else if(paramName.equalsIgnoreCase("Work Ticket Number")){
							parameters.put(parameter.getName(), noteID);
						}
						else if(paramName.equalsIgnoreCase("Customer File Number")){
							parameters.put(parameter.getName(), custID);
						}
						else if(paramName.equalsIgnoreCase("Claim Number")){
							parameters.put(parameter.getName(), claimNumber);
						}
						else if(paramName.equalsIgnoreCase("Book Number")){
							parameters.put(parameter.getName(), bookNumber);
						}
						else if(paramName.equalsIgnoreCase("Invoice Number")){
							parameters.put(parameter.getName(), invoiceNumber);
						}else if(paramName.equalsIgnoreCase("Quotation File Number")){
							parameters.put(parameter.getName(), jobNumber);
						}
						else{
							parameters.put(parameter.getName(), "");
						}
					}
				}
				jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);
				jasperPrints.add(jasperPrint);
			}
			
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename="+fileName+".pdf");
			
			JRExporter exporterPDF = new JRPdfExporter();
			exporterPDF.setParameter(JRPdfExporterParameter.IS_CREATING_BATCH_MODE_BOOKMARKS, true);
			exporterPDF.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, jasperPrints);
			exporterPDF.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
			exporterPDF.exportReport();
			
			ouputStream.write(byteArrayOutputStream.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@SkipValidation
	public String accountDashboard() throws Exception {
		return SUCCESS;
	}
	
	
	
	/* 
	 * Bug# 8118 - Mobile Mover Integration
	 * Method for creating workticket pdf and saving at location
	 * Use for mobile mover dispatch email
	 */
	@SkipValidation
	public String saveWorkTicketMobileMoverPDF() throws Exception{
		String fileType = "PDF";
		 Map parameters = new HashMap();
		 parameters.put("Work Ticket Number", ticket);
		 parameters.put("Corporate ID", sessionCorpID);
		   
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
		File dirPath = new File(uploadDir);
		byte[] output;
		Reports reportLocal = reportsManager.getReportTemplateForWT(jasperName,"",sessionCorpID);
		id=reportLocal.getId();
		//Map parameters = buildParameters();
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
		Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
		Locale locale = new Locale("en", "US");
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		HttpServletResponse response = getResponse();
		
		JasperPrint jasperPrint = reportsManager.generateReport(id, parameters);
		/*PrintWriter ouputStream = response.getWriter();*/
		reports = reportsManager.get(id);
		String fileName = "WorkTicket-"+ticket ;
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
		output = JasperExportManager.exportReportToPdf(jasperPrint);
		//ouputStream.write(output);
		if (!dirPath.exists()) {
				dirPath.mkdirs();
		}
		reports = reportsManager.get(id);
		String tempFileFileName = fileName;
		fileFileName = tempFileFileName+".pdf";
		File newFile = new File(uploadDir + fileFileName);
		String filePath="";
		filePath =(uploadDir+fileFileName );
		if (newFile.exists()) {
			newFile.delete();
		}
		FileOutputStream fos = new FileOutputStream(newFile);
		DataOutputStream bos =  new DataOutputStream(fos);
		bos.write(output);
		bos.close();
		return filePath;
	}
	
	

	@SkipValidation
	public String accountDashboardShipment(){
		/*List list = reportsManager.getServiceOrderGraph(sessionCorpID,"Shipment Analysis By Weight','Shipment Analysis By SO','Shipment Analysis By DPort");
		Iterator iterator = list.iterator();
		while(iterator.hasNext()){
			Object object = (Object)iterator.next();
			if(list != null && !list.isEmpty()){
				graphSoNumber = object[1].toString();
				graphSoWeight = object[0].toString();
				graphSoDPort = object[2].toString();
			}
		}*/
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String viewMultiDashboard() throws Exception {
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String viewDashboard() throws Exception {
		byte[] output;
		Map parameters = new HashMap();
		
		if(formFrom.equalsIgnoreCase("list")){
			JasperReport reportTemplate = reportsManager.getReportTemplate(id);
			JRParameter[] allParameters = reportTemplate.getParameters();
			reportParameters = new ArrayList<JRParameter>();
			
			for (JRParameter parameter : allParameters) {
				if (!parameter.isSystemDefined()) {
					reportParameters.add(parameter);
				}
			}
			for (JRParameter parameter : reportParameters) {
				if (!parameter.getValueClassName().equals("java.util.Date")) {
					String paramName = parameter.getName();
					
					if(paramName.equalsIgnoreCase("Service Order Number")){
						parameters.put(parameter.getName(), jobNumber);
					}
					else if(paramName.equalsIgnoreCase("Regnum")){
						parameters.put(parameter.getName(), regNumber);
					}
					else if(paramName.equalsIgnoreCase("User Name")){
						parameters.put(parameter.getName(), getRequest().getRemoteUser());
					}
					else if(paramName.equalsIgnoreCase("Corporate ID")){
						parameters.put(parameter.getName(), sessionCorpID);
					}
					else if(paramName.equalsIgnoreCase("Work Ticket Number")){
						parameters.put(parameter.getName(), noteID);
					}
					else if(paramName.equalsIgnoreCase("Customer File Number")){
						parameters.put(parameter.getName(), custID);
					}
					else if(paramName.equalsIgnoreCase("Claim Number")){
						parameters.put(parameter.getName(), claimNumber);
					}
					else if(paramName.equalsIgnoreCase("Book Number")){
						parameters.put(parameter.getName(), bookNumber);
					}
					else if(paramName.equalsIgnoreCase("Invoice Number")){
						parameters.put(parameter.getName(), invoiceNumber);
					}else if(paramName.equalsIgnoreCase("Quotation File Number")){
						parameters.put(parameter.getName(), jobNumber);
					}
					else{
						parameters.put(parameter.getName(), "");
					}
				}
			}
		}else{
			parameters = buildParameters();
		}
		
		HttpServletResponse response = getResponse();
		HttpServletRequest request = getRequest();
		JasperPrint jasperPrint = new JasperPrint();
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
		Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
		Locale locale = new Locale("en", "US");
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		ServletOutputStream ouputStream = response.getOutputStream();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		reports = reportsManager.get(id);
		
		String modelName=(((reports.getFormFieldName()!=null) && (!reports.getFormFieldName().equalsIgnoreCase(""))) ?reports.getFormFieldName().substring(0, reports.getFormFieldName().indexOf(".")):"");
		String fieldName=(((reports.getFormFieldName()!=null) && (!reports.getFormFieldName().equalsIgnoreCase(""))) ?reports.getFormFieldName().substring(reports.getFormFieldName().indexOf(".")+1,reports.getFormFieldName().length()):"");
		String value = reports.getFormValue();
		if((!modelName.equalsIgnoreCase(""))&&(!fieldName.equalsIgnoreCase(""))&&(!value.equalsIgnoreCase(""))){
			if(modelName.equalsIgnoreCase("serviceOrder")){
				Long sid=null;
				try{
					sid=serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString());
				}catch (Exception e){
					sid=Long.parseLong(accountLineManager.findShipNumByInvoiceId(parameters.get("Invoice Number").toString()).get(0).toString());
				}
					reportsManager.updateTableStatus(modelName,fieldName,value,sid);
			}else if(modelName.equalsIgnoreCase("customerFile")){
				Long sid=customerFileManager.findRemoteCustomerFile(parameters.get("Customer File Number").toString());
				reportsManager.updateTableStatus(modelName,fieldName,value,sid);
			}else if(modelName.equalsIgnoreCase("workTicket")){
				Long sid=workTicketManager.findRemoteWorkTicket(parameters.get("Work Ticket Number").toString());
				reportsManager.updateTableStatus(modelName,fieldName,value,sid);
			}else if(modelName.equalsIgnoreCase("accountLine")){
				Long sid=Long.parseLong(accountLineManager.getAllRemoteAccountLine(serviceOrderManager.findRemoteServiceOrder(parameters.get("Service Order Number").toString())).toString());
				reportsManager.updateTableStatus(modelName,fieldName,value,sid);
			}
		}
		String temp = reports.getReportName();
		reports.setReportHits(reports.getReportHits()+1);
		String fileName = temp.substring(0, temp.indexOf("."));
		String fileType = request.getParameter("fileType");
/*	if (fileType.length() != 0 && fileType.equalsIgnoreCase("HTML")) {
			try{
				JRExporter exporterHTML = new JRHtmlExporter();
				exporterHTML.setParameter(JRHtmlExporterParameter.CHARACTER_ENCODING, "UTF-8");
				exporterHTML.setParameter(JRHtmlExporterParameter.JASPER_PRINT, reportsManager.generateReport(id, parameters));
				exporterHTML.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+ "/redsky/servlet/image?image=");//"." + imageFolderName.substring(imageFolderName.lastIndexOf("\\", imageFolderName.length())) + "\\"); 
				request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, reportsManager.generateReport(id, parameters));
			   	exporterHTML.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.TRUE);
				exporterHTML.setParameter(JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR, Boolean.FALSE);
				exporterHTML.setParameter(JRHtmlExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
				exporterHTML.setParameter(JRHtmlExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
				exporterHTML.exportReport();
				}
			catch (Exception e) {
					System.out.println("Jasper HTML Exception:" + e);
				}
		}*/
	
	
	if (fileType.length() != 0 && fileType.equalsIgnoreCase("HTML")) {
		try{
			HashMap imagesMap = new HashMap();
		    request.getSession().setAttribute("IMAGES_MAP", imagesMap);
			request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, reportsManager.generateReport(id, parameters));
		    JRExporter exporterHTML = new JRHtmlExporter();
			exporterHTML.setParameter(JRHtmlExporterParameter.CHARACTER_ENCODING, "UTF-8");
			exporterHTML.setParameter(JRHtmlExporterParameter.IMAGES_MAP, imagesMap);
			exporterHTML.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+ "/redsky/servlet/image?image=");//"." + imageFolderName.substring(imageFolderName.lastIndexOf("\\", imageFolderName.length())) + "\\"); 
			exporterHTML.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.TRUE);
			exporterHTML.setParameter(JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR, Boolean.FALSE);
			exporterHTML.setParameter(JRHtmlExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			exporterHTML.setParameter(JRHtmlExporterParameter.JASPER_PRINT, reportsManager.generateReport(id, parameters));
			exporterHTML.setParameter(JRCsvExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
			exporterHTML.exportReport();
			ouputStream.write(byteArrayOutputStream.toByteArray());
			ouputStream.flush();
			ouputStream.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			ouputStream.flush();
			ouputStream.close();
		}
	}
	
	
		
	/*if (fileType.length() != 0 && fileType.equalsIgnoreCase("HTML")){
			
			try {
			String serverName = request.getServerName();
			String serverPort = String.valueOf(request.getServerPort());
			if (serverPort != null && serverPort.trim().length() > 0)
			serverName = serverName + ":" + serverPort + "/";
			JRExporter exporterHTML = new JRHtmlExporter();
			exporterHTML.setParameter(JRExporterParameter.JASPER_PRINT, reportsManager.generateReport(id, parameters));
			exporterHTML.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getScheme()+"://"+serverName+ "/servlet/image?image=");
			request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, reportsManager.generateReport(id, parameters));
			exporterHTML.setParameter(JRHtmlExporterParameter.OUTPUT_STREAM, ouputStream);
			exporterHTML.exportReport();
			StringBuffer sbHtml = new StringBuffer();
			exporterHTML.setParameter(JRHtmlExporterParameter.OUTPUT_STRING_BUFFER,sbHtml);	
		    //request.getSession().removeAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE);
			//request.getSession().removeValue(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE);
			
						} catch (Exception e) {
				System.out.println("Jasper HTML Exception:" + e);
			}
	}*/
		
		if (fileType.length() != 0 && fileType.equalsIgnoreCase("EXTRACT")) {
			response.setContentType("application/vnd.ms-excel; charset=UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xls");
			String header = new String();
			String query = reportsManager.generateReportExtract(id, parameters);
			query = query.replaceAll(":", "-");
			List extractList = sqlExtractManager.sqlDataExtract(query);
			try {
				if (!extractList.isEmpty()) {
					
					int starValue = query.indexOf("AS")+3;
					int endValue = query.indexOf("FROM");
					String subQuery = query.substring(starValue,endValue);
					subQuery = subQuery+",";
					subQuery = subQuery.replaceAll("\n", "");
					String [] tempString = null;
			       
					tempString = subQuery.split("AS ");
					int totalLength = tempString.length;
					String finalOP ="";
					for (int i = 0 ; i < totalLength ; i++) {
			     	   String tmp = tempString[i];
			     	   finalOP += tmp.substring(0,tmp.indexOf(","))+"\t";
					}
					header = finalOP.toUpperCase().substring(0,finalOP.length())+"\n";
					ouputStream.write(header.getBytes());

					Iterator it = extractList.iterator();
					while (it.hasNext()) {
						Object[] row = (Object[]) it.next();

						for(int i=1; i<totalLength+1; i++){
							if (row[i] != null) {
								String data = row[i].toString();
								ouputStream.write((data + "\t").getBytes());
							} else {
								ouputStream.write("\t".getBytes());
							}
						}
						ouputStream.write("\n".getBytes());
				    }
				}else{
					header = "NO Record Found , thanks";
					ouputStream.write(header.getBytes());
				}
			} catch (Exception e) {
				System.out.println("ERROR:" + e);
			}
			
		}
		
		ouputStream.flush();
		ouputStream.close();
		return null;
	}
	
	
	@SkipValidation
	public void combinDoc() throws Exception {
		byte[] output;
		Map parameters = new HashMap();
		if(formFrom.equalsIgnoreCase("list")){
			JasperReport reportTemplate = reportsManager.getReportTemplate(id);
			JRParameter[] allParameters = reportTemplate.getParameters();
			reportParameters = new ArrayList<JRParameter>();
			for (JRParameter parameter : allParameters) {
				if (!parameter.isSystemDefined()) {
					reportParameters.add(parameter);
				}
			}
			for (JRParameter parameter : reportParameters) {
				if (!parameter.getValueClassName().equals("java.util.Date")) {
					String paramName = parameter.getName();
					if(paramName.equalsIgnoreCase("Service Order Number")){
						parameters.put(parameter.getName(), jobNumber);
					}
					else if(paramName.equalsIgnoreCase("Regnum")){
						parameters.put(parameter.getName(), regNumber);
					}
					else if(paramName.equalsIgnoreCase("User Name")){
						parameters.put(parameter.getName(), getRequest().getRemoteUser());
					}
					else if(paramName.equalsIgnoreCase("Corporate ID")){
						parameters.put(parameter.getName(), sessionCorpID);
					}
					else if(paramName.equalsIgnoreCase("Work Ticket Number")){
						parameters.put(parameter.getName(), noteID);
					}
					else if(paramName.equalsIgnoreCase("Customer File Number")){
						parameters.put(parameter.getName(), custID);
					}
					else if(paramName.equalsIgnoreCase("Claim Number")){
						parameters.put(parameter.getName(), claimNumber);
					}
					else if(paramName.equalsIgnoreCase("Book Number")){
						parameters.put(parameter.getName(), bookNumber);
					}
					else if(paramName.equalsIgnoreCase("Invoice Number")){
						parameters.put(parameter.getName(), invoiceNumber);
					}else if(paramName.equalsIgnoreCase("Quotation File Number")){
						parameters.put(parameter.getName(), jobNumber);
					}
					else{
						parameters.put(parameter.getName(), "");
					}
				}
			}
		}else{
			parameters = buildParameters();
		}
		HttpServletRequest request = getRequest();
		JasperPrint jasperPrint = new JasperPrint();
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
		Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
		Locale locale = new Locale("en", "US");
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
		reports = reportsManager.get(id);
		String temp = reports.getReportName();
		String fileName = temp.substring(0, temp.indexOf("."));
		String fileType = request.getParameter("fileType");
		if(fileType.equals(""))
		{
			fileType="PDF";
		}
		String file_loc ="";
		if (fileType.length() != 0 && fileType.equalsIgnoreCase("PDF")) {
			jasperPrint = reportsManager.generateReport(reportsManager.getReportTemplate(id,preferredLanguage), parameters);			
			output = JasperExportManager.exportReportToPdf(jasperPrint);
			
	        String uploadDir =
	            ServletActionContext.getServletContext().getRealPath("/images") +
	            "/" ;
	        String rightUploadDir = uploadDir.replace("redsky", "userData");  
	        File dirPath = new File(rightUploadDir);
	        if (!dirPath.exists()) {
	            dirPath.mkdirs();
	        }
	        file_loc = rightUploadDir+fileName+"."+fileType;
			
			
			if(new File(file_loc).exists())
			finaldocFileList= new ArrayList();
			finaldocFileList.add(file_loc);
			FileOutputStream  out = new FileOutputStream (file_loc);  
	        out.write(output);
			out.close();
			
		}  
		
		String docSet=refJobDocumentTypeManager.getAllDoument(jobType, sessionCorpID);
		if(docSet!="")
		{
			String docArr[]=docSet.split(",");
			for(int i=0;i<docArr.length;i++)
			{
				docFileList1.add(docArr[i]);
			}
			Iterator it = docFileList1.iterator();
			while(it.hasNext())
			{
				docFileList2=myFileManager.findByDocs(it.next().toString(), jobNumber, sessionCorpID);
				Iterator it1 = docFileList2.iterator();
				while(it1.hasNext())
				{
					String loc=(String)it1.next();
					File file = new File(loc); 
					if(file.exists())
					{
					if(loc.indexOf(".pdf")>0)
						finaldocFileList.add(loc);
					}
				}
				
			}
		
		}

		
		
		if(!(finaldocFileList.isEmpty())&& finaldocFileList!=null)
		{
			String outFile2 = jobNumber+""+sessionCorpID+".pdf";
			HttpServletResponse response1 = getResponse();
			OutputStream output1 = response1.getOutputStream();
			response1.setContentType("application/pdf");
			response1.setHeader("Content-Disposition", "attachment; filename = " + outFile2);
			response1.setHeader("Pragma", "public");
			int pageOffset = 0;
			ArrayList master = new ArrayList();
			try
	    		{
				int flag=0;
					Document document = null;
					PdfCopy  writer = null;
			    
					Iterator pdfit = finaldocFileList.iterator();
	    		
	    			while (pdfit.hasNext()) {
	    							PdfReader reader=null;
    								reader = new PdfReader((String)pdfit.next());
	    							reader.consolidateNamedDestinations();
	    							int n = reader.getNumberOfPages();
	    							List bookmarks = SimpleBookmark.getBookmark(reader);
	    							if (bookmarks != null) {
	    								if (pageOffset != 0)
	    									SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
	    									master.addAll(bookmarks);
	    							}
	    								pageOffset += n;
	    								if (flag++ == 0) {                      
	    									document = new Document(reader.getPageSizeWithRotation(1));                  
	    									writer = new PdfCopy(document,output1);                       
	    									document.open();     
	    								}
	    								PdfImportedPage page;
	    								for (int i = 0; i < n; i++) {
	    									try{page = writer.getImportedPage(reader, i+1);
	    									writer.addPage(page);}catch (Exception e) {																					}
	    								}
	    								PRAcroForm form = reader.getAcroForm();
	    								if (form != null)
	    									writer.copyAcroForm(reader);	    								
	    				}
	    			if (!master.isEmpty())
	    				writer.setOutlines(master);
	    				document.close();   
	    				output1.write("\r\n".getBytes());
	    		}catch(Exception e){}
		}
	}	 
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ReportsManager getReportsManager() {
		return reportsManager;
	}

	public void setReportsManager(ReportsManager reportsManager) {
		this.reportsManager = reportsManager;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public List getReportss() {
		return reportss;
	}

	public void setReportss(List reportss) {
		this.reportss = reportss;
	}

	public Reports getReports() {
		return reports;
	}

	public void setReports(Reports reports) {
		this.reports = reports;
	}

	public List getReportParameters() {
		return reportParameters;
	}

	public void setReportParameters(List reportParameters) {
		this.reportParameters = reportParameters;
	}

	public String getReportSubModule() {
		return reportSubModule;
	}

	public void setReportSubModule(String reportSubModule) {
		this.reportSubModule = reportSubModule;
	}

	public String getReportModule() {
		return reportModule;
	}

	public void setReportModule(String reportModule) {
		this.reportModule = reportModule;
	}

	public RefMaster getRefMaster() {
		return refMaster;
	}

	public void setRefMaster(RefMaster refMaster) {
		this.refMaster = refMaster;
	}

	
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void setDocFileType(String docFileType) {
		this.docFileType = docFileType;
	}

	public MyFile getMyFile() {
		return myFile;
	}

	public void setMyFile(MyFile myFile) {
		this.myFile = myFile;
	}

	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}

	public String getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getList() {
		return list;
	}

	public void setList(String list) {
		this.list = list;
	}

	public String getFormReportFlag() {
		return formReportFlag;
	}

	public void setFormReportFlag(String formReportFlag) {
		this.formReportFlag = formReportFlag;
	}

	public  Map<String, String> getReportFlag() {
		return reportFlag;
	}

	public  void setReportFlag(Map<String, String> reportFlag) {
		this.reportFlag = reportFlag;
	}

	public Long getAccID() {
		return accID;
	}

	public void setAccID(Long accID) {
		this.accID = accID;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public List getSoList() {
		return soList;
	}

	public void setSoList(List soList) {
		this.soList = soList;
	}

	public Map<String,String> getHouse() {
		return house;
	}

	public String getReportValidationFlag() {
		return reportValidationFlag;
	}

	public void setReportValidationFlag(String reportValidationFlag) {
		this.reportValidationFlag = reportValidationFlag;
	}

	public Map<String, String> getPayableStatusList() {
		return payableStatusList;
	}

	public void setPayableStatusList(Map<String, String> payableStatusList) {
		this.payableStatusList = payableStatusList;
	}
	
	public Map<String, String> getPayingStatusList() {
		return payingStatusList;
	}

	public void setPayingStatusList(Map<String, String> payingStatusList) {
		this.payingStatusList = payingStatusList;
	}

	public Map<String, String> getOMNI() {
		return OMNI;
	}

	public void setOMNI(Map<String, String> omni) {
		this.OMNI = omni;
	}	

	public Map<String, String> getCOMMODITS() {
		return COMMODITS;
	}

	public void setCOMMODITS(Map<String, String> commodits) {
		COMMODITS = commodits;
	}

	public List getCREWNAME() {
		return CREWNAME;
	}

	public void setCREWNAME(List crewname) {
		CREWNAME = crewname;
	}

	public static Map<String, String> getActionType() {
		return actionType;
	}

	public static void setActionType(Map<String, String> actionType) {
		ReportAction.actionType = actionType;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public List getAvailRoles() {
		return availRoles;
	}

	public void setAvailRoles(List availRoles) {
		this.availRoles = availRoles;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setRoleManager(RoleManager roleManager) {
		this.roleManager = roleManager;
	}

	public Map<String, String> getROUTING() {
		return ROUTING;
	}

	public void setROUTING(Map<String, String> routing) {
		ROUTING = routing;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Map<String, String> getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(Map<String, String> currency) {
		CURRENCY = currency;
	}

	public Map<String, String> getJOB() {
		return JOB;
	}

	public void setJOB(Map<String, String> job) {
		JOB = job;
	}

	public List getCOMPANYCODE() {
		return COMPANYCODE;
	}

	public void setCOMPANYCODE(List companycode) {
		COMPANYCODE = companycode;
	}

	public Map<String, String> getSale() {
		return sale;
	}

	public List getEsList() {
		return esList;
	}

	public void setEsList(List esList) {
		this.esList = esList;
	}

	public String getMarked() {
		return marked;
	}

	public void setMarked(String marked) {
		this.marked = marked;
	}

	public String getSessionUserName() {
		return sessionUserName;
	}

	public void setSessionUserName(String sessionUserName) {
		this.sessionUserName = sessionUserName;
	}

	public void setReportBookmarkManager(ReportBookmarkManager reportBookmarkManager) {
		this.reportBookmarkManager = reportBookmarkManager;
	}

	/**
	 * @return the reportssExt
	 */
	public ExtendedPaginatedList getReportssExt() {
		return reportssExt;
	}

	/**
	 * @param reportssExt the reportssExt to set
	 */
	public void setReportssExt(ExtendedPaginatedList reportssExt) {
		this.reportssExt = reportssExt;
	}

	/**
	 * @param pagingLookupManager the pagingLookupManager to set
	 */
	public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
		this.pagingLookupManager = pagingLookupManager;
	}

	/**
	 * @return the paginateListFactory
	 */
	public PaginateListFactory getPaginateListFactory() {
		return paginateListFactory;
	}

	/**
	 * @param paginateListFactory the paginateListFactory to set
	 */
	public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
		this.paginateListFactory = paginateListFactory;
	}

	/**
	 * @return the moduleReport
	 */
	public Map<String, String> getModuleReport() {
		return moduleReport;
	}

	/**
	 * @param moduleReport the moduleReport to set
	 */
	public void setModuleReport(Map<String, String> moduleReport) {
		this.moduleReport = moduleReport;
	}

	/**
	 * @return the subModuleReport
	 */
	public Map<String, String> getSubModuleReport() {
		return subModuleReport;
	}

	/**
	 * @param subModuleReport the subModuleReport to set
	 */
	public void setSubModuleReport(Map<String, String> subModuleReport) {
		this.subModuleReport = subModuleReport;
	}

	/**
	 * @return the jobSearchPersistValue
	 */
	public String getJobSearchPersistValue() {
		return jobSearchPersistValue;
	}

	/**
	 * @param jobSearchPersistValue the jobSearchPersistValue to set
	 */
	public void setJobSearchPersistValue(String jobSearchPersistValue) {
		this.jobSearchPersistValue = jobSearchPersistValue;
	}

	public void setSqlExtractManager(SQLExtractManager sqlExtractManager) {
		this.sqlExtractManager = sqlExtractManager;
	}

	public List getContract() {
		return contract;
	}

	public void setContract(List contract) {
		this.contract = contract;
	}

	public String getRId() {
		return rId;
	}

	public void setRId(String id) {
		rId = id;
	}

	public static Map<String, String> getMapList() {
		return mapList;
	}

	public static void setMapList(Map<String, String> mapList) {
		ReportAction.mapList = mapList;
	}

	public String getMapFolder() {
		return mapFolder;
	}

	public void setMapFolder(String mapFolder) {
		this.mapFolder = mapFolder;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public String getBookNumber() {
		return bookNumber;
	}

	public void setBookNumber(String bookNumber) {
		this.bookNumber = bookNumber;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public List getUsrName() {
		return usrName;
	}

	public void setUsrName(List usrName) {
		this.usrName = usrName;
	}

	public Map<String, String> getBaseScore() {
		return BaseScore;
	}

	public void setBaseScore(Map<String, String> baseScore) {
		BaseScore = baseScore;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public Map<String, String> getOwner() {
		return owner;
	}

	public void setOwner(Map<String, String> owner) {
		this.owner = owner;
	}

	public String getFormFrom() {
		return formFrom;
	}

	public void setFormFrom(String formFrom) {
		this.formFrom = formFrom;
	}

	public Map<String, String> getOrder() {
		return order;
	}

	public void setOrder(Map<String, String> order) {
		this.order = order;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getREPORT_LOCALE() {
		return REPORT_LOCALE;
	}

	public void setREPORT_LOCALE(String report_locale) {
		REPORT_LOCALE = report_locale;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setRefJobDocumentTypeManager(
			RefJobDocumentTypeManager refJobDocumentTypeManager) {
		this.refJobDocumentTypeManager = refJobDocumentTypeManager;
	}

	public List getFinaldocFileList() {
		return finaldocFileList;
	}

	public void setFinaldocFileList(List finaldocFileList) {
		this.finaldocFileList = finaldocFileList;
	}

	public Map<String, String> getStorageBillingGroup() {
	    return storageBillingGroup;
	}

	public void setStorageBillingGroup(Map<String, String> storageBillingGroup) {
	    this.storageBillingGroup = storageBillingGroup;
	}

	public String getBillCode() {
	    return billCode;
	}

	public void setBillCode(String billCode) {
	    this.billCode = billCode;
	}

	public List getCheckBillCodes() {
	    return checkBillCodes;
	}

	public void setCheckBillCodes(List checkBillCodes) {
	    this.checkBillCodes = checkBillCodes;
	}

	public String getBillToCode() {
	    return billToCode;
	}

	public void setBillToCode(String billToCode) {
	    this.billToCode = billToCode;
	}

	public Long getReportHits() {
	    return reportHits;
	}

	public void setReportHits(Long reportHits) {
	    this.reportHits = reportHits;
	}

	public String getBillTo() {
		return BillTo;
	}

	public void setBillTo(String billTo) {
		BillTo = billTo;
	}

	public Map<String, String> getPartnerCodeArrayList() {
		return partnerCodeArrayList;
	}

	public void setPartnerCodeArrayList(Map<String, String> partnerCodeArrayList) {
		this.partnerCodeArrayList = partnerCodeArrayList;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public List getUserListName() {
		return userListName;
	}

	public void setUserListName(List userListName) {
		this.userListName = userListName;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public String getFormNameVal() {
		return formNameVal;
	}

	public void setFormNameVal(String formNameVal) {
		this.formNameVal = formNameVal;
	}

	public String getReportSubject() {
		return reportSubject;
	}

	public void setReportSubject(String reportSubject) {
		this.reportSubject = reportSubject;
	}

	public String getReportBody() {
		return reportBody;
	}

	public void setReportBody(String reportBody) {
		this.reportBody = reportBody;
	}

	public String getReportSignature() {
		return reportSignature;
	}

	public void setReportSignature(String reportSignature) {
		this.reportSignature = reportSignature;
	}

	public String getRecipientTo() {
		return recipientTo;
	}

	public void setRecipientTo(String recipientTo) {
		this.recipientTo = recipientTo;
	}

	public String getRecipientCC() {
		return recipientCC;
	}

	public void setRecipientCC(String recipientCC) {
		this.recipientCC = recipientCC;
	}

	public String getTypedoc() {
		return typedoc;
	}

	public void setTypedoc(String typedoc) {
		this.typedoc = typedoc;
	}

	public String getStrFilePath() {
		return strFilePath;
	}

	public void setStrFilePath(String strFilePath) {
		this.strFilePath = strFilePath;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getEmailStatusVal() {
		return emailStatusVal;
	}

	public void setEmailStatusVal(String emailStatusVal) {
		this.emailStatusVal = emailStatusVal;
	}

	public String getEmailFlag() {
		return emailFlag;
	}

	public void setEmailFlag(String emailFlag) {
		this.emailFlag = emailFlag;
	}

	public String getReportSignaturePart() {
		return reportSignaturePart;
	}

	public void setReportSignaturePart(String reportSignaturePart) {
		this.reportSignaturePart = reportSignaturePart;
	}

	public String getEmailStatusMsg() {
		return emailStatusMsg;
	}

	public void setEmailStatusMsg(String emailStatusMsg) {
		this.emailStatusMsg = emailStatusMsg;
	}

	public EmailSetup getEmailSetup() {
		return emailSetup;
	}

	public void setEmailSetup(EmailSetup emailSetup) {
		this.emailSetup = emailSetup;
	}

	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public String getFromDt() {
		return fromDt;
	}

	public void setFromDt(String fromDt) {
		this.fromDt = fromDt;
	}

	public String getToDt() {
		return toDt;
	}

	public void setToDt(String toDt) {
		this.toDt = toDt;
	}

	public String getWhouse() {
		return whouse;
	}

	public void setWhouse(String whouse) {
		this.whouse = whouse;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getUserParentAgent() {
		return userParentAgent;
	}

	public void setUserParentAgent(String userParentAgent) {
		this.userParentAgent = userParentAgent;
	}

	public String getSearchFlag() {
		return searchFlag;
	}

	public void setSearchFlag(String searchFlag) {
		this.searchFlag = searchFlag;
	}

	public String getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}

	public String getReportParameter_Agent() {
		return reportParameter_Agent;
	}

	public void setReportParameter_Agent(String reportParameter_Agent) {
		this.reportParameter_Agent = reportParameter_Agent;
	}

	public String getReportParameter_Service() {
		return reportParameter_Service;
	}

	public void setReportParameter_Service(String reportParameter_Service) {
		this.reportParameter_Service = reportParameter_Service;
	}

	public String getMarketagentId() {
		return MarketagentId;
	}

	public void setMarketagentId(String marketagentId) {
		MarketagentId = marketagentId;
	}

	public String getJasperName() {
		return jasperName;
	}

	public void setJasperName(String jasperName) {
		this.jasperName = jasperName;
	}

	public String getCheckAgent() {
		return checkAgent;
	}

	public void setCheckAgent(String checkAgent) {
		this.checkAgent = checkAgent;
	}

	public Boolean getIsBookingAgent() {
		return isBookingAgent;
	}

	public void setIsBookingAgent(Boolean isBookingAgent) {
		this.isBookingAgent = isBookingAgent;
	}

	public Boolean getIsNetworkAgent() {
		return isNetworkAgent;
	}

	public void setIsNetworkAgent(Boolean isNetworkAgent) {
		this.isNetworkAgent = isNetworkAgent;
	}

	public Boolean getIsOriginAgent() {
		return isOriginAgent;
	}

	public void setIsOriginAgent(Boolean isOriginAgent) {
		this.isOriginAgent = isOriginAgent;
	}

	public Boolean getIsSubOriginAgent() {
		return isSubOriginAgent;
	}

	public void setIsSubOriginAgent(Boolean isSubOriginAgent) {
		this.isSubOriginAgent = isSubOriginAgent;
	}

	public Boolean getIsDestAgent() {
		return isDestAgent;
	}

	public void setIsDestAgent(Boolean isDestAgent) {
		this.isDestAgent = isDestAgent;
	}

	public Boolean getIsSubDestAgent() {
		return isSubDestAgent;
	}

	public void setIsSubDestAgent(Boolean isSubDestAgent) {
		this.isSubDestAgent = isSubDestAgent;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getInvoiceIncludeFlag() {
		return invoiceIncludeFlag;
	}

	public void setInvoiceIncludeFlag(String invoiceIncludeFlag) {
		this.invoiceIncludeFlag = invoiceIncludeFlag;
	}

	public String getAccountLinecompanyDivision() {
		return accountLinecompanyDivision;
	}

	public void setAccountLinecompanyDivision(String accountLinecompanyDivision) {
		this.accountLinecompanyDivision = accountLinecompanyDivision;
	}

	public Boolean getVisibilityForSalesPortla() {
		return visibilityForSalesPortla;
	}

	public void setVisibilityForSalesPortla(Boolean visibilityForSalesPortla) {
		this.visibilityForSalesPortla = visibilityForSalesPortla;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getPrintOptionsValue() {
		return printOptionsValue;
	}

	public void setPrintOptionsValue(String printOptionsValue) {
		this.printOptionsValue = printOptionsValue;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public String getJrxmlName() {
		return jrxmlName;
	}

	public void setJrxmlName(String jrxmlName) {
		this.jrxmlName = jrxmlName;
	}

	public String getSaveInFileCabinet() {
		return saveInFileCabinet;
	}

	public void setSaveInFileCabinet(String saveInFileCabinet) {
		this.saveInFileCabinet = saveInFileCabinet;
	}


	public void setReportsFieldSectionManager(
			ReportsFieldSectionManager reportsFieldSectionManager) {
		this.reportsFieldSectionManager = reportsFieldSectionManager;
	}

	public ReportsFieldSection getReportsFieldSection() {
		return reportsFieldSection;
	}

	public void setReportsFieldSection(ReportsFieldSection reportsFieldSection) {
		this.reportsFieldSection = reportsFieldSection;
	}

	public String getReportfieldsectionlist() {
		return reportfieldsectionlist;
	}

	public void setReportfieldsectionlist(String reportfieldsectionlist) {
		this.reportfieldsectionlist = reportfieldsectionlist;
	}

	public List getReportfieldSectionList() {
		return reportfieldSectionList;
	}

	public void setReportfieldSectionList(List reportfieldSectionList) {
		this.reportfieldSectionList = reportfieldSectionList;
	}

	public int getReportfieldSectionListSize() {
		return reportfieldSectionListSize;
	}

	public void setReportfieldSectionListSize(int reportfieldSectionListSize) {
		this.reportfieldSectionListSize = reportfieldSectionListSize;
	}

	public String getFieldnameUpdated() {
		return fieldnameUpdated;
	}

	public void setFieldnameUpdated(String fieldnameUpdated) {
		this.fieldnameUpdated = fieldnameUpdated;
	}

	public String getFieldvalueUpdated() {
		return fieldvalueUpdated;
	}

	public void setFieldvalueUpdated(String fieldvalueUpdated) {
		this.fieldvalueUpdated = fieldvalueUpdated;
	}


	public String getrSignature() {
		return rSignature;
	}

	public void setrSignature(String rSignature) {
		this.rSignature = rSignature;
	}

	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public Date getEmailSetupDateSent() {
		return emailSetupDateSent;
	}

	public void setEmailSetupDateSent(Date emailSetupDateSent) {
		this.emailSetupDateSent = emailSetupDateSent;
	}

	public String getCheckfromfilecabinetinvoicereportflag() {
		return checkfromfilecabinetinvoicereportflag;
	}

	public void setCheckfromfilecabinetinvoicereportflag(
			String checkfromfilecabinetinvoicereportflag) {
		this.checkfromfilecabinetinvoicereportflag = checkfromfilecabinetinvoicereportflag;
	}

	public String getCheckfilepath() {
		return checkfilepath;
	}

	public void setCheckfilepath(String checkfilepath) {
		this.checkfilepath = checkfilepath;
	}

	public Map<String, String> getRouting() {
		return routing;
	}

	public void setRouting(Map<String, String> routing) {
		this.routing = routing;
	}

	public int getReportfieldSectionListControlSize() {
		return reportfieldSectionListControlSize;
	}

	public void setReportfieldSectionListControlSize(
			int reportfieldSectionListControlSize) {
		this.reportfieldSectionListControlSize = reportfieldSectionListControlSize;
	}

	public String getControlFieldNameUpdated() {
		return controlFieldNameUpdated;
	}

	public void setControlFieldNameUpdated(String controlFieldNameUpdated) {
		this.controlFieldNameUpdated = controlFieldNameUpdated;
	}

	public String getControlFieldValueUpdated() {
		return controlFieldValueUpdated;
	}

	public void setControlFieldValueUpdated(String controlFieldValueUpdated) {
		this.controlFieldValueUpdated = controlFieldValueUpdated;
	}

	public List getReportfieldSectionControlList() {
		return reportfieldSectionControlList;
	}

	public void setReportfieldSectionControlList(List reportfieldSectionControlList) {
		this.reportfieldSectionControlList = reportfieldSectionControlList;
	}

	public String getReportfieldSectionControllist() {
		return reportfieldSectionControllist;
	}

	public void setReportfieldSectionControllist(
			String reportfieldSectionControllist) {
		this.reportfieldSectionControllist = reportfieldSectionControllist;
	}

	public Map<String, String> getRoleMap() {
		return roleMap;
	}

	public void setRoleMap(Map<String, String> roleMap) {
		this.roleMap = roleMap;
	}

	public List getMultiplAgentRolesType() {
		return multiplAgentRolesType;
	}

	public void setMultiplAgentRolesType(List multiplAgentRolesType) {
		this.multiplAgentRolesType = multiplAgentRolesType;
	}

	public List getMultiplRoleType() {
		return multiplRoleType;
	}

	public void setMultiplRoleType(List multiplRoleType) {
		this.multiplRoleType = multiplRoleType;
	}

	public String getRoutingfilter() {
		return routingfilter;
	}

	public void setRoutingfilter(String routingfilter) {
		this.routingfilter = routingfilter;
	}

	public String getRecInvNumb() {
		return recInvNumb;
	}

	public void setRecInvNumb(String recInvNumb) {
		this.recInvNumb = recInvNumb;
	}

	public Long getId1() {
		return id1;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public String getJrxmlNameRev() {
		return jrxmlNameRev;
	}

	public void setJrxmlNameRev(String jrxmlNameRev) {
		this.jrxmlNameRev = jrxmlNameRev;
	}

	public boolean isExtractFileAudit() {
		return extractFileAudit;
	}

	public void setExtractFileAudit(boolean extractFileAudit) {
		this.extractFileAudit = extractFileAudit;
	}

	public void setExtractedFileLogManager(
			ExtractedFileLogManager extractedFileLogManager) {
		this.extractedFileLogManager = extractedFileLogManager;
	}

	public ExtractedFileLog getExtractedFileLog() {
		return extractedFileLog;
	}

	public void setExtractedFileLog(ExtractedFileLog extractedFileLog) {
		this.extractedFileLog = extractedFileLog;
	}

	public String getDefaultBillingEmail() {
		return defaultBillingEmail;
	}

	public void setDefaultBillingEmail(String defaultBillingEmail) {
		this.defaultBillingEmail = defaultBillingEmail;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	public Map<String, String> getprintOptions() {
		return printOptions;
	}

	public void setprintOptions(Map<String, String> printOptions) {
		this.printOptions = printOptions;
	}

	public Map<String, String> getTcktGrp() {
		return tcktGrp;
	}

	public void setTcktGrp(Map<String, String> tcktGrp) {
		this.tcktGrp = tcktGrp;
	}

}