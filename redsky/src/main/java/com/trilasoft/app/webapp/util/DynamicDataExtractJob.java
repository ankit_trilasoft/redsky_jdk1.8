/**
 * Implementation of <strong>ActionSupport</strong> that contains convenience methods.
 * This class represents the basic actions on "DataExtraction"  in Redsky its use "QUARTZ".
 * @Class Name	DataExtractJob
 * @Author      Ravi Mishra
 * @Version     V01.0
 * @Since       1.0
 * @Date        29-Dec-2008
 */
package com.trilasoft.app.webapp.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.trilasoft.app.dao.CompanyDao;
import com.trilasoft.app.dao.ImfEntitlementsDao;
import com.trilasoft.app.dao.SystemDefaultDao;
import com.trilasoft.app.dao.ToDoRuleDao;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.EmailSetup;
import com.trilasoft.app.model.ExtractQueryFile;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ExtractQueryFileManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ToDoRuleManager;

import java.util.Calendar;

public class DynamicDataExtractJob extends QuartzJobBean {
	
	private static final String APPLICATION_CONTEXT_KEY = "applicationContext";
	private ApplicationContext appCtx;
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd";
	private Company company;
	private CompanyManager companymanager;
	private List<Company> companyList;
	private ExtractQueryFileManager extractManager;
	private EmailSetupManager emailSetupManager;
	private Long id;
	private ExtractQueryFile extractQueryFile;
	private ClaimManager claimManager;
	private User user;
	private ServiceOrderManager serviceOrderManager;
	private UserManager userManager;
	private String emailOut=(String)AppInitServlet.redskyConfigMap.get("email.out");
	private String emailDomain=(String)AppInitServlet.redskyConfigMap.get("email.domainfilter");
	private ApplicationContext getApplicationContext(JobExecutionContext context)
			throws Exception {
		ApplicationContext appCtx = null;
		appCtx = (ApplicationContext) context.getScheduler().getContext().get(
				APPLICATION_CONTEXT_KEY);
		if (appCtx == null) {
			throw new JobExecutionException(
					"No application context available in scheduler context for key \""
							+ APPLICATION_CONTEXT_KEY + "\"");
		}
		return appCtx;
	}
	
	//This method automatically called when you run jetty see applicationContext.xml 
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		String UserEmailId="";
		String ipAddress = "";
		try {
			ipAddress = "" + InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		try {
			System.out.print(emailOut+"\n\n\n\n\n\n\n\n\n\n\n\n\n Data extract");
			String executionTime="";
			appCtx = getApplicationContext(context);  
			ExtractQueryFileManager extractManager=(ExtractQueryFileManager)appCtx.getBean("extractManager");
			ClaimManager claimManager=(ClaimManager)appCtx.getBean("claimManager");
			ServiceOrderManager serviceOrderManager=(ServiceOrderManager)appCtx.getBean("serviceOrderManager"); 
			emailSetupManager=(EmailSetupManager)appCtx.getBean("emailSetupManager");
			UserManager userManager=(UserManager)appCtx.getBean("userManager");  
			List calenderSchedulerQueryList=new ArrayList();
			List schedulerQueryList =extractManager.getSchedulerQueryList();
			Iterator schedulerIterator = schedulerQueryList.iterator(); 
			SimpleDateFormat format = new SimpleDateFormat("MM-dd");
		    StringBuilder systemDate = new StringBuilder(format.format(new Date()));
		    Date todayDate=format.parse(systemDate.toString());
			while(schedulerIterator.hasNext()){
			try{
				//id=(Long)schedulerIterator.next(); 
				String idData=schedulerIterator.next().toString();
				id = Long.parseLong(idData)  ;
				extractQueryFile = extractManager.get(id);
				if(extractQueryFile.getQueryScheduler().equalsIgnoreCase("D")){
					calenderSchedulerQueryList.add(id);
				}
				if(extractQueryFile.getQueryScheduler().equalsIgnoreCase("Q")){
					Date firstQuarterDate=format.parse("03-31");
					Date secondQuarterDate=format.parse("06-30");
					Date thirdQuarterDate=format.parse("09-30");
					Date fourthQuarterDate=format.parse("12-31");
					if(todayDate.equals(firstQuarterDate)||todayDate.equals(secondQuarterDate)||todayDate.equals(thirdQuarterDate)||todayDate.equals(fourthQuarterDate)){
						calenderSchedulerQueryList.add(id);
					}
				}
				if(extractQueryFile.getQueryScheduler().equalsIgnoreCase("W")){
					GregorianCalendar newCal = new GregorianCalendar( );
					int day = newCal.get( Calendar.DAY_OF_WEEK );
					String perWeekScheduler =new String("0");
					perWeekScheduler=extractQueryFile.getPerWeekScheduler();
					if((Integer.parseInt(perWeekScheduler))==day){
						calenderSchedulerQueryList.add(id);	
					}

				}
				if(extractQueryFile.getQueryScheduler().equalsIgnoreCase("M")){
					GregorianCalendar newCal = new GregorianCalendar( );
					int day = newCal.get( Calendar.DAY_OF_MONTH ); 
					String perMonthScheduler =new String("0");
					perMonthScheduler=extractQueryFile.getPerMonthScheduler();
					if((Integer.parseInt(perMonthScheduler))==day){
						calenderSchedulerQueryList.add(id);	
					}

				}
			}catch (Exception e) { 
				e.printStackTrace();
				String emailmessage=e.toString();
			 	 if(emailmessage.length()>200){
			 		emailmessage=emailmessage.substring(200);	 
			 	 }
			 	 extractManager.updateSendMailStatus(id,UserEmailId,"error",emailmessage) ;
		    }	
			}
			String queryScheduler=new String(""); 
			Iterator itscheduler = calenderSchedulerQueryList.iterator(); 
			while(itscheduler.hasNext()){
			try{	
				id=(Long)itscheduler.next(); 
			extractQueryFile = extractManager.get(id);
			String tempCorpId=extractQueryFile.getCorpID();
			String whereCondition = extractQueryFile.getQueryCondition();
			String datePeriodCondition=extractQueryFile.getDatePeriod();
			String[] datePeriodArray=datePeriodCondition.split(",");
			String whereConditionNew="";
			String periodDateValue="";
			String[] arraywhere=whereCondition.split("#"); 
			List<String> wordList = Arrays.asList(arraywhere); 
			Iterator iterator =wordList.iterator();
			if(datePeriodCondition!=null &&(!(datePeriodCondition.equals("")))){
			while(iterator.hasNext()){
				String whereConditionElement= (String)iterator.next();
				//System.out.println("\n\n\n\n\n\n\n\n\n  whereConditionElement   "+whereConditionElement);
				String ifCondition="true";
				if((whereConditionElement.indexOf("DATE_FORMAT(t.loadA,'%Y-%m-%d')")>0) && (datePeriodCondition.indexOf("loadActualDatePeriod")>=0)){
					int arrayLength = datePeriodArray.length; 
			  		for(int i=0;i<arrayLength;i++)
			  		 { 
			  			periodDateValue =datePeriodArray[i]; 
			  		   if(!(periodDateValue.indexOf("loadActualDatePeriod")<0)){
			  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
			  		     break;
			  		   }else{
			  			 periodDateValue=""; 
			  		   }
				          
			         }
			  		if(!periodDateValue.equals("")){
					String  returnValue=   findPeriodDate(periodDateValue);
		        	String [] returnValueArray=returnValue.split("#");
		            String	Date1 = new String(returnValueArray[0]);
		            String	Date2 = new String(returnValueArray[1]);   
					String whereElement=whereConditionElement;
		            if(whereConditionElement.indexOf(">=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf(">="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date1);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   if(whereConditionElement.indexOf("<=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf("<="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date2);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   } 
			  		}
			  		else{
			  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
			  		}
			  		ifCondition="false";
			  	} else if((whereConditionElement.indexOf("(DATE_FORMAT(s.createdon,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("createDatePeriod")>=0) ){
					int arrayLength = datePeriodArray.length; 
			  		for(int i=0;i<arrayLength;i++)
			  		 { 
			  			periodDateValue =datePeriodArray[i]; 
			  		   if(!(periodDateValue.indexOf("createDatePeriod")<0)){
			  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
			  		     break;
			  		   }else{
			  			 periodDateValue=""; 
			  		   }
				          
			         }
			  		if(!periodDateValue.equals("")){
					String  returnValue=   findPeriodDate(periodDateValue);
		        	String [] returnValueArray=returnValue.split("#");
		            String	Date1 = new String(returnValueArray[0]);
		            String	Date2 = new String(returnValueArray[1]);   
					String whereElement=whereConditionElement;
		            if(whereConditionElement.indexOf(">=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf(">="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date1);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   if(whereConditionElement.indexOf("<=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf("<="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date2); 
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   } 
			  		}
			  		else{
			  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
			  		}
			  		ifCondition="false";
			  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(s.updatedOn,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("updateDatePeriod")>=0) ){
					int arrayLength = datePeriodArray.length; 
			  		for(int i=0;i<arrayLength;i++)
			  		 { 
			  			periodDateValue =datePeriodArray[i]; 
			  		   if(!(periodDateValue.indexOf("updateDatePeriod")<0)){
			  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
			  		     break;
			  		   }else{
			  			 periodDateValue=""; 
			  		   }
				          
			         }
			  		if(!periodDateValue.equals("")){
					String  returnValue=   findPeriodDate(periodDateValue);
		        	String [] returnValueArray=returnValue.split("#");
		            String	Date1 = new String(returnValueArray[0]);
		            String	Date2 = new String(returnValueArray[1]);   
					String whereElement=whereConditionElement;
		            if(whereConditionElement.indexOf(">=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf(">="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date1);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   if(whereConditionElement.indexOf("<=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf("<="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date2); 
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   } 
			  		}
			  		else{
			  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
			  		}
			  		ifCondition="false";
			  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("deliveryTargetDatePeriod")>=0)){
					int arrayLength = datePeriodArray.length; 
			  		for(int i=0;i<arrayLength;i++)
			  		 { 
			  			periodDateValue =datePeriodArray[i]; 
			  		   if(!(periodDateValue.indexOf("deliveryTargetDatePeriod")<0)){
			  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
			  		     break;
			  		   }else{
			  			 periodDateValue=""; 
			  		   }
				          
			         }
			  		if(!periodDateValue.equals("")){
					String  returnValue=   findPeriodDate(periodDateValue);
		        	String [] returnValueArray=returnValue.split("#");
		            String	Date1 = new String(returnValueArray[0]);
		            String	Date2 = new String(returnValueArray[1]);   
					String whereElement=whereConditionElement;
		            if(whereConditionElement.indexOf(">=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf(">="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date1); 
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   if(whereConditionElement.indexOf("<=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf("<="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date2); 
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   } 
			  		}
			  		else{
			  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
			  		}
			  		ifCondition="false";
			  	} else if((whereConditionElement.indexOf("(DATE_FORMAT(t.deliveryA,'%Y-%m-%d')")>0 )&&(datePeriodCondition.indexOf("deliveryActualDatePeriod")>=0)){
					int arrayLength = datePeriodArray.length; 
			  		for(int i=0;i<arrayLength;i++)
			  		 { 
			  			periodDateValue =datePeriodArray[i]; 
			  		   if(!(periodDateValue.indexOf("deliveryActualDatePeriod")<0)){
			  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
			  		     break;
			  		   }else{
			  			 periodDateValue=""; 
			  		   }
				          
			         }
			  		if(!periodDateValue.equals("")){
					String  returnValue=   findPeriodDate(periodDateValue);
		        	String [] returnValueArray=returnValue.split("#");
		            String	Date1 = new String(returnValueArray[0]);
		            String	Date2 = new String(returnValueArray[1]);   
					String whereElement=whereConditionElement;
		            if(whereConditionElement.indexOf(">=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf(">="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date1); 
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   if(whereConditionElement.indexOf("<=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf("<="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date2); 
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   } 
			  		}
			  		else{
			  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
			  		}
			  		ifCondition="false";
			  	} else if((whereConditionElement.indexOf("(DATE_FORMAT(t.beginLoad,'%Y-%m-%d')")>0) &&(datePeriodCondition.indexOf("loadTargetDatePeriod")>=0) ){
					int arrayLength = datePeriodArray.length; 
			  		for(int i=0;i<arrayLength;i++)
			  		 { 
			  			periodDateValue =datePeriodArray[i]; 
			  		   if(!(periodDateValue.indexOf("loadTargetDatePeriod")<0)){
			  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
			  		     break;
			  		   }else{
			  			 periodDateValue=""; 
			  		   }
				          
			         }
			  		if(!periodDateValue.equals("")){
					String  returnValue=   findPeriodDate(periodDateValue);
		        	String [] returnValueArray=returnValue.split("#");
		            String	Date1 = new String(returnValueArray[0]);
		            String	Date2 = new String(returnValueArray[1]);   
					String whereElement=whereConditionElement;
		            if(whereConditionElement.indexOf(">=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf(">="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date1);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   if(whereConditionElement.indexOf("<=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf("<="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date2); 
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   } 
			  		}
			  		else{
			  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
			  		}
			  		ifCondition="false";
			  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.recPostDate,'%Y-%m-%d')")>0) &&(datePeriodCondition.indexOf("invoicePostingDatePeriod")>=0 || datePeriodCondition.indexOf("PostingPeriodDatePeriod")>=0 )){
					 if(whereConditionElement.indexOf("or")<0){
						 int arrayLength = datePeriodArray.length; 
				  		for(int i=0;i<arrayLength;i++)
				  		 { 
				  			periodDateValue =datePeriodArray[i]; 
				  		   if(!(periodDateValue.indexOf("invoicePostingDatePeriod")<0)){
				  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
				  		     break;
				  		   }else{
				  			 periodDateValue=""; 
				  		   }
					          
				         }
				  		if(!periodDateValue.equals("")){
						String  returnValue=   findPeriodDate(periodDateValue);
			        	String [] returnValueArray=returnValue.split("#");
			            String	Date1 = new String(returnValueArray[0]);
			            String	Date2 = new String(returnValueArray[1]);   
						String whereElement=whereConditionElement;
			            if(whereConditionElement.indexOf(">=")>0){ 
							   String dateValue=whereConditionElement;
							   dateValue=dateValue.trim();
							   dateValue= dateValue.substring(dateValue.indexOf(">="));
							   dateValue=dateValue.trim();  
							   dateValue = dateValue.substring(2); 
						    if (dateValue.indexOf("'") == 0) {
						    	dateValue = dateValue.substring(1);
							}
						    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							whereElement=whereElement.replaceAll(dateValue, Date1);
							whereConditionNew=whereConditionNew+"#"+whereElement; 
						   }
						   if(whereConditionElement.indexOf("<=")>0){ 
							   String dateValue=whereConditionElement;
							   dateValue=dateValue.trim();
							   dateValue= dateValue.substring(dateValue.indexOf("<="));
							   dateValue=dateValue.trim();  
							   dateValue = dateValue.substring(2); 
						    if (dateValue.indexOf("'") == 0) {
						    	dateValue = dateValue.substring(1);
							}
						    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							whereElement=whereElement.replaceAll(dateValue, Date2); 
							whereConditionNew=whereConditionNew+"#"+whereElement; 
						   } 
				  		}else{
				  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
				  		}
				  		}else if(whereConditionElement.indexOf("or")>0){
							 int arrayLength = datePeriodArray.length; 
						  		for(int i=0;i<arrayLength;i++)
						  		 { 
						  			periodDateValue =datePeriodArray[i]; 
						  		   if(!(periodDateValue.indexOf("PostingPeriodDatePeriod")<0)){
						  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
						  		     break;
						  		   }else{
						  			 periodDateValue=""; 
						  		   }
							          
						         }
						  		if(!periodDateValue.equals("")){
								String  returnValue=   findPeriodDate(periodDateValue);
					        	String [] returnValueArray=returnValue.split("#");
					            String	Date1 = new String(returnValueArray[0]);
					            String	Date2 = new String(returnValueArray[1]);   
								String whereElement=whereConditionElement;
					            if(whereConditionElement.indexOf(">=")>0){ 
									   String dateValue=whereConditionElement;
									   dateValue=dateValue.trim();
									   dateValue= dateValue.substring(dateValue.indexOf(">="));
									   dateValue=dateValue.trim();  
									   dateValue = dateValue.substring(2); 
								    if (dateValue.indexOf("'") == 0) {
								    	dateValue = dateValue.substring(1);
									}
								    int j=dateValue.indexOf("'");
								    dateValue= dateValue.substring(0, j);
								    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
										dateValue = dateValue.substring(0, dateValue.length() - 1);
									}
									if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
										dateValue = dateValue.substring(0, dateValue.length() - 1);
									}
									whereElement=whereElement.replaceAll(dateValue, Date1); 
									
								   }
								   if(whereConditionElement.indexOf("<=")>0){ 
									   String dateValue=whereConditionElement;
									   dateValue=dateValue.trim();
									   dateValue= dateValue.substring(dateValue.indexOf("<="));
									   dateValue=dateValue.trim();  
									   dateValue = dateValue.substring(2); 
								    if (dateValue.indexOf("'") == 0) {
								    	dateValue = dateValue.substring(1);
									}
								    int j=dateValue.indexOf("'");
								    dateValue= dateValue.substring(0, j);
								    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
										dateValue = dateValue.substring(0, dateValue.length() - 1);
									}
									if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
										dateValue = dateValue.substring(0, dateValue.length() - 1);
									}
									whereElement=whereElement.replaceAll(dateValue, Date2);
									
								   }
								   whereConditionNew=whereConditionNew+"#"+whereElement; 
						  		}else{
						  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
						  		}
						  		} 
					 else{
						   whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	   
					   }
					 ifCondition="false";
			  	} else if((whereConditionElement.indexOf("(DATE_FORMAT(a.payPostDate,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("payablePostingDatePeriod")>=0 || datePeriodCondition.indexOf("PostingPeriodDatePeriod")>=0 )){
					   if(whereConditionElement.indexOf("or")<=0){
							 int arrayLength = datePeriodArray.length; 
						  		for(int i=0;i<arrayLength;i++)
						  		 { 
						  			periodDateValue =datePeriodArray[i]; 
						  		   if(!(periodDateValue.indexOf("payablePostingDatePeriod")<0)){
						  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
						  		     break;
						  		   }else{
						  			 periodDateValue=""; 
						  		   }
							          
						         }
						  		if(!periodDateValue.equals("")){
								String  returnValue=   findPeriodDate(periodDateValue);
					        	String [] returnValueArray=returnValue.split("#");
					            String	Date1 = new String(returnValueArray[0]);
					            String	Date2 = new String(returnValueArray[1]);   
								String whereElement=whereConditionElement;
					            if(whereConditionElement.indexOf(">=")>0){ 
									   String dateValue=whereConditionElement;
									   dateValue=dateValue.trim();
									   dateValue= dateValue.substring(dateValue.indexOf(">="));
									   dateValue=dateValue.trim();  
									   dateValue = dateValue.substring(2); 
								    if (dateValue.indexOf("'") == 0) {
								    	dateValue = dateValue.substring(1);
									}
								    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
										dateValue = dateValue.substring(0, dateValue.length() - 1);
									}
									if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
										dateValue = dateValue.substring(0, dateValue.length() - 1);
									}
									whereElement=whereElement.replaceAll(dateValue, Date1); 
									whereConditionNew=whereConditionNew+"#"+whereElement; 
								   }
								   if(whereConditionElement.indexOf("<=")>0){ 
									   String dateValue=whereConditionElement;
									   dateValue=dateValue.trim();
									   dateValue= dateValue.substring(dateValue.indexOf("<="));
									   dateValue=dateValue.trim();  
									   dateValue = dateValue.substring(2); 
								    if (dateValue.indexOf("'") == 0) {
								    	dateValue = dateValue.substring(1);
									}
								    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
										dateValue = dateValue.substring(0, dateValue.length() - 1);
									}
									if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
										dateValue = dateValue.substring(0, dateValue.length() - 1);
									}
									whereElement=whereElement.replaceAll(dateValue, Date2);
									whereConditionNew=whereConditionNew+"#"+whereElement; 
								   }
								   
						  		}else{
						  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
						  		}
						  		}else{
									   whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	   
								   }
					   ifCondition="false";   
			  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.receivedDate,'%Y-%m-%d')")>0)&& (datePeriodCondition.indexOf("receivedDatePeriod")>=0) ){
						int arrayLength = datePeriodArray.length; 
				  		for(int i=0;i<arrayLength;i++)
				  		 { 
				  			periodDateValue =datePeriodArray[i]; 
				  		   if(!(periodDateValue.indexOf("receivedDatePeriod")<0)){
				  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
				  		     break;
				  		   }else{
				  			 periodDateValue=""; 
				  		   }
					          
				         }
				  		if(!periodDateValue.equals("")){
						String  returnValue=   findPeriodDate(periodDateValue);
			        	String [] returnValueArray=returnValue.split("#");
			            String	Date1 = new String(returnValueArray[0]);
			            String	Date2 = new String(returnValueArray[1]);   
						String whereElement=whereConditionElement;
			            if(whereConditionElement.indexOf(">=")>0){ 
							   String dateValue=whereConditionElement;
							   dateValue=dateValue.trim();
							   dateValue= dateValue.substring(dateValue.indexOf(">="));
							   dateValue=dateValue.trim();  
							   dateValue = dateValue.substring(2); 
						    if (dateValue.indexOf("'") == 0) {
						    	dateValue = dateValue.substring(1);
							}
						    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							whereElement=whereElement.replaceAll(dateValue, Date1);
							whereConditionNew=whereConditionNew+"#"+whereElement; 
						   }
						   if(whereConditionElement.indexOf("<=")>0){ 
							   String dateValue=whereConditionElement;
							   dateValue=dateValue.trim();
							   dateValue= dateValue.substring(dateValue.indexOf("<="));
							   dateValue=dateValue.trim();  
							   dateValue = dateValue.substring(2); 
						    if (dateValue.indexOf("'") == 0) {
						    	dateValue = dateValue.substring(1);
							}
						    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							whereElement=whereElement.replaceAll(dateValue, Date2); 
							whereConditionNew=whereConditionNew+"#"+whereElement; 
						   }
						   
				  		}
				  		else{
				  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
				  		}
				  		ifCondition="false";	
			  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')")>0)&& (datePeriodCondition.indexOf("InvoicingdatePeriod")>=0)){

					int arrayLength = datePeriodArray.length; 
			  		for(int i=0;i<arrayLength;i++)
			  		 { 
			  			periodDateValue =datePeriodArray[i]; 
			  		   if(!(periodDateValue.indexOf("InvoicingdatePeriod")<0)){
			  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
			  		     break;
			  		   }else{
			  			 periodDateValue=""; 
			  		   }
				          
			         }
			  		if(!periodDateValue.equals("")){
					String  returnValue=   findPeriodDate(periodDateValue);
		        	String [] returnValueArray=returnValue.split("#");
		            String	Date1 = new String(returnValueArray[0]);
		            String	Date2 = new String(returnValueArray[1]);   
					String whereElement=whereConditionElement;
		            if(whereConditionElement.indexOf(">=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf(">="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date1); 
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   if(whereConditionElement.indexOf("<=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf("<="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date2);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   } 
			  		}
			  		else{
			  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
			  		} 
			  		ifCondition="false";
			  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')")>0)&& (datePeriodCondition.indexOf("storageDatePeriod")>=0)){
	                int arrayLength = datePeriodArray.length; 
			  		for(int i=0;i<arrayLength;i++)
			  		 { 
			  			periodDateValue =datePeriodArray[i]; 
			  		   if(!(periodDateValue.indexOf("storageDatePeriod")<0)){
			  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
			  		     break;
			  		   }else{
			  			 periodDateValue=""; 
			  		   }
				          
			         }
			  		if(!periodDateValue.equals("")){
					String  returnValue=   findPeriodDate(periodDateValue);
		        	String [] returnValueArray=returnValue.split("#");
		            String	Date1 = new String(returnValueArray[0]);
		            String	Date2 = new String(returnValueArray[1]);   
					String whereElement=whereConditionElement;
		            if(whereConditionElement.indexOf(">=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf(">="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date1);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   if(whereConditionElement.indexOf("<=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf("<="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date2);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   } 
			  		}
			  		else{
			  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
			  		} 
				
			  		ifCondition="false";
			  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')")>0 )&& (datePeriodCondition.indexOf("storageDatePeriod")>=0)){

	                int arrayLength = datePeriodArray.length; 
			  		for(int i=0;i<arrayLength;i++)
			  		 { 
			  			periodDateValue =datePeriodArray[i]; 
			  		   if(!(periodDateValue.indexOf("storageDatePeriod")<0)){
			  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
			  		     break;
			  		   }else{
			  			 periodDateValue=""; 
			  		   }
				          
			         }
			  		if(!periodDateValue.equals("")){
					String  returnValue=   findPeriodDate(periodDateValue);
		        	String [] returnValueArray=returnValue.split("#");
		            String	Date1 = new String(returnValueArray[0]);
		            String	Date2 = new String(returnValueArray[1]);   
					String whereElement=whereConditionElement;
		            if(whereConditionElement.indexOf(">=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf(">="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date1);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   if(whereConditionElement.indexOf("<=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf("<="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date2); 
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   } 
			  		}
			  		else{
			  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
			  		}  
			  		ifCondition="false";
			  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')")>0)&& (datePeriodCondition.indexOf("revenueRecognitionPeriod")>=0)  ){


	                int arrayLength = datePeriodArray.length; 
			  		for(int i=0;i<arrayLength;i++)
			  		 { 
			  			periodDateValue =datePeriodArray[i]; 
			  		   if(!(periodDateValue.indexOf("revenueRecognitionPeriod")<0)){
			  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
			  		     break;
			  		   }else{
			  			 periodDateValue=""; 
			  		   }
				          
			         }
			  		if(!periodDateValue.equals("")){
					String  returnValue=   findPeriodDate(periodDateValue);
		        	String [] returnValueArray=returnValue.split("#");
		            String	Date1 = new String(returnValueArray[0]);
		            String	Date2 = new String(returnValueArray[1]);   
					String whereElement=whereConditionElement;
		            if(whereConditionElement.indexOf(">=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf(">="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date1);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   if(whereConditionElement.indexOf("<=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf("<="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date2);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   } 
			  		}
			  		else{
			  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
			  		} 
			  		ifCondition="false";
				} 
			  	else if(ifCondition.equals("true")){
				      whereConditionNew=whereConditionNew+"#"+whereConditionElement; 
				}
			}
			whereCondition=whereConditionNew;
			} 
			whereCondition=whereCondition.replaceAll("#", " ");
			String selectQuery=extractQueryFile.getSelectQuery();
			String extractType=extractQueryFile.getExtractType();
			String createdBy=extractQueryFile.getCreatedby();
			String QueryName=extractQueryFile.getQueryName();
			String corpid=extractQueryFile.getCorpID();
			if(extractQueryFile.getQueryScheduler().equalsIgnoreCase("D")){
			 queryScheduler="daily";
			}
			if(extractQueryFile.getQueryScheduler().equalsIgnoreCase("W")){
				 queryScheduler="Weekly";
				}
			if(extractQueryFile.getQueryScheduler().equalsIgnoreCase("M")){
				 queryScheduler="Monthly";
				}
			if(extractQueryFile.getQueryScheduler().equalsIgnoreCase("Q")){
				 queryScheduler="Quarterly";
				}
			user = (User) claimManager.findUserDetails(createdBy).get(0);
			UserEmailId=extractQueryFile.getEmail(); 
			String columnSelect="";
			if(selectQuery.equals("")||selectQuery.equals(","))
		  	  {
		  	  }
		  	 else
		  	  {
		  		selectQuery=selectQuery.trim();
		  	    if(selectQuery.indexOf("#")==0)
		  		 {
		  	    	selectQuery=selectQuery.substring(1);
		  		 }
		  		if(selectQuery.lastIndexOf("#")==selectQuery.length()-1)
		  		 {
		  			selectQuery=selectQuery.substring(0, selectQuery.length()-1);
		  		 }
		  		columnSelect=selectQuery;
		  		selectQuery=selectQuery.replaceAll("#",",");
		  	  }
			List dataExtractList=new ArrayList();
			File file =new File("");
			String uploadDir =("/resources");
			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local"+ "/" + "redskydoc" + "/" + "dataExtract" + "/" + tempCorpId + "/");
			try{
			 File dirPath = new File(uploadDir);
		        if (!dirPath.exists()) {
		            dirPath.mkdirs();
		        }
			}catch(Exception e){}		        
			String file_name = "c:/ashish/test.txt";
			//System.out.println("\n\n\n\n\n\n\n Email1 ex    ");
			if(extractType.equalsIgnoreCase("shipmentAnalysis")){
				dataExtractList = serviceOrderManager.dynamicActiveShipments(selectQuery, whereCondition.toString(),corpid);
				file = new File("ActiveShipments_"+extractQueryFile.getId());
				file_name="ActiveShipments_"+extractQueryFile.getId();
			}
			if(extractType.equalsIgnoreCase("dynamicFinancialSummary")){
				dataExtractList = serviceOrderManager.dynamicFinancialSummary(selectQuery, whereCondition.toString(),corpid);
				file = new File("DynamicFinancialSummary_"+extractQueryFile.getId());
				file_name="DynamicFinancialSummary_"+extractQueryFile.getId();
			}
			if(extractType.equalsIgnoreCase("dynamicFinancialDetails")){
				dataExtractList = serviceOrderManager.dynamicDataFinanceDetails(selectQuery, whereCondition.toString(),corpid);
				file = new File("DynamicFinancialDetails_"+extractQueryFile.getId());
				file_name="DynamicFinancialDetails_"+extractQueryFile.getId();
			}
			if(extractType.equalsIgnoreCase("domesticAnalysis")){
				dataExtractList = serviceOrderManager.dynamicDataDomesticAnalysis(selectQuery, whereCondition.toString(),corpid);
				file = new File("DomesticAnalysis_"+extractQueryFile.getId());
				file_name="DomesticAnalysis_"+extractQueryFile.getId();
			}
			if(extractType.equalsIgnoreCase("storageBillingAnalysis")){
				dataExtractList = serviceOrderManager.dynamicStorageAnalysis(selectQuery, whereCondition.toString(),corpid);
				file = new File("StorageBillingAnalysis_"+extractQueryFile.getId());
				file_name="StorageBillingAnalysis_"+extractQueryFile.getId();
			} 
			if(extractType.equalsIgnoreCase("RelocationAnalysis")){
				dataExtractList = serviceOrderManager.dynamicReloServices(selectQuery, whereCondition.toString(),corpid);
				file = new File("RelocationAnalysis_"+extractQueryFile.getId());
				file_name="RelocationAnalysis_"+extractQueryFile.getId();
			} 
			String fileName=file_name+".xls";
			file_name=uploadDir+file_name+".xls";
	        FileWriter file1 = new FileWriter(file_name);
	        BufferedWriter out = new BufferedWriter (file1);  
			String bA_SSCW = ""; 
			String[] conditionArray=columnSelect.split("#");
		    int arrayLength = conditionArray.length;
			for(int i=0;i<arrayLength;i++)
			 {	
			   String condition=conditionArray[i]; 
			   String []columnArray=condition.split(" as"); 
			   String column=columnArray[1];
			   out.write((column+"\t"));
			 } 
			out.write("\r\n");
			Iterator it2 = dataExtractList.iterator();

			while(it2.hasNext()){
				Object[] row = (Object[]) it2.next();

				for(int i=0; i<arrayLength; i++){
					if (row[i] != null) {
						String data = row[i].toString();
						out.write((data + "\t"));
					} else {
						out.write("\t");
					}
				}
				out.write("\n");
		    }
			out.close(); 
	           String host = "localhost";
			   String from = "support@redskymobility.com";
				String tempRecipient="";
				String tempRecipientArr[]=UserEmailId.split(",");
				for(String str1:tempRecipientArr){
					if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
						if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
						tempRecipient += str1;
					}
				}
				UserEmailId=tempRecipient;
				if((emailOut!=null)&&(!emailOut.equalsIgnoreCase(""))&&(emailOut.equalsIgnoreCase("YES"))){
					//Run for Dev1 & dev2
					if((emailDomain!=null)&&(!emailDomain.equalsIgnoreCase(""))){
						 String chk[]= UserEmailId.split(",");
						 String strtemp="";
						 for(String str:chk){
							 if((emailDomain.indexOf(str.substring(str.indexOf("@")+1, str.length()))>-1)){
									if (!strtemp.equalsIgnoreCase("")) strtemp += ",";
									strtemp += str;
							 }
						 }
						 UserEmailId=strtemp;
						} 
					 if(!UserEmailId.equalsIgnoreCase("")){
						 //String to[]= UserEmailId.split(",");
						   //String to[] =  new String[]{UserEmailId}; 
						   //String filename = file.getName() + ".xls";
						   // Get system properties
						   //System.out.println("\n\n\n\n\n\n\n Email5 ex    ");
						   /*Properties props = System.getProperties();
						   props.put("mail.smtp.host", host);
						   Session session = Session.getInstance(props, null);
						   //System.out.println(session.getProperties());
						   Message message = new MimeMessage(session);
						   message.setFrom(new InternetAddress(from)); 
						   InternetAddress[] toAddress = new InternetAddress[to.length];
						   for (int i = 0; i < to.length; i++)
						     toAddress[i] = new InternetAddress(to[i]);
						   message.setRecipients(Message.RecipientType.TO, toAddress);    
						   message.setSubject(QueryName);
						   BodyPart messageBodyPart = new MimeBodyPart();*/
						   SimpleDateFormat format1 = new SimpleDateFormat("MMM-dd-yyyy");
						    StringBuilder systemDate1 = new StringBuilder(format1.format(new Date()));
						   String msgText1 = "Dear Requester,\n\nThis email was sent by the RedSky.Reports Scheduler that was setup to run "+queryScheduler+" on "+systemDate1.toString()+" \n\nSchedule Details\nFile Name:"+fileName+"\n\n\n\n Regards, "+"\n RedSky Support Team.";
						   msgText1 = msgText1 	+ "\n\nSource # " + ipAddress.substring(ipAddress.lastIndexOf(".")+1, ipAddress.length()) + "\n";
						/*   messageBodyPart.setText(msgText1);
						   Multipart multipart = new MimeMultipart();
						   multipart.addBodyPart(messageBodyPart);
						   messageBodyPart = new MimeBodyPart();
						   DataSource source = new FileDataSource(file_name);
						   messageBodyPart.setDataHandler(new DataHandler(source));
						   messageBodyPart.setFileName(fileName);
						   multipart.addBodyPart(messageBodyPart);
						   message.setContent(multipart);
						  try{
						       Transport.send(message);
						       extractManager.updateSendMailStatus(id,UserEmailId,"successfully","mail send successfully") ; 
						  }
						  catch(SendFailedException sfe)
						   {
						 	 message.setRecipients(Message.RecipientType.TO,  sfe.getValidUnsentAddresses());
						 	 Transport.send(message);
						 	 String emailmessage=sfe.toString();
						 	 if(emailmessage.length()>200){
						 		emailmessage=emailmessage.substring(200);	 
						 	 }
						 	 extractManager.updateSendMailStatus(id,UserEmailId,"error",emailmessage) ;
						   }*/ 
						   EmailSetup emailSetup = new EmailSetup();
							emailSetup.setRetryCount(0);
							emailSetup.setRecipientTo(UserEmailId);
							emailSetup.setRecipientCc("");
							emailSetup.setRecipientBcc("");
							emailSetup.setSubject(QueryName);
							emailSetup.setBody(msgText1);
							emailSetup.setSignature(from);
							emailSetup.setAttchedFileLocation(file_name);
							emailSetup.setDateSent(new Date());
							emailSetup.setEmailStatus("SaveForEmail");
							emailSetup.setCorpId(extractQueryFile.getCorpID());
							emailSetup.setCreatedOn(new Date());
							emailSetup.setCreatedBy("EMAILSETUP");
							emailSetup.setUpdatedOn(new Date());
							emailSetup.setUpdatedBy("EMAILSETUP");
							emailSetup.setSignaturePart("");
							emailSetup.setModule("Data Extract");
							emailSetup.setFileNumber("");		
							emailSetupManager.save(emailSetup);
							extractManager.updateSendMailStatus(id,UserEmailId,"successfully","mail send successfully") ;
					 	}
					
				}
		   }catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			String emailmessage=e.toString();
		 	 if(emailmessage.length()>200){
		 		emailmessage=emailmessage.substring(200);	 
		 	 }
		 	 extractManager.updateSendMailStatus(id,UserEmailId,"error",emailmessage) ;
	       }	
		} 
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			String emailmessage=e.toString();
		 	 if(emailmessage.length()>200){
		 		emailmessage=emailmessage.substring(200);	 
		 	 }
		 	 extractManager.updateSendMailStatus(id,UserEmailId,"error",emailmessage) ;
	}	
		
	}

	public String findPeriodDate(String periodName){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd ");
		Date date = new Date();
		String currentDate=dateFormat.format(date); 
		String [] currentDateArray=currentDate.split("-");
		String fromDate="";
		String toDate="";
		String returnValue="";
		String halfyear1="";
		if(periodName.toString().equals("LastYear")){ 
			int lastYear=(Integer.parseInt(currentDateArray[0]))-1;
			fromDate=lastYear+"-0"+1+"-0"+1;
			toDate=lastYear+"-"+12+"-"+31;
			returnValue=fromDate+"#"+toDate; 
		} 
		if(periodName.toString().equals("CurrentYear")){ 
			int currentYear=Integer.parseInt(currentDateArray[0]);
			fromDate=currentYear+"-0"+1+"-0"+1;
			toDate=currentYear+"-"+12+"-"+31;
			returnValue=fromDate+"#"+toDate; 
		} 
		if(periodName.toString().equals("LastHalfYear")){
			int month=Integer.parseInt(currentDateArray[1]);
			int lastYear=(Integer.parseInt(currentDateArray[0]))-1;
			int currentYear=Integer.parseInt(currentDateArray[0]);
			if(month>6){
				fromDate=currentYear+"-0"+1+"-0"+1;	
				toDate=currentYear+"-0"+6+"-"+30;
				returnValue=fromDate+"#"+toDate; 
			}else if(month<6|| month==6){
				fromDate=lastYear+"-0"+7+"-0"+1;	
				toDate=lastYear+"-"+12+"-"+31;
				returnValue=fromDate+"#"+toDate; 
				
			}
		} 
		if(periodName.toString().equals("CurrentHalfYear")){
			int month=Integer.parseInt(currentDateArray[1]);
			int lastYear=(Integer.parseInt(currentDateArray[0]))-1;
			int currentYear=Integer.parseInt(currentDateArray[0]);
			if(month>6){
				fromDate=currentYear+"-0"+7+"-0"+1;	
				toDate=currentYear+"-"+12+"-"+31;
				returnValue=fromDate+"#"+toDate; 
			}else if(month<6|| month==6){
				fromDate=currentYear+"-0"+1+"-0"+1;	
				toDate=currentYear+"-0"+6+"-"+30;
				returnValue=fromDate+"#"+toDate; 
				
			}
		} 
		if(periodName.toString().equals("LastQuarter")){
			int month=Integer.parseInt(currentDateArray[1]);
			int lastYear=(Integer.parseInt(currentDateArray[0]))-1;
			int currentYear=Integer.parseInt(currentDateArray[0]);
			if(month<3 || month==3){
				fromDate=lastYear+"-"+10+"-0"+1;	
				toDate=lastYear+"-"+12+"-"+31;
				returnValue=fromDate+"#"+toDate; 
			}else if((month>3 && month<6)||month==6){
				fromDate=currentYear+"-0"+1+"-0"+1;	
				toDate=currentYear+"-0"+3+"-"+31;
				returnValue=fromDate+"#"+toDate; 
			} else if((month>6&& month<9)||month==9){
				fromDate=currentYear+"-0"+4+"-0"+1;	
				toDate=currentYear+"-0"+6+"-"+30;
				returnValue=fromDate+"#"+toDate; 
			}
			else if((month>9)){
				fromDate=currentYear+"-0"+7+"-0"+1;	
				toDate=currentYear+"-"+"0"+9+"-"+30;
				returnValue=fromDate+"#"+toDate; 
			}
		}
		if(periodName.toString().equals("CurrentQuarter")){
			int month=Integer.parseInt(currentDateArray[1]);
			int lastYear=(Integer.parseInt(currentDateArray[0]))-1;
			int currentYear=Integer.parseInt(currentDateArray[0]);
			if(month<3 || month==3){
				fromDate=currentYear+"-0"+1+"-0"+1;	
				toDate=currentYear+"-0"+3+"-"+31;
				returnValue=fromDate+"#"+toDate; 
			}else if((month>3 && month<6)||month==6){
				fromDate=currentYear+"-0"+4+"-0"+1;	
				toDate=currentYear+"-0"+6+"-"+30;
				returnValue=fromDate+"#"+toDate; 
			} else if((month>6&& month<9)||month==9){
				fromDate=currentYear+"-0"+7+"-0"+1;	
				toDate=currentYear+"-0"+9+"-"+30;
				returnValue=fromDate+"#"+toDate; 
			}
			else if((month>9)){
				fromDate=currentYear+"-"+10+"-0"+1;	
				toDate=currentYear+"-"+12+"-"+31;
				returnValue=fromDate+"#"+toDate; 
			}
		}
		if(periodName.toString().equals("LastMonth")){
			int currentmonth=(Integer.parseInt(currentDateArray[1])); 
			int lastmonth=(Integer.parseInt(currentDateArray[1]))-1; 
			int currentYear=Integer.parseInt(currentDateArray[0]);
			int lastYear=(Integer.parseInt(currentDateArray[0]))-1;
			if(currentmonth==1){
				lastmonth=12;	
				currentYear=lastYear;
			}
			String	Stringlastmonth="";
			if(lastmonth<10){
				Stringlastmonth="0"+lastmonth;
			} else {
				Stringlastmonth=""+lastmonth;
			}
			
			Calendar calendar = Calendar.getInstance(); 
			calendar.set(currentYear,lastmonth-1,1);
		    int lastDate = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
			fromDate=currentYear+"-"+Stringlastmonth+"-0"+1;	
			toDate=currentYear+"-"+Stringlastmonth+"-"+lastDate;
			returnValue=fromDate+"#"+toDate; 
		}
		if(periodName.toString().equals("CurrentMonth")){
			int currentmonth=(Integer.parseInt(currentDateArray[1])); 
			String	Stringlastmonth="";
			if(currentmonth<10){
				Stringlastmonth="0"+currentmonth;
			} else {
				Stringlastmonth=""+currentmonth;
			}
			int currentYear=Integer.parseInt(currentDateArray[0]);
			Calendar calendar = Calendar.getInstance(); 
			calendar.set(currentYear,currentmonth-1,1);
		    int lastDate = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
			fromDate=currentYear+"-"+Stringlastmonth+"-0"+1;	
			toDate=currentYear+"-"+Stringlastmonth+"-"+lastDate;
			returnValue=fromDate+"#"+toDate; 
		}
		
		return returnValue;	
	}
	
	public void setCompanymanager(CompanyManager companymanager) {
		this.companymanager = companymanager;
	}

	public List<Company> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public ExtractQueryFileManager getExtractManager() {
		return extractManager;
	}

	public void setExtractManager(ExtractQueryFileManager extractManager) {
		this.extractManager = extractManager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ExtractQueryFile getExtractQueryFile() {
		return extractQueryFile;
	}

	public void setExtractQueryFile(ExtractQueryFile extractQueryFile) {
		this.extractQueryFile = extractQueryFile;
	}

	public ClaimManager getClaimManager() {
		return claimManager;
	}

	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

}
