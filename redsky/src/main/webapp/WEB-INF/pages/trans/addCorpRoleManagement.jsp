<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Corp Role Management</title> 
<meta name="heading" content="<fmt:message key='roleBasedPermissionForm.heading'/>"/>

<style type="text/css">
.text-area {
border:1px solid #219DD1;
}
</style>
</head>

 <s:form id="roleCorpManagementForm" name="roleCorpManagementForm" action="saveRoleCorpManagementForm" method="post" validate="true">
 <s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
 <c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.rolePermissionList' }">
    <c:redirect url="/roleBasedPermissionList.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>

 <div id="Layer1" style="width:100%" onkeydown="changeStatus();"> 

		<div id="newmnav">
		  <ul>
		  	<li><a onclick="setReturnString('gototab.rolePermissionList');return autoSave();"><span>Corp ID Role</span></a></li>
		  	<!--<li><a href="roleBasedPermissionList.html"><span>Role Permission List</span></a></li>
		  	
		  	--><li id="newmnav1" style="background:#FFF "><a class="current"><span>Corp ID Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div>
	
<table class="" cellspacing="1" cellpadding="1" border="0" style="width:85%;margin:0px;">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
  	<table width="100%" border="0" cellpadding="5" cellspacing="0" class="detailTabLabel">
		  <tbody>  	
		  	 		<tr>
		  		<td width="10%" height="3" align="left"></td>
		  	</tr>
		  	<tr>
		  		<td align="right">Corp ID<font color="red" size="2">*</font></td>
	  		  <td width="19%" align="left" colspan="2"><s:select cssClass="list-menu"   name="role.corpID" list="%{companyCorpId}" headerKey="" headerValue="" cssStyle="width:120px" onchange="changeStatus()"/></td>
	  		
		  	  <td width="1%" align="left" rowspan="2" style="padding-top:30px;">Category</td>
		  	  <td align="left" valign="top" rowspan="3" style="margin:0px;padding:0px;"><s:select cssClass="list-menu" name="role.category" list="%{usertype}" value="%{selectedUsertype}" multiple="true" headerKey="" headerValue="" cssStyle="width:120px; height:80px" onchange="changeStatus()"/></td>
		  	  <td align="right">&nbsp;</td>
	  	    </tr>
		  	<tr>
		  	  <td align="right">Role Name<font color="red" size="2">*</font></td>
		  	  <td align="left"><s:textfield name="role.name" required="true" cssClass="input-text" size="60" /></td>
		  	  <td align="right">&nbsp;</td>
	  	    </tr>
		  	<tr>
		  		<td align="right" >Description</td>
		  		<td colspan="2" align="left"><s:textarea cssClass="textarea" name="role.description"  rows="4" cols="57" readonly="false"/></td>
		  	</tr>
		  	<tr><td height="15px"></td></tr>
  </tbody>
</table>
<s:hidden name="role.id" />
	 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	
</tr>   
		 
</div>
<table class="detailTabLabel" border="0">
		  <tbody> 			
		   <tr>
		  		<td align="left">
        			<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save"/>  
        		</td>
       
      		 	<td align="left">
        			<input type="button" class="cssbutton1" value="Cancel" onclick="cancelSetup();"/>  
        		</td>
       
        		<td align="right">
        			<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
       	  	</tr>		  	
		  </tbody>
		  </table>
</s:form>

<script type="text/javascript">

function autoSave(){
 if ('${autoSavePrompt}' == 'No'){
 
 if(document.forms['roleCorpManagementForm'].elements['gotoPageString'].value == 'gototab.rolePermissionList'){
		noSaveAction = 'corpRoleManagement.html';
	}
	processAutoSave(document.forms['roleCorpManagementForm'], 'saveRoleCorpManagementForm!saveOnTabChange.html', noSaveAction);
 }else
 {
 	if(document.forms['roleCorpManagementForm'].elements['gotoPageString'].value == 'gototab.rolePermissionList'){
		noSaveAction = 'corpRoleManagement.html';
	}
	processAutoSave(document.forms['roleCorpManagementForm'], 'saveRoleCorpManagementForm!saveOnTabChange.html', noSaveAction);
 }

}


function cancelSetup(){
	location.href = 'corpRoleManagement.html';
}
</script>