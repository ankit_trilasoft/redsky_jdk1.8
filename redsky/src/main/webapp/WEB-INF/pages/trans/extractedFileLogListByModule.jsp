<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title>Log List</title>
</head> 
<display:table name="fileListByModule" class="table" requestURI="" id="fileListByModule" pagesize="5" >   
		 <display:column property="createdBy" title="User Name" />
		 <display:column property="createdOn" title="File Processed On" format="{0,date,yyyy-MMM-dd HH:mm:ss}"/>		 
		 <display:column property="fileName" title="File Name" /> 
		 <display:column property="messageLog" title="Log Details"/>
</display:table>
