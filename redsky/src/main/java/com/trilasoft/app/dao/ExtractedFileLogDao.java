package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao; 

import com.trilasoft.app.model.ExtractedFileLog;

public interface ExtractedFileLogDao extends GenericDao<ExtractedFileLog, Long>  {
	List<ExtractedFileLog> searchExtractedFileLog(String sessionCorpID, String moduleName, String extractCreatedbyFirstName, String extractCreatedbyLastName, String fromExtractDate, String toExtractDate);
	public List getFileLogListByModule(String sessionCorpID,String moduleName);
}
