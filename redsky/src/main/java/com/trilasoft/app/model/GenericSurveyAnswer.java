package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="genericsurveyanswer")
public class GenericSurveyAnswer extends BaseObject{
	
	private Long id;
	private String corpId;
	private Integer sequenceNumber;
	private String answer;
	private boolean status= false;
	private Long surveyQuestionId;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpId", corpId)
				.append("sequenceNumber", sequenceNumber)
				.append("answer", answer)
				.append("status", status)
				.append("surveyQuestionId", surveyQuestionId)
				.append("createdBy", createdBy)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof GenericSurveyAnswer))
			return false;
		GenericSurveyAnswer castOther = (GenericSurveyAnswer) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(answer, castOther.answer)
				.append(status, castOther.status)
				.append(surveyQuestionId, castOther.surveyQuestionId)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id).append(corpId)
				.append(sequenceNumber).append(answer)
				.append(status).append(surveyQuestionId)
				.append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn)
				.toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Long getSurveyQuestionId() {
		return surveyQuestionId;
	}

	public void setSurveyQuestionId(Long surveyQuestionId) {
		this.surveyQuestionId = surveyQuestionId;
	}

}
