<%@ include file="/common/taglibs.jsp"%> 

<head>
	<meta name="heading" content="<fmt:message key='vanLineGLTypeForm.title'/>"/> 
	<title><fmt:message key="vanLineForm.title"/></title>
	
<style type="text/css">
		h2 {background-color: #FBBFFF}		
</style>
<script>
function saveAuto(clickType){

	if ('${autoSavePrompt}' == 'No'){
	
		var noSaveAction;
		if(document.forms['vanLineGLTypeForm'].elements['gotoPageString'].value == 'gototab.vanlinegl'){
			noSaveAction = 'vanLineGLTypeList.html';
		}
		processAutoSave(document.forms['vanLineGLTypeForm'],'saveVanLineGLType!saveOnTabChange.html', noSaveAction );
	
	}
	else{
     if(!(clickType == 'save')){
       var id = document.forms['vanLineGLTypeForm'].elements['vanLineGLType.id'].value;
	   if (document.forms['vanLineGLTypeForm'].elements['formStatus'].value == '1'){
	       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='vanLineForm.title'/>");
		       if(agree){
		           document.forms['vanLineGLTypeForm'].action = 'saveVanLineGLType!saveOnTabChange.html';
		           document.forms['vanLineGLTypeForm'].submit();
		       }else{
			      if(id != ''){
			           if(document.forms['vanLineGLTypeForm'].elements['gotoPageString'].value == 'gototab.vanlinegl'){
				               location.href = 'vanLineGLTypeList.html';
				         }
				     }
       			}
   			}else{
   				if(id != ''){
			  	   if(document.forms['vanLineGLTypeForm'].elements['gotoPageString'].value == 'gototab.vanlinegl'){
			               location.href = 'vanLineGLTypeList.html';
			       }
   				}
   			}
   		}
   	}		
  }


function changeStatus(){
   document.forms['vanLineGLTypeForm'].elements['formStatus'].value = '1';
}
function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
	}
function onlyAlphaNumericAllow(evt)	{
		var keyCode = evt.which ? evt.which : evt.keyCode;
		return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36);
}
function chkSelectFloatVal(temp)
{
        	if (checkFloatWithArth('vanLineGLTypeForm','vanLineGLType.discrepancy','Invalid data in Discrepancy Field') == false)  {
              document.forms['vanLineGLTypeForm'].elements['vanLineGLType.discrepancy'].value='0.00';
              return false;
            }
        	if (checkFloatWithArth('vanLineGLTypeForm','vanLineGLType.shortHaul','Invalid data in ShortHaul Field') == false)  {
              document.forms['vanLineGLTypeForm'].elements['vanLineGLType.shortHaul'].value='';
              return false;
            }
/*            var i;
			var s = document.forms['vanLineGLTypeForm'].elements['vanLineGLType.glType'].value;
			var count = 0;
		    for (i = 0; i < s.length; i++)
		    {   
		        var c = s.charAt(i);
	        
			      if (c == '%27') 
			        {
		        	alert("Enter valid GLType without quote");
			        document.forms['vanLineGLTypeForm'].elements['vanLineGLType.glType'].value="";
			        return false;
			        }
		    }*/
            if(temp=='autoSave'){
			  saveAuto('none');
			}
}
function checktextArea(){
	var formula=document.forms['vanLineGLTypeForm'].elements['vanLineGLType.calc'].value
	formula=formula.trim();
	if(formula=="")
		{
	  		alert('Please enter the formula in the text Area');
	  		return false;
	  		
    	 }
    	 testChargeFormula();
	}
	
function testChargeFormula(){
     var expressionFormula=document.forms['vanLineGLTypeForm'].elements['vanLineGLType.calc'].value;
	 var url="testFormulaForVanLine.html?ajax=1&decorator=simple&popup=true&expressionFormula=" +encodeURI(expressionFormula);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponsetestFormula;
     http2.send(null);
	}	
	
function handleHttpResponsetestFormula()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                alert(results);
             }
        }
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
</script>

<style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}

</style>

</head>
<body style="background-color:#444444;">
<s:form id="vanLineGLTypeForm" name="vanLineGLTypeForm" action="saveVanLineGLType" method="post" validate="true" onsubmit="return chkSelectFloatVal('noSave');">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="vanLineGLType.id" value="%{vanLineGLType.id}"/>

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
	<c:choose>
		<c:when test="${gotoPageString == 'gototab.vanlinegl'}">
		   <c:redirect url="/vanLineGLTypeList.html"/>
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose> 
</c:if>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%" >
<div id="newmnav">
		  <ul>
			    <li><a onclick="setReturnString('gototab.vanlinegl');return chkSelectFloatVal('autoSave');"><span>GL Type List</span></a></li>
			    <li id="newmnav1" style="background:#FFF "><a class="current"><span>GL Type Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			   	<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${vanLineGLType.id}&tableName=vanlinegltype&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
	  	  </ul>
</div><div class="spn">&nbsp;</div>
<div style="padding-bottom:2px;!padding-bottom:5px;"></div>
<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:0px;!margin-top:0px;"><span></span></div>
<div class="center-content">	
 <table class="" cellspacing="1" cellpadding="1" border="0"  >
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" border="0">
		  <tbody> 	  	
		  	<tr>
		  		<td align="right" width="30px">
		  	   	<td align="right"><fmt:message key="vanLineGLType.glType"/></td>
		  		<td align="left" colspan="4">
		  		<c:if test="${empty vanLineGLType.id}">
		  			<s:textfield name="vanLineGLType.glType"  size="20"   cssClass="input-text" cssStyle="width:150px" tabindex="1" onkeydown="return onlyAlphaNumericAllowed(event,this,'special');"/>
		  		</c:if>	
		  		<c:if test="${not empty vanLineGLType.id}">
		  			<s:textfield name="vanLineGLType.glType"  size="20"   cssClass="input-textUpper" cssStyle="width:150px" tabindex="1"  readonly="true"/>
		  		</c:if>	
		  		</td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px">
		  		<td align="right"><fmt:message key="vanLineGLType.description"/></td>
		  		<td align="left" colspan="4"><s:textfield name="vanLineGLType.description"  size="50"  cssClass="input-text" cssStyle="width:300px" tabindex="2"/></td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px">
		  	   	<td align="right" ><fmt:message key="vanLineGLType.glCode"/></td>
		  		<td align="left" colspan="4"><s:select cssClass="list-menu" name="vanLineGLType.glCode" headerKey=" " headerValue=" " list="%{glCode}" cssStyle="width:300px" onchange="changeStatus();" tabindex="3"/></td> 
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px">
		  		<td></td>
		  		<td align="left" width="5px"><s:checkbox key="vanLineGLType.doNotAuto" fieldValue="true" onclick="changeStatus();" tabindex="4"/></td>
		  		<td align="left" width="190px;"><fmt:message key="vanLineGLType.doNotAuto"/></td>
		  		<td align="right" width=""><s:checkbox key="vanLineGLType.calcByDrvr" fieldValue="true" onclick="changeStatus();" tabindex="5"/></td>
		  		<td align="left"><fmt:message key="vanLineGLType.calcByDrvr"/></td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px">
		  		<td></td>
		  		<td align="left"><s:checkbox key="vanLineGLType.putGL" fieldValue="true" onclick="changeStatus();" tabindex="6"/></td>
		  		<td align="left" ><fmt:message key="vanLineGLType.putGL"/></td>
		  		<td align="right" width=""><s:checkbox key="vanLineGLType.useDrvrGL" fieldValue="true" onclick="changeStatus();" tabindex="7"/></td>
		  		<td align="left"><fmt:message key="vanLineGLType.useDrvrGL"/></td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px">
		  	   	<td align="right"><fmt:message key="vanLineGLType.discrepancy"/></td>
		  		<td align="left" colspan="2" ><s:textfield name="vanLineGLType.discrepancy"  size="20" cssClass="input-text" cssStyle="width:150px" tabindex="8" onkeydown="return onlyFloatNumsAllowed(event)" maxlength="11" /></td>
		  	   	<td align="right"><fmt:message key="vanLineGLType.shortHaul"/></td>
		  		<td align="left"><s:textfield name="vanLineGLType.shortHaul"  size="20"   cssClass="input-text" cssStyle="width:150px" tabindex="9" onkeydown="return onlyFloatNumsAllowed(event)" maxlength="11"/></td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px">
		  		<td align="right"><fmt:message key="vanLineGLType.bucket"/></td>
		  		<td align="left" colspan="2"><s:textfield name="vanLineGLType.bucket"  size="50"  cssClass="input-text" cssStyle="width:150px" tabindex="10"/></td>
		  		<td align="right"><fmt:message key="vanLineGLType.invItem"/></td>
		  		<td align="left"><s:textfield name="vanLineGLType.invItem"  size="50"  cssClass="input-text" cssStyle="width:150px" tabindex="11"/></td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px">
		  		<td align="right" valign="top"><fmt:message key="vanLineGLType.calc"/></td>
		  		<td align="left" colspan="4" ><s:textarea name="vanLineGLType.calc" cssStyle="!width:430px;width:418px;" rows="10" tabindex="12" cssClass="textarea"/></td>
		  		<td align="left" class="listwhitetext"><input type="button" class="cssbutton" name="TestFormula" value="Test Formula" style="width: 100px;height: 30px" onclick="checktextArea();" tabindex="13"/></td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	
		  	<tr>
		  		<td align="left" height="10px"></td>
		  	</tr>
		</tbody>
	</table> 
	<!--  <td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr> -->  
	
 	
	
	</tbody>

	
  	</table>
  		</div>
<div class="bottom-header"><span></span></div>
</div>
</div>		
</div>
<table class="detailTabLabel" border="0">
	<tr>
		<td>
	 		<table>
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='vanLineGLType.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="vanLineGLTypeCreatedOnFormattedValue" value="${vanLineGLType.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="vanLineGLType.createdOn" value="${vanLineGLTypeCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${vanLineGLType.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='vanLineGLType.createdBy' /></b></td>
							<c:if test="${not empty vanLineGLType.id}">
								<s:hidden name="vanLineGLType.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{vanLineGLType.createdBy}"/></td>
							</c:if>
							<c:if test="${empty vanLineGLType.id}">
								<s:hidden name="vanLineGLType.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='vanLineGLType.updatedOn'/></b></td>
							<fmt:formatDate var="vanLineGLTypeupdatedOnFormattedValue" value="${vanLineGLType.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="vanLineGLType.updatedOn" value="${vanLineGLTypeupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${vanLineGLType.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='vanLineGLType.updatedBy' /></b></td>
							<c:if test="${not empty vanLineGLType.id}">
								<s:hidden name="vanLineGLType.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{vanLineGLType.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty vanLineGLType.id}">
								<s:hidden name="vanLineGLType.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
		</td>
	</tr>
	</table>
<table>
		<tbody> 	
			<tr>
			  	<td align="left" height="3px"></td>
			</tr>	  	
			<tr>
			  	<td align="left"><s:submit cssClass="cssbutton1" type="button" method="save" key="button.save"/></td>
	       		<td align="right"><s:reset cssClass="cssbutton1"  type="button" key="Reset"/></td>
	        </tr>		  	
	</tbody> 
	</table>
</s:form>


		