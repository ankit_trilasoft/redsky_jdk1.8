package com.trilasoft.app.webapp.action;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import static java.lang.System.out;
import org.apache.commons.lang.StringEscapeUtils;

import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Contract;
import com.trilasoft.app.model.CountryDeviation;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.Discount;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.model.ResourceGrid;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ContractManager;
import com.trilasoft.app.service.CountryDeviationManager;
import com.trilasoft.app.service.DiscountManager;
import com.trilasoft.app.service.ExpressionManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.RateGridManager;
import com.trilasoft.app.service.RefMasterManager;

public class ChargesAction extends BaseAction implements Preparable {

	private String sessionCorpID;
	private ChargesManager chargesManager; 
	private ContractManager contractManager;
	private CountryDeviationManager countryDeviationManager;
	private List refMasters;
	private RefMasterManager refMasterManager;
	private List chargess;
	private List charge;
	private List charges1;
	private List ChargeCodeList;
	private  List price1;
    private  List quantity1;
    private  List extra1;
	private Charges charges;
	private Charges copyCharge;
	private Charges chargesCopy;
	private Contract contracts;
	private Long id;
	private Long sid;
	private Long cid;
	private Long i;
	private List glcodes;
	private Long chagneId;
	private String hitFlag;
	
	private RateGridManager rateGridManager;
	private RateGrid rateGrid;
	private Map<String, String> workDays;
	private int rateListSize;
	private String rateGridCorpId;
	public String chargeOrgId; 
	private Company company;
	private CompanyManager companyManager;
	private Boolean chargeDiscountFlag=false;
	private Long soIdNum;
	private Boolean status;
	private Boolean quotescharges=false;
	public int getRateListSize() {
		return rateListSize;
	}

	public void setRateListSize(int rateListSize) {
		this.rateListSize = rateListSize;
	}
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(ChargesAction.class);

	
	public ChargesAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
     	}
	private Boolean vatCalc;
	public Boolean getVatCalc() {
		return vatCalc;
	}

	public void setVatCalc(Boolean vatCalc) {
		this.vatCalc = vatCalc;
	}

	public void prepare() throws Exception {
		chargeOrgId=sessionCorpID;
		costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
		List vatFlag=chargesManager.findVatCalculationForCharge(sessionCorpID);
		Boolean flag=false;
		if((vatFlag!=null)&&(!vatFlag.isEmpty()))
		{
			String flag1=vatFlag.get(0).toString();
			if(flag1.equalsIgnoreCase("true"))
			{
				flag=true;
			}
		}
		vatCalc=flag;
		company=companyManager.findByCorpID(sessionCorpID).get(0);	
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: company "+company);
		if(company!=null){
			 if((company.getChargeDiscountSetting()!=null)&&(company.getChargeDiscountSetting())){
				 chargeDiscountFlag = true;
				 }
			}
	}

	public void setChargesManager(ChargesManager chargesManager) {
		this.chargesManager = chargesManager;
	}

  
    public void setSid(Long sid) {
		this.sid = sid;
	}


	public List getChargess() {
		return chargess;
	}
	public List getCharge() {
		return charge;
	}
	private Billing billing;
	private BillingManager billingManager;
	private MiscellaneousManager miscellaneousManager;
	private String contract;
	public String copyAgentAccount;
	private String accountCompanyDivision;
	private Map<String, String> quantityField;
	private Map<String, String> priceField;
	private Map<String, String> tariffField;
	private List<RateGrid> rateGridList;
	private List<CountryDeviation> countryDeviationList;
	private String chid;
	private String contractName;
	private String chargeCode;
	private Map<String, String> discount;
	private Map<String, String> costLevel;
	private  Map<String,String>basis;
	private Map<String, String> storageType;
	private Map<String, String> GSTHSNCodeMap= new LinkedHashMap<String, String>(); 
	private int countCharges;
	private static Map<String, String> contact_frequency;
	private Map<String, String> ocountry;
	private Map<String, String> dcountry;
	private List mode;
	private String originCountry;
	private String destinationCountry;
	private String serviceMode;
	private Map<String, String> twoDType;
	private String onSiteStatusCheck;
	private String onSiteRateCheck;
    private String onSiteQuantity2Check;
	private String twoDG;
	private List exportRateList; 
	public int rateGridCount;
	private Map<String, String> deviationTypeList;
	private Map<String, String> commissionDrop;
	private String onSiteBuyRateCheck;
	private  Map<String,String>currency;
	 private String contractId;
	 private String routing;
	 private String jobType;
	 private String  soCompanyDivision="";

	public String getOnSiteBuyRateCheck() {
		return onSiteBuyRateCheck;
	}

	public void setOnSiteBuyRateCheck(String onSiteBuyRateCheck) {
		this.onSiteBuyRateCheck = onSiteBuyRateCheck;
	}

	public String getContract() {
		return contract;
	}


	public void setContract(String contract) {
		this.contract = contract;
	}


	public Billing getBilling() {
		return billing;
	}


	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}  
    private Boolean costElementFlag;
	public String getComboList(String corpId) { 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//System.out.println("SUNIL");
		//glcodes = refMasterManager.findByParameter(corpId, "GLCODES");
		GSTHSNCodeMap=refMasterManager.findByParameter(corpId,"GSTHSN");
		storageType=refMasterManager.findByParameter(corpId,"STORAGE_TYPE");
		glcodes=refMasterManager.findOnlyCode("GLCODES", corpId);
		quantityField=chargesManager.findQuantityField(sessionCorpID,"quantity");
		priceField=chargesManager.findPriceField(sessionCorpID,"price");
		tariffField=refMasterManager.findByParameter(corpId, "TARIFF");
		discount=refMasterManager.findByParameter(corpId, "DISCOUNT");
		costLevel=refMasterManager.findByParameter(corpId, "COSTLEVEL");
		basis=refMasterManager.findByParameter(corpId, "ACC_BASIS");
		contact_frequency = refMasterManager.findByParameter(corpId, "SETTLEMENT_FREQUENCY");
		ocountry = refMasterManager.findByParameter(corpId, "COUNTRY");
	 	dcountry = refMasterManager.findByParameter(corpId, "COUNTRY");
	 	mode = refMasterManager.findByParameters(corpId, "MODE");
	 	twoDType=refMasterManager.findByParameter(corpId,"2DUNIT");
	 	deviationTypeList=refMasterManager.findByParameter(corpId,"DeviationType");
	 	costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
	 	currency=refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
		price1 = new ArrayList();
		price1.add("Ask user?");
		price1.add("Preset");
		price1.add("From Field");
		price1.add("From Tarriff");
		price1.add("Build Formula");
	  	
		quantity1 = new ArrayList();
		quantity1.add("Ask user?");
		quantity1.add("Preset");
		quantity1.add("From Field");
		quantity1.add("Get total of lines");
		
		extra1 = new ArrayList();
		extra1.add("Ask user?");
		extra1.add("Preset");
		extra1.add("From Field");
		
		workDays = new LinkedHashMap<String, String>();
		workDays.put("1", "Sun");
		workDays.put("2", "Mon");
		workDays.put("3", "Tue");
		workDays.put("4", "Wed");
		workDays.put("5", "Thu");
		workDays.put("6", "Fri");
		workDays.put("7", "Sat");
		
		
		commissionDrop = new LinkedHashMap<String, String>();
		commissionDrop.put("All", "All");
		commissionDrop.put("true", "Yes");
		commissionDrop.put("false", "No");
		
		//chargess = chargesManager.getAll(); 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End"); 
        return SUCCESS;   
    }
	public  List getPrice1() {
		return price1;
	}
    public  List getQuantity1() {
		return quantity1;
	}
    public  List getExtra1() {
		return extra1;
	}
    private String categoryType;
	public String list() 
	{   		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(categoryType==null){
			categoryType="";
		}else{
			 if(!costElementFlag)	{
				 categoryType="";
			 }
		}
		try{
		chargess = chargesManager.findForContractChargeList(contract, sessionCorpID,originCountry,destinationCountry,serviceMode,categoryType);
		}catch(Exception e){
			logger.warn(e);
			chargess=null;
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;   
    }
	public String internalCostChargeList() 
	{
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	try{
	chargess = chargesManager.findForInternalCostChargeList(accountCompanyDivision, sessionCorpID,originCountry,destinationCountry,serviceMode);
	}catch(Exception e){
		logger.warn(e);
		chargess = null;
	}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");    
	return SUCCESS;   
    }
	
	
	public String defaultTemplateList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		chargess = chargesManager.findDefaultTemplateContract(contract, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End"); 
		return SUCCESS;   
	}
	private String soId;
	private String wordingTemp;
	public String getWordingTemp() {
		return wordingTemp;
	}

	public void setWordingTemp(String wordingTemp) {
		this.wordingTemp = wordingTemp;
	}

	public String getSoId() {
		return soId;
	}

	public void setSoId(String soId) {
		this.soId = soId;
	}
	@SkipValidation
	public String populateCostElementDataAjax(){
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		  if((soId==null)||(soId.toString().trim().equalsIgnoreCase(""))){
			soId="";
		  }
		  wordingTemp="";
		  try {
			if(!soId.equalsIgnoreCase("")){
				  wordingTemp=chargesManager.findPopulateCostElementData(chargeCode, contract,sessionCorpID);
			  }
		} catch (Exception e) {
			e.printStackTrace();
		}
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		  return SUCCESS;
	}
	public String checkChargesDataAjax(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if((soId==null)||(soId.toString().trim().equalsIgnoreCase(""))){
			soId="";
		}
		String wording="";
		  if(!soId.equalsIgnoreCase("")){
			  List al=chargesManager.findChargeId(chargeCode, contract);
			  if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().equalsIgnoreCase(""))){
				  Charges charge=chargesManager.get(Long.parseLong(al.get(0).toString()));
				  Billing billing=billingManager.get(Long.parseLong(soId));
				  Miscellaneous miscellaneous=miscellaneousManager.get(Long.parseLong(soId));
				  if((charge.getWording()!=null)&&(!charge.getWording().toString().trim().equalsIgnoreCase(""))){
					  try {

							Map wordingValues = new HashMap();
							if (billing.getOnHand() != null) {
								wordingValues.put("billing.onHand", new BigDecimal(billing.getOnHand().toString()));
							} else {
								wordingValues.put("billing.onHand", 0.00);
							}
							if (charge.getPerValue() != null) {
								wordingValues.put("charges.perValue", new BigDecimal(charge.getPerValue().toString()));
							} else {
								wordingValues.put("charges.perValue", 0.00);
							}
							if (billing.getCycle() != 0 ) {
								String cy=billing.getCycle()+"";
								wordingValues.put("billing.cycle", new BigDecimal(cy));
							} else {
								wordingValues.put("billing.cycle", 0.00);
							}
							if (billing.getInsuranceRate() != null) {
								wordingValues.put("billing.insuranceRate", (BigDecimal) billing.getInsuranceRate());
							} else {
								wordingValues.put("billing.insuranceRate", 0.00);
							}
							if (billing.getInsuranceValueActual() != null) {
								wordingValues.put("billing.insuranceValueActual", (BigDecimal) billing.getInsuranceValueActual());
							} else {
								wordingValues.put("billing.insuranceValueActual", 0.00);
							}
							if (billing.getBaseInsuranceValue() != null) {
								wordingValues.put("billing.baseInsuranceValue", (BigDecimal) billing.getBaseInsuranceValue());
							} else {
								wordingValues.put("billing.baseInsuranceValue", 0.00);
							}
							if (charge.getMinimum() != null) {
								wordingValues.put("charges.minimum", (BigDecimal) charge.getMinimum());
							} else {
								wordingValues.put("charges.minimum", 0.00);
							}
							if (billing.getPostGrate() != null) {
								wordingValues.put("billing.postGrate", (BigDecimal) billing.getPostGrate().setScale(2,BigDecimal.ROUND_HALF_UP));
							} else {
								wordingValues.put("billing.postGrate", 0.00000);
							}
							if (charge.getDescription() != null) {
								wordingValues.put("charges.description", charge.getDescription().toString());
							} else {
								wordingValues.put("charges.description", "");
							}
							if (charge.getUseDiscount() != null) {
								wordingValues.put("charges.useDiscount", charge.getUseDiscount().toString());
							} else {
								wordingValues.put("charges.useDiscount", "");
							}
							if (charge.getPricePre() != 0.00) {
								wordingValues.put("charges.pricepre", charge.getPricePre());
							} else {
								wordingValues.put("charges.pricepre", "");
							}
							if (charge.getPerItem() != null) {
								wordingValues.put("form.qty", charge.getPerItem().toString());
							} else {
								wordingValues.put("form.qty", 0.0);
							}
							
							if(miscellaneous.getActualNetWeight()!=null){
								wordingValues.put("miscellaneous.actualNetWeight", new BigDecimal(miscellaneous.getActualNetWeight().toString()));
							}else{
								wordingValues.put("miscellaneous.actualNetWeight", 0.00);								
							}
							if(miscellaneous.getActualGrossWeight()!=null){
								wordingValues.put("miscellaneous.actualGrossWeight", new BigDecimal(miscellaneous.getActualGrossWeight().toString()));								
							}else{
								wordingValues.put("miscellaneous.actualGrossWeight", 0.00);
							}
							if(miscellaneous.getEstimateGrossWeight()!=null){
								wordingValues.put("miscellaneous.estimateGrossWeight", new BigDecimal(miscellaneous.getEstimateGrossWeight().toString()));								
							}else{
								wordingValues.put("miscellaneous.estimateGrossWeight", 0.00);
							}
							if(miscellaneous.getEstimatedNetWeight()!=null){
								wordingValues.put("miscellaneous.estimatedNetWeight", new BigDecimal(miscellaneous.getEstimatedNetWeight().toString()));								
							}else{
								wordingValues.put("miscellaneous.estimatedNetWeight", 0.00);
							}
							if(miscellaneous.getEstimateGrossWeightKilo()!=null){
								wordingValues.put("miscellaneous.estimateGrossWeightKilo", new BigDecimal(miscellaneous.getEstimateGrossWeightKilo().toString()));								
							}else{
								wordingValues.put("miscellaneous.estimateGrossWeightKilo", 0.00);
							}
							if(miscellaneous.getEstimatedNetWeightKilo()!=null){
								wordingValues.put("miscellaneous.estimatedNetWeightKilo", new BigDecimal(miscellaneous.getEstimatedNetWeightKilo().toString()));								
							}else{
								wordingValues.put("miscellaneous.estimatedNetWeightKilo", 0.00);
							}
							if(miscellaneous.getChargeableGrossWeight()!=null){
								wordingValues.put("miscellaneous.chargeableGrossWeight", new BigDecimal(miscellaneous.getChargeableGrossWeight().toString()));								
							}else{
								wordingValues.put("miscellaneous.chargeableGrossWeight", 0.00);
							}
							if(miscellaneous.getChargeableNetWeight()!=null){
								wordingValues.put("miscellaneous.chargeableNetWeight", new BigDecimal(miscellaneous.getChargeableNetWeight().toString()));								
							}else{
								wordingValues.put("miscellaneous.chargeableNetWeight", 0.00);
							}
							if(miscellaneous.getEntitleCubicFeet()!=null){
       							wordingValues.put("miscellaneous.entitleCubicFeet", new BigDecimal(miscellaneous.getEntitleCubicFeet().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleCubicFeet", 0.00);
       						}
							if(miscellaneous.getActualCubicFeet()!=null){
								wordingValues.put("miscellaneous.actualCubicFeet", new BigDecimal(miscellaneous.getActualCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.actualCubicFeet", 0.00);
							}
							   if(miscellaneous.getNetActualCubicFeet()!=null){
									wordingValues.put("miscellaneous.netActualCubicFeet", new BigDecimal(miscellaneous.getNetActualCubicFeet().toString()));								
								}else{
									wordingValues.put("miscellaneous.netActualCubicFeet", 0.00);
								}
							if(miscellaneous.getActualNetWeightKilo()!=null){
								wordingValues.put("miscellaneous.actualNetWeightKilo", new BigDecimal(miscellaneous.getActualNetWeightKilo().toString()));								
							}else{
								wordingValues.put("miscellaneous.actualNetWeightKilo", 0.00);
							}
							if(miscellaneous.getActualGrossWeightKilo()!=null){
								wordingValues.put("miscellaneous.actualGrossWeightKilo", new BigDecimal(miscellaneous.getActualGrossWeightKilo().toString()));								
							}else{
								wordingValues.put("miscellaneous.actualGrossWeightKilo", 0.00);
							}
							if(miscellaneous.getActualCubicMtr()!=null){
								wordingValues.put("miscellaneous.actualCubicMtr", new BigDecimal(miscellaneous.getActualCubicMtr().toString()));								
							}else{
								wordingValues.put("miscellaneous.actualCubicMtr", 0.00);
							}
							if(miscellaneous.getNetActualCubicMtr()!=null){
								wordingValues.put("miscellaneous.netActualCubicMtr", new BigDecimal(miscellaneous.getNetActualCubicMtr().toString()));								
							}else{
								wordingValues.put("miscellaneous.netActualCubicMtr", 0.00);
							}
							
							if(miscellaneous.getEstimateCubicFeet()!=null){
								wordingValues.put("miscellaneous.estimateCubicFeet", new BigDecimal(miscellaneous.getEstimateCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.estimateCubicFeet", 0.00);
							}
							if(miscellaneous.getNetEstimateCubicFeet()!=null){
								wordingValues.put("miscellaneous.netEstimateCubicFeet", new BigDecimal(miscellaneous.getNetEstimateCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.netEstimateCubicFeet", 0.00);
							}
							if(miscellaneous.getEstimateCubicMtr()!=null){
								wordingValues.put("miscellaneous.estimateCubicMtr", new BigDecimal(miscellaneous.getEstimateCubicMtr().toString()));								
							}else{
								wordingValues.put("miscellaneous.estimateCubicMtr", 0.00);
							}
							if(miscellaneous.getNetEstimateCubicMtr()!=null){
								wordingValues.put("miscellaneous.netEstimateCubicMtr", new BigDecimal(miscellaneous.getNetEstimateCubicMtr().toString()));								
							}else{
								wordingValues.put("miscellaneous.netEstimateCubicMtr", 0.00);
							}
							if(miscellaneous.getChargeableCubicFeet()!=null){
								wordingValues.put("miscellaneous.chargeableCubicFeet", new BigDecimal(miscellaneous.getChargeableCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.chargeableCubicFeet", 0.00);
							}
							if(miscellaneous.getChargeableNetCubicFeet()!=null){
								wordingValues.put("miscellaneous.chargeableNetCubicFeet", new BigDecimal(miscellaneous.getChargeableNetCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.chargeableNetCubicFeet", 0.00);
							}
							if(miscellaneous.getChargeableCubicMtr()!=null){
								wordingValues.put("miscellaneous.chargeableCubicMtr", new BigDecimal(miscellaneous.getChargeableCubicMtr().toString()));								
							}else{
								wordingValues.put("miscellaneous.chargeableCubicMtr", 0.00);
							}
							if(miscellaneous.getChargeableNetCubicMtr()!=null){
								wordingValues.put("miscellaneous.chargeableNetCubicMtr", new BigDecimal(miscellaneous.getChargeableNetCubicMtr().toString()));								
							}else{
								wordingValues.put("miscellaneous.chargeableNetCubicMtr", 0.00);
							}
							
     						if(miscellaneous.getActualAutoWeight()!=null){
     							wordingValues.put("miscellaneous.actualAutoWeight", new BigDecimal(miscellaneous.getActualAutoWeight().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.actualAutoWeight", 0.00);
     						}	
     						if(miscellaneous.getActualAutoWeight()!=null){
     							wordingValues.put("miscellaneous.actualAutoWeight", new BigDecimal(miscellaneous.getActualAutoWeight().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.actualAutoWeight", 0.00);
     						}	
     						if(billing.getPackDiscount()!=null){
     							wordingValues.put("billing.packDiscount", new BigDecimal(billing.getPackDiscount().toString()));								
     						}else{
     							wordingValues.put("billing.packDiscount", 0.00);
     						}
     						if(billing.getStorageMeasurement()!=null){
     							wordingValues.put("billing.storageMeasurement", billing.getStorageMeasurement().toString());								
     						}else{
     							wordingValues.put("billing.storageMeasurement", "");
     						}
     						if(miscellaneous.getChargeableGrossWeightKilo()!=null){
     							wordingValues.put("miscellaneous.chargeableGrossWeightKilo", new BigDecimal(miscellaneous.getChargeableGrossWeightKilo().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.chargeableGrossWeightKilo", 0.00);
     						}
     						if(miscellaneous.getEntitleGrossWeight()!=null){
     							wordingValues.put("miscellaneous.entitleGrossWeight", new BigDecimal(miscellaneous.getEntitleGrossWeight().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.entitleGrossWeight", 0.00);
     						}
     						if(miscellaneous.getEntitleTareWeight()!=null){
     							wordingValues.put("miscellaneous.entitleTareWeight", new BigDecimal(miscellaneous.getEntitleTareWeight().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.entitleTareWeight", 0.00);
     						}
     						if(miscellaneous.getEstimateTareWeight()!=null){
     							wordingValues.put("miscellaneous.estimateTareWeight", new BigDecimal(miscellaneous.getEstimateTareWeight().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.estimateTareWeight", 0.00);
     						}
     						if(miscellaneous.getActualTareWeight()!=null){
     							wordingValues.put("miscellaneous.actualTareWeight", new BigDecimal(miscellaneous.getActualTareWeight().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.actualTareWeight", 0.00);
     						}
     						if(miscellaneous.getChargeableTareWeight()!=null){
     							wordingValues.put("miscellaneous.chargeableTareWeight", new BigDecimal(miscellaneous.getChargeableTareWeight().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.chargeableTareWeight", 0.00);
     						}
     						if(miscellaneous.getEntitleNetWeight()!=null){
     							wordingValues.put("miscellaneous.entitleNetWeight", new BigDecimal(miscellaneous.getEntitleNetWeight().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.entitleNetWeight", 0.00);
     						}
     						if(miscellaneous.getEntitleCubicFeet()!=null){
     							wordingValues.put("miscellaneous.entitleCubicFeet", new BigDecimal(miscellaneous.getEntitleCubicFeet().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.entitleCubicFeet", 0.00);
     						}
     						if(miscellaneous.getNetEntitleCubicFeet()!=null){
     							wordingValues.put("miscellaneous.netEntitleCubicFeet", new BigDecimal(miscellaneous.getNetEntitleCubicFeet().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.netEntitleCubicFeet", 0.00);
     						}
     						if(miscellaneous.getRwghGross()!=null){
     							wordingValues.put("miscellaneous.rwghGross", new BigDecimal(miscellaneous.getRwghGross().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.rwghGross", 0.00);
     						}
     						if(miscellaneous.getRwghTare()!=null){
     							wordingValues.put("miscellaneous.rwghTare", new BigDecimal(miscellaneous.getRwghTare().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.rwghTare", 0.00);
     						}
     						if(miscellaneous.getRwghNet()!=null){
     							wordingValues.put("miscellaneous.rwghNet", new BigDecimal(miscellaneous.getRwghNet().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.rwghNet", 0.00);
     						}
     						if(miscellaneous.getRwghCubicFeet()!=null){
     							wordingValues.put("miscellaneous.rwghCubicFeet", new BigDecimal(miscellaneous.getRwghCubicFeet().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.rwghCubicFeet", 0.00);
     						}
     						if(miscellaneous.getRwghNetCubicFeet()!=null){
     							wordingValues.put("miscellaneous.rwghNetCubicFeet", new BigDecimal(miscellaneous.getRwghNetCubicFeet().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.rwghNetCubicFeet", 0.00);
     						}
     						if(miscellaneous.getEntitleGrossWeightKilo()!=null){
     							wordingValues.put("miscellaneous.entitleGrossWeightKilo", new BigDecimal(miscellaneous.getEntitleGrossWeightKilo().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.entitleGrossWeightKilo", 0.00);
     						}
     						if(miscellaneous.getEntitleTareWeightKilo()!=null){
     							wordingValues.put("miscellaneous.entitleTareWeightKilo", new BigDecimal(miscellaneous.getEntitleTareWeightKilo().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.entitleTareWeightKilo", 0.00);
     						}
     						if(miscellaneous.getEstimateTareWeightKilo()!=null){
     							wordingValues.put("miscellaneous.estimateTareWeightKilo", new BigDecimal(miscellaneous.getEstimateTareWeightKilo().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.estimateTareWeightKilo", 0.00);
     						}
     						if(miscellaneous.getActualTareWeightKilo()!=null){
     							wordingValues.put("miscellaneous.actualTareWeightKilo", new BigDecimal(miscellaneous.getActualTareWeightKilo().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.actualTareWeightKilo", 0.00);
     						}
     						if(miscellaneous.getChargeableTareWeightKilo()!=null){
     							wordingValues.put("miscellaneous.chargeableTareWeightKilo", new BigDecimal(miscellaneous.getChargeableTareWeightKilo().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.chargeableTareWeightKilo", 0.00);
     						}
     						if(miscellaneous.getEntitleNetWeightKilo()!=null){
     							wordingValues.put("miscellaneous.entitleNetWeightKilo", new BigDecimal(miscellaneous.getEntitleNetWeightKilo().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.entitleNetWeightKilo", 0.00);
     						}
     						if(miscellaneous.getEntitleCubicMtr()!=null){
     							wordingValues.put("miscellaneous.entitleCubicMtr", new BigDecimal(miscellaneous.getEntitleCubicMtr().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.entitleCubicMtr", 0.00);
     						}
     						if(miscellaneous.getNetEntitleCubicMtr()!=null){
     							wordingValues.put("miscellaneous.netEntitleCubicMtr", new BigDecimal(miscellaneous.getNetEntitleCubicMtr().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.netEntitleCubicMtr", 0.00);
     						}
     						if(miscellaneous.getRwghGrossKilo()!=null){
     							wordingValues.put("miscellaneous.rwghGrossKilo", new BigDecimal(miscellaneous.getRwghGrossKilo().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.rwghGrossKilo", 0.00);
     						}
     						if(miscellaneous.getRwghTareKilo()!=null){
     							wordingValues.put("miscellaneous.rwghTareKilo", new BigDecimal(miscellaneous.getRwghTareKilo().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.rwghTareKilo", 0.00);
     						}
     						if(miscellaneous.getRwghNetKilo()!=null){
     							wordingValues.put("miscellaneous.rwghNetKilo", new BigDecimal(miscellaneous.getRwghNetKilo().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.rwghNetKilo", 0.00);
     						}
     						if(miscellaneous.getRwghCubicMtr()!=null){
     							wordingValues.put("miscellaneous.rwghCubicMtr", new BigDecimal(miscellaneous.getRwghCubicMtr().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.rwghCubicMtr", 0.00);
     						}
     						if(miscellaneous.getRwghNetCubicMtr()!=null){
     							wordingValues.put("miscellaneous.rwghNetCubicMtr", new BigDecimal(miscellaneous.getRwghNetCubicMtr().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.rwghNetCubicMtr", 0.00);
     						}
     						if(miscellaneous.getChargeableNetWeightKilo()!=null){
     							wordingValues.put("miscellaneous.chargeableNetWeightKilo", new BigDecimal(miscellaneous.getChargeableNetWeightKilo().toString()));								
     						}else{
     							wordingValues.put("miscellaneous.chargeableNetWeightKilo", 0.00);
     						}
     						
       						if(miscellaneous.getEntitleNumberAuto()!=null){
       							wordingValues.put("miscellaneous.entitleNumberAuto", new BigDecimal(miscellaneous.getEntitleNumberAuto().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleNumberAuto", 0.00);
       						}
       						if(miscellaneous.getEntitleAutoWeight()!=null){
       							wordingValues.put("miscellaneous.entitleAutoWeight", new BigDecimal(miscellaneous.getEntitleAutoWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleAutoWeight", 0.00);
       						}
       						if(miscellaneous.getEstimateAuto()!=null){
       							wordingValues.put("miscellaneous.estimateAuto", new BigDecimal(miscellaneous.getEstimateAuto().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.estimateAuto", 0.00);
       						}
       						if(miscellaneous.getEstimatedAutoWeight()!=null){
       							wordingValues.put("miscellaneous.estimatedAutoWeight", new BigDecimal(miscellaneous.getEstimatedAutoWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.estimatedAutoWeight", 0.00);
       						}
       						if(miscellaneous.getActualAuto()!=null){
       							wordingValues.put("miscellaneous.actualAuto", new BigDecimal(miscellaneous.getActualAuto().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.actualAuto", 0.00);
       						}
       						
       						if(miscellaneous.getEntitleNumberAuto()!=null){
       							wordingValues.put("miscellaneous.entitleNumberAuto", new BigDecimal(miscellaneous.getEntitleNumberAuto().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleNumberAuto", 0.00);
       						}
       						if(miscellaneous.getEntitleAutoWeight()!=null){
       							wordingValues.put("miscellaneous.entitleAutoWeight", new BigDecimal(miscellaneous.getEntitleAutoWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleAutoWeight", 0.00);
       						}
       						if(miscellaneous.getEstimateAuto()!=null){
       							wordingValues.put("miscellaneous.estimateAuto", new BigDecimal(miscellaneous.getEstimateAuto().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.estimateAuto", 0.00);
       						}
       						if(miscellaneous.getEstimatedAutoWeight()!=null){
       							wordingValues.put("miscellaneous.estimatedAutoWeight", new BigDecimal(miscellaneous.getEstimatedAutoWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.estimatedAutoWeight", 0.00);
       						}
       						if(miscellaneous.getActualAuto()!=null){
       							wordingValues.put("miscellaneous.actualAuto", new BigDecimal(miscellaneous.getActualAuto().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.actualAuto", 0.00);
       						}
     						wordingValues.put("form.price", 0);
     						wordingValues.put("form.extra", 0);
     						wordingValues.put("nprice", 0);
     						wordingValues.put("form.perqty", 0);
							String chargeWordingTemp=charge.getWording().toString();
							while(chargeWordingTemp.indexOf('$')>-1)
								chargeWordingTemp=chargeWordingTemp.replace('$', '~');
							wording = expressionManager.executeExpression(chargeWordingTemp, wordingValues).toString();
							while(wording.indexOf('~')>-1)
								wording=wording.replace('~', '$');
						

					} catch (Exception ex) {
						}
				  }
			  }
		  }
		  wordingTemp=wording;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	private DiscountManager discountManager;
	private Map<String,String> discounts;
	public String checkChargeDiscount(){
		List <Discount>al=discountManager.discountList(contract,chargeCode,sessionCorpID);
		if((al!=null)&&(!al.isEmpty())){
			discounts=new HashMap<String, String>();
			discounts.put("","");
			for(Discount discountRec:al){
				discounts.put(discountRec.getDiscount()+"", discountRec.getDescription());
			}
		}
		return SUCCESS;	
	}
	public String checkChargeCode()
	{ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if((soId==null)||(soId.toString().trim().equalsIgnoreCase(""))){
			soId="";
		}
		try{
			/*byte ptext[] = chargeCode.getBytes("ISO-8859-1"); 
			chargeCode = new String(ptext, "UTF-8");*/ 
			
		  if(!costElementFlag)	{
	      ChargeCodeList=chargesManager.findChargeCode(chargeCode, contract);
		  }
		  else{
		  ChargeCodeList=chargesManager.findChargeCodeCostElement(chargeCode, contract,sessionCorpID,jobType,routing,soCompanyDivision);  
		  }
		  String wording="";
		  if(!soId.equalsIgnoreCase("")){
			  List al=chargesManager.findChargeId(chargeCode, contract);
			  if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().equalsIgnoreCase(""))){
				  Charges charge=chargesManager.get(Long.parseLong(al.get(0).toString()));
				  Billing billing=billingManager.get(Long.parseLong(soId));
				  Miscellaneous miscellaneous=miscellaneousManager.get(Long.parseLong(soId));
				  if((charge.getWording()!=null)&&(!charge.getWording().toString().trim().equalsIgnoreCase(""))){
					  try {

							Map wordingValues = new HashMap();
							
							if (billing.getOnHand() != null) {
								wordingValues.put("billing.onHand", new BigDecimal(billing.getOnHand().toString()));
							} else {
								wordingValues.put("billing.onHand", 0.00);
							}
							if (charge.getPerValue() != null) {
								wordingValues.put("charges.perValue", new BigDecimal(charge.getPerValue().toString()));
							} else {
								wordingValues.put("charges.perValue", 0.00);
							}
							if (billing.getCycle() != 0 ) {
								String cy=billing.getCycle()+"";
								wordingValues.put("billing.cycle", new BigDecimal(cy));
							} else {
								wordingValues.put("billing.cycle", 0.00);
							}
							if (billing.getInsuranceRate() != null) {
								wordingValues.put("billing.insuranceRate", (BigDecimal) billing.getInsuranceRate());
							} else {
								wordingValues.put("billing.insuranceRate", 0.00);
							}
							if (billing.getInsuranceValueActual() != null) {
								wordingValues.put("billing.insuranceValueActual", (BigDecimal) billing.getInsuranceValueActual());
							} else {
								wordingValues.put("billing.insuranceValueActual", 0.00);
							}
							if (billing.getBaseInsuranceValue() != null) {
								wordingValues.put("billing.baseInsuranceValue", (BigDecimal) billing.getBaseInsuranceValue());
							} else {
								wordingValues.put("billing.baseInsuranceValue", 0.00);
							}
							if (charge.getMinimum() != null) {
								wordingValues.put("charges.minimum", (BigDecimal) charge.getMinimum());
							} else {
								wordingValues.put("charges.minimum", 0.00);
							}
							if (billing.getPostGrate() != null) {
								wordingValues.put("billing.postGrate", (BigDecimal) billing.getPostGrate().setScale(2,BigDecimal.ROUND_HALF_UP));
							} else {
								wordingValues.put("billing.postGrate", 0.00000);
							}
							if (charge.getDescription() != null) {
								wordingValues.put("charges.description", charge.getDescription().toString());
							} else {
								wordingValues.put("charges.description", "");
							}
							if (charge.getUseDiscount() != null) {
								wordingValues.put("charges.useDiscount", charge.getUseDiscount().toString());
							} else {
								wordingValues.put("charges.useDiscount", "");
							}
							if (charge.getPricePre() != 0.00) {
								wordingValues.put("charges.pricepre", charge.getPricePre());
							} else {
								wordingValues.put("charges.pricepre", "");
							}
							if (charge.getPerItem() != null) {
								wordingValues.put("form.qty", charge.getPerItem().toString());
							} else {
								wordingValues.put("form.qty", 0.0);
							}
							if(miscellaneous.getActualNetWeight()!=null){
								wordingValues.put("miscellaneous.actualNetWeight", new BigDecimal(miscellaneous.getActualNetWeight().toString()));
							}else{
								wordingValues.put("miscellaneous.actualNetWeight", 0.00);								
							}
							if(miscellaneous.getActualGrossWeight()!=null){
								wordingValues.put("miscellaneous.actualGrossWeight", new BigDecimal(miscellaneous.getActualGrossWeight().toString()));								
							}else{
								wordingValues.put("miscellaneous.actualGrossWeight", 0.00);
							}
							if(miscellaneous.getEstimateGrossWeight()!=null){
								wordingValues.put("miscellaneous.estimateGrossWeight", new BigDecimal(miscellaneous.getEstimateGrossWeight().toString()));								
							}else{
								wordingValues.put("miscellaneous.estimateGrossWeight", 0.00);
							}
							if(miscellaneous.getEstimatedNetWeight()!=null){
								wordingValues.put("miscellaneous.estimatedNetWeight", new BigDecimal(miscellaneous.getEstimatedNetWeight().toString()));								
							}else{
								wordingValues.put("miscellaneous.estimatedNetWeight", 0.00);
							}
							if(miscellaneous.getEstimateGrossWeightKilo()!=null){
								wordingValues.put("miscellaneous.estimateGrossWeightKilo", new BigDecimal(miscellaneous.getEstimateGrossWeightKilo().toString()));								
							}else{
								wordingValues.put("miscellaneous.estimateGrossWeightKilo", 0.00);
							}
							if(miscellaneous.getEstimatedNetWeightKilo()!=null){
								wordingValues.put("miscellaneous.estimatedNetWeightKilo", new BigDecimal(miscellaneous.getEstimatedNetWeightKilo().toString()));								
							}else{
								wordingValues.put("miscellaneous.estimatedNetWeightKilo", 0.00);
							}
							if(miscellaneous.getChargeableGrossWeight()!=null){
								wordingValues.put("miscellaneous.chargeableGrossWeight", new BigDecimal(miscellaneous.getChargeableGrossWeight().toString()));								
							}else{
								wordingValues.put("miscellaneous.chargeableGrossWeight", 0.00);
							}
							if(miscellaneous.getChargeableNetWeight()!=null){
								wordingValues.put("miscellaneous.chargeableNetWeight", new BigDecimal(miscellaneous.getChargeableNetWeight().toString()));								
							}else{
								wordingValues.put("miscellaneous.chargeableNetWeight", 0.00);
							}
							if(miscellaneous.getActualCubicFeet()!=null){
								wordingValues.put("miscellaneous.actualCubicFeet", new BigDecimal(miscellaneous.getActualCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.actualCubicFeet", 0.00);
							}
						   if(miscellaneous.getNetActualCubicFeet()!=null){
								wordingValues.put("miscellaneous.netActualCubicFeet", new BigDecimal(miscellaneous.getNetActualCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.netActualCubicFeet", 0.00);
							}
							if(miscellaneous.getActualNetWeightKilo()!=null){
								wordingValues.put("miscellaneous.actualNetWeightKilo", new BigDecimal(miscellaneous.getActualNetWeightKilo().toString()));								
							}else{
								wordingValues.put("miscellaneous.actualNetWeightKilo", 0.00);
							}
							if(miscellaneous.getActualGrossWeightKilo()!=null){
								wordingValues.put("miscellaneous.actualGrossWeightKilo", new BigDecimal(miscellaneous.getActualGrossWeightKilo().toString()));								
							}else{
								wordingValues.put("miscellaneous.actualGrossWeightKilo", 0.00);
							}
							if(miscellaneous.getActualCubicMtr()!=null){
								wordingValues.put("miscellaneous.actualCubicMtr", new BigDecimal(miscellaneous.getActualCubicMtr().toString()));								
							}else{
								wordingValues.put("miscellaneous.actualCubicMtr", 0.00);
							}
							if(miscellaneous.getNetActualCubicMtr()!=null){
								wordingValues.put("miscellaneous.netActualCubicMtr", new BigDecimal(miscellaneous.getNetActualCubicMtr().toString()));								
							}else{
								wordingValues.put("miscellaneous.netActualCubicMtr", 0.00);
							}
							
							if(miscellaneous.getEstimateCubicFeet()!=null){
								wordingValues.put("miscellaneous.estimateCubicFeet", new BigDecimal(miscellaneous.getEstimateCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.estimateCubicFeet", 0.00);
							}
							if(miscellaneous.getNetEstimateCubicFeet()!=null){
								wordingValues.put("miscellaneous.netEstimateCubicFeet", new BigDecimal(miscellaneous.getNetEstimateCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.netEstimateCubicFeet", 0.00);
							}
							if(miscellaneous.getEstimateCubicMtr()!=null){
								wordingValues.put("miscellaneous.estimateCubicMtr", new BigDecimal(miscellaneous.getEstimateCubicMtr().toString()));								
							}else{
								wordingValues.put("miscellaneous.estimateCubicMtr", 0.00);
							}
							if(miscellaneous.getNetEstimateCubicMtr()!=null){
								wordingValues.put("miscellaneous.netEstimateCubicMtr", new BigDecimal(miscellaneous.getNetEstimateCubicMtr().toString()));								
							}else{
								wordingValues.put("miscellaneous.netEstimateCubicMtr", 0.00);
							}
							if(miscellaneous.getChargeableCubicFeet()!=null){
								wordingValues.put("miscellaneous.chargeableCubicFeet", new BigDecimal(miscellaneous.getChargeableCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.chargeableCubicFeet", 0.00);
							}
							if(miscellaneous.getChargeableNetCubicFeet()!=null){
								wordingValues.put("miscellaneous.chargeableNetCubicFeet", new BigDecimal(miscellaneous.getChargeableNetCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.chargeableNetCubicFeet", 0.00);
							}
							if(miscellaneous.getChargeableCubicMtr()!=null){
								wordingValues.put("miscellaneous.chargeableCubicMtr", new BigDecimal(miscellaneous.getChargeableCubicMtr().toString()));								
							}else{
								wordingValues.put("miscellaneous.chargeableCubicMtr", 0.00);
							}
							if(miscellaneous.getChargeableNetCubicMtr()!=null){
								wordingValues.put("miscellaneous.chargeableNetCubicMtr", new BigDecimal(miscellaneous.getChargeableNetCubicMtr().toString()));								
							}else{
								wordingValues.put("miscellaneous.chargeableNetCubicMtr", 0.00);
							}
       						if(miscellaneous.getActualAutoWeight()!=null){
       							wordingValues.put("miscellaneous.actualAutoWeight", new BigDecimal(miscellaneous.getActualAutoWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.actualAutoWeight", 0.00);
       						}	
       						if(miscellaneous.getActualAutoWeight()!=null){
       							wordingValues.put("miscellaneous.actualAutoWeight", new BigDecimal(miscellaneous.getActualAutoWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.actualAutoWeight", 0.00);
       						}	
       						if(billing.getPackDiscount()!=null){
       							wordingValues.put("billing.packDiscount", new BigDecimal(billing.getPackDiscount().toString()));								
       						}else{
       							wordingValues.put("billing.packDiscount", 0.00);
       						}
       						if(billing.getStorageMeasurement()!=null){
       							wordingValues.put("billing.storageMeasurement", billing.getStorageMeasurement().toString());								
       						}else{
       							wordingValues.put("billing.storageMeasurement", "");
       						}
       						if(miscellaneous.getChargeableGrossWeightKilo()!=null){
       							wordingValues.put("miscellaneous.chargeableGrossWeightKilo", new BigDecimal(miscellaneous.getChargeableGrossWeightKilo().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.chargeableGrossWeightKilo", 0.00);
       						}
       						if(miscellaneous.getEntitleGrossWeight()!=null){
       							wordingValues.put("miscellaneous.entitleGrossWeight", new BigDecimal(miscellaneous.getEntitleGrossWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleGrossWeight", 0.00);
       						}
       						if(miscellaneous.getEntitleTareWeight()!=null){
       							wordingValues.put("miscellaneous.entitleTareWeight", new BigDecimal(miscellaneous.getEntitleTareWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleTareWeight", 0.00);
       						}
       						if(miscellaneous.getEstimateTareWeight()!=null){
       							wordingValues.put("miscellaneous.estimateTareWeight", new BigDecimal(miscellaneous.getEstimateTareWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.estimateTareWeight", 0.00);
       						}
       						if(miscellaneous.getActualTareWeight()!=null){
       							wordingValues.put("miscellaneous.actualTareWeight", new BigDecimal(miscellaneous.getActualTareWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.actualTareWeight", 0.00);
       						}
       						if(miscellaneous.getChargeableTareWeight()!=null){
       							wordingValues.put("miscellaneous.chargeableTareWeight", new BigDecimal(miscellaneous.getChargeableTareWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.chargeableTareWeight", 0.00);
       						}
       						if(miscellaneous.getEntitleNetWeight()!=null){
       							wordingValues.put("miscellaneous.entitleNetWeight", new BigDecimal(miscellaneous.getEntitleNetWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleNetWeight", 0.00);
       						}
       						if(miscellaneous.getEntitleCubicFeet()!=null){
       							wordingValues.put("miscellaneous.entitleCubicFeet", new BigDecimal(miscellaneous.getEntitleCubicFeet().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleCubicFeet", 0.00);
       						}
       						if(miscellaneous.getNetEntitleCubicFeet()!=null){
       							wordingValues.put("miscellaneous.netEntitleCubicFeet", new BigDecimal(miscellaneous.getNetEntitleCubicFeet().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.netEntitleCubicFeet", 0.00);
       						}
       						if(miscellaneous.getRwghGross()!=null){
       							wordingValues.put("miscellaneous.rwghGross", new BigDecimal(miscellaneous.getRwghGross().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.rwghGross", 0.00);
       						}
       						if(miscellaneous.getRwghTare()!=null){
       							wordingValues.put("miscellaneous.rwghTare", new BigDecimal(miscellaneous.getRwghTare().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.rwghTare", 0.00);
       						}
       						if(miscellaneous.getRwghNet()!=null){
       							wordingValues.put("miscellaneous.rwghNet", new BigDecimal(miscellaneous.getRwghNet().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.rwghNet", 0.00);
       						}
       						if(miscellaneous.getRwghCubicFeet()!=null){
       							wordingValues.put("miscellaneous.rwghCubicFeet", new BigDecimal(miscellaneous.getRwghCubicFeet().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.rwghCubicFeet", 0.00);
       						}
       						if(miscellaneous.getRwghNetCubicFeet()!=null){
       							wordingValues.put("miscellaneous.rwghNetCubicFeet", new BigDecimal(miscellaneous.getRwghNetCubicFeet().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.rwghNetCubicFeet", 0.00);
       						}
       						if(miscellaneous.getEntitleGrossWeightKilo()!=null){
       							wordingValues.put("miscellaneous.entitleGrossWeightKilo", new BigDecimal(miscellaneous.getEntitleGrossWeightKilo().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleGrossWeightKilo", 0.00);
       						}
       						if(miscellaneous.getEntitleTareWeightKilo()!=null){
       							wordingValues.put("miscellaneous.entitleTareWeightKilo", new BigDecimal(miscellaneous.getEntitleTareWeightKilo().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleTareWeightKilo", 0.00);
       						}
       						if(miscellaneous.getEstimateTareWeightKilo()!=null){
       							wordingValues.put("miscellaneous.estimateTareWeightKilo", new BigDecimal(miscellaneous.getEstimateTareWeightKilo().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.estimateTareWeightKilo", 0.00);
       						}
       						if(miscellaneous.getActualTareWeightKilo()!=null){
       							wordingValues.put("miscellaneous.actualTareWeightKilo", new BigDecimal(miscellaneous.getActualTareWeightKilo().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.actualTareWeightKilo", 0.00);
       						}
       						if(miscellaneous.getChargeableTareWeightKilo()!=null){
       							wordingValues.put("miscellaneous.chargeableTareWeightKilo", new BigDecimal(miscellaneous.getChargeableTareWeightKilo().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.chargeableTareWeightKilo", 0.00);
       						}
       						if(miscellaneous.getEntitleNetWeightKilo()!=null){
       							wordingValues.put("miscellaneous.entitleNetWeightKilo", new BigDecimal(miscellaneous.getEntitleNetWeightKilo().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleNetWeightKilo", 0.00);
       						}
       						if(miscellaneous.getEntitleCubicMtr()!=null){
       							wordingValues.put("miscellaneous.entitleCubicMtr", new BigDecimal(miscellaneous.getEntitleCubicMtr().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleCubicMtr", 0.00);
       						}
       						if(miscellaneous.getNetEntitleCubicMtr()!=null){
       							wordingValues.put("miscellaneous.netEntitleCubicMtr", new BigDecimal(miscellaneous.getNetEntitleCubicMtr().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.netEntitleCubicMtr", 0.00);
       						}
       						if(miscellaneous.getRwghGrossKilo()!=null){
       							wordingValues.put("miscellaneous.rwghGrossKilo", new BigDecimal(miscellaneous.getRwghGrossKilo().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.rwghGrossKilo", 0.00);
       						}
       						if(miscellaneous.getRwghTareKilo()!=null){
       							wordingValues.put("miscellaneous.rwghTareKilo", new BigDecimal(miscellaneous.getRwghTareKilo().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.rwghTareKilo", 0.00);
       						}
       						if(miscellaneous.getRwghNetKilo()!=null){
       							wordingValues.put("miscellaneous.rwghNetKilo", new BigDecimal(miscellaneous.getRwghNetKilo().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.rwghNetKilo", 0.00);
       						}
       						if(miscellaneous.getRwghCubicMtr()!=null){
       							wordingValues.put("miscellaneous.rwghCubicMtr", new BigDecimal(miscellaneous.getRwghCubicMtr().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.rwghCubicMtr", 0.00);
       						}
       						if(miscellaneous.getRwghNetCubicMtr()!=null){
       							wordingValues.put("miscellaneous.rwghNetCubicMtr", new BigDecimal(miscellaneous.getRwghNetCubicMtr().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.rwghNetCubicMtr", 0.00);
       						}
       						
       						if(miscellaneous.getEntitleNumberAuto()!=null){
       							wordingValues.put("miscellaneous.entitleNumberAuto", new BigDecimal(miscellaneous.getEntitleNumberAuto().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleNumberAuto", 0.00);
       						}
       						if(miscellaneous.getEntitleAutoWeight()!=null){
       							wordingValues.put("miscellaneous.entitleAutoWeight", new BigDecimal(miscellaneous.getEntitleAutoWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.entitleAutoWeight", 0.00);
       						}
       						if(miscellaneous.getEstimateAuto()!=null){
       							wordingValues.put("miscellaneous.estimateAuto", new BigDecimal(miscellaneous.getEstimateAuto().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.estimateAuto", 0.00);
       						}
       						if(miscellaneous.getEstimatedAutoWeight()!=null){
       							wordingValues.put("miscellaneous.estimatedAutoWeight", new BigDecimal(miscellaneous.getEstimatedAutoWeight().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.estimatedAutoWeight", 0.00);
       						}
       						if(miscellaneous.getActualAuto()!=null){
       							wordingValues.put("miscellaneous.actualAuto", new BigDecimal(miscellaneous.getActualAuto().toString()));								
       						}else{
       							wordingValues.put("miscellaneous.actualAuto", 0.00);
       						}
       						wordingValues.put("form.price", 0);
       						wordingValues.put("form.extra", 0);
       						wordingValues.put("nprice", 0);
       						wordingValues.put("form.perqty", 0);
							String chargeWordingTemp=charge.getWording().toString();
							chargeWordingTemp=chargeWordingTemp.replaceAll("FromField", charge.getQuantityRevisedSource());
							while(chargeWordingTemp.indexOf('$')>-1)
								chargeWordingTemp=chargeWordingTemp.replace('$', '~');
							wording = expressionManager.executeExpression(chargeWordingTemp, wordingValues).toString();
							while(wording.indexOf('~')>-1)
								wording=wording.replace('~', '$');
						} catch (Exception ex) {
						}
				  } 
			  }
		  }
		  if(ChargeCodeList!=null && !ChargeCodeList.isEmpty() && ChargeCodeList.get(0)!=null && !ChargeCodeList.get(0).toString().equalsIgnoreCase("")){
		  String str=ChargeCodeList.get(0).toString();
		  str=str+"#"+wording;
		  ChargeCodeList= new ArrayList();
		  ChargeCodeList.add(str);
		  }		  
		  
		}
		catch (Exception e) {
		
			logger.warn(e);
			ChargeCodeList=null;
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	public String checkInternalCostChargeCode()
	{ 
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	if((soId==null)||(soId.toString().trim().equalsIgnoreCase(""))){
		soId="";
	}
	 try{
	 if(!costElementFlag)	{    
	     ChargeCodeList=chargesManager.findInternalCostChargeCodeChargeCode(chargeCode, accountCompanyDivision,sessionCorpID);
	 }else{
		 ChargeCodeList=chargesManager.findInternalCostChargeCodeCostElement(chargeCode, accountCompanyDivision,sessionCorpID,jobType,routing,soCompanyDivision); 	 
	 }
	 String wording="";
	  if(!soId.equalsIgnoreCase("")){
		  List al=chargesManager.findChargeId(chargeCode, contract);
		  if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().equalsIgnoreCase(""))){
			  Charges charge=chargesManager.get(Long.parseLong(al.get(0).toString()));
			  Billing billing=billingManager.get(Long.parseLong(soId));
			  Miscellaneous miscellaneous=miscellaneousManager.get(Long.parseLong(soId));
			  if((charge.getWording()!=null)&&(!charge.getWording().toString().trim().equalsIgnoreCase(""))){
				  try {

						Map wordingValues = new HashMap();
						
						if (billing.getOnHand() != null) {
							wordingValues.put("billing.onHand", new BigDecimal(billing.getOnHand().toString()));
						} else {
							wordingValues.put("billing.onHand", 0.00);
						}
						if (charge.getPerValue() != null) {
							wordingValues.put("charges.perValue", new BigDecimal(charge.getPerValue().toString()));
						} else {
							wordingValues.put("charges.perValue", 0.00);
						}
						if (billing.getCycle() != 0 ) {
							String cy=billing.getCycle()+"";
							wordingValues.put("billing.cycle", new BigDecimal(cy));
						} else {
							wordingValues.put("billing.cycle", 0.00);
						}
						if (billing.getInsuranceRate() != null) {
							wordingValues.put("billing.insuranceRate", (BigDecimal) billing.getInsuranceRate());
						} else {
							wordingValues.put("billing.insuranceRate", 0.00);
						}
						if (billing.getInsuranceValueActual() != null) {
							wordingValues.put("billing.insuranceValueActual", (BigDecimal) billing.getInsuranceValueActual());
						} else {
							wordingValues.put("billing.insuranceValueActual", 0.00);
						}
						if (billing.getBaseInsuranceValue() != null) {
							wordingValues.put("billing.baseInsuranceValue", (BigDecimal) billing.getBaseInsuranceValue());
						} else {
							wordingValues.put("billing.baseInsuranceValue", 0.00);
						}
						if (charge.getMinimum() != null) {
							wordingValues.put("charges.minimum", (BigDecimal) charge.getMinimum());
						} else {
							wordingValues.put("charges.minimum", 0.00);
						}
						if (billing.getPostGrate() != null) {
							wordingValues.put("billing.postGrate", (BigDecimal) billing.getPostGrate().setScale(2,BigDecimal.ROUND_HALF_UP));
						} else {
							wordingValues.put("billing.postGrate", 0.00000);
						}
						if (charge.getDescription() != null) {
							wordingValues.put("charges.description", charge.getDescription().toString());
						} else {
							wordingValues.put("charges.description", "");
						}
						if (charge.getUseDiscount() != null) {
							wordingValues.put("charges.useDiscount", charge.getUseDiscount().toString());
						} else {
							wordingValues.put("charges.useDiscount", "");
						}
						if (charge.getPricePre() != 0.00) {
							wordingValues.put("charges.pricepre", charge.getPricePre());
						} else {
							wordingValues.put("charges.pricepre", "");
						}
						if (charge.getPerItem() != null) {
							wordingValues.put("form.qty", charge.getPerItem().toString());
						} else {
							wordingValues.put("form.qty", 0.0);
						}
						
						if(miscellaneous.getActualNetWeight()!=null){
							wordingValues.put("miscellaneous.actualNetWeight", new BigDecimal(miscellaneous.getActualNetWeight().toString()));
						}else{
							wordingValues.put("miscellaneous.actualNetWeight", 0.00);								
						}
						if(miscellaneous.getActualGrossWeight()!=null){
							wordingValues.put("miscellaneous.actualGrossWeight", new BigDecimal(miscellaneous.getActualGrossWeight().toString()));								
						}else{
							wordingValues.put("miscellaneous.actualGrossWeight", 0.00);
						}
						if(miscellaneous.getEstimateGrossWeight()!=null){
							wordingValues.put("miscellaneous.estimateGrossWeight", new BigDecimal(miscellaneous.getEstimateGrossWeight().toString()));								
						}else{
							wordingValues.put("miscellaneous.estimateGrossWeight", 0.00);
						}
						if(miscellaneous.getEstimatedNetWeight()!=null){
							wordingValues.put("miscellaneous.estimatedNetWeight", new BigDecimal(miscellaneous.getEstimatedNetWeight().toString()));								
						}else{
							wordingValues.put("miscellaneous.estimatedNetWeight", 0.00);
						}
						if(miscellaneous.getEstimateGrossWeightKilo()!=null){
							wordingValues.put("miscellaneous.estimateGrossWeightKilo", new BigDecimal(miscellaneous.getEstimateGrossWeightKilo().toString()));								
						}else{
							wordingValues.put("miscellaneous.estimateGrossWeightKilo", 0.00);
						}
						if(miscellaneous.getEstimatedNetWeightKilo()!=null){
							wordingValues.put("miscellaneous.estimatedNetWeightKilo", new BigDecimal(miscellaneous.getEstimatedNetWeightKilo().toString()));								
						}else{
							wordingValues.put("miscellaneous.estimatedNetWeightKilo", 0.00);
						}
						if(miscellaneous.getChargeableGrossWeight()!=null){
							wordingValues.put("miscellaneous.chargeableGrossWeight", new BigDecimal(miscellaneous.getChargeableGrossWeight().toString()));								
						}else{
							wordingValues.put("miscellaneous.chargeableGrossWeight", 0.00);
						}
						if(miscellaneous.getChargeableNetWeight()!=null){
							wordingValues.put("miscellaneous.chargeableNetWeight", new BigDecimal(miscellaneous.getChargeableNetWeight().toString()));								
						}else{
							wordingValues.put("miscellaneous.chargeableNetWeight", 0.00);
						}
						
						if(miscellaneous.getActualCubicFeet()!=null){
							wordingValues.put("miscellaneous.actualCubicFeet", new BigDecimal(miscellaneous.getActualCubicFeet().toString()));								
						}else{
							wordingValues.put("miscellaneous.actualCubicFeet", 0.00);
						}
						  if(miscellaneous.getNetActualCubicFeet()!=null){
								wordingValues.put("miscellaneous.netActualCubicFeet", new BigDecimal(miscellaneous.getNetActualCubicFeet().toString()));								
							}else{
								wordingValues.put("miscellaneous.netActualCubicFeet", 0.00);
							}
						if(miscellaneous.getActualNetWeightKilo()!=null){
							wordingValues.put("miscellaneous.actualNetWeightKilo", new BigDecimal(miscellaneous.getActualNetWeightKilo().toString()));								
						}else{
							wordingValues.put("miscellaneous.actualNetWeightKilo", 0.00);
						}
						if(miscellaneous.getActualGrossWeightKilo()!=null){
							wordingValues.put("miscellaneous.actualGrossWeightKilo", new BigDecimal(miscellaneous.getActualGrossWeightKilo().toString()));								
						}else{
							wordingValues.put("miscellaneous.actualGrossWeightKilo", 0.00);
						}
						if(miscellaneous.getActualCubicMtr()!=null){
							wordingValues.put("miscellaneous.actualCubicMtr", new BigDecimal(miscellaneous.getActualCubicMtr().toString()));								
						}else{
							wordingValues.put("miscellaneous.actualCubicMtr", 0.00);
						}
						if(miscellaneous.getNetActualCubicMtr()!=null){
							wordingValues.put("miscellaneous.netActualCubicMtr", new BigDecimal(miscellaneous.getNetActualCubicMtr().toString()));								
						}else{
							wordingValues.put("miscellaneous.netActualCubicMtr", 0.00);
						}
						
						if(miscellaneous.getEstimateCubicFeet()!=null){
							wordingValues.put("miscellaneous.estimateCubicFeet", new BigDecimal(miscellaneous.getEstimateCubicFeet().toString()));								
						}else{
							wordingValues.put("miscellaneous.estimateCubicFeet", 0.00);
						}
						if(miscellaneous.getNetEstimateCubicFeet()!=null){
							wordingValues.put("miscellaneous.netEstimateCubicFeet", new BigDecimal(miscellaneous.getNetEstimateCubicFeet().toString()));								
						}else{
							wordingValues.put("miscellaneous.netEstimateCubicFeet", 0.00);
						}
						if(miscellaneous.getEstimateCubicMtr()!=null){
							wordingValues.put("miscellaneous.estimateCubicMtr", new BigDecimal(miscellaneous.getEstimateCubicMtr().toString()));								
						}else{
							wordingValues.put("miscellaneous.estimateCubicMtr", 0.00);
						}
						if(miscellaneous.getNetEstimateCubicMtr()!=null){
							wordingValues.put("miscellaneous.netEstimateCubicMtr", new BigDecimal(miscellaneous.getNetEstimateCubicMtr().toString()));								
						}else{
							wordingValues.put("miscellaneous.netEstimateCubicMtr", 0.00);
						}
						if(miscellaneous.getChargeableCubicFeet()!=null){
							wordingValues.put("miscellaneous.chargeableCubicFeet", new BigDecimal(miscellaneous.getChargeableCubicFeet().toString()));								
						}else{
							wordingValues.put("miscellaneous.chargeableCubicFeet", 0.00);
						}
						if(miscellaneous.getChargeableNetCubicFeet()!=null){
							wordingValues.put("miscellaneous.chargeableNetCubicFeet", new BigDecimal(miscellaneous.getChargeableNetCubicFeet().toString()));								
						}else{
							wordingValues.put("miscellaneous.chargeableNetCubicFeet", 0.00);
						}
						if(miscellaneous.getChargeableCubicMtr()!=null){
							wordingValues.put("miscellaneous.chargeableCubicMtr", new BigDecimal(miscellaneous.getChargeableCubicMtr().toString()));								
						}else{
							wordingValues.put("miscellaneous.chargeableCubicMtr", 0.00);
						}
						if(miscellaneous.getChargeableNetCubicMtr()!=null){
							wordingValues.put("miscellaneous.chargeableNetCubicMtr", new BigDecimal(miscellaneous.getChargeableNetCubicMtr().toString()));								
						}else{
							wordingValues.put("miscellaneous.chargeableNetCubicMtr", 0.00);
						}
						
   						if(miscellaneous.getActualAutoWeight()!=null){
   							wordingValues.put("miscellaneous.actualAutoWeight", new BigDecimal(miscellaneous.getActualAutoWeight().toString()));								
   						}else{
   							wordingValues.put("miscellaneous.actualAutoWeight", 0.00);
   						}	
   						if(billing.getPackDiscount()!=null){
   							wordingValues.put("billing.packDiscount", new BigDecimal(billing.getPackDiscount().toString()));								
   						}else{
   							wordingValues.put("billing.packDiscount", 0.00);
   						}
   						if(billing.getStorageMeasurement()!=null){
   							wordingValues.put("billing.storageMeasurement", billing.getStorageMeasurement().toString());								
   						}else{
   							wordingValues.put("billing.storageMeasurement", "");
   						}
   						
   						if(miscellaneous.getEntitleCubicFeet()!=null){
   							wordingValues.put("miscellaneous.entitleCubicFeet", new BigDecimal(miscellaneous.getEntitleCubicFeet().toString()));								
   						}else{
   							wordingValues.put("miscellaneous.entitleCubicFeet", 0.00);
   						}
   						if(miscellaneous.getChargeableNetWeightKilo()!=null){
   							wordingValues.put("miscellaneous.chargeableNetWeightKilo", new BigDecimal(miscellaneous.getChargeableNetWeightKilo().toString()));
   							}else{
   							wordingValues.put("miscellaneous.chargeableNetWeightKilo", 0.00);
   							}
   						
   						if(miscellaneous.getEntitleNumberAuto()!=null){
   							wordingValues.put("miscellaneous.entitleNumberAuto", new BigDecimal(miscellaneous.getEntitleNumberAuto().toString()));								
   						}else{
   							wordingValues.put("miscellaneous.entitleNumberAuto", 0.00);
   						}
   						if(miscellaneous.getEntitleAutoWeight()!=null){
   							wordingValues.put("miscellaneous.entitleAutoWeight", new BigDecimal(miscellaneous.getEntitleAutoWeight().toString()));								
   						}else{
   							wordingValues.put("miscellaneous.entitleAutoWeight", 0.00);
   						}
   						if(miscellaneous.getEstimateAuto()!=null){
   							wordingValues.put("miscellaneous.estimateAuto", new BigDecimal(miscellaneous.getEstimateAuto().toString()));								
   						}else{
   							wordingValues.put("miscellaneous.estimateAuto", 0.00);
   						}
   						if(miscellaneous.getEstimatedAutoWeight()!=null){
   							wordingValues.put("miscellaneous.estimatedAutoWeight", new BigDecimal(miscellaneous.getEstimatedAutoWeight().toString()));								
   						}else{
   							wordingValues.put("miscellaneous.estimatedAutoWeight", 0.00);
   						}
   						if(miscellaneous.getActualAuto()!=null){
   							wordingValues.put("miscellaneous.actualAuto", new BigDecimal(miscellaneous.getActualAuto().toString()));								
   						}else{
   							wordingValues.put("miscellaneous.actualAuto", 0.00);
   						}
   						wordingValues.put("form.price", 0);
   						wordingValues.put("form.extra", 0);
   						wordingValues.put("nprice", 0);
   						wordingValues.put("form.perqty", 0);
						String chargeWordingTemp=charge.getWording().toString();
						while(chargeWordingTemp.indexOf('$')>-1)
							chargeWordingTemp=chargeWordingTemp.replace('$', '~');
						wording = expressionManager.executeExpression(chargeWordingTemp, wordingValues).toString();
						while(wording.indexOf('~')>-1)
							wording=wording.replace('~', '$');
					} catch (Exception ex) {
					}
			  }
		  }
	  }
	  if(ChargeCodeList!=null && !ChargeCodeList.isEmpty() && ChargeCodeList.get(0)!=null && !ChargeCodeList.get(0).toString().equalsIgnoreCase("")){
	  String str=ChargeCodeList.get(0).toString();
	  str=str+"#"+wording;
	  ChargeCodeList= new ArrayList();
	  ChargeCodeList.add(str);
	  }		 
	 }catch(Exception e){
		 logger.warn(e);
		 ChargeCodeList=null;
	 }
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   	return SUCCESS;	
	}
	
	public String defaultTemplateChargeCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
			 if(!costElementFlag){ 
				 ChargeCodeList=chargesManager.findDefaultTemplateChargeCode(chargeCode, contract);
			 }else{
				 ChargeCodeList=chargesManager.findChargeCodeCostElement(chargeCode, contract,sessionCorpID,jobType,routing,soCompanyDivision); 
			 }
		}catch(Exception e){
			 logger.warn(e);
			 ChargeCodeList=null;
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	@SkipValidation
	public String vendorChargeList() 
	{ 
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
      chargess = chargesManager.getAll();
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
          return SUCCESS;   
    }
	private List companyDivis = new ArrayList();
	private List contractList ;
	private Map<String, String> job;
	private String commissionContract;
	private String commissionChargeCode;
	private String commissionJobType;
	private String commissionCompanyDivision;
	private Boolean commissionIsActive;
	private Boolean commissionIsCommissionable;
	private Boolean isThirdParty;
	private Boolean isGrossMargin;
	private String commission;
	private List saleCommissionList;
	private String commisionId;
	
	private String revenueListServer;
	private String commListServer;
	private String saleCommisionList;
	
	
	
	@SkipValidation
	public String saveSaleCommissionRateGrid(){
		if(saleCommisionList!=null && (!(saleCommisionList.trim().equals("")))){
			try{
				List list = chargesManager.findChargeIdForOtherCorpid(commissionChargeCode, contract, sessionCorpID);
				String chId = list.get(0).toString();
				
				String[] str = saleCommisionList.split("~");
				for (String string : str) {
					String[] strRes=string.split(":");
					
					String tempRev = strRes[0].toString().trim();
					String tempCom = strRes[1].toString().trim();
					
					RateGrid rateGrid = new RateGrid();
					rateGrid.setCorpID(sessionCorpID);
		        		
					rateGrid.setCreatedBy(getRequest().getRemoteUser());
					rateGrid.setCreatedOn(new Date());
					rateGrid.setUpdatedBy(getRequest().getRemoteUser());
					rateGrid.setUpdatedOn(new Date());
					rateGrid.setCharge(chId);
					rateGrid.setContractName(contract);
					rateGrid.setQuantity1(Double.parseDouble(tempRev));
					rateGrid.setRate1(tempCom);
					
					rateGrid.setQuantity2(0.0);
					rateGrid.setBuyRate(String.valueOf(0));
					
					rateGrid=rateGridManager.save(rateGrid);
				}
			}catch(Exception e){}
		}		
		try{
			if(revenueListServer!=null && (!revenueListServer.equalsIgnoreCase(""))){
				String [] temp = revenueListServer.split("~");
				for(String res : temp){
					String [] resArr  =res.split(":");
					Long tempResId = Long.parseLong(resArr[0]);
					RateGrid resObj = rateGridManager.get(tempResId);
					resObj.setQuantity1(Double.parseDouble(resArr[1]));
					resObj.setUpdatedBy(getRequest().getRemoteUser());
					resObj.setUpdatedOn(new Date());
					rateGridManager.save(resObj);
				}
			}
		}catch(Exception e){}
		try{
			if(commListServer!=null && (!commListServer.equalsIgnoreCase(""))){
				String [] temp = commListServer.split("~");
				for(String res : temp){
					String [] resArr  =res.split(":");
					Long tempResId = Long.parseLong(resArr[0]);
					RateGrid resObj = rateGridManager.get(tempResId);
					resObj.setRate1((resArr[1]==null?"":resArr[1].trim()));
					resObj.setUpdatedBy(getRequest().getRemoteUser());
					resObj.setUpdatedOn(new Date());
					rateGridManager.save(resObj);
				}
			}
		}catch(Exception e){}
		return SUCCESS;
	}
	
	 @SkipValidation 
	    public String salesCommissionMgmt() {  
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 //getComboList(sessionCorpID);
		 job = refMasterManager.findByParameter(sessionCorpID, "JOB");
		 contractList = contractManager.getAllContractbyCorpId(sessionCorpID);
		 companyDivis=contractManager.findComanyDivision(sessionCorpID);
		 
			if(commissionContract==null){
				commissionContract="";
			}
			if(commissionChargeCode==null){
				commissionChargeCode="";
			}
			if(commissionJobType==null){
				commissionJobType="";
			}
			if(commissionCompanyDivision==null){
				commissionCompanyDivision="";
			}
			if(commissionIsActive==null){
				commissionIsActive=true;
			}
			if(commissionIsCommissionable==null){
				commissionIsCommissionable=true;
			}
		 saleCommissionList=chargesManager.getSaleCommisionableList(commissionContract,commissionChargeCode,commissionJobType,commissionCompanyDivision,commissionIsActive,commissionIsCommissionable,sessionCorpID);
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		   return SUCCESS;   
	    }	
	 private String companyDiv;
	 private String bucketMatchContract;
	 private String bucketMatchChargeCode;
	 private String comison;
	 @SkipValidation 
	    public String saleCommisionRateGrid() {  
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 rateGridList = rateGridManager.findSaleCommissionRateGrid(sessionCorpID, contract, companyDiv, jobType);
		 String contractCharge = rateGridManager.findSaleCommissionContractAndCharge(companyDiv, jobType, sessionCorpID);
		 if (!"".equals(contractCharge)) {
			String []str = contractCharge.split("~");
			bucketMatchContract = str[0];
			bucketMatchChargeCode = str[1];
		 }
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		   return SUCCESS;   
	      }
	 
	 @SkipValidation 
	    public String autoSaveSaleCommissionAjax() {  
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		  chargesManager.updateSaleCommission(Long.parseLong(commisionId),commissionIsCommissionable,commission,isThirdParty,isGrossMargin,sessionCorpID,getRequest().getRemoteUser());
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		   return SUCCESS;   
	      }
	 
	 @SkipValidation 
	    public String chargeForSaleCommision() {  
		 	chargeCdeList = chargesManager.findForContract(contract, sessionCorpID);
		   return SUCCESS;   
	    }
	 
	 @SkipValidation
     public String searchChargeForSaleCommision() { 
       	try{
       		chargeCdeList = chargesManager.findChargeForSaleCommission(contract,chargeCde,description,sessionCorpID);
       	}catch(Exception e){
       	}
 		return SUCCESS;   
     }
	 
	 @SkipValidation 
	    public String JChargeList() {  
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 getComboList(sessionCorpID);
		 chargeOrgId=sessionCorpID;
	  	  contracts = contractManager.get(id); 
	  	  chargess = chargesManager.findForContract(contracts.getContract(), sessionCorpID);
	  	  int i=chargesManager.countContractCharges(contracts.getContract(),sessionCorpID);
		   countCharges =i;
		   	getRequest().getSession().setAttribute("chargeSession", null);
	    	getRequest().getSession().setAttribute("contractSession", null);
		 	getRequest().getSession().setAttribute("chargeDescriptionSession", null);
		 	getRequest().getSession().setAttribute("chargeGlSession", null);
		 	getRequest().getSession().setAttribute("chargeExpGlSession", null);
		 	getRequest().getSession().setAttribute("chargeCommissionableSession", null);
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		   return SUCCESS;   
	      }
	 	 // method used for remove charge list.
	 @SkipValidation 
	    public String JChargeDelete() { 
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 List allId=chargesManager.searchID();
		 Iterator it=allId.iterator();		
			while(it.hasNext())
		       {
				Long dbId=(Long)it.next();
				if(chagneId.equals(dbId))
				{
					chargesManager.remove(chagneId);
					saveMessage(getText("charges.deleted")); 
				}
		       }
		          JChargeList();		  
	  	 /// contracts = contractManager.get(id); 
	  	 //// chargess = chargesManager.findForContract(contracts.getContract(), sessionCorpID);
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	  	  return SUCCESS;   
	      }	 
	 
	 
	public String list1() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String estimateActual="A";
		//System.out.println("\n\n In List1 \n\n");
		billing=billingManager.get(sid);
		
		//System.out.println("\n\n Billing record is\n\n"+billing);
		//System.out.println("\n\n Billing contract is n\n"+billing.getContract());
		
		 chargess = chargesManager.findByEstimateActual(estimateActual,billing.getContract());
		//chargess = chargesManager.getAll();  
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS;   
    }
    
    public void setId(Long id) {   
        this.id = id;   
    }   
    public void setI(Long i) {   
        this.i = i;   
    }     
    
    public Charges getCharges() {
		return charges;
	}

    public void setCharges(Charges charges) {
		this.charges = charges;
	}


	public String delete() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");  
		chargesManager.remove(charges.getId());   
        saveMessage(getText("charges.deleted"));   
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }   
	
    public String edit() { 
    		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    		getComboList(sessionCorpID);
    		contracts=contractManager.get(Long.parseLong(contractId));
    	if (id != null) {   
        	charges = chargesManager.get(id);
        	rateGridCount=rateGridManager.findTwoDNumber(charges.getId().toString(), sessionCorpID);
        }else {
        	charges = new Charges(); 
        	charges.setCreatedOn(new Date());
        	charges.setUpdatedOn(new Date());
        	charges.setIncludeLHF(true);
        	charges.setVATExclude(false);
        	charges.setPrintOnInvoice(true);
        	charges.setContractCurrency(contracts.getContractCurrency());
        	charges.setPayableContractCurrency(contracts.getPayableContractCurrency());
       } 
        
        charges.setCorpID(sessionCorpID);
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }   
  
    public String addNewCharge() { 
    	 	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList(sessionCorpID);
       	contracts=contractManager.get(Long.parseLong(contractId));
    	//contracts = contractManager.get(id); 
	  	  chargess = chargesManager.findForContract(contracts.getContract(), sessionCorpID);
	  	int i=chargesManager.countContractCharges(contracts.getContract(),sessionCorpID);
		countCharges =i;
		 
    	charges = new Charges(); 
    	charges.setContract(contracts.getContract());
    	charges.setCorpID(sessionCorpID);
    	charges.setCreatedOn(new Date());
    	charges.setUpdatedOn(new Date());
    	charges.setIncludeLHF(true);
    	charges.setPrintOnInvoice(false);
    	charges.setVATExclude(false);
    	charges.setContractCurrency(contracts.getContractCurrency());
    	charges.setPayableContractCurrency(contracts.getPayableContractCurrency());
    	String val="";
		  try{
			  val=getRequest().getParameter("countryFlexbilityCheck");
		  }catch (Exception e) {}
		if((val!=null)&&(!val.equalsIgnoreCase(""))){
			charges.setCountryFlexibility(true);
		}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;  
    }
      
    public String save() throws Exception { 
    	  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
        if (cancel != null) {   
            return "cancel";   
        }   
      
        if (delete != null) {   
            return delete();   
        }   
      
        boolean isNew = (charges.getId() == null); 
        
        if (isNew) {
        	charges.setCreatedOn(new Date());
		}
        charges.setUpdatedOn(new Date());
        charges.setUpdatedBy(getRequest().getRemoteUser());
        if(charges.getMultquantity()==null || (charges.getMultquantity()).equals(""))
        {
        charges.setMultquantity("P");
        }
        else{
        	charges.setMultquantity((charges.getMultquantity()));
        }   
        if(charges.getQuantity1()==null)
        {
        	charges.setQuantity1("");
        }
        if(charges.getPrice1()==null)
        {
        	charges.setPrice1("");
        }
        if(charges.getQuantityEstimate()==null)
        {
        	charges.setQuantityEstimate("");
        }
        if(charges.getQuantityRevised()==null)
        {
        	charges.setQuantityRevised("");
        }
       /* if(charges.getPerValue()!= 0.00){
        if(charges.getDivideMultiply().equalsIgnoreCase(" ")){
        		String key = "Please select / or * Drop/Down.";
				saveMessage(getText(key));
	            return SUCCESS;
        	}
        }*/
        if(charges.getDeviation()==null){
        	charges.setDeviation("NCT");
        }
        if(charges.getBuyDependSell()==null){
        	charges.setBuyDependSell("N");
        }
        try{
        	charges=chargesManager.save(charges);
        }catch(Exception e){
        	e.printStackTrace();
        }
        rateGridCount=rateGridManager.findTwoDNumber(charges.getId().toString(), sessionCorpID);
        getComboList(sessionCorpID);
        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
        String key = (isNew) ? "charges.added" : "charges.updated";   
        saveMessage(getText(key));
        }
        charges.setCorpID(sessionCorpID);
        if (!isNew) {   
            return INPUT;   
        } else {  
        	
        	i = Long.parseLong(chargesManager.findMaxId().get(0).toString());
            //System.out.println(i);
            charges = chargesManager.get(i);
            logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;   
        }   
    }  
    
    public String addressDetails() {  
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	charges = chargesManager.get(charges.getId()); 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }
    
    public String editDetail() {  
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	charges = chargesManager.get(charges.getId()); 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	        return SUCCESS;   
    }

   
    
    public String searchCharges() {
    	  	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList(sessionCorpID);
    	contracts=contractManager.get(Long.parseLong(contractId));
        	try{
    	if(charges != null){
    	boolean lastName = (charges.getCharge() == null);
    	boolean partnerCode = (contracts.getContract() == null);
    	boolean description=(contracts.getDescription()== null);
    	boolean gl=(charges.getGl()== null);
    	boolean expGl=(charges.getExpGl()== null);
    	//boolean billingCountryCode = (partner.getBillingCountryCode() == null);
    	getRequest().getSession().setAttribute("chargeSession", charges.getCharge());
    	getRequest().getSession().setAttribute("contractSession", contracts.getContract());
	 	getRequest().getSession().setAttribute("chargeDescriptionSession", charges.getDescription());
	 	getRequest().getSession().setAttribute("chargeGlSession", charges.getGl());
	 	getRequest().getSession().setAttribute("chargeExpGlSession", charges.getExpGl());
	 	getRequest().getSession().setAttribute("chargeCommissionableSession", comison);
	 	if(!partnerCode) {
    		chargess = chargesManager.findForCharges(charges.getCharge(),contracts.getContract(),charges.getDescription(),sessionCorpID,charges.getGl(),charges.getExpGl(),((comison==null || comison.equalsIgnoreCase("All"))?"":comison));
    	int i=chargesManager.countContractCharges(contracts.getContract(),sessionCorpID);
    		countCharges =i;
    	}
    	}else
    	{
    		 String chargeSS= (String)getRequest().getSession().getAttribute("chargeSession");
             String contractSS=(String)getRequest().getSession().getAttribute("contractSession");
             String chargeDescription= (String)getRequest().getSession().getAttribute("chargeDescriptionSession");
             String chargeGl= (String)getRequest().getSession().getAttribute("chargeGlSession");
             String chargeExpGl= (String)getRequest().getSession().getAttribute("chargeExpGlSession");
             Boolean chargeCommissionable= (Boolean)getRequest().getSession().getAttribute("chargeCommissionableSession");
            chargess = chargesManager.findForCharges(chargeSS,contractSS,chargeDescription,sessionCorpID,chargeGl,chargeExpGl,comison);
    		int i=chargesManager.countContractCharges(contracts.getContract(),sessionCorpID);
    		countCharges =i;	
    	}
    	}catch(Exception e)
    	{
    		JChargeList();
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	 	return SUCCESS;   
    	
    }
    @SkipValidation
    public String searchChargesContract() { 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	boolean lastName = (charges.getCharge() == null);
    	//boolean partnerCode = (billing.getContract() == null);
    	charges=chargesManager.get(id);
    	billing=billingManager.get(id);
    	//billing=billingManager.get(sid);
    	try{
    	if(originCountry ==null){	
    	chargess = chargesManager.findForChargesContract(charges.getCharge(),charges.getDescription(),billing.getContract(),sessionCorpID,"","","");
    	}else
    	{
    	 chargess = chargesManager.findForPopupChargesContract(charges.getCharge(),charges.getDescription(),billing.getContract(),sessionCorpID,"","","");	
    	}
    	}catch(Exception e){logger.warn(e);
    	chargess = null;
    	}
    		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    		return SUCCESS;   
    }
    @SkipValidation
    public String searchList(){   
      	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
      boolean charge = (charges.getCharge() == null);
      boolean desc = (charges.getDescription() == null);
         //if(!charge || !firstName || !shipNumber || !status || !statusDate || !job) {
        chargess = chargesManager.findByChargeCode(charges.getCharge(), charges.getDescription());
      	  // chargess = new HashSet(chargess);
      	   //System.out.println(chargess);
      //}
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
      return SUCCESS;     
      }
    
    public String rateGrid(){   
   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	charges=chargesManager.get(Long.parseLong(chid));
    	List tempIdList = contractManager.countRow(charges.getContract(), sessionCorpID);
    	Long tempId = Long.parseLong(tempIdList.get(0).toString());
    	contracts = contractManager.get(tempId);
    	//long strtTime=System.currentTimeMillis();
    	rateGridList=chargesManager.findRateGrid(Long.parseLong(chid));
    	//logger.warn("Time taken to exicute rate grid query=" +(System.currentTimeMillis()-strtTime));
    	rateListSize = rateGridList.size();
    	rateListSize = (rateListSize/10)+1;
    	rateGridCorpId = sessionCorpID;
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;     
    }

	public String saveRateGrid() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	Long maxId=null;
		if(onSiteStatusCheck.equals("") && onSiteRateCheck.equals("") && onSiteBuyRateCheck.equals("")){
			if(!onSiteQuantity2Check.equals("")){	
				String[] str = onSiteQuantity2Check.split(",");
				for(int i=0;i<str.length;i++){
					
					String[] rateGD2= str[i].split("#");
					rateGD2[0]=rateGD2[0].trim();
					if(!"newGrid".equals(rateGD2[0])){
					    rateGrid=rateGridManager.get(Long.parseLong(rateGD2[0]));
					    double b=(Double.parseDouble(rateGD2[1]));
					    rateGrid.setQuantity2(b);
					    rateGridManager.save(rateGrid);
					}
					else{
						rateGrid=new RateGrid();
						rateGrid.setCharge(chid);
				    	rateGrid.setCorpID(sessionCorpID);
				    	rateGrid.setContractName(contractName);
				    	rateGrid.setCreatedOn(new Date());
				    	rateGrid.setCreatedBy(getRequest().getRemoteUser());
					    rateGrid.setUpdatedOn(new Date());
					    rateGrid.setUpdatedBy(getRequest().getRemoteUser());
		    			double b=(Double.parseDouble(rateGD2[1]));
		    			rateGrid.setQuantity2(b);
		    			rateGridManager.save(rateGrid);
		    			maxId=Long.parseLong(rateGridManager.findMaximumId().get(0).toString());
					}	
					
				}
				}
			
		}else{
			if(!onSiteQuantity2Check.equals("")){	
				String[] str = onSiteQuantity2Check.split(",");
				for(int i=0;i<str.length;i++){
					
					String[] rateGD2= str[i].split("#");
					rateGD2[0]=rateGD2[0].trim();
					if(!"newGrid".equals(rateGD2[0])){
					    rateGrid=rateGridManager.get(Long.parseLong(rateGD2[0]));
					    double b=(Double.parseDouble(rateGD2[1]));
					    rateGrid.setQuantity2(b);
					    rateGridManager.save(rateGrid);
					}
					else{
						rateGrid=new RateGrid();
						rateGrid.setCharge(chid);
				    	rateGrid.setCorpID(sessionCorpID);
				    	rateGrid.setContractName(contractName);
				    	rateGrid.setCreatedOn(new Date());
				    	rateGrid.setCreatedBy(getRequest().getRemoteUser());
					    rateGrid.setUpdatedOn(new Date());
					    rateGrid.setUpdatedBy(getRequest().getRemoteUser());
		    			double b=(Double.parseDouble(rateGD2[1]));
		    			rateGrid.setQuantity2(b);
		    			rateGridManager.save(rateGrid);
		    			maxId=Long.parseLong(rateGridManager.findMaximumId().get(0).toString());
					}	
					
				}
				}
			
			
		if(!onSiteStatusCheck.equals("")){
			// condition when user don't want to use 2D rate grid 
			if(onSiteQuantity2Check.equals("")){
					String[] str = onSiteStatusCheck.split(",");
					for(int i=0;i<str.length;i++){
						
						String[] rateGD= str[i].split("#");
						rateGD[0]=rateGD[0].trim();
						if(!"newGrid".equals(rateGD[0])){
						    rateGrid=rateGridManager.get(Long.parseLong(rateGD[0]));
						    double b=(Double.parseDouble(rateGD[1]));
						    rateGrid.setQuantity1(b);
						      rateGrid.setQuantity2(rateGrid.getQuantity2());
						    rateGridManager.save(rateGrid);
						}
						else{
							//rateGrid=rateGridManager.get(maxId);
							rateGrid=new RateGrid();
							rateGrid.setCharge(chid);
					    	rateGrid.setCorpID(sessionCorpID);
					    	rateGrid.setContractName(contractName);
					    	rateGrid.setCreatedOn(new Date());
					    	rateGrid.setCreatedBy(getRequest().getRemoteUser());
						    rateGrid.setUpdatedOn(new Date());
						    rateGrid.setUpdatedBy(getRequest().getRemoteUser());
			    			//rateGrid=rateGridManager.get(Long.parseLong(rateGD[0]));
			    			double b=(Double.parseDouble(rateGD[1]));
			    			rateGrid.setQuantity1(b);
			    			rateGrid.setQuantity2(0);
			    			rateGridManager.save(rateGrid);
			    			maxId=Long.parseLong(rateGridManager.findMaximumId().get(0).toString());
						}	
						
					}
					
			}else{
		String[] str = onSiteStatusCheck.split(",");
		for(int i=0;i<str.length;i++){
			
			String[] rateGD= str[i].split("#");
			rateGD[0]=rateGD[0].trim();
			if(!"newGrid".equals(rateGD[0])){
			    rateGrid=rateGridManager.get(Long.parseLong(rateGD[0]));
			    double b=(Double.parseDouble(rateGD[1]));
			    rateGrid.setQuantity1(b);
			     rateGridManager.save(rateGrid);
			}
			else{
				rateGrid=rateGridManager.get(maxId);
				//rateGrid=new RateGrid();
				rateGrid.setCharge(chid);
		    	rateGrid.setCorpID(sessionCorpID);
		    	rateGrid.setContractName(contractName);
		    	rateGrid.setCreatedOn(new Date());
		    	rateGrid.setCreatedBy(getRequest().getRemoteUser());
			    rateGrid.setUpdatedOn(new Date());
			    rateGrid.setUpdatedBy(getRequest().getRemoteUser());
    			//rateGrid=rateGridManager.get(Long.parseLong(rateGD[0]));
    			double b=(Double.parseDouble(rateGD[1]));
    			rateGrid.setQuantity1(b);
    			 rateGridManager.save(rateGrid);
    			//maxId=Long.parseLong(rateGridManager.findMaximumId().get(0).toString());
			}	
			
		}
		}
		}else{
			rateGrid=new RateGrid();
			rateGrid.setCharge(chid);
	    	rateGrid.setCorpID(sessionCorpID);
	    	rateGrid.setContractName(contractName);
	    	rateGrid.setCreatedOn(new Date());
	    	rateGrid.setCreatedBy(getRequest().getRemoteUser());
		    rateGrid.setUpdatedOn(new Date());
		    rateGrid.setUpdatedBy(getRequest().getRemoteUser());
			rateGrid.setQuantity1(0);
			 rateGridManager.save(rateGrid);
			 maxId=Long.parseLong(rateGridManager.findMaximumId().get(0).toString());
		}	
			
			
			
		if(!onSiteRateCheck.equals("")){
			String rate="";
		String[] strRate = onSiteRateCheck.split(",");
		for(int j=0;j<strRate.length;j++){
			
			String[] rateGD1= strRate[j].split("#");
			if(!"newGrid".equals(rateGD1[0])){
			    rateGrid=rateGridManager.get(Long.parseLong(rateGD1[0]));
			    DecimalFormat df = new DecimalFormat("0.0000");
			    rate = df.format(Double.parseDouble(rateGD1[1].toString()));
			    rateGrid.setRate1(rate);
			    if((rateGrid.getBuyRate()==null)||(rateGrid.getBuyRate().equalsIgnoreCase(""))){
			    	rateGrid.setBuyRate("0");
			    }
			    rateGridManager.save(rateGrid);
			}
			else{
				rateGrid=rateGridManager.get(maxId);
				DecimalFormat df = new DecimalFormat("0.0000");
				rate = df.format(Double.parseDouble(rateGD1[1].toString()));
    			rateGrid.setRate1(rate);
			    if((rateGrid.getBuyRate()==null)||(rateGrid.getBuyRate().equalsIgnoreCase(""))){
			    	rateGrid.setBuyRate("0");
			    }    			
    			rateGridManager.save(rateGrid);
				
			}
		}
		}
		
		if(!onSiteBuyRateCheck.equals("")){
			String buyRate="";
			String[] strRate = onSiteBuyRateCheck.split(",");
			for(int j=0;j<strRate.length;j++){
				
				String[] rateGD1= strRate[j].split("#");
				if(!"buyRatenew".equals(rateGD1[0])){
				    rateGrid=rateGridManager.get(Long.parseLong(rateGD1[0]));
				    DecimalFormat df = new DecimalFormat("0.0000");
				    buyRate = df.format(Double.parseDouble(rateGD1[1].toString()));
				    rateGrid.setBuyRate(buyRate);
				    rateGridManager.save(rateGrid);
				}
				else{
					rateGrid=rateGridManager.get(maxId);
					DecimalFormat df = new DecimalFormat("0.0000");
				    buyRate = df.format(Double.parseDouble(rateGD1[1].toString()));
	    			rateGrid.setBuyRate(buyRate);
	    			rateGridManager.save(rateGrid);
					
				}
			}
			}
		}
		rateGridList=chargesManager.findRateGrid(Long.parseLong(chid));
		rateListSize = rateGridList.size();
    	rateListSize = (rateListSize/10)+1;
		if(rateGridList.size()>=1){
    		chargesManager.deleteRateGridInvalidRecord(Long.parseLong(chid));
    	}
		charges=chargesManager.get(Long.parseLong(chid));
		List tempIdList = contractManager.countRow(charges.getContract(), sessionCorpID);
    	Long tempId = Long.parseLong(tempIdList.get(0).toString());
    	contracts = contractManager.get(tempId);
		rateGridList=chargesManager.findRateGrid(Long.parseLong(chid));
    	onSiteQuantity2Check="";
    	onSiteStatusCheck="";
    	onSiteRateCheck="";
    	onSiteBuyRateCheck="";
    	rateGridCorpId = sessionCorpID;
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
}
	
    public String addRowToGrid() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	//System.out.println("\n\n rateGrid ID-->>"+chid);
    	saveRateGrid();
    	rateGrid=new RateGrid();
    	// rateGrid.setCharge(chid);
    //	rateGrid.setCorpID(sessionCorpID);
   // 	rateGrid.setContractName(contractName);
  //  	rateGrid.setCreatedOn(new Date());
//	rateGrid.setCreatedBy(getRequest().getRemoteUser());
//	rateGrid.setUpdatedOn(new Date());
//	rateGrid.setUpdatedBy(getRequest().getRemoteUser());
	// rateGrid.setId(getLastRecordId()+1);
    	// rateGrid= rateGridManager.save(rateGrid);
    	rateGridList=chargesManager.findRateGrid(Long.parseLong(chid)); 
    	rateListSize = rateGridList.size();
    	rateListSize = (rateListSize/10)+1;
    	rateGridList.add(rateGrid); 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
   
}
    
    public void exportRateGrid()throws Exception{
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	exportRateList=rateGridManager.getRates(chid,contractName,sessionCorpID);
    	if(!exportRateList.isEmpty()){
    	HttpServletResponse response = getResponse();
    	response.setContentType("application/vnd.ms-excel");
    	ServletOutputStream outputStream = response.getOutputStream();
    	File file = new File("Rates");
    	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
    	response.setHeader("Pragma", "public");
    	response.setHeader("Cache-Control", "max-age=0");
    	out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + exportRateList.size() + "\t is extracted");
    	outputStream.write("Distance(Up To)\t".getBytes());
    	outputStream.write("Quantity(Up To)\t".getBytes());
    	outputStream.write("Sell Rate \t".getBytes());
    	outputStream.write("Buy Rate \t".getBytes());
    	outputStream.write("\r\n".getBytes());
    	Iterator it = exportRateList.iterator();
    	while(it.hasNext()){
    		Object []rates = (Object[]) it.next();
    		if(rates[0]!=null){
    			outputStream.write((""+(rates[0].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(rates[1]!=null){
    			outputStream.write((""+(rates[1].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(rates[2]!=null){
    			outputStream.write((""+(rates[2].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		if(rates[3]!=null){
    			outputStream.write((""+(rates[3].toString() + "\t")).getBytes());
    		}else{
    			outputStream.write("\t".getBytes());
    		}
    		outputStream.write("\r\n".getBytes());
    	}
    	}
    	else{
    		HttpServletResponse response = getResponse();
			ServletOutputStream outputStream = response.getOutputStream();
			File file1=new File("NO RECORD FOUND");
			response.setContentType("application/vnd.ms-excel");
			//response.setHeader("Cache-Control", "no-cache");			
			response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			String header="NO Record Found , thanks";
			outputStream.write(header.getBytes());
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
       }


	public void setContractManager(ContractManager contractManager) {
		this.contractManager = contractManager;
	}


	public Contract getContracts() {
		return contracts;
	}


	public void setContracts(Contract contracts) {
		this.contracts = contracts;
	}

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public Long getId() {
		return id;

	}
	String chargeVal;
	String chargeVal1;
	public String searchChargesForPopup(){
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(charges.getCharge() == null || charges.getDescription()==null){
			chargeVal = "";
			chargeVal1="";
		}else{
			chargeVal = charges.getCharge();
			chargeVal1=charges.getDescription();
		} 
		if(originCountry ==null){	
		chargess = chargesManager.findForChargesContract(chargeVal,chargeVal1,contract,sessionCorpID,"","","");
		}else{
		chargess = chargesManager.findForPopupChargesContract(chargeVal,chargeVal1,contract,sessionCorpID,"","","");	
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String searchChargesInternalCost(){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		chargess = chargesManager.findForChargesInternalCost(chargeCode,description,accountCompanyDivision,sessionCorpID,"","","");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	private String gotoPageString;
	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	String s = save();
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return gotoPageString;
    }
	
	private String wording;
	private String perValue;
	private String minimumValue;
	private String totalRevenue;
	private String description;
	private String formQuantity;
	private String discountValue;
	private ExpressionManager expressionManager;
	private List chargesByContract;
	private List copyCharges;
	private String contractCopy;
	private RateGrid rateGridCopy;
	private CountryDeviation countryDeviationCopy;
	private String chargeID;
	private String chargeCde;
	private List chargeCdeList;
	private String contractCde;
	@SkipValidation
	public String testwording(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			  Charges charge=chargesManager.get(id);
	//System.out.println("\n\n\n\n wording-->"+wording);
		
		try{
		Map values = new HashMap();
		values.put("billing.onHand", 0); 
		values.put("billing.cycle", 0); 
		values.put("billing.postGrate", 0);
		values.put("billing.insuranceRate",0);
		values.put("billing.insuranceValueActual", 0);
		values.put("billing.baseInsuranceValue", 0);
		values.put("charges.minimum", new BigDecimal(minimumValue).setScale(2));
		values.put("charges.perValue", new BigDecimal(perValue));
		values.put("charges.description", description);
		values.put("form.qty", formQuantity);
		values.put("charges.useDiscount", discountValue);
		values.put("form.price", 0);
		values.put("charges.pricepre", 0);
		values.put("miscellaneous.actualNetWeight", 0);
		values.put("form.extra", 0);
		values.put("nprice", 0);
		values.put("billing.packDiscount", 0);
		values.put("miscellaneous.actualAutoWeight", 0);
		values.put("form.perqty", 0);
		values.put("serviceorder.destinationCountry", "USA");
		values.put("billing.vendorStoragePerMonth", 10);
		values.put("billing.storagePerMonth", 10);
		values.put("billing.totalInsrVal", 10);
		values.put("billing.insurancePerMonth", 10);
		values.put("billing.insuranceValueEntitle", 10);
		values.put("billing.payableRate", 10);
		values.put("billing.storageMeasurement", 0);
		values.put("miscellaneous.estimateGrossWeightKilo", 0);
		values.put("miscellaneous.estimatedNetWeightKilo", 0);
		values.put("miscellaneous.estimatedNetWeight", 0);
		values.put("miscellaneous.estimateCubicMtr", 0);
		values.put("miscellaneous.netEstimateCubicMtr", 0);
		values.put("miscellaneous.actualGrossWeightKilo", 0);
		values.put("miscellaneous.actualNetWeightKilo", 0);
		values.put("miscellaneous.chargeableGrossWeightKilo", 0);
		values.put("miscellaneous.chargeableNetWeightKilo", 0);
		values.put("miscellaneous.chargeableCubicMtr", 0);
		values.put("miscellaneous.chargeableNetCubicMtr", 0);
		values.put("miscellaneous.estimateGrossWeight", 0);
		values.put("miscellaneous.estimateCubicFeet", 0);
		values.put("miscellaneous.netEstimateCubicFeet", 0);
		values.put("miscellaneous.actualGrossWeight", 0);
		values.put("miscellaneous.actualNetWeight", 0);
		values.put("miscellaneous.actualCubicFeet", 0);
		values.put("miscellaneous.netActualCubicFeet", 0);
		values.put("miscellaneous.chargeableGrossWeight", 0);
		values.put("miscellaneous.chargeableNetWeight", 0);
		values.put("miscellaneous.chargeableCubicFeet", 0);
		values.put("miscellaneous.chargeableNetCubicFeet", 0);
		values.put("miscellaneous.rwghNetCubicMtr", 0);
		values.put("miscellaneous.rwghCubicMtr", 0);
		values.put("miscellaneous.rwghNetKilo", 0);
		values.put("miscellaneous.rwghTareKilo", 0);
		values.put("miscellaneous.rwghGrossKilo", 0);
		values.put("miscellaneous.netEntitleCubicMtr", 0);
		values.put("miscellaneous.entitleNetWeightKilo", 0);
		values.put("miscellaneous.chargeableTareWeightKilo", 0);
		values.put("miscellaneous.actualTareWeightKilo", 0);
		values.put("miscellaneous.estimateTareWeightKilo", 0);
		values.put("miscellaneous.entitleTareWeightKilo", 0);
		values.put("miscellaneous.rwghNetCubicFeet", 0);
		values.put("miscellaneous.rwghCubicFeet", 0);
		values.put("miscellaneous.rwghNet", 0);
		values.put("miscellaneous.rwghTare", 0);
		values.put("miscellaneous.rwghGross", 0);
		values.put("miscellaneous.netEntitleCubicFeet", 0);
		values.put("miscellaneous.entitleCubicFeet", 0);
		values.put("miscellaneous.entitleNetWeight", 0);
		values.put("miscellaneous.chargeableTareWeight", 0);
		values.put("miscellaneous.actualTareWeight", 0);
		values.put("miscellaneous.estimateTareWeight", 0);
		values.put("miscellaneous.entitleTareWeight", 0);
		values.put("miscellaneous.entitleGrossWeight", 0);
		values.put("miscellaneous.entitleGrossWeightKilo", 0);
		values.put("miscellaneous.entitleCubicMtr", 0);
		values.put("miscellaneous.actualCubicMtr", 0);
		values.put("miscellaneous.netActualCubicMtr", 0);	
		
		values.put("miscellaneous.entitleNumberAuto", 0);
		values.put("miscellaneous.entitleAutoWeight", 0);
		values.put("miscellaneous.estimateAuto", 0);
		values.put("miscellaneous.estimatedAutoWeight", 0);
		values.put("miscellaneous.actualAuto", 0);
		while(wording.indexOf('~')>-1) 
		wording=wording.replace('~', '+'); 
		while(wording.indexOf('`')>-1) 
			wording=wording.replace('`', '&'); 	
		while(wording.indexOf('$')>-1)
			wording=wording.replace('$', '~');
		wording=wording.replaceAll("\\\\", "\\\\\\\\");
		wording=wording.replaceAll("FromField", charge.getQuantityRevisedSource());
        totalRevenue=expressionManager.executeTestWording(wording, values);
		while(totalRevenue.indexOf('~')>-1)
		totalRevenue=totalRevenue.replace('~', '$');	
		totalRevenue=StringEscapeUtils.escapeHtml(totalRevenue);
		
		}catch(Exception ex)	
		{
			totalRevenue="Please correct the proper wording";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
    }
	
	public String testCount(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		List testCount=chargesManager.findCount(chargeCode,contractName);
		/* During Refactoring process verified condition for empty list */
		if(!testCount.isEmpty() && testCount.size() > 0){
			totalRevenue=testCount.get(0).toString();
		}
		/* Code ends */
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	private Charges chargesObj;	
	@SkipValidation
	public String addAndCopyCharges() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		chargesByContract = chargesManager.findChargesByContract(sessionCorpID);
		
		List tempCharegs = chargesManager.findChargesRespectToContract(contract,sessionCorpID);
		 Iterator itrChagres = tempCharegs.iterator();
			while(itrChagres.hasNext()){
				String temp = itrChagres.next().toString();
				chargesObj = chargesManager.get(Long.parseLong(temp));
				
		// copy of rate grid
		List tempParentGrid = chargesManager.getAllGridForCharges(temp, sessionCorpID, contract);		 
		 Iterator itrGrid = tempParentGrid.iterator();
		 while(itrGrid.hasNext()){
			 Long rateGridId = Long.parseLong(itrGrid.next().toString());
			 RateGrid grid = rateGridManager.get(rateGridId);
			 RateGrid newGrid = new RateGrid();
			 BeanUtilsBean beanUtilsGrid = BeanUtilsBean.getInstance();
			 beanUtilsGrid.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			 beanUtilsGrid.copyProperties(newGrid, grid);
			 newGrid.setCorpID(sessionCorpID);
			 newGrid.setCharge(chargesObj.getId().toString());
			 newGrid.setId(null);
			 rateGridManager.save(newGrid);
		 }
		 
		// copy of CountryDeviation
		List tempCountryDeviation = countryDeviationManager.findAllDeviation(temp,sessionCorpID, contract); 		
		 Iterator itrDev = tempCountryDeviation.iterator();
		 while(itrDev.hasNext()){
			 Long devId = Long.parseLong(itrDev.next().toString());
			 CountryDeviation countryDev = countryDeviationManager.get(devId);
			 CountryDeviation countryDevNew = new CountryDeviation();
			 BeanUtilsBean beanUtilsDev = BeanUtilsBean.getInstance();
			 beanUtilsDev.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			 beanUtilsDev.copyProperties(countryDevNew, countryDev);
			 countryDevNew.setCorpID(sessionCorpID);
			 countryDevNew.setCharge(chargesObj.getId().toString());
			 countryDevNew.setId(null);
			 countryDeviationManager.save(countryDevNew);
		 }
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		
	}
	
	@SkipValidation
	public String copyRateGrid() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		copyCharges = chargesManager.findForContract(contract,sessionCorpID);
		rateGridList=chargesManager.findRateGrid(chargesCopy.getId());
		getComboList(sessionCorpID);
		for (int y=0; y<rateGridList.size(); y++){
	    		rateGridCopy = (RateGrid)chargesManager.findRateGrid(chargesCopy.getId()).get(y);
	    		if (id == null) {
		    		
		    		chargeID = (chargesManager.findChargeId(chargesCopy.getCharge(), contractCopy)).get(0).toString();
		  		RateGrid rateGrid=new RateGrid();
				rateGrid.setCharge(chargeID);
				rateGrid.setContractName(contractCopy);
				rateGrid.setCorpID(rateGridCopy.getCorpID());
				rateGrid.setQuantity1(rateGridCopy.getQuantity1());
				rateGrid.setRate1(rateGridCopy.getRate1());
				rateGrid.setBuyRate(rateGridCopy.getBuyRate());
				rateGrid.setQuantity2(rateGridCopy.getQuantity2());
				
				rateGrid.setCreatedOn(new Date());
				rateGrid.setCreatedBy(getRequest().getRemoteUser());
				rateGrid.setUpdatedOn(new Date());
				rateGrid.setUpdatedBy(getRequest().getRemoteUser());
				rateGridManager.save(rateGrid);
			}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;		
	}

	@SkipValidation
	public String copyRateGridNew(List rateGridList, Long chargeId, String contractName) throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" copyRateGridNew() Start");
		try{
		Iterator itr=rateGridList.iterator();
		while(itr.hasNext()){
				rateGrid =(RateGrid)itr.next();
				RateGrid rateGridNew=new RateGrid();
				rateGridNew.setCharge(chargeId.toString());
				rateGridNew.setContractName(contractName);
				rateGridNew.setCorpID(rateGrid.getCorpID());
				rateGridNew.setQuantity1(rateGrid.getQuantity1());
				rateGridNew.setRate1(rateGrid.getRate1());
				rateGridNew.setBuyRate(rateGrid.getBuyRate());
				rateGridNew.setQuantity2(rateGrid.getQuantity2());
				
				rateGridNew.setCreatedOn(new Date());
				rateGridNew.setCreatedBy(getRequest().getRemoteUser());
				rateGridNew.setUpdatedOn(new Date());
				rateGridNew.setUpdatedBy(getRequest().getRemoteUser());
				rateGridManager.save(rateGridNew);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" copyRateGridNew() End");
		return SUCCESS;		
	}
	
	@SkipValidation
	public String copyCharges() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" copyCharges() Start");
		//getComboList(sessionCorpID);
		/*copyCharges = chargesManager.findContractForCopyCharges(contract,sessionCorpID);
		for (int x=0; x<copyCharges.size(); x++){
			try{
			chargesCopy = (Charges)copyCharges.get(x);
					
					Charges charges=new Charges();
					 BeanUtilsBean beanUtilsCh = BeanUtilsBean.getInstance();
					 beanUtilsCh.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					 beanUtilsCh.copyProperties(charges, chargesCopy);
					 charges.setCorpID(sessionCorpID);
					 charges.setCreatedOn(new Date());
					 charges.setCreatedBy(getRequest().getRemoteUser());
					 charges.setUpdatedOn(new Date());
					 charges.setUpdatedBy(getRequest().getRemoteUser());
					 charges.setContract(contractCopy);
					 charges.setId(null);
					 try{
						 charges=chargesManager.save(charges);
					 }catch(Exception e){
				        	e.printStackTrace();
				     }
				}catch(Exception e){
					System.out.println("Duplicate Charge Found.");
				}	
			try{
				rateGridList=chargesManager.findRateGrid(chargesCopy.getId());
				if (charges.getPriceType()!=null && charges.getPriceType().equalsIgnoreCase("RateGrid") && !(rateGridList.isEmpty())){
					copyRateGridNew(rateGridList,charges.getId(),charges.getContract());	
				}
			}catch(Exception e){
				System.out.println("Error in coping RATEGRID .");
			}
			try{
				countryDeviationList = chargesManager.findCountryDeviation(chargesCopy.getId());
				if(!countryDeviationList.isEmpty()){
					copyCountryDeviationNew(countryDeviationList,charges.getId(),charges.getContract());
				}
			}catch(Exception e){
				System.out.println("Error in coping Country Deviation .");
			}
		}*/
		
		// Copy Charges functionality by Procedure
		try{			
			String returnVal = chargesManager.callCopyCharge(sessionCorpID,contract,contractCopy,getRequest().getRemoteUser(),copyAgentAccount);
		}catch(Exception e){
			e.printStackTrace();
		}
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" copyCharges() End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String copyCountryDeviationNew(List countryDeviationList, Long chargeId, String contractName) throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" copyCountryDeviationNew() Start");
		try{
		Iterator itr=countryDeviationList.iterator();
		while(itr.hasNext()){
				countryDeviationCopy =(CountryDeviation)itr.next();
		    	CountryDeviation countryDeviation=new CountryDeviation();
		    	countryDeviation.setCharge(chargeId.toString());
		    	countryDeviation.setContractName(contractName);
		    	countryDeviation.setCorpID(countryDeviationCopy.getCorpID());
		    	countryDeviation.setPurchase(countryDeviationCopy.getPurchase());
		    	countryDeviation.setDeviation(countryDeviationCopy.getDeviation());
		    	countryDeviation.setCountryCode(countryDeviationCopy.getCountryCode());
				
		    	countryDeviation.setCreatedOn(new Date());
		    	countryDeviation.setCreatedBy(getRequest().getRemoteUser());
		    	countryDeviation.setUpdatedOn(new Date());
		    	countryDeviation.setUpdatedBy(getRequest().getRemoteUser());
		    	countryDeviationManager.save(countryDeviation);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" copyCountryDeviationNew() End");
		return SUCCESS;		
	}
	
	@SkipValidation
	public String copyCountryDeviation() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		copyCharges = chargesManager.findForContract(contract,sessionCorpID);
		countryDeviationList=chargesManager.findCountryDeviation(chargesCopy.getId());
		getComboList(sessionCorpID);
		for (int y=0; y<countryDeviationList.size(); y++){
			countryDeviationCopy = (CountryDeviation)chargesManager.findCountryDeviation(chargesCopy.getId()).get(y);
	    		if (id == null) {
		    	
		    	chargeID = (chargesManager.findChargeId(chargesCopy.getCharge(), contractCopy)).get(0).toString();
		    	CountryDeviation countryDeviation=new CountryDeviation();
		    	countryDeviation.setCharge(chargeID);
		    	countryDeviation.setContractName(contractCopy);
		    	countryDeviation.setCorpID(countryDeviationCopy.getCorpID());
		    	countryDeviation.setPurchase(countryDeviationCopy.getPurchase());
		    	countryDeviation.setDeviation(countryDeviationCopy.getDeviation());
		    	countryDeviation.setCountryCode(countryDeviationCopy.getCountryCode());
				
		    	countryDeviation.setCreatedOn(new Date());
		    	countryDeviation.setCreatedBy(getRequest().getRemoteUser());
		    	countryDeviation.setUpdatedOn(new Date());
		    	countryDeviation.setUpdatedBy(getRequest().getRemoteUser());
		    	countryDeviationManager.save(countryDeviation);
			}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;		
	}
	
	
    @SkipValidation 
    public String addCopyCharge() { 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
        	contracts=contractManager.get(Long.parseLong(contractId));
    	getComboList(sessionCorpID);
        if (id != null) {   
            copyCharge = (Charges)chargesManager.get(id);
            charges  = new Charges ();
            charges.setDescription(copyCharge.getDescription());
            charges.setContract(copyCharge.getContract());
        	charges.setWording(copyCharge.getWording());
        	charges.setQuantityType(copyCharge.getQuantityType());
        	charges.setQuantityPreset(copyCharge.getQuantityPreset());
        	charges.setQuantitySource(copyCharge.getQuantitySource());
        	charges.setPriceType(copyCharge.getPriceType());
        	charges.setPricePre(copyCharge.getPricePre());
        	charges.setPriceFormula(copyCharge.getPriceFormula());
        	charges.setPriceSource(copyCharge.getPriceSource());
        	charges.setPerItem(copyCharge.getPerItem());
        	charges.setPerValue(copyCharge.getPerValue());
        	charges.setMinimum(copyCharge.getMinimum());
        	charges.setBucket(copyCharge.getBucket());
        	charges.setEstimateActual(copyCharge.getEstimateActual());
        	charges.setGl(copyCharge.getGl());
        	charges.setItem(copyCharge.getItem());
        	charges.setDivideMultiply(copyCharge.getDivideMultiply());
        	charges.setRealPrice(copyCharge.getRealPrice());
        	charges.setRealWords(copyCharge.getRealWords());
        	charges.setMinimumWeight(copyCharge.getMinimumWeight());
        	charges.setTariff(copyCharge.getTariff());
        	charges.setUseDiscount(copyCharge.getUseDiscount());
        	charges.setService(copyCharge.isService());
        	charges.setOtherCost(copyCharge.getOtherCost());
        	charges.setItem400n(copyCharge.getItem400n());
        	charges.setSubItem(copyCharge.getSubItem());
        	charges.setSortGroup(copyCharge.getSortGroup());
        	charges.setSystemFunction(copyCharge.getSystemFunction());
        	charges.setQuantity2Type(copyCharge.getQuantity2Type());
        	charges.setQuantity2preset(copyCharge.getQuantity2preset());
        	charges.setQuantity2source(copyCharge.getQuantity2source());
        	charges.setItem2(copyCharge.getItem2());
        	charges.setExpGl(copyCharge.getExpGl());
        	charges.setQuantity1(copyCharge.getQuantity1());
        	charges.setPrice1(copyCharge.getPrice1());
        	charges.setExtra1(copyCharge.getExtra1());
        	charges.setUseOptionalWording(copyCharge.isUseOptionalWording());
        	charges.setQuantityItemEstimate(copyCharge.getQuantityItemEstimate());
        	charges.setQuantityItemRevised(copyCharge.getQuantityItemRevised());
        	charges.setQuantityEstimate(copyCharge.getQuantityEstimate());
        	charges.setQuantityRevised(copyCharge.getQuantityRevised());
        	charges.setQuantityEstimatePreset(copyCharge.getQuantityEstimatePreset());
        	charges.setQuantityEstimateSource(copyCharge.getQuantityEstimateSource());
        	charges.setQuantityRevisedPreset(copyCharge.getQuantityRevisedPreset());
        	charges.setQuantityRevisedSource(copyCharge.getQuantityRevisedSource());
        	charges.setExtraItemEstimate(copyCharge.getExtraItemEstimate());
        	charges.setExtraItemRevised(copyCharge.getExtraItemRevised());
        	charges.setExtraEstimate(copyCharge.getExtraEstimate());
        	charges.setExtraRevised(copyCharge.getExtraRevised());
        	charges.setExtraEstimatePreset(copyCharge.getExtraEstimatePreset());
        	charges.setExtraEstimateSource(copyCharge.getExtraEstimateSource());
        	charges.setExtraRevisedPreset(copyCharge.getExtraRevisedPreset());
        	charges.setExtraRevisedSource(copyCharge.getExtraRevisedSource());
        	charges.setCheckBreakPoints(copyCharge.getCheckBreakPoints());
        	charges.setBasis(copyCharge.getBasis());
        	charges.setCommission(copyCharge.getCommission());
			charges.setCorpID(sessionCorpID);
			charges.setCreatedOn(new Date());
			charges.setUpdatedOn(new Date());
			charges.setContractCurrency(copyCharge.getContractCurrency());
			charges.setPayableContractCurrency(copyCharge.getPayableContractCurrency());
			charges.setStorageFlag(copyCharge.getStorageFlag());
			charges.setOriginCountry(copyCharge.getOriginCountry());
			charges.setDestinationCountry(copyCharge.getDestinationCountry());
			charges.setStorageType(copyCharge.getStorageType());
			charges.setMode(copyCharge.getMode());
			charges.setIncludeLHF(copyCharge.getIncludeLHF());
			charges.setPrintOnInvoice(copyCharge.getPrintOnInvoice());
			charges.setTwoDGridUnit(copyCharge.getTwoDGridUnit());
			charges.setScostElementDescription(copyCharge.getScostElementDescription());
			charges.setGl(copyCharge.getGl());
			charges.setExpGl(copyCharge.getExpGl());
			charges.setCostElement(copyCharge.getCostElement());
			charges.setPayablePreset(copyCharge.getPayablePreset());
			charges.setCommissionable(copyCharge.getCommissionable());
			charges.setMultquantity(copyCharge.getMultquantity());
			charges.setPayablePriceType(copyCharge.getPayablePriceType());
			charges.setPayablePreset(copyCharge.getPayablePreset());
			charges.setExpensePrice(copyCharge.getExpensePrice());
			charges.setVATExclude(copyCharge.getVATExclude());
			charges.setEarnout(copyCharge.getEarnout());
			try{
			charges.setRollUpInInvoice(copyCharge.getRollUpInInvoice());
			}catch(Exception e){
				
			}
			
	}
        saveMessage(getText("Please enter new charge code and description"));
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }
    
    @SkipValidation
	public String checkChargeForContractExists(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	chargeCdeList=chargesManager.checkChargeForContractAvailability(chargeCde.trim(), contractCde.trim(), sessionCorpID);
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS; 
	}
    
  
   
    String comDiv;
    @SkipValidation
    public String defaultChargesByComDiv(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	chargess=chargesManager.defaultChargesByComDiv(comDiv,sessionCorpID);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
    @SkipValidation
    public String updateChargeStatus(){
    	chargesManager.updateChargeStatus(soIdNum, status);
    	return SUCCESS;
    }
    private List newChargeListValue;
    @SkipValidation
    public String checkChargeCodeAjax(){   
  	  try {
  		newChargeListValue=chargesManager.checkChargeCodeListValueNew(chargeCde.trim(), contractCde.trim(), sessionCorpID);
  		
  	} catch (Exception e) {
  		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
      	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
      	 e.printStackTrace();
      	 return CANCEL;
  	}
  		return SUCCESS; 
    }
	public List getGlcodes() {
		return glcodes;
	}

	public void setGlcodes(List glcodes) {
		this.glcodes = glcodes;
	}

	public List getRefMasters() {
		return refMasters;
	}

	public void setRefMasters(List refMasters) {
		this.refMasters = refMasters;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	

	public Map<String, String> getPriceField() {
		return priceField;
	}

	public void setPriceField(Map<String, String> priceField) {
		this.priceField = priceField;
	}

	public Map<String, String> getTariffField() {
		return tariffField;
	}

	public void setTariffField(Map<String, String> tariffField) {
		this.tariffField = tariffField;
	}

	public RateGrid getRateGrid() {
		return rateGrid;
	}

	public void setRateGrid(RateGrid rateGrid) {
		this.rateGrid = rateGrid;
	}

	public void setRateGridManager(RateGridManager rateGridManager) {
		this.rateGridManager = rateGridManager;
	}

	public List<RateGrid> getRateGridList() {
		return rateGridList;
	}

	public void setRateGridList(List<RateGrid> rateGridList) {
		this.rateGridList = rateGridList;
	}

	public String getChid() {
		return chid;
	}

	public void setChid(String chid) {
		this.chid = chid;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public String getOnSiteRateCheck() {
		return onSiteRateCheck;
	}

	public void setOnSiteRateCheck(String onSiteRateCheck) {
		this.onSiteRateCheck = onSiteRateCheck;
	}

	public String getOnSiteStatusCheck() {
		return onSiteStatusCheck;
	}

	public void setOnSiteStatusCheck(String onSiteStatusCheck) {
		this.onSiteStatusCheck = onSiteStatusCheck;
	}

	public Map<String, String> getQuantityField() {
		return quantityField;
	}

	public void setQuantityField(Map<String, String> quantityField) {
		this.quantityField = quantityField;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public List getChargeCodeList() {
		return ChargeCodeList;
	}

	public void setChargeCodeList(List chargeCodeList) {
		ChargeCodeList = chargeCodeList;
	}

	public Map<String, String> getDiscount() {
		return discount;
	}

	public void setDiscount(Map<String, String> discount) {
		this.discount = discount;
	}

	public String getMinimumValue() {
		return minimumValue;
	}

	public void setMinimumValue(String minimumValue) {
		this.minimumValue = minimumValue;
	}

	public String getPerValue() {
		return perValue;
	}

	public void setPerValue(String perValue) {
		this.perValue = perValue;
	}

	public String getWording() {
		return wording;
	}

	public void setWording(String wording) {
		this.wording = wording;
	}

	public String getTotalRevenue() {
		return totalRevenue;
	}

	public void setTotalRevenue(String totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	public void setExpressionManager(ExpressionManager expressionManager) {
		this.expressionManager = expressionManager;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(String discountValue) {
		this.discountValue = discountValue;
	}

	public String getFormQuantity() {
		return formQuantity;
	}

	public void setFormQuantity(String formQuantity) {
		this.formQuantity = formQuantity;
	}

	public Map<String, String> getCostLevel() {
		return costLevel;
	}

	public void setCostLevel(Map<String, String> costLevel) {
		this.costLevel = costLevel;
	}

	public Long getChagneId() {
		return chagneId;
	}

	public void setChagneId(Long chagneId) {
		this.chagneId = chagneId;
	}

	public  Map<String, String> getBasis() {
		return basis;
	}

	public  void setBasis(Map<String, String> basis) {
		this.basis = basis;
	}

	public int getCountCharges() {
		return countCharges;
	}

	public void setCountCharges(int countCharges) {
		this.countCharges = countCharges;
	}

	public List getChargesByContract() {
		return chargesByContract;
	}

	public void setChargesByContract(List chargesByContract) {
		this.chargesByContract = chargesByContract;
	}

	public List getCopyCharges() {
		return copyCharges;
	}

	public void setCopyCharges(List copyCharges) {
		this.copyCharges = copyCharges;
	}

	public Charges getChargesCopy() {
		return chargesCopy;
	}

	public void setChargesCopy(Charges chargesCopy) {
		this.chargesCopy = chargesCopy;
	}

	public String getContractCopy() {
		return contractCopy;
	}

	public void setContractCopy(String contractCopy) {
		this.contractCopy = contractCopy;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public RateGrid getRateGridCopy() {
		return rateGridCopy;
	}

	public void setRateGridCopy(RateGrid rateGridCopy) {
		this.rateGridCopy = rateGridCopy;
	}

	public String getChargeID() {
		return chargeID;
	}

	public void setChargeID(String chargeID) {
		this.chargeID = chargeID;
	}

	public Charges getCopyCharge() {
		return copyCharge;
	}

	public void setCopyCharge(Charges copyCharge) {
		this.copyCharge = copyCharge;
	}

	public String getChargeCde() {
		return chargeCde;
	}

	public void setChargeCde(String chargeCde) {
		this.chargeCde = chargeCde;
	}

	public List getChargeCdeList() {
		return chargeCdeList;
	}

	public void setChargeCdeList(List chargeCdeList) {
		this.chargeCdeList = chargeCdeList;
	}

	public String getContractCde() {
		return contractCde;
	}

	public void setContractCde(String contractCde) {
		this.contractCde = contractCde;
	}

	/**
	 * @return the strogeType
	 */
	public Map<String, String> getStorageType() {
		return storageType;
	}

	public static Map<String, String> getContact_frequency() {
		return contact_frequency;
	}

	public static void setContact_frequency(Map<String, String> contact_frequency) {
		ChargesAction.contact_frequency = contact_frequency;
	}

	public Map<String, String> getWorkDays() {
		return workDays;
	}

	public void setWorkDays(Map<String, String> workDays) {
		this.workDays = workDays;
	}

	public Map<String, String> getDcountry() {
		return dcountry;
	}

	public void setDcountry(Map<String, String> dcountry) {
		this.dcountry = dcountry;
	}

	public Map<String, String> getOcountry() {
		return ocountry;
	}

	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}

	public List getMode() {
		return mode;
	}

	public void setMode(List mode) {
		this.mode = mode;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	public String getServiceMode() {
		return serviceMode;
	}

	public void setServiceMode(String serviceMode) {
		this.serviceMode = serviceMode;
	}

	public String getAccountCompanyDivision() {
		return accountCompanyDivision;
	}

	public void setAccountCompanyDivision(String accountCompanyDivision) {
		this.accountCompanyDivision = accountCompanyDivision;
	}

	public String getComDiv() {
		return comDiv;
	}

	public void setComDiv(String comDiv) {
		this.comDiv = comDiv;
	}

	
	public Map<String, String> getTwoDType() {
		return twoDType;
	}

	public void setTwoDType(Map<String, String> twoDType) {
		this.twoDType = twoDType;
	}

	public String getOnSiteQuantity2Check() {
		return onSiteQuantity2Check;
	}

	public void setOnSiteQuantity2Check(String onSiteQuantity2Check) {
		this.onSiteQuantity2Check = onSiteQuantity2Check;
	}
	public String getTwoDG() {
		return twoDG;
	}

	public void setTwoDG(String twoDG) {
		this.twoDG = twoDG;
	}
	public List getExportRateList() {
		return exportRateList;
	}

	public void setExportRateList(List exportRateList) {
		this.exportRateList = exportRateList;
	}
	public int getRateGridCount() {
		return rateGridCount;
	}

	public void setRateGridCount(int rateGridCount) {
		this.rateGridCount = rateGridCount;
	}
	
	public Map<String, String> getDeviationTypeList() {
		return deviationTypeList;
	}

	public void setDeviationTypeList(Map<String, String> deviationTypeList) {
		this.deviationTypeList = deviationTypeList;
	}

	public Boolean getCostElementFlag() {
		return costElementFlag;
	}

	public void setCostElementFlag(Boolean costElementFlag) {
		this.costElementFlag = costElementFlag;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public Map<String, String> getCurrency() {
		return currency;
	}

	public void setCurrency(Map<String, String> currency) {
		this.currency = currency;
	}

	public String getRateGridCorpId() {
		return rateGridCorpId;
	}

	public void setRateGridCorpId(String rateGridCorpId) {
		this.rateGridCorpId = rateGridCorpId;
	}

	public String getChargeOrgId() {
		return chargeOrgId;
	}

	public void setChargeOrgId(String chargeOrgId) {
		this.chargeOrgId = chargeOrgId;
	}

	public List<CountryDeviation> getCountryDeviationList() {
		return countryDeviationList;
	}

	public void setCountryDeviationList(List<CountryDeviation> countryDeviationList) {
		this.countryDeviationList = countryDeviationList;
	}

	public CountryDeviation getCountryDeviationCopy() {
		return countryDeviationCopy;
	}

	public void setCountryDeviationCopy(CountryDeviation countryDeviationCopy) {
		this.countryDeviationCopy = countryDeviationCopy;
	}

	public void setCountryDeviationManager(
			CountryDeviationManager countryDeviationManager) {
		this.countryDeviationManager = countryDeviationManager;
	}

	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public Map<String, String> getJob() {
		return job;
	}

	public void setJob(Map<String, String> job) {
		this.job = job;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}

	public String getCommissionContract() {
		return commissionContract;
	}

	public void setCommissionContract(String commissionContract) {
		this.commissionContract = commissionContract;
	}

	public String getCommissionChargeCode() {
		return commissionChargeCode;
	}

	public void setCommissionChargeCode(String commissionChargeCode) {
		this.commissionChargeCode = commissionChargeCode;
	}

	public String getCommissionJobType() {
		return commissionJobType;
	}

	public void setCommissionJobType(String commissionJobType) {
		this.commissionJobType = commissionJobType;
	}

	public String getCommissionCompanyDivision() {
		return commissionCompanyDivision;
	}

	public void setCommissionCompanyDivision(String commissionCompanyDivision) {
		this.commissionCompanyDivision = commissionCompanyDivision;
	}

	public Boolean getCommissionIsActive() {
		return commissionIsActive;
	}

	public void setCommissionIsActive(Boolean commissionIsActive) {
		this.commissionIsActive = commissionIsActive;
	}

	public Boolean getCommissionIsCommissionable() {
		return commissionIsCommissionable;
	}

	public void setCommissionIsCommissionable(Boolean commissionIsCommissionable) {
		this.commissionIsCommissionable = commissionIsCommissionable;
	}

	public List getSaleCommissionList() {
		return saleCommissionList;
	}

	public void setSaleCommissionList(List saleCommissionList) {
		this.saleCommissionList = saleCommissionList;
	}

	public Boolean getIsThirdParty() {
		return isThirdParty;
	}

	public void setIsThirdParty(Boolean isThirdParty) {
		this.isThirdParty = isThirdParty;
	}

	public Boolean getIsGrossMargin() {
		return isGrossMargin;
	}

	public void setIsGrossMargin(Boolean isGrossMargin) {
		this.isGrossMargin = isGrossMargin;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getCommisionId() {
		return commisionId;
	}

	public void setCommisionId(String commisionId) {
		this.commisionId = commisionId;
	}

	public String getCompanyDiv() {
		return companyDiv;
	}

	public void setCompanyDiv(String companyDiv) {
		this.companyDiv = companyDiv;
	}

	public String getRevenueListServer() {
		return revenueListServer;
	}

	public void setRevenueListServer(String revenueListServer) {
		this.revenueListServer = revenueListServer;
	}

	public String getCommListServer() {
		return commListServer;
	}

	public void setCommListServer(String commListServer) {
		this.commListServer = commListServer;
	}

	public String getSaleCommisionList() {
		return saleCommisionList;
	}

	public void setSaleCommisionList(String saleCommisionList) {
		this.saleCommisionList = saleCommisionList;
	}

	public List getContractList() {
		return contractList;
	}

	public void setContractList(List contractList) {
		this.contractList = contractList;
	}

	public String getBucketMatchContract() {
		return bucketMatchContract;
	}

	public void setBucketMatchContract(String bucketMatchContract) {
		this.bucketMatchContract = bucketMatchContract;
	}

	public String getBucketMatchChargeCode() {
		return bucketMatchChargeCode;
	}

	public void setBucketMatchChargeCode(String bucketMatchChargeCode) {
		this.bucketMatchChargeCode = bucketMatchChargeCode;
	}

	public Charges getChargesObj() {
		return chargesObj;
	}

	public void setChargesObj(Charges chargesObj) {
		this.chargesObj = chargesObj;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}
	public  class DTOForCommissionContract{	
	 	private Object contract;

		public Object getContract() {
			return contract;
		}

		public void setContract(Object contract) {
			this.contract = contract;
		}
	}
	@SkipValidation
	public String distinctContractList(){
		contractList = contractManager.getAllContractbyCorpId(sessionCorpID);
		 Iterator it=contractList.iterator();
		 List <Object> agentList = new ArrayList<Object>();
		 DTOForCommissionContract dTOForCommissionContract=null;
		  while(it.hasNext()){
			  Object  row=it.next();
			  dTOForCommissionContract=new DTOForCommissionContract(); 
			  dTOForCommissionContract.setContract(row);
			  agentList.add(dTOForCommissionContract);
		  }
		  contractList=agentList;
		return SUCCESS; 
	}

	public String getSoCompanyDivision() {
		return soCompanyDivision;
	}

	public void setSoCompanyDivision(String soCompanyDivision) {
		this.soCompanyDivision = soCompanyDivision;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public void setDiscountManager(DiscountManager discountManager) {
		this.discountManager = discountManager;
	}

	public Map<String, String> getDiscounts() {
		return discounts;
	}

	public void setDiscounts(Map<String, String> discounts) {
		this.discounts = discounts;
	}
	public Boolean getChargeDiscountFlag() {
		return chargeDiscountFlag;
	}
	public void setChargeDiscountFlag(Boolean chargeDiscountFlag) {
		this.chargeDiscountFlag = chargeDiscountFlag;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public Long getSoIdNum() {
		return soIdNum;
	}

	public void setSoIdNum(Long soIdNum) {
		this.soIdNum = soIdNum;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Map<String, String> getCommissionDrop() {
		return commissionDrop;
	}

	public void setCommissionDrop(Map<String, String> commissionDrop) {
		this.commissionDrop = commissionDrop;
	}

	public String getComison() {
		return comison;
	}

	public void setComison(String comison) {
		this.comison = comison;
	}

	public Boolean getQuotescharges() {
		return quotescharges;
	}

	public void setQuotescharges(Boolean quotescharges) {
		this.quotescharges = quotescharges;
	}
	public List getNewChargeListValue() {
		return newChargeListValue;
	}
	public void setNewChargeListValue(List newChargeListValue) {
		this.newChargeListValue = newChargeListValue;
	}

	public String getCopyAgentAccount() {
		return copyAgentAccount;
	}

	public void setCopyAgentAccount(String copyAgentAccount) {
		this.copyAgentAccount = copyAgentAccount;
	}
	public Map<String, String> getGSTHSNCodeMap() {
		return GSTHSNCodeMap;
	}

	public void setGSTHSNCodeMap(Map<String, String> gSTHSNCodeMap) {
		GSTHSNCodeMap = gSTHSNCodeMap;
	}

}
