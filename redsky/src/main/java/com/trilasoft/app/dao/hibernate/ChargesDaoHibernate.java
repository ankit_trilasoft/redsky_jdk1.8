package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

import javax.persistence.Transient;

import org.apache.struts2.components.If;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;
import org.hibernate.Query;

import com.trilasoft.app.dao.ChargesDao;
import com.trilasoft.app.dao.hibernate.StorageDaoHibernate.StroageDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.CountryDeviation;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.ListLinkData;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.model.RefMaster;
public class ChargesDaoHibernate extends GenericDaoHibernate<Charges, Long> implements ChargesDao {   
	private List chargess;
	private HibernateUtil hibernateUtil;
    public ChargesDaoHibernate() {   
        super(Charges.class);   
    }   
    public List<Charges> findByEstimateActual(String estimateActual, String contract) {
    	return getHibernateTemplate().find("from Charges where estimateActual='"+estimateActual+"' and contract like '%"+contract+"%'");
    }

    public List<Charges> findForCharges(String charge,String contract,String description,String corpID,String gl,String expGl,String com){   
    		chargess = getHibernateTemplate().find("from Charges where charge like '%" + charge.replaceAll("'", "''") + "%' and contract ='"+contract+"' and description like '%"+description.replaceAll("'", "''") +"%' and gl like '%"+gl.replaceAll("'", "''") +"%' and expGl like '%"+expGl.replaceAll("'", "''") +"%' "+(com.equalsIgnoreCase("")?"":" and commissionable is "+com)+" and corpID='"+corpID+"'");
			return chargess;
	} 
public List findMaxId() {
		
		return getHibernateTemplate().find("select max(id) from Charges");
	}
public List<Charges> findByChargeCode(String charge,String description){
	return getHibernateTemplate().find("from Charges where charge like '"+ charge +"%' AND description like '"+ description +"%'");
}
public List<Charges> findForContract(String contract,String corpID){
	//System.out.println("from Charges where contract='"+contract+"' and corpID='"+corpID+"'");
	return getHibernateTemplate().find("from Charges where contract='"+contract.replaceAll("'", "''")+"' and corpID='"+corpID+"'");
}
public List<Charges> findForContractChargeList(String contract,String corpID,String originCountry,String destCountry,String serviceMode,String category){
	String query="";
	if(category.equalsIgnoreCase("")){
		query="select id,if(((originCountry is null or originCountry='') " +
	     " and (destinationCountry is null or destinationCountry=''))," +
	     " 'YES',if((originCountry='"+originCountry+"' or " +
	     " destinationCountry ='"+destCountry+"'),'YES','NO'))," +
	     " originCountry,destinationCountry,countryFlexibility from " +
	     " charges where contract='"+contract+"' and corpID='"+corpID+"' " +
	     " and (mode like '"+serviceMode+"%' or mode is null or mode='') and status is true";
		
	}else {		
		query="select ch.id,if(((ch.originCountry is null or ch.originCountry='') " +
			  " and (ch.destinationCountry is null or ch.destinationCountry='')),'YES', "+
			  " if((ch.originCountry='"+originCountry+"' or ch.destinationCountry ='"+destCountry+"'),'YES','NO')), "+
			  " ch.originCountry,ch.destinationCountry,ch.countryFlexibility "+
			  " from charges ch, contract ct, costelement ce "+
			  " where ch.contract=ct.contract and ch.costElement = ce.costElement "+
			  " and ch.contract='"+contract+"' "+
			  " and ch.corpID='"+corpID+"' "+
			  " and ct.corpID='"+corpID+"' "+
			  " and ce.corpID='"+corpID+"' "+
			  " and ce.reportingAlias ='"+category+"' "+
			  " and (ch.mode like '"+serviceMode+"%' or ch.mode is null or ch.mode='') and ch.status is true";		
		
	}
	List lista= this.getSession().createSQLQuery(query).list(); 
	Iterator it=lista.iterator();
	String idList="";
		while(it.hasNext()){
				Object []obj=(Object[])it.next();				
				if(idList.equalsIgnoreCase("")){
					if(obj[1].toString().equalsIgnoreCase("YES")){
						if((obj[4]!=null)&&(obj[4].toString().equalsIgnoreCase("true"))){
							if(((obj[2]==null)||(obj[2].toString().trim().equalsIgnoreCase("")))&&((obj[3]==null)||(obj[3].toString().trim().equalsIgnoreCase("")))){
								idList=obj[0]+"".toString().trim();
							}else if((obj[2]!=null)&&(obj[2].toString().trim().equalsIgnoreCase(originCountry))&&(obj[3]!=null)&&(obj[3].toString().trim().equalsIgnoreCase(destCountry))){
								idList=obj[0]+"".toString().trim();
							}else{
								
							}
						}else{
							idList=obj[0]+"".toString().trim();
						}
					}
				}else{
					if(obj[1].toString().equalsIgnoreCase("YES")){
						if((obj[4]!=null)&&(obj[4].toString().equalsIgnoreCase("true"))){
							if(((obj[2]==null)||(obj[2].toString().trim().equalsIgnoreCase("")))&&((obj[3]==null)||(obj[3].toString().trim().equalsIgnoreCase("")))){
								idList=idList+","+obj[0]+"".toString().trim();
							}else if((obj[2]!=null)&&(obj[2].toString().trim().equalsIgnoreCase(originCountry))&&(obj[3]!=null)&&(obj[3].toString().trim().equalsIgnoreCase(destCountry))){
								idList=idList+","+obj[0]+"".toString().trim();
							}else{
								
							}
						}else{
							idList=idList+","+obj[0]+"".toString().trim();
						}
					}
				}
		}
		List<Charges> al=new ArrayList<Charges>();
		List<Charges> listWithOriginDestinationCountry=new ArrayList<Charges>();
		List<Charges> listWithOriginCountry=new ArrayList<Charges>();
		List<Charges> listWithDestinationCountry=new ArrayList<Charges>();
		List<Charges> listWithAllData=new ArrayList<Charges>();
		if(!idList.equalsIgnoreCase("")){
			al=getHibernateTemplate().find("from Charges where contract='"+contract+"' and corpID='"+corpID+"' and id in ("+idList+") and (status is true) and charge not in ('DMMFXFEE','DMMFEE') order by description");
            for(Iterator<Charges> i = al.iterator(); i.hasNext(); ) {
			Charges charges = i.next();
			if(charges.getOriginCountry()!=null && ((charges.getOriginCountry().toString().trim().equals(originCountry))) && charges.getDestinationCountry()!=null && ((charges.getDestinationCountry().toString().trim().equals(destCountry)))){
				listWithOriginDestinationCountry.add(charges);
			} 
			else if(charges.getOriginCountry()!=null && ((charges.getOriginCountry().toString().trim().equals(originCountry)))){
				listWithOriginCountry.add(charges);
			}else if(charges.getDestinationCountry()!=null && ((charges.getDestinationCountry().toString().trim().equals(destCountry)))) {
				listWithDestinationCountry.add(charges);
			}else {
				listWithAllData.add(charges);
			}
			
		}
		al=new ArrayList<Charges>();
		al.addAll(listWithOriginDestinationCountry);
		al.addAll(listWithOriginCountry);
		al.addAll(listWithDestinationCountry);
		al.addAll(listWithAllData);
		}
	 return al;
	}
public List<Charges> findForChargesContract(String charge,String description,String contract,String corpID,String originCountry,String destCountry,String serviceMode){
	chargess = getHibernateTemplate().find("from Charges where charge like '%" + charge.replaceAll("'", "''") + "%' AND description like '%"+ description.replaceAll("'", "''") + "%' and contract ='"+contract+"' and corpID='"+corpID+"'and ((originCountry like '"+originCountry+"%' or originCountry is null or originCountry='')  OR (destinationCountry like '"+destCountry+"%' or destinationCountry is null or destinationCountry='') and (mode like '"+serviceMode+"%' or mode is null or mode=''))and status is true");
	return chargess;
}
public Map<String,String> findQuantityField(String sessionCorpID,String charge) {
	List<DataCatalog> list =  getHibernateTemplate().find("from DataCatalog where charge='"+ charge +"' and corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') order by description ");
	Map<String, String> parameterMap = new LinkedHashMap<String, String>();
	
	for(DataCatalog dataCatalog : list){
		parameterMap.put(dataCatalog.getRulesFiledName(), dataCatalog.getDescription());
		}
	return parameterMap;
}
public List<RateGrid> findRateGrid(Long chargeId) {
	return getHibernateTemplate().find("from RateGrid where charge='"+ chargeId +"'");
}
public void deleteRateGridInvalidRecord(Long chargeId) {
	getHibernateTemplate().bulkUpdate("delete from RateGrid where charge='"+ chargeId +"' and (quantity1=0 or quantity1=0.0 ) " );
}
public List getChargeRadio(String charge,String billingContract,String sessionCorpID)
{	
	List chargesRadio= getHibernateTemplate().find("select concat(quantityRevised,'#',priceType,'#',quantity1,'#',quantityEstimate,'#',deviation,'#',id,'#',payablePreset,'#',expensePrice) from Charges where charge='"+charge+"'AND contract='"+billingContract+"' and corpID='"+sessionCorpID+"'" );
	//System.out.println("\n\nchargesRadio-->>"+chargesRadio);
	if(chargesRadio!=null && (!(chargesRadio.isEmpty())) && chargesRadio.get(0)!=null){
	String chargesRadioData=chargesRadio.get(0).toString();
	String[] chargesRadioArray=chargesRadioData.split("#"); 
	String chargesId=chargesRadioArray[5];
	String temp= getHibernateTemplate().find("select count(*) from RateGrid where charge='"+chargesId+"' and buyRate is not null and  buyRate<>'' and buyRate <> 0.0  and corpID='"+sessionCorpID+"' ").get(0).toString();
	int count = Integer.parseInt(temp); 
	chargesRadioData=chargesRadioData+"#"+count; 
	chargesRadio =new ArrayList();
	chargesRadio.add(chargesRadioData);
	}
	return chargesRadio;
}
public  class DTO
{		
	 	private Object glCode;
	  	private Object recInvoiceNumber;
		private Object createdBy; 
		private Object billToCode;
		private Object billToName; 
		private Object description;
		private Object payingStatus;
		public Object getBillToCode() {
			return billToCode;
		}
		public void setBillToCode(Object billToCode) {
			this.billToCode = billToCode;
		}
		public Object getBillToName() {
			return billToName;
		}
		public void setBillToName(Object billToName) {
			this.billToName = billToName;
		}
		public Object getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(Object createdBy) {
			this.createdBy = createdBy;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getGlCode() {
			return glCode;
		}
		public void setGlCode(Object glCode) {
			this.glCode = glCode;
		}
		public Object getPayingStatus() {
			return payingStatus;
		}
		public void setPayingStatus(Object payingStatus) {
			this.payingStatus = payingStatus;
		}
		public Object getRecInvoiceNumber() {
			return recInvoiceNumber;
		}
		public void setRecInvoiceNumber(Object recInvoiceNumber) {
			this.recInvoiceNumber = recInvoiceNumber;
		}
}
public Map<String, String> findPriceField(String sessionCorpID,String charge) {
	
	List<DataCatalog> list =  getHibernateTemplate().find("from DataCatalog where charge='"+ charge +"' and corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') order by rulesFiledName ");
	
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		
		for(DataCatalog dataCatalog : list){
			parameterMap.put(dataCatalog.getRulesFiledName(), dataCatalog.getRulesFiledName());
			}
		
		return parameterMap;
}
public List  getQuantity(String fieldName, String shipNumber) {
	
	String[] str1 = fieldName.split("\\.");
	String newfieldName = str1[1];
	String str="";
	if(newfieldName.equals("onHand")||newfieldName.equals("cycle")||newfieldName.equals("postGrate")||newfieldName.equals("insuranceRate")||newfieldName.equals("insuranceValueActual"))
	{
		 str= "select " + newfieldName + " from Billing where shipNumber='"+shipNumber+"'";
	} 
	else if(newfieldName.equals("distance")){
		str= "select " + newfieldName + " from ServiceOrder where shipNumber='"+shipNumber+"'";	
	}
	else {
		str= "select " + newfieldName + " from Miscellaneous where shipNumber='"+shipNumber+"'";
	
	}
	List quantity= getHibernateTemplate().find(str);
	if(!quantity.isEmpty()&& quantity.get(0)!=null)
	{
		//System.out.println("\n\n\n if "+quantity);
		return quantity;
		
	}
	else
	{
		 quantity=new ArrayList();
		 quantity.add("0");
		 //System.out.println("\n\n\n else "+quantity);
		 return quantity;
		 
	}
	
}
/*public List<RateGrid> findRateFromRateGrid(String quantity, String charge) {
	List rate= getHibernateTemplate().find("select rate1 from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"'");
	return rate;
}*/
BigDecimal Q1=new BigDecimal(0);
BigDecimal Q2=new BigDecimal(0);
String rate1="0";
String rate2="0";
Double quantity1 = new Double(0);
Double quantity2 = new Double(0);
Long r1;
BigDecimal rt = new BigDecimal(0);
String s1;


public List<RateGrid> findRateFromRateGrid(String quantity, String charge, String multquantity, String distance, String twoDGridUnit) {
   
	List list=new ArrayList();
	String quantityItemRevised="";
	String  gridQuantity2= new String("0");
	String  gridQuantity1= new String("0");
	String checkBreakPoint="false";
	double incrementalStep=0.00;
	if(twoDGridUnit.equals("")||twoDGridUnit.equals("ND")){ 
	List quantityItemRevisedList=getHibernateTemplate().find("select quantityItemRevised from  Charges where id='"+charge+"'");
	if(!quantityItemRevisedList.isEmpty())
	{
		quantityItemRevised=quantityItemRevisedList.get(0).toString();
	}
	List checkBreakPoints=this.getSession().createSQLQuery("select concat(if(checkBreakPoints is true,'true','false'),'#',if((incrementalStep is null or incrementalStep=''),'0.00',incrementalStep)) from charges where id='"+charge+"'").list();
	if(checkBreakPoints!=null && (!(checkBreakPoints.isEmpty())) && checkBreakPoints.get(0)!=null && (!(checkBreakPoints.get(0).toString().trim().equals("")))){
		try{
		String[] str1 = checkBreakPoints.get(0).toString().split("#");
		checkBreakPoint= str1[0];
		incrementalStep=new Double(str1[1]);
		}catch(Exception e){
			
		}
	}
	if(checkBreakPoint.toString().equalsIgnoreCase("false"))
	{
		
		List firstQuantity= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"' limit 1").list();
		if(Double.parseDouble(quantity) <= Double.parseDouble(firstQuantity.get(0).toString()))
		{
			List rate= getHibernateTemplate().find("select rate1,quantity1,id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"'");
			if(!rate.isEmpty()){
			Iterator it=rate.iterator();
			Long objNext=new Long(0);
			while(it.hasNext())
			{
				Object []obj=(Object[])it.next();
				objNext=(Long)obj[2];
				//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
				//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
				
			}
			List <Object> currentList = new ArrayList<Object>();
			 currentList=this.getSession().createSQLQuery("select rate1,quantity1,id from rategrid where charge='"+charge+"' and id >= "+objNext + " limit 2").list();
			
			Iterator itNext=currentList.iterator();
			
			int count=1;
			while(itNext.hasNext())
			{
				Object[] obj=(Object[])itNext.next();
				
			if(count==1){
						    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
							if(rate1==null || rate1.equals("")){
								rate1="0";
							}else{
							
							}
							if(quantity1 == null || quantity1.toString().equals("")){
								quantity1 = new Double(0);
							}else{
							
							 }
						//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
						Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity1));
			}
			else{
						rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
							if(rate2==null || rate2.equals("")){
								rate2="0";
							}else{
							
							}
							if(quantity2==null || quantity2.toString().equals("")){
								quantity2 = new Double(0);
							}else{
							
							}
						//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
							Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity2));
			}
			count++;
			}
			if(quantity==null || quantity.equals("")){
				quantity="0";
			}
			else{
				
			}
			if(rate1==null || rate1.equals("")){
				rate1="0";
			}
			else{}
					//if(Double.parseDouble(quantity) * Long.parseLong(rate1) > Q2){
						
						 //r1= Long.parseLong(rate1);
						rt = new BigDecimal(rate1);
						 s1="Minimum quantity of "+quantity1+" "+quantityItemRevised+" applied";
				
			
			
			//list.add(r1.toString());
			list.add(rt.toString());
			list.add(s1);
			list.add(quantity1);
			//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
		}
		}
		else{
	List rate= getHibernateTemplate().find("select rate1,quantity1,id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"'");
	if(!rate.isEmpty()){
	Iterator it=rate.iterator();
	Long objNext=new Long(0);
	while(it.hasNext())
	{
		Object []obj=(Object[])it.next();
		objNext=(Long)obj[2];
		//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
		//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
		
	}
	List <Object> currentList = new ArrayList<Object>();
	 currentList=this.getSession().createSQLQuery("select rate1,quantity1,id from rategrid where charge='"+charge+"' and id >= "+objNext + " limit 2").list();
	 if(currentList.size()==1)
		{
			Iterator itNext=currentList.iterator();
			while(itNext.hasNext())
			{
				Object[] obj=(Object[])itNext.next();
				rate1=(String)obj[0]; 
				quantity1=(Double.valueOf(obj[1].toString()));
				s1="";
				list.add(rate1.toString());
				list.add(s1);
				list.add(quantity);
			}
			
		
		}
	 else{
	Iterator itNext=currentList.iterator();
	
	int count=1;
	while(itNext.hasNext())
	{
		Object[] obj=(Object[])itNext.next();
		
	if(count==1){
				    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
					if(rate1==null || rate1.equals("")){
						rate1="0";
					}else{
					
					}
					if(quantity1 == null || quantity1.toString().equals("")){
						quantity1 = new Double(0);
					}else{
					
					 }
				//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
				Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity1));
	}
	else{
				rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
					if(rate2==null || rate2.equals("")){
						rate2="0";
					}else{
					
					}
					if(quantity2==null || quantity2.toString().equals("")){
						quantity2 = new Double(0);
					}else{
					
					}
				//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
					Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity2));
	}
	count++;
	}
	if(quantity==null || quantity.equals("")){
		quantity="0";
	}
	else{
		
	}
	if(rate1==null || rate1.equals("")){
		rate1="0";
	}
	else{}
			//if(Double.parseDouble(quantity) * Long.parseLong(rate1) > Q2){
				
				rt = new BigDecimal(rate1);
				 s1="";
		
	//list.add(r1.toString());
	list.add(rt.toString());
	list.add(s1);
	list.add(quantity);
	//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");

	}
	}
	
	else{
		List find= getHibernateTemplate().find("select rate1,quantity1,id from RateGrid where quantity1 =(select max(quantity1) from RateGrid where quantity1<='"+ quantity +"' and charge='"+charge +"') and  charge='"+charge +"'");
		Iterator it=find.iterator();
		while(it.hasNext())
		{
			Object []obj=(Object[])it.next();
		
		rate1=(String)obj[0];
		}
		rt = new BigDecimal(rate1);
		s1="";
		list.add(rt.toString());
		list.add(s1);
		list.add(quantity);
	}
	}
	}
	
	else{
		
		List firstQuantity= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"' limit 1").list();
		if(Double.parseDouble(quantity) <= Double.parseDouble(firstQuantity.get(0).toString()))
		{
			List rate= getHibernateTemplate().find("select rate1,(quantity1+"+incrementalStep+"),id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where (quantity1)>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"'");
			
			if(!rate.isEmpty()){
			Iterator it=rate.iterator();
			Long objNext=new Long(0);
			while(it.hasNext())
			{
				Object []obj=(Object[])it.next();
				objNext=(Long)obj[2];
				//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
				//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
				
			}
			List <Object> currentList = new ArrayList<Object>();
			 currentList=this.getSession().createSQLQuery("select rate1,(quantity1+"+incrementalStep+"),id from rategrid where charge='"+charge+"' and id >= "+objNext + " limit 2").list();
			 
			Iterator itNext=currentList.iterator();
			
			int count=1;
			while(itNext.hasNext())
			{
				Object[] obj=(Object[])itNext.next();
				
			if(count==1){
						    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
							if(rate1==null || rate1.equals("")){
								rate1="0";
							}else{
							
							}
							if(quantity1 == null || quantity1.toString().equals("")){
								quantity1 = new Double(0);
							}else{
							
							 }
						//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
						Q1=new BigDecimal(rate1).multiply(new BigDecimal(firstQuantity.get(0).toString()));
			}
			else{
						rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
							if(rate2==null || rate2.equals("")){
								rate2="0";
							}else{
							
							}
							if(quantity2==null || quantity2.toString().equals("")){
								quantity2 = new Double(0);
							}else{
							
							}
						//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
							Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity1));
			}
			count++;
			}
			if(quantity==null || quantity.equals("")){
				quantity="0";
			}
			else{
				
			}
			if(rate1==null || rate1.equals("")){
				rate1="0";
			}
			else{}
					
						 //r1= Long.parseLong(rate1);
						rt = new BigDecimal(rate1);
						s1="Minimum quantity of "+quantity1+" "+quantityItemRevised+" applied";
						BigDecimal temp = new BigDecimal(firstQuantity.get(0).toString()).multiply(new BigDecimal(rate1));
						if(temp.doubleValue()> Q2.doubleValue()){
						  //r1=Q2/Long.parseLong(quantity);
							rt = new BigDecimal(rate2);
							
						  s1="Break-Point applied " + " as " +quantity1+ " "+quantityItemRevised+" "+ " @ $ "+ rate2;
					      }
					      else{
						      //r1= Long.parseLong(rate1);
						      rt = new BigDecimal(rate1);
						      s1="";
					      }
						
			
			//list.add(r1.toString());
			list.add(rt.toString());
			list.add(s1);
			//list.add(quantity1);
			if(temp.doubleValue()> Q2.doubleValue())
			{
				list.add(quantity1);
			}
			else{
				list.add(firstQuantity.get(0).toString());
			}
			//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
			
			}
		}
		
		
		else{
		List rate= getHibernateTemplate().find("select rate1,(quantity1+"+incrementalStep+"),id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"'");
		
		if(!rate.isEmpty()){
		Iterator it=rate.iterator();
		Long objNext=new Long(0);
		while(it.hasNext())
		{
			Object []obj=(Object[])it.next();
			objNext=(Long)obj[2];
			//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
			//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
			
		}
		List <Object> currentList = new ArrayList<Object>();
		 currentList=this.getSession().createSQLQuery("select rate1,(quantity1+"+incrementalStep+"),id from rategrid where charge='"+charge+"' and id >= "+objNext + " limit 2").list();
		 
		 if(currentList.size()==1)
		{
			Iterator itNext=currentList.iterator();
			while(itNext.hasNext())
			{
				Object[] obj=(Object[])itNext.next();
				rate1=(String)obj[0]; 
				quantity1=(Double.valueOf(obj[1].toString()));
				s1="";
				list.add(rate1.toString());
				list.add(s1);
				list.add(quantity);
			}
			
		
		}
		else{
		Iterator itNext=currentList.iterator();
		
		int count=1;
		while(itNext.hasNext())
		{
			Object[] obj=(Object[])itNext.next();
			
		if(count==1){
					    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
						if(rate1==null || rate1.equals("")){
							rate1="0";
						}else{
						
						}
						if(quantity1 == null || quantity1.toString().equals("")){
							quantity1 = new Double(0);
						}else{
						
						 }
					//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
					Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity));
		}
		else{
					rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
						if(rate2==null || rate2.equals("")){
							rate2="0";
						}else{
						
						}
						if(quantity2==null || quantity2.toString().equals("")){
							quantity2 = new Double(0);
						}else{
						
						}
					//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
						Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity1));
		}
		count++;
		}
		if(quantity==null || quantity.equals("")){
			quantity="0";
		}
		else{
			
		}
		if(rate1==null || rate1.equals("")){
			rate1="0";
		}
		else{}
				//if(Double.parseDouble(quantity) * Long.parseLong(rate1) > Q2){
					BigDecimal temp = new BigDecimal(quantity).multiply(new BigDecimal(rate1));
					if(temp.doubleValue()> Q2.doubleValue()){
					  //r1=Q2/Long.parseLong(quantity);
						rt = new BigDecimal(rate2);
						
					  s1="Break-Point applied " + " as " +quantity1+ " "+quantityItemRevised+" "+ " @ $ "+ rate2;
				}
				else{
					 //r1= Long.parseLong(rate1);
					rt = new BigDecimal(rate1);
					 s1="";
				}
		
		
		//list.add(r1.toString());
		list.add(rt.toString());
		list.add(s1);
		if(temp.doubleValue()> Q2.doubleValue())
		{
			list.add(quantity1);
		}
		else{
			list.add(quantity);
		}
		//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
		
		}
		}
		else{
			List find= getHibernateTemplate().find("select rate1,quantity1,id from RateGrid where quantity1 =(select max(quantity1) from RateGrid where quantity1<='"+ quantity +"' and charge='"+charge +"') and  charge='"+charge +"'");
			Iterator it=find.iterator();
			while(it.hasNext())
			{
				Object []obj=(Object[])it.next();
			
			rate1=(String)obj[0];
			}
			rt = new BigDecimal(rate1);
			s1="";
			list.add(rt.toString());
			list.add(s1);
			list.add(quantity);
		}
	}
	}
	}else{
		if(multquantity.equalsIgnoreCase("P")){
			List firstQuantity1= this.getSession().createSQLQuery("select quantity2 from rategrid where charge='"+charge+"' limit 1").list();
			List quantity2List= getHibernateTemplate().find("select quantity2 from RateGrid where quantity2 in(select min(quantity2) from RateGrid where quantity2 >='"+ distance +"' and charge='"+charge+"') and charge='"+charge+"'");	
			if(quantity2List!=null && (!(quantity2List.isEmpty())) && (quantity2List.get(0)!=null)){
				gridQuantity2=quantity2List.get(0).toString();
			}
			if(Double.parseDouble(distance) < Double.parseDouble(firstQuantity1.get(0).toString()))
			{
				quantity2List= getHibernateTemplate().find("select quantity2 from RateGrid where quantity2 in(select min(quantity2) from RateGrid where quantity2 >'"+ firstQuantity1.get(0).toString() +"' and charge='"+charge+"') and charge='"+charge+"'");	
				if(quantity2List!=null && (!(quantity2List.isEmpty())) && (quantity2List.get(0)!=null)){
					gridQuantity2=quantity2List.get(0).toString();
				}
				
			}
			List quantityItemRevisedList=getHibernateTemplate().find("select quantityItemRevised from  Charges where id='"+charge+"'");
			if(!quantityItemRevisedList.isEmpty())
			{
				quantityItemRevised=quantityItemRevisedList.get(0).toString();
			}
			List checkBreakPoints=this.getSession().createSQLQuery("select concat(if(checkBreakPoints is true,'true','false'),'#',if((incrementalStep is null or incrementalStep=''),'0.00',incrementalStep)) from charges where id='"+charge+"'").list();
			if(checkBreakPoints!=null && (!(checkBreakPoints.isEmpty())) && checkBreakPoints.get(0)!=null && (!(checkBreakPoints.get(0).toString().trim().equals("")))){
				try{
				String[] str1 = checkBreakPoints.get(0).toString().split("#");
				checkBreakPoint= str1[0];
				incrementalStep=new Double(str1[1]);
				}catch(Exception e){
					
				}
			}
			if(checkBreakPoint.toString().equalsIgnoreCase("false"))
			{
				List firstQuantity= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"' limit 1").list();
				if(firstQuantity != null && firstQuantity.size() > 0 && Double.parseDouble(quantity) <= Double.parseDouble(firstQuantity.get(0).toString()))
				{
					List rate= this.getSession().createSQLQuery("select rate1,quantity1,id from rategrid where quantity1 in(select quantity1 from rategrid where charge='"+charge+"' and quantity2='"+gridQuantity2+"') and charge='"+charge +"' and quantity2='"+gridQuantity2+"' limit 1 ").list();
					if(!rate.isEmpty()){
					Iterator it=rate.iterator();
					Long objNext=new Long(0);
					while(it.hasNext())
					{
						Object []obj=(Object[])it.next();
						objNext=Long.parseLong(obj[2].toString());
						//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
						//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
						
					}
					List <Object> currentList = new ArrayList<Object>();
					 currentList=this.getSession().createSQLQuery("select rate1,quantity1,id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity2='"+gridQuantity2+"' limit 2").list();
					
					Iterator itNext=currentList.iterator();
					
					int count=1;
					while(itNext.hasNext())
					{
						Object[] obj=(Object[])itNext.next();
						
					if(count==1){
								    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
									if(rate1==null || rate1.equals("")){
										rate1="0";
									}else{
									
									}
									if(quantity1 == null || quantity1.toString().equals("")){
										quantity1 = new Double(0);
									}else{
									
									 }
								//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
								Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity1));
					}
					else{
								rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
									if(rate2==null || rate2.equals("")){
										rate2="0";
									}else{
									
									}
									if(quantity2==null || quantity2.toString().equals("")){
										quantity2 = new Double(0);
									}else{
									
									}
								//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
									Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity2));
					}
					count++;
					}
					if(quantity==null || quantity.equals("")){
						quantity="0";
					}
					else{
						
					}
					if(rate1==null || rate1.equals("")){
						rate1="0";
					}
					else{}
							//if(Double.parseDouble(quantity) * Long.parseLong(rate1) > Q2){
								
								 //r1= Long.parseLong(rate1);
								rt = new BigDecimal(rate1);
								 s1="Minimum quantity of "+quantity1+" "+quantityItemRevised+" applied";
						
					
					
					//list.add(r1.toString());
					list.add(rt.toString());
					list.add(s1);
					list.add(firstQuantity.get(0).toString());
					//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
				}
				}
				else{
			List rate= getHibernateTemplate().find("select rate1,quantity1,id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"' and quantity2='"+gridQuantity2+"'");
			if(!rate.isEmpty()){
			Iterator it=rate.iterator();
			Long objNext=new Long(0);
			while(it.hasNext())
			{
				Object []obj=(Object[])it.next();
				objNext=(Long)obj[2];
				//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
				//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
				
			}
			List <Object> currentList = new ArrayList<Object>();
			 currentList=this.getSession().createSQLQuery("select rate1,quantity1,id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity2='"+gridQuantity2+"'  limit 2").list();
			 if(currentList.size()==1)
				{
					Iterator itNext=currentList.iterator();
					while(itNext.hasNext())
					{
						Object[] obj=(Object[])itNext.next();
						rate1=(String)obj[0]; 
						quantity1=(Double.valueOf(obj[1].toString()));
						s1="";
						list.add(rate1.toString());
						list.add(s1);
						list.add(quantity);
					}
					
				
				}
			 else{
			Iterator itNext=currentList.iterator();
			
			int count=1;
			while(itNext.hasNext())
			{
				Object[] obj=(Object[])itNext.next();
				
			if(count==1){
						    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
							if(rate1==null || rate1.equals("")){
								rate1="0";
							}else{
							
							}
							if(quantity1 == null || quantity1.toString().equals("")){
								quantity1 = new Double(0);
							}else{
							
							 }
						//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
						Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity1));
			}
			else{
						rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
							if(rate2==null || rate2.equals("")){
								rate2="0";
							}else{
							
							}
							if(quantity2==null || quantity2.toString().equals("")){
								quantity2 = new Double(0);
							}else{
							
							}
						//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
							Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity2));
			}
			count++;
			}
			if(quantity==null || quantity.equals("")){
				quantity="0";
			}
			else{
				
			}
			if(rate1==null || rate1.equals("")){
				rate1="0";
			}
			else{}
					//if(Double.parseDouble(quantity) * Long.parseLong(rate1) > Q2){
						
						rt = new BigDecimal(rate1);
						 s1="";
				
			//list.add(r1.toString());
			list.add(rt.toString());
			list.add(s1);
			list.add(quantity);
			//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");

			}
			}
			
			else{
				List find= getHibernateTemplate().find("select rate1,quantity1,id from RateGrid where quantity1 =(select max(quantity1) from RateGrid where quantity1<='"+ quantity +"' and charge='"+charge +"') and  charge='"+charge +"'  and quantity2='"+gridQuantity2+"'");
				Iterator it=find.iterator();
				while(it.hasNext())
				{
					Object []obj=(Object[])it.next();
				
				rate1=(String)obj[0];
				}
				rt = new BigDecimal(rate1);
				s1="";
				list.add(rt.toString());
				list.add(s1);
				list.add(quantity);
			}
			}
			}
			
			else{
				
				List firstQuantity= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"'  limit 1").list();
				if(Double.parseDouble(quantity) <= Double.parseDouble(firstQuantity.get(0).toString()))
				{
					List rate= this.getSession().createSQLQuery("select rate1,(quantity1+"+incrementalStep+"),id from rategrid where quantity1 in(select quantity1 from rategrid where charge='"+charge+"' and quantity2='"+gridQuantity2+"') and charge='"+charge +"' and quantity2='"+gridQuantity2+"' limit 1 ").list();
					if(!rate.isEmpty()){
					Iterator it=rate.iterator();
					Long objNext=new Long(0);
					while(it.hasNext())
					{
						Object []obj=(Object[])it.next();
						objNext=Long.parseLong(obj[2].toString());
						//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
						//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
						
					}
					List <Object> currentList = new ArrayList<Object>();
					 currentList=this.getSession().createSQLQuery("select rate1,(quantity1+"+incrementalStep+"),id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity2='"+gridQuantity2+"' limit 2").list();
					
					Iterator itNext=currentList.iterator();
					
					int count=1;
					while(itNext.hasNext())
					{
						Object[] obj=(Object[])itNext.next();
						
					if(count==1){
								    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
									if(rate1==null || rate1.equals("")){
										rate1="0";
									}else{
									
									}
									if(quantity1 == null || quantity1.toString().equals("")){
										quantity1 = new Double(0);
									}else{
									
									 }
								//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
								Q1=new BigDecimal(rate1).multiply(new BigDecimal(firstQuantity.get(0).toString()));
					}
					else{
								rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
									if(rate2==null || rate2.equals("")){
										rate2="0";
									}else{
									
									}
									if(quantity2==null || quantity2.toString().equals("")){
										quantity2 = new Double(0);
									}else{
									
									}
								//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
									Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity1));
					}
					count++;
					}
					if(quantity==null || quantity.equals("")){
						quantity="0";
					}
					else{
						
					}
					if(rate1==null || rate1.equals("")){
						rate1="0";
					}
					else{}
							
								 //r1= Long.parseLong(rate1);
								rt = new BigDecimal(rate1);
								s1="Minimum quantity of "+quantity1+" "+quantityItemRevised+" applied";
					
								BigDecimal temp = new BigDecimal(firstQuantity.get(0).toString()).multiply(new BigDecimal(rate1));
								if(temp.doubleValue()> Q2.doubleValue()){
								  //r1=Q2/Long.parseLong(quantity);
									rt = new BigDecimal(rate2);
									
								  s1="Break-Point applied " + " as " +quantity1+ " "+quantityItemRevised+" "+ " @ $ "+ rate2;
							      }
							      else{
								 //r1= Long.parseLong(rate1);
								    rt = new BigDecimal(rate1);
								    s1="";
							      }
					//list.add(r1.toString());
					list.add(rt.toString());
					list.add(s1);
					//list.add(quantity1);
					if(temp.doubleValue()> Q2.doubleValue())
					{
						list.add(quantity1);
					}
					else{
						list.add(firstQuantity.get(0).toString());
					}
					//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
					
					}
				}
				
				
				else{
				List rate= getHibernateTemplate().find("select rate1,(quantity1+"+incrementalStep+"),id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"' and quantity2='"+gridQuantity2+"' ");
				if(!rate.isEmpty()){
				Iterator it=rate.iterator();
				Long objNext=new Long(0);
				while(it.hasNext())
				{
					Object []obj=(Object[])it.next();
					objNext=(Long)obj[2];
					//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
					//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
					
				}
				List <Object> currentList = new ArrayList<Object>();
				 currentList=this.getSession().createSQLQuery("select rate1,(quantity1+"+incrementalStep+"),id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity2='"+gridQuantity2+"' limit 2").list();
				if(currentList.size()==1)
				{
					Iterator itNext=currentList.iterator();
					while(itNext.hasNext())
					{
						Object[] obj=(Object[])itNext.next();
						rate1=(String)obj[0]; 
						quantity1=(Double.valueOf(obj[1].toString()));
						s1="";
						list.add(rate1.toString());
						list.add(s1);
						list.add(quantity);
					}
					
				
				}
				else{
				Iterator itNext=currentList.iterator();
				
				int count=1;
				while(itNext.hasNext())
				{
					Object[] obj=(Object[])itNext.next();
					
				if(count==1){
							    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
								if(rate1==null || rate1.equals("")){
									rate1="0";
								}else{
								
								}
								if(quantity1 == null || quantity1.toString().equals("")){
									quantity1 = new Double(0);
								}else{
								
								 }
							//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
							Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity));
				}
				else{
							rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
								if(rate2==null || rate2.equals("")){
									rate2="0";
								}else{
								
								}
								if(quantity2==null || quantity2.toString().equals("")){
									quantity2 = new Double(0);
								}else{
								
								}
							//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
								Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity1));
				}
				count++;
				}
				if(quantity==null || quantity.equals("")){
					quantity="0";
				}
				else{
					
				}
				if(rate1==null || rate1.equals("")){
					rate1="0";
				}
				else{}
						//if(Double.parseDouble(quantity) * Long.parseLong(rate1) > Q2){
							BigDecimal temp = new BigDecimal(quantity).multiply(new BigDecimal(rate1));
							if(temp.doubleValue()> Q2.doubleValue()){
							  //r1=Q2/Long.parseLong(quantity);
								rt = new BigDecimal(rate2);
								
							  s1="Break-Point applied " + " as " +quantity1+ " "+quantityItemRevised+" "+ " @ $ "+ rate2;
						}
						else{
							 //r1= Long.parseLong(rate1);
							rt = new BigDecimal(rate1);
							 s1="";
						}
				
				
				//list.add(r1.toString());
				list.add(rt.toString());
				list.add(s1);
				if(temp.doubleValue()> Q2.doubleValue())
				{
					list.add(quantity1);
				}
				else{
					list.add(quantity);
				}
				//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
				
				}
				}
				else{
					List find= getHibernateTemplate().find("select rate1,quantity1,id from RateGrid where quantity1 =(select max(quantity1) from RateGrid where quantity1<='"+ quantity +"' and charge='"+charge +"') and  charge='"+charge +"' and quantity2='"+gridQuantity2+"'");
					Iterator it=find.iterator();
					while(it.hasNext())
					{
						Object []obj=(Object[])it.next();
					
					rate1=(String)obj[0];
					}
					rt = new BigDecimal(rate1);
					s1="";
					list.add(rt.toString());
					list.add(s1);
					list.add(quantity);
				}
			}
			}
			}else if(multquantity.equalsIgnoreCase("S")){
				List firstQuantity1= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"' limit 1").list();
				List quantity1List= getHibernateTemplate().find("select quantity1 from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"' ");	
				if(quantity1List!=null && (!(quantity1List.isEmpty())) && (quantity1List.get(0)!=null)){
					gridQuantity1=quantity1List.get(0).toString();
				}
				if(Double.parseDouble(quantity) < Double.parseDouble(firstQuantity1.get(0).toString()))
				{
					quantity1List= getHibernateTemplate().find("select quantity1 from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>'"+ firstQuantity1.get(0).toString() +"' and charge='"+charge +"') and charge='"+charge +"' ");	
					if(quantity1List!=null && (!(quantity1List.isEmpty())) && (quantity1List.get(0)!=null)){
						gridQuantity1=quantity1List.get(0).toString();
					}
					
				}
				List quantityItemRevisedList=getHibernateTemplate().find("select quantityItemRevised from  Charges where id='"+charge+"'");
				if(!quantityItemRevisedList.isEmpty())
				{
					quantityItemRevised=quantityItemRevisedList.get(0).toString();
				}
				List checkBreakPoints=this.getSession().createSQLQuery("select concat(if(checkBreakPoints is true,'true','false'),'#',if((incrementalStep is null or incrementalStep=''),'0.00',incrementalStep)) from charges where id='"+charge+"'").list();
				if(checkBreakPoints!=null && (!(checkBreakPoints.isEmpty())) && checkBreakPoints.get(0)!=null && (!(checkBreakPoints.get(0).toString().trim().equals("")))){
					try{
					String[] str1 = checkBreakPoints.get(0).toString().split("#");
					checkBreakPoint= str1[0];
					incrementalStep=new Double(str1[1]);
					}catch(Exception e){
						
					}
				}
				if(checkBreakPoint.toString().equalsIgnoreCase("false"))
				
				{
					List firstQuantity= this.getSession().createSQLQuery("select quantity2 from rategrid where charge='"+charge+"' limit 1").list();
					if(Double.parseDouble(distance) <= Double.parseDouble(firstQuantity.get(0).toString()))
					{
						List rate= this.getSession().createSQLQuery("select rate1,quantity2,id from rategrid where quantity2 in(select quantity2 from rategrid where charge='"+charge+"' and quantity1='"+gridQuantity1+"') and charge='"+charge +"' and quantity1='"+gridQuantity1+"'  limit 1").list();
						if(!rate.isEmpty()){
						Iterator it=rate.iterator();
						Long objNext=new Long(0);
						while(it.hasNext())
						{
							Object []obj=(Object[])it.next();
							objNext=Long.parseLong(obj[2].toString());
							//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
							//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
							
						}
						List <Object> currentList = new ArrayList<Object>();
						 currentList=this.getSession().createSQLQuery("select rate1,quantity2,id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity1='"+gridQuantity1+"' limit 2").list();
						
						Iterator itNext=currentList.iterator();
						
						int count=1;
						while(itNext.hasNext())
						{
							Object[] obj=(Object[])itNext.next();
							
						if(count==1){
									    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
										if(rate1==null || rate1.equals("")){
											rate1="0";
										}else{
										
										}
										if(quantity1 == null || quantity1.toString().equals("")){
											quantity1 = new Double(0);
										}else{
										
										 }
									//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
									Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity1));
						}
						else{
									rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
										if(rate2==null || rate2.equals("")){
											rate2="0";
										}else{
										
										}
										if(quantity2==null || quantity2.toString().equals("")){
											quantity2 = new Double(0);
										}else{
										
										}
									//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
										Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity2));
						}
						count++;
						}
						if(distance==null || distance.equals("")){
							distance="0";
						}
						else{
							
						}
						if(rate1==null || rate1.equals("")){
							rate1="0";
						}
						else{}
								//if(Double.parseDouble(distance) * Long.parseLong(rate1) > Q2){
									
									 //r1= Long.parseLong(rate1);
									rt = new BigDecimal(rate1);
									 s1="Minimum distance of "+quantity1+" "+quantityItemRevised+" applied";
							
						
						
						//list.add(r1.toString());
						list.add(rt.toString());
						list.add(s1);
						list.add(firstQuantity.get(0).toString());
						//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
					}
					}
					else{
				List rate= getHibernateTemplate().find("select rate1,quantity2,id from RateGrid where quantity2 in(select min(quantity2) from RateGrid where quantity2>='"+ distance +"' and charge='"+charge +"') and charge='"+charge +"' and quantity1='"+gridQuantity1+"'");
				if(!rate.isEmpty()){
				Iterator it=rate.iterator();
				Long objNext=new Long(0);
				while(it.hasNext())
				{
					Object []obj=(Object[])it.next();
					objNext=(Long)obj[2];
					//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
					//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
					
				}
				List <Object> currentList = new ArrayList<Object>();
				 currentList=this.getSession().createSQLQuery("select rate1,quantity2,id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity1='"+gridQuantity1+"'  limit 2").list();
				 if(currentList.size()==1)
					{
						Iterator itNext=currentList.iterator();
						while(itNext.hasNext())
						{
							Object[] obj=(Object[])itNext.next();
							rate1=(String)obj[0]; 
							quantity1=(Double.valueOf(obj[1].toString()));
							s1="";
							list.add(rate1.toString());
							list.add(s1);
							list.add(distance);
						}
						
					
					}
				 else{
				Iterator itNext=currentList.iterator();
				
				int count=1;
				while(itNext.hasNext())
				{
					Object[] obj=(Object[])itNext.next();
					
				if(count==1){
							    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
								if(rate1==null || rate1.equals("")){
									rate1="0";
								}else{
								
								}
								if(quantity1 == null || quantity1.toString().equals("")){
									quantity1 = new Double(0);
								}else{
								
								 }
							//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
							Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity1));
				}
				else{
							rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
								if(rate2==null || rate2.equals("")){
									rate2="0";
								}else{
								
								}
								if(quantity2==null || quantity2.toString().equals("")){
									quantity2 = new Double(0);
								}else{
								
								}
							//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
								Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity2));
				}
				count++;
				}
				if(distance==null || distance.equals("")){
					distance="0";
				}
				else{
					
				}
				if(rate1==null || rate1.equals("")){
					rate1="0";
				}
				else{}
						//if(Double.parseDouble(distance) * Long.parseLong(rate1) > Q2){
							
							rt = new BigDecimal(rate1);
							 s1="";
					
				//list.add(r1.toString());
				list.add(rt.toString());
				list.add(s1);
				list.add(distance);
				//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");

				}
				}
				
				else{
					List find= getHibernateTemplate().find("select rate1,quantity2,id from RateGrid where quantity2 =(select max(quantity2) from RateGrid where quantity2<='"+ distance +"' and charge='"+charge +"') and  charge='"+charge +"'  and quantity1='"+gridQuantity1+"'");
					Iterator it=find.iterator();
					while(it.hasNext())
					{
						Object []obj=(Object[])it.next();
					
					rate1=(String)obj[0];
					}
					rt = new BigDecimal(rate1);
					s1="";
					list.add(rt.toString());
					list.add(s1);
					list.add(distance);
				}
				}
				}
				
				else{
					
					List firstQuantity= this.getSession().createSQLQuery("select quantity2 from rategrid where charge='"+charge+"' limit 1").list();
					if(Double.parseDouble(distance) <= Double.parseDouble(firstQuantity.get(0).toString()))
					{
						List rate= this.getSession().createSQLQuery("select rate1,(quantity2+"+incrementalStep+"),id from rategrid where quantity2 in(select quantity2 from rategrid where charge='"+charge+"' and quantity1='"+gridQuantity1+"') and charge='"+charge +"' and quantity1='"+gridQuantity1+"' limit 1 ").list();
						if(!rate.isEmpty()){
						Iterator it=rate.iterator();
						Long objNext=new Long(0);
						while(it.hasNext())
						{
							Object []obj=(Object[])it.next();
							objNext=Long.parseLong(obj[2].toString());
							//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
							//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
							
						}
						List <Object> currentList = new ArrayList<Object>();
						 currentList=this.getSession().createSQLQuery("select rate1,(quantity2+"+incrementalStep+"),id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity1='"+gridQuantity1+"' limit 2").list();
						
						Iterator itNext=currentList.iterator();
						
						int count=1;
						while(itNext.hasNext())
						{
							Object[] obj=(Object[])itNext.next();
							
						if(count==1){
									    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
										if(rate1==null || rate1.equals("")){
											rate1="0";
										}else{
										
										}
										if(quantity1 == null || quantity1.toString().equals("")){
											quantity1 = new Double(0);
										}else{
										
										 }
									//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
									Q1=new BigDecimal(rate1).multiply(new BigDecimal(firstQuantity.get(0).toString()));
						}
						else{
									rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
										if(rate2==null || rate2.equals("")){
											rate2="0";
										}else{
										
										}
										if(quantity2==null || quantity2.toString().equals("")){
											quantity2 = new Double(0);
										}else{
										
										}
									//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
										Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity1));
						}
						count++;
						}
						if(distance==null || distance.equals("")){
							distance="0";
						}
						else{
							
						}
						if(rate1==null || rate1.equals("")){
							rate1="0";
						}
						else{}
								
									 //r1= Long.parseLong(rate1);
									rt = new BigDecimal(rate1);
									s1="Minimum distance of "+quantity1+" "+quantityItemRevised+" applied";
						
									BigDecimal temp = new BigDecimal(firstQuantity.get(0).toString()).multiply(new BigDecimal(rate1));
									if(temp.doubleValue()> Q2.doubleValue()){
									  //r1=Q2/Long.parseLong(quantity);
										rt = new BigDecimal(rate2);
										
									  s1="Break-Point applied " + " as " +quantity1+ " "+quantityItemRevised+" "+ " @ $ "+ rate2;
								     }
								     else{
									    //r1= Long.parseLong(rate1);
									    rt = new BigDecimal(rate1);
									    s1="";
								     }


						//list.add(r1.toString());
						list.add(rt.toString());
						list.add(s1);
						//list.add(quantity1);
						if(temp.doubleValue()> Q2.doubleValue())
						{
							list.add(quantity1);
						}
						else{
							list.add(firstQuantity.get(0).toString());
						}
						//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
						
						}
					}
					
					
					else{
					List rate= getHibernateTemplate().find("select rate1,(quantity2+"+incrementalStep+"),id from RateGrid where quantity2 in(select min(quantity2) from RateGrid where quantity2>='"+ distance +"' and charge='"+charge +"') and charge='"+charge +"' and quantity1='"+gridQuantity1+"' ");
					if(!rate.isEmpty()){
					Iterator it=rate.iterator();
					Long objNext=new Long(0);
					while(it.hasNext())
					{
						Object []obj=(Object[])it.next();
						objNext=(Long)obj[2];
						//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
						//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
						
					}
					List <Object> currentList = new ArrayList<Object>();
					 currentList=this.getSession().createSQLQuery("select rate1,(quantity2+"+incrementalStep+"),id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity1='"+gridQuantity1+"' limit 2").list();
					if(currentList.size()==1)
					{
						Iterator itNext=currentList.iterator();
						while(itNext.hasNext())
						{
							Object[] obj=(Object[])itNext.next();
							rate1=(String)obj[0]; 
							quantity1=(Double.valueOf(obj[1].toString()));
							s1="";
							list.add(rate1.toString());
							list.add(s1);
							list.add(distance);
						}
						
					
					}
					else{
					Iterator itNext=currentList.iterator();
					
					int count=1;
					while(itNext.hasNext())
					{
						Object[] obj=(Object[])itNext.next();
						
					if(count==1){
								    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
									if(rate1==null || rate1.equals("")){
										rate1="0";
									}else{
									
									}
									if(quantity1 == null || quantity1.toString().equals("")){
										quantity1 = new Double(0);
									}else{
									
									 }
								//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
								Q1=new BigDecimal(rate1).multiply(new BigDecimal(distance));
					}
					else{
								rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
									if(rate2==null || rate2.equals("")){
										rate2="0";
									}else{
									
									}
									if(quantity2==null || quantity2.toString().equals("")){
										quantity2 = new Double(0);
									}else{
									
									}
								//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
									Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity1));
					}
					count++;
					}
					if(distance==null || distance.equals("")){
						distance="0";
					}
					else{
						
					}
					if(rate1==null || rate1.equals("")){
						rate1="0";
					}
					else{}
							//if(Double.parseDouble(distance) * Long.parseLong(rate1) > Q2){
								BigDecimal temp = new BigDecimal(distance).multiply(new BigDecimal(rate1));
								if(temp.doubleValue()> Q2.doubleValue()){
								  //r1=Q2/Long.parseLong(distance);
									rt = new BigDecimal(rate2);
									
								  s1="Break-Point applied " + " as " +quantity1+ " "+quantityItemRevised+" "+ " @ $ "+ rate2;
							}
							else{
								 //r1= Long.parseLong(rate1);
								rt = new BigDecimal(rate1);
								 s1="";
							}
					
					
					//list.add(r1.toString());
					list.add(rt.toString());
					list.add(s1);
					if(temp.doubleValue()> Q2.doubleValue())
					{
						list.add(quantity1);
					}
					else{
						list.add(distance);
					}
					//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
					
					}
					}
					else{
						List find= getHibernateTemplate().find("select rate1,quantity2,id from RateGrid where quantity2 =(select max(quantity2) from RateGrid where quantity2<='"+ distance +"' and charge='"+charge +"') and  charge='"+charge +"' and quantity1='"+gridQuantity1+"'");
						Iterator it=find.iterator();
						while(it.hasNext())
						{
							Object []obj=(Object[])it.next();
						
						rate1=(String)obj[0];
						}
						rt = new BigDecimal(rate1);
						s1="";
						list.add(rt.toString());
						list.add(s1);
						list.add(distance);
					}
				}
				}
				
				
			}
			else  if(multquantity.equalsIgnoreCase("N")){
			
				List rate= getHibernateTemplate().find("select rate1 from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+quantity+"' and charge='"+charge +"') and quantity2 in(select min(quantity2) from RateGrid where quantity2>='"+distance+"' and charge='"+charge +"') and charge='"+charge +"'");	
				if( (rate!=null) && (!(rate.isEmpty())) && (rate.get(0)!=null)){
					
					list.add(rate.get(0).toString());
					list.add("");
					list.add("1");
				}else{
					List rateList= getHibernateTemplate().find("select rate1 from RateGrid where quantity1 in(select max(quantity1) from RateGrid where quantity1<='"+quantity+"' and charge='"+charge +"') and quantity2 in(select max(quantity2) from RateGrid where quantity2 <= '"+distance+"' and charge='"+charge +"') and charge='"+charge +"'");		
					if( (rateList!=null) && (!(rateList.isEmpty())) && (rateList.get(0)!=null)){
						list.add(rateList.get(0).toString());
						list.add("Minimum distance of "+quantity1+" "+quantityItemRevised+" applied");
						list.add("1");	
					}else{
						list.add("1");
						list.add("");
						list.add("1");
					}
				}
				
			}
			else  if(multquantity.equalsIgnoreCase("B")){
				double rate1B=0.00;
				double rate2B=0.00;
				double quantity1B=0.00;
				double quantity2B=0.00;
				Double totalRate=new Double(0.00);
				Double totalQuantity=new Double(0.00);
				List checkBreakPoints=this.getSession().createSQLQuery("select concat(if(checkBreakPoints is true,'true','false'),'#',if((incrementalStep is null or incrementalStep=''),'0.00',incrementalStep)) from charges where id='"+charge+"'").list();
				if(checkBreakPoints!=null && (!(checkBreakPoints.isEmpty())) && checkBreakPoints.get(0)!=null && (!(checkBreakPoints.get(0).toString().trim().equals("")))){
					try{
					String[] str1 = checkBreakPoints.get(0).toString().split("#");
					checkBreakPoint= str1[0];
					incrementalStep=new Double(str1[1]);
					}catch(Exception e){
						
					}
				}
				if(checkBreakPoint.toString().equalsIgnoreCase("false"))
				{
					
					 List firstQuantityList= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"'  limit 1").list();
					 double firstQuantity=Double.parseDouble(firstQuantityList.get(0).toString());
					 List firstQuantity2List= this.getSession().createSQLQuery("select quantity2 from rategrid where charge='"+charge+"' limit 1").list();
					 double firstQuantity2=Double.parseDouble(firstQuantity2List.get(0).toString());
                     String quantity1Condition="quantity1>='"+quantity+"'";
					 if(Double.parseDouble(quantity)<=firstQuantity) {
						 quantity1Condition="quantity1>'"+firstQuantity+"'";
                     }
					 String quantity2Condition="quantity2>='"+distance+"'";
					 if(Double.parseDouble(distance)<=firstQuantity2) {
						 quantity2Condition="quantity2>'"+firstQuantity2+"'";
                     }
					 
				List rate= getHibernateTemplate().find("select rate1 from RateGrid where quantity1 in(select min(quantity1) from RateGrid where "+quantity1Condition+" and charge='"+charge +"') and quantity2 in(select min(quantity2) from RateGrid where  "+quantity2Condition+" and charge='"+charge +"') and charge='"+charge +"'");	
				if( (rate!=null) && (!(rate.isEmpty())) && (rate.get(0)!=null) && (!(rate.get(0).toString().trim().equals("")))){ 
					rate1B=Double.parseDouble(rate.get(0).toString());
					if(Double.parseDouble(quantity)<=firstQuantity) {
						quantity=firstQuantityList.get(0).toString();
                   }
					 
					 if(Double.parseDouble(distance)<=firstQuantity2) {
						 distance=firstQuantity2List.get(0).toString();
                   }
					if(!(quantity.trim().equals(""))){
						quantity1B=	Double.parseDouble(quantity);
					}
					if(!(distance.trim().equals(""))){
						quantity2B=	Double.parseDouble(distance);
					}
					totalRate=rate1B;
					totalQuantity=quantity1B*quantity2B;
					list.add(totalRate.toString());
					list.add("");
					list.add(totalQuantity.toString());
				}else{
					List rateList= getHibernateTemplate().find("select rate1 from RateGrid where quantity1 in(select max(quantity1) from RateGrid where quantity1<='"+quantity+"' and charge='"+charge +"') and quantity2 in(select max(quantity2) from RateGrid where quantity2 <= '"+distance+"' and charge='"+charge +"') and charge='"+charge +"'");		
					if( (rateList!=null) && (!(rateList.isEmpty())) && (rateList.get(0)!=null)){
						rate1B=Double.parseDouble(rateList.get(0).toString());
						if(!(quantity.trim().equals(""))){
							quantity1B=	Double.parseDouble(quantity);
						}
						if(!(distance.trim().equals(""))){
							quantity2B=	Double.parseDouble(distance);
						}
						totalRate=rate1B;
						totalQuantity=quantity1B*quantity2B;
						list.add(totalRate.toString());
						list.add("Minimum distance of "+quantity1+" "+quantityItemRevised+" applied");
						list.add(totalQuantity.toString());	
					}else{
						list.add("1");
						list.add("");
						list.add("1");
					}
				}
				
				
			}else{
				double inputQuantity=0.00;
				double inputDistance=0.00;
				double firstQuantity1=0.00;
				double firstQuantity2=0.00;
				double secondQuantity1=0.00;
				double secondQuantity2=0.00;
				double firstRate1=0.00;
				double secondRate1=0.00;
				double thirdRate1=0.00;
				double fourthRate1=0.00;
				double firstTotalRate1=0.00; 
				double secondTotalRate1=0.00;
				double thirdTotalRate1=0.00;
				double fourthTotalRate1=0.00;
				double firstTotalQuantity=0.00; 
				double secondTotalQuantity=0.00; 
				double thirdTotalQuantity=0.00; 
				double fourthTotalQuantity=0.00; 
				if(quantity!=null && (!(quantity.trim().equals("")))){
					inputQuantity=Double.parseDouble(quantity);	
				}
				if(distance!=null && (!(distance.trim().equals("")))){
					inputDistance=Double.parseDouble(distance);	
				}
				List firstQuantity1List= getHibernateTemplate().find("select quantity1 from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and quantity2 in(select min(quantity2) from RateGrid where quantity2 >='"+ distance +"' and charge='"+charge+"') and charge='"+charge +"' ");	
				List firstQuantity2List= getHibernateTemplate().find("select quantity2 from RateGrid where quantity2 in(select min(quantity2) from RateGrid where quantity2 >='"+ distance +"' and charge='"+charge+"') and quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"')  and charge='"+charge+"'");		
			    if(firstQuantity1List!=null && (!(firstQuantity1List.isEmpty())) && firstQuantity1List.get(0)!=null && firstQuantity2List!=null && (!(firstQuantity2List.isEmpty())) && firstQuantity2List.get(0)!=null){
			    	firstQuantity1=Double.parseDouble(firstQuantity1List.get(0).toString());
			    	firstQuantity2=Double.parseDouble(firstQuantity2List.get(0).toString());
			   List secondQuantity1List= getHibernateTemplate().find("select quantity1 from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>'"+ firstQuantity1 +"' and charge='"+charge +"') and quantity2 in(select min(quantity2) from RateGrid where quantity2 >'"+ firstQuantity2 +"' and charge='"+charge+"') and charge='"+charge +"' ");	
			   List secondQuantity2List= getHibernateTemplate().find("select quantity2 from RateGrid where quantity2 in(select min(quantity2) from RateGrid where quantity2 >'"+ firstQuantity2 +"' and charge='"+charge+"') and quantity1 in(select min(quantity1) from RateGrid where quantity1>'"+ firstQuantity1 +"' and charge='"+charge +"') and charge='"+charge+"'");			
			   if(secondQuantity1List!=null && (!(secondQuantity1List.isEmpty())) && secondQuantity1List.get(0)!=null && secondQuantity2List!=null && (!(secondQuantity2List.isEmpty())) && secondQuantity2List.get(0)!=null){ 
				   secondQuantity1=Double.parseDouble(secondQuantity1List.get(0).toString());
				   secondQuantity2=Double.parseDouble(secondQuantity2List.get(0).toString());
			   }else{
			     secondQuantity1=firstQuantity1;
				 secondQuantity2=firstQuantity2; 
			   }
			   List<RateGrid> multquantityBothDataList;
			   multquantityBothDataList= getHibernateTemplate().find("from RateGrid where quantity2 in(select quantity2 from RateGrid where quantity2 >='"+distance+"' and charge='"+charge+"' )and charge='"+charge+"' and quantity1 in(select quantity1 from RateGrid where quantity1 >='"+ quantity +"' and charge='"+charge+"') and rate1 is not null and rate1<> '' and quantity1 is not null and quantity1<> '' and quantity2 is not null and quantity2<> '' order by quantity2,quantity1");
			   Map<String, String> multquantityBothDataMap = new LinkedHashMap<String, String>();
				for (RateGrid rateGrid : multquantityBothDataList) {
					try{
					multquantityBothDataMap.put(Double.toString(rateGrid.getQuantity1()) +"~"+Double.toString(rateGrid.getQuantity2()), rateGrid.getRate1());
					}catch(Exception e){
						
					}
				}
				if(multquantityBothDataMap.containsKey(Double.toString(firstQuantity1)+"~"+Double.toString(firstQuantity2))){
				firstRate1=Double.parseDouble(multquantityBothDataMap.get(Double.toString(firstQuantity1)+"~"+Double.toString(firstQuantity2)));
				}
				firstTotalRate1=inputQuantity*inputDistance*firstRate1;
				firstTotalQuantity=inputQuantity*inputDistance;
				if(multquantityBothDataMap.containsKey(Double.toString(secondQuantity1)+"~"+Double.toString(firstQuantity2))){
				secondRate1=Double.parseDouble(multquantityBothDataMap.get(Double.toString(secondQuantity1)+"~"+Double.toString(firstQuantity2)));
				}
				secondTotalRate1=inputDistance*(firstQuantity1+incrementalStep)*secondRate1;
				secondTotalQuantity=inputDistance*(firstQuantity1+incrementalStep);
				if(multquantityBothDataMap.containsKey(Double.toString(firstQuantity1)+"~"+Double.toString(secondQuantity2))){
				thirdRate1=Double.parseDouble(multquantityBothDataMap.get(Double.toString(firstQuantity1)+"~"+Double.toString(secondQuantity2)));
				}
				thirdTotalRate1=(firstQuantity2+incrementalStep)*inputQuantity*thirdRate1;
				thirdTotalQuantity=(firstQuantity2+incrementalStep)*inputQuantity;
				if(multquantityBothDataMap.containsKey(Double.toString(secondQuantity1)+"~"+Double.toString(secondQuantity2))){
				fourthRate1=Double.parseDouble(multquantityBothDataMap.get(Double.toString(secondQuantity1)+"~"+Double.toString(secondQuantity2)));
				}
				fourthTotalRate1=(firstQuantity2+incrementalStep)*(firstQuantity1+incrementalStep)*fourthRate1;
				fourthTotalQuantity=(firstQuantity2+incrementalStep)*(firstQuantity1+incrementalStep);
				
				SortedMap dataMap= new TreeMap();
				dataMap.put(firstTotalRate1, ""+firstTotalQuantity+"#"+firstRate1);
				dataMap.put(secondTotalRate1, ""+secondTotalQuantity+"#"+secondRate1);
				dataMap.put(thirdTotalRate1, ""+thirdTotalQuantity+"#"+thirdRate1);
				dataMap.put(fourthTotalRate1, ""+fourthTotalQuantity+"#"+fourthRate1);
				String finalData =dataMap.firstKey().toString();
				String finalRate= "1";
				String finalQuantity= "1";
				String finalDataValue= "1";
				if(dataMap.containsKey(dataMap.firstKey())){
					finalDataValue=	dataMap.get(dataMap.firstKey()).toString();
					String finalDataArray[]=finalDataValue.split("#");
					finalQuantity=finalDataArray[0];
					finalRate=finalDataArray[1];
				}
				s1="Break-Point applied ";
				list.add(finalRate);
				list.add(s1);
				list.add(finalQuantity);
			    }else{
					
					 List firstQuantityList= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"'  limit 1").list();
					 double firstQuantity=Double.parseDouble(firstQuantityList.get(0).toString());
					 List firstQuantity2List1= this.getSession().createSQLQuery("select quantity2 from rategrid where charge='"+charge+"' limit 1").list();
					 firstQuantity2=Double.parseDouble(firstQuantity2List1.get(0).toString());
                    String quantity1Condition="quantity1>='"+quantity+"'";
					 if(Double.parseDouble(quantity)<=firstQuantity) {
						 quantity1Condition="quantity1>'"+firstQuantity+"'";
                    }
					 String quantity2Condition="quantity2>='"+distance+"'";
					 if(Double.parseDouble(distance)<=firstQuantity2) {
						 quantity2Condition="quantity2>'"+firstQuantity2+"'";
                    }
					 
				List rate= getHibernateTemplate().find("select id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where "+quantity1Condition+" and charge='"+charge +"') and quantity2 in(select min(quantity2) from RateGrid where  "+quantity2Condition+" and charge='"+charge +"') and charge='"+charge +"'");	
				if( (rate!=null) && (!(rate.isEmpty())) && (rate.get(0)!=null) && (!(rate.get(0).toString().trim().equals("")))){ 
					Long objNext=new Long(0);
					objNext=Long.parseLong(rate.get(0).toString().trim());
					List currentList=this.getSession().createSQLQuery("select concat(rate1,'~',quantity1,'~',quantity2) from rategrid where charge='"+charge+"' and id >= "+objNext + "  limit 2").list();
					if(currentList.size()==1)
					{
					String	currentListData=currentList.get(0).toString();
					String finalDataArray[]=currentListData.split("~");
					
					rate1B=Double.parseDouble(finalDataArray[0].toString());
					if(Double.parseDouble(quantity)<=firstQuantity) {
						quantity=firstQuantityList.get(0).toString();
                  }
					 
					 if(Double.parseDouble(distance)<=firstQuantity2) {
						 distance=firstQuantity2List.get(0).toString();
                  }
					if(!(quantity.trim().equals(""))){
						quantity1B=	Double.parseDouble(quantity);
					}
					if(!(distance.trim().equals(""))){
						quantity2B=	Double.parseDouble(distance);
					}
					
					totalRate=rate1B;
					totalQuantity=quantity1B*quantity2B;
					list.add(totalRate.toString());
					list.add("");
					list.add(totalQuantity.toString());
					}
					else{
						String	currentListData=currentList.get(0).toString();
						String finalDataArray1[]=currentListData.split("~");
						String	currentListData2=currentList.get(1).toString();
						String finalDataArray2[]=currentListData2.split("~");
						rate1B=Double.parseDouble(finalDataArray1[0].toString());
						rate2B=Double.parseDouble(finalDataArray2[0].toString());
						double rateGridQuantity1=Double.parseDouble(finalDataArray1[1].toString());
						double rateGridQuantity2=Double.parseDouble(finalDataArray1[2].toString());
						if(Double.parseDouble(quantity)<=firstQuantity) {
							quantity=firstQuantityList.get(0).toString();
	                      }
						 
						if(Double.parseDouble(distance)<=firstQuantity2) {
							 distance=firstQuantity2List.get(0).toString();
	                      }
						if(!(quantity.trim().equals(""))){
							quantity1B=	Double.parseDouble(quantity);
						}
						if(!(distance.trim().equals(""))){
							quantity2B=	Double.parseDouble(distance);
						}
						
						firstTotalRate1=quantity1B*quantity2B*rate1B;
						firstTotalQuantity=quantity1B*quantity2B;
						if(Double.parseDouble(quantity)<=firstQuantity) {
						secondTotalRate1=quantity1B*(rateGridQuantity2+incrementalStep)*rate2B;
						secondTotalQuantity=quantity1B*(rateGridQuantity2+incrementalStep);
						}
						if(Double.parseDouble(distance)<=firstQuantity2) {
							thirdTotalRate1=quantity2B*(rateGridQuantity1+incrementalStep)*rate2B;
							thirdTotalQuantity=quantity2B*(rateGridQuantity1+incrementalStep);

						}
						
						
						SortedMap dataMap= new TreeMap();
						dataMap.put(firstTotalRate1, ""+firstTotalQuantity+"#"+rate1B);
						if(Double.parseDouble(quantity)<=firstQuantity) {
						dataMap.put(secondTotalRate1, ""+secondTotalQuantity+"#"+rate2B);
						}
						if(Double.parseDouble(distance)<=firstQuantity2) {
						dataMap.put(thirdTotalRate1, ""+thirdTotalQuantity+"#"+rate2B);
						} 
						String finalData =dataMap.firstKey().toString();
						String finalRate= "1";
						String finalQuantity= "1";
						String finalDataValue= "1";
						if(dataMap.containsKey(dataMap.firstKey())){
							finalDataValue=	dataMap.get(dataMap.firstKey()).toString();
							String finalDataArray[]=finalDataValue.split("#");
							finalQuantity=finalDataArray[0];
							finalRate=finalDataArray[1];
						}
						s1="Break-Point applied ";
						list.add(finalRate);
						list.add(s1);
						list.add(finalQuantity);
					}
				}else{
					List rateList= getHibernateTemplate().find("select rate1 from RateGrid where quantity1 in(select max(quantity1) from RateGrid where quantity1<='"+quantity+"' and charge='"+charge +"') and quantity2 in(select max(quantity2) from RateGrid where quantity2 <= '"+distance+"' and charge='"+charge +"') and charge='"+charge +"'");		
					if( (rateList!=null) && (!(rateList.isEmpty())) && (rateList.get(0)!=null)){
						rate1B=Double.parseDouble(rateList.get(0).toString());
						if(!(quantity.trim().equals(""))){
							quantity1B=	Double.parseDouble(quantity);
						}
						if(!(distance.trim().equals(""))){
							quantity2B=	Double.parseDouble(distance);
						}
						totalRate=rate1B;
						totalQuantity=quantity1B*quantity2B;
						list.add(totalRate.toString());
						list.add("Minimum distance of "+quantity1+" "+quantityItemRevised+" applied");
						list.add(totalQuantity.toString());	
					}else{
						list.add("1");
						list.add("");
						list.add("1");
					}
				}
				
				
			}
			}
			}
			}
	return list;
	
}


public List findChargeId(String charge, String contract) {
	return getHibernateTemplate().find("select id from Charges where charge='"+ charge+"' and contract='"+contract+"' and status is true");
}
public List getChargeEntitle(String charge, String billingContract) {
	return getHibernateTemplate().find("select concat(quantityPreset,'#',quantitySource,'#',quantity1,'#',divideMultiply,'#',priceType,'#',perValue,'#',basis,'#',quantity2preset,'#', twoDGridUnit, '#', multquantity ,'#',if(VATExclude,'Y','N') ) from Charges where charge='"+charge+"' AND contract='"+billingContract+"'");
}
public List getChargeEstimate(String charge, String billingContract,String sessionCorpID) {
	String query="select concat(if(quantityEstimatePreset is null or quantityEstimatePreset ='','',quantityEstimatePreset),'#',if(quantityEstimateSource is null or quantityEstimateSource ='','',quantityEstimateSource),'#',if(quantityEstimate is null or quantityEstimate ='','',quantityEstimate),'#',if(priceType is null or priceType ='','',priceType),'#',if(quantity2preset is null or quantity2preset ='','',quantity2preset),'#',if(priceSource is null or priceSource ='','',priceSource),'#',if(priceFormula is null or priceFormula ='','',priceFormula),'#',if(divideMultiply is null or divideMultiply ='','',divideMultiply),'#',if(basis is null or basis ='','',basis),'#',if(perValue is null or perValue ='','',perValue),'#',if(minimum is null or minimum ='','',minimum),'#',if(twoDGridUnit is null or twoDGridUnit ='','',twoDGridUnit),'#',if(multquantity is null or multquantity ='','',multquantity),'#',if(deviation is null or deviation ='','',deviation),'#',if(buyDependSell is null or buyDependSell ='','',buyDependSell),'#',if(VATExclude,'Y','N'),'#',if(contractCurrency is null or contractCurrency ='','',contractCurrency),'#',if(payableContractCurrency is null or payableContractCurrency ='','',payableContractCurrency),'#',if(payablePreset is null or payablePreset ='','',payablePreset),'#',if(payablePriceType is null or payablePriceType ='','',payablePriceType),'#',if(expensePrice is null or expensePrice ='','',expensePrice)) from charges where charge='"+charge+"' AND contract='"+billingContract+"' and corpID='"+sessionCorpID+"'";
	return this.getSession().createSQLQuery(query).list();
}
public List findChargeCode(String charge, String contract) {
	
	return getHibernateTemplate().find("select concat(charge,'#',gl,'#',expGl,'#',contract,'#',description,'#',if(includeLHF,'Y','N'),'#',if(printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(VATExclude,'Y','N'),'#',contractCurrency,'#',payableContractCurrency,'#','','#','','#',if(rollUpInInvoice,'Y','N')) from Charges where charge='"+ charge+"' and contract='"+contract+"' and status is true and charge not in ('DMMFXFEE','DMMFEE')");
}
public List getCharge(String charge,String billingContract,String corpId)
{
	String query="";
	query="select concat( "+
		  " if(quantityRevisedPreset is null,'',quantityRevisedPreset), "+
		  " '#',if(quantity2preset is null,'',quantity2preset), "+
		  " '#',if(quantityItemRevised is null,'',quantityItemRevised), "+
		  " '#',if(divideMultiply is null,'',divideMultiply), "+
		  " '#',if(wording is null,'',wording), "+
		  " '#',if(perValue is null,'',perValue), "+
		  " '#',if(perItem is null,'',perItem), "+
		  " '#',if(quantityRevisedSource is null,'',quantityRevisedSource), "+
		  " '#',if(quantityRevised is null,'',quantityRevised), "+
		  " '#',if(priceType is null,'',priceType), "+
		  " '#',if((priceFormula is null or priceFormula=''),'No',priceFormula), "+
		  " '#',if(basis is null,'',basis), "+
		  " '#',if(description is null,'',description), "+
		  " '#',if(minimum is null,'',minimum), "+
		  " '#',if(checkBreakPoints is null or checkBreakPoints is false,false,true), "+
		  " '#',if(twoDGridUnit is null,'',twoDGridUnit), "+
		  " '#',if(multquantity is null,'',multquantity), "+
		  " '#',if(deviation is null,'',deviation), "+
		  " '#',if(buyDependSell is null,'',buyDependSell), "+
		  " '#',if(VATExclude,'Y','N'), "+
		  " '#',if(contractCurrency is null,'',contractCurrency), "+
		  " '#',if(payableContractCurrency is null,'',payableContractCurrency), "+
		  " '#',if(payablePreset is null,'',payablePreset), "+
		  " '#',if(payablePriceType is null,'',payablePriceType), "+
		  " '#',if((expensePrice is null or expensePrice=''),'No',expensePrice)) "+
		  " from charges where charge='"+charge+"' AND contract='"+billingContract+"' and corpId='"+corpId+"'";
	return this.getSession().createSQLQuery(query).list();
}

public List searchID(){
	return getHibernateTemplate().find("select id from Charges");
}

public List findChargesByContract(String sessionCorpID){
	
	String query= "select c.id, x.contract, c.begin, c.ending, count(*) from charges x, contract c where c.corpid = '"+sessionCorpID+"' and x.corpid = '"+sessionCorpID+"' and x.contract = c.contract and x.status is true group by x.contract having count(*) > 0 order by x.contract, c.begin ";
	List list= this.getSession().createSQLQuery(query).list();	
	//System.out.println("\n\nQUERY::"+query);
	List charges= new ArrayList();
	ChargesDTO chargesDTO;
	Iterator it=list.iterator();
	while(it.hasNext())
	{		 
		Object []obj=(Object[]) it.next();
		
		chargesDTO=new ChargesDTO();
		chargesDTO.setId(obj[0]);
		chargesDTO.setContract(obj[1]);
		chargesDTO.setBegin(obj[2]);
		chargesDTO.setEnding(obj[3]);
		chargesDTO.setCount(obj[4]);
		charges.add(chargesDTO);
	}
	return charges;
}

public List checkChargeCodeListValueNew(String chargeCode,String contract,String corpID){
	
	String query= "select concat(if(VATExclude,'Y','N'),'#',charge,'#',description) from charges where contract='"+contract+"' and charge='"+chargeCode+"' and corpId='"+corpID+"' and status is true";
	List list= this.getSession().createSQLQuery(query).list();	
	return list;
}
public class ChargesDTO implements ListLinkData
{
	
	private Object id;
	private Object contract;
	private Object corpID;
	private Object charge;
	private Object description;
	private Object begin;
	private Object ending;
	private Object count;
	private Object gl;
	private Object includeLHF;
	private Object expGl;
	private Object printOnInvoice;
	private Object costElement;
	private Object buyDependSell;
	private Object VATExclude;
	private Object contractCurrency;
	
	public Object getVATExclude() {
		return VATExclude;
	}
	public void setVATExclude(Object vATExclude) {
		VATExclude = vATExclude;
	}
	public Object getBegin() {
		return begin;
	}
	public void setBegin(Object begin) {
		this.begin = begin;
	}
	public Object getCharge() {
		return charge;
	}
	public void setCharge(Object charge) {
		this.charge = charge;
	}
	public Object getContract() {
		return contract;
	}
	public void setContract(Object contract) {
		this.contract = contract;
	}
	public Object getCorpID() {
		return corpID;
	}
	public void setCorpID(Object corpID) {
		this.corpID = corpID;
	}
	public Object getDescription() {
		return description;
	}
	public void setDescription(Object description) {
		this.description = description;
	}
	public Object getEnding() {
		return ending;
	}
	public void setEnding(Object ending) {
		this.ending = ending;
	}
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
	public Object getCount() {
		return count;
	}
	public void setCount(Object count) {
		this.count = count;
	}
	@Transient
	public String getListCode() {
		String chargeCode=charge.toString();
		return chargeCode; // return the code that needs to be set on the parent
		// page
	}

	@Transient
	public String getListDescription() {
		String descriptions=description.toString();
		return descriptions; // return the description that needs to be
		// set on the parent page
	}

	@Transient
	public String getListSecondDescription() {
		String gls=gl.toString();
		return gls;
	}

	@Transient
	public String getListThirdDescription() {
		String expGls=expGl.toString();
		return expGls;
	}

	@Transient
	public String getListFourthDescription() {
		String descriptions=description.toString();
		return descriptions;
	}

	@Transient
	public String getListFifthDescription() {
		if(includeLHF!=null){
		return this.includeLHF.toString();
		}else{
			return "FALSE";	
		}
	}

	@Transient
	public String getListSixthDescription() {
		if(printOnInvoice!=null){
			return this.printOnInvoice.toString();
			}else{
				return "FALSE";	
			}
	}
	@Transient
	public String getListSeventhDescription() {
		if(costElement!=null){
		return this.costElement.toString();
		}else{
		return "";	
		}
	}
	@Transient
	public String getListEigthDescription() {
		if(buyDependSell!=null){
		return this.buyDependSell.toString();
		}else{
		return "";	
		}
	}

	@Transient
	public String getListNinthDescription() {
		if(VATExclude!=null){
			return this.VATExclude.toString();
			}else{
				return "FALSE";	
			}
	}

	@Transient
	public String getListTenthDescription() {
		return "";
	}
	public Object getExpGl() {
		return expGl;
	}
	public void setExpGl(Object expGl) {
		this.expGl = expGl;
	}
	public Object getGl() {
		return gl;
	}
	public void setGl(Object gl) {
		this.gl = gl;
	}
	public Object getIncludeLHF() {
		return includeLHF;
	}
	public void setIncludeLHF(Object includeLHF) {
		this.includeLHF = includeLHF;
	}
	public Object getPrintOnInvoice() {
		return printOnInvoice;
	}
	public void setPrintOnInvoice(Object printOnInvoice) {
		this.printOnInvoice = printOnInvoice;
	}
	public Object getCostElement() {
		return costElement;
	}
	public void setCostElement(Object costElement) {
		this.costElement = costElement;
	}
	public Object getBuyDependSell() {
		return buyDependSell;
	}
	public void setBuyDependSell(Object buyDependSell) {
		this.buyDependSell = buyDependSell;
	}
	public Object getContractCurrency() {
		return contractCurrency;
	}
	public void setContractCurrency(Object contractCurrency) {
		this.contractCurrency = contractCurrency;
	}
	
	
}
public List getBreakPoints(String chargeId, String sessionCorpID) {
	return getHibernateTemplate().find("select checkBreakPoints from Charges where id='"+chargeId+"' and corpID='"+sessionCorpID+"'");
}
public List getFirstQuantity(String chargeId, String sessionCorpID, String multquantity, String stringDistance, String qty) {
	List firstQuantity=new ArrayList();
	if(multquantity.equalsIgnoreCase("P")){
	firstQuantity= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+chargeId+"' and corpID='"+sessionCorpID+"'  limit 1").list();	
	}else{
		firstQuantity= this.getSession().createSQLQuery("select quantity2 from rategrid where charge='"+chargeId+"' and corpID='"+sessionCorpID+"'  limit 1").list();		
	}
	return firstQuantity;
}
public List findCount(String chargeCode, String contractName) {
	return this.getSession().createSQLQuery("select count(*) from serviceorder s, billing b where s.id = b.id and s.job in ('STG','STO','STF','STL') and s.status not in ('CNCL','CLSD','DWNLD') and b.storageOut is null and b.charge = '"+chargeCode+"' and b.contract = '"+contractName+"'").list();
}
public List findDefaultTemplateContract(String contract, String sessionCorpID) {
	return getHibernateTemplate().find("from Charges where contract in ('"+contract.replaceAll("'", "''")+"') and corpID='"+sessionCorpID+"' and (status is true)");
	
}
public List findDefaultTemplateChargeCode(String chargeCode, String contract) {
	
	return getHibernateTemplate().find("select concat(charge,'#',gl,'#',expGl,'#',contract,'#',description) from Charges where status is true and  charge='"+ chargeCode+"' and contract  in ('"+contract+"') and charge not in ('DMMFXFEE','DMMFEE')");
}
public List checkChargeForContractAvailability(String chargeCde, String contractCde, String sessionCorpID){
	return getHibernateTemplate().find("select charge from Charges where charge='"+chargeCde+"' and contract ='"+contractCde+"' and corpID=?", sessionCorpID);
}
public List findForInternalCostChargeList(String accountCompanyDivision, String sessionCorpID, String originCountry, String destinationCountry, String serviceMode) {
		 String query= "select ch.charge,ch.description,ch.gl,ch.expGl,ch.includeLHF,ch.printOnInvoice,ch.costElement,ch.buyDependSell,ch.VATExclude,ch.contractCurrency from charges ch, contract ct Where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"' and ct.internalCostContract is true and ch.corpID='"+sessionCorpID+"' and (((originCountry like '"+originCountry+"%' or originCountry is null or originCountry='')  or (destinationCountry like '"+destinationCountry+"%' or destinationCountry is null or destinationCountry='')) and (mode like '"+serviceMode+"%' or mode is null or mode='')) and ch.status is true and ch.charge not in ('DMMFXFEE','DMMFEE')";
		 List list= this.getSession().createSQLQuery(query).list();	
	//System.out.println("\n\nQUERY::"+query);
	List charges= new ArrayList();
	ChargesDTO chargesDTO;
	Iterator it=list.iterator();
	while(it.hasNext())
	{		 
		Object []obj=(Object[]) it.next();
		
		chargesDTO=new ChargesDTO();
		chargesDTO.setCharge(obj[0]);
		chargesDTO.setDescription(obj[1]); 
		chargesDTO.setGl(obj[2]); 
		chargesDTO.setExpGl(obj[3]); 
		chargesDTO.setIncludeLHF(obj[4]); 
		chargesDTO.setPrintOnInvoice(obj[5]); 
		chargesDTO.setCostElement(obj[6]); 
		chargesDTO.setBuyDependSell(obj[7]); 
		chargesDTO.setVATExclude(obj[8]);
		chargesDTO.setContractCurrency(obj[9]);
		charges.add(chargesDTO);
	}
	return charges;
}
public List findForChargesInternalCost(String chargeCode, String description, String accountCompanyDivision, String sessionCorpID, String originCountry, String destinationCountry, String serviceMode) {
    String query= "select ch.charge,ch.description,ch.gl,ch.expGl,ch.includeLHF,ch.printOnInvoice,ch.costElement,ch.buyDependSell,ch.VATExclude,ch.contractCurrency  from charges ch, contract ct Where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"' and ct.internalCostContract is true and ch.charge like '" + chargeCode.replaceAll("'", "''") + "%' AND ch.description like '"+ description.replaceAll("'", "''") + "%' and ch.corpID='"+sessionCorpID+"' and (originCountry like '"+originCountry+"%' or originCountry is null or originCountry='')  and (destinationCountry like '"+destinationCountry+"%' or destinationCountry is null or destinationCountry='') and (mode like '"+serviceMode+"%' or mode is null or mode='')";
    //System.out.println("\n\nQUERY::"+query);
    List list= this.getSession().createSQLQuery(query).list();	 
	List charges= new ArrayList();
	ChargesDTO chargesDTO;
	Iterator it=list.iterator();
	while(it.hasNext())
	{		 
		Object []obj=(Object[]) it.next();
		
		chargesDTO=new ChargesDTO();
		chargesDTO.setCharge(obj[0]);
		chargesDTO.setDescription(obj[1]); 
		chargesDTO.setGl(obj[2]); 
		chargesDTO.setExpGl(obj[3]); 
		chargesDTO.setIncludeLHF(obj[4]); 
		chargesDTO.setPrintOnInvoice(obj[5]); 
		chargesDTO.setCostElement(obj[6]); 
		chargesDTO.setBuyDependSell(obj[7]); 
		chargesDTO.setVATExclude(obj[8]);
		chargesDTO.setContractCurrency(obj[9]);
		charges.add(chargesDTO);
	}
	return charges;

}
public List findInternalCostChargeCodeChargeCode(String chargeCode, String accountCompanyDivision, String sessionCorpID) {
	return this.getSession().createSQLQuery("select concat(ch.charge,'#',ch.gl,'#',ch.expGl,'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',ch.buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency) as valueText from charges ch, contract ct where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"' and ct.internalCostContract is true and  ch.charge='"+ chargeCode+"' and ch.corpID='"+sessionCorpID+"' and ch.status is true and ch.charge not in ('DMMFXFEE','DMMFEE')").addScalar("valueText", Hibernate.STRING).list();	 
}
public List findRadioValueInternalCost(String charge, String accountCompanyDivision, String sessionCorpID) {
	return this.getSession().createSQLQuery("select concat(quantityRevised,'#',priceType,'#',quantity1,'#',quantityEstimate) as valueText from charges ch, contract ct where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"' and ct.internalCostContract is true and  ch.charge='"+ charge+"' and ch.corpID='"+sessionCorpID+"'" ).addScalar("valueText", Hibernate.STRING).list();
	
}
public List getInternalCostsCharge(String charge, String accountCompanyDivision, String sessionCorpID) {
	return this.getSession().createSQLQuery("select concat(ch.quantityRevisedPreset,'#',ch.payablePreset,'#',ch.quantityItemRevised,'#',ch.divideMultiply,'#',ch.wording,'#',ch.perValue,'#',ch.perItem,'#',ch.quantityRevisedSource,'#',ch.quantityRevised,'#',ch.priceType,'#',ch.priceFormula,'#',ch.basis,'#',ch.description,'#',ch.minimum,'#',ch.checkBreakPoints ,'#', ch.twoDGridUnit, '#', ch.multquantity,'#', if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency) as valueText from charges ch, contract ct where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"' and ct.internalCostContract is true and  ch.charge='"+charge+"' and ch.corpID='"+sessionCorpID+"'" ).addScalar("valueText", Hibernate.STRING).list();
}
public List findInternalCostsChargeId(String charge, String accountCompanyDivision, String sessionCorpID) {
	return this.getSession().createSQLQuery("select ch.id from charges ch, contract ct where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"' and ct.internalCostContract is true and  ch.charge='"+charge+"' and ch.corpID='"+sessionCorpID+"'").list();
}
public List getInternalCostsChargeEntitle(String charge, String accountCompanyDivision, String sessionCorpID) {
	return this.getSession().createSQLQuery("select concat(ch.quantityPreset,'#',ch.quantitySource,'#',ch.quantity1,'#',ch.divideMultiply,'#',ch.priceType,'#',ch.perValue,'#',ch.basis,'#',ch.payablePreset ,'#', ch.twoDGridUnit,'#', ch.multquantity ,'#',if(ch.VATExclude,'Y','N')) as valueText from charges ch, contract ct where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"' and ct.internalCostContract is true and  ch.charge='"+charge+"' and ch.corpID='"+sessionCorpID+"'").addScalar("valueText", Hibernate.STRING).list();
	
}
public List getInternalCostChargeEstimate(String charge, String accountCompanyDivision, String sessionCorpID) {
	return this.getSession().createSQLQuery("select concat(ch.quantityEstimatePreset,'#',ch.quantityEstimateSource,'#',ch.quantityEstimate,'#',ch.priceType,'#',ch.payablePreset,'#',ch.priceSource,'#',ch.priceFormula,'#',ch.divideMultiply,'#',ch.basis,'#',ch.perValue,'#',ch.minimum ,'#', ch.twoDGridUnit, '#', ch.multquantity,'#',if(ch.VATExclude,'Y','N') ) as valueText from charges ch, contract ct where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"' and ct.internalCostContract is true and  ch.charge='"+charge+"' and ch.corpID='"+sessionCorpID+"'").addScalar("valueText", Hibernate.STRING).list();
	
}
public List findInsuranceLHFAmount(String insuranceLHFAmount, String chargeid) { 
	return  getHibernateTemplate().find("select rate1 from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ insuranceLHFAmount +"' and charge='"+chargeid +"') and charge='"+chargeid +"'");
}
public List defaultChargesByComDiv(String comDiv, String corpId){
	String query= "select ch.charge,ch.description,ch.gl,ch.expGl,ch.includeLHF,ch.printOnInvoice,ch.costElement,ch.buyDependSell,ch.VATExclude from charges ch, contract ct Where ch.contract=ct.contract and ct.companyDivision='"+comDiv+"' and ct.internalCostContract is true and ch.corpID='"+corpId+"'";
	List list= this.getSession().createSQLQuery(query).list();	
	//System.out.println("\n\nQUERY::"+query);
	List charges= new ArrayList();
	ChargesDTO chargesDTO;
	Iterator it=list.iterator();
	while(it.hasNext())
	{		 
		Object []obj=(Object[]) it.next();
		
		chargesDTO=new ChargesDTO();
		chargesDTO.setCharge(obj[0]);
		chargesDTO.setDescription(obj[1]); 
		chargesDTO.setGl(obj[2]); 
		chargesDTO.setExpGl(obj[3]); 
		chargesDTO.setIncludeLHF(obj[4]); 
		chargesDTO.setPrintOnInvoice(obj[5]); 
		chargesDTO.setCostElement(obj[6]); 
		chargesDTO.setBuyDependSell(obj[7]); 
		chargesDTO.setVATExclude(obj[8]);
		charges.add(chargesDTO);
	}
	return charges;
	
}
public List findChargeCodeCostElement(String chargeCode, String contract, String sessionCorpID, String jobType, String routing, String soCompanyDivision) {
	//System.out.println("\n\n\n\n\n select concat(charge,'#',ce.recGl,'#',ce.payGl,'#',contract,'#',c.description,'#',if(includeLHF,'Y','N'),'#',if(printOnInvoice,'Y','N')) as valueText from charges c ,costelement ce where charge='"+ chargeCode+"' and contract='"+contract+"' and c.costElement = ce.costElement and c.corpid='"+sessionCorpID+"' and ce.corpid='"+sessionCorpID+"'");
	 //return this.getSession().createSQLQuery("select concat(charge,'#',ce.recGl,'#',ce.payGl,'#',contract,'#',c.description,'#',if(includeLHF,'Y','N'),'#',if(printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(c.VATExclude,'Y','N'),'#',c.contractCurrency,'#',c.payableContractCurrency,'#',ce.costElement) as valueText from charges c ,costelement ce where charge='"+ chargeCode+"' and contract='"+contract+"' and c.costElement = ce.costElement and c.corpid='"+sessionCorpID+"' and ce.corpid='"+sessionCorpID+"'").addScalar("valueText", Hibernate.STRING).list();
	List costElementDetail = new ArrayList(); 
	String query ="";
	 // if(jobType!=null && !jobType.equalsIgnoreCase("") && routing!=null && !routing.equalsIgnoreCase("")){
	query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias,'#',if(ch.rollUpInInvoice,'Y','N') ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and  ch.charge='"+chargeCode+"' and ch.contract='"+contract+"' and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id  and (cg.gridJob = '"+jobType+"' OR cg.gridJob is null OR cg.gridJob='') and (cg.gridRouting = '"+routing+"' OR cg.gridRouting is null OR cg.gridRouting='')  and (cg.companyDivision = '"+soCompanyDivision+"' OR cg.companyDivision is null OR cg.companyDivision='') and cg.corpID='"+sessionCorpID+"' and ch.status is true and ch.charge not in ('DMMFXFEE','DMMFEE') limit 1 ";
	costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	/* if(costElementDetail ==null || costElementDetail.isEmpty()){	
		 query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and  ch.charge='"+chargeCode+"' and ch.contract='"+contract+"' and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id  and cg.gridJob = '"+jobType+"' and cg.gridRouting = '' and cg.companyDivision = '"+soCompanyDivision+"' and cg.corpID='"+sessionCorpID+"' limit 1 ";
		 costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 }
	 if(costElementDetail ==null || costElementDetail.isEmpty()){
		 query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and  ch.charge='"+chargeCode+"' and ch.contract='"+contract+"' and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id  and cg.gridJob = '' and cg.gridRouting = '"+routing+"' and cg.companyDivision = '"+soCompanyDivision+"' and cg.corpID='"+sessionCorpID+"' limit 1 "; 
		 costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 }
	 if(costElementDetail ==null || costElementDetail.isEmpty()){
		 query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and  ch.charge='"+chargeCode+"' and ch.contract='"+contract+"' and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id  and cg.gridJob = '' and cg.gridRouting = '' and cg.companyDivision = '"+soCompanyDivision+"' and cg.corpID='"+sessionCorpID+"' limit 1 "; 
		 costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 }
	 if(costElementDetail ==null || costElementDetail.isEmpty()){
		 query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and  ch.charge='"+chargeCode+"' and ch.contract='"+contract+"' and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id  and cg.gridJob = '"+jobType+"' and cg.gridRouting = '"+routing+"' and cg.companyDivision = '' and cg.corpID='"+sessionCorpID+"' limit 1 "; 
		 costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 }
	 if(costElementDetail ==null || costElementDetail.isEmpty()){
		 query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and  ch.charge='"+chargeCode+"' and ch.contract='"+contract+"' and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id  and cg.gridJob = '"+jobType+"' and cg.gridRouting = '' and cg.companyDivision = '' and cg.corpID='"+sessionCorpID+"' limit 1 "; 
		 costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 }
	 if(costElementDetail ==null || costElementDetail.isEmpty()){
		 query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and  ch.charge='"+chargeCode+"' and ch.contract='"+contract+"' and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id  and cg.gridJob = '' and cg.gridRouting = '"+routing+"' and cg.companyDivision = '' and cg.corpID='"+sessionCorpID+"' limit 1 "; 
		 costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 }
	 if(costElementDetail ==null || costElementDetail.isEmpty()){
		 query = "select concat(ch.charge,'#',ce.recGl,'#',ce.payGl,'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce where ch.contract=ct.contract and  ch.charge='"+chargeCode+"' and ch.contract='"+contract+"' and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' limit 1 ";
		 costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 }*/
	  /*}else if((jobType==null || jobType.equalsIgnoreCase("")) && (routing!=null && !routing.equalsIgnoreCase(""))){
		  query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and  ch.charge='"+chargeCode+"' and ch.contract='"+contract+"' and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id  and cg.gridJob = '' and cg.gridRouting = '"+routing+"' and cg.corpID='"+sessionCorpID+"' limit 1 ";
	  }else if((jobType!=null && !jobType.equalsIgnoreCase("") ) && (routing==null || routing.equalsIgnoreCase(""))){
		  query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and  ch.charge='"+chargeCode+"' and ch.contract='"+contract+"' and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id  and cg.gridJob = '"+jobType+"' and cg.gridRouting = '' and cg.corpID='"+sessionCorpID+"' limit 1 ";
	  }else{
		  query = "select concat(ch.charge,'#',ce.recGl,'#',ce.payGl,'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce where ch.contract=ct.contract and  ch.charge='"+chargeCode+"' and ch.contract='"+contract+"' and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' limit 1 ";
	  }*/
	if(costElementDetail ==null || costElementDetail.isEmpty()){
		query = "select concat(ch.charge,'#',ce.recGl,'#',ce.payGl,'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias ,'#',if(ch.rollUpInInvoice,'Y','N')) as valueText from charges ch, contract ct, costelement ce where ch.contract=ct.contract and  ch.charge='"+chargeCode+"' and ch.contract='"+contract+"' and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and ch.status is true and ch.charge not in ('DMMFXFEE','DMMFEE') limit 1 ";
		costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	}
	 // return this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 return costElementDetail;
}
public List findInternalCostChargeCodeCostElement(String chargeCode, String accountCompanyDivision, String sessionCorpID, String jobType, String routing, String soCompanyDivision) {
	//System.out.println("\n\n\n\n\nselect concat(ch.charge,'#',ce.recGl,'#',ce.payGl,'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N')) as valueText from charges ch, contract ct, costelement ce  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"'");
	//return this.getSession().createSQLQuery("select concat(ch.charge,'#',ce.recGl,'#',ce.payGl,'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement) as valueText from charges ch, contract ct, costelement ce  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"'").addScalar("valueText", Hibernate.STRING).list();
	List costElementDetail = new ArrayList();
	String query ="";
	// if(jobType!=null && !jobType.equalsIgnoreCase("") && routing!=null && !routing.equalsIgnoreCase("")){
	query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#', ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and (cg.gridJob = '"+jobType+"' OR cg.gridJob is null OR cg.gridJob='') and (cg.gridRouting = '"+routing+"' OR cg.gridRouting is null OR cg.gridRouting='') and (cg.companyDivision='"+soCompanyDivision+"' OR cg.companyDivision is null OR cg.companyDivision='') and cg.corpID='"+sessionCorpID+"' and ch.status is true and ch.charge not in ('DMMFXFEE','DMMFEE') limit 1 ";
	costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 /*if(costElementDetail ==null || costElementDetail.isEmpty()){
		query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#',ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '"+jobType+"' and cg.gridRouting = '' and cg.companyDivision='"+soCompanyDivision+"' and cg.corpID='"+sessionCorpID+"' limit 1";
		costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 }
	 if(costElementDetail ==null || costElementDetail.isEmpty()){
		query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#',ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '' and cg.gridRouting = '"+routing+"' and cg.companyDivision='"+soCompanyDivision+"'  and cg.corpID='"+sessionCorpID+"' limit 1 ";
		costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 }
	 if(costElementDetail ==null || costElementDetail.isEmpty()){
			query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#',ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '' and cg.gridRouting = '' and cg.companyDivision='"+soCompanyDivision+"'  and cg.corpID='"+sessionCorpID+"' limit 1 ";
			costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
		 }
	 if(costElementDetail ==null || costElementDetail.isEmpty()){
			query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#',ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '"+jobType+"' and cg.gridRouting = '"+routing+"' and cg.companyDivision=''  and cg.corpID='"+sessionCorpID+"' limit 1 ";
			costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
		 }
	 if(costElementDetail ==null || costElementDetail.isEmpty()){
			query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#',ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '"+jobType+"' and cg.gridRouting = '' and cg.companyDivision=''  and cg.corpID='"+sessionCorpID+"' limit 1 ";
			costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
		 }
	 if(costElementDetail ==null || costElementDetail.isEmpty()){
			query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#',ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '' and cg.gridRouting = '"+routing+"' and cg.companyDivision=''  and cg.corpID='"+sessionCorpID+"' limit 1 ";
			costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
		 }
	 if(costElementDetail ==null || costElementDetail.isEmpty()){
		 query = "select concat(ch.charge,'#',ce.recGl,,'#', ce.payGl , '#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#',ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' limit 1 ";
		 costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 }*/
	/*}else if((jobType==null || jobType.equalsIgnoreCase("")) && (routing!=null && !routing.equalsIgnoreCase(""))){
		query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#',ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '' and cg.gridRouting = '"+routing+"' and cg.corpID='"+sessionCorpID+"' limit 1";
	}else if((jobType!=null && !jobType.equalsIgnoreCase("") ) && (routing==null || routing.equalsIgnoreCase(""))){
		query = "select concat(ch.charge,'#',(if(cg.gridRecGl is null or cg.gridRecGl = '',ce.recGl,cg.gridRecGl)),'#',(if(cg.gridPayGl is null or cg.gridPayGl = '', ce.payGl , cg.gridPayGl)),'#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#',ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce , glcoderategrid cg  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and  cg.costElementId=ce.id and cg.gridJob = '"+jobType+"' and cg.gridRouting = '' and cg.corpID='"+sessionCorpID+"' limit 1 ";
	}else{
		query = "select concat(ch.charge,'#',ce.recGl,,'#', ce.payGl , '#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#',ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' limit 1 ";
	}*/
	if(costElementDetail ==null || costElementDetail.isEmpty()){
		query = "select concat(ch.charge,'#',ce.recGl,,'#', ce.payGl , '#',ch.contract,'#',ch.description,'#',if(ch.includeLHF,'Y','N'),'#',if(ch.printOnInvoice,'Y','N'),'#',buyDependSell,'#',if(ch.VATExclude,'Y','N'),'#',ch.contractCurrency,'#',ch.payableContractCurrency,'#',ce.costElement,'#',ce.reportingAlias ) as valueText from charges ch, contract ct, costelement ce  where ch.contract=ct.contract and ct.companyDivision='"+accountCompanyDivision+"'  and ct.internalCostContract is true and  ch.charge='"+chargeCode+"'  and ch.corpID='"+sessionCorpID+"' and ch.costElement = ce.costElement  and ce.corpid='"+sessionCorpID+"' and ch.status is true and ch.charge not in ('DMMFXFEE','DMMFEE') limit 1 ";
		costElementDetail =  this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();	
	}
	//return this.getSession().createSQLQuery(query).addScalar("valueText", Hibernate.STRING).list();
	 return costElementDetail;
}
public String findDeviation(String chargeid, String originCountryCode, String purchase, String sessionCorpID) {
	String buyDevation="100";
	List buyDevationList= getHibernateTemplate().find("select deviation from CountryDeviation where charge='"+chargeid+"' and purchase='"+purchase+"' and countryCode='"+originCountryCode+"' and corpID='"+sessionCorpID+"'  ");
	if(buyDevationList!=null && (!(buyDevationList.isEmpty())) && (buyDevationList.get(0)!=null) && (!(buyDevationList.get(0).toString().trim().equals(""))) ){
		buyDevation=buyDevationList.get(0).toString().trim();	
	}
	return buyDevation;
}
public String findMaxDeviation(String chargeid, String originCountryCode, String destinationCountryCode, String purchase, String sessionCorpID) {
	String buyDevation="100";
	List buyDevationList= getHibernateTemplate().find("select max(deviation) from CountryDeviation where charge='"+chargeid+"' and purchase='"+purchase+"'  and corpID='"+sessionCorpID+"' and (countryCode='"+originCountryCode+"'  or countryCode= '"+destinationCountryCode+"') ");
	if(buyDevationList!=null && (!(buyDevationList.isEmpty())) && (buyDevationList.get(0)!=null) && (!(buyDevationList.get(0).toString().trim().equals(""))) ){
		buyDevation=buyDevationList.get(0).toString().trim();	
	}
	return buyDevation;
}
public String findSumDeviation(String chargeid, String originCountryCode, String destinationCountryCode, String purchase, String sessionCorpID) {
	String devation="100";
	double devationValue=0.00;
	List buyDevationList= getHibernateTemplate().find("select deviation from CountryDeviation where charge='"+chargeid+"'   and corpID='"+sessionCorpID+"'  and purchase='"+purchase+"' and (countryCode='"+originCountryCode+"'  or countryCode= '"+destinationCountryCode+"') ");
	if(buyDevationList!=null && (!(buyDevationList.isEmpty())) && (buyDevationList.get(0)!=null) && (!(buyDevationList.get(0).toString().trim().equals(""))) ){
		Iterator it=buyDevationList.iterator();
		while(it.hasNext()){
		double value=0.00;
		Object obj=it.next();
		if(obj!=null && (!(obj.toString().trim().equals(""))));
		devationValue=devationValue+Double.parseDouble(obj.toString().trim())	;
		}
		if((devationValue<0 && devationValue<-100)||devationValue<100){
			devationValue=devationValue+100;	
		}else{
			devationValue=devationValue-100;
		} 
		try{
		devationValue=Math.abs(devationValue);
		}catch(Exception e){
			
		}
		devation=""+devationValue;	
	}
	return devation;
}
public List findVatCalculationForCharge(String corpId){
	return this.getSession().createSQLQuery("select if(vatCalculation is true,'true','false') from systemdefault where corpID='"+corpId+"';").list();
}
public List findChargesRespectToContract(String contract,String sessionCorpID){
	List check = this.getSession().createSQLQuery("select id from charges where contract = '"+contract+"' and corpID='"+sessionCorpID+"' and status is true").list(); 
	return check;
}
public List getAllGridForCharges(String chargeId , String corpId,	String contract){
	String Query ="select id from rategrid where contractName='"+contract+"' and corpid='"+corpId+"' and charge='"+chargeId+"'";
	List check = this.getSession().createSQLQuery("select id from rategrid where contractName='"+contract+"' and corpid='"+corpId+"' and charge='"+chargeId+"'").list(); 
	return check;
}
public List findBuyRateFromRateGrid(String quantity, String charge, String multquantity, String distance, String twoDGridUnit) {

	rate1="0";   
	List list=new ArrayList();
	String quantityItemRevised="";
	String  gridQuantity2= new String("0");
	String  gridQuantity1= new String("0");
	String checkBreakPoint="false";
	double incrementalStep=0.00;
	if(twoDGridUnit.equals("")||twoDGridUnit.equals("ND")){ 
	List quantityItemRevisedList=getHibernateTemplate().find("select quantityItemRevised from  Charges where id='"+charge+"'");
	if(!quantityItemRevisedList.isEmpty())
	{
		quantityItemRevised=quantityItemRevisedList.get(0).toString();
	}
	List checkBreakPoints=this.getSession().createSQLQuery("select concat(if(checkBreakPoints is true,'true','false'),'#',if((incrementalStep is null or incrementalStep=''),'0.00',incrementalStep)) from charges where id='"+charge+"'").list();
	if(checkBreakPoints!=null && (!(checkBreakPoints.isEmpty())) && checkBreakPoints.get(0)!=null && (!(checkBreakPoints.get(0).toString().trim().equals("")))){
		try{
		String[] str1 = checkBreakPoints.get(0).toString().split("#");
		checkBreakPoint= str1[0];
		incrementalStep=new Double(str1[1]);
		}catch(Exception e){
			
		}
	}
	if(checkBreakPoint.toString().equalsIgnoreCase("false"))
	{
		List firstQuantity= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"' limit 1").list();
		if(Double.parseDouble(quantity) <= Double.parseDouble(firstQuantity.get(0).toString()))
		{
			List rate= getHibernateTemplate().find("select buyRate,quantity1,id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"'");
			if(!rate.isEmpty()){
			Iterator it=rate.iterator();
			Long objNext=new Long(0);
			while(it.hasNext())
			{
				Object []obj=(Object[])it.next();
				objNext=(Long)obj[2];
				//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
				//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
				
			}
			List <Object> currentList = new ArrayList<Object>();
			 currentList=this.getSession().createSQLQuery("select buyRate,quantity1,id from rategrid where charge='"+charge+"' and id >= "+objNext + " limit 2").list();
			
			Iterator itNext=currentList.iterator();
			
			int count=1;
			while(itNext.hasNext())
			{
				Object[] obj=(Object[])itNext.next();
				
			if(count==1){
						    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
							if(rate1==null || rate1.equals("")){
								rate1="0";
							}else{
							
							}
							if(quantity1 == null || quantity1.toString().equals("")){
								quantity1 = new Double(0);
							}else{
							
							 }
						//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
						Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity1));
			}
			else{
						rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
							if(rate2==null || rate2.equals("")){
								rate2="0";
							}else{
							
							}
							if(quantity2==null || quantity2.toString().equals("")){
								quantity2 = new Double(0);
							}else{
							
							}
						//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
							Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity2));
			}
			count++;
			}
			if(quantity==null || quantity.equals("")){
				quantity="0";
			}
			else{
				
			}
			if(rate1==null || rate1.equals("")){
				rate1="0";
			}
			else{}
					//if(Double.parseDouble(quantity) * Long.parseLong(rate1) > Q2){
						
						 //r1= Long.parseLong(rate1);
						rt = new BigDecimal(rate1);
						 s1="Minimum quantity of "+quantity1+" "+quantityItemRevised+" applied";
				
			
			
			//list.add(r1.toString());
			list.add(rt.toString());
			list.add(s1);
			list.add(quantity1);
			//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
		}
		}
		else{
	List rate= getHibernateTemplate().find("select buyRate,quantity1,id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"'");
	if(!rate.isEmpty()){
	Iterator it=rate.iterator();
	Long objNext=new Long(0);
	while(it.hasNext())
	{
		Object []obj=(Object[])it.next();
		objNext=(Long)obj[2];
		//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
		//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
		
	}
	List <Object> currentList = new ArrayList<Object>();
	 currentList=this.getSession().createSQLQuery("select buyRate,quantity1,id from rategrid where charge='"+charge+"' and id >= "+objNext + " limit 2").list();
	 if(currentList.size()==1)
		{
			Iterator itNext=currentList.iterator();
			while(itNext.hasNext())
			{
				Object[] obj=(Object[])itNext.next();
				rate1=(String)obj[0]; 
				quantity1=(Double.valueOf(obj[1].toString()));
				s1="";
				list.add(rate1.toString());
				list.add(s1);
				list.add(quantity);
			}
			
		
		}
	 else{
	Iterator itNext=currentList.iterator();
	
	int count=1;
	while(itNext.hasNext())
	{
		Object[] obj=(Object[])itNext.next();
		
	if(count==1){
				    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
					if(rate1==null || rate1.equals("")){
						rate1="0";
					}else{
					
					}
					if(quantity1 == null || quantity1.toString().equals("")){
						quantity1 = new Double(0);
					}else{
					
					 }
				//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
				Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity1));
	}
	else{
				rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
					if(rate2==null || rate2.equals("")){
						rate2="0";
					}else{
					
					}
					if(quantity2==null || quantity2.toString().equals("")){
						quantity2 = new Double(0);
					}else{
					
					}
				//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
					Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity2));
	}
	count++;
	}
	if(quantity==null || quantity.equals("")){
		quantity="0";
	}
	else{
		
	}
	if(rate1==null || rate1.equals("")){
		rate1="0";
	}
	else{}
			//if(Double.parseDouble(quantity) * Long.parseLong(rate1) > Q2){
				
				rt = new BigDecimal(rate1);
				 s1="";
		
	//list.add(r1.toString());
	list.add(rt.toString());
	list.add(s1);
	list.add(quantity);
	//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");

	}
	}
	
	else{
		List find= getHibernateTemplate().find("select buyRate,quantity1,id from RateGrid where quantity1 =(select max(quantity1) from RateGrid where quantity1<='"+ quantity +"' and charge='"+charge +"') and  charge='"+charge +"'");
		Iterator it=find.iterator();
		while(it.hasNext())
		{
			Object []obj=(Object[])it.next();
		
		rate1=(String)obj[0];
		}
		rt = new BigDecimal(rate1);
		s1="";
		list.add(rt.toString());
		list.add(s1);
		list.add(quantity);
	}
	}
	}
	
	else{
		
		List firstQuantity= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"' limit 1").list();
		if(Double.parseDouble(quantity) <= Double.parseDouble(firstQuantity.get(0).toString()))
		{
			List rate= getHibernateTemplate().find("select buyRate,(quantity1+"+incrementalStep+"),id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"'");
			if(!rate.isEmpty()){
			Iterator it=rate.iterator();
			Long objNext=new Long(0);
			while(it.hasNext())
			{
				Object []obj=(Object[])it.next();
				objNext=(Long)obj[2];
				//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
				//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
				
			}
			List <Object> currentList = new ArrayList<Object>();
			 currentList=this.getSession().createSQLQuery("select buyRate,(quantity1+"+incrementalStep+"),id from rategrid where charge='"+charge+"' and id >= "+objNext + " limit 2").list();
			
			Iterator itNext=currentList.iterator();
			
			int count=1;
			while(itNext.hasNext())
			{
				Object[] obj=(Object[])itNext.next();
				
			if(count==1){
						    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
							if(rate1==null || rate1.equals("")){
								rate1="0";
							}else{
							
							}
							if(quantity1 == null || quantity1.toString().equals("")){
								quantity1 = new Double(0);
							}else{
							
							 }
						//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
						Q1=new BigDecimal(rate1).multiply(new BigDecimal(firstQuantity.get(0).toString()));
			}
			else{
						rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
							if(rate2==null || rate2.equals("")){
								rate2="0";
							}else{
							
							}
							if(quantity2==null || quantity2.toString().equals("")){
								quantity2 = new Double(0);
							}else{
							
							}
						//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
							Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity1));
			}
			count++;
			}
			if(quantity==null || quantity.equals("")){
				quantity="0";
			}
			else{
				
			}
			if(rate1==null || rate1.equals("")){
				rate1="0";
			}
			else{}
					
						 //r1= Long.parseLong(rate1);
						rt = new BigDecimal(rate1);
						s1="Minimum quantity of "+quantity1+" "+quantityItemRevised+" applied";
						BigDecimal temp = new BigDecimal(firstQuantity.get(0).toString()).multiply(new BigDecimal(rate1));
						if(temp.doubleValue()> Q2.doubleValue()){
						  //r1=Q2/Long.parseLong(quantity);
							rt = new BigDecimal(rate2);
							
						  s1="Break-Point applied " + " as " +quantity1+ " "+quantityItemRevised+" "+ " @ $ "+ rate2;
					      }
					      else{
						      //r1= Long.parseLong(rate1);
						      rt = new BigDecimal(rate1);
						      s1="";
					      }




			
			//list.add(r1.toString());
			list.add(rt.toString());
			list.add(s1);
			//list.add(quantity1);
			if(temp.doubleValue()> Q2.doubleValue())
			{
				list.add(quantity1);
			}
			else{
				list.add(firstQuantity.get(0).toString());
			}
			//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
			
			}
		}
		
		
		else{
		List rate= getHibernateTemplate().find("select buyRate,(quantity1+"+incrementalStep+"),id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"'");
		if(!rate.isEmpty()){
		Iterator it=rate.iterator();
		Long objNext=new Long(0);
		while(it.hasNext())
		{
			Object []obj=(Object[])it.next();
			objNext=(Long)obj[2];
			//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
			//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
			
		}
		List <Object> currentList = new ArrayList<Object>();
		 currentList=this.getSession().createSQLQuery("select buyRate,(quantity1+"+incrementalStep+"),id from rategrid where charge='"+charge+"' and id >= "+objNext + " limit 2").list();
		if(currentList.size()==1)
		{
			Iterator itNext=currentList.iterator();
			while(itNext.hasNext())
			{
				Object[] obj=(Object[])itNext.next();
				rate1=(String)obj[0]; 
				quantity1=(Double.valueOf(obj[1].toString()));
				s1="";
				list.add(rate1.toString());
				list.add(s1);
				list.add(quantity);
			}
			
		
		}
		else{
		Iterator itNext=currentList.iterator();
		
		int count=1;
		while(itNext.hasNext())
		{
			Object[] obj=(Object[])itNext.next();
			
		if(count==1){
					    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
						if(rate1==null || rate1.equals("")){
							rate1="0";
						}else{
						
						}
						if(quantity1 == null || quantity1.toString().equals("")){
							quantity1 = new Double(0);
						}else{
						
						 }
					//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
					Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity));
		}
		else{
					rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
						if(rate2==null || rate2.equals("")){
							rate2="0";
						}else{
						
						}
						if(quantity2==null || quantity2.toString().equals("")){
							quantity2 = new Double(0);
						}else{
						
						}
					//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
						Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity1));
		}
		count++;
		}
		if(quantity==null || quantity.equals("")){
			quantity="0";
		}
		else{
			
		}
		if(rate1==null || rate1.equals("")){
			rate1="0";
		}
		else{}
				//if(Double.parseDouble(quantity) * Long.parseLong(rate1) > Q2){
					BigDecimal temp = new BigDecimal(quantity).multiply(new BigDecimal(rate1));
					if(temp.doubleValue()> Q2.doubleValue()){
					  //r1=Q2/Long.parseLong(quantity);
						rt = new BigDecimal(rate2);
						
					  s1="Break-Point applied " + " as " +quantity1+ " "+quantityItemRevised+" "+ " @ $ "+ rate2;
				}
				else{
					 //r1= Long.parseLong(rate1);
					rt = new BigDecimal(rate1);
					 s1="";
				}
		
		
		//list.add(r1.toString());
		list.add(rt.toString());
		list.add(s1);
		if(temp.doubleValue()> Q2.doubleValue())
		{
			list.add(quantity1);
		}
		else{
			list.add(quantity);
		}
		//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
		
		}
		}
		else{
			List find= getHibernateTemplate().find("select buyRate,quantity1,id from RateGrid where quantity1 =(select max(quantity1) from RateGrid where quantity1<='"+ quantity +"' and charge='"+charge +"') and  charge='"+charge +"'");
			Iterator it=find.iterator();
			while(it.hasNext())
			{
				Object []obj=(Object[])it.next();
			
			rate1=(String)obj[0];
			}
			rt = new BigDecimal(rate1);
			s1="";
			list.add(rt.toString());
			list.add(s1);
			list.add(quantity);
		}
	}
	}
	}else{
		if(multquantity.equalsIgnoreCase("P")){
			List firstQuantity1= this.getSession().createSQLQuery("select quantity2 from rategrid where charge='"+charge+"' limit 1").list();
			List quantity2List= getHibernateTemplate().find("select quantity2 from RateGrid where quantity2 in(select min(quantity2) from RateGrid where quantity2 >='"+ distance +"' and charge='"+charge+"') and charge='"+charge+"'");	
			if(quantity2List!=null && (!(quantity2List.isEmpty())) && (quantity2List.get(0)!=null)){
				gridQuantity2=quantity2List.get(0).toString();
			}
			if(Double.parseDouble(distance) < Double.parseDouble(firstQuantity1.get(0).toString()))
			{
				quantity2List= getHibernateTemplate().find("select quantity2 from RateGrid where quantity2 in(select min(quantity2) from RateGrid where quantity2 >'"+ firstQuantity1.get(0).toString() +"' and charge='"+charge+"') and charge='"+charge+"'");	
				if(quantity2List!=null && (!(quantity2List.isEmpty())) && (quantity2List.get(0)!=null)){
					gridQuantity2=quantity2List.get(0).toString();
				}
				
			}
			List quantityItemRevisedList=getHibernateTemplate().find("select quantityItemRevised from  Charges where id='"+charge+"'");
			if(!quantityItemRevisedList.isEmpty())
			{
				quantityItemRevised=quantityItemRevisedList.get(0).toString();
			}
			List checkBreakPoints=this.getSession().createSQLQuery("select concat(if(checkBreakPoints is true,'true','false'),'#',if((incrementalStep is null or incrementalStep=''),'0.00',incrementalStep)) from charges where id='"+charge+"'").list();
			if(checkBreakPoints!=null && (!(checkBreakPoints.isEmpty())) && checkBreakPoints.get(0)!=null && (!(checkBreakPoints.get(0).toString().trim().equals("")))){
				try{
				String[] str1 = checkBreakPoints.get(0).toString().split("#");
				checkBreakPoint= str1[0];
				incrementalStep=new Double(str1[1]);
				}catch(Exception e){
					
				}
			}
			if(checkBreakPoint.toString().equalsIgnoreCase("false"))
			{
				List firstQuantity= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"' limit 1").list();
				if(firstQuantity != null && firstQuantity.size() > 0 && Double.parseDouble(quantity) <= Double.parseDouble(firstQuantity.get(0).toString()))
				{
					List rate= this.getSession().createSQLQuery("select buyRate,quantity1,id from rategrid where quantity1 in(select quantity1 from rategrid where charge='"+charge+"' and quantity2='"+gridQuantity2+"') and charge='"+charge +"' and quantity2='"+gridQuantity2+"' limit 1 ").list();
					if(!rate.isEmpty()){
					Iterator it=rate.iterator();
					Long objNext=new Long(0);
					while(it.hasNext())
					{
						Object []obj=(Object[])it.next();
						objNext=Long.parseLong(obj[2].toString());
						//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
						//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
						
					}
					List <Object> currentList = new ArrayList<Object>();
					 currentList=this.getSession().createSQLQuery("select buyRate,quantity1,id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity2='"+gridQuantity2+"' limit 2").list();
					
					Iterator itNext=currentList.iterator();
					
					int count=1;
					while(itNext.hasNext())
					{
						Object[] obj=(Object[])itNext.next();
						
					if(count==1){
								    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
									if(rate1==null || rate1.equals("")){
										rate1="0";
									}else{
									
									}
									if(quantity1 == null || quantity1.toString().equals("")){
										quantity1 = new Double(0);
									}else{
									
									 }
								//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
								Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity1));
					}
					else{
								rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
									if(rate2==null || rate2.equals("")){
										rate2="0";
									}else{
									
									}
									if(quantity2==null || quantity2.toString().equals("")){
										quantity2 = new Double(0);
									}else{
									
									}
								//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
									Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity2));
					}
					count++;
					}
					if(quantity==null || quantity.equals("")){
						quantity="0";
					}
					else{
						
					}
					if(rate1==null || rate1.equals("")){
						rate1="0";
					}
					else{}
							//if(Double.parseDouble(quantity) * Long.parseLong(rate1) > Q2){
								
								 //r1= Long.parseLong(rate1);
								rt = new BigDecimal(rate1);
								 s1="Minimum quantity of "+quantity1+" "+quantityItemRevised+" applied";
						
					
					
					//list.add(r1.toString());
					list.add(rt.toString());
					list.add(s1);
					list.add(firstQuantity.get(0).toString());
					//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
				}
				}
				else{
			List rate= getHibernateTemplate().find("select buyRate,quantity1,id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"' and quantity2='"+gridQuantity2+"'");
			if(!rate.isEmpty()){
			Iterator it=rate.iterator();
			Long objNext=new Long(0);
			while(it.hasNext())
			{
				Object []obj=(Object[])it.next();
				objNext=(Long)obj[2];
				//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
				//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
				
			}
			List <Object> currentList = new ArrayList<Object>();
			 currentList=this.getSession().createSQLQuery("select buyRate,quantity1,id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity2='"+gridQuantity2+"'  limit 2").list();
			 if(currentList.size()==1)
				{
					Iterator itNext=currentList.iterator();
					while(itNext.hasNext())
					{
						Object[] obj=(Object[])itNext.next();
						rate1=(String)obj[0]; 
						quantity1=(Double.valueOf(obj[1].toString()));
						s1="";
						list.add(rate1.toString());
						list.add(s1);
						list.add(quantity);
					}
					
				
				}
			 else{
			Iterator itNext=currentList.iterator();
			
			int count=1;
			while(itNext.hasNext())
			{
				Object[] obj=(Object[])itNext.next();
				
			if(count==1){
						    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
							if(rate1==null || rate1.equals("")){
								rate1="0";
							}else{
							
							}
							if(quantity1 == null || quantity1.toString().equals("")){
								quantity1 = new Double(0);
							}else{
							
							 }
						//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
						Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity1));
			}
			else{
						rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
							if(rate2==null || rate2.equals("")){
								rate2="0";
							}else{
							
							}
							if(quantity2==null || quantity2.toString().equals("")){
								quantity2 = new Double(0);
							}else{
							
							}
						//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
							Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity2));
			}
			count++;
			}
			if(quantity==null || quantity.equals("")){
				quantity="0";
			}
			else{
				
			}
			if(rate1==null || rate1.equals("")){
				rate1="0";
			}
			else{}
					//if(Double.parseDouble(quantity) * Long.parseLong(rate1) > Q2){
						
						rt = new BigDecimal(rate1);
						 s1="";
				
			//list.add(r1.toString());
			list.add(rt.toString());
			list.add(s1);
			list.add(quantity);
			//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");

			}
			}
			
			else{
				List find= getHibernateTemplate().find("select buyRate,quantity1,id from RateGrid where quantity1 =(select max(quantity1) from RateGrid where quantity1<='"+ quantity +"' and charge='"+charge +"') and  charge='"+charge +"'  and quantity2='"+gridQuantity2+"'");
				Iterator it=find.iterator();
				while(it.hasNext())
				{
					Object []obj=(Object[])it.next();
				
				rate1=(String)obj[0];
				}
				rt = new BigDecimal(rate1);
				s1="";
				list.add(rt.toString());
				list.add(s1);
				list.add(quantity);
			}
			}
			}
			
			else{
				
				List firstQuantity= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"'  limit 1").list();
				if(Double.parseDouble(quantity) <= Double.parseDouble(firstQuantity.get(0).toString()))
				{
					List rate= this.getSession().createSQLQuery("select buyRate,(quantity1+"+incrementalStep+"),id from rategrid where quantity1 in(select quantity1 from rategrid where charge='"+charge+"' and quantity2='"+gridQuantity2+"') and charge='"+charge +"' and quantity2='"+gridQuantity2+"' limit 1 ").list();
					if(!rate.isEmpty()){
					Iterator it=rate.iterator();
					Long objNext=new Long(0);
					while(it.hasNext())
					{
						Object []obj=(Object[])it.next();
						objNext=Long.parseLong(obj[2].toString());
						//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
						//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
						
					}
					List <Object> currentList = new ArrayList<Object>();
					 currentList=this.getSession().createSQLQuery("select buyRate,(quantity1+"+incrementalStep+"),id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity2='"+gridQuantity2+"' limit 2").list();
					
					Iterator itNext=currentList.iterator();
					
					int count=1;
					while(itNext.hasNext())
					{
						Object[] obj=(Object[])itNext.next();
						
					if(count==1){
								    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
									if(rate1==null || rate1.equals("")){
										rate1="0";
									}else{
									
									}
									if(quantity1 == null || quantity1.toString().equals("")){
										quantity1 = new Double(0);
									}else{
									
									 }
								//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
								Q1=new BigDecimal(rate1).multiply(new BigDecimal(firstQuantity.get(0).toString()));
					}
					else{
								rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
									if(rate2==null || rate2.equals("")){
										rate2="0";
									}else{
									
									}
									if(quantity2==null || quantity2.toString().equals("")){
										quantity2 = new Double(0);
									}else{
									
									}
								//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
									Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity1));
					}
					count++;
					}
					if(quantity==null || quantity.equals("")){
						quantity="0";
					}
					else{
						
					}
					if(rate1==null || rate1.equals("")){
						rate1="0";
					}
					else{}
							
								 //r1= Long.parseLong(rate1);
								rt = new BigDecimal(rate1);
								s1="Minimum quantity of "+quantity1+" "+quantityItemRevised+" applied";
								BigDecimal temp = new BigDecimal(firstQuantity.get(0).toString()).multiply(new BigDecimal(rate1));
								if(temp.doubleValue()> Q2.doubleValue()){
								  //r1=Q2/Long.parseLong(quantity);
									rt = new BigDecimal(rate2);
									
								  s1="Break-Point applied " + " as " +quantity1+ " "+quantityItemRevised+" "+ " @ $ "+ rate2;
							      }
							      else{
								      //r1= Long.parseLong(rate1);
								      rt = new BigDecimal(rate1);
								      s1="";
							      }

					
					//list.add(r1.toString());
					list.add(rt.toString());
					list.add(s1);
					//list.add(quantity1);
					if(temp.doubleValue()> Q2.doubleValue())
					{
						list.add(quantity1);
					}
					else{
						list.add(firstQuantity.get(0).toString());
					}
					//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
					
					}
				}
				
				
				else{
				List rate= getHibernateTemplate().find("select buyRate,(quantity1+"+incrementalStep+"),id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"' and quantity2='"+gridQuantity2+"' ");
				if(!rate.isEmpty()){
				Iterator it=rate.iterator();
				Long objNext=new Long(0);
				while(it.hasNext())
				{
					Object []obj=(Object[])it.next();
					objNext=(Long)obj[2];
					//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
					//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
					
				}
				List <Object> currentList = new ArrayList<Object>();
				 currentList=this.getSession().createSQLQuery("select buyRate,(quantity1+"+incrementalStep+"),id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity2='"+gridQuantity2+"' limit 2").list();
				if(currentList.size()==1)
				{
					Iterator itNext=currentList.iterator();
					while(itNext.hasNext())
					{
						Object[] obj=(Object[])itNext.next();
						rate1=(String)obj[0]; 
						quantity1=(Double.valueOf(obj[1].toString()));
						s1="";
						list.add(rate1.toString());
						list.add(s1);
						list.add(quantity);
					}
					
				
				}
				else{
				Iterator itNext=currentList.iterator();
				
				int count=1;
				while(itNext.hasNext())
				{
					Object[] obj=(Object[])itNext.next();
					
				if(count==1){
							    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
								if(rate1==null || rate1.equals("")){
									rate1="0";
								}else{
								
								}
								if(quantity1 == null || quantity1.toString().equals("")){
									quantity1 = new Double(0);
								}else{
								
								 }
							//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
							Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity));
				}
				else{
							rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
								if(rate2==null || rate2.equals("")){
									rate2="0";
								}else{
								
								}
								if(quantity2==null || quantity2.toString().equals("")){
									quantity2 = new Double(0);
								}else{
								
								}
							//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
								Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity1));
				}
				count++;
				}
				if(quantity==null || quantity.equals("")){
					quantity="0";
				}
				else{
					
				}
				if(rate1==null || rate1.equals("")){
					rate1="0";
				}
				else{}
						//if(Double.parseDouble(quantity) * Long.parseLong(rate1) > Q2){
							BigDecimal temp = new BigDecimal(quantity).multiply(new BigDecimal(rate1));
							if(temp.doubleValue()> Q2.doubleValue()){
							  //r1=Q2/Long.parseLong(quantity);
								rt = new BigDecimal(rate2);
								
							  s1="Break-Point applied " + " as " +quantity1+ " "+quantityItemRevised+" "+ " @ $ "+ rate2;
						}
						else{
							 //r1= Long.parseLong(rate1);
							rt = new BigDecimal(rate1);
							 s1="";
						}
				
				
				//list.add(r1.toString());
				list.add(rt.toString());
				list.add(s1);
				if(temp.doubleValue()> Q2.doubleValue())
				{
					list.add(quantity1);
				}
				else{
					list.add(quantity);
				}
				//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
				
				}
				}
				else{
					List find= getHibernateTemplate().find("select buyRate,quantity1,id from RateGrid where quantity1 =(select max(quantity1) from RateGrid where quantity1<='"+ quantity +"' and charge='"+charge +"') and  charge='"+charge +"' and quantity2='"+gridQuantity2+"'");
					Iterator it=find.iterator();
					while(it.hasNext())
					{
						Object []obj=(Object[])it.next();
					
					rate1=(String)obj[0];
					}
					rt = new BigDecimal(rate1);
					s1="";
					list.add(rt.toString());
					list.add(s1);
					list.add(quantity);
				}
			}
			}
			}else if(multquantity.equalsIgnoreCase("S")){
				List firstQuantity1= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"' limit 1").list();
				List quantity1List= getHibernateTemplate().find("select quantity1 from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and charge='"+charge +"' ");	
				if(quantity1List!=null && (!(quantity1List.isEmpty())) && (quantity1List.get(0)!=null)){
					gridQuantity1=quantity1List.get(0).toString();
				}
				if(Double.parseDouble(quantity) < Double.parseDouble(firstQuantity1.get(0).toString()))
				{
					quantity1List= getHibernateTemplate().find("select quantity1 from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>'"+ firstQuantity1.get(0).toString() +"' and charge='"+charge +"') and charge='"+charge +"' ");	
					if(quantity1List!=null && (!(quantity1List.isEmpty())) && (quantity1List.get(0)!=null)){
						gridQuantity1=quantity1List.get(0).toString();
					}
					
				}
				List quantityItemRevisedList=getHibernateTemplate().find("select quantityItemRevised from  Charges where id='"+charge+"'");
				if(!quantityItemRevisedList.isEmpty())
				{
					quantityItemRevised=quantityItemRevisedList.get(0).toString();
				}
				List checkBreakPoints=this.getSession().createSQLQuery("select concat(if(checkBreakPoints is true,'true','false'),'#',if((incrementalStep is null or incrementalStep=''),'0.00',incrementalStep)) from charges where id='"+charge+"'").list();
				if(checkBreakPoints!=null && (!(checkBreakPoints.isEmpty())) && checkBreakPoints.get(0)!=null && (!(checkBreakPoints.get(0).toString().trim().equals("")))){
					try{
					String[] str1 = checkBreakPoints.get(0).toString().split("#");
					checkBreakPoint= str1[0];
					incrementalStep=new Double(str1[1]);
					}catch(Exception e){
						
					}
				}
				if(checkBreakPoint.toString().equalsIgnoreCase("false"))
				{
					List firstQuantity= this.getSession().createSQLQuery("select quantity2 from rategrid where charge='"+charge+"' limit 1").list();
					if(Double.parseDouble(distance) <= Double.parseDouble(firstQuantity.get(0).toString()))
					{
						List rate= this.getSession().createSQLQuery("select buyRate,quantity2,id from rategrid where quantity2 in(select quantity2 from rategrid where charge='"+charge+"' and quantity1='"+gridQuantity1+"') and charge='"+charge +"' and quantity1='"+gridQuantity1+"'  limit 1").list();
						if(!rate.isEmpty()){
						Iterator it=rate.iterator();
						Long objNext=new Long(0);
						while(it.hasNext())
						{
							Object []obj=(Object[])it.next();
							objNext=Long.parseLong(obj[2].toString());
							//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
							//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
							
						}
						List <Object> currentList = new ArrayList<Object>();
						 currentList=this.getSession().createSQLQuery("select buyRate,quantity2,id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity1='"+gridQuantity1+"' limit 2").list();
						
						Iterator itNext=currentList.iterator();
						
						int count=1;
						while(itNext.hasNext())
						{
							Object[] obj=(Object[])itNext.next();
							
						if(count==1){
									    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
										if(rate1==null || rate1.equals("")){
											rate1="0";
										}else{
										
										}
										if(quantity1 == null || quantity1.toString().equals("")){
											quantity1 = new Double(0);
										}else{
										
										 }
									//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
									Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity1));
						}
						else{
									rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
										if(rate2==null || rate2.equals("")){
											rate2="0";
										}else{
										
										}
										if(quantity2==null || quantity2.toString().equals("")){
											quantity2 = new Double(0);
										}else{
										
										}
									//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
										Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity2));
						}
						count++;
						}
						if(distance==null || distance.equals("")){
							distance="0";
						}
						else{
							
						}
						if(rate1==null || rate1.equals("")){
							rate1="0";
						}
						else{}
								//if(Double.parseDouble(distance) * Long.parseLong(rate1) > Q2){
									
									 //r1= Long.parseLong(rate1);
									rt = new BigDecimal(rate1);
									 s1="Minimum distance of "+quantity1+" "+quantityItemRevised+" applied";
							
						
						
						//list.add(r1.toString());
						list.add(rt.toString());
						list.add(s1);
						list.add(firstQuantity.get(0).toString());
						//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
					}
					}
					else{
				List rate= getHibernateTemplate().find("select buyRate,quantity2,id from RateGrid where quantity2 in(select min(quantity2) from RateGrid where quantity2>='"+ distance +"' and charge='"+charge +"') and charge='"+charge +"' and quantity1='"+gridQuantity1+"'");
				if(!rate.isEmpty()){
				Iterator it=rate.iterator();
				Long objNext=new Long(0);
				while(it.hasNext())
				{
					Object []obj=(Object[])it.next();
					objNext=(Long)obj[2];
					//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
					//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
					
				}
				List <Object> currentList = new ArrayList<Object>();
				 currentList=this.getSession().createSQLQuery("select buyRate,quantity2,id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity1='"+gridQuantity1+"'  limit 2").list();
				 if(currentList.size()==1)
					{
						Iterator itNext=currentList.iterator();
						while(itNext.hasNext())
						{
							Object[] obj=(Object[])itNext.next();
							rate1=(String)obj[0]; 
							quantity1=(Double.valueOf(obj[1].toString()));
							s1="";
							list.add(rate1.toString());
							list.add(s1);
							list.add(distance);
						}
						
					
					}
				 else{
				Iterator itNext=currentList.iterator();
				
				int count=1;
				while(itNext.hasNext())
				{
					Object[] obj=(Object[])itNext.next();
					
				if(count==1){
							    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
								if(rate1==null || rate1.equals("")){
									rate1="0";
								}else{
								
								}
								if(quantity1 == null || quantity1.toString().equals("")){
									quantity1 = new Double(0);
								}else{
								
								 }
							//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
							Q1=new BigDecimal(rate1).multiply(new BigDecimal(quantity1));
				}
				else{
							rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
								if(rate2==null || rate2.equals("")){
									rate2="0";
								}else{
								
								}
								if(quantity2==null || quantity2.toString().equals("")){
									quantity2 = new Double(0);
								}else{
								
								}
							//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
								Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity2));
				}
				count++;
				}
				if(distance==null || distance.equals("")){
					distance="0";
				}
				else{
					
				}
				if(rate1==null || rate1.equals("")){
					rate1="0";
				}
				else{}
						//if(Double.parseDouble(distance) * Long.parseLong(rate1) > Q2){
							
							rt = new BigDecimal(rate1);
							 s1="";
					
				//list.add(r1.toString());
				list.add(rt.toString());
				list.add(s1);
				list.add(distance);
				//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");

				}
				}
				
				else{
					List find= getHibernateTemplate().find("select buyRate,quantity2,id from RateGrid where quantity2 =(select max(quantity2) from RateGrid where quantity2<='"+ distance +"' and charge='"+charge +"') and  charge='"+charge +"'  and quantity1='"+gridQuantity1+"'");
					Iterator it=find.iterator();
					while(it.hasNext())
					{
						Object []obj=(Object[])it.next();
					
					rate1=(String)obj[0];
					}
					rt = new BigDecimal(rate1);
					s1="";
					list.add(rt.toString());
					list.add(s1);
					list.add(distance);
				}
				}
				}
				
				else{
					
					List firstQuantity= this.getSession().createSQLQuery("select quantity2 from rategrid where charge='"+charge+"' limit 1").list();
					if(Double.parseDouble(distance) <= Double.parseDouble(firstQuantity.get(0).toString()))
					{
						List rate= this.getSession().createSQLQuery("select buyRate,(quantity2+"+incrementalStep+"),id from rategrid where quantity2 in(select quantity2 from rategrid where charge='"+charge+"' and quantity1='"+gridQuantity1+"') and charge='"+charge +"' and quantity1='"+gridQuantity1+"' limit 1 ").list();
						if(!rate.isEmpty()){
						Iterator it=rate.iterator();
						Long objNext=new Long(0);
						while(it.hasNext())
						{
							Object []obj=(Object[])it.next();
							objNext=Long.parseLong(obj[2].toString());
							//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
							//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
							
						}
						List <Object> currentList = new ArrayList<Object>();
						 currentList=this.getSession().createSQLQuery("select buyRate,(quantity2+"+incrementalStep+"),id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity1='"+gridQuantity1+"' limit 2").list();
						
						Iterator itNext=currentList.iterator();
						
						int count=1;
						while(itNext.hasNext())
						{
							Object[] obj=(Object[])itNext.next();
							
						if(count==1){
									    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
										if(rate1==null || rate1.equals("")){
											rate1="0";
										}else{
										
										}
										if(quantity1 == null || quantity1.toString().equals("")){
											quantity1 = new Double(0);
										}else{
										
										 }
									//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
									Q1=new BigDecimal(rate1).multiply(new BigDecimal(firstQuantity.get(0).toString()));
						}
						else{
									rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
										if(rate2==null || rate2.equals("")){
											rate2="0";
										}else{
										
										}
										if(quantity2==null || quantity2.toString().equals("")){
											quantity2 = new Double(0);
										}else{
										
										}
									//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
										Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity1));
						}
						count++;
						}
						if(distance==null || distance.equals("")){
							distance="0";
						}
						else{
							
						}
						if(rate1==null || rate1.equals("")){
							rate1="0";
						}
						else{}
								
									 //r1= Long.parseLong(rate1);
									rt = new BigDecimal(rate1);
									s1="Minimum distance of "+quantity1+" "+quantityItemRevised+" applied";
						
									BigDecimal temp = new BigDecimal(firstQuantity.get(0).toString()).multiply(new BigDecimal(rate1));
									if(temp.doubleValue()> Q2.doubleValue()){
									  //r1=Q2/Long.parseLong(quantity);
										rt = new BigDecimal(rate2);
										
									  s1="Break-Point applied " + " as " +quantity1+ " "+quantityItemRevised+" "+ " @ $ "+ rate2;
								      }
								      else{
									      //r1= Long.parseLong(rate1);
									      rt = new BigDecimal(rate1);
									      s1="";
								      }

						//list.add(r1.toString());
						list.add(rt.toString());
						list.add(s1);
						//list.add(quantity1);
						if(temp.doubleValue()> Q2.doubleValue())
						{
							list.add(quantity1);
						}
						else{
							list.add(firstQuantity.get(0).toString());
						}
						//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
						
						}
					}
					
					
					else{
					List rate= getHibernateTemplate().find("select buyRate,(quantity2+"+incrementalStep+"),id from RateGrid where quantity2 in(select min(quantity2) from RateGrid where quantity2>='"+ distance +"' and charge='"+charge +"') and charge='"+charge +"' and quantity1='"+gridQuantity1+"' ");
					if(!rate.isEmpty()){
					Iterator it=rate.iterator();
					Long objNext=new Long(0);
					while(it.hasNext())
					{
						Object []obj=(Object[])it.next();
						objNext=(Long)obj[2];
						//System.out.println("\n\n\n\n\n\n\n>>>>>>>>>put rete grid rate Id"+rate);
						//System.out.println("\n\n\n\n\n\n\n\n\n>>>>>>>>>>>after Iterate Id"+obj[2]);
						
					}
					List <Object> currentList = new ArrayList<Object>();
					 currentList=this.getSession().createSQLQuery("select buyRate,(quantity2+"+incrementalStep+"),id from rategrid where charge='"+charge+"' and id >= "+objNext + " and quantity1='"+gridQuantity1+"' limit 2").list();
					if(currentList.size()==1)
					{
						Iterator itNext=currentList.iterator();
						while(itNext.hasNext())
						{
							Object[] obj=(Object[])itNext.next();
							rate1=(String)obj[0]; 
							quantity1=(Double.valueOf(obj[1].toString()));
							s1="";
							list.add(rate1.toString());
							list.add(s1);
							list.add(distance);
						}
						
					
					}
					else{
					Iterator itNext=currentList.iterator();
					
					int count=1;
					while(itNext.hasNext())
					{
						Object[] obj=(Object[])itNext.next();
						
					if(count==1){
								    rate1=(String)obj[0]; quantity1=(Double.valueOf(obj[1].toString()));
									if(rate1==null || rate1.equals("")){
										rate1="0";
									}else{
									
									}
									if(quantity1 == null || quantity1.toString().equals("")){
										quantity1 = new Double(0);
									}else{
									
									 }
								//Q1=Long.parseLong(rate1) * Long.parseLong(quantity1);
								Q1=new BigDecimal(rate1).multiply(new BigDecimal(distance));
					}
					else{
								rate2=(String)obj[0];quantity2=(Double.valueOf(obj[1].toString()));
									if(rate2==null || rate2.equals("")){
										rate2="0";
									}else{
									
									}
									if(quantity2==null || quantity2.toString().equals("")){
										quantity2 = new Double(0);
									}else{
									
									}
								//Q2=Long.parseLong(rate2) * Long.parseLong(quantity2);
									Q2=new BigDecimal(rate2).multiply(new BigDecimal(quantity1));
					}
					count++;
					}
					if(distance==null || distance.equals("")){
						distance="0";
					}
					else{
						
					}
					if(rate1==null || rate1.equals("")){
						rate1="0";
					}
					else{}
							//if(Double.parseDouble(distance) * Long.parseLong(rate1) > Q2){
								BigDecimal temp = new BigDecimal(distance).multiply(new BigDecimal(rate1));
								if(temp.doubleValue()> Q2.doubleValue()){
								  //r1=Q2/Long.parseLong(distance);
									rt = new BigDecimal(rate2);
									
								  s1="Break-Point applied " + " as " +quantity1+ " "+quantityItemRevised+" "+ " @ $ "+ rate2;
							}
							else{
								 //r1= Long.parseLong(rate1);
								rt = new BigDecimal(rate1);
								 s1="";
							}
					
					
					//list.add(r1.toString());
					list.add(rt.toString());
					list.add(s1);
					if(temp.doubleValue()> Q2.doubleValue())
					{
						list.add(quantity1);
					}
					else{
						list.add(distance);
					}
					//System.out.println("\n\n\n\n\n\n\n\n\n>>>In the End"+list+"list\n\n");
					
					}
					}
					else{
						List find= getHibernateTemplate().find("select buyRate,quantity2,id from RateGrid where quantity2 =(select max(quantity2) from RateGrid where quantity2<='"+ distance +"' and charge='"+charge +"') and  charge='"+charge +"' and quantity1='"+gridQuantity1+"'");
						Iterator it=find.iterator();
						while(it.hasNext())
						{
							Object []obj=(Object[])it.next();
						
						rate1=(String)obj[0];
						}
						rt = new BigDecimal(rate1);
						s1="";
						list.add(rt.toString());
						list.add(s1);
						list.add(distance);
					}
				}
				}
				
				
			}
			else  if(multquantity.equalsIgnoreCase("N")){
			
				List rate= getHibernateTemplate().find("select buyRate from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+quantity+"' and charge='"+charge +"') and quantity2 in(select min(quantity2) from RateGrid where quantity2>='"+distance+"' and charge='"+charge +"') and charge='"+charge +"'");	
				if( (rate!=null) && (!(rate.isEmpty())) && (rate.get(0)!=null)){
					
					list.add(rate.get(0).toString());
					list.add("");
					list.add("1");
				}else{
					List rateList= getHibernateTemplate().find("select buyRate from RateGrid where quantity1 in(select max(quantity1) from RateGrid where quantity1<='"+quantity+"' and charge='"+charge +"') and quantity2 in(select max(quantity2) from RateGrid where quantity2 <= '"+distance+"' and charge='"+charge +"') and charge='"+charge +"'");		
					if( (rateList!=null) && (!(rateList.isEmpty())) && (rateList.get(0)!=null)){
						list.add(rateList.get(0).toString());
						list.add("Minimum distance of "+quantity1+" "+quantityItemRevised+" applied");
						list.add("1");	
					}else{
						list.add("1");
						list.add("");
						list.add("1");
					}
				}
				
			}
			else  if(multquantity.equalsIgnoreCase("B")){
				double rate1B=0.00;
				double rate2B=0.00;
				double quantity1B=0.00;
				double quantity2B=0.00;
				Double totalRate=new Double(0.00);
				Double totalQuantity=new Double(0.00);
				List checkBreakPoints=this.getSession().createSQLQuery("select concat(if(checkBreakPoints is true,'true','false'),'#',if((incrementalStep is null or incrementalStep=''),'0.00',incrementalStep)) from charges where id='"+charge+"'").list();
				if(checkBreakPoints!=null && (!(checkBreakPoints.isEmpty())) && checkBreakPoints.get(0)!=null && (!(checkBreakPoints.get(0).toString().trim().equals("")))){
					try{
					String[] str1 = checkBreakPoints.get(0).toString().split("#");
					checkBreakPoint= str1[0];
					incrementalStep=new Double(str1[1]);
					}catch(Exception e){
						
					}
				}
				if(checkBreakPoint.toString().equalsIgnoreCase("false"))
				{ 
					List firstQuantityList= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"'  limit 1").list();
					 double firstQuantity=Double.parseDouble(firstQuantityList.get(0).toString());
					 List firstQuantity2List= this.getSession().createSQLQuery("select quantity2 from rategrid where charge='"+charge+"' limit 1").list();
					 double firstQuantity2=Double.parseDouble(firstQuantity2List.get(0).toString());
                    String quantity1Condition="quantity1>='"+quantity+"'";
					 if(Double.parseDouble(quantity)<=firstQuantity) {
						 quantity1Condition="quantity1>'"+firstQuantity+"'";
                    }
					 String quantity2Condition="quantity2>='"+distance+"'";
					 if(Double.parseDouble(distance)<=firstQuantity2) {
						 quantity2Condition="quantity2>'"+firstQuantity2+"'";
                    }
					 
				List rate= getHibernateTemplate().find("select buyRate from RateGrid where quantity1 in(select min(quantity1) from RateGrid where "+quantity1Condition+" and charge='"+charge +"') and quantity2 in(select min(quantity2) from RateGrid where  "+quantity2Condition+" and charge='"+charge +"') and charge='"+charge +"'");	
				if( (rate!=null) && (!(rate.isEmpty())) && (rate.get(0)!=null) && (!(rate.get(0).toString().trim().equals("")))){ 
					rate1B=Double.parseDouble(rate.get(0).toString());
					if(Double.parseDouble(quantity)<=firstQuantity) {
						quantity=firstQuantityList.get(0).toString();
                   }
					 
					 if(Double.parseDouble(distance)<=firstQuantity2) {
						 distance=firstQuantity2List.get(0).toString();
                   }
					if(!(quantity.trim().equals(""))){
						quantity1B=	Double.parseDouble(quantity);
					}
					if(!(distance.trim().equals(""))){
						quantity2B=	Double.parseDouble(distance);
					}
					totalRate=rate1B;
					totalQuantity=quantity1B*quantity2B;
					list.add(totalRate.toString());
					list.add("");
					list.add(totalQuantity.toString());
				}else{
					List rateList= getHibernateTemplate().find("select buyRate from RateGrid where quantity1 in(select max(quantity1) from RateGrid where quantity1<='"+quantity+"' and charge='"+charge +"') and quantity2 in(select max(quantity2) from RateGrid where quantity2 <= '"+distance+"' and charge='"+charge +"') and charge='"+charge +"'");		
					if( (rateList!=null) && (!(rateList.isEmpty())) && (rateList.get(0)!=null)){
						rate1B=Double.parseDouble(rateList.get(0).toString());
						if(!(quantity.trim().equals(""))){
							quantity1B=	Double.parseDouble(quantity);
						}
						if(!(distance.trim().equals(""))){
							quantity2B=	Double.parseDouble(distance);
						}
						totalRate=rate1B;
						totalQuantity=quantity1B*quantity2B;
						list.add(totalRate.toString());
						list.add("Minimum distance of "+quantity1+" "+quantityItemRevised+" applied");
						list.add(totalQuantity.toString());	
					}else{
						list.add("1");
						list.add("");
						list.add("1");
					}
				}
				
				
			}else{
				double inputQuantity=0.00;
				double inputDistance=0.00;
				double firstQuantity1=0.00;
				double firstQuantity2=0.00;
				double secondQuantity1=0.00;
				double secondQuantity2=0.00;
				double firstRate1=0.00;
				double secondRate1=0.00;
				double thirdRate1=0.00;
				double fourthRate1=0.00;
				double firstTotalRate1=0.00; 
				double secondTotalRate1=0.00;
				double thirdTotalRate1=0.00;
				double fourthTotalRate1=0.00;
				double firstTotalQuantity=0.00; 
				double secondTotalQuantity=0.00; 
				double thirdTotalQuantity=0.00; 
				double fourthTotalQuantity=0.00; 
				if(quantity!=null && (!(quantity.trim().equals("")))){
					inputQuantity=Double.parseDouble(quantity);	
				}
				if(distance!=null && (!(distance.trim().equals("")))){
					inputDistance=Double.parseDouble(distance);	
				}
				List firstQuantity1List= getHibernateTemplate().find("select quantity1 from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"') and quantity2 in(select min(quantity2) from RateGrid where quantity2 >='"+ distance +"' and charge='"+charge+"') and charge='"+charge +"' ");	
				List firstQuantity2List= getHibernateTemplate().find("select quantity2 from RateGrid where quantity2 in(select min(quantity2) from RateGrid where quantity2 >='"+ distance +"' and charge='"+charge+"') and quantity1 in(select min(quantity1) from RateGrid where quantity1>='"+ quantity +"' and charge='"+charge +"')  and charge='"+charge+"'");		
			    if(firstQuantity1List!=null && (!(firstQuantity1List.isEmpty())) && firstQuantity1List.get(0)!=null && firstQuantity2List!=null && (!(firstQuantity2List.isEmpty())) && firstQuantity2List.get(0)!=null){
			    	firstQuantity1=Double.parseDouble(firstQuantity1List.get(0).toString());
			    	firstQuantity2=Double.parseDouble(firstQuantity2List.get(0).toString());
			   List secondQuantity1List= getHibernateTemplate().find("select quantity1 from RateGrid where quantity1 in(select min(quantity1) from RateGrid where quantity1>'"+ firstQuantity1 +"' and charge='"+charge +"') and quantity2 in(select min(quantity2) from RateGrid where quantity2 >'"+ firstQuantity2 +"' and charge='"+charge+"') and charge='"+charge +"' ");	
			   List secondQuantity2List= getHibernateTemplate().find("select quantity2 from RateGrid where quantity2 in(select min(quantity2) from RateGrid where quantity2 >'"+ firstQuantity2 +"' and charge='"+charge+"') and quantity1 in(select min(quantity1) from RateGrid where quantity1>'"+ firstQuantity1 +"' and charge='"+charge +"') and charge='"+charge+"'");			
			   if(secondQuantity1List!=null && (!(secondQuantity1List.isEmpty())) && secondQuantity1List.get(0)!=null && secondQuantity2List!=null && (!(secondQuantity2List.isEmpty())) && secondQuantity2List.get(0)!=null){ 
				   secondQuantity1=Double.parseDouble(secondQuantity1List.get(0).toString());
				   secondQuantity2=Double.parseDouble(secondQuantity2List.get(0).toString());
			   }else{
			     secondQuantity1=firstQuantity1;
				 secondQuantity2=firstQuantity2; 
			   }
			   List<RateGrid> multquantityBothDataList;
			   multquantityBothDataList= getHibernateTemplate().find("from RateGrid where quantity2 in(select quantity2 from RateGrid where quantity2 >='"+distance+"' and charge='"+charge+"' )and charge='"+charge+"' and quantity1 in(select quantity1 from RateGrid where quantity1 >='"+ quantity +"' and charge='"+charge+"') and buyRate is not null and buyRate<> '' and quantity1 is not null and quantity1<> '' and quantity2 is not null and quantity2<> '' order by quantity2,quantity1");
			   Map<String, String> multquantityBothDataMap = new LinkedHashMap<String, String>();
				for (RateGrid rateGrid : multquantityBothDataList) {
					try{
					multquantityBothDataMap.put(Double.toString(rateGrid.getQuantity1()) +"~"+Double.toString(rateGrid.getQuantity2()), rateGrid.getBuyRate());
					}catch(Exception e){
						
					}
				}
				if(multquantityBothDataMap.containsKey(Double.toString(firstQuantity1)+"~"+Double.toString(firstQuantity2))){
				firstRate1=Double.parseDouble(multquantityBothDataMap.get(Double.toString(firstQuantity1)+"~"+Double.toString(firstQuantity2)));
				}
				firstTotalRate1=inputQuantity*inputDistance*firstRate1;
				firstTotalQuantity=inputQuantity*inputDistance;
				if(multquantityBothDataMap.containsKey(Double.toString(secondQuantity1)+"~"+Double.toString(firstQuantity2))){
				secondRate1=Double.parseDouble(multquantityBothDataMap.get(Double.toString(secondQuantity1)+"~"+Double.toString(firstQuantity2)));
				}
				secondTotalRate1=inputDistance*(firstQuantity1+incrementalStep)*secondRate1;
				secondTotalQuantity=inputDistance*(firstQuantity1+incrementalStep);
				if(multquantityBothDataMap.containsKey(Double.toString(firstQuantity1)+"~"+Double.toString(secondQuantity2))){
				thirdRate1=Double.parseDouble(multquantityBothDataMap.get(Double.toString(firstQuantity1)+"~"+Double.toString(secondQuantity2)));
				}
				thirdTotalRate1=(firstQuantity2+incrementalStep)*inputQuantity*thirdRate1;
				thirdTotalQuantity=(firstQuantity2+incrementalStep)*inputQuantity;
				if(multquantityBothDataMap.containsKey(Double.toString(secondQuantity1)+"~"+Double.toString(secondQuantity2))){
				fourthRate1=Double.parseDouble(multquantityBothDataMap.get(Double.toString(secondQuantity1)+"~"+Double.toString(secondQuantity2)));
				}
				fourthTotalRate1=(firstQuantity2+incrementalStep)*(firstQuantity1+incrementalStep)*fourthRate1;
				fourthTotalQuantity=(firstQuantity2+incrementalStep)*(firstQuantity1+incrementalStep);
				
				SortedMap dataMap= new TreeMap();
				dataMap.put(firstTotalRate1, ""+firstTotalQuantity+"#"+firstRate1);
				dataMap.put(secondTotalRate1, ""+secondTotalQuantity+"#"+secondRate1);
				dataMap.put(thirdTotalRate1, ""+thirdTotalQuantity+"#"+thirdRate1);
				dataMap.put(fourthTotalRate1, ""+fourthTotalQuantity+"#"+fourthRate1);
				String finalData =dataMap.firstKey().toString();
				String finalRate= "1";
				String finalQuantity= "1";
				String finalDataValue= "1";
				if(dataMap.containsKey(dataMap.firstKey())){
					finalDataValue=	dataMap.get(dataMap.firstKey()).toString();
					String finalDataArray[]=finalDataValue.split("#");
					finalQuantity=finalDataArray[0];
					finalRate=finalDataArray[1];
				}
				s1="Break-Point applied ";
				list.add(finalRate);
				list.add(s1);
				list.add(finalQuantity);
			    }else{
					
					 List firstQuantityList= this.getSession().createSQLQuery("select quantity1 from rategrid where charge='"+charge+"'  limit 1").list();
					 double firstQuantity=Double.parseDouble(firstQuantityList.get(0).toString());
					 List firstQuantity2List1= this.getSession().createSQLQuery("select quantity2 from rategrid where charge='"+charge+"' limit 1").list();
					 firstQuantity2=Double.parseDouble(firstQuantity2List1.get(0).toString());
                   String quantity1Condition="quantity1>='"+quantity+"'";
					 if(Double.parseDouble(quantity)<=firstQuantity) {
						 quantity1Condition="quantity1>'"+firstQuantity+"'";
                   }
					 String quantity2Condition="quantity2>='"+distance+"'";
					 if(Double.parseDouble(distance)<=firstQuantity2) {
						 quantity2Condition="quantity2>'"+firstQuantity2+"'";
                   }
					 
				List rate= getHibernateTemplate().find("select id from RateGrid where quantity1 in(select min(quantity1) from RateGrid where "+quantity1Condition+" and charge='"+charge +"') and quantity2 in(select min(quantity2) from RateGrid where  "+quantity2Condition+" and charge='"+charge +"') and charge='"+charge +"'");	
				if( (rate!=null) && (!(rate.isEmpty())) && (rate.get(0)!=null) && (!(rate.get(0).toString().trim().equals("")))){ 
					Long objNext=new Long(0);
					objNext=Long.parseLong(rate.get(0).toString().trim());
					List currentList=this.getSession().createSQLQuery("select concat(buyRate,'~',quantity1,'~',quantity2) from rategrid where charge='"+charge+"' and id >= "+objNext + "  limit 2").list();
					if(currentList.size()==1)
					{
						String	currentListData=currentList.get(0).toString();
						String finalDataArray[]=currentListData.split("~");
					rate1B=Double.parseDouble(finalDataArray[0].toString());
					if(Double.parseDouble(quantity)<=firstQuantity) {
						quantity=firstQuantityList.get(0).toString();
                 }
					 
					 if(Double.parseDouble(distance)<=firstQuantity2) {
						 distance=firstQuantity2List.get(0).toString();
                 }
					if(!(quantity.trim().equals(""))){
						quantity1B=	Double.parseDouble(quantity);
					}
					if(!(distance.trim().equals(""))){
						quantity2B=	Double.parseDouble(distance);
					}
					
					totalRate=rate1B;
					totalQuantity=quantity1B*quantity2B;
					list.add(totalRate.toString());
					list.add("");
					list.add(totalQuantity.toString());
					}
					else{
						String	currentListData=currentList.get(0).toString();
						String finalDataArray1[]=currentListData.split("~");
						String	currentListData2=currentList.get(1).toString();
						String finalDataArray2[]=currentListData2.split("~");
						rate1B=Double.parseDouble(finalDataArray1[0].toString());
						rate2B=Double.parseDouble(finalDataArray2[0].toString());
						double rateGridQuantity1=Double.parseDouble(finalDataArray1[1].toString());
						double rateGridQuantity2=Double.parseDouble(finalDataArray1[2].toString());
						if(Double.parseDouble(quantity)<=firstQuantity) {
							quantity=firstQuantityList.get(0).toString();
	                      }
						 
						if(Double.parseDouble(distance)<=firstQuantity2) {
							 distance=firstQuantity2List.get(0).toString();
	                      }
						if(!(quantity.trim().equals(""))){
							quantity1B=	Double.parseDouble(quantity);
						}
						if(!(distance.trim().equals(""))){
							quantity2B=	Double.parseDouble(distance);
						}
						
						firstTotalRate1=quantity1B*quantity2B*rate1B;
						firstTotalQuantity=quantity1B*quantity2B;
						if(Double.parseDouble(quantity)<=firstQuantity) {
							secondTotalRate1=quantity1B*(rateGridQuantity2+incrementalStep)*rate2B;
							secondTotalQuantity=quantity1B*(rateGridQuantity2+incrementalStep);
						}
						if(Double.parseDouble(distance)<=firstQuantity2) {
							thirdTotalRate1=quantity2B*(rateGridQuantity1+incrementalStep)*rate2B;
							thirdTotalQuantity=quantity2B*(rateGridQuantity1+incrementalStep);

						}
						
						
						SortedMap dataMap= new TreeMap();
						dataMap.put(firstTotalRate1, ""+firstTotalQuantity+"#"+rate1B);
						if(Double.parseDouble(quantity)<=firstQuantity) {
						dataMap.put(secondTotalRate1, ""+secondTotalQuantity+"#"+rate2B);
						}
						if(Double.parseDouble(distance)<=firstQuantity2) {
						dataMap.put(thirdTotalRate1, ""+thirdTotalQuantity+"#"+rate2B);
						} 
						String finalData =dataMap.firstKey().toString();
						String finalRate= "1";
						String finalQuantity= "1";
						String finalDataValue= "1";
						if(dataMap.containsKey(dataMap.firstKey())){
							finalDataValue=	dataMap.get(dataMap.firstKey()).toString();
							String finalDataArray[]=finalDataValue.split("#");
							finalQuantity=finalDataArray[0];
							finalRate=finalDataArray[1];
						}
						s1="Break-Point applied ";
						list.add(finalRate);
						list.add(s1);
						list.add(finalQuantity);
					}
				}else{
					List rateList= getHibernateTemplate().find("select buyRate from RateGrid where quantity1 in(select max(quantity1) from RateGrid where quantity1<='"+quantity+"' and charge='"+charge +"') and quantity2 in(select max(quantity2) from RateGrid where quantity2 <= '"+distance+"' and charge='"+charge +"') and charge='"+charge +"'");		
					if( (rateList!=null) && (!(rateList.isEmpty())) && (rateList.get(0)!=null)){
						rate1B=Double.parseDouble(rateList.get(0).toString());
						if(!(quantity.trim().equals(""))){
							quantity1B=	Double.parseDouble(quantity);
						}
						if(!(distance.trim().equals(""))){
							quantity2B=	Double.parseDouble(distance);
						}
						totalRate=rate1B;
						totalQuantity=quantity1B*quantity2B;
						list.add(totalRate.toString());
						list.add("Minimum distance of "+quantity1+" "+quantityItemRevised+" applied");
						list.add(totalQuantity.toString());	
					}else{
						list.add("1");
						list.add("");
						list.add("1");
					}
				}
				
				
			}
			}
			}
			}
	return list;
}
public int countContractCharges(String contract,String sessionCorpID ){
	String temp  = this.getSession().createSQLQuery("select count(*) from charges where contract='"+contract+"' and corpID='"+sessionCorpID+"' ").list().get(0).toString();	
    int countValue=Integer.parseInt(temp);
	return countValue;
}
public List findChargeIdForOtherCorpid(String charge, String contract, String externalCorpID) {
	return this.getSession().createSQLQuery("select id from charges where charge='"+ charge+"' and contract='"+contract+"' and corpID='"+externalCorpID+"'").list();
}
public List<CountryDeviation> findCountryDeviation(Long chargeId){
	return getHibernateTemplate().find("from CountryDeviation where charge='"+ chargeId +"'");
}
	public String findCharges(String charge,String contract,String sessionCorpID){
		String query = "from Charges where charge='"+ charge +"' and corpId='"+sessionCorpID+"' and contract='"+contract+"'";
		List<Charges> list = getHibernateTemplate().find(query);
		String payable ="";
		if(list.get(0).getPayablePreset() != null){
			payable = list.get(0).getPayablePreset().toString();
		}
			
		return payable+"-"+list.get(0).getQuantity2preset();
	}
	
	
	public  class CommisionableDTO{		
		 	private Object id;
		  	private Object contract;
			private Object charge; 
			private Object description;
			private Object commissionable; 
			private Object commission;
			private Object thirdParty;
			private Object grossMargin;
			private Object jobType;
			private Object companyDivision;
			private Object internalCostContract;
			private Object bucket;
			private Object earnout;
			
			public Object getId() {
				return id;
			}
			public void setId(Object id) {
				this.id = id;
			}
			public Object getContract() {
				return contract;
			}
			public void setContract(Object contract) {
				this.contract = contract;
			}
			public Object getCharge() {
				return charge;
			}
			public void setCharge(Object charge) {
				this.charge = charge;
			}
			public Object getDescription() {
				return description;
			}
			public void setDescription(Object description) {
				this.description = description;
			}
			public Object getCommissionable() {
				return commissionable;
			}
			public void setCommissionable(Object commissionable) {
				this.commissionable = commissionable;
			}
			public Object getCommission() {
				return commission;
			}
			public void setCommission(Object commission) {
				this.commission = commission;
			}
			public Object getThirdParty() {
				return thirdParty;
			}
			public void setThirdParty(Object thirdParty) {
				this.thirdParty = thirdParty;
			}
			public Object getGrossMargin() {
				return grossMargin;
			}
			public void setGrossMargin(Object grossMargin) {
				this.grossMargin = grossMargin;
			}
			public Object getJobType() {
				return jobType;
			}
			public void setJobType(Object jobType) {
				this.jobType = jobType;
			}
			public Object getCompanyDivision() {
				return companyDivision;
			}
			public void setCompanyDivision(Object companyDivision) {
				this.companyDivision = companyDivision;
			}
			public Object getInternalCostContract() {
				return internalCostContract;
			}
			public void setInternalCostContract(Object internalCostContract) {
				this.internalCostContract = internalCostContract;
			}
			public Object getBucket() {
				return bucket;
			}
			public void setBucket(Object bucket) {
				this.bucket = bucket;
			}
			public Object getEarnout() {
				return earnout;
			}
			public void setEarnout(Object earnout) {
				this.earnout = earnout;
			}
	}
	public List getSaleCommisionableList(String commissionContract,String commissionChargeCode,String commissionJobType,String commissionCompanyDivision,Boolean commissionIsActive,Boolean commissionIsCommissionable,String sessionCorpID){
		List saleCommisionList=new ArrayList();
		String query = "select ch.id,ch.contract,ch.charge,ch.description,ch.commissionable,ch.commission,if(ch.bucket='TP',true,false) as ThirdParty,ch.grossMargin,c.jobType,c.companyDivision,c.internalCostContract,ch.bucket,ch.earnout "+
						"from charges ch, contract c "+
						"where ch.contract=c.contract and ch.corpId='"+sessionCorpID+"' and c.corpId = ch.corpId and ch.charge like '%"+commissionChargeCode+"%' "+
						"and ch.contract like '%"+commissionContract+"%' and commissionable is  "+commissionIsCommissionable+" and c.companyDivision like '%"+commissionCompanyDivision+"%' "+
						 "and  c.jobType like '%"+commissionJobType+"%' ";
		if(commissionIsActive){
			query=query+"  AND ((c.begin is null OR c.begin <= DATE_FORMAT(now(),'%Y-%m-%d')) "+ 
		  	" AND (c.ending is null OR DATE_FORMAT(c.ending,'%Y-%m-%d')>= DATE_FORMAT(now(),'%Y-%m-%d')))";	
		}else{
			query=query+ " AND ((DATE_FORMAT(ending,'%Y-%m-%d')< DATE_FORMAT(now(),'%Y-%m-%d')) " 
			 + " OR (DATE_FORMAT(begin,'%Y-%m-%d')> DATE_FORMAT(now(),'%Y-%m-%d')))";	
		}
		saleCommisionList = this.getSession().createSQLQuery(query).list();
		List charges= new ArrayList();
		CommisionableDTO commisionableDTO;
		Iterator it=saleCommisionList.iterator();
		while(it.hasNext()){		 
			Object []obj=(Object[]) it.next();
			
			commisionableDTO=new CommisionableDTO();
			commisionableDTO.setId(obj[0]);
			commisionableDTO.setContract((obj[1]== null)?"":obj[1]);
			commisionableDTO.setCharge((obj[2]== null)?"":obj[2]);
			commisionableDTO.setDescription((obj[3]== null)?"":obj[3]);
			commisionableDTO.setCommissionable((obj[4]== null)?"":obj[4]);
			commisionableDTO.setCommission((obj[5]== null)?"":obj[5]);
			commisionableDTO.setThirdParty((obj[6]== null)?"":obj[6]);
			commisionableDTO.setGrossMargin((obj[7]== null)?"":obj[7]);
			
			commisionableDTO.setCompanyDivision((obj[9]== null)?"":obj[9]);
			commisionableDTO.setInternalCostContract((obj[10]== null)?"":obj[10]);
			commisionableDTO.setBucket((obj[11]== null)?"":obj[11]);
			commisionableDTO.setEarnout((obj[12]== null)?"":obj[12]);
			
			if(obj[8]!= null && obj[8].toString().trim().length() > 0){ 
				String text="";
				String param ="'"+commisionableDTO.getId().toString()+"','"+commisionableDTO.getCompanyDivision().toString()+"','"+commisionableDTO.getInternalCostContract().toString()+"','"+commisionableDTO.getContract().toString().replaceAll(" ", "&nbsp;")+"','"+commisionableDTO.getCharge().toString().replaceAll(" ", "&nbsp;")+"'";
				for (String string : obj[8].toString().replace("(","").replace(")", "").replaceAll("'", "").split(",")) {
					if(text.equalsIgnoreCase("")){
						text="<a onclick=commisionRateGrid('"+string+"',"+param+")>"+string+"</a>";
					}else{
						text=text+",<a onclick=commisionRateGrid('"+string+"',"+param+")>"+string+"</a>";						
					}
							
				}
				commisionableDTO.setJobType(text);
			}else{
				commisionableDTO.setJobType("");
			}
			
			charges.add(commisionableDTO);
		}					
		return charges;
	}
	public void updateSaleCommission(Long commisionId,Boolean commissionIsCommissionable,String commission,Boolean isThirdParty,Boolean isGrossMargin,String sessionCorpID, String userName){
		String query="";
		try {
			if(isThirdParty){
				query = "update charges set commissionable="+commissionIsCommissionable+",commission='"+commission+"',grossMargin="+isGrossMargin+",bucket='TP',updatedOn=now(),updatedBy='"+userName+"' where id="+commisionId;	
			}else{
				query = "update charges set commissionable="+commissionIsCommissionable+",commission='"+commission+"',grossMargin="+isGrossMargin+",bucket='',updatedOn=now(),updatedBy='"+userName+"'  where id="+commisionId;
			}
			this.getSession().createSQLQuery(query).executeUpdate();
		} catch (Exception e) {}
	}
	public List<Charges> findChargeForSaleCommission(String contract,String chargeCode,String description,String sessionCorpID){
		return getHibernateTemplate().find("from Charges where contract='"+contract.replaceAll("'", "''")+"' and corpID='"+sessionCorpID+"' and charge like '%"+chargeCode.replaceAll("'", "''")+"%' and description like '%"+description+"%'");
	}
	public List<Charges> findChargesList(String charge, String contract,String sessionCorpID) {
		return getHibernateTemplate().find("from Charges where contract='"+contract.replaceAll("'", "''")+"' and corpID='"+sessionCorpID+"' and charge = '"+charge.replaceAll("'", "''")+"'");
	}
	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	public String findPopulateCostElementData(String charge, String contract,String sessionCorpID){
		List temp  = this.getSession().createSQLQuery("select concat(if(costElement is null,'N/A',costElement),'~',if(scostElementDescription is null,'N/A',scostElementDescription)) from charges where contract='"+contract.replaceAll("'", "''")+"' and corpID='"+sessionCorpID+"' and costElement is not null and costElement<>'' and charge = '"+charge.replaceAll("'", "''")+"'").list();
		String costElementData="";
		try {
			if((temp!=null)&&(!temp.isEmpty())&&(temp.get(0)!=null)&&(!temp.get(0).toString().trim().equalsIgnoreCase("")) && (!(temp.get(0).toString().trim().equalsIgnoreCase("~")))){
				costElementData=temp.get(0).toString().trim();
			}
			if(costElementData.trim().equals("~")){
				costElementData="";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return costElementData;
	}
	public Map<String,String> findChargeValExcludeMap(String contract,String sessionCorpID,String shipNumber){
		Map<String,String> chargeValExcludeMap =new HashMap<String, String>();
		String query="";
		query = "select if(c.VATExclude,'T','F'),c.charge,c.contract from charges c,contract ct,accountline a "+
				" where c.contract=ct.contract and a.contract=ct.contract and a.chargeCode=c.charge"+
				" and a.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' and ct.corpID='"+sessionCorpID+"' "+
				" and a.contract='"+contract+"' and a.shipNumber='"+shipNumber+"' and a.status is true";
		List temp  = this.getSession().createSQLQuery(query).list();
		Iterator itr=temp.iterator();
		while(itr.hasNext()){
			Object[] obj=(Object[])itr.next();
			chargeValExcludeMap.put(obj[2]+"~"+obj[1], obj[0].toString());
		}
		return chargeValExcludeMap;
	}
	public void updateChargeStatus(Long soIdNum, Boolean status){
		String query="";
		try {
		    query = "update charges set status="+status+" where id="+soIdNum;
			this.getSession().createSQLQuery(query).executeUpdate();
		} catch (Exception e) {}
	}
	public List<Charges> findContractForCopyCharges(String contract,String corpID){
		//System.out.println("from Charges where contract='"+contract+"' and corpID='"+corpID+"'");
		return getHibernateTemplate().find("from Charges where status is true and contract='"+contract.replaceAll("'", "''")+"' and corpID='"+corpID+"'");
	}
	public void findDeleteChargeRateGridOfOtherCorpId(String contract,String agentsCorpId){
		try {
		List chargeList = this.getSession().createSQLQuery("select id from charges where contract = '"+contract+"' and corpID='"+agentsCorpId+"'").list(); 
		if(chargeList.size()>0){
			String chargesId = chargeList.toString().replace("[", "").replace("]", "");
			String query = "delete from charges where id in("+chargesId+") and corpid='"+agentsCorpId+"'";
			this.getSession().createSQLQuery(query).executeUpdate();
		}
		List rateGridList = this.getSession().createSQLQuery("select id from rategrid where contractName='"+contract+"' and corpid='"+agentsCorpId+"'").list();
		if(rateGridList.size()>0){
			String rateGridId = rateGridList.toString().replace("[", "").replace("]", "");
			String query = "delete from rategrid where id in("+rateGridId+") and corpid='"+agentsCorpId+"'";
			this.getSession().createSQLQuery(query).executeUpdate();
		}
		List countryDeviationList = this.getSession().createSQLQuery("select id from countrydeviation where contractName='"+contract+"' and corpid='"+agentsCorpId+"'").list();
		if(countryDeviationList.size()>0){
			String countryDeviationId = countryDeviationList.toString().replace("[", "").replace("]", "");
			String query = "delete from countrydeviation where id in("+countryDeviationId+") and corpid='"+agentsCorpId+"'";
			this.getSession().createSQLQuery(query).executeUpdate();
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List getTotalChargeCount(String contractName,String sessionCorpID){
		List check = this.getSession().createSQLQuery("select count(id) from charges where contract = '"+contractName+"' and corpID='"+sessionCorpID+"' and status is true").list(); 
		return check;
	}
	
	public List getPresentChargeCountInAgentInstance(String contractName,String agentCorpId){
		List check = this.getSession().createSQLQuery("select count(id) from charges where contract = '"+contractName+"' and corpID='"+agentCorpId+"' and status is true").list(); 
		return check;
	}
	
	public String callCopyCharge(String sessionCorpID,String contract,String contractCopy, String updatedBy,String copyAgentAccount){
		String returnVal="";
		 try {
			 Query q = this.getSession().createSQLQuery(" { call copy_charges(?,?,?,?,?) }");
			 q.setParameter(0,contract);
			 q.setParameter(1,contractCopy);
			 q.setParameter(2,sessionCorpID);
			 q.setParameter(3,updatedBy);
			 q.setParameter(4,copyAgentAccount);
			 q.executeUpdate();
		 } catch (Exception e) {
				e.printStackTrace();
		}
		 return returnVal;
	}
	public List findForPopupChargesContract(String charge, String description, String contract, String corpID, String originCountry, String destCountry, String serviceMode) {
		chargess = getHibernateTemplate().find("from Charges where charge like '%" + charge.replaceAll("'", "''") + "%' AND description like '%"+ description.replaceAll("'", "''") + "%' and contract ='"+contract+"' and corpID='"+corpID+"'and ((originCountry like '"+originCountry+"%' or originCountry is null or originCountry='')  OR (destinationCountry like '"+destCountry+"%' or destinationCountry is null or destinationCountry='') and (mode like '"+serviceMode+"%' or mode is null or mode=''))and status is true and charge not in ('DMMFXFEE','DMMFEE') ");
		return chargess;
}
	
} 


