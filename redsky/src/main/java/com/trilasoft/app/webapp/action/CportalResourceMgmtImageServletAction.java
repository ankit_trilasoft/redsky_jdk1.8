package com.trilasoft.app.webapp.action;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CPortalResourceMgmt;
import com.trilasoft.app.service.CPortalResourceMgmtManager;

public class CportalResourceMgmtImageServletAction extends BaseAction implements Preparable {
	private String sessionCorpID;
	   private String imageURL;
	   private String contentType;
	   private String fileFileName;
	   private CPortalResourceMgmt cPortalResourceMgmt;
	   private CPortalResourceMgmtManager cportalResourceManager;
	   static final Logger logger = Logger.getLogger(CportalResourceMgmtImageServletAction.class);
	   Date currentdate = new Date();
	   InputStream fileInputStream;
		 public InputStream getFileInputStream() {
				return fileInputStream;
			}	
		public CportalResourceMgmtImageServletAction(){
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal(); 
			this.sessionCorpID = user.getCorpID(); 
		}
		public void prepare() throws Exception { 
			
		}
		@SkipValidation
		public String extractFileImageServlet() throws ServletException,IOException{
			 String id = getRequest().getParameter("id");
			 //HttpServletResponse response = getResponse();
			 //ServletOutputStream out = response.getOutputStream();
			 String resultType="success";
				try {
					 if((id!=null)&&(!id.toString().equalsIgnoreCase(""))){
						 logger.warn("ID: "+id+" Start");
						 cPortalResourceMgmt=cportalResourceManager.get(Long.parseLong((id.toString())));
							imageURL = cPortalResourceMgmt.getDocumentLocation();
							contentType = cPortalResourceMgmt.getFileContentType();
						    contentType = "application/octet-stream";
						    fileFileName = cPortalResourceMgmt.getFileFileName();
						    fileFileName=fileFileName.replaceAll(" ", "");
						    logger.warn("ID: "+id+" Start");
					 }else{
						 resultType="error";
					 }
				} catch (Exception e) {
					resultType="error";
				}
				try {
					logger.warn("imageURL: "+imageURL+" Start");
					fileInputStream = new FileInputStream(new File(imageURL));
					logger.warn("imageURL: "+imageURL+" Start");
					return SUCCESS;		
					}
					catch (Exception e) {
						resultType="error";
					}
					return (resultType.equalsIgnoreCase("error")?ERROR:SUCCESS);
		}
		
		public String getSessionCorpID() {
			return sessionCorpID;
		}
		public void setSessionCorpID(String sessionCorpID) {
			this.sessionCorpID = sessionCorpID;
		}
		public String getImageURL() {
			return imageURL;
		}
		public void setImageURL(String imageURL) {
			this.imageURL = imageURL;
		}
		public String getContentType() {
			return contentType;
		}
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
		public String getFileFileName() {
			return fileFileName;
		}
		public void setFileFileName(String fileFileName) {
			this.fileFileName = fileFileName;
		}
		public CPortalResourceMgmt getcPortalResourceMgmt() {
			return cPortalResourceMgmt;
		}
		public void setcPortalResourceMgmt(CPortalResourceMgmt cPortalResourceMgmt) {
			this.cPortalResourceMgmt = cPortalResourceMgmt;
		}
		public void setCportalResourceManager(
				CPortalResourceMgmtManager cportalResourceManager) {
			this.cportalResourceManager = cportalResourceManager;
		}

}
