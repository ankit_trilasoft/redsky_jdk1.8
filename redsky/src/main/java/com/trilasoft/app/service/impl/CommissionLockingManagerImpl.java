package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CommissionLockingDao;
import com.trilasoft.app.model.CommissionLocking;
import com.trilasoft.app.service.CommissionLockingManager;

public class CommissionLockingManagerImpl extends GenericManagerImpl<CommissionLocking, Long> implements CommissionLockingManager{
	CommissionLockingDao commissionLockingDao;
	public CommissionLockingManagerImpl(CommissionLockingDao commissionLockingDao){
		super(commissionLockingDao);
		this.commissionLockingDao=commissionLockingDao;
		
	}
	public List getCommissionLineId(String cid,String corpId,String commisionGiven,Long aid){
		return commissionLockingDao.getCommissionLineId(cid, corpId,commisionGiven,aid);
	}
	public String getCommisionLineId(String corpId,Long aid,String commisionGiven){
		return commissionLockingDao.getCommisionLineId(corpId, aid, commisionGiven);
	}
	public String getSentToCommisionLineId(String corpId,String aidList,String commisionGiven,Long aid){
		return commissionLockingDao.getSentToCommisionLineId(corpId, aidList, commisionGiven,aid);
	}
}
