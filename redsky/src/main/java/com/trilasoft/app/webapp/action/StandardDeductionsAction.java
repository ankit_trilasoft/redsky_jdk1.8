package com.trilasoft.app.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.trilasoft.app.dao.hibernate.StandardDeductionsDaoHibernate.PreviewDTO;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.StandardDeductions;
import com.trilasoft.app.model.SubcontractorCharges;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ExpressionManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.StandardDeductionsManager;
import com.trilasoft.app.service.SubcontractorChargesManager;

public class StandardDeductionsAction extends BaseAction{

		private List standardDeduction;
	    private Long id;
		private Long partnerId;
		private List refMasters;
		private List standardDeductionPreview;
		
		private StandardDeductions standardDeductions;
		private PartnerPublic partner;
		private PartnerPrivate partnerPrivate;
		private ChargesManager chargesManager;
		
		private StandardDeductionsManager standardDeductionsManager;
	    private RefMasterManager refMasterManager;
	    private CustomerFileManager customerFileManager;
	    private PartnerPublicManager partnerPublicManager;
	    private PartnerPrivateManager partnerPrivateManager;
	    private ExpressionManager expressionManager;
		private SubcontractorChargesManager subcontractorChargesManager;
		
		private String sessionCorpID;
	    private String gotoPageString;
		private String validateFormNav;
		private String hitflag;
		private List companyDivis;
		private Date fromDate;
		//private Date toDate;
		private String companyDivision;
		private String process;
		private String userCheck;
		private String ownerPayTo;
		private List subContChargesForFinalizeList;
		
		public String saveOnTabChange() throws Exception {
		    	String s = save(); 
		        validateFormNav = "OK";
		        return gotoPageString;
		}
	    
	    public StandardDeductionsAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
		}
	    
	    public String edit() {
			getComboList(sessionCorpID);
			partner = partnerPublicManager.get(partnerId);
	        if (id != null) {   
	        	standardDeductions = standardDeductionsManager.get(id);
	        } else {   
	        	standardDeductions = new StandardDeductions();
	        	standardDeductions.setCorpID(sessionCorpID);
	        	standardDeductions.setAmtDeducted(new BigDecimal("0.00"));
	        	standardDeductions.setPartnerCode(partner.getPartnerCode());
	        	standardDeductions.setStatus("Open");
	        	standardDeductions.setAmount(new BigDecimal(999999));
	        	standardDeductions.setMaxDeductAmt(new BigDecimal(999999));
	        	standardDeductions.setBalanceAmount(new BigDecimal(999999));
	        	standardDeductions.setLastDeductionDate(new Date());
	        	standardDeductions.setCreatedOn(new Date());
	        	standardDeductions.setUpdatedOn(new Date());
	        }   
	        return SUCCESS;   
	    }   
	    
	    public String save() throws Exception { 
	    	getComboList(sessionCorpID);
	    	partner = partnerPublicManager.get(partnerId);
	        
	    	standardDeductions.setCorpID(sessionCorpID);
	        boolean isNew = (standardDeductions.getId() == null); 

			if(isNew){
				standardDeductions.setCreatedOn(new Date());
	        }
			standardDeductions.setUpdatedOn(new Date());
			standardDeductions.setUpdatedBy(getRequest().getRemoteUser());
			
			standardDeductions = standardDeductionsManager.save(standardDeductions);   
	        hitflag = "1";
	        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		        String key = (isNew) ? "Standard Deductions has been added successfully." : "Standard Deductions has been updated successfully.";   
		        saveMessage(getText(key));   
	        }
	        //standardDeduction = standardDeductionsManager.getSDListByPartnerCode(partner.getPartnerCode());
	        return SUCCESS; 
	    } 
	    
	    public String delete() {
	    	standardDeductions = standardDeductionsManager.get(id);
	    	standardDeductionsManager.remove(id);
			saveMessage(getText("Standard Deductions has been deleted successfully."));
			id = partnerId;
			list();
	    	hitflag = "1";
			return SUCCESS;
		}
	    
	    @SkipValidation 
	    public String list() {
	    	partner = partnerPublicManager.get(id);
	    	standardDeduction = standardDeductionsManager.getSDListByPartnerCode(partner.getPartnerCode());
	        return SUCCESS;   
	    }
	    
	    @SkipValidation
	    public String deductions(){
	    	companyDivis = customerFileManager.findCompanyDivision(sessionCorpID);
	    	return SUCCESS;
	    }
	    
	    @SkipValidation
	    public String deductionProcessing(){
	    	int scCount = 0;
	    	int countMessage = 0;
	    	
	    	List previewSD = standardDeductionsManager.deuctionPreviewList(fromDate, companyDivision, sessionCorpID, "DedProcess", ownerPayTo);
	    	Iterator it = previewSD.iterator();
			while(it.hasNext()){
				BigDecimal dedAmount = new BigDecimal("0.00");
				BigDecimal balancedAmount = new BigDecimal("0.00");				
				BigDecimal personalEscrowAmount = new BigDecimal("0.00");
				Object [] row = (Object[])it.next();
				standardDeductions = standardDeductionsManager.get(new Long (row[0].toString()));
				Date lastDeductionDateSD = null;
				Long chargeId = Long.parseLong(row[14].toString());
				Charges charges = chargesManager.get(chargeId);
				if(charges.getDeductionType() != null && charges.getDeductionType().equalsIgnoreCase("Per Schedule")){
					Map dateProcessMap = processStandardDeduction(charges, fromDate, standardDeductions.getLastDeductionDate());
					Iterator itMap = dateProcessMap.entrySet().iterator();
					while (itMap.hasNext()) {
						Map.Entry entry = (Map.Entry) itMap.next();
						String mapKey = (String) entry.getKey();
						if(mapKey.equalsIgnoreCase("numberOfDeductions")){
							scCount = Integer.parseInt(dateProcessMap.get(mapKey).toString());
						}
						if(mapKey.equalsIgnoreCase("lastDeductionDate")){
							try {
								if(dateProcessMap.get(mapKey) != null){
									lastDeductionDateSD =(new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy")).parse(dateProcessMap.get(mapKey).toString());
								}
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
					}
					
					if(scCount > 0){
						countMessage = scCount;
						if(standardDeductions.getAmtDeducted().intValue() <  standardDeductions.getMaxDeductAmt().intValue()){
							try{
								if(row[4] != null && !(row[4].toString().trim().equalsIgnoreCase("")) && !(row[4].toString().equalsIgnoreCase("0.00")) && !(row[4].toString().equalsIgnoreCase("0"))){
									dedAmount = new BigDecimal(row[4].toString());
								}else{
									if(row[9] != null){//priceType
										String priceType = row[9].toString();
										if(priceType.equalsIgnoreCase("Preset")){
											dedAmount = new BigDecimal(row[10] == null ? "0.00" : row[10].toString());
										}else if(priceType.equalsIgnoreCase("BuildFormula")){
											String expression = row[11] == null ? "0.00" : row[11].toString();
											Map values = new HashMap();
											values.put("standardDeductions.maxDeductAmt", new BigDecimal(row[6] == null ? "0.00" : row[6].toString()));
											values.put("standardDeductions.rate", new BigDecimal(row[5] == null ? "0.00" : row[5].toString()));
											values.put("standardDeductions.deduction", new BigDecimal(row[4] == null ? "0.00" : row[4].toString()));
											dedAmount = (BigDecimal)expressionManager.executeExpression(expression, values);
										}
									}else{
										dedAmount = new BigDecimal("0.00");
									}
								}
							}catch(Exception ex){
								dedAmount = new BigDecimal("0.00");
							}
							
							/*if(standardDeductions.getAmtDeducted().add(dedAmount).intValue() > standardDeductions.getMaxDeductAmt().intValue()){
								dedAmount = standardDeductions.getMaxDeductAmt().subtract(standardDeductions.getAmtDeducted());
							}*/
							
							BigDecimal amtDed=new BigDecimal("0.00");
							for (int i=0; i<scCount; i++){
							List lastUpdatedAmount=standardDeductionsManager.getUpdatedRecord(standardDeductions.getId());
							if(lastUpdatedAmount!=null && (!(lastUpdatedAmount.isEmpty()))){
								amtDed=new BigDecimal(lastUpdatedAmount.get(0).toString());
							}
							
							if(amtDed.add(dedAmount).intValue() > standardDeductions.getMaxDeductAmt().intValue()){
								dedAmount = standardDeductions.getMaxDeductAmt().subtract(amtDed);
							}
							createSubcontractorCharges(row, dedAmount, "finalize", standardDeductions, fromDate, charges.getExpGl());
							String status = "";
							if(amtDed.add(dedAmount).equals(standardDeductions.getMaxDeductAmt())){
								status = "Closed";
							}else{
								status = "";
							}						
							balancedAmount=standardDeductions.getMaxDeductAmt().subtract(amtDed.add(dedAmount));							
							standardDeductionsManager.updateStandardDeduction(standardDeductions.getId(), amtDed.add(dedAmount),balancedAmount, lastDeductionDateSD, status);
							if(standardDeductions.getPersonalEscrow()!=null && standardDeductions.getPersonalEscrow()==true){
								List perEscrowAmount=standardDeductionsManager.partnerPrivatePersonalEscrow(standardDeductions.getPartnerCode(), sessionCorpID);
								personalEscrowAmount=new BigDecimal(perEscrowAmount.get(0).toString()).add(standardDeductions.getDeduction());														
								standardDeductionsManager.updatePartnerPrivate(standardDeductions.getPartnerCode(),sessionCorpID,personalEscrowAmount);
							}
							if(status.equalsIgnoreCase("Closed")){
								break;
							}
							}							
						}
					}
				}
			}
			String dateFrom = "";
			if (fromDate != null){
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(fromDate));
				dateFrom = nowYYYYMMDD1.toString();
			}
			String key = "";
			if(countMessage > 0){
				key = "Deductions processing has been done uptill "+dateFrom+".";   
		        saveMessage(getText(key));
			}else{
				key = "No record found for deductions processing uptill "+dateFrom+"."; 
				saveMessage(getText(key));
			}
			return SUCCESS;
	    }
	    
	    @SkipValidation
	    public String previewDeduction(){
	    	standardDeductionsManager.deleteSubContChargesForStandardDeduction(companyDivision,sessionCorpID);
	    	List previewSD = standardDeductionsManager.deuctionPreviewList(fromDate, companyDivision, sessionCorpID, "DriverProcess", ownerPayTo);
	    	if(previewSD!=null && (!(previewSD.isEmpty()))){
	    	Iterator it = previewSD.iterator();
			while(it.hasNext()){
				BigDecimal dedAmount = new BigDecimal("0.00");
				Object [] row = (Object[])it.next();
				standardDeductions = standardDeductionsManager.get(new Long (row[0].toString()));
				
				Long chargeId = Long.parseLong(row[14].toString());
				Charges charges = chargesManager.get(chargeId);
				
				if(charges.getDeductionType() != null && (charges.getDeductionType().equalsIgnoreCase("Per Trip") || charges.getDeductionType().equalsIgnoreCase("Per Pay"))){
					if(standardDeductions.getAmtDeducted().intValue() <  standardDeductions.getMaxDeductAmt().intValue()){
						try{
							if(row[4] != null && !(row[4].toString().trim().equalsIgnoreCase("")) && !(row[4].toString().equalsIgnoreCase("0.00")) && !(row[4].toString().equalsIgnoreCase("0"))){
								dedAmount = new BigDecimal(row[4].toString());
							}else{
								if(row[9] != null){//priceType
									String priceType = row[9].toString();
									if(priceType.equalsIgnoreCase("Preset")){
										dedAmount = new BigDecimal(row[10] == null ? "0.00" : row[10].toString());
									}else if(priceType.equalsIgnoreCase("BuildFormula")){
										String expression = row[11] == null ? "0.00" : row[11].toString();
										Map values = new HashMap();
										values.put("standardDeductions.maxDeductAmt", new BigDecimal(row[6] == null ? "0.00" : row[6].toString()));
										values.put("standardDeductions.rate", new BigDecimal(row[5] == null ? "0.00" : row[5].toString()));
										values.put("standardDeductions.deduction", new BigDecimal(row[4] == null ? "0.00" : row[4].toString()));
										dedAmount = (BigDecimal)expressionManager.executeExpression(expression, values);
									}
								}else{
									dedAmount = new BigDecimal("0.00");
								}
							}
						}catch(Exception ex){
							dedAmount = new BigDecimal("0.00");
						}
						
						if(standardDeductions.getAmtDeducted().add(dedAmount).intValue() > standardDeductions.getMaxDeductAmt().intValue()){
							dedAmount = standardDeductions.getMaxDeductAmt().subtract(standardDeductions.getAmtDeducted());
						}
						
						if(charges.getDeductionType() != null && charges.getDeductionType().equalsIgnoreCase("Per Trip")){
							String tripCount = standardDeductionsManager.getTripCount(fromDate, standardDeductions.getPartnerCode(), sessionCorpID).get(0).toString();
							dedAmount = dedAmount.multiply(new BigDecimal(tripCount));
							if(!(tripCount.equalsIgnoreCase("0"))){
								createSubcontractorCharges(row,dedAmount,process,standardDeductions, fromDate, charges.getExpGl());
								//findStandardDeductionPreviewList(fromDate, companyDivision, sessionCorpID, process,ownerPayTo);
							}
						}else{						
						       createSubcontractorCharges(row,dedAmount,process,standardDeductions, fromDate, charges.getExpGl());
						       //findStandardDeductionPreviewList(fromDate, companyDivision, sessionCorpID, process,ownerPayTo);
						}
					}
				}
			}
			findStandardDeductionPreviewList(fromDate, companyDivision, sessionCorpID, process,ownerPayTo);
	    	}
	    	else{
	    		standardDeductionPreview=new ArrayList();
	    		//standardDeductionPreview = standardDeductionsManager.getStandardDeductionPreviewList(fromDate, companyDivision, sessionCorpID, process,ownerPayTo);
	    	}
			return SUCCESS;
	    }

	    public String findStandardDeductionPreviewList(Date fromDate, String companyDivision, String sessionCorpID, String process,String ownerPayTo){
	    	List standardDeductionPreviewTemp = standardDeductionsManager.getStandardDeductionPreviewList(fromDate, companyDivision, sessionCorpID, process,ownerPayTo);
			if(standardDeductionPreviewTemp!=null && (!(standardDeductionPreviewTemp.isEmpty()))){
	    	SortedMap <String, BigDecimal> forwardBalMap = new TreeMap<String, BigDecimal>();
			String key = new String("");
			String partnerCode = "";
			String agency = "";
			String partnerName = "";
			String balanceForward = "";
			String personId ="";
			String personName ="";
			String branch ="";
			BigDecimal amt  =new BigDecimal("0.00");
			BigDecimal amount = new BigDecimal("0.00");
			BigDecimal carryForwardBal = new BigDecimal("0");
			
			Iterator itPreview = standardDeductionPreviewTemp.iterator();
			while(itPreview.hasNext()){
				PreviewDTO previewDTO = (PreviewDTO)itPreview.next();
				partnerCode = previewDTO.getCode() == null ? "" : previewDTO.getCode().toString();
				partnerName = previewDTO.getName() == null ? "" : previewDTO.getName().toString();
				agency = previewDTO.getBranch() == null ? "" : previewDTO.getBranch().toString();
				balanceForward = previewDTO.getBalanceForward().toString();
				
				BigDecimal actualExpense = new BigDecimal(previewDTO.getActualExpense().toString());
				BigDecimal amountSCC = new BigDecimal(previewDTO.getAmount().toString());
				
				if(balanceForward.equalsIgnoreCase("true")){
					amount = amountSCC;
				}else{
					amount = actualExpense.add(amountSCC);
				}
				
				carryForwardBal = amount;
				key = partnerCode +"~"+ partnerName +"`"+ agency;
				BigDecimal forBalance = forwardBalMap.get(key);
				if(forBalance != null){
					carryForwardBal = carryForwardBal.add(forBalance);
				}
				forwardBalMap.put(key, carryForwardBal);
			}

			Iterator forBalIterator = forwardBalMap.entrySet().iterator(); 
			BigDecimal totalAmt=new BigDecimal("0");
			while (forBalIterator.hasNext()) {
				Map.Entry entryFB = (Map.Entry) forBalIterator.next();
				String mapKey = (String) entryFB.getKey();
				amt = (BigDecimal) entryFB.getValue();
				totalAmt=totalAmt.add(amt);
				 personId = mapKey.substring(0, mapKey.indexOf("~"));
				 personName = mapKey.substring(mapKey.indexOf("~")+1, mapKey.indexOf("`"));
				 branch = mapKey.substring(mapKey.indexOf("`")+1, mapKey.length());
			}
			SubcontractorCharges subcontractorCharges = new SubcontractorCharges();
	    	subcontractorCharges.setPersonType("D");
			subcontractorCharges.setCompanyDivision(companyDivision);
			subcontractorCharges.setCorpID(sessionCorpID);
			subcontractorCharges.setDeductTempFlag(true);
			subcontractorCharges.setIsSettled(false);
			subcontractorCharges.setBranch(branch);
			subcontractorCharges.setPersonId(personId);
			subcontractorCharges.setPersonName(personName);
			subcontractorCharges.setAmount(totalAmt);
			subcontractorCharges.setDescription("Std. Ded.: Carry Forward Balance" );
			if(totalAmt.intValue()<0){			
			subcontractorCharges.setBalanceForward(true);
			}else{
				subcontractorCharges.setDescription("Std. Ded.: Settlement Amount" );
				subcontractorCharges.setBalanceForward(false);
			}
			subcontractorCharges.setSource("Carry Forward");
			subcontractorCharges.setApproved(fromDate);
			subcontractorCharges.setPost(fromDate);
			subcontractorCharges.setCreatedOn(new Date());
			subcontractorCharges.setCreatedBy(getRequest().getRemoteUser());
			subcontractorCharges.setUpdatedOn(new Date());
			subcontractorCharges.setUpdatedBy(getRequest().getRemoteUser());
			subcontractorCharges.setPayTo(ownerPayTo);
			subcontractorCharges = subcontractorChargesManager.save(subcontractorCharges);
			}
			standardDeductionPreview = standardDeductionsManager.getStandardDeductionPreviewList(fromDate, companyDivision, sessionCorpID, process,ownerPayTo);
			return SUCCESS;
	    }
	    
	    
	    @SkipValidation
	    public String finalizeDeduction(){
	    	if (userCheck.equals("") || userCheck.equals(",")) {
			}else{
				userCheck = userCheck.trim();
				if (userCheck.indexOf(",") == 0) {
					userCheck = userCheck.substring(1);
				}
				if (userCheck.lastIndexOf(",") == userCheck.length() - 1) {
					userCheck = userCheck.substring(0, userCheck.length() - 1);
				}
				String[] arrayId = userCheck.split(",");
				int arrayLength = arrayId.length;
				for (int i = 0; i < arrayLength; i++) {
					String personId = arrayId[i];
					List<SubcontractorCharges> subcontractorChargesList = standardDeductionsManager.getSubContChargesForFinalize(personId);
					Iterator<SubcontractorCharges> itSubCont = subcontractorChargesList.iterator();
					while(itSubCont.hasNext()){
						BigDecimal balancedAmount = new BigDecimal("0.00");
						BigDecimal personalEscrowAmount = new BigDecimal("0.00");						
						SubcontractorCharges subcontractorCharges = itSubCont.next();
						StandardDeductions standardDeductions = standardDeductionsManager.get(subcontractorCharges.getParentId());
						BigDecimal dedAmount = subcontractorCharges.getAmount();
						dedAmount = standardDeductions.getAmtDeducted().add(dedAmount.abs());
						String status = "";
						if(dedAmount.equals(standardDeductions.getMaxDeductAmt())) status = "Closed";
						balancedAmount=standardDeductions.getMaxDeductAmt().subtract(dedAmount);
						standardDeductionsManager.updateStandardDeduction(standardDeductions.getId(),dedAmount,balancedAmount, fromDate, status);
						Charges charges = chargesManager.get(Long.parseLong(standardDeductionsManager.chargesObject(standardDeductions.getChargeCode(), sessionCorpID).get(0).toString()));
						if(standardDeductions.getPersonalEscrow()!=null && standardDeductions.getPersonalEscrow()==true){
							List perEscrowAmount=standardDeductionsManager.partnerPrivatePersonalEscrow(standardDeductions.getPartnerCode(), sessionCorpID);
							String tripCount = standardDeductionsManager.getTripCount(fromDate, standardDeductions.getPartnerCode(), sessionCorpID).get(0).toString();
							BigDecimal dedAmount1 =new BigDecimal("0.00"); 
							if(charges.getDeductionType() != null && charges.getDeductionType().equalsIgnoreCase("Per Trip")){
							dedAmount1=standardDeductions.getDeduction().multiply(new BigDecimal(tripCount));
							}else{
								dedAmount1=standardDeductions.getDeduction();
							}
							personalEscrowAmount=new BigDecimal(perEscrowAmount.get(0).toString()).add(dedAmount1);							
							standardDeductionsManager.updatePartnerPrivate(standardDeductions.getPartnerCode(),sessionCorpID,personalEscrowAmount);
						}						
					}					
					List<SubcontractorCharges> subcontractorChargesList1 = standardDeductionsManager.getSubContChargesForPersonalEscrow(personId);
					Iterator<SubcontractorCharges> itSubCont1 = subcontractorChargesList1.iterator();
					while(itSubCont1.hasNext()){
						BigDecimal personalEscrowAmountSub = new BigDecimal("0.00");
						SubcontractorCharges subcontractorCharges1 = itSubCont1.next();
						if(subcontractorCharges1.getPersonalEscrow()!=null && subcontractorCharges1.getPersonalEscrow()==true){
							List perEscrowAmount=standardDeductionsManager.partnerPrivatePersonalEscrow(personId, sessionCorpID);
								personalEscrowAmountSub=new BigDecimal(perEscrowAmount.get(0).toString()).subtract(subcontractorCharges1.getAmount().abs());
							    standardDeductionsManager.updatePartnerPrivate(personId,sessionCorpID,personalEscrowAmountSub);
						}
					}
					List subcontractorChargesFinalList = standardDeductionsManager.subContractorChargesFinalList(personId, sessionCorpID, companyDivision);
					Iterator itSubContFinal=subcontractorChargesFinalList.iterator();
					while(itSubContFinal.hasNext()){
						Long id= Long.parseLong(itSubContFinal.next().toString());
						standardDeductionsManager.updateSubContChargesForStandardDeduction(personId,id,getRequest().getRemoteUser());
					}
					standardDeductionsManager.updateDomestic(personId);
										
				}
			}
	    	
	    	//standardDeductionsManager.updateSubContChargesForStandardDeduction(fromDate, companyDivision, sessionCorpID);
			standardDeductionsManager.deleteSubContChargesForStandardDeduction(companyDivision,sessionCorpID);
			
			String dateFrom = "";
			if (fromDate != null){
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(fromDate));
				dateFrom = nowYYYYMMDD1.toString();
			}
			String key = "Driver processing has been done uptill "+dateFrom+".";   
	        saveMessage(getText(key));
			return SUCCESS;
	    }
	    
	    
	    private SubcontractorCharges createSubcontractorCharges(Object [] row, BigDecimal dedAmount, String process, StandardDeductions standardDeductions, Date fromDate, String glCode){
	    	SubcontractorCharges subcontractorCharges = new SubcontractorCharges();
	    	subcontractorCharges.setPersonType("D");
			subcontractorCharges.setCompanyDivision(companyDivision);
			subcontractorCharges.setCorpID(sessionCorpID);
			subcontractorCharges.setParentId(standardDeductions.getId());
			subcontractorCharges.setBranch(row[15]== null ? "" : row[15].toString());
			subcontractorCharges.setPersonId(row[12]== null ? "" : row[12].toString());
			subcontractorCharges.setPersonName(row[13]== null ? "" : row[13].toString());
			BigDecimal sbuDedAmount= new BigDecimal("0.00");
			subcontractorCharges.setAmount(sbuDedAmount.subtract(dedAmount));
			subcontractorCharges.setDescription("Std. Ded.: " + standardDeductions.getChargeCodeDesc());
			subcontractorCharges.setSource("Standard Deduction");
			if(process != null && process.equalsIgnoreCase("preview")){
				subcontractorCharges.setDeductTempFlag(true);
			}else if(process != null && process.equalsIgnoreCase("finalize")){
				subcontractorCharges.setDeductTempFlag(false);
				subcontractorCharges.setPost(fromDate);
			}
			subcontractorCharges.setBalanceForward(false);
			subcontractorCharges.setIsSettled(false);
			subcontractorCharges.setApproved(fromDate);
			
			subcontractorCharges.setGlCode(glCode);
			subcontractorCharges.setCreatedOn(new Date());
			subcontractorCharges.setCreatedBy(getRequest().getRemoteUser());
			subcontractorCharges.setUpdatedOn(new Date());
			subcontractorCharges.setUpdatedBy(getRequest().getRemoteUser());
			subcontractorCharges = subcontractorChargesManager.save(subcontractorCharges);
			return subcontractorCharges;
	    }
	    								           //charges, fromDate, standardDeductions.getLastDeductionDate()
	    private Map processStandardDeduction(Charges charges, Date toDate, Date fromDate){
	    	
	    	List<Integer> monthList = new ArrayList();
			List<Integer> weekList = new ArrayList();
			int weekDay = 0;
			if(charges.getWorkDay() != null && charges.getWorkDay().trim().length()>0){
				weekDay = Integer.parseInt(charges.getWorkDay());
			}
			
			if(charges.getJan()==true) monthList.add(0);
			if(charges.getFeb()==true) monthList.add(1);
			if(charges.getMar()==true) monthList.add(2);
			if(charges.getApr()==true) monthList.add(3);
			if(charges.getMay()==true) monthList.add(4);
			if(charges.getJun()==true) monthList.add(5);
			if(charges.getJul()==true) monthList.add(6);
			if(charges.getAug()==true) monthList.add(7);
			if(charges.getSep()==true) monthList.add(8);
			if(charges.getOct()==true) monthList.add(9);
			if(charges.getNov()==true) monthList.add(10);
			if(charges.getDecem()==true) monthList.add(11);
			
			if(charges.getWeek1()==true) weekList.add(1);
			if(charges.getWeek2()==true) weekList.add(2);
			if(charges.getWeek3()==true) weekList.add(3);
			if(charges.getWeek4()==true) weekList.add(4);
			if(charges.getWeek5()==true) weekList.add(5);
			if(charges.getWeek6()==true) weekList.add(6);
			
			fromDate = (fromDate == null ? new Date() : fromDate); 
			
			Map deductionDetail = new HashMap();
			int numberOfDeductions = 0;
			
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(fromDate);
			int fromYear = calendar.get(Calendar.YEAR);
			calendar.setTime(toDate);
			int toYear = calendar.get(Calendar.YEAR);
			Date lastDeductionDate = null;
			Date lastDeductionDateDiffYear = null;
			Map<String, Date> fromToDatesForWeekOfMonth = getWeeksForYear(fromDate);
			if (fromYear != toYear) fromToDatesForWeekOfMonth.putAll(getWeeksForYear(toDate));
			for (int month:monthList){
				for (int week:weekList){
					if(charges.getFrequency().equalsIgnoreCase("Monthly")){
					if(week>1 && week<3){
						Date weekFromDate = fromToDatesForWeekOfMonth.get(fromYear+String.format("%02d", month) + "W" + (week-1) + "From");
						Date weekToDate = fromToDatesForWeekOfMonth.get(fromYear+String.format("%02d", month) + "W" + (week-1) + "To");						
						if(weekFromDate!=null && weekToDate!=null){
						calendar.setTime(weekFromDate);
						int weekFromDay = calendar.get(Calendar.DAY_OF_WEEK);
						calendar.setTime(weekToDate);
						int weekToDay = calendar.get(Calendar.DAY_OF_WEEK);
						if (weekDay >= weekFromDay && weekDay <= weekToDay){
							
						}else{
							week=week+1;
						}
						}
					}
					}
					Date weekFromDate = fromToDatesForWeekOfMonth.get(fromYear+String.format("%02d", month) + "W" + week + "From");
					Date weekToDate = fromToDatesForWeekOfMonth.get(fromYear+String.format("%02d", month) + "W" + week + "To");
					if (weekFromDate!=null && weekToDate!=null && ((weekFromDate.getTime() >= fromDate.getTime() &&  weekFromDate.getTime() <= toDate.getTime()) ||  
							(weekToDate.getTime() >= fromDate.getTime() &&  weekToDate.getTime() <= toDate.getTime())) ){
							calendar.setTime(weekFromDate);
							int weekFromDay = calendar.get(Calendar.DAY_OF_WEEK);
							calendar.setTime(weekToDate);
							int weekToDay = calendar.get(Calendar.DAY_OF_WEEK);
							if (weekDay >= weekFromDay && weekDay <= weekToDay){
								calendar.set(Calendar.DAY_OF_WEEK, weekDay);
								lastDeductionDate = calendar.getTime();
								if (lastDeductionDate.getTime() > fromDate.getTime()) {
									numberOfDeductions ++;
								}
							}else if (weekDay < weekFromDay && week == 1 && weekList.size() == 1) {
								weekToDate = fromToDatesForWeekOfMonth.get(fromYear+String.format("%02d", month) + "W" + (week+1) + "To");
								calendar.setTime(weekToDate);
								calendar.set(Calendar.DAY_OF_WEEK, weekDay);
								lastDeductionDate = calendar.getTime();
								if (lastDeductionDate.getTime() > fromDate.getTime()) {
									numberOfDeductions ++;
								}
							}
					} else if (fromYear != toYear){
						if(charges.getFrequency().equalsIgnoreCase("Monthly")){
							if(week>1 && week<3){
								 weekFromDate = fromToDatesForWeekOfMonth.get(fromYear+String.format("%02d", month) + "W" + (week-1) + "From");
								 weekToDate = fromToDatesForWeekOfMonth.get(fromYear+String.format("%02d", month) + "W" + (week-1) + "To");						
								if(weekFromDate!=null && weekToDate!=null){
								calendar.setTime(weekFromDate);
								int weekFromDay = calendar.get(Calendar.DAY_OF_WEEK);
								calendar.setTime(weekToDate);
								int weekToDay = calendar.get(Calendar.DAY_OF_WEEK);
								if (weekDay >= weekFromDay && weekDay <= weekToDay){
									
								}else{
									week=week+1;
								}
								}
							}
							}
						weekFromDate = fromToDatesForWeekOfMonth.get(toYear+String.format("%02d", month) + "W" + week + "From");
						weekToDate = fromToDatesForWeekOfMonth.get(toYear+String.format("%02d", month) + "W" + week + "To");
						if (weekFromDate!=null && weekToDate!=null && ((weekFromDate.getTime() >= fromDate.getTime() &&  weekFromDate.getTime() <= toDate.getTime()) ||  
								(weekToDate.getTime() >= fromDate.getTime() &&  weekToDate.getTime() <= toDate.getTime())) ){
							calendar.setTime(weekFromDate);
							int weekFromDay = calendar.get(Calendar.DAY_OF_WEEK);
							calendar.setTime(weekToDate);
							int weekToDay = calendar.get(Calendar.DAY_OF_WEEK);
							if (weekDay >= weekFromDay && weekDay <= weekToDay){
								calendar.set(Calendar.DAY_OF_WEEK, weekDay);
								lastDeductionDateDiffYear = calendar.getTime();
								if (lastDeductionDateDiffYear.getTime() > fromDate.getTime()) {
									numberOfDeductions ++;
								}
							}else if (weekDay < weekFromDay && week == 1 && weekList.size() == 1) {
								weekToDate = fromToDatesForWeekOfMonth.get(toYear+String.format("%02d", month) + "W" + (week+1) + "To");
								calendar.setTime(weekToDate);
								calendar.set(Calendar.DAY_OF_WEEK, weekDay);
								lastDeductionDateDiffYear = calendar.getTime();
								if (lastDeductionDateDiffYear.getTime() > fromDate.getTime()) {
									numberOfDeductions ++;
								}
							}						
						}					
					}
				}
			}
			deductionDetail.put("numberOfDeductions", numberOfDeductions);
			if(fromYear != toYear && lastDeductionDateDiffYear!=null){
			deductionDetail.put("lastDeductionDate", lastDeductionDateDiffYear);
			}else{
			deductionDetail.put("lastDeductionDate", lastDeductionDate);
			}
			return deductionDetail;
	    }
	    
	    private static Map getWeeksForYear(Date year){
			Map<String, Date> fromToDatesForWeekOfMonth = new TreeMap<String, Date>();
			SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(year);
			int yearNumber = calendar.get(Calendar.YEAR);
			int months[] = {0,1,2,3,4,5,6,7,8,9,10,11};
			for (int month:months){
				String dateString = String.format("%02d", month+1) + "-01-" + yearNumber;
				Date firstOfMonth = null;
				try {
					firstOfMonth = format.parse(dateString);
				} catch (ParseException e) {
					System.out.println("ERROR: Cannot parse \"" + dateString + "\"");
				}
				calendar.setTime(firstOfMonth);
				fromToDatesForWeekOfMonth.put(yearNumber+String.format("%02d", month)+"W1From", firstOfMonth);
				calendar.set(Calendar.DATE, 1+(7-calendar.get(Calendar.DAY_OF_WEEK)));
				Date lastDateInWeek1 = calendar.getTime();
				fromToDatesForWeekOfMonth.put(yearNumber+String.format("%02d", month)+"W1To", lastDateInWeek1);
				int lastDate = calendar.getActualMaximum(Calendar.DATE);
				int weekNum = 1;
				while (calendar.get(Calendar.DATE) < lastDate){
					weekNum++;
					int nextFromDate = calendar.get(Calendar.DATE)+1;
					calendar.set(Calendar.DATE, nextFromDate);
					fromToDatesForWeekOfMonth.put(yearNumber+String.format("%02d", month) + "W" + weekNum + "From", calendar.getTime());
					int nextToDate = calendar.get(Calendar.DATE)+6; 
					if(nextToDate > lastDate) nextToDate = lastDate; 
					calendar.set(Calendar.DATE, nextToDate);
					fromToDatesForWeekOfMonth.put(yearNumber+String.format("%02d", month) + "W" + weekNum + "To", calendar.getTime());
				}
			}
			return fromToDatesForWeekOfMonth;
		}
	    
	    
	    @SkipValidation
	    public String getComboList(String corpId){
	    	return SUCCESS;
	    }
	    private List validatePayToList;
	    @SkipValidation
	    public String validatePayTo(){
	    	validatePayToList=standardDeductionsManager.validatePayTo(ownerPayTo,sessionCorpID);
	    	return SUCCESS;
	    }
	    public void setId(Long id) {   
	        this.id = id;   
	    }  
	    
	    public void setPartnerId(Long partnerId) {
			this.partnerId = partnerId;
		}
		
		public void setRefMasterManager(RefMasterManager refMasterManager) {
	        this.refMasterManager = refMasterManager;
	    }
	    public List getRefMasters(){
	    	return refMasters;
	    }
	    
		public String getValidateFormNav() {
			return validateFormNav;
		}

		public void setValidateFormNav(String validateFormNav) {
			this.validateFormNav = validateFormNav;
		}
		
		public String getGotoPageString() {
			return gotoPageString;
		}

		public void setGotoPageString(String gotoPageString) {
			this.gotoPageString = gotoPageString;
		}

		public String getSessionCorpID() {
			return sessionCorpID;
		}

		public void setSessionCorpID(String sessionCorpID) {
			this.sessionCorpID = sessionCorpID;
		}

		public Long getId() {
			return id;
		}

		public Long getPartnerId() {
			return partnerId;
		}

		public void setRefMasters(List refMasters) {
			this.refMasters = refMasters;
		}

		public PartnerPublic getPartner() {
			return partner;
		}

		public void setPartner(PartnerPublic partner) {
			this.partner = partner;
		}

		public String getHitflag() {
			return hitflag;
		}

		public void setHitflag(String hitflag) {
			this.hitflag = hitflag;
		}

		public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
			this.partnerPublicManager = partnerPublicManager;
		}

		public StandardDeductions getStandardDeductions() {
			return standardDeductions;
		}

		public void setStandardDeductions(StandardDeductions standardDeductions) {
			this.standardDeductions = standardDeductions;
		}

		public void setStandardDeductionsManager(StandardDeductionsManager standardDeductionsManager) {
			this.standardDeductionsManager = standardDeductionsManager;
		}

		public List getStandardDeduction() {
			return standardDeduction;
		}

		public void setStandardDeduction(List standardDeduction) {
			this.standardDeduction = standardDeduction;
		}

		public List getCompanyDivis() {
			return companyDivis;
		}

		public void setCompanyDivis(List companyDivis) {
			this.companyDivis = companyDivis;
		}

		public void setCustomerFileManager(CustomerFileManager customerFileManager) {
			this.customerFileManager = customerFileManager;
		}

		public String getCompanyDivision() {
			return companyDivision;
		}

		public void setCompanyDivision(String companyDivision) {
			this.companyDivision = companyDivision;
		}

		public Date getFromDate() {
			return fromDate;
		}

		public void setFromDate(Date fromDate) {
			this.fromDate = fromDate;
		}

		/*public Date getToDate() {
			return toDate;
		}

		public void setToDate(Date toDate) {
			this.toDate = toDate;
		}*/

		public List getStandardDeductionPreview() {
			return standardDeductionPreview;
		}

		public void setStandardDeductionPreview(List standardDeductionPreview) {
			this.standardDeductionPreview = standardDeductionPreview;
		}

		public void setExpressionManager(ExpressionManager expressionManager) {
			this.expressionManager = expressionManager;
		}

		public void setSubcontractorChargesManager(SubcontractorChargesManager subcontractorChargesManager) {
			this.subcontractorChargesManager = subcontractorChargesManager;
		}

		public String getProcess() {
			return process;
		}

		public void setProcess(String process) {
			this.process = process;
		}

		public void setChargesManager(ChargesManager chargesManager) {
			this.chargesManager = chargesManager;
		}

		public List getSubContChargesForFinalizeList() {
			return subContChargesForFinalizeList;
		}

		public void setSubContChargesForFinalizeList(List subContChargesForFinalizeList) {
			this.subContChargesForFinalizeList = subContChargesForFinalizeList;
		}

		public String getUserCheck() {
			return userCheck;
		}

		public void setUserCheck(String userCheck) {
			this.userCheck = userCheck;
		}

		public String getOwnerPayTo() {
			return ownerPayTo;
		}

		public void setOwnerPayTo(String ownerPayTo) {
			this.ownerPayTo = ownerPayTo;
		}

		public List getValidatePayToList() {
			return validatePayToList;
		}

		public void setValidatePayToList(List validatePayToList) {
			this.validatePayToList = validatePayToList;
		}

		public PartnerPrivate getPartnerPrivate() {
			return partnerPrivate;
		}

		public void setPartnerPrivate(PartnerPrivate partnerPrivate) {
			this.partnerPrivate = partnerPrivate;
		}

		public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
			this.partnerPrivateManager = partnerPrivateManager;
		}
	    
	}