package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.InlandAgent;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.InlandAgentManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.VehicleManager;

public class InlandAgentAction extends BaseAction implements Preparable {
	private String sessionCorpID;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private Long sid;
	Date currentdate = new Date();
	private InlandAgent inlandAgent;
	private InlandAgentManager inlandAgentManager;
	static final Logger logger = Logger.getLogger(InlandAgentAction.class);
	private Long id;
	private CustomerFile customerFile;
	private String maxChild;
	private String countChild;
	private String minChild;
	private CustomerFileManager customerFileManager;
	private String shipSize;
	private String minShip;
	private String countShip;
	private  List containerNumberList;
	private ServicePartnerManager servicePartnerManager;
	private String gotoPageString;
	private Miscellaneous miscellaneous;
	private MiscellaneousManager miscellaneousManager;
	private TrackingStatus trackingStatus;
	private Billing billing;
	private BillingManager billingManager;
	private TrackingStatusManager trackingStatusManager;
	private List inlandAgentsList;
	private String idNumber;
	private Long autoId;
	private Long idMax;
	private VehicleManager vehicleManager;
	private String shipNumber;
	private String userName;
	private String query;
	private String fieldName;
	private String fieldValue;
	private String fieldType;
	private List containerNumberListInlandAgent;

	public InlandAgentAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	public void prepare() throws Exception {
		userName=getRequest().getRemoteUser();
		}
	
	private String forwardingAjaxUrl;
	private String forwardingAjaxVal;
	private String hitFlag;
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
  	  boolean isNew = (inlandAgent.getId() == null);   
  	inlandAgent.setServiceOrder(serviceOrder);
      miscellaneous = miscellaneousManager.get(serviceOrder.getId());
  		trackingStatus=trackingStatusManager.get(serviceOrder.getId());
  		inlandAgent.setUpdatedOn(new Date());
  		inlandAgent.setUpdatedBy(getRequest().getRemoteUser());
  		if(isNew){
    	  inlandAgent.setCreatedOn(new Date());
    	  inlandAgent.setCreatedBy(userName);
  		}
  		List maxIdNumber = inlandAgentManager.findMaximumIdNumber(serviceOrder.getShipNumber());
	       if (maxIdNumber.get(0) == null ) 
	        {          
	      	 	idNumber = "01";
	        }else {
	          		autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
	          		if((autoId.toString()).length() == 1) 
	          			{
	        			idNumber = "0"+(autoId.toString());
	          			}else {
	          				idNumber=autoId.toString();
	        			}
	        	} 
	       if(isNew){
	    	   inlandAgent.setIdNumber(idNumber);
	       }else{
	    	   inlandAgent.setIdNumber(inlandAgent.getIdNumber());
	       }
	       inlandAgent=inlandAgentManager.save(inlandAgent); 
     
	       serviceOrder=inlandAgent.getServiceOrder();
     
	       if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
    	  String key = (isNew) ? "inlandAgent.added" : "inlandAgent.updated";   
    	  saveMessage(getText(key)); 
	       }
      
      if (!isNew) { 
      		serviceOrder=serviceOrderManager.get(serviceOrder.getId());
      		inlandAgentsList = new ArrayList( serviceOrder.getInlandAgent());
    	   	getSession().setAttribute("inlandAgentsList", inlandAgentsList);
    	   	containerNumberList= containerNumberList();
    		if(containerNumberList==null || containerNumberList.isEmpty() || containerNumberList.get(0)==null){
    			containerNumberList= new ArrayList();
    		}
    	    hitFlag="1";
          return SUCCESS;   
          
      } else {   
      		idMax = Long.parseLong(inlandAgentManager.findMaxId().get(0).toString());
      		inlandAgent = inlandAgentManager.get(idMax);
      		serviceOrder=serviceOrderManager.get(serviceOrder.getId());
      		inlandAgentsList = new ArrayList( serviceOrder.getInlandAgent());
     	    getSession().setAttribute("inlandAgentsList", inlandAgentsList);
     	   containerNumberList= containerNumberList();
     	   	if(containerNumberList==null || containerNumberList.isEmpty() || containerNumberList.get(0)==null){
     	   		containerNumberList= new ArrayList();
     	   }
      }
      
	    hitFlag="1";
	   	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String edit() { 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (id != null) {   
			inlandAgent = inlandAgentManager.get(id);
	          	serviceOrder = inlandAgent.getServiceOrder();
	          	miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		        trackingStatus=trackingStatusManager.get(serviceOrder.getId());
	          	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	          	billing = billingManager.get(serviceOrder.getId());
	          	customerFile = serviceOrder.getCustomerFile();
	          	maxChild = inlandAgentManager.findMaximumChild(serviceOrder.getShipNumber()).get(0).toString();
	            minChild =  inlandAgentManager.findMinimumChild(serviceOrder.getShipNumber()).get(0).toString();
	            countChild =  inlandAgentManager.findCountChild(serviceOrder.getShipNumber()).get(0).toString();
	          } else {   
	        	  inlandAgent = new InlandAgent();
	        	  serviceOrder=serviceOrderManager.get(sid);
	        	  getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	        	  miscellaneous = miscellaneousManager.get(sid);
	        	  trackingStatus=trackingStatusManager.get(sid);
	        	  billing = billingManager.get(sid);
	        	  customerFile = serviceOrder.getCustomerFile();
	        	  inlandAgent.setShipNumber(serviceOrder.getShipNumber());
	        	  inlandAgent .setCreatedOn(new Date());
	        	  inlandAgent .setUpdatedOn(new Date());
	        	  inlandAgent.setCorpID(serviceOrder.getCorpID());
	          }
			shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
			minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
			countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
			containerNumberList= containerNumberList();
			if(containerNumberList==null || containerNumberList.isEmpty() || containerNumberList.get(0)==null){
				containerNumberList= new ArrayList();
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;   
    }  
	
	
	@SkipValidation
	public String getInlandAgentDetailsAjax(){
		try {
			serviceOrder = serviceOrderManager.get(id);
			containerNumberListInlandAgent=inlandAgentManager.getContainerNumber(serviceOrder.getShipNumber());
			inlandAgentsList = new ArrayList( inlandAgentManager.getByShipNumber(serviceOrder.getShipNumber()));
		} catch (Exception e) {			
	    	 return CANCEL;
		}		
		return SUCCESS;
	}
	
	public List containerNumberList() {
		   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
	   		containerNumberList=servicePartnerManager.getContainerNumber(serviceOrder.getShipNumber());
	   		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   		return containerNumberList;
	   }
	
	@SkipValidation
	public String updateInlandAgentAjax(){
    		userName=getRequest().getRemoteUser();
    		inlandAgentManager.updateInlandAgentDetailsFromList(id,fieldName,fieldValue,sessionCorpID,shipNumber,userName,fieldType);
    		return SUCCESS;
	}
	
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public Date getCurrentdate() {
		return currentdate;
	}
	public void setCurrentdate(Date currentdate) {
		this.currentdate = currentdate;
	}
	public ServiceOrderManager getServiceOrderManager() {
		return serviceOrderManager;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public Long getSid() {
		return sid;
	}
	public void setSid(Long sid) {
		this.sid = sid;
	}
	public InlandAgent getInlandAgent() {
		return inlandAgent;
	}
	public void setInlandAgent(InlandAgent inlandAgent) {
		this.inlandAgent = inlandAgent;
	}
	public InlandAgentManager getInlandAgentManager() {
		return inlandAgentManager;
	}
	public void setInlandAgentManager(InlandAgentManager inlandAgentManager) {
		this.inlandAgentManager = inlandAgentManager;
	}
	public String getForwardingAjaxUrl() {
		return forwardingAjaxUrl;
	}
	public void setForwardingAjaxUrl(String forwardingAjaxUrl) {
		this.forwardingAjaxUrl = forwardingAjaxUrl;
	}
	public String getForwardingAjaxVal() {
		return forwardingAjaxVal;
	}
	public void setForwardingAjaxVal(String forwardingAjaxVal) {
		this.forwardingAjaxVal = forwardingAjaxVal;
	}
	public String getHitFlag() {
		return hitFlag;
	}
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public String getMaxChild() {
		return maxChild;
	}
	public void setMaxChild(String maxChild) {
		this.maxChild = maxChild;
	}
	public String getCountChild() {
		return countChild;
	}
	public void setCountChild(String countChild) {
		this.countChild = countChild;
	}
	public String getMinChild() {
		return minChild;
	}
	public void setMinChild(String minChild) {
		this.minChild = minChild;
	}
	public CustomerFileManager getCustomerFileManager() {
		return customerFileManager;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public String getShipSize() {
		return shipSize;
	}
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}
	public String getMinShip() {
		return minShip;
	}
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}
	public String getCountShip() {
		return countShip;
	}
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}
	public List getContainerNumberList() {
		return containerNumberList;
	}
	public void setContainerNumberList(List containerNumberList) {
		this.containerNumberList = containerNumberList;
	}
	public ServicePartnerManager getServicePartnerManager() {
		return servicePartnerManager;
	}
	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}
	public String getGotoPageString() {
		return gotoPageString;
	}
	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}
	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}
	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}
	public MiscellaneousManager getMiscellaneousManager() {
		return miscellaneousManager;
	}
	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}
	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}
	public Billing getBilling() {
		return billing;
	}
	public void setBilling(Billing billing) {
		this.billing = billing;
	}
	public BillingManager getBillingManager() {
		return billingManager;
	}
	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}
	public TrackingStatusManager getTrackingStatusManager() {
		return trackingStatusManager;
	}
	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}
	public List getInlandAgentsList() {
		return inlandAgentsList;
	}
	public void setInlandAgentsList(List inlandAgentsList) {
		this.inlandAgentsList = inlandAgentsList;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public Long getAutoId() {
		return autoId;
	}
	public void setAutoId(Long autoId) {
		this.autoId = autoId;
	}
	public Long getIdMax() {
		return idMax;
	}
	public void setIdMax(Long idMax) {
		this.idMax = idMax;
	}
	public VehicleManager getVehicleManager() {
		return vehicleManager;
	}
	public void setVehicleManager(VehicleManager vehicleManager) {
		this.vehicleManager = vehicleManager;
	}
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public List getContainerNumberListInlandAgent() {
		return containerNumberListInlandAgent;
	}
	public void setContainerNumberListInlandAgent(
			List containerNumberListInlandAgent) {
		this.containerNumberListInlandAgent = containerNumberListInlandAgent;
	}
	
}
