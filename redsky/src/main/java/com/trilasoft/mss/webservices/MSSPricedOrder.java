
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MSSPricedOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MSSPricedOrder">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MSSPricedServices" type="{http://www.mss1.com/webservices/}ArrayOfMSSPricedService" minOccurs="0"/>
 *         &lt;element name="MSSPricedCrates" type="{http://www.mss1.com/webservices/}ArrayOfMSSPricedCrate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MSSPricedOrder", propOrder = {
    "mssPricedServices",
    "mssPricedCrates"
})
public class MSSPricedOrder {

    @XmlElement(name = "MSSPricedServices")
    protected ArrayOfMSSPricedService mssPricedServices;
    @XmlElement(name = "MSSPricedCrates")
    protected ArrayOfMSSPricedCrate mssPricedCrates;

    /**
     * Gets the value of the mssPricedServices property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSPricedService }
     *     
     */
    public ArrayOfMSSPricedService getMSSPricedServices() {
        return mssPricedServices;
    }

    /**
     * Sets the value of the mssPricedServices property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSPricedService }
     *     
     */
    public void setMSSPricedServices(ArrayOfMSSPricedService value) {
        this.mssPricedServices = value;
    }

    /**
     * Gets the value of the mssPricedCrates property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSPricedCrate }
     *     
     */
    public ArrayOfMSSPricedCrate getMSSPricedCrates() {
        return mssPricedCrates;
    }

    /**
     * Sets the value of the mssPricedCrates property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSPricedCrate }
     *     
     */
    public void setMSSPricedCrates(ArrayOfMSSPricedCrate value) {
        this.mssPricedCrates = value;
    }

}
