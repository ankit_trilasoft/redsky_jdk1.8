<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
 
<head>
<style>
span.pagelinks {
display: block;font-size: 0.95em;margin-bottom: 5px;margin-top: -18px;padding: 1px 0;text-align: right; width: 100%;
}
form{margin-top:-30px;}
</style>
<title><fmt:message key="userGuidesList.title" />
</title>
<meta name="heading"
	content="<fmt:message key='userGuidesList.heading'/>" />
</head>
<s:form id="userGuideList" name="userGuidesList" action="searchUserGuides" method="post" >

<c:set var="buttons"> 
    <input type="button" class="cssbuttonA" align="left" 
        onclick="location.href='<c:url value="/editUserGuides.html','height=400,width=850,top=20, left=100, scrollbars=yes,resizable=yes"/>'" 
        value="<fmt:message key="button.add"/>"/> 
        
</c:set>

<div id="Layer1" style="width:100%"> 
				<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
				<table class="table" width="99%" >
				<thead>
				<tr>
				<th>Module</th>
				<th>Document&nbsp;Name</th>
				<th>Enable</th>
				<th></th>
				</thead>
				</tr>
				<tr>
				<td>
				<s:select cssClass="list-menu"   name="userGuides.module" list="%{userGuidesModule}" headerKey="" headerValue="" cssStyle="width:150px"/>
				<td><s:textarea name="userGuides.documentName" cssStyle="height:18px;width:150px;" cssClass="textarea" /></td>
				<td><s:checkbox key="activeStatus" cssStyle="vertical-align:middle; margin:5px 33px 3px 0px;"   /></td>
				<td width="200px">
			    <s:submit cssClass="cssbutton" cssStyle="width:57px;"  key="button.search" />
		       <input type="button" class="cssbutton" value="Clear" style="width:57px;" onclick="clear_fields();"/>   
			   </td>			
				</tr>
		</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
<s:set name="userGuidesList" value="userGuidesList" scope="request"/> 
<display:table name="userGuidesList" defaultsort="1" class="table" id="userGuidesList" requestURI="" pagesize="25" style="width:100%;"> 
<display:column property="module" title="Module" url="/editUserGuides.html" paramId="id" paramProperty="id" style="width:70px;" sortable="true" sortProperty="module">
<%-- 	<c:forEach var="entry" items="${userGuidesModule}">
	<c:if test="${userGuidesList.module==entry.key}">
	<c:out value="${entry.value}" />
	</c:if>
	</c:forEach> --%>
	</display:column>
	<display:column property="documentName"  title="Document/&nbsp;Video&nbsp;Name" sortable="true" sortProperty="documentName"  style="width:200px;" />
	<display:column property="displayOrder"  title="Display&nbsp;Order" sortable="true" sortProperty="displayOrder"  style="width:20px;" maxLength="20"  />
	<display:column property="corpidChecks" title="CorpID&nbsp;Checks" style="width:100px;" sortable="true" sortProperty="corpidChecks">
	
	</display:column>
	<display:column title="Document" style="width:150px;" maxLength="30" >
	<a onclick="javascript:openWindow('UserGuidesResourceMgmtImageServletAction.html?id=${userGuidesList.id}&decorator=popup&popup=true',900,600);">
    <c:out value="${userGuidesList.fileName}" escapeXml="false"/></a>
    </display:column>
    <display:column property="corpId" title="Corp Id" paramId="id" paramProperty="id" style="width:50px;" />
	<display:column title="Enable"   style="width:50px;padding:0px;text-align:center;">
	   <c:if test="${userGuidesList.visible==true }">
	<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
	</c:if>
	<c:if test="${userGuidesList.visible==false }">
	<img src="${pageContext.request.contextPath}/images/cancel001.gif"  /> 
	</c:if>
	</display:column>
	<display:column title="Remove" style="width:45px;">
	<a><img align="middle" onclick="confirmSubmit(${userGuidesList.id});"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	</display:column>
</display:table>
<tr><td align="left" style="margin-left:50px;"><c:out value="${buttons}" escapeXml="false" /></td></tr>
</s:form> 
 <script type="text/javascript"> 
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure to remove this row? ");
	var did = targetElement;
	if (agree){
		location.href="deleteUserGuidesDoc.html?id="+did;
		
		}
	else{
		return false;
	}
}

function clear_fields(){
	  	document.forms['userGuidesList'].elements['userGuides.module'].value = "";
		document.forms['userGuidesList'].elements['userGuides.documentName'].value = "";
		document.forms['userGuidesList'].elements['activeStatus'].checked = false;
		document.forms['userGuidesList'].elements['userGuides.visible'].checked = false;
		
	}
</script>