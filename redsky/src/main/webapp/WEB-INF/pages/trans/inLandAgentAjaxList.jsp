<%@ include file="/common/taglibs.jsp"%>  
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<%@page import="com.trilasoft.app.model.Company" %> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<head>   
    <title></title>   
    <meta name="heading" content=""/> 
    <style type="text/css">
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
  
</head> 

<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="ServiceOrderID" value="${serviceOrder.id}"/>
<s:hidden name="ServiceOrderID" value="${serviceOrder.id}"/>
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.registrationNumber" value="%{serviceOrder.registrationNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/> 
<s:hidden name="customerFile.id" />
<c:set var="mydate" value="{0,date,dd-MMM-yyyy}"/>
<div id="Layer3" style="width:100%">

<c:set var="buttons">   
     <input type="button" class="cssbutton1" onclick="window.open('editInlandAgentAjax.html?decorator=popup&popup=true&sid=${ServiceOrderID}','sqlExtractInputForm','height=500,width=950,top=0, scrollbars=yes,resizable=yes').focus();" 
       value="Add" style="width:60px; height:25px;margin-bottom:5px;" />    
</c:set>  

<s:set name="inlandAgentsList" value="inlandAgentsList" scope="request"/>

<display:table name="inlandAgentsList" class="table" requestURI="" id="inlandAgentsList" export="false" defaultsort="0" style="width:99%;">
	<display:column  titleKey="vehicle.idNumber" style="width:20px;" >
     	<a href="javascript: void(0)"  onclick="window.open('editInlandAgentAjax.html?id=${inlandAgentsList.id}&decorator=popup&popup=true','sqlExtractInputForm','height=500,width=950,top=0, scrollbars=yes,resizable=yes').focus();" style="cursor:pointer">  
   		<c:out value="${inlandAgentsList.idNumber}"/></a>
    </display:column>   
	<display:column  titleKey="inlandAgent.VendorName" >
		<s:hidden name="vendorId${inlandAgentsList.id}" id="vendorId${inlandAgentsList.id}" value="${inlandAgentsList.vendorId}" />
		<input type="text" class="input-textUpper" style="text-align:left;width:100px" name="inlandAgentsList.vendorName${inlandAgentsList.id}" id="vendorName${inlandAgentsList.id}" readonly="true" value="${inlandAgentsList.vendorName}"  />
		<img class="openpopup" style="vertical-align:top;" width="15" height="18" onclick="findVendorName('vendorId${inlandAgentsList.id}','vendorName${inlandAgentsList.id}','${inlandAgentsList.id}');" src="<c:url value='/images/open-popup.gif'/>" />
	</display:column>
	<display:column  titleKey="inlandAgent.ContainerType" >
    	<select name ="inlandAgentsList.containerType${inlandAgentsList.id}" id="containerType${inlandAgentsList.id}" style="width:115px" onchange="updateInlandAgentContainerDetails('${inlandAgentsList.id}','containerType',this,'S');" class="list-menu">
						<option value="<c:out value='' />">
							<c:out value=""></c:out>
							</option> 		      
				      <c:forEach var="chrms" items="${containerNumberListInlandAgent}" varStatus="loopStatus">
				           <c:choose>
			                        <c:when test="${chrms == inlandAgentsList.containerType}">
			                        <c:set var="selectedInd" value=" selected"></c:set>
			                        </c:when>
			                    <c:otherwise>
			                        <c:set var="selectedInd" value=""></c:set>
			                     </c:otherwise>
		                    </c:choose>
				      		<option value="<c:out value='${chrms}' />" <c:out value='${selectedInd}' />>
				                    <c:out value="${chrms}"></c:out>
				             </option>
					</c:forEach>		      
			</select>     
    </display:column>
    <display:column  titleKey="inlandAgent.LastFreeDatewithContainer" >    
	    <fmt:parseDate pattern="yyyy-MM-dd" value="${inlandAgentsList.freeDatewithContainer}" var="parsedfreeDatewithContainer" />
		<fmt:formatDate pattern="dd-MMM-yy" value="${parsedfreeDatewithContainer}" var="formattedfreeDatewithContainer" />    
    	<input type="text" class="input-textUpper" style="text-align:right;width:60px" id="freeDatewithContainer${inlandAgentsList.id}" name="inlandAgent.freeDatewithContainer${inlandAgentsList.id}" readonly="true" value="${formattedfreeDatewithContainer}"  />
    	<img id="freeDatewithContainer${inlandAgentsList.id}_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="updateDatethroughAuto('${inlandAgentsList.id}','freeDatewithContainer');"/>
    </display:column>
    <display:column  titleKey="inlandAgent.ChasisProvided" >
     	<c:choose>
         <c:when test="${inlandAgentsList.chasisProvide}">
    	<input type="hidden" style="text-align:right;width:20px" maxlength="10" name="inlandAgent.chasisProvide${inlandAgentsList.id}" id="inlandAgent.chasisProvide${inlandAgentsList.id}" value="${inlandAgentsList.chasisProvide}"/>
     	<input type="checkbox"  name="inlandAgent.chasisProvide${inlandAgentsList.id}"  checked="checked" id="chasisProvide${inlandAgentsList.id}"  onchange="updateDetailsCheckBox('${inlandAgentsList.id}','chasisProvide',false,'B');"/>                                  
       </c:when>
       <c:otherwise>
       <input type="hidden"  name="inlandAgent.chasisProvide${inlandAgentsList.id}" value="${inlandAgentsList.chasisProvide}" id="inlandAgent.chasisProvide${inlandAgentsList.id}"/>
      	<input type="checkbox"  name="inlandAgent.chasisProvide${inlandAgentsList.id}" id="chasisProvide${inlandAgentsList.id}" onchange="updateDetailsCheckBox('${inlandAgentsList.id}','chasisProvide',true,'B');"/>                                  
       </c:otherwise>
       </c:choose>
    </display:column>
	<display:column  titleKey="inlandAgent.SentTruckingOrderForPickup">
	<fmt:parseDate pattern="yyyy-MM-dd" value="${inlandAgentsList.sentTOFPickUp}" var="parsedsentTOFPickUp" />
		<fmt:formatDate pattern="dd-MMM-yy" value="${parsedsentTOFPickUp}" var="formattedsentTOFPickUp" />    
    	<input type="text" class="input-textUpper" style="text-align:right;width:60px" id="sentTOFPickUp${inlandAgentsList.id}" name="inlandAgent.sentTOFPickUp${inlandAgentsList.id}" readonly="true" value="${formattedsentTOFPickUp}" />
    	<img id="sentTOFPickUp${inlandAgentsList.id}_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="updateDatethroughAuto('${inlandAgentsList.id}','sentTOFPickUp');"/>
	</display:column>
    <display:column  titleKey="inlandAgent.LastFreeDateAtPort" >
    	<fmt:parseDate pattern="yyyy-MM-dd" value="${inlandAgentsList.freeDateAtPort}" var="parsedfreeDateAtPort" />
		<fmt:formatDate pattern="dd-MMM-yy" value="${parsedfreeDateAtPort}" var="formattedfreeDateAtPort" />    
    	<input type="text" class="input-textUpper" style="text-align:right;width:60px" id="freeDateAtPort${inlandAgentsList.id}" name="inlandAgent.freeDateAtPort${inlandAgentsList.id}" readonly="true" value="${formattedfreeDateAtPort}" />
    	<img id="freeDateAtPort${inlandAgentsList.id}_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="updateDatethroughAuto('${inlandAgentsList.id}','freeDateAtPort');"/>
    </display:column>
    <display:column  titleKey="inlandAgent.OutOfPort" >
    	<fmt:parseDate pattern="yyyy-MM-dd" value="${inlandAgentsList.outOfPort}" var="parsedoutOfPort" />
		<fmt:formatDate pattern="dd-MMM-yy" value="${parsedoutOfPort}" var="formattedoutOfPort" />    
    	<input type="text" class="input-textUpper" style="text-align:right;width:60px" id="outOfPort${inlandAgentsList.id}" name="inlandAgent.formattedoutOfPort${inlandAgentsList.id}" readonly="true" value="${formattedoutOfPort}" />
    	<img id="outOfPort${inlandAgentsList.id}_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="updateDatethroughAuto('${inlandAgentsList.id}','outOfPort');"/>
    </display:column>
    
    <display:column  titleKey="inlandAgent.RequestedPickupDate" >
    	<fmt:parseDate pattern="yyyy-MM-dd" value="${inlandAgentsList.requestPickDate}" var="parsedrequestPickDate" />
		<fmt:formatDate pattern="dd-MMM-yy" value="${parsedrequestPickDate}" var="formattedrequestPickDate" />    
    	<input type="text" class="input-textUpper" style="text-align:right;width:60px" id="requestPickDate${inlandAgentsList.id}" name="inlandAgent.requestPickDate${inlandAgentsList.id}" readonly="true" value="${formattedrequestPickDate}" />
    	<img id="requestPickDate${inlandAgentsList.id}_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="updateDatethroughAuto('${inlandAgentsList.id}','requestPickDate');"/>
    </display:column>
	</display:table>
   <c:out value="${buttons}" escapeXml="false" />  
  </div>
  
  	<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
	<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
	<c:set var="noteFor" value="ServiceOrder" scope="session"/>
	<c:if test="${empty serviceOrder.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty serviceOrder.id}">
		<c:set var="isTrue" value="true" scope="request"/>
	</c:if>
	<s:hidden name="id" ></s:hidden>
	<s:hidden name="firstDescription" />
	<s:hidden name="secondDescription" />    
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription" />
	<s:hidden name="sixthDescription" />
	<s:hidden name="trackingUrl"/>
<script type="text/javascript">
<configByCorp:fieldVisibility componentId="component.section.forwarding.InLandAgent">
setOnSelectBasedMethods(["calcDays(),calcDays1(),updateDateDetailsInlandAgent()"]);
setCalendarFunctionality();
</configByCorp:fieldVisibility>
</script>

<script type="text/javascript">
function getHTTPObjectInlandAgent()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var httpVendorInlandAgent = getHTTPObjectInlandAgent();
var httpCheckBox = getHTTPObjectInlandAgent();
var httpCodeNameInlandAgent = getHTTPObjectInlandAgent();
var httpCodeIdInlandAgent = getHTTPObjectInlandAgent();
var httpDateInlandAgent = getHTTPObjectInlandAgent();
function updateInlandAgentContainerDetails(idContainer,fieldNameContainer,targetContainer,fieldTypeContainer){
	showOrHide(1); 
	var fieldValueContainer = targetContainer.value;
	 var urlContainer = "updateInlandAgentAjax.html?ajax=1&id="+idContainer+"&fieldName="+fieldNameContainer+"&fieldValue="+fieldValueContainer+"&fieldType="+fieldTypeContainer+"&shipNumber="+'${serviceOrder.shipNumber}';
	 httpVendorInlandAgent.open("GET", urlContainer, true); 
	 httpVendorInlandAgent.onreadystatechange = handleHttpResponseVendor;
	 httpVendorInlandAgent.send(null);	  
}

function handleHttpResponseVendor(){
	if (httpVendorInlandAgent.readyState == 4){
            var resultsContainer = httpVendorInlandAgent.responseText
            resultsContainer = resultsContainer.trim();
            getInlandAgentDetails();
            showOrHide(0);
	}
}

function updateDetailsCheckBox(idCheckBox,fieldNameCheckBox,targetCheckBox,fieldTypeCheckBox){
	showOrHide(1); 
	var fieldValueCheckBox = targetCheckBox;
	 var url = "updateInlandAgentAjax.html?ajax=1&id="+idCheckBox+"&fieldName="+fieldNameCheckBox+"&fieldValue="+fieldValueCheckBox+"&fieldType="+fieldTypeCheckBox+"&shipNumber="+'${serviceOrder.shipNumber}';
	 httpCheckBox.open("GET", url, true); 
	 httpCheckBox.onreadystatechange = handleHttpResponseCheckBox;
	 httpCheckBox.send(null);	  
}

function handleHttpResponseCheckBox(){
	if (httpCheckBox.readyState == 4){
            var resultsCheckBox = httpCheckBox.responseText
            resultsCheckBox = resultsCheckBox.trim();
            getInlandAgentDetails();
            showOrHide(0);
	}
}
var autoidInland="";
var dateValueInland="";
var dateFieldInland="";
function updateDatethroughAuto(id2Inland,fieldNameInland){
	autoidInland=id2Inland;
	 dateFieldInland=fieldNameInland;
	 
	}


 function updateDateDetailsInlandAgent() {        
	 if(dateFieldInland=='freeDatewithContainer' || dateFieldInland=='sentTOFPickUp' || dateFieldInland=='freeDateAtPort' || dateFieldInland=='outOfPort' || dateFieldInland=='requestPickDate'){
	 showOrHide(1);
	 var dateId=autoidInland;
     var type="D";
     if(document.getElementById(dateFieldInland+dateId)!=null){
     dateValueInland = document.getElementById(dateFieldInland+dateId).value;
     var url = "updateInlandAgentAjax.html?ajax=1&id="+dateId+"&fieldName="+dateFieldInland+"&fieldValue="+dateValueInland+"&fieldType="+type+"&shipNumber="+'${serviceOrder.shipNumber}';
     httpDateInlandAgent.open("GET", url, true); 
     httpDateInlandAgent.onreadystatechange = handleHttpResponseDateInlandAgent;
     httpDateInlandAgent.send(null);
     }
	 }
} 
 
 
 function handleHttpResponseDateInlandAgent(){
		if (httpDateInlandAgent.readyState == 4){
	            var resultsDate = httpDateInlandAgent.responseText
	            resultsDate = resultsDate.trim();
	            showOrHide(0);
		}
	}
 var tempIdAgent="";
 var fieldDescriptionAgent="";
 function findVendorName(vendorId,vendorName,idVendorName){    	
 	tempIdAgent=idVendorName;
 	fieldDescriptionAgent = vendorName;
	    openWindow('agentPartnersPopUp.html?id=${serviceOrder.id}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=trackingUrl&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+vendorName+'&fld_code='+vendorId);
 }
 var vendorNameValue ="";
 var vendorIdValue ="";
 var vendorIdfieldName ="";
 var vendorNamefieldName ="";
 function updateInlandAgentNameAndCodeDetails(){
 	showOrHide(1);
 	if(tempIdAgent!=null && tempIdAgent!=''){
			var regVendorId=tempIdAgent;
			tempIdAgent="";
		if(fieldDescriptionAgent.indexOf("vendorName")!= -1){
			vendorNameValue = document.getElementById('vendorName'+regVendorId).value;
			vendorNamefieldName = 'vendorName';
			vendorIdValue = document.getElementById('vendorId'+regVendorId).value;
			vendorIdfieldName = 'vendorId';
		}else{
			
		}
		var url='';
		var fieldType="S";
		if(vendorNameValue!=null){
			var url = "updateInlandAgentAjax.html?ajax=1&id="+regVendorId+"&fieldName="+vendorNamefieldName+"&fieldValue="+encodeURIComponent(vendorNameValue)+"&fieldType="+fieldType+"&shipNumber="+'${serviceOrder.shipNumber}';
 		httpCodeNameInlandAgent.open("GET", url, true); 
 		httpCodeNameInlandAgent.onreadystatechange = handleHttpResponseCodeNameInlandAgent;
 		httpCodeNameInlandAgent.send(null);	
		}
		 if(vendorIdValue!=null){
			var url = "updateInlandAgentAjax.html?ajax=1&id="+regVendorId+"&fieldName="+vendorIdfieldName+"&fieldValue="+encodeURIComponent(vendorIdValue)+"&fieldType="+fieldType+"&shipNumber="+'${serviceOrder.shipNumber}';
			httpCodeIdInlandAgent.open("GET", url, true); 
			httpCodeIdInlandAgent.onreadystatechange = handleHttpResponseCodeIdInlandAgent;
			httpCodeIdInlandAgent.send(null);	
		} else{}
 	}
 }
 function handleHttpResponseCodeNameInlandAgent(){
 	if (httpCodeNameInlandAgent.readyState == 4){
             var resultsVendorName = httpCodeNameInlandAgent.responseText
             resultsVendorName = resultsVendorName.trim();
             showOrHide(0);
 	}
 }
  function handleHttpResponseCodeIdInlandAgent(){
	 	if (httpCodeIdInlandAgent.readyState == 4){
	             var resultsVendorCode = httpCodeIdInlandAgent.responseText
	             resultsVendorCode = resultsVendorCode.trim();
	             showOrHide(0);
	 	}
	 } 
</script>
<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>
