
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Agent Locator</title>

	<%@ include file="/common/taglibs.jsp"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

    
    <script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAI84qPhYnOgvX4X9KS7AtFhT8PMUw5z7_OLJoE1lh2VQyfb-WOxRv8WD1vEh3NGxefb3ipm1bAZp5fA"
      type="text/javascript"></script>
      
	<!-- load the dojo toolkit base -->
	<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>"
    djConfig="parseOnLoad:true, isDebug:false"></script>
          
    <script type="text/javascript"><!--

    //<![CDATA[



   var geocoder;
   var map;

   var address = "821 Mohawk Street, Columbus OH";

	window.onunload = function() { 
		GUnload();
	}
   
   // On page load, call this function

   function load()
   {
      // Create new map object
      map = new GMap2(document.getElementById("map"));

      // Create new geocoding object
      geocoder = new GClientGeocoder();

      // Retrieve location information, pass it to addToMap()
      geocoder.getLocations(document.partnerLocatorForm.address.value, addToMap);
   }

   function locatePartners(){
	// post some data, ignore the response:
	   dojo.xhrPost({
	       form: "partnerLocatorForm", // read the url: from the action="" of the <form>
	       timeout: 3000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
		   		//var jsonData = dojo.toJson(data)
	           for(var i=0; i < jsonData.partners.length; i++){
	        	   console.log("lat: " + jsonData.partners[i].latitude + ", long: " +  jsonData.partners[i].longitude);
	        	   point = new GLatLng(jsonData.partners[i].latitude, jsonData.partners[i].longitude);
	        	   addCoordsToMap(point, jsonData.partners[i]);
	        	   var delme = 0;
	           }
	       }	       
	   });
   }

   // This function adds the point to the map

   function addToMap(response)
   {
      // Retrieve the object
      place = response.Placemark[0];

      document.partnerLocatorForm.inputLatitute.value = place.Point.coordinates[1];
      document.partnerLocatorForm.inputLongitude.value = place.Point.coordinates[0];

      // Retrieve the latitude and longitude
      point = new GLatLng(place.Point.coordinates[1],
              place.Point.coordinates[0]);

      map.setCenter(point, 8);
      
      addCoordsToMap(point, null);

      //locatePartners();

   }

   function addCoordsToMap(point, partnerDetails){

	      
	      

	      // Create a marker
	      marker = new GMarker(point);

	      // Add the marker to map
	      map.addOverlay(marker);

	      // Add address information to marker
	      //marker.openInfoWindowHtml(place.address);
	      var fn = markerClickFn(point, partnerDetails);
	      GEvent.addListener(marker, "click", fn);
	      
	      

	  }

   function markerClickFn(point, partnerDetails) {
	   return function() {
		   if (!partnerDetails) return;
	       var title = partnerDetails.name;
	       var url = partnerDetails.url;
	       var fileurl = partnerDetails.locationPhotoUrl;
	       var infoHtml = '<div style="width:210px;"><h3>' + title
	         + '</h3><div style="width:200px;height:200px;line-height:200px;margin:2px 0;text-align:center;">'
	         + '<a id="infoimg" href="' + fileurl + '" target="_blank">Loading...</a></div><br/>'
	         + '<h5>Quote: $'+partnerDetails.rate+'</h5>'
	         + '<h6>'+partnerDetails.address1+'</h6>'
	         + '<h6>'+partnerDetails.address2+'</h6><br/>'
	         + '<h6>'+partnerDetails.phone+'</h6>';
	       var img = document.createElement("img");
	       GEvent.addDomListener(img, "load", function() {
	                               if ($("infoimg") == null) {
	                                 return;
	                               }
	                               img = adjustImage(img, 200, 200);
	                               img.style.cssText = "vertical-align:middle;padding:1px;border:1px solid #EAEAEA;";
	                               $("infoimg").innerHTML = "";
	                               $("infoimg").appendChild(img);
	                             });
	       img.src = "UserImage?location=" + fileurl;
	       if(img.readyState == "complete" || img.readyState == "loaded") {
	         img = adjustImage(img, 280, 200);
	         infoHtml += '<img width=' + img.width + ' height=' + img.height
	           + ' style="vertical-align:middle;padding:1px;border:1px solid #aAaAaA"></img>';
	       }
	       infoHtml += '</div></div>';
	       map.openInfoWindowHtml(point, infoHtml);

	   }
   }

   function adjustImage(img, maxwidth, maxheight) {
	     var wid = img.width;
	     var hei = img.height;
	     var newwid = wid;
	     var newhei = hei;
	     if(wid / maxwidth > hei / maxheight){
	       if(wid > maxwidth){
	         newwid = maxwidth;
	         newhei = parseInt(hei * newwid / wid);
	       }
	     } else {
	       if(hei > maxheight) {
	         newhei = maxheight;
	         newwid = parseInt(wid * newhei / hei);
	       }
	     }
	     var src = img.src;
	     img = document.createElement("img");
	     img.src = src;
	     img.width = newwid;
	     img.height = newhei;
	     return img;
	   }
   
    //]]>
    --></script>
    
  </head>
<form name="partnerLocatorForm" id="partnerLocatorForm" action="locatePartners.html">
<table>
<tr><th>Weight</th><th>Address</th><th>Country</th><th>Shipment Leg</th><th>Mode</th><th>POE</th></tr>
<tr>
<td><input type="text" name="weight" value="9500"></td>  
<td><input type="text" name="address" value="London" style="width: 33em;"></td>
<td><select class="select" id="countryCode" name="countryCode">
    <option selected="selected" value=""/>
    <option value="AL">Albania</option>
    <option value="DZ">Algeria</option>
    <option value="AR">Argentina</option>
    <option value="AU">Australia</option>
    <option value="AT">Austria</option>
    <option value="BH">Bahrain</option>
    <option value="BY">Belarus</option>
    <option value="BE">Belgium</option>
    <option value="BO">Bolivia</option>
    <option value="BA">Bosnia and Herzegovina</option>
    <option value="BR">Brazil</option>
    <option value="BG">Bulgaria</option>
    <option value="CA">Canada</option>
    <option value="CL">Chile</option>
    <option value="CN">China</option>
    <option value="CO">Colombia</option>
    <option value="CR">Costa Rica</option>
    <option value="HR">Croatia</option>
    <option value="CZ">Czech Republic</option>
    <option value="DK">Denmark</option>
    <option value="DO">Dominican Republic</option>
    <option value="EC">Ecuador</option>
    <option value="EG">Egypt</option>
    <option value="SV">El Salvador</option>
    <option value="EE">Estonia</option>
    <option value="FI">Finland</option>
    <option value="FR">France</option>
    <option value="DE">Germany</option>
    <option value="GR">Greece</option>
    <option value="GT">Guatemala</option>
    <option value="HN">Honduras</option>
    <option value="HK">Hong Kong</option>
    <option value="HU">Hungary</option>
    <option value="IS">Iceland</option>
    <option value="IN">India</option>
    <option value="IQ">Iraq</option>
    <option value="IE">Ireland</option>
    <option value="IL">Israel</option>
    <option value="IT">Italy</option>
    <option value="JP">Japan</option>
    <option value="JO">Jordan</option>
    <option value="KW">Kuwait</option>
    <option value="LV">Latvia</option>
    <option value="LB">Lebanon</option>
    <option value="LY">Libya</option>
    <option value="LT">Lithuania</option>
    <option value="LU">Luxembourg</option>
    <option value="MK">Macedonia</option>
    <option value="MX">Mexico</option>
    <option value="MA">Morocco</option>
    <option value="NL">Netherlands</option>
    <option value="NZ">New Zealand</option>
    <option value="NI">Nicaragua</option>
    <option value="NO">Norway</option>
    <option value="OM">Oman</option>
    <option value="PA">Panama</option>
    <option value="PY">Paraguay</option>
    <option value="PE">Peru</option>
    <option value="PL">Poland</option>
    <option value="PT">Portugal</option>
    <option value="PR">Puerto Rico</option>
    <option value="QA">Qatar</option>
    <option value="RO">Romania</option>
    <option value="RU">Russia</option>
    <option value="SA">Saudi Arabia</option>
    <option value="CS">Serbia and Montenegro</option>
    <option value="SG">Singapore</option>
    <option value="SK">Slovakia</option>
    <option value="SI">Slovenia</option>
    <option value="ZA">South Africa</option>
    <option value="KR">South Korea</option>
    <option value="ES">Spain</option>
    <option value="SD">Sudan</option>
    <option value="SE">Sweden</option>
    <option value="CH">Switzerland</option>
    <option value="SY">Syria</option>
    <option value="TW">Taiwan</option>
    <option value="TH">Thailand</option>
    <option value="TN">Tunisia</option>
    <option value="TR">Turkey</option>
    <option value="UA">Ukraine</option>
    <option value="AE">United Arab Emirates</option>
    <option selected="selected" value="GB">United Kingdom</option>
    <option value="US">United States</option>
    <option value="UY">Uruguay</option>
    <option value="VE">Venezuela</option>
    <option value="VN">Vietnam</option>
    <option value="YE">Yemen</option>
</select></td>
<td><select class="select" id="tariffApplicability" name="tariffApplicability">
    <option selected="selected" value="Destination">Destination</option>
    <option value="Origin">Origin</option>
</select></td>
<td><select class="select" id="packingMode" name="packingMode">
    <option selected="selected" value=""></option>
    <option value="Air">Air</option>
    <option value="L20">L20</option>
    <option selected="selected" value="L40">L40</option>
    <option value="LCL">LCL</option>
    <option value="FCL">FCL</option>
    <option value="GRP">GRP</option>
</select></td>
<td><input type="text" name="poe" value="Southampton"></td>
<td><input type="button" onClick="load()" value="Initialize"></td>
<td><input type="button" onClick="locatePartners()" value="Find Partners"></td>
<td><input type="hidden" name="inputLatitute"></input></td>
<td><input type="hidden" name="inputLongitude"></input></td>
</tr>
</table>
<p></p>
<div id="map" style="width: 800px; height: 500px"></div>
</br>
</form>
  
 