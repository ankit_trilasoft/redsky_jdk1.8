<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
	<title>"Partner Alert Note"</title>   
	<meta name="heading" content="Partner Alert Note"/>   
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
	<tr valign="top"> 	
		<td align="left"><b>Partner Alert List </b></td>
		<td align="right"  style="width:30px;">
			<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
		</td>
	</tr>
</table>  
<display:table name="partnerAlertList" class="table" requestURI="" id="partnerAlertLists" export="false" defaultsort="1" >
	<display:column title="Notes" ><a onclick="goToUrlPartnerAlert('${partnerAlertLists.id}');" ><c:out value="${partnerAlertLists.subject}" /></display:column>      
</display:table>