package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ItemsJbkEquipDao;
import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.OperationsResourceLimits;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.ItemsJbkEquipManager;
  
public class ItemsJbkEquipManagerImpl extends GenericManagerImpl<ItemsJbkEquip, Long> implements ItemsJbkEquipManager {   
	ItemsJbkEquipDao itemsJbkEquipDao;   
  
    public ItemsJbkEquipManagerImpl(ItemsJbkEquipDao itemsJbkEquipDao) {   
        super(itemsJbkEquipDao);   
        this.itemsJbkEquipDao = itemsJbkEquipDao;   
    }   
  
    public List<ItemsJbkEquip> findByType(String type) {   
        return itemsJbkEquipDao.findByType(type);   
    }
    
    public List<ItemsJbkEquip> findById(Long id) {   
        return itemsJbkEquipDao.findById(id);   
    }  
    
    public List<ItemsJbkEquip> findByDescript(String descript) {   
        return itemsJbkEquipDao.findByDescript(descript);
    }
    
    public List<ItemsJbkEquip> findByDescript(String type,String descript) {   
        return itemsJbkEquipDao.findByDescript(type,descript);
    }
    public List<ItemsJbkEquip> findSection(Long ticket, String itemType) {   
        return itemsJbkEquipDao.findSection(ticket,itemType);
    }
    public void genrateInvoiceNumber(Long id3) {   
       itemsJbkEquipDao.genrateInvoiceNumber(id3);
    }
    
    public int updateWorkTicketCartons(Long ticket){
    	return itemsJbkEquipDao.updateWorkTicketCartons(ticket);
    }
    
    public List findByShipNum(String shipNumber,String corpId){
    	return itemsJbkEquipDao.findByShipNum(shipNumber,corpId);
    }
    public String updateItemsJbkResource(String resourceListServer,String categoryListServer, String qtyListServer, String returnedListServer, String actQtyListServer, String costListServer, String actualListServer,	String sessionCorpID, WorkTicket workTicket, String fromDate, String toDate, String hubId, String chkLimit,String estListServer,String comListServer, String updatedBy){
    	return itemsJbkEquipDao.updateItemsJbkResource(resourceListServer, categoryListServer, qtyListServer, returnedListServer, actQtyListServer, costListServer, actualListServer, sessionCorpID, workTicket, fromDate, toDate, hubId, chkLimit,estListServer,comListServer, updatedBy);
    }

    public OperationsResourceLimits getOperationsResourceHub(String warehouse, String sessionCorpID){
    	return itemsJbkEquipDao.getOperationsResourceHub(warehouse, sessionCorpID);
    }
    public List usedEstQuanitityList(String category, String resource,	String fromDate, String toDate, String hubId, String service, String sessionCorpID,Long ticket){
    	return itemsJbkEquipDao.usedEstQuanitityList(category, resource, fromDate, toDate, hubId, service, sessionCorpID,ticket);
    }
    public List getDailyResourceLimit(String category, String resource, String fromDate, String toDate, String hubId, String sessionCorpID){
    	return itemsJbkEquipDao.getDailyResourceLimit(category, resource, fromDate, toDate, hubId, sessionCorpID);
    }
    public String checkIsExist(Long itemsJbkId, String category, String resource, String sessionCorpID){
    	return itemsJbkEquipDao.checkIsExist(itemsJbkId, category, resource, sessionCorpID);
    }

	public List<ItemsJbkEquip> checkResourceDuplicate(String shipNumber,String corpId, String category, String resource) {
		return itemsJbkEquipDao.checkResourceDuplicate(shipNumber,corpId, category, resource);
	}

	public int findDayTemplate(String shipNumber, String corpID) {
		return itemsJbkEquipDao.findDayTemplate(shipNumber,corpID);
	}

	public void deleteAll(String shipNumber, String corpID, List day) {
		itemsJbkEquipDao.deleteAll(shipNumber, corpID, day);
	}

	public Map<String, List> findByShipNumAndDay(String shipNumber,String corpId,String dayFocus) {
		return itemsJbkEquipDao.findByShipNumAndDay(shipNumber,corpId,dayFocus);
	}
	
	public Map<String, Map> findByShipNumAndDayMap(String shipNumber,String corpId,String dayFocus){
		return itemsJbkEquipDao.findByShipNumAndDayMap(shipNumber,corpId,dayFocus);
	}

	 public List getResourceAutoComplete(String corpId,String tempDescript,String tempCategory){
		 return itemsJbkEquipDao.getResourceAutoComplete(corpId, tempDescript, tempCategory);
	 }

	public void saveDateAjax(String shipNum,String day, String beginDt, String endDt) {
		itemsJbkEquipDao.saveDateAjax(shipNum,day,beginDt,endDt);
	}

	public List<ItemsJbkEquip> getResource(String corpId, String shipNum,String day) {
		return itemsJbkEquipDao.getResource(corpId, shipNum,day);
	}
	public void updateTicketTransferStatus(String shipNumber,Set set){
		itemsJbkEquipDao.updateTicketTransferStatus(shipNumber,set);
	}
	 public void updateAccountLineForCreatePricing(String shipNumber,String category,String resource,String day){
		 itemsJbkEquipDao.updateAccountLineForCreatePricing(shipNumber,category,resource,day);
	 }
	 public void deleteQtyBased(String shipNumber,String corpID){
		 itemsJbkEquipDao.deleteQtyBased(shipNumber,corpID);
	 }
	 public String findByShipNumAndCategoryNull(String shipNumber,String corpID,String day){
		 return itemsJbkEquipDao.findByShipNumAndCategoryNull(shipNumber,corpID,day);
	 }

	public List findByShipNumAndDay(String shipNumber, String day) {
		return itemsJbkEquipDao.findByShipNumAndDay(shipNumber,day);
	}
	 public void deleteItemsJbkResource(ItemsJbkEquip jbkEquip,Boolean isDelete){
		 itemsJbkEquipDao.deleteItemsJbkResource(jbkEquip,isDelete);
	 }
	 public List findByShipNumAndTicket(String shipNumber,String ticket){
		 return itemsJbkEquipDao.findByShipNumAndTicket(shipNumber, ticket);
	 }
	 public String findDateByShipNum(String shipNumber){
		 return itemsJbkEquipDao.findDateByShipNum(shipNumber);
	 }
	 public List findActualCost(String sessionCorpID, String shipnum, Long ticket, String itemType){
		 return itemsJbkEquipDao.findActualCost(sessionCorpID, shipnum, ticket, itemType);
	 }
	 public String getMaxSeqNumber(String sessionCorpID){
		 return itemsJbkEquipDao.getMaxSeqNumber(sessionCorpID);
	 }
	 public List findAllResourcesAjax(String shipNumber,String sessionCorpID)
	 {
		 return itemsJbkEquipDao.findAllResourcesAjax(shipNumber,sessionCorpID);
	 }
	 public Map<String, String> getItemsjequip(String sessionCorpID){
		 return itemsJbkEquipDao.getItemsjequip(sessionCorpID);
	 }
}
