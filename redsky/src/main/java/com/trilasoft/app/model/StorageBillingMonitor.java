package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="storagebillingmonitor")
public class StorageBillingMonitor extends BaseObject{
	private Long id;
	private String corpId;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;	
	
	private String billingType;
	private Date billThroughFrom;
	private Date billThroughTo;	
	private String bookerCode;
	private String storageBillingGroup;
	private String contractType;
	private String incExcGroup;	
	private Date invoiceDate;
	private Date postingDate;
	private Boolean invoice;
	private Boolean billingFlag;	
	private Integer totalCount;
	private Integer presentCount;
	private String status;
	private String type;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpId", corpId).append("createdOn", createdOn)
				.append("createdBy", createdBy).append("updatedOn", updatedOn)
				.append("updatedBy", updatedBy)
				.append("billingType", billingType)
				.append("billThroughFrom", billThroughFrom)
				.append("billThroughTo", billThroughTo)
				.append("bookerCode", bookerCode)
				.append("storageBillingGroup", storageBillingGroup)
				.append("contractType", contractType)
				.append("incExcGroup", incExcGroup)
				.append("invoiceDate", invoiceDate)
				.append("postingDate", postingDate).append("invoice", invoice)
				.append("billingFlag", billingFlag)
				.append("totalCount", totalCount)
				.append("presentCount", presentCount).append("status", status)
				.append("type", type).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof StorageBillingMonitor))
			return false;
		StorageBillingMonitor castOther = (StorageBillingMonitor) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(billingType, castOther.billingType)
				.append(billThroughFrom, castOther.billThroughFrom)
				.append(billThroughTo, castOther.billThroughTo)
				.append(bookerCode, castOther.bookerCode)
				.append(storageBillingGroup, castOther.storageBillingGroup)
				.append(contractType, castOther.contractType)
				.append(incExcGroup, castOther.incExcGroup)
				.append(invoiceDate, castOther.invoiceDate)
				.append(postingDate, castOther.postingDate)
				.append(invoice, castOther.invoice)
				.append(billingFlag, castOther.billingFlag)
				.append(totalCount, castOther.totalCount)
				.append(presentCount, castOther.presentCount)
				.append(status, castOther.status).append(type, castOther.type)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(createdOn).append(createdBy).append(updatedOn)
				.append(updatedBy).append(billingType).append(billThroughFrom)
				.append(billThroughTo).append(bookerCode)
				.append(storageBillingGroup).append(contractType)
				.append(incExcGroup).append(invoiceDate).append(postingDate)
				.append(invoice).append(billingFlag)
				.append(totalCount).append(presentCount).append(status)
				.append(type).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public String getBillingType() {
		return billingType;
	}
	public void setBillingType(String billingType) {
		this.billingType = billingType;
	}
	@Column
	public Date getBillThroughFrom() {
		return billThroughFrom;
	}
	public void setBillThroughFrom(Date billThroughFrom) {
		this.billThroughFrom = billThroughFrom;
	}
	@Column
	public Date getBillThroughTo() {
		return billThroughTo;
	}
	public void setBillThroughTo(Date billThroughTo) {
		this.billThroughTo = billThroughTo;
	}
	@Column
	public String getBookerCode() {
		return bookerCode;
	}
	public void setBookerCode(String bookerCode) {
		this.bookerCode = bookerCode;
	}
	@Column
	public String getStorageBillingGroup() {
		return storageBillingGroup;
	}
	public void setStorageBillingGroup(String storageBillingGroup) {
		this.storageBillingGroup = storageBillingGroup;
	}
	@Column
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	@Column
	public String getIncExcGroup() {
		return incExcGroup;
	}
	public void setIncExcGroup(String incExcGroup) {
		this.incExcGroup = incExcGroup;
	}
	@Column
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	@Column
	public Date getPostingDate() {
		return postingDate;
	}
	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}
	@Column
	public Boolean getInvoice() {
		return invoice;
	}
	public void setInvoice(Boolean invoice) {
		this.invoice = invoice;
	}
	@Column
	public Boolean getBillingFlag() {
		return billingFlag;
	}
	public void setBillingFlag(Boolean billingFlag) {
		this.billingFlag = billingFlag;
	}
	@Column
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	@Column
	public Integer getPresentCount() {
		return presentCount;
	}
	public void setPresentCount(Integer presentCount) {
		this.presentCount = presentCount;
	}
	@Column
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
