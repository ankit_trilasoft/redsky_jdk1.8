/**
 * WSCheckAllThatApplyQuestion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class WSCheckAllThatApplyQuestion  extends com.keysurvey.api.v68.form.design.WSBaseQuestion  implements java.io.Serializable {
    private com.keysurvey.api.v68.form.design.WSAnswer other;

    private com.keysurvey.api.v68.form.design.WSAnswerRestriction restrict;

    public WSCheckAllThatApplyQuestion() {
    }

    public WSCheckAllThatApplyQuestion(
           long versionId,
           com.keysurvey.api.v68.form.design.WSAnswerSortType answerSortType,
           long formId,
           long questionId,
           java.lang.String text,
           com.keysurvey.api.v68.form.design.WSAnswer[] answers,
           com.keysurvey.api.v68.form.design.WSAnswer other,
           com.keysurvey.api.v68.form.design.WSAnswerRestriction restrict) {
        super(
            versionId,
            answerSortType,
            formId,
            questionId,
            text,
            answers);
        this.other = other;
        this.restrict = restrict;
    }


    /**
     * Gets the other value for this WSCheckAllThatApplyQuestion.
     * 
     * @return other
     */
    public com.keysurvey.api.v68.form.design.WSAnswer getOther() {
        return other;
    }


    /**
     * Sets the other value for this WSCheckAllThatApplyQuestion.
     * 
     * @param other
     */
    public void setOther(com.keysurvey.api.v68.form.design.WSAnswer other) {
        this.other = other;
    }


    /**
     * Gets the restrict value for this WSCheckAllThatApplyQuestion.
     * 
     * @return restrict
     */
    public com.keysurvey.api.v68.form.design.WSAnswerRestriction getRestrict() {
        return restrict;
    }


    /**
     * Sets the restrict value for this WSCheckAllThatApplyQuestion.
     * 
     * @param restrict
     */
    public void setRestrict(com.keysurvey.api.v68.form.design.WSAnswerRestriction restrict) {
        this.restrict = restrict;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSCheckAllThatApplyQuestion)) return false;
        WSCheckAllThatApplyQuestion other = (WSCheckAllThatApplyQuestion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.other==null && other.getOther()==null) || 
             (this.other!=null &&
              this.other.equals(other.getOther()))) &&
            ((this.restrict==null && other.getRestrict()==null) || 
             (this.restrict!=null &&
              this.restrict.equals(other.getRestrict())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getOther() != null) {
            _hashCode += getOther().hashCode();
        }
        if (getRestrict() != null) {
            _hashCode += getRestrict().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSCheckAllThatApplyQuestion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSCheckAllThatApplyQuestion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("other");
        elemField.setXmlName(new javax.xml.namespace.QName("", "other"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("restrict");
        elemField.setXmlName(new javax.xml.namespace.QName("", "restrict"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswerRestriction"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
