// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `daystoManageAlert` INTEGER DEFAULT 0 AFTER `sequence`;

ALTER TABLE `redsky`.`billing` ADD COLUMN `networkBillToCode` VARCHAR(8) DEFAULT '',
ADD COLUMN `networkBillToName` VARCHAR(225) DEFAULT '' AFTER `networkBillToCode`;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `networkBillToCode` VARCHAR(8) DEFAULT '' ,
 ADD COLUMN `networkBillToName` VARCHAR(225) DEFAULT '' AFTER `networkBillToCode`;
 
 ALTER TABLE `redsky`.`app_user` ADD COLUMN `defaultOperationCalendar` VARCHAR(45) AFTER `serviceType`;
 
 Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'Ops Calendar', 'Ops Calendar', 25, 'OPSCALENDAR','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'Crew Calendar', 'Crew Calendar', 25, 'OPSCALENDAR','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'Work Planning', 'Work Planning', 25, 'OPSCALENDAR','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'Planning Calendar', 'Planning Calendar', 25, 'OPSCALENDAR','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','OPSCALENDAR',25,true,now(),'TSFT_SETUP',now(),'TSFT_SETUP',1,1,'');

update refmaster set flex5='MR3' where corpid='SSCW' and parameter='HOUSE' and flex5='LDA';
update refmaster set flex5='MR4' where corpid='SSCW'and parameter='HOUSE' and flex5='LCY';

update networkdatafields set transactiontype='Create' where fieldName='initialContactDate';

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('AccountLine','networkBillToCode' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','billToCode','DMM'),
      ('AccountLine','networkBillToName' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','billToName','DMM'),
      ('AccountLine','networkBillToCode' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','billToCode','DMM'),
      ('AccountLine','networkBillToName' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','billToName','DMM');
 
      ALTER TABLE `redsky`.`partnerbankinformation` CHANGE COLUMN `cropId` `corpId` VARCHAR(15) NULL DEFAULT NULL;     

      CREATE TABLE  `redsky`.`itemdata` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `workTicketId` bigint(20) NOT NULL DEFAULT '0',
  `serviceOrderId` bigint(20) NOT NULL DEFAULT '0',
  `itemDesc` varchar(100) DEFAULT NULL,
  `quantityReceived` int(40) DEFAULT NULL,
  `warehouse` varchar(80) DEFAULT NULL,
  `line` int(10) DEFAULT NULL,
  `volume` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `itemUomid` varchar(40) DEFAULT NULL,
  `totalWeight` double DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
)

ALTER TABLE `redsky`.`itemdata` ADD COLUMN `updatedBy` VARCHAR(82),
 ADD COLUMN `updatedOn` DATETIME ,
 ADD COLUMN `createdBy` VARCHAR(82) ,
 ADD COLUMN `createdOn` DATETIME,
 ADD COLUMN `corpId` VARCHAR(5);
 
 alter table serviceorder add clientId varchar(140) ;
 
 ALTER TABLE `redsky`.`itemdata` ADD COLUMN `qtyExpected` VARCHAR(40) ,
 ADD COLUMN `itemQtyExpected` VARCHAR(40) ;
 
 ALTER TABLE `redsky`.`itemdata` ADD COLUMN `itemQtyReceived` VARCHAR(40) ;
 
insert into extractcolumnmgmt(corpID, createdBy, createdOn, updatedBy, updatedOn, reportName,
tableName, fieldName, displayName, columnSequenceNumber)
values('VOER', 'adminvoerman', now(), 'adminvoerman', now(), 'dynamicFinancialSummary', 'serviceorder',
"s.clientId as CLIENT_ID", 'CLIENT_ID', 1950);

insert into extractcolumnmgmt(corpID, createdBy, createdOn, updatedBy, updatedOn, reportName,
tableName, fieldName, displayName, columnSequenceNumber)
values('VOER', 'adminvoerman', now(), 'adminvoerman', now(), 'dynamicFinancialDetails', 'serviceorder',
"s.clientId as CLIENT_ID", 'CLIENT_ID', 1951);