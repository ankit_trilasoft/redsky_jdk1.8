<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="AA_Missing_Est_Weight"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="792"
		 pageHeight="612"
		 columnWidth="756"
		 columnSpacing="0"
		 leftMargin="18"
		 rightMargin="18"
		 topMargin="18"
		 bottomMargin="18"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="begindate" isForPrompting="true" class="java.util.Date">
		<defaultValueExpression ><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="enddate" isForPrompting="true" class="java.util.Date">
		<defaultValueExpression ><![CDATA[new Date() ]]></defaultValueExpression>
	</parameter>
	<queryString><![CDATA[SELECT
     'AA_Missing_Est_Weight',
     serviceorder.`corpID` AS serviceorder_corpID,
     serviceorder.`shipNumber` AS serviceorder_shipNumber,
     serviceorder.`status` AS serviceorder_status,
     serviceorder.`routing` AS serviceorder_routing,
     serviceorder.`packingMode` AS serviceorder_packingMode,
     serviceorder.`mode` AS serviceorder_mode,
     serviceorder.`lastName` AS serviceorder_lastName,
     serviceorder.`job` AS serviceorder_job,
     serviceorder.`firstName` AS serviceorder_firstName,
     CONCAT(serviceorder.`lastName`,", ",serviceorder.`firstName`)AS Shipper,
     serviceorder.`email2` AS serviceorder_email2,
     serviceorder.`email` AS serviceorder_email,
     serviceorder.`registrationNumber` AS serviceorder_registrationNumber,
     workticket.`city` AS workticket_city,
     workticket.`confirm` AS workticket_confirm,
     workticket.`confirmDate` AS workticket_confirmDate,
     workticket.`corpID` AS workticket_corpID,
     workticket.`crews` AS workticket_crews,
     workticket.`date1` AS workticket_date1,
     workticket.`date2` AS workticket_date2,
     workticket.`estimatedCartoons` AS workticket_estimatedCartoons,
     workticket.`estimatedCubicFeet` AS workticket_estimatedCubicFeet,
     workticket.`estimatedWeight` AS workticket_estimatedWeight,
     workticket.`homePhone` AS workticket_homePhone,
     workticket.`instructions` AS workticket_instructions,
     workticket.`instructionCode` AS workticket_instructionCode,
     workticket.`leaveWareHouse` AS workticket_leaveWareHouse,
     workticket.`originCountry` AS workticket_originCountry,
     workticket.`originMobile` AS workticket_originMobile,
     workticket.`originFax` AS workticket_originFax,
     workticket.`phone` AS workticket_phone,
     workticket.`scheduled1` AS workticket_scheduled1,
     workticket.`scheduled2` AS workticket_scheduled2,
     workticket.`scanned` AS workticket_scanned,
     workticket.`shipNumber` AS workticket_shipNumber,
     workticket.`service` AS workticket_service,
     workticket.`ticket` AS workticket_ticket,
     workticket.`targetActual` AS workticket_targetActual,
     workticket.`warehouse` AS workticket_warehouse,
     workticket.`zip` AS workticket_zip,
     workticket.`coordinator` AS workticket_coordinator,
     serviceorder.`coordinator` AS serviceorder_coordinator,
     workticket.`serviceOrderId` AS workticket_serviceOrderId,
     serviceorder.`id` AS serviceorder_id,
     customerfile.`id` AS customerfile_id,
     customerfile.`survey` AS customerfile_survey,
     serviceorder.`commodity` AS serviceorder_commodity,
     customerfile.`salesMan` AS customerfile_salesMan,
     customerfile.`estimator` AS customerfile_estimator,
     customerfile.`estimatorName` AS customerfile_estimatorName,
     serviceorder.`customerFileId` AS serviceorder_customerFileId
FROM
     `workticket` workticket LEFT OUTER JOIN `serviceorder` serviceorder ON workticket.`serviceOrderId` = serviceorder.`id`
     LEFT OUTER JOIN `customerfile` customerfile ON serviceorder.`customerFileId` = customerfile.`id`
WHERE
     workticket.`corpID` = "SSCW"
 and serviceorder.`job` in ("UVL","MVL","STO","INT","LOC","DOM","GST","VAI")
 AND workticket.`targetActual` <> 'C'
 AND workticket.`service` IN ("PK","LD","DL","DU","UP")
 AND (workticket.`estimatedWeight` = 0
     or workticket.`estimatedWeight` IS null)
AND (
       (workticket.`date1` between $P{begindate}  AND  $P{enddate})
    OR (workticket.`date2` between $P{begindate}  AND $P{enddate})
    OR (workticket.`date1` < $P{begindate} AND workticket.`date2` > $P{enddate})
)]]></queryString>

	<field name="AA_Missing_Est_Weight" class="java.lang.String"/>
	<field name="serviceorder_corpID" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_status" class="java.lang.String"/>
	<field name="serviceorder_routing" class="java.lang.String"/>
	<field name="serviceorder_packingMode" class="java.lang.String"/>
	<field name="serviceorder_mode" class="java.lang.String"/>
	<field name="serviceorder_lastName" class="java.lang.String"/>
	<field name="serviceorder_job" class="java.lang.String"/>
	<field name="serviceorder_firstName" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="serviceorder_email2" class="java.lang.String"/>
	<field name="serviceorder_email" class="java.lang.String"/>
	<field name="serviceorder_registrationNumber" class="java.lang.String"/>
	<field name="workticket_city" class="java.lang.String"/>
	<field name="workticket_confirm" class="java.lang.String"/>
	<field name="workticket_confirmDate" class="java.sql.Timestamp"/>
	<field name="workticket_corpID" class="java.lang.String"/>
	<field name="workticket_crews" class="java.lang.Integer"/>
	<field name="workticket_date1" class="java.sql.Timestamp"/>
	<field name="workticket_date2" class="java.sql.Timestamp"/>
	<field name="workticket_estimatedCartoons" class="java.lang.Integer"/>
	<field name="workticket_estimatedCubicFeet" class="java.lang.Double"/>
	<field name="workticket_estimatedWeight" class="java.lang.Double"/>
	<field name="workticket_homePhone" class="java.lang.String"/>
	<field name="workticket_instructions" class="java.lang.String"/>
	<field name="workticket_instructionCode" class="java.lang.String"/>
	<field name="workticket_leaveWareHouse" class="java.lang.String"/>
	<field name="workticket_originCountry" class="java.lang.String"/>
	<field name="workticket_originMobile" class="java.lang.String"/>
	<field name="workticket_originFax" class="java.lang.String"/>
	<field name="workticket_phone" class="java.lang.String"/>
	<field name="workticket_scheduled1" class="java.sql.Timestamp"/>
	<field name="workticket_scheduled2" class="java.sql.Timestamp"/>
	<field name="workticket_scanned" class="java.sql.Timestamp"/>
	<field name="workticket_shipNumber" class="java.lang.String"/>
	<field name="workticket_service" class="java.lang.String"/>
	<field name="workticket_ticket" class="java.lang.Long"/>
	<field name="workticket_targetActual" class="java.lang.String"/>
	<field name="workticket_warehouse" class="java.lang.String"/>
	<field name="workticket_zip" class="java.lang.String"/>
	<field name="workticket_coordinator" class="java.lang.String"/>
	<field name="serviceorder_coordinator" class="java.lang.String"/>
	<field name="workticket_serviceOrderId" class="java.lang.Long"/>
	<field name="serviceorder_id" class="java.lang.Long"/>
	<field name="customerfile_id" class="java.lang.Long"/>
	<field name="customerfile_survey" class="java.sql.Timestamp"/>
	<field name="serviceorder_commodity" class="java.lang.String"/>
	<field name="customerfile_salesMan" class="java.lang.String"/>
	<field name="customerfile_estimator" class="java.lang.String"/>
	<field name="customerfile_estimatorName" class="java.lang.String"/>
	<field name="serviceorder_customerFileId" class="java.math.BigInteger"/>

	<sortField name="workticket_date1" />
	<sortField name="Shipper" />
	<sortField name="serviceorder_shipNumber" />

	<variable name="tktcount" class="java.lang.Integer" resetType="Group" resetGroup="date1" calculation="DistinctCount">
		<variableExpression><![CDATA[$F{workticket_ticket}]]></variableExpression>
	</variable>

		<group  name="date1" >
			<groupExpression><![CDATA[$F{workticket_date1}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="35"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="#,##0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="257"
						y="10"
						width="64"
						height="18"
						forecolor="#0000CC"
						key="textField"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$V{tktcount}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="33"
						y="10"
						width="148"
						height="18"
						forecolor="#0000CC"
						key="staticText-22"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Number of tickets for ]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="184"
						y="10"
						width="56"
						height="18"
						forecolor="#0000CC"
						key="textField-4"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-BoldOblique" size="10" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{workticket_date1}]]></textFieldExpression>
				</textField>
				<rectangle>
					<reportElement
						x="0"
						y="7"
						width="756"
						height="0"
						key="rectangle-1"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="4"
						width="756"
						height="0"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="48"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="185"
						y="0"
						width="412"
						height="23"
						forecolor="#663300"
						key="staticText-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#663300"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#663300"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" size="18" isBold="true"/>
					</textElement>
				<text><![CDATA[Workticket Missing Estimated Weight]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="483"
						y="23"
						width="107"
						height="20"
						key="textField-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="14"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{enddate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="340"
						y="25"
						width="98"
						height="18"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="14"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{begindate}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="437"
						y="23"
						width="36"
						height="21"
						key="staticText-19"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Arial" size="14"/>
					</textElement>
				<text><![CDATA[  to]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="201"
						y="23"
						width="136"
						height="21"
						key="staticText-23"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="14"/>
					</textElement>
				<text><![CDATA[Workticket Date  ]]></text>
				</staticText>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="34"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="1"
						y="16"
						width="92"
						height="16"
						key="staticText-4"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Shipper]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="539"
						y="16"
						width="31"
						height="16"
						key="staticText-6"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Svc]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="268"
						y="16"
						width="52"
						height="14"
						key="staticText-7"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[S/O #]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="387"
						y="16"
						width="61"
						height="16"
						key="staticText-8"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Tcket #]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="451"
						y="16"
						width="43"
						height="16"
						key="staticText-10"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[WH #]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="500"
						y="16"
						width="34"
						height="16"
						key="staticText-13"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Job]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="600"
						y="16"
						width="64"
						height="16"
						key="staticText-14"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Coordinator]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="681"
						y="16"
						width="73"
						height="16"
						key="staticText-15"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Sales Rep]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="188"
						y="16"
						width="69"
						height="15"
						key="staticText-21"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Survey Date]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="19"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="539"
						y="0"
						width="31"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{workticket_service}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="267"
						y="0"
						width="102"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{workticket_shipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="387"
						y="0"
						width="61"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.Long"><![CDATA[$F{workticket_ticket}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="466"
						y="0"
						width="27"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{workticket_warehouse}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="0"
						width="171"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Shipper}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="500"
						y="0"
						width="33"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_job}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="193"
						y="0"
						width="66"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{customerfile_survey}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="674"
						y="0"
						width="78"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_estimator}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="585"
						y="0"
						width="78"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_coordinator}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="27"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="574"
						y="8"
						width="134"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="715"
						y="8"
						width="36"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="8"
						width="209"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" size="10"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="6"
						width="756"
						height="0"
						forecolor="#00FFCC"
						key="line-1"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="2.0" lineStyle="Solid"/>
</graphicElement>
				</line>
				<staticText>
					<reportElement
						x="302"
						y="8"
						width="224"
						height="13"
						key="staticText-20"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[AA_Missing_Est_Weight.jrxml]]></text>
				</staticText>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
