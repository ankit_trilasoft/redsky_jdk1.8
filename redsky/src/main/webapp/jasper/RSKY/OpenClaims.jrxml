<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="OpenClaims" pageWidth="736" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="676" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="76ace272-e767-4204-bf11-8a4149d65f00">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT	"OpenClaims-R-RSKY.jrxml",
	if(billing.billToCode is null || billing.billToCode='','',billing.billToCode) AS billing_billToCode,
	billing.`billToName` AS billing_billToName,
	claim.claimNumber AS claim_claimNumber,
	if(serviceorder.companyDivision is null || serviceorder.companyDivision='','',serviceorder.companyDivision) AS serviceorder_companyDivision,
	if((serviceorder.`firstName` is null || trim(serviceorder.`firstName`)=''),trim(serviceorder.`lastName`),concat(trim(serviceorder.`firstName`)," ",trim(serviceorder.`lastName`))) AS shipper_name,
	DATE_FORMAT(claim.requestForm,'%d-%b-%y') AS claim_requestForm,
	DATE_FORMAT(claim.formSent,'%d-%b-%y') AS claim_formSent,
	DATE_FORMAT(claim.formRecived,'%d-%b-%y') AS claim_formRecived,
	DATE_FORMAT(claim.insuranceCompanyPaid,'%d-%b-%y') AS claim_insuranceCompanyPaid,
	DATE_FORMAT(claim.insurerNotification,'%d-%b-%y') AS claim_insurerNotification,
	DATE_FORMAT(now(),'%d-%b-%y') AS currentdate
FROM	claim claim
	LEFT OUTER JOIN billing billing on claim.serviceorderid=billing.id AND billing.corpId=$P{Corporate ID}
	INNER JOIN serviceorder serviceorder on billing.id=serviceorder.id AND serviceorder.corpId=$P{Corporate ID}

WHERE claim.`corpId`=$P{Corporate ID}
	AND claim.`cancelled` is null
	AND claim.`closeDate` is null
	AND serviceorder.`status` not in ('CNCL')
ORDER BY serviceorder.companyDivision, billing.`billToCode`, claim.`claimNumber`]]>
	</queryString>
	<field name="OpenClaims-R-RSKY.jrxml" class="java.lang.String"/>
	<field name="billing_billToCode" class="java.lang.String"/>
	<field name="billing_billToName" class="java.lang.String"/>
	<field name="claim_claimNumber" class="java.lang.Long"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="shipper_name" class="java.lang.String"/>
	<field name="claim_requestForm" class="java.lang.String"/>
	<field name="claim_formSent" class="java.lang.String"/>
	<field name="claim_formRecived" class="java.lang.String"/>
	<field name="claim_insuranceCompanyPaid" class="java.lang.String"/>
	<field name="claim_insurerNotification" class="java.lang.String"/>
	<field name="currentdate" class="java.lang.String"/>
	<group name="CompDiv" isStartNewPage="true">
		<groupExpression><![CDATA[$F{serviceorder_companyDivision}]]></groupExpression>
		<groupHeader>
			<band height="18" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" x="102" y="0" width="156" height="18" uuid="bc5857da-72da-4e4b-a656-95d0a9c94255"/>
					<textElement>
						<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{serviceorder_companyDivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-13" x="0" y="0" width="102" height="18" uuid="e7fdd1d1-5cbd-477e-892f-052ea44460a6"/>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="64" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" x="0" y="35" width="64" height="29" uuid="1c0fd277-88a4-4cbe-b926-3f66847b3af7"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill To Code]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" x="64" y="35" width="113" height="29" uuid="c8eebacc-4d08-4d1b-942c-6fff9a556500"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill To Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" x="177" y="35" width="44" height="29" uuid="4209c5ea-08f0-4a2b-aa9b-1c4d9de8470e"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Claim #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" x="221" y="35" width="128" height="29" uuid="845d3e1b-3aa6-4cbe-9e5b-3b5e43052905"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" x="349" y="35" width="64" height="29" uuid="b976f4c8-baea-4822-a85a-2ad05b4d949f"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Req. Form]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="413" y="35" width="64" height="29" uuid="4fa7d654-226c-4821-a03c-caa63284095e"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Form Sent]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="477" y="35" width="64" height="29" uuid="e139f79f-15d2-43e6-b52b-adef2c28b632"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Form Recd.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" x="541" y="35" width="64" height="29" uuid="9fc37d0b-06e7-4bc3-a9df-0b6252a5166e"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Insurer Notification]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-11" x="0" y="7" width="676" height="24" uuid="0ad985af-5850-4a2a-b470-8e2079979df2"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="13" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Open Claims]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-12" x="605" y="35" width="71" height="29" uuid="4d35dbe7-3265-4ff3-a0d3-bcf8813930e0"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Insurance Comp Paid]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="26" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="0" y="1" width="64" height="25" uuid="106605be-de49-4773-a1dd-170e79c66b53"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_billToCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="64" y="1" width="113" height="25" uuid="9ee4737f-86cb-4879-9ef2-8178bb376155"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_billToName}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="177" y="1" width="44" height="25" uuid="1ce261a2-80da-43ab-b787-8d1ddda6be8b"/>
				<textElement textAlignment="Left">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{claim_claimNumber}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="221" y="1" width="128" height="25" uuid="a991062e-99d6-43ce-a980-ed76ab7ed935"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{shipper_name}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="349" y="1" width="64" height="25" uuid="abf06a6e-ec6d-4053-a2a8-3e38f6b33888"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{claim_requestForm}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="413" y="1" width="64" height="25" uuid="2e98f0ae-058d-429d-964f-aa5e7be71bc0"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{claim_formSent}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="477" y="1" width="64" height="25" uuid="5fbdf931-a27c-438c-8071-52a4702bd4d7"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{claim_formRecived}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="541" y="1" width="64" height="25" uuid="e381bf22-5c2c-4195-942a-0928ce7b29d2"/>
				<textElement textAlignment="Center">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{claim_insurerNotification}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-4" x="605" y="1" width="71" height="25" uuid="d34eecb5-8857-4c74-8463-1170c9c6a2f4"/>
				<textElement textAlignment="Center">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{claim_insuranceCompanyPaid}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="17" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-1" x="505" y="2" width="130" height="15" uuid="1ec4f6a2-4f80-4ac1-a183-06c9aef5a4c8"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="true">
				<reportElement key="textField-2" x="635" y="2" width="41" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="aa3525de-f689-4627-bc58-f590755fc707"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="CP1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-3" x="1" y="2" width="142" height="15" uuid="1c94eb3d-7744-4f7d-a769-4a6a0b94f2c7"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{currentdate}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-10" x="200" y="1" width="276" height="15" uuid="93445317-8332-4df9-8bb0-97c468d8b171"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[OpenClaims.jrxml]]></text>
			</staticText>
			<line>
				<reportElement key="line-1" x="0" y="1" width="676" height="1" uuid="59b88c9c-e594-46bc-af9d-c7b56b32e707"/>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
