package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.WorkTicketTimeManagement;

public interface WorkTicketTimeManagementDao extends GenericDao<WorkTicketTimeManagement, Long> {
	public List getTimeManagementManagerDetails(Long Id, Long ticket, String sessionCorpID);
}
