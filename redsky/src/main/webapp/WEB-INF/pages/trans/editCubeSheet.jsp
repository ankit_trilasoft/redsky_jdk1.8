<%@ include file="/common/taglibs.jsp"%>
<head>


<title>Survey Detail</title>
<meta name="heading"
	content="Survey Detail" />
<style type=text/css>
div.autocomplete {
      position:absolute;
      width:250px;
      background-color:white;
      border:1px solid #888;
      margin:0px;
      padding:0px;
      z-index:99999;
    }
    div.autocomplete ul {
      list-style-type:none;
      margin:0px;
      padding:0px;
    }
    div.autocomplete ul li.selected { background-color: #ffb;}
    div.autocomplete ul li {
      list-style-type:none;
      display:block;
      margin:0;
      padding:0px;
      padding-left:3px;   
      cursor:pointer;
    }
</style>	
<%-- <script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>--%>
<script type="text/javascript">


function calculationTotalCft(){
var quantity= 0;
var total=0;
var totalRound=0;
var weight=0;
var totalWeight=0;
var totalRoundWeight=0
quantity=document.forms['cubeSheetForm'].elements['inventoryData.quantity'].value;
weight=document.forms['cubeSheetForm'].elements['inventoryData.weight'].value;
var cft =0
cft =document.forms['cubeSheetForm'].elements['inventoryData.cft'].value;
if(cft=='.' ){
cft=0;
document.forms['cubeSheetForm'].elements['inventoryData.cft'].value=0;
}
total=cft*quantity;
totalRound=Math.round(total*100)/100;
totalWeight=weight*quantity;
totalRoundWeight=Math.round(totalWeight*100)/100; 
document.forms['cubeSheetForm'].elements['inventoryData.total'].value=totalRound;
document.forms['cubeSheetForm'].elements['inventoryData.totalWeight'].value=totalRoundWeight;
}
function add(){
	window.location ="editSurveyDetails.html?cid=${cid}&id=&sid=${sid}&SOflag=${SOflag}&Quote=${Quote}&SoMode=${inventoryData.mode}";
	}
function linkToImages(){
   window.open('imageListUpload.html?cid=${cid}&id=${id}','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');
}
function showHideOther(result){
   if(result.value=='other'){
   document.getElementById('otherlable').style.display='block';
   document.getElementById('othertextfield').style.display='block';
   }else{
   document.getElementById('otherlable').style.display='none';
   document.getElementById('othertextfield').style.display='none';
   }
}
window.onload=function(){
new Ajax.Autocompleter("autocomplete", "autocomplete_choices", "/redsky/articleAutocomplete.html?decorator=simple&popup=true", {afterUpdateElement : getSelectionId});
}
function getSelectionId(text, li) {
	//alert('hello');
   // alert (li.id);
}
function checkValue(result){
var values=(result.id).split('~');
document.forms['cubeSheetForm'].elements['inventoryData.cft'].value=values[0];
document.forms['cubeSheetForm'].elements['inventoryData.weight'].value=values[1];

 document.getElementById('autocomplete').value=result.innerHTML;
 var quantity= document.forms['cubeSheetForm'].elements['inventoryData.quantity'].value;
 if(quantity ==''){
   quantity=1;
   document.forms['cubeSheetForm'].elements['inventoryData.quantity'].value=1;
   }
var cft =document.forms['cubeSheetForm'].elements['inventoryData.cft'].value;
var weight=document.forms['cubeSheetForm'].elements['inventoryData.weight'].value;
var total=0;
var totalRound=0;
var totalWeight=0;
var totalRoundWeight=0;
total=cft*quantity;
totalRound=Math.round(total*100)/100;
totalWeight=weight*quantity;
totalRoundWeight=Math.round(totalWeight*100)/100;  
document.forms['cubeSheetForm'].elements['inventoryData.total'].value=totalRound;
document.forms['cubeSheetForm'].elements['inventoryData.totalWeight'].value=totalRoundWeight;
}
function checkvalidation(){
var validationQuantity="";
var validationCft="";
var validationAtricle="";
var quantity=document.forms['cubeSheetForm'].elements['inventoryData.quantity'].value;
var volume=document.forms['cubeSheetForm'].elements['inventoryData.cft'].value;
var article=document.forms['cubeSheetForm'].elements['inventoryData.atricle'].value;

if(quantity.trim()==''){
    validationQuantity="Quantity is a required field";
}
if(volume.trim()==''){
    validationCft="Volume is a required field";
}
if(article.trim()==''){
    validationAtricle="Article is a required field";
}
if(validationQuantity!='' || validationCft !='' || validationAtricle!=''){
alert(validationAtricle+'\n'+validationCft+'\n'+validationQuantity);
return false;
}else{
  return true;
  }
}
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0,limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

function wordCount(comment){
	commentLenght = comment.value.length;
	if(commentLenght>1950){
		alert("Please enter comment less than 2000 charecters.");
		return false;
	}
}
function putNTBSValue(){
var txtValue = document.forms['cubeSheetForm'].elements['inventoryData.other'].value.toUpperCase();
	if(txtValue=='NOT GOING'){
		document.forms['cubeSheetForm'].elements['inventoryData.ntbsFlag'].checked = true ;
	}
}

function onlyNumeric(targetElement)
	{   
	var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        return false;
	        }
	    }
	    return true;
	}
</script>	
</head>
<s:form id="cubeSheetForm" name="cubeSheetForm" action="saveSurveyDetails" method="post" validate="true"  onsubmit="return checkvalidation();" > 
<div id="Layer1" style="width:100%">
<s:hidden name="id" value="${id}"/>
<s:hidden name="cid" value="${cid}"/>
<s:hidden name="SOflag" value="${SOflag}"/>
<s:hidden name="inventoryData.id" value="${inventoryData.id}"/>
<s:hidden name="inventoryData.corpID" value="${inventoryData.corpID}"/>
<s:hidden name="inventoryData.customerFileID" value="${inventoryData.customerFileID}"/>
<s:hidden name="inventoryData.serviceOrderID" value="${sid}"/>
<s:hidden name="inventoryData.imagesAvailablityFlag"/>
<s:hidden name="Quote" value="${Quote}"/>
<s:hidden name="countdown"  value="250"/>
<c:if test="${SOflag=='CF'}">
<c:set var="idOfWhom" value="${cid}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<c:if test="${empty cid}">
	 <c:set var="isTrue" value="false" scope="request" />
 </c:if> 
  <c:if test="${not empty cid}">
	 <c:set var="isTrue" value="true" scope="request" />
 </c:if>
</c:if>
<c:if test="${SOflag=='SO'}">
<c:set var="idOfWhom" value="${sid}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/> 
<c:if test="${empty sid}">
	 <c:set var="isTrue" value="false" scope="request" />
 </c:if> 
  <c:if test="${not empty sid}">
	 <c:set var="isTrue" value="true" scope="request" />
 </c:if> 
</c:if>
<c:set var="soMode" value="<%= request.getParameter("SoMode") %>" />
<div id="newmnav">
		    <ul>
		    <c:if test="${SOflag=='CF'}">
			<li  ><a href="surveyDetails.html?cid=${cid}&Quote=${Quote}"><span>Survey Data List</span></a></li>
			</c:if>
			<c:if test="${SOflag=='SO'}">
			<li ><a href="inventoryDataList.html?cid=${cid}&id=${sid}&Quote=${Quote}"><span>Survey Data List</span></a></li>
			</c:if>
			<li id="newmnav1" style="background:#FFF;"><a class="current"><span>Survey Data<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    </ul>
		    </div><div class="spn">&nbsp;</div>
		    <div style="margin-bottom:-2px;!padding-bottom:6px;"></div>
		    <div id="content" align="center" >
            <div id="liquid-round">
            <div class="top" ><span></span></div>
            <div class="center-content">  
           
            <table cellspacing="0" cellpadding="2" class="detailTabLabel" style="margin:0px;">
	  	   <tr>
	  	   		 <td width="25px" class="listwhitetext" align="right" ></td>
	  	   		<td  class="listwhitetext" align="right">Mode</td>
	  		<c:if test="${SOflag=='CF'}">
			<td align="left" class="listwhitetext" style="width:90px;"  ><s:select id="inventoryMode" name="inventoryData.mode"  list="{'AIR','SEA','STORAGE','TRUCK','other'}"  cssStyle="width:95px;" cssClass="list-menu" onchange="showHideOther(this)" /></td>
			</c:if>
			<c:if test="${SOflag=='SO'}">
			<td align="left" class="listwhitetext" style="width:90px;"  >
			<c:if test="${empty inventoryData.id}">
				<s:textfield name="inventoryData.mode"  cssStyle="width:73px; " cssClass="input-textUpper"  value="${soMode}" readonly="true" />
			</c:if>
			<c:if test="${not empty inventoryData.id}">
				<s:textfield name="inventoryData.mode"  cssStyle="width:73px; " cssClass="input-textUpper"  value="${inventoryData.mode}" readonly="true" />
			</c:if>
			 <!--  <s:select name="inventoryData.mode"  list="{'AIR','SEA','STORAGE','TRUCK','other'}"  cssStyle="width:70px; !width:50px;" cssClass="list-menu"  onchange="this.value='${soMode}';showHideOther(this);" /> -->
			
			
			</td>
			</c:if>	  				  				  	   	    
	  	   	    <td width="" colspan="2" id="otherlable">
	  	   	    <table cellspacing="0" cellpadding="0" border="0" class="detailTabLabel">
	  	   		<tr>	  	   	    
	  	   	    <td  class="listwhitetext" align="right" >Other</td>
	  	   	    <td align="left" class="listwhitetext" id="othertextfield"><s:textfield cssClass="input-text"  name="inventoryData.other" size="20" maxlength="25" onchange="putNTBSValue();" /></td>
	  	   	    </tr>
	  	   	    </table>	  	   	    
	  	   	    </td>
	  	   	     <td  class="listwhitetext" align="right" width="79px">Room</td>	  			
	  			<td align="left" class="listwhitetext" ><s:select cssClass="list-menu" cssStyle="width:96px;" name="inventoryData.room" list="%{roomType}"    headerKey="" headerValue="" /></td>
	  	   	   
	  	   	    <td width="74px" class="listwhitetext" align="right" >Article&nbsp;<font size="2" color="red">*</font></td>	  		
	  			<td  width="150px"><s:textfield cssClass="input-text" id="autocomplete"  name="inventoryData.atricle"  size="30" maxlength="58" />
	  			<span id="indicator1" style="display: none"><img src="/images/spinner.gif" alt="Working..." /></span>
                <div id="autocomplete_choices" class="autocomplete" ></div> 
	  			</td>	                        	   	   
	  	   	    <td  width="2px"></td>
	  	   	    <td  class="listwhitetext" align="right"  >Type</td>
	  			  					
	  			<td align="left" class="listwhitetext" ><s:select name="inventoryData.type"  cssStyle="width:50px;" cssClass="list-menu" list="{'A','M'}" /></td>	  	   	      			  				 	  			  				  		
				
				<td  class="listwhitetext" align="right" width="60">Location</td>
	  	   	    <td align="left" class="listwhitetext" id="othertextfield"><s:textfield cssClass="input-text"  name="inventoryData.location" size="29" maxlength="25" onchange="putNTBSValue();" /></td>
	  	   	    
				
				</tr>			
	  		</table>
	  		
	  		 <table cellspacing="0" cellpadding="2" border="0">
	  		<tr>
	  			<td height="5px" colspan="8"></td>
	  		</tr>
	  		<tr>	  		    	  	   	    
	  	   	    <td  class="listwhitetext" align="right"  >Quantity&nbsp;<font size="2" color="red">*</font></td>
	  	   		
	  			<td ><s:textfield cssStyle="width:91px; " cssClass="input-textRight"  name="inventoryData.quantity" onchange="onlyNumeric(this);calculationTotalCft();" size="20" maxlength="2"   /></td>	
	  			<td  class="listwhitetext" align="right"  >Volume&nbsp;<font size="2" color="red">*</font></td>
	  			  					
	  			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" name="inventoryData.cft"  onchange="onlyFloat(this);calculationTotalCft();" size="10" cssStyle="width:91px;" maxlength="5" /></td>
	 		    
	 		    <td  class="listwhitetext" align="right"  >Total</td>
	  			  					
	  			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight input-textUpper" name="inventoryData.total" size="10" maxlength="4" cssStyle="width: 170px;"  readonly="true" /></td>
	 		</tr>
	 		<tr>
	  			<td height="2px" colspan="4"></td>
	  		</tr>
	  		<tr>	  		    	  	   	    
	  	   	    <td  class="listwhitetext" align="right"  >Width</td>
	  	   		
	  			<td ><s:textfield cssStyle="width:91px; " cssClass="input-textRight"  name="inventoryData.width" onchange="onlyNumeric(this);" size="20" maxlength="4"   /></td>	
	  			<td  class="listwhitetext" align="right"  >Height</td>
	  			  					
	  			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" name="inventoryData.height"  size="10"cssStyle="width:91px;"  onchange="onlyNumeric(this);" maxlength="4" /></td>
	 		    
	 		    <td  class="listwhitetext" align="right"  >Length</td>
	  			  					
	  			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" name="inventoryData.length" size="10" cssStyle="width: 170px;" onchange="onlyNumeric(this);" maxlength="4"  /></td>
	 		</tr>	 		
	 		<tr>
	  			<td height="2px" colspan="4"></td>
	  		</tr>
	 		<tr>
	 		
	 			<td  class="listwhitetext" align="right"  >Weight</td>
	  	   		
	  			<td ><s:textfield  cssClass="input-textRight"  name="inventoryData.weight"  size="9" cssStyle="width:91px; "  maxlength="6" onchange="onlyFloat(this);calculationTotalCft();" /></td>
	 		  		 	  			  					  					
	  		    <td  class="listwhitetext"  width="80px" align="right" >Total Weight</td>
	  	   		
	  			<td align="left" class="listwhitetext" ><s:textfield  cssClass="input-textRight input-textUpper" name="inventoryData.totalWeight"  size="10" cssStyle="width:91px;" maxlength="4" readonly="true" /></td>	
	  		     
	  		     <td  class="listwhitetext" align="right"   width="75px">Condition</td>	  			  					
	  			 <td align="left" class="listwhitetext" colspan="3" ><s:textfield cssClass="input-text" name="inventoryData.articleCondition" size="30" maxlength="45"  /></td>
	  		     <c:if test="${inventoryData.id!=null}">
	  		    <td  class="listwhitetext" align="right" width="55px">Images</td>	  		  
	  		    <td  width="2px"><img id="userImage" src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="24" height="24" onclick="linkToImages()"/></td>
	  		    </c:if>
	  		</tr>
	  		 <tr>
	  			<td height="2px" colspan="4"></td>
	  		</tr>
	  		<tr>
	  			
	  			<td  class="listwhitetext" align="right" >Comment</td>		
	  			<td colspan="3"><s:textarea cssStyle="width:274px;"  rows="2" cssClass="textarea"  name="inventoryData.comment" onkeydown="limitText(this,this.form.countdown,1950);" 
                     onkeyup="limitText(this,this.form.countdown,1950);" onchange="return wordCount(this);" /></td>
	  		     
	  		     <td  class="listwhitetext" align="right"  >Valuation</td>
	  	   		<td ><s:textfield  cssClass="input-textRight"  name="inventoryData.valuation"  size="9" maxlength="6" cssStyle="width: 170px;" onchange="onlyNumeric(this);" /></td>
	 		  	
	  		     <td  class="listwhitetext" align="right"  >NTBS</td>	  	   		
	  			<td ><s:checkbox name="inventoryData.ntbsFlag" value="${inventoryData.ntbsFlag}" fieldValue="true" /></td>
	  		</tr>
	  		 <tr>
	  			<td height="5px" colspan="4"></td>
	  		</tr>	  		
	  		<tr>	  		    	  	   	    
	  	   	    <td  class="listwhitetext" align="right"  >Dismantling</td>
	  	   		
	  			<td ><s:checkbox name="inventoryData.dismantling" value="${inventoryData.dismantling}" fieldValue="true" /></td>
	  			<td  class="listwhitetext" align="right"  >Assembling</td>	  			  					
	  			<td ><s:checkbox name="inventoryData.assembling" value="${inventoryData.assembling}" fieldValue="true" /></td>	 		    
	 		    <td  class="listwhitetext" align="right"  >Container</td>	  			  					
	  			<td ><s:checkbox name="inventoryData.specialContainer" value="${inventoryData.specialContainer}" fieldValue="true" /></td>
	 		 <td  class="listwhitetext" align="right"  >isValuable</td>	  			  					
	  			<td ><s:checkbox name="inventoryData.isValuable" value="${inventoryData.isValuable}" fieldValue="true" /></td>
	 		</tr>
	 		<tr>
	  			<td height="2px" colspan="4"></td>
	  		</tr>
     </table> 
     <div style="clear:both;"></div>
	        </div>    
	        <div class="bottom-header"><span></span></div>
</div>	
</div>
<table width="750px">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${inventoryData.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="inventoryData.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td style="width:120px"><fmt:formatDate value="${inventoryData.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty inventoryData.id}">
								<s:hidden name="inventoryData.createdBy"/>
								<td style="width:120px"><s:label name="createdBy" value="%{inventoryData.createdBy}"/></td>
							</c:if>
							<c:if test="${empty inventoryData.id}">
								<s:hidden name="inventoryData.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${inventoryData.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="inventoryData.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td style="width:120px"><fmt:formatDate value="${inventoryData.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty inventoryData.id}">
							<s:hidden name="inventoryData.updatedBy"/>
							<td style="width:110px"><s:label name="updatedBy" value="%{inventoryData.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty inventoryData.id}">
							<s:hidden name="inventoryData.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
			
			<s:submit  cssClass="cssbutton" cssStyle="width:55px; height:25px;margin-left:35px;" method="save" key="button.save"    /> 
           <c:if test="${inventoryData.id!=null}">
           <input type="button"  class="cssbutton" style="width:55px; height:25px;margin-left:10px;"  value="Add" onclick="add();"  />
           </c:if>
           <c:if test="${inventoryData.id==null}">
           <input type="button"  class="cssbutton" style="width:55px; height:25px;margin-left:10px;"  value="Add" disabled="disabled"  />
           </c:if>
</div>
</s:form>
<script type="text/javascript">
<%-- $(document).ready( function(){
	$("select,input,textfield,textarea,button").each(function() {
    $(this).addClass('myclass');
    
});

$(".myclass").each(function(i=0,ele) {
    $(this).attr('tabindex', i+1) ;
    });
    
})   --%>
</script>
<script type="text/javascript">
try{
putNTBSValue();
showHideOther(document.forms['cubeSheetForm'].elements['inventoryData.mode']);
<c:if test="${hitflag=='1'}">
	<c:if test="${SOflag=='CF'}">
		<c:if test="${Quote!='y'}">
			 	<c:redirect url="/surveyDetails.html?cid=${cid}"/>
		</c:if>
		<c:if test="${Quote=='y'}">
			<c:redirect url="/surveyDetails.html?cid=${cid}&Quote=y"/>
		</c:if>
	</c:if>
	<c:if test="${SOflag=='SO'}">
		<c:if test="${Quote!='y'}">
			 <c:redirect url="/inventoryDataList.html?cid=${cid}&id=${sid}&Quote="/>
		</c:if>
		<c:if test="${Quote=='y'}">
			<c:redirect url="/inventoryDataList.html?cid=${cid}&id=${sid}&Quote=y"/>
		</c:if>
	</c:if>
</c:if>
}

catch(e){}



</script>	
<script type="text/javascript">

try{  	
			<c:if test="${ usertype== 'AGENT' || ( not empty inventoryData.id && not empty inventoryData.networkSynchedId && inventoryData.id != inventoryData.networkSynchedId ) }">
			var elementsLen=document.forms['cubeSheetForm'].elements.length;
				for(i=0;i<=elementsLen-1;i++){
				if(document.forms['cubeSheetForm'].elements[i].type=='text'){
							document.forms['cubeSheetForm'].elements[i].readOnly =true;
							document.forms['cubeSheetForm'].elements[i].className = 'input-textUpper';
					}else{
							document.forms['cubeSheetForm'].elements[i].disabled=true;
					}
				}		
			</c:if>
		
			document.getElementById('inventoryMode').focus();	
	}catch(e){}
	
</script>

	