<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<head>
	<title>Survey Schedule</title>
	<meta name="heading" content="Survey Schedule"/>
    <meta name="menu" content="UserIDHelp"/>
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
<style>
 input[type="button"] { 
  cursor: pointer; 
  } 
#close a{
background:transparent url(images/close.gif) repeat scroll 0 0;
border:0 none;
display:block;
float:right;
font-size:0;
height:15px;
line-height:0;
position:relative;
text-decoration:none;
width:15px;
z-index:902;
}

#content {
color:#003366;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
z-index:901;
}

.note-para{
    margin-top: 4px;
    margin-bottom: 4px;
    font-size: 11px;
    margin-left: 4px;
}
#priceinfotab_bg{
min-width:auto !important;
width:auto !important;
}



#content-containertable.override{
width:212px;
}

#content-containertable.override div{
/* height:460px;
overflow:scroll; */

overflow-y: auto;
    height: 874px;

}

#content-containertable.override1{
width:190px;
}

#content-containertable.override1 div{
height:150px;
overflow:scroll;
}

.listwhitetext-head {
color:#FFFFFF;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
}

#mainPopup {
padding-left:5px;
padding-right:5px;
padding-top:5px;
margin-top:-20px;
}

.curved-left {
/* border:1px dotted #219DD1;
-moz-border-radius:8px;
-webkit-border-radius:8px;
behavior:url(border-radius.htc);*/
behavior:url(border-radius.htc);
background:url(images/survey-br-left.png);
height:75px;
width:514px;
}
.curved-right {
/* border:1px dotted #219DD1;
-moz-border-radius:8px;
-webkit-border-radius:8px;
behavior:url(border-radius.htc);*/
behavior:url(border-radius.htc);
background:url(images/survey-br-right.png);
height:75px;
width:341px;
}

.inner-tab{background:#ffffff url(images/basic-inner.png) repeat-x; height:35px;}
.inner-tab a, a:link a:active {
color:#FFFFFF;text-decoration:none;font-weight:bold;font-family:arial,verdana; font-size:11px;
}
.price_tleft{float:left;padding:3px 2px 0px 8px;!margin-top:0px;}

.filter-head{color: #484848;font:11px arial,verdana ; font-weight:bold; cursor: pointer; }
.price-bg{background-image:url(images/ng-bg.png); background-repeat:repeat-x;margin:0px;padding:0px;}
.filter-bg{background-image:url(images/border-filter.png); background-repeat:no-repeat; text-align:left;margin:3px; height:77px; width:852px;}
</style>

</head>
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>"djConfig="parseOnLoad:true, isDebug:false"></script>



<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>

<script language="javascript" type="text/javascript">
function completeTimeString() {
	
		stime1 = document.forms['surveyScheduleForm'].elements['surveyTime'].value;
		stime2 = document.forms['surveyScheduleForm'].elements['surveyTime2'].value;;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['surveyScheduleForm'].elements['surveyTime'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['surveyScheduleForm'].elements['surveyTime'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['surveyScheduleForm'].elements['surveyTime'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['surveyScheduleForm'].elements['surveyTime'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['surveyScheduleForm'].elements['surveyTime'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['surveyScheduleForm'].elements['surveyTime'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['surveyScheduleForm'].elements['surveyTime2'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['surveyScheduleForm'].elements['surveyTime2'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['surveyScheduleForm'].elements['surveyTime2'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['surveyScheduleForm'].elements['surveyTime2'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['surveyScheduleForm'].elements['surveyTime2'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['surveyScheduleForm'].elements['surveyTime2'].value = "0" + stime2;
			}
		}
		//IsValidTime();
	
		var testH=document.forms['surveyScheduleForm'].elements['surveyTime'].value;
		testH= testH.substring(0,2);
		var test1H=document.forms['surveyScheduleForm'].elements['surveyTime2'].value;
		test1H= test1H.substring(0,2);
		var testM=document.forms['surveyScheduleForm'].elements['surveyTime'].value;
		testM= testM.substring(3,5);
		var test1M=document.forms['surveyScheduleForm'].elements['surveyTime2'].value;
		test1M= test1M.substring(3,5);
		if(testH > 24){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['surveyTime'].value='';
		return false;
		}
		if(testM > 60){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['surveyTime'].value='';
		return false;
		}
		if(test1H > 24){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['surveyTime2'].value='';
		return false;
		}
		if(test1M > 60){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['surveyTime2'].value='';
		return false;
		}
		var testCheck1 = document.forms['surveyScheduleForm'].elements['surveyTime'].value;
		testCheck1 = testCheck1.substring(0,2);
		var testCheck2 = document.forms['surveyScheduleForm'].elements['surveyTime2'].value;
		testCheck2 = testCheck2.substring(0,2);
		if(testCheck1 > testCheck2 && testCheck2!=00 ){
		alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['surveyTime2'].value='';
		return false;
		}
		
	}

function consultantTimeString() {
	
		stime1 = document.forms['surveyScheduleForm'].elements['consultantTime'].value;
		stime2 = document.forms['surveyScheduleForm'].elements['consultantTime2'].value;;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['surveyScheduleForm'].elements['consultantTime'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['surveyScheduleForm'].elements['consultantTime'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['surveyScheduleForm'].elements['consultantTime'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['surveyScheduleForm'].elements['consultantTime'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['surveyScheduleForm'].elements['consultantTime'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['surveyScheduleForm'].elements['consultantTime'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['surveyScheduleForm'].elements['consultantTime2'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['surveyScheduleForm'].elements['consultantTime2'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['surveyScheduleForm'].elements['consultantTime2'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['surveyScheduleForm'].elements['consultantTime2'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['surveyScheduleForm'].elements['consultantTime2'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['surveyScheduleForm'].elements['consultantTime2'].value = "0" + stime2;
			}
		}
		//IsValidTime();
		var testH=document.forms['surveyScheduleForm'].elements['consultantTime'].value;
		testH= testH.substring(0,2);
		var test1H=document.forms['surveyScheduleForm'].elements['consultantTime2'].value;
		test1H= test1H.substring(0,2);
		var testM=document.forms['surveyScheduleForm'].elements['consultantTime'].value;
		testM= testM.substring(3,5);
		var test1M=document.forms['surveyScheduleForm'].elements['consultantTime2'].value;
		test1M= test1M.substring(3,5);
		if(testH > 24){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['consultantTime'].value='';
		return false;
		}
		if(testM > 60){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['consultantTime'].value='';
		return false;
		}
		if(test1H > 24){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['consultantTime2'].value='';
		return false;
		}
		if(test1M > 60){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['consultantTime2'].value='';
		return false;
		}
		var testCheck1 = document.forms['surveyScheduleForm'].elements['consultantTime'].value;
		testCheck1 = testCheck1.substring(0,2);
		var testCheck2 = document.forms['surveyScheduleForm'].elements['consultantTime2'].value;
		testCheck2 = testCheck2.substring(0,2);
		if(testCheck1 > testCheck2 && testCheck2!=00 ){
		alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['consultantTime2'].value='';
		return false;
		}
	}


	
function IsValidTime() {

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['surveyScheduleForm'].elements['surveyTime'].value;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format. Please use HH:MM format");
document.forms['surveyScheduleForm'].elements['surveyTime'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("'Pre Move Survey' time must between 0 to 23 (Hrs)");
document.forms['surveyScheduleForm'].elements['surveyTime'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
document.forms['surveyScheduleForm'].elements['surveyTime'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
document.forms['surveyScheduleForm'].elements['surveyTime'].focus();
return false;
}

// **************Check for Survey Time2*************************

var time2Str = document.forms['surveyScheduleForm'].elements['surveyTime2'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. please Use HH:MM format");
document.forms['surveyScheduleForm'].elements['surveyTime2'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];

if (hourTime2 < 0  || hourTime2 > 23) {
alert("'Pre Move Survey' time must between 0 to 23 (Hrs)");
document.forms['surveyScheduleForm'].elements['surveyTime2'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
document.forms['surveyScheduleForm'].elements['surveyTime2'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
document.forms['surveyScheduleForm'].elements['surveyTime2'].focus();
return false;
}
}

function openTargetOriginAddressLocation() {
	map = new google.maps.Map(document.getElementById("map"));
	geocoder = new google.maps.Geocoder();
	getSurveySchedulePoints();
}
</script>
<script language="javascript" type="text/javascript">
var geocoder;
var map;
var letter1 = "";
var letter = "";
var letter3 = "";
var GOOGLE_MAP_KEY = googlekey;
window.onload = function() { 
	  var script = document.createElement('script');
	  script.type = 'text/javascript';
	  script.src = 'https://maps.googleapis.com/maps/api/js?v=3&callback=openTargetOriginAddressLocation&key='+GOOGLE_MAP_KEY; 
	  document.body.appendChild(script);
}
   var surveyData;
   var toDisplay =new Array();
   var surveyDataMap = new Object();
   var letterMap = new Object();
   var barToProcess = new Object();
   
   function showhide(id){ 
		if (document.getElementById){ 
			obj = document.getElementById(id); 
			if (obj.style.display == "none"){ 
				obj.style.display = ""; 
			} else { 
				obj.style.display = "none"; 
			} 
		} 
	} 
   
   
   var addrArray =new Array();
  	function getSurveySchedulePoints(){
		dojo.xhrPost({
	       form: "surveyScheduleForm",
	       timeout: 900000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	        		//var jsonData = dojo.toJson(data)
	        var d=document.getElementById("surveyListScheduleList");
	        d.innerHTML='';	  
  			var i=0;
  			var oldEstimator;
  			var j = 0;
  			
  			if(jsonData.count>0)
  			{
	           for(i=0; i < jsonData.surveyListSchedulePoints.length; i++){
	             	
	             	letter1 = jsonData.surveyListSchedulePoints[i].activityName;
	             	var newEstimator= jsonData.surveyListSchedulePoints[i].estimator
	             	if( letter1== 'CF')
	             	{
	             	   letter = String.fromCharCode("A".charCodeAt(0) + j);
	             	   j++;
	           		}
          		
	           		document.forms['surveyScheduleForm'].elements['fromDate'].value=jsonData.surveyListSchedulePoints[i].startDay;
	           		document.forms['surveyScheduleForm'].elements['toDate'].value = jsonData.surveyListSchedulePoints[i].add7Day;
	           		document.forms['surveyScheduleForm'].elements['surveyTime'].value=jsonData.surveyListSchedulePoints[i].fromTime;
	           		document.forms['surveyScheduleForm'].elements['surveyTime2'].value = jsonData.surveyListSchedulePoints[i].toTime;
	           		
	           		 if(newEstimator!=oldEstimator){
	           		 if( letter1== 'CF')
	           		 {
	           		 d.innerHTML+='<table width="198" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;"></td><td><a style="text-decoration: underline; cursor: pointer;" onclick=moveDateBackByOne("refreshAppointments","'+jsonData.surveyListSchedulePoints[i].estimator+'")>'+jsonData.surveyListSchedulePoints[i].estimator+'</a></td></tr></table>'+
	           		 '</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext" width="200px" >'+jsonData.surveyListSchedulePoints[i].survey+' - '+
					''+jsonData.surveyListSchedulePoints[i].surveyTime1+''+
					'-'+jsonData.surveyListSchedulePoints[i].surveyTime2+''  
					//+'  <a style="text-decoration: underline; cursor: pointer; color:red; font-weight:bold" onclick=markerClickFnCunsltant("'+jsonData.surveyListSchedulePoints[i].cfnumber+'","'+jsonData.surveyListSchedulePoints[i].customerName+'","'+encodeURI(jsonData.surveyListSchedulePoints[i].address)+'")>('+letter+')</td></table>';
					+'  <a style="text-decoration: underline; cursor: pointer; color:red; font-weight:bold" onclick=javascript:showhide("'+jsonData.surveyListSchedulePoints[i].cfnumber+'")>('+letter+')</td></table>';
	           		 d.innerHTML+='<table class="listwhitetext" style="margin:0px; paddding:0px; border:1px dotted #8FCDFF;-moz-border-radius:8px; background-color:#efefef; " id="'+jsonData.surveyListSchedulePoints[i].cfnumber+'"><tr><td><b>C/F#:</b> </td><td>'+jsonData.surveyListSchedulePoints[i].cfnumber+' </td><td><a style="text-decoration: underline; cursor: pointer; color:red; font-weight:bold" onclick=javascript:showhide("'+jsonData.surveyListSchedulePoints[i].cfnumber+'")><img src="${pageContext.request.contextPath}/images/cancel.png"/></td></tr><tr><td><b>Name:</b> </td><td>'+jsonData.surveyListSchedulePoints[i].customerName+'</td></tr><tr><td valign="top"><b>Address:</b> </td><td valign="top" colspan="2">'+jsonData.surveyListSchedulePoints[i].address+'</td></tr></table>';
	           		 showhide(jsonData.surveyListSchedulePoints[i].cfnumber);
	           		 }
	           		 else
	           		 {
	           		 	d.innerHTML+='<table width="198" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;"></td><td><a style="text-decoration: underline; cursor: pointer;"onclick=moveDateBackByOne("refreshAppointments","'+jsonData.surveyListSchedulePoints[i].estimator+'")>'+jsonData.surveyListSchedulePoints[i].estimator+'</a></td></tr></table>'+
	           		 '</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext" width="200px" >'+jsonData.surveyListSchedulePoints[i].survey+' - '+
					''+jsonData.surveyListSchedulePoints[i].surveyTime1+''+
					'-'+jsonData.surveyListSchedulePoints[i].surveyTime2+''  
					+' '+jsonData.surveyListSchedulePoints[i].activityName+' </td></table>';
	           		 }
	           		 
	           		 }else{
	           		 if( letter1== 'CF')
	           		 {
	           		 d.innerHTML+='<table width="198" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"></tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext" width="200px" >'+jsonData.surveyListSchedulePoints[i].survey+' - '+
					''+jsonData.surveyListSchedulePoints[i].surveyTime1+''+
					'-'+jsonData.surveyListSchedulePoints[i].surveyTime2+'' 
					+'  <a style="text-decoration: underline; cursor: pointer; color:red; font-weight:bold" onclick=javascript:showhide("'+jsonData.surveyListSchedulePoints[i].cfnumber+'")>('+letter+')</td></table>';
	           		 d.innerHTML+='<table class="listwhitetext" style="margin:0px; paddding:0px; border:1px dotted #8FCDFF;-moz-border-radius:8px; background-color:#efefef;" id="'+jsonData.surveyListSchedulePoints[i].cfnumber+'"><tr><td><b>C/F#:</b> </td><td>'+jsonData.surveyListSchedulePoints[i].cfnumber+' </td><td><a style="text-decoration: underline; cursor: pointer; color:red; font-weight:bold" onclick=javascript:showhide("'+jsonData.surveyListSchedulePoints[i].cfnumber+'")><img src="${pageContext.request.contextPath}/images/cancel.png"/></td></tr><tr><td><b>Name:</b> </td><td>'+jsonData.surveyListSchedulePoints[i].customerName+'</td></tr><tr><td valign="top"><b>Address:</b> </td><td valign="top" colspan="2">'+jsonData.surveyListSchedulePoints[i].address+'</td></tr></table>';
	           		 showhide(jsonData.surveyListSchedulePoints[i].cfnumber);
	           		 }
	           		 else
	           		 {
	           		 d.innerHTML+='<table width="198" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"></tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext" width="200px" >'+jsonData.surveyListSchedulePoints[i].survey+' - '+
					''+jsonData.surveyListSchedulePoints[i].surveyTime1+''+
					'-'+jsonData.surveyListSchedulePoints[i].surveyTime2+'' 
					+' '+jsonData.surveyListSchedulePoints[i].activityName+' </td></table>';
	           		 }
	           		 
	           		 }    		
	              						
	                oldEstimator=jsonData.surveyListSchedulePoints[i].estimator;
	              
	               console.log("address: " + jsonData.surveyListSchedulePoints[i].address + ", estimator: " +  jsonData.surveyListSchedulePoints[i].estimator);
	        	   surveyData=jsonData.surveyListSchedulePoints[i];
	        	   toDisplay[i] = surveyData;	  
	        	   
	        	   surveyDataMap[jsonData.surveyListSchedulePoints[i].address] = jsonData.surveyListSchedulePoints[i];
	        	   letterMap[jsonData.surveyListSchedulePoints[i].address] = letter;
	        	   barToProcess[jsonData.surveyListSchedulePoints[i].address] = letter3;
	        	   var timeout = i * 225; 
	        	   addrArray[i] = jsonData.surveyListSchedulePoints[i].address;
	        	   console.log(addrArray[i])
	   			window.setTimeout(function() { geoCodeLookup(); }, timeout);    
	         	} 
	        	 
	        }
  			
	        if(jsonData.locCount>0){
	        for(i=0; i < jsonData.surveyListSchedulePoints.length; i++){
	        document.forms['surveyScheduleForm'].elements['fromDate'].value=jsonData.surveyListSchedulePoints[i].startDay;
	        document.forms['surveyScheduleForm'].elements['toDate'].value = jsonData.surveyListSchedulePoints[i].add7Day;
	        surveyDataMap[jsonData.surveyListSchedulePoints[i].consultantHomeAdd] = jsonData.surveyListSchedulePoints[i];
	        console.log("address: " + jsonData.surveyListSchedulePoints[i].consultantHomeAdd);
	        geocoder.geocode({'address': jsonData.surveySchedulePoints[i].consultantHomeAdd}, function(results, status) {
	          if ( status == google.maps.GeocoderStatus.OK ) 
	            	addHomeAddToMap(results[0].geometry.location, jsonData.surveySchedulePoints[i].consultantHomeAdd);
	               
	            });
	       
	        }
	        }       	  
	        	}       	       
	   });
	
	}  
  	var addrCtr = 0;
	var address;
  	function 	geoCodeLookup() {
  		
		var addr = addrArray[addrCtr];
		addrCtr++;
	    geocoder =  new google.maps.Geocoder();
		geocoder.geocode({'address': addr}, function(results, status) {
            var addr_type = results[0].types[0];
            
            // type of address inputted that was geocoded
            if ( status == google.maps.GeocoderStatus.OK ) 
            	addZipCoordsToMap(results[0].geometry.location, addr);
               
        }); 
	}
	function addHomeAddToMap(response,address){
		point = response;
        var fn

		   var baseIcon = "http://www.google.com/mapfiles/shadow50.png";
           var shadow = new google.maps.MarkerImage(
      		  baseIcon,
      	      // The shadow image is larger in the horizontal dimension
      	      // while the position and offset are the same as for the main image.
      	      new google.maps.Size(37, 32),
      	      new google.maps.Point(0,0),
      	      new google.maps.Point(0, 32));
			  var bounds = new google.maps.LatLngBounds();
		  marker =  new google.maps.Marker({
		  position: point,
		  shadow: shadow,
	     
        icon: letteredIcon,
        map: map
      });
		  
		  bounds.extend(marker.position);
		  map.fitBounds(bounds);
		  map.setZoom(3);
		  fn = markerClickFnAddLoc(point,surveyDataMap[address],marker);
	      google.maps.event.addListener(marker, "click", fn);
				//showOrHide(0);
	  } 
	
	var add;
 	function addZipCoordsToMap(response,address){
 	
		  point = response;
          var fn;
          var baseIcon = "http://www.google.com/mapfiles/shadow50.png";
          var shadow = new google.maps.MarkerImage(
        		  baseIcon,
        	      // The shadow image is larger in the horizontal dimension
        	      // while the position and offset are the same as for the main image.
        	      new google.maps.Size(37, 32),
        	      new google.maps.Point(0,0),
        	      new google.maps.Point(0, 32));
          
		 var letteredIcon ="http://chart.apis.google.com/chart?chst=d_map_spin&chld=1.|0|FF0000|40|b|"+letterMap[address];
		 var image = new google.maps.MarkerImage(
					  letteredIcon,
				      // This marker is 20 pixels wide by 32 pixels tall.
				      new google.maps.Size(5, 5),
				      // The origin for this image is 0,0.
				      new google.maps.Point(0,0),
				      // The anchor for this image is the base of the flagpole at 0,32.
				      new google.maps.Point(0, 32));

          // This marker is 20 pixels wide by 32 pixels high.
         // Set up our GMarkerOptions object
		  var bounds = new google.maps.LatLngBounds();
		  marker =  new google.maps.Marker({
		  position: point,
		  shadow: shadow,
	     
          icon: letteredIcon,
          map: map
        });
		  
		  bounds.extend(marker.position);
		  map.fitBounds(bounds);
		  map.setZoom(3);
	     fn = markerClickFnPorts(point,surveyDataMap[address],marker);
		
		  google.maps.event.addListener(marker, "click", fn);
				//showOrHide(0);
				//showOrHide(0);
 }  
	
	function markerClickFnPorts(point, surveyResults,marker) {
		
		   return function() {
			   if (!surveyResults) return;
		       var customerName = surveyResults.customerName;
		       var originCity = surveyResults.originCity;
		       var originState = surveyResults.originState;
		       var survey = surveyResults.survey;
		       var surveyTime1 = surveyResults.surveyTime1;
		       var surveyTime2 = surveyResults.surveyTime2;
		       var estimator = surveyResults.estimator;
		       var survey1 = surveyResults.survey1;
		       var cfnumber = surveyResults.cfnumber;
		       var billtocode="";
		       try{
		       billtocode = surveyResults.billtocode;
		       }catch(e){billtocode="";}
		       var bounds = new google.maps.LatLngBounds();
		       bounds.extend(marker.position);
			   map.fitBounds(bounds);
			   map.setZoom(3);
				  
		       var infoHtml = '<div style="width:100%;"><h5><B>C/F #: </B>'+cfnumber+' ('+billtocode+')<BR><B>Customer: </B>'+customerName+'<Br><B>Survey: </B>'+survey+' - '+surveyTime1+' - '+surveyTime2+'<Br><B>City: </B>'+originCity+', '+originState+'<Br><B>Consultant: </B>'+estimator+'</h5>'+
		       '<td></td>';
		            
		       infoHtml += '</div></div>';
		       var infowindow = new google.maps.InfoWindow({
		           content: infoHtml
		         });
		       infowindow.open(map, marker);
		  }
	   }  


   
   
// for populating estimator

function putSelectedEstimator(estimator,survey)   
   {
        document.forms['surveyScheduleForm'].elements['consultantDate'].value=survey;
   		document.forms['surveyScheduleForm'].elements['estimatorList'].value=estimator;
   		document.forms['surveyScheduleForm'].elements['consultantDate'].focus();
   
   }
// for target

</script>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('address', 'fade=1,persist=0,hide=0')
animatedcollapse.addDiv('info', 'fade=1,persist=0,hide=1')
animatedcollapse.addDiv('pricing', 'fade=1,persist=0,hide=1')
animatedcollapse.init()
</script>
<script language="JavaScript" type="text/JavaScript">
var namesVec = new Array("130.png", "129.png");
var root='images/';
function swapImg(ima){
// divides the path
nr = ima.getAttribute('src').split('/');
// gets the last part of path, ie name
nr = nr[nr.length-1]
// former was .split('.')[0];
 
if(nr==namesVec[0]){ima.setAttribute('src',root+namesVec[1]);}
else{ima.setAttribute('src',root+namesVec[0]);}
 
}

function setCustomerFile()
{
		var surveyCustomerDate=document.forms['surveyScheduleForm'].elements['consultantDate'].value;
   		var estimatorCustomer=document.forms['surveyScheduleForm'].elements['estimatorList'].value;
   		var consultantTime=document.forms['surveyScheduleForm'].elements['consultantTime'].value;
   		var consultantTime2=document.forms['surveyScheduleForm'].elements['consultantTime2'].value;
   		
        if(surveyCustomerDate !='' && estimatorCustomer !='' & consultantTime !='' & consultantTime !='')
        {   		
   		var url="customerSurveyShedule.html?ajax=1&decorator=simple&popup=true&surveyCustomerDate="+ encodeURI(surveyCustomerDate)+"&estimatorCustomer="+estimatorCustomer+"&consultantTime="+consultantTime+"&consultantTime2="+consultantTime2;
   		http2.open("GET", url, true);
        http2.onreadystatechange = handleHttpResponse1;
        http2.send(null);
        }
        else{
        alert("Please select all the fields to Confirm Appt.");
        }
}
function handleHttpResponse1()
     {
     	var surveyCustomerDate=document.forms['surveyScheduleForm'].elements['consultantDate'].value;
   		var estimatorCustomer=document.forms['surveyScheduleForm'].elements['estimatorList'].value;
   		var consultantTime=document.forms['surveyScheduleForm'].elements['consultantTime'].value;
   		var consultantTime2=document.forms['surveyScheduleForm'].elements['consultantTime2'].value;
          if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>2)
                {
                	var confirm1 = confirm(estimatorCustomer+" is already scheduled for the same date and time.\nDo you want to confirm this appointment?");
                	if(confirm1)
                	{
	                	window.opener.document.forms['customerFileForm'].elements['customerFile.survey'].value = surveyCustomerDate;
	                	window.opener.document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = consultantTime;
	                	window.opener.document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = consultantTime2;
	                	window.opener.document.forms['customerFileForm'].elements['customerFile.estimator'].value = estimatorCustomer;
	                	window.close();
                	}
                else{
                document.forms['surveyScheduleForm'].elements['consultantTime'].value='00:00';
   				document.forms['surveyScheduleForm'].elements['consultantTime2'].value='00:00';
        			}
                } 
                else
                {
                	    window.opener.document.forms['customerFileForm'].elements['customerFile.survey'].value = surveyCustomerDate;
	                	window.opener.document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = consultantTime;
	                	window.opener.document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = consultantTime2;
	                	window.opener.document.forms['customerFileForm'].elements['customerFile.estimator'].value = estimatorCustomer;
	                	window.close();
                }
    }
    
    }
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}    
function moveDateBackByOne(target,consultant){ 
	 var fromDate = document.forms['surveyScheduleForm'].elements['fromDate'].value; 
	 var toDate = document.forms['surveyScheduleForm'].elements['toDate'].value;	
	 var jobTypeSchedule=document.forms['surveyScheduleForm'].elements['jobTypeSchedule'].value;
	 var lessThanOneDay=document.forms['surveyScheduleForm'].elements['lessThanOneDay'].value;
	 var targetAddress=document.forms['surveyScheduleForm'].elements['targetAddress'].value;
	 var surveyTime=document.forms['surveyScheduleForm'].elements['surveyTime'].value;
	 var surveyTime2=document.forms['surveyScheduleForm'].elements['surveyTime2'].value;
	 var surveyCity = document.forms['surveyScheduleForm'].elements['surveyCity'].value;
	 var lessThanOneDay;
	 var consultant2=document.forms['surveyScheduleForm'].elements['oldEstimated'].value;
	 var oldEstimated=document.forms['surveyScheduleForm'].elements['oldEstimated'].value;	
	if(target=='lessThanOneDay')
	{
	lessThanOneDay=target;
	}
	else if(target=='greaterThanOneDay'){
	lessThanOneDay=target;
	}
	else if(target=='refreshAppointments'){
	lessThanOneDay=target;
	}
	else if(target=='homeDetails'){
	lessThanOneDay=target;
	}
	if(consultant == 'nan')
	{
		consultant = consultant2;
	}
	location.href = "surveyListSchedule.html?jobTypeSchedule="+jobTypeSchedule+"&lessThanOneDay="+lessThanOneDay+"&fromDate="+fromDate+"&toDate="+toDate+"&targetAddress="+targetAddress+"&surveyTime="+surveyTime+"&surveyTime2="+surveyTime2+"&estimated="+consultant+"&surveyCity="+surveyCity+"&oldEstimated="+oldEstimated+"&decorator=popup&popup=true","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes";
} 

	
function copyFromDate(){
 var date1 = document.forms['surveyScheduleForm'].elements['fromDate'].value; 
 var date2 = document.forms['surveyScheduleForm'].elements['toDate'].value;
 if(date1 !='')
 {
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   
   var finalDate = year+"-"+month+"-"+day;
  document.forms['surveyScheduleForm'].elements['fromDate'].value=finalDate;
  }
  if(date2 !='')
 {
   var mySplitResult = date2.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   
   var finalDate = year+"-"+month+"-"+day;
   document.forms['surveyScheduleForm'].elements['toDate'].value=finalDate;
  }
 }

function changeStatus(){
	document.forms['surveyScheduleForm'].elements['formStatus'].value = '1';
}
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns()
		showOrHide(1);
	</script>
	
<s:form id="surveyScheduleForm" name="surveyScheduleForm" action='surveyValuedSchedule.html' method="post" validate="true">
<s:hidden key="jobTypeSchedule"  value="<%=request.getParameter("jobTypeSchedule") %>"/>


<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="formStatus" value=""/>
<s:hidden name="lessThanOneDay"/>
<s:hidden name="estimated"/>
<c:set var="oldEstimated" value="%{oldEstimated}"/>
<s:hidden name="oldEstimated" id ="oldEstimated" value="%{oldEstimated}" />
<s:hidden name="surveyCity"/>
<s:hidden name="fromDate"/>
<s:hidden name="toDate"/>
<s:hidden name="surveyTime"/>
<s:hidden name="surveyTime2"/>
<s:hidden name="targetAddress" value="<%=request.getParameter("targetAddress") %>"/>
<div id="priceinfotab_bg" style="width: 99.8%;"><div class="price_tleft">Survey Schedule</div>
<span style="cursor:auto;"><input type="button" class="movebutton" value="Close" style="width:63px;margin-top:2px;margin-right:75px;cursor:hand;" onclick="window.close();"/></span>
<span style="cursor:auto;"><input type="button" class="movebutton" value="Refresh" style="width:63px;margin-top:2px;margin-right:10px;cursor:hand;" onclick="moveDateBackByOne('refreshAppointments','nan');"/></span>

</div>
<table class="mainDetailTable" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;" >
  <tbody>
  <%-- <tr>
  <td colspan="2" align="left" valign="middle" height="82" class="price-bg">

  <table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
  <tr>
  <td width="3">&nbsp;</td> 
  <td valign="top">
  <div class="curved-left">
  <table cellpadding="1" cellspacing="2" border="0" style="margin:0px;padding:0px;">
  <tr>  
   		
		
		<td colspan="9">
		<table cellpadding="2" cellspacing="1" border="0" style="margin:0px;padding:0px;">
  		<tr> 
  		<td width="1">&nbsp;
  		</td>
		<td align="right" class="listwhitetext">
		<a><img align="middle" onclick="javascript:moveDateBackByOne('lessThanOneDay','nan');" HEIGHT="25" WIDTH="112" BORDER="0"  src="images/s-backward.png"/></a>
		</td>
		<td align="right" class="listwhitetext">From:</td>					
		<td><s:textfield id="fromDates" cssClass="input-text" name="fromDate"  size="7" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this)"/></td>
		<td><img id="calender1" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="changeStatus(),cal.select(document.forms['surveyScheduleForm'].fromDates,'calender1',document.forms['surveyScheduleForm'].dateFormat.value); return false;"/></td>
	   <td align="right" class="listwhitetext">To:</td>	
	   <td><s:textfield id="toDates" cssClass="input-text" name="toDate" size="7" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this)"/></td>
	   <td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="changeStatus(),cal.select(document.forms['surveyScheduleForm'].toDates,'calender2',document.forms['surveyScheduleForm'].dateFormat.value); return false;"/></td>
	 	<td align="right" class="listwhitetext">
	 	<a><img align="middle" onclick="javascript:moveDateBackByOne('greaterThanOneDay','nan');" HEIGHT="25" WIDTH="127" BORDER="0"  src="images/s-forward.png"/></a>
	 	</td>
	 
	 	</tr>
	 	</table>
	 	
		</td>
	
	</tr>
	
	<tr>
	
	<td colspan="9">
		<table cellpadding="2" cellspacing="1" border="0" style="margin:0px;padding:0px;">
  	<tr> 
  	<td align="right" width="115" class="listwhitetext">
	 <input type="button" class="movebutton" value="Consultant&nbsp;Home" style="width:105px;" onclick="moveDateBackByOne('homeDetails','nan');"/>
	</td>
  	<td width="2">&nbsp;</td>  	
	<td align="right" class="listwhitetext">Time: </td>
  	<td class="listwhitetext"><s:textfield key="surveyTime" cssClass="input-text" size="2" maxlength="5" onchange="completeTimeString();"/>hrs</td>
	<td width="10">&nbsp;</td>
	<td align="right" class="listwhitetext">To: </td>
  	<td class="listwhitetext"><s:textfield key="surveyTime2" cssClass="input-text" size="2" maxlength="5" onchange="completeTimeString();"/>hrs</td>
  	<td width="3">&nbsp;</td>
  	<td align="right"  class="listwhitetext">
	 <input type="button" class="movebutton" value="Refresh Appointments" style="width:163px;" onclick="moveDateBackByOne('refreshAppointments','nan');"/>
	</td>
	</tr>
	</table>
	--%><%-- 
	</div>
	</td>
	</tr>
	</table>
  </td>
  <td width="1"></td>
  <td valign="top">
  <div class="curved-right">
  
  <table cellpadding="2" cellspacing="2" border="0" style="margin:0px;padding:0px;">
  <tr>
  
  <td colspan="5">
		<table cellpadding="2" cellspacing="1" border="0" style="margin:0px;padding:0px;">
  	<tr>
  <td align="right" class="listwhitetext">Consultant</td>
  <td><s:select cssClass="list-menu" name="estimator" id="estimatorList" list="%{estimatorList}"   cssStyle="width:130px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
	<td align="right" class="listwhitetext">Date: </td>	
	<td><s:textfield id="consultantDates" cssClass="input-text" name="consultantDate"  size="7" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this)"/></td>
	<td><img id="calender3" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="changeStatus(),cal.select(document.forms['surveyScheduleForm'].consultantDates,'calender3',document.forms['surveyScheduleForm'].dateFormat.value); return false;"/></td>
 </tr>
 </table>

 </td>
 
  </tr>
  <tr>
  <td colspan="5">
		<table cellpadding="2" cellspacing="1" border="0" style="margin:0px;padding:0px;">
  	<tr>
  <td align="right" class="listwhitetext" width="50">Time: </td>
  	<td class="listwhitetext" width="70"><s:textfield key="consultantTime" cssClass="input-text" size="2" maxlength="5" onchange="consultantTimeString();"/>hrs</td>
    <td align="right" class="listwhitetext">To: </td>
  	<td class="listwhitetext"><s:textfield key="consultantTime2" cssClass="input-text" size="2" maxlength="5" onchange="consultantTimeString();"/>hrs</td>
  	<td><input type="button" class="movebutton" value="Confirm Appt" style="width:105px;" onclick="setCustomerFile();"/>
  	</td>
  	</tr>
  	</table>
  	
  	</td>
  </tr>
  </table>
  </div>
  </td>
  </tr>
  </table>
  
  
  
  
  
  
	
  </td>
  </tr>
  --%>
  
  <tr>
  				  <td valign="top" align="center" style="margin:0px;padding: 0px; background-color:#F6F9FA;">
					<table width="100%" style="margin:0px;padding:0px;" border="0" cellpadding="0" cellspacing="0" >    
						<tr>
							<td style="padding-left:1px;"></td>
						</tr>
						<tr>
					   		<td align="left" colspan="2">
					   			<div id="priceinfotab_bg">
					   				<div class="price_tleft">Consultants Availability:</div>
					   			</div>
					       
					    		<div id="content-containertable" class="override" >
					 				<div id="surveyListScheduleList"></div>
					 			</div>
					   		</td>
					   		<td valign="top" style="margin:0px;padding: 0px;width:88%;border:1px solid #969694;">
									<div id="map" style="width: 100%; height: 900px;margin:0px;padding: 0px; z-index:1">
								</td>
					 	</tr>
					 	

														 	
					 </table>
				 </td>

			 </tr>
		 
		</tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table><DIV ID="layerH" style="position:absolute;left:400px;top:80px; z-index:910; ">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait, Map is loading.......</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
			</div>
        <div id="mydiv" style="position:absolute; z-index:905;"></div>
    
</s:form>

<script>
setTimeout("showOrHide(0)",6000);
copyFromDate();
//openTargetOriginAddressLocation();
</script>