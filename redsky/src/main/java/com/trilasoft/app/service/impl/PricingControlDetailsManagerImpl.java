package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.PricingControlDetailsDao;
import com.trilasoft.app.model.PricingControlDetails;
import com.trilasoft.app.service.PricingControlDetailsManager;


public class PricingControlDetailsManagerImpl extends GenericManagerImpl<PricingControlDetails, Long> implements PricingControlDetailsManager {

	PricingControlDetailsDao PricingControlDetailsDao;

	public PricingControlDetailsManagerImpl(PricingControlDetailsDao PricingControlDetailsDao) {
		super(PricingControlDetailsDao);
		this.PricingControlDetailsDao=PricingControlDetailsDao;
	}

	public List checkById(Long id) {
		return null;
	}

	public List getSelectedAgent(Long id, String tariffApplicability, String sessionCorpID) {
		return PricingControlDetailsDao.getSelectedAgent(id, tariffApplicability, sessionCorpID);
	}

	public List getFreightList(String originPOE, String destinationPOE, String containerSize, Date expectedLoadDate, String originPortRange, String destinationPortRange, String mode, String originCountry, String destinationCountry, String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude) {
		return PricingControlDetailsDao.getFreightList(originPOE, destinationPOE, containerSize, expectedLoadDate, originPortRange, destinationPortRange, mode, originCountry, destinationCountry, originLatitude, originLongitude, destinationLatitude, destinationLongitude);
	}

	public List getSelectedRefFreight(Long id, String tariffApplicability, String sessionCorpID) {
		return PricingControlDetailsDao.getSelectedRefFreight(id, tariffApplicability, sessionCorpID);
	}

	public List getSelectedFreightFromDetails(Long id, String sessionCorpID) {
		return PricingControlDetailsDao.getSelectedFreightFromDetails(id, sessionCorpID);
	}

	public void selectMinimumQuoteAgent(Long id, String tarrifApplicability, String sessionCorpID, Boolean isFreight) {
		PricingControlDetailsDao.selectMinimumQuoteAgent(id, tarrifApplicability, sessionCorpID, isFreight);
	}

	public void selectMinimumFreight(String originPOE, String destinationPOE, String containerSize, Date expectedLoadDate, Long pricingControlID, String baseCurrency, String mode, String contract, String originPortRange, String destinationPortRange, String originCountry, String destinationCountry, String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude) {
		PricingControlDetailsDao.selectMinimumFreight(originPOE, destinationPOE, containerSize, expectedLoadDate, pricingControlID, baseCurrency, mode, contract, originPortRange,  destinationPortRange,  originCountry,  destinationCountry,  originLatitude,  originLongitude,  destinationLatitude,  destinationLongitude);
		
	}

	public String getPortsCode(String portRange, String mode, String country, String latitude, String longitude) {
		return PricingControlDetailsDao.getPortsCode(portRange, mode, country, latitude, longitude);
	}

	public String getMinimumFreightCode(String originPOE, String destinationPOE, String containerSize, Date expectedLoadDate, Long pricingControlID, String baseCurrency, String mode, String contract, String originPortRange, String destinationPortRange, String originCountry, String destinationCountry, String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude) {
		return PricingControlDetailsDao.getMinimumFreightCode(originPOE, destinationPOE, containerSize, expectedLoadDate, pricingControlID, baseCurrency, mode, contract, originPortRange, destinationPortRange, originCountry, destinationCountry, originLatitude, originLongitude, destinationLatitude, destinationLongitude); 
	}

	public String getPortsPerFreight(String originPOE, String destinationPOE, String containerSize, Date expectedLoadDate, Long id, String baseCurrency, String mode, String contract, String originPortRange, String destinationPortRange, String originCountry, String destinationCountry, String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude) {
		return PricingControlDetailsDao.getPortsPerFreight(originPOE, destinationPOE, containerSize, expectedLoadDate, id, baseCurrency, mode, contract, originPortRange, destinationPortRange, originCountry, destinationCountry, originLatitude, originLongitude, destinationLatitude, destinationLongitude); 
	}

	public List getMasterFreightList(Long pricingControlId, String sessionCorpID) {
		return PricingControlDetailsDao.getMasterFreightList(pricingControlId, sessionCorpID);
	}

	public void updateRespectiveRecords(Long originAgentRecordId, Long destinationAgentRecordId, Long freightRecordId, Long pricingControlID) {
		PricingControlDetailsDao.updateRespectiveRecords(originAgentRecordId, destinationAgentRecordId, freightRecordId, pricingControlID);
	}

	public void updateFreightDetails(Long pricingControlId, Long freightAgentId) {
		PricingControlDetailsDao.updateFreightDetails(pricingControlId, freightAgentId);
		
	}

	public void updatePricingControlDetails(Long pricingControlId, Long agentId, String tariffApplicability) {
		PricingControlDetailsDao.updatePricingControlDetails(pricingControlId, agentId, tariffApplicability);
		
	}

	

	
	

	

}
