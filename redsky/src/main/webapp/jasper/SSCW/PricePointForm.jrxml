<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="PricePointForm"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="842"
		 pageHeight="595"
		 columnWidth="782"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isFloatColumnFooter="true"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.zoom" value="1.0" />
	<property name="ireport.x" value="0" />
	<property name="ireport.y" value="139" />
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Service Order Number" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Agent" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Market Area" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Declared Volume" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Declared Weight" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Chargeable Volume" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Chargeable Weight" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Minimum Density" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Tariff Published" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Rate" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Destination THC" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Invoice Total" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Converted Currency Total" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Type" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Rate Supplemental Charges" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Rate Basis" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Service" isForPrompting="true" class="java.lang.String"/>
	<parameter name="pricePoint_rate_basis" isForPrompting="true" class="java.lang.String"/>
	<parameter name="weight_unit" isForPrompting="true" class="java.lang.String"/>
	<parameter name="volume_unit" isForPrompting="true" class="java.lang.String"/>
	<parameter name="pricePoint_currency" isForPrompting="true" class="java.lang.String"/>
	<parameter name="pricePoint_Base_currency" isForPrompting="true" class="java.lang.String"/>
	<parameter name="pricePoint_VolumeUnit" isForPrompting="true" class="java.lang.String"/>
	<parameter name="pricePoint_WeightUnit" isForPrompting="false" class="java.lang.String"/>
	<parameter name="AgentConsignment" isForPrompting="true" class="java.lang.String"/>
	<parameter name="AgentAdditional" isForPrompting="true" class="java.lang.String"/>
	<parameter name="AgentCustoms" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT "PricePointForm-F-SSCW-pdf.jrxml",
     serviceorder.`shipNumber` AS serviceorder_shipNumber,
     serviceorder.`firstName` AS serviceorder_firstName,
     serviceorder.`lastName` AS serviceorder_lastName,
     serviceorder.`mode` AS serviceorder_mode,
     serviceorder.`originCity` AS serviceorder_originCity,
     serviceorder.`originCountryCode` AS serviceorder_originCountryCode,
     serviceorder.`destinationCity` AS serviceorder_destinationCity,
     serviceorder.`destinationCountryCode` AS serviceorder_destinationCountryCode,
     date_format(now(),'%b %d, %Y') as currentdate
FROM
     `serviceorder` serviceorder
where
	serviceorder.corpid = $P{Corporate ID}
	and serviceorder.`shipNumber` = $P{Service Order Number}]]></queryString>

	<field name="PricePointForm-F-SSCW-pdf.jrxml" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_firstName" class="java.lang.String"/>
	<field name="serviceorder_lastName" class="java.lang.String"/>
	<field name="serviceorder_mode" class="java.lang.String"/>
	<field name="serviceorder_originCity" class="java.lang.String"/>
	<field name="serviceorder_originCountryCode" class="java.lang.String"/>
	<field name="serviceorder_destinationCity" class="java.lang.String"/>
	<field name="serviceorder_destinationCountryCode" class="java.lang.String"/>
	<field name="currentdate" class="java.lang.String"/>


		<group  name="AgentNote" >
			<groupExpression><![CDATA[]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="48"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="18"
						width="781"
						height="30"
						key="textField-41"
						stretchType="RelativeToTallestObject"
						isPrintInFirstWholeBand="true"
						isPrintWhenDetailOverflows="true"/>
					<box rightPadding="3"></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Agent Notes for : "+($P{AgentConsignment}==null ? "" :$P{AgentConsignment})+"\n"+
($P{AgentAdditional}==null ? "" : $P{AgentAdditional})+"\n"+
($P{AgentCustoms}==null ? "" : $P{AgentCustoms})]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="1"
						y="1"
						width="781"
						height="17"
						forecolor="#007A8D"
						backcolor="#BCD2EF"
						key="textField-42"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Agent Notes"]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="368"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Opaque"
						x="1"
						y="346"
						width="781"
						height="22"
						backcolor="#BCD2EF"
						key="staticText-4"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="28"
						width="459"
						height="16"
						key="textField-1"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["As On : "+$F{currentdate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="44"
						width="459"
						height="16"
						key="textField-2"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Transferee Name                        "+$F{serviceorder_firstName}+" "+$F{serviceorder_lastName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="60"
						width="459"
						height="16"
						key="textField-3"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Quotation#                                 "+$F{serviceorder_shipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="124"
						width="198"
						height="16"
						key="textField-4"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Market Area "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="156"
						width="198"
						height="16"
						key="textField-6"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Declared Weight "+($P{weight_unit}.equals("kg") ? "(kgs)" : "(lbs)")]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="172"
						width="198"
						height="16"
						key="textField-7"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Chargeable Volume "+($P{pricePoint_VolumeUnit}.equals("mt") ? "(cbm)" : "(cft)")]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="188"
						width="198"
						height="16"
						key="textField-8"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Chargeable Weight "+($P{pricePoint_WeightUnit}.equals("kg") ? "(kgs)" : "(lbs)")]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="204"
						width="198"
						height="16"
						key="textField-9"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Minimum Density "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="220"
						width="198"
						height="16"
						key="textField-10"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Tariff Published"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="236"
						width="198"
						height="16"
						key="textField-12"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{pricePoint_rate_basis}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="252"
						width="198"
						height="16"
						key="textField-13"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Destination THC"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="268"
						width="198"
						height="16"
						key="textField-14"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Invoice Total"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="284"
						width="198"
						height="16"
						key="textField-15"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Converted Currency Total "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="60"
						width="198"
						height="16"
						key="textField-19"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Residence    "+$F{serviceorder_destinationCountryCode}+"-"+$F{serviceorder_destinationCity}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="124"
						width="198"
						height="16"
						key="textField-20"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{Market Area}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="156"
						width="198"
						height="16"
						key="textField-22"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{Declared Weight}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="172"
						width="198"
						height="16"
						key="textField-23"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[(new DecimalFormat("#,##0.00")).format(new BigDecimal($P{Chargeable Volume}==null ? "0.00" : $P{Chargeable Volume}))]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="188"
						width="198"
						height="16"
						key="textField-24"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[(new DecimalFormat("#,##0.00")).format(new BigDecimal($P{Chargeable Weight}))]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="204"
						width="198"
						height="16"
						key="textField-25"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{Minimum Density}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="220"
						width="198"
						height="16"
						key="textField-26"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{Tariff Published}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="236"
						width="198"
						height="16"
						key="textField-28"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{pricePoint_currency}+" "+(new DecimalFormat("#,##0.00")).format(new BigDecimal($P{Rate}))]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="252"
						width="198"
						height="16"
						key="textField-29"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{pricePoint_currency}+" "+(new DecimalFormat("#,##0.00")).format(new BigDecimal($P{Destination THC}))]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="268"
						width="198"
						height="16"
						key="textField-30"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{pricePoint_currency}+" "+(new DecimalFormat("#,##0.00")).format(new BigDecimal($P{Invoice Total}))]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="284"
						width="198"
						height="16"
						key="textField-31"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{pricePoint_Base_currency}+" "+(new DecimalFormat("#,##0.00")).format(new BigDecimal($P{Converted Currency Total}))]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="351"
						width="354"
						height="17"
						forecolor="#007A8D"
						key="textField-32"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Type"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="355"
						y="351"
						width="77"
						height="17"
						forecolor="#007A8D"
						key="textField-33"/>
					<box rightPadding="9"></box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Rate ("+$P{pricePoint_currency}+")"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="432"
						y="351"
						width="350"
						height="17"
						forecolor="#007A8D"
						key="textField-34"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Rate Basis"]]></textFieldExpression>
				</textField>
				<image  onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="-2"
						width="782"
						height="29"
						key="image-1"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/blue_band.jpg"]]></imageExpression>
				</image>
				<image  onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="77"
						width="782"
						height="29"
						key="image-2"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/blue_band.jpg"]]></imageExpression>
				</image>
				<image  onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="317"
						width="782"
						height="29"
						key="image-3"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/blue_band.jpg"]]></imageExpression>
				</image>
				<staticText>
					<reportElement
						x="45"
						y="3"
						width="169"
						height="20"
						forecolor="#007A8D"
						key="staticText-1"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Price Quote]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="45"
						y="82"
						width="169"
						height="20"
						forecolor="#007A8D"
						key="staticText-2"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Shipment]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="45"
						y="322"
						width="169"
						height="20"
						forecolor="#007A8D"
						key="staticText-3"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Supplemental Charges]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="108"
						width="198"
						height="16"
						key="textField-38"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Mode"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="108"
						width="198"
						height="16"
						key="textField-39"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_mode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="235"
						y="3"
						width="541"
						height="20"
						forecolor="#007A8D"
						key="textField-40"/>
					<box></box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Agent/Service: "+$P{Agent}+"/"+($P{Service}.equals("origin") ? "Origin" : ($P{Service}.equals("destination") ? "Destination" : $P{Service}))]]></textFieldExpression>
				</textField>
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="30"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="-228"
						width="198"
						height="16"
						key="textField-5"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Declared Volume "+($P{volume_unit}.equals("mt") ? "(cbm)" : "(cft)")]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="-67"
						width="781"
						height="16"
						key="textField-16"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["*Destination THC is estimate only, to be Charged at actual. 3rd party invoice backup required if higher amount. "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="578"
						y="-228"
						width="198"
						height="16"
						key="textField-21"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{Declared Volume}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="0"
						width="354"
						height="30"
						key="textField-35"
						stretchType="RelativeToBandHeight"
						isPrintWhenDetailOverflows="true"/>
					<box rightPadding="3"></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{Type}.replaceAll("~", "\n" )]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="355"
						y="0"
						width="77"
						height="30"
						key="textField-36"
						stretchType="RelativeToBandHeight"
						isPrintWhenDetailOverflows="true"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{Rate Supplemental Charges}.replaceAll("~", "\n" )]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="432"
						y="0"
						width="350"
						height="30"
						key="textField-37"
						stretchType="RelativeToBandHeight"
						isPrintWhenDetailOverflows="true"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{Rate Basis}.replaceAll("~", "\n" )]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
