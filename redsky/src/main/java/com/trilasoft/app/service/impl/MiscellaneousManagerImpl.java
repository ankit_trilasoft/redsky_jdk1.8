package com.trilasoft.app.service.impl;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.trilasoft.app.dao.MiscellaneousDao; 
import com.trilasoft.app.model.Miscellaneous; 
import com.trilasoft.app.service.MiscellaneousManager; 
import org.appfuse.service.impl.GenericManagerImpl;  
 
public class MiscellaneousManagerImpl extends GenericManagerImpl<Miscellaneous, Long> implements MiscellaneousManager
{ 
    MiscellaneousDao miscellaneousDao; 
    public MiscellaneousManagerImpl(MiscellaneousDao miscellaneousDao)
    { 
        super(miscellaneousDao); 
        this.miscellaneousDao = miscellaneousDao; 
    } 
     public List<Miscellaneous> findBySequenceNumber(String sequenceNumber) 
     { 
        return miscellaneousDao.findBySequenceNumber(sequenceNumber); 
    }
     public List findMaximumId(){
     	return miscellaneousDao.findMaximumId(); 
     }
     public List checkById(Long id) { 
         return miscellaneousDao.checkById(id); 
     }
	public void updateTrackingStatus(Date beginPacking, Date endPacking, Date packA, Date beginLoad, Date endLoad, Date loadA, Date deliveryShipper, Date deliveryLastDay, Date deliveryTA, Date deliveryA, Long id,String packATime,String deliveryATime,String deliveryTATime,String loadATime) {
		miscellaneousDao.updateTrackingStatus(beginPacking, endPacking, packA, beginLoad, endLoad, loadA, deliveryShipper, deliveryLastDay, deliveryTA, deliveryA,id,packATime,deliveryATime,deliveryTATime,loadATime);
	}
	public void updateBilling(Date deliveryA, Long id, String loginUser) {
		miscellaneousDao.updateBilling(deliveryA, id, loginUser);
		
	}
	public void updateTrackingStatusDestination(Date sitDestinationIn,Long id)
	{
		miscellaneousDao.updateTrackingStatusDestination(sitDestinationIn,id);
	}
	public void updateTrackingStatussitDestinationA(Date sitDestinationA,Long id)
	{
		miscellaneousDao.updateTrackingStatussitDestinationA(sitDestinationA, id);
	}
	public void updateTrackingStatusDestinationTA(String sitDestinationTA,Long id){
		miscellaneousDao.updateTrackingStatusDestinationTA(sitDestinationTA, id);
	}
	public void updateTrackingStatusDestinationDays(int sitDestinationDays,Long id)
	{
		miscellaneousDao.updateTrackingStatusDestinationDays(sitDestinationDays, id);
	}
	public List findHaulingMgtList(Date fromdate, Date todate,String corpid,String filterdDrivers,String opsHubList,String driverAgency,String selfHaul,String fromArea, String toArea, String fromZipCodes, String toZipCodes, String fromRadius, String toRadius, String job,String filterdTripNumber,String JobStatus,String filterdRegumber) {
		return miscellaneousDao.findHaulingMgtList(fromdate, todate, corpid, filterdDrivers, opsHubList, driverAgency, selfHaul, fromArea, toArea, fromZipCodes, toZipCodes, fromRadius, toRadius, job, filterdTripNumber,JobStatus,filterdRegumber);
	}
	public List findByOwner(String partnerCode,String corpId){
		return miscellaneousDao.findByOwner(partnerCode, corpId);
	}
	public List findByOwnerAndContact(String partnerCode,String corpId){
		return miscellaneousDao.findByOwnerAndContact(partnerCode, corpId);
	}
	public List findByOwnerFromTruck(String partnerCode,String truckNumber,String corpId){
		return miscellaneousDao.findByOwnerFromTruck(partnerCode,truckNumber, corpId);
	}
	public List findByOwnerPerticularTruck(String truckNumber,String corpId){
		return miscellaneousDao.findByOwnerPerticularTruck(truckNumber, corpId);
	}
	public List findByTrailerPerticularTruck(String truckNumber,String corpId){
		return miscellaneousDao.findByTrailerPerticularTruck(truckNumber, corpId);
	}
	public void updateMiscellaneous(Long id,String status){
		miscellaneousDao.updateMiscellaneous(id, status);	
	}
	public Map<String, String> getDriverAgency(String corpid){
		return miscellaneousDao.getDriverAgency(corpid);
	}
	public void updateTrackingStatusLateReason(String partyResponsible,String lateReason, Long id){
		 miscellaneousDao.updateTrackingStatusLateReason(partyResponsible,lateReason,id);
	}
	public void updatePackingDriverId(String partnerCode, String service,String shipNumber, String sessionCorpID){
		miscellaneousDao.updatePackingDriverId(partnerCode, service,shipNumber, sessionCorpID);
	}
	  public List partyResponsible(String corpId, String spNumber){
			return miscellaneousDao.partyResponsible(corpId,spNumber);
		}
	  public String weightForAmountDistribution(Long miscId,String grpId, String sessionCorpId){
		  return miscellaneousDao.weightForAmountDistribution(miscId,grpId, sessionCorpId);
	  }
	  public String findMasterOrderWeight(String id,String corpId){
		  return miscellaneousDao.findMasterOrderWeight(id, corpId);  
	  }
	public List findZipCodeList(String sessionCorpID, String lattitude1,String lattitude2, String longitude1, String longitude2) {
		return miscellaneousDao.findZipCodeList(sessionCorpID, lattitude1, lattitude2, longitude1, longitude2);
	}
	public List findLattLongList(String sessionCorpID, String zipCode) {
		return miscellaneousDao.findLattLongList(sessionCorpID, zipCode);
	}
	public void updateTripNumber(String tripNumber,String tripid){
		miscellaneousDao.updateTripNumber(tripNumber, tripid);
	}
	public List getTripOrder(String tripNumber, String sessionCorpID){
		return miscellaneousDao.getTripOrder(tripNumber, sessionCorpID);
	}
	public List findmaxTripNumber(String sessionCorpID){
		return miscellaneousDao.findmaxTripNumber(sessionCorpID);
	}
	public List getTripNumber(String tripNumber, String sessionCorpID){
		return miscellaneousDao.getTripNumber(tripNumber,sessionCorpID);
	}
	public void removeTripNumber(String tripid){
		miscellaneousDao.removeTripNumber(tripid);
	}
	public List findTripName(String sessionCorpID){
		return miscellaneousDao.findTripName(sessionCorpID);
	}
	public void updateJobStatus(String sessionCorpID,String soStatus,String orderNo){
		miscellaneousDao.updateJobStatus(sessionCorpID,soStatus,orderNo);
	}
	public List findToolTipForVehicle(String orderNo, String sessionCorpID, String job){
		return miscellaneousDao.findToolTipForVehicle(orderNo, sessionCorpID, job);
	}
	public void updateTargetRevenueRecognition(Date deliveryA, Long id, String loginUser) {
		miscellaneousDao.updateTargetRevenueRecognition(deliveryA, id, loginUser);
		
	}
}