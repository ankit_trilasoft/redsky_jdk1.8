package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.model.User;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PasswordOTPDao;
import com.trilasoft.app.model.PasswordOTP;
import com.trilasoft.app.service.PasswordOTPManager;



public class PasswordOTPManagerImpl extends GenericManagerImpl<PasswordOTP, Long> implements PasswordOTPManager {  

    PasswordOTPDao passwordOTPDao;
	
	public PasswordOTPManagerImpl(PasswordOTPDao passwordOTPDao) {
		super(passwordOTPDao); 
        this.passwordOTPDao = passwordOTPDao; 	
	}
	
	
	 public List checkById(Long id) {
		 
		 return passwordOTPDao.checkById(id);
	 }
	 public List<User> loadUserByUsername(String username) {
		 
		 return passwordOTPDao.loadUserByUsername(username);
	 }


	public List checkUserOTP(Long id, String verificationCode) { 
		return passwordOTPDao.checkUserOTP(id, verificationCode);
	}
}

