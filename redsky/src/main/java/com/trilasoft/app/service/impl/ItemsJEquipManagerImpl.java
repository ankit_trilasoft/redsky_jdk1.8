package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ItemsJEquipDao;
import com.trilasoft.app.model.ItemsJEquip;
import com.trilasoft.app.service.ItemsJEquipManager;

  
public class ItemsJEquipManagerImpl extends GenericManagerImpl<ItemsJEquip, Long> implements ItemsJEquipManager {   
	 ItemsJEquipDao itemsJEquipDao;   
  
    public ItemsJEquipManagerImpl(ItemsJEquipDao itemsJEquipDao) {   
        super(itemsJEquipDao);   
        this.itemsJEquipDao = itemsJEquipDao;   
    }   
  
    public List<ItemsJEquip> findByType(String type, String wcontract, Long id) {   
        return itemsJEquipDao.findByType(type, wcontract, id);   
    }
    
    public List<ItemsJEquip> findById(Long id) {   
        return itemsJEquipDao.findById(id);   
    }  
    
    
    
    public List<ItemsJEquip> findByDescript(String descript) {   
        return itemsJEquipDao.findByDescript(descript);
    }
    
    public List<ItemsJEquip> findSection(String itemType, String wcontract) {   
        return itemsJEquipDao.findSection(itemType, wcontract);
    }

	public List findMaterialExtraInfo(Long id,String workTicket, String contract,String corpID, String type) {
		return itemsJEquipDao.findMaterialExtraInfo(id,workTicket,contract,corpID,type);
	}
	public List miscellaneousRequestList(Long id,String ship,String corpID, String type,String division,String resource) {
		return itemsJEquipDao.miscellaneousRequestList(id,ship, corpID, type,division,resource);
	}
	public List salesRequestList(Long id,String ship,String corpID, String type) {
		return itemsJEquipDao.salesRequestList(id,ship, corpID, type);
	}
	 public List overallSalesRequestList(Long id,String ship,String corpID, String type,String begin,String end){
		 return itemsJEquipDao.overallSalesRequestList(id,ship, corpID, type,begin,end);
	 }
	public List findMaterials(Long id,String workTicket, String contract,String corpID, String type) {
		return itemsJEquipDao.findMaterials(id,workTicket,contract,corpID,type);
	}

	public List searchMaterials(String description, String workTicket, String contract, String corpID, String type) {
		return itemsJEquipDao.searchMaterials(description, workTicket, contract, corpID,type);
	}

	public List<ItemsJEquip> findByDescript(String type, String descript, String wcontract, Long id1) {
		// TODO Auto-generated method stub
		return null;
	}

	public List findContractFromBilling(String shipNumber) {
		return itemsJEquipDao.findContractFromBilling(shipNumber);
	}

	public List findContractFromCustomerFile(String shipNumber) {
		return itemsJEquipDao.findContractFromCustomerFile(shipNumber);
	}

	public List checkforBilling(String shipNumber) {
		return itemsJEquipDao.checkforBilling(shipNumber);
	}
	public List findMaterialByContract(String contract,String type,String corpID){
		return itemsJEquipDao.findMaterialByContract(contract,type,corpID);
	}
	public List findMaterialExtraInfoByContract(String contract,String type,String corpID,String division,String resource){
		return itemsJEquipDao.findMaterialExtraInfoByContract(contract,type,corpID,division,resource);
	}
	public List materialByCategory(String type,String corpID){
		return itemsJEquipDao.materialByCategory(type,corpID);
	}
	public List findMaterialExtraInfoByContractForSales(String contract,String type,String corpID){
		return itemsJEquipDao.findMaterialExtraInfoByContractForSales(contract,type,corpID);
	}
	
	
	public List findMaterialByContractTab(String contract,String type,String corpID){
		return itemsJEquipDao.findMaterialByContractTab(contract, type, corpID);
	}
	
	public List findByDescription(String descript, String contract, String corpID, String type){
		return itemsJEquipDao.findByDescription(descript, contract, corpID, type);
	}
	public List findWareHouseDesc(String wrehouse, String sessionCorpID){
		return itemsJEquipDao.findWareHouseDesc(wrehouse, sessionCorpID); 
	}
	public List getListValue(String descript, String type, Boolean controlled){
		return itemsJEquipDao.getListValue(descript, type, controlled);
	}
	public List findContractAndType(String corpID){
		return itemsJEquipDao.findContractAndType(corpID);
	}
	public List findChargesAndType(String contractType,String corpID){
		return itemsJEquipDao.findChargesAndType(contractType,corpID);
	}
	public List findResourceContractByPartnerId(Long parentId, String corpID){
		return itemsJEquipDao.findResourceContractByPartnerId(parentId,corpID);
	}
	public List getResource(String sessionCorpID,String type,String descript){
		return itemsJEquipDao.getResource(sessionCorpID,type,descript);
	}
	 public List findCharges(String contract,String chargeCode,String chargeDesc,String sessionCorpID){
		 return itemsJEquipDao.findCharges(contract,chargeCode,chargeDesc,sessionCorpID);
	 }

	public List findContracts(String contractType, String contractDesc,String sessionCorpID) {
		return itemsJEquipDao.findContracts(contractType, contractDesc,sessionCorpID);
	}

	public List searchResourceList(String resource, String ticket, String contract, String sessionCorpID, String category){
		return itemsJEquipDao.searchResourceList(resource, ticket, contract, sessionCorpID, category);
	} 

	public List findResource(String corpID) {
		return itemsJEquipDao.findResource(corpID);
	}
	public List findMaterialByCategory(String category, String sessionCorpID){
		return itemsJEquipDao.findMaterialByCategory(category, sessionCorpID);
	}

	public List findResourceTemplate(String contractType, String corpID) {
		return itemsJEquipDao.findResourceTemplate(contractType,corpID);
	}
	public List checkDuplicateResource(String descript, String sessionCorpID){
		return itemsJEquipDao.checkDuplicateResource(descript, sessionCorpID);
	}

	public List checkDuplicateResourceDayBasis(String descript,String sessionCorpID, String day,String shipNumber) {
		return itemsJEquipDao.checkDuplicateResourceDayBasis(descript,sessionCorpID, day,shipNumber);
	}
	public List findMaterialExtraMasterInfo(String sessionCorpID,String cat,String branch,String division,String resource) {
		return itemsJEquipDao.findMaterialExtraMasterInfo(sessionCorpID,cat,branch,division,resource);
	}
	public List getMaterialExtraMasterInfo(String sessionCorpID,String cat,String branch,String division) {
		return itemsJEquipDao.getMaterialExtraMasterInfo(sessionCorpID,cat,branch,division);
	}
	public List findMaterialExtraMasterInfoByContract(String cont, String sessionCorpID,String cat) {
		return itemsJEquipDao.findMaterialExtraMasterInfoByContract(cont,sessionCorpID,cat);
	}
	public java.util.Map findInvDescriptionList(String resource,String division,String category,String sessionCorpID){
		return itemsJEquipDao.findInvDescriptionList(resource,division,category,sessionCorpID);
	}
	public void  deleteInventory(Long id){
		 itemsJEquipDao.deleteInventory(id);
	}
	public List checkContract( Long id ,String sessionCorpID , String shipNumber)
	 {
		 return itemsJEquipDao.checkContract(id ,sessionCorpID,shipNumber );
	 }
	public List findResourceTemplateForAccPortal(String contractType,String corpID){
		return itemsJEquipDao.findResourceTemplateForAccPortal(contractType, corpID);
	}
	public String getResourceCategoryValue(String sessionCorpID,Long ticket,String resourceType,String resourceDescription){
		return itemsJEquipDao.getResourceCategoryValue(sessionCorpID, ticket,resourceType,resourceDescription);
	}
	
	
}