package com.trilasoft.app.webapp.tags;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;

import com.trilasoft.app.webapp.action.RoleBasedComponentPermissionUtil;
import com.trilasoft.app.webapp.util.SecurityHtmlReplacementUtil;

public class ControlAccessByRoleTag extends TagSupport {

	// ~ Instance fields
	// ================================================================================================

	private static Logger logger = Logger.getLogger(ControlAccessByRoleTag.class);
	
	private String componentId = "";
	
	private String replacementHtml = "";

	private RoleBasedComponentPermissionUtil roleBasedComponentPermissionUtil = null;
	
	
	public ControlAccessByRoleTag(){
	}

	// ~ Methods
	// ========================================================================================================

	public int doStartTag() throws JspException {

		if ((null == componentId) || "".equals(componentId)) {
			return Tag.SKIP_BODY;
		}
		
		ServletContext context = pageContext.getServletContext();
		XmlWebApplicationContext ctx = (XmlWebApplicationContext) context
				.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);

		roleBasedComponentPermissionUtil = (RoleBasedComponentPermissionUtil) ctx.getBean("componentConfigByRoleUtil");		
		//boolean accessAllowed = roleBasedComponentPermissionUtil.getComponentAccessAttrbute(componentId);
		int permission = roleBasedComponentPermissionUtil.getComponentPermission(componentId);
		pageContext.getRequest().setAttribute(componentId + "Permission", permission);
		
		//logger.warn("Setting permission for "+componentId+" permission "+permission);
		
		if ( permission < 2) {
			if (replacementHtml != null && !replacementHtml.equals(""))
				try {
		
					logger.warn("No permission to view "+componentId+" ");
					if (replacementHtml.equals("htmlReplacement:disableKeyBoardInputScript")) 
						replacementHtml = SecurityHtmlReplacementUtil.getDisableKeyBoardInputScript();
					pageContext.getOut().write(replacementHtml);
				} catch (IOException e) {
					throw new JspException (e.getMessage());
				}
			return  Tag.SKIP_BODY;
		}
		
		return Tag.EVAL_BODY_INCLUDE;
	}


	public void setRoleBasedComponentPermissionUtil(
			RoleBasedComponentPermissionUtil roleBasedComponentPermissionUtil) {
		this.roleBasedComponentPermissionUtil = roleBasedComponentPermissionUtil;
	}

	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public String getReplacementHtml() {
		return replacementHtml;
	}

	public void setReplacementHtml(String replacementHtml) {
		this.replacementHtml = replacementHtml;
	}
}
