

DELIMITER $$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `redsky`.`trigger_add_history_miscellaneous`
BEFORE UPDATE ON `redsky`.`miscellaneous`
FOR EACH ROW
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
  DECLARE unit LONGTEXT;

  DECLARE updatedon DATETIME;
  set NEW.updatedon=now();
  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
  SET unit = " ";

  
     IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
      select CONCAT(fieldNameList,'miscellaneous.sequenceNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
  END IF;

  IF (OLD.ship <> NEW.ship ) THEN
      select CONCAT(fieldNameList,'miscellaneous.ship~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ship,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ship,'~') into newValueList;
  END IF;

  IF (OLD.shipNumber <> NEW.shipNumber ) THEN
      select CONCAT(fieldNameList,'miscellaneous.shipNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
  END IF;
  IF (OLD.listLotNumber <> NEW.listLotNumber ) THEN
      select CONCAT(fieldNameList,'miscellaneous.listLotNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.listLotNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.listLotNumber,'~') into newValueList;
  END IF;
IF ((OLD.estimateGrossWeight <> NEW.estimateGrossWeight) or (OLD.estimateGrossWeight is null and NEW.estimateGrossWeight is not null)
      or (OLD.estimateGrossWeight is not null and NEW.estimateGrossWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.estimateGrossWeight~') into fieldNameList;
        IF(OLD.estimateGrossWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimateGrossWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimateGrossWeight is not null) then
            select CONCAT(oldValueList,OLD.estimateGrossWeight,'~') into oldValueList;
        END IF;
        IF(NEW.estimateGrossWeight is not null) then
            select CONCAT(newValueList,NEW.estimateGrossWeight,'~') into newValueList;
        END IF;
    END IF;
    
IF ((OLD.actualLineHaul <> NEW.actualLineHaul) or (OLD.actualLineHaul is null and NEW.actualLineHaul is not null)
      or (OLD.actualLineHaul is not null and NEW.actualLineHaul is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualLineHaul~') into fieldNameList;
        IF(OLD.actualLineHaul is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualLineHaul is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualLineHaul is not null) then
            select CONCAT(oldValueList,OLD.actualLineHaul,'~') into oldValueList;
        END IF;
        IF(NEW.actualLineHaul is not null) then
            select CONCAT(newValueList,NEW.actualLineHaul,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((OLD.entitleCubicMtr <> NEW.entitleCubicMtr) or (OLD.entitleCubicMtr is null and NEW.entitleCubicMtr is not null)
      or (OLD.entitleCubicMtr is not null and NEW.entitleCubicMtr is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.entitleCubicMtr~') into fieldNameList;
        IF(OLD.entitleCubicMtr is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.entitleCubicMtr is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.entitleCubicMtr is not null) then
            select CONCAT(oldValueList,OLD.entitleCubicMtr,'~') into oldValueList;
        END IF;
        IF(NEW.entitleCubicMtr is not null) then
            select CONCAT(newValueList,NEW.entitleCubicMtr,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.estimateCubicMtr <> NEW.estimateCubicMtr) or (OLD.estimateCubicMtr is null and NEW.estimateCubicMtr is not null)
      or (OLD.estimateCubicMtr is not null and NEW.estimateCubicMtr is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.estimateCubicMtr~') into fieldNameList;
        IF(OLD.estimateCubicMtr is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimateCubicMtr is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimateCubicMtr is not null) then
            select CONCAT(oldValueList,OLD.estimateCubicMtr,'~') into oldValueList;
        END IF;
        IF(NEW.estimateCubicMtr is not null) then
            select CONCAT(newValueList,NEW.estimateCubicMtr,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.actualCubicMtr <> NEW.actualCubicMtr) or (OLD.actualCubicMtr is null and NEW.actualCubicMtr is not null)
      or (OLD.actualCubicMtr is not null and NEW.actualCubicMtr is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualCubicMtr~') into fieldNameList;
        IF(OLD.actualCubicMtr is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualCubicMtr is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualCubicMtr is not null) then
            select CONCAT(oldValueList,OLD.actualCubicMtr,'~') into oldValueList;
        END IF;
        IF(NEW.actualCubicMtr is not null) then
            select CONCAT(newValueList,NEW.actualCubicMtr,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.netEntitleCubicMtr <> NEW.netEntitleCubicMtr) or (OLD.netEntitleCubicMtr is null and NEW.netEntitleCubicMtr is not null)
      or (OLD.netEntitleCubicMtr is not null and NEW.netEntitleCubicMtr is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.netEntitleCubicMtr~') into fieldNameList;
        IF(OLD.netEntitleCubicMtr is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.netEntitleCubicMtr is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.netEntitleCubicMtr is not null) then
            select CONCAT(oldValueList,OLD.netEntitleCubicMtr,'~') into oldValueList;
        END IF;
        IF(NEW.netEntitleCubicMtr is not null) then
            select CONCAT(newValueList,NEW.netEntitleCubicMtr,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.netEstimateCubicMtr <> NEW.netEstimateCubicMtr) or (OLD.netEstimateCubicMtr is null and NEW.netEstimateCubicMtr is not null)
      or (OLD.netEstimateCubicMtr is not null and NEW.netEstimateCubicMtr is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.netEstimateCubicMtr~') into fieldNameList;
        IF(OLD.netEstimateCubicMtr is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.netEstimateCubicMtr is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.netEstimateCubicMtr is not null) then
            select CONCAT(oldValueList,OLD.netEstimateCubicMtr,'~') into oldValueList;
        END IF;
        IF(NEW.netEstimateCubicMtr is not null) then
            select CONCAT(newValueList,NEW.netEstimateCubicMtr,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.netActualCubicMtr <> NEW.netActualCubicMtr) or (OLD.netActualCubicMtr is null and NEW.netActualCubicMtr is not null)
      or (OLD.netActualCubicMtr is not null and NEW.netActualCubicMtr is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.netActualCubicMtr~') into fieldNameList;
        IF(OLD.netActualCubicMtr is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.netActualCubicMtr is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.netActualCubicMtr is not null) then
            select CONCAT(oldValueList,OLD.netActualCubicMtr,'~') into oldValueList;
        END IF;
        IF(NEW.netActualCubicMtr is not null) then
            select CONCAT(newValueList,NEW.netActualCubicMtr,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.rwghGrossKilo <> NEW.rwghGrossKilo) or (OLD.rwghGrossKilo is null and NEW.rwghGrossKilo is not null)
      or (OLD.rwghGrossKilo is not null and NEW.rwghGrossKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.rwghGrossKilo~') into fieldNameList;
        IF(OLD.rwghGrossKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rwghGrossKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rwghGrossKilo is not null) then
            select CONCAT(oldValueList,OLD.rwghGrossKilo,'~') into oldValueList;
        END IF;
        IF(NEW.rwghGrossKilo is not null) then
            select CONCAT(newValueList,NEW.rwghGrossKilo,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.rwghTareKilo <> NEW.rwghTareKilo) or (OLD.rwghTareKilo is null and NEW.rwghTareKilo is not null)
      or (OLD.rwghTareKilo is not null and NEW.rwghTareKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.rwghTareKilo~') into fieldNameList;
        IF(OLD.rwghTareKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rwghTareKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rwghTareKilo is not null) then
            select CONCAT(oldValueList,OLD.rwghTareKilo,'~') into oldValueList;
        END IF;
        IF(NEW.rwghTareKilo is not null) then
            select CONCAT(newValueList,NEW.rwghTareKilo,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.rwghNetKilo <> NEW.rwghNetKilo) or (OLD.rwghNetKilo is null and NEW.rwghNetKilo is not null)
      or (OLD.rwghNetKilo is not null and NEW.rwghNetKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.rwghNetKilo~') into fieldNameList;
        IF(OLD.rwghNetKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rwghNetKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rwghNetKilo is not null) then
            select CONCAT(oldValueList,OLD.rwghNetKilo,'~') into oldValueList;
        END IF;
        IF(NEW.rwghNetKilo is not null) then
            select CONCAT(newValueList,NEW.rwghNetKilo,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.chargeableGrossWeightKilo <> NEW.chargeableGrossWeightKilo) or (OLD.chargeableGrossWeightKilo is null and NEW.chargeableGrossWeightKilo is not null)
      or (OLD.chargeableGrossWeightKilo is not null and NEW.chargeableGrossWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.chargeableGrossWeightKilo~') into fieldNameList;
        IF(OLD.chargeableGrossWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.chargeableGrossWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.chargeableGrossWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.chargeableGrossWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.chargeableGrossWeightKilo is not null) then
            select CONCAT(newValueList,NEW.chargeableGrossWeightKilo,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.chargeableNetWeightKilo <> NEW.chargeableNetWeightKilo) or (OLD.chargeableNetWeightKilo is null and NEW.chargeableNetWeightKilo is not null)
      or (OLD.chargeableNetWeightKilo is not null and NEW.chargeableNetWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.chargeableNetWeightKilo~') into fieldNameList;
        IF(OLD.chargeableNetWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.chargeableNetWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.chargeableNetWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.chargeableNetWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.chargeableNetWeightKilo is not null) then
            select CONCAT(newValueList,NEW.chargeableNetWeightKilo,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.chargeableTareWeightKilo <> NEW.chargeableTareWeightKilo) or (OLD.chargeableTareWeightKilo is null and NEW.chargeableTareWeightKilo is not null)
      or (OLD.chargeableTareWeightKilo is not null and NEW.chargeableTareWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.chargeableTareWeightKilo~') into fieldNameList;
        IF(OLD.chargeableTareWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.chargeableTareWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.chargeableTareWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.chargeableTareWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.chargeableTareWeightKilo is not null) then
            select CONCAT(newValueList,NEW.chargeableTareWeightKilo,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.rwghCubicMtr <> NEW.rwghCubicMtr) or (OLD.rwghCubicMtr is null and NEW.rwghCubicMtr is not null)
      or (OLD.rwghCubicMtr is not null and NEW.rwghCubicMtr is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.rwghCubicMtr~') into fieldNameList;
        IF(OLD.rwghCubicMtr is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rwghCubicMtr is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rwghCubicMtr is not null) then
            select CONCAT(oldValueList,OLD.rwghCubicMtr,'~') into oldValueList;
        END IF;
        IF(NEW.rwghCubicMtr is not null) then
            select CONCAT(newValueList,NEW.rwghCubicMtr,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.rwghNetCubicMtr <> NEW.rwghNetCubicMtr) or (OLD.rwghNetCubicMtr is null and NEW.rwghNetCubicMtr is not null)
      or (OLD.rwghNetCubicMtr is not null and NEW.rwghNetCubicMtr is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.rwghNetCubicMtr~') into fieldNameList;
        IF(OLD.rwghNetCubicMtr is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rwghNetCubicMtr is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rwghNetCubicMtr is not null) then
            select CONCAT(oldValueList,OLD.rwghNetCubicMtr,'~') into oldValueList;
        END IF;
        IF(NEW.rwghNetCubicMtr is not null) then
            select CONCAT(newValueList,NEW.rwghNetCubicMtr,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.chargeableNetCubicMtr <> NEW.chargeableNetCubicMtr) or (OLD.chargeableNetCubicMtr is null and NEW.chargeableNetCubicMtr is not null)
      or (OLD.chargeableNetCubicMtr is not null and NEW.chargeableNetCubicMtr is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.chargeableNetCubicMtr~') into fieldNameList;
        IF(OLD.chargeableNetCubicMtr is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.chargeableNetCubicMtr is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.chargeableNetCubicMtr is not null) then
            select CONCAT(oldValueList,OLD.chargeableNetCubicMtr,'~') into oldValueList;
        END IF;
        IF(NEW.chargeableNetCubicMtr is not null) then
            select CONCAT(newValueList,NEW.chargeableNetCubicMtr,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.chargeableCubicMtr <> NEW.chargeableCubicMtr) or (OLD.chargeableCubicMtr is null and NEW.chargeableCubicMtr is not null)
      or (OLD.chargeableCubicMtr is not null and NEW.chargeableCubicMtr is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.chargeableCubicMtr~') into fieldNameList;
        IF(OLD.chargeableCubicMtr is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.chargeableCubicMtr is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.chargeableCubicMtr is not null) then
            select CONCAT(oldValueList,OLD.chargeableCubicMtr,'~') into oldValueList;
        END IF;
        IF(NEW.chargeableCubicMtr is not null) then
            select CONCAT(newValueList,NEW.chargeableCubicMtr,'~') into newValueList;
        END IF;
    END IF;
   

    
 
IF ((OLD.estimateGrossWeightKilo <> NEW.estimateGrossWeightKilo) or (OLD.estimateGrossWeightKilo is null and NEW.estimateGrossWeightKilo is not null)
      or (OLD.estimateGrossWeightKilo is not null and NEW.estimateGrossWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.estimateGrossWeightKilo~') into fieldNameList;
        IF(OLD.estimateGrossWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimateGrossWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimateGrossWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.estimateGrossWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.estimateGrossWeightKilo is not null) then
            select CONCAT(newValueList,NEW.estimateGrossWeightKilo,'~') into newValueList;
        END IF;
    END IF;


    
    
        
IF ((OLD.actualGrossWeight <> NEW.actualGrossWeight) or (OLD.actualGrossWeight is null and NEW.actualGrossWeight is not null)
      or (OLD.actualGrossWeight is not null and NEW.actualGrossWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualGrossWeight~') into fieldNameList;
        IF(OLD.actualGrossWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualGrossWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualGrossWeight is not null) then
            select CONCAT(oldValueList,OLD.actualGrossWeight,'~') into oldValueList;
        END IF;
        IF(NEW.actualGrossWeight is not null) then
            select CONCAT(newValueList,NEW.actualGrossWeight,'~') into newValueList;
        END IF;
    END IF;



      

IF ((OLD.actualGrossWeightKilo <> NEW.actualGrossWeightKilo) or (OLD.actualGrossWeightKilo is null and NEW.actualGrossWeightKilo is not null)
      or (OLD.actualGrossWeightKilo is not null and NEW.actualGrossWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualGrossWeightKilo~') into fieldNameList;
        IF(OLD.actualGrossWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualGrossWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualGrossWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.actualGrossWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.actualGrossWeightKilo is not null) then
            select CONCAT(newValueList,NEW.actualGrossWeightKilo,'~') into newValueList;
        END IF;
    END IF;

  IF ((OLD.entitleTareWeight <> NEW.entitleTareWeight) or (OLD.entitleTareWeight is null and NEW.entitleTareWeight is not null)
      or (OLD.entitleTareWeight is not null and NEW.entitleTareWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.entitleTareWeight~') into fieldNameList;
        IF(OLD.entitleTareWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.entitleTareWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.entitleTareWeight is not null) then
            select CONCAT(oldValueList,OLD.entitleTareWeight,'~') into oldValueList;
        END IF;
        IF(NEW.entitleTareWeight is not null) then
            select CONCAT(newValueList,NEW.entitleTareWeight,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.estimateTareWeight <> NEW.estimateTareWeight) or (OLD.estimateTareWeight is null and NEW.estimateTareWeight is not null)
      or (OLD.estimateTareWeight is not null and NEW.estimateTareWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.estimateTareWeight~') into fieldNameList;
        IF(OLD.estimateTareWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimateTareWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimateTareWeight is not null) then
            select CONCAT(oldValueList,OLD.estimateTareWeight,'~') into oldValueList;
        END IF;
        IF(NEW.estimateTareWeight is not null) then
            select CONCAT(newValueList,NEW.estimateTareWeight,'~') into newValueList;
        END IF;
    END IF;


IF ((OLD.estimateTareWeightKilo <> NEW.estimateTareWeightKilo) or (OLD.estimateTareWeightKilo is null and NEW.estimateTareWeightKilo is not null)
      or (OLD.estimateTareWeightKilo is not null and NEW.estimateTareWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.estimateTareWeightKilo~') into fieldNameList;
        IF(OLD.estimateTareWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimateTareWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimateTareWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.estimateTareWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.estimateTareWeightKilo is not null) then
            select CONCAT(newValueList,NEW.estimateTareWeightKilo,'~') into newValueList;
        END IF;
    END IF;

     
IF ((OLD.actualTareWeight <> NEW.actualTareWeight) or (OLD.actualTareWeight is null and NEW.actualTareWeight is not null)
      or (OLD.actualTareWeight is not null and NEW.actualTareWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualTareWeight~') into fieldNameList;
        IF(OLD.actualTareWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualTareWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualTareWeight is not null) then
            select CONCAT(oldValueList,OLD.actualTareWeight,'~') into oldValueList;
        END IF;
        IF(NEW.actualTareWeight is not null) then
            select CONCAT(newValueList,NEW.actualTareWeight,'~') into newValueList;
        END IF;
    END IF;



IF ((OLD.actualTareWeightKilo <> NEW.actualTareWeightKilo) or (OLD.actualTareWeightKilo is null and NEW.actualTareWeightKilo is not null)
      or (OLD.actualTareWeightKilo is not null and NEW.actualTareWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualTareWeightKilo~') into fieldNameList;
        IF(OLD.actualTareWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualTareWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualTareWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.actualTareWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.actualTareWeightKilo is not null) then
            select CONCAT(newValueList,NEW.actualTareWeightKilo,'~') into newValueList;
        END IF;
    END IF;

   IF ((OLD.entitleNetWeight <> NEW.entitleNetWeight) or (OLD.entitleNetWeight is null and NEW.entitleNetWeight is not null)
      or (OLD.entitleNetWeight is not null and NEW.entitleNetWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.entitleNetWeight~') into fieldNameList;
        IF(OLD.entitleNetWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.entitleNetWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.entitleNetWeight is not null) then
            select CONCAT(oldValueList,OLD.entitleNetWeight,'~') into oldValueList;
        END IF;
        IF(NEW.entitleNetWeight is not null) then
            select CONCAT(newValueList,NEW.entitleNetWeight,'~') into newValueList;
        END IF;
    END IF;


  IF ((OLD.entitleNetWeightKilo <> NEW.entitleNetWeightKilo) or (OLD.entitleNetWeightKilo is null and NEW.entitleNetWeightKilo is not null)
      or (OLD.entitleNetWeightKilo is not null and NEW.entitleNetWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.entitleNetWeightKilo~') into fieldNameList;
        IF(OLD.entitleNetWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.entitleNetWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.entitleNetWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.entitleNetWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.entitleNetWeightKilo is not null) then
            select CONCAT(newValueList,NEW.entitleNetWeightKilo,'~') into newValueList;
        END IF;
    END IF;


 IF ((OLD.entitleGrossWeight <> NEW.entitleGrossWeight) or (OLD.entitleGrossWeight is null and NEW.entitleGrossWeight is not null)
      or (OLD.entitleGrossWeight is not null and NEW.entitleGrossWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.entitleGrossWeight~') into fieldNameList;
        IF(OLD.entitleGrossWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.entitleGrossWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.entitleGrossWeight is not null) then
            select CONCAT(oldValueList,OLD.entitleGrossWeight,'~') into oldValueList;
        END IF;
        IF(NEW.entitleGrossWeight is not null) then
            select CONCAT(newValueList,NEW.entitleGrossWeight,'~') into newValueList;
        END IF;
    END IF;

  IF ((OLD.entitleGrossWeightKilo <> NEW.entitleGrossWeightKilo) or (OLD.entitleGrossWeightKilo is null and NEW.entitleGrossWeightKilo is not null)
      or (OLD.entitleGrossWeightKilo is not null and NEW.entitleGrossWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.entitleGrossWeightKilo~') into fieldNameList;
        IF(OLD.entitleGrossWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.entitleGrossWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.entitleGrossWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.entitleGrossWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.entitleGrossWeightKilo is not null) then
            select CONCAT(newValueList,NEW.entitleGrossWeightKilo,'~') into newValueList;
        END IF;
    END IF;

  IF ((OLD.estimatedNetWeight <> NEW.estimatedNetWeight) or (OLD.estimatedNetWeight is null and NEW.estimatedNetWeight is not null)
      or (OLD.estimatedNetWeight is not null and NEW.estimatedNetWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.estimatedNetWeight~') into fieldNameList;
        IF(OLD.estimatedNetWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimatedNetWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimatedNetWeight is not null) then
            select CONCAT(oldValueList,OLD.estimatedNetWeight,'~') into oldValueList;
        END IF;
        IF(NEW.estimatedNetWeight is not null) then
            select CONCAT(newValueList,NEW.estimatedNetWeight,'~') into newValueList;
        END IF;
    END IF;

    IF ((OLD.estimatedNetWeightKilo <> NEW.estimatedNetWeightKilo) or (OLD.estimatedNetWeightKilo is null and NEW.estimatedNetWeightKilo is not null)
      or (OLD.estimatedNetWeightKilo is not null and NEW.estimatedNetWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.estimatedNetWeightKilo~') into fieldNameList;
        IF(OLD.estimatedNetWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimatedNetWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimatedNetWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.estimatedNetWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.estimatedNetWeightKilo is not null) then
            select CONCAT(newValueList,NEW.estimatedNetWeightKilo,'~') into newValueList;
        END IF;
    END IF;

 IF ((OLD.actualNetWeight <> NEW.actualNetWeight) or (OLD.actualNetWeight is null and NEW.actualNetWeight is not null)
      or (OLD.actualNetWeight is not null and NEW.actualNetWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualNetWeight~') into fieldNameList;
        IF(OLD.actualNetWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualNetWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualNetWeight is not null) then
            select CONCAT(oldValueList,OLD.actualNetWeight,'~') into oldValueList;
        END IF;
        IF(NEW.actualNetWeight is not null) then
            select CONCAT(newValueList,NEW.actualNetWeight,'~') into newValueList;
        END IF;
  END IF;


 
 IF ((OLD.actualNetWeightKilo <> NEW.actualNetWeightKilo) or (OLD.actualNetWeightKilo is null and NEW.actualNetWeightKilo is not null)
      or (OLD.actualNetWeightKilo is not null and NEW.actualNetWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualNetWeightKilo~') into fieldNameList;
        IF(OLD.actualNetWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualNetWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualNetWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.actualNetWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.actualNetWeightKilo is not null) then
            select CONCAT(newValueList,NEW.actualNetWeightKilo,'~') into newValueList;
        END IF;
 END IF;

   IF ((OLD.entitleNumberAuto <> NEW.entitleNumberAuto) or (OLD.entitleNumberAuto is null and NEW.entitleNumberAuto is not null)
      or (OLD.entitleNumberAuto is not null and NEW.entitleNumberAuto is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.entitleNumberAuto~') into fieldNameList;
        IF(OLD.entitleNumberAuto is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.entitleNumberAuto is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.entitleNumberAuto is not null) then
            select CONCAT(oldValueList,OLD.entitleNumberAuto,'~') into oldValueList;
        END IF;
        IF(NEW.entitleNumberAuto is not null) then
            select CONCAT(newValueList,NEW.entitleNumberAuto,'~') into newValueList;
        END IF;
    END IF;

IF ((OLD.estimateAuto <> NEW.estimateAuto) or (OLD.estimateAuto is null and NEW.estimateAuto is not null)
      or (OLD.estimateAuto is not null and NEW.estimateAuto is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.estimateAuto~') into fieldNameList;
        IF(OLD.estimateAuto is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimateAuto is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimateAuto is not null) then
            select CONCAT(oldValueList,OLD.estimateAuto,'~') into oldValueList;
        END IF;
        IF(NEW.estimateAuto is not null) then
            select CONCAT(newValueList,NEW.estimateAuto,'~') into newValueList;
        END IF;
    END IF;


 IF ((OLD.actualAuto <> NEW.actualAuto) or (OLD.actualAuto is null and NEW.actualAuto is not null)
      or (OLD.actualAuto is not null and NEW.actualAuto is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualAuto~') into fieldNameList;
        IF(OLD.actualAuto is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualAuto is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualAuto is not null) then
            select CONCAT(oldValueList,OLD.actualAuto,'~') into oldValueList;
        END IF;
        IF(NEW.actualAuto is not null) then
            select CONCAT(newValueList,NEW.actualAuto,'~') into newValueList;
        END IF;
    END IF;


  IF ((OLD.entitleCubicFeet <> NEW.entitleCubicFeet) or (OLD.entitleCubicFeet is null and NEW.entitleCubicFeet is not null)
      or (OLD.entitleCubicFeet is not null and NEW.entitleCubicFeet is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.entitleCubicFeet~') into fieldNameList;
        IF(OLD.entitleCubicFeet is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.entitleCubicFeet is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.entitleCubicFeet is not null) then
            select CONCAT(oldValueList,OLD.entitleCubicFeet,'~') into oldValueList;
        END IF;
        IF(NEW.entitleCubicFeet is not null) then
            select CONCAT(newValueList,NEW.entitleCubicFeet,'~') into newValueList;
        END IF;
    END IF;

 IF ((OLD.estimateCubicFeet <> NEW.estimateCubicFeet) or (OLD.estimateCubicFeet is null and NEW.estimateCubicFeet is not null)
      or (OLD.estimateCubicFeet is not null and NEW.estimateCubicFeet is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.estimateCubicFeet~') into fieldNameList;
        IF(OLD.estimateCubicFeet is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimateCubicFeet is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimateCubicFeet is not null) then
            select CONCAT(oldValueList,OLD.estimateCubicFeet,'~') into oldValueList;
        END IF;
        IF(NEW.estimateCubicFeet is not null) then
            select CONCAT(newValueList,NEW.estimateCubicFeet,'~') into newValueList;
        END IF;
    END IF;


 IF ((OLD.actualCubicFeet <> NEW.actualCubicFeet) or (OLD.actualCubicFeet is null and NEW.actualCubicFeet is not null)
      or (OLD.actualCubicFeet is not null and NEW.actualCubicFeet is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualCubicFeet~') into fieldNameList;
        IF(OLD.actualCubicFeet is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualCubicFeet is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualCubicFeet is not null) then
            select CONCAT(oldValueList,OLD.actualCubicFeet,'~') into oldValueList;
        END IF;
        IF(NEW.actualCubicFeet is not null) then
            select CONCAT(newValueList,NEW.actualCubicFeet,'~') into newValueList;
        END IF;
    END IF;



 IF ((OLD.netEntitleCubicFeet <> NEW.netEntitleCubicFeet) or (OLD.netEntitleCubicFeet is null and NEW.netEntitleCubicFeet is not null)
      or (OLD.netEntitleCubicFeet is not null and NEW.netEntitleCubicFeet is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.netEntitleCubicFeet~') into fieldNameList;
        IF(OLD.netEntitleCubicFeet is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.netEntitleCubicFeet is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.netEntitleCubicFeet is not null) then
            select CONCAT(oldValueList,OLD.netEntitleCubicFeet,'~') into oldValueList;
        END IF;
        IF(NEW.netEntitleCubicFeet is not null) then
            select CONCAT(newValueList,NEW.netEntitleCubicFeet,'~') into newValueList;
        END IF;
    END IF;



  IF ((OLD.netEstimateCubicFeet <> NEW.netEstimateCubicFeet) or (OLD.netEstimateCubicFeet is null and NEW.netEstimateCubicFeet is not null)
      or (OLD.netEstimateCubicFeet is not null and NEW.netEstimateCubicFeet is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.netEstimateCubicFeet~') into fieldNameList;
        IF(OLD.netEstimateCubicFeet is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.netEstimateCubicFeet is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.netEstimateCubicFeet is not null) then
            select CONCAT(oldValueList,OLD.netEstimateCubicFeet,'~') into oldValueList;
        END IF;
        IF(NEW.netEstimateCubicFeet is not null) then
            select CONCAT(newValueList,NEW.netEstimateCubicFeet,'~') into newValueList;
        END IF;
   END IF;


  IF ((OLD.netActualCubicFeet <> NEW.netActualCubicFeet) or (OLD.netActualCubicFeet is null and NEW.netActualCubicFeet is not null)
      or (OLD.netActualCubicFeet is not null and NEW.netActualCubicFeet is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.netActualCubicFeet~') into fieldNameList;
        IF(OLD.netActualCubicFeet is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.netActualCubicFeet is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.netActualCubicFeet is not null) then
            select CONCAT(oldValueList,OLD.netActualCubicFeet,'~') into oldValueList;
        END IF;
        IF(NEW.netActualCubicFeet is not null) then
            select CONCAT(newValueList,NEW.netActualCubicFeet,'~') into newValueList;
        END IF;
    END IF;


  IF (OLD.bookerSelfPacking <> NEW.bookerSelfPacking ) THEN
      select CONCAT(fieldNameList,'miscellaneous.bookerSelfPacking~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bookerSelfPacking,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bookerSelfPacking,'~') into newValueList;
  END IF;
  IF (OLD.bookerOwnAuthority <> NEW.bookerOwnAuthority ) THEN
      select CONCAT(fieldNameList,'miscellaneous.bookerOwnAuthority~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bookerOwnAuthority,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bookerOwnAuthority,'~') into newValueList;
  END IF;

  IF (OLD.packAuthorize <> NEW.packAuthorize ) THEN
      select CONCAT(fieldNameList,'miscellaneous.packAuthorize~') into fieldNameList;
      select CONCAT(oldValueList,OLD.packAuthorize,'~') into oldValueList;
      select CONCAT(newValueList,NEW.packAuthorize,'~') into newValueList;
  END IF;
  IF (OLD.unPack <> NEW.unPack ) THEN
      select CONCAT(fieldNameList,'miscellaneous.unPack~') into fieldNameList;
      select CONCAT(oldValueList,OLD.unPack,'~') into oldValueList;
      select CONCAT(newValueList,NEW.unPack,'~') into newValueList;
  END IF;


  IF (OLD.packingBulky <> NEW.packingBulky ) THEN
      select CONCAT(fieldNameList,'miscellaneous.packingBulky~') into fieldNameList;
      select CONCAT(oldValueList,OLD.packingBulky,'~') into oldValueList;
      select CONCAT(newValueList,NEW.packingBulky,'~') into newValueList;
  END IF;
  IF (OLD.codeHauling <> NEW.codeHauling ) THEN
      select CONCAT(fieldNameList,'miscellaneous.codeHauling~') into fieldNameList;
      select CONCAT(oldValueList,OLD.codeHauling,'~') into oldValueList;
      select CONCAT(newValueList,NEW.codeHauling,'~') into newValueList;
  END IF;

  IF (OLD.tarif <> NEW.tarif ) THEN
      select CONCAT(fieldNameList,'miscellaneous.tarif~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tarif,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tarif,'~') into newValueList;
  END IF;

  IF (OLD.section <> NEW.section ) THEN
      select CONCAT(fieldNameList,'miscellaneous.section~') into fieldNameList;
      select CONCAT(oldValueList,OLD.section,'~') into oldValueList;
      select CONCAT(newValueList,NEW.section,'~') into newValueList;
  END IF;
  IF (OLD.applicationNumber3rdParty <> NEW.applicationNumber3rdParty ) THEN
      select CONCAT(fieldNameList,'miscellaneous.applicationNumber3rdParty~') into fieldNameList;
      select CONCAT(oldValueList,OLD.applicationNumber3rdParty,'~') into oldValueList;
      select CONCAT(newValueList,NEW.applicationNumber3rdParty,'~') into newValueList;
  END IF;
  IF (OLD.needWaiveEstimate <> NEW.needWaiveEstimate ) THEN
      select CONCAT(fieldNameList,'miscellaneous.needWaiveEstimate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.needWaiveEstimate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.needWaiveEstimate,'~') into newValueList;
  END IF;


  IF (OLD.needWaiveSurvey <> NEW.needWaiveSurvey ) THEN
      select CONCAT(fieldNameList,'miscellaneous.needWaiveSurvey~') into fieldNameList;
      select CONCAT(oldValueList,OLD.needWaiveSurvey,'~') into oldValueList;
      select CONCAT(newValueList,NEW.needWaiveSurvey,'~') into newValueList;
  END IF;

  IF (OLD.needWaivePeakRate <> NEW.needWaivePeakRate ) THEN
      select CONCAT(fieldNameList,'miscellaneous.needWaivePeakRate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.needWaivePeakRate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.needWaivePeakRate,'~') into newValueList;
  END IF;

  IF (OLD.needWaiveSRA <> NEW.needWaiveSRA ) THEN
      select CONCAT(fieldNameList,'miscellaneous.needWaiveSRA~') into fieldNameList;
      select CONCAT(oldValueList,OLD.needWaiveSRA,'~') into oldValueList;
      select CONCAT(newValueList,NEW.needWaiveSRA,'~') into newValueList;
  END IF;
  IF (OLD.vanLineOrderNumber <> NEW.vanLineOrderNumber ) THEN
      select CONCAT(fieldNameList,'miscellaneous.vanLineOrderNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanLineOrderNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanLineOrderNumber,'~') into newValueList;
  END IF;
  IF (OLD.vanLineNationalCode <> NEW.vanLineNationalCode ) THEN
      select CONCAT(fieldNameList,'miscellaneous.vanLineNationalCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanLineNationalCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanLineNationalCode,'~') into newValueList;
  END IF;

  IF (OLD.originCountyCode <> NEW.originCountyCode ) THEN
      select CONCAT(fieldNameList,'miscellaneous.originCountyCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originCountyCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originCountyCode,'~') into newValueList;
  END IF;

  IF (OLD.originStateCode <> NEW.originStateCode ) THEN
      select CONCAT(fieldNameList,'miscellaneous.originStateCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originStateCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originStateCode,'~') into newValueList;
  END IF;

  IF (OLD.originCounty <> NEW.originCounty ) THEN
      select CONCAT(fieldNameList,'miscellaneous.originCounty~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originCounty,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originCounty,'~') into newValueList;
  END IF;

  IF (OLD.originState <> NEW.originState ) THEN
      select CONCAT(fieldNameList,'miscellaneous.originState~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originState,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originState,'~') into newValueList;
  END IF;


  IF (OLD.destinationCountyCode <> NEW.destinationCountyCode ) THEN
      select CONCAT(fieldNameList,'miscellaneous.destinationCountyCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationCountyCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationCountyCode,'~') into newValueList;
  END IF;

  IF (OLD.destinationStateCode <> NEW.destinationStateCode ) THEN
      select CONCAT(fieldNameList,'miscellaneous.destinationStateCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationStateCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationStateCode,'~') into newValueList;
  END IF;

  IF (OLD.destinationCounty <> NEW.destinationCounty ) THEN
      select CONCAT(fieldNameList,'miscellaneous.destinationCounty~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationCounty,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationCounty,'~') into newValueList;
  END IF;

  IF (OLD.destinationState <> NEW.destinationState ) THEN
      select CONCAT(fieldNameList,'miscellaneous.destinationState~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationState,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationState,'~') into newValueList;
  END IF;


  IF (OLD.extraStopYN <> NEW.extraStopYN ) THEN
      select CONCAT(fieldNameList,'miscellaneous.extraStopYN~') into fieldNameList;
      select CONCAT(oldValueList,OLD.extraStopYN,'~') into oldValueList;
      select CONCAT(newValueList,NEW.extraStopYN,'~') into newValueList;
  END IF;

  IF ((OLD.mile <> NEW.mile) or (OLD.mile is null and NEW.mile is not null)
      or (OLD.mile is not null and NEW.mile is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.mile~') into fieldNameList;
        IF(OLD.mile is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.mile is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.mile is not null) then
            select CONCAT(oldValueList,OLD.mile,'~') into oldValueList;
        END IF;
        IF(NEW.mile is not null) then
            select CONCAT(newValueList,NEW.mile,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.ratePSTG <> NEW.ratePSTG) or (OLD.ratePSTG is null and NEW.ratePSTG is not null)
      or (OLD.ratePSTG is not null and NEW.ratePSTG is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.ratePSTG~') into fieldNameList;
        IF(OLD.ratePSTG is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.ratePSTG is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.ratePSTG is not null) then
            select CONCAT(oldValueList,OLD.ratePSTG,'~') into oldValueList;
        END IF;
        IF(NEW.ratePSTG is not null) then
            select CONCAT(newValueList,NEW.ratePSTG,'~') into newValueList;
        END IF;
    END IF;
  IF ((OLD.entitleAutoWeight <> NEW.entitleAutoWeight) or (OLD.entitleAutoWeight is null and NEW.entitleAutoWeight is not null)
      or (OLD.entitleAutoWeight is not null and NEW.entitleAutoWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.entitleAutoWeight~') into fieldNameList;
        IF(OLD.entitleAutoWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.entitleAutoWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.entitleAutoWeight is not null) then
            select CONCAT(oldValueList,OLD.entitleAutoWeight,'~') into oldValueList;
        END IF;
        IF(NEW.entitleAutoWeight is not null) then
            select CONCAT(newValueList,NEW.entitleAutoWeight,'~') into newValueList;
        END IF;
    END IF;


  IF ((OLD.actualAutoWeight <> NEW.actualAutoWeight) or (OLD.actualAutoWeight is null and NEW.actualAutoWeight is not null)
      or (OLD.actualAutoWeight is not null and NEW.actualAutoWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualAutoWeight~') into fieldNameList;
        IF(OLD.actualAutoWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualAutoWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualAutoWeight is not null) then
            select CONCAT(oldValueList,OLD.actualAutoWeight,'~') into oldValueList;
        END IF;
        IF(NEW.actualAutoWeight is not null) then
            select CONCAT(newValueList,NEW.actualAutoWeight,'~') into newValueList;
        END IF;
    END IF;



  IF ((OLD.estimatedAutoWeight <> NEW.estimatedAutoWeight) or (OLD.estimatedAutoWeight is null and NEW.estimatedAutoWeight is not null)
      or (OLD.estimatedAutoWeight is not null and NEW.estimatedAutoWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.estimatedAutoWeight~') into fieldNameList;
        IF(OLD.estimatedAutoWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimatedAutoWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimatedAutoWeight is not null) then
            select CONCAT(oldValueList,OLD.estimatedAutoWeight,'~') into oldValueList;
        END IF;
        IF(NEW.estimatedAutoWeight is not null) then
            select CONCAT(newValueList,NEW.estimatedAutoWeight,'~') into newValueList;
        END IF;
    END IF;
  IF ((OLD.estimatedRevenue <> NEW.estimatedRevenue) or (OLD.estimatedRevenue is null and NEW.estimatedRevenue is not null)
      or (OLD.estimatedRevenue is not null and NEW.estimatedRevenue is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.estimatedRevenue~') into fieldNameList;
        IF(OLD.estimatedRevenue is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimatedRevenue is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimatedRevenue is not null) then
            select CONCAT(oldValueList,OLD.estimatedRevenue,'~') into oldValueList;
        END IF;
        IF(NEW.estimatedRevenue is not null) then
            select CONCAT(newValueList,NEW.estimatedRevenue,'~') into newValueList;
        END IF;
    END IF;
  IF (OLD.warehouse <> NEW.warehouse ) THEN
      select CONCAT(fieldNameList,'miscellaneous.warehouse~') into fieldNameList;
      select CONCAT(oldValueList,OLD.warehouse,'~') into oldValueList;
      select CONCAT(newValueList,NEW.warehouse,'~') into newValueList;
  END IF;
IF (OLD.tag <> NEW.tag ) THEN
      select CONCAT(fieldNameList,'miscellaneous.tag~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tag,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tag,'~') into newValueList;
  END IF;

  IF ((OLD.actualHouseHoldGood <> NEW.actualHouseHoldGood) or (OLD.actualHouseHoldGood is null and NEW.actualHouseHoldGood is not null)
      or (OLD.actualHouseHoldGood is not null and NEW.actualHouseHoldGood is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualHouseHoldGood~') into fieldNameList;
        IF(OLD.actualHouseHoldGood is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualHouseHoldGood is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualHouseHoldGood is not null) then
            select CONCAT(oldValueList,OLD.actualHouseHoldGood,'~') into oldValueList;
        END IF;
        IF(NEW.actualHouseHoldGood is not null) then
            select CONCAT(newValueList,NEW.actualHouseHoldGood,'~') into newValueList;
        END IF;
    END IF;
   IF ((OLD.actualRevenueDollor <> NEW.actualRevenueDollor) or (OLD.actualRevenueDollor is null and NEW.actualRevenueDollor is not null)
      or (OLD.actualRevenueDollor is not null and NEW.actualRevenueDollor is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualRevenueDollor~') into fieldNameList;
        IF(OLD.actualRevenueDollor is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualRevenueDollor is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualRevenueDollor is not null) then
            select CONCAT(oldValueList,OLD.actualRevenueDollor,'~') into oldValueList;
        END IF;
        IF(NEW.actualRevenueDollor is not null) then
            select CONCAT(newValueList,NEW.actualRevenueDollor,'~') into newValueList;
        END IF;
    END IF;



  IF (OLD.agentPerformNonperform <> NEW.agentPerformNonperform ) THEN
      select CONCAT(fieldNameList,'miscellaneous.agentPerformNonperform~') into fieldNameList;
      select CONCAT(oldValueList,OLD.agentPerformNonperform,'~') into oldValueList;
      select CONCAT(newValueList,NEW.agentPerformNonperform,'~') into newValueList;
  END IF;



  IF (OLD.destination24HR <> NEW.destination24HR ) THEN
      select CONCAT(fieldNameList,'miscellaneous.destination24HR~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destination24HR,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destination24HR,'~') into newValueList;
  END IF;
  IF (OLD.origin24Hr <> NEW.origin24Hr ) THEN
      select CONCAT(fieldNameList,'miscellaneous.origin24Hr~') into fieldNameList;
      select CONCAT(oldValueList,OLD.origin24Hr,'~') into oldValueList;
      select CONCAT(newValueList,NEW.origin24Hr,'~') into newValueList;
  END IF;


  IF (OLD.vanLinePickupNumber <> NEW.vanLinePickupNumber ) THEN
      select CONCAT(fieldNameList,'miscellaneous.vanLinePickupNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanLinePickupNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanLinePickupNumber,'~') into newValueList;
  END IF;



  IF ((date_format(OLD.shipmentRegistrationDate,'%Y-%m-%d') <> date_format(NEW.shipmentRegistrationDate,'%Y-%m-%d')) or (date_format(OLD.shipmentRegistrationDate,'%Y-%m-%d') is null and date_format(NEW.shipmentRegistrationDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.shipmentRegistrationDate,'%Y-%m-%d') is not null and date_format(NEW.shipmentRegistrationDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.shipmentRegistrationDate~') into fieldNameList;
          IF(OLD.shipmentRegistrationDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.shipmentRegistrationDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.shipmentRegistrationDate is not null) THEN
        	select CONCAT(oldValueList,OLD.shipmentRegistrationDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.shipmentRegistrationDate is not null) THEN
        	select CONCAT(newValueList,NEW.shipmentRegistrationDate,'~') into newValueList;
      	END IF;
  END IF;

  IF ((date_format(OLD.transDocument,'%Y-%m-%d') <> date_format(NEW.transDocument,'%Y-%m-%d')) or (date_format(OLD.transDocument,'%Y-%m-%d') is null and date_format(NEW.transDocument,'%Y-%m-%d') is not null)
      or (date_format(OLD.transDocument,'%Y-%m-%d') is not null and date_format(NEW.transDocument,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.transDocument~') into fieldNameList;
          IF(OLD.transDocument is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.transDocument is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.transDocument is not null) THEN
        	select CONCAT(oldValueList,OLD.transDocument,'~') into oldValueList;
     	END IF;
      	IF(NEW.transDocument is not null) THEN
        	select CONCAT(newValueList,NEW.transDocument,'~') into newValueList;
      	END IF;
  END IF;

  IF ((date_format(OLD.recivedPO,'%Y-%m-%d') <> date_format(NEW.recivedPO,'%Y-%m-%d')) or (date_format(OLD.recivedPO,'%Y-%m-%d') is null and date_format(NEW.recivedPO,'%Y-%m-%d') is not null)
      or (date_format(OLD.recivedPO,'%Y-%m-%d') is not null and date_format(NEW.recivedPO,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.recivedPO~') into fieldNameList;
          IF(OLD.recivedPO is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.recivedPO is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.recivedPO is not null) THEN
        	select CONCAT(oldValueList,OLD.recivedPO,'~') into oldValueList;
     	END IF;
      	IF(NEW.recivedPO is not null) THEN
        	select CONCAT(newValueList,NEW.recivedPO,'~') into newValueList;
      	END IF;
  END IF;

   IF ((date_format(OLD.recivedHVI,'%Y-%m-%d') <> date_format(NEW.recivedHVI,'%Y-%m-%d')) or (date_format(OLD.recivedHVI,'%Y-%m-%d') is null and date_format(NEW.recivedHVI,'%Y-%m-%d') is not null)
      or (date_format(OLD.recivedHVI,'%Y-%m-%d') is not null and date_format(NEW.recivedHVI,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.recivedHVI~') into fieldNameList;
          IF(OLD.recivedHVI is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.recivedHVI is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.recivedHVI is not null) THEN
        	select CONCAT(oldValueList,OLD.recivedHVI,'~') into oldValueList;
     	END IF;
      	IF(NEW.recivedHVI is not null) THEN
        	select CONCAT(newValueList,NEW.recivedHVI,'~') into newValueList;
      	END IF;
  END IF;

IF ((date_format(OLD.confirmOriginAgent,'%Y-%m-%d') <> date_format(NEW.confirmOriginAgent,'%Y-%m-%d')) or (date_format(OLD.confirmOriginAgent,'%Y-%m-%d') is null and date_format(NEW.confirmOriginAgent,'%Y-%m-%d') is not null)
      or (date_format(OLD.confirmOriginAgent,'%Y-%m-%d') is not null and date_format(NEW.confirmOriginAgent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.confirmOriginAgent~') into fieldNameList;
          IF(OLD.confirmOriginAgent is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.confirmOriginAgent is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.confirmOriginAgent is not null) THEN
        	select CONCAT(oldValueList,OLD.confirmOriginAgent,'~') into oldValueList;
     	END IF;
      	IF(NEW.confirmOriginAgent is not null) THEN
        	select CONCAT(newValueList,NEW.confirmOriginAgent,'~') into newValueList;
      	END IF;
  END IF;

  IF ((date_format(OLD.atlasDown,'%Y-%m-%d') <> date_format(NEW.atlasDown,'%Y-%m-%d')) or (date_format(OLD.atlasDown,'%Y-%m-%d') is null and date_format(NEW.atlasDown,'%Y-%m-%d') is not null)
      or (date_format(OLD.atlasDown,'%Y-%m-%d') is not null and date_format(NEW.atlasDown,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.atlasDown~') into fieldNameList;
          IF(OLD.atlasDown is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.atlasDown is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.atlasDown is not null) THEN
        	select CONCAT(oldValueList,OLD.atlasDown,'~') into oldValueList;
     	END IF;
      	IF(NEW.atlasDown is not null) THEN
        	select CONCAT(newValueList,NEW.atlasDown,'~') into newValueList;
      	END IF;
  END IF;

IF ((date_format(OLD.printGbl,'%Y-%m-%d') <> date_format(NEW.printGbl,'%Y-%m-%d')) or (date_format(OLD.printGbl,'%Y-%m-%d') is null and date_format(NEW.printGbl,'%Y-%m-%d') is not null)
      or (date_format(OLD.printGbl,'%Y-%m-%d') is not null and date_format(NEW.printGbl,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.printGbl~') into fieldNameList;
          IF(OLD.printGbl is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.printGbl is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.printGbl is not null) THEN
        	select CONCAT(oldValueList,OLD.printGbl,'~') into oldValueList;
     	END IF;
      	IF(NEW.printGbl is not null) THEN
        	select CONCAT(newValueList,NEW.printGbl,'~') into newValueList;
      	END IF;
  END IF;

 IF ((date_format(OLD.recivedPayment,'%Y-%m-%d') <> date_format(NEW.recivedPayment,'%Y-%m-%d')) or (date_format(OLD.recivedPayment,'%Y-%m-%d') is null and date_format(NEW.recivedPayment,'%Y-%m-%d') is not null)
      or (date_format(OLD.recivedPayment,'%Y-%m-%d') is not null and date_format(NEW.recivedPayment,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.recivedPayment~') into fieldNameList;
          IF(OLD.recivedPayment is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.recivedPayment is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.recivedPayment is not null) THEN
        	select CONCAT(oldValueList,OLD.recivedPayment,'~') into oldValueList;
     	END IF;
      	IF(NEW.recivedPayment is not null) THEN
        	select CONCAT(newValueList,NEW.recivedPayment,'~') into newValueList;
      	END IF;
  END IF;

 IF ((date_format(OLD.quoteOn,'%Y-%m-%d') <> date_format(NEW.quoteOn,'%Y-%m-%d')) or (date_format(OLD.quoteOn,'%Y-%m-%d') is null and date_format(NEW.quoteOn,'%Y-%m-%d') is not null)
      or (date_format(OLD.quoteOn,'%Y-%m-%d') is not null and date_format(NEW.quoteOn,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.quoteOn~') into fieldNameList;
          IF(OLD.quoteOn is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.quoteOn is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.quoteOn is not null) THEN
        	select CONCAT(oldValueList,OLD.quoteOn,'~') into oldValueList;
     	END IF;
      	IF(NEW.quoteOn is not null) THEN
        	select CONCAT(newValueList,NEW.quoteOn,'~') into newValueList;
      	END IF;
  END IF;

  IF ((date_format(OLD.dd1840,'%Y-%m-%d') <> date_format(NEW.dd1840,'%Y-%m-%d')) or (date_format(OLD.dd1840,'%Y-%m-%d') is null and date_format(NEW.dd1840,'%Y-%m-%d') is not null)
      or (date_format(OLD.dd1840,'%Y-%m-%d') is not null and date_format(NEW.dd1840,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.dd1840~') into fieldNameList;
          IF(OLD.dd1840 is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.dd1840 is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.dd1840 is not null) THEN
        	select CONCAT(oldValueList,OLD.dd1840,'~') into oldValueList;
     	END IF;
      	IF(NEW.dd1840 is not null) THEN
        	select CONCAT(newValueList,NEW.dd1840,'~') into newValueList;
      	END IF;
  END IF;

IF ((date_format(OLD.millitarySurveyDate,'%Y-%m-%d') <> date_format(NEW.millitarySurveyDate,'%Y-%m-%d')) or (date_format(OLD.millitarySurveyDate,'%Y-%m-%d') is null and date_format(NEW.millitarySurveyDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.millitarySurveyDate,'%Y-%m-%d') is not null and date_format(NEW.millitarySurveyDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.millitarySurveyDate~') into fieldNameList;
          IF(OLD.millitarySurveyDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.millitarySurveyDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.millitarySurveyDate is not null) THEN
        	select CONCAT(oldValueList,OLD.millitarySurveyDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.millitarySurveyDate is not null) THEN
        	select CONCAT(newValueList,NEW.millitarySurveyDate,'~') into newValueList;
      	END IF;
  END IF;


IF ((date_format(OLD.millitarySurveyResults,'%Y-%m-%d') <> date_format(NEW.millitarySurveyResults,'%Y-%m-%d')) or (date_format(OLD.millitarySurveyResults,'%Y-%m-%d') is null and date_format(NEW.millitarySurveyResults,'%Y-%m-%d') is not null)
      or (date_format(OLD.millitarySurveyResults,'%Y-%m-%d') is not null and date_format(NEW.millitarySurveyResults,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.millitarySurveyResults~') into fieldNameList;
          IF(OLD.millitarySurveyResults is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.millitarySurveyResults is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.millitarySurveyResults is not null) THEN
        	select CONCAT(oldValueList,OLD.millitarySurveyResults,'~') into oldValueList;
     	END IF;
      	IF(NEW.millitarySurveyResults is not null) THEN
        	select CONCAT(newValueList,NEW.millitarySurveyResults,'~') into newValueList;
      	END IF;
  END IF;


  
IF ((date_format(OLD.millitaryDeliveryDate,'%Y-%m-%d') <> date_format(NEW.millitaryDeliveryDate,'%Y-%m-%d')) or (date_format(OLD.millitaryDeliveryDate,'%Y-%m-%d') is null and date_format(NEW.millitaryDeliveryDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.millitaryDeliveryDate,'%Y-%m-%d') is not null and date_format(NEW.millitaryDeliveryDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.millitaryDeliveryDate~') into fieldNameList;
          IF(OLD.millitaryDeliveryDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.millitaryDeliveryDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.millitaryDeliveryDate is not null) THEN
        	select CONCAT(oldValueList,OLD.millitaryDeliveryDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.millitaryDeliveryDate is not null) THEN
        	select CONCAT(newValueList,NEW.millitaryDeliveryDate,'~') into newValueList;
      	END IF;
  END IF;


IF ((date_format(OLD.millitarySitDestinationDate,'%Y-%m-%d') <> date_format(NEW.millitarySitDestinationDate,'%Y-%m-%d')) or (date_format(OLD.millitarySitDestinationDate,'%Y-%m-%d') is null and date_format(NEW.millitarySitDestinationDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.millitarySitDestinationDate,'%Y-%m-%d') is not null and date_format(NEW.millitarySitDestinationDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.millitarySitDestinationDate~') into fieldNameList;
          IF(OLD.millitarySitDestinationDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.millitarySitDestinationDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.millitarySitDestinationDate is not null) THEN
        	select CONCAT(oldValueList,OLD.millitarySitDestinationDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.millitarySitDestinationDate is not null) THEN
        	select CONCAT(newValueList,NEW.millitarySitDestinationDate,'~') into newValueList;
      	END IF;
  END IF;


  
IF ((date_format(OLD.preApprovalDate1,'%Y-%m-%d') <> date_format(NEW.preApprovalDate1,'%Y-%m-%d')) or (date_format(OLD.preApprovalDate1,'%Y-%m-%d') is null and date_format(NEW.preApprovalDate1,'%Y-%m-%d') is not null)
      or (date_format(OLD.preApprovalDate1,'%Y-%m-%d') is not null and date_format(NEW.preApprovalDate1,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.preApprovalDate1~') into fieldNameList;
          IF(OLD.preApprovalDate1 is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.preApprovalDate1 is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.preApprovalDate1 is not null) THEN
        	select CONCAT(oldValueList,OLD.preApprovalDate1,'~') into oldValueList;
     	END IF;
      	IF(NEW.preApprovalDate1 is not null) THEN
        	select CONCAT(newValueList,NEW.preApprovalDate1,'~') into newValueList;
      	END IF;
  END IF;



  
IF ((date_format(OLD.preApprovalDate2,'%Y-%m-%d') <> date_format(NEW.preApprovalDate2,'%Y-%m-%d')) or (date_format(OLD.preApprovalDate2,'%Y-%m-%d') is null and date_format(NEW.preApprovalDate2,'%Y-%m-%d') is not null)
      or (date_format(OLD.preApprovalDate2,'%Y-%m-%d') is not null and date_format(NEW.preApprovalDate2,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.preApprovalDate2~') into fieldNameList;
          IF(OLD.preApprovalDate2 is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.preApprovalDate2 is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.preApprovalDate2 is not null) THEN
        	select CONCAT(oldValueList,OLD.preApprovalDate2,'~') into oldValueList;
     	END IF;
      	IF(NEW.preApprovalDate2 is not null) THEN
        	select CONCAT(newValueList,NEW.preApprovalDate2,'~') into newValueList;
      	END IF;
  END IF;



IF ((date_format(OLD.preApprovalDate3,'%Y-%m-%d') <> date_format(NEW.preApprovalDate3,'%Y-%m-%d')) or (date_format(OLD.preApprovalDate3,'%Y-%m-%d') is null and date_format(NEW.preApprovalDate3,'%Y-%m-%d') is not null)
      or (date_format(OLD.preApprovalDate3,'%Y-%m-%d') is not null and date_format(NEW.preApprovalDate3,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.preApprovalDate3~') into fieldNameList;
          IF(OLD.preApprovalDate3 is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.preApprovalDate3 is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.preApprovalDate3 is not null) THEN
        	select CONCAT(oldValueList,OLD.preApprovalDate3,'~') into oldValueList;
     	END IF;
      	IF(NEW.preApprovalDate3 is not null) THEN
        	select CONCAT(newValueList,NEW.preApprovalDate3,'~') into newValueList;
      	END IF;
  END IF;



IF ((date_format(OLD.settledDate,'%Y-%m-%d') <> date_format(NEW.settledDate,'%Y-%m-%d')) or (date_format(OLD.settledDate,'%Y-%m-%d') is null and date_format(NEW.settledDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.settledDate,'%Y-%m-%d') is not null and date_format(NEW.settledDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.settledDate~') into fieldNameList;
          IF(OLD.settledDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.settledDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.settledDate is not null) THEN
        	select CONCAT(oldValueList,OLD.settledDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.settledDate is not null) THEN
        	select CONCAT(newValueList,NEW.settledDate,'~') into newValueList;
      	END IF;
  END IF;


  

IF ((date_format(OLD.assignDate,'%Y-%m-%d') <> date_format(NEW.assignDate,'%Y-%m-%d')) or (date_format(OLD.assignDate,'%Y-%m-%d') is null and date_format(NEW.assignDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.assignDate,'%Y-%m-%d') is not null and date_format(NEW.assignDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.assignDate~') into fieldNameList;
          IF(OLD.assignDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.assignDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.assignDate is not null) THEN
        	select CONCAT(oldValueList,OLD.assignDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.assignDate is not null) THEN
        	select CONCAT(newValueList,NEW.assignDate,'~') into newValueList;
      	END IF;
  END IF;


IF ((date_format(OLD.millitaryActualWeightDate,'%Y-%m-%d') <> date_format(NEW.millitaryActualWeightDate,'%Y-%m-%d')) or (date_format(OLD.millitaryActualWeightDate,'%Y-%m-%d') is null and date_format(NEW.millitaryActualWeightDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.millitaryActualWeightDate,'%Y-%m-%d') is not null and date_format(NEW.millitaryActualWeightDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.millitaryActualWeightDate~') into fieldNameList;
          IF(OLD.millitaryActualWeightDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.millitaryActualWeightDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.millitaryActualWeightDate is not null) THEN
        	select CONCAT(oldValueList,OLD.millitaryActualWeightDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.millitaryActualWeightDate is not null) THEN
        	select CONCAT(newValueList,NEW.millitaryActualWeightDate,'~') into newValueList;
      	END IF;
  END IF;

IF ((date_format(OLD.originDocsToBase,'%Y-%m-%d') <> date_format(NEW.originDocsToBase,'%Y-%m-%d')) or (date_format(OLD.originDocsToBase,'%Y-%m-%d') is null and date_format(NEW.originDocsToBase,'%Y-%m-%d') is not null)
      or (date_format(OLD.originDocsToBase,'%Y-%m-%d') is not null and date_format(NEW.originDocsToBase,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.originDocsToBase~') into fieldNameList;
          IF(OLD.originDocsToBase is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.originDocsToBase is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.originDocsToBase is not null) THEN
        	select CONCAT(oldValueList,OLD.originDocsToBase,'~') into oldValueList;
     	END IF;
      	IF(NEW.originDocsToBase is not null) THEN
        	select CONCAT(newValueList,NEW.originDocsToBase,'~') into newValueList;
      	END IF;
  END IF;

  
IF ((date_format(OLD.actualPickUp,'%Y-%m-%d') <> date_format(NEW.actualPickUp,'%Y-%m-%d')) or (date_format(OLD.actualPickUp,'%Y-%m-%d') is null and date_format(NEW.actualPickUp,'%Y-%m-%d') is not null)
      or (date_format(OLD.actualPickUp,'%Y-%m-%d') is not null and date_format(NEW.actualPickUp,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'miscellaneous.actualPickUp~') into fieldNameList;
          IF(OLD.actualPickUp is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.actualPickUp is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.actualPickUp is not null) THEN
        	select CONCAT(oldValueList,OLD.actualPickUp,'~') into oldValueList;
     	END IF;
      	IF(NEW.actualPickUp is not null) THEN
        	select CONCAT(newValueList,NEW.actualPickUp,'~') into newValueList;
      	END IF;
  END IF;

 IF ((OLD.rwghGross <> NEW.rwghGross) or (OLD.rwghGross is null and NEW.rwghGross is not null)
      or (OLD.rwghGross is not null and NEW.rwghGross is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.rwghGross~') into fieldNameList;
        IF(OLD.rwghGross is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rwghGross is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rwghGross is not null) then
            select CONCAT(oldValueList,OLD.rwghGross,'~') into oldValueList;
        END IF;
        IF(NEW.rwghGross is not null) then
            select CONCAT(newValueList,NEW.rwghGross,'~') into newValueList;
        END IF;
    END IF;
    
 IF ((OLD.rwghTare <> NEW.rwghTare) or (OLD.rwghTare is null and NEW.rwghTare is not null)
      or (OLD.rwghTare is not null and NEW.rwghTare is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.rwghTare~') into fieldNameList;
        IF(OLD.rwghTare is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rwghTare is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rwghTare is not null) then
            select CONCAT(oldValueList,OLD.rwghTare,'~') into oldValueList;
        END IF;
        IF(NEW.rwghTare is not null) then
            select CONCAT(newValueList,NEW.rwghTare,'~') into newValueList;
        END IF;
    END IF;

IF ((OLD.rwghNet <> NEW.rwghNet) or (OLD.rwghNet is null and NEW.rwghNet is not null)
      or (OLD.rwghNet is not null and NEW.rwghNet is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.rwghNet~') into fieldNameList;
        IF(OLD.rwghNet is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rwghNet is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rwghNet is not null) then
            select CONCAT(oldValueList,OLD.rwghNet,'~') into oldValueList;
        END IF;
        IF(NEW.rwghNet is not null) then
            select CONCAT(newValueList,NEW.rwghNet,'~') into newValueList;
        END IF;
    END IF;
 	IF ((OLD.actualBoat <> NEW.actualBoat) or (OLD.actualBoat is null and NEW.actualBoat is not null)
      or (OLD.actualBoat is not null and NEW.actualBoat is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.actualBoat~') into fieldNameList;
        IF(OLD.actualBoat is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualBoat is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualBoat is not null) then
            select CONCAT(oldValueList,OLD.actualBoat,'~') into oldValueList;
        END IF;
        IF(NEW.actualBoat is not null) then
            select CONCAT(newValueList,NEW.actualBoat,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.trailerVolumeWeight <> NEW.trailerVolumeWeight) or (OLD.trailerVolumeWeight is null and NEW.trailerVolumeWeight is not null)
      or (OLD.trailerVolumeWeight is not null and NEW.trailerVolumeWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.trailerVolumeWeight~') into fieldNameList;
        IF(OLD.trailerVolumeWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.trailerVolumeWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.trailerVolumeWeight is not null) then
            select CONCAT(oldValueList,OLD.trailerVolumeWeight,'~') into oldValueList;
        END IF;
        IF(NEW.trailerVolumeWeight is not null) then
            select CONCAT(newValueList,NEW.trailerVolumeWeight,'~') into newValueList;
        END IF;
    END IF;




  IF (OLD.gate <> NEW.gate ) THEN
      select CONCAT(fieldNameList,'miscellaneous.gate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gate,'~') into newValueList;
  END IF;


  IF (OLD.trailer <> NEW.trailer ) THEN
      select CONCAT(fieldNameList,'miscellaneous.trailer~') into fieldNameList;
      select CONCAT(oldValueList,OLD.trailer,'~') into oldValueList;
      select CONCAT(newValueList,NEW.trailer,'~') into newValueList;
  END IF;


  IF (OLD.pendingNumber <> NEW.pendingNumber ) THEN
      select CONCAT(fieldNameList,'miscellaneous.pendingNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.pendingNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.pendingNumber,'~') into newValueList;
  END IF;
  IF ((OLD.vanLineCODAmount <> NEW.vanLineCODAmount) or (OLD.vanLineCODAmount is null and NEW.vanLineCODAmount is not null)
      or (OLD.vanLineCODAmount is not null and NEW.vanLineCODAmount is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.vanLineCODAmount~') into fieldNameList;
        IF(OLD.vanLineCODAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.vanLineCODAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.vanLineCODAmount is not null) then
            select CONCAT(oldValueList,OLD.vanLineCODAmount,'~') into oldValueList;
        END IF;
        IF(NEW.vanLineCODAmount is not null) then
            select CONCAT(newValueList,NEW.vanLineCODAmount,'~') into newValueList;
        END IF;
    END IF;
  IF (OLD.weightTicket <> NEW.weightTicket ) THEN
      select CONCAT(fieldNameList,'miscellaneous.weightTicket~') into fieldNameList;
      select CONCAT(oldValueList,OLD.weightTicket,'~') into oldValueList;
      select CONCAT(newValueList,NEW.weightTicket,'~') into newValueList;
  END IF;


  IF (OLD.domesticInstruction <> NEW.domesticInstruction ) THEN
      select CONCAT(fieldNameList,'miscellaneous.domesticInstruction~') into fieldNameList;
      select CONCAT(oldValueList,OLD.domesticInstruction,'~') into oldValueList;
      select CONCAT(newValueList,NEW.domesticInstruction,'~') into newValueList;
  END IF;



  IF (OLD.peakRate <> NEW.peakRate ) THEN
      select CONCAT(fieldNameList,'miscellaneous.peakRate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.peakRate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.peakRate,'~') into newValueList;
  END IF;

IF ((OLD.chargeableGrossWeight <> NEW.chargeableGrossWeight) or (OLD.chargeableGrossWeight is null and NEW.chargeableGrossWeight is not null)
      or (OLD.chargeableGrossWeight is not null and NEW.chargeableGrossWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.chargeableGrossWeight~') into fieldNameList;
        IF(OLD.chargeableGrossWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.chargeableGrossWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.chargeableGrossWeight is not null) then
            select CONCAT(oldValueList,OLD.chargeableGrossWeight,'~') into oldValueList;
        END IF;
        IF(NEW.chargeableGrossWeight is not null) then
            select CONCAT(newValueList,NEW.chargeableGrossWeight,'~') into newValueList;
        END IF;
    END IF;



  IF ((OLD.chargeableNetWeight <> NEW.chargeableNetWeight) or (OLD.chargeableNetWeight is null and NEW.chargeableNetWeight is not null)
      or (OLD.chargeableNetWeight is not null and NEW.chargeableNetWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.chargeableNetWeight~') into fieldNameList;
        IF(OLD.chargeableNetWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.chargeableNetWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.chargeableNetWeight is not null) then
            select CONCAT(oldValueList,OLD.chargeableNetWeight,'~') into oldValueList;
        END IF;
        IF(NEW.chargeableNetWeight is not null) then
            select CONCAT(newValueList,NEW.chargeableNetWeight,'~') into newValueList;
        END IF;
    END IF;


  IF ((OLD.chargeableTareWeight <> NEW.chargeableTareWeight) or (OLD.chargeableTareWeight is null and NEW.chargeableTareWeight is not null)
      or (OLD.chargeableTareWeight is not null and NEW.chargeableTareWeight is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.chargeableTareWeight~') into fieldNameList;
        IF(OLD.chargeableTareWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.chargeableTareWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.chargeableTareWeight is not null) then
            select CONCAT(oldValueList,OLD.chargeableTareWeight,'~') into oldValueList;
        END IF;
        IF(NEW.chargeableTareWeight is not null) then
            select CONCAT(newValueList,NEW.chargeableTareWeight,'~') into newValueList;
        END IF;
    END IF;

	IF ((OLD.rwghCubicFeet <> NEW.rwghCubicFeet) or (OLD.rwghCubicFeet is null and NEW.rwghCubicFeet is not null)
      or (OLD.rwghCubicFeet is not null and NEW.rwghCubicFeet is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.rwghCubicFeet~') into fieldNameList;
        IF(OLD.rwghCubicFeet is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rwghCubicFeet is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rwghCubicFeet is not null) then
            select CONCAT(oldValueList,OLD.rwghCubicFeet,'~') into oldValueList;
        END IF;
        IF(NEW.rwghCubicFeet is not null) then
            select CONCAT(newValueList,NEW.rwghCubicFeet,'~') into newValueList;
        END IF;
    END IF;



  IF ((OLD.rwghNetCubicFeet <> NEW.rwghNetCubicFeet) or (OLD.rwghNetCubicFeet is null and NEW.rwghNetCubicFeet is not null)
      or (OLD.rwghNetCubicFeet is not null and NEW.rwghNetCubicFeet is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.rwghNetCubicFeet~') into fieldNameList;
        IF(OLD.rwghNetCubicFeet is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rwghNetCubicFeet is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rwghNetCubicFeet is not null) then
            select CONCAT(oldValueList,OLD.rwghNetCubicFeet,'~') into oldValueList;
        END IF;
        IF(NEW.rwghNetCubicFeet is not null) then
            select CONCAT(newValueList,NEW.rwghNetCubicFeet,'~') into newValueList;
        END IF;
    END IF;


IF ((OLD.chargeableCubicFeet <> NEW.chargeableCubicFeet) or (OLD.chargeableCubicFeet is null and NEW.chargeableCubicFeet is not null)
      or (OLD.chargeableCubicFeet is not null and NEW.chargeableCubicFeet is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.chargeableCubicFeet~') into fieldNameList;
        IF(OLD.chargeableCubicFeet is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.chargeableCubicFeet is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.chargeableCubicFeet is not null) then
            select CONCAT(oldValueList,OLD.chargeableCubicFeet,'~') into oldValueList;
        END IF;
        IF(NEW.chargeableCubicFeet is not null) then
            select CONCAT(newValueList,NEW.chargeableCubicFeet,'~') into newValueList;
        END IF;
    END IF;



 IF ((OLD.chargeableNetCubicFeet <> NEW.chargeableNetCubicFeet) or (OLD.chargeableNetCubicFeet is null and NEW.chargeableNetCubicFeet is not null)
      or (OLD.chargeableNetCubicFeet is not null and NEW.chargeableNetCubicFeet is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.chargeableNetCubicFeet~') into fieldNameList;
        IF(OLD.chargeableNetCubicFeet is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.chargeableNetCubicFeet is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.chargeableNetCubicFeet is not null) then
            select CONCAT(oldValueList,OLD.chargeableNetCubicFeet,'~') into oldValueList;
        END IF;
        IF(NEW.chargeableNetCubicFeet is not null) then
            select CONCAT(newValueList,NEW.chargeableNetCubicFeet,'~') into newValueList;
        END IF;
    END IF;



  IF (OLD.unit1 <> NEW.unit1 ) THEN
      select CONCAT(fieldNameList,'miscellaneous.unit1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.unit1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.unit1,'~') into newValueList;
  END IF;


  IF (OLD.unit2 <> NEW.unit2 ) THEN
      select CONCAT(fieldNameList,'miscellaneous.unit2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.unit2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.unit2,'~') into newValueList;
  END IF;



 

  IF (OLD.driverId <> NEW.driverId ) THEN
      select CONCAT(fieldNameList,'miscellaneous.driverId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.driverId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.driverId,'~') into newValueList;
  END IF;

  IF (OLD.driverName <> NEW.driverName ) THEN
      select CONCAT(fieldNameList,'miscellaneous.driverName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.driverName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.driverName,'~') into newValueList;

      update sodashboard set driver=NEW.driverName where serviceorderid=NEW.id;

  END IF;
  
  IF (OLD.driverCell <> NEW.driverCell ) THEN
      select CONCAT(fieldNameList,'miscellaneous.driverCell~') into fieldNameList;
      select CONCAT(oldValueList,OLD.driverCell,'~') into oldValueList;
      select CONCAT(newValueList,NEW.driverCell,'~') into newValueList;
  END IF;

    IF (OLD.carrier <> NEW.carrier ) THEN
      select CONCAT(fieldNameList,'miscellaneous.carrier~') into fieldNameList;
      select CONCAT(oldValueList,OLD.carrier,'~') into oldValueList;
      select CONCAT(newValueList,NEW.carrier,'~') into newValueList;
      update sodashboard set truck=NEW.carrier where serviceorderid=NEW.id;

  END IF;
    IF (OLD.fromNonTempStorage <> NEW.fromNonTempStorage ) THEN
      select CONCAT(fieldNameList,'miscellaneous.fromNonTempStorage~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fromNonTempStorage,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fromNonTempStorage,'~') into newValueList;
  END IF;


  IF (OLD.haulerName <> NEW.haulerName ) THEN
      select CONCAT(fieldNameList,'miscellaneous.haulerName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.haulerName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.haulerName,'~') into newValueList;
  END IF;
  
  IF ((OLD.totalDiscountPercentage <> NEW.totalDiscountPercentage) or (OLD.totalDiscountPercentage is null and NEW.totalDiscountPercentage is not null)
      or (OLD.totalDiscountPercentage is not null and NEW.totalDiscountPercentage is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.totalDiscountPercentage~') into fieldNameList;
        IF(OLD.totalDiscountPercentage is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.totalDiscountPercentage is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.totalDiscountPercentage is not null) then
            select CONCAT(oldValueList,OLD.totalDiscountPercentage,'~') into oldValueList;
        END IF;
        IF(NEW.totalDiscountPercentage is not null) then
            select CONCAT(newValueList,NEW.totalDiscountPercentage,'~') into newValueList;
        END IF;
    END IF;

  IF (OLD.haulingAgentCode <> NEW.haulingAgentCode ) THEN
      select CONCAT(fieldNameList,'miscellaneous.haulingAgentCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.haulingAgentCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.haulingAgentCode,'~') into newValueList;
  END IF;

select weightunit into unit from systemdefault sd where sd.corpid=NEW.corpid;

if(unit='Kgs') then

update sodashboard set estweight=NEW.estimatedNetWeightKilo,actualweight=NEW.actualNetWeightKilo,estvolume=NEW.netEstimateCubicMtr,actVolume=NEW.netActualCubicMtr where serviceorderid=NEW.id;

end if;

if(unit='Lbs') then

update sodashboard set estweight=NEW.estimatedNetWeight,actualweight=NEW.actualNetWeight,estvolume=NEW.netEstimateCubicFeet,actVolume=NEW.netActualCubicFeet where serviceorderid=NEW.id;

end if;





IF (OLD.weaponsIncluded <> NEW.weaponsIncluded ) THEN
      select CONCAT(fieldNameList,'miscellaneous.weaponsIncluded~') into fieldNameList;
      select CONCAT(oldValueList,OLD.weaponsIncluded,'~') into oldValueList;
      select CONCAT(newValueList,NEW.weaponsIncluded,'~') into newValueList;
  END IF;  
  
  
  IF (OLD.paperWork <> NEW.paperWork ) THEN
      select CONCAT(fieldNameList,'miscellaneous.paperWork~') into fieldNameList;
      select CONCAT(oldValueList,OLD.paperWork,'~') into oldValueList;
      select CONCAT(newValueList,NEW.paperWork,'~') into newValueList;
  END IF;
  
  
IF (OLD.shortFuse <> NEW.shortFuse ) THEN
       select CONCAT(fieldNameList,'miscellaneous.shortFuse~') into fieldNameList;
       IF(OLD.shortFuse = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.shortFuse = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.shortFuse = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.shortFuse = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
  
  
   IF (OLD.preApprovalRequest1 <> NEW.preApprovalRequest1 ) THEN
      select CONCAT(fieldNameList,'miscellaneous.preApprovalRequest1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.preApprovalRequest1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.preApprovalRequest1,'~') into newValueList;
  END IF;
  
  IF (OLD.preApprovalRequest2 <> NEW.preApprovalRequest2 ) THEN
      select CONCAT(fieldNameList,'miscellaneous.preApprovalRequest2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.preApprovalRequest2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.preApprovalRequest2,'~') into newValueList;
  END IF;
  
  IF (OLD.preApprovalRequest3 <> NEW.preApprovalRequest3 ) THEN
      select CONCAT(fieldNameList,'miscellaneous.preApprovalRequest3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.preApprovalRequest3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.preApprovalRequest3,'~') into newValueList;
  END IF;
  
IF (OLD.millitaryShipment <> NEW.millitaryShipment ) THEN
      select CONCAT(fieldNameList,'miscellaneous.millitaryShipment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.millitaryShipment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.millitaryShipment,'~') into newValueList;
  END IF;  
  
  
  IF (OLD.dp3 <> NEW.dp3 ) THEN
       select CONCAT(fieldNameList,'miscellaneous.dp3~') into fieldNameList;
       IF(OLD.dp3 = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.dp3 = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.dp3 = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.dp3 = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
  
  IF (OLD.selfHaul <> NEW.selfHaul ) THEN
      select CONCAT(fieldNameList,'miscellaneous.selfHaul~') into fieldNameList;
      select CONCAT(oldValueList,OLD.selfHaul,'~') into oldValueList;
      select CONCAT(newValueList,NEW.selfHaul,'~') into newValueList;
  END IF;  
  
   IF (OLD.haulingAgentVanlineCode <> NEW.haulingAgentVanlineCode ) THEN
      select CONCAT(fieldNameList,'miscellaneous.haulingAgentVanlineCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.haulingAgentVanlineCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.haulingAgentVanlineCode,'~') into newValueList;
  END IF;  
   IF (OLD.carrierName <> NEW.carrierName ) THEN
      select CONCAT(fieldNameList,'miscellaneous.carrierName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.carrierName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.carrierName,'~') into newValueList;
  END IF;  
   IF (OLD.originScore <> NEW.originScore ) THEN
      select CONCAT(fieldNameList,'miscellaneous.originScore~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originScore,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originScore,'~') into newValueList;
  END IF;  
   IF (OLD.status <> NEW.status ) THEN
      select CONCAT(fieldNameList,'miscellaneous.status~') into fieldNameList;
      select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
      select CONCAT(newValueList,NEW.status,'~') into newValueList;
  END IF;  
  
   IF (OLD.packingDriverId <> NEW.packingDriverId ) THEN
      select CONCAT(fieldNameList,'miscellaneous.packingDriverId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.packingDriverId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.packingDriverId,'~') into newValueList;
  END IF; 
  
  
   IF (OLD.loadingDriverId <> NEW.loadingDriverId ) THEN
      select CONCAT(fieldNameList,'miscellaneous.loadingDriverId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.loadingDriverId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.loadingDriverId,'~') into newValueList;
  END IF; 
  
   IF (OLD.deliveryDriverId <> NEW.deliveryDriverId ) THEN
      select CONCAT(fieldNameList,'miscellaneous.deliveryDriverId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.deliveryDriverId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.deliveryDriverId,'~') into newValueList;
  END IF; 
  
   IF (OLD.sitDestinationDriverId <> NEW.sitDestinationDriverId ) THEN
      select CONCAT(fieldNameList,'miscellaneous.sitDestinationDriverId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitDestinationDriverId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitDestinationDriverId,'~') into newValueList;
  END IF; 
  
  IF ((OLD.loadCount <> NEW.loadCount) or (OLD.loadCount is null and NEW.loadCount is not null)
      or (OLD.loadCount is not null and NEW.loadCount is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.loadCount~') into fieldNameList;
        IF(OLD.loadCount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.loadCount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.loadCount is not null) then
            select CONCAT(oldValueList,OLD.loadCount,'~') into oldValueList;
        END IF;
        IF(NEW.loadCount is not null) then
            select CONCAT(newValueList,NEW.loadCount,'~') into newValueList;
        END IF;
    END IF;
	
	IF ((OLD.deliveryCount <> NEW.deliveryCount) or (OLD.deliveryCount is null and NEW.deliveryCount is not null)
      or (OLD.deliveryCount is not null and NEW.deliveryCount is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.deliveryCount~') into fieldNameList;
        IF(OLD.deliveryCount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.deliveryCount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.deliveryCount is not null) then
            select CONCAT(oldValueList,OLD.deliveryCount,'~') into oldValueList;
        END IF;
        IF(NEW.deliveryCount is not null) then
            select CONCAT(newValueList,NEW.deliveryCount,'~') into newValueList;
        END IF;
    END IF;
   IF (OLD.loadSignature <> NEW.loadSignature ) THEN
      select CONCAT(fieldNameList,'miscellaneous.loadSignature~') into fieldNameList;
      select CONCAT(oldValueList,OLD.loadSignature,'~') into oldValueList;
      select CONCAT(newValueList,NEW.loadSignature,'~') into newValueList;
  END IF; 
  
   IF (OLD.deliverySignature <> NEW.deliverySignature ) THEN
      select CONCAT(fieldNameList,'miscellaneous.deliverySignature~') into fieldNameList;
      select CONCAT(oldValueList,OLD.deliverySignature,'~') into oldValueList;
      select CONCAT(newValueList,NEW.deliverySignature,'~') into newValueList;
  END IF; 
  
    IF (OLD.tractorAgentVanLineCode <> NEW.tractorAgentVanLineCode ) THEN
      select CONCAT(fieldNameList,'miscellaneous.tractorAgentVanLineCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tractorAgentVanLineCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tractorAgentVanLineCode,'~') into newValueList;
  END IF; 
  
    IF (OLD.tractorAgentName <> NEW.tractorAgentName ) THEN
      select CONCAT(fieldNameList,'miscellaneous.tractorAgentName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tractorAgentName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tractorAgentName,'~') into newValueList;
  END IF; 
  
    IF (OLD.vanAgentVanLineCode <> NEW.vanAgentVanLineCode ) THEN
      select CONCAT(fieldNameList,'miscellaneous.vanAgentVanLineCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanAgentVanLineCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanAgentVanLineCode,'~') into newValueList;
  END IF; 
  
    IF (OLD.vanAgentName <> NEW.vanAgentName ) THEN
      select CONCAT(fieldNameList,'miscellaneous.vanAgentName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanAgentName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanAgentName,'~') into newValueList;
  END IF; 
  
    IF (OLD.vanName <> NEW.vanName ) THEN
      select CONCAT(fieldNameList,'miscellaneous.vanName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanName,'~') into newValueList;
  END IF; 
  
    IF (OLD.vanID <> NEW.vanID ) THEN
      select CONCAT(fieldNameList,'miscellaneous.vanID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanID,'~') into newValueList;
  END IF; 
  
    IF (OLD.tripNumber <> NEW.tripNumber ) THEN
      select CONCAT(fieldNameList,'miscellaneous.tripNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tripNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tripNumber,'~') into newValueList;
  END IF; 
  
    IF (OLD.ugwIntId <> NEW.ugwIntId ) THEN
      select CONCAT(fieldNameList,'miscellaneous.ugwIntId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ugwIntId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ugwIntId,'~') into newValueList;
  END IF; 
  
IF (OLD.g11 <> NEW.g11 ) THEN
       select CONCAT(fieldNameList,'miscellaneous.g11~') into fieldNameList;
       IF(OLD.g11 = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.g11 = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.g11 = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.g11 = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   
   
   IF ((OLD.estimatedExpense <> NEW.estimatedExpense ) or (OLD.estimatedExpense is null and NEW.estimatedExpense is not null)
      or (OLD.estimatedExpense is not null and NEW.estimatedExpense is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.estimatedExpense~') into fieldNameList;
        IF(OLD.estimatedExpense is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimatedExpense is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimatedExpense is not null) then
            select CONCAT(oldValueList,OLD.estimatedExpense,'~') into oldValueList;
        END IF;
        IF(NEW.estimatedExpense is not null) then
            select CONCAT(newValueList,NEW.estimatedExpense,'~') into newValueList;
        END IF;
    END IF;
	
	IF ((OLD.totalActualRevenue <> NEW.totalActualRevenue) or (OLD.totalActualRevenue is null and NEW.totalActualRevenue is not null)
      or (OLD.totalActualRevenue is not null and NEW.totalActualRevenue is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.totalActualRevenue~') into fieldNameList;
        IF(OLD.totalActualRevenue is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.totalActualRevenue is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.totalActualRevenue is not null) then
            select CONCAT(oldValueList,OLD.totalActualRevenue,'~') into oldValueList;
        END IF;
        IF(NEW.totalActualRevenue is not null) then
            select CONCAT(newValueList,NEW.totalActualRevenue,'~') into newValueList;
        END IF;
    END IF;
	
	IF ((OLD.discount <> NEW.discount) or (OLD.discount is null and NEW.discount is not null)
      or (OLD.discount is not null and NEW.discount is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.discount~') into fieldNameList;
        IF(OLD.discount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.discount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.discount is not null) then
            select CONCAT(oldValueList,OLD.discount,'~') into oldValueList;
        END IF;
        IF(NEW.discount is not null) then
            select CONCAT(newValueList,NEW.discount,'~') into newValueList;
        END IF;
    END IF;
	IF ((OLD.entitleTareWeightKilo <> NEW.entitleTareWeightKilo) or (OLD.entitleTareWeightKilo is null and NEW.entitleTareWeightKilo is not null)
      or (OLD.entitleTareWeightKilo is not null and NEW.entitleTareWeightKilo is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.entitleTareWeightKilo~') into fieldNameList;
        IF(OLD.entitleTareWeightKilo is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.entitleTareWeightKilo is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.entitleTareWeightKilo is not null) then
            select CONCAT(oldValueList,OLD.entitleTareWeightKilo,'~') into oldValueList;
        END IF;
        IF(NEW.entitleTareWeightKilo is not null) then
            select CONCAT(newValueList,NEW.entitleTareWeightKilo,'~') into newValueList;
        END IF;
    END IF;
    IF (OLD.equipment <> NEW.equipment ) THEN
      select CONCAT(fieldNameList,'miscellaneous.equipment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.equipment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.equipment,'~') into newValueList;
  END IF;
  IF (OLD.excessWeightBilling <> NEW.excessWeightBilling ) THEN
      select CONCAT(fieldNameList,'miscellaneous.excessWeightBilling~') into fieldNameList;
      select CONCAT(oldValueList,OLD.excessWeightBilling,'~') into oldValueList;
      select CONCAT(newValueList,NEW.excessWeightBilling,'~') into newValueList;
  END IF;
	


if(date_format(OLD.updatedOn,'%Y-%m-%d %H:%i:%s') <> date_format(NEW.updatedOn,'%Y-%m-%d %H:%i:%s'))then
		CALL add_tblHistory (OLD.id,"miscellaneous", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
	end if;
if(date_format(OLD.updatedOn,'%Y-%m-%d %H:%i:%s') = date_format(NEW.updatedOn,'%Y-%m-%d %H:%i:%s'))then
		CALL add_tblHistory (OLD.id,"miscellaneous", fieldNameList, oldValueList, newValueList, 'System', OLD.corpID, now());
	end if;

END
$$