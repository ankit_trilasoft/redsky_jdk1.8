package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.MssGrid;

public interface MssGridDao extends GenericDao<MssGrid, Long>{
	
	public List findRecord(Long id, String sessionCorpID);
	public void deleteDataOfInventoryData(String inventoryFlag);

}
