<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags"%>  

<head>
    <title><fmt:message key="historyExchangeRate.title"/></title>
    <meta name="heading" content="<fmt:message key='historyExchangeRate.heading'/>"/>  

<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
   	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery.min.js"></script>
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.min.js"></script>  
</head>
<!-- Starting of form tag --> 
<s:form id="exchangeForm" name="exchangeForm" action="saveexchangeRate1" method="post" validate="true"> 
<s:hidden name="exchangeRate1.id"/> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.exchangeRate1FormList' }">
    <c:redirect url="/exchangeRate1FormList.html"/>
</c:when>

</c:choose>
</c:if>
<div id="layer3" style="width: 100%">
<div id="newmnav" onkeydown="changeStatus()">
		  <ul>
		    <li id="newmnav1"  style="background:#FFF "><a class="current"><span>History Exchange Rate Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<%-- <li><a onclick="setReturnString('gototab.exchangeRate1FormList');return autoSave();"><span>Exchange Rate List</span></a></li> --%>
			<li> <a href="historyExchangeList.html"><span>History&nbsp;Exchange&nbsp;List</span></a></li>
		  </ul>
		</div>
	
		<div class="spn">&nbsp;</div>
	
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:2px;" ><span></span></div>
   <div class="center-content">		
<table class="" cellspacing="0" cellpadding="0" border="0">
		<tbody>
			<tr>
				<td>
			
				<table class="detailTabLabel" cellspacing="2" cellpadding="2" border="0">
					<tbody>
						<tr>
							<td align="right" class="listwhitetext" style="height:2px;"></td>	
						</tr>
						<tr>						
							<td class="listwhitetext" align="right">Currency<font color="red" size="2">*</font></td>							
							<td>
							<s:select name="exchangeRate1.currency" list="%{currency}" cssClass="list-menu" cssStyle="width:130px" headerKey="" headerValue=""  tabindex="1"/>							
							</td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td class="listwhitetext"  align="right">Base Currency</td>							
							<td class="listwhitetext" align="left"><s:textfield key="exchangeRate1.baseCurrency"  required="false"  cssClass="input-text" tabindex="2"/></td>							
						</tr>
						
						<tr>						
						    <td class="listwhitetext" align="right" style="width:150px" >Official Rate</td>						
							<td class="listwhitetext"><s:textfield key="exchangeRate1.officialRate" cssStyle="text-align:right" onkeydown="return onlyNumsAllowed(event)" required="false" onchange="setOfficialRate();changeStatus();"   cssClass="input-text" tabindex="3"/></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td class="listwhitetext" align="right" style="width:150px" >Base Currency Rate</td>							
							<td class="listwhitetext"><s:textfield key="exchangeRate1.baseCurrencyRate" cssStyle="text-align:right" onkeydown="return onlyNumsAllowed(event)" required="false" onchange="setCurrencyBase();changeStatus();"   cssClass="input-text" tabindex="4"/></td>
							
						</tr>
						
						<tr>								
							<td class="listwhitetext" align="right">Currency Base Rate</td>						
							<td class="listwhitetext"><s:textfield key="exchangeRate1.currencyBaseRate" cssStyle="text-align:right" onkeydown="return onlyNumsAllowed(event)" required="false" onchange="setBaseCurrency();changeStatus();" cssClass="input-text" tabindex="5"/></td>							
						    <td align="right" class="listwhitetext" style="width:10px"></td>				
							<td class="listwhitetext" align="right">Value Date</td>														
							<c:if test="${not empty exchangeRate1.valueDate}">
								<s:text id="exchangeRate1valueDate" name="${FormDateValue}"><s:param name="value" value="exchangeRate1.valueDate"/></s:text>
								<td><s:textfield id="valueDate" cssClass="input-text" name="exchangeRate1.valueDate" value="%{exchangeRate1valueDate}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event, this)" onselect="changeStatus();" readonly="false" />
								</td>
							</c:if>
							<c:if test="${empty exchangeRate1.valueDate}">
								<td><s:textfield id="valueDate" cssClass="input-text" name="exchangeRate1.valueDate" cssStyle="width:65px" maxlength="11" readonly="false" onselect="changeStatus();" />
								</td>
							</c:if>
                            </td>							
							<td align="right" class="listwhitetext" style="width:10px"></td>							
							<td>							
						</tr>
						
						<tr>
						<td align="right" class="listwhitetext">Margin</td>										
				        <td align="left"><s:textfield name="exchangeRate1.marginApplied" required="true" cssClass="input-text" size="3" maxlength="9" onkeydown="return onlyNumsAllowed(event)" tabindex="6"/><b>%</b></td>	
				        <td align="right" class="listwhitetext" style="width:10px"></td>
				        <td align="right" class="listwhitetext" width="150">Manual Update </td>
		                <td width="70px"><s:select cssClass="list-menu" name="exchangeRate1.manualUpdate" list="%{yesno}" cssStyle="width:67px;" onchange="changeStatus();" tabindex="7"/></td>
					    </tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:10px;height:10px;"></td>	
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
			<table border="0" width="700px">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>							
							<td valign="top">							
							</td>
							<td style="width:140px">
							<fmt:formatDate var="exchangeRate1CreatedOnFormattedValue" value="${exchangeRate1.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="exchangeRate1.createdOn" value="${exchangeRate1CreatedOnFormattedValue}"/>
							<fmt:formatDate value="${exchangeRate1.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty exchangeRate1.id}">
								<s:hidden name="exchangeRate1.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{exchangeRate1.createdBy}"/></td>
							</c:if>
							<c:if test="${empty exchangeRate1.id}">
								<s:hidden name="exchangeRate1.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:80px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="exchangeRate1updatedOnFormattedValue" value="${exchangeRate1.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="exchangeRate1.updatedOn" value="${exchangeRate1updatedOnFormattedValue}"/>
							<td style="width:140px"><fmt:formatDate value="${exchangeRate1.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty exchangeRate1.id}">
								<s:hidden name="exchangeRate1.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{exchangeRate1.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty exchangeRate1.id}">
								<s:hidden name="exchangeRate1.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>

</s:form>
<!-- End of form tag  --> 


<script type="text/javascript">
document.getElementById('exchangeForm').readOnly = true;
$('#exchangeForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
$('#exchangeForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper');  
</script> 

