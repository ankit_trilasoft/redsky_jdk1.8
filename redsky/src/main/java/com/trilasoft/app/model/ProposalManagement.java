package com.trilasoft.app.model;
import java.math.BigDecimal;
import java.util.Date;
import javax.servlet.Servlet;
import org.appfuse.model.BaseObject;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/*
Generate your getters and setters using your favorite IDE: 
In Eclipse:
Right-click -> Source -> Generate Getters and Setters
*/
@Entity
@Table(name="proposalmanagement")

public class ProposalManagement extends BaseObject{
	
		 /**
	     * 
	    */
		//private static final long serialVersionUID = 1L;

		private Long id;
	    private String createdBy;
	    private Date createdOn;
	    private String modifyBy;
	    private Date modifyOn;
	    private String corpId;
	    private String module;
	    private String clientRequestor;
	    private String clientApprover;
	    private String subject;
	    private BigDecimal amountProposed= new BigDecimal(0);
	    private String projectManager;
	    private Boolean isFixed=new Boolean(true);
	    private String description;
	    private String status;
	    private String fileName;
	    private String location;
	    private String ticketNo;
	    private Date initiationDate;
	    private Date approvalDate;
	    private String currency;
	    private String approvalFileName;
	    private String approvalFileLocation;
	    private BigDecimal billTillDate=new BigDecimal("0.00");
	    private Date releaseDate;
	    
	    @Override
	    public String toString(){
	    	return new ToStringBuilder(this).append("id", id)
	    			.append("createdBy", createdBy)
	    			.append("createdOn",createdOn)
	    			.append("modifyBy",modifyBy)
	    			.append("modifyOn",modifyOn)
	    			.append("corpId",corpId)
	    			.append("module",module)
	    			.append("clientRequestor",clientRequestor)
	    			.append("clientApprover",clientApprover)
	    			.append("subject",subject)
	    			.append("amountProposed",amountProposed)
	    			.append("projectManager",projectManager)
	    			.append("isFixed",isFixed)
	    			.append("description",description)
	    			.append("status",status)
	    			.append("fileName",fileName)
	    			.append("location",location)
	    			.append("ticketNo",ticketNo)
	    			.append("initiationDate",initiationDate)
	    			.append("approvalDate",approvalDate)
	    			.append("currency",currency)
	    			.append("approvalFileName",approvalFileName)
	    			.append("approvalFileLocation",approvalFileLocation)
	    			.append("billTillDate",billTillDate)
	    			.append("releaseDate",releaseDate)
	    			.toString();
	    	
	    }
	    
	    @Override
	    public boolean equals(final Object other) {
	    	if (!(other instanceof ProposalManagement))
				return false;
	    	ProposalManagement castOther = (ProposalManagement) other;
			return new EqualsBuilder().append(id, castOther.id)
					.append(createdBy, castOther.createdBy)
	    			.append(createdOn,castOther.createdOn)
	    			.append(modifyBy,castOther.modifyBy)
	    			.append(modifyOn,castOther.modifyOn)
	    			.append(corpId,castOther.corpId)
	    			.append(module,castOther.module)
	    			.append(clientRequestor,castOther.clientRequestor)
	    			.append(clientApprover,castOther.clientApprover)
	    			.append(subject,castOther.subject)
	    			.append(amountProposed,castOther.amountProposed)
	    			.append(projectManager,castOther.projectManager)
	    			.append(isFixed,castOther.isFixed)
	    			.append(description,castOther.description)
	    			.append(status,castOther.status)
	    			.append(fileName,castOther.fileName)
	    			.append(location,castOther.location)
	    			.append(ticketNo,castOther.ticketNo)
	    			.append(initiationDate,castOther.initiationDate)
	    			.append(approvalDate,castOther.approvalDate)
	    			.append(currency,castOther.currency)
	    			.append(approvalFileName,castOther.approvalFileName)
	    			.append(approvalFileLocation,castOther.approvalFileLocation)
	    			.append(billTillDate, castOther.billTillDate)
	    			.append(releaseDate, castOther.releaseDate)
	    			.isEquals();
	    }
	    
	    @Override
	    public int hashCode(){
	    	return new HashCodeBuilder().append(id).append(createdBy)
	    			.append(createdOn).append(modifyBy).append(modifyOn)
	    			.append(corpId).append(module).append(clientRequestor)
	    			.append(clientApprover).append(subject).append(amountProposed)
	    			.append(projectManager).append(isFixed).append(description)
	    			.append(status).append(fileName).append(location).append(ticketNo)
	    			.append(initiationDate).append(approvalDate).append(currency).append(approvalFileName)
	    			.append(approvalFileLocation).append(billTillDate).append(releaseDate)
	    			.toHashCode();
	    }
	    
	    @Id @GeneratedValue(strategy = GenerationType.AUTO)
	    
		public Long getId() {
			return id;
		}
	    @Column
		public String getCreatedBy() {
			return createdBy;
		}
	    @Column
		public Date getCreatedOn() {
			return createdOn;
		}
	    @Column
		public String getModifyBy() {
			return modifyBy;
		}
	    @Column
		public Date getModifyOn() {
			return modifyOn;
		}
	    @Column
		public String getCorpId() {
			return corpId;
		}
	    @Column
		public String getModule() {
			return module;
		}
	    @Column
		public String getClientRequestor() {
			return clientRequestor;
		}
	    @Column
		public String getClientApprover() {
			return clientApprover;
		}
	    @Column
		public String getSubject() {
			return subject;
		}
	    @Column
		public BigDecimal getAmountProposed() {
			return amountProposed;
		}
	    @Column
		public String getProjectManager() {
			return projectManager;
		}
	    @Column
		public Boolean getIsFixed() {
			return isFixed;
		}
	    @Column
		public String getDescription() {
			return description;
		}
	    @Column
		public String getStatus() {
			return status;
		}
	    @Column
		public String getFileName() {
			return fileName;
		}
	    @Column
		public String getLocation() {
			return location;
		}
	    @Column
		public String getTicketNo() {
			return ticketNo;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}
		public void setModifyBy(String modifyBy) {
			this.modifyBy = modifyBy;
		}
		public void setModifyOn(Date modifyOn) {
			this.modifyOn = modifyOn;
		}
		public void setCorpId(String corpId) {
			this.corpId = corpId;
		}
		public void setModule(String module) {
			this.module = module;
		}
		public void setClientRequestor(String clientRequestor) {
			this.clientRequestor = clientRequestor;
		}
		public void setClientApprover(String clientApprover) {
			this.clientApprover = clientApprover;
		}
		public void setSubject(String subject) {
			this.subject = subject;
		}
		public void setAmountProposed(BigDecimal amountProposed) {
			this.amountProposed = amountProposed;
		}
		public void setProjectManager(String projectManager) {
			this.projectManager = projectManager;
		}
		public void setIsFixed(Boolean isFixed) {
			this.isFixed = isFixed;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public void setLocation(String location) {
			this.location = location;
		}
		public void setTicketNo(String ticketNo) {
			this.ticketNo = ticketNo;
		}
		@Column
		public Date getInitiationDate() {
			return initiationDate;
		}
		@Column
		public Date getApprovalDate() {
			return approvalDate;
		}

		public void setInitiationDate(Date initiationDate) {
			this.initiationDate = initiationDate;
		}

		public void setApprovalDate(Date approvalDate) {
			this.approvalDate = approvalDate;
		}
		@Column
		public String getCurrency() {
			return currency;
		}

		public void setCurrency(String currency) {
			this.currency = currency;
		}
		@Column
		public String getApprovalFileName() {
			return approvalFileName;
		}

		public void setApprovalFileName(String approvalFileName) {
			this.approvalFileName = approvalFileName;
		}
		@Column
		public String getApprovalFileLocation() {
			return approvalFileLocation;
		}

		public void setApprovalFileLocation(String approvalFileLocation) {
			this.approvalFileLocation = approvalFileLocation;
		}
		@Column
		public BigDecimal getBillTillDate() {
			return billTillDate;
		}

		public void setBillTillDate(BigDecimal billTillDate) {
			this.billTillDate = billTillDate;
		}

		public Date getReleaseDate() {
			return releaseDate;
		}

		public void setReleaseDate(Date releaseDate) {
			this.releaseDate = releaseDate;
		}

	    
	}