<%@ include file="/common/taglibs.jsp" %> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>

<html>
<head>
<title>2 Step Action Tracking Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <style><%@ include file="/common/calenderStyle.css"%></style>
  
   <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>    
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<!-- Modification closed here -->


</head>

	
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}

div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-15px;
!margin-top:-10px;
}

div#main {
margin:-5px 0 0;
}

 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head>   


<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:52px; height:25px;"  method="twoStepProcessFiles" key="button.search" onclick="return goToSearchCustomerDetail();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px; height:25px;" onclick="clear_fields();"/> 
</c:set>   

<s:hidden name="fileID"  id= "fileID" value=""/> 
<c:set var="fileID"  value=""/>
<body>

<script language="javascript" type="text/javascript">
function clear_fields(){
	
		document.forms['actionTrackerForm'].elements['soNumber'].value  = "";
		document.forms['actionTrackerForm'].elements['action'].value  = "";
		document.forms['actionTrackerForm'].elements['userName'].value  = "";
		document.forms['actionTrackerForm'].elements['actionTime'].value  = "";
		document.forms['actionTrackerForm'].elements['corpID'].value  = "";
		
}


function goToSearchCustomerDetail(){
	var soNumber=document.forms['actionTrackerForm'].elements['soNumber'].value;
	var action=document.forms['actionTrackerForm'].elements['action'].value;
	var userName=document.forms['actionTrackerForm'].elements['userName'].value;
	var actionTime=document.forms['actionTrackerForm'].elements['actionTime'].value;
	var trackCorpID=document.forms['actionTrackerForm'].elements['corpID'].value;
	document.forms['actionTrackerForm'].action = 'stepTrackerSearch.html';
	document.forms['actionTrackerForm'].submit();
}

</script>

		
 <s:form id="ugwwActionTracker" name="actionTrackerForm" action="stepTrackerSearch" method="post" validate="true"> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:set name="ugwwList" value="%{ugwwList}" scope="session"/>   
    
<s:hidden name="id" />
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content" style="padding-left:15px;">
<table class="table" style="width:100%"  >
<thead>
<tr>
<th>S/O#</th>
<th>Action</th>
<th>User Name</th>
<th>Action Time</th>
<th>CorpID</th>

</tr></thead>	
		<tbody>
		<tr>
			<td width="20" align="left">
			    <s:textfield name="soNumber" required="true" cssClass="input-text" cssStyle="width:90px" size="15" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td width="20" align="left">
			    <s:textfield name="action" required="true" cssClass="input-text" size="16" />
			</td>
			<td width="20" align="left">
			    <s:textfield name="userName" required="true" cssClass="input-text" size="10" />
			</td>
			
			<td width="20" align="left">
			    <%-- <s:textfield name="actionTime" id="trackTime" required="true" cssClass="input-text" cssStyle="width:80px" size="20" onfocus="" onkeydown="return onlyDel(event,this)"/>
			    &nbsp;<img id="trackTime_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20" onclick="document.forms['actionTrackerForm'].elements['actionTime'].focus();return false;"/>  --%>
			 <c:if test="${not empty actionTime}">
					<s:text id="trackingStatusSitDestinationAFormattedValueassignDate" name="${FormDateValue}"><s:param name="value" value="actionTime"/></s:text>
					<s:textfield cssClass="input-text" id="actionTime" name="actionTime" value="%{trackingStatusSitDestinationAFormattedValueassignDate}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" />
					<img id="actionTime_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['actionTrackerForm'].elements['actionTime'].focus();  return false;"/>
					</c:if> 
					<c:if test="${empty actionTime}"> 
					<s:textfield cssClass="input-text" id="actionTime" name="actionTime" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" />
					<img id="actionTime_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['actionTrackerForm'].elements['actionTime'].focus();  return false;"/>
					 </c:if>	 
			</td>
					
			<td width="20" align="left">
			    <s:textfield name="corpID" required="true" cssClass="input-text" size="10" />
			</td>
			
			
			</tr>
			<tr>
			
			<td colspan="6" style="border:none;vertical-align:bottom; text-align:right;" >						
			    <c:out value="${searchbuttons}" escapeXml="false" />
			</td>
			</tr>
			</table>  
			       
			</td>
			
		</tr>

		</tbody>
	</table>
</div>
<div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>
</div>
</div> 

<c:out value="${searchresults}" escapeXml="false" />  
		<div id="otabs" style="margin-top: -10px;">
		  <ul>
		    <li><a class="current"><span>Action Tracker List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

 
<display:table name="ugwwList" class="table" requestURI="" id="ugwwList" export="true" defaultsort="4" pagesize="200" style="width:99;%margin-top:1px;margin-left: 5px; " >   
    
    <display:column property="sonumber" sortable="true" title="S/O#" />
    <display:column property="action" sortable="true" title="Action"/>
    <display:column property="username"  title="User Name"/>
    <display:column  sortable="true" title="Action Time"> 
     	<fmt:formatDate type="both"  pattern="dd-MMM-yy HH:mm:ss"    value="${ugwwList.actiontime}" />
    </display:column>
    <display:column property="corpid" sortable="true" title="CorpID"/>     

</display:table>   
  

</div>
</s:form>
 <script type="text/javascript">	
	setCalendarFunctionality();
</script>
</body>
</html>