DELIMITER $$
CREATE TRIGGER trigger_add_history_delete_vehicle BEFORE DELETE ON vehicle
FOR EACH ROW BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

   IF (OLD.licNumber <> '') THEN
       select CONCAT(fieldNameList,'vehicle.licNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.licNumber,'~') into oldValueList;
   END IF;
   IF (OLD.cntnrNumber <> '') THEN
       select CONCAT(fieldNameList,'vehicle.cntnrNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cntnrNumber,'~') into oldValueList;
   END IF;
   IF (OLD.sequenceNumber <> '') THEN
       select CONCAT(fieldNameList,'vehicle.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
   END IF;
   IF (OLD.ship <> '') THEN
       select CONCAT(fieldNameList,'vehicle.ship~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ship,'~') into oldValueList;
   END IF;
   IF (OLD.shipNumber <> '') THEN
       select CONCAT(fieldNameList,'vehicle.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
   END IF;
   IF (OLD.volume <> '' or OLD.volume is not null) THEN
       select CONCAT(fieldNameList,'vehicle.volume~') into fieldNameList;
       select CONCAT(oldValueList,OLD.volume,'~') into oldValueList;
   END IF;
  IF (OLD.idNumber <> '') THEN
       select CONCAT(fieldNameList,'vehicle.idNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.idNumber,'~') into oldValueList;
   END IF;
IF (OLD.createdOn <> '' or OLD.createdOn is not null) THEN
       select CONCAT(fieldNameList,'vehicle.createdOn~') into fieldNameList;
       select CONCAT(oldValueList,OLD.createdOn,'~') into oldValueList;
   END IF;
IF (OLD.corpID <> '') THEN
       select CONCAT(fieldNameList,'vehicle.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
   END IF;
   IF (OLD.createdBy <> '') THEN
       select CONCAT(fieldNameList,'vehicle.createdBy~') into fieldNameList;
       select CONCAT(oldValueList,OLD.createdBy,'~') into oldValueList;
   END IF;
IF (OLD.updatedOn <> '' or OLD.updatedOn is not null) THEN
       select CONCAT(fieldNameList,'vehicle.updatedOn~') into fieldNameList;
       select CONCAT(oldValueList,OLD.updatedOn,'~') into oldValueList;
   END IF;
   IF (OLD.updatedBy <> '') THEN
       select CONCAT(fieldNameList,'vehicle.updatedBy~') into fieldNameList;
       select CONCAT(oldValueList,OLD.updatedBy,'~') into oldValueList;
   END IF;
   IF (OLD.unit1 <> '') THEN
       select CONCAT(fieldNameList,'vehicle.unit1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit1,'~') into oldValueList;
   END IF;
   IF (OLD.unit2 <> '') THEN
       select CONCAT(fieldNameList,'vehicle.unit2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit2,'~') into oldValueList;
   END IF;
   IF (OLD.unit3 <> '') THEN
       select CONCAT(fieldNameList,'vehicle.unit3~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit3,'~') into oldValueList;
   END IF;
   IF (OLD.serviceOrderId <> '' or OLD.serviceOrderId is not null) THEN
       select CONCAT(fieldNameList,'vehicle.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
   END IF;
   IF (OLD.model <> '') THEN
       select CONCAT(fieldNameList,'vehicle.model~') into fieldNameList;
       select CONCAT(oldValueList,OLD.model,'~') into oldValueList;
   END IF;
   IF (OLD.classEPA <> '') THEN
       select CONCAT(fieldNameList,'vehicle.classEPA~') into fieldNameList;
       select CONCAT(oldValueList,OLD.classEPA,'~') into oldValueList;
   END IF;
    IF (OLD.totalLine <> '') THEN
       select CONCAT(fieldNameList,'vehicle.totalLine~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalLine,'~') into oldValueList;
   END IF;
   IF (OLD.weight <> '' or OLD.weight is not null) THEN
       select CONCAT(fieldNameList,'vehicle.weight~') into fieldNameList;
       select CONCAT(oldValueList,OLD.weight,'~') into oldValueList;
   END IF;
   IF (OLD.width <> '' or OLD.width is not null) THEN
       select CONCAT(fieldNameList,'vehicle.width~') into fieldNameList;
       select CONCAT(oldValueList,OLD.width,'~') into oldValueList;
   END IF;
   IF (OLD.systemDate <> '' or OLD.systemDate is not null) THEN
       select CONCAT(fieldNameList,'vehicle.systemDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.systemDate,'~') into oldValueList;
   END IF;
   IF (OLD.title <> '' or OLD.title is not null) THEN
       select CONCAT(fieldNameList,'vehicle.title~') into fieldNameList;
       select CONCAT(oldValueList,OLD.title,'~') into oldValueList;
   END IF;
   IF (OLD.doors <> '' or OLD.doors is not null) THEN
       select CONCAT(fieldNameList,'vehicle.doors~') into fieldNameList;
       select CONCAT(oldValueList,OLD.doors,'~') into oldValueList;
   END IF;
   IF (OLD.year <> '' or OLD.year is not null) THEN
       select CONCAT(fieldNameList,'vehicle.year~') into fieldNameList;
       select CONCAT(oldValueList,OLD.year,'~') into oldValueList;
   END IF;
   IF (OLD.cylinders <> '' or OLD.cylinders is not null) THEN
       select CONCAT(fieldNameList,'vehicle.cylinders~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cylinders,'~') into oldValueList;
   END IF;
   IF (OLD.inventory <> '' or OLD.inventory is not null) THEN
       select CONCAT(fieldNameList,'vehicle.inventory~') into fieldNameList;
       select CONCAT(oldValueList,OLD.inventory,'~') into oldValueList;
   END IF;
   IF (OLD.length <> '' or OLD.length is not null) THEN
       select CONCAT(fieldNameList,'vehicle.length~') into fieldNameList;
       select CONCAT(oldValueList,OLD.length,'~') into oldValueList;
   END IF;
   IF (OLD.color <> '') THEN
       select CONCAT(fieldNameList,'vehicle.color~') into fieldNameList;
       select CONCAT(oldValueList,OLD.color,'~') into oldValueList;
   END IF;
   IF (OLD.make <> '') THEN
       select CONCAT(fieldNameList,'vehicle.make~') into fieldNameList;
       select CONCAT(oldValueList,OLD.make,'~') into oldValueList;
   END IF;
   IF (OLD.proNumber <> '') THEN
       select CONCAT(fieldNameList,'vehicle.proNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.proNumber,'~') into oldValueList;
   END IF;
   IF (OLD.serial <> '') THEN
       select CONCAT(fieldNameList,'vehicle.serial~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serial,'~') into oldValueList;
   END IF;
   IF (OLD.titleNumber <> '') THEN
       select CONCAT(fieldNameList,'vehicle.titleNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.titleNumber,'~') into oldValueList;
   END IF;
    CALL add_tblHistory (OLD.serviceOrderId,"vehicle", fieldNameList, oldValueList, newValueList, OLD.updatedby, OLD.corpID, now());
END
$$