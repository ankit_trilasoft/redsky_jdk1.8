package com.trilasoft.app.webapp.action;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.model.VanLineGLType;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.VanLineGLTypeManager;

public class VanLineGLTypeAction extends BaseAction {

	private List vanLineGLTypeList;

	private Long id;

	private VanLineGLTypeManager vanLineGLTypeManager;

	private String sessionCorpID;

	private VanLineGLType vanLineGLType;

	private RefMasterManager refMasterManager;

	private Map<String, String> glCode;
	
	private String gotoPageString;
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(VanLineGLTypeAction.class);

	public String getGotoPageString() {
		 return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

	   this.gotoPageString = gotoPageString;

	}

	
	private String validateFormNav;
    
    public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	
	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		    //if (option enabled for this company in the system parameters table then call save) {
		validateFormNav = "OK";
	    String s = save();    // else simply navigate to the requested page)
	      logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    return gotoPageString;
	} 

	public VanLineGLTypeAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		vanLineGLTypeList = vanLineGLTypeManager.vanLineGLTypeList();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		glCode = refMasterManager.findByParameter(corpId, "GLCODES");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				if (id != null) {
			vanLineGLType = vanLineGLTypeManager.get(id);
		} else {
			vanLineGLType = new VanLineGLType();
			vanLineGLType.setCreatedOn(new Date());
			vanLineGLType.setUpdatedOn(new Date());
		}
		getComboList(sessionCorpID);
		vanLineGLType.setCorpID(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (vanLineGLType.getId() == null);
		getComboList(sessionCorpID);
		List isexisted=vanLineGLTypeManager.isExisted(vanLineGLType.getGlType(),sessionCorpID);
		if(!isexisted.isEmpty() && isNew){
			errorMessage("GL Type already exist.");
			return INPUT; 
		}else{ 
		if (isNew) {
			vanLineGLType.setCreatedOn(new Date());
		}
		vanLineGLType.setUpdatedOn(new Date());
		vanLineGLType.setUpdatedBy(getRequest().getRemoteUser());
		vanLineGLType.setCorpID(sessionCorpID);
		vanLineGLType = vanLineGLTypeManager.save(vanLineGLType);
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "Van Line GL Type has been added successfully" : "Van Line GL Type has been updated successfully";
		saveMessage(getText(key));
		}
			if (!isNew) {
				return INPUT;
			} else {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
		}	
	}
	
	@SkipValidation 
	  public String searchGLType(){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean glType = (vanLineGLType.getGlType() == null);
	    boolean desc = (vanLineGLType.getDescription() == null);
	    boolean glCode = (vanLineGLType.getGlCode() == null);
	    
          if(glType ||desc ||glCode ) {
        	  vanLineGLTypeList =  vanLineGLTypeManager.searchVanLineGLType(vanLineGLType.getGlType(), vanLineGLType.getDescription(), vanLineGLType.getGlCode());
        	  
          }else{
        	  vanLineGLTypeList = vanLineGLTypeManager.searchVanLineGLType(vanLineGLType.getGlType(), vanLineGLType.getDescription(), vanLineGLType.getGlCode());
          }
          logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;     
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setVanLineGLTypeManager(VanLineGLTypeManager vanLineGLTypeManager) {
		this.vanLineGLTypeManager = vanLineGLTypeManager;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public VanLineGLType getVanLineGLType() {
		return vanLineGLType;
	}

	public void setVanLineGLType(VanLineGLType vanLineGLType) {
		this.vanLineGLType = vanLineGLType;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getGlCode() {
		return glCode;
	}

	public void setGlCode(Map<String, String> glCode) {
		this.glCode = glCode;
	}

	public List getVanLineGLTypeList() {
		return vanLineGLTypeList;
	}

	public void setVanLineGLTypeList(List vanLineGLTypeList) {
		this.vanLineGLTypeList = vanLineGLTypeList;
	}

	
}
