<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<script type="text/javascript">
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
   try{
   document.getElementById("successMessages").style.visibility='hidden'; 
   }catch (e) { 
   }
      if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}
function winLoad(){
		parent.window.opener.document.location.reload(); 
		window.close();
}
function uploadFile(){
	if(document.forms['uploadForm'].elements['policyDocument.documentName'].value==""){
		alert("Select  Document Name");
		return false;
	}else if(document.forms['uploadForm'].elements['policyDocument.description'].value==""){
		alert("Select Description");
		return false;
	}else if(document.forms['uploadForm'].elements['file'].value==""){
		document.forms['uploadForm'].elements['file'].focus;
		alert("Please select a file to upload");
		return false;
	}else if(document.forms['uploadForm'].elements['policyDocument.docSequenceNumber'].value==""){
		alert("Please select document Sequence");
	}
	else{
		showUploadMsg();
		var partnerCode1 = gup('partnerCode');
		var sectionName1 = gup('sectionName');
		var language1 = gup('language');
	var url = "uploadPolicyAdminDocument.html?sectionName="+sectionName1+"&language="+language1+"&decorator=popup&popup=true";
	document.forms['uploadForm'].action = url;
	document.forms['uploadForm'].submit();
	return true;
	}
	return true;
}
function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}
function showUploadMsg()
{
 var mymsg = document.getElementById('layerH').style.display = "block";
}
function limitText() {
	var limitField= document.forms['uploadForm'].elements['policyDocument.description'].value;
	limitNum=300;
		if (limitField.length > limitNum) {
			limitField= limitField.substring(0,limitNum);
			document.forms['uploadForm'].elements['policyDocument.description'].value=limitField;
		}
	}
function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
}
</script>
<head>   
    <title>Policy Upload</title>
    </head>
     <DIV ID="layerH" style="display:none">
 <table border="0" width="100%" align="middle">
 <tr>
 <td align="center">
	<font size="4" color="#1666C9"><b><blink>Uploading...</blink></b></font>
	</td>
	</tr>
	</table>
</DIV> 
    <s:form id="uploadForm" name="uploadForm"   enctype="multipart/form-data" method="post" validate="true" >
    <c:set var="id" value="${policyDocument.id}"/>
    <s:hidden name="id" value="${policyDocument.id}"/>
    <s:hidden name="policyDocument.id" />
    <s:hidden name="policyDocument.partnerCode" value=""/>
     <c:set var="policyDocument.sectionName" value="<%= request.getParameter("sectionName")%>"/>
    <s:hidden name="policyDocument.sectionName" value="<%= request.getParameter("sectionName")%>"/>
     <c:set var="policyDocument.language" value="<%= request.getParameter("language")%>"/>
    <s:hidden name="policyDocument.language" value="<%= request.getParameter("language")%>"/>
        <div id="Layer3" style="width:100%; margin:0px; padding:0px">
 <div id="newmnav">
 <ul>
 <li id="newmnav1" style="background:#FFF "><a class="current" ><span>File Upload<img src="images/navarrow.gif" align="absmiddle"/></span></a></li> 

 	</ul> 
</div>
		<div class="spn spnSF">&nbsp;</div> 
        <div id="content" align="center">
        <div id="liquid-round">
        <div class="top" style="margin-top:0px;!margin-top:5px;"><span></span></div>
        <div class="center-content"> 
				<table cellspacing="1" cellpadding="2" border="0" style="margin:0px; ">
					<tbody> 
					<tr>
					<td style="width:120px" align="right" class="listwhitetext">Document Name<font color="red" size="2">*</font></td>
					<td  align="left"><s:textfield name="policyDocument.documentName" onchange="valid(this,'special');" cssClass="input-text" size="53"  maxlength="255"/> </td>
					
					</tr>
					<tr>						      
						      <td align="right" class="listwhitetext">Document Description<font color="red" size="2">*</font></td>
							  <td align="left" colspan="6"><s:textarea name="policyDocument.description"   onchange="valid(this,'special');"  onkeydown="limitText()" onkeyup="limitText()" cols="50" rows="5"  cssClass="textarea" /></td>
					      </tr> 
					      <c:if test="${empty policyDocument.id}">
						<tr>
							<td align="right" class="listwhitetext" width="30px">File To Upload <img src="${pageContext.request.contextPath}/images/external.png" style="cursor:auto;"/></td>
							<td align="left" class="listwhitetext"><s:file name="file" label="%{getText('uploadForm.file')}" size="25"/></td>
							
						</tr> 
						<tr>
						<td align="right" class="listwhitetext">Sequence #<font color="red" size="2">*</font></td>
					     <td class="listwhitetext"><s:textfield name="policyDocument.docSequenceNumber" onkeypress="return isNumberKey(event)" cssClass="input-text" maxlength="10" size="8"></s:textfield></td>
						</tr>
						<tr><td height="10px"></td></tr>  
						<tr>
						<td align="right" class="listwhitetext" width="30px"></td>						
						<td align="right" style="padding-right:45px;">
						<input type="button" class="cssbutton"  value="Upload File" cssStyle="width:100px; height:27px"  onclick="return uploadFile(); "  />
                                     
                        </td><!--
                        <td>
                        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:27px" key="Reset"/></td>                       
                        --></tr>
                        <tr>
                        <td></td>
                        <td colspan="3" align="center"><font color="red" size="2">Note:Max file size allowed is 5 MB </font></td>
                        </tr>
                        </c:if>
                        <tr><td height="10px"></td></tr>  
					</tbody>
				</table> 
	    </div>
       <div class="bottom-header"><span></span></div>
       </div>
       </div>
       </div> 
       <c:if test=""></c:if>
    </s:form>
  <script type="text/javascript">
try{
	<c:if test="${hitFlag=='1'}">
	winLoad();
	</c:if>
	<c:if test="${hitFlag=='0'}">
	alert('${fileNotExists}');
	</c:if>	
}catch(e){}
 </script>