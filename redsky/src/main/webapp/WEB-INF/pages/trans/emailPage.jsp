<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<meta name="heading" content="Email"/> 
<title>Email</title> 
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
</head>

<style type="text/css">h2 {background-color: #FBBFFF}</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>	
	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns()
	</script>
	
<script>
	function submitAction(){
		agree= confirm("Are you sure you want to dismiss this note?");
		if(agree){
		var url = "dismissReminder1.html?decorator=popup&popup=true";
		document.forms['notesControl'].action = url;
		document.forms['notesControl'].submit();
		window.refreshParent();
		setTimeout("window.self.close()", 700);
		}
		else{
			return false;
		}	
	}
		
</script>	

<script language="javascript" type="text/javascript">

function imposeMaxLength(Object, MaxLen)
{
return (Object.value.length <= MaxLen);
}
</script>



<s:form id="emailPage" name="emailPage" action="saveEmailPage.html?decorator=popup&popup=true" method="post" validate="true" >
<s:hidden name="id" value="${pricingControl.id}" />

<div id="layer1"  style="width:100%;margin-bottom:1px; ">
 	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:5px;"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="1" cellpadding="1" border="0" style="" width="100%">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext" width="100%">
	  	<table class="detailTabLabel" border="0">
			<tbody>  	
				  	<tr>
				  		<td align="left" height="3px" colspan="4"></td>
				  	</tr>
				  	<tr>
					  	<td align="right">From:</td>
					  	<td align="left"><s:textfield name="fromEmailAddress"   size="110"  cssClass="input-text" /></td>
				  	</tr>
							
				</tbody>
			</table>
		  
		<table class="detailTabLabel" border="0" width="100%">
		<tbody> 
			  			   
			
			  <tr><td class="listwhitetext" style="font-size: 14px; font-weight: bold">Message:</td></tr>
			  <tr><td class="vertlinedata"></td></tr>
			  	<tr>
			  		<td align="left" colspan="4"><s:textarea name="emailMessage"  onkeypress="return imposeMaxLength(this,1900);" rows="4" cols="112" readonly="false" cssClass="textarea"/></td>
			  	</tr>
		</tbody>
  </table>	  
		
		
		
		</td>
		</tr>
		
		</tbody>
		
	</table>
		 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
	
	<table class="detailTabLabel" border="0">
				<tbody>
					
				<tr align="center">
				<td align="left"> 
					<input type="submit" class="cssbutton1" name="Send" style="width: 120px" value="Send"/>
				</td>   
			</tr>
		</tbody>
	</table>

<div id="mydiv" style="position:absolute"></div>
</s:form>
<script>
try{
<c:if test="${hitFlag == 1}">
alert('Email has been successfully sent.');
window.close();
</c:if>
}
catch(e){}

</script>