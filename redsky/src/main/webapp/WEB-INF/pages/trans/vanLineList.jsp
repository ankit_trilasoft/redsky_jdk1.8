<%@ include file="/common/taglibs.jsp"%>  
<head>   
<title><fmt:message key="vanLineList.title"/></title>   
<meta name="heading" content="<fmt:message key='vanLineList.heading'/>"/>   
<script language="javascript" type="text/javascript">
function clear_fields(){
	var i;
		for(i=0;i<=1;i++){
			document.forms['searchForm'].elements['vanLine.agent'].value = "";
			document.forms['searchForm'].elements['vanLine.weekEnding'].value = "";			
			document.forms['searchForm'].elements['vanLine.glType'].value = "";
			document.forms['searchForm'].elements['vanLine.reconcileStatus'].value = "";
		}
}
</script>
<style>
	<%@ include file="/common/calenderStyle.css"%>
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="JavaScript" type="text/javascript">
	var cal = new CalendarPopup(); 
	cal.showYearNavigation(); 
	cal.showYearNavigationInput();  
</script>

<style>
 span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:95%;
!width:95%;
}
</style>
</head>   
  
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editVanLine.html"/>'"  value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px" align="top" method="searchVanLine" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>   

     
<s:form id="searchForm" action="searchVanLine" method="post" validate="true" >
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>


<table class="table" style="width:95%"  >
	<thead>
		<tr>
			<!--<th><fmt:message key="vanLine.regNum"/></th>
			<th><fmt:message key="vanLine.tranDate"/></th>
			<td width="95px" align="left"><s:textfield id="tranDate" cssClass="input-text" name="vanLine.weekEnding" 
			cssStyle="width:85px" readonly="true" onkeydown="return onlyDel(event,this)"/>
			-->
			<th><fmt:message key="vanLine.agencyCode"/></th>
			<th><fmt:message key="vanLine.weekEnding"/></th>
			<th><fmt:message key="vanLine.glType"/></th>
			<th><fmt:message key="vanLine.reconcileStatus"/></th>			
			<th style="background:border-color;border:noborder-color;"></th>
		</tr>
			<tbody>
		<tr>
			<td width="20%" align="left"> <s:textfield name="vanLine.agent" required="true" cssClass="input-text" size="25"/> </td>
			<c:if test="${not empty vanLine.weekEnding}">
			<s:text id="vanLineDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="vanLine.weekEnding"/></s:text>
			<td width="95px" align="left">
				<s:textfield cssClass="input-text" id="tranDate" name="vanLine.weekEnding" value="%{vanLineDateFormattedValue}" onkeydown="return onlyDel(event,this)" size="12" maxlength="11" readonly="true" />
				<img id="tranDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
			</td>
			</c:if>
			<c:if test="${empty vanLine.weekEnding}">
			<td width="95px" align="left">
				<s:textfield cssClass="input-text" id="tranDate" name="vanLine.weekEnding" value="%{vanLineDateFormattedValue}" onkeydown="return onlyDel(event,this)" size="12" maxlength="11" readonly="true" />
				<img id="tranDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
			</td>
			</c:if>
			<td width="20%" align="left"> <s:textfield name="vanLine.glType" required="true" cssClass="input-text" size="25"/> </td>
			<td width="20%" align="left"><s:select cssClass="list-menu"   name="vanLine.reconcileStatus" list="{'','Reconcile','Suspense','Approved','Dispute'}" cssStyle="width:112px" onchange="changeStatus();"/></td>
			<td width="130px" align="center"> <c:out value="${searchbuttons}" escapeXml="false" /> </td>
			</tr>
		</tbody>			
		
	</thead>	
		
	</table>


<c:out value="${searchresults}" escapeXml="false" />  
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Settlement List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<s:set name="vanLineListExt" value="vanLineListExt" scope="request"/>   
<display:table name="vanLineListExt" class="table" requestURI="" id="vanLineList" export="true" defaultsort="2" pagesize="10" style="width:95%" >   
	<display:column property="agent" sortable="true" titleKey="vanLine.agent" href="editVanLine.html" paramId="id" paramProperty="id" />
	<display:column property="weekEnding" sortable="true" titleKey="vanLine.weekEnding" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>	
	<display:column property="shipper" sortable="true" titleKey="vanLine.shipper"/>
	<display:column property="amtDueAgent" sortable="true" titleKey="vanLine.amtDueAgent" style="text-align: right;"/>	
	<display:column property="reconcileAmount" sortable="true" titleKey="vanLine.reconcileAmount" style="text-align: right;"/>	
	<display:column property="glType"  sortable="true" titleKey="vanLine.glType"/>
	<display:column property="reconcileStatus" sortable="true" titleKey="vanLine.reconcileStatus"/>
	
	
       
    <display:setProperty name="paging.banner.item_name" value="vanLine"/>   
    <display:setProperty name="paging.banner.items_name" value="vanLines"/>    
    <display:setProperty name="export.excel.filename" value="Van Line List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Van Line List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Van Line List.pdf"/>   
</display:table>   
  
<c:out value="${buttons}" escapeXml="false" />   
<c:set var="isTrue" value="false" scope="session"/>
</s:form>  
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script> 
<script type="text/javascript">   
    highlightTableRows("vanLineList");  
    Form.focusFirstElement($("searchForm"));    
</script> 