package com.trilasoft.app.dao.hibernate.dto;

public class AdditionalChargesDTO {

	// Ticket No.6951 Additional Charges extraction in account portal

	private Object transferee;
	private Object customerFile;
	private Object serviceOrder;
	private Object cost;
	private Object description;
	private Object currency;
	private Object amount;
	private Object totalAmount;

	public Object getTransferee() {
		return transferee;
	}

	public Object getCustomerFile() {
		return customerFile;
	}

	public Object getServiceOrder() {
		return serviceOrder;
	}

	public Object getCost() {
		return cost;
	}

	public Object getDescription() {
		return description;
	}

	public Object getCurrency() {
		return currency;
	}

	public Object getAmount() {
		return amount;
	}

	public Object getTotalAmount() {
		return totalAmount;
	}

	public void setTransferee(Object transferee) {
		this.transferee = transferee;
	}

	public void setCustomerFile(Object customerFile) {
		this.customerFile = customerFile;
	}

	public void setServiceOrder(Object serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public void setCost(Object cost) {
		this.cost = cost;
	}

	public void setDescription(Object description) {
		this.description = description;
	}

	public void setCurrency(Object currency) {
		this.currency = currency;
	}

	public void setAmount(Object amount) {
		this.amount = amount;
	}

	public void setTotalAmount(Object totalAmount) {
		if (totalAmount.toString().indexOf(".") == -1) {
			totalAmount = totalAmount + ".00";
		} else {
			totalAmount = totalAmount.toString().replace(".", "~");
			String arr[] = totalAmount.toString().split("~");
			if (arr.length == 2 && arr[1].trim().length() != 2) {
				totalAmount = arr[0] + "." + arr[1]+ "00".substring(0, arr[1].trim().length());
			} else if (arr.length != 2) {
				totalAmount = totalAmount + "00";
			}
		}
		totalAmount = totalAmount.toString().replace("~", ".");

		this.totalAmount = totalAmount;
	}

	@Override
	public String toString() {
		String returnString = transferee + "seprator" + customerFile
				+ "seprator" + serviceOrder + "seprator" + cost + "seprator"
				+ description + "seprator" + currency + "seprator" + amount
				+ "seprator" + totalAmount;
		return returnString;
	}

}
