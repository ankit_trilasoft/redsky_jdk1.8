package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="updateruleresult")

public class UpdateRuleResult extends BaseObject  {
	
	private String corpID; 
	private Long id;
	private String tableName;
	private String fieldName;
	private String ruleName;
	private String valueOfField;
	private String updatefieldValue;
	private String updatedBy;
	private Date updatedOn;
	private String createdBy;
	private Date createdOn;
	private Long updatedRuleId;
	private Long ruleNumber;
	private Long orderId;
	private String shipNumber;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id",id)
				.append("corpID",corpID)
				.append("tableName",tableName)
				.append("fieldName",fieldName)
				.append("ruleName",ruleName)
				.append("valueOfField",valueOfField)
				.append("updatedBy",updatedBy)
				.append("createdBy",createdBy)
				.append("createdOn",createdOn)
				.append("updatedOn",updatedOn)
				.append("updatedRuleId",updatedRuleId)
				.append("ruleNumber",ruleNumber)
				.append("orderId",orderId)
				.append("shipNumber",shipNumber)
				.toString();
	}

	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof UserGuides))
			return false;
		UpdateRuleResult castOther = (UpdateRuleResult) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(tableName, castOther.tableName)
				.append(fieldName, castOther.fieldName)
				.append(ruleName, castOther.ruleName)
				.append(valueOfField, castOther.valueOfField)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedRuleId, castOther.updatedRuleId)
				.append(ruleNumber, castOther.ruleNumber)
				.append(orderId, castOther.orderId)
				.append(shipNumber, castOther.shipNumber)
				.isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(tableName).append(fieldName).append(ruleName).append(valueOfField).append(updatedBy).append(createdBy)
				.append(createdOn).append(updatedOn)
				.append(updatedRuleId).append(ruleNumber)
				.append(orderId).append(shipNumber)
				.toHashCode();
	}

	
	

	@Column
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	@Column
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	@Column
	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	@Column
	public String getValueOfField() {
		return valueOfField;
	}

	public void setValueOfField(String valueOfField) {
		this.valueOfField = valueOfField;
	}
	@Column
	public String getUpdatefieldValue() {
		return updatefieldValue;
	}

	public void setUpdatefieldValue(String updatefieldValue) {
		this.updatefieldValue = updatefieldValue;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column
	public Long getUpdatedRuleId() {
		return updatedRuleId;
	}


	public void setUpdatedRuleId(Long updatedRuleId) {
		this.updatedRuleId = updatedRuleId;
	}

	@Column
	public Long getRuleNumber() {
		return ruleNumber;
	}


	public void setRuleNumber(Long ruleNumber) {
		this.ruleNumber = ruleNumber;
	}

	@Column
	public Long getOrderId() {
		return orderId;
	}


	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	@Column
	public String getShipNumber() {
		return shipNumber;
	}


	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

}
