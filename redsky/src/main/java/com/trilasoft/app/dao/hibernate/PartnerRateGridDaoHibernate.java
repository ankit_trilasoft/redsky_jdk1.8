package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;



import com.trilasoft.app.dao.PartnerRateGridDao;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.PartnerRateGrid;
import com.trilasoft.app.model.RefMaster;

public class PartnerRateGridDaoHibernate extends GenericDaoHibernate<PartnerRateGrid, Long> implements PartnerRateGridDao{
	public PartnerRateGridDaoHibernate() {   
        super(PartnerRateGrid.class);   
    }

	public List getPartnerRateGridList(String sessionCorpID, String partnerCode) {
		
		return getHibernateTemplate().find("from PartnerRateGrid where partnerCode='"+partnerCode+"' ");
	}

	public List getRateGridTemplates(String sessionCorpID) {
		return getHibernateTemplate().find("from PartnerRateGridTemplate where corpID in ('" + sessionCorpID + "','XXXX','TSFT','NULL') order by sortRateGrid");
	}

	public Map<String, String> findStateList(String terminalCountryCode, String sessionCorpID) {

		if(terminalCountryCode == null || terminalCountryCode.equals("")){
			terminalCountryCode = "NoCountry";
		}
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		List<RefMaster> list = getHibernateTemplate().find("from RefMaster where  bucket2 like '"+terminalCountryCode+"%' order by description");
		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
			}
		return parameterMap;
	
	}
 
	public List getStandardInclusionsNote(String corpID) {
		return getHibernateTemplate().find("select standardInclusionsOrigin from PartnerRateGridDefaults where corpID='"+corpID+"'");
	}

	public List getStandardDestinationNote(String corpID) {
		return getHibernateTemplate().find("select standardInclusionsDestination from PartnerRateGridDefaults where corpID='"+corpID+"'");
	}

	public int updateRateGridStatus(Long id, String status, String sessionCorpID, String user) {
	    int i=0;
		if(status.toString().equalsIgnoreCase("Withdraw")){
	    	i =getHibernateTemplate().bulkUpdate("update PartnerRateGrid set status='" +status+ "',updatedOn=now(),updatedBy='"+user+"' where id='"+id+"'  "); 	
	    }
		if(status.toString().equalsIgnoreCase("New")){
			i =getHibernateTemplate().bulkUpdate("update PartnerRateGrid set status='" +status+ "',updatedOn=now(),updatedBy='"+user+"',publishTariff = false where id='"+id+"'   "); 	
		}
	    return i;
	}

	public int findSubmitCount(String partnerCode, String sessionCorpID, String tariffApplicability) {
		List countList= getHibernateTemplate().find("select count(*) from PartnerRateGrid where partnerCode='"+partnerCode+"' and corpID in ('TSFT','"+sessionCorpID+"') and status='Submitted' and tariffApplicability='"+tariffApplicability+"' ");
		int count=Integer.parseInt(countList.get(0).toString());
		return count;
	}

	public int findUniqueCount(String sessionCorpID, String partnerCode, String tariffApplicability, String metroCity, BigDecimal serviceRangeMiles, String effectiveDate) {
		List countList= getHibernateTemplate().find("select count(*) from PartnerRateGrid where partnerCode='"+partnerCode+"' and corpID in ('TSFT','"+sessionCorpID+"') and tariffApplicability='"+tariffApplicability+"' and metroCity='"+metroCity+"' and serviceRangeMiles='"+serviceRangeMiles+"' and effectiveDate='"+effectiveDate+"'");
		int count=Integer.parseInt(countList.get(0).toString());
		return count;
		
	}

	public List getRateGridRules(String corpID) {
		return getHibernateTemplate().find("select rateGridRules from PartnerRateGridDefaults where corpID='"+corpID+"'");
	}

	public List findPartnerRateMatrix(String sessionCorpID, String partnerCode, String partnerName, String terminalCountry, String tariffApplicability, String status, boolean publishTariff) {
		
		if(publishTariff){
		return getHibernateTemplate().find("from PartnerRateGrid where partnerCode  like '%" + partnerCode.replaceAll("'", "''") + "%' and partnerName like '%" + partnerName.replaceAll("'", "''") + "%' and terminalCountry like '%" + terminalCountry.replaceAll("'", "''") + "%' and tariffApplicability like '%" + tariffApplicability.replaceAll("'", "''") + "%' and status like '%" + status.replaceAll("'", "''") + "%' and publishTariff =true ");
		}else{
			return getHibernateTemplate().find("from PartnerRateGrid where partnerCode  like '%" + partnerCode.replaceAll("'", "''") + "%' and partnerName like '%" + partnerName.replaceAll("'", "''") + "%' and terminalCountry like '%" + terminalCountry.replaceAll("'", "''") + "%' and tariffApplicability like '%" + tariffApplicability.replaceAll("'", "''") + "%' and status like '%" + status.replaceAll("'", "''") + "%' and publishTariff =false ");	
		}
		}

	public int updatePublisStatus(Long id, String publishTariff) { 
		if(publishTariff.equals("true"))
		{	
		return getHibernateTemplate().bulkUpdate("update PartnerRateGrid set publishTariff =true where id='"+id+"'");		
		}
		else
		{			
			return getHibernateTemplate().bulkUpdate("update PartnerRateGrid set publishTariff =false where id='"+id+"'");
		}
	
	}

	public List getStandardCountryHauling(String corpID) {
		return getHibernateTemplate().find("select standardCountryHauling from SystemDefault where standardCountryHauling is not null and corpID=?", corpID);
	}

	public List getStandardCountryCharges(String corpID) {
		return getHibernateTemplate().find("select standardCountryCharges from SystemDefault where standardCountryCharges is not null and corpID=?", corpID);
	}

	public List searchBaseCurrency(String sessionCorpID) {
		return getHibernateTemplate().find("select baseCurrency from SystemDefault where baseCurrency is not null and corpID=?", sessionCorpID);
	}

	public List getPartnerRateGridListView(String sessionCorpID, String partnerCode) {
		return getHibernateTemplate().find("from PartnerRateGrid where partnerCode='"+partnerCode+"'  and publishTariff =true ");
	}
}
