package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="customerinformation")
public class CustomerInformation extends BaseObject{
	private Long id;
	private String corpID;
	private Long  quoteId;
	private String firstName;
	private String lastName;
	private String country;
	private String city;
	private String state;
	private String zip;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String emailAddress;
	private String phone1;
	private String phone2;
	
	private String cardHoldersName;
	private String creditCardNumber;
	private String creditCardType;
	private String expiryMonth;
	private String expiryYear;
	private String cvvNumber;
	
	
	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("quoteId", quoteId).append("firstName",
				firstName).append("lastName", lastName).append("country",
				country).append("city", city).append("state", state).append(
				"zip", zip).append("addressLine1", addressLine1).append(
				"addressLine2", addressLine2).append("addressLine3",
				addressLine3).append("emailAddress", emailAddress).append(
				"phone1", phone1).append("phone2", phone2).append(
				"cardHoldersName", cardHoldersName).append("creditCardNumber",
				creditCardNumber).append("creditCardType", creditCardType)
				.append("expiryMonth", expiryMonth).append("expiryYear",
						expiryYear).append("cvvNumber", cvvNumber).append(
						"createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CustomerInformation))
			return false;
		CustomerInformation castOther = (CustomerInformation) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(quoteId, castOther.quoteId).append(
				firstName, castOther.firstName).append(lastName,
				castOther.lastName).append(country, castOther.country).append(
				city, castOther.city).append(state, castOther.state).append(
				zip, castOther.zip)
				.append(addressLine1, castOther.addressLine1).append(
						addressLine2, castOther.addressLine2).append(
						addressLine3, castOther.addressLine3).append(
						emailAddress, castOther.emailAddress).append(phone1,
						castOther.phone1).append(phone2, castOther.phone2)
				.append(cardHoldersName, castOther.cardHoldersName).append(
						creditCardNumber, castOther.creditCardNumber).append(
						creditCardType, castOther.creditCardType).append(
						expiryMonth, castOther.expiryMonth).append(expiryYear,
						castOther.expiryYear).append(cvvNumber,
						castOther.cvvNumber).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(quoteId)
				.append(firstName).append(lastName).append(country)
				.append(city).append(state).append(zip).append(addressLine1)
				.append(addressLine2).append(addressLine3).append(emailAddress)
				.append(phone1).append(phone2).append(cardHoldersName).append(
						creditCardNumber).append(creditCardType).append(
						expiryMonth).append(expiryYear).append(cvvNumber)
				.append(createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).toHashCode();
	}
	@Column
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getAddressLine3() {
		return addressLine3;
	}
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}
	public String getCardHoldersName() {
		return cardHoldersName;
	}
	public void setCardHoldersName(String cardHoldersName) {
		this.cardHoldersName = cardHoldersName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	public String getCreditCardType() {
		return creditCardType;
	}
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}
	public String getCvvNumber() {
		return cvvNumber;
	}
	public void setCvvNumber(String cvvNumber) {
		this.cvvNumber = cvvNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getExpiryMonth() {
		return expiryMonth;
	}
	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}
	public String getExpiryYear() {
		return expiryYear;
	}
	public void setExpiryYear(String expiryYear) {
		this.expiryYear = expiryYear;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public Long getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(Long quoteId) {
		this.quoteId = quoteId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	
		
	
}
