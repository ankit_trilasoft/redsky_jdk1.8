<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Pertaining Loss</title>  
</head>
<p style="margin-top:-17px;"></p>
<table style="float:right;margin:0px;padding:0px;" >
  <tr><b>Other Responsible Agents</b>
 <td align="right"  style="padding:3px 5px 0px 0px;">
		<img align="right" class="openpopup" onclick="hideTooltip();" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
  </tr>
</table>
<div style="clear:both;"></div>
<s:set name="pertainingloss" value="pertainingloss" scope="request"/>
<display:table name="pertainingloss" class="table" requestURI="" id="pertainingloss"  defaultsort="2" pagesize="10" style="margin:0px;width:1000px;">
<display:column property="agentCode" title="Agent&nbsp;Code"></display:column>
<display:column property="partnerCode" title="RedSky&nbsp;Agent&nbsp;Code"></display:column>
<display:column property="partnerName" title="Agent&nbsp;Name"></display:column>
<display:column property="damageByAction" title="Damage&nbsp;By"></display:column>
<display:column property="lossAction" title="Action"></display:column>
<display:column property="itmDnlRsnCode" title="Denial&nbsp;Reason"></display:column>
<display:column property="itrRespExcpCode" title="Exception&nbsp;Code"></display:column>
<display:column property="itrAssessdLiabilityAmt" title="Assessed&nbsp;Liability"></display:column>
<display:column property="itrTotalLiabilityAmt" title="Total&nbsp;Liability"></display:column>
<display:column property="chargeBackAmount" title="Charge Back&nbsp;Amount"></display:column>
<display:column property="itrPmntId" title="Payment&nbsp;ID"></display:column>
</display:table>