<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/common/tooltip.jsp"%>  
 <head>  
    <title>VanLine Code List</title>   
    <meta name="heading" content="Service Order List"/> 
</head>   

<s:form id="trackingInfoListVanLineCode" method="post" validate="true">  

<div class="spnblk" >&nbsp;</div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr>
	<td align="left"  style="padding-left:5px;min-width:120px;">
		<b>Van Line Code List</b>
	</td>
	<td align="right"  style="padding-right:5px;">
		<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  
<s:set name="vanLineList" value="vanLineList" scope="request"/>
<display:table name="vanLineList" class="table" requestURI="" id="vanLineList" style="width:100%;" partialList="true" size="1"> 
  
  <display:column  property="vanLineCode" title="Van Line Code" style="width:190px"/> 
  </display:table>
</s:form>

