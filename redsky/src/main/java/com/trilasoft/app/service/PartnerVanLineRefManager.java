package com.trilasoft.app.service;

import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.PartnerVanLineRef;

public interface PartnerVanLineRefManager extends GenericManager<PartnerVanLineRef, Long> {
	
	public List getPartnerVanLineReferenceList(String corpId, String partnerCodeForRef);

	public List getpartner(String partnerCode, String sessionCorpID);

	public List checkJobType(String vanLineCode, String corpID, String jobType);

	public int updateDeleteStatus(Long ids);

}
