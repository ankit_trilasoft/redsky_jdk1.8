
delimiter $$
CREATE trigger redsky.trigger_add_history_accountcontact
BEFORE UPDATE on redsky.accountcontact
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
   IF (OLD.corpId <> NEW.corpId ) THEN
      select CONCAT(fieldNameList,'accountcontact.corpId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpId,'~') into newValueList;
  END IF;
   IF (OLD.department <> NEW.department ) THEN
      select CONCAT(fieldNameList,'accountcontact.department~') into fieldNameList;
      select CONCAT(oldValueList,OLD.department,'~') into oldValueList;
      select CONCAT(newValueList,NEW.department,'~') into newValueList;
  END IF;
   IF (OLD.jobType <> NEW.jobType ) THEN
      select CONCAT(fieldNameList,'accountcontact.jobType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.jobType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.jobType,'~') into newValueList;
  END IF;
   IF (OLD.contactName <> NEW.contactName ) THEN
      select CONCAT(fieldNameList,'accountcontact.contactName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contactName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contactName,'~') into newValueList;
  END IF;
   IF (OLD.contactEmail <> NEW.contactEmail ) THEN
      select CONCAT(fieldNameList,'accountcontact.contactEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contactEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contactEmail,'~') into newValueList;
  END IF;
   IF (OLD.partnerCode <> NEW.partnerCode ) THEN
      select CONCAT(fieldNameList,'accountcontact.partnerCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerCode,'~') into newValueList;
  END IF;
   IF (OLD.contractType <> NEW.contractType ) THEN
      select CONCAT(fieldNameList,'accountcontact.contractType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contractType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contractType,'~') into newValueList;
  END IF;
   IF (OLD.jobTypeOwner <> NEW.jobTypeOwner ) THEN
      select CONCAT(fieldNameList,'accountcontact.jobTypeOwner~') into fieldNameList;
      select CONCAT(oldValueList,OLD.jobTypeOwner,'~') into oldValueList;
      select CONCAT(newValueList,NEW.jobTypeOwner,'~') into newValueList;
  END IF;
   IF (OLD.primaryPhone <> NEW.primaryPhone ) THEN
      select CONCAT(fieldNameList,'accountcontact.primaryPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.primaryPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.primaryPhone,'~') into newValueList;
  END IF;
   IF (OLD.primaryPhoneType <> NEW.primaryPhoneType ) THEN
      select CONCAT(fieldNameList,'accountcontact.primaryPhoneType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.primaryPhoneType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.primaryPhoneType,'~') into newValueList;
  END IF;
   IF (OLD.secondaryPhone <> NEW.secondaryPhone ) THEN
      select CONCAT(fieldNameList,'accountcontact.secondaryPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.secondaryPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.secondaryPhone,'~') into newValueList;
  END IF;
   IF (OLD.secondaryPhoneType <> NEW.secondaryPhoneType ) THEN
      select CONCAT(fieldNameList,'accountcontact.secondaryPhoneType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.secondaryPhoneType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.secondaryPhoneType,'~') into newValueList;
  END IF;
   IF (OLD.thirdPhone <> NEW.thirdPhone ) THEN
      select CONCAT(fieldNameList,'accountcontact.thirdPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.thirdPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.thirdPhone,'~') into newValueList;
  END IF;
   IF (OLD.thirdPhoneType <> NEW.thirdPhoneType ) THEN
      select CONCAT(fieldNameList,'accountcontact.thirdPhoneType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.thirdPhoneType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.thirdPhoneType,'~') into newValueList;
  END IF;
   IF (OLD.contactFrequency <> NEW.contactFrequency ) THEN
      select CONCAT(fieldNameList,'accountcontact.contactFrequency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contactFrequency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contactFrequency,'~') into newValueList;
  END IF;
   IF (OLD.title <> NEW.title ) THEN
      select CONCAT(fieldNameList,'accountcontact.title~') into fieldNameList;
      select CONCAT(oldValueList,OLD.title,'~') into oldValueList;
      select CONCAT(newValueList,NEW.title,'~') into newValueList;
  END IF;
   IF (OLD.salutation <> NEW.salutation ) THEN
      select CONCAT(fieldNameList,'accountcontact.salutation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.salutation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.salutation,'~') into newValueList;
  END IF;
   IF (OLD.contactLastName <> NEW.contactLastName ) THEN
      select CONCAT(fieldNameList,'accountcontact.contactLastName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contactLastName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contactLastName,'~') into newValueList;
  END IF;
 IF (OLD.leadSource <> NEW.leadSource ) THEN
      select CONCAT(fieldNameList,'accountcontact.leadSource~') into fieldNameList;
      select CONCAT(oldValueList,OLD.leadSource,'~') into oldValueList;
      select CONCAT(newValueList,NEW.leadSource,'~') into newValueList;
  END IF;
 IF (OLD.reportTo <> NEW.reportTo ) THEN
      select CONCAT(fieldNameList,'accountcontact.reportTo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reportTo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reportTo,'~') into newValueList;
  END IF;
IF (OLD.assistant <> NEW.assistant ) THEN
      select CONCAT(fieldNameList,'accountcontact.assistant~') into fieldNameList;
      select CONCAT(oldValueList,OLD.assistant,'~') into oldValueList;
      select CONCAT(newValueList,NEW.assistant,'~') into newValueList;
  END IF;
IF (OLD.assistantPhone <> NEW.assistantPhone ) THEN
      select CONCAT(fieldNameList,'accountcontact.assistantPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.assistantPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.assistantPhone,'~') into newValueList;
  END IF;
IF (OLD.email2 <> NEW.email2 ) THEN
      select CONCAT(fieldNameList,'accountcontact.email2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.email2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.email2,'~') into newValueList;
  END IF;
IF (OLD.emailType <> NEW.emailType ) THEN
      select CONCAT(fieldNameList,'accountcontact.emailType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.emailType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.emailType,'~') into newValueList;
  END IF;
IF (OLD.address <> NEW.address ) THEN
      select CONCAT(fieldNameList,'accountcontact.address~') into fieldNameList;
      select CONCAT(oldValueList,OLD.address,'~') into oldValueList;
      select CONCAT(newValueList,NEW.address,'~') into newValueList;
  END IF;
  IF (OLD.country <> NEW.country ) THEN
      select CONCAT(fieldNameList,'accountcontact.country~') into fieldNameList;
      select CONCAT(oldValueList,OLD.country,'~') into oldValueList;
      select CONCAT(newValueList,NEW.country,'~') into newValueList;
  END IF;
IF (OLD.state <> NEW.state ) THEN
      select CONCAT(fieldNameList,'accountcontact.state~') into fieldNameList;
      select CONCAT(oldValueList,OLD.state,'~') into oldValueList;
      select CONCAT(newValueList,NEW.state,'~') into newValueList;
  END IF;
IF (OLD.city <> NEW.city ) THEN
      select CONCAT(fieldNameList,'accountcontact.city~') into fieldNameList;
      select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
      select CONCAT(newValueList,NEW.city,'~') into newValueList;
  END IF;
IF (OLD.zip <> NEW.zip ) THEN
      select CONCAT(fieldNameList,'accountcontact.zip~') into fieldNameList;
      select CONCAT(oldValueList,OLD.zip,'~') into oldValueList;
      select CONCAT(newValueList,NEW.zip,'~') into newValueList;
  END IF;
IF (OLD.leadSourceOther <> NEW.leadSourceOther ) THEN
      select CONCAT(fieldNameList,'accountcontact.leadSourceOther~') into fieldNameList;
      select CONCAT(oldValueList,OLD.leadSourceOther,'~') into oldValueList;
      select CONCAT(newValueList,NEW.leadSourceOther,'~') into newValueList;
  END IF;
IF (OLD.userName <> NEW.userName ) THEN
      select CONCAT(fieldNameList,'accountcontact.userName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.userName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.userName,'~') into newValueList;
  END IF;
IF (OLD.basedAt <> NEW.basedAt ) THEN
      select CONCAT(fieldNameList,'accountcontact.basedAt~') into fieldNameList;
      select CONCAT(oldValueList,OLD.basedAt,'~') into oldValueList;
      select CONCAT(newValueList,NEW.basedAt,'~') into newValueList;
  END IF;
IF (OLD.basedAtName <> NEW.basedAtName ) THEN
      select CONCAT(fieldNameList,'accountcontact.basedAtName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.basedAtName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.basedAtName,'~') into newValueList;
  END IF;
IF ((OLD.dateOfBirth <> NEW.dateOfBirth) or (OLD.dateOfBirth is null and NEW.dateOfBirth is not null)
      or (OLD.dateOfBirth is not null and NEW.dateOfBirth is null)) THEN

       select CONCAT(fieldNameList,'accountcontact.dateOfBirth~') into fieldNameList;
        IF(OLD.dateOfBirth is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.dateOfBirth is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.dateOfBirth is not null) THEN
        	select CONCAT(oldValueList,OLD.dateOfBirth,'~') into oldValueList;
     	END IF;
      	IF(NEW.dateOfBirth is not null) THEN
        	select CONCAT(newValueList,NEW.dateOfBirth,'~') into newValueList;
      	END IF;

   END IF;

   IF ((OLD.visitedOn <> NEW.visitedOn) or (OLD.visitedOn is null and NEW.visitedOn is not null)
      or (OLD.visitedOn is not null and NEW.visitedOn is null)) THEN
       select CONCAT(fieldNameList,'accountcontact.visitedOn~') into fieldNameList;
       IF(OLD.visitedOn is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.visitedOn is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.visitedOn is not null) THEN
        	select CONCAT(oldValueList,OLD.visitedOn,'~') into oldValueList;
     	END IF;
      	IF(NEW.visitedOn is not null) THEN
        	select CONCAT(newValueList,NEW.visitedOn,'~') into newValueList;
      	END IF;
   END IF;
  IF (OLD.doNotCall <> NEW.doNotCall ) THEN
       select CONCAT(fieldNameList,'accountcontact.doNotCall~') into fieldNameList;
       IF(OLD.doNotCall = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.doNotCall = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.doNotCall = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.doNotCall = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
  IF (OLD.optedOut1 <> NEW.optedOut1 ) THEN
       select CONCAT(fieldNameList,'accountcontact.optedOut1~') into fieldNameList;
       IF(OLD.optedOut1 = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.optedOut1 = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.optedOut1 = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.optedOut1 = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.status <> NEW.status ) THEN
       select CONCAT(fieldNameList,'accountcontact.status~') into fieldNameList;
       IF(OLD.status = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.status = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.status = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.status = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
  IF (OLD.invalid1 <> NEW.invalid1 ) THEN
       select CONCAT(fieldNameList,'accountcontact.invalid1~') into fieldNameList;
       IF(OLD.invalid1 = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.invalid1 = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.invalid1 = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.invalid1 = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
  IF (OLD.optedOut2 <> NEW.optedOut2 ) THEN
       select CONCAT(fieldNameList,'accountcontact.optedOut2~') into fieldNameList;
       IF(OLD.optedOut2 = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.optedOut2 = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.optedOut2 = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.optedOut2 = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

  IF (OLD.invalid2 <> NEW.invalid2 ) THEN
       select CONCAT(fieldNameList,'accountcontact.invalid2~') into fieldNameList;
       IF(OLD.invalid2 = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.invalid2 = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.invalid2 = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.invalid2 = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
 CALL add_tblHistory (OLD.id,"accountcontact", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END $$
delimiter;