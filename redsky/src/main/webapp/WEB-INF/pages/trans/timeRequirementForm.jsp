<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  

    <script type="text/javascript">
    function saveTimeManagement(){
    	var resourceExList="";
    	<c:forEach var="timeRequirementItem" items="${timeRequirementList}" >
    	if(resourceExList==''){
         	resourceExList='${timeRequirementItem.id}'+"@ "+document.getElementById('Req'+${timeRequirementItem.id}).value+"@"+document.getElementById('ST'+${timeRequirementItem.id}).value+"@"+document.getElementById('ET'+${timeRequirementItem.id}).value;
	             }else{
         	resourceExList= resourceExList+"~"+'${timeRequirementItem.id}'+"@ "+document.getElementById('Req'+${timeRequirementItem.id}).value+"@"+document.getElementById('ST'+${timeRequirementItem.id}).value+"@"+document.getElementById('ET'+${timeRequirementItem.id}).value;
	             }
    	</c:forEach>
    	//alert(resourceExList);
    	document.getElementById('resourceExList').value =resourceExList; 
    	    	
    	 var SETimelist="";
	     var newlineid=document.getElementById('newlineIdTime12').value;
	     if(newlineid!=''){
	            var arrayLine=newlineid.split("~");
	            for(var i=0;i<arrayLine.length;i++){
		            var Req=document.getElementById('Req'+arrayLine[i]).value;
		            if(Req=='' || Req=='00:00'){
		            	Req="A";
		            }
		            var ST=document.getElementById('ST'+arrayLine[i]).value;
		            if(ST==''|| ST=='00:00'){
		            	ST="A";
		            }
		            var ET=document.getElementById('ET'+arrayLine[i]).value;
		            if(ET=='' || ET=='00:00'){
		            	ET="A";
		            }
		            if( Req=='A' && ST=='A' && ET=='A' ){		        
		            }else{
		                if(SETimelist==''){
		                	SETimelist=Req+"@"+ST+" @"+ET;
		                }else{
		                	SETimelist=SETimelist+"~"+Req+" @"+ST+" @"+ET;
		                }
		            }
	              }
	         }
       //  alert(SETimelist)
	    document.getElementById('timeManagementList').value =SETimelist;
    }
    </script>
    <table   id="dataTableO" style="margin:0px;margin-bottom:5px; " >
								<thead>
								<td class="listwhitetext" style="padding:0.2em; width:272px" >Time&nbsp;Requirements</td><td class="listwhitetext" style="padding:0.2em;">Start Time&nbsp;</td><td></td><td class="listwhitetext" style="padding:0.2em;">End Time&nbsp;</td><td></td><td class="listwhitetext" style="padding:0.2em;">Add&nbsp;Line</td><td class="listwhitetext" style="padding:0.2em;">Remove</td></tr>
								</thead>
								<tbody>
								<c:if test="${not empty timeRequirementList}">
								<c:forEach var="timeRequirementItem" items="${timeRequirementList}" varStatus="loop" >	
								<tr>																
								<td ><s:textfield cssClass="input-text" name="timeRequirementsList" value="${timeRequirementItem.timeRequirements}"  id="Req${timeRequirementItem.id}" onblur="valid(this,'special')"   maxlength="60" /></td>
								<td><s:textfield cssClass="input-text" name="startTimeList" value="${timeRequirementItem.startTime}"  id="ST${timeRequirementItem.id}" cssStyle="text-align:right"  size="10" maxlength="5" onchange="return IsTimeValidFortimeRequirement13('ST${timeRequirementItem.id}','${timeRequirementItem.startTime}'),IsTimeassigntimeairthmatic('${timeRequirementItem.id}');"  tabindex="75" /></td>
								<td><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;padding-left:2px;" >  </td>
								<td><s:textfield name="endtimeList" cssClass="input-text" value="${timeRequirementItem.endtime}"  cssStyle="text-align:right" id="ET${timeRequirementItem.id}" size="10" maxlength="5" onchange="return IsTimeValidFortimeRequirement13('ET${timeRequirementItem.id}','${timeRequirementItem.endtime}'),IsTimeassigntimeairthmatic('${timeRequirementItem.id}');" tabindex="75" /></td>
								<td><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;padding-left:2px;" >  </td>
								<c:if test="${!loop.last}">
								<td></td>
								</c:if>
								<c:if test="${loop.last}">
								<td><a><img align="middle" onclick="addRowOrigin('dataTableO');this.src='${pageContext.request.contextPath}/images/blankWhite.png';this.onclick=null;" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/addNew.png"/></a></td>
							    </c:if>
							    <td><a><img align="middle" onclick="timeRequirementRemove('${timeRequirementItem.id}');" style="margin: 0px 0px 0px 8px;" HEIGHT=14 WIDTH=14  src="${pageContext.request.contextPath}/images/deRecycle.png"/></a></td>
								</tr>
								</c:forEach>
								</c:if>							
								</tbody>
								</table>
								<c:if test="${not empty timeRequirementList}">
								<s:hidden id="timeRequirementDetailsList" value="false"/>
								</c:if>
								<c:if test="${empty timeRequirementList}">
								<s:hidden id="timeRequirementDetailsList" value="true"/>
								</c:if>