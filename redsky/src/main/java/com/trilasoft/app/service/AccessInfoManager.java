package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.AccessInfo;

public interface AccessInfoManager extends GenericManager<AccessInfo, Long>{
	public  List getAccessInfo(Long cid);
	public  List getAccessInfoForSO(Long cid);
	public  List getIdBySoID(Long cid);
}
