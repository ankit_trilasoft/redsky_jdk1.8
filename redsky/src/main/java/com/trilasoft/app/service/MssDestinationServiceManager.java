package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.MssDestinationService;

public interface MssDestinationServiceManager extends GenericManager<MssDestinationService, Long>{

}
