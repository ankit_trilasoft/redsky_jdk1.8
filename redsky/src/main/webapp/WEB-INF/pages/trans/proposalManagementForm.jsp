<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
 <title>Proposal&nbsp;Management&nbsp;Form</title>
 <meta name="heading" content="<fmt:message key='proposalManagementDetail.heading'/>"/>
 <style type="text/css">
   #mainPopup { padding-left:0px; padding-right:0px; }
	div#page { margin:0; padding:0; text-align:center; }
</style>
<script language="javascript" type="text/javascript">
function setFlagValue(){
		document.forms['proposalManagementForm'].elements['hitFlag'].value = '1';
	   }
function changeStatus(){
	document.forms['proposalManagementForm'].elements['formStatus'].value = '1';
}
	   function checkFormValue()
	   {
	   	var module=document.forms['proposalManagementForm'].elements['proposalManagement.module'].value;
	   	var status=document.forms['proposalManagementForm'].elements['proposalManagement.status'].value;
	   	var subject=document.forms['proposalManagementForm'].elements['proposalManagement.subject'].value;
	   	var corpIds=document.forms['proposalManagementForm'].elements['proposalManagement.corpId'].value;
	   	
	   		   	
	   	if(corpIds.trim()=='')
	   	{
	   		alert('CorpId is a required field.');
	   		return false;
	   	}else if(module.trim()=='')
	   	{
	   		alert('Module is a required field.');
	   		return false;
	   	}
	   	else if(subject.trim()=='')
	   	{
	   		alert('Subject is a required field.');
	   		return false;
	   	}else if(status.trim()=='')
	   	{
	   		alert('Status is a required field.');
	   		return false;
	   	}
	   	
	   }
	   
	   function onlyFloatNumsAllowed(evt){
			var keyCode = evt.which ? evt.which : evt.keyCode; 
		  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110)||( keyCode==109); 
		}
	   
	   function chkSelect(target) { 
    		  if (checkFloatWithArth('proposalManagementForm','proposalManagement.amountProposed','Invalid data in Amount Proposed') == false)  {
                      document.forms['proposalManagementForm'].elements['proposalManagement.amountProposed'].value='0'; 
             return false
           }else if(checkFloatWithArth('proposalManagementForm','proposalManagement.billTillDate','Invalid data in Amount BillTillDate') == false)  {
        	   document.forms['proposalManagementForm'].elements['proposalManagement.billTillDate'].value='0'; 
        	   return false
           }
	   }
	   </script>

	     <!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
	   
	    <script language="javascript" type="text/javascript">
			<%@ include file="/common/formCalender.js"%>
		</script> 
	   
	    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
		<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
	    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

	<!-- Modification closed here -->
</head>
<s:form id="proposalManagementForm" action="saveProposalManagement.html" method="post" validate="true" enctype="multipart/form-data">
<s:hidden name="proposalManagement.id" value="%{proposalManagement.id}"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="id1" value="<%=request.getParameter("id")%>"/> 
<s:hidden name="corpID" value="<%= request.getParameter("corpID")%>"/>
<s:hidden name="formStatus" value="" />
<c:if test="${not empty proposalManagement.fileName}">
<s:hidden name="fileFileName1" value="${proposalManagement.fileName}"/>
<s:hidden name="location1" value="${proposalManagement.location}"/>
<s:hidden name="fileFileName2" value="${proposalManagement.approvalFileName}"/>
<s:hidden name="location2" value="${proposalManagement.approvalFileLocation}"/>
</c:if>

<div id="newmnav">
	<ul>
		<li id="newmnav1" style="background:#FFF"><a class="current"><span>Proposal Details Entry</span></a></li>
		<li><a href="proposalManagements.html?true&corpID=${sessionCorpID}"><span>Proposal List</span></a></li>
	</ul>
</div>
<div class="spn" style="!margin-top:2px;">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
    <table class="" cellspacing="0" cellpadding="0" border="0" style="width:820px;">
	<tbody>
	<tr><td align="left" class="listwhitebox">
<table cellspacing="2" cellpadding="3" border="0" class="detailTabLabel">
		  <tbody>  	
		  	<tr>	
		  	<td class="listwhitetext" align="right" >Corp&nbsp;Id<font color="red" size="2">*</font></td>	  	
		  	<td colspan="8">
			<table class="" cellspacing="0" cellpadding="0" border="0" style="margin:0px;paddding:0px;">
			<tr>				
    			<td align="left"><s:select cssClass="list-menu" name="proposalManagement.corpId" list="%{distinctCorpId}" cssStyle="width:70px" headerKey="" headerValue=""/></td>
    			<td class="listwhitetext" align="right" valign="top" width="58"><fmt:message key="proposalManagement.module"/><font color="red" size="2">*</font></td>
	  			<td align="left" ><s:select cssClass="list-menu" name="proposalManagement.module" list="%{moduleReport}" cssStyle="width:180px" headerKey="" headerValue=""/> </td>
	  		</tr>
	  		</table>
	  		</td>
	  		</tr>
	  		<tr>
	  			<td align="right" class="listwhitetext"><fmt:message key="proposalManagement.clientApprover"/></td>
    			<td colspan="4"><s:textfield name="proposalManagement.clientApprover" cssStyle="width:303px;" maxlength='49' required="true" cssClass="input-text" /></td>
	  		</tr>
	  		
	  		<tr>
	  			<td align="right" class="listwhitetext"><fmt:message key="proposalManagement.projectManager"/></td>
    			<td colspan="4"><s:textfield name="proposalManagement.projectManager" cssStyle="width:303px;" maxlength='25' required="true" cssClass="input-text" /></td>
	  		</tr>
	  		<tr>
	  			<td align="right" class="listwhitetext"><fmt:message key="proposalManagement.subject"/><font color="red" size="2">*</font></td>
    			<td colspan="4"><s:textfield name="proposalManagement.subject" cssStyle="width:303px;" maxlength='79' required="true" cssClass="input-text" /></td>
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right" >Description</td>
    			<td colspan="4"><s:textarea name="proposalManagement.description" cssStyle="height:70px;width:303px;" cssClass="textarea" /></td>
	  		</tr>
	  		<tr>
	  		<td align="right" class="listwhitetext">Initiation&nbsp;Date</td>
							<td colspan="4">
							<table class="" cellspacing="0" cellpadding="0" border="0" style="margin:0px;paddding:0px;">
							<tr>
							<c:if test="${not empty proposalManagement.initiationDate}">
							<s:text id="customerFileActualSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="proposalManagement.initiationDate"/></s:text>
							<td><s:textfield cssClass="input-text" id="initiationDate" name="proposalManagement.initiationDate" value="%{customerFileActualSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="initiationDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty proposalManagement.initiationDate}">
							<td><s:textfield cssClass="input-text" id="initiationDate" name="proposalManagement.initiationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
							<img id="initiationDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td align="right" class="listwhitetext" width="11"></td>	
							<td align="right" class="listwhitetext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Approval&nbsp;Date&nbsp;&nbsp;</td>	
							<c:if test="${not empty proposalManagement.approvalDate}">
							<s:text id="customerFileActualSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="proposalManagement.approvalDate"/></s:text>
							<td><s:textfield cssClass="input-text" id="approvalDate" name="proposalManagement.approvalDate" value="%{customerFileActualSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="approvalDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty proposalManagement.approvalDate}">
							<td><s:textfield cssClass="input-text" id="approvalDate" name="proposalManagement.approvalDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
							<img id="approvalDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td align="right" class="listwhitetext" width="11"></td>	
							<td align="right" class="listwhitetext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Release&nbsp;Date&nbsp;&nbsp;</td>	
							<c:if test="${not empty proposalManagement.releaseDate}">
							<s:text id="customerFileActualSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="proposalManagement.releaseDate"/></s:text>
							<td><s:textfield cssClass="input-text" id="releaseDate" name="proposalManagement.releaseDate" value="%{customerFileActualSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="releaseDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty proposalManagement.releaseDate}">
							<td><s:textfield cssClass="input-text" id="releaseDate" name="proposalManagement.releaseDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
							<img id="releaseDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							</tr>
							</table>
							</td> 
	  		</tr>
	  		
	  		<tr>
	  			<td align="right" class="listwhitetext"><fmt:message key="proposalManagement.status"/><font color="red" size="2">*</font></td>
    			<td colspan="6">
				<table class="" cellspacing="0" cellpadding="0" border="0" style="margin:0px;paddding:0px;">
				<tr>
				<td align="left" ><s:select cssClass="list-menu" name="proposalManagement.status" list="%{statusReport}" cssStyle="width:132px" headerKey="" headerValue=""/> </td>
				<td align="left" class="listwhitetext" width="38px"></td>
				<td align="right" class="listwhitetext">Bill Till Date&nbsp;</td>
    			<td><s:textfield name="proposalManagement.billTillDate" cssStyle="text-align:right;width:79px;" maxlength='10'  onkeydown="return onlyFloatNumsAllowed(event)" onblur="chkSelect('1');" required="true" cssClass="input-text" />
    			<td align="left" class="listwhitetext" width="67px"></td>
    			<td align="right" class="listwhitetext"><fmt:message key="proposalManagement.amountProposed"/>&nbsp;&nbsp;</td>
    			<td><s:textfield name="proposalManagement.amountProposed" size="9" cssStyle="text-align:right" maxlength='8'  onkeydown="return onlyFloatNumsAllowed(event)" onblur="chkSelect('1');" required="true" cssClass="input-text" />
    			<td align="left" class="listwhitetext" width="10px"></td>
    			<td align="right" class="listwhitetext">Currency&nbsp;&nbsp;</td>
    			<td align="left" ><s:select cssClass="list-menu" name="proposalManagement.currency" list="%{baseCurrency}" cssStyle="width:60px" headerKey="" headerValue=""/> </td>
    			<td align="left" class="listwhitetext" width="10px"></td>
    			<td>Fixed&nbsp;&nbsp;<s:checkbox name="proposalManagement.isFixed"  value ="${proposalManagement.isFixed }" cssStyle="vertical-align:middle;margin:0px;"/> </td>
				</tr>
				</table>
				</td>		
	  		</tr>
	  		<tr>
	  			<td align="right" class="listwhitetext"><fmt:message key="proposalManagement.ticketNo"/></td>
    			<td colspan="4"><s:textfield name="proposalManagement.ticketNo" cssStyle="width:306px" maxlength='80' required="true" cssClass="input-text" /></td>
    		</tr>
	  		
	  		<tr>
	  		<td class="listwhitetext"  align="right" >Proposal&nbsp;to&nbsp;Upload</td>
	  		<td colspan="3"><s:file name="file" label="%{getText('proposalManagementForm.file')}"  size="33" />
			</td>
			<c:if test="${not empty proposalManagement.id}">
	  		<td><b><c:out value="${proposalManagement.fileName}"/></b>
	  		<a onclick="javascript:openWindow('ProposalResourceMgmtImageServletAction.html?id=${proposalManagement.id}&decorator=popup&popup=true',900,600);">
    		<c:if test="${not empty proposalManagement.fileName}">
    		<img src="images/downarrow.png" border="0" align="middle" style="cursor:pointer;"/></c:if></a> 
	  		</td>
	  		</c:if>
	  		</tr>
	  	 		
	  		<tr>
	  		<td class="listwhitetext"  align="right" >Approval&nbsp;to&nbsp;Upload</td>
			<td colspan="3"><s:file name="file1" label="%{getText('proposalManagementForm.file1')}"  size="33" />
			</td>
			<c:if test="${not empty proposalManagement.id}">
	  		<td><b><c:out value="${proposalManagement.approvalFileName}"/></b>
	  		<a onclick="javascript:openWindow('ProposalApprovalMgmtImageServletAction.html?id=${proposalManagement.id}&decorator=popup&popup=true',900,600);">
    		<c:if test="${not empty proposalManagement.approvalFileName}">
    		<img src="images/downarrow.png" border="0" align="middle" style="cursor:pointer;"/></c:if></a>  </td>
	  		<%-- <td><a onclick="javascript:openWindow('ProposalApprovalMgmtImageServletAction.html?id=${proposalManagementList.id}&decorator=popup&popup=true',900,600);">
    		<c:out value="${proposalManagementList.approvalFileName}" escapeXml="false"/></a></td> --%>
	  		</c:if>
	  		</tr>
	  		
	  		</tbody>
	  		</table>
	  		</td></tr></tbody></table>	  		
	  		</div>
	  		<div class="bottom-header"><span></span></div>
</div>
</div>

<table border="0">
		<tbody>
			<tr><td  width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message
					key='proposalManagement.createdOn' /></b></td>
				
				<td style="width:130px"><fmt:formatDate
					var="customerFileCreatedOnFormattedValue"
					value="${proposalManagement.createdOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="proposalManagement.createdOn"
					value="${customerFileCreatedOnFormattedValue}" /> <fmt:formatDate
					value="${proposalManagement.createdOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="right" class="listwhitetext" style="width:65px"><b><fmt:message
					key='proposalManagement.createdBy' /></b></td>
				<c:if test="${not empty proposalManagement.id}">
					<s:hidden name="proposalManagement.createdBy" />
					<td style="width:65px"><s:label name="createdBy"
						value="%{proposalManagement.createdBy}" /></td>
				</c:if>
				<c:if test="${empty proposalManagement.id}">
					<s:hidden name="proposalManagement.createdBy"
						value="${pageContext.request.remoteUser}" />
					<td style="width:65px"><s:label name="createdBy"
						value="${pageContext.request.remoteUser}" /></td>
				</c:if>				
			<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message
					key='proposalManagement.updatedOn' /></b></td>
				<fmt:formatDate var="customerFileupdatedOnFormattedValue"
					value="${proposalManagement.modifyOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="proposalManagement.modifyOn"
					value="${customerFileupdatedOnFormattedValue}" />
				<td style="width:120px"><fmt:formatDate
					value="${proposalManagement.modifyOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message
					key='billing.updatedBy' /></b></td>	
				<c:if test="${not empty proposalManagement.id}">
								<s:hidden name="proposalManagement.modifyBy"/>
								<td style="width:85px ;font-size:1em;"><s:label name="updatedBy" value="%{proposalManagement.modifyBy}"/></td>
							</c:if>
							<c:if test="${empty proposalManagement.id}">
								<s:hidden name="proposalManagement.modifyBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px;font-size:1em;"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>	
			</tr>
		</tbody>
	</table>
	

            <s:submit cssClass="cssbuttonA" method="save" key="button.save" theme="simple" onclick="setFlagValue();return checkFormValue()"/>
         	<c:if test="${not empty proposalManagement.id}">
		    <input type="button" class="cssbutton1" 
	        onclick="location.href='<c:url   value="/editProposalManagement.html?true&corpID=true"/>'" 
	        value="<fmt:message  key="button.add"/>" tabindex="88"/> 
         	</c:if>
         	<s:reset cssClass="cssbutton1" key="Reset" />
         	<c:if test="${not empty proposalManagement.id}">
         	<input type="button" class="cssbutton1" value="<fmt:message  key="button.addAndCopy"/>" onclick="location.href='<c:url   value="/addAndCopyProposalMangForm.html?true&cid=${proposalManagement.id}"/>'" style="width:100px; height:25px" />
         	</c:if> 


    			<c:set var="hitFlag" value="<%=request.getParameter("hitFlag") %>" />
				<s:hidden name="hitFlag" />
				<c:if test="${hitFlag == 1}" >
					<c:redirect url=""/>
				</c:if>	
</s:form>
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>
<script type="text/javascript"> 

try{
document.forms['proposalManagementForm'].elements['proposalManagement.currency'].focus();

<c:if test="${empty proposalManagement.id}">
   	document.forms['proposalManagementForm'].elements['proposalManagement.currency'].value="USD";
</c:if>
    
}catch(e){} 
</script>
