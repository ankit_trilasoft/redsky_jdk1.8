package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.TenancyUtilityService;


public interface TenancyUtilityServiceDao extends GenericDao<TenancyUtilityService, Long>{

}
