package com.trilasoft.app.webapp.action;

import java.util.Date;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.MssDestinationService;
import com.trilasoft.app.service.MssDestinationServiceManager;

public class MssDestinationServiceAction extends BaseAction{
	private Long id;
	private String sessionCorpID;
	private MssDestinationService mssDestinationService;
	private MssDestinationServiceManager mssDestinationServiceManager;
	
	public MssDestinationServiceAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 		
	}
	
	public String getComboList(String corpID){
		
	 	 return SUCCESS;
	}
	
	public String edit() {
		getComboList(sessionCorpID);
		if (id != null) {
			mssDestinationService = mssDestinationServiceManager.get(id);
		} else {			
			mssDestinationService = new MssDestinationService(); 
			mssDestinationService.setCreatedOn(new Date());
			mssDestinationService.setUpdatedOn(new Date());
		
		} 
		return SUCCESS;
	}
	public String save() throws Exception {
		getComboList(sessionCorpID);
		boolean isNew = (mssDestinationService.getId() == null);  
				if (isNew) { 
					mssDestinationService.setCreatedOn(new Date());
		        } 
				mssDestinationService.setCorpId(sessionCorpID);
				mssDestinationService.setUpdatedOn(new Date());
				mssDestinationService.setUpdatedBy(getRequest().getRemoteUser()); 
				mssDestinationServiceManager.save(mssDestinationService);  
			    String key = (isNew) ?"Mss Destination have been saved." :"Mss Destination have been saved." ;
			    saveMessage(getText(key)); 
				return SUCCESS;
			
	}
	public String delete(){		
		mssDestinationServiceManager.remove(id);    
	  return SUCCESS;		    
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public MssDestinationService getMssDestinationService() {
		return mssDestinationService;
	}

	public void setMssDestinationService(MssDestinationService mssDestinationService) {
		this.mssDestinationService = mssDestinationService;
	}

	public void setMssDestinationServiceManager(
			MssDestinationServiceManager mssDestinationServiceManager) {
		this.mssDestinationServiceManager = mssDestinationServiceManager;
	}

}
