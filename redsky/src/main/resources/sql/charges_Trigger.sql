DELIMITER $$
DROP trigger IF EXISTS `redsky`.`trigger_add_history_charges` $$
CREATE  trigger redsky.trigger_add_history_charges
BEFORE UPDATE on redsky.charges
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";

  IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'charges.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;
 
  IF (OLD.charge <> NEW.charge ) THEN
      select CONCAT(fieldNameList,'charges.charge~') into fieldNameList;
      select CONCAT(oldValueList,OLD.charge,'~') into oldValueList;
      select CONCAT(newValueList,NEW.charge,'~') into newValueList;
  END IF;

  IF (OLD.contract <> NEW.contract ) THEN
      select CONCAT(fieldNameList,'charges.contract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contract,'~') into newValueList;
  END IF;
  
  IF (OLD.gl <> NEW.gl ) THEN
      select CONCAT(fieldNameList,'charges.gl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gl,'~') into newValueList;
  END IF;
 
  IF (OLD.bucket <> NEW.bucket ) THEN
      select CONCAT(fieldNameList,'charges.bucket~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bucket,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bucket,'~') into newValueList;
  END IF;

    IF (OLD.divideMultiply <> NEW.divideMultiply ) THEN
      select CONCAT(fieldNameList,'charges.divideMultiply~') into fieldNameList;
      select CONCAT(oldValueList,OLD.divideMultiply,'~') into oldValueList;
      select CONCAT(newValueList,NEW.divideMultiply,'~') into newValueList;
  END IF;
  
  IF (OLD.GSTHSNCode <> NEW.GSTHSNCode ) THEN
      select CONCAT(fieldNameList,'charges.GSTHSNCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.GSTHSNCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.GSTHSNCode,'~') into newValueList;
  END IF;
  
    IF (OLD.estimateActual <> NEW.estimateActual ) THEN
      select CONCAT(fieldNameList,'charges.estimateActual~') into fieldNameList;
      select CONCAT(oldValueList,OLD.estimateActual,'~') into oldValueList;
      select CONCAT(newValueList,NEW.estimateActual,'~') into newValueList;
  END IF;

      IF (OLD.expGl <> NEW.expGl ) THEN
      select CONCAT(fieldNameList,'charges.expGl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.expGl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.expGl,'~') into newValueList;
  END IF;

        IF (OLD.item2 <> NEW.item2 ) THEN
      select CONCAT(fieldNameList,'charges.item2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.item2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.item2,'~') into newValueList;
  END IF;

          IF (OLD.item400n <> NEW.item400n ) THEN
      select CONCAT(fieldNameList,'charges.item400n~') into fieldNameList;
      select CONCAT(oldValueList,OLD.item400n,'~') into oldValueList;
      select CONCAT(newValueList,NEW.item400n,'~') into newValueList;
  END IF;
  IF ((OLD.minimumWeight <> NEW.minimumWeight) or (OLD.minimumWeight is null and NEW.minimumWeight is not null)
      or (OLD.minimumWeight is not null and NEW.minimumWeight is null)) THEN
        select CONCAT(fieldNameList,'charges.minimumWeight~') into fieldNameList;
        IF(OLD.minimumWeight is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.minimumWeight is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.minimumWeight is not null) then
            select CONCAT(oldValueList,OLD.minimumWeight,'~') into oldValueList;
        END IF;
        IF(NEW.minimumWeight is not null) then
            select CONCAT(newValueList,NEW.minimumWeight,'~') into newValueList;
        END IF;
    END IF;
         IF (OLD.otherCost <> NEW.otherCost ) THEN
      select CONCAT(fieldNameList,'charges.otherCost~') into fieldNameList;
      select CONCAT(oldValueList,OLD.otherCost,'~') into oldValueList;
      select CONCAT(newValueList,NEW.otherCost,'~') into newValueList;
  END IF;

         IF (OLD.perItem <> NEW.perItem ) THEN
      select CONCAT(fieldNameList,'charges.perItem~') into fieldNameList;
      select CONCAT(oldValueList,OLD.perItem,'~') into oldValueList;
      select CONCAT(newValueList,NEW.perItem,'~') into newValueList;
  END IF;
    IF ((OLD.perValue <> NEW.perValue) or (OLD.perValue is null and NEW.perValue is not null)
      or (OLD.perValue is not null and NEW.perValue is null)) THEN
        select CONCAT(fieldNameList,'charges.perValue~') into fieldNameList;
        IF(OLD.perValue is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.perValue is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.perValue is not null) then
            select CONCAT(oldValueList,OLD.perValue,'~') into oldValueList;
        END IF;
        IF(NEW.perValue is not null) then
            select CONCAT(newValueList,NEW.perValue,'~') into newValueList;
        END IF;
    END IF;
         IF (OLD.priceFormula <> NEW.priceFormula ) THEN
      select CONCAT(fieldNameList,'charges.priceFormula~') into fieldNameList;
      select CONCAT(oldValueList,OLD.priceFormula,'~') into oldValueList;
      select CONCAT(newValueList,NEW.priceFormula,'~') into newValueList;
  END IF;
    IF ((OLD.pricePre <> NEW.pricePre) or (OLD.pricePre is null and NEW.pricePre is not null)
      or (OLD.pricePre is not null and NEW.pricePre is null)) THEN
        select CONCAT(fieldNameList,'charges.pricePre~') into fieldNameList;
        IF(OLD.pricePre is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.pricePre is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.pricePre is not null) then
            select CONCAT(oldValueList,OLD.pricePre,'~') into oldValueList;
        END IF;
        IF(NEW.pricePre is not null) then
            select CONCAT(newValueList,NEW.pricePre,'~') into newValueList;
        END IF;
    END IF;
     IF (OLD.priceSource <> NEW.priceSource ) THEN
      select CONCAT(fieldNameList,'charges.priceSource~') into fieldNameList;
      select CONCAT(oldValueList,OLD.priceSource,'~') into oldValueList;
      select CONCAT(newValueList,NEW.priceSource,'~') into newValueList;
  END IF;
 
         IF (OLD.priceType <> NEW.priceType ) THEN
      select CONCAT(fieldNameList,'charges.priceType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.priceType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.priceType,'~') into newValueList;
  END IF;
 
    IF ((OLD.quantity2preset <> NEW.quantity2preset) or (OLD.quantity2preset is null and NEW.quantity2preset is not null)
      or (OLD.quantity2preset is not null and NEW.quantity2preset is null)) THEN
        select CONCAT(fieldNameList,'charges.quantity2preset~') into fieldNameList;
        IF(OLD.quantity2preset is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.quantity2preset is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.quantity2preset is not null) then
            select CONCAT(oldValueList,OLD.quantity2preset,'~') into oldValueList;
        END IF;
        IF(NEW.quantity2preset is not null) then
            select CONCAT(newValueList,NEW.quantity2preset,'~') into newValueList;
        END IF;
    END IF;
    
            IF (OLD.quantity2source <> NEW.quantity2source ) THEN
      select CONCAT(fieldNameList,'charges.quantity2source~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quantity2source,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quantity2source,'~') into newValueList;
  END IF;

 IF (OLD.quantity2Type <> NEW.quantity2Type ) THEN
      select CONCAT(fieldNameList,'charges.quantity2Type~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quantity2Type,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quantity2Type,'~') into newValueList;
  END IF;


IF ((OLD.quantityPreset <> NEW.quantityPreset) or (OLD.quantityPreset is null and NEW.quantityPreset is not null)
      or (OLD.quantityPreset is not null and NEW.quantityPreset is null)) THEN
        select CONCAT(fieldNameList,'charges.quantityPreset~') into fieldNameList;
        IF(OLD.quantityPreset is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.quantityPreset is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.quantityPreset is not null) then
            select CONCAT(oldValueList,OLD.quantityPreset,'~') into oldValueList;
        END IF;
        IF(NEW.quantityPreset is not null) then
            select CONCAT(newValueList,NEW.quantityPreset,'~') into newValueList;
        END IF;
    END IF;
    
 IF (OLD.quantitySource <> NEW.quantitySource ) THEN
      select CONCAT(fieldNameList,'charges.quantitySource~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quantitySource,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quantitySource,'~') into newValueList;
  END IF;


   IF (OLD.quantityType <> NEW.quantityType ) THEN
      select CONCAT(fieldNameList,'charges.quantityType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quantityType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quantityType,'~') into newValueList;
  END IF;

  IF (OLD.realPrice <> NEW.realPrice ) THEN
      select CONCAT(fieldNameList,'charges.realPrice~') into fieldNameList;
      select CONCAT(oldValueList,OLD.realPrice,'~') into oldValueList;
      select CONCAT(newValueList,NEW.realPrice,'~') into newValueList;
  END IF;

 IF (OLD.realWords <> NEW.realWords ) THEN
      select CONCAT(fieldNameList,'charges.realWords~') into fieldNameList;
      select CONCAT(oldValueList,OLD.realWords,'~') into oldValueList;
      select CONCAT(newValueList,NEW.realWords,'~') into newValueList;
  END IF;

   IF (OLD.sortGroup <> NEW.sortGroup ) THEN
      select CONCAT(fieldNameList,'charges.sortGroup~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sortGroup,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sortGroup,'~') into newValueList;
  END IF;
 
   IF (OLD.subItem <> NEW.subItem ) THEN
      select CONCAT(fieldNameList,'charges.subItem~') into fieldNameList;
      select CONCAT(oldValueList,OLD.subItem,'~') into oldValueList;
      select CONCAT(newValueList,NEW.subItem,'~') into newValueList;
  END IF;
 
   IF (OLD.systemFunction <> NEW.systemFunction ) THEN
      select CONCAT(fieldNameList,'charges.systemFunction~') into fieldNameList;
      select CONCAT(oldValueList,OLD.systemFunction,'~') into oldValueList;
      select CONCAT(newValueList,NEW.systemFunction,'~') into newValueList;
  END IF;

   IF (OLD.tariff <> NEW.tariff ) THEN
      select CONCAT(fieldNameList,'charges.tariff~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tariff,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tariff,'~') into newValueList;
  END IF;

   IF (OLD.useDiscount <> NEW.useDiscount ) THEN
      select CONCAT(fieldNameList,'charges.useDiscount~') into fieldNameList;
      select CONCAT(oldValueList,OLD.useDiscount,'~') into oldValueList;
      select CONCAT(newValueList,NEW.useDiscount,'~') into newValueList;
  END IF;

   IF (OLD.wording <> NEW.wording ) THEN
      select CONCAT(fieldNameList,'charges.wording~') into fieldNameList;
      select CONCAT(oldValueList,OLD.wording,'~') into oldValueList;
      select CONCAT(newValueList,NEW.wording,'~') into newValueList;
  END IF;

  IF (OLD.item <> NEW.item ) THEN
      select CONCAT(fieldNameList,'charges.item~') into fieldNameList;
      select CONCAT(oldValueList,OLD.item,'~') into oldValueList;
      select CONCAT(newValueList,NEW.item,'~') into newValueList;
  END IF;


   IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'charges.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;


IF (OLD.service <> NEW.service ) THEN
       select CONCAT(fieldNameList,'charges.service~') into fieldNameList;
       IF(OLD.service = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.service = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.service = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.service = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
   IF (OLD.incrementalStep <> NEW.incrementalStep ) THEN
       select CONCAT(fieldNameList,'charges.incrementalStep~') into fieldNameList;
       IF(OLD.incrementalStep = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.incrementalStep = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.incrementalStep = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.incrementalStep = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

   IF ((OLD.minimum <> NEW.minimum) or (OLD.minimum is null and NEW.minimum is not null)
      or (OLD.minimum is not null and NEW.minimum is null)) THEN
        select CONCAT(fieldNameList,'charges.minimum~') into fieldNameList;
        IF(OLD.minimum is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.minimum is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.minimum is not null) then
            select CONCAT(oldValueList,OLD.minimum,'~') into oldValueList;
        END IF;
        IF(NEW.minimum is not null) then
            select CONCAT(newValueList,NEW.minimum,'~') into newValueList;
        END IF;
    END IF;
  IF (OLD.quantity1 <> NEW.quantity1 ) THEN
      select CONCAT(fieldNameList,'charges.quantity1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quantity1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quantity1,'~') into newValueList;
  END IF;


  IF (OLD.price1 <> NEW.price1 ) THEN
      select CONCAT(fieldNameList,'charges.price1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.price1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.price1,'~') into newValueList;
  END IF;

  
  IF (OLD.extra1 <> NEW.extra1 ) THEN
      select CONCAT(fieldNameList,'charges.extra1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.extra1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.extra1,'~') into newValueList;
  END IF;

  
IF (OLD.useOptionalWording <> NEW.useOptionalWording ) THEN
       select CONCAT(fieldNameList,'charges.useOptionalWording~') into fieldNameList;
       IF(OLD.useOptionalWording = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.useOptionalWording = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.useOptionalWording = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.useOptionalWording = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

 IF (OLD.quantityItemEstimate <> NEW.quantityItemEstimate ) THEN
      select CONCAT(fieldNameList,'charges.quantityItemEstimate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quantityItemEstimate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quantityItemEstimate,'~') into newValueList;
  END IF;

 IF (OLD.quantityItemRevised <> NEW.quantityItemRevised ) THEN
      select CONCAT(fieldNameList,'charges.quantityItemRevised~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quantityItemRevised,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quantityItemRevised,'~') into newValueList;
  END IF;

 IF (OLD.quantityEstimate <> NEW.quantityEstimate ) THEN
      select CONCAT(fieldNameList,'charges.quantityEstimate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quantityEstimate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quantityEstimate,'~') into newValueList;
  END IF;


IF (OLD.quantityRevised <> NEW.quantityRevised ) THEN
      select CONCAT(fieldNameList,'charges.quantityRevised~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quantityRevised,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quantityRevised,'~') into newValueList;
  END IF;
IF (OLD.quantityEstimateSource <> NEW.quantityEstimateSource ) THEN
      select CONCAT(fieldNameList,'charges.quantityEstimateSource~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quantityEstimateSource,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quantityEstimateSource,'~') into newValueList;
  END IF;

IF (OLD.quantityRevisedSource <> NEW.quantityRevisedSource ) THEN
      select CONCAT(fieldNameList,'charges.quantityRevisedSource~') into fieldNameList;
      select CONCAT(oldValueList,OLD.quantityRevisedSource,'~') into oldValueList;
      select CONCAT(newValueList,NEW.quantityRevisedSource,'~') into newValueList;
  END IF;


IF (OLD.extraItemEstimate <> NEW.extraItemEstimate ) THEN
      select CONCAT(fieldNameList,'charges.extraItemEstimate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.extraItemEstimate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.extraItemEstimate,'~') into newValueList;
  END IF;

IF (OLD.extraItemRevised <> NEW.extraItemRevised ) THEN
      select CONCAT(fieldNameList,'charges.extraItemRevised~') into fieldNameList;
      select CONCAT(oldValueList,OLD.extraItemRevised,'~') into oldValueList;
      select CONCAT(newValueList,NEW.extraItemRevised,'~') into newValueList;
  END IF;
IF (OLD.extraEstimate <> NEW.extraEstimate ) THEN
      select CONCAT(fieldNameList,'charges.extraEstimate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.extraEstimate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.extraEstimate,'~') into newValueList;
  END IF;

IF (OLD.extraRevised <> NEW.extraRevised ) THEN
      select CONCAT(fieldNameList,'charges.extraRevised~') into fieldNameList;
      select CONCAT(oldValueList,OLD.extraRevised,'~') into oldValueList;
      select CONCAT(newValueList,NEW.extraRevised,'~') into newValueList;
  END IF;

IF (OLD.extraEstimateSource <> NEW.extraEstimateSource ) THEN
      select CONCAT(fieldNameList,'charges.extraEstimateSource~') into fieldNameList;
      select CONCAT(oldValueList,OLD.extraEstimateSource,'~') into oldValueList;
      select CONCAT(newValueList,NEW.extraEstimateSource,'~') into newValueList;
  END IF;

IF (OLD.extraRevisedSource <> NEW.extraRevisedSource ) THEN
      select CONCAT(fieldNameList,'charges.extraRevisedSource~') into fieldNameList;
      select CONCAT(oldValueList,OLD.extraRevisedSource,'~') into oldValueList;
      select CONCAT(newValueList,NEW.extraRevisedSource,'~') into newValueList;
  END IF;

   IF ((OLD.quantityEstimatePreset <> NEW.quantityEstimatePreset) or (OLD.quantityEstimatePreset is null and NEW.quantityEstimatePreset is not null)
      or (OLD.quantityEstimatePreset is not null and NEW.quantityEstimatePreset is null)) THEN
        select CONCAT(fieldNameList,'charges.quantityEstimatePreset~') into fieldNameList;
        IF(OLD.quantityEstimatePreset is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.quantityEstimatePreset is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.quantityEstimatePreset is not null) then
            select CONCAT(oldValueList,OLD.quantityEstimatePreset,'~') into oldValueList;
        END IF;
        IF(NEW.quantityEstimatePreset is not null) then
            select CONCAT(newValueList,NEW.quantityEstimatePreset,'~') into newValueList;
        END IF;
    END IF;

    
   IF ((OLD.quantityRevisedPreset <> NEW.quantityRevisedPreset) or (OLD.quantityRevisedPreset is null and NEW.quantityRevisedPreset is not null)
      or (OLD.quantityRevisedPreset is not null and NEW.quantityRevisedPreset is null)) THEN
        select CONCAT(fieldNameList,'charges.quantityRevisedPreset~') into fieldNameList;
        IF(OLD.quantityRevisedPreset is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.quantityRevisedPreset is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.quantityRevisedPreset is not null) then
            select CONCAT(oldValueList,OLD.quantityRevisedPreset,'~') into oldValueList;
        END IF;
        IF(NEW.quantityRevisedPreset is not null) then
            select CONCAT(newValueList,NEW.quantityRevisedPreset,'~') into newValueList;
        END IF;
    END IF;
 IF ((OLD.extraEstimatePreset <> NEW.extraEstimatePreset) or (OLD.extraEstimatePreset is null and NEW.extraEstimatePreset is not null)
      or (OLD.extraEstimatePreset is not null and NEW.extraEstimatePreset is null)) THEN
        select CONCAT(fieldNameList,'charges.extraEstimatePreset~') into fieldNameList;
        IF(OLD.extraEstimatePreset is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.extraEstimatePreset is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.extraEstimatePreset is not null) then
            select CONCAT(oldValueList,OLD.extraEstimatePreset,'~') into oldValueList;
        END IF;
        IF(NEW.extraEstimatePreset is not null) then
            select CONCAT(newValueList,NEW.extraEstimatePreset,'~') into newValueList;
        END IF;
    END IF;
     
   IF ((OLD.extraRevisedPreset <> NEW.extraRevisedPreset) or (OLD.extraRevisedPreset is null and NEW.extraRevisedPreset is not null)
      or (OLD.extraRevisedPreset is not null and NEW.extraRevisedPreset is null)) THEN
        select CONCAT(fieldNameList,'charges.extraRevisedPreset~') into fieldNameList;
        IF(OLD.extraRevisedPreset is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.extraRevisedPreset is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.extraRevisedPreset is not null) then
            select CONCAT(oldValueList,OLD.extraRevisedPreset,'~') into oldValueList;
        END IF;
        IF(NEW.extraRevisedPreset is not null) then
            select CONCAT(newValueList,NEW.extraRevisedPreset,'~') into newValueList;
        END IF;
    END IF;


    IF (OLD.checkBreakPoints <> NEW.checkBreakPoints ) THEN
       select CONCAT(fieldNameList,'charges.checkBreakPoints~') into fieldNameList;
       IF(OLD.checkBreakPoints = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.checkBreakPoints = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.checkBreakPoints = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.checkBreakPoints = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;


IF (OLD.basis <> NEW.basis ) THEN
      select CONCAT(fieldNameList,'charges.basis~') into fieldNameList;
      select CONCAT(oldValueList,OLD.basis,'~') into oldValueList;
      select CONCAT(newValueList,NEW.basis,'~') into newValueList;
  END IF;


     IF (OLD.commission <> NEW.commission ) THEN
      select CONCAT(fieldNameList,'charges.commission~') into fieldNameList;
      select CONCAT(oldValueList,OLD.commission,'~') into oldValueList;
      select CONCAT(newValueList,NEW.commission,'~') into newValueList;
  END IF;

      IF (OLD.storageFlag <> NEW.storageFlag ) THEN
       select CONCAT(fieldNameList,'charges.storageFlag~') into fieldNameList;
       IF(OLD.storageFlag = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.storageFlag = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.storageFlag = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.storageFlag = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

     IF (OLD.storageType <> NEW.storageType ) THEN
      select CONCAT(fieldNameList,'charges.storageType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageType,'~') into newValueList;
  END IF;
  
     IF (OLD.tariffFormulaCharges <> NEW.tariffFormulaCharges ) THEN
      select CONCAT(fieldNameList,'charges.tariffFormulaCharges~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tariffFormulaCharges,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tariffFormulaCharges,'~') into newValueList;
  END IF;
  
     IF (OLD.invoiceItem <> NEW.invoiceItem ) THEN
      select CONCAT(fieldNameList,'charges.invoiceItem~') into fieldNameList;
      select CONCAT(oldValueList,OLD.invoiceItem,'~') into oldValueList;
      select CONCAT(newValueList,NEW.invoiceItem,'~') into newValueList;
  END IF;
  
     IF (OLD.frequency <> NEW.frequency ) THEN
      select CONCAT(fieldNameList,'charges.frequency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.frequency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.frequency,'~') into newValueList;
  END IF;

 
      IF (OLD.week1 <> NEW.week1 ) THEN
       select CONCAT(fieldNameList,'charges.week1~') into fieldNameList;
       IF(OLD.week1 = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.week1 = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.week1 = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.week1 = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
      IF (OLD.week2 <> NEW.week2 ) THEN
       select CONCAT(fieldNameList,'charges.week2~') into fieldNameList;
       IF(OLD.week2 = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.week2 = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.week2 = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.week2 = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
      IF (OLD.week3 <> NEW.week3 ) THEN
       select CONCAT(fieldNameList,'charges.week3~') into fieldNameList;
       IF(OLD.week3 = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.week3 = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.week3 = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.week3 = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
      IF (OLD.week4 <> NEW.week4 ) THEN
       select CONCAT(fieldNameList,'charges.week4~') into fieldNameList;
       IF(OLD.week4 = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.week4 = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.week4 = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.week4 = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

  
      IF (OLD.week5 <> NEW.week5 ) THEN
       select CONCAT(fieldNameList,'charges.week5~') into fieldNameList;
       IF(OLD.week5 = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.week5 = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.week5 = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.week5 = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
      IF (OLD.jan <> NEW.jan ) THEN
       select CONCAT(fieldNameList,'charges.jan~') into fieldNameList;
       IF(OLD.jan = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.jan = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.jan = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.jan = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
      IF (OLD.feb <> NEW.feb ) THEN
       select CONCAT(fieldNameList,'charges.feb~') into fieldNameList;
       IF(OLD.feb = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.feb = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.feb = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.feb = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
      IF (OLD.mar <> NEW.mar ) THEN
       select CONCAT(fieldNameList,'charges.mar~') into fieldNameList;
       IF(OLD.mar = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.mar = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.mar = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.mar = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
      IF (OLD.apr <> NEW.apr ) THEN
       select CONCAT(fieldNameList,'charges.apr~') into fieldNameList;
       IF(OLD.apr = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.apr = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.apr = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.apr = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
      IF (OLD.may <> NEW.may ) THEN
       select CONCAT(fieldNameList,'charges.may~') into fieldNameList;
       IF(OLD.may = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.may = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.may = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.may = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
      IF (OLD.jun <> NEW.jun ) THEN
       select CONCAT(fieldNameList,'charges.jun~') into fieldNameList;
       IF(OLD.jun = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.jun = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.jun = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.jun = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

   
      IF (OLD.jul <> NEW.jul ) THEN
       select CONCAT(fieldNameList,'charges.jul~') into fieldNameList;
       IF(OLD.jul = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.jul = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.jul = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.jul = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

  
      IF (OLD.aug <> NEW.aug ) THEN
       select CONCAT(fieldNameList,'charges.aug~') into fieldNameList;
       IF(OLD.aug = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.aug = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.aug = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.aug = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

   
      IF (OLD.sep <> NEW.sep ) THEN
       select CONCAT(fieldNameList,'charges.sep~') into fieldNameList;
       IF(OLD.sep = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.sep = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.sep = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.sep = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
      IF (OLD.oct <> NEW.oct ) THEN
       select CONCAT(fieldNameList,'charges.oct~') into fieldNameList;
       IF(OLD.oct = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.oct = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.oct = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.oct = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

   
      IF (OLD.nov <> NEW.nov ) THEN
       select CONCAT(fieldNameList,'charges.nov~') into fieldNameList;
       IF(OLD.nov = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.nov = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.nov = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.nov = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

   
      IF (OLD.decem <> NEW.decem ) THEN
       select CONCAT(fieldNameList,'charges.decem~') into fieldNameList;
       IF(OLD.decem = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.decem = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.decem = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.decem = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;


        IF (OLD.deductionType <> NEW.deductionType ) THEN
      select CONCAT(fieldNameList,'charges.deductionType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.deductionType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.deductionType,'~') into newValueList;
  END IF;

   
      IF (OLD.week6 <> NEW.week6 ) THEN
       select CONCAT(fieldNameList,'charges.week6~') into fieldNameList;
       IF(OLD.week6 = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.week6 = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.week6 = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.week6 = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
        IF (OLD.workDay <> NEW.workDay ) THEN
      select CONCAT(fieldNameList,'charges.workDay~') into fieldNameList;
      select CONCAT(oldValueList,OLD.workDay,'~') into oldValueList;
      select CONCAT(newValueList,NEW.workDay,'~') into newValueList;
  END IF;

      IF (OLD.includeLHF <> NEW.includeLHF ) THEN
       select CONCAT(fieldNameList,'charges.includeLHF~') into fieldNameList;
       IF(OLD.includeLHF = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.includeLHF = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.includeLHF = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.includeLHF = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

   
      IF (OLD.printOnInvoice <> NEW.printOnInvoice ) THEN
       select CONCAT(fieldNameList,'charges.printOnInvoice~') into fieldNameList;
       IF(OLD.printOnInvoice = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.printOnInvoice = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.printOnInvoice = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.printOnInvoice = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;


    IF (OLD.originCountry <> NEW.originCountry ) THEN
      select CONCAT(fieldNameList,'charges.originCountry~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originCountry,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originCountry,'~') into newValueList;
  END IF;


     IF (OLD.destinationCountry <> NEW.destinationCountry ) THEN
      select CONCAT(fieldNameList,'charges.destinationCountry~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationCountry,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationCountry,'~') into newValueList;
  END IF;


     IF (OLD.mode <> NEW.mode ) THEN
      select CONCAT(fieldNameList,'charges.mode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mode,'~') into newValueList;
  END IF;


     IF (OLD.costElement <> NEW.costElement ) THEN
      select CONCAT(fieldNameList,'charges.costElement~') into fieldNameList;
      select CONCAT(oldValueList,OLD.costElement,'~') into oldValueList;
      select CONCAT(newValueList,NEW.costElement,'~') into newValueList;
  END IF;


       IF (OLD.scostElementDescription <> NEW.scostElementDescription ) THEN
      select CONCAT(fieldNameList,'charges.scostElementDescription~') into fieldNameList;
      select CONCAT(oldValueList,OLD.scostElementDescription,'~') into oldValueList;
      select CONCAT(newValueList,NEW.scostElementDescription,'~') into newValueList;
  END IF;


   IF (OLD.VATExclude <> NEW.VATExclude ) THEN
       select CONCAT(fieldNameList,'charges.VATExclude~') into fieldNameList;
       IF(OLD.VATExclude = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.VATExclude = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.VATExclude = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.VATExclude = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

   
       IF (OLD.contractCurrency <> NEW.contractCurrency ) THEN
      select CONCAT(fieldNameList,'charges.contractCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contractCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contractCurrency,'~') into newValueList;
  END IF;


       IF (OLD.twoDGridUnit <> NEW.twoDGridUnit ) THEN
      select CONCAT(fieldNameList,'charges.twoDGridUnit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.twoDGridUnit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.twoDGridUnit,'~') into newValueList;
  END IF;

       IF (OLD.multquantity <> NEW.multquantity ) THEN
      select CONCAT(fieldNameList,'charges.multquantity~') into fieldNameList;
      select CONCAT(oldValueList,OLD.multquantity,'~') into oldValueList;
      select CONCAT(newValueList,NEW.multquantity,'~') into newValueList;
  END IF;
       IF (OLD.deviation <> NEW.deviation ) THEN
      select CONCAT(fieldNameList,'charges.deviation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.deviation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.deviation,'~') into newValueList;
  END IF;

       IF (OLD.buyDependSell <> NEW.buyDependSell ) THEN
      select CONCAT(fieldNameList,'charges.buyDependSell~') into fieldNameList;
      select CONCAT(oldValueList,OLD.buyDependSell,'~') into oldValueList;
      select CONCAT(newValueList,NEW.buyDependSell,'~') into newValueList;
  END IF;

       IF (OLD.payableContractCurrency <> NEW.payableContractCurrency ) THEN
      select CONCAT(fieldNameList,'charges.payableContractCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payableContractCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payableContractCurrency,'~') into newValueList;
  END IF;

  IF ((OLD.payablePreset <> NEW.payablePreset) or (OLD.payablePreset is null and NEW.payablePreset is not null)
      or (OLD.payablePreset is not null and NEW.payablePreset is null)) THEN
        select CONCAT(fieldNameList,'charges.payablePreset~') into fieldNameList;
        IF(OLD.payablePreset is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.payablePreset is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.payablePreset is not null) then
            select CONCAT(oldValueList,OLD.payablePreset,'~') into oldValueList;
        END IF;
        IF(NEW.payablePreset is not null) then
            select CONCAT(newValueList,NEW.payablePreset,'~') into newValueList;
        END IF;
    END IF;

   
   IF (OLD.commissionable <> NEW.commissionable ) THEN
       select CONCAT(fieldNameList,'charges.commissionable~') into fieldNameList;
       IF(OLD.commissionable = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.commissionable = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.commissionable = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.commissionable = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
       
   IF (OLD.grossMargin <> NEW.grossMargin ) THEN
       select CONCAT(fieldNameList,'charges.grossMargin~') into fieldNameList;
       IF(OLD.grossMargin = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.grossMargin = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.grossMargin = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.grossMargin = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;


    IF (OLD.payablePriceType <> NEW.payablePriceType ) THEN
      select CONCAT(fieldNameList,'charges.payablePriceType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payablePriceType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payablePriceType,'~') into newValueList;
  END IF;



   IF (OLD.expensePrice <> NEW.expensePrice ) THEN
      select CONCAT(fieldNameList,'charges.expensePrice~') into fieldNameList;
      select CONCAT(oldValueList,OLD.expensePrice,'~') into oldValueList;
      select CONCAT(newValueList,NEW.expensePrice,'~') into newValueList;
  END IF;


      
   IF (OLD.countryFlexibility <> NEW.countryFlexibility ) THEN
       select CONCAT(fieldNameList,'charges.countryFlexibility~') into fieldNameList;
       IF(OLD.countryFlexibility = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.countryFlexibility = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.countryFlexibility = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.countryFlexibility = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

   
   IF (OLD.servicedetail<> NEW.servicedetail) THEN
      select CONCAT(fieldNameList,'charges.servicedetail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.servicedetail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.servicedetail,'~') into newValueList;
  END IF;

 IF (OLD.earnout<> NEW.earnout) THEN
      select CONCAT(fieldNameList,'charges.earnout~') into fieldNameList;
      select CONCAT(oldValueList,OLD.earnout,'~') into oldValueList;
      select CONCAT(newValueList,NEW.earnout,'~') into newValueList;
  END IF;


  IF (OLD.rollUpInInvoice<> NEW.rollUpInInvoice) THEN
       select CONCAT(fieldNameList,'charges.rollUpInInvoice~') into fieldNameList;
       IF(OLD.rollUpInInvoice= false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.rollUpInInvoice = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.rollUpInInvoice = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.rollUpInInvoice = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

IF (OLD.excludeSalesAdditionalCommission<> NEW.excludeSalesAdditionalCommission) THEN
       select CONCAT(fieldNameList,'charges.excludeSalesAdditionalCommission~') into fieldNameList;
       IF(OLD.excludeSalesAdditionalCommission= false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.excludeSalesAdditionalCommission= true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.excludeSalesAdditionalCommission= false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.excludeSalesAdditionalCommission= true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;


IF (OLD.status<> NEW.status) THEN
       select CONCAT(fieldNameList,'charges.status~') into fieldNameList;
       IF(OLD.status= false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.status= true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.status= false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.status= true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   
   IF (OLD.compensation<> NEW.compensation) THEN
       select CONCAT(fieldNameList,'charges.compensation~') into fieldNameList;
       IF(OLD.compensation= false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.compensation= true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.compensation= false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.compensation= true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

   CALL add_tblHistory (OLD.id,"charges", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$

DELIMITER ;
