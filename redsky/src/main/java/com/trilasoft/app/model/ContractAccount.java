package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="contractaccount")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class ContractAccount extends BaseObject{
	
	private Long id;
	private String corpID;
	private String accountCode;
	private String accountName;
	private String contract;
	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Date valueDate;
	private String accountType;
	private Double cmmFeeThreshold;
	private Double cmmFeePCT;
	private Double cmmFeeMin;
	private Double cmmFeeMax;
	private String agentCorpID;
	private String parentAccountCode;
	private String published;
	private String  vatDesc;
	private String vatPercent;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("accountCode", accountCode)
				.append("accountName", accountName)
				.append("contract", contract).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("valueDate", valueDate)
				.append("accountType", accountType)
				.append("cmmFeeThreshold", cmmFeeThreshold)
				.append("cmmFeePCT", cmmFeePCT).append("cmmFeeMin", cmmFeeMin)
				.append("cmmFeeMax", cmmFeeMax)
				.append("agentCorpID", agentCorpID)
				.append("parentAccountCode", parentAccountCode)
				.append("published", published)
				.append("vatDesc", vatDesc)
				.append("vatPercent", vatPercent).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ContractAccount))
			return false;
		ContractAccount castOther = (ContractAccount) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(accountCode, castOther.accountCode)
				.append(accountName, castOther.accountName)
				.append(contract, castOther.contract)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(valueDate, castOther.valueDate)
				.append(accountType, castOther.accountType)
				.append(cmmFeeThreshold, castOther.cmmFeeThreshold)
				.append(cmmFeePCT, castOther.cmmFeePCT)
				.append(cmmFeeMin, castOther.cmmFeeMin)
				.append(cmmFeeMax, castOther.cmmFeeMax)
				.append(agentCorpID, castOther.agentCorpID)
				.append(parentAccountCode, castOther.parentAccountCode)
				.append(published, castOther.published)
				.append(vatDesc, castOther.vatDesc)
				.append(vatPercent, castOther.vatPercent).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(accountCode).append(accountName).append(contract)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(valueDate).append(accountType)
				.append(cmmFeeThreshold).append(cmmFeePCT).append(cmmFeeMin)
				.append(cmmFeeMax).append(agentCorpID)
				.append(parentAccountCode).append(published)
				.append(vatDesc).append(vatPercent).toHashCode();
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	public Double getCmmFeeThreshold() {
		return cmmFeeThreshold;
	}
	public void setCmmFeeThreshold(Double cmmFeeThreshold) {
		this.cmmFeeThreshold = cmmFeeThreshold;
	}
	public Double getCmmFeePCT() {
		return cmmFeePCT;
	}
	public void setCmmFeePCT(Double cmmFeePCT) {
		this.cmmFeePCT = cmmFeePCT;
	}
	public Double getCmmFeeMin() {
		return cmmFeeMin;
	}
	public void setCmmFeeMin(Double cmmFeeMin) {
		this.cmmFeeMin = cmmFeeMin;
	}
	public Double getCmmFeeMax() {
		return cmmFeeMax;
	}
	public void setCmmFeeMax(Double cmmFeeMax) {
		this.cmmFeeMax = cmmFeeMax;
	}
	
	
	@Column(length=25)
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	@Column(length=100)
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	@Column(length=25)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length=25)
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column(length=25)
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=25)
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	@Column(length=100)
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}

	@Column(length=5)
	public String getAgentCorpID() {
		return agentCorpID;
	}
	public void setAgentCorpID(String agentCorpID) {
		this.agentCorpID = agentCorpID;
	}
	@Column
	public String getParentAccountCode() {
		return parentAccountCode;
	}
	public void setParentAccountCode(String parentAccountCode) {
		this.parentAccountCode = parentAccountCode;
	}
	@Column
	public String getPublished() {
		return published;
	}
	public void setPublished(String published) {
		this.published = published;
	}
	
	@Column(length=30)
	public String getVatDesc() {
		return vatDesc;
	}
	public void setVatDesc(String vatDesc) {
		this.vatDesc = vatDesc;
	}
	
	@Column(length=7)
	public String getVatPercent() {
		return vatPercent;
	}
	public void setVatPercent(String vatPercent) {
		this.vatPercent = vatPercent;
	}
}
