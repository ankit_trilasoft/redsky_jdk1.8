package com.trilasoft.app.webapp.action;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.MyMessage;
import com.trilasoft.app.service.MessageManager;
import com.trilasoft.app.service.RefMasterManager;

public class MessageAction extends BaseAction {   
	
	private MessageManager messageManager;
    private List myMessages; 
    private Long id;
    private String fromUser;
    private String loggedInUser;
    private String status;
    private MyMessage myMessage;
	private RefMasterManager refMasterManager;
    private List refMasters;
    private List users;
    private static Map<String, String> notestatus;
    private static Map<String, String> notetype;
    private static Map<String, String> notesubtype;
    private  static Map<String, String> all_user = new LinkedHashMap<String, String>();
    
    private static String countUnreadMessages;

    private String sessionCorpID;
    
    public MessageAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
    
    public void setMessageManager(MessageManager messageManager) {
        this.messageManager = messageManager;
    }
    public void setRefMasterManager(RefMasterManager refMasterManager) {
        this.refMasterManager = refMasterManager;
    }
    
    public List getMyMessages() { 
    	//System.out.println(2);
        return myMessages;   
    }   
    public List getRefMasters(){
    	return refMasters;
    } 
    
    public List getUsers(){
    	return users;
    }    
   
    public String list() { 
    	
        myMessages = messageManager.getAll();  
        //System.out.println(1);
        return SUCCESS;   
    }  
    public String getComboList(String corpId){
 	    notestatus = refMasterManager.findByParameter(corpId, "NOTESTATUS");
 	 	notetype = refMasterManager.findByParameter(corpId, "NOTETYPE");
 	 	notesubtype = refMasterManager.findByParameter(corpId, "NOTESUBTYPE");
 	 	all_user = refMasterManager.findUser(corpId, "ALL_USER");
 	 	return SUCCESS;
    }      
	public  Map<String, String> getNotetype() {
		return notetype;
	}
	public  Map<String, String> getNotesubtype() {
		return notesubtype;
	}
	public  Map<String, String> getNotestatus() {
		return notestatus;
	}   
	public Map<String, String> getAll_user() {
		return all_user;
	}
	
    public void setId(Long id) {   
        this.id = id;   
    }
    
    public void setFromUser(String fromUser) {   
        this.fromUser = fromUser;   
    }
    
    public void setLoggedInUser(String loggedInUser) {   
        this.loggedInUser = loggedInUser;   
    }
 
    public MyMessage getMyMessage() {   
        return myMessage;   
    }   
      
    public void setMyMessage(MyMessage myMessage) {   
        this.myMessage = myMessage;   
    }   
      
    public String delete() {   
        messageManager.remove(myMessage.getId());   
        saveMessage(getText("myMessage.deleted"));   
      
        return SUCCESS;   
    }   
    public String edit() throws Exception {
        if (id != null) {   
            myMessage = messageManager.get(id);   
            if( myMessage.getStatus().equals("sent")){
            	myMessage.setStatus("sent");
            }else {
            	myMessage.setStatus("Read");
            	messageManager.updateFwdStatus(myMessage.getNoteId());
            }
            save();
        }else {
            myMessage = new MyMessage();  
            myMessage.setStatus("UnRead");
        }   
      
        return SUCCESS;   
    }   
      
    public String save() throws Exception {
    	getComboList(sessionCorpID);
        if (cancel != null) {   
            return "cancel";   
        }   
      
        if (delete != null) {   
            return delete();   
        }   
      
        boolean isNew = (myMessage.getId() == null);   
        //System.out.println(isNew);
        if(!isNew){
        	
        }else{
        	myMessage.setStatus("sent");
        }
        
        myMessage.setCorpID(sessionCorpID);
    	messageManager.save(myMessage);   
        /*
        String key = (isNew) ? "myMessage.sent" : "";   
        saveMessage(getText(key));   
      */
        if (!isNew) {   
            return INPUT;   
        } else {   
        	
            return SUCCESS;   
        }   
    }  
    
    
    public String getCountUnreadMessages() {
		return countUnreadMessages;
	}

	public String search() {  
		//System.out.println(fromUser);
		myMessages = messageManager.findByFromUser(fromUser);
    	List m = messageManager.findByUnreadMessages(fromUser);
		if( m.get(0) == null ){
			countUnreadMessages = "0";
		}else {
			countUnreadMessages = ((m).get(0)).toString() ;
		}
		//System.out.println(countUnreadMessages);
		return SUCCESS;   
    }  
	
	public String searchOutbox() {  
		//System.out.println(loggedInUser);
		myMessages = messageManager.findByOutbox("Sent",loggedInUser);
    	return SUCCESS;   
    }  
}
