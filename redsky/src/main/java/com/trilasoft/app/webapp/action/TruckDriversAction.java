package com.trilasoft.app.webapp.action;
import org.apache.log4j.PropertyConfigurator;

import org.apache.log4j.Logger;

 

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.ControlExpirations;
import com.trilasoft.app.model.TruckDrivers;
import com.trilasoft.app.service.TruckDriversManager;


public class TruckDriversAction extends BaseAction{
	private TruckDrivers truckDrivers;
	private TruckDriversManager truckDriversManager;
	private Long id;
	private Long tdid;
	private String sessionCorpID;
	private String gotoPageString;
	private String partnerCode;
	private String truckNumber;
	private List truckDriverList;
	private String hitFlag;
	private List primaryFlagStatus = new ArrayList();
	private String primaryFlags;
	private String primaryFlagId;
	private String validateString;
	
	Date currentdate = new Date();
    static final Logger logger = Logger.getLogger(TruckDriversAction.class);

	public TruckDriversAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	@SkipValidation
	public String edit() {
		//getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			
		if (id != null) {
			truckDrivers = truckDriversManager.get(id);
			validateString=truckDrivers.getDriverCode();
			//states = customerFileManager.findDefaultStateList(truck.getCountry(), sessionCorpID);
		} else {
			truckDrivers = new TruckDrivers();
			//states = customerFileManager.findDefaultStateList("", sessionCorpID);
			truckDrivers.setCorpID(sessionCorpID);
			truckDrivers.setPartnerCode(partnerCode);
			truckDrivers.setTruckNumber(truckNumber);
			truckDrivers.setCreatedOn(new Date());
			truckDrivers.setUpdatedOn(new Date());
		}
		primaryFlagStatus=truckDriversManager.getPrimaryFlagStatus(truckNumber, sessionCorpID);
		if(!primaryFlagStatus.isEmpty()){
			  Iterator it= primaryFlagStatus.iterator();
				while(it.hasNext()){
					Object [] row=(Object[])it.next();
					primaryFlagId=row[0].toString();
					primaryFlags=row[1].toString();
				}
		}
		  if(primaryFlagStatus.isEmpty()){
			  primaryFlags="show";
		  }
		//driverList =truckManager.findDriverList(truck.getWarehouse(),sessionCorpID);
		//ownerOperatorList=truckManager.findOwnerOperatorList(parentId, sessionCorpID);
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String save() {
		//getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		truckDrivers.setCorpID(sessionCorpID);
		boolean isNew = (truckDrivers.getId() == null);

		if (isNew) {
			truckDrivers.setCreatedOn(new Date());
			Long i=Long.parseLong(truckDriversManager.getDriverValidation(truckDrivers.getDriverCode(), truckDrivers.getTruckNumber(),truckDrivers.getPartnerCode(), sessionCorpID).get(0).toString());
			if(i>=1){
			//primaryFlags=truckDrivers.getPrimaryDriver().toString();
			//primaryFlagId=id.toString();			
			String key1 ="Driver code already exists for this Truck.";
			saveMessage(getText(key1));
			return SUCCESS;
			}
		} 
		String newValidateString=truckDrivers.getDriverCode();
		if(!validateString.equals(newValidateString)){
		Long i=Long.parseLong(truckDriversManager.getDriverValidation(truckDrivers.getDriverCode(), truckDrivers.getTruckNumber(),truckDrivers.getPartnerCode(), sessionCorpID).get(0).toString());
		if(i>=1){
			//primaryFlags=truckDrivers.getPrimaryDriver().toString();
			//primaryFlagId=(truckDrivers.getId().toString());			
		String key1 ="Driver code already exists for this Truck.";
		saveMessage(getText(key1));
		return SUCCESS;
		}
		}
		truckDrivers.setUpdatedOn(new Date());
		truckDrivers.setUpdatedBy(getRequest().getRemoteUser());
		truckDrivers = truckDriversManager.save(truckDrivers);
		primaryFlagStatus=truckDriversManager.getPrimaryFlagStatus(truckNumber, sessionCorpID);
		if(!primaryFlagStatus.isEmpty()){
			  Iterator it= primaryFlagStatus.iterator();
				while(it.hasNext()){
					Object [] row=(Object[])it.next();
					primaryFlagId=row[0].toString();
					primaryFlags=row[1].toString();
				}
		}
		  if(primaryFlagStatus.isEmpty()){
			  primaryFlags="show";
		  }
		//states = customerFileManager.findDefaultStateList(truck.getCountry(), sessionCorpID);
		//driverList =truckManager.findDriverList(truck.getWarehouse(),sessionCorpID);
		if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			String key = (isNew) ? "Driver has been added successfully." : "Driver has been updated successfully.";
			saveMessage(getText(key));
		}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	
//	 Method for driver List
	@SkipValidation
	public String truckDriveList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		truckDriverList = truckDriversManager.findDriverList(truckNumber, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String delete(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		truckDriversManager.remove(tdid);
		hitFlag="1";
		String key2 ="Driver has been removed successfully.";
		saveMessage(getText(key2));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	/**
	 * @return the truckDrivers
	 */
	public TruckDrivers getTruckDrivers() {
		return truckDrivers;
	}
	/**
	 * @param truckDrivers the truckDrivers to set
	 */
	public void setTruckDrivers(TruckDrivers truckDrivers) {
		this.truckDrivers = truckDrivers;
	}
	/**
	 * @param truckDriversManager the truckDriversManager to set
	 */
	public void setTruckDriversManager(TruckDriversManager truckDriversManager) {
		this.truckDriversManager = truckDriversManager;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the sessionCorpID
	 */
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	/**
	 * @param sessionCorpID the sessionCorpID to set
	 */
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	/**
	 * @return the gotoPageString
	 */
	public String getGotoPageString() {
		return gotoPageString;
	}

	/**
	 * @param gotoPageString the gotoPageString to set
	 */
	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	/**
	 * @return the partnerCode
	 */
	public String getPartnerCode() {
		return partnerCode;
	}

	/**
	 * @param partnerCode the partnerCode to set
	 */
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	/**
	 * @return the truckNumber
	 */
	public String getTruckNumber() {
		return truckNumber;
	}

	/**
	 * @param truckNumber the truckNumber to set
	 */
	public void setTruckNumber(String truckNumber) {
		this.truckNumber = truckNumber;
	}

	/**
	 * @return the truckDriverList
	 */
	public List getTruckDriverList() {
		return truckDriverList;
	}

	/**
	 * @param truckDriverList the truckDriverList to set
	 */
	public void setTruckDriverList(List truckDriverList) {
		this.truckDriverList = truckDriverList;
	}

	/**
	 * @return the tdid
	 */
	public Long getTdid() {
		return tdid;
	}

	/**
	 * @param tdid the tdid to set
	 */
	public void setTdid(Long tdid) {
		this.tdid = tdid;
	}

	/**
	 * @return the hitFlag
	 */
	public String getHitFlag() {
		return hitFlag;
	}

	/**
	 * @param hitFlag the hitFlag to set
	 */
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	/**
	 * @return the primaryFlagStatus
	 */
	public List getPrimaryFlagStatus() {
		return (primaryFlagStatus!=null && !primaryFlagStatus.isEmpty())?primaryFlagStatus:truckDriversManager.getPrimaryFlagStatus(truckDrivers.getTruckNumber(), sessionCorpID);
	}

	/**
	 * @param primaryFlagStatus the primaryFlagStatus to set
	 */
	public void setPrimaryFlagStatus(List primaryFlagStatus) {
		this.primaryFlagStatus = primaryFlagStatus;
	}

	/**
	 * @return the primaryFlagId
	 */
	public String getPrimaryFlagId() {
		return primaryFlagId;
	}

	/**
	 * @param primaryFlagId the primaryFlagId to set
	 */
	public void setPrimaryFlagId(String primaryFlagId) {
		this.primaryFlagId = primaryFlagId;
	}

	/**
	 * @return the primaryFlags
	 */
	public String getPrimaryFlags() {
		return primaryFlags;
	}

	/**
	 * @param primaryFlags the primaryFlags to set
	 */
	public void setPrimaryFlags(String primaryFlags) {
		this.primaryFlags = primaryFlags;
	}

	/**
	 * @return the validateString
	 */
	public String getValidateString() {
		return validateString;
	}

	/**
	 * @param validateString the validateString to set
	 */
	public void setValidateString(String validateString) {
		this.validateString = validateString;
	}
}
