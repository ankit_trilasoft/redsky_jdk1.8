package com.trilasoft.app.webapp.action;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import org.displaytag.properties.SortOrderEnum;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Userlogfile;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.UserlogfileManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.webapp.helper.SearchCriterion;

   public class UserlogfileAction extends BaseAction  {
   private Userlogfile userlogfile;
	private UserlogfileManager  userlogfileManager;
	private List userlogfileList= new ArrayList();
	private List userPermissionList = new ArrayList();
	private List assignedSecurityList;
	private String userNamePermission;
	private String sessionCorpID;
	 private Boolean activeIps;
	private RefMasterManager refMasterManager;
	private  Map<String, String> usertype;
	
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(UserlogfileAction.class);

	
	public UserlogfileAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	} 
	
	public String list()
	{ 	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		usertype=new HashMap<String, String>(); 
		usertype = refMasterManager.findByParameter(sessionCorpID, "USER_TYPE"); 
		if(!sessionCorpID.equalsIgnoreCase("TSFT"))
		{
		usertype.remove("AGENT");
		}
		activeIps = true;
		userlogFileExt= paginateListFactory.getPaginatedListFromRequest(getRequest());
		//userlogfileList=userlogfileManager.getAll(); 
		//String key = "Please enter your search criteria below";
		//saveMessage(getText(key));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	private ExtendedPaginatedList userlogFileExt;
	private PaginateListFactory paginateListFactory;
	private PagingLookupManager pagingLookupManager;
	private Long partnerId;
	private String loginDate;
	private List logTimeList;
	
	public String search()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List activeIpsList=userlogfileManager.getexcludeIPsList(sessionCorpID); 
		usertype=new HashMap<String, String>(); 
		usertype = refMasterManager.findByParameter(sessionCorpID, "USER_TYPE");
		if(!sessionCorpID.equalsIgnoreCase("TSFT"))
		{
		usertype.remove("AGENT");
		}
		String userNameSearch="";
		String userTypeSearch="";
		String loginDateSearch="";
		String excludeIPsList="";
		try
        {  
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
             Date du = new Date();
             du = df.parse(loginDateSearch);
             df = new SimpleDateFormat("yyyy-MM-dd");
             loginDateSearch = df.format(du);
             
        } 
		catch (ParseException e)
       {
			loginDateSearch="";
       }
		if (!(userlogfile == null)) {
		boolean userName=(userlogfile.getUserName() == null);
		boolean userType = (userlogfile.getUserType() == null);
		boolean login = (userlogfile.getLogin() == null);
		if ((userlogfile.getUserName() == null)) {
			userNameSearch = "";
		} else {
			userNameSearch = userlogfile.getUserName();
		}
		if ((userlogfile.getUserType() == null)) {
			userTypeSearch = "";
		} else {
			userTypeSearch = userlogfile.getUserType();
		}
		if ((userlogfile.getLogin() == null)) {
			loginDateSearch = "";
		} else {
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		    StringBuilder loginDate = new StringBuilder(format1.format(userlogfile.getLogin()));	
			loginDateSearch = loginDate.toString();
		}
		try{
		if(!userName|| !userType || !login)
		{ 
			if(activeIps==true ){ 
			if(activeIpsList!=null && (!(activeIpsList.isEmpty()))){ 
			  excludeIPsList=activeIpsList.get(0).toString();
			  if(!excludeIPsList.equals("")){
			 excludeIPsList=excludeIPsList.trim();
			   if(excludeIPsList.indexOf("(")==0)
			    {
		    	   excludeIPsList=excludeIPsList.substring(1);
			    }
			  if(excludeIPsList.lastIndexOf(")")==excludeIPsList.length()-1)
			    {
				   excludeIPsList=excludeIPsList.substring(0, excludeIPsList.length()-1);
			    }
			  if(excludeIPsList.indexOf(",")==0)
				{
			    	excludeIPsList=excludeIPsList.substring(1);
				}
			 if(excludeIPsList.lastIndexOf(",")==excludeIPsList.length()-1)
				{
					excludeIPsList=excludeIPsList.substring(0, excludeIPsList.length()-1);
				}
			 
			}
			} else{
				excludeIPsList="";
			}
			
			userlogfileList= userlogfileManager.searchuUserLogFile(sessionCorpID,userNameSearch,userTypeSearch,loginDateSearch,excludeIPsList);
			}
			else { 
				excludeIPsList="";
				userlogfileList= 	userlogfileManager.searchuUserLogFile(sessionCorpID,userNameSearch,userTypeSearch,loginDateSearch,excludeIPsList);
			}
			} 
		}catch(Exception e){
			excludeIPsList="";
			userlogfileList= userlogfileManager.searchuUserLogFile(sessionCorpID,userNameSearch,userTypeSearch,"","");
			}
		}else{
			excludeIPsList="";
			userlogfileList= userlogfileManager.searchuUserLogFile(sessionCorpID,"","","","");
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}

	public String searchDetail(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List activeIpsList=userlogfileManager.getexcludeIPsList(sessionCorpID); 
		usertype=new HashMap<String, String>(); 
		usertype = refMasterManager.findByParameter(sessionCorpID, "USER_TYPE"); 
		if(!sessionCorpID.equalsIgnoreCase("TSFT"))
		{
		usertype.remove("AGENT");
		}
		boolean userName=(userlogfile.getUserName() == null);
		boolean userType = (userlogfile.getUserType() == null);
		boolean login = (userlogfile.getLogin() == null);
		if(!userName|| !userType || !login)
		{
			userlogFileExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
			String sort = (String) getRequest().getParameter("sort");
			if (sort == null || sort.equals("")) {
			userlogFileExt.setSortCriterion("login");
			userlogFileExt.setSortDirection(SortOrderEnum.DESCENDING);
			}
			List<SearchCriterion> searchCriteria = new ArrayList();
			if(activeIps==true ){ 
			searchCriteria.add(new SearchCriterion("userName", userlogfile.getUserName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
			searchCriteria.add(new SearchCriterion("userType", userlogfile.getUserType(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
			if (userlogfile.getLogin() != null) {
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			    StringBuilder logiDate = new StringBuilder(format1.format(userlogfile.getLogin()));	
			    String dateCriteria = "(DATE_FORMAT(login,'%Y-%m-%d') = '" + logiDate + "')";
				searchCriteria.add(new SearchCriterion("", dateCriteria, SearchCriterion.FIELD_TYPE_SQL, SearchCriterion.OPERATOR_NONE));
			    //searchCriteria.add(new SearchCriterion("login", logiDate, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
			}
			try{
			if(activeIpsList!=null && (!(activeIpsList.isEmpty()))){
				String excludeIPsList="";
			  excludeIPsList=activeIpsList.get(0).toString();
			  if(!excludeIPsList.equals("")){
			 excludeIPsList=excludeIPsList.trim();
			   if(excludeIPsList.indexOf("(")==0)
			    {
		    	   excludeIPsList=excludeIPsList.substring(1);
			    }
			  if(excludeIPsList.lastIndexOf(")")==excludeIPsList.length()-1)
			    {
				   excludeIPsList=excludeIPsList.substring(0, excludeIPsList.length()-1);
			    }
			  if(excludeIPsList.indexOf(",")==0)
				{
			    	excludeIPsList=excludeIPsList.substring(1);
				}
			 if(excludeIPsList.lastIndexOf(",")==excludeIPsList.length()-1)
				{
					excludeIPsList=excludeIPsList.substring(0, excludeIPsList.length()-1);
				}
			    String[] arrayexcludeIPs=excludeIPsList.split(",");
			    int arrayLength = arrayexcludeIPs.length;
				for(int i=0;i<arrayLength;i++)
				 {
					String IPaddress=arrayexcludeIPs[i];
					if(IPaddress.indexOf("'")==0)
					 {
						IPaddress=IPaddress.substring(1);
					 }
					if(IPaddress.lastIndexOf("'")==IPaddress.length()-1)
					 {
						IPaddress=IPaddress.substring(0, IPaddress.length()-1);
					 }
					searchCriteria.add(new SearchCriterion("IPaddress", IPaddress, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
				 }
			}
			}
			}
			catch(Exception e){ 
				searchCriteria.add(new SearchCriterion("userName", userlogfile.getUserName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				searchCriteria.add(new SearchCriterion("userType", userlogfile.getUserType(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				if (userlogfile.getLogin() != null) {
					SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				    StringBuilder logiDate = new StringBuilder(format1.format(userlogfile.getLogin()));	
				    String dateCriteria = "(DATE_FORMAT(login,'%Y-%m-%d') = '" + logiDate + "')";
					searchCriteria.add(new SearchCriterion("", dateCriteria, SearchCriterion.FIELD_TYPE_SQL, SearchCriterion.OPERATOR_NONE));
				    //searchCriteria.add(new SearchCriterion("login", logiDate, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				}  
			}
			}else { 
				searchCriteria.add(new SearchCriterion("userName", userlogfile.getUserName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				searchCriteria.add(new SearchCriterion("userType", userlogfile.getUserType(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				if (userlogfile.getLogin() != null) {
					SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				    StringBuilder logiDate = new StringBuilder(format1.format(userlogfile.getLogin()));	
				    String dateCriteria = "(DATE_FORMAT(login,'%Y-%m-%d') = '" + logiDate + "')";
					searchCriteria.add(new SearchCriterion("", dateCriteria, SearchCriterion.FIELD_TYPE_SQL, SearchCriterion.OPERATOR_NONE));
				    //searchCriteria.add(new SearchCriterion("login", logiDate, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				}  
			}
			pagingLookupManager.getAllRecordsPage(Userlogfile.class, userlogFileExt, searchCriteria);
			//userlogfileList=userlogfileManager.search(userlogfile.getUserName(), userlogfile.getUserType(), userlogfile.getLogin());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	
	}
	
	public String findUserPermission(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		userPermissionList=userlogfileManager.getPermission(sessionCorpID,userNamePermission);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
    public String findLogTime(){ 
    	//System.out.println("\n\n\n\n logTimeList"+loginDate+"user   "+userNamePermission);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	try
        {  
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
             Date du = new Date();
             du = df.parse(loginDate);
             df = new SimpleDateFormat("yyyy-MM-dd");
             loginDate = df.format(du);
             
        } 
		catch (ParseException e)
       {
			loginDate="";
       }
    	logTimeList=userlogfileManager.findLogTime(sessionCorpID,userNamePermission,loginDate);
    	//System.out.println("\n\n\n\n logTimeList"+logTimeList);
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS; 
	}
	
	public String getAssignedSecurity(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		assignedSecurityList=userlogfileManager.getAssignedSecurity(sessionCorpID, partnerId);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
		
	}
	
	public Userlogfile getUserlogfile() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return userlogfile;
	}

	public void setUserlogfile(Userlogfile userlogfile) {
		this.userlogfile = userlogfile;
	}

	

	public List getUserlogfileList() {
		return userlogfileList;
	}

	public void setUserlogfileList(List userlogfileList) {
		this.userlogfileList = userlogfileList;
	}

	public void setUserlogfileManager(UserlogfileManager userlogfileManager) {
		this.userlogfileManager = userlogfileManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Map<String, String> getUsertype() {
		return usertype;
	}

	public void setUsertype(Map<String, String> usertype) {
		this.usertype = usertype;
	}

	public ExtendedPaginatedList getUserlogFileExt() {
		return userlogFileExt;
	}

	public void setUserlogFileExt(ExtendedPaginatedList userlogFileExt) {
		this.userlogFileExt = userlogFileExt;
	}

	public PaginateListFactory getPaginateListFactory() {
		return paginateListFactory;
	}

	public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
		this.paginateListFactory = paginateListFactory;
	}

	public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
		this.pagingLookupManager = pagingLookupManager;
	}

	public Boolean getActiveIps() {
		return activeIps;
	}

	public void setActiveIps(Boolean activeIps) {
		this.activeIps = activeIps;
	}

	public List getUserPermissionList() {
		return userPermissionList;
	}

	public void setUserPermissionList(List userPermissionList) {
		this.userPermissionList = userPermissionList;
	}

	public String getUserNamePermission() {
		return userNamePermission;
	}

	public void setUserNamePermission(String userNamePermission) {
		this.userNamePermission = userNamePermission;
	}

	public List getAssignedSecurityList() {
		return assignedSecurityList;
	}

	public void setAssignedSecurityList(List assignedSecurityList) {
		this.assignedSecurityList = assignedSecurityList;
	}

	public Long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public String getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(String loginDate) {
		this.loginDate = loginDate;
	}

	public List getLogTimeList() {
		return logTimeList;
	}

	public void setLogTimeList(List logTimeList) {
		this.logTimeList = logTimeList;
	}

	

		
}
