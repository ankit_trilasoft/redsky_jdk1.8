// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`claim` ADD COLUMN `claimType` VARCHAR(100)   AFTER  `assistanceRequired` ;

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','CLAIMTYPE',100,true,now(),'UTSI_SETUP',now(),'UTSI_SETUP',1,1,'');

ALTER TABLE `redsky`.`inventorydata` CHANGE COLUMN `comment` `comment` VARCHAR(2000) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL  ;

ALTER TABLE `redsky`.`dspdetails`
 ADD COLUMN `RNT_leaseExtensionNeeded` VARCHAR(2) AFTER `VIS_visaExtensionNeeded`;
 
 ALTER TABLE `redsky`.`itemdata` ADD COLUMN `ticket` BIGINT(20) DEFAULT 0 ,
 ADD COLUMN `unitOfVolume` VARCHAR(10) ,
 ADD COLUMN `unitOfWeight` VARCHAR(10) ,
 ADD COLUMN `released` DATETIME ,
 ADD COLUMN `packId` VARCHAR(255),
 ADD COLUMN `totalVolume` DOUBLE DEFAULT 0,
 ADD COLUMN `shipNumber` VARCHAR(15) DEFAULT '',
 ADD COLUMN `hoTicket` VARCHAR(20),
 ADD COLUMN `quantityOrdered` BIGINT(20) DEFAULT 0 ,
 ADD COLUMN `quantityShipped` BIGINT(20) DEFAULT 0 ;