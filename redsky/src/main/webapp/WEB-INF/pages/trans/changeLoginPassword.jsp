<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/common/tooltip.jsp"%> 
<%@ page import="org.acegisecurity.context.SecurityContextHolder" %>
 <%@ page import="org.appfuse.model.User" %>
<%@ page import="org.acegisecurity.Authentication" %>
 <%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User quickUser = (User) auth.getPrincipal();
String quickCorpId = quickUser.getCorpID();
String quickUserType = quickUser.getUserType();
Boolean newsUpdateFlag=quickUser.isNewsUpdateFlag();
String quickFileCabinetView = quickUser.getFileCabinetView();
%>
<head>
    <title><fmt:message key="passwordChange.title"/></title>
    <meta name="heading" content="<fmt:message key='changeLoginPassword.heading'/>"/>
    <meta name="menu" content="Login"/>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
    	

 <style>
 body{ background-color:#dee9f6; text-align:left; margin:0px; padding:0px; background-image:url(images/indexbg-final.gif); background-repeat:repeat-x;}
 #logincontainer{ width:925px; position:absolute; height:417px; background-image:url(images/loginbg-opt2.png); background-repeat:no-repeat; margin-top:80px;}
 #errormessage {width:915px; padding-left:20px;}
 #leftcolumn {float:left;margin-top:50px;top:150px;width:440px;}

 #rightcolumn { width:430px; float:left; margin-top:25px;}
		
 div#branding {float: left; width: 100%; margin: 0; text-align:left; height:0px; } 
        	
div#header {
margin:0;
text-align:left;
display:none;
}
div.error, span.error, li.error, div.message {
background:#FFFFCC none repeat scroll 0 0;
border:1px solid #000000;
color:#000000;
font-family:Arial,Helvetica,sans-serif;
font-weight:normal;
margin:10px auto;
padding:3px;
text-align:left;
vertical-align:bottom;
width:500px;
position:absolute;
top:35px;
left:25%;
}
</style>
</head>
<body id="login"/>
<form method="post" id="loginForm" name="loginForm" action="resetPassword.html">
<c:set var="pwdreset" value="true" scope="session"/>
  <c:set var="userType" value="<%=quickUserType%>"/>  
   <div id="wrapper">

<div id="logincontainer">

<div id="indexlogo"><img src="${pageContext.request.contextPath}/images/indexlogo.gif" style="cursor: default;"/></div>

<div id="leftcolumn">
  <table width="360" border="0" align="right" cellpadding="0" cellspacing="0">
  <tr><td height="60px"></td></tr>
    <tr>
     <td align="center" valign="top"><img src="${pageContext.request.contextPath}/images/Cp-hint.gif" style="cursor: default;" HEIGHT=52 WIDTH=292 onclick="notExists();"/></td>
     </tr>
    </table>
</div>

<div id="rightcolumn">
  <table  style="border:1px dotted #CFCFCF;" cellspacing="0" cellpadding="0">
      <tr>
        <td height="160" valign="top" align="left">
        <table width="100%" border="0" style="font:normal 12px arial,verdana; color:#FFFFFF;" cellspacing="0" cellpadding="4">
          <tr align="center">
            <td colspan="2">&nbsp;</td>
            </tr>
          
         <tr>
            <td width="172" height="30" align="right" class="content"><label for="j_password" class="required desc">
            	<fmt:message key="label.passwordNew"/> <span class="req"><font color="#e30000">*</font></span>
        	</label></td>            
             <c:choose>
			 		  			<c:when test="${systemDefault.passwordPolicy=='true' && userType=='USER'}">	  			
			 		  			
			 		  			<div id="pswdpolicyId">		    		
			 		    		<td width="193"><s:password name="passwordNew" required="true" cssClass="input-text" onchange="passwordChanged(this)"/>
			 		  			<a onmouseout="ajax_hideTooltip();" onmouseover="ajax_showTooltip('findtoolTipForPswd.html?ajax=1&decorator=simple&popup=true',this)"><font style="font-weight:bold;">Password&nbsp;Policy</font></a></font></td>			 		  			
			 		  						 		  			
			 		  			</div>
			 		  				  			
			 		  			
			 		  			</c:when>			 		  			
			 		  			<c:otherwise>
			 		  			<td width="193"><s:password name="passwordNew" required="true" cssClass="input-text" onchange="passwordChanged(this)"/></td>			 		  			
			 		  			</c:otherwise>		  			
			 		  		</c:choose>
          </tr>
          <tr>
            <td width="500" height="30" align="right" class="content"><label for="j_confirmPassword" class="required desc">
            	<fmt:message key="label.confirmPassword"/> <span class="req"><font color="#e30000">*</font></span>
        	</label></td>
            <td width="193"><s:password name="confirmPasswordNew"  required="true" cssClass="input-text" onchange="passwordChanged(this)" /></td>
          </tr>
          <tr>
          <td width="500" height="30" align="right" class="content"><label for="j_passwordHintQues" class="required desc">
            <fmt:message key="label.passwordHintQues"/><span class="req"><font color="#e30000">*</font></span></label></td>
       		<td align="left"><s:select cssClass="list-menu" name="pwdHintQues" id="pwdHintQues" list="%{PWD_HINT}" onchange="onPageLoads();" cssStyle="width:250px"/></td>
         </tr>
          <tr>
            <td width="172" height="30" align="right" class="content"><fmt:message key="label.passwordHint"/> <span class="req"><font color="#e30000">*</font></span>
        	</td>
            <td><s:textfield cssClass="text medium" name="pwdHint" id="pwdHint" /></td>
          </tr>
          <tr>
			 <td align="right" class="content"><fmt:message key="user.email"/><font color="red" size="2">*</font></td>
			 <td align="left"><s:textfield name="user.email" cssClass="input-text" readonly="true" size="44"/></td>
		 </tr>
		 <tr>
	       <td>&nbsp;</td>
          <td><input type="submit" class="cssbutton" name="submit"  onclick="trimText(this.form);return validateFields();" value="<fmt:message key='button.submit'/>" tabindex="4" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 " >&nbsp;<input type="button" class="cssbutton" name="Clear" value="Clear" onclick="clear_fileds();" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 "></td>
          </tr>
         </table></td>
      </tr>
    </table> 
  </div>
</div>
<div class="clear"></div>
</div>
</form>

<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script language="javascript" type="text/javascript">
function clear_fileds(){
    var i;
	for(i=0;i<=3;i++){
       	document.forms['loginForm'].elements['passwordNew'].value = "";
    	document.forms['loginForm'].elements['confirmPasswordNew'].value = "";
    	document.forms['loginForm'].elements['pwdHint'].value = "";
    }
}
</script>
<script>
function validateFields(){
	var passwordNew = document.forms['loginForm'].elements['passwordNew'].value;
	var confirmPasswordNew = document.forms['loginForm'].elements['confirmPasswordNew'].value;
	var pwdHintQues = document.forms['loginForm'].elements['pwdHintQues'].value;
	var pwdHint = document.forms['loginForm'].elements['pwdHint'].value;
	
	if(passwordNew != confirmPasswordNew){
		alert('Password and Confirm Password should be the same.');
		document.forms['loginForm'].elements['confirmPasswordNew'].value = '';
		return false;
	}
	
	if(passwordNew=='' || confirmPasswordNew==''){
	alert("Fill all Mandatory Fields");
		return false;
	}
	//
	var chkvalflagReq="0";
	var sysPaswdPolicy='${systemDefault.passwordPolicy}';
	var userType= '<%=quickUserType%>';
		//alert(sysPaswdPolicy);
		if((sysPaswdPolicy!=null && sysPaswdPolicy=='true') && userType!=null && userType=='USER'){
			chkvalflagReq="1";
		}  		
		var pwd= document.forms['loginForm'].elements['passwordNew'].value;
	//var regx=/^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$/;
	var regx=/^(?=.*[A-Z]).{6,}$/;
	var charchk="+/*-<>/?!@#$()";
	var flag=false;
	//alert((regx.test(pwd)));
	if(regx.test(pwd)){
		for(var i=0;i<charchk.length;i++){
		if(pwd.indexOf(charchk.charAt(i))>=0){
			//alert("true")
			flag=true;
			break;
		}
		}
	}
	//alert(flag);
	var msgval="The entered password does not meet the requirement of the password policy. Secure password require at least:6 characters,1 capital letter (A-Z),1 symbol  {+/*-<>/?!@#$()} "
		if((chkvalflagReq=='1') && pwd!=null && pwd!='' && !(flag))
		{
			alert(msgval);
			document.forms['loginForm'].elements['passwordNew'].focus();
			return false;
		}
	var flag1=false;
	//alert((regx.test(confirmPasswordNew)));
	if(regx.test(confirmPasswordNew)){
		for(var i=0;i<charchk.length;i++){
		if(confirmPasswordNew.indexOf(charchk.charAt(i))>=0){
			//alert("true")
			flag1=true;
			break;
		}
		}
	}
	//alert(flag1);
	 msgval="The entered confirm password does not meet the requirement of the password policy. Secure password require at least:6 characters,1 capital letter (A-Z),1 symbol  {+/*-<>/?!@#$()} "
	  		if((chkvalflagReq=='1')&& confirmPasswordNew!=null && confirmPasswordNew!='' && !(flag1))
	  		{
	  			alert(msgval);
	  			document.forms['loginForm'].elements['confirmPasswordNew'].focus();
	  			return false;
	  		}
	//
	if(pwdHintQues ==' DEFAULT'){
	alert("Please select the Password Hint Question");
		return false;
	}
    if(pwdHint==''){
	alert("Fill all Mandatory Fields");
		return false;
	}
	else{
		return true;
	}
}

function onPageLoads(){
	var pwdHintQues = document.forms['loginForm'].elements['pwdHintQues'].value;
	if(pwdHintQues ==' DEFAULT'){
		document.forms['loginForm'].elements['pwdHint'].readOnly=true;
		document.forms['loginForm'].elements['pwdHint'].value='';
	}
	else{
		document.forms['loginForm'].elements['pwdHint'].readOnly=false;
	}
}
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript">
 try{
 onPageLoads();
 }
 catch(e){}
    try{
    Form.focusFirstElement(document.forms["loginForm"]);
    }
    catch(e){}
    try{
    highlightFormElements();
}
catch(e){}
    function passwordChanged(passwordField) {
    try{
        if (passwordField.name == "user.password") {
            var origPassword = "<s:property value="user.passwordNew"/>";
        } else if (passwordField.name == "user.confirmPassword") {
            var origPassword = "<s:property value="user.confirmPasswordNew"/>";
        }
        
        if (passwordField.value != origPassword) {
            createFormElement("input", "hidden",  "encryptPass", "encryptPass",
                              "true", passwordField.form);
        }
    }
catch(e){}
}

</script>
<%@ include file="/scripts/login.js"%>