package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CommissionStructureDao;
import com.trilasoft.app.model.CommissionStructure;
import com.trilasoft.app.service.CommissionStructureManager;

public class CommissionStructureManagerImpl extends GenericManagerImpl<CommissionStructure, Long> implements CommissionStructureManager{
	CommissionStructureDao commissionStructureDao;
	public CommissionStructureManagerImpl(CommissionStructureDao commissionStructureDao) {
		super(commissionStructureDao);
		this.commissionStructureDao=commissionStructureDao;
	}
	public List getCommPersentAcRange(String corpId,String contract,String chargeCode){
		return commissionStructureDao.getCommPersentAcRange(corpId,contract,chargeCode);
	}
}
