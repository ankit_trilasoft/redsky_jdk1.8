<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><fmt:message key="customerFileDetail.title" /></title>
<meta name="heading" content="<fmt:message key='customerFileDetail.heading'/>" />
<style type="text/css">	
  legend {
  font-family:arial,verdana,sans-serif;font-size:11px;font-weight:bold;margin:0;
   }
 .upper-case{
  text-transform:capitalize;
   }
  div#content {padding:0px; min-height:50px; margin-left:0px;}
  .ui-autocomplete { max-height: 250px; overflow-y: auto;
   /* prevent horizontal scrollbar */
   overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height */
  * html .ui-autocomplete {
    height: 100px;
  }
  div.error, span.error, li.error, div.message {
  text-align:left;
  }
   div#successMessages {
  text-align:center;
  }
  div.error img.icon {padding-left:25%;}
  
</style>
<style><%@ include file="/common/calenderStyle.css"%></style>
<script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/common/formCalender.js"></script> 
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-2.0.3.min.js"></script> 
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script type="text/javascript">
var http3 = getHTTPObject();
//
function showOnLoadPartnerAlert(display, code, position){
	try{
	    if(code == ''){ 
	        if(document.getElementById(position) != null){
	    		document.getElementById(position).style.display="none";
	        }
	    	ajax_hideTooltip();
	    }else if(code != ''){	    	
	    	var myArray1='${agentNotesMapJson}';
	    	var myArray = JSON.parse(myArray1);
	    	for(var key in myArray){
	            var attrName = key;
	            var attrValue = myArray[key];
	            //alert("key:::::"+attrName);
	            //alert("value:::::"+attrValue);
	            if(attrName==code){
		            if(attrValue!=''){
		            	document.getElementById(position).style.display="block";
		            }else{
		            	document.getElementById(position).style.display="none";
		            }
	            }
	        }
	    	
	    }  
	    }catch(e){} }
function showPartnerAlert(display, code, position){
	try{
	    if(code == ''){ 
	        if(document.getElementById(position) != null){
	    		document.getElementById(position).style.display="none";
	        }
	    	ajax_hideTooltip();
	    }else if(code != ''){
	    	getBAPartner(display,code,position);
	    }  }catch(e){} }
	    
function getBAPartner(display,code,position){
	var url = "findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
    http3.open("GET", url, true);
    http3.onreadystatechange = function() {handleHttpResponsePartner(display,code,position)};
    http3.send(null);
}
function handleHttpResponsePartner(display,code,position){
	if (http3.readyState == 4){
		var results = http3.responseText
       results = results.trim();
         if(results.indexOf("Nothing found to display")>1) { 
          //   exit();
          return;
         }else{
        if(results.length > 565){
       	 if(document.getElementById(position) != null){
         		document.getElementById(position).style.display="block";
       	 }
         	if(display != 'onload'){
         		getPartnerAlert(code,position);
         	}
     	}else{
     		if(document.getElementById(position) != null){
     			document.getElementById(position).style.display="none";
     		}
     		ajax_hideTooltip();
     	} } }
}
</script>
<script type="text/javascript">
function openadd()
{
animatedcollapse.ontoggle=function($, divobj, state){
	
	if (divobj.id=="address"){ //if "peter" DIV is being toggled
				  if (state=="block"){ //if div is expanded
		   animatedcollapse.show("additional");
		}
	 else{
	      animatedcollapse.hide("additional");
		 }
					  
	
	}
}
}
</script>

<script type="text/javascript"> 
function titleCase(element){
	var txt=element.value; var spl=txt.split(" "); 
	var upstring=""; for(var i=0;i<spl.length;i++){ 
	try{ 
	upstring+=spl[i].charAt(0).toUpperCase(); 
	}catch(err){} 
	upstring+=spl[i].substring(1, spl[i].length); 
	upstring+=" ";   
	} 
	element.value=upstring.substring(0,upstring.length-1); 
	 
}
</script> 
<script type="text/javascript">

$(document).ready( function(){
	
	$("select,input[type='checkbox'],input[type='button'],input[type='text'],input[type='submit'],textarea,button").each(function(i,ele) {
		
    $(this).addClass('myclass').attr('tabindex', i+1);  
    
});
})
</script>
<script type="text/JavaScript">
function callPostalCode(){
	window.open('http://www.canadapost.ca/cpo/mc/personal/postalcode/fpc.jsf', '_blank');
}

function addAndCopyWithFamilyCustomerFile(){
	<configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
	<c:set var="closedCompanyDivision" value="Y" />
    </configByCorp:fieldVisibility>
    <c:if test="${closedCompanyDivision=='Y'}">
    if('${customerFile.companyDivision}'!=''){
   	findClosedCompanyDivisionForCopy('${customerFile.companyDivision}'); 
    }
	</c:if> 
	 <c:if test="${closedCompanyDivision!='Y'}">	
	 window.location.href = 'addAndCopyFamilyCustomerFile.html?cid=${customerFile.id}&flagForCopyFamily=Y';
</c:if>
}

function findClosedCompanyDivisionForCopy(companyCode){ 
	 new Ajax.Request('/redsky/findClosedCompanyDivisionAjax.html?ajax=1&companyCode='+companyCode+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      if(response.trim()=="false"){
			    	  window.location.href = 'addAndCopyFamilyCustomerFile.html?cid=${customerFile.id}&flagForCopyFamily=Y';  
			       }else{
			    	  alert('You can not copy this customerfile, As Company Division is closed.')
			    	  return false;  
			    	 
			      }
			    },
			    onFailure: function(){ 
				    }
			  });
}
</script> 

</head>
<s:hidden name="noteFor" value="CustomerFile" />
<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>
<s:hidden name="fileID" id ="fileID" value="%{customerFile.id}" />
<c:set var="fileID" value="%{customerFile.id}"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>

<s:form id="customerFileForm" name="customerFileForm" action="saveCustomerFile" onsubmit="return submit_form();return saveValidation()" method="post" validate="true" >
<c:set var="statusChange" value="N"/>
<configByCorp:fieldVisibility componentId="component.customerfile.field.status.change">
<c:set var="statusChange" value="Y"/>
</configByCorp:fieldVisibility>
<c:set var="mandatorySource" value="N" />
<configByCorp:fieldVisibility componentId="component.customerfile.field.source">
<c:set var="mandatorySource" value="Y"/>
</configByCorp:fieldVisibility>
<s:hidden  name="accountCodeValidationFlag"  value="${accountCodeValidationFlag}"/>	
<s:hidden  name="oldCustomerFileAccountCode"  value="${customerFile.accountCode}"/>
<s:hidden  name="oldCustomerFileBilltoCode"  value="${customerFile.billToCode}"/>	
<s:hidden value="${customerFile.lastName}" name="CompanyLastName" />
<s:hidden value ="${QuotesNumber}" name="QuotesNumber"/>	
<div id="Layer1" onkeydown="changeStatus();" style="width:100%;">
<s:hidden name="formStatus" value="" id="formStatus"/>
<s:hidden name="isSOExtract"/>
<s:hidden name="regUpdate"/>
<s:hidden name="updaterIntegration"/>
<s:hidden name="firstNameFlag"/>
<s:hidden name="lastNameFlag"/>
<s:hidden name="isPrivatePartyFlag"/>
<s:hidden name="flagForPrivateBillToCode"/>
<s:hidden name="surveyDateChange"/>
<s:hidden name ="changeLangugeConfirmation"/>
<s:hidden name="mandatorySource" value="${mandatorySource}"/> 
<s:if test="hasFieldErrors()">
</s:if>
<c:if test="${customerFile.job =='RLO'}">
<s:hidden cssClass="input-text" name="customerFile.prefSurveyDate1" id="prefSurveyDate1" />							
<s:hidden cssClass="input-text" name="customerFile.prefSurveyDate2" id="prefSurveyDate2"/>
<s:hidden cssClass="input-text" name="customerFile.prefSurveyTime1" id="prefSurveyTime1" />							
<s:hidden cssClass="input-text" name="customerFile.prefSurveyTime2" id="prefSurveyTime2"/>
<s:hidden cssClass="input-text" name="customerFile.prefSurveyTime3" id="prefSurveyTime3" />							
<s:hidden cssClass="input-text" name="customerFile.prefSurveyTime4" id="prefSurveyTime4"/>
</c:if>
		<configByCorp:fieldVisibility componentId="component.field.ServiceOrder.checkCompanyBAA">
		<s:hidden name="checkCompanyDivBA" value="YES"/>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.field.SO.Extarct">
	     <s:hidden name="isDecaExtract" value="1"/>
	    </configByCorp:fieldVisibility>
	    <configByCorp:fieldVisibility componentId="component.field.forAllJob.showCordinator">
		<s:hidden name="coordinatorForAllJob" value="YES"/>
		</configByCorp:fieldVisibility>		
		<configByCorp:fieldVisibility componentId="component.tab.customerFile.salesMan">
		<s:hidden name="updateSalesMan" value="YES"/>
		</configByCorp:fieldVisibility>
		<s:hidden name="customerFile.surveyEmailLanguage" />
		<s:hidden name="customerFile.moveType" />
		<s:hidden name="customerFile.cportalEmailLanguage" />
		<s:hidden name="customerFile.ugwIntId" />
		<s:hidden  name="customerFile.partnerEntitle" value="${customerFile.partnerEntitle}"/>
		<s:hidden  name="customerFile.homeCountryCode" />
	    <s:hidden name="HomeCountryFlex3Value" id="HomeCountryFlex3Value" />
		<s:hidden name="reset" id="reset" value=""/>
		<s:hidden name="customerFile.id" value="%{customerFile.id}" />
		<s:hidden name="customerFile.parentAgent"  />
		<s:hidden name="customerFile.isNetworkGroup"  value="${customerFile.isNetworkGroup}" />
		<s:hidden name="customerFile.contractType"  value="${customerFile.contractType}" />
		<s:hidden name="customerFile.orderAction" />
		<s:hidden name="statusA"/>
		<s:hidden name="contractValidFlag" id="contractValidFlag" value="Y"/>
		<s:hidden name="contractStartYear" id="contractStartYear"/>
		<s:hidden name="contractEndYear" id="contractEndYear"/>
		<s:hidden name="orderInitiationASMLForDaysClick" id="orderInitiationASMLForDaysClick"/>
		<s:hidden name="customerFile.isNetworkRecord" />
		<s:hidden name="customerFile.bookingAgentSequenceNumber" />
		<s:hidden name="customerFile.servicePet" />	
		<s:hidden name="custJobType" value="<%=request.getParameter("custJobType") %>" />
		<s:hidden name="contractBillCode" value="<%=request.getParameter("contractBillCode") %>" />
		<s:hidden name="custCreatedOn" value="<%=request.getParameter("custCreatedOn") %>" />
		<s:hidden name="custStatus"/>
		<s:hidden name="oldCustStatus" value="${customerFile.status}"/>
		<s:hidden name="customerFile.corpID" />
		<s:hidden name="customerFile.surveyReferenceNumber" />
		<s:hidden name="customerFile.sourceCode" />
		<s:hidden name="customerFile.orgLocId" />
		<s:hidden name="customerFile.destLocId" />
		<s:hidden name="customerFile.taskId" /> 
		<s:hidden name="customerFile.orderIntiationStatus" />
		<s:hidden name="customerFile.jimExtract" />
	    <s:hidden name="stdAddDestinState" />
	      <s:hidden name="stdAddOriginState" />
	    <s:hidden name="checkConditionForOriginDestin" />
		<s:hidden name="id" value="<%=request.getParameter("id") %>" />
		<s:hidden name="gotoPageString" id="gotoPageString" value="" />
		<s:hidden name="idOfWhom" value="${customerFile.id}" />
		 <s:hidden name="originCountryFlex3Value"  id="originCountryFlex3Value"/>
		 <s:hidden name="DestinationCountryFlex3Value" id="DestinationCountryFlex3Value" />
		<s:hidden id="countNotes" name="countNotes" value="<%=request.getParameter("countNotes") %>"/>
		<s:hidden id="countOriginNotes" name="countOriginNotes" value="<%=request.getParameter("countOriginNotes") %>"/>
		<s:hidden id="countDestinationNotes" name="countDestinationNotes" value="<%=request.getParameter("countDestinationNotes") %>"/>
		<s:hidden id="countSurveyNotes" name="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>"/>
		<s:hidden id="countVipNotes" name="countVipNotes" value="<%=request.getParameter("countVipNotes") %>"/>
		<s:hidden id="countBillingNotes" name="countBillingNotes" value="<%=request.getParameter("countBillingNotes") %>"/>
		<s:hidden id="countSpouseNotes" name="countSpouseNotes" value="<%=request.getParameter("countSpouseNotes") %>"/>
		<s:hidden id="countContactNotes" name="countContactNotes" value="<%=request.getParameter("countContactNotes") %>"/>
		<s:hidden id="countCportalNotes" name="countCportalNotes" value="<%=request.getParameter("countCportalNotes") %>"/>
		<s:hidden id="countEntitlementNotes" name="countEntitlementNotes" value="<%=request.getParameter("countEntitlementNotes") %>"/>
		<c:set var="countNotes" value="<%=request.getParameter("countNotes") %>" />
		<c:set var="countOriginNotes" value="<%=request.getParameter("countOriginNotes") %>" />
		<c:set var="countDestinationNotes" value="<%=request.getParameter("countDestinationNotes") %>" />
		<c:set var="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>" />
		<c:set var="countVipNotes" value="<%=request.getParameter("countVipNotes") %>" />
		<c:set var="countBillingNotes" value="<%=request.getParameter("countBillingNotes") %>"/>
		<c:set var="countSpouseNotes" value="<%=request.getParameter("countSpouseNotes") %>"/>
		<c:set var="countContactNotes" value="<%=request.getParameter("countContactNotes") %>"/>
		<c:set var="countCportalNotes" value="<%=request.getParameter("countCportalNotes") %>"/>
		<c:set var="countEntitlementNotes" value="<%=request.getParameter("countEntitlementNotes") %>"/>
		<s:hidden name="dCountry" />
		<s:hidden name="oCountry" />
		<s:hidden name="emailTypeFlag" />
		<s:hidden name="emailTypeVOERFlag" />
		<s:hidden name="emailTypeBOURFlag" />
		<s:hidden name="emailSurveyFlag" />
		<s:hidden name="emailSetupValue" />
		<s:hidden name="count" />
		<s:hidden name="moveCount" />
		<s:hidden name="customerFile.lastRunMMTime" />
	    <s:hidden name="accountSearchValidation"/>
		<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
		<c:if test="${not empty customerFile.welcomeEmailOn}">
			<s:text id="customerFileWelcomeEmailOnFormattedValue" name="${FormDateValue}">
			 <s:param name="value" value="customerFile.welcomeEmailOn" /></s:text>
			<s:hidden  name="customerFile.welcomeEmailOn" value="%{customerFileWelcomeEmailOnFormattedValue}" /> 
	 	</c:if>
		<c:set var="visibilityEntitlements" value="false"/>
		<configByCorp:fieldVisibility componentId="component.field.VisibilityEntitlements.show">
		<c:set var="visibilityEntitlements" value="true"/>
		</configByCorp:fieldVisibility>
		<s:hidden name="visibilityEntitlements" value="${visibilityEntitlements}"/>
		<s:hidden name="imfEntitlements.id" />
		<s:hidden name="imfEntitlements.sequenceNumber" value="${customerFile.sequenceNumber }" />
		<c:set var="agentClassificationShow" value="N"/>
		<configByCorp:fieldVisibility componentId="component.partner.agentClassification.show">
		 <c:set var="agentClassificationShow" value="Y"/>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.contract.reloJob.tenancyBilling">
			<s:hidden name="makeCompetitiveMadatory" value="YES"/>
		</configByCorp:fieldVisibility>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	   <c:if test="${validateFormNav == 'OK'}">
	<c:choose>
			<c:when test="${gotoPageString == 'gototab.serviceorder' }">
				<c:redirect url="/customerServiceOrders.html?id=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.raterequest' }">
				<c:redirect url="/customerRateOrders.html?id=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.surveys' }">
				<c:redirect url="/surveysList.html?id1=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.accountpolicy' }">
				<c:redirect url="/showAccountPolicy.html?id=${customerFile.id}&jobNumber=${customerFile.sequenceNumber}&code=${customerFile.billToCode}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.imfEntitlements' }">
				<c:redirect url="/editImfEntitlement.html?cid=${customerFile.id}" />
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
		</c:if>
		<c:set var="from" value="<%=request.getParameter("from") %>"/>
		<c:set var="field" value="<%=request.getParameter("field") %>"/>
		<s:hidden name="field" value="<%=request.getParameter("field") %>" />
		<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
		<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
    <s:hidden name="jobNumber" value="%{customerFile.sequenceNumber}"/>
	<c:set var="jobNumber" value="<%=request.getParameter("sequenceNumber") %>" />
	<s:set name="reportss" value="reportss" scope="request"/> 
	<c:if test="${not empty customerFile.id}">
		<div id="newmnav">
		  <ul>
		  	<sec-auth:authComponent componentId="module.tab.customerFile.customerFileTab">
		    	<li id="newmnav1" style="background:#FFF "><a href="customerFiles.html" class="current"><span>Customer File</span></a></li>		
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.customerFile.serviceOrderTab">
		    	<c:if test="${usertype=='ACCOUNT' || usertype=='AGENT'}">
		    			<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		    				<li><a href="customerServiceOrders.html?id=${customerFile.id}"><span>Service Order</span></a></li>
		    			</c:if>
		    			<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
		    				<li><a href="customerServiceOrders.html?id=${customerFile.id}"><span>Quotes</span></a></li>
		    			</c:if>
		    	</c:if>
		    	<c:if test="${usertype!='ACCOUNT' && usertype!='AGENT'}">
		    			<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		    				<li><a onmouseover="completeTimeString();" onclick="trimTextOnTabChange(this);setReturnString('gototab.serviceorder');return IsValidTime('none');"><span>Service Order</span></a></li>
		    			</c:if>
		    			<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
		    				<li><a onmouseover="completeTimeString();" onclick="trimTextOnTabChange(this);setReturnString('gototab.serviceorder');return IsValidTime('none');"><span>Quotes</span></a></li>
		    			</c:if>
		    	</c:if>
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.script.form.corpSalesScript">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}"><span>Service Order</span></a></li>		  
			</sec-auth:authComponent>
		   <%-- <sec-auth:authComponent componentId="module.tab.customerFile.rateRequestTab">
		    	<configByCorp:fieldVisibility componentId="component.tab.customerFile.rateRequestTab">
		    			<li><a onmouseover="completeTimeString();" onclick="trimTextOnTabChange(this);setReturnString('gototab.raterequest');return IsValidTime();"><span>Rate Request</span></a></li>
		    	</configByCorp:fieldVisibility>
		    </sec-auth:authComponent>--%>
		    <sec-auth:authComponent componentId="module.tab.customerFile.SurveysTab">
		    	<configByCorp:fieldVisibility componentId="component.tab.customerFile.SurveysTab">		    		
		    		<li><a onmouseover="completeTimeString();" onclick="trimTextOnTabChange(this);setReturnString('gototab.surveys');return IsValidTime();"><span>Surveys</span></a></li>
		    	</configByCorp:fieldVisibility>
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.customerFile.accountPolicyTab">
		    	<configByCorp:fieldVisibility componentId="component.tab.customerFile.AccountPolicy">		    	
		    			<li><a onmouseover="completeTimeString();" onclick="trimTextOnTabChange(this);setReturnString('gototab.accountpolicy');return IsValidTime();"><span>Account Policy</span></a></li>
		    	</configByCorp:fieldVisibility>
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    	</c:if>
		    	<c:if test="${not empty customerFile.id && mmValidation =='Yes'}"> 
		    		<li><a href="inventoryDetailsForm.html?cid=${customerFile.id}"><span>Inventory Details</span></a></li>
		    	</c:if>
		    </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.customerFile.serviceOrderTab">
		    <c:if test="${ usertype=='AGENT' && surveyTab}">
		  		 <li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    </c:if>
		     </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.customerFile.reportTab">
		    <configByCorp:fieldVisibility componentId="component.tab.customerFile.Reports">
		    <c:if test="${customerFile.moveType=='Quote'}">
		    	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&reportModule=quotation&preferredLanguage=${customerFile.customerLanguagePreference}&reportSubModule=quotation&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		   	</c:if>
		   	<c:if test="${customerFile.moveType!='Quote'}">
		    	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&reportModule=customerFile&preferredLanguage=${customerFile.customerLanguagePreference}&reportSubModule=customerFile&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		   	</c:if>
		    </configByCorp:fieldVisibility>
		    </sec-auth:authComponent> 
		    
		    <sec-auth:authComponent componentId="module.tab.customerFile.auditTab">
		   	    <c:if test="${not empty customerFile.id}"> 
		    <li>
              <a onclick="window.open('auditList.html?id=${customerFile.id}&tableName=customerfile&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')">
              <span>Audit</span></a></li>
		    </c:if>
		    <c:if test="${empty customerFile.id}">
		    	<li><a><span>Audit</span></a></li>
		    </c:if>
		    </sec-auth:authComponent>
		    <configByCorp:fieldVisibility componentId="component.tab.task.taskplanning">
		    <c:if test="${ usertype=='AGENT' || usertype=='USER'}">
		    <c:if test="${not empty customerFile.id && customerFile.job !='RLO'}"> 
		    <li><a href="taskPlanningList.html?cid=${customerFile.id}"><span>Task Planning</span></a></li>
		    </c:if>
		    </c:if>
		    </configByCorp:fieldVisibility>
		    <c:if test="${usertype=='USER'}">
			    <configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
			  		<li><a href="findEmailSetupTemplateByModuleNameCF.html?cid=${customerFile.id}"><span>View Emails</span></a></li>
			  	</configByCorp:fieldVisibility>
		  	</c:if> 
		  </ul>
		</div><div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;"></div>
	</c:if>	
	<c:if test="${empty customerFile.id}">
		<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a href="customerFiles.html" class="current"><span>Customer File</span></a></li>
		    <li><a onclick="notExists()"><span>Service Orders</span></a></li>
		   <%-- <li><a onclick="notExists()"><span>Rate Request</span></a></li>--%>
		    <li><a onclick="notExists()"><span>Surveys</span></a></li>
		    <li><a onclick="notExists()"><span>Account Policy</span></a></li> 
		  	<li><a onclick="notExists()"><span>Forms</span></a></li>  
		  </ul>
		</div><div class="spn">&nbsp;</div>
	</c:if>
<div id="content" align="center">
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px;padding:0px;">
		<tbody>
			<tr>
				<td>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;padding:0px;" >
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center" style="cursor:default; ">&nbsp;&nbsp;Customer Details
</td>
<td width="28" valign="top" class="headtab_bg" style="cursor:default; "></td>
<td class="headtab_bg_center" style="cursor:default; ">
<a href="javascript:animatedcollapse.hide(['address', 'alternative', 'billing', 'entitlement','removal','additional','cportal','family','benefits'])">Collapse All<img src="${pageContext.request.contextPath}/images/section_contract.png" HEIGHT=14 WIDTH=14 align="top"></a> | <a href="javascript:animatedcollapse.show(['address', 'alternative', 'billing', 'entitlement','removal','additional','cportal','family','benefits'])">Expand All<img src="${pageContext.request.contextPath}/images/section_expanded.png" HEIGHT=14 WIDTH=14 align="top"></a>
</td>
<td class="headtab_right">
</td>
</tr>
</table>
				<jsp:include flush="true" page="customerFileCustomerDetails.jsp"></jsp:include>
				</td>
				</tr>
				<tr>
					<td height="10" width="100%" align="left" style="margin: 0px">
  				<div  onClick="javascript:animatedcollapse.toggle('address'); openadd() " style="margin: 0px">
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Address Details
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
						</div>
						<div id="address" class="switchgroup1">
			<jsp:include flush="true" page="customerFileAddressDetail.jsp"></jsp:include>
										</div>
										</td>
									</tr>
									
<jsp:include flush="true" page="customerFileSecForm.jsp"></jsp:include>  <!-- Calendar Modifications are also available in this file (Kunal) -->
<div class="bottom-header"><span></span></div>
</div>
	</div>	
				<table border="0">
					<tbody>
						<tr>
							<td align="left" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${customerFile.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${customerFile.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{customerFile.createdBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${customerFile.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${customerFile.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.updatedBy"/>
								<td style="width:120px"><s:label name="updatedBy" value="%{customerFile.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
				</div>
<s:hidden name="componentId" value="customerFile" />
<sec-auth:authComponent componentId="module.script.form.corpAccountScript"> 
<configByCorp:fieldVisibility componentId="component.accportal.editableFields">
<s:submit cssClass="cssbuttonA" method="save"  id="saveIkea" name="saveIkea" key="button.save" onclick="return enableAccPortalElements();getValueEnt3();trimText(this.form);"/>
<s:hidden name="hitFlag" />
		<c:if test="${not empty customerFile.id}">
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/editCustomerFile.html?id=${customerFile.id}&moveTypeFlag=${moveTypeFlag}&flagForCopyFamily=${flagForCopyFamily}&accountCodeValidationFlag=${accountCodeValidationFlag}"  />
		</c:if></c:if>

</configByCorp:fieldVisibility>
</sec-auth:authComponent>
<sec-auth:authComponent componentId="module.script.form.agentScript">
	<s:submit cssClass="cssbuttonA" method="save" key="button.save" onmouseover="completeTimeString();" onclick="enableElements();getValueEnt3();trimText(this.form);return IsValidTime('save');" />
	<s:reset cssClass="cssbutton1" key="Reset" />
	<s:hidden name="hitFlag" />
		<c:if test="${not empty customerFile.id}">
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/editCustomerFile.html?id=${customerFile.id}&moveTypeFlag=${moveTypeFlag}&flagForCopyFamily=${flagForCopyFamily}&accountCodeValidationFlag=${accountCodeValidationFlag}"  />
		</c:if></c:if>
</sec-auth:authComponent>
<sec-auth:authComponent componentId="module.customerFileList.edit" replacementHtml=""> 	
<s:submit cssClass="cssbuttonA" method="save" key="button.save" onmouseover="completeTimeString();" onclick="getValueEnt3();trimText(this.form);return IsValidTime('save');" />
				<c:if test="${not empty customerFile.id}">
		    <input type="button" class="cssbutton1" 
	        onclick="location.href='<c:url   value="/editCustomerFile.html"/>'" 
	        value="<fmt:message  key="button.add"/>" /> 
	    </c:if>  
		<s:hidden name="hitFlag" />
		<c:if test="${not empty customerFile.id}">
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/editCustomerFile.html?id=${customerFile.id}&moveTypeFlag=${moveTypeFlag}&flagForCopyFamily=${flagForCopyFamily}&accountCodeValidationFlag=${accountCodeValidationFlag}"  />
		</c:if></c:if>
<!-- 		<input type="submit" name="saveBtn" value="Save" style="width:87px; height:30px" />  -->
<s:hidden name="submitCnt" value="<%=request.getParameter("submitCnt") %>"/>
<s:hidden name="submitType" value="<%=request.getParameter("submitType") %>"/>
<c:set var="submitType" value="<%=request.getParameter("submitType") %>"/>
	<c:choose>
		<c:when test="${submitType == 'job'}">
			<s:reset type="button" cssClass="cssbutton1" key="Reset" onclick="fillAccountFlagReset()"/>
		</c:when>
		<c:otherwise>
		<c:if test="${not empty customerFile.id}">
			<s:reset type="button" cssClass="cssbutton1" key="Reset" onclick="findResetCompanyDivisionByBookAg();resetFamilyDetails();changeReset();disabledSODestinBtnReset();disabledSOBtnReset(); newFunctionForCountryState(); getContractReset();getResetCoord();getEntitleRest1();setVipImageReset();setAssignmentReset();fillAccountFlagReset();" />
		</c:if>
		<c:if test="${empty customerFile.id}">
		<s:reset type="button" cssClass="cssbutton1" key="Reset" onclick="findResetCompanyDivisionByBookAg();resetbuttondisabled();changeReset();setVipImageReset();setAssignmentReset();setStateReset();fillAccountFlagReset();" />
		</c:if>
		</c:otherwise>
	 </c:choose>
    
	 <c:if test="${customerFile.controlFlag!='G' }">
	 <c:if test="${not empty customerFile.id}">
		    <input type="button" class="cssbutton1" 
	        onclick="location.href='<c:url   value="/addAndCopyCustomerFile.html?cid=${customerFile.id}&flagForCopyFamily=Y"/>'" 
	        value="Add And Copy without Family Details " style="width:220px; height:25px" /> 
	    </c:if>  
	   <c:if test="${not empty customerFile.id}">
		    <input type="button" class="cssbutton1" 
	        onclick="addAndCopyWithFamilyCustomerFile();" 
	        value="Add And Copy with Family Details" style="width:199px; height:25px" /> 
	    </c:if>
	    </c:if>    
</sec-auth:authComponent>
 <configByCorp:fieldVisibility componentId="component.field.CustomerFile.MOL">	
<c:if test="${not empty customerFile.id}">
		<c:if test="${customerFile.controlFlag == 'C' && customerFile.moveType=='Quote'}">
			        <input type="button" class="cssbutton1" 
			        onclick="sendToMOL('Starline')" 
			        value="Send Quotation Number to Starline MOL" style="width:250px; height:25px" tabindex="83"/>
			        <input type="button" class="cssbutton1" 
			        onclick="sendToMOL('Highland')" 
			        value="Send Quotation Number to Highland MOL" style="width:250px; height:25px" tabindex="84"/>
	    </c:if>  
	    </c:if>
</configByCorp:fieldVisibility>		
<s:hidden name="customerFile.statusNumber" />	
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="customerFile.controlFlag" />	
<s:text id="customerFileSystemDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.systemDate" /></s:text>
<td valign="top"><s:hidden name="customerFile.systemDate" value="%{customerFileSystemDateFormattedValue}" /></td>							
<s:hidden name="sendEmail" />
<c:if test="${parentAgentForOrderinitiation=='true'}">
<s:hidden name="removalRelocationService.isIta60" />
<s:hidden name="removalRelocationService.isIta1m3" />
<s:hidden name="removalRelocationService.isIta2m3" />
<s:hidden name="removalRelocationService.isIta3m3" />
<s:hidden name="removalRelocationService.isFtExpat20" />
<s:hidden name="removalRelocationService.isFtExpat40" />
<s:hidden name="removalRelocationService.isKgExpat30" />
<s:hidden name="removalRelocationService.isTransportAllow" />
<s:hidden name="removalRelocationService.isFtSingle20" />
<s:hidden name="removalRelocationService.isFtFamily40" />
<s:hidden name="removalRelocationService.isStorage" />
<s:hidden name="removalRelocationService.isOrientation8" />
<s:hidden name="removalRelocationService.isOrientation16" />
<s:hidden name="removalRelocationService.isOrientation24" />
<s:hidden name="removalRelocationService.isHomeSearch8" />
<s:hidden name="removalRelocationService.isHomeSearch16" />
<s:hidden name="removalRelocationService.isHomeSearch24" />
<s:hidden name="removalRelocationService.isHomeFurnished" />
<s:hidden name="removalRelocationService.isHomeUnfurnished" />
<s:hidden name="removalRelocationService.isUsdollar" />
<s:hidden name="removalRelocationService.isDollar" />
<s:hidden name="removalRelocationService.isOtherCurrency" />
<s:hidden name="removalRelocationService.isSchoolSearch8" />
<s:hidden name="removalRelocationService.isSchoolSearch16" />
<s:hidden name="removalRelocationService.isSchoolSearch24" />
<s:hidden name="removalRelocationService.isSelfInSearch8" />
<s:hidden name="removalRelocationService.isSelfInSearch16" />
<s:hidden name="removalRelocationService.isSelfInSearch24" />
<s:hidden name="removalRelocationService.id" />
<s:hidden name="removalRelocationService.customerFileId" />
<s:hidden name="removalRelocationService.corpId" />
<s:hidden name="removalRelocationService.usdollarAmounts" />
<s:hidden name="removalRelocationService.dollarAmounts" />
<s:hidden name="removalRelocationService.otherCurrencyAmounts" />
<s:hidden name="removalRelocationService.otherServicesRequired" />
<s:hidden name="removalRelocationService.ftExpat40ExceptionalItems" />
<s:hidden name="removalRelocationService.kgExpat30ExceptionalItems" />
<s:hidden name="removalRelocationService.transportAllowExceptionalItems" />
<s:hidden name="removalRelocationService.orientationTrip" />
<s:hidden name="removalRelocationService.homeSearch" />
<s:hidden name="removalRelocationService.settlingInAssistance" />
<s:hidden name="removalRelocationService.schoolSearch" />
<s:hidden name="removalRelocationService.authorizedDays" />
<s:hidden name="removalRelocationService.additionalNotes" />
<s:hidden name="removalRelocationService.furnished" />
<s:hidden name="removalRelocationService.unFurnished" />
<s:hidden name="removalRelocationService.entryVisa" />
<s:hidden name="removalRelocationService.longTermStayVisa" />
<s:hidden name="removalRelocationService.workPermit" />
</c:if>
 <s:hidden name="SNumber" value="%{customerFile.sequenceNumber}"/>
 <c:set var="SNumber" value="${customerFile.sequenceNumber}" />
        <%
        String value=(String)pageContext.getAttribute("SNumber") ;
        Cookie cookie = new Cookie("TempnotesId",value);
        cookie.setMaxAge(3600);
        response.addCookie(cookie);
        %>
<s:hidden name="parentAgentForOrderinitiation" id="parentAgentForOrderinitiation"/>
<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
<c:set var="idOfTasks" value="${customerFile.id}" scope="session"/>
<c:set var="tableName" value="customerfile" scope="session"/>
<c:if test="${empty customerFile.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty customerFile.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<input type="hidden" name="encryptPass" value="true" />
<s:hidden key="user.id"/>
<s:hidden key="user.version"/>
</s:form>

<%-- Script shifted from top on 14-Sep-2012 --%>
		<%@ include file="/common/CustomerFileJavaScript.js"%>
<script type="text/javascript">
	try{  	
	document.getElementById('status1').focus();
  	}catch(e){}

  	function isSoExtFlag(){
  	    document.forms['customerFileForm'].elements['isSOExtract'].value="yes";
  	  }
  	function isUpdaterFlag(){
  	    document.forms['customerFileForm'].elements['updaterIntegration'].value="yes";
  	  }
	  
	  /**
	  Method is calling on change of First Name to set firstNameFlag value as yes
	  so that before updating workticket on CF save we can check First Name is
	  changed or not to improve save method performance
	  */
	function setFirstNameFlag(){
  	    document.forms['customerFileForm'].elements['firstNameFlag'].value="yes";
  	  }
	 /**
	  Method is calling on change of First Name to set lastNameFlag value as yes
	  so that before updating workticket on CF save we can check Last Name is
	  changed or not to improve save method performance
	  */
	function setLastNameFlag(){
  	    document.forms['customerFileForm'].elements['lastNameFlag'].value="yes";
  	  }
	<c:if test="${not empty customerFile.id}">
	
	function checkRegUpdate(){
		var corpID = document.forms['customerFileForm'].elements['customerFile.corpID'].value;
		  if(corpID=='JKMS') {
			  document.forms['customerFileForm'].elements['regUpdate'].value="yes";
		  }
  	  }
	
	function lastNameValidation()
	{
		var orders = document.forms['customerFileForm'].elements['QuotesNumber'].value;
		if(orders>0)
			{
		if (confirm("Changing Last name will update service order/work ticket. Do you want to continue?") == true) {
	       return true;
	    } else {
	    	var test1=document.forms['customerFileForm'].elements['CompanyLastName'].value;
	    	var test2=document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	    	document.forms['customerFileForm'].elements['customerFile.lastName'].value=test1;
	      
	    }
			}
	}
    </c:if>
  	function surveyDateChng(){
  		 document.forms['customerFileForm'].elements['surveyDateChange'].value="yes";
  	}
  	try{  	
  		<c:if test="${ usertype=='USER'}">
  		var  permissionTest  = "";
  		<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
  		permissionTest="14";
  		</sec-auth:authComponent>
  		if(permissionTest==''){
  		window.onload =function ModifyPlaceHolder () {		 
  	        var input = document.getElementById ("customerFileBookingAgentName");
  		     input.placeholder = "Booking Agent Search";
  		     var input1 = document.getElementById ("originAgentName");
  		     input1.placeholder = "Origin Agent Search";
  		     var input2 = document.getElementById ("billToNameId");
  		     input2.placeholder = "Bill To Name Search";
  		     var input3 = document.getElementById ("accountNameIdCF");
  		     input3.placeholder = "Account Name Search";  		   
  	    }
  		}
  	    </c:if>
  	}catch(e){}  	 	
  		var lan='${customerFile.customerLanguagePreference}';
  		if(lan=='' || lan==null){
			document.forms['customerFileForm'].elements['customerFile.customerLanguagePreference'].value='en';
  		}
  		
  		function changeLanguage(){
  			<c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices!='N/A'}">
  			var lan='${customerFile.customerLanguagePreference}';
  			var orders = document.forms['customerFileForm'].elements['QuotesNumber'].value;
  			if(orders>0)
  				{
  			if (confirm("Program will change Inclusions and Exclusions language in all S/o of this customer file.") == true) {
  				document.forms['customerFileForm'].elements['changeLangugeConfirmation'].value='Yes';
  		       return true;
  		    } else {
  		    	document.forms['customerFileForm'].elements['customerFile.customerLanguagePreference'].value=lan;
  		      
  		    }
  				}
  			</c:if>
  		}
  		
</script>
<script type="text/javascript">
function getContactForHr()
{
	try{
		var codeForHr=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
		if(codeForHr!=''){
		var url = 'searchUserContactCf.html?decorator=popup&popup=true&codeForHr='+codeForHr+'&myFileFor=CF';
		window.openWindow(url);
		}
	}catch(e){}
}
showAddressImage();
</script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>