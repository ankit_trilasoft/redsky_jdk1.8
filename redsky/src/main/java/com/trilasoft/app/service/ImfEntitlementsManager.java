package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ImfEntitlements;

public interface ImfEntitlementsManager extends GenericDao<ImfEntitlements,Long>{
	public List findById(Long id);
	public List findMaximumId();
	public List getImfEntitlements(String sequenceNumber);
	
	public List getImfEstimate();
	public List getImfEstimateList();
	public List getImfEstimateActual();
	
	public List getImfEstimateAgent();
	public List getImfEstimateDown();
	public void getImfEstimateListUpdate(String sequenceNumber);
	public void getImfEstimateUpdate(String shipNumber);
	public void getImfEstimateActualUpdate(String shipNumber);
	public List getImfEstimateSscar();
	public List getImfEstimateTsto();
	public List getImfEstimateTbkstg();
	public List getImfEstimateSshist();
	public List checkById(String seqNum);
	public List getImfEstimateTstghist();
	public List officeDocMove(String corpID);
	public List docExtract(String corpID);
	public List findDosSCAC(String sessionCorpID);
	public List officeDocMoveNew(String corpID);
  }
