<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="quoteList.title"/></title>   
    <meta name="heading" content="<fmt:message key='quoteList.heading'/>"/>   
	
</head>

<c:set var="buttons">
	
    <input type="button" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editPartnerQuote.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>
<s:form id="quoteListForm">
<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		
	</tbody>
</table>
<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
			<!--  
				<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
				    <tbody><tr>
				      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-left.jpg'/>"/></td>
				      <td id="searchLabelCenter" width="90"><div align="center" class="content-head">Partner Quote For</div></td>
				      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-right.jpg'/>"/></td>
				      <td></td>
				    </tr>
				</tbody></table>
				<table>
					<tbody>
						<tr>
							<td>
							    <s:textfield name="customerFile.lastName" required="true" cssClass="text medium"/>
							</td>
							<td>
							    <s:textfield name="customerFile.firstName" required="true" cssClass="text medium"/>
							</td>
						</tr>
					</tbody>
				</table>
				-->
				
				<s:set name="partnerQuotes" value="partnerQuotes" scope="request"/>  
				<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
				    <tbody><tr>
				      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-left.jpg'/>"/></td>
				      <td id="searchLabelCenter" width="90"><div align="center" class="content-head">List</div></td>
				      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-right.jpg'/>"/></td>
				      <td></td>
				    </tr>
				</tbody></table>
				<display:table name="partnerQuotes" class="table" requestURI="" id="partnerQuoteList" export="true" defaultsort="2" pagesize="10" decorator="org.displaytag.decorator.TotalTableDecorator" >   
				    <display:column property="id" sortable="true" href="editPartnerQuote.html"    
				        paramId="id" paramProperty="id" titleKey="partnerQuote.id" />
				        
				     <display:column property="corpID" sortable="true" titleKey="partnerQuote.corpID"/>
				      
				    <display:column property="quote" sortable="true" titleKey="partnerQuote.quote"/>
				    <display:column property="quoteType" sortable="true" titleKey="partnerQuote.quoteType"/>
				    <display:column property="quoteStatus" sortable="true" titleKey="partnerQuote.quoteStatus" />
				     
				    <display:setProperty name="paging.banner.item_name" value="partnerQuote"/>   
				    <display:setProperty name="paging.banner.items_name" value="partnerQuotes"/>   
				  
				    <display:setProperty name="export.excel.filename" value="PartnerQuote List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="PartnerQuote List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="PartnerQuote List.pdf"/>   
				</display:table>  
			</td>
		</tr>
	</tbody>
</table>
</div>
<a href="editNewQuote.html?id=<%=request.getParameter("id") %> "><input type="button" value="Add" /></a>
<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
<c:if test="${empty customerFile.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty customerFile.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
</s:form>

<script type="text/javascript">   
    highlightTableRows("partnerQuoteList");   
</script>
