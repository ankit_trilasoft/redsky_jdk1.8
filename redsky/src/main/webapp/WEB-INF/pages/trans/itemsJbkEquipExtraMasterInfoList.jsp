<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>  
    <title>Material And Equipment List</title>
	    <meta name="heading" content="Material And Equipment List"/>
	<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>    
		<style>
span.pagelinks {
    display: block;
    font-size: 0.85em;
    margin-bottom: 5px;
    margin-top: -9px;
    padding: 2px 0;
    text-align: right;
    width: 99%;
}
</style>		
</head>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
 <s:form id="itemsJbkEquipListForm" action="searchItemsJbkEquips" method="post" > 
 <s:set name="materialsList" value="materialsList" scope="request"/> 
</s:form>
<s:form id="assignItemsForm" action="updatestatusitem" method="post">
</s:form>
<script type="text/javascript">
    highlightTableRows("itemsJbkEquipList"); 
    Form.focusFirstElement($("itemsJbkEquipListForm")); 
</script> 
<script type="text/javascript">
		function search(){		
		var itemType = document.forms['gridForm'].elements['itemType'].value;		
		var url = "searchItemsJbkEquipsMasterInfo.html";
    	document.forms['gridForm'].action = url;
		document.forms['gridForm'].submit();
		}
function clear_fields()
{		
	document.forms['gridForm'].elements['branch'].value="";	
	document.forms['gridForm'].elements['division'].value="";	
	document.forms['gridForm'].elements['itemType'].value="";
	document.forms['gridForm'].elements['resource'].value="";		
			
}

function checkKey()
{
    if (window.event.keyCode == 13)
    {
        alert("you hit return!");
    }
}

function whouseReset(tmp)
{		
	document.forms['gridForm'].elements['cngwhouse'].value='true';	
			
}

function resourceReset(tmp)
{		
	document.forms['gridForm'].elements['cngres'].value='true';	
			
}
function resetFields()
{	
	var t1=document.forms['gridForm'].elements['cngres'].value;	
	var t2=document.forms['gridForm'].elements['cngwhouse'].value;
	if(t1=="true"){		
	}else{		
		document.forms['gridForm'].elements['equipMaterialsLimits.resource'].disabled=true;		
	}
	if(t2=="true"){
			
		
	}else if(t2=="false"){		
		document.forms['gridForm'].elements['equipMaterialsLimits.branch'].disabled=true;
	}
			
}

function closePopup(){
	document.getElementById('saveBox').style.display="none";
  	document.getElementById('searchBox111').style.display="block";
  	
}

function keyPressed(e){
	//alert("evennnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
	  var code;
	  if(window.event){ //IE
	   code = e.keyCode;
	  }
	  else{ //other browsers
	    code = e.which;
	  }
	  //check, for example, if the Enter key was pressed (code 13)
	  if(code == 13){
		  if(document.forms['gridForm'].elements['chkSearch'].value=='0'){
		  var itemType = document.forms['gridForm'].elements['itemType'].value;		
			var url = "searchItemsJbkEquipsMasterInfo.html";
	    	document.forms['gridForm'].action = url;
			document.forms['gridForm'].submit();
			document.forms['gridForm'].elements['chkSearch'].value='0';
			 return false;
		  }else if(document.forms['gridForm'].elements['chkSearch'].value=='1'){
			  document.forms['gridForm'].elements['chkSearch'].value='0';			 
			  return goForValidate();
	  }else{
	  }
	  }
	  else{
		  return true;
			
	  }
	}

</script> 
<body >
<s:form id="gridForm" name="gridForm" action="saveEquipMaterialLimit" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    <s:hidden name="cngwhouse" value="false"/>
    <s:hidden name="backUp" value=""/>
    <s:hidden name="backUpOnSave" value=""/>
	<s:hidden name="cngres" value="false" />
	<s:hidden name="listData" />
	<s:hidden name="listFieldNames" />	
	<s:hidden name="listFieldEditability" />	
	<s:hidden name="listFieldTypes" />		
	<s:hidden name="listIdField" value="id"/>	
	<s:hidden name="listIdFieldType" value="long"/>	
	<s:hidden name="showAdd" value="Yes" />	
	<s:hidden name="chkSearch" value="0" />		
	<c:set var="id" value="<%=request.getParameter("id") %>" scope ="session"/>
	<s:hidden name="id"  value="${id}"/>
<div class="spn">&nbsp;</div>	
<table width="100%" style="margin:0px;"><tr><td style="margin:0px;"><%-- <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%> --%></td></tr></table>
</td>	

		<div class="spnblk">&nbsp;</div>
</table>
<div id="searchBox111" >
<div id="content" align="center" >
<div id="liquid-round-top">
    <div class="top" style="margin-top:0px;!margin-top: 0px; "><span></span></div>
    <div class="center-content" >
 <table class="table" width="97%" >
		<thead>
			<tr>
				<th>Warehouse</th>
				<th>Division</th>
				<th>Category Type</th>
				<th>Item</th>
				<th></th>
			</tr>
		</thead>	
			<tbody>
				<tr>			
							    	
						    <td ><s:select cssClass="list-menu" name="branch" list="%{house}" cssStyle="width:150px" headerKey="" headerValue="" /></td>						   
					  		<td ><s:select cssClass="list-menu" name="division" list="%{comapnyDivisionCodeDescp}" cssStyle="width:150px" headerKey="" headerValue="" /></td>																 
					  		<td ><s:select cssClass="list-menu" name="itemType" list="{'M','E'}" cssStyle="width:150px" headerKey="" headerValue="" /></td>					  		
					  		<td width="235px"><s:textfield name="resource"  cssClass="input-text" cssStyle="width:150px;" readonly="false"  /></td>
								
					<td>
					    <input type="button"  class="cssbutton1" value="Search" style="!margin-bottom: 15px;"  onclick="search()"/>   
					    <input type="button"  class="cssbutton1" value="Clear" style="!margin-bottom: 15px;"  onclick="clear_fields();"/>  
					</td>   
				    
				</tr>
							
								
								
		</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div> 
</div>
</div>
<!-- New save section -->
<div style="display:none" id="saveBox">
<s:hidden name="equipMaterialsLimits.id"/>
<s:hidden name="equipMaterialsCost.id"/>
<s:hidden name="equipMaterialsCost.equipMaterialsId"/>
<%-- <s:hidden name="equipMaterialsLimits.qty"/> --%>
<s:hidden name="hub" value="<%=request.getParameter("hub")%>" />
<s:hidden name="operationsResourceLimits.hubId" value="${hub}"/>
<s:hidden name="dummsectionName" />
<div id="Layer1" style="width:100%"> 
<div id="newmnav">
		  <ul>		  		
		  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Resource Limit<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  		
		  </ul>
</div><div class="spn">&nbsp;</div>
<div style="padding-bottom:0px;!margin-bottom:3px;"></div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%">
	  <tbody>
		  <tr>
		  	<td align="left" class="listwhitetext">
			  	<table class="detailTabLabel" border="0" cellpadding="2" >
					  <tbody>  	
					  	<tr>
					  		<td align="left" height="3px"></td>
					  	</tr>
					  	
						<tr>
							<td align="left" width="15px">
							<td align="right" width="">Division<font color="red" size="2">*</font></td>
					  		<td width="235px"><s:select cssClass="list-menu" name="equipMaterialsLimits.division" list="%{comapnyDivisionCodeDescp}" cssStyle="width:200px" headerKey="" headerValue="" /></td><!--
						    <td align="right" width="">Category<font color="red" size="2">*</font></td>
						    <td width="250px"><s:select cssClass="list-menu" name="equipMaterialsLimits.category" list="%{invCategMap}" cssStyle="width:200px" headerKey="" headerValue="" /></td>
                    		--><td align="right" width="">Available&nbsp;Quantity<font color="red" size="2">*</font></td>
						    <td width="250px"><s:textfield cssClass="input-text" name="equipMaterialsLimits.qty" maxlength="15" cssStyle="text-align:right;width:120px;" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
				    		<%-- <img align="left" class="openpopup" width="17" height="20" style="float:right;!float:none;!vertical-align:bottom;" onclick="assignResources();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /> --%></td>	
					  	    <td align="right" width="">Purchase&nbsp;Price<font color="red" size="2">*</font></td>
						    <td width="250px"><s:textfield cssClass="input-text" name="equipMaterialsCost.unitCost" cssStyle="text-align:right;width:120px;"  /></td>
					  	    
					  	</tr>
					  	<tr>
					  		<td align="left" width="15px">
					  		 <td align="right" width="">Category<font color="red" size="2">*</font></td>
						    <td width="250px"><s:select cssClass="list-menu" name="equipMaterialsLimits.category" list="%{invCategMap}" cssStyle="width:200px" headerKey="" headerValue="" /></td>
					  		<td align="right" width="">Min&nbsp;Limit<font color="red" size="2">*</font></td>
					  		<td width="235px"><s:textfield name="equipMaterialsLimits.minResourceLimit"  maxlength="15" cssClass="input-text" cssStyle="text-align:right;width:120px;" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
					  		
					  	   <td align="right" width="">Customer&nbsp;Price</td>
						    <td width="250px"><s:textfield cssClass="input-text" name="equipMaterialsCost.salestCost" cssStyle="text-align:right;width:120px;"   /></td>
					  	   
					  	</tr>
					  	<tr>
					  		<td align="left" width="15px">
					  		<td align="right" width="">Item<font color="red" size="2">*</font></td>
					  		<td width="235px"><s:textfield name="equipMaterialsLimits.resource"  cssClass="input-text" cssStyle="width:200px;" readonly="false" onkeydown="return onlyDel(event,this);" onchange="resourceReset(this);"/></td>
					  		<td align="right" width="">Max&nbsp;Limit<font color="red" size="2">*</font></td>
					  		<td width="235px"><s:textfield name="equipMaterialsLimits.maxResourceLimit"  maxlength="15" cssClass="input-text" cssStyle="text-align:right;width:120px;" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
					  		
					  		 <td align="right" width="">OO&nbsp;Price</td>
						    <td width="250px"><s:textfield cssClass="input-text" name="equipMaterialsCost.ownerOpCost" cssStyle="text-align:right;width:120px;"   /></td>
					  		<%-- <td align="right" width="">Branch</td>
						    <td width="250px"><s:select cssClass="list-menu" name="equipMaterialsLimits.branch" list="{'Edmonton','Calgary'}" cssStyle="width:200px" headerKey="" headerValue="" /></td> --%>
					  	</tr>
					  	<tr>
					  		<td align="left" width="15px">
					  		
					  		<td align="right" width="">Warehouse<font color="red" size="2">*</font></td>
						    <td width="250px"><s:select cssClass="list-menu" name="equipMaterialsLimits.branch" list="%{house}" cssStyle="width:200px" headerKey="" headerValue="" onchange="whouseReset(this);"/></td>
					  	</tr>
					 </tbody>
				</table>
			 </td>
		    </tr>		    		  	
		 </tbody>
	</table>
	<table class="detailTabLabel" border="0">
		<tbody> 	
			<tr>
				<td align="left" height="3px"></td>
			</tr>	  	
			<tr>
			<td align="left" width="77"></td>
				<td align="left"><s:submit id="scan" cssClass="cssbutton1" type="button" key="button.save"  onclick="return goForValidate();"/></td>
				<c:if test="${hub!='0'}">
		       	<td align="right"><input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onclick="resetData();" tabindex="137" /><%-- <s:reset cssClass="cssbutton1" type="button" key="Reset" onclick="this.form.reset();"/> --%></td>
		       	<td align="right"><input type="button" class="cssbutton1" type="button" value="Cancel" onclick="closePopup();"/></td> 
		       	</c:if>
			</tr>
			<tr>
				<td align="left" height="13px"></td>
			</tr>		  	
		</tbody>
	</table>
	</div>
	<div class="bottom-header"><span></span></div>
</div>
</div>	
</div> 
	<script type="text/javascript"><!--
	function resetData(){
		    var bdata=document.forms['gridForm'].elements['backUp'].value;
		    var badatColl=bdata.split("~");
		    //alert(bdata);
		    document.forms['gridForm'].elements['equipMaterialsLimits.category'].value=badatColl[11];
	    	document.forms['gridForm'].elements['equipMaterialsLimits.branch'].value=badatColl[2];
	    	document.forms['gridForm'].elements['equipMaterialsLimits.division'].value=badatColl[3];
	    	document.forms['gridForm'].elements['equipMaterialsLimits.resource'].value=badatColl[1];            	
	    	document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value=badatColl[5];  
	    	document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value=badatColl[4];
	    	document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value=badatColl[6];
			document.forms['gridForm'].elements['equipMaterialsCost.salestCost'].value=badatColl[7];    	
		    document.forms['gridForm'].elements['equipMaterialsCost.ownerOpCost'].value=badatColl[8];
		    document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value=badatColl[9];
		
	   
	}
	
	
function goForValidate(){
	 
	if(document.forms['gridForm'].elements['equipMaterialsLimits.category'].value==''){
		alert("Category is Required");
		return false;
	}
	if(document.forms['gridForm'].elements['equipMaterialsLimits.resource'].value==''){
		alert("Item is Required");
				return false;
	}
	if(document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value==''){
		alert("Please input valid limits for Min and/or Max limits.");
				return false;
	}
	if(document.forms['gridForm'].elements['equipMaterialsLimits.branch'].value==''){
		alert("Warehouse is Required");
				return false;
	}
	if(document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value==''){
		alert("Please input valid limits for Min and/or Max limits.");
				return false;
	}
	if(document.forms['gridForm'].elements['equipMaterialsLimits.division'].value==''){
		alert("Division is Required");
				return false;
	}
	if(document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value==''){
		alert("Purchase price needs to have value minimum of 0.");
				return false;
	}
	if(document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value=='' ){
		alert("Please input Available Quantity");
		return false;
	}
	if(parseInt(document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value)<0 ){
		alert("Available Quantity should not be less than zero");
		return false;
	}
	var avlQty=document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value;	
	if(parseInt(avlQty)!=parseFloat(avlQty)) {
		alert("Available Quantity should be whole number");
		return false;
	}

	if(document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value!=null && document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value!="" ){
		if((parseInt(document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value))<0){
			alert("Max Limit should not be less than zero");
			return false;
		}
	}

	if(document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value!=null && document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value!=""){
		if((parseInt(document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value))<0){
			alert("Min Limit should not be less than zero");
			return false;
		}
		}

	if(document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value!=null && document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value!=""){
		if((parseInt(document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value))<0){
			alert("Purchase Price should not be less than zero");
			return false;
		}
		}

	
	if(document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value!=null && document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value!="" && document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value!=null && document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value!=""){
		if((parseInt(document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value))<(parseInt(document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value))){
			alert("Available Quantity should not exceed Max Limit");
			return false;
		}
	}
	
	if(document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value!=null && document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value!="" && document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value!=null && document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value!=""){
		if((parseInt(document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value))<(parseInt(document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value))){
			alert("Min Limit should not exceed Max Limit");
			return false;
		}
	}
	if(document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value!=null && document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value!="" && document.forms['gridForm'].elements['equipMaterialsCost.salestCost'].value!=null && document.forms['gridForm'].elements['equipMaterialsCost.salestCost'].value!=""){
		if(parseInt(document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value)>0 &&(parseInt(document.forms['gridForm'].elements['equipMaterialsCost.salestCost'].value))<=(parseInt(document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value))){
			alert("Purchase Price Shall not be greater than Customer and/or O/O Price.");
			return false;
		}
	}
	if(document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value!=null && document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value!="" && document.forms['gridForm'].elements['equipMaterialsCost.ownerOpCost'].value!=null && document.forms['gridForm'].elements['equipMaterialsCost.ownerOpCost'].value!=""){
		if(parseInt(document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value)>0 && (parseInt(document.forms['gridForm'].elements['equipMaterialsCost.ownerOpCost'].value))<=(parseInt(document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value))){
			alert("Purchase Price Shall not be greater than Customer and/or O/O Price.");
			return false;
		}
	}
	var minlimit=document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value;
	//var maxlimit=document.forms['operationsResourceLimits'].elements['equipMaterialsLimits.maxResourceLimit'].value;
	var avqty=document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value;
	//hidden
	             var tmp=document.forms['gridForm'].elements['equipMaterialsLimits.id'].value;
            	var category=document.forms['gridForm'].elements['equipMaterialsLimits.category'].value;
            	var branch=document.forms['gridForm'].elements['equipMaterialsLimits.branch'].value;
            	var div=document.forms['gridForm'].elements['equipMaterialsLimits.division'].value;
            	var des=document.forms['gridForm'].elements['equipMaterialsLimits.resource'].value;            	
            	var min=document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value;  
            	var max=document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value;
            	var avlq=document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value;    		        	
            	var sp=document.forms['gridForm'].elements['equipMaterialsCost.salestCost'].value;    	
            	var op=document.forms['gridForm'].elements['equipMaterialsCost.ownerOpCost'].value;
            	var ucost=document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value;
            	var tmp=document.forms['gridForm'].elements['equipMaterialsCost.equipMaterialsId'].value;
            	//var eqmateid= document.forms['gridForm'].elements['equipMaterialsCost.id'].value;         	    
            	//var branch=document.forms['gridForm'].elements['equipMaterialsLimits.resource'].readOnly=true; 
            	var branch=document.forms['gridForm'].elements['backUpOnSave'].value=tmp+"~"+des+"~"+branch+"~"+div+"~"+max+"~"+min+"~"+avlq+"~"+sp+"~"+op+"~"+ucost+"~"+category;         	   	
                //alert(document.forms['gridForm'].elements['backUpOnSave'].value);
            	document.forms['gridForm'].submit();
            	//hidden end
	//alert((avqty<minlimit));
	//alert(minlimit);
	//alert(avqty);
	/**if(minlimit!=null && avqty!=null &&  (avqty<minlimit) ){
		alert('Minimum Limit should not exceed Available Quantity');
		return false;
	}*/
	  
}	
	
	</script>
</div> 
<!-- end save section -->
</s:form>
<script type="text/javascript">
<c:if test="${valFlag=='openSaveBlock'}">
document.getElementById('saveBox').style.display="block";
document.getElementById('searchBox111').style.display="none";
//alert('${backUpOnSave}')
var bdata='${backUpOnSave}';
//alert(bdata);
             var badatColl=bdata.split("~");	
		    document.forms['gridForm'].elements['equipMaterialsLimits.category'].value=badatColl[10];
	    	document.forms['gridForm'].elements['equipMaterialsLimits.branch'].value=badatColl[2];
	    	document.forms['gridForm'].elements['equipMaterialsLimits.division'].value=badatColl[3];
	    	document.forms['gridForm'].elements['equipMaterialsLimits.resource'].value=badatColl[1];            	
	    	document.forms['gridForm'].elements['equipMaterialsLimits.minResourceLimit'].value=badatColl[5];  
	    	document.forms['gridForm'].elements['equipMaterialsLimits.maxResourceLimit'].value=badatColl[4];
	    	document.forms['gridForm'].elements['equipMaterialsLimits.qty'].value=badatColl[6];
			document.forms['gridForm'].elements['equipMaterialsCost.salestCost'].value=badatColl[7];    	
		    document.forms['gridForm'].elements['equipMaterialsCost.ownerOpCost'].value=badatColl[8];
		    document.forms['gridForm'].elements['equipMaterialsCost.unitCost'].value=badatColl[9];    
</c:if>
document.onkeydown = keyPressed;
</script>
<%@ include file="/common/enable-dojo-gridList.jsp" %>
</body>