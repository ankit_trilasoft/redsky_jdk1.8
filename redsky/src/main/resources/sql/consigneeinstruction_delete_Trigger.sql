DELIMITER $$
CREATE TRIGGER trigger_add_history_delete_consigneeInstruction BEFORE DELETE ON consigneeInstruction
FOR EACH ROW BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

  
   IF (OLD.corpID <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
   END IF;
 
   IF (OLD.sequenceNumber <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
   END IF;
   IF (OLD.ship <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.ship~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ship,'~') into oldValueList;
   END IF;
   IF (OLD.shipNumber <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
   END IF;
   IF (OLD.consigneeAddress <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.consigneeAddress~') into fieldNameList;
       select CONCAT(oldValueList,OLD.consigneeAddress,'~') into oldValueList;
   END IF;
   IF (OLD.consignmentInstructions <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.consignmentInstructions~') into fieldNameList;
       select CONCAT(oldValueList,OLD.consignmentInstructions,'~') into oldValueList;
   END IF;
   IF (OLD.notification <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.notification~') into fieldNameList;
       select CONCAT(oldValueList,OLD.notification,'~') into oldValueList;
   END IF;
   IF (OLD.notify <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.notify~') into fieldNameList;
       select CONCAT(oldValueList,OLD.notify,'~') into oldValueList;
   END IF;
   IF (OLD.shipperAddress <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.shipperAddress~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipperAddress,'~') into oldValueList;
   END IF;
   IF (OLD.specialInstructions <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.specialInstructions~') into fieldNameList;
       select CONCAT(oldValueList,OLD.specialInstructions,'~') into oldValueList;
   END IF;
   IF (OLD.serviceOrderId <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
   END IF;
   IF (OLD.shipmentLocation <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.shipmentLocation~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipmentLocation,'~') into oldValueList;
   END IF;
   IF (OLD.expressRelease <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.expressRelease~') into fieldNameList;
       select CONCAT(oldValueList,OLD.expressRelease,'~') into oldValueList;
   END IF;
   IF (OLD.collect <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.collect~') into fieldNameList;
       select CONCAT(oldValueList,OLD.collect,'~') into oldValueList;
   END IF;
   IF (OLD.ugwIntId <> '') THEN
       select CONCAT(fieldNameList,'consigneeInstruction.ugwIntId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ugwIntId,'~') into oldValueList;
   END IF;
 
   CALL add_tblHistory (OLD.serviceOrderId,"consigneeInstruction", fieldNameList, oldValueList, newValueList, OLD.updatedby, OLD.corpID, now());
END
$$