ALTER TABLE `redsky`.`reports` ADD COLUMN `targetCabinet` VARCHAR(10) DEFAULT NULL AFTER `mode`;

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','HAULINGJOB',3,true,now(),'subrat',now(),'subrat',1,1,'');

ALTER TABLE `redsky`.`removalrelocationservice` ADD COLUMN `isIta2m3` TINYINT(1) AFTER `workPermit`;

update reports set docsxfer='No' where (docsxfer='Yes' or docsxfer=' ' or docsxfer is null) and formReportFlag='R';

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','CLAIMSTATUS',10,true,now(),'subrat',now(),'subrat',1,1,'');

Insert into refmaster (corpid, code, description, fieldlength,parameter,language,createdon,createdby,updatedon,updatedby) values('TSFT', 'New', 'New',10, 'CLAIMSTATUS','en',now(),'subrat',now(),'subrat'),('TSFT', 'Submitted', 'Submitted',10, 'CLAIMSTATUS','en',now(),'subrat',now(),'subrat'),('TSFT', 'Closed', 'Closed',10, 'CLAIMSTATUS','en',now(),'subrat',now(),'subrat'),('TSFT', 'Cancelled', 'Cancelled',10, 'CLAIMSTATUS','en',now(),'subrat',now(),'subrat');