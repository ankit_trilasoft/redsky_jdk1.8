/**
 * Business Service Interface to handle communication between web and persistence layer.
 * Gets Claim information based on Claim id.
 * @param 			  ClaimId the Claim's id
 * This interface represents the basic actions on "Claim" object in Redsky that allows for Claim management.
 * @Interface Name	  ClaimManager
 * @Author            Sangeeta Dwivedi
 * @Version           V01.0
 * @Since             1.0
 * @Date              01-Dec-2008
 */

package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.Claim;
 
public interface ClaimManager extends GenericManager<Claim, Long> { 
	public List<Claim> findByShipNumber(String shipNumber);
    public Long findMaximum(String corpId);
    public List findMaximumId();
    public List findClaims(Long claimNumber, String lastName, String shipNumber, Date closeDate,String registrationNumber,String claimPerson,BigInteger origClmsAmt,String corpId);
    public List checkByShipNumber(String ship);
    public List checkByCloseDate(String ship);
    public List findUserDetails(String userName);
	public void resetPassword(String passwordNew,String confirmPasswordNew,String pwdHintQues,String pwdHint,String userName);
	public List findPwordHintQ(String pwdHintQues,String userName);
	public void forgotPassword(String pwdHintQues,String pwdHint,String userName);
	public List checkBySirviceOrderId(Long serviceOrderId, String corpId);
	public List claimSortOrderByUser(String username);
	public List claimList(Long sid);
	public List findResponsiblePerson(String custJobType, String companyDivision, String sessionCorpID);
	public Long findRemoteClaim(String idNumber, Long sid);
	public Long findMaximumForOtherCorpid(String sessionCorpID);
	public String getClaimPersonName(String claimPerson, String sessionCorpID);
	public List  findPartnerCodeOtherCorpID(String insurerCode,String sessionCorpID);
	public List findMaximumIdNumber(String shipNumber);
	public Integer findClaimByValue(String corpId,String shipNumber);
	public List findUserByBasedAt(String basedAt,String corpId);
	public int  updateSurveyCalData(String startdate,String id);
	public List findUserCompanyDivision(String basedAt,String corpId);
	} 
