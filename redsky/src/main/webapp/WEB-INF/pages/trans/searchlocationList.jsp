<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="locationList.title" /></title>
<meta name="heading" content="<fmt:message key='locationList.heading'/>" />
</head>


<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0"
					border="0">
					<tbody>
						<tr>
						<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a
					href="editWorkTicketUpdate.html?id=${workTicket.id} ">Work Ticket</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td align="left"></td>
				<td width="4"></td>
				<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="bookStorages.html?id=${workTicket.id} ">Work On Ticket</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td align="left"></td>
							
							<td width="4"></td>
							
							<td width="1" align="right"><img width="1" height="20"
								src="<c:url value='/images/tab-left.jpg'/>" alt="" /></td>
							<td width="130" class="detailActiveTabLabel content-tab">Add Item To Location</td>
							<td width="1" align="left"><img width="1" height="20"
								src="<c:url value='/images/tab-right.jpg'/>" alt="" /></td>

							<td width="4"></td>
							<td width="1" align="right"><img width="1" height="20"
								src="<c:url value='/images/tab-left.jpg'/>" alt="" /></td>
							<td width="160" class="content-tab"><a
								href="storages.html?id=<%=request.getParameter("id") %>">Access Release Move Items </a></td>
							<td width="1" align="left"><img width="1" height="20"
								src="<c:url value='/images/tab-right.jpg'/>" alt="" /></td>
							<td width="4"></td>

						</tr>
						
					</tbody>
				</table>
				
		<table class="" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
				<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
				    <tbody><tr>
				    <td width="1" align="right"><img width="1" height="20"
								src="<c:url value='/images/tab-left.jpg'/>" alt="" /></td>
							<td width="160" class="content-tab"><a
								>Add Item to Location For </a></td>
							<td width="1" align="left"><img width="1" height="20"
								src="<c:url value='/images/tab-right.jpg'/>" alt="" /></td>
							<td width="4"></td>
				      
				    </tr>
				</tbody></table>
				<table>
					<tbody>
						<tr>
						<td class="listwhite">Ticket</td>
							<td>
							    <s:textfield name="workTicket.ticket" readonly="true" required="true" />
							</td>
							<td>
							    <s:hidden name="shipNumber"  value="%{workTicket.shipNumber}" />
							</td>
							<td>
							    
							</td>
							
						</tr>
					</tbody>
				</table>
	
		

<s:form id="searchform"  action="searchLocations" method="post">
	
	<s:hidden name="id" value="<%=request.getParameter("id") %>"/> 
	<s:hidden name="id1"  value="<%=request.getParameter("id1") %>"/>
	<s:hidden name="ticket" value="<%=request.getParameter("ticket") %>" />
	<s:hidden name="shipNumber" value="<%=request.getParameter("shipNumber") %>"/>
	
				<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
		<tbody>
			<tr>
				<td width="7"><img width="7" height="20"
					src="<c:url value='/images/head-left.jpg'/>" /></td>
				<td id="searchLabelCenter" width="90">
				<div align="center" class="content-head">Search</div>
				</td>
				<td width="7"><img width="7" height="20"
					src="<c:url value='/images/head-right.jpg'/>" /></td>
				<td></td>
			</tr>
		</tbody>
	</table>
	<table class="table" width="90%">
		<thead>
			<tr>
				<th><fmt:message key="location.locationId" /></th>
				
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><s:textfield name="location.locationId" required="true"
					cssClass="text medium" /></td>
				
				<td><s:submit cssClass="button" method="search"
					key="button.search" /></td>
					
			</tr>
		</tbody>
	</table>









	<c:out value="${searchresults}" escapeXml="false" />
	

	<s:set name="locations" value="locations" scope="request" />
	<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
		<tbody>
			<tr>
				<td width="7"><img width="7" height="20"
					src="<c:url value='/images/head-left.jpg'/>" /></td>
				<td id="searchLabelCenter" width="90">
				<div align="center" class="content-head">List</div>
				</td>
				<td width="7"><img width="7" height="20"
					src="<c:url value='/images/head-right.jpg'/>" /></td>
				<td></td>
			</tr>
		</tbody>
	</table>
	<display:table name="locations" class="table" requestURI=""
		id="locationList" export="true" pagesize="10">
		
<display:column>
			<input type="radio" name="dd" onclick="check(this)"
				value="${locationList.id}" />
		</display:column>

		
			<display:column property="locationId" sortable="true" titleKey="location.locationId" />
 
			
		<display:column property="capacity" sortable="true"
			titleKey="location.capacity" />
		<display:column property="cubicFeet" sortable="true"
			titleKey="location.cubicFeet" />
		<display:column property="occupied" sortable="true"
			titleKey="location.occupied" />
		<display:column property="type" sortable="true"
			titleKey="location.type" />
		


		<display:setProperty name="paging.banner.item_name" value="location" />
		<display:setProperty name="paging.banner.items_name" value="location" />
		<display:setProperty name="export.excel.filename"
			value="Location List.xls" />
		<display:setProperty name="export.csv.filename"
			value="Location List.csv" />
		<display:setProperty name="export.pdf.filename"
			value="Location List.pdf" />
	</display:table>
	</td>
		</tr>
	</tbody>
</table>
</div> 
 
	<table>
	<tr>
	<td>
	<c:set var="buttons">
	
	
		
	</c:set>
	<c:out value="${buttons}" escapeXml="false" />
	
</s:form>
<s:form id="assignItemsForm" action="editLocation" method="post">
	<s:hidden name="id" /> 
	<s:hidden name="id1"  value="%{workTicket.id}"  required="true" cssClass="text medium"/>
	<s:hidden name="ticket" value="%{workTicket.ticket}"  required="true" cssClass="text medium"/>
	<s:hidden name="shipNumber" value="%{workTicket.shipNumber}"  required="true" cssClass="text medium"/>
	<s:hidden name="locate"  />
	
	
	<input type="submit" name="forwardBtn"  disabled value="Add" style="width:87px; height:25px"  />
	
	
</s:form>  
<td>
<td>

</td>
</tr>
	</table>

<script type="text/javascript"> 
    highlightTableRows("locationList");
     Form.focusFirstElement($("customerFileNotesForm"));    
</script>
<script type="text/javascript">
		function check(targetElement)
		  {
		  try{
		  document.forms['assignItemsForm'].elements['id'].value=targetElement.value;
		document.forms['assignItemsForm'].elements['locate'].value=document.forms['assignItemsForm'].elements['id'].value;
		
	document.forms['assignItemsForm'].elements['forwardBtn'].disabled = false ;
		
		  //document.forms['assignItemsForm'].elements['id1'].value=targetElement.value;
		  }
		  catch(e){}
		  }
	</script>

