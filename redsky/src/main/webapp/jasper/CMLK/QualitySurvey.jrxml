<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="QualitySurvey" pageWidth="595" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="535" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="b6ce269f-dd58-41a6-9e04-e0edf3bbbc57">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<parameter name="Customer File Number" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT	"QualitySurvey-F-PDF-CMLK.jrxml",
	Concat(if(customerfile.prefix is null || trim(customerfile.prefix)='',"",trim(customerfile.prefix)),"",if(customerfile.firstName is null || trim(customerfile.firstName)='',"",if((customerfile.prefix is null || trim(customerfile.prefix)=''),trim(customerfile.firstName),concat(" ",trim(customerfile.firstName)))),"",if(customerfile.lastName is null || trim(customerfile.lastName)='',"",if((customerfile.prefix is null || trim(customerfile.prefix)='') AND (customerfile.firstName is null || trim(customerfile.firstName)=''),trim(customerfile.lastName),concat(" ",trim(customerfile.lastName))))) AS Shipper,
	customerfile.originCountry AS customerfile_originCountry,
	customerfile.destinationCountry AS customerfile_destinationCountry,
	customerfile.billtoname AS customerfile_billtoname,
	customerfile.sequenceNumber AS customerfile_sequenceNumber,
	concat("Please return via Fax: ",if(companydivision.billingFax is null || trim(companydivision.billingFax)="","",trim(companydivision.billingFax)),"  or via email: ",if(companydivision.billingEmail is null || trim(companydivision.billingEmail)="","",trim(companydivision.billingEmail))) AS Companydivision_fax_email,
	concat("Please rate your satisfaction with the ",if (companydivision.description is null || trim(companydivision.description)="","",trim(companydivision.description))," service person that coordinated your move. (   ) ") AS comment1,
	concat("Please rate your ",if(companydivision.description is null || trim(companydivision.description)="","",trim(companydivision.description))," move overall. (   ) ") AS comment2,
	trim(app_user.alias) AS customerfile_coordinator,
	concat("Would you use or recommend ",if(companydivision.description is null || trim(companydivision.description)="","",trim(companydivision.description)),"  again? Y (   )     N (   )  ") AS comment3,
	concat("Thank you very much for your time and please do not hesitate to let us know if there is anything else ",if(companydivision.description is null || trim(companydivision.description)="","",trim(companydivision.description))," can do to assist in making your move your best move yet! ") AS comment4

FROM	customerfile customerfile
	LEFT OUTER JOIN `companydivision` companydivision on customerfile.companyDivision=companydivision.companyCode and companydivision.corpid = $P{Corporate ID}
	LEFT OUTER JOIN `app_user` app_user on customerfile.coordinator=app_user.username and app_user.corpID=$P{Corporate ID}
WHERE	customerfile.corpid = $P{Corporate ID}
	AND customerfile.sequenceNumber = $P{Customer File Number}]]>
	</queryString>
	<field name="QualitySurvey-F-PDF-CMLK.jrxml" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="customerfile_originCountry" class="java.lang.String"/>
	<field name="customerfile_destinationCountry" class="java.lang.String"/>
	<field name="customerfile_billtoname" class="java.lang.String"/>
	<field name="customerfile_sequenceNumber" class="java.lang.String"/>
	<field name="Companydivision_fax_email" class="java.lang.String"/>
	<field name="comment1" class="java.lang.String"/>
	<field name="comment2" class="java.lang.String"/>
	<field name="customerfile_coordinator" class="java.lang.String"/>
	<field name="comment3" class="java.lang.String"/>
	<field name="comment4" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="83" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="130" y="45" width="274" height="35" uuid="f6c5cae8-94cd-45a7-9cd3-f64fc999a465"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="18" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Quality Survey Report
]]></text>
			</staticText>
			<image hAlign="Center" onErrorType="Blank">
				<reportElement key="image-1" mode="Opaque" x="313" y="0" width="100" height="50" uuid="e19f03bd-a0ae-4a38-9eee-7f4ba3235178"/>
				<imageExpression><![CDATA["../../images/FidiLogo.png"]]></imageExpression>
			</image>
			<image onErrorType="Blank">
				<reportElement key="image-3" mode="Opaque" x="46" y="0" width="120" height="55" uuid="4b5d8ff7-5480-43ab-a425-f2b208c25728"/>
				<imageExpression><![CDATA["../../images/CMLK_logo.png"]]></imageExpression>
			</image>
			<image hAlign="Center" onErrorType="Blank">
				<reportElement key="image-4" mode="Opaque" x="402" y="0" width="100" height="50" uuid="0b7f5002-2440-417c-b70b-da1bbee9c17d"/>
				<imageExpression><![CDATA["../../images/IATA_BLUE.png"]]></imageExpression>
			</image>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="583" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" isPrintRepeatedValues="false" mode="Transparent" x="0" y="0" width="100" height="18" isPrintInFirstWholeBand="true" uuid="23432425-bc5e-4746-bb90-d451919a1d12"/>
				<box leftPadding="1">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Transferee Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" isPrintRepeatedValues="false" mode="Transparent" x="281" y="0" width="120" height="18" isPrintInFirstWholeBand="true" uuid="84fd9d81-0e1a-4a72-9765-0e31cb8ca6a4"/>
				<box leftPadding="1">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Job Number]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" isPrintRepeatedValues="false" mode="Transparent" x="0" y="36" width="100" height="18" isPrintInFirstWholeBand="true" uuid="f066241a-5f0b-4560-a3f0-8f855b7eba47"/>
				<box leftPadding="1">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Country of Origin]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" isPrintRepeatedValues="false" mode="Transparent" x="0" y="18" width="100" height="18" isPrintInFirstWholeBand="true" uuid="454190dc-4809-44ec-acfe-e9bb069cce20"/>
				<box leftPadding="1">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Company]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" isPrintRepeatedValues="false" mode="Transparent" x="391" y="36" width="144" height="18" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" uuid="2576b6f5-3683-41c6-a8ea-2dfd54a60092"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customerfile_destinationCountry}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-2" mode="Transparent" x="95" y="36" width="185" height="18" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" isPrintWhenDetailOverflows="true" uuid="3b17e2f2-841c-49a9-91ca-a6e959b8f39d"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customerfile_originCountry}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-3" isPrintRepeatedValues="false" mode="Transparent" x="95" y="0" width="185" height="18" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" uuid="416f443f-624a-459c-a76d-b49381b4f9b8"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Shipper}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-15" isPrintRepeatedValues="false" mode="Transparent" x="281" y="36" width="120" height="18" isPrintInFirstWholeBand="true" uuid="e06c4973-54f1-40a4-bf40-0a8bf749fcf7"/>
				<box leftPadding="1">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Destination]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-24" isPrintRepeatedValues="false" mode="Transparent" x="281" y="18" width="120" height="18" isPrintInFirstWholeBand="true" uuid="1e6ebf83-e989-42c4-a2ab-ed6fe92dc271"/>
				<box leftPadding="1">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Moving Coordinator]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-21" isPrintRepeatedValues="false" mode="Transparent" x="391" y="0" width="144" height="18" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" uuid="2f2b20e4-f201-4ff7-8302-183202b97039"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customerfile_sequenceNumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-22" isPrintRepeatedValues="false" mode="Transparent" x="391" y="18" width="144" height="18" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" uuid="80b05095-2171-44de-8803-982da395686b"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customerfile_coordinator}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-23" isPrintRepeatedValues="false" mode="Transparent" x="95" y="18" width="185" height="18" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" uuid="dcd658e8-e361-4eb1-9bbd-06d4059e1c44"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customerfile_billtoname}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-52" x="0" y="111" width="535" height="15" uuid="e6a458ac-d5ca-401e-9305-1def29409d4a"/>
				<textElement>
					<font isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Please rate our services on a scale of 1 to 8, 8 being Excellence.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-55" x="0" y="68" width="535" height="42" uuid="a9e52664-aa4a-4bc5-8150-8fd22bffd2be"/>
				<textElement>
					<font isBold="false" isItalic="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[The purpose of this survey is to evaluate the services performed by us so that we could better serve our customers in future. Your feedback is valuable to us and your cooperation in completing this survey is greatly appreciated.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-56" x="0" y="126" width="535" height="27" uuid="02a35301-5447-4b06-b46c-f109f06e2aa2"/>
				<box leftPadding="24"/>
				<textElement>
					<font isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Please rate your satisfaction with the service provided to you by the surveyor/estimator who visited your home. (   )]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-58" x="0" y="140" width="535" height="15" uuid="afe8ae99-efd7-4ed5-a032-d98bc07b3f59"/>
				<box leftPadding="24"/>
				<textElement>
					<font isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Please rate your satisfaction with the services at origin. (   )]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-59" x="0" y="155" width="535" height="15" uuid="4ae6e90e-b588-45d1-9d07-15575c18414c"/>
				<box leftPadding="24"/>
				<textElement>
					<font isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Please rate your satisfaction with the services at destination. (   )]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-63" x="0" y="215" width="535" height="15" uuid="0e0ef5ef-8f6d-4e40-81ab-4f7e0747b69d"/>
				<box leftPadding="24"/>
				<textElement>
					<font isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Do you intend to file a claim? Y (   )      N (   )]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-64" x="0" y="230" width="535" height="27" uuid="f76d863a-a1ea-4fdc-8335-bf3df4d373ea"/>
				<box leftPadding="24"/>
				<textElement>
					<font isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[What did you like the most about our service?]]></text>
			</staticText>
			<line>
				<reportElement key="line-1" x="-1" y="254" width="536" height="1" uuid="7de3c957-bbcb-422c-9582-99ee520fb91e"/>
			</line>
			<line>
				<reportElement key="line-2" x="-1" y="322" width="536" height="1" uuid="af4d89ac-7783-406d-aeef-6751a0d1e96e"/>
			</line>
			<staticText>
				<reportElement key="staticText-65" x="0" y="260" width="535" height="25" uuid="c40da3a4-3f08-43dd-b5e0-669727feae62"/>
				<box leftPadding="24"/>
				<textElement>
					<font isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[What could we have done better?]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-66" x="0" y="295" width="535" height="25" uuid="78ce4bfe-ef39-43fc-8770-5e2cc14fadb8"/>
				<box leftPadding="24"/>
				<textElement>
					<font isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[What additional services do you wish we provided at the time of your move?]]></text>
			</staticText>
			<line>
				<reportElement key="line-3" x="-1" y="287" width="536" height="1" uuid="775f134f-b7c1-49b6-98cf-c553c3a3dbb2"/>
			</line>
			<staticText>
				<reportElement key="staticText-67" x="0" y="329" width="535" height="25" uuid="1c27f50a-b30c-47df-a750-28ac52b4a520"/>
				<box leftPadding="24"/>
				<textElement>
					<font isBold="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[Do we have your permission to use your comments as a reference? Y (   )       N (   )]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-69" x="0" y="359" width="535" height="63" uuid="d214cdd8-06c7-49d0-82c3-c421611eb699"/>
				<textElement>
					<font isBold="true" isItalic="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[I realize it is an extremely busy time for you but your feedback is extremely important to us and ultimately determines the course of relationship as a preferred vendor.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-71" isPrintRepeatedValues="false" mode="Transparent" x="306" y="516" width="229" height="18" isPrintInFirstWholeBand="true" uuid="3963817d-beca-4085-8f08-d5cdf162bbd4"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[DATE:_____________________]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-72" isPrintRepeatedValues="false" mode="Transparent" x="0" y="516" width="303" height="18" isPrintInFirstWholeBand="true" uuid="c647cb62-c160-4b6e-9bdc-46944fdba854"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[CUSTOMER:_____________________]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-73" x="5" y="121" width="13" height="15" uuid="db4781b3-f305-4608-854c-99268b66848f"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Times New Roman" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-74" x="5" y="135" width="13" height="15" uuid="592956ba-f566-4f9c-8396-0a92b127c88f"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Times New Roman" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-75" x="5" y="150" width="13" height="15" uuid="9e2b202a-4c75-4f9c-a062-377bf6518729"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Times New Roman" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-76" x="5" y="165" width="13" height="15" uuid="5801436c-5c65-44b2-b98e-53802c7fbea3"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Times New Roman" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-77" x="5" y="180" width="13" height="15" uuid="002b46b9-f578-40f5-a3e6-f708b04db703"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Times New Roman" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-78" x="5" y="196" width="13" height="15" uuid="3a6d90b8-d0bf-49b5-9660-02530a6f94dc"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Times New Roman" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-79" x="5" y="225" width="13" height="15" uuid="c21a8d98-1f00-4edf-9637-aa5e9910a302"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Times New Roman" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-80" x="5" y="210" width="13" height="15" uuid="bd4e70cc-8f88-4806-96cd-54de54a0860b"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Times New Roman" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-81" x="5" y="255" width="13" height="15" uuid="fac77bef-85c4-44d8-88b4-28e395ba1975"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Times New Roman" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-82" x="5" y="289" width="13" height="15" uuid="f4a22e56-49dc-4e79-9aac-2c6cd8d0ce75"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Times New Roman" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-83" x="5" y="324" width="13" height="15" uuid="a1f08960-5771-49f6-9a6a-9400d54baaee"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font fontName="Times New Roman" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[.]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-29" x="0" y="404" width="535" height="53" uuid="72ae9ed3-e7b7-4add-ba45-f10b39190d3a"/>
				<textFieldExpression><![CDATA[$F{comment4}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-30" x="0" y="200" width="534" height="16" uuid="d41a8352-dbde-4c4e-b48b-029367b97926"/>
				<box leftPadding="24"/>
				<textFieldExpression><![CDATA[$F{comment3}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-31" x="0" y="183" width="535" height="15" uuid="53999386-c6ff-4bb9-8fd9-d5706cf1e1b1"/>
				<box leftPadding="24"/>
				<textFieldExpression><![CDATA[$F{comment2}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-32" x="0" y="169" width="535" height="26" uuid="f5ad9a21-3402-40ef-bf8c-f1a564fe432a"/>
				<box leftPadding="24"/>
				<textFieldExpression><![CDATA[$F{comment1}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-33" x="0" y="456" width="535" height="48" uuid="bb6573fe-d6d1-40ef-b2da-ba198307e657"/>
				<textFieldExpression><![CDATA[$F{Companydivision_fax_email}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-84" x="89" y="0" width="5" height="18" uuid="d5d2cacc-ceff-432d-b218-dc08fd92d9d0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-85" x="89" y="18" width="5" height="18" uuid="e251d150-613f-41f0-9c8c-303bc9d8ccc8"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-86" x="89" y="36" width="5" height="18" uuid="f06ad90a-f839-4305-b216-6a455e6cd7a4"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-87" x="385" y="0" width="5" height="18" uuid="8b92daba-6b2b-46bd-a690-998423ee441c"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-88" x="385" y="18" width="5" height="18" uuid="00387884-999f-47f0-9635-9d38d2244887"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-89" x="385" y="36" width="5" height="18" uuid="629c2252-a2dc-4e9b-adef-46e25a8b2a68"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[:]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band splitType="Stretch"/>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
