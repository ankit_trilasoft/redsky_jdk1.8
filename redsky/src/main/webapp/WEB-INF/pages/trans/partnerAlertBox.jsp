<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Partner Alert Note</title>   
<meta name="heading" content="Partner Alert Note"/>  
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/redskyditor/tiny_mce.js"></script>  
<style type="text/css">
	h2 {background-color: #CCCCCC}
</style>
<style type="text/css">
table {
	font-size:1em;
	margin:0pt 0pt 0em;
	!margin-top:5px;
	padding:0pt;
}

</style>
<script type="text/javascript">
			// Use it to attach the editor to all textareas with full featured setup
			//WYSIWYG.attach('all', full);
			<configByCorp:fieldVisibility componentId="component.field.partnerUser.showEditor">	
			// Use it to attach the editor directly to a defined textarea
			 tinyMCE.init({
			
			 mode : "textareas",
			 theme : "advanced",
			 relative_urls : false,
			 remove_script_host : false,
			 plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
			 
			 theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,cleanup,code,|,insertdate,inserttime|,forecolor,backcolor",
				theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
				//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
				 theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_resizing : true,
					// Example content CSS (should be your site CSS)
					content_css : "css/content.css",

					// Drop lists for link/image/media/template dialogs
					template_external_list_url : "lists/template_list.js",
					external_link_list_url : "lists/link_list.js",
					external_image_list_url : "lists/image_list.js",
					media_external_list_url : "lists/media_list.js"
			 }) 
			</configByCorp:fieldVisibility>
		</script> 
</head>

<s:form name="scrachCardDetailForm"> 
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    <s:hidden name="scrachCard.id" />
    <s:hidden name="formStatus" value=""/>
<table class="mainDetailTable" style="width:100%; height:100%;" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width:100%; height:100%;">
				<div class="subcontent-tab" >Partner Note&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
				<table cellspacing="0" style="width:100%;" cellpadding="5" border="0">
					<tbody>
					<tr><td align="left" height="30px" valign="bottom">Note Information :</td></tr>
						<tr>
							<td align="left" >
							<s:textarea name="notes.note" cssStyle="width:99%;" rows="20" cssClass="textarea" readonly="true"/></td>
						</tr>
						<tr>
							<td align="left" class="" class="listwhite">
								<table style="width:100%;">
									<tr><td height="10px"></td></tr>
									<tr>	
										<td align="left"><input type="button" class="cssbuttonA" name="close" value="Close" onclick="window.close();" style="width:87px; height:25px; padding-top:0pt"/></td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</s:form>