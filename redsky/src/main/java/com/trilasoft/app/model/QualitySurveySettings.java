package com.trilasoft.app.model;


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table ( name="qualitysurveysettings") 
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class QualitySurveySettings extends BaseObject{
	private Long id;
	private Long surveyId;
	private String accountId;
	private Long surveyEmailRecipientId;
	private Long surveyEmailSubjectId;
	private Long surveyEmailBodyId;
	private Long surveyEmailSignatureId;
	private String mode;
	private Integer delayInterval;
	private Integer sendLimit;
	private Integer timeSent;
	private String corpId;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String surveyFrom;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("surveyId", surveyId).append("accountId", accountId)
				.append("surveyEmailRecipientId", surveyEmailRecipientId)
				.append("surveyEmailSubjectId", surveyEmailSubjectId)
				.append("surveyEmailBodyId", surveyEmailBodyId)
				.append("surveyEmailSignatureId", surveyEmailSignatureId)
				.append("mode", mode).append("delayInterval", delayInterval)
				.append("sendLimit", sendLimit).append("timeSent", timeSent)
				.append("corpId", corpId).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("surveyFrom",surveyFrom).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof QualitySurveySettings))
			return false;
		QualitySurveySettings castOther = (QualitySurveySettings) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(surveyId, castOther.surveyId)
				.append(accountId, castOther.accountId)
				.append(surveyEmailRecipientId,
						castOther.surveyEmailRecipientId)
				.append(surveyEmailSubjectId, castOther.surveyEmailSubjectId)
				.append(surveyEmailBodyId, castOther.surveyEmailBodyId)
				.append(surveyEmailSignatureId,
						castOther.surveyEmailSignatureId)
				.append(mode, castOther.mode)
				.append(delayInterval, castOther.delayInterval)
				.append(sendLimit, castOther.sendLimit)
				.append(timeSent, castOther.timeSent)
				.append(corpId, castOther.corpId)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn).append(surveyFrom, castOther.surveyFrom).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(surveyId)
				.append(accountId).append(surveyEmailRecipientId)
				.append(surveyEmailSubjectId).append(surveyEmailBodyId)
				.append(surveyEmailSignatureId).append(mode)
				.append(delayInterval).append(sendLimit).append(timeSent)
				.append(corpId).append(createdBy).append(createdOn)
				.append(updatedBy).append(updatedOn).append(surveyFrom).toHashCode();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(Long surveyId) {
		this.surveyId = surveyId;
	}
	@Column(length = 8)
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}


	@Column(length = 20)
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public Integer getDelayInterval() {
		return delayInterval;
	}
	public void setDelayInterval(Integer delayInterval) {
		this.delayInterval = delayInterval;
	}
	public Integer getSendLimit() {
		return sendLimit;
	}
	public void setSendLimit(Integer sendLimit) {
		this.sendLimit = sendLimit;
	}
	public Integer getTimeSent() {
		return timeSent;
	}
	public void setTimeSent(Integer timeSent) {
		this.timeSent = timeSent;
	}
	@Column(length=15)
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getSurveyEmailRecipientId() {
		return surveyEmailRecipientId;
	}
	public void setSurveyEmailRecipientId(Long surveyEmailRecipientId) {
		this.surveyEmailRecipientId = surveyEmailRecipientId;
	}
	public Long getSurveyEmailSubjectId() {
		return surveyEmailSubjectId;
	}
	public void setSurveyEmailSubjectId(Long surveyEmailSubjectId) {
		this.surveyEmailSubjectId = surveyEmailSubjectId;
	}
	public Long getSurveyEmailBodyId() {
		return surveyEmailBodyId;
	}
	public void setSurveyEmailBodyId(Long surveyEmailBodyId) {
		this.surveyEmailBodyId = surveyEmailBodyId;
	}
	public Long getSurveyEmailSignatureId() {
		return surveyEmailSignatureId;
	}
	public void setSurveyEmailSignatureId(Long surveyEmailSignatureId) {
		this.surveyEmailSignatureId = surveyEmailSignatureId;
	}
	@Column(length=200)
	public String getSurveyFrom() {
		return surveyFrom;
	}
	public void setSurveyFrom(String surveyFrom) {
		this.surveyFrom = surveyFrom;
	}

}
