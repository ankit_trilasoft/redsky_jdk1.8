<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Reciprocity Analysis</title>
<meta name="heading" content="Reciprocity Analysis" />
	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style> <%@ include file="/common/calenderStyle.css"%> </style>
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<script language="javascript" type="text/javascript">
function fieldValidate(){
       if(document.forms['analysisForm'].elements['fromDate'].value==''){
	      	alert("Please select the from date"); 
	     	return false;
	   }if(document.forms['analysisForm'].elements['toDate'].value==''){
	       	alert("Please select the to date "); 
	       	return false;
	   }
}  
function fieldValidate1(){
       if(document.forms['analysisForm'].elements['fromDate'].value==''){
	      	alert("Please select the from date"); 
	     	return false;
	   }else if(document.forms['analysisForm'].elements['toDate'].value==''){
	       	alert("Please select the to date "); 
	       	return false;
	   }else{
	   document.forms['analysisForm'].action ='reciprocityCountryReport.html';
       document.forms['analysisForm'].submit(); 
	   }
}  


function summaryList(){
	var fromDate = document.forms['analysisForm'].elements['fromDate'].value;
	var toDate = document.forms['analysisForm'].elements['toDate'].value;
	if(fromDate == ''){
	      	alert("Please select the from date"); 
	     	return false;
	}if(toDate == ''){
	       	alert("Please select the to date "); 
	       	return false;
	}
	if(fromDate != '' && toDate != ''){
		location.href='summaryList.html?fromDate='+fromDate+'&toDate='+toDate;
	}
}

function calcDays(){
  document.forms['analysisForm'].elements['formStatus'].value = '1';

 var date2 = document.forms['analysisForm'].elements['fromDate'].value;	 
 var date1 = document.forms['analysisForm'].elements['toDate'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];

  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }

   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];

   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");

  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);

  if(daysApart < 0){
    alert("Reciprocity Analysis From Date must be less than To Date");
    document.forms['analysisForm'].elements['toDate'].value='';
  } 
 	document.forms['analysisForm'].elements['checkDaysClickTicket'].value = '';
	
}


function forDays(){
 document.forms['analysisForm'].elements['checkDaysClickTicket'].value =1; 
}

function checkPeriodDate(temp,from,to) { 
    var mydate=new Date();
	var year=mydate.getFullYear()
	
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var day1=mydate.getDay()
	var month=mydate.getMonth()+1
	var month1=mydate.getMonth()+1
	if(temp.value=='LastYear'){
    year=year-1;
	month=1;
	month1=12;
	day=1;
	day1=31; 
	} 
	if(temp.value=='CurrentYear'){
    year=year;
	month=1;
	month1=12;
	day=1;
	day1=31; 
	} 
	if(temp.value=='LastHalfYear'){
	if(month>6){
	year=year;
	month=1;
	month1=6;
	day=1;
	day1=30; 
	}else if(month<6|| month==6){
	year=year-1;
	month=7;
	month1=12;
	day=1;
	day1=31; 
	
	}
	}
	if(temp.value=='CurrentHalfYear'){
	if(month>6){
	year=year;
	month=7;
	month1=12;
	day=1;
	day1=31; 
	}else if(month<6|| month==6){
	year=year;
	month=1;
	month1=6;
	day=1;
	day1=30; 
	
	}
	}
	if(temp.value=='LastQuarter'){
	if(month<3 || month==3){
	year=year-1;
	month=10;
	month1=12;
	day=1;
	day1=31; 
	}else if((month>3 && month<6)||month==6){
	year=year;
	month=1;
	month1=3;
	day=1;
	day1=31;  
	} else if((month>6&& month<9)||month==9){
	year=year;
	month=4;
	month1=6;
	day=1;
	day1=30; 
	} else if((month>9)){
	year=year;
	month=7;
	month1=9;
	day=1;
	day1=30;  
	}
	}
	if(temp.value=='CurrentQuarter'){
	if(month<3 || month==3){
	year=year;
	month=1;
	month1=3;
	day=1;
	day1=31; 
	}else if((month>3 && month<6)||month==6){
	year=year;
	month=4;
	month1=6;
	day=1;
	day1=30;  
	} else if((month>6&& month<9)||month==9){
	year=year;
	month=7;
	month1=9;
	day=1;
	day1=30; 
	} else if((month>9)){
	year=year;
	month=10;
	month1=12;
	day=1;
	day1=31;  
	}
	}
	if(temp.value=='LastMonth'){
	year=year;
	if(month==1){
	month=12;
	month1=12;
	year=year-1;
	}else{
	month=month-1;
	month1=month1-1;
	}
	day=1; 
	var lastday = getLastDayOfMonth(month,year); 
	day1=lastday; 
	}
	if(temp.value=='CurrentMonth'){
	year=year;
	month=month;
	month1=month1;
	day=1; 
	var lastday = getLastDayOfMonth(month,year); 
	day1=lastday; 
	}
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec";
	if(month1 == 1)month1="Jan";
	if(month1 == 2)month1="Feb";
	if(month1 == 3)month1="Mar";
	if(month1 == 4)month1="Apr";
	if(month1 == 5)month1="May";
	if(month1 == 6)month1="Jun";
	if(month1 == 7)month1="Jul";
	if(month1 == 8)month1="Aug";
	if(month1 == 9)month1="Sep";
	if(month1 == 10)month1="Oct";
	if(month1 == 11)month1="Nov";
	if(month1 == 12)month1="Dec";
	
	if (day<10){
		day="0"+day
	}
	var y=""+year; 
	var fromdate=day+"-"+month+"-"+y.substring(2,4);
	var todate=day1+"-"+month1+"-"+y.substring(2,4);
	if(temp.value!=''){
	document.forms['analysisForm'].elements[from].value=fromdate;
	document.forms['analysisForm'].elements[to].value=todate;
	} else if(temp.value==''){
	document.forms['analysisForm'].elements[from].value='';
	document.forms['analysisForm'].elements[to].value='';
	}
}

function getLastDayOfMonth(month,year){
    var day;
    switch(month){
        case 1 :
        case 3 :
        case 5 :
        case 7 :
        case 8 :
        case 10 :
        case 12 :
            day = 31;
            break;
        case 4 :
        case 6 :
        case 9 :
        case 11 :
               day = 30;
            break;
        case 2 :
            if( ( (year % 4 == 0) && ( year % 100 != 0) )
                           || (year % 400 == 0) )
                day = 29;
            else
                day = 28;
            break;
    }
    return day;
}

function checkDateChange(){
	var beginDate = document.forms['analysisForm'].elements['fromDate'].value;
	var endDate = document.forms['analysisForm'].elements['toDate'].value;
	
   var mySplitResult = endDate.split("-");
   var formEndDay = mySplitResult[0];
   var formEndMonth = mySplitResult[1];
   var formEndYear = mySplitResult[2];
   
   var mySplitResult1 = beginDate.split("-");
   var formBeginDay = mySplitResult1[0];
   var formBeginMonth = mySplitResult1[1];
   var formBeginYear = mySplitResult1[2];	
	    
    var mydate=new Date();
	var year=mydate.getFullYear()		
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var day1=mydate.getDay()
	var month=mydate.getMonth()+1
	var month1=mydate.getMonth()+1
	var y=""+year;
	
	if(month == 'Jan')month="1";
	if(month == 'Feb')month="2";
	if(month == 'Mar')month="3";
	if(month == 'Apr')month="4";
	if(month == 'May')month="5";
	if(month == 'Jun')month="6";
	if(month == 'Jul')month="7";
	if(month == 'Aug')month="8";
	if(month == 'Sep')month="9";
	if(month == 'Oct')month="10";
	if(month == 'Nov')month="11";
	if(month == 'Dec')month="12";
	if(month1 == 'Jan')month1="1";
	if(month1 == 'Feb')month1="2";
	if(month1 == 'Mar')month1="3";
	if(month1 == 'Apr')month1="4";
	if(month1 == 'May')month1="5";
	if(month1 == 'Jun')month1="6";
	if(month1 == 'Jul')month1="7";
	if(month1 == 'Aug')month1="8";
	if(month1 == 'Sep')month1="9";
	if(month1 == 'Oct')month1="10";
	if(month1 == 'Nov')month1="11";
	if(month1 == 'Dec')month1="12";

	  var periodVal = document.forms['analysisForm'].elements['loadTargetDatePeriod'].value;
	  if(periodVal=='CurrentYear'){
		  	year=year;
			month="Jan";
			month1="Dec";
			day=1;
			day1=31;			
	  	}
	  if(periodVal=='LastYear'){
		    year=year-1;
		    month="Jan";
			month1="Dec";
			day=1;
			day1=31;
			
	  }
	  if(periodVal=='LastHalfYear'){
		  if(month>6){
				year=year;
				month="Jan";
				month1="Jun";
				day=1;
				day1=30;				
				}else if(month<6|| month==6){
				year=year-1;
				month="Jul";
				month1="Dec";
				day=1;
				day1=31;
			}		  
	 }
	  if(periodVal=='CurrentHalfYear'){
		  if(month>6){
				year=year;
				month="Jul";
				month1="Dec";
				day=1;
				day1=31; 
				}else if(month<6|| month==6){
				year=year;
				month="Jan";
				month1="Jun";
				day=1;
				day1=30; 
			}			
	 }
	  if(periodVal=='LastQuarter'){
		  if(month<3 || month==3){
				year=year-1;
				month="Oct";
				month1="Dec";
				day=1;
				day1=31; 
				}else if((month>3 && month<6)||month==6){
				year=year;
				month="Jan";
				month1="Mar";
				day=1;
				day1=31;  
				} else if((month>6&& month<9)||month==9){
				year=year;
				month="Apr";
				month1="Jun";
				day=1;
				day1=30; 
				} else if((month>9)){
				year=year;
				month="Jul";
				month1="Sep";
				day=1;
				day1=30;  
			}
			
	 }
	  if(periodVal=='CurrentQuarter'){
		  if(month<3 || month==3){
				year=year;
				month="Jan";
				month1="Mar";
				day=1;
				day1=31; 
				}else if((month>3 && month<6)||month==6){
				year=year;
				month="Apr";
				month1="Jun";
				day=1;
				day1=30;  
				} else if((month>6&& month<9)||month==9){
				year=year;
				month="Jul";
				month1="Sep";
				day=1;
				day1=30; 
				} else if((month>9)){
				year=year;
				month="Oct";
				month1="Dec";
				day=1;
				day1=31;  
			} 
			
	 }
	  if(periodVal=='LastMonth'){
		  year=year;
			if(month==1){
			month="Dec";
			month1="Dec";
			year=year-1;
			}else{
			month=month-1;
			month1=month1-1;
			}
			day=1; 
			var lastday = getLastDayOfMonth(month,year); 
			day1=lastday; 	
			
			if(month == 1)month="Jan";
			if(month == 2)month="Feb";
			if(month == 3)month="Mar";
			if(month == 4)month="Apr";
			if(month == 5)month="May";
			if(month == 6)month="Jun";
			if(month == 7)month="Jul";
			if(month == 8)month="Aug";
			if(month == 9)month="Sep";
			if(month == 10)month="Oct";
			if(month == 11)month="Nov";
			if(month == 12)month="Dec";
			if(month1 == 1)month1="Jan";
			if(month1 == 2)month1="Feb";
			if(month1 == 3)month1="Mar";
			if(month1 == 4)month1="Apr";
			if(month1 == 5)month1="May";
			if(month1 == 6)month1="Jun";
			if(month1 == 7)month1="Jul";
			if(month1 == 8)month1="Aug";
			if(month1 == 9)month1="Sep";
			if(month1 == 10)month1="Oct";
			if(month1 == 11)month1="Nov";
			if(month1 == 12)month1="Dec";
	  }
	  if(periodVal=='CurrentMonth'){
		  	year=year;
			month=month;
			month1=month1;
			day=1; 
			var lastday = getLastDayOfMonth(month,year); 
			day1=lastday;
			
			if(month == 1)month="Jan";
			if(month == 2)month="Feb";
			if(month == 3)month="Mar";
			if(month == 4)month="Apr";
			if(month == 5)month="May";
			if(month == 6)month="Jun";
			if(month == 7)month="Jul";
			if(month == 8)month="Aug";
			if(month == 9)month="Sep";
			if(month == 10)month="Oct";
			if(month == 11)month="Nov";
			if(month == 12)month="Dec";
			if(month1 == 1)month1="Jan";
			if(month1 == 2)month1="Feb";
			if(month1 == 3)month1="Mar";
			if(month1 == 4)month1="Apr";
			if(month1 == 5)month1="May";
			if(month1 == 6)month1="Jun";
			if(month1 == 7)month1="Jul";
			if(month1 == 8)month1="Aug";
			if(month1 == 9)month1="Sep";
			if(month1 == 10)month1="Oct";
			if(month1 == 11)month1="Nov";
			if(month1 == 12)month1="Dec";
	  }
	  if(periodVal=='LastYear'){
			var year=mydate.getFullYear()		
			if (year < 1000)
			year+=1900
			year=year-1;
			y=""+year;
		}
		if (day<10){
			day="0"+day
		}
		
		if(formBeginDay!=day || formBeginMonth!=month || formBeginYear!=y.substring(2,4)){
			document.forms['analysisForm'].elements['loadTargetDatePeriod'].value='';
		}			
		
		if(formEndDay!=day1 || formEndMonth!=month1 || formEndYear!=y.substring(2,4)){
			document.forms['analysisForm'].elements['loadTargetDatePeriod'].value='';
		}
}
//action -- >>  reciprocityAnalysisReport
</script>
</head>

<s:form name="analysisForm" id="analysisForm" action="reciprocityAnalysisReport" method="post" validate="true">
<div id="Layer1" style="width:100%;">
<c:set var="FormDateValue" value="dd-NNN-yy"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden id="checkDaysClickTicket" name="checkDaysClickTicket" />
<configByCorp:fieldVisibility componentId="component.tab.ReciprocityAnalysis.subOADA">
<s:hidden  id="subOADAReciprocityAnalysis" name="subOADAReciprocityAnalysis" value="Yes" />
</configByCorp:fieldVisibility>
<s:hidden name="formStatus" value=""/>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table cellspacing="2" cellpadding="2" border="0" style="width:75%" >	
		<tr>
			<td class="bgblue" colspan="7">Reciprocity Analysis</td>
		</tr>
		<tr><td height="10px"></td></tr>
		<tr>		  		
				<td align="right" width="150" class="listwhitetext">Reciprocity&nbsp;Analysis&nbsp;From<font color="red" size="2">*</font></td>	
				<td colspan="2">
				<table cellspacing="0" cellpadding="0" style="margin:0px;">
			  <tr>			  
		  	  	<td> 	 
			  		<s:textfield cssClass="input-text" id="fromDate" name="fromDate" value="%{fromDate}" cssStyle="width:65px;" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this);" /> 
			  		<img id="fromDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays();" />
			  	</td>
				<td width="10"></td>
				<td align="right" class="listwhitetext"> To&nbsp;</td>		
			  	<td align="left" colspan="1" class="listwhitetext">
			  		<s:textfield cssClass="input-text" id="toDate" name="toDate" value="%{toDate}" cssStyle="width:65px;" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this);"/> 
			  		<img id="toDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays();" />
			  	</td>
			  	</tr>
			  </table>
			  </td>
			  	
			  	<td align="right" class="listwhitetext">Period</td>
			  	<td><s:select cssClass="list-menu" cssStyle="width:150px"  name="loadTargetDatePeriod"  list="%{periodScheduler}"  headerKey="" headerValue=""  onchange="checkPeriodDate(this,'fromDate','toDate');" />
			  	</td>			  
		</tr> 
		
		<tr>
		    <td align="right" class="listwhitetext">Agent Group</td>
			<td  align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" name="agentGroup"  cssStyle="width:205px;" maxlength="30"    /> </td>
			<td align="right" class="listwhitetext">Country</td> 
			<td  align="left" colspan="3" class="listwhitetext"><s:select cssClass="list-menu" name="countryCode" list="%{country}" cssStyle="width:150px"  headerKey="" headerValue="" /></td>
		</tr>
		<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.millitaryShipment">
		<tr>
		    <td align="right" class="listwhitetext">Military Shipment</td>	
			<td  align="left"><s:checkbox name="militaryShipment" cssStyle="margin:0px;" /> </td>
			
			<td  align="left"></td>
		</tr>
		</configByCorp:fieldVisibility>
		<tr> 
	  	   <td class="listwhitetext" align="right">Job Type</td> 
	  	   
	  	   <td width="130px" style="padding-bottom:0px; padding-left:0px;"><s:select cssClass="list-menu" cssStyle="width:210px;height:50px"  name="jobTypes" list="%{jobtype}"   value="%{multiplejobType}" multiple="true"  /></td>
	  	   <td class="listwhitetext" align="left" colspan="4" style="font-size:10px; font-style: italic;">
            <b>* Use Control + mouse to select multiple Job Type</b>
           </td> 
	    </tr>
	    <tr><td height="5px"></td></tr>
	  	<tr> 
	  	   <td class="listwhitetext" align="right">Company Division</td>
	  	
	  	   <td width="130px" style="padding-bottom:0px; padding-left:0px;" ><s:select cssClass="list-menu" cssStyle="width:210px;height:50px"  name="companyDivisions" list="%{CompanyDivisionList}"   value="%{multipleCompanyDivision}" multiple="true"  /></td>
  	       <td class="listwhitetext" align="left" colspan="4" style="font-size:10px; font-style: italic;">
            <b>* Use Control + mouse to select multiple Company Division</b>
           </td> 
	   </tr>
	  <tr><td height=""></td></tr>
		<tr>	
		  	<td width=""></td>			
			<td colspan="3"><s:submit cssClass="cssbutton" cssStyle="width:150px;margin-left:5px;"  value="Report By Agent Group" onclick="return fieldValidate();"/></td>
			<td colspan="3"><input type="button"  class="cssbutton" style="width:150px;" value="Report By Country" onclick="return fieldValidate1();"/></td>
		</tr>
		<tr><td height="20"></td></tr>	 				
	</table>  
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
</div>     
</s:form>

<script type="text/javascript">
	setOnSelectBasedMethods(["checkDateChange();"]);
	setCalendarFunctionality();
</script>
