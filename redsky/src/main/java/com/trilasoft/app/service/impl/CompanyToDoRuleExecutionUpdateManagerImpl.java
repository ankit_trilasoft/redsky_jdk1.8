package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CompanyToDoRuleExecutionUpdateDao;
import com.trilasoft.app.model.CompanyToDoRuleExecutionUpdate;
import com.trilasoft.app.service.CompanyToDoRuleExecutionUpdateManager;

public class CompanyToDoRuleExecutionUpdateManagerImpl extends GenericManagerImpl<CompanyToDoRuleExecutionUpdate, Long> implements CompanyToDoRuleExecutionUpdateManager {
	CompanyToDoRuleExecutionUpdateDao companyToDoRuleExecutionUpdateDao;

	public CompanyToDoRuleExecutionUpdateManagerImpl(CompanyToDoRuleExecutionUpdateDao companyToDoRuleExecutionUpdateDao) {
		super(companyToDoRuleExecutionUpdateDao);
		this.companyToDoRuleExecutionUpdateDao = companyToDoRuleExecutionUpdateDao;
	}

	public List<CompanyToDoRuleExecutionUpdate> findByCorpID(String corpId){
		return companyToDoRuleExecutionUpdateDao.findByCorpID(corpId);
	}
	 

}
