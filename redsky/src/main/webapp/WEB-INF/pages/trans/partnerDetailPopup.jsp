<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="partnerDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerDetail.heading'/>"/>  
    
<style type="text/css">
		h2 {background-color: #FBBFFF}
		
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>

<script language="javascript" type="text/javascript">
function onLoad() {
	//document.forms['partnerAddForm'].elements['partner.billingCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.billingCountry'].value;
	//document.forms['partnerAddForm'].elements['partner.terminalCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.terminalCountry'].value;
	//document.forms['partnerAddForm'].elements['partner.mailingCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.mailingCountry'].value;
	//document.forms['partnerAddForm'].elements['partner.billingInstructionCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.billingInstructionCode'].value+' : '+document.forms['partnerAddForm'].elements['partner.billingInstruction'].value;
	if(navigator.appName == 'Microsoft Internet Explorer'){
			document.forms['partnerAddForm'].elements['dateFormat'].value="MM/dd/yyyy";
		}
		if(navigator.appName == 'Netscape'){
			document.forms['partnerAddForm'].elements['dateFormat'].value="MM/dd/yyyy";
		}
	var f = document.getElementById('partnerAddForm'); 
	f.setAttribute("autocomplete", "off"); 
	
}

</script>
	<script type="text/javascript"> 
		function copyBillToMail(){ 
			document.forms['partnerAddForm'].elements['partner.mailingAddress1'].value=document.forms['partnerAddForm'].elements['partner.billingAddress1'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress2'].value=document.forms['partnerAddForm'].elements['partner.billingAddress2'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress3'].value=document.forms['partnerAddForm'].elements['partner.billingAddress3'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress4'].value=document.forms['partnerAddForm'].elements['partner.billingAddress4'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingEmail'].value=document.forms['partnerAddForm'].elements['partner.billingEmail'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingCity'].value=document.forms['partnerAddForm'].elements['partner.billingCity'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingFax'].value=document.forms['partnerAddForm'].elements['partner.billingFax'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingZip'].value=document.forms['partnerAddForm'].elements['partner.billingZip'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value=document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingPhone'].value=document.forms['partnerAddForm'].elements['partner.billingPhone'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingTelex'].value=document.forms['partnerAddForm'].elements['partner.billingTelex'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingState'].value=document.forms['partnerAddForm'].elements['partner.billingState'].value;		
			//document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value=document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value;		
			//document.forms['partnerAddForm'].elements['partner.mailingCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.billingCountryCodeTemp'].value;		
			
		}
		function copyTermToMail(){ 
			document.forms['partnerAddForm'].elements['partner.mailingAddress1'].value=document.forms['partnerAddForm'].elements['partner.terminalAddress1'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress2'].value=document.forms['partnerAddForm'].elements['partner.terminalAddress2'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress3'].value=document.forms['partnerAddForm'].elements['partner.terminalAddress3'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingAddress4'].value=document.forms['partnerAddForm'].elements['partner.terminalAddress4'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingEmail'].value=document.forms['partnerAddForm'].elements['partner.terminalEmail'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingCity'].value=document.forms['partnerAddForm'].elements['partner.terminalCity'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingFax'].value=document.forms['partnerAddForm'].elements['partner.terminalFax'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingZip'].value=document.forms['partnerAddForm'].elements['partner.terminalZip'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value=document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingPhone'].value=document.forms['partnerAddForm'].elements['partner.terminalPhone'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingTelex'].value=document.forms['partnerAddForm'].elements['partner.terminalTelex'].value;		
			document.forms['partnerAddForm'].elements['partner.mailingState'].value=document.forms['partnerAddForm'].elements['partner.terminalState'].value;		
			//document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value=document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value;		
			//document.forms['partnerAddForm'].elements['partner.mailingCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.terminalCountryCodeTemp'].value;		
		}
		function copyBillToTerm(){ 
			document.forms['partnerAddForm'].elements['partner.terminalAddress1'].value=document.forms['partnerAddForm'].elements['partner.billingAddress1'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalAddress2'].value=document.forms['partnerAddForm'].elements['partner.billingAddress2'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalAddress3'].value=document.forms['partnerAddForm'].elements['partner.billingAddress3'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalAddress4'].value=document.forms['partnerAddForm'].elements['partner.billingAddress4'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalEmail'].value=document.forms['partnerAddForm'].elements['partner.billingEmail'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalCity'].value=document.forms['partnerAddForm'].elements['partner.billingCity'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalFax'].value=document.forms['partnerAddForm'].elements['partner.billingFax'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalZip'].value=document.forms['partnerAddForm'].elements['partner.billingZip'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value=document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalPhone'].value=document.forms['partnerAddForm'].elements['partner.billingPhone'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalTelex'].value=document.forms['partnerAddForm'].elements['partner.billingTelex'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalState'].value=document.forms['partnerAddForm'].elements['partner.billingState'].value;		
			//document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value=document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value;		
			//document.forms['partnerAddForm'].elements['partner.terminalCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.billingCountryCodeTemp'].value;		
		}
		function copyMailToTerm(){ 
			document.forms['partnerAddForm'].elements['partner.terminalAddress1'].value=document.forms['partnerAddForm'].elements['partner.mailingAddress1'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalAddress2'].value=document.forms['partnerAddForm'].elements['partner.mailingAddress2'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalAddress3'].value=document.forms['partnerAddForm'].elements['partner.mailingAddress3'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalAddress4'].value=document.forms['partnerAddForm'].elements['partner.mailingAddress4'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalEmail'].value=document.forms['partnerAddForm'].elements['partner.mailingEmail'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalCity'].value=document.forms['partnerAddForm'].elements['partner.mailingCity'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalFax'].value=document.forms['partnerAddForm'].elements['partner.mailingFax'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalZip'].value=document.forms['partnerAddForm'].elements['partner.mailingZip'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value=document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalPhone'].value=document.forms['partnerAddForm'].elements['partner.mailingPhone'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalTelex'].value=document.forms['partnerAddForm'].elements['partner.mailingTelex'].value;		
			document.forms['partnerAddForm'].elements['partner.terminalState'].value=document.forms['partnerAddForm'].elements['partner.mailingState'].value;		
			//document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value=document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value;		
			//document.forms['partnerAddForm'].elements['partner.terminalCountryCodeTemp'].value=document.forms['partnerAddForm'].elements['partner.mailingCountryCodeTemp'].value;		
		}
	</script>
	<script type="text/javascript">
/*		function autoPopulate_partner_billingCountry(targetElement) {
		
			var billingCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['partnerAddForm'].elements['partner.billingCountryCode'].value=billingCountryCode.substring(0,billingCountryCode.indexOf(":")-1);
			targetElement.form.elements['partner.billingCountry'].value=billingCountryCode.substring(billingCountryCode.indexOf(":")+2,billingCountryCode.length);
		}
		function autoPopulate_partner_terminalCountry(targetElement) {
		
			var terminalCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['partnerAddForm'].elements['partner.terminalCountryCode'].value=terminalCountryCode.substring(0,terminalCountryCode.indexOf(":")-1);
			targetElement.form.elements['partner.terminalCountry'].value=terminalCountryCode.substring(terminalCountryCode.indexOf(":")+2,terminalCountryCode.length);
		}
		function autoPopulate_partner_mailingCountry(targetElement) {
			
			var mailingCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['partnerAddForm'].elements['partner.mailingCountryCode'].value=mailingCountryCode.substring(0,mailingCountryCode.indexOf(":")-1);
			targetElement.form.elements['partner.mailingCountry'].value=mailingCountryCode.substring(mailingCountryCode.indexOf(":")+2,mailingCountryCode.length);
	   }
*/	  
	function autoPopulate_partner_billingInstruction(targetElement) {
		//var billingInstructionCode=targetElement.options[targetElement.selectedIndex].value;
	    //document.forms['partnerAddForm'].elements['partner.billingInstructionCode'].value=targetElement.options[targetElement.selectedIndex].value;
		//targetElement.form.elements['partner.billingInstruction'].value=billingInstructionCode.substring(billingInstructionCode.indexOf(":")+2,billingInstructionCode.length);
	}
	</script>
	<script language="JavaScript">
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
</script>	
<script type="text/javascript">

// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
var s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;
	
	if (checkInternationalPhone(Phone)==false){
		alert("Please Enter a Valid Phone Number")
		targetElelemnt.value="";
		//document.forms['partnerAddForm'].elements[targetElelemnt].focus();
		//targetElement.focus = true;
		return false;
	}
	return true;
 }
 
 
function submitForm(){
	var partnerCode= document.forms['partnerAddForm'].elements['partner.partnerCode'].value;
	var lastName= document.forms['partnerAddForm'].elements['partner.lastName'].value;
	document.forms['partnerAddForm'].action="savePartnerPopupForBiling.html";
	document.forms['partnerAddForm'].submit();
	
}

function userauth(){
	if(document.forms['partnerAddForm'].elements['partner.lastName'].value == ''){
	 alert("Last Name of Customer is required field");
	 return false;
	}else{
		submitForm();
	}
}
</script>
<style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}

</style>	
</head> 

<s:form id="partnerAddForm" name="partnerAddForm" action='${empty param.popup?"savePartner.html":"savePartner.html?decorator=popup&popup=true"}' method="post" validate="true">   
<s:hidden name="popupval" value="${papam.popup}"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="<%=request.getParameter("fld_code") %>" />
	<s:hidden name="fld_description" value="<%=request.getParameter("fld_description") %>" />	
	<s:hidden name="fld_secondDescription" value="<%=request.getParameter("fld_secondDescription") %>" />	
	<s:hidden name="fld_thirdDescription" value="<%=request.getParameter("fld_thirdDescription") %>" />	
	<s:hidden name="fld_fourthDescription" value="<%=request.getParameter("fld_fourthDescription") %>" />	
	<s:hidden name="fld_fifthDescription" value="<%=request.getParameter("fld_fifthDescription") %>" />	
	<s:hidden name="fld_sixthDescription" value="<%=request.getParameter("fld_sixthDescription") %>" />	
	<c:set var="fld_code" value="<%=request.getParameter("fld_code") %>" />
	<c:set var="fld_description" value="<%=request.getParameter("fld_description") %>" />	
	<c:set var="fld_secondDescription" value="<%=request.getParameter("fld_secondDescription") %>" />	
	<c:set var="fld_thirdDescription" value="<%=request.getParameter("fld_thirdDescription") %>" />	
	<c:set var="fld_fourthDescription" value="<%=request.getParameter("fld_fourthDescription") %>" />	
	<c:set var="fld_fifthDescription" value="<%=request.getParameter("fld_fifthDescription") %>" />	
	<c:set var="fld_sixthDescription" value="<%=request.getParameter("fld_sixthDescription") %>" />	
</c:if>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="partner.id" />
<s:hidden name="partner.corpID" />
<s:hidden name="partner.isAccountParty" />
<s:hidden id="formClose" name="formClose" />
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />

<s:hidden name="popup" />

<!--      <s:hidden name="partner.partnerCode" value="%{partner.partnerCode}"/>  -->  
<c:if test="${empty partner.id && partnerType == 'PP'}">
	<div id="newmnav" style="padding:20px 20px;">
		<ul>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Private Party Detail</span></a></li>
			<li><a href="searchPartner.html?id=${partner.id}&partnerType=PP&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Partner List</span></a></li>
		</ul>
	</div><div class="spn">&nbsp;</div><br>	
</c:if>
<c:if test="${not empty partner.id}">
<div id="newmnav" style="padding:20px 20px;">
				  <ul>
				  	<c:if test="${partnerType == 'AC'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				   		<li><a href="searchPartner.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account List</span></a></li>
			  			<li><a href="accountInfoPage.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Info</span></a></li>
			  			<li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Profile</span></a></li>
			  			<li><a href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Contact</span></a></li>
			  			<li><a href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Policy</span></a></li>
				    </c:if>
				    <c:if test="${partnerType == 'AG'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    	<li><a href="searchPartner.html?id=${partner.id}&partnerType=AG&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Agent List</span></a></li>
				    </c:if>
				    <c:if test="${partnerType == 'VN'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Vendor Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    	<li><a href="searchPartner.html?id=${partner.id}&partnerType=VN&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Vendor List</span></a></li>
				    </c:if>
				    <c:if test="${partnerType == 'PP'}">
						<li id="newmnav1" style="background:#FFF "><a class="current"><span>Private Party Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						<li><a href="searchPartner.html?id=${partner.id}&partnerType=PP&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Partner List</span></a></li>
					</c:if>
				    <c:if test="${partnerType == 'CR'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Carrier Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    	<li><a href="searchPartner.html?id=${partner.id}&partnerType=CR&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Carrier List</span></a></li>
				    </c:if>
				    <c:if test="${partnerType == 'OO'}">
				    	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Owner Ops<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    	<li><a href="searchPartner.html?id=${partner.id}&partnerType=OO&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Owner Ops List</span></a></li>
				    </c:if> 
			</ul>
		</div><div class="spn">&nbsp;</div><br>
</c:if>
<div id="Layer1" style="padding-left:20px ;">
<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
				<table cellspacing="0" cellpadding="3" border="0">
							<tbody>
								<tr>
									<td align="left" class="listwhitetext">Name<font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerSuffix'/></td>
									<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode'/><font color="red" size="2">*</font></td>
								</tr>
								<tr>
									<s:hidden key="partner.firstName" value=""/>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.lastName" required="true" cssClass="input-text"
									size="79" maxlength="80"  onkeydown="return onlyCharsAllowed(event)"/> </td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerSuffix" size="1" required="true"
									cssClass="input-text" maxlength="10" onkeydown="return onlyCharsAllowed(event)"/></td>
									<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerCode"
									required="true" cssClass="input-textUpper" maxlength="8" size="15" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true"/> </td>
								</tr>
							</tbody>
						</table>
					<table>
						<tbody>
							<tr><td align="left"><font color="#003366">Billing Address</font></td></tr>
							<tr>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress1"  size="50" maxlength="30"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingCountryCode'/></td>
								<td><s:select cssClass="list-menu" name="partner.billingCountryCode" list="%{country}" cssStyle="width:162px"  headerKey="" headerValue="" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingFax'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingFax" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
							</tr>
							<tr>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress2" size="50" maxlength="30"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingCity'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingCity" cssStyle="width:162px" maxlength="20" onkeydown="return onlyCharsAllowed(event)"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingPhone'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingPhone" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
							</tr>
							<tr>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress3" size="50" maxlength="30"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingState'/></td>
								<td><s:select name="partner.billingState" list="%{bstates}" cssStyle="width:162px"  headerKey="" headerValue=""/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingTelex'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingTelex" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
							</tr>
							<tr>
								<td colspan="2"><s:textfield cssClass="input-text" name="partner.billingAddress4" size="50" maxlength="30"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingZip'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingZip" cssStyle="width:162px"  maxlength="10" onkeydown="return onlyNumsAllowed(event)"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.billingEmail'/></td>
								<td><s:textfield cssClass="input-text" name="partner.billingEmail"  size="22" maxlength="65"/></td>
							</tr>
						</tbody>
					</table>
					<c:if test="${partnerType == 'VN' || partnerType == 'CR' || partnerType == 'AG' || partnerType == '' }">
					<table>
						<tbody>
							<tr><td align="left"><font color="#003366">Terminal Address</font></td></tr>
							<tr>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.terminalAddress1"  size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalCountryCode'/></td>
									<td><s:select cssClass="list-menu" name="partner.terminalCountryCode" list="%{country}" cssStyle="width:162px"   headerKey="" headerValue="" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalFax'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalFax" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
							</tr>
							<tr>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.terminalAddress2" size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalCity'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalCity" cssStyle="width:162px"  maxlength="15" onkeydown="return onlyCharsAllowed(event)"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalPhone'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalPhone" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
							</tr>
							<tr>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.terminalAddress3" size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalState'/></td>
									<td><s:select name="partner.terminalState" list="%{tstates}" cssStyle="width:162px"  headerKey="" headerValue="" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalTelex'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalTelex" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
							</tr>
							<tr>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.terminalAddress4" size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalZip'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalZip" cssStyle="width:162px"  maxlength="10" onkeydown="return onlyNumsAllowed(event)"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.terminalEmail'/></td>
									<td><s:textfield cssClass="input-text" name="partner.terminalEmail"  size="22"  maxlength="65"/></td>
									
							</tr>
							
						</tbody>
					</table>
					</c:if>
					<table>
						<tbody>
								<tr><td align="left"><font color="#003366">Mailing Address</font></td></tr>
								<tr>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress1"  size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCountryCode'/></td>
									<td><s:select cssClass="list-menu" name="partner.mailingCountryCode" list="%{country}" cssStyle="width:162px"  headerKey="" headerValue=""/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingFax'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingFax" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
								</tr>
								<tr>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress2" size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCity'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingCity" cssStyle="width:162px" maxlength="15" onkeydown="return onlyCharsAllowed(event)"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingPhone'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingPhone" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
								</tr>
								<tr>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress3" size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingState'/></td>
									<td><s:select name="partner.mailingState" list="%{mstates}" cssStyle="width:162px"  headerKey="" headerValue="" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingTelex'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingTelex" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
								</tr>
								<tr>
									<td colspan="2"><s:textfield cssClass="input-text" name="partner.mailingAddress4" size="50" maxlength="30"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingZip'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingZip" cssStyle="width:162px"  maxlength="10" onkeydown="return onlyNumsAllowed(event)"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingEmail'/></td>
									<td><s:textfield cssClass="input-text" name="partner.mailingEmail"  size="22" maxlength="65"/></td>
									
								</tr>
								
						</tbody>
					</table>
				
			</td>
		</tr>

<tr>
<td height="10" align="left" class="listwhitetext" colspan="2">
					<c:set var="isAccountFlag" value="false"/>
					<c:set var="isAgentFlag" value="false"/>
					<c:set var="isBrokerFlag" value="false"/>
					<c:set var="isVendorFlag" value="false"/>
					<c:set var="isCarrierFlag" value="false"/>
					<c:set var="isOwnerOpFlag" value="false"/>
					<c:set var="qcFlag" value="false"/>
					<c:set var="fidiFlag" value="false"/>
					<c:set var="privateFlag" value="false"/>
					<c:set var="seaFlag" value="false" />
					<c:set var="surfaceFlag" value="false" />
					<c:set var="airFlag" value="false" />
					<c:if test="${partner.isAccount}">
						 <c:set var="isAccountFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isAgent}">
						 <c:set var="isAgentFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isBroker}">
						 <c:set var="isBrokerFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isVendor}">
						 <c:set var="isVendorFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isCarrier}">
						 <c:set var="isCarrierFlag" value="true"/>
					</c:if>
					<c:if test="${partner.qc}">
						 <c:set var="qcFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isPrivateParty}">
						 <c:set var="privateFlag" value="true"/>
					</c:if>
					<c:if test="${partner.sea}">
						 <c:set var="seaFlag" value="true"/>
					</c:if>
					<c:if test="${partner.surface}">
						 <c:set var="surfaceFlag" value="true"/>
					</c:if>
					<c:if test="${partner.air}">
						 <c:set var="airFlag" value="true"/>
					</c:if>
					<c:if test="${partner.isOwnerOp}">
						 <c:set var="isOwnerOpFlag" value="true"/>
					</c:if>
											
<c:choose>
<c:when test="${partnerType == 'CR'}">
<s:hidden name="partner.isAccount" value="true" />
<s:hidden name="partner.isAgent" value="false" />
<s:hidden name="partner.isBroker" value="false" />
<s:hidden name="partner.isVendor" value="false" />
<s:hidden name="partner.qc" value="false" />
<s:hidden name="partner.isPrivateParty" value="false" />
<s:hidden name="partner.isCarrier" value="true" />

<table>
<tbody>
<tr>
<td colspan="3" align="center">
	<table>
		<tr>
			<td>Carrier Detail</td>
			<td style="color:#003366;font-size:11px;font-family:arial,verdana;font-weight:normal;margin-left:30px"><s:checkbox key="partner.sea" value="${seaFlag}" fieldValue="true"/>Sea</td>
			<td style="color:#003366;font-size:11px;font-family:arial,verdana;font-weight:normal;margin-left:10px"><s:checkbox key="partner.surface" value="${surfaceFlag}" fieldValue="true"/>Surface</td>
			<td style="color:#003366;font-size:11px;font-family:arial,verdana;font-weight:normal;margin-left:10px"><s:checkbox key="partner.air" value="${airFlag}" fieldValue="true"/>Air</td>
		</tr>
	</table>
</td>
</tr>
</tbody>
</table>
</c:when>
<c:when test="${partnerType == 'AC'}">
<s:hidden name="partner.isAccount" value="true" />
<s:hidden name="partner.isAgent" value="false" />
<s:hidden name="partner.isBroker" value="false" />
<s:hidden name="partner.isVendor" value="false" />
<s:hidden name="partner.isCarrier" value="false" />
<s:hidden name="partner.qc" value="false" />
<s:hidden name="partner.isPrivateParty" value="false" />
<s:hidden name="partner.sea" value="false" />
<s:hidden name="partner.surface" value="false" />
<s:hidden name="partner.air" value="false" />
</c:when>
<c:when test="${partnerType == 'PP'}">
<s:hidden name="partner.isAccount" value="false" />
<s:hidden name="partner.isAgent" value="false" />
<s:hidden name="partner.isBroker" value="false" />
<s:hidden name="partner.isVendor" value="false" />
<s:hidden name="partner.isCarrier" value="false" />
<s:hidden name="partner.qc" value="false" />
<s:hidden name="partner.isPrivateParty" value="true" />
<s:hidden name="partner.sea" value="false" />
<s:hidden name="partner.surface" value="false" />
<s:hidden name="partner.air" value="false" />
<s:hidden name="partner.status" value="Approved" />

</c:when>

<c:otherwise>
</c:otherwise>
</c:choose>
</td>
</tr>


<tr>

<td height="10" align="left" class="listwhitetext" colspan="2">
<c:if test="${partnerType == 'CR'}">
<div class="subcontent-tab"><a href="javascript:void(0)"
class="dsphead" onclick="dsp(this)"><span
class="dspchar2" ><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" border="0" align="absmiddle" /></span>&nbsp;&nbsp;Forwarding Details</a></div>
				
  
<div class="dspcont" id="Layer1">

<table class="detailTabLabel" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
				
					<table>
					<tr><td colspan="3">
					<dl>
					<dt><font size="2">Remarks/Fees....</font></dt>
					</dl>
					</td></tr>
					<tr><td><font size="1" color="black">Per Diem Costs</font></td><td></td><td><font size="1" color="black">Five Lines Of Remarks</font></td></tr>
					<tr><td align="right" class="listwhitetext"><fmt:message key='partner.perDiemFreeDays'/></td><td><s:textfield cssClass="input-text" name="partner.perDiemFreeDays"  size="10" maxlength="11" onkeydown="return onlyNumsAllowed(event)"/></td><td></td><td><s:textfield name="partner.remarks1"  cssClass="text large" maxlength="50"/></td></tr>
					<tr><td align="right" class="listwhitetext"><fmt:message key='partner.perDiemDays2'/></td><td><s:textfield cssClass="input-text" name="partner.perDiemDays2"  size="10" maxlength="11" onkeydown="return onlyNumsAllowed(event)"/></td><td></td><td><s:textfield name="partner.remarks2"  cssClass="text large" maxlength="50"/></td></tr>
					<tr><td align="right" class="listwhitetext"><fmt:message key='partner.perDiemCost1'/></td><td><s:textfield cssClass="input-text" name="partner.perDiemCost1"  size="10" maxlength="7" onkeydown="return onlyFloatNumsAllowed(event)"/></td><td></td><td><s:textfield name="partner.remarks3"  cssClass="text large" maxlength="50"/></td></tr>
					<tr><td align="right" class="listwhitetext"><fmt:message key='partner.perDiemCost2'/></td><td><s:textfield cssClass="input-text" name="partner.perDiemCost2"  size="10" maxlength="7" onkeydown="return onlyFloatNumsAllowed(event)"/></td><td></td><td><s:textfield name="partner.remarks4"  cssClass="text large" maxlength="50"/></td></tr>
					<tr><td></td><td></td><td></td><td><s:textfield name="partner.remarks5"  cssClass="text large" maxlength="50"/></td></tr>
					<tr height="9" />
				</table>
				
			</td>
		</tr>
	</tbody>
</table>

</div>
</c:if>
</td>
</tr>
<tr>
	<td height="10" align="left" class="listwhitetext"></td>
</tr>
<tr>
<td align="center">
<table>
<tbody>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partner.createdOn'/></td>
						<c:if test="${not empty partner.createdOn}">
							<s:text id="createdOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.createdOn"/></s:text>
							<td><s:hidden cssClass="input-text" id="createdOn" name="partner.createdOn" value="%{createdOnFormattedValue}" /></td>
						</c:if>
						<c:if test="${empty partner.createdOn}">
							<td><s:hidden cssClass="input-text" id="createdOn" name="partner.createdOn" /></td>
						</c:if>
						
						<td style="width:120px"><fmt:formatDate value="${partner.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext"><fmt:message key='partner.createdBy' /></td>
						<c:if test="${not empty partner.id}">
								<s:hidden name="partner.createdBy"/>
								<td><s:label name="createdBy" value="%{partner.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="partner.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext"><fmt:message key='partner.updatedOn'/></td>
						<s:hidden name="partner.updatedOn"/>
						<td style="width:120px"><fmt:formatDate value="${partner.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext"><fmt:message key='partner.updatedBy' /></td>
						
						<c:if test="${not empty partner.id}">
								<s:hidden name="partner.updatedBy"/>
								<td><s:label name="updatedBy" value="%{partner.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="partner.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						
					</tr>
</tbody>
</table>  



</td>
</tr>


</tbody>

</table>


</div>
<table>
<c:set var="isTrue" value="false" scope="session"/>
 
<c:if test="${not empty param.popup && partnerType == 'PP'}" >
<tr>
	<td align="left">&nbsp;&nbsp;&nbsp;
   		<input align="left" type="button" class="cssbutton"  onclick="return userauth('this');" 
        	value="<fmt:message key="button.save"/>" style="width:70px; height:25px"/>        
        <s:reset type="button" key="Reset" />  
    </td>
</tr>      
</c:if>
</table>


    	 
</s:form>
