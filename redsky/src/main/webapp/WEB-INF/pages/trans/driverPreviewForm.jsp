<%@ include file="/common/taglibs.jsp"%> 
<head>
	 
	<title>Driver Preview</title>
    <style type="text/css">
    #overlayMarket {
	filter:alpha(opacity=70);
	-moz-opacity:0.7;
	-khtml-opacity: 0.7;
	opacity: 0.7;
	position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
	background:url(images/over-load.png);
	}
    </style> 	
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.10.1.min.js"></script> 
<script type="text/javascript">
var flagValue = 0;
var val = 0;
var lastId ='';
function displayResult(regNum,orderNumber,controlFlag,position,id)
{
	//alert("regNum "+regNum)
	if(flagValue==0){
		var agent=document.forms['DriverForm'].elements['agent'].value;
		var ownerCode=document.forms['DriverForm'].elements['ownerCode'].value;
		//alert("agent "+agent)
		var weekEnding=document.forms['DriverForm'].elements['weekEnding'].value;	
		//alert("weekEnding "+weekEnding)
		var table=document.getElementById("driverPreviewDetailsId");
		var rownum = document.getElementById(id);
		lastId = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val = newrow+1;
		var row=table.insertRow(val);
		row.setAttribute("id",val);		
		//alert('subrat')	
		$.get("driverRegNum.html?ajax=1&decorator=simple&popup=true", 
					{vanRegNum: regNum, agent: agent,weekEnding: weekEnding,controlFlag:controlFlag,orderNumber:orderNumber,ownerCode:ownerCode},
					function(data){						
						$("#"+regNum).html(data);				
				});
		     row.innerHTML = "<td colspan=\"7\"><div id="+regNum+"></div></td>";
		     flagValue = 1;
		     return 0;
	}
	if(flagValue==1){
		document.getElementById(val).parentNode.removeChild(document.getElementById(val));
		flagValue = 0;
		var agent=document.forms['DriverForm'].elements['agent'].value;
		var weekEnding=document.forms['DriverForm'].elements['weekEnding'].value;	
		var ownerCode=document.forms['DriverForm'].elements['ownerCode'].value;
		var table=document.getElementById("driverPreviewDetailsId");
		var rownum = document.getElementById(id);
		if(lastId!=rownum.id){
		lastId = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val = newrow+1;
		var row=table.insertRow(val);
		row.setAttribute("id",val);		
		$.get("driverRegNum.html?ajax=1&decorator=simple&popup=true", 
				{vanRegNum: regNum, agent: agent,weekEnding: weekEnding,controlFlag:controlFlag,orderNumber:orderNumber,ownerCode:ownerCode},
				function(data){						
					$("#"+regNum).html(data);			
			});		
		     row.innerHTML = "<td colspan=\"7\"><div id="+regNum+"></div></td>";
		     flagValue = 1;
		}
	     return 1;
	}
}
function driverList(chargeCode,variance,target){
	var actionValue=target.value;
	//alert(actionValue)
	//alert(variance)
	if(actionValue=='Dispute'){
	document.getElementById('amount'+chargeCode).value=0;
	}else if(actionValue=='Approved'){
	document.getElementById('amount'+chargeCode).value=variance;
	}else{
	document.getElementById('amount'+chargeCode).value="";
	}
}
	function accDetailsList(chargeCodeId,sid,chargeCode,vendorCode,estimateVendorName,glType,target){
		var actionValue=target.value;
		var accIdDetails="";
		var checkDetails=document.forms['DriverForm'].elements['venderDetailsList'].value;
		var amount= document.getElementById('amount'+chargeCodeId).value
		if(actionValue=='Approved' && amount!=''){		
	
		//alert(amount)
		var accValueDetails=chargeCode+"@"+sid+"@"+amount+"@"+vendorCode+"@"+estimateVendorName+"@"+glType+"@"+"Approved";
		//alert("3"+accValueDetails)
		accIdDetails=chargeCodeId+"~"+accValueDetails;
		//alert(accIdDetails)
		}else{
		accIdDetails=chargeCodeId+"~"+"B";
		}
		if(checkDetails==''){
			document.forms['DriverForm'].elements['venderDetailsList'].value=accIdDetails;	
		}else{
			document.forms['DriverForm'].elements['venderDetailsList'].value=checkDetails+"`"+accIdDetails;	
		}
		//alert(accIdDetails)
		//var action=document.getElementById('action'+id).value;
	}
	function amountOnchange(chargeCodeId,sid,chargeCode,vendorCode,estimateVendorName,glType,target){
		var action= document.getElementById('action'+chargeCodeId).value;
		var accIdDetails="";
		var amountDetails=target.value;
		var checkDetails=document.forms['DriverForm'].elements['venderDetailsList'].value;
		if(action=='Approved' && amountDetails!='' && amountDetails!='0'){
			//var amountDetails=target.value;
			
			//alert(amountDetails)
			var accValueDetails=chargeCode+"@"+sid+"@"+amountDetails+"@"+vendorCode+"@"+estimateVendorName+"@"+glType+"@"+"Approved";
			//alert(accValueDetails)
			 accIdDetails=chargeCodeId+"~"+accValueDetails;
			//alert(accIdDetails)
		}else if(action=='Dispute' && amountDetails!='' && amountDetails!='0'){
			//var checkDetails=document.forms['DriverForm'].elements['venderDetailsList'].value;
			//var amountDetails=target.value;
			
			//alert(amountDetails)
			var accValueDetails=chargeCode+"@"+sid+"@"+amountDetails+"@"+vendorCode+"@"+estimateVendorName+"@"+glType+"@"+"Dispute";
			//alert(accValueDetails)
			 accIdDetails=chargeCodeId+"~"+accValueDetails;
			//alert(accIdDetails)
		}else{
			accIdDetails=chargeCodeId+"~"+"B";
		}
		//alert(accIdDetails)
		if(checkDetails==''){
			document.forms['DriverForm'].elements['venderDetailsList'].value=accIdDetails;	
		}else{
			document.forms['DriverForm'].elements['venderDetailsList'].value=checkDetails+"`"+accIdDetails;	
		}
	}
	function processAccountLines(){
		document.getElementById("overlayMarket").style.display = "block";
		var checkDetails=document.forms['DriverForm'].elements['venderDetailsList'].value;
		var agent=document.forms['DriverForm'].elements['agent'].value;
		var weekEnding=document.forms['DriverForm'].elements['weekEnding'].value;
		var ownerCode=document.forms['DriverForm'].elements['ownerCode'].value;
		if(checkDetails==',')
			checkDetails='';
		$.post("updateAccLineDriver.html?ajax=1&decorator=simple&popup=true", 
				{agent: agent,weekEnding: weekEnding,driverDetailsPay:checkDetails,ownerCode:ownerCode},
				function(data){
					 self.location=self.location;
					  document.forms['DriverForm'].elements['venderDetailsList'].value='';
					  document.getElementById("overlayMarket").style.display = "none";
					 		
					//$("#"+regNum).html(data);			
			});
		// var url = "updateAccLineDriver.html?weekEnding="+weekEnding+"&driverDetailsPay="+checkDetails;
		// document.forms['DriverForm'].action = url;
		 //document.forms['DriverForm'].submit(); 
	}

	function onlyFloatDriver(e){
		var t;
		var text=e.value;
		 var pattern = /^-?[0-9]+(.[0-9]{1,4})?$/; 
	         if (text.match(pattern)==null) 
	         {
			alert('Enter valid Number');
			document.getElementById(e.id).value=0;document.getElementById(e.id).select();
			   return false
	         } 
		    else
		    {
		    	return true
		    }
		}
</script>
</head>
<s:form id="DriverForm"  method="post" >

<c:set var="newAccountline" value="N" />
<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
<c:set var="newAccountline" value="Y" />
</sec-auth:authComponent>
<s:hidden name="agent" value="${agent}" ></s:hidden>
<s:hidden name="weekEnding" value="<%=request.getParameter("weekEnding") %>"/>
<c:set var="weekEnding" value="<%=request.getParameter("weekEnding") %>"/>
<s:hidden name="ownerCode" value="<%=request.getParameter("ownerCode") %>"/>
<c:set var="ownerCode" value="<%=request.getParameter("ownerCode") %>"/>
<s:hidden name="venderDetailsList" />
	<div id="newmnav">
		  <ul>
		      <li id="newmnav1" style="background:#FFF "><a class="current"><span>Driver Settlement<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		      <li><a href="reconciliationDriver.html"><span>Driver Reconciliation</span></a></li>
		  </ul>
</div><div class="spn" style="!margin-bottom:0px;">&nbsp;</div>
	<display:table name="driverPreviewDetails" class="table" id="driverPreviewDetailsId">
	<display:column>
	<img id="${driverPreviewDetailsId_rowNum}" onclick ="displayResult('${driverPreviewDetailsId.registration}','${driverPreviewDetailsId.orderNumber}','C',this,this.id);" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	</display:column>
	<c:choose>
    <c:when test="${newAccountline=='Y'}">
    <display:column  title="Order #">
	<a href="javascript:;" onclick="window.open('pricingList.html?sid=${driverPreviewDetailsId.soId}')" ><c:out value="${driverPreviewDetailsId.orderNumber}" /></a> 
	</display:column>
    </c:when>
    <c:otherwise>
	<display:column  title="Order #">
	<a href="javascript:;" onclick="window.open('accountLineList.html?sid=${driverPreviewDetailsId.soId}')" ><c:out value="${driverPreviewDetailsId.orderNumber}" /></a> 
	</display:column>
	</c:otherwise>
	</c:choose>
	<display:column property="registration" title="Registration"></display:column>
	<display:column property="statementDate" format="{0,date,dd-MMM-yyyy}" title="Statement Date"></display:column>
	<display:column property="shipperName" title="Shipper"></display:column>
	<display:column property="totalPaid" title="Total Paid" style="text-align: right;" > </display:column>
	<display:column property="total" title="Total" style="text-align: right;" > </display:column>	
	</display:table>
<c:if test="${not empty driverPreviewDetails  }">
	<input type="button" name="processAcc" value ="Process Accounting" onclick="return processAccountLines()"  class = "cssbutton1" style="width: 145px;"/>
</c:if>
<c:if test="${ empty driverPreviewDetails  }">
	<input type="button" name="processAcc" value ="Process Accounting" disabled="disabled" class = "cssbutton1" style="width: 145px;"/>
</c:if>
      		<div id="overlayMarket" style="display:none">
			<div id="layerLoading">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
			       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
			           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
			       </td>
			       </tr>
			       <tr>
			      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
			       <img src="<c:url value='/images/ajax-loader-hr.gif'/>" />       
			       </td>
			       </tr>
			     </table>
			   </td>
			  </tr>
			</table>  
			   </div>   
			   </div>
</s:form>