<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
   <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<head>   
    <title><fmt:message key="menuItemList.title"/></title>   
    <meta name="heading" content="<fmt:message key='menuItemList.heading'/>"/> 

<style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}
</style>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>
<script language="javascript" type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Menu Item?");
	var did = targetElement;
	if (agree){
		location.href="menuItemDelete.html?id="+did;
	}else{
		return false;
	}
}		
function clear_fields(){
		document.forms['form2'].elements['userCorpIDs'].value  = ""; 
		document.forms['form2'].elements['menuItem.parentName'].value  = "";
		document.forms['form2'].elements['menuItem.name'].value  = "";
		document.forms['form2'].elements['menuItem.title'].value  = "";
}


function goToSearchMenuItem(){
		document.forms['form2'].action = 'searchMenuItem.html';
}			
			
</script>
</head>

<s:form id="form2" action="searchMenuItem.html" method="post" validate="true">
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" style="width:100%"  >
<thead>
<tr>
<th><fmt:message key="menuItem.corpID"/></th>
<th><fmt:message key="menuItem.parentName"/></th>
<th><fmt:message key="menuItem.name"/></th>
<th colspan="2"><fmt:message key="menuItem.title"/></th>

</tr></thead>	
		<tbody>
		<tr>
			<td  align="left">
			 <s:select  name="userCorpIDs" list="%{corpList}" cssClass="list-menu" cssStyle="width:100px" headerKey="" headerValue="" />
			</td>
			<td  align="left">
			<s:select name="menuItem.parentName" list="%{parentList}" cssClass="list-menu" cssStyle="width:150px" headerKey="" headerValue="" />
			</td>
			<td  align="left">
			    <s:textfield name="menuItem.name" required="true" cssClass="input-text" size="20" />
			</td>
			<td  width="50" align="left">
			    <s:textfield name="menuItem.title" required="true" cssClass="input-text" size="20"/>
			</td>
			<td  align="left" style="border-left: hidden;">
			 
			</td>
		</tr>
		<tr>
			<td colspan="3"></td>
			<td align="left" style="border-left: hidden;border-right: none;" width="150">
			   <s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px;" align="top" method="search" key="button.search" onclick="return goToSearchMenuItem();"/>   
    		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
			</td>
		</tr>
		
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

<div id="Layer1" style="width:100%">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>

<tr>
  <td align="left">
  <ul id="newmnav">
  <li class="current"><a><span>Menu Item List</span></a></li>
  </ul>
</td></tr>
</tbody></table>
<s:set name="menuItems" value="menuItems" scope="request"/>   
<display:table name="menuItems" class="table" requestURI="" id="menuItemList" export="false" defaultsort="1" pagesize="10" style="width:100%;">
<display:column property="id" sortable="true" titleKey="menuItem.id" href="editMenuItem.html" paramId="id" paramProperty="id"/>
<display:column property="corpID" sortable="true" titleKey="menuItem.corpID" />
<display:column property="parentName" sortable="true" titleKey="menuItem.parentName" />  
<display:column property="name" sortable="true" titleKey="menuItem.name" />
<display:column property="title" sortable="true" titleKey="menuItem.title" />
<display:column property="url" sortable="true" titleKey="menuItem.url" />
<display:column title="Page&nbsp;Security" >
<c:set var="trimText" value="${fn:trim(menuItemList.parentName)}"/>
	<c:if test="${trimText != ''}">
   <input type="button" class="cssbutton" onclick="javascript:openWindow('pageUrlList.html?menuUrl=${menuItemList.url}&menuId=${menuItemList.id}&menuName=${menuItemList.name}&parentN=${menuItemList.parentName}&corp=${menuItemList.corpID}&decorator=popup&popup=true');" value="Page URLs"/>		
	</c:if>
</display:column>
<display:column title="Remove" style="text-align: center" ><a><img align="middle" onclick="return confirmSubmit(${menuItemList.id});" alt="Delete Menu Item" title="Delete Menu Item" style="margin: 0px 0px 0px 1px;" src="images/recyle-trans.gif"/></a></display:column>	 
</display:table>   
</div> 
<c:out value="${button1}" escapeXml="false" />  
<input type="button" class="cssbutton" style="width:60px; height:25px" onclick="location.href='<c:url value="/editMenuItem.html"/>'"  value="<fmt:message key="button.add"/>"/>  
</s:form>   