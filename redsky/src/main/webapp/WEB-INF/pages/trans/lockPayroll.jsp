<%@ include file="/common/taglibs.jsp"%> 

<head>
<meta name="heading" content="Lock Payroll"/> 
<title>Lock Payroll</title> 

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<!-- Modification closed here -->

<script language="javascript" type="text/javascript">

function checkDatOfWeek(targetElement){
	
		dt = (targetElement.value).split('-');
		var month = dt[1];
		if(month == "Jan")month=1;
		if(month == "Feb")month=2;
		if(month == "Mar")month=3;
		if(month == "Apr")month=4;
		if(month == "May")month=5;
		if(month == "Jun")month=6;
		if(month == "Jul")month=7;
		if(month == "Aug")month=8;
		if(month == "Sep")month=9;
		if(month == "Oct")month=10;
		if(month == "Nov")month=11;
		if(month == "Dec")month=12;
		today=new Date('20'+dt[2],month-1,dt[0]);
		var myDays=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
		thisDay=today.getDay();
		thisDay=myDays[thisDay];
		if(targetElement.name == 'refMaster.stopDate'){
			if(document.forms['lockPayroll'].elements['refMaster.stopDate'].value != ''){
				document.forms['lockPayroll'].elements['beginDateDay'].value = thisDay;
				document.forms['lockPayroll'].elements['lockPayrollDaysClick'].value = '';
			}			
		}
		
}
function forDays(){
 document.forms['lockPayroll'].elements['lockPayrollDaysClick'].value = '1';
}

function changeStatus(){
    document.forms['lockPayroll'].elements['formStatus'].value = '1';
}

function checkDayOfWeek(){
	if(document.forms['lockPayroll'].elements['refMaster.code'].value == ''){
		alert("Select a warehouse to continue....");
		return false;
	}
	if(document.forms['lockPayroll'].elements['refMaster.stopDate'].value == ''){
		alert("Select Date to continue....");
		return false;
	}/* else{
	if(document.forms['lockPayroll'].elements['beginDateDay'].value != 'Saturday'){
		alert("Date should be Saturday for processing Lock");
		return false;
	} 
	}*/
}


</script>

</head>

<s:form name="lockPayroll" action="updatelockPayroll">
<s:hidden name="refMaster.billCrew"/>
<div id="Layer1" style="width:95%;">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="newmnav" >
		  <ul>
		    <li id="newmnav1" ><a  class="current"><span>Payroll Lock</span></a></li>
		   	<li><a href="lockPayroll.html"><span>Payroll Lock List</span></a></li>
  		</ul>
</div>
<div class="spn spnSF">&nbsp;</div>

<s:hidden id="lockPayrollDaysClick" name="lockPayrollDaysClick" />
<s:hidden name="formStatus" />

<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" ><span></span></div>
   <div class="center-content">
<table cellpadding="2" cellspacing="0" border="0">

<tr>
	<td height="5px"></td>
</tr>

<tr>
		
				<td align="right" class="listwhitetext"  width="">Code</td>
				<td align="left" style="width:250px"><s:textfield name="refMaster.code" cssStyle="width:105px"  cssClass="input-textUpper" readonly="true"/></td>
				
				
				<td align="right" class="listwhitetext"  width="150">Description</td>
				<td align="left" style="width:250px"><s:textfield name="refMaster.description" cssStyle="width:205px"  cssClass="input-textUpper" readonly="true"/></td>
				
</tr>
<tr>
				
				<td align="right" class="listwhitetext"  width="">Length</td>
				<td align="left" style="width:250px"><s:textfield name="refMaster.fieldLength" cssStyle="width:105px;text-align:right"  cssClass="input-text" /></td>
				
				<td align="right" class="listwhitetext"  width="">IMF xref</td>
				<td align="left" style="width:250px"><s:textfield name="refMaster.bucket2" cssStyle="width:105px"  cssClass="input-text"/></td>
				
				
</tr>
<tr>
	
				<td align="right" class="listwhitetext"  width="">Bucket</td>
				<td align="left" style="width:250px"><s:textfield name="refMaster.bucket" cssStyle="width:105px"  cssClass="input-text" /></td>
				
				<td align="right" class="listwhitetext"  width="">Number</td>
				<td align="left" style="width:250px"><s:textfield name="refMaster.number" cssStyle="width:105px;text-align:right"  cssClass="input-text" /></td>
</tr>

<tr>
				<td align="right" class="listwhitetext"  width="">Address</td>
				<td align="left" style="width:250px" colspan="3"><s:textfield name="refMaster.address1" cssStyle="width:332px"  cssClass="input-text" /></td>
</tr>
<tr>
				<td align="right" class="listwhitetext"  width=""></td>
				<td align="left" style="width:250px" colspan="3"><s:textfield name="refMaster.address2" cssStyle="width:332px"  cssClass="input-text" /></td>
</tr>
<tr>
				<td align="right" class="listwhitetext"  width="">City</td>
				<td align="left" style="width:250px"><s:textfield name="refMaster.city" cssStyle="width:105px"  cssClass="input-text" /></td>
				
				<td align="right" class="listwhitetext"  width="">State</td>
				<td align="left" style="width:250px"><s:select cssClass="list-menu" name="refMaster.state" list="%{state}" headerKey="" headerValue="" cssStyle="width:206px"/></td>
				
</tr>

<tr>
				
				
				<td align="right" class="listwhitetext"  width="">Zip</td>
				<td align="left" style="width:250px"><s:textfield name="refMaster.zip" cssStyle="width:105px"  cssClass="input-text" /></td>

				<td align="right" class="listwhitetext"  width="">Branch</td>
				<td align="left" style="width:250px"><s:textfield name="refMaster.branch" cssStyle="width:205px" id="refMasterBranch" onselect="checkDatOfWeek(document.forms['lockPayroll'].elements['refMaster.stopDate']);" cssClass="input-text" /></td>
</tr>

<tr>
				<td align="right" class="listwhitetext">Lock&nbsp;Date<font color="red">*</font></td>
				<td align="left" colspan="3">
				<c:if test="${not empty refMaster.stopDate}">
				<s:text id="refMasterStopDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="refMaster.stopDate"/></s:text>
				<s:textfield cssClass="input-text" id="beginDt" value="%{refMasterStopDateFormattedValue}" name="refMaster.stopDate" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"  />
				<img id="beginDt_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/ onclick="forDays();" />
				</c:if>
				<c:if test="${empty refMaster.stopDate}">
				<s:textfield cssClass="input-text" id="beginDt" name="refMaster.stopDate" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/>
				<img id="beginDt_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/ onclick="forDays();" />
				</c:if>
				&nbsp;&nbsp;&nbsp;<input type="text" name="beginDateDay" id="beginDateDay" style="width:102px;border:0;text-align:left;font-weight:400;-webkit-user-select:none;-webkit-border-color:none;-moz-user-input:none; "/> </td>
</tr>
<tr>
	<td height="15px"></td>
</tr>

</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>


<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td></td>
	<td align="left" style="padding-left:4px;" colspan="2"><s:submit cssClass="cssbutton" cssStyle="width:58px; height:25px" key="button.save" onclick="return checkDayOfWeek();"/>
	<input type="button" class="cssbutton1" value="Cancel" size="20" style="width:55px; height:25px" onclick="location.href='<c:url value="/lockPayroll.html"/>'" /></td>
</tr>
</table>

<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="refMasterCreatedOnFormattedValue" value="${refMaster.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="refMaster.createdOn" value="${refMasterCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${refMaster.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							
								<s:hidden name="refMaster.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{refMaster.createdBy}"/></td>
						
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							
							<fmt:formatDate var="refMasterupdatedOnFormattedValue" value="${refMaster.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="refMaster.updatedOn" value="${refMasterupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${refMaster.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							
							
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							
								<s:hidden name="refMaster.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{refMaster.updatedBy}"/></td>
							
						</tr>
					</tbody>
				</table></tr>
</div>
</s:form>
<script>
try{
checkDatOfWeek(document.forms['lockPayroll'].elements['refMaster.stopDate']);
}
catch(e){}
</script>

<script type="text/javascript">
	setOnSelectBasedMethods(["checkDatOfWeek(document.getElementById('beginDt'))"]);
	setCalendarFunctionality();
</script>
