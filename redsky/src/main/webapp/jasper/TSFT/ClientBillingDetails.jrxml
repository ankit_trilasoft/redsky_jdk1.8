<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ClientBillingDetails" printOrder="Horizontal" pageWidth="1322" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="1262" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" whenResourceMissingType="Empty" uuid="e98871c1-eb6d-4b8a-baf4-77fed2797440">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="RedSky Customer" class="java.lang.String"/>
	<parameter name="Begin Date" class="java.util.Date"/>
	<parameter name="End Date" class="java.util.Date"/>
	<queryString>
		<![CDATA[SELECT "ClientBillingDetails-R-TSFT.jrxml",
    CUST.NAME AS 'Customer',
    PRJ.NAME AS 'Project',
    ENTRY.ENTRY_DATE AS 'Date',
    USR.LAST_NAME AS 'Name',
    replace(replace(replace(replace(replace(replace(replace(ENTRY.COMMENT,'\r\n',''),'\r\n',''),'\r\n',''),'\r\n',''),'\r\n',''),'\r\n',''),'\r\n','') AS Ticket,
    replace(replace(replace(replace(replace(replace(replace(replace(ENTRY.COMMENT,'\r\n',''),'\r\n',''),'\r\n',''),'\r\n',''),'\r\n',''),'\r\n',''),'\r\n',''),'\t','') AS 'comment',
    SUM(ENTRY.HOURS) AS 'Hours',
    if(PRJ.BILLABLE='Y',USR.HOURLY_RATE,0.0) AS 'Rate',
    SUM(ENTRY.HOURS * if(PRJ.BILLABLE='Y',USR.HOURLY_RATE,0.0)) AS 'Amount'
FROM
     ehour.TIMESHEET_ENTRY ENTRY,
     ehour.CUSTOMER CUST,
     ehour.PROJECT PRJ,
     ehour.PROJECT_ASSIGNMENT PAG,
     ehour.USERS USR
WHERE
     ENTRY.ASSIGNMENT_ID = PAG.ASSIGNMENT_ID
        AND PAG.PROJECT_ID = PRJ.PROJECT_ID
        AND PRJ.CUSTOMER_ID = CUST.CUSTOMER_ID
        AND PAG.USER_ID = USR.USER_ID
	AND ENTRY.ignorebilling is not true
        AND date(ENTRY.ENTRY_DATE) >=$P{Begin Date}
        AND date(ENTRY.ENTRY_DATE) <=$P{End Date}
	AND CUST.NAME like $P{RedSky Customer}
GROUP BY PAG.ASSIGNMENT_ID , ENTRY.ENTRY_DATE , CUST.CUSTOMER_ID , CUST.NAME ,
CUST.CODE , PAG.PROJECT_ID , PAG.USER_ID ,
USR.FIRST_NAME , USR.LAST_NAME , PAG.ROLE , PRJ.PROJECT_ID , PRJ.NAME , ENTRY.COMMENT order by 2,3;]]>
	</queryString>
	<field name="ClientBillingDetails-R-TSFT.jrxml" class="java.lang.String"/>
	<field name="Customer" class="java.lang.String"/>
	<field name="Project" class="java.lang.String"/>
	<field name="Date" class="java.sql.Date"/>
	<field name="Name" class="java.lang.String"/>
	<field name="Ticket" class="java.lang.String"/>
	<field name="comment" class="java.lang.String"/>
	<field name="Hours" class="java.math.BigDecimal"/>
	<field name="Rate" class="java.math.BigDecimal"/>
	<field name="Amount" class="java.math.BigDecimal"/>
	<variable name="ProjectHours" class="java.math.BigDecimal" resetType="Group" resetGroup="Project" calculation="Sum">
		<variableExpression><![CDATA[$F{Hours}]]></variableExpression>
	</variable>
	<variable name="ProjectRate" class="java.math.BigDecimal" resetType="Group" resetGroup="Project" calculation="Sum">
		<variableExpression><![CDATA[$F{Rate}]]></variableExpression>
	</variable>
	<variable name="ProjectAmount" class="java.math.BigDecimal" resetType="Group" resetGroup="Project" calculation="Sum">
		<variableExpression><![CDATA[$F{Amount}]]></variableExpression>
	</variable>
	<variable name="CustomerHours" class="java.math.BigDecimal" resetType="Group" resetGroup="Customer" calculation="Sum">
		<variableExpression><![CDATA[$F{Hours}]]></variableExpression>
	</variable>
	<variable name="CustomerRate" class="java.math.BigDecimal" resetType="Group" resetGroup="Customer" calculation="Sum">
		<variableExpression><![CDATA[$F{Hours}]]></variableExpression>
	</variable>
	<variable name="CustomerAmount" class="java.math.BigDecimal" resetType="Group" resetGroup="Customer" calculation="Sum">
		<variableExpression><![CDATA[$F{Amount}]]></variableExpression>
	</variable>
	<group name="Customer" isStartNewPage="true">
		<groupExpression><![CDATA[$F{Customer}]]></groupExpression>
		<groupHeader>
			<band height="122" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" stretchType="RelativeToTallestObject" x="169" y="3" width="1093" height="30" isRemoveLineWhenBlank="true" isPrintWhenDetailOverflows="true" forecolor="#000066" uuid="e66b5874-0e04-42e2-9a64-e75417cb57fc"/>
					<box leftPadding="700"/>
					<textElement textAlignment="Left">
						<font fontName="Arial" size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA["Customer: "+$F{Customer}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="textField-3" x="1" y="101" width="168" height="18" uuid="6dc35872-dfec-4447-a187-128800b6f4ab"/>
					<box leftPadding="2"/>
					<textElement markup="none">
						<font fontName="Arial" size="8"/>
					</textElement>
					<text><![CDATA[Project]]></text>
				</staticText>
				<staticText>
					<reportElement key="textField-4" x="169" y="101" width="100" height="18" uuid="5dabe0a6-c7fd-4f35-aee9-5edd07780f59"/>
					<box leftPadding="2"/>
					<textElement markup="none">
						<font fontName="Arial" size="8"/>
					</textElement>
					<text><![CDATA[Date]]></text>
				</staticText>
				<staticText>
					<reportElement key="textField-5" x="269" y="101" width="98" height="18" uuid="102972c7-0340-482e-a45b-1cba3dfd15ef"/>
					<box leftPadding="2"/>
					<textElement markup="none">
						<font fontName="Arial" size="8"/>
					</textElement>
					<text><![CDATA[Name]]></text>
				</staticText>
				<staticText>
					<reportElement key="textField-6" x="396" y="101" width="659" height="18" uuid="ed788f82-0186-4400-9103-dc5bbb308419"/>
					<box leftPadding="2"/>
					<textElement markup="none">
						<font fontName="Arial" size="8"/>
					</textElement>
					<text><![CDATA[Comment]]></text>
				</staticText>
				<staticText>
					<reportElement key="textField-7" x="1055" y="101" width="50" height="18" uuid="c2f7e9dc-f62c-423f-9687-5a51aa1bd509"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" markup="none">
						<font fontName="Arial" size="8"/>
					</textElement>
					<text><![CDATA[Hours]]></text>
				</staticText>
				<staticText>
					<reportElement key="textField-8" x="1105" y="101" width="80" height="18" uuid="7c3fd637-1b4a-445a-9fc3-c06526123b6b"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" markup="none">
						<font fontName="Arial" size="8"/>
					</textElement>
					<text><![CDATA[Rate]]></text>
				</staticText>
				<staticText>
					<reportElement key="textField-9" x="1185" y="101" width="77" height="18" uuid="e596afc2-5293-4ac9-bab0-52ee386a34f4"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right" markup="none">
						<font fontName="Arial" size="8"/>
					</textElement>
					<text><![CDATA[Amount]]></text>
				</staticText>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-15" x="169" y="33" width="1093" height="25" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" uuid="417e21db-ee34-4fb9-9626-be5fce760bac"/>
					<box leftPadding="700"/>
					<textElement textAlignment="Left" markup="none">
						<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA["RedSky Client Billing Reports"]]></textFieldExpression>
				</textField>
				<image scaleImage="RetainShape" onErrorType="Blank">
					<reportElement key="image-1" mode="Opaque" x="0" y="3" width="169" height="55" uuid="18f65e76-986a-417b-be8f-9af6f39958a5"/>
					<imageExpression><![CDATA["../../images/highres_logo.png"]]></imageExpression>
				</image>
				<line>
					<reportElement key="line-1" x="0" y="120" width="1262" height="1" uuid="1c1937de-224a-4fd7-a98f-5cca1ffa00fd"/>
					<graphicElement>
						<pen lineWidth="1.0" lineStyle="Solid"/>
					</graphicElement>
				</line>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-16" x="169" y="58" width="1093" height="25" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" uuid="c3bf9748-23ad-450e-9de2-f5bfc798c555"/>
					<box leftPadding="700"/>
					<textElement textAlignment="Left" markup="none">
						<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA["From: "+(new java.text.SimpleDateFormat("dd-MMM-yyyy")).format($P{Begin Date})+" TO: "+(new java.text.SimpleDateFormat("dd-MMM-yyyy")).format($P{End Date})]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="textField-18" x="367" y="101" width="29" height="18" uuid="56093c4e-5324-4235-b505-3c6174a8bf36"/>
					<box leftPadding="2"/>
					<textElement markup="none">
						<font fontName="Arial" size="8"/>
					</textElement>
					<text><![CDATA[Ticket]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="23" splitType="Stretch">
				<textField pattern="##0.00" isBlankWhenNull="false">
					<reportElement key="textField-11" x="1055" y="2" width="50" height="18" forecolor="#000066" uuid="ddd0031e-f740-4b3f-b209-f0019aeae9b6"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{CustomerHours}]]></textFieldExpression>
				</textField>
				<textField pattern="$#,##0.00" isBlankWhenNull="false">
					<reportElement key="textField-13" x="1185" y="2" width="77" height="18" forecolor="#000066" uuid="35e5552f-726c-4f6f-9e43-1b1dabb6bdee"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{CustomerAmount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-14" x="1" y="2" width="1054" height="18" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" forecolor="#000066" uuid="ee4f1d4a-0bc2-42a5-a317-444493bb549a"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Left" markup="none">
						<font fontName="Arial" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Customer}+" Grand Total"]]></textFieldExpression>
				</textField>
				<line>
					<reportElement key="line-2" x="0" y="21" width="1262" height="1" uuid="c8a9f0d8-0c3d-4270-a637-bb2b3b142d99"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<group name="Project">
		<groupExpression><![CDATA[$F{Project}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="24" splitType="Stretch">
				<textField pattern="##0.00" isBlankWhenNull="false">
					<reportElement key="textField" x="1055" y="4" width="50" height="18" forecolor="#000066" uuid="f33185e6-f3e2-4b02-accb-f30fdc1acc2b"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{ProjectHours}]]></textFieldExpression>
				</textField>
				<textField pattern="$#,##0.00" isBlankWhenNull="false">
					<reportElement key="textField" x="1185" y="4" width="77" height="18" forecolor="#000066" uuid="74fe66ff-ac06-4189-9bdf-d0f014689b6f"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{ProjectAmount}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-10" x="1" y="4" width="1054" height="18" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" forecolor="#000066" uuid="03fb27fa-075c-4633-b527-e74ea4a47a0c"/>
					<box rightPadding="2"/>
					<textElement textAlignment="Left" markup="none">
						<font fontName="Arial" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Project}+" Sub Total"]]></textFieldExpression>
				</textField>
				<line>
					<reportElement key="line-3" x="1055" y="2" width="207" height="1" uuid="74acfd8e-c55a-4215-8c4d-96cff24e7a59"/>
					<graphicElement>
						<pen lineWidth="0.5" lineStyle="Solid"/>
					</graphicElement>
				</line>
				<line>
					<reportElement key="line-4" x="1055" y="22" width="207" height="1" uuid="4163b059-c47b-4412-9e76-d0141ca392b4"/>
					<graphicElement>
						<pen lineWidth="0.5" lineStyle="Solid"/>
					</graphicElement>
				</line>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="32" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField" stretchType="RelativeToTallestObject" isPrintRepeatedValues="false" x="1" y="0" width="168" height="32" isRemoveLineWhenBlank="true" isPrintWhenDetailOverflows="true" uuid="b1cbd735-1a1f-44aa-aa2b-fe77a8ccf22d"/>
				<box leftPadding="2"/>
				<textElement>
					<font fontName="Arial" size="8" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Project}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" stretchType="RelativeToTallestObject" x="269" y="0" width="98" height="32" isRemoveLineWhenBlank="true" isPrintWhenDetailOverflows="true" uuid="5d809cb6-5e78-41bd-9744-2e18b6549b77"/>
				<box leftPadding="2"/>
				<textElement>
					<font fontName="Arial" size="8" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Name}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" stretchType="RelativeToTallestObject" x="396" y="0" width="659" height="32" isPrintWhenDetailOverflows="true" uuid="49075175-d467-473e-9238-3b4647e0f6df"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="8" isBold="false" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{comment}.replaceAll("\n", "").replaceAll("\t", "")]]></textFieldExpression>
			</textField>
			<textField pattern="##0.00" isBlankWhenNull="true">
				<reportElement key="textField" stretchType="RelativeToTallestObject" x="1055" y="0" width="50" height="32" isRemoveLineWhenBlank="true" isPrintWhenDetailOverflows="true" uuid="c75932ad-a7d1-4e74-b3e1-0869e6925acd"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Hours}]]></textFieldExpression>
			</textField>
			<textField pattern="$#,##0.0" isBlankWhenNull="true">
				<reportElement key="textField" stretchType="RelativeToTallestObject" x="1105" y="0" width="80" height="32" isRemoveLineWhenBlank="true" isPrintWhenDetailOverflows="true" uuid="010254f2-47e8-4ccb-a2b9-f9264e6479bf"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Rate}]]></textFieldExpression>
			</textField>
			<textField pattern="$#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" stretchType="RelativeToTallestObject" x="1185" y="0" width="77" height="32" isRemoveLineWhenBlank="true" isPrintWhenDetailOverflows="true" uuid="ffc699f8-d3db-4fb6-b202-92e6bc9bdbf3"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Amount}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yyyy" isBlankWhenNull="true">
				<reportElement key="textField" x="169" y="0" width="100" height="32" uuid="9cf9be12-bcf1-43ab-afb9-ee7b0376f7a3"/>
				<box leftPadding="2"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Date}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-17" stretchType="RelativeToTallestObject" x="367" y="0" width="29" height="32" isRemoveLineWhenBlank="true" isPrintWhenDetailOverflows="true" uuid="49067c10-c088-4482-b4ce-05ae5031cc89"/>
				<box leftPadding="2"/>
				<textElement>
					<font fontName="Arial" size="8" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{comment}.contains("# ") ? $F{Ticket}.replaceAll("[^0-9# ]","").replace(" # ","\n ").replace("# "," ") : ""]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
