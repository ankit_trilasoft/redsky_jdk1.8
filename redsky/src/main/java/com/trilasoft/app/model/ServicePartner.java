/**
 * Implementation of <strong>ModelSupport</strong> that contains convenience methods.
 * This class represents the basic model on "ServicePartner" object in Redsky that allows for Job management.
 * @Class Name	ServicePartner
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */



package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
//import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="servicepartner")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class ServicePartner extends BaseObject implements ToDoRuleRecord, ListLinkData {   
	private String corpID;
	
	private Long id;
	private Long serviceOrderId;
	private String sequenceNumber;
	private String ship;
	private String shipNumber;
	private String carrierNumber;
	private String carrierCode;
	private String carrierName;
	private String carrierVessels;
	private Date etDepart;
	private Date atDepart;
	private Date etArrival;
	private Date houseBillIssued;
	private Date atArrival;
	private String carrierArrival;
	private String carrierDeparture;
	private String eTATA;
	private String eTDTA;
	private String carrierPhone;
	private String carrierClicd;
	private String omni;
	private String bookNumber;
	private Integer miles;
	private BigDecimal pdCost1;
	private BigDecimal pdCost2;
	private short pdFreedays;
	private short pdDays2;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String city;
	private String email;
	private String coord;
	private String partnerType;
	private Boolean dropPick;
	private Boolean liveLoad;
	private Boolean transhipped;
	private String blNumber;
	private String poeCode;
	private String polCode; 
	private String cntnrNumber;
	private String actgCode;
	private String aesNumber; 
	private String skids;
	private Boolean clone;
	private ServiceOrder serviceOrder;
	private Long servicePartnerId;
	private String ugwIntId;
	private String houseBillNumber;
	private String caed;
	private boolean status =true;
	private String subcarrierName;
	private String subcarrierCode;
	private String subcarrierBooking;
	private String cost;
	private Date bookingdate;
	private Date revisedETD;
	private Date revisedETA;
	private String hblNumber;
	


	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("serviceOrderId", serviceOrderId)
				.append("sequenceNumber", sequenceNumber).append("ship", ship)
				.append("shipNumber", shipNumber)
				.append("carrierNumber", carrierNumber)
				.append("carrierCode", carrierCode)
				.append("carrierName", carrierName)
				.append("carrierVessels", carrierVessels)
				.append("etDepart", etDepart).append("atDepart", atDepart)
				.append("etArrival", etArrival)
				.append("houseBillIssued", houseBillIssued)
				.append("atArrival", atArrival)
				.append("carrierArrival", carrierArrival)
				.append("carrierDeparture", carrierDeparture)
				.append("eTATA", eTATA).append("eTDTA", eTDTA)
				.append("carrierPhone", carrierPhone)
				.append("carrierClicd", carrierClicd).append("omni", omni)
				.append("bookNumber", bookNumber).append("miles", miles)
				.append("pdCost1", pdCost1).append("pdCost2", pdCost2)
				.append("pdFreedays", pdFreedays).append("pdDays2", pdDays2)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("city", city).append("email", email)
				.append("coord", coord).append("partnerType", partnerType)
				.append("dropPick", dropPick).append("liveLoad", liveLoad)
				.append("transhipped", transhipped)
				.append("blNumber", blNumber).append("poeCode", poeCode)
				.append("polCode", polCode).append("cntnrNumber", cntnrNumber)
				.append("actgCode", actgCode).append("aesNumber", aesNumber)
				.append("skids", skids).append("clone", clone) 
				.append("servicePartnerId", servicePartnerId)
				.append("ugwIntId", ugwIntId)
				.append("houseBillNumber", houseBillNumber)
				.append("caed", caed)
				.append("status", status)
				.append("subcarrierName", subcarrierName)
				.append("subcarrierCode", subcarrierCode)
				.append("subcarrierBooking", subcarrierBooking)
				.append("cost", cost)
				.append("bookingdate", bookingdate)
				.append("revisedETD", revisedETD)
				.append("revisedETA", revisedETA)
				.append("hblNumber", hblNumber)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ServicePartner))
			return false;
		ServicePartner castOther = (ServicePartner) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(ship, castOther.ship)
				.append(shipNumber, castOther.shipNumber)
				.append(carrierNumber, castOther.carrierNumber)
				.append(carrierCode, castOther.carrierCode)
				.append(carrierName, castOther.carrierName)
				.append(carrierVessels, castOther.carrierVessels)
				.append(etDepart, castOther.etDepart)
				.append(atDepart, castOther.atDepart)
				.append(etArrival, castOther.etArrival)
				.append(houseBillIssued, castOther.houseBillIssued)
				.append(atArrival, castOther.atArrival)
				.append(carrierArrival, castOther.carrierArrival)
				.append(carrierDeparture, castOther.carrierDeparture)
				.append(eTATA, castOther.eTATA).append(eTDTA, castOther.eTDTA)
				.append(carrierPhone, castOther.carrierPhone)
				.append(carrierClicd, castOther.carrierClicd)
				.append(omni, castOther.omni)
				.append(bookNumber, castOther.bookNumber)
				.append(miles, castOther.miles)
				.append(pdCost1, castOther.pdCost1)
				.append(pdCost2, castOther.pdCost2)
				.append(pdFreedays, castOther.pdFreedays)
				.append(pdDays2, castOther.pdDays2)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(city, castOther.city).append(email, castOther.email)
				.append(coord, castOther.coord)
				.append(partnerType, castOther.partnerType)
				.append(dropPick, castOther.dropPick)
				.append(liveLoad, castOther.liveLoad)
				.append(transhipped, castOther.transhipped)
				.append(blNumber, castOther.blNumber)
				.append(poeCode, castOther.poeCode)
				.append(polCode, castOther.polCode)
				.append(cntnrNumber, castOther.cntnrNumber)
				.append(actgCode, castOther.actgCode)
				.append(aesNumber, castOther.aesNumber)
				.append(skids, castOther.skids).append(clone, castOther.clone) 
				.append(servicePartnerId, castOther.servicePartnerId)
				.append(ugwIntId, castOther.ugwIntId)
				.append(houseBillNumber, castOther.houseBillNumber)
				.append(caed, castOther.caed)
				.append(status, castOther.status)
				.append(subcarrierName, castOther.subcarrierName)
				.append(subcarrierCode, castOther.subcarrierCode)
				.append(subcarrierBooking, castOther.subcarrierBooking)
				.append(cost, castOther.cost)
				.append(bookingdate, castOther. bookingdate)
				.append(revisedETD, castOther. revisedETD)
				.append(revisedETA, castOther. revisedETA)
				.append(hblNumber, castOther. hblNumber)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(serviceOrderId).append(sequenceNumber).append(ship)
				.append(shipNumber).append(carrierNumber).append(carrierCode)
				.append(carrierName).append(carrierVessels).append(etDepart)
				.append(atDepart).append(etArrival).append(houseBillIssued)
				.append(atArrival).append(carrierArrival)
				.append(carrierDeparture).append(eTATA).append(eTDTA)
				.append(carrierPhone).append(carrierClicd).append(omni)
				.append(bookNumber).append(miles).append(pdCost1)
				.append(pdCost2).append(pdFreedays).append(pdDays2)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(city).append(email).append(coord)
				.append(partnerType).append(dropPick).append(liveLoad)
				.append(transhipped).append(blNumber).append(poeCode)
				.append(polCode).append(cntnrNumber).append(actgCode)
				.append(aesNumber).append(skids).append(clone)
				.append(servicePartnerId).append(ugwIntId)
				.append(houseBillNumber).append(caed).append(status)
				.append(subcarrierName)
				.append(subcarrierCode)
				.append(subcarrierBooking)
				.append(cost)
				.append(bookingdate)
				.append(revisedETD)
				.append(revisedETA)
				.append(hblNumber)
				.toHashCode();
	}

	@ManyToOne
	@JoinColumn(name="serviceOrderId", nullable=false, updatable=false,
			referencedColumnName="id")
			public ServiceOrder getServiceOrder() {
			        return serviceOrder;
			    }
			    
				 public void setServiceOrder(ServiceOrder serviceOrder) {
						this.serviceOrder = serviceOrder;
				}
	
	
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=30)
	public String getBookNumber() {
		return bookNumber;
	}
	public void setBookNumber(String bookNumber) {
		this.bookNumber = bookNumber;
	}
	
	@Column(length=3)
	public String getCarrierClicd() {
		return carrierClicd;
	}
	public void setCarrierClicd(String carrierClicd) {
		this.carrierClicd = carrierClicd;
	}
	@Column(length=8)
	public String getCarrierCode() {
		return carrierCode;
	}
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}
	
	@Column(length=65)
	public String getCarrierName() {
		return carrierName;
	}
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}
	@Column(length=2)
	public String getCarrierNumber() {
		return carrierNumber;
	}
	public void setCarrierNumber(String carrierNumber) {
		this.carrierNumber = carrierNumber;
	}
	@Column(length=14)
	public String getCarrierPhone() {
		return carrierPhone;
	}
	public void setCarrierPhone(String carrierPhone) {
		this.carrierPhone = carrierPhone;
	}
	
	
	@Column(length=20)
	public String getCarrierVessels() {
		return carrierVessels;
	}
	
	public void setCarrierVessels(String carrierVessels) {
		this.carrierVessels = carrierVessels;
	}
	@Column(length=8)
	public String getETATA() {
		return eTATA;
	}
	public void setETATA(String etata) {
		eTATA = etata;
	}
	@Column(length=8)
	public String getETDTA() {
		return eTDTA;
	}
	public void setETDTA(String etdta) {
		eTDTA = etdta;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length=5)
	public Integer getMiles() {
		return miles;
	}

	public void setMiles(Integer miles) {
		this.miles = miles;
	}

	@Column(length=8)
	public String getOmni() {
		return omni;
	}
	public void setOmni(String omni) {
		this.omni = omni;
	}
	@Column(length=20)
	public BigDecimal getPdCost1() {
		return pdCost1;
	}
	public void setPdCost1(BigDecimal pdCost1) {
		this.pdCost1 = pdCost1;
	}
	@Column(length=20)
	public BigDecimal getPdCost2() {
		return pdCost2;
	}
	public void setPdCost2(BigDecimal pdCost2) {
		this.pdCost2 = pdCost2;
	}
	@Column(length=5)
	public short getPdDays2() {
		return pdDays2;
	}
	public void setPdDays2(short pdDays2) {
		this.pdDays2 = pdDays2;
	}
	@Column(length=5)
	public short getPdFreedays() {
		return pdFreedays;
	}
	public void setPdFreedays(short pdFreedays) {
		this.pdFreedays = pdFreedays;
	}
	@Column(length=15)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column(length=2)
	public String getShip() {
		return ship;
	}
	public void setShip(String ship) {
		this.ship = ship;
	}
	@Column(length=15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	
	
	@Column(length=200)
	public String getCarrierArrival() {
		return carrierArrival;
	}

	public void setCarrierArrival(String carrierArrival) {
		this.carrierArrival = carrierArrival;
	}
	@Column(length=200)
	public String getCarrierDeparture() {
		return carrierDeparture;
	}

	public void setCarrierDeparture(String carrierDeparture) {
		this.carrierDeparture = carrierDeparture;
	}

	@Column
    public Date getAtArrival() {
		return atArrival;
	}

	public void setAtArrival(Date atArrival) {
		this.atArrival = atArrival;
	}
	@Column
	public Date getAtDepart() {
		return atDepart;
	}

	public void setAtDepart(Date atDepart) {
		this.atDepart = atDepart;
	}
	@Column
	public Date getEtArrival() {
		return etArrival;
	}

	public void setEtArrival(Date etArrival) {
		this.etArrival = etArrival;
	}
	@Column
	public Date getEtDepart() {
		return etDepart;
	}

	public void setEtDepart(Date etDepart) {
		this.etDepart = etDepart;
	}

	@Column(length=50)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	@Column(length=35)
	public String getCoord() {
		return coord;
	}

	public void setCoord(String coord) {
		this.coord = coord;
	}
	@Column(length=65)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	@Column(length=10)
	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	@Column
	public Boolean getDropPick() {
		return dropPick;
	}

	public void setDropPick(Boolean dropPick) {
		this.dropPick = dropPick;
	}
    @Column
	public Boolean getLiveLoad() {
		return liveLoad;
	}

	public void setLiveLoad(Boolean liveLoad) {
		this.liveLoad = liveLoad;
	}
	@Column
	public Boolean getTranshipped() {
		return transhipped;
	}

	public void setTranshipped(Boolean transhipped) {
		this.transhipped = transhipped;
	}
	@Column(length=30)
	public String getBlNumber() {
		return blNumber;
	}

	public void setBlNumber(String blNumber) {
		this.blNumber = blNumber;
	}
	@Column(length=200)
	public String getPoeCode() {
		return poeCode;
	}

	public void setPoeCode(String poeCode) {
		this.poeCode = poeCode;
	}
	@Column(length=200)
	public String getPolCode() {
		return polCode;
	}

	public void setPolCode(String polCode) {
		this.polCode = polCode;
	}
	@Column(length=30)
	public String getHblNumber() {
		return hblNumber;
	}

	public void setHblNumber(String hblNumber) {
		this.hblNumber = hblNumber;
	}
// For look up Return values
	
    @Transient
    public String getListCode() {        
    	  return carrierCode;
    }

    @Transient
    public String getListDescription() {
    	return this.carrierName;
    } 

	@Transient
	public String getListSecondDescription() {
		return this.partnerType;
	}
	
	@Transient
	public String getListThirdDescription() {
		return null;
	}
	
	@Transient
	public String getListFourthDescription() {
		return null;
	}
	
	@Transient
	public String getListFifthDescription() {
		return null;
	}
	
	@Transient
	public String getListSixthDescription() {
		return null;
	}

	@Transient
	public String getListSeventhDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Column( length=20 )
	public String getActgCode() {
		return actgCode;
	}

	public void setActgCode(String actgCode) {
		this.actgCode = actgCode;
	}

	/**
	 * @return the serviceOrderId
	 */
	@Column(length=20, insertable=false, updatable=false)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}

	/**
	 * @param serviceOrderId the serviceOrderId to set
	 */
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}

	/**
	 * @return the houseBillIssued
	 */
	@Column
	public Date getHouseBillIssued() {
		return houseBillIssued;
	}

	/**
	 * @param houseBillIssued the houseBillIssued to set
	 */
	public void setHouseBillIssued(Date houseBillIssued) {
		this.houseBillIssued = houseBillIssued;
	}

	/**
	 * @return the cntnrNumber
	 */
	@Column(length=50)
	public String getCntnrNumber() {
		return cntnrNumber;
	}

	/**
	 * @param cntnrNumber the cntnrNumber to set
	 */
	public void setCntnrNumber(String cntnrNumber) {
		this.cntnrNumber = cntnrNumber;
	}

	/**
	 * @return the aesNumber
	 */
	@Column(length=30)
	public String getAesNumber() {
		return aesNumber;
	}

	/**
	 * @param aesNumber the aesNumber to set
	 */
	public void setAesNumber(String aesNumber) {
		this.aesNumber = aesNumber;
	}

	public String getSkids() {
		return skids;
	}

	public void setSkids(String skids) {
		this.skids = skids;
	}

	public Boolean getClone() {
		return clone;
	}

	public void setClone(Boolean clone) {
		this.clone = clone;
	}
	@Transient
	public String getListEigthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListNinthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListTenthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Column(length=8)
	public Long getServicePartnerId() {
		return servicePartnerId;
	}

	public void setServicePartnerId(Long servicePartnerId) {
		this.servicePartnerId = servicePartnerId;
	}

	@Column
	public String getUgwIntId() {
		return ugwIntId;
	}

	public void setUgwIntId(String ugwIntId) {
		this.ugwIntId = ugwIntId;
	}

	public String getHouseBillNumber() {
		return houseBillNumber;
	}

	public void setHouseBillNumber(String houseBillNumber) {
		this.houseBillNumber = houseBillNumber;
	}
	@Column
	public String getCaed() {
		return caed;
	}

	public void setCaed(String caed) {
		this.caed = caed;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	@Column
	public String getSubcarrierName() {
		return subcarrierName;
	}

	public void setSubcarrierName(String subcarrierName) {
		this.subcarrierName = subcarrierName;
	}
	@Column
	public String getSubcarrierCode() {
		return subcarrierCode;
	}

	public void setSubcarrierCode(String subcarrierCode) {
		this.subcarrierCode = subcarrierCode;
	}
	@Column
	public String getSubcarrierBooking() {
		return subcarrierBooking;
	}

	public void setSubcarrierBooking(String subcarrierBooking) {
		this.subcarrierBooking = subcarrierBooking;
	}
	@Column
	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public Date getBookingdate() {
		return bookingdate;
	}

	public void setBookingdate(Date bookingdate) {
		this.bookingdate = bookingdate;
	}
	@Column
	public Date getRevisedETD() {
		return revisedETD;
	}

	public void setRevisedETD(Date revisedETD) {
		this.revisedETD = revisedETD;
	}
	@Column
	public Date getRevisedETA() {
		return revisedETA;
	}

	public void setRevisedETA(Date revisedETA) {
		this.revisedETA = revisedETA;
	}

	


}
