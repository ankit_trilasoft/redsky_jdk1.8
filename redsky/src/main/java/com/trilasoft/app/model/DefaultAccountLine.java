package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;


@Entity
@Table(name="defaultaccountline")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class DefaultAccountLine extends BaseObject
{
	private String corpID;
	private Long id;
	private String route;
	private String jobType;
	private String mode;
	private String categories;
	private String chargeBack;
	private String basis;
	private Double quantity=0.0;
	private Double rate=0.0;
	private Double amount=0.0;
	private BigDecimal sellRate= new BigDecimal("0.0");
	private BigDecimal estimatedRevenue = new BigDecimal("0.0");
	private Integer markUp= new Integer(0);
	private String serviceType;
	private String vendorCode;
	private String vendorName;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String contract;
	private String chargeCode;
	private String recGl;
	private String payGl;
	private String billToCode;
	private String billToName;
	private String description;
	private String commodity;
	private String packMode;
	private int orderNumber=0;
	private String companyDivision;
	private String estVatPercent;
    private String estVatDescr;
    private BigDecimal estVatAmt= new BigDecimal(0);
    private String originCountry;
    private String destinationCountry;
    private String equipment;
    private String estExpVatDescr;
    private String estExpVatPercent;
    private Boolean uploaddataflag=false;
    private String originCity;
    private String destinationCity;
    
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("route", route).append("jobType", jobType).append(
				"mode", mode).append("categories", categories).append(
				"chargeBack", chargeBack).append("basis", basis).append(
				"quantity", quantity).append("rate", rate).append("amount",
				amount).append("sellRate", sellRate).append("estimatedRevenue",
				estimatedRevenue).append("markUp", markUp).append(
				"serviceType", serviceType).append("vendorCode", vendorCode)
				.append("vendorName", vendorName)
				.append("createdBy", createdBy).append("createdOn", createdOn)
				.append("updatedBy", updatedBy).append("updatedOn", updatedOn)
				.append("contract", contract).append("chargeCode", chargeCode)
				.append("recGl", recGl).append("payGl", payGl).append(
						"billToCode", billToCode).append("billToName",
						billToName).append("description", description).append(
						"commodity", commodity).append("packMode", packMode)
				.append("orderNumber", orderNumber)
				.append("companyDivision", companyDivision)
				.append("estVatPercent", estVatPercent)
				.append("estVatDescr", estVatDescr)
				.append("estVatAmt", estVatAmt)
				.append("originCountry",originCountry)
				.append("destinationCountry",destinationCountry)
				.append("equipment",equipment).append("estExpVatDescr",estExpVatDescr)
				.append("estExpVatPercent",estExpVatPercent)
				.append("uploaddataflag",uploaddataflag)
				.append("originCity",originCity)
				.append("destinationCity",destinationCity)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DefaultAccountLine))
			return false;
		DefaultAccountLine castOther = (DefaultAccountLine) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(route, castOther.route).append(jobType,
				castOther.jobType).append(mode, castOther.mode).append(
				categories, castOther.categories).append(chargeBack,
				castOther.chargeBack).append(basis, castOther.basis).append(
				quantity, castOther.quantity).append(rate, castOther.rate)
				.append(amount, castOther.amount).append(sellRate,
						castOther.sellRate).append(estimatedRevenue,
						castOther.estimatedRevenue).append(markUp,
						castOther.markUp).append(serviceType,
						castOther.serviceType).append(vendorCode,
						castOther.vendorCode).append(vendorName,
						castOther.vendorName).append(createdBy,
						castOther.createdBy).append(createdOn,
						castOther.createdOn).append(updatedBy,
						castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(contract,
						castOther.contract).append(chargeCode,
						castOther.chargeCode).append(recGl, castOther.recGl)
				.append(payGl, castOther.payGl).append(billToCode,
						castOther.billToCode).append(billToName,
						castOther.billToName).append(description,
						castOther.description).append(commodity,
						castOther.commodity).append(packMode,
						castOther.packMode).append(orderNumber,
						castOther.orderNumber).append(companyDivision,
						castOther.companyDivision)
						.append(estVatPercent, castOther.estVatPercent)
						.append(estVatDescr, castOther.estVatDescr)
						.append("estVatAmt", castOther.estVatAmt)
						.append("originCountry",castOther.originCountry)
						.append("destinationCountry",castOther.destinationCountry)
						.append("equipment",castOther.equipment)
						.append("estExpVatDescr",castOther.estExpVatDescr)
						.append("estExpVatPercent",castOther.estExpVatPercent)
						.append("uploaddataflag",castOther.uploaddataflag)
						.append("originCity",castOther.originCity)
						.append("destinationCity",castOther.destinationCity)
						.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(route)
				.append(jobType).append(mode).append(categories).append(
						chargeBack).append(basis).append(quantity).append(rate)
				.append(amount).append(sellRate).append(estimatedRevenue)
				.append(markUp).append(serviceType).append(vendorCode).append(
						vendorName).append(createdBy).append(createdOn).append(
						updatedBy).append(updatedOn).append(contract).append(
						chargeCode).append(recGl).append(payGl).append(
						billToCode).append(billToName).append(description)
				.append(commodity).append(packMode).append(orderNumber)
				.append(companyDivision)
				.append(estVatPercent)
				.append(estVatDescr).append(estVatAmt)
				.append(originCountry).append(destinationCountry).append(equipment)
				.append(estExpVatDescr).append(estExpVatPercent)
				.append(uploaddataflag).append(originCity).append(destinationCity)
				.toHashCode();
	}
	@Column(length=50)
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	@Column(length=50)
	public String getCategories() {
		return categories;
	}
	public void setCategories(String categories) {
		this.categories = categories;
	}
	@Column(length=50)
	public String getChargeBack() {
		return chargeBack;
	}
	public void setChargeBack(String chargeBack) {
		this.chargeBack = chargeBack;
	}
	@Column(length=50)
	public String getCorpID() {
		return corpID;
	}
	
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length=50)
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=50)
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	@Column(length=50)
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	@Column(length=50)
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column(length=50)
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=50)
	public String getServiceType() {
		return serviceType;
	}
	
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	@Column
	public Double getAmount() {
		return amount;
	}
	@Column
	public Double getQuantity() {
		return quantity;
	}
	@Column
	public Double getRate() {
		return rate;
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	@Column(length=255)
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	@Column(length=25)
	public String getChargeCode() {
		return chargeCode;
	}
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}
	@Column(length=100)
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	@Column(length=20)
	public String getPayGl() {
		return payGl;
	}
	public void setPayGl(String payGl) {
		this.payGl = payGl;
	}
	@Column(length=20)
	public String getRecGl() {
		return recGl;
	}
	public void setRecGl(String recGl) {
		this.recGl = recGl;
	}
	@Column( length=8 )
	public String getBillToCode() {
		return billToCode;
	}
	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	@Column
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	@Column(length=5000)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(length = 5)
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	@Column(length = 15)
	public String getPackMode() {
		return packMode;
	}
	public void setPackMode(String packMode) {
		this.packMode = packMode;
	}
	@Column(length=5)
	public Integer getMarkUp() {
		return markUp;
	}
	public void setMarkUp(Integer markUp) {
		this.markUp = markUp;
	}
	@Column(length=25)
	public BigDecimal getSellRate() {
		return sellRate;
	}
	public void setSellRate(BigDecimal sellRate) {
		this.sellRate = sellRate;
	}
	@Column(length=25)
	public BigDecimal getEstimatedRevenue() {
		return estimatedRevenue;
	}
	public void setEstimatedRevenue(BigDecimal estimatedRevenue) {
		this.estimatedRevenue = estimatedRevenue;
	}
	
	@Column(length=10)
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	public String getEstVatPercent() {
		return estVatPercent;
	}
	public void setEstVatPercent(String estVatPercent) {
		this.estVatPercent = estVatPercent;
	}
	public String getEstVatDescr() {
		return estVatDescr;
	}
	public void setEstVatDescr(String estVatDescr) {
		this.estVatDescr = estVatDescr;
	}
	public BigDecimal getEstVatAmt() {
		return estVatAmt;
	}
	public void setEstVatAmt(BigDecimal estVatAmt) {
		this.estVatAmt = estVatAmt;
	}
	
	@Column(length=3)
	public String getOriginCountry() {
		return originCountry;
	}
	
	@Column(length=3)
	public String getDestinationCountry() {
		return destinationCountry;
	}
	
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	@Column
	public String getEquipment() {
		return equipment;
	}
	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}
	@Column
	public String getEstExpVatDescr() {
		return estExpVatDescr;
	}
	public void setEstExpVatDescr(String estExpVatDescr) {
		this.estExpVatDescr = estExpVatDescr;
	}
	@Column
	public String getEstExpVatPercent() {
		return estExpVatPercent;
	}
	public void setEstExpVatPercent(String estExpVatPercent) {
		this.estExpVatPercent = estExpVatPercent;
	}
	public Boolean getUploaddataflag() {
		return uploaddataflag;
	}
	public void setUploaddataflag(Boolean uploaddataflag) {
		this.uploaddataflag = uploaddataflag;
	}
	public int getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
		
}
