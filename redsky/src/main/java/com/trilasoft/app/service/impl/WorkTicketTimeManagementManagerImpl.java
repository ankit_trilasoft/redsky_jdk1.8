package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.NotesDao;
import com.trilasoft.app.dao.WorkTicketTimeManagementDao;
import com.trilasoft.app.model.WorkTicketTimeManagement;
import com.trilasoft.app.service.WorkTicketTimeManagementManager;

public class WorkTicketTimeManagementManagerImpl extends GenericManagerImpl<WorkTicketTimeManagement, Long> implements WorkTicketTimeManagementManager {
	WorkTicketTimeManagementDao workTicketTimeManagementDao;
	public WorkTicketTimeManagementManagerImpl(WorkTicketTimeManagementDao workTicketTimeManagementDao) {
		super(workTicketTimeManagementDao);
		this.workTicketTimeManagementDao = workTicketTimeManagementDao;
	}
	public List getTimeManagementManagerDetails(Long Id, Long ticket, String sessionCorpID){
		return workTicketTimeManagementDao.getTimeManagementManagerDetails(Id,ticket,sessionCorpID);
	}
}
