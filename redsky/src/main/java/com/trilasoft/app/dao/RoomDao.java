/**
 * 
 */
package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Room;

/**
 * @author GVerma
 *
 */
public interface RoomDao extends GenericDao<Room, Long> {

	public List getAllRoom(String sessionCorpID, String shipno);
	public List checkRoom(String corpID, int roomCode, String shipNumber);
}
