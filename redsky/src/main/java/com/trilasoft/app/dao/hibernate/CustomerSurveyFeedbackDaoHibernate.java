package com.trilasoft.app.dao.hibernate;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CustomerSurveyFeedbackDao;
import com.trilasoft.app.model.CustomerSurveyFeedback;

public class CustomerSurveyFeedbackDaoHibernate extends GenericDaoHibernate<CustomerSurveyFeedback, Long> implements CustomerSurveyFeedbackDao {
   public CustomerSurveyFeedbackDaoHibernate(){
	   super(CustomerSurveyFeedback.class);
   }
	
}
