<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %> 
<script type="text/javascript">

$(document).ready(function() 
	    { 		
        
	  $('#userContactList').tablesorter();
   	 
	    } 
	); 
</script>
<display:table name="userContactList" class="table tablesorter" requestURI="" id="userContactList" defaultsort="2" pagesize="10" style="width:100%;" >   
	    <display:column sortable="true" title="Full Name" style="width:65px"> 
	    	<a href="javascript: void(0)" onclick= "fillContactValue('${userContactList.fullName}','${userContactList.email}');">
	    	 <c:out value="${userContactList.fullName}" /></a>
	    </display:column>
        <display:column property="jobFunction" sortable="true" title="Job Function" style="width:65px"/>  
        <display:column title="Relocation Contact" sortable="true" style="width:45px">      
 		<c:if test="${userContactList.relocationContact==false}">				
			<img src="${pageContext.request.contextPath}/images/cancel001.gif" />				 	
		</c:if>
		<c:if test="${userContactList.relocationContact==true}">				
			 <img src="${pageContext.request.contextPath}/images/tick01.gif" />		 	
		</c:if>
		</display:column>	
        <display:column property="userTitle" sortable="true" title="Job Title" style="width:65px"/>
	    <display:column sortable="true" title="Email Address" style="width:65px"> 
	    	<a href="javascript: void(0)" onclick= "sendEmail('${userContactList.email}');">
	    	 <c:out value="${userContactList.email}" /></a>
	    </display:column>
	    <display:column property="NPSscore" sortable="true" title="NPS score" style="width:65px"/> 
	    <display:column sortable="true" title="Phone&nbsp;Number" style="width:65px"> 
	    	  	 <c:out value="${userContactList.phoneNumber}" /></a>
	    </display:column>
</display:table>