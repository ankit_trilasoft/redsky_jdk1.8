package com.trilasoft.app.webapp.action;

import java.util.*;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DsSchoolEducationalCounseling;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DsSchoolEducationalCounselingManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
public class DsSchoolEducationalCounselingAction extends BaseAction implements Preparable {
	

	private Long id;
	private String sessionCorpID;
	private DsSchoolEducationalCounseling dsSchoolEducationalCounseling;
	private DsSchoolEducationalCounselingManager dsSchoolEducationalCounselingManager;
	private Long serviceOrderId;
	private CustomerFile customerFile;
	private CustomerFileManager customerFileManager;
	private Miscellaneous  miscellaneous;
	private MiscellaneousManager miscellaneousManager ;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private String countForDSSchoolEducationalCounselingNotes;	
	private NotesManager notesManager;
	private String gotoPageString;
	private String validateFormNav;
	
	
	
	public DsSchoolEducationalCounselingAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
	
public void prepare() throws Exception { 
		
	}
	
private String shipSize;
private String minShip;
private String countShip;
private Map<String, String> relocationServices;
private RefMasterManager refMasterManager;

public String edit() { 
	if (id != null) {
		trackingStatus=trackingStatusManager.get(id);
		serviceOrder=serviceOrderManager.get(id);
    	customerFile = serviceOrder.getCustomerFile();
    	miscellaneous = miscellaneousManager.get(id); 
    	dsSchoolEducationalCounseling = dsSchoolEducationalCounselingManager.get(id);
	} else {
		dsSchoolEducationalCounseling = new DsSchoolEducationalCounseling(); 
		dsSchoolEducationalCounseling.setCreatedOn(new Date());
		dsSchoolEducationalCounseling.setUpdatedOn(new Date());
		dsSchoolEducationalCounseling.setServiceOrderId(serviceOrderId);
	} 
	shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
	minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
	countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();		
	relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
	
	getNotesForIconChange();
	return SUCCESS;
}
	


public String save() throws Exception {
	
	boolean isNew = (dsSchoolEducationalCounseling.getId() == null);  
			if (isNew) { 
				dsSchoolEducationalCounseling.setCreatedOn(new Date());
	        }
			trackingStatus=trackingStatusManager.get(id);
			serviceOrder=serviceOrderManager.get(id);
	    	customerFile = serviceOrder.getCustomerFile();
	    	miscellaneous = miscellaneousManager.get(id); 
	    	dsSchoolEducationalCounseling.setCorpID(sessionCorpID);
	    	dsSchoolEducationalCounseling.setUpdatedOn(new Date());
	    	dsSchoolEducationalCounseling.setUpdatedBy(getRequest().getRemoteUser()); 
	    	dsSchoolEducationalCounselingManager.save(dsSchoolEducationalCounseling);
	    	if(!(validateFormNav=="OK"))
	    	{
	    	
		    String key = (isNew) ? "dsSchoolEducationalCounseling.added" : "dsSchoolEducationalCounseling.updated";
	    
		    saveMessage(getText(key));
	    	}
		    getNotesForIconChange();
			return SUCCESS;
			
		
}
@SkipValidation
public String saveOnTabChange() throws Exception {
	validateFormNav = "OK";
	String s = save();
	return SUCCESS;
}

@SkipValidation
public String getNotesForIconChange() {
	List noteDS = notesManager.countForDSSchoolEducationalCounselingNotes(serviceOrder.getShipNumber());
	
	if (noteDS.isEmpty()) {
		countForDSSchoolEducationalCounselingNotes = "0";
	} else {
		countForDSSchoolEducationalCounselingNotes = ((noteDS).get(0)).toString();
	}
		
	return SUCCESS;
			
}

/**
 * @return the countForDSSchoolEducationalCounselingNotes
 */
public String getCountForDSSchoolEducationalCounselingNotes() {
	return countForDSSchoolEducationalCounselingNotes;
}

/**
 * @param countForDSSchoolEducationalCounselingNotes the countForDSSchoolEducationalCounselingNotes to set
 */
public void setCountForDSSchoolEducationalCounselingNotes(
		String countForDSSchoolEducationalCounselingNotes) {
	this.countForDSSchoolEducationalCounselingNotes = countForDSSchoolEducationalCounselingNotes;
}

/**
 * @return the customerFile
 */
public CustomerFile getCustomerFile() {
	return customerFile;
}

/**
 * @param customerFile the customerFile to set
 */
public void setCustomerFile(CustomerFile customerFile) {
	this.customerFile = customerFile;
}

/**
 * @return the dsSchoolEducationalCounseling
 */
public DsSchoolEducationalCounseling getDsSchoolEducationalCounseling() {
	return dsSchoolEducationalCounseling;
}

/**
 * @param dsSchoolEducationalCounseling the dsSchoolEducationalCounseling to set
 */
public void setDsSchoolEducationalCounseling(
		DsSchoolEducationalCounseling dsSchoolEducationalCounseling) {
	this.dsSchoolEducationalCounseling = dsSchoolEducationalCounseling;
}

/**
 * @return the id
 */
public Long getId() {
	return id;
}

/**
 * @param id the id to set
 */
public void setId(Long id) {
	this.id = id;
}

/**
 * @return the miscellaneous
 */
public Miscellaneous getMiscellaneous() {
	return miscellaneous;
}

/**
 * @param miscellaneous the miscellaneous to set
 */
public void setMiscellaneous(Miscellaneous miscellaneous) {
	this.miscellaneous = miscellaneous;
}

/**
 * @return the serviceOrder
 */
public ServiceOrder getServiceOrder() {
	return serviceOrder;
}

/**
 * @param serviceOrder the serviceOrder to set
 */
public void setServiceOrder(ServiceOrder serviceOrder) {
	this.serviceOrder = serviceOrder;
}

/**
 * @return the serviceOrderId
 */
public Long getServiceOrderId() {
	return serviceOrderId;
}

/**
 * @param serviceOrderId the serviceOrderId to set
 */
public void setServiceOrderId(Long serviceOrderId) {
	this.serviceOrderId = serviceOrderId;
}

/**
 * @return the sessionCorpID
 */
public String getSessionCorpID() {
	return sessionCorpID;
}

/**
 * @param sessionCorpID the sessionCorpID to set
 */
public void setSessionCorpID(String sessionCorpID) {
	this.sessionCorpID = sessionCorpID;
}

/**
 * @return the trackingStatus
 */
public TrackingStatus getTrackingStatus() {
	return trackingStatus;
}

/**
 * @param trackingStatus the trackingStatus to set
 */
public void setTrackingStatus(TrackingStatus trackingStatus) {
	this.trackingStatus = trackingStatus;
}

/**
 * @param customerFileManager the customerFileManager to set
 */
public void setCustomerFileManager(CustomerFileManager customerFileManager) {
	this.customerFileManager = customerFileManager;
}

/**
 * @param dsSchoolEducationalCounselingManager the dsSchoolEducationalCounselingManager to set
 */
public void setDsSchoolEducationalCounselingManager(
		DsSchoolEducationalCounselingManager dsSchoolEducationalCounselingManager) {
	this.dsSchoolEducationalCounselingManager = dsSchoolEducationalCounselingManager;
}

/**
 * @param miscellaneousManager the miscellaneousManager to set
 */
public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
	this.miscellaneousManager = miscellaneousManager;
}

/**
 * @param notesManager the notesManager to set
 */
public void setNotesManager(NotesManager notesManager) {
	this.notesManager = notesManager;
}

/**
 * @param serviceOrderManager the serviceOrderManager to set
 */
public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
	this.serviceOrderManager = serviceOrderManager;
}

/**
 * @param trackingStatusManager the trackingStatusManager to set
 */
public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
	this.trackingStatusManager = trackingStatusManager;
}

/**
 * @return the gotoPageString
 */
public String getGotoPageString() {
	return gotoPageString;
}

/**
 * @param gotoPageString the gotoPageString to set
 */
public void setGotoPageString(String gotoPageString) {
	this.gotoPageString = gotoPageString;
}

/**
 * @return the validateFormNav
 */
public String getValidateFormNav() {
	return validateFormNav;
}

/**
 * @param validateFormNav the validateFormNav to set
 */
public void setValidateFormNav(String validateFormNav) {
	this.validateFormNav = validateFormNav;
}

/**
 * @return the countShip
 */
public String getCountShip() {
	return countShip;
}

/**
 * @param countShip the countShip to set
 */
public void setCountShip(String countShip) {
	this.countShip = countShip;
}

/**
 * @return the minShip
 */
public String getMinShip() {
	return minShip;
}

/**
 * @param minShip the minShip to set
 */
public void setMinShip(String minShip) {
	this.minShip = minShip;
}

/**
 * @return the relocationServices
 */
public Map<String, String> getRelocationServices() {
	return relocationServices;
}

/**
 * @param relocationServices the relocationServices to set
 */
public void setRelocationServices(Map<String, String> relocationServices) {
	this.relocationServices = relocationServices;
}

/**
 * @return the shipSize
 */
public String getShipSize() {
	return shipSize;
}

/**
 * @param shipSize the shipSize to set
 */
public void setShipSize(String shipSize) {
	this.shipSize = shipSize;
}

/**
 * @param refMasterManager the refMasterManager to set
 */
public void setRefMasterManager(RefMasterManager refMasterManager) {
	this.refMasterManager = refMasterManager;
}
}
