delimiter $$

DROP trigger IF EXISTS `redsky`.`Trigger_after_insert_Storage` $$

CREATE  TRIGGER redsky.Trigger_after_insert_Storage
after insert ON redsky.storage
FOR EACH ROW
BEGIN

if(NEW.volUnit='Cft') then
	update location
	set utilizedVolCft= if(utilizedVolCft is null,0,utilizedVolCft)+NEW.volume, utilizedVolCbm= ((if(utilizedVolCft='',0,utilizedVolCft)+NEW.volume) * 0.0283168466)
	where locationid  = NEW.locationid and corpid=NEW.corpid;
end if;

if(NEW.volUnit='Cbm') then
	update location
	set utilizedVolCbm= if(utilizedVolCbm is null,0,utilizedVolCbm)+NEW.volume, utilizedVolCft= ((if(utilizedVolCbm='',0,utilizedVolCbm)+NEW.volume) * 35.3147)
	where locationid  = NEW.locationid and corpid=NEW.corpid;
	
	update storagelibrary
	set usedVolumeCbm= if(usedVolumeCbm is null,0,usedVolumeCbm)+NEW.volume, usedVolumeCft= ((if(usedVolumeCbm='',0,usedVolumeCbm)+NEW.volume) * 35.3147)
	, availVolumeCbm=volumeCbm-usedVolumeCbm where locationid  = NEW.locationid and storageId  = NEW.storageId and corpid=NEW.corpid;
	
end if;

end;
$$


delimiter;