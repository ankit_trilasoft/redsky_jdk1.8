package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Query;

import com.trilasoft.app.dao.RedskyBillingDao; 
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.DTO;
import com.trilasoft.app.model.RedskyBilling;

public class RedskyBillingDaoHibernate  extends GenericDaoHibernate<RedskyBilling, Long> implements RedskyBillingDao {

	public RedskyBillingDaoHibernate() {
		super(RedskyBilling.class);
		 
	}
	public class DTO{
    	private Object corpID;
    	private Object monthName;
    	private Object month;
    	private Object soCount;
		private Object shipNumber;
		private Object firstName;
		private Object lastName;
		private Object createdOn;
		private Object createdBy;
		private Object year;
		private Object originZip;
		private Object originZipCountNo;
		private Object destZipCountNo;
		private Object destinationZip;
		private Object latitude;
		private Object longitude;
		private Object redskyBillDate;
		private Object shipper;
		private Object job;
		private Object  billtoName;
		private Object  companyDivision;
		private Object routing;
		
		private Object packingA;
		private Object loadA;
		private Object deliveryA;
		private Object invoiceNumber;
		private Object invoiceDate;
		public Object getCorpID() {
			return corpID;
		}
		public void setCorpID(Object corpID) {
			this.corpID = corpID;
		}
		public Object getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(Object createdBy) {
			this.createdBy = createdBy;
		}
		public Object getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Object createdOn) {
			this.createdOn = createdOn;
		}
		public Object getDestinationZip() {
			return destinationZip;
		}
		public void setDestinationZip(Object destinationZip) {
			this.destinationZip = destinationZip;
		}
		public Object getDestZipCountNo() {
			return destZipCountNo;
		}
		public void setDestZipCountNo(Object destZipCountNo) {
			this.destZipCountNo = destZipCountNo;
		}
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getLatitude() {
			return latitude;
		}
		public void setLatitude(Object latitude) {
			this.latitude = latitude;
		}
		public Object getLongitude() {
			return longitude;
		}
		public void setLongitude(Object longitude) {
			this.longitude = longitude;
		}
		public Object getMonth() {
			return month;
		}
		public void setMonth(Object month) {
			this.month = month;
		}
		public Object getMonthName() {
			return monthName;
		}
		public void setMonthName(Object monthName) {
			this.monthName = monthName;
		}
		public Object getOriginZip() {
			return originZip;
		}
		public void setOriginZip(Object originZip) {
			this.originZip = originZip;
		}
		public Object getOriginZipCountNo() {
			return originZipCountNo;
		}
		public void setOriginZipCountNo(Object originZipCountNo) {
			this.originZipCountNo = originZipCountNo;
		}
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getSoCount() {
			return soCount;
		}
		public void setSoCount(Object soCount) {
			this.soCount = soCount;
		}
		public Object getYear() {
			return year;
		}
		public void setYear(Object year) {
			this.year = year;
		} 
		public Object getJob() {
			return job;
		}
		public void setJob(Object job) {
			this.job = job;
		} 
		public Object getShipper() {
			return shipper;
		}
		public void setShipper(Object shipper) {
			this.shipper = shipper;
		}
		public Object getCompanyDivision() {
			return companyDivision;
		}
		public void setCompanyDivision(Object companyDivision) {
			this.companyDivision = companyDivision;
		}
		public Object getRedskyBillDate() {
			return redskyBillDate;
		}
		public void setRedskyBillDate(Object redskyBillDate) {
			this.redskyBillDate = redskyBillDate;
		}
		public Object getBilltoName() {
			return billtoName;
		}
		public void setBilltoName(Object billtoName) {
			this.billtoName = billtoName;
		}
		public Object getPackingA() {
			return packingA;
		}
		public void setPackingA(Object packingA) {
			this.packingA = packingA;
		}
		public Object getLoadA() {
			return loadA;
		}
		public void setLoadA(Object loadA) {
			this.loadA = loadA;
		}
		public Object getDeliveryA() {
			return deliveryA;
		}
		public void setDeliveryA(Object deliveryA) {
			this.deliveryA = deliveryA;
		}
		public Object getRouting() {
			return routing;
		}
		public void setRouting(Object routing) {
			this.routing = routing;
		}
		public Object getInvoiceNumber() {
			return invoiceNumber;
		}
		public void setInvoiceNumber(Object invoiceNumber) {
			this.invoiceNumber = invoiceNumber;
		}
		public Object getInvoiceDate() {
			return invoiceDate;
		}
		public void setInvoiceDate(Object invoiceDate) {
			this.invoiceDate = invoiceDate;
		}

	}
	 public List  putRedskyBillingList(String rskyBillGroup) {
		 List billingDateList= new ArrayList();
			List redskyBillingList= new ArrayList();
			List returnRedskyBillingList= new ArrayList();
			List goLiveDateList= new ArrayList();
			String billingDate="";
			String goLiveDate="";
			List tempCorpId;
			tempCorpId = this.getSession().createSQLQuery("SELECT corpid FROM company where rskyBillGroup='"+rskyBillGroup+"'").list();
			Iterator it=tempCorpId.iterator();
		String tempCorpId1 ="";
			while(it.hasNext()){
				tempCorpId1 =it.next().toString();
			billingDateList =this.getSession().createSQLQuery("select DATE_FORMAT(clientBillingDate,'%Y-%m-%d') from company c where c.corpID = '"+tempCorpId1+"' and c.clientBillingDate is not null").list();
			
			//billingDateList =getHibernateTemplate().find("select DATE_FORMAT(clientBillingDate,'%Y-%m-%d') from Company c,RedskyBilling b where c.corpID='"+sessionCorpID+"' and c.clientBillingDate is not null and b.billingRate = c.defaultBillingRate");
			if(billingDateList!=null && (!(billingDateList.isEmpty()))&& billingDateList.get(0)!=null){
				billingDate=billingDateList.get(0).toString();
			}  
			goLiveDateList=getHibernateTemplate().find("select DATE_FORMAT(goLiveDate,'%Y-%m-%d') from Company where corpID in ('"+tempCorpId1+"') and goLiveDate is not null");
			if(goLiveDateList!=null && (!(goLiveDateList.isEmpty()))&& goLiveDateList.get(0)!=null){
				goLiveDate=goLiveDateList.get(0).toString();
			}  
			if((!(billingDate.equals(""))) && (!(goLiveDate.equals("")))){ 
				redskyBillingList=this.getSession().createSQLQuery("select s.id from serviceorder s ,billing b where  DATE_FORMAT(s.redskyBillDate,'%Y-%m-%d')  >= '"+billingDate+"' and s.status <> 'CNCL' and s.corpID = '"+tempCorpId1+"' and (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d') >='"+goLiveDate+"' or  b.revenueRecognition is null )and b.id=s.id ").list();	
				getHibernateTemplate().bulkUpdate("update Company set clientBillingDate = now() where corpID = '"+tempCorpId1+"'");
				String sql = "update company set clientbillingdate=now() where corpID='"+tempCorpId1+"'";
				Query query = this.getSession().createSQLQuery(sql);
			}
			returnRedskyBillingList.addAll(redskyBillingList);
			}
			return returnRedskyBillingList;
	 
	 }
	

	public List getRedskyBillingList(String corpID, int year) {		
		List list1 = new ArrayList();
		 List billingDetailList = this.getSession().createSQLQuery("select corpID,MONTH(redskyBillDate)  as aa,YEAR(redskyBillDate),MONTHNAME(redskyBillDate)  as monthname,count(distinct shipNumber) from redskybilling where corpID ='"+corpID+"'  and YEAR(redskyBillDate)='"+year+"' group BY CAST(DATE_FORMAT(redskyBillDate,'%M') as char) order by aa").list();
	
		Iterator it=billingDetailList.iterator();
		while(it.hasNext())
	       {
				Object []row= (Object [])it.next();
				DTO dTO=new DTO();
				dTO.setCorpID(row[0].toString());
				dTO.setMonth(row[1].toString());
				dTO.setYear(row[2].toString());
				dTO.setMonthName(row[3].toString());
				dTO.setSoCount(row[4]);
				
				list1.add(dTO);
	       }
		return list1;
	}

	public List getRedskyBillingListDetails(String corpID, int month, int year) {
		List list1 = new ArrayList();
		String query="select distinct redskybilling.corpID as 'CORPID', redskybilling.companydivision as 'Divn', date_format(redskybilling.redskybilldate,'%Y-%m-%d') as 'Date', redskybilling.job, redskybilling.shipNumber as 'SO#', concat( redskybilling.lastname,', ',redskybilling.firstname) as 'Shipper', redskybilling.billtoname as 'Biller',date_format(redskybilling.packingA,'%Y-%m-%d') as 'Packing',date_format(redskybilling.deliveryA,'%Y-%m-%d') as 'Delivery',date_format(redskybilling.loadA,'%Y-%m-%d') as 'Load',redskybilling.routing,accountline.recInvoiceNumber as 'Invoice_No', Date_Format(accountline.receivedInvoiceDate,'%Y-%m-%d') as 'Invoice_Date' FROM redskybilling redskybilling left outer join accountline accountline on redskybilling.shipNumber=accountline.shipNumber and accountline.corpID = '"+corpID+"' and accountline.recInvoiceNumber <>''where MONTH(redskybilling.redskybilldate) = '"+month+"' and redskybilling.corpID = '"+corpID+"' and YEAR(redskybilling.redskyBillDate) like '%"+year+"%' order by redskybilling.shipNumber, Invoice_Date, redskybilling.corpid, redskybilling.companydivision, redskybilling.job, redskybilling.redskybilldate";
		List billingList=this.getSession().createSQLQuery(query).list();
		Iterator it=billingList.iterator();
		String sameShipNumber="";
		while(it.hasNext())
	       {
				Object []row= (Object [])it.next();
				DTO dTO=new DTO();
				dTO.setCorpID(row[0].toString()); 
				if(row[1]==null){
					dTO.setCompanyDivision("");
				}else
				{
					dTO.setCompanyDivision(row[1].toString());
				} 
				if(row[2]==null){
					dTO.setRedskyBillDate("");
				}else
				{
					dTO.setRedskyBillDate(row[2].toString());
				} 
				if(row[3]==null){
					dTO.setJob("");
				}else
				{
					dTO.setJob(row[3].toString());
				}
				String newShipNumber="";
				if(row[4]==null){
					dTO.setShipNumber("");
				}else
				{
					dTO.setShipNumber(row[4].toString());
					newShipNumber=row[4].toString();
				}
				if(row[5]==null){
					dTO.setShipper("");
				}else
				{
					dTO.setShipper(row[5].toString());
				}
				if(row[6]==null){
					dTO.setBilltoName("");
				}else
				{
					dTO.setBilltoName(row[6].toString());
				}
				if(row[7]==null){
					dTO.setPackingA("");
				}else
				{
					dTO.setPackingA(row[7].toString());
				}
				if(row[8]==null){
					dTO.setDeliveryA("");
				}else
				{
					dTO.setDeliveryA(row[8].toString());
				}
				if(row[9]==null){
					dTO.setLoadA("");
				}else
				{
					dTO.setLoadA(row[9].toString());
				}
				if(row[10]==null){
					dTO.setRouting("");
				}
				
				else{
					dTO.setRouting(row[10].toString());
				}
				if(row[11]==null){
					dTO.setInvoiceNumber("");
				}
				
				else{
					dTO.setInvoiceNumber(row[11].toString());
				}
				if(row[12]==null){
					dTO.setInvoiceDate("");
				}
				
				else{
					dTO.setInvoiceDate(row[12].toString());
				}
				
				
				if(sameShipNumber!=null && newShipNumber !=null && (!(sameShipNumber.trim().equalsIgnoreCase(newShipNumber.trim())))){
				list1.add(dTO);
				}
				sameShipNumber=newShipNumber;
	       }
		return list1;
	}

	public List gettotalCountOfSo(String corpID, int year) { 
		List list1= this.getSession().createSQLQuery("select count(distinct shipNumber) from redskybilling where corpID='"+corpID+"'  and YEAR(redskyBillDate)='"+year+"' group BY corpID").list();
		return list1;
	}
	public String checkByShipNumber(String corpID,String shipNumber){
		String id="";
		List list1= this.getSession().createSQLQuery("select id from redskybilling where corpID='"+corpID+"'  and shipNumber='"+shipNumber+"'").list();
		if((list1!=null)&&(!list1.isEmpty())&&(list1.get(0)!=null)&&(!list1.get(0).toString().equalsIgnoreCase(""))){
			id=list1.get(0).toString();
		}
		return id;
	}
	public int getDetailsByProcedure(int proc,Date date){
		int i = 0;
		if(proc==2){
	       Query q = this.getSession().createSQLQuery(" { call Update_RedskyBillingDate_UGWW(?) }");
	        q.setParameter(0,date); 
	        i=q.executeUpdate();
	    	return i;
		}else if(proc==4){
			 Query q = this.getSession().createSQLQuery(" {call Update_RedskyBillingDate_WITH_DATES(?) }");
			 q.setParameter(0,date); 
			 i=q.executeUpdate();
		 	return i;
		  
		}else if(proc==3){
			 Query q = this.getSession().createSQLQuery(" { call Update_RedskyBillingDate() }");
		     i=q.executeUpdate();
		 	return i;
		}else if(proc==1){
			 Query q = this.getSession().createSQLQuery(" { call Update_RedskyBillingDate_UGJP(?) }");
			 q.setParameter(0,date); 
		     i=q.executeUpdate();
		 	return i;
		}
		return i;
		
		
	}
}
