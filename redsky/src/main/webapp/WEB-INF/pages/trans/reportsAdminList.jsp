<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head> 
    <title><fmt:message key="reportAdminList.title"/></title> 
    <meta name="heading" content="<fmt:message key='reportAdminList.heading'/>"/> 
    <script language="javascript" type="text/javascript">
		function clear_fields(){
			var i;
			for(i=0;i<=5;i++){
				document.forms['searchForm'].elements[i].value = "";
			}
		}
	</script>
<script language="javascript" type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Base?");
	var did = targetElement;
	if (agree){
		location.href="deleteReport.html?id="+did;
	}else{
		return false;
	}
}
		
</script>
<style>
 span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}

#content{margin-bottom:26px !important;}
</style>	
</head>
<c:set var="buttons">
    <!-- <input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/reportForm.html"/>'" value="<fmt:message key="button.add"/>"/> -->
    <input type="button" class="cssbutton1" value="Add" onclick="location.href='<c:url value="/reportForm.html"/>'" />
</c:set> 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom:10px;" align="top" method="searchAdmin" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
</c:set> 
<body style="background-color:#444444;">
	<%
	String ua = request.getHeader( "User-Agent" );
	boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
	boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
	response.setHeader( "Vary", "User-Agent" );
	%>
	
	<% if( isFirefox ){ %>
		<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
		<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
	<% } %>
	
	<% if( isMSIE ){ %>
		<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
		<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
	<% } %>   
<s:form id="searchForm" action="searchAdminReport" method="post" validate="true">   
<div id="otabs">
	<ul>
		<li><a class="current"><span>Search</span></a></li>
	</ul>
</div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-5px; "><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%">
	<thead>
		<tr>
			<th align="center"><fmt:message key="reports.menu"/>
			<th align="center"><fmt:message key="reports.module"/>
			<th align="center"><fmt:message key="reports.subModule"/>
			<th align="center"><fmt:message key="reports.description"/></th>
			<th align="center"><fmt:message key="reports.reportName"/>
			<th align="center"><fmt:message key="reports.formReportFlag"/>
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td width="120" align="left"><s:textfield name="reports.menu" required="true" cssClass="input-text" size="20"/></td>
			<td width="120" align="left">
			<s:select cssClass="list-menu" name="reports.module" list="%{moduleReport}" cssStyle="width:150px" headerKey="" headerValue="" />
			</td>
			<td width="120" align="left">
			<s:select cssClass="list-menu" name="reports.subModule" list="%{subModuleReport}" cssStyle="width:150px" headerKey="" headerValue="" />    
			</td>
			<td width="120" align="left"><s:textfield name="reports.description" required="true" cssClass="input-text" size="20"/></td>
			<td width="120" align="left"><s:textfield name="reports.reportName" required="true" cssClass="input-text" size="20"/></td>
			<td width="110" align="left"><s:select cssClass="list-menu" name="reports.formReportFlag" list="%{reportFlag}" headerValue="" headerKey="" cssStyle="width:110px"/></td>
			</tr>
			<tr>
			<td colspan="5"></td>
			<td width="150px" align="center" style="border-left: hidden;"><c:out value="${searchbuttons}" escapeXml="false" /></td>
		</tr>
	</tbody>
</table>
		  </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:out value="${searchresults}" escapeXml="false" />
<s:set name="reportss" value="reportss" scope="request"/> 
<div id="otabs" style="margin-top: -15px;">
	<ul>
		<li><a class="current"><span>Reports List</span></a></li>
	</ul>
</div>
<div class="spnblk">&nbsp;</div>
<display:table name="reportss" class="table" requestURI="" id="reportsList" defaultsort="2" pagesize="10" style="width:100%">
    <display:column property="formReportFlag" sortable="true" titleKey="reports.formReportFlag">
    	
	</display:column>	
    <display:column sortable="true" style="width:350px;" titleKey="reports.menu" sortProperty="menu">
	    <a href="editReport.html?id=${reportsList.id}">
	    	<c:out value="${reportsList.menu}" />
	    </a>
    </display:column>
    <display:column property="module" sortable="true" titleKey="reports.module"/>
    <display:column property="subModule" sortable="true" titleKey="reports.subModule"/>
    <display:column property="description" sortable="true" titleKey="reports.description" style="width: 125px;"/>
    <display:column property="reportName" sortable="true" titleKey="reports.reportName"/>
    <display:column property="reportComment" sortable="true" titleKey="reports.reportComment"  />
    
    <display:column  title="Param"  style="text-align: center" >
	    <c:if test="${reportsList.runTimeParameterReq=='Yes'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
		</c:if>
		<c:if test="${reportsList.runTimeParameterReq=='No'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/cancel001.gif"/>
		</c:if>
	</display:column> 
	
    <display:column  titleKey="reports.enabled"  style="text-align: center" >
	    <c:if test="${reportsList.enabled=='Yes'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
		</c:if>
		<c:if test="${reportsList.enabled=='No'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/cancel001.gif"/>
		</c:if>
	</display:column> 
	<display:column titleKey="reports.docsxfer">
		<c:if test="${reportsList.docsxfer=='Yes'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
		</c:if>
		<c:if test="${reportsList.docsxfer=='No'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/cancel001.gif"/> 
		</c:if>
	</display:column>
	
	<%-- 
	<display:column title="Remove" style="width: 15px;">
						<a>
							<img align="middle" title="" onclick="confirmSubmit('${reportsList.id}');" style="margin: 0px 0px 0px 8px;" src="images/recyle-trans.gif"/>
						</a>
	</display:column>
	 --%>	 
	<%-- 
		<display:column property="createdBy" sortable="true" titleKey="reports.createdBy" />
	    <display:column property="createdOn" sortable="true" titleKey="reports.createdOn" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column property="updatedBy" sortable="true" titleKey="reports.updatedBy" />
	    <display:column property="updatedOn" sortable="true" titleKey="reports.updatedOn" format="{0,date,dd-MMM-yyyy}"/>
	--%>
    <display:setProperty name="paging.banner.item_name" value="reports"/> 
    <display:setProperty name="paging.banner.items_name" value="reports"/>
    
    <display:setProperty name="export.excel.filename" value="Reports List.xls"/> 
    <display:setProperty name="export.csv.filename" value="Reports List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="Reports List.pdf"/>
</display:table>

<c:out value="${buttons}" escapeXml="false" /> 
</s:form>
</body>
<script type="text/javascript"> 
    //highlightTableRows("reportsList"); 
</script> 