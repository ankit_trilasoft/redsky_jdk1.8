package com.trilasoft.app.dao.hibernate;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;

import org.appfuse.dao.GenericDao;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.hibernate.HibernateException;

import com.trilasoft.app.dao.MyFileDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.SystemDefaultManager;

public class MyFileDaoHibernate extends GenericDaoHibernate<MyFile, Long> implements MyFileDao {

	private HibernateUtil hibernateUtil;
	private MyFileManager myFileManager;
	private ErrorLogManager errorLogManager;
	private String sessionCorpID;
	
	public MyFileDaoHibernate() {
		
		super(MyFile.class);
			
	} 

	public class SDTO{
		private  Object id;
		private Object fileType;
		private  Object description;
		private Object updatedBy;
		private Object updatedOn;
		private Object documentCategory;
		private Object vendorCode;

		
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getFileType() {
			return fileType;
		}
		public void setFileType(Object fileType) {
			this.fileType = fileType;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(Object updatedBy) {
			this.updatedBy = updatedBy;
		}
		public Object getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Object updatedOn) {
			this.updatedOn = updatedOn;
		}
		public Object getDocumentCategory() {
			return documentCategory;
		}
		public void setDocumentCategory(Object documentCategory) {
			this.documentCategory = documentCategory;
		}
		public Object getVendorCode() {
			return vendorCode;
		}
		public void setVendorCode(Object vendorCode) {
			this.vendorCode = vendorCode;
		}

	}
	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}
	
	public void getSessionCorpID()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();	
	}
	
	protected HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();  
    }
	
	public List getListByDocId(String fileId, String active, String secure ,String categorySearchValue) {
		try {
			List myFileList = new ArrayList();
			if(!categorySearchValue.equalsIgnoreCase("")){
				if (secure.equalsIgnoreCase("false")) {
					if (active.equalsIgnoreCase("true")) {
						myFileList = getHibernateTemplate().find("from MyFile where (isSecure is false or  isSecure is null) AND active = true AND fileId='"+fileId+"' and documentCategory like '"+categorySearchValue+"%' order by description" );
					}
					if (active.equalsIgnoreCase("false")) {
						myFileList = getHibernateTemplate().find("from MyFile where (isSecure is false or  isSecure is null) AND active = false AND fileId='"+fileId+"' and documentCategory like '"+categorySearchValue+"%' order by description");
					}
				} else if (secure.equalsIgnoreCase("true")) {
					if (active.equalsIgnoreCase("true")) {
						myFileList = getHibernateTemplate().find("from MyFile where isSecure is true AND active = true AND fileId='"+fileId+"' and documentCategory like '"+categorySearchValue+"%' order by description");
					}
					if (active.equalsIgnoreCase("false")) {
						myFileList = getHibernateTemplate().find("from MyFile where isSecure is true AND active = false AND fileId='"+fileId+"' and documentCategory like '"+categorySearchValue+"%' order by description");
					}
				} else {
					if (active.equalsIgnoreCase("true")) {
						myFileList = getHibernateTemplate().find("from MyFile where active = true AND fileId='"+fileId+"' and documentCategory like '"+categorySearchValue+"%' order by description");
					}
					if (active.equalsIgnoreCase("false")) {
						myFileList = getHibernateTemplate().find("from MyFile where active = false AND fileId='"+fileId+"' and documentCategory like '"+categorySearchValue+"%' order by description");
					}
				}
			}else{
			if (secure.equalsIgnoreCase("false")) {
				if (active.equalsIgnoreCase("true")) {
					myFileList = getHibernateTemplate().find("from MyFile where (isSecure is false or  isSecure is null) AND active = true AND fileId='"+fileId+"' order by description" );
				}
				if (active.equalsIgnoreCase("false")) {
					myFileList = getHibernateTemplate().find("from MyFile where (isSecure is false or  isSecure is null) AND active = false AND fileId='"+fileId+"' order by description");
				}
			} else if (secure.equalsIgnoreCase("true")) {
				if (active.equalsIgnoreCase("true")) {
					myFileList = getHibernateTemplate().find("from MyFile where isSecure is true AND active = true AND fileId='"+fileId+"' order by description");
				}
				if (active.equalsIgnoreCase("false")) {
					myFileList = getHibernateTemplate().find("from MyFile where isSecure is true AND active = false AND fileId='"+fileId+"' order by description");
				}
			} else {
				if (active.equalsIgnoreCase("true")) {
					myFileList = getHibernateTemplate().find("from MyFile where active = true AND fileId='"+fileId+"' order by description");
				}
				if (active.equalsIgnoreCase("false")) {
					myFileList = getHibernateTemplate().find("from MyFile where active = false AND fileId='"+fileId+"' order by description");
				}
			}
			}
			return myFileList;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List getListByCustomerNumber(String customerNumber, String active) {
		try {
			List myFileList = new ArrayList();
			if (active.equalsIgnoreCase("true")) {
				myFileList = getHibernateTemplate().find("from MyFile where active = true and (isSecure is false or  isSecure is null) AND (customerNumber like'"+customerNumber+"%' or fileId like '"+customerNumber+"%') order by description");
			}
			if (active.equalsIgnoreCase("false")) {
				myFileList = getHibernateTemplate().find("from MyFile where active = false and (isSecure is false or  isSecure is null) AND (customerNumber like '"+customerNumber+"%' or fileId like '"+customerNumber+"%') order by description");
			}
			return myFileList;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List getListByCustomerNumberForTicket(String sequenceNumber,String shipNumber, String active){
		try {
			List myFileList = new ArrayList();
			if (active.equalsIgnoreCase("true")) {
				myFileList = getHibernateTemplate().find("from MyFile where active = true and (isSecure is false or  isSecure is null) AND (fileId ='"+sequenceNumber+"' or fileId = '"+shipNumber+"') order by description");
			}
			
			return myFileList;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	
	
	public List getListByDesc(Long id) {
		try {
			return getHibernateTemplate().find("select description from MyFile where id=?", id);
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List<MyFile> findByDocs(String fileType, String fileId) {
		try {
			return getHibernateTemplate().find("from MyFile where active = true AND fileId='" + fileId + "' AND fileType like '" + fileType + "%'");
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List findByDocs(String fileType, String fileId,String sessionCorpID)
	{
		try {
			return getHibernateTemplate().find("SELECT location FROM MyFile WHERE active = true AND fileId='"+fileId+"' and corpID='"+sessionCorpID+"' and fileType='"+fileType+"'");
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	
	public Long findByLocation(String location,String sessionCorpID)
	{
		Long id=null;
		try {
			List al= this.getSession().createSQLQuery("SELECT id FROM myfile WHERE location='"+location+"' and corpID='"+sessionCorpID+"' ").list();
			if(al!=null && !al.isEmpty() && al.get(0)!=null  && !al.get(0).toString().equalsIgnoreCase("")){
				id=Long.parseLong(al.get(0).toString());
			}
			return id;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}	
	public List<MyFile> findByReleventDocs(String fileType, String fileId) {
		try {
			return getHibernateTemplate().find("from MyFile where active = true AND fileId='" + fileId + "' AND fileType like '" + fileType + "%'");
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List getMaxId(String sessionCorpid) {
		try {
			return this.getSession().createSQLQuery("select max(id) from myfile").list();
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public int updateMyfileStatus(String status, Long ids, String updatedBy) {
		try {
			List<MyFile> myFile = getHibernateTemplate().find("from MyFile where id="+ids);
			for (MyFile myFile2 : myFile) {
				if(myFile2.getNetworkLinkId() != null && myFile2.getNetworkLinkId().trim().length() > 0){
					if (status.equals("true")) {
						return this.getSession().createSQLQuery("update myfile set isCportal=true,updatedOn = now(),updatedBy='" + updatedBy + "' where networkLinkId='" + myFile2.getNetworkLinkId() + "' and active is true;").executeUpdate();
					} else {
						return this.getSession().createSQLQuery("update myfile set isCportal=false,updatedOn = now(),updatedBy='" + updatedBy + "' where networkLinkId='" + myFile2.getNetworkLinkId() + "' and active is true;").executeUpdate();
					}
				}else{
					if (status.equals("true")) {
						return getHibernateTemplate().bulkUpdate("update MyFile set isCportal=true,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + ids + "'");
					} else {
						return getHibernateTemplate().bulkUpdate("update MyFile set isCportal=false,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + ids + "'");
					}
				}
			}
			return 0;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return 0;
	}

	public int updateMyfileAccStatus(String status, Long ids, String updatedBy) {
		try {
			List<MyFile> myFile = getHibernateTemplate().find("from MyFile where id="+ids);
			for (MyFile myFile2 : myFile) {
				if(myFile2.getNetworkLinkId() != null && myFile2.getNetworkLinkId().trim().length() > 0){
					if (status.equals("true")) {
						return this.getSession().createSQLQuery("update myfile set isAccportal=true,updatedOn = now(),updatedBy='" + updatedBy + "' where networkLinkId='" + myFile2.getNetworkLinkId() + "' and active is true;").executeUpdate();
					} else {
						return this.getSession().createSQLQuery("update myfile set isAccportal=false,updatedOn = now(),updatedBy='" + updatedBy + "' where networkLinkId='" + myFile2.getNetworkLinkId() + "' and active is true;").executeUpdate();
					}
				}else{
					if (status.equals("true")) {
						return getHibernateTemplate().bulkUpdate("update MyFile set isAccportal=true,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + ids + "'");
					} else {
						return getHibernateTemplate().bulkUpdate("update MyFile set isAccportal=false,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + ids + "'");
					}
				}
			}
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return 0;
	}
	private Set<Role> roles;
	public List getAccountFiles(String seqNum, String sessionCorpID) {
		try {
			List accountFile = new ArrayList();
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal();
			String usertype = user.getUserType();
			Set<Role> roles = user.getRoles();
			String shipNum=seqNum.substring(0, seqNum.length()-2);
			if (usertype.trim().equalsIgnoreCase("ACCOUNT")) {
				if((roles.toString().indexOf("ROLE_CORP_ACC_SP")) > -1){
					accountFile = getHibernateTemplate().find("from MyFile where active = true AND isServiceProvider=true and (fileId ='" + seqNum + "' or fileId='"+shipNum+"') and corpId='" + sessionCorpID + "' order by description");
				}else{
					accountFile = getHibernateTemplate().find("from MyFile where active = true AND isAccportal=true and (fileId ='" + seqNum + "' or fileId='"+shipNum+"') and corpId='" + sessionCorpID + "' order by description");
				}
			}
			if ((usertype.trim().equalsIgnoreCase("AGENT"))|| (usertype.trim().equalsIgnoreCase("PARTNER"))) {
				accountFile = getHibernateTemplate().find("from MyFile where active = true AND isPartnerPortal=true and ( fileId ='" + seqNum + "' or customerNumber ='"+seqNum+"' ) and corpId='" + sessionCorpID + "' order by description");
			}
			return accountFile;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public int updateMyfilePartnerStatus(String status, Long ids, String updatedBy) {
		try {
			List<MyFile> myFile = getHibernateTemplate().find("from MyFile where id="+ids);
			for (MyFile myFile2 : myFile) {
				if(myFile2.getNetworkLinkId() != null && myFile2.getNetworkLinkId().trim().length() > 0){
					if (status.equals("true")) {
						return this.getSession().createSQLQuery("update myfile set isPartnerPortal=true,updatedOn = now(),updatedBy='" + updatedBy + "' where networkLinkId='" + myFile2.getNetworkLinkId() + "' and active is true;").executeUpdate();
					} else {
						return this.getSession().createSQLQuery("update myfile set isPartnerPortal=false,updatedOn = now(),updatedBy='" + updatedBy + "' where networkLinkId='" + myFile2.getNetworkLinkId() + "' and active is true;").executeUpdate();
					}
				}else{
					if (status.equals("true")) {
						return getHibernateTemplate().bulkUpdate("update MyFile set isPartnerPortal=true,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + ids + "'");
					} else {
						return getHibernateTemplate().bulkUpdate("update MyFile set isPartnerPortal=false,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + ids + "'");
					}
				}
			}
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return 0;
	}
	
	public int updateMyfileServiceProviderStatus(String status, Long ids, String updatedBy) {
		try {
			List<MyFile> myFile = getHibernateTemplate().find("from MyFile where id="+ids);
			for (MyFile myFile2 : myFile) {
				if(myFile2.getNetworkLinkId() != null && myFile2.getNetworkLinkId().trim().length() > 0){
					if (status.equals("true")) {
						return this.getSession().createSQLQuery("update myfile set isServiceProvider=true,updatedOn = now(),updatedBy='" + updatedBy + "' where networkLinkId='" + myFile2.getNetworkLinkId() + "' and active is true;").executeUpdate();
					} else {
						return this.getSession().createSQLQuery("update myfile set isServiceProvider=false,updatedOn = now(),updatedBy='" + updatedBy + "' where networkLinkId='" + myFile2.getNetworkLinkId() + "' and active is true;").executeUpdate();
					}
				}else{
					if (status.equals("true")) {
						return getHibernateTemplate().bulkUpdate("update MyFile set isServiceProvider=true,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + ids + "'");
					} else {
						return getHibernateTemplate().bulkUpdate("update MyFile set isServiceProvider=false,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + ids + "'");
					}
				}
			}
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return 0;
	}

	String updatedDate = "";
	public List searchMyFiles(String fileType, String description, String fileId, String updatedBy, Date updatedOn, String sessionCorpID, String index, String duplicates) {
		try {
			getHibernateTemplate().setMaxResults(500);
			if (!(updatedOn == null)) {
				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(updatedOn));
				updatedDate = nowYYYYMMDD2.toString();
			}
			//System.out.println("duplicates" + duplicates);
			List<Object> myFiles = new ArrayList<Object>();
			if (duplicates.equalsIgnoreCase("true")) {
				String query = "";
				if (!(updatedOn == null)) {

					if (fileId.trim().equalsIgnoreCase("")) {
						query = "SELECT a.id,a.fileType, a.description, a.fileId ,a.fileFileName,a.fileSize,a.isCportal,a.isAccportal,a.isPartnerPortal,a.createdOn,a.createdBy FROM myfile a,( SELECT fileId, fileType, fileSize, count(*) as '#'FROM myfile WHERE fileSize is not null AND corpID = '"
						     +sessionCorpID+"' AND active is true AND (fileId is not null or fileId !='') AND createdOn = '"+ updatedDate+"' GROUP BY fileId, fileType, fileSize HAVING count(*) > 1 ORDER BY fileId, fileType, fileSize) b " +
							 "WHERE a.fileId = b.fileId AND a.fileType = b.fileType AND a.fileSize = b.fileSize AND (a.fileType like '"
						     +fileType+ "%' or a.fileType is null) AND (a.description like '"
						     +description.replaceAll("'", "''")+ "%' or a.description is null) and (a.createdBy like '"
						     +updatedBy.replaceAll("'", "''")+ "%' or a.createdBy is null) limit 500";
					} else {
						query = "SELECT a.id,a.fileType, a.description, a.fileId ,a.fileFileName,a.fileSize,a.isCportal,a.isAccportal,a.isPartnerPortal,a.createdOn,a.createdBy FROM myfile a,( SELECT fileId, fileType, fileSize, count(*) as '#'FROM myfile WHERE fileSize is not null AND corpID = '"
						     +sessionCorpID+"' AND active is true AND (fileId is not null or fileId !='') AND createdOn = '"+ updatedDate+"' GROUP BY fileId, fileType, fileSize HAVING count(*) > 1 ORDER BY fileId, fileType, fileSize) b " +
							 "WHERE a.fileId = b.fileId AND a.fileType = b.fileType AND a.fileSize = b.fileSize AND (a.fileType like '"
						     +fileType+ "%' or a.fileType is null) AND (a.description like '"
						     +description.replaceAll("'", "''")+ "%' or a.description is null) and (a.fileId like '"
							 +fileId.replaceAll("'", "''")+ "%') and (a.createdBy like '"
						     +updatedBy.replaceAll("'", "''")+ "%' or a.createdBy is null) limit 500 ";
					}
				} else {
					if (fileId.trim().equalsIgnoreCase("")) {
						query = "SELECT a.id,a.fileType, a.description, a.fileId ,a.fileFileName,a.fileSize,a.isCportal,a.isAccportal,a.isPartnerPortal,a.createdOn,a.createdBy FROM myfile a,( SELECT fileId, fileType, fileSize, count(*) as '#'FROM myfile WHERE fileSize is not null AND corpID = '"
							     +sessionCorpID+"' AND active is true AND (fileId is not null or fileId !='') GROUP BY fileId, fileType, fileSize HAVING count(*) > 1 ORDER BY fileId, fileType, fileSize) b " +
								 "WHERE a.fileId = b.fileId AND a.fileType = b.fileType AND a.fileSize = b.fileSize AND (a.fileType like '"
							     +fileType+ "%' or a.fileType is null) AND (a.description like '"
							     +description.replaceAll("'", "''")+ "%' or a.description is null) and (a.createdBy like '"
							     +updatedBy.replaceAll("'", "''")+ "%' or a.createdBy is null) limit 500";
					} else {
						query = "SELECT a.id,a.fileType, a.description, a.fileId ,a.fileFileName,a.fileSize,a.isCportal,a.isAccportal,a.isPartnerPortal,a.createdOn,a.createdBy FROM myfile a,( SELECT fileId, fileType, fileSize, count(*) as '#'FROM myfile WHERE fileSize is not null AND corpID = '"
							     +sessionCorpID+"' AND active is true AND (fileId is not null or fileId !='') GROUP BY fileId, fileType, fileSize HAVING count(*) > 1 ORDER BY fileId, fileType, fileSize) b " +
								 "WHERE a.fileId = b.fileId AND a.fileType = b.fileType AND a.fileSize = b.fileSize AND (a.fileType like '"
							     +fileType+ "%' or a.fileType is null) AND (a.description like '"
							     +description.replaceAll("'", "''")+ "%' or a.description is null) and (a.fileId like '"
							     +fileId.replaceAll("'", "''")+ "%') and (a.createdBy like '"
							     +updatedBy.replaceAll("'", "''")+ "%' or a.createdBy is null) limit 500";
					}
				}
				List myFiles1 = this.getSession().createSQLQuery(query).list();
				MyFile myFile;
				Iterator it = myFiles1.iterator();
				while (it.hasNext()) {
					Object[] obj = (Object[]) it.next();
					myFile = new MyFile();
					myFile.setId(new Long((new BigInteger(obj[0].toString())).toString()));
					myFile.setFileType((String) obj[1]);
					myFile.setDescription((String) obj[2]);
					myFile.setFileId((String) obj[3]);
					myFile.setFileFileName((String) obj[4]);
					myFile.setFileSize((String) obj[5]);
					myFile.setIsCportal((Boolean) obj[6]);
					myFile.setIsAccportal((Boolean) obj[7]);
					myFile.setIsPartnerPortal((Boolean) obj[8]);
					myFile.setCreatedOn((Date) obj[9]);
					myFile.setCreatedBy((String) obj[10]);
					myFiles.add(myFile);
				}
				return myFiles;
			} else {
				if (index.equalsIgnoreCase("true")) {
					if (!(updatedOn == null)) {
						if (fileId.trim().equalsIgnoreCase("")) {
							return getHibernateTemplate().find(
									"from MyFile where active = true AND (fileType like '" + fileType+ "%' or fileType is null) and (description like '" + description.replaceAll("'", "''")
									+ "%' or description is null) and (fileId is not null or fileId != '') and (createdBy like '" + updatedBy.replaceAll("'", "''")
									+ "%' or createdBy is null) and date_format(createdOn,'%Y-%m-%d') = '" + updatedDate + "' and corpId='" + sessionCorpID + "'");
							
							//return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("").setFetchSize(100).setFirstResult(10).setMaxResults(10).list();
							
						} else {
							return getHibernateTemplate().find(
									"from MyFile where active = true AND (fileType like '" + fileType + "%' or fileType is null) and (description like '" + description.replaceAll("'", "''")
									+ "%' or description is null) and (fileId like '" + fileId.replaceAll("'", "''") + "%') and (createdBy like '" + updatedBy.replaceAll("'", "''")
									+ "%' or createdBy is null) and date_format(createdOn,'%Y-%m-%d') = '" + updatedDate + "' and corpId='" + sessionCorpID + "' ");
						}
					} else {
						if (fileId.trim().equalsIgnoreCase("")) {
							return getHibernateTemplate().find(
									"from MyFile where active = true AND (fileType like '" + fileType + "%' or fileType is null) and (description like '" + description.replaceAll("'", "''")
									+ "%' or description is null) and (fileId is not null or fileId != '') and (createdBy like '" + updatedBy.replaceAll("'", "''")
									+ "%' or createdBy is null) and corpId='" + sessionCorpID + "'");
						} else {
							return getHibernateTemplate().find(
									"from MyFile where active = true AND (fileType like '" + fileType + "%' or fileType is null) and (description like '" + description.replaceAll("'", "''")
									+ "%' or description is null) and (fileId like '" + fileId.replaceAll("'", "''") + "%') and (createdBy like '" + updatedBy.replaceAll("'", "''")
									+ "%' or createdBy is null) and corpId='" + sessionCorpID + "' ");
						}
					}
				} else {
					if (!(updatedOn == null)) {
						if (fileId.trim().equalsIgnoreCase("")) {
							return getHibernateTemplate().find(
									"from MyFile where active = true AND (fileType like '" + fileType + "%' or fileType is null) and (description like '" + description.replaceAll("'", "''")
									+ "%' or description is null) and (fileId is null or fileId = '') and (createdBy like '" + updatedBy.replaceAll("'", "''")
									+ "%' or createdBy is null) and date_format(createdOn,'%Y-%m-%d') = '" + updatedDate + "' and corpId='" + sessionCorpID + "'");
						} else {
							return getHibernateTemplate().find(
									"from MyFile where active = true AND (fileType like '" + fileType + "%' or fileType is null) and (description like '" + description.replaceAll("'", "''")
									+ "%' or description is null) and (fileId like '" + fileId.replaceAll("'", "''") + "%') and (createdBy like '" + updatedBy.replaceAll("'", "''")
									+ "%' or createdBy is null) and date_format(createdOn,'%Y-%m-%d') = '" + updatedDate + "' and corpId='" + sessionCorpID + "' ");
						}

					} else {
						if (fileId.trim().equalsIgnoreCase("")) {
							return getHibernateTemplate().find(
									"from MyFile where active = true AND (fileType like '" + fileType + "%' or fileType is null) and (description like '" + description.replaceAll("'", "''")
									+ "%' or description is null) and (fileId is null or fileId = '') and (createdBy like '" + updatedBy.replaceAll("'", "''")
									+ "%' or createdBy is null) and corpId='" + sessionCorpID + "' ");
						} else {
							return getHibernateTemplate().find(
									"from MyFile where active = true AND (fileType like '" + fileType + "%' or fileType is null) and (description like '" + description.replaceAll("'","''")
									+ "%' or description is null) and (fileId like '" + fileId.replaceAll("'", "''") + "%') and (createdBy like '" + updatedBy.replaceAll("'", "''")
									+ "%' or createdBy is null) and corpId='" + sessionCorpID + "'");
						}

					}
				}
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List getIndexedList(String sessionCorpID, String updatedBy) {
		try {
			return getHibernateTemplate().find("from MyFile where active = true AND (fileId is null or fileId ='') and updatedBy='" + updatedBy + "'  and corpId='" + sessionCorpID + "'");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List getDocAccessControl(String fileType, String corpID) {
		try {
			if(corpID.equalsIgnoreCase("TSFT"))
			{
				
				return this.getSession().createSQLQuery("select concat(if(isCportal=true,'true','false'),'#',if(isAccportal=true,'true','false'),'#',if(isPartnerPortal=true,'true','false'),'#',if(isBookingAgent=true,'true','false'),'#',if(isNetworkAgent=true,'true','false'),'#',if(isOriginAgent=true,'true','false'),'#',if(isSubOriginAgent=true,'true','false'),'#',if(isDestAgent=true,'true','false'),'#',if(isSubDestAgent=true,'true','false'),'#',if(isServiceProvider=true,'true','false'),'#',if(invoiceAttachment=true,'true','false'),'#',if(isvendorCode=true,'true','false'),'#',if(isPaymentStatus=true,'true','false')) from documentaccesscontrol where fileType='"+ fileType + "' and corpID in (select corpID  from company where allowAgentInvoiceUpload=true)").list();

			}
			else
			{
			return this.getSession().createSQLQuery("select concat(if(isCportal=true,'true','false'),'#',if(isAccportal=true,'true','false'),'#',if(isPartnerPortal=true,'true','false'),'#',if(isBookingAgent=true,'true','false'),'#',if(isNetworkAgent=true,'true','false'),'#',if(isOriginAgent=true,'true','false'),'#',if(isSubOriginAgent=true,'true','false'),'#',if(isDestAgent=true,'true','false'),'#',if(isSubDestAgent=true,'true','false'),'#',if(isServiceProvider=true,'true','false'),'#',if(invoiceAttachment=true,'true','false'),'#',if(isvendorCode=true,'true','false'),'#',if(isPaymentStatus=true,'true','false')) from documentaccesscontrol where fileType='"+ fileType + "' and corpID='" + corpID + "'").list();
			}
			} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public void updateMyfileStripped(Long id) {
		try {
			getHibernateTemplate().bulkUpdate("update MyFile set coverPageStripped=true where id='" + id + "'");
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
	}

	public List findSOName(String fileId, String sessionCorpID) {
		try {
			List seqFile = new ArrayList();

			seqFile = this.getSession().createSQLQuery("select concat(if(lastname is null || lastName='','1',lastName) ,'#',if(firstName is null || firstName='','1',firstName)) from serviceorder where (sequenceNumber = '"+ fileId + "' or shipNumber = '" + fileId + "') and corpId = '" + sessionCorpID + "'").list();

			if (seqFile.isEmpty()) {
				seqFile = this.getSession().createSQLQuery("select concat(if(lastname is null || lastName='','1',lastName) ,'#',if(firstName is null || firstName='','1',firstName)) from customerfile where sequenceNumber = '"+ fileId + "' and corpId = '" + sessionCorpID + "'").list();
			}
			return seqFile;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public void updateActive(Long id, String updatedBy) {
		try {
			List<MyFile> myFile = getHibernateTemplate().find("from MyFile where id="+id);
			for (MyFile myFile2 : myFile) {
					this.getSession().createSQLQuery("update myfile set active = false, isCportal = false, isAccportal= false, isPartnerPortal = false,updatedOn = now(),updatedBy='" + updatedBy + "' where (networkLinkId='" + myFile2.getNetworkLinkId() + "' or id='"+ id + "') ").executeUpdate();
			}
			//getHibernateTemplate().bulkUpdate("update MyFile set active = false, isCportal = false, isAccportal= false, isPartnerPortal = false,updatedOn = now(),updatedBy='" + updatedBy + "' where id='"+ id + "'");
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
	}

	public List getBasketList(Long fileId) {
		try {
			return getHibernateTemplate().find("from MyFile where active = false AND fileId='" + fileId + "'");
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public void recoverDoc(Long id, String updatedBy) {
		try {
			getHibernateTemplate().bulkUpdate("update MyFile set active = true,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + id + "'");
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
	}

	public List getSequanceNum(String fileId, String sessionCorpID) {
		try {
			List seqFile = new ArrayList();

			seqFile = this.getSession().createSQLQuery("select sequenceNumber from serviceorder where (sequenceNumber = '" + fileId + "' or shipNumber = '" + fileId + "') and corpId = '" + sessionCorpID + "'").list();

			if (seqFile.isEmpty()) {
				seqFile = this.getSession().createSQLQuery("select sequenceNumber from customerfile where sequenceNumber = '" + fileId + "' and corpId = '" + sessionCorpID + "'").list();
			}
			return seqFile;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	public String getAccountLinePayingStatus(String accountlineShipNumber,String accountlineVendorCode,String sessionCorpID){
	String accountLinePayingStatus="";
	try {
		List accPayingStatusList= this.getSession().createSQLQuery("select distinct payingStatus from accountline where shipNumber = '" + accountlineShipNumber + "' and vendorCode='"+accountlineVendorCode+"' and payingStatus!='' and payingStatus is not null and corpId = '" + sessionCorpID + "'").list();
		
		if(accPayingStatusList!=null && !accPayingStatusList.isEmpty() && accPayingStatusList.get(0)!=null){
			accountLinePayingStatus=accPayingStatusList.get(0).toString();
		}
	} catch (Exception e) {
		logger.error("Error executing query"+ e,e);
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
    	e.printStackTrace();
	}	
	return accountLinePayingStatus;
	}
	public List getSequanceNumByInvNum(String fileId, String sessionCorpID) {
		try {
			return this.getSession().createSQLQuery("select distinct shipNumber from accountline where recInvoiceNumber = '" + fileId + "' and corpId = '" + sessionCorpID + "'").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List getSequanceNumByClm(String fileId, String sessionCorpID) {
		try {
			return this.getSession().createSQLQuery("select shipNumber from claim where claimNumber = '" + fileId + "' and corpId = '" + sessionCorpID + "'").list();
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List getSequanceNumByTkt(String fileId, String sessionCorpID) {
		try {
			return this.getSession().createSQLQuery("select shipNumber from workticket where ticket = '" + fileId + "' and corpId = '" + sessionCorpID + "'").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public void getUpdateTransDate(String id,String updatedBy) {
		try {
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(new Date()));
			updatedDate = nowYYYYMMDD2.toString();
			getHibernateTemplate().bulkUpdate("update MyFile set docSent = '" + updatedDate + "',transDocStatus='READY_TO_UPLOAD',transferredBy='"+updatedBy+"' where id in(" + id + ") AND mapFolder is not null AND mapFolder != ''");
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
	}

	public List getMapFolder(String fileType, String corpID) {
		try {
			return this.getSession().createSQLQuery("select bucket2 from refmaster where parameter='DOCUMENT' AND code='" + fileType + "' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')").list();
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public void updateFileSize(String updateSize, Long id) {
		try {
			getHibernateTemplate().bulkUpdate("update MyFile set fileSize = '" + updateSize + "' where id='" + id + "'");
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
	}

	public List getIsSecure(String fileType, String corpID) {
		try {
			return this.getSession().createSQLQuery("select bucket from refmaster where parameter='DOCUMENT' AND code='" + fileType + "' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpID)+"','"+corpID+"')").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List getWasteBasketAll(String corpId) {
		try {
			List myFileList = new ArrayList();
			myFileList = getHibernateTemplate().find("from MyFile where active = false and corpID='" + corpId + "'");
			return myFileList;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List searchWasteFiles(String fileType, String description, String fileId, String updatedBy, Date updatedOn, String sessionCorpID) {
		try {
			if (!(updatedOn == null)) {
				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(updatedOn));
				updatedDate = nowYYYYMMDD2.toString();
				return getHibernateTemplate().find("from MyFile where active = false AND (fileType like '" + fileType + "%' or fileType is null) and (description like '" + description
						+ "%' or description is null) and (fileId like '" + fileId + "%') and (createdBy like '" + updatedBy
						+ "%' or createdBy is null) and  createdOn like '" + updatedDate + "%' and corpId='" + sessionCorpID + "'");
			} else {
				return getHibernateTemplate().find("from MyFile where active = false AND (fileType like '" + fileType + "%' or fileType is null) and (description like '" + description
						+ "%' or description is null) and (fileId like '" + fileId + "%')  and (createdBy like '" + updatedBy + "%' or createdBy is null) and corpId='" + sessionCorpID + "'");

			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List findAccountPortalFileList(String sequenceNumber,String sessionCorpID) {
		try {
			return 	getHibernateTemplate().find("from MyFile where  active = true AND isAccportal=true and corpID='"+sessionCorpID+"' and customerNumber='"+sequenceNumber+"' order by description" );
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
		
	}

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	public List getAllMyfile(String fileId, String sessionCorpID){
		try {
			return 	getHibernateTemplate().find("from MyFile where corpID='"+sessionCorpID+"' and fileId='"+fileId+"' and active=true" );
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	public List getMyFileList(String linkFile, String sessionCorpID){
		try {
			return getHibernateTemplate().find("from MyFile where corpID='"+sessionCorpID+"' and id in ("+linkFile+") and active=true");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	public List getLinkedShipNumber(String shipNumber, String type, Boolean isBookingAgent, Boolean isNetworkAgent,	Boolean isOriginAgent, Boolean isSubOriginAgent,Boolean isDestAgent, Boolean isSubDestAgent) {
		try {
			String query="select concat(";
			if(isNetworkAgent!=null && isNetworkAgent==true){query=query+" if(networkAgentExSO is null or networkAgentExSO='','',networkAgentExSO),'#',if(bookingAgentShipNumber is null or bookingAgentShipNumber='','',bookingAgentShipNumber),'#',";}
			if(isOriginAgent!=null && isOriginAgent==true){query=query+" if(originAgentExSO is null or originAgentExSO='','',originAgentExSO),'#',";}
			if(isSubOriginAgent!=null && isSubOriginAgent==true){query=query+" if(originSubAgentExSO is null or originSubAgentExSO='','',originSubAgentExSO),'#',";}
			if(isDestAgent!=null && isDestAgent==true){query=query+" if(destinationAgentExSO is null or destinationAgentExSO='','',destinationAgentExSO),'#',";}
			if(isSubDestAgent!=null && isSubDestAgent==true){query=query+" if(destinationSubAgentExSO is null or destinationSubAgentExSO='','',destinationSubAgentExSO),'#',";}
			if(isBookingAgent!=null && isBookingAgent==true){query=query+" if(t.shipNumber is null or t.shipNumber='','',t.shipNumber) ,'#', if(bookingAgentExSO is null or bookingAgentExSO='','',bookingAgentExSO)";}
			
			if(query.endsWith(",")==true){
				query = query.substring(0, query.lastIndexOf(",")-4);
			}
			List linkedShipNumberList= new ArrayList();
			List linkedList = new ArrayList();
			List linkedShipNumber= this.getSession().createSQLQuery("select if(bookingAgentShipNumber is null or bookingAgentShipNumber='','1',bookingAgentShipNumber) from serviceorder where  shipNumber='"+shipNumber+"' and bookingAgentShipNumber not like 'UTSI%' ").list();
			if(linkedShipNumber!=null && !linkedShipNumber.isEmpty() && (!(linkedShipNumber.get(0).toString().equalsIgnoreCase("1")))){
				query = query+" ) from trackingstatus t , serviceorder s where t.shipNumber='"+linkedShipNumber.get(0).toString()+"' and s.id=t.id ";	
			}else{
				query = query+" ) from trackingstatus t , serviceorder s where t.shipNumber='"+shipNumber+"' and t.id=s.id ";
			}
			linkedList=this.getSession().createSQLQuery(query).list();
			if(linkedList!=null && !linkedList.isEmpty()){
			String listString=linkedList.get(0).toString();
			String [] listArray=listString.split("#");
			for(String sNumber:listArray){
				if(!sNumber.trim().equalsIgnoreCase("")){
					linkedShipNumberList.add(sNumber);	
				}
			  }
			 }
			return linkedShipNumberList;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	public List getParentShipNumber(String shipNumber){
		try {
			List list= this.getSession().createSQLQuery("select shipNumber from trackingstatus where (bookingAgentExSO='"+shipNumber+"' or networkAgentExSO='"+shipNumber+"' or originAgentExSO='"+shipNumber+"' or originSubAgentExSO='"+shipNumber+"' or destinationAgentExSO='"+shipNumber+"' or destinationSubAgentExSO='"+shipNumber+"')").list();
			return list;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public void upDateNetworkLinkId(String networkLinkId, Long id) {
		try {
			getHibernateTemplate().bulkUpdate("update MyFile set networkLinkId='"+networkLinkId+"' where id='"+id+"'");
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
	}
	public List findlinkedIdList(String networkLinkId){
		try {
			List list= this.getSession().createSQLQuery("select id from myfile where networkLinkId = '"+networkLinkId+"'").list();
			return list;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List findMyFilePortalList(String extSO, String sessionCorpID) {
		try {
			List list= this.getSession().createSQLQuery("select id from myfile where fileId = '"+extSO+"' and isPartnerPortal is true").list();
			return list;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public void removeNetworkLinkId(String networkLinkId, String fileId, String sessionCorpID) {
		try {
			List list = this.getSession().createSQLQuery("select id from myfile where fileId='"+fileId+"'").list();
			if(list != null && !list.isEmpty()){
				Iterator it = list.iterator();
			    while (it.hasNext()) {
			    	 Long id = Long.parseLong(it.next().toString());
					 MyFile newMyFile = myFileManager.getForOtherCorpid(id);
			    	 newMyFile.setNetworkLinkId("");
			    	 myFileManager.save(newMyFile);	
				}
			}
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
	}
	
	public List getLinkedAllShipNumber(String shipNumber, String checkFlagBA,String checkFlagNA, String checkFlagOA, String checkFlagSOA,String checkFlagDA, String checkFlagSDA, String type) {
			try {
				String query="select concat(";
				if(type.equalsIgnoreCase("checked")){
					if((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NA")){query=query+" if(networkAgentExSO is null or networkAgentExSO='','',networkAgentExSO),'#', if(bookingAgentShipNumber is null or bookingAgentShipNumber='','',bookingAgentShipNumber),'#',";}
					if((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("OA")){query=query+" if(originAgentExSO is null or originAgentExSO='','',originAgentExSO),'#',";}
					if((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("SOA")){query=query+" if(originSubAgentExSO is null or originSubAgentExSO='','',originSubAgentExSO),'#',";}
					if((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("DA")){query=query+" if(destinationAgentExSO is null or destinationAgentExSO='','',destinationAgentExSO),'#',";}
					if((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("SDA")){query=query+" if(destinationSubAgentExSO is null or destinationSubAgentExSO='','',destinationSubAgentExSO),'#',";}
					if((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("BA")){query=query+" if(t.shipNumber is null or t.shipNumber='','',t.shipNumber),'#', if(bookingAgentExSO is null or bookingAgentExSO='','',bookingAgentExSO)" ;}
				}else{
					if((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NNA")){query=query+" if(networkAgentExSO is null or networkAgentExSO='','',networkAgentExSO),'#', if(bookingAgentShipNumber is null or bookingAgentShipNumber='','',bookingAgentShipNumber),'#',";}
					if((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("NOA")){query=query+" if(originAgentExSO is null or originAgentExSO='','',originAgentExSO),'#',";}
					if((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("NSOA")){query=query+" if(originSubAgentExSO is null or originSubAgentExSO='','',originSubAgentExSO),'#',";}
					if((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("NDA")){query=query+" if(destinationAgentExSO is null or destinationAgentExSO='','',destinationAgentExSO),'#',";}
					if((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("NSDA")){query=query+" if(destinationSubAgentExSO is null or destinationSubAgentExSO='','',destinationSubAgentExSO),'#',";}
					if((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("NBA")){query=query+" if(t.shipNumber is null or t.shipNumber='','',t.shipNumber) ,'#', if(bookingAgentExSO is null or bookingAgentExSO='','',bookingAgentExSO) ";}	
				}
				if(query.endsWith(",")==true){
					query = query.substring(0, query.lastIndexOf(",")-4);
				}
				List linkedShipNumberList= new ArrayList();
				List linkedList = new ArrayList();
				List linkedShipNumber= this.getSession().createSQLQuery("select if(bookingAgentShipNumber is null or bookingAgentShipNumber='','1',bookingAgentShipNumber) from serviceorder where shipNumber='"+shipNumber+"' and  bookingAgentShipNumber not like 'UTSI%' ").list();
				if(linkedShipNumber!=null && !linkedShipNumber.isEmpty() && (!(linkedShipNumber.get(0).toString().equalsIgnoreCase("1")))){
					query = query+" ) from trackingstatus t  , serviceorder s where t.shipNumber='"+linkedShipNumber.get(0).toString()+"' and s.id=t.id ";	
				}else{
					query = query+" ) from trackingstatus t , serviceorder s where t.shipNumber='"+shipNumber+"' and s.id=t.id ";
				}
				linkedList=this.getSession().createSQLQuery(query).list();
				if(linkedList!=null && !linkedList.isEmpty()){
				String listString=linkedList.get(0).toString();
				String [] listArray=listString.split("#");
				for(String sNumber:listArray){
					if(!sNumber.trim().equalsIgnoreCase("")){
						linkedShipNumberList.add(sNumber);	
					}
				  }
				 }
				return linkedShipNumberList;
			} catch (Exception e) {
				getSessionCorpID();
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	e.printStackTrace();
			}
			return null;
		}
	
	public List getDefCheckedShipNumber(String shipNumber){	  
		try {
			List linkedList = new ArrayList();	
			String soNetworkGroup="F";
			String utsiNetworkGroup="F";
			String agentNetworkGroup="F";
			List trackingStatusDataList=this.getSession().createSQLQuery("select concat(if(soNetworkGroup is true,'T','F'),'~',if(utsiNetworkGroup is true,'T','F'),'~',if(agentNetworkGroup is true,'T','F')) from trackingstatus where shipNumber='"+shipNumber+"' ").list();
			if(trackingStatusDataList!=null && (!(trackingStatusDataList.isEmpty())) && trackingStatusDataList.get(0)!=null){
				try{
				String[] trackingStatusData=trackingStatusDataList.get(0).toString().split("~"); 
				soNetworkGroup=trackingStatusData[0];
				utsiNetworkGroup=trackingStatusData[1];
				agentNetworkGroup=trackingStatusData[2];
				}catch(Exception e){
					getSessionCorpID();
					logger.error("Error executing query"+ e,e);
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	e.printStackTrace();
				}
			}
			String linkedShip="NOT";
			if(soNetworkGroup.equalsIgnoreCase("T")){
				String linkedShipNumberQuery="select if(bookingAgentShipNumber is null or bookingAgentShipNumber='','1',bookingAgentShipNumber) from serviceorder where shipNumber='"+shipNumber+"'";
			   if(utsiNetworkGroup.equalsIgnoreCase("T")){
				   linkedShipNumberQuery="select if(bookingAgentExSO is null or bookingAgentExSO='','1',bookingAgentExSO) from trackingstatus where shipNumber='"+shipNumber+"'";  
			   }
				List linkedShipNumber= this.getSession().createSQLQuery(linkedShipNumberQuery).list();
				if(linkedShipNumber!=null && !linkedShipNumber.isEmpty() && (!(linkedShipNumber.get(0).toString().equalsIgnoreCase("1")))){
					linkedShip=linkedShipNumber.get(0).toString();
					String query = "select if((networkAgentExSO='"+shipNumber+"' or bookingAgentExSO='"+shipNumber+"'),'NA',if(originAgentExSO='"+shipNumber+"','OA',if(originSubAgentExSO='"+shipNumber+"','SOA',if(destinationAgentExSO='"+shipNumber+"','DA',if(destinationSubAgentExSO='"+shipNumber+"','SDA','NOTAAGENT')))))" +
					" from trackingstatus where shipNumber='"+linkedShip+"' " ;
					if(utsiNetworkGroup.equalsIgnoreCase("T")){
						query = "select if((networkAgentExSO='"+linkedShip+"' or bookingAgentExSO='"+linkedShip+"'),'NA',if(originAgentExSO='"+linkedShip+"','OA',if(originSubAgentExSO='"+linkedShip+"','SOA',if(destinationAgentExSO='"+linkedShip+"','DA',if(destinationSubAgentExSO='"+linkedShip+"','SDA','NOTAAGENT')))))" +
						" from trackingstatus where shipNumber='"+shipNumber+"' " ;	
					}
					if(agentNetworkGroup.equalsIgnoreCase("T")){
						query="select if(bookingAgentShipNumber='"+linkedShip+"','BA','NOTAAGENT') from serviceorder where shipNumber='"+shipNumber+"'";
					}
					linkedList = this.getSession().createSQLQuery(query).list();
				}else{
					linkedList=this.getSession().createSQLQuery("select if(shipnumber='"+shipNumber+"','BA','1') from serviceorder where shipnumber='"+shipNumber+"' ").list();
				}
					
			}else{
			List linkedShipNumber= this.getSession().createSQLQuery("select if(bookingAgentShipNumber is null or bookingAgentShipNumber='','1',bookingAgentShipNumber) from serviceorder where shipNumber='"+shipNumber+"' ").list();
			if(linkedShipNumber!=null && !linkedShipNumber.isEmpty() && (!(linkedShipNumber.get(0).toString().equalsIgnoreCase("1")))){
				linkedShip=linkedShipNumber.get(0).toString();
				String query = "select if((networkAgentExSO='"+shipNumber+"' or bookingAgentExSO='"+shipNumber+"'),'NA',if(originAgentExSO='"+shipNumber+"','OA',if(originSubAgentExSO='"+shipNumber+"','SOA',if(destinationAgentExSO='"+shipNumber+"','DA',if(destinationSubAgentExSO='"+shipNumber+"','SDA','NOTAAGENT')))))" +
				" from trackingstatus where shipNumber='"+linkedShip+"' " ;
				linkedList = this.getSession().createSQLQuery(query).list();
			}else{
				String corpID= (shipNumber.substring(0, 4));
				linkedList=this.getSession().createSQLQuery("select if(findnetworkcorpid(s.bookingagentcode)= '"+corpID+"' ,'BA', if(findnetworkcorpid(t.originAgentCode)= '"+corpID+"' ,'OA'," +
		   		"if(findnetworkcorpid(t.destinationAgentCode)= '"+corpID+"','DA'," +
	   			"if(findnetworkcorpid(t.networkPartnerCode)= '"+corpID+"','NA'," +
	   					"if(findnetworkcorpid(t.originSubAgentCode)= '"+corpID+"','SOA'," +
	   							"if(findnetworkcorpid(t.destinationSubAgentCode)= '"+corpID+"','SDA','NoLink')))))) " +
	   									"from serviceorder s ,trackingstatus t where s.shipnumber='"+shipNumber+"' and t.id = s.id   ").list();
			}
			}
			return linkedList;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	public void updateAgentFlag(String networkLinkId, String checkAgent){
		try {
			List list = this.getSession().createSQLQuery("select id from myfile where networkLinkId='"+networkLinkId+"'").list();
			if(list != null && !list.isEmpty()){
				Iterator it = list.iterator();
			    while (it.hasNext()) {
			    	Long id = Long.parseLong(it.next().toString());
					MyFile newMyFile = myFileManager.getForOtherCorpid(id);
					if(checkAgent.equalsIgnoreCase("BA")){
						newMyFile.setIsBookingAgent(true);
					}else if(checkAgent.equalsIgnoreCase("NA")){
						newMyFile.setIsNetworkAgent(true);
					}else if(checkAgent.equalsIgnoreCase("OA")){
						newMyFile.setIsOriginAgent(true);
					}else if(checkAgent.equalsIgnoreCase("SOA")){
						newMyFile.setIsSubOriginAgent(true);
					}else if(checkAgent.equalsIgnoreCase("DA")){
						newMyFile.setIsDestAgent(true);
					}else if(checkAgent.equalsIgnoreCase("SDA")){
						newMyFile.setIsSubDestAgent(true);
					}
			    	myFileManager.save(newMyFile);	
				}
			}
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}		
	}
	
	public List getShipNumberList(String shipNumber){
		try {
			String query="select concat(if(networkAgentExSO is null or networkAgentExSO='','1','NA'),'#',if(bookingAgentExSO is null or bookingAgentExSO='','1','NA'),'#', if(originAgentExSO is null or originAgentExSO='','1','OA'),'#',if(originSubAgentExSO is null or originSubAgentExSO='','1','SOA'),'#',if(destinationAgentExSO is null or destinationAgentExSO='','1','DA'),'#',if(destinationSubAgentExSO is null or destinationSubAgentExSO='','1','SDA'),'#',if(shipNumber is null or shipNumber='','1','BA')";
			List linkedShipNumberList= new ArrayList();
			List linkedList = new ArrayList();
			List linkedShipNumber= this.getSession().createSQLQuery("select if(bookingAgentShipNumber is null or bookingAgentShipNumber='','1',bookingAgentShipNumber) from serviceorder where shipNumber='"+shipNumber+"' ").list();
			if(linkedShipNumber!=null && !linkedShipNumber.isEmpty() && (!(linkedShipNumber.get(0).toString().equalsIgnoreCase("1")))){
				query = query+" ) from trackingstatus where shipNumber in ('"+linkedShipNumber.get(0).toString()+"','"+shipNumber+"') ";	
			}else{
				query = query+" ) from trackingstatus where shipNumber='"+shipNumber+"' ";
			}
			linkedList=this.getSession().createSQLQuery(query).list();
			if(linkedList!=null && !linkedList.isEmpty()){
			Iterator it=linkedList.iterator();
			while(it.hasNext())	{
			String listString=it.next().toString();
			String [] listArray=listString.split("#");
			for(String sNumber:listArray){
				if(!sNumber.trim().equalsIgnoreCase("") && (!(linkedShipNumberList.contains(sNumber.trim())))){
					linkedShipNumberList.add(sNumber);	
				}
			  }
			 }
}
			return linkedShipNumberList;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	public List findTransDocSystemDefault(String corpID){
		try {
			return getHibernateTemplate().find("select transDoc from SystemDefault where corpID=?",corpID);
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	public List searchMaxSizeFiles(String fileType, String description, String fileId, String updatedBy, Date updatedOn, String sessionCorpID, String docName, Integer docSize,String index){
		
		try {
			if (!(updatedOn == null)) {
				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(updatedOn));
				updatedDate = nowYYYYMMDD2.toString();
				
			}
			if(fileType==null){
				fileType="";
			}
			if(description==null){
				description="";
			}
			if(fileId==null){
				fileId="";
			}
			if(updatedBy==null){
				updatedBy="";
			}if(docName==null){
				docName="";
			}
			if(docSize==null){
				docSize=0;
			}
			String testQuery ="";
			if(index.equals("true"))
			{
				testQuery = "and updatedBy != 'fileresize' and";
				//index = "true";
			}else{
				testQuery = "and";
			}
			List<Object> myFileSize = new ArrayList<Object>();
			List tempFileSizeList=new ArrayList();
			if (!(updatedOn == null)) {
				 testQuery = "SELECT id,fileType,description,fileId ,fileFileName,fileSize,mapfolder,createdOn,createdBy FROM myfile " +
				"WHERE if(Right(fileSize, 2)='MB',LEFT(fileSize, 2) > "+docSize+",'')  and filefilename like '%pdf' " +
				"and (fileType like '"+fileType.replaceAll("'", "''")+ "%' or fileType is null) " +
				"and (description like '"+description.replaceAll("'", "''")+ "%' or description is null) " +
				"and (createdBy like '" +updatedBy.replaceAll("'", "''")+ "%' or createdBy is null) " +
				"and (fileId like '"+fileId.replaceAll("'", "''")+ "%' or fileId is null) " +
				testQuery +
				"createdOn like '" + updatedDate + "%' limit 500 ";
				 tempFileSizeList=this.getSession().createSQLQuery(testQuery).list();
			
			}else{			
				 testQuery = " SELECT id,fileType,description,fileId ,fileFileName,fileSize,mapfolder,createdOn,createdBy FROM myfile " +
				"WHERE if(Right(fileSize, 2)='MB',LEFT(fileSize, 2) > "+docSize+",'')  and filefilename like '%pdf' " +
				"and (fileType like '"+fileType.replaceAll("'", "''")+ "%' or fileType is null) " +
				"and (description like '"+description.replaceAll("'", "''")+ "%' or description is null) " +
				"and (createdBy like '" +updatedBy.replaceAll("'", "''")+ "%' or createdBy is null) " +
				testQuery +
				"(fileId like '"+fileId.replaceAll("'", "''")+ "%' or fileId is null) " +
				" limit 500 ";
			tempFileSizeList=this.getSession().createSQLQuery(testQuery).list();
			}
System.out.println("tempFileSizeList"+tempFileSizeList);
			Iterator it = tempFileSizeList.iterator();
			
			MyFile myFile;
			while(it.hasNext()){
			Object[] obj = (Object[]) it.next();
			myFile = new MyFile();
			myFile.setId(new Long((new BigInteger(obj[0].toString())).toString()));
			myFile.setFileType((String) obj[1]);
			myFile.setDescription((String) obj[2]);
			myFile.setFileId((String) obj[3]);
			myFile.setFileFileName((String) obj[4]);
			myFile.setFileSize((String) obj[5]);
			myFile.setMapFolder((String) obj[6]);
			myFile.setCreatedOn((Date) obj[7]);
			myFile.setCreatedBy((String) obj[8]);
			myFileSize.add(myFile);
			}
			return myFileSize;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	public List getcompressListId(Long fid){
		try {
			List ListBy=new ArrayList();
			//return getHibernateTemplate().find("select description from MyFile where id=?", id);
			ListBy=this.getSession().createSQLQuery("select description from myfile where id='"+fid+"'").list();
			
			return ListBy;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	public List findShipNumberBySequenceNumer(String sequenceNumber,String sessionCorpID,Boolean isNetworkRecord){
		try {
			List shipNumList=new ArrayList();
			if(isNetworkRecord!=null && isNetworkRecord ){
				shipNumList=this.getSession().createSQLQuery("select shipNumber from serviceorder where sequenceNumber='"+sequenceNumber+"' and corpid='"+sessionCorpID+"' and isNetworkRecord is true ").list();
				return shipNumList;
			}else{
				shipNumList=this.getSession().createSQLQuery("select shipNumber from serviceorder where sequenceNumber='"+sequenceNumber+"' and corpid='"+sessionCorpID+"' ").list();
				return shipNumList;
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	public void updateNetworkLinkIdCheck(String networkLinkId, String linkedSeqNum ,String valueTarget,String FileId,Long id,String checkValue){
		try {
			if(checkValue.equalsIgnoreCase("check")){
				//String abc="update myfile set '"+valueTarget+"'=true where fileId='" +FileId+ "' and id='"+id+"' and networkLinkId='"+networkLinkId+"' and active is true";
				this.getSession().createSQLQuery("update myfile set "+valueTarget+"=true where fileId='" +FileId+ "' and id='"+id+"' and networkLinkId='"+networkLinkId+"' and active is true").executeUpdate();
			}else{
				//this.getSession().createSQLQuery("update myfile set networkLinkId='',"+valueTarget+"=false where fileId='" +linkedSeqNum+ "' and active is true;").executeUpdate();
				this.getSession().createSQLQuery("update myfile set "+valueTarget+"=false where fileId='" +FileId+ "' and  id='"+id+"' and networkLinkId='"+networkLinkId+"' and active is true").executeUpdate();
				
			}
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}		
	}
	public List getLinkedId(String networkLinkId, String sessionCorpID){
		try {
			List list= this.getSession().createSQLQuery("select id from myfile where networkLinkId = '"+networkLinkId+"' and corpid = '"+sessionCorpID+"'").list();
			return list;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	
	public TreeMap<String,List<MyFile>> getdocTypeMapList(String fileId,String active, String secure, String categorySearchValue,String tempSortOrder,String sortingOrder) {
		TreeMap <String, List<MyFile>> myFileMap = new TreeMap<String, List<MyFile>>();
		try {
			List<MyFile> myFileList = new ArrayList<MyFile>();
			List <String> tempDoctTypeList = new ArrayList<String>();
			if(tempSortOrder== null || tempSortOrder.equals("") ){
				tempSortOrder ="description "+sortingOrder;
			}else{
				if(tempSortOrder.contains(",")){
					tempSortOrder = tempSortOrder.replaceAll(",", " "+sortingOrder+", ");
					tempSortOrder = tempSortOrder +" "+sortingOrder;
				}else{
					tempSortOrder = tempSortOrder +" "+sortingOrder;
				}
			}
			tempDoctTypeList = this.getSession().createSQLQuery("select if(documentCategory is null,'',documentCategory) from myfile where (isSecure is false or  isSecure is null) AND active = true AND fileId='"+fileId+"' group by documentCategory order by "+tempSortOrder+" " ).list();
			for (String fileType : tempDoctTypeList) {
				if(fileType.equals("")){
					myFileList = getHibernateTemplate().find("from MyFile where (isSecure is false or  isSecure is null) AND active = true AND fileId='"+fileId+"' and ( documentCategory is null or documentCategory='' ) order by "+tempSortOrder+"  " );
				}else{
					myFileList = getHibernateTemplate().find("from MyFile where (isSecure is false or  isSecure is null) AND active = true AND fileId='"+fileId+"' and documentCategory='"+fileType+"' order by "+tempSortOrder+" " );
				}
				myFileMap.put(fileType, myFileList);
			}
			
			
			// Query for secure list
			myFileList = getHibernateTemplate().find("from MyFile where isSecure is true AND active = true AND fileId='"+fileId+"' order by "+tempSortOrder+" ");
			myFileMap.put("ZZ-Secure Doc", myFileList);
			
			
			// Query for waste basket
			myFileList = getHibernateTemplate().find("from MyFile where (active = false or active is null ) AND fileId='"+fileId+"' order by "+tempSortOrder+" ");
			myFileMap.put("ZZ-Waste Basket", myFileList);
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		
		
		return myFileMap;
	}
	
	public TreeMap<String, List<MyFile>> getListByCustomerNumberGrpByType(String customerNumber, String active,String tempSortOrder,String sortingOrder){
		if(tempSortOrder== null || tempSortOrder.equals("") ){
			tempSortOrder ="description "+sortingOrder;
		}else{
			if(tempSortOrder.contains(",")){
				tempSortOrder = tempSortOrder.replaceAll(",", " "+sortingOrder+", ");
				tempSortOrder = tempSortOrder +" "+sortingOrder;
			}else{
				tempSortOrder = tempSortOrder +" "+sortingOrder;
			}
		}
		TreeMap <String, List<MyFile>> myFileMap = new TreeMap<String, List<MyFile>>();
		List<MyFile> myFileList = new ArrayList<MyFile>();
			myFileList = getHibernateTemplate().find("from MyFile where active = true and (isSecure is false or  isSecure is null) AND (customerNumber like'"+customerNumber+"%' or fileId like '"+customerNumber+"%')  order by "+tempSortOrder+" ");
			for (MyFile myFile : myFileList) {
				String tempCategory ="";
				if(myFile.getDocumentCategory()==null){
					tempCategory ="";
				}else{
					tempCategory = myFile.getDocumentCategory();
				}
				if(myFileMap.containsKey(tempCategory.trim())){
					List <MyFile> tempList = new ArrayList<MyFile>();
					tempList = myFileMap.get(tempCategory.trim());
					tempList.add(myFile);
					myFileMap.put(tempCategory.trim(),tempList);
		 		}else{
		 			List <MyFile> tempList = new ArrayList<MyFile>();
		 			tempList.add(myFile);
		 			myFileMap.put(tempCategory.trim(),tempList);
		 		}
			}
		
		
		
		// Query for secure list
		myFileList = getHibernateTemplate().find("from MyFile where isSecure is true AND active = true AND  (customerNumber like '"+customerNumber+"%' or fileId like '"+customerNumber+"%') order by  "+tempSortOrder+" ");
		myFileMap.put("ZZ-Secure Doc", myFileList);
		
		
		// Query for waste basket
		myFileList = getHibernateTemplate().find("from MyFile where (active = false or active is null ) AND (customerNumber like '"+customerNumber+"%' or fileId like '"+customerNumber+"%') order by  "+tempSortOrder+"  ");
		myFileMap.put("ZZ-Waste Basket", myFileList);
		
		return myFileMap;
	}

	

	public TreeMap<String, List<MyFile>> getdocTypeMapList(String shipNumber,
			String active, String secure, String categorySearchValue) {
		// TODO Auto-generated method stub
		return null;
	}

	public TreeMap<String, List<MyFile>> getListByCustomerNumberGrpByType(
			String sequenceNumber, String active) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}
	public List recipientWithEmailStatus(String jobNumber,String noteFor,String sessionCorpID,String fileId) {
		List myFileList = new ArrayList();
	myFileList= getHibernateTemplate().find("FROM EmailSetup where (module = '" + noteFor + "' )  AND (filenumber like '%"+fileId+"%') order by datesent desc ");		
		return myFileList;
	}
	
	public List fileTypeForPartnerCode(String corpID) {
		try {
			return this.getSession().createSQLQuery("select fileType from documentaccesscontrol where isvendorCode is true and corpID='" + corpID + "'").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public int updateInvoiceAttachment(String status, Long ids, String updatedBy) {
		try {
			List<MyFile> myFile = getHibernateTemplate().find("from MyFile where id="+ids);
			for (MyFile myFile2 : myFile) {
				if(myFile2.getNetworkLinkId() != null && myFile2.getNetworkLinkId().trim().length() > 0){
					if (status.equals("true")) {
						return this.getSession().createSQLQuery("update myfile set invoiceAttachment=true,updatedOn = now(),updatedBy='" + updatedBy + "' where networkLinkId='" + myFile2.getNetworkLinkId() + "' and active is true;").executeUpdate();
					} else {
						return this.getSession().createSQLQuery("update myfile set invoiceAttachment=false,updatedOn = now(),updatedBy='" + updatedBy + "' where networkLinkId='" + myFile2.getNetworkLinkId() + "' and active is true;").executeUpdate();
					}
				}else{
					if (status.equals("true")) {
						return getHibernateTemplate().bulkUpdate("update MyFile set invoiceAttachment=true,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + ids + "'");
					} else {
						return getHibernateTemplate().bulkUpdate("update MyFile set invoiceAttachment=false,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + ids + "'");
					}
				}
			}
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return 0;
	}

	public List accountPortalFiles(String securityQuery, String corpID) {
		return getHibernateTemplate().find("from MyFile where active = true and isAccportal=true  and fileId in (" + securityQuery + ")  and corpId='" + corpID + "' order by description");
	}
	//13674 - Create dashboard
	public List getDocumentCategoryList(String shipNumber, String documentCategory,String sessionCorpID) {
		List list1 = new ArrayList();
		List documentList = new ArrayList();
	
	
			if(documentCategory!=null && !documentCategory.equals("")){
				documentList =  this.getSession().createSQLQuery("select  id,fileType,description,updatedBy,date_format(updatedOn,'%d-%b-%y') as updatedOn ,documentCategory from myfile where corpID='"+sessionCorpID+"' and fileid='" +shipNumber+ "' and documentCategory='" +documentCategory + "' and active is true  order by id desc limit 4" ).list();
			}else{
			documentList =  this.getSession().createSQLQuery("select  id,fileType,description,updatedBy,date_format(updatedOn,'%d-%b-%y') as updatedOn ,documentCategory from myfile where corpID='"+sessionCorpID+"' and fileid='" +shipNumber+ "' and active is true order by id desc limit 4").list();
			}
			 Iterator it=documentList.iterator();
				while(it.hasNext()){
					Object []row= (Object [])it.next();
					SDTO dto=new SDTO();
					dto.setId(row[0]);
					dto.setFileType(row[1]);
					dto.setDescription(row[2]);
					dto.setUpdatedBy(row[3]);
					dto.setUpdatedOn(row[4]);
					dto.setDocumentCategory(row[5]);
			
					list1.add(dto);
				}
			       
		return list1;
	}
	
	public List getListByDesc(String shipNumber,String sessionCorpID) {
			List list2 = new ArrayList();	
			List documentList =  this.getSession().createSQLQuery("select distinct date_format(updatedOn,'%d-%b-%y') as updatedOn ,documentCategory from myfile where corpID='"+sessionCorpID+"' and fileid='" +shipNumber+ "'  and active is true order by id desc limit 4" ).list();
				 Iterator it=documentList.iterator();
					while(it.hasNext()){
						Object []row= (Object [])it.next();
						SDTO dto=new SDTO();
						dto.setUpdatedOn(row[0]);
						  dto.setDocumentCategory(row[1]);

						list2.add(dto);
					}
				       
			return list2;
	}
//End dashboard

	public List findFileLocation(String openFrom, String closeFrom, String companyCorpID) {
		List documentList =  this.getSession().createSQLQuery("select location,fileId,description from myfile where createdon>='"+openFrom+"' and createdon<='"+closeFrom+"'  and corpid='"+companyCorpID+"'").list();
		return documentList;
	}
	
	
	public int updateMyfileDriverPortalStatus(String status, Long ids, String updatedBy) {
		try {
			List<MyFile> myFile = getHibernateTemplate().find("from MyFile where id="+ids);
			for (MyFile myFile2 : myFile) {
				if(myFile2.getNetworkLinkId() != null && myFile2.getNetworkLinkId().trim().length() > 0){
					if (status.equals("true")) {
						return this.getSession().createSQLQuery("update myfile set isDriver=true,updatedOn = now(),updatedBy='" + updatedBy + "' where networkLinkId='" + myFile2.getNetworkLinkId() + "' and active is true;").executeUpdate();
					} else {
						return this.getSession().createSQLQuery("update myfile set isDriver=false,updatedOn = now(),updatedBy='" + updatedBy + "' where networkLinkId='" + myFile2.getNetworkLinkId() + "' and active is true;").executeUpdate();
					}
				}else{
					if (status.equals("true")) {
						return getHibernateTemplate().bulkUpdate("update MyFile set isDriver=true,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + ids + "'");
					} else {
						return getHibernateTemplate().bulkUpdate("update MyFile set isDriver=false,updatedOn = now(),updatedBy='" + updatedBy + "' where id='" + ids + "'");
					}
				}
			}
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return 0;
	}
	
	public TreeMap<String,List<MyFile>> getDriverDocTypeMapList(String fileId,String active, String secure, String categorySearchValue,String tempSortOrder,String sortingOrder) {
		TreeMap <String, List<MyFile>> myFileMap = new TreeMap<String, List<MyFile>>();
		try {
			List<MyFile> myFileList = new ArrayList<MyFile>();
			List <String> tempDoctTypeList = new ArrayList<String>();
			if(tempSortOrder== null || tempSortOrder.equals("") ){
				tempSortOrder ="description "+sortingOrder;
			}else{
				if(tempSortOrder.contains(",")){
					tempSortOrder = tempSortOrder.replaceAll(",", " "+sortingOrder+", ");
					tempSortOrder = tempSortOrder +" "+sortingOrder;
				}else{
					tempSortOrder = tempSortOrder +" "+sortingOrder;
				}
			}
			tempDoctTypeList = this.getSession().createSQLQuery("select if(documentCategory is null,'',documentCategory) from myfile where (isSecure is false or  isSecure is null) AND active = true and isDriver = true AND fileId='"+fileId+"' group by documentCategory order by "+tempSortOrder+" " ).list();
			for (String fileType : tempDoctTypeList) {
				if(fileType.equals("")){
					myFileList = getHibernateTemplate().find("from MyFile where (isSecure is false or  isSecure is null) AND active = true and isDriver = true AND fileId='"+fileId+"' and ( documentCategory is null or documentCategory='' ) order by "+tempSortOrder+"  " );
				}else{
					myFileList = getHibernateTemplate().find("from MyFile where (isSecure is false or  isSecure is null) AND active = true and isDriver = true AND fileId='"+fileId+"' and documentCategory='"+fileType+"' order by "+tempSortOrder+" " );
				}
				myFileMap.put(fileType, myFileList);
			}
			
			
			// Query for secure list
			myFileList = getHibernateTemplate().find("from MyFile where isSecure is true AND active = true and isDriver = true AND fileId='"+fileId+"' order by "+tempSortOrder+" ");
			myFileMap.put("ZZ-Secure Doc", myFileList);
			
			
			// Query for waste basket
			myFileList = getHibernateTemplate().find("from MyFile where (active = false or active is null ) and isDriver = true AND fileId='"+fileId+"' order by "+tempSortOrder+" ");
			myFileMap.put("ZZ-Waste Basket", myFileList);
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		
		
		return myFileMap;
	}
	public List getdocAgentTypeMapList(String shipNumber,String vendorCode,String sessionCorpID) {
		
		 
		    List accountFile = new ArrayList(); 
		    List al = new ArrayList(); 
			String fileType="Invoice Vendor";
			 String networkAgentId="";
			 al=this.getSession().createSQLQuery("select myFileFileName from accountline  where  shipNumber = '" + shipNumber + "' and   vendorCode='"+vendorCode+"' and payingStatus='A'  and corpID='"+sessionCorpID+"' and myFileFileName is not null and myFileFileName !='' ").list();
			System.out.println("al---"+al);
			 if(al!=null && !al.isEmpty() && al.get(0)!=null  && !al.get(0).toString().equalsIgnoreCase("")){
				 Iterator agentIdIterator =al.iterator();
		    	   
		    	    while(agentIdIterator.hasNext()){
		    	    	String accId=agentIdIterator.next().toString();
		    	    	accId=accId.substring(0, accId.indexOf(":"));

		    	    	if(networkAgentId.equals("")){
		    	    		networkAgentId=accId;
		    	    	}else{
		    	    		networkAgentId=networkAgentId+","+accId;
		    	    	}
		    	    }
		    	   
			}
			
			if(networkAgentId !=null && (!(networkAgentId.trim().equals("")))){
			accountFile = getHibernateTemplate().find("from MyFile where active = true and isPartnerPortal=true and id not in ("+networkAgentId+") and fileId='"+shipNumber+"' and accountLineVendorCode='"+vendorCode+"' and fileType='Invoice Vendor' and corpid='"+sessionCorpID+"' " );
			}else{
				accountFile = getHibernateTemplate().find("from MyFile where active = true and isPartnerPortal=true and fileId='"+shipNumber+"' and accountLineVendorCode='"+vendorCode+"' and fileType='Invoice Vendor' and corpid='"+sessionCorpID+"' " );
					
			}
			
			
				
			 return accountFile;
	
	}

	public List fileTypeForPayableProcessing(String sessionCorpID) {
		try {
			String Query="select fileType from documentaccesscontrol where isPaymentStatus is true and corpID='" + sessionCorpID + "' ";
			if(sessionCorpID !=null && sessionCorpID.equalsIgnoreCase("TSFT")){
				Query="select fileType from documentaccesscontrol where isPaymentStatus is true and corpID in (select corpID  from company where allowAgentInvoiceUpload=true) ";
			} 
			return this.getSession().createSQLQuery(Query).list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	
	public List getDocumentCode(String fileType, String sessionCorpID){

		try {
			return this.getSession().createSQLQuery("select flex8 from refmaster where parameter='DOCUMENT' AND code='" + fileType + "' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
}
