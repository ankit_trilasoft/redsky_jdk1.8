package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.model.Role;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.RoleCorpManagementDao;
import com.trilasoft.app.service.RoleCorpManagementManager;

public class RoleCorpManagementManagerImpl extends GenericManagerImpl<Role, Long>
		implements RoleCorpManagementManager {
	RoleCorpManagementDao roleCorpManagementDao;

	public RoleCorpManagementManagerImpl(RoleCorpManagementDao roleCorpManagementDao) {
		super(roleCorpManagementDao);
		this.roleCorpManagementDao = roleCorpManagementDao;
	}
	
	public List<Role> corpRoles(String corpIdList){
		return roleCorpManagementDao.corpRoles(corpIdList);
	}
	
	public Role fetchRoleDetail(Long id,String corpID){
		return roleCorpManagementDao.fetchRoleDetail(id,corpID);
	}
	public List<Role> searchRole(String name,String corpID,String category){
		return roleCorpManagementDao.searchRole(name, corpID, category);
	}
	
	public boolean checkRoleForExistance(String name, String corpID, String category){
		return roleCorpManagementDao.checkRoleForExistance(name, corpID, category);
	}
	
	public List<String> onlyRoles(String corpIDList){
		return roleCorpManagementDao.onlyRoles(corpIDList);
	}
}
