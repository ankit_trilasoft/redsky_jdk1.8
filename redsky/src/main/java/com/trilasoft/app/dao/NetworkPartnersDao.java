package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.NetworkPartners;

public interface NetworkPartnersDao extends GenericDao<NetworkPartners, Long>{
	public  List getCorpidList(String sessionCorpID);
	public List findNetworkPartnerList(String sessionCorpID, String selectedCorpId);
	public List findByCorpId(String sessionCorpID, String selectedCorpId);
	public Integer countNetworkPartnerByCorpId(String sessionCorpID, String selectedCorpId);
	public void removeByCorpId(String sessionCorpID, String selectedCorpId);
	public Integer countCompanyDivisionByCorpId(String sessionCorpID, String selectedCorpId);
}
