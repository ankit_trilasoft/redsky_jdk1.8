package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CostingDao;
import com.trilasoft.app.model.Costing;
import com.trilasoft.app.service.CostingManager;

public class CostingManagerImpl extends GenericManagerImpl<Costing, Long> implements CostingManager {   
		CostingDao costingDao;   
	  
	    public CostingManagerImpl(CostingDao costingDao) {   
	        super(costingDao);   
	        this.costingDao = costingDao;   
	    }   
	    public List findMaxId() {
	    	
			return costingDao.findMaxId();
		} 
		
	    public List checkById(Long id) { 
	        return costingDao.checkById(id); 
	    }
	    public int genrateInvoiceNumber(Long newInvoiceNumber,String shipNumber)
	    {
	    	return costingDao.genrateInvoiceNumber(newInvoiceNumber,shipNumber);
	    }
	    public List findInvoiceNumber()
	    {
	    	return costingDao.findInvoiceNumber();
	    } 
}
