<%@page import="java.util.SortedMap"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.math.BigDecimal"%>


<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<%@page import="java.util.TreeMap"%>
<head>
  <title>Reciprocity Analysis</title>
  <meta name="heading" content="Reciprocity Analysis "/>
<style>
.spacer{width:147px; display:block}
body {
    margin: 0;
    padding: 0 0 0px !important;
}
</style>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.init()
</script>
<script type="text/javascript">
function printReciprocityReport(){ 
	var date1 = document.forms['summaryForm'].elements['fromDate'].value;
	var date2 = document.forms['summaryForm'].elements['toDate'].value; 
	var agentGroup = document.forms['summaryForm'].elements['agentGroup'].value;
	var militaryShipments = document.forms['summaryForm'].elements['militaryShipment'].value;
	var projectedRevenue = document.forms['summaryForm'].elements['projectedRevenue'].value; 
	var includeChildCorpids = document.forms['summaryForm'].elements['includeChildCorpids'].value; 
	var childCompanys = document.forms['summaryForm'].elements['childCompanys'].value; 
	var subOADAReciprocityAnalysis = document.forms['summaryForm'].elements['subOADAReciprocityAnalysis'].value;
	var jobTypes = document.forms['summaryForm'].elements['jobTypes'].value; 
	var companyDivisions = document.forms['summaryForm'].elements['companyDivisions'].value;  
	window.open("printRevenueReciprocityReport.html?fromDate="+date1+"&toDate="+date2+"&agentGroup="+agentGroup+"&jobTypes="+jobTypes+"&companyDivisions="+companyDivisions+"&militaryShipment="+militaryShipments+"&projectedRevenue="+projectedRevenue+"&includeChildCorpids="+includeChildCorpids+"&childCompanys="+childCompanys+"&subOADAReciprocityAnalysis="+subOADAReciprocityAnalysis+"&decorator=popup&popup=true")
 }
function getGivenReciprocityReport(tempAgent){ 
	var date1 = document.forms['summaryForm'].elements['fromDate'].value;
	var date2 = document.forms['summaryForm'].elements['toDate'].value; 
	//var countryCode = document.forms['summaryForm'].elements['countryCode'].value; 
	var militaryShipments = document.forms['summaryForm'].elements['militaryShipment'].value;
	var projectedRevenue = document.forms['summaryForm'].elements['projectedRevenue'].value; 
	var includeChildCorpids = document.forms['summaryForm'].elements['includeChildCorpids'].value;
	var childCompanys = document.forms['summaryForm'].elements['childCompanys'].value;
	var subOADAReciprocityAnalysis = document.forms['summaryForm'].elements['subOADAReciprocityAnalysis'].value;
	var jobTypes = document.forms['summaryForm'].elements['jobTypes'].value; 
	var companyDivisions = document.forms['summaryForm'].elements['companyDivisions'].value;  
	openWindow("givenReciprocityReport.html?fromDate="+date1+"&toDate="+date2+"&jobTypes="+jobTypes+"&companyDivisions="+companyDivisions+"&militaryShipment="+militaryShipments+"&projectedRevenue="+projectedRevenue+"&includeChildCorpids="+includeChildCorpids+"&childCompanys="+childCompanys+"&subOADAReciprocityAnalysis="+subOADAReciprocityAnalysis+"&agent="+tempAgent+"&decorator=popup&popup=true",1100,500)
 }
function getRecvReciprocityReport(tempAgent){ 
	var date1 = document.forms['summaryForm'].elements['fromDate'].value;
	var date2 = document.forms['summaryForm'].elements['toDate'].value; 
	//var countryCode = document.forms['summaryForm'].elements['countryCode'].value; 
	var militaryShipments = document.forms['summaryForm'].elements['militaryShipment'].value;
	var projectedRevenue = document.forms['summaryForm'].elements['projectedRevenue'].value;
	var includeChildCorpids = document.forms['summaryForm'].elements['includeChildCorpids'].value; 
	var childCompanys = document.forms['summaryForm'].elements['childCompanys'].value;
	var subOADAReciprocityAnalysis = document.forms['summaryForm'].elements['subOADAReciprocityAnalysis'].value;
	var jobTypes = document.forms['summaryForm'].elements['jobTypes'].value; 
	var companyDivisions = document.forms['summaryForm'].elements['companyDivisions'].value;  
	openWindow("recvReciprocityReport.html?fromDate="+date1+"&toDate="+date2+"&jobTypes="+jobTypes+"&companyDivisions="+companyDivisions+"&militaryShipment="+militaryShipments+"&projectedRevenue="+projectedRevenue+"&includeChildCorpids="+includeChildCorpids+"&childCompanys="+childCompanys+"&subOADAReciprocityAnalysis="+subOADAReciprocityAnalysis+"&agent="+tempAgent+"&decorator=popup&popup=true",1100,500)
 }
</script>
<script type="text/javascript">
function toggelTable(target){
	//alert(target);
	animatedcollapse.toggle(target);
	setTimeout('setToggle('+target+')',800);
}
function generateExcellSheet(agentGroupWithCode){
	if(agentGroupWithCode!=''){
		var date1 = document.forms['summaryForm'].elements['fromDate'].value;
		var date2 = document.forms['summaryForm'].elements['toDate'].value; 
		//var countryCode = document.forms['summaryForm'].elements['countryCode'].value; 
		var militaryShipments = document.forms['summaryForm'].elements['militaryShipment'].value;
		var projectedRevenue = document.forms['summaryForm'].elements['projectedRevenue'].value;
		var includeChildCorpids = document.forms['summaryForm'].elements['includeChildCorpids'].value;
		var childCompanys = document.forms['summaryForm'].elements['childCompanys'].value;
		var subOADAReciprocityAnalysis = document.forms['summaryForm'].elements['subOADAReciprocityAnalysis'].value;
		var jobTypes = document.forms['summaryForm'].elements['jobTypes'].value; 
		var companyDivisions = document.forms['summaryForm'].elements['companyDivisions'].value;  
		window.location="reciprocityReport.html?fromDate="+date1+"&toDate="+date2+"&jobTypes="+jobTypes+"&companyDivisions="+companyDivisions+"&militaryShipment="+militaryShipments+"&projectedRevenue="+projectedRevenue+"&includeChildCorpids="+includeChildCorpids+"&childCompanys="+childCompanys+"&subOADAReciprocityAnalysis="+subOADAReciprocityAnalysis+"&agentGroupWithCode="+agentGroupWithCode+"&decorator=popup&popup=true";
	}
}
function setToggle(target)
{
	
	if(target.style.display == "block")
		{
		
		target.style.display = "table";
		
		}
	
	}
</script>
</head>
<s:form id="summaryForm" action="" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<c:if test="${not empty fromDate}">
<s:text id="fromDate" name="${FormDateValue}"> <s:param name="value" value="fromDate" /></s:text>
<td><s:hidden  id="storageEndDate" name="fromDate" value="%{fromDate}" /> </td>
</c:if>
<c:if test="${not empty toDate}">
<s:text id="toDate" name="${FormDateValue}"> <s:param name="value" value="toDate" /></s:text>
<td><s:hidden  id="toDate" name="toDate" value="%{toDate}" /> </td>
</c:if> 
<s:hidden  id="agentGroup" name="agentGroup" value="${agentGroup}" />
<s:hidden  id="militaryShipment" name="militaryShipment" value="${militaryShipment}" />
<s:hidden  id="projectedRevenue" name="projectedRevenue" value="${projectedRevenue}" />
<s:hidden  id="includeChildCorpids" name="includeChildCorpids" value="${includeChildCorpids}" />
<s:hidden  id="childCompanys" name="childCompanys" value="${childCompanys}" />

<s:hidden  id="subOADAReciprocityAnalysis" name="subOADAReciprocityAnalysis" value="${subOADAReciprocityAnalysis}" />
<s:hidden  id="jobTypes" name="jobTypes" value="${jobTypes}" />
<s:hidden  id="companyDivisions" name="companyDivisions" value="${companyDivisions}" />
<div id="layer1" style="width:100%">
<div id="newmnav">
        <ul>
      <li  id="newmnav1" style="background:#FFF "><a class="current"><span>Reciprocity Analysis<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
        </ul>
</div>
<div class="spnblk" style="">&nbsp;</div>
<div class="" style="margin-left: 250px; margin-top:-14px;!margin-top:-35px; "><b>Click on agent group name for transaction details</b><input type="button" class="cssbuttonA" style="width:50px; height:25px;margin-left:310px; "  name="Print"  value="Print" onclick="printReciprocityReport();"/></div>
<div style="padding-bottom:1px;"></div>
<table class="mainDetailTable" cellpadding="0" cellspacing="0" border="0" style="margin: 0px; padding: 0px; width:100%;">
 <tr style="margin: 0px; padding: 0px;">
      <td class="heading-bg" align="center" width="7%">AGENT GROUP</td>
      <td class="heading-bg" align="center" width="24%">AGENT NAME</td>
      <td class="heading-bg" align="center" width="7%">CITY</td>
      <td class="heading-bg" align="center" width="10%">COUNTRY</td>
      <td class="heading-bg" align="center" width="10%"></td>
      <td class="heading-bg" width="7%" align="center" >OA&nbsp;GIVEN</td>
      <td class="heading-bg" width="7%" align="center">DA&nbsp;GIVEN</td>
      <td class="heading-bg" width="7%" align="center">SUB-TOTAL</td>
      <td class="heading-bg" width="7%" align="center">OA&nbsp;RECVD</td>
      <td class="heading-bg" width="7%" align="center">DA&nbsp;RECVD</td>
      <td class="heading-bg" width="7%" align="center">SUB-TOTAL</td>
  </tr>
</table>
<div style="overflow: auto;height: 430px; ">
<table class="mainDetailTable" cellpadding="0" cellspacing="0" border="0" style="margin: 0px; padding: 0px; width:100%;">
   
<tr>

<td colspan="11" width="100%">
  <%
      SortedMap <String, String> companyMap = (SortedMap <String, String>)request.getAttribute("companyMap");
      Iterator companyIterator = companyMap.entrySet().iterator();
      String AgentGroup="";
      String AgentGroup1="";
      String AgentGroup2="";
      String agentName="";
      String agentCode="";
      String Country="";
      String City="";
      String temp="";
      long DA_Total_Given=0;
      long OA_Total_Given=0;
      long Total_Given=0;
      long DA_Total_Recd=0;
      long OA_Total_Recd=0;
      long Total_Recd=0; 
      double DA_Revenue_Total_Given=0;
      double OA_Revenue_Total_Given=0;
      double Total_Revenue_Given=0;
      double DA_Revenue_Total_Recd=0;
      double OA_Revenue_Total_Recd=0;
      double Total_Revenue_Recd=0; 
      long DA_Total_WtsGiven=0;
      long OA_Total_WtsGiven=0;
      long Total_WtsGiven=0;
      long DA_Total_WtsRecd=0;
      long OA_Total_WtsRecd=0;
      long Total_WtsRecd=0;
      
      long DA_Total_WtsKGGiven=0;
      long OA_Total_WtsKGGiven=0;
      long Total_WtsKGGiven=0;
      long DA_Total_WtsKGRecd=0;
      long OA_Total_WtsKGRecd=0;
      long Total_WtsKGRecd=0;
      long OA_TONNAGEKG_given=0;
      long DA_TONNAGEKG_given=0;
      long subtotal_TONNAGEKG_given=0; 
      
      long OA_TONNAGEKG_recd=0;
      long DA_TONNAGEKG_recd=0;
      long subtotal_TONNAGEKG_recd=0;
    
      int j = 0;
      Map<String,String> agentGroupWithCode = new HashMap<String,String>();
      while (companyIterator.hasNext()) {
          int OA_GIVEN=0;
          long OA_TONNAGE_given=0;
          int DA_GIVEN=0;
          long DA_TONNAGE_given=0;
          int OA_RECD=0;
          long OA_TONNAGE_recd=0;
          int DA_RECD=0;
          long DA_TONNAGE_recd=0;
          int subtotal_given=0;
          int subtotal_recd=0;
          long subtotal_TONNAGE_recd=0;
          long subtotal_TONNAGE_given=0; 
          double OA_Revenue_GIVEN=0;
          double DA_Revenue_GIVEN=0;
          double OA_Revenue_RECD=0;
          double DA_Revenue_RECD=0;
          double subtotal_Revenue_given=0;
          double subtotal_Revenue_recd=0;
          
          OA_TONNAGEKG_given=0;
          DA_TONNAGEKG_given=0;
          subtotal_TONNAGEKG_given=0; 
          
           OA_TONNAGEKG_recd=0;
           DA_TONNAGEKG_recd=0;
           subtotal_TONNAGEKG_recd=0;
          
          
          Map.Entry entry = (Map.Entry) companyIterator.next();
          String key = (String) entry.getKey();
          String[] str1 = key.toString().split("#");
          AgentGroup=str1[0];
          AgentGroup1=str1[0];
          if(AgentGroup.equalsIgnoreCase("ZZZ")){
    		  AgentGroup="Un-Group";
    		  AgentGroup1=""; 
    	  }
          AgentGroup2=AgentGroup;
          AgentGroup2=AgentGroup2.replaceAll(" ","").replaceAll("&","").replaceAll("-","");
          if(str1[1].toString().equals("a"))
          {
              agentName="";
          }else {
          agentName=str1[1];
          agentName=agentName.replaceAll("~","-");
          String[] agentCode1 = key.toString().split("~");
          agentCode=agentCode1[1];
          String[] agentCode2 = agentCode.toString().split("#");
          agentCode=agentCode2[0];
          agentCode=agentCode.trim();
          City=str1[2];
          if(City.equalsIgnoreCase("ZZZ")){
        	  City="";  
          }
          }
          String detail= companyMap.get(key);
          String[] str2 = detail.toString().split("#");
          if(str2[0].toString().equals("a"))
          {
              Country="";
          }else {
              Country=str2[0];
              if(Country.equalsIgnoreCase("ZZZ")){
            	  Country="";  
              }
          }
          OA_GIVEN=Integer.parseInt(str2[1])  ; 
          DA_GIVEN=Integer.parseInt(str2[2]); 
          subtotal_given=Integer.parseInt(str2[3]); 
          OA_RECD=Integer.parseInt(str2[4]); 
          DA_RECD=Integer.parseInt(str2[5]); 
          subtotal_recd=Integer.parseInt(str2[6]);  
          OA_TONNAGE_given=Long.parseLong(str2[7]) ; 
          DA_TONNAGE_given=Long.parseLong(str2[8]); 
          subtotal_TONNAGE_given=Long.parseLong(str2[9]); 
          OA_TONNAGE_recd=Long.parseLong(str2[10]); 
          DA_TONNAGE_recd=Long.parseLong(str2[11]); 
          subtotal_TONNAGE_recd=Long.parseLong(str2[12]); 
          OA_Revenue_GIVEN=Double.parseDouble(str2[13]);
          DA_Revenue_GIVEN=Double.parseDouble(str2[14]);
          subtotal_Revenue_given=Double.parseDouble(str2[15]);
          OA_Revenue_RECD=Double.parseDouble(str2[16]);
          DA_Revenue_RECD=Double.parseDouble(str2[17]);
          subtotal_Revenue_recd=Double.parseDouble(str2[18]);
          OA_TONNAGEKG_given=Long.parseLong(str2[19]) ; 
          DA_TONNAGEKG_given=Long.parseLong(str2[20]); 
          subtotal_TONNAGEKG_given=Long.parseLong(str2[21]); 
          OA_TONNAGEKG_recd=Long.parseLong(str2[22]); 
          DA_TONNAGEKG_recd=Long.parseLong(str2[23]); 
          subtotal_TONNAGEKG_recd=Long.parseLong(str2[24]); 
          
                                    if(agentName.equals(""))
              {
                                    	OA_Total_Given=OA_Total_Given+OA_GIVEN;
                                    	DA_Total_Given=DA_Total_Given+DA_GIVEN;
                                    	Total_Given=Total_Given+subtotal_given;
                                    	OA_Total_Recd=OA_Total_Recd+OA_RECD;
                                    	DA_Total_Recd=DA_Total_Recd+DA_RECD;
                                    	Total_Recd=Total_Recd+subtotal_recd;
                                    	OA_Total_WtsGiven=OA_Total_WtsGiven+OA_TONNAGE_given;
                                    	DA_Total_WtsGiven=DA_Total_WtsGiven+DA_TONNAGE_given;
                                    	Total_WtsGiven=Total_WtsGiven+subtotal_TONNAGE_given;
                                    	OA_Total_WtsRecd=OA_Total_WtsRecd+OA_TONNAGE_recd;
                                    	DA_Total_WtsRecd=DA_Total_WtsRecd+DA_TONNAGE_recd;
                                    	Total_WtsRecd=Total_WtsRecd+subtotal_TONNAGE_recd; 
                                    	OA_Revenue_Total_Given=OA_Revenue_Total_Given+OA_Revenue_GIVEN;
                                    	DA_Revenue_Total_Given=DA_Revenue_Total_Given+DA_Revenue_GIVEN;
                                    	Total_Revenue_Given=Total_Revenue_Given+subtotal_Revenue_given;
                                    	OA_Revenue_Total_Recd=OA_Revenue_Total_Recd+OA_Revenue_RECD;
                                    	DA_Revenue_Total_Recd=DA_Revenue_Total_Recd+DA_Revenue_RECD;
                                    	Total_Revenue_Recd=Total_Revenue_Recd+subtotal_Revenue_recd;
                                    	
                                    	OA_Total_WtsKGGiven=OA_Total_WtsKGGiven+OA_TONNAGEKG_given;
                                    	DA_Total_WtsKGGiven=DA_Total_WtsKGGiven+DA_TONNAGEKG_given;
                                    	Total_WtsKGGiven=Total_WtsKGGiven+subtotal_TONNAGEKG_given;
                                    	OA_Total_WtsKGRecd=OA_Total_WtsKGRecd+OA_TONNAGEKG_recd;
                                    	DA_Total_WtsKGRecd=DA_Total_WtsKGRecd+DA_TONNAGEKG_recd;
                                    	Total_WtsKGRecd=Total_WtsKGRecd+subtotal_TONNAGEKG_recd; 
                                    	
                  j=0;
%>      
                  <table id="reci_<%=AgentGroup2%>"  class="mainDetailTable" cellpadding="3" cellspacing="0" border="0" style="margin: 0px; padding: 0px; width: 100%;">
                                     <script type="text/javascript">
                  animatedcollapse.addDiv('reci_<%=AgentGroup2%>', 'fade=0,persist=0,hide=1')
                  </script>
                  <tr height="20px" style="margin: 0px;  padding: 0px;">
                                    <td class="list-columnmain2" style="" width="7%"><b>
                  <div  onClick="javascript:toggelTable('reci_<%=AgentGroup2%>')" style="margin: 0px;cursor: pointer;"  >
                  <img src="${pageContext.request.contextPath}/images/up_down.png" align="top"/>&nbsp;<%=AgentGroup%></div></b></td>
                  <td class="list-columnmain2" width="24%"><img src="${pageContext.request.contextPath}/images/xls1.gif" onclick="javascript:generateExcellSheet('<%=agentGroupWithCode.get(AgentGroup)%>')" align="top"/> <font color="blue" >Excel</font>  <b><%=agentName%></b></td>
                  <td class="list-columnmain2" width="7%"></td>
                  <td class="list-columnmain2" width="10%" style="text-align: right;"><b></b></td>
                  <td class="list-columnmain2" width="10%" style="text-align: right;"><b>Agt Total #</b></td>
                  <td class="list-columnmain2" width="7%"style=" text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_GIVEN%>" />
                  </b></td>
                  <td class="list-columnmain2" width="7%" style=" text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_GIVEN%>" />
                  </b></td>
                  <td class="list-columnmain2" width="7%" style="text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_given%>" />
                  </b></td>
                  <td class="list-columnmain2" width="7%" style="text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_RECD%>" />
                  </b></td>
                  <td class="list-columnmain2" width="7%" style="text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_RECD%>" />
                  </b></td>
                  <td class="list-columnmain2" width="7%" style="text-align: right;"><b>
                 <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_recd%>" />
                  </b></td>
                  </tr>
                  <tr height="25px" style="margin: 0px;  padding: 0px;">
                  <td class="list-columnmain2"  style="text-align: right;" ></td>
                  <td class="list-columnmain2"  style="text-align: right;" ></td>
                  <td class="list-columnmain2"  style="text-align: right;" ></td>
                  <td class="list-columnmain2"  style="text-align: right;" ></td>
                  <td class="list-columnmain2"  style="text-align: right;" ><b>Agt Total Wts </b></td>
                  <td class="list-columnmain2"  style=" text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_TONNAGE_given%>" />&nbsp;Lbs <% temp=OA_TONNAGEKG_given+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%>
                   <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_TONNAGEKG_given%>" />&nbsp;Kgs
                  </b></td>
                  <td class="list-columnmain2"  style="text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_TONNAGE_given%>" />&nbsp;Lbs <% temp=DA_TONNAGEKG_given+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_TONNAGEKG_given%>" />&nbsp;Kgs
                  </b></td>
                  <td class="list-columnmain2"  style="text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_TONNAGE_given%>" />&nbsp;Lbs <% temp=subtotal_TONNAGEKG_given+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_TONNAGEKG_given%>" />&nbsp;Kgs
                  </b></td>
                  <td class="list-columnmain2"  style=" text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_TONNAGE_recd%>" />&nbsp;Lbs <% temp=OA_TONNAGEKG_recd+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_TONNAGEKG_recd%>" />&nbsp;Kgs
                  </b></td>
                  <td class="list-columnmain2"  style="text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_TONNAGE_recd%>" />&nbsp;Lbs <% temp=DA_TONNAGEKG_recd+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_TONNAGEKG_recd%>" />&nbsp;Kgs
                  </b></td>
                  <td class="list-columnmain2"  style="text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_TONNAGE_recd%>" />&nbsp;Lbs <% temp=subtotal_TONNAGEKG_recd+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_TONNAGEKG_recd%>" />&nbsp;Kgs
                  </b></td>
                  </tr>
                  
                  
                  <tr height="25px" style="margin: 0px;  padding: 0px;">
                  <td class="list-columnmain2"  style="text-align: right;" ></td>
                  <td class="list-columnmain2"  style="text-align: right;" ></td>
                  <td class="list-columnmain2"  style="text-align: right;" ></td>
                  <td class="list-columnmain2"  style="text-align: right;" ></td>
                  <td class="list-columnmain2"  style="text-align: right;" ><b>Agt Total Revenue&nbsp;${baseCurrency} </b></td>
                  <td class="list-columnmain2"  style=" text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_Revenue_GIVEN%>" />
                  </b></td>
                  <td class="list-columnmain2"  style="text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_Revenue_GIVEN%>" />
                  </b></td>
                  <td class="list-columnmain2"  style="text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_Revenue_given%>" />
                  </b></td>
                  <td class="list-columnmain2"  style=" text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_Revenue_RECD%>" />
                  </b></td>
                  <td class="list-columnmain2"  style="text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_Revenue_RECD%>" />
                  </b></td>
                  <td class="list-columnmain2"  style="text-align: right;"><b>
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_Revenue_recd%>" />
                  </b></td>
                  </tr>
                  
                  
                  </table>
          <%
          if(!companyIterator.hasNext()){
        	%> 
        	 <c:if test="${agentGroup ==''}">	 
        	 <table id=""  class="mainDetailTable" cellpadding="3" cellspacing="0" border="0" style="margin: 0px; padding: 0px; width: 100%;">
             <tr height="20px" style="margin: 0px;  padding: 0px;" >
             <td class="list-columnmain2" width="7%"  ><b>Report Total </b></td>
             <td class="list-columnmain2" width="24%" > <span class="spacer"></span></td>
             <td class="list-columnmain2" width="7%"></td>
             <td class="list-columnmain2" width="10%" ></td>
             <td class="list-columnmain2" width="10%" style="text-align: right;" ><b>Total #</b></td>
             <td class="list-columnmain2" width="7%" style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=OA_Total_Given%>" /></b>
             </td>
             <td class="list-columnmain2" width="7%" style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=DA_Total_Given%>" /></b>
             </td>
             <td class="list-columnmain2" width="7%" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0" 
              groupingUsed="true" value="<%=Total_Given%>" /></b>
             </td>
             <td class="list-columnmain2" width="7%" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=OA_Total_Recd%>" /></b>
             </td>

             <td class="list-columnmain2" width="7%" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=DA_Total_Recd%>" /></b>
             </td>
             <td class="list-columnmain2" width="7%"  style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=Total_Recd%>" /></b>
             </td>
             </tr>
                                                 <tr height="25px">
             <td class="list-columnmain2"  style="text-align: right;" ></td>
             <td class="list-columnmain2"  style="text-align: right;" ></td>
             <td class="list-columnmain2"  style="text-align: right;" ></td>
             <td class="list-columnmain2"  style="text-align: right;" ></td>
             <td class="list-columnmain2"  style="text-align: right;" ><b>Total Wts</b> </td>
             <td class="list-columnmain2"  style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=OA_Total_WtsGiven%>" />&nbsp;Lbs <% temp=OA_Total_WtsKGGiven+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=OA_Total_WtsKGGiven%>" />&nbsp;Kgs</b>
             </td>
             <td class="list-columnmain2" width="" style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=DA_Total_WtsGiven%>" />&nbsp;Lbs <% temp=DA_Total_WtsKGGiven+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=DA_Total_WtsKGGiven%>" />&nbsp;Kgs</b>
             </td>
             <td class="list-columnmain2" width="" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=Total_WtsGiven%>" />&nbsp;Lbs <% temp=Total_WtsKGGiven+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=Total_WtsKGGiven%>" />&nbsp;Kgs</b>
             </td>
             <td class="list-columnmain2" width="" style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=OA_Total_WtsRecd%>" />&nbsp;Lbs <% temp=OA_Total_WtsKGRecd+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=OA_Total_WtsKGRecd%>" />&nbsp;Kgs</b>
             </td>
             <td class="list-columnmain2" width="" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=DA_Total_WtsRecd%>" />&nbsp;Lbs <% temp=DA_Total_WtsKGRecd+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=DA_Total_WtsKGRecd%>" />&nbsp;Kgs</b>
             </td>
             <td class="list-columnmain2" width="" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=Total_WtsRecd%>" />&nbsp;Lbs <% temp=Total_WtsKGRecd+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=Total_WtsKGRecd%>" />&nbsp;Kgs</b>
             </td>
             </tr>
                                                  <tr height="25px">
             <td class="list-columnmain2"  style="text-align: right;" ></td>
             <td class="list-columnmain2"  style="text-align: right;" ></td>
             <td class="list-columnmain2"  style="text-align: right;" ></td>
             <td class="list-columnmain2"  style="text-align: right;" ></td>
             <td class="list-columnmain2"  style="text-align: right;" ><b>Total Revenue&nbsp;${baseCurrency}</b> </td>
             <td class="list-columnmain2"  style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=OA_Revenue_Total_Given%>" /></b>
             </td>
             <td class="list-columnmain2" width="" style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=DA_Revenue_Total_Given%>" /></b>
             </td>
             <td class="list-columnmain2" width="" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=Total_Revenue_Given%>" /></b>
             </td>
             <td class="list-columnmain2" width="" style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=OA_Revenue_Total_Recd%>" /></b>
             </td>
             <td class="list-columnmain2" width="" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=DA_Revenue_Total_Recd%>" /></b>
             </td>
             <td class="list-columnmain2" width="" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=Total_Revenue_Recd%>" /></b>
             </td>
             </tr></table></c:if>
         <%  }
              }
              else
              {
                  if(j==0)
                  {
          %>

                  <table id="reci_<%=AgentGroup2%>"  class="mainDetailTable" cellpadding="3" cellspacing="0" border="0" style="margin: 0px; padding: 0px; width: 100%;">
                   <%}
                  j=2;
                  %>
                  <tr height="20px" style="margin: 0px;  padding: 0px;" >
                  <td class="rec-columnmain" width="7%"  > <%=AgentGroup1%>  </td>
                  <td class="rec-columnmain" width="24%" ><%=agentName%> <span class="spacer"></span></td>
                  <td class="rec-columnmain" width="7%" ><%=City%></td>
                  <td class="rec-columnmain" width="10%" ><%=Country%></td>
                  <td class="rec-columnmain" width="10%" style="text-align: right;" >#</td>
                  <td class="rec-columnmain" width="7%" style=" text-align: right;">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_GIVEN%>" />
                  </td>
                  <td class="rec-columnmain" width="7%" style=" text-align: right;">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_GIVEN%>" />
                  </td>
                  <td class="rec_sum_bg" width="7%" style="text-align: right;  " >
                  <fmt:formatNumber type="number" maxFractionDigits="0" 
                   groupingUsed="true" value="<%=subtotal_given%>" />
                  </td>
                  <td class="rec-columnmain" width="7%" style="text-align: right;">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_RECD%>" />
                  </td>

                  <td class="rec-columnmain" width="7%" style="text-align: right;">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_RECD%>" />
                  </td>
                  <td class="rec_sum_bg" width="7%"  style="text-align: right; "  >
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_recd%>" />
                  </td>
                  </tr>
                                                      <tr height="25px">
                  <td class="list-columnmain"  style="text-align: right;" ></td>
                  <td class="list-columnmain"  style="text-align: right;" ></td>
                  <td class="list-columnmain"  style="text-align: right;" ></td>
                  <td class="list-columnmain"  style="text-align: right;" ></td>
                  <td class="list-columnmain"  style="text-align: right;" >Wts </td>
                  <td class="list-columnmain"  style=" text-align: right;">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_TONNAGE_given%>" />&nbsp;Lbs <% temp=OA_TONNAGEKG_given+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_TONNAGEKG_given%>" />&nbsp;Kgs
                  </td>
                  <td class="list-columnmain" width="" style=" text-align: right;">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_TONNAGE_given%>" />&nbsp;Lbs <% temp=DA_TONNAGEKG_given+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_TONNAGEKG_given%>" />&nbsp;Kgs
                  </td>
                  <td class="rec_sum_bg" width="" style="text-align: right;">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_TONNAGE_given%>" />&nbsp;Lbs <% temp=subtotal_TONNAGEKG_given+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_TONNAGEKG_given%>" />&nbsp;Kgs
                  </td>
                  <td class="list-columnmain" width="" style=" text-align: right;">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_TONNAGE_recd%>" />&nbsp;Lbs <% temp=OA_TONNAGEKG_recd+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_TONNAGEKG_recd%>" />&nbsp;Kgs
                  </td>
                  <td class="list-columnmain" width="" style="text-align: right;">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_TONNAGE_recd%>" />&nbsp;Lbs <% temp=DA_TONNAGEKG_recd+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_TONNAGEKG_recd%>" />&nbsp;Kgs
                  </td>
                  <td class="rec_sum_bg" width="" style="text-align: right;">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_TONNAGE_recd%>" />&nbsp;Lbs <% temp=subtotal_TONNAGEKG_recd+""; if(temp != null && temp.equalsIgnoreCase("") && temp.equalsIgnoreCase("0") && temp.equalsIgnoreCase("0.0") && temp.equalsIgnoreCase("0.00") && temp.length()<4){ %>&nbsp;&nbsp;&nbsp;&nbsp;<%}%><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_TONNAGEKG_recd%>" />&nbsp;Kgs
                  </td>
                  </tr>
                                                     <tr height="25px">
                  <td class="list-columnmain"  style="text-align: right;" ></td>
                  <td class="list-columnmain"  style="text-align: right;" ></td>
                  <td class="list-columnmain"  style="text-align: right;" ></td>
                  <td class="list-columnmain"  style="text-align: right;" ></td>
                  <td class="list-columnmain"  style="text-align: right; background-color: #C4E395" >Revenue&nbsp;${baseCurrency} </td>
                  <td class="list-columnmain"  style=" text-align: right; background-color: #C4E395">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_Revenue_GIVEN%>" />
                  </td>
                  <td class="list-columnmain" width="" style=" text-align: right; background-color: #C4E395">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_Revenue_GIVEN%>" />
                  </td>
                  <%
                  if(agentGroupWithCode.isEmpty()){
                	  agentGroupWithCode.put(AgentGroup,agentCode);                	  
                  }else{
                	  if(agentGroupWithCode.containsKey(AgentGroup)){
                		  String str=agentGroupWithCode.get(AgentGroup);
                		  if(str==null ||  (!(str.contains(agentCode)))){
                		  str=str+","+agentCode;
                		  }
                		  agentGroupWithCode.put(AgentGroup,str);
                	  }else{
                		  agentGroupWithCode.put(AgentGroup,agentCode);
                	  }
                  }
                  %>
                  <td class="rec_sum_bg" width="" style="text-align: right; cursor: pointer; background-color: #C4E395" onclick="getGivenReciprocityReport('<%=agentCode%>')">
                  <u><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_Revenue_given%>" /></u>
                  </td>
                  <td class="list-columnmain" width="" style=" text-align: right; background-color: #C4E395">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=OA_Revenue_RECD%>" />
                  </td>
                  <td class="list-columnmain" width="" style="text-align: right; background-color: #C4E395">
                  <fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=DA_Revenue_RECD%>" />
                  </td>
                  <td class="rec_sum_bg" width="" style="text-align: right; cursor: pointer; background-color: #C4E395" onclick="getRecvReciprocityReport('<%=agentCode%>')">
                  <u><fmt:formatNumber type="number" maxFractionDigits="0"
                   groupingUsed="true" value="<%=subtotal_Revenue_recd%>" /></u>
                  </td>
                  </tr>
<%
              }
              %>

              <%
      }
%>
          </table>
          </td>
                    </tr>
          </table></div>
                    </div>
                    </s:form>


