<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<title>Inventory Transfer</title>
<meta name="heading"
	content="Inventory Transfer" />
	<SCRIPT type="text/javascript">
	
	function alertMode(){
		var mode = document.forms['popupSOConformation'].elements['modeMessage'].value;
		if(mode!=''){
			alert('Service Order Not Created for Mode - '+mode+'.');
		}
	}
	function checkShipNumber(mode,ship,variable){
	var result = document.getElementById('shipAndModeList').value;
	var flag=false;
	var searchvar;
	result=result.toLowerCase();
	if(mode.length>5){
		searchvar=mode.substr(0,3)+':'+variable.value.substr(ship.length-2,ship.length);
	}else{
		searchvar=mode+':'+variable.value.substr(ship.length-2,ship.length);
	}
	searchvar=searchvar.toLowerCase();
	var resultarray=result.split(',');
	for(var i=0; i<resultarray.length;i++){
	if(searchvar==resultarray[i]){
	      flag=true;
	      }
	}
	if(!flag){
	alert('Service Order of Sequence No. '+variable.value.substr(0,ship.length-2)+' doesn\'t have Mode '+mode);
	variable.value=ship;
	    }
	}
	
	function InventryTransfer(){
	    document.getElementById('modeSORelation').value;
	    var rowid =document.getElementById('rowid').value.split(',');
	    for(var i=0;i<rowid.length;i++){
	    if(document.getElementById('modeSORelation').value==""){
	      document.getElementById('modeSORelation').value= document.getElementById(rowid[i]).childNodes[1].childNodes[0].value+":"+document.getElementById(rowid[i]).childNodes[3].childNodes[0].value;
	    }else{
	    document.getElementById('modeSORelation').value=document.getElementById('modeSORelation').value+","+ document.getElementById(rowid[i]).childNodes[1].childNodes[0].value+":"+document.getElementById(rowid[i]).childNodes[3].childNodes[0].value;
	    }
	}
	document.forms['popupSOConformation'].action="inventrySOTransfer.html?decorator=popup&popup=true&Quote=${Quote}";
	document.forms['popupSOConformation'].submit();
	}
	function closeWindow(){
	window.close();
	}
	
	function maintainRowId(result){
	if(document.getElementById('rowid').value==""){
	   document.getElementById('rowid').value=result;
	 }else{
	    document.getElementById('rowid').value= document.getElementById('rowid').value+","+result;
	   }
	}		
	function pick() {	
  		parent.window.opener.document.location="surveyDetails.html?cid=${cid}&message=${message}&Quote=${Quote}";
  		window.close();
	}	
	</SCRIPT>
	<SCRIPT type="text/javascript">
   <c:if test="${hitflag=='1'}">
   pick();
   </c:if> 
</script>
</head>
<body>
<s:form action="" name="popupSOConformation">
<s:hidden name="shipAndModeList"  id="shipAndModeList"/>
<s:hidden name="rowid"  id="rowid" value=""/>
<s:hidden name="modeWithOutSO" value="" />
<s:hidden name="modeWithOneSO"/>
<s:hidden name="modeSORelation" id="modeSORelation" value=""/>
<s:hidden name="cid" />
<s:hidden name="message" />
<s:hidden name="modeMessage" value="<%= request.getParameter("modeMessage") %>"/>
</s:form >
<script type="text/javascript">
try{
	alertMode();
	}
	catch(e){}
</script>
	     <div id="otabs" style="margin-top: 10px; margin-bottom:0px;">
		  <ul>
		       <c:if test="${Quote!='y'}">	  			
	  			 <li><a class="current"><span>Survey Data For SO</span></a></li>  			
	  		  </c:if>
	  		  <c:if test="${Quote=='y'}">	  			
	  			 <li><a class="current"><span>Survey Data For Quotes</span></a></li>  			
	  		  </c:if>		   
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
		<div>
<table cellspacing="0" cellpadding="0" border="0" class="table">  	
	  	  
	  	    <tr>
	  	      <th height="5px" class="listwhitetext"><b>Mode</b></th>
	  	      <c:if test="${Quote!='y'}">	  			
	  			<th height="5px" class="listwhitetext"><b>Service Order</b></th>	  			
	  		  </c:if>
	  		  <c:if test="${Quote=='y'}">
	  		  <th height="5px" class="listwhitetext"><b>Quotes</b></th>
	  		  </c:if>
	  		  <th height="5px" class="listwhitetext"><b>City,State,Country</b></th>
	  		</tr>
	  	    <c:forEach var="articlesList" items="${modeShipMap}" >
	  	      <script type="text/javascript">
	  	        maintainRowId('articlelistfor${articlesList.key}');
	  	       </script>
	  	    <tr id="articlelistfor${articlesList.key}">  	       
	  			<td height="5px" class="listwhitetext"><input type="text" class="input-textUpper" readonly="readonly" value="${articlesList.key}"></td>
	  			<td height="5px" class="listwhitetext"><input type="text" class="input-text" value="${articlesList.value.shipNumber}" onchange="checkShipNumber('${articlesList.key}','${articlesList.value.shipNumber}',this);"></td>
	  		    <td height="5px" class="listwhitetext"><input type="text" class="input-textUpper" style="width:200px;" readonly="readonly" value="${articlesList.value.originCity},${articlesList.value.originState},${articlesList.value.originCountry}"></td>
	  		</tr>
	  		</c:forEach>
</table>
</div>
<input type="button"  Class="cssbutton" Style="width:60px; height:25px;margin-left:25px;"  value="OK" onclick="InventryTransfer();"/>		
<input type="button"  Class="cssbutton" Style="width:60px; height:25px;margin-left:25px;"  value="Cancel" onclick="closeWindow();"/>
</body>

