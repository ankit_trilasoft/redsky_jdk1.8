<%@ include file="/common/taglibs.jsp"%> 
<head>   
    <title><fmt:message key="partnerDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerDetail.heading'/>"/>  
    
<style type="text/css">
		h2 {background-color: #FBBFFF}
		
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  

	
	
	function isFloat(targetElement)
	{   var i;
	var s = targetElement.value;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        if (c == ".") {
        
        }else{
        alert("Only numbers are allowed here");
        //alert(targetElement.id);
        document.getElementById(targetElement.id).value='';
        document.getElementById(targetElement.id).select();
        return false;
        }
    }
    }
    return true;
}

var r={
 'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\.'&'\('&'\)'&'\|'&'\['&'\]'&'\,'&'\`'&'\=']/g,
 'quotes':/['\''&'\"']/g,
 'notnumbers':/[^\d]/g
};

function valid(targetElement,w){
 targetElement.value = targetElement.value.replace(r[w],'');
}

function chkSelect()
	{
		if (checkFloat('partnerAddForm','partner.perDiemCost1','Invalid data in Per Diem Cost1 ') == false)
           {
              document.forms['partnerAddForm'].elements['partner.perDiemCost1'].focus();
              return false
           }
           
           if (checkFloat('partnerAddForm','partner.perDiemCost2','Invalid data in Per Diem Cost1 ') == false)
           {
              document.forms['partnerAddForm'].elements['partner.perDiemCost2'].focus();
              return false
           }
	
	}
	
	
	</script>

<script>
function checkdate(clickType){
	
	if(!(clickType == 'save')){
	if ('${autoSavePrompt}' == 'No'){
	document.forms['partnerAddForm'].action = 'saveForwardingDetails!saveOnTabChange.html';
	document.forms['partnerAddForm'].submit();
}else{
	var id1 = document.forms['partnerAddForm'].elements['partner.id'].value;
	var partnerType = document.forms['partnerAddForm'].elements['partnerType'].value;
	
	if (document.forms['partnerAddForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the 'Forwarding Details'");
		if(agree){
			document.forms['partnerAddForm'].action = 'saveForwardingDetails!saveOnTabChange.html';
			document.forms['partnerAddForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.agentdtl'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.vendordtl'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.carrierdtl'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				location.href = 'searchPartner.html?id='+id1+'&partnerType='+partnerType;
				}
		}
		}
	}else{
	if(id1 != ''){
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.agentdtl'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.vendordtl'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.carrierdtl'){
				location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType='+partnerType;
				}
			if(document.forms['partnerAddForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				location.href = 'searchPartner.html?id='+id1+'&partnerType='+partnerType;
				}
	}
	}
}
}
}
function changeStatus(){
	document.forms['partnerAddForm'].elements['formStatus'].value = '1';
}
</script>
</head>

<s:form id="partnerAddForm" name="partnerAddForm" action="saveForwardingDetails" method="post" validate="true">
<div id="Layer1">
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="id" value="<%= request.getParameter("id")%>" />
<s:hidden name="id" value="<%= request.getParameter("id")%>" />
<s:hidden name="partner.id"/>
<s:hidden name="popupval" value="${papam.popup}"/>

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>

<c:if test="${validateFormNav == 'OK' }">
	<c:choose>
	<c:when test="${gotoPageString == 'gototab.agentdtl' }">
		<c:redirect url="/editPartnerAddForm.html?partnerType=${partnerType}&id=${partner.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.vendordtl' }">
		<c:redirect url="/editPartnerAddForm.html?partnerType=${partnerType }&id=${partner.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.carrierdtl' }">
		<c:redirect url="/editPartnerAddForm.html?partnerType=${partnerType }&id=${partner.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.partnerlist' }">
		<c:redirect url="/searchPartner.html?partnerType=${partnerType}"/>
	</c:when>
	<c:otherwise>
	</c:otherwise>
	</c:choose>
</c:if>

<div id="newmnav">
		  <ul>
		  	<c:if test="${partnerType == 'CR'}">
		    <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.carrierdtl');return checkdate('none');"><span>Carrier Details</span></a></li>
		    <!--<li><a href="editPartnerAddForm.html?partnerType=${partnerType }&id=${id}"><span>Carrier Details</span></a></li>
		    --></c:if>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Forwarding Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <c:if test="${partnerType == 'AG'}">
		    <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.agentdtl');return checkdate('none');"><span>Agent Details</span></a></li>
		    <!--<li><a href="editPartnerAddForm.html?partnerType=${partnerType }&id=${id}"><span>Agent Details</span></a></li>
		    --></c:if>
		    <c:if test="${partnerType == 'VN'}">
		    <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.vendordtl');return checkdate('none');"><span>Vendor Details</span></a></li>
		    <!--<li><a href="editPartnerAddForm.html?partnerType=${partnerType }&id=${id}"><span>Vendor Details</span></a></li>
		    --></c:if>
		    
		    
		     <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.partnerlist');return checkdate('none');"><span>Partner List</span></a></li>
		     <!--<li><a href="searchPartner.html?partnerType=${partnerType}"><span>Partner List</span></a></li>
		    
  --></ul>
		</div><div class="spn">&nbsp;</div><br>
<div id="Layer1" onkeydown="changeStatus();">
	<table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr>
		  		<td colspan="3">
					<dl>
					<dt><font size="2">Remarks/Fees....</font></dt>
					</dl>
					</td></tr>
					<tr><td><font size="1" color="black">Per Diem Costs</font></td><td></td><td><font size="1" color="black">Five Lines Of Remarks</font></td></tr>
					<tr>
					<td align="right" class="listwhitetext"><fmt:message key='partner.perDiemFreeDays'/></td>
					<td><s:textfield cssClass="input-text" name="partner.perDiemFreeDays"  size="10" maxlength="11" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')"/></td>
					<td></td>
					<td><s:textfield name="partner.remarks1"  cssClass="text large" maxlength="50"/></td>
					</tr>
					<tr>
					<td align="right" class="listwhitetext"><fmt:message key='partner.perDiemDays2'/></td>
					<td><s:textfield cssClass="input-text" name="partner.perDiemDays2"  size="10" maxlength="11" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')"/></td>
					<td></td>
					<td><s:textfield name="partner.remarks2"  cssClass="text large" maxlength="50"/></td>
					</tr>
					<tr>
					<td align="right" class="listwhitetext"><fmt:message key='partner.perDiemCost1'/></td>
					<td><s:textfield cssClass="input-text" name="partner.perDiemCost1"  size="10" maxlength="7" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this)" onkeyup="valid(this,'special')"/></td>
					<td></td>
					<td><s:textfield name="partner.remarks3"  cssClass="text large" maxlength="50"/></td>
					</tr>
					<tr>
					<td align="right" class="listwhitetext"><fmt:message key='partner.perDiemCost2'/></td>
					<td><s:textfield cssClass="input-text" name="partner.perDiemCost2"  size="10" maxlength="7" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this)" onkeyup="valid(this,'special')"/></td>
					<td></td>
					<td><s:textfield name="partner.remarks4"  cssClass="text large" maxlength="50"/></td>
					</tr>
					<tr>
					<td></td><td></td><td></td>
					<td><s:textfield name="partner.remarks5"  cssClass="text large" maxlength="50"/></td>
					</tr>
					<tr height="9" />
					<%-- <tr><td>Five Lines Of Remarks</td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks1"  cssClass="text large" maxlength="50"/></td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks2"  cssClass="text large" maxlength="50"/></td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks3"  cssClass="text large" maxlength="50"/></td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks4"  cssClass="text large" maxlength="50"/></td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks5"  cssClass="text large" maxlength="50"/></td></tr>
					--%></table>
				
			</td>
		</tr>
	</tbody>
</table>

<table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" key="button.save" onmouseover="return chkSelect();"/>  
        		</td>
       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
        		
       	  	</tr>		  	
		  </tbody>
		  </table>
<s:hidden name="partner.createdOn"/>
<s:hidden name="partner.createdBy"/>
<s:hidden name="partner.updatedOn"/>
<s:hidden name="partner.updatedBy"/>
<s:hidden name="partner.lastName"/>
<s:hidden name="partner.partnerSuffix"/>
<s:hidden name="partner.partnerCode"/>
<s:hidden name="partner.mailingAddress1"/>
<s:hidden name="partner.mailingCity"/>
<s:hidden name="partner.mailingFax"/>
<s:hidden name="partner.mailingAddress2"/>
<s:hidden name="partner.mailingState" />
<s:hidden name="partner.mailingPhone"/>
<s:hidden name="partner.mailingAddress3"/>
<s:hidden name="partner.mailingZip"/>
<s:hidden name="partner.mailingTelex"/>
<s:hidden name="partner.mailingAddress4"/>
<s:hidden name="partner.mailingCountryCode"/>
<s:hidden name="partner.mailingCountry"/>
<s:hidden name="partner.mailingEmail" />
<s:hidden name="partner.terminalAddress1"/>
<s:hidden name="partner.terminalCity"/>
<s:hidden name="partner.terminalFax"/>
<s:hidden name="partner.terminalAddress2"/>
<s:hidden name="partner.terminalState" />
<s:hidden name="partner.terminalPhone"/>
<s:hidden name="partner.terminalAddress3"/>
<s:hidden name="partner.terminalZip"/>
<s:hidden name="partner.terminalTelex"/>
<s:hidden name="partner.terminalAddress4"/>
<s:hidden name="partner.terminalCountryCode"/>
<s:hidden name="partner.terminalCountry"/>
<s:hidden name="partner.terminalEmail" />
<s:hidden name="partner.billingAddress1"/>
<s:hidden name="partner.billingCity"/>
<s:hidden name="partner.billingFax"/>
<s:hidden name="partner.billingAddress2"/>
<s:hidden name="partner.billingState" />
<s:hidden name="partner.billingPhone"/>
<s:hidden name="partner.billingAddress3"/>
<s:hidden name="partner.billingZip"/>
<s:hidden name="partner.billingTelex"/>
<s:hidden name="partner.billingAddress4"/>
<s:hidden name="partner.billingCountryCode"/>
<s:hidden name="partner.billingCountry"/>
<s:hidden name="partner.billingEmail" />
<s:hidden name="partner.sea" />
<s:hidden name="partner.surface" />
<s:hidden name="partner.air" />
<s:hidden name="partner.location1" />
<s:hidden name="partner.location2" />
<s:hidden name="partner.location3" />
<s:hidden name="partner.location4" />
<s:hidden name="partner.acctDefaultJobType" />
<s:hidden name="partner.accountingDefault" />
<s:hidden name="partner.url" />
<s:hidden name="partner.latitude" />
<s:hidden name="partner.longitude" />

<s:hidden name="partner.companyProfile" />
<s:hidden name="partner.yearEstablished" />
<s:hidden name="partner.companyFacilities" />
<s:hidden name="partner.companyCapabilities" />
<s:hidden name="partner.companyDestiantionProfile" />
<s:hidden name="partner.serviceRangeKms" />
<s:hidden name="partner.serviceRangeMiles" />
<s:hidden name="partner.fidiNumber" />
<s:hidden name="partner.OMNINumber" />
<s:hidden name="partner.IAMNumber" />
<s:hidden name="partner.AMSANumber" />
<s:hidden name="partner.WERCNumber" />
<s:hidden name="partner.facilitySizeSQFT" />
<s:hidden name="partner.facilitySizeSQMT" />
<s:hidden name="partner.qualityCertifications" />
<s:hidden name="partner.vanLineAffiliation" />
<s:hidden name="partner.serviceLines" />
</div></s:form>
		  	
		  	
		  	
		  	
		  	
		  	
		  	
		  	
		  	