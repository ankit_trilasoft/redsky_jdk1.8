<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Price Calendar</title>
<meta name="heading" content="Price Calendar" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<head >
<!-- Modified script 12-Dec-2013 --> 
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/fullcalendar/fullcalendar.css'/>" /> 
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/fullcalendar/fullcalendar.print.css'/>" media='print'/> 
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/lib/jquery.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/lib/jquery-ui.custom.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/styles/fullcalendar/fullcalendar.min.js"></script>
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script type="text/javascript">
$(document).ready(function() {
	$('#calendar').fullCalendar({
		header: {
		left: 'prev,next today',
		center: 'title',
		right: ''
		},
	    eventSources: [
	       	        {
	    	            events: [ 
	    	                     {
	    	                         title  : 'Number Of Men Used = N/A\n2 Men Rate = N/A',
	    	                         start  : '2011-01-16',
	    	                         end    : '${lowestCurrentDate}'
	    	                     }
	    	                          
	    				<c:forEach var="entry" items="${dynamicPricingCalenderList}" varStatus="rowCounter">  
	    				 ,
	    	                {
	 		                <fmt:parseNumber var="val" type="number" value="${type2Temp}" />
			                <fmt:parseNumber var="cont" type="number" value="${entry.extraManRate}" />
			                <c:set var="sum" value="${val + val}"/>  
			                <c:if test="${sum != cont}">		    	                
	    						url    : 'javascript:popupwindow("pricingCalendarFullView.html?ajax=1&decorator=simple&popup=true&men2=${entry.menRate2}&men3=${entry.menRate3}&men4=${entry.menRate4}&extraMen=${entry.extraManRate}","","80","80")',
	    	                    title  : 'Number Of Men = ${entry.noOfMen}\n2 Men Rate = ${entry.menRate2}',
	    	                    start  : '${entry.currentDate}'	
		    	            </c:if>
			                <c:if test="${sum == cont}">
		                    title  : 'Number Of Men = ${entry.noOfMen}\n2 Men Rate = ${entry.menRate2}',
		                    start  : '${entry.currentDate}',
			                color : '#F5D3C4'    
			                </c:if>           		    	            	    	              
	    	                }
	    	            </c:forEach>
	    	            
	    	            ]

	    	        }
	    ],
	    eventRender: function(event, element) {
		    var str=event.title;
		    var ss=str.split("\n");
	        element.find(element.find('.fc-event-title').html('<span style="color:#ff0000">'+ss[0]+'</span><br><span style="color:#0F6E0F">'+ss[1]+'</span>'));
	    }
		});
	});

$(document).ready(function() {
	$('#calendar1').fullCalendar({
		header: {
		left: 'prev,next today',
		center: 'title',
		right: ''
		},
	    eventSources: [
	        {
	            events: [ 
	                     {
	                         title  : 'Number Of Men Used = N/A\n2 Men Rate = N/A',
	                         start  : '2011-01-16',
	                         end    : '${lowestCurrentDate1}'
	                     }
	                          
				<c:forEach var="entry" items="${dynamicPricingCalenderList1}" varStatus="rowCounter">  
				 ,
	                {
		                <fmt:parseNumber var="val" type="number" value="${type2Temp}" />
		                <fmt:parseNumber var="cont" type="number" value="${entry.extraManRate}" />
		                <c:set var="sum" value="${val + val}"/>  
		                <c:if test="${sum != cont}">
					  	url    : 'javascript:popupwindow("pricingCalendarFullView.html?ajax=1&decorator=simple&popup=true&men2=${entry.menRate2}&men3=${entry.menRate3}&men4=${entry.menRate4}&extraMen=${entry.extraManRate}","","80","80")',
	                    title  : 'Number Of Men = ${entry.noOfMen}\n2 Men Rate = ${entry.menRate2}',
	                    start  : '${entry.currentDate}'		    
		                </c:if>	 
		                <c:if test="${sum == cont}">
	                    title  : 'Number Of Men = ${entry.noOfMen}\n2 Men Rate = ${entry.menRate2}',
	                    start  : '${entry.currentDate}'	,
	                    color : '#F5D3C4'    	    		                  
		                </c:if>           
	                }
	            </c:forEach>
	            
	            ]

	            
	        },
	        
	    ],

	    eventRender: function(event, element) {
		    var str=event.title;
		    var ss=str.split("\n");
	        element.find(element.find('.fc-event-title').html('<span style="color:#ff0000">'+ss[0]+'</span><br><span style="color:#0F6E0F">'+ss[1]+'</span>'));
	    }
	  
		});
	});

function popupwindow(url, title, w, h) {
    var left = 35;
    var top = 55;
    var w = 250;
    var h = 150;
    var popup;
    popup=window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    popup.focus(); 
}
	</script>	
</head>
<style> <%@ include file="/common/calenderStyle.css"%> 

</style>
<style type="text/css">
 #calendar {
  width: 900px;
  height: 350px;
  margin: 0 auto;
  margin-top: 75px;
  text-align: center;
  font-size: 12px;
  font-family: verdana,arial,helvetica,sans-serif;
  font-color: 'blue';
  } 
   #calendar1 {
  width: 900px;
  height: 350px;
  margin: 0 auto;
  margin-top: 880px;
  text-align: center;
  font-size: 12px;
  font-family: verdana,arial,helvetica,sans-serif;
  font-color: 'blue';
  } 
	h2 {
		background-color: #CCCCCC
	} 
	.subcontent-tab {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;font-weight: bold;
		color: #15428B;text-decoration: none;
		background:url(images/collapsebg.gif) #BCD2EF;
		padding:4px 3px 1px 5px; height:15px;
		width:598px; 
		border:1px solid #99BBE8; 
		border-right:none; 
		border-left:none
	} 
	a.dsphead{
		text-decoration:none;
		color:#000000;
	}
	.dspchar2{
		padding-left:0px;
	}
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}
	.bgblue{
		background:url(images/blue_band.jpg); 
		height: 30px; 
		width:630px; 
		background-repeat: no-repeat;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		font-weight: bold;
		color: #25383c; 
		padding-left: 40px; 
	}
	.fc-state-highlight {
	background: #808080;
	}
	
	.fc-event {
    background-color: #C9E9F2;
    border: 1px solid #3A87AD;
    color: #FFFFFF;
    cursor: default;
    font-size: 0.85em;
}
form { margin-top:-10px;}
</style>

<script type="text/javascript">

function getCalendar(){
	var hubID=document.forms['priceCalendarForm'].elements['dynamicPricingCategoryTypeView'].value;	
	if((hubID!=null)&&(hubID.trim()!='')){
		var url="dynamicPricingCalenderView.html?decorator=popup&popup=true";
		document.forms['priceCalendarForm'].action = url;
		document.forms['priceCalendarForm'].submit();
	}else{
		alert("Kindly select Dynamic Pricing.");
	}
}
function opsCalendarForm(){
	location.href = 'opsCalendar.html?decorator=popup&popup=true';
}

</script>

</head>

<s:form name="priceCalendarForm" id="priceCalendarForm" action="" method="post" validate="true">

<c:set var="FormDateValue" value="dd-NNN-yy"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<fmt:formatDate var="fromFormattedValue" value="${fromDate}" pattern="${displayDateTimeFormat}"/>
<s:hidden name="fromDate" value="${fromFormattedValue}"/>
<s:hidden name="type"/>
<div id="layer1" style="width:810px; margin: 0px auto;">
<div id="otabs" style="margin-left:30px">
	<ul>
		<li><a onclick="opsCalendarForm();" ><span>Ops Calendar</span></a></li>
		<li><a class="current"><span>Price Calendar</span></a></li>
	</ul>
</div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="left" >
<div id="liquid-round" style="width:810px">
<div class="top" style="margin-top: 10px;!margin-top:-4px;"><span></span></div>
<div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0" style="" width="100%">		
	<tr><td height="5px">&nbsp;</td></tr>	
	<tr>		
		<td align="right" class="listwhitetext" style="padding-bottom:5px">Dynamic Pricing</td>
  		<td align="left" class="listwhitetext" style="padding-bottom:5px; padding-left:5px;"><s:select cssClass="list-menu" name="dynamicPricingCategoryTypeView" list="{'Edmonton','Calgary'}" headerKey="" headerValue="" cssStyle="width:150px"/></td>
		<td align="right"><input type="button" class="cssbutton" style="width:105px; height:25px" align="top" value="View Calendar" onclick="getCalendar();"/></td>
	</tr>
</table> 
</div>

<c:if test="${dynamicPricingCategoryTypeView == 'Edmonton'}">
<div style="position: absolute; margin-top: 14%; font-weight: bold; font-size: 13px;">Edmonton - Summer</div>
</c:if>
<c:if test="${dynamicPricingCategoryTypeView == 'Calgary'}">
<div style="position: absolute; margin-top: 14%; font-weight: bold; font-size: 13px;">Calgary - Summer</div>
</c:if>		
<div id="calendar" style="position:absolute">
</div>
<c:if test="${dynamicPricingCategoryTypeView == 'Edmonton'}">
<div style="position: absolute; margin-top: 113%; font-weight: bold; font-size: 13px;">Edmonton - Winter</div>
</c:if>
<c:if test="${dynamicPricingCategoryTypeView == 'Calgary'}">
<div style="position: absolute; margin-top: 113%; font-weight: bold; font-size: 13px;">Calgary - Winter</div>
</c:if>
<div id="calendar1" style="position:absolute">
</div>
<div class="bottom-header" style="width:809px;"><span></span></div>
</div>
</div>  
</div>
<script type="text/javascript">

</script>
</s:form>
