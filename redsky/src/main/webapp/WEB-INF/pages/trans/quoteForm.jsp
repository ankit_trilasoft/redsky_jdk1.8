<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="quoteDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='quoteDetail.heading'/>"/>  
    <link href="<s:url value="/css/main.css"/>" rel="stylesheet" type="text/css"/>
    <style type="text/css">
	h2 {background-color: #CCCCCC}
	fieldset {
			
				padding: 0.0em;
				position: relative;
			    width: 70%;  
			  }
	</style>

	<script type="text/javascript">
		function calculate() {
			var x=((document.forms['quoteForm'].elements['quote.quantity'].value*document.forms['quoteForm'].elements['quote.rate'].value)*(100-document.forms['quoteForm'].elements['quote.discount'].value))/100;
			if(document.forms['quoteForm'].elements['quote.divideOrMultiply'].value=="/") {
				document.forms['quoteForm'].elements['quote.amount'].value= x/document.forms['quoteForm'].elements['quote.per'].value;
			}else if (document.forms['quoteForm'].elements['quote.divideOrMultiply'].value=="*"){
				document.forms['quoteForm'].elements['quote.amount'].value= x*document.forms['quoteForm'].elements['quote.per'].value;
			}
		}
	</script>	

</head>

<s:form id="quoteForm" action="saveQuote" method="post" validate="true"> 
<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="4" align="right"><img width="4" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="customerFiles.html" >Customer
			File</a></td>
			<td width="4" align="left"><img width="4" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Service Orders</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="7" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td width="100" class="detailActiveTabLabel content-tab">Quotations</td>
			<td width="7" align="left"><img width="7" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Surveys</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Notes</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Reports</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Account Policy</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td align="left"></td>
		</tr>
	</tbody>
</table>
<table class="mainDetailTable" cellspacing="1" cellpadding="0"	border="1">
	<tbody>
		<tr>
			<td>
			<table cellspacing="0" cellpadding="3" border="0">
				<tbody>
					<tr>
						<td align="left" class="listwhite"></td>
						<td align="left" class="listwhite"></td>
						<td align="left" class="listwhite"></td>
						<td align="left" class="listwhite"></td>
						<td align="left" class="listwhite"></td>
						<td align="right" class="listwhite"><fmt:message key='quote.quoteNumber'/></td>
						<td><s:textfield key="quote.quoteNumber" required="true" size="11" maxlength="10"/> </td>
						<td><s:textfield key="quote.billed" required="true" size="11" /> </td>
					</tr>
					<tr>
						<td align="right" class="listwhite"><fmt:message key='customerFile.sequenceNumber'/></td>
						<td><s:textfield name="customerFile.sequenceNumber"  size="10"/></td>
						<td align="right" class="listwhite"><fmt:message key='quote.type'/></td>
						<td><s:textfield name="quote.type"  size="10"/></td>
						<td><input type="button" value="Estimate"/></td>
						<td align="right" class="listwhite"><fmt:message key='quote.id'/></td>
						<td><s:textfield key="quote.id" required="true" size="11" maxlength="10"/> </td>
						<td align="left" class="listwhite"></td>
					</tr>
					<tr>
						<td align="right" class="listwhite"><fmt:message key='customerFile.billToCode'/></td>
						<td><s:textfield name="customerFile.billToCode"  size="10"/></td>
						<td colspan="3"><s:textfield name="customerFile.billToName"  size="25"/></td>
						<td align="right" class="listwhite"><fmt:message key='customerFile.contract'/></td>
						<td colspan="2"><s:textfield name="customerFile.contract"  size="20"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhite"><fmt:message key='quote.providedBy'/></td>
						<td><s:textfield name="quote.providedBy"  size="10"/></td>
						<td colspan="3"><s:textfield name="providedBy"  size="25"/></td>
						<td align="right" class="listwhite"><fmt:message key='quote.providedAs'/></td>
						<td colspan="2"><s:select name="quote.providedAs"  list="{'A','B','C','D'}"/></td>
					</tr>
				</tbody>
			</table>
			<fieldset>
				<table cellspacing="0" cellpadding="3" border="0">
					<tbody>
						<tr>
							<td align="center" class="listwhite"></td>
							<td align="center" class="listwhite"><fmt:message key='quote.quantity'/></td>
							<td align="center" class="listwhite"><fmt:message key='quote.item'/></td>
							<td align="center" class="listwhite"><fmt:message key='quote.rate'/></td>
							<td align="center" class="listwhite"><fmt:message key='quote.discount'/></td>
							<td align="center" class="listwhite"><s:select name="quote.divideOrMultiply" list="{'/','*'}" /></td>
							<td align="center" class="listwhite"><fmt:message key='quote.perItem'/></td>
							<td align="center" class="listwhite"><fmt:message key='quote.amount'/></td>
							<td align="center" class="listwhite"></td>
						</tr>
						<tr>
							<td width="4" align="left" class="listwhite"></td>
							<td align="right" class="listwhite"><s:textfield name="quote.quantity" size="5"/></td>
							<td align="right" class="listwhite"><s:textfield name="quote.item" size="8"/></td>
							<td align="right" class="listwhite"><s:textfield name="quote.rate" size="5"/></td>
							<td align="right" class="listwhite"><s:textfield name="quote.discount" size="5"/></td>
							<td align="right" class="listwhite"><s:textfield name="quote.per" size="8"/></td>
							<td align="right" class="listwhite"><s:textfield name="quote.perItem" size="8"/></td>
							<td align="right" class="listwhite"><s:textfield name="quote.amount" size="8"/></td>
							<td align="left" class="listwhite"><input type="button" value="Calc" onClick="calculate();" /></td>
						</tr>
						<tr>
							<td width="4" align="left" class="listwhite"></td>
							<td align="left" class="listwhite"><fmt:message key='quote.description'/></td>
							<td colspan="6"><s:textfield name="quote.description" size="90"/></td>
							<td align="left" class="listwhite"></td>
						</tr>
					</tbody>
				</table>
			</fieldset>
			<table>
				<tbody>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
					</tr>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
					</tr>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
					</tr>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
					</tr>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
					</tr>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
					</tr>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
					</tr>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
					</tr>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
					</tr>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
					</tr>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
						<td align="right" class="listwhite"><fmt:message key='quote.comment1'/></td>
						<td align="left" class="listwhite"><s:textfield name="quote.comment1" size="90"/></td>
					</tr>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
						<td align="right" class="listwhite"><fmt:message key='quote.comment2'/></td>
						<td align="left" class="listwhite"><s:textfield name="quote.comment2" size="90"/></td>
					</tr>
					<tr>
						<td align="left" class="listwhite"><s:label label="" /></td>
						<td align="right" class="listwhite"><fmt:message key="quote.sentWB"/></td>
						<td align="left" class="listwhite"><s:textfield name="quote.sentWB" size="15"/></td>
						<td align="right"><s:submit cssClass="button" method="save" key="button.save"/></td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</div>

</s:form>

<s:form id="cancelForm" action="quotes" method="post" validate="true"> 
<!-- 	<s:hidden name="customerFile.sequenceNumber"/>  
	<s:hidden name="quote.id"/>
	<table>
		<tr>
			<td>
				<s:submit cssClass="button" method="listQuotes" key="button.cancel"/> 
			</td>
			<td>
				<c:if test="${not empty quote.id}">    
        			<s:submit cssClass="button" method="delete" key="button.delete" onclick="return confirmDelete('quote')"/>   
    			</c:if>    
    		</td>
    	</tr>
    </table>    -->
</s:form>

<script type="text/javascript">   
	Form.focusFirstElement($("quoteForm"));   
</script>  