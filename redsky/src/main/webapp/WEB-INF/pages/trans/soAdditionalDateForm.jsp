<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<head> 
<title>Additional Date Form</title> 
<meta name="heading" content="Additional Date Form"/>


<style type="text/css">
.text-area {
border:1px solid #219DD1;
}
span.pagelinks {   
    margin-bottom: -21px; 
}
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/pdfobject.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
  
  <script language="javascript" type="text/javascript">  
    	function completeTimeString() {
		var stime1=document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].value;
		var stime2=document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].value;
		var stime3=document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].value;
		var stime4=document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].value;
		var stime5=document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].value;
		
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].value = "0" + stime2;
			}
		}
		if(stime3.substring(stime3.indexOf(":")+1,stime3.length) == "" || stime3.length==1 || stime3.length==2){
			if(stime3.length==1 || stime3.length==2){
				if(stime3.length==2){
					document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].value = stime3 + ":00";
				}
				if(stime3.length==1){
					document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].value = "0" + stime3 + ":00";
				}
			}else{
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].value = stime3 + "00";
			}
		}else{
			if(stime3.indexOf(":") == -1 && stime3.length==3){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].value = "0" + stime3.substring(0,1) + ":" + stime3.substring(1,stime3.length);
			}
			if(stime3.indexOf(":") == -1 && (stime3.length==4 || stime3.length==5) ){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].value = stime3.substring(0,2) + ":" + stime3.substring(2,4);
			}
			if(stime3.indexOf(":") == 1){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].value = "0" + stime3;
			}
		}
		if(stime4.substring(stime4.indexOf(":")+1,stime4.length) == "" || stime4.length==1 || stime4.length==2){
			if(stime4.length==1 || stime4.length==2){
				if(stime4.length==2){
					document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].value = stime4 + ":00";
				}
				if(stime4.length==1){
					document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].value = "0" + stime4 + ":00";
				}
			}else{
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].value = stime4 + "00";
			}
		}else{
			if(stime4.indexOf(":") == -1 && stime4.length==3){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].value = "0" + stime4.substring(0,1) + ":" + stime4.substring(1,stime4.length);
			}
			if(stime4.indexOf(":") == -1 && (stime4.length==4 || stime4.length==5) ){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].value = stime4.substring(0,2) + ":" + stime4.substring(2,4);
			}
			if(stime4.indexOf(":") == 1){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].value = "0" + stime4;
			}
		}
		if(stime5.substring(stime5.indexOf(":")+1,stime5.length) == "" || stime5.length==1 || stime5.length==2){
			if(stime5.length==1 || stime5.length==2){
				if(stime5.length==2){
					document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].value = stime5 + ":00";
				}
				if(stime5.length==1){
					document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].value = "0" + stime5 + ":00";
				}
			}else{
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].value = stime5 + "00";
			}
		}else{
			if(stime5.indexOf(":") == -1 && stime5.length==3){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].value = "0" + stime5.substring(0,1) + ":" + stime5.substring(1,stime5.length);
			}
			if(stime5.indexOf(":") == -1 && (stime5.length==4 || stime5.length==5) ){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].value = stime5.substring(0,2) + ":" + stime5.substring(2,4);
			}
			if(stime5.indexOf(":") == 1){
				document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].value = "0" + stime5;
			}
		}
	}


    	function IsTimeValid(clickType) {

    	var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
    	var timeStr = document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].value;
    	var matchArray = timeStr.match(timePat);
    	if (matchArray == null) {
    	alert("Time is not in a valid format. Please Use HH:MM Format");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].focus();
    	return false;
    	}
    	hour = matchArray[1];
    	minute = matchArray[2];
    	second = matchArray[4];
    	ampm = matchArray[6];

    	if (second=="") { second = null; }
    	if (ampm=="") { ampm = null }

    	if (hour < 0  || hour > 23) {
    	alert("Hour must be between 0 and 23");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].focus();
    	return false;
    	}
    	if (minute<0 || minute > 59) {
    	alert ("Minute must be between 0 and 59.");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].focus();
    	return false;
    	}
    	if (second != null && (second < 0 || second > 59)) {
    	alert ("Second must be between 0 and 59.");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeFrom'].focus();
    	return false;
    	}

    	var time2Str = document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].value;
    	var matchTime2Array = time2Str.match(timePat);
    	if (matchTime2Array == null) {
    	alert("Time is not in a valid format. Please Use HH:MM Format");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].focus();
    	return false;
    	}
    	hourTime2 = matchTime2Array[1];
    	minuteTime2 = matchTime2Array[2];
    	secondTime2 = matchTime2Array[4];
    	ampmTime2 = matchTime2Array[6];

    	if (hourTime2 < 0  || hourTime2 > 23) {
    	alert("Hour must be between 0 and 23");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].focus();
    	return false;
    	}
    	if (minuteTime2<0 || minuteTime2 > 59) {
    	alert ("Minute must be between 0 and 59.");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].focus();
    	return false;
    	}
    	if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
    	alert ("Second must be between 0 and 59.");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.loadTimeTo'].focus();
    	return false;
    	}


    	var time3Str = document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].value;
    	var matchTime3Array = time3Str.match(timePat);
    	if (matchTime3Array == null) {
    	alert("Time is not in a valid format. Please Use HH:MM Format");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].focus();
    	return false;
    	}
    	hourTime3 = matchTime3Array[1];
    	minuteTime3 = matchTime3Array[2];
    	secondTime3 = matchTime3Array[4];
    	ampmTime3 = matchTime3Array[6];

    	if (hourTime3 < 0  || hourTime3 > 23) {
    	alert("Hour must be between 0 and 23");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].focus();
    	return false;
    	}
    	if (minuteTime3<0 || minuteTime3 > 59) {
    	alert ("Minute must be between 0 and 59.");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].focus();
    	return false;
    	}
    	if (secondTime3 != null && (secondTime3 < 0 || secondTime3 > 59)) {
    	alert ("Second must be between 0 and 59.");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeFrom'].focus();
    	return false;
    	}


    	var time4Str = document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].value;
    	var matchTime4Array = time4Str.match(timePat);
    	if (matchTime4Array == null) {
    	alert("Time is not in a valid format. Please Use HH:MM Format");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].focus();
    	return false;
    	}
    	hourTime4 = matchTime4Array[1];
    	minuteTime4 = matchTime4Array[2];
    	secondTime4 = matchTime4Array[4];
    	ampmTime4 = matchTime4Array[6];

    	if (hourTime4 < 0  || hourTime4 > 23) {
    	alert("Hour must be between 0 and 23");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].focus();
    	return false;
    	}
    	if (minuteTime4<0 || minuteTime4 > 59) {
    	alert ("Minute must be between 0 and 59.");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].focus();
    	return false;
    	}
    	if (secondTime4 != null && (secondTime4 < 0 || secondTime4 > 59)) {
    	alert ("Second must be between 0 and 59.");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.deliveryTimeTo'].focus();

    	return false;
    	}


    	var time5Str = document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].value;
    	var matchTime5Array = time5Str.match(timePat);
    	if (matchTime5Array == null) {
    	alert("Time is not in a valid format. Please Use HH:MM Format");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].focus();
    	return false;
    	}
    	hourTime5 = matchTime5Array[1];
    	minuteTime5 = matchTime5Array[2];
    	secondTime5 = matchTime5Array[4];
    	ampmTime5 = matchTime5Array[6];

    	if (hourTime5 < 0  || hourTime5 > 23) {
    	alert("Hour must be between 0 and 23");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].focus();
    	return false;
    	}
    	if (minuteTime5<0 || minuteTime5 > 59) {
    	alert ("Minute must be between 0 and 59.");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].value='00:00';
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].focus();
    	return false;
    	}
    	if (secondTime5 != null && (secondTime5 < 0 || secondTime5 > 59)) {
    	alert ("Second must be between 0 and 59.");
    	document.forms['soAdditionalDateForm'].elements['soAdditionalDate.possessionTime'].focus();
    	return false;
    	}
    	}
   	function findCustomerOtherSO(position) {
    		 var sid=document.forms['soAdditionalDateForm'].elements['customerFile.id'].value;
    		 var soIdNum=document.forms['soAdditionalDateForm'].elements['serviceOrder.id'].value;
    		 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
    		  ajax_showTooltip(url,position);	
    	}
    function goToUrl(id){
		location.href = "soAdditionalDateDetails.html?sid="+id;
	}
    function goPrev() {
    	var soIdNum =document.forms['soAdditionalDateForm'].elements['serviceOrder.id'].value;
    	var seqNm =document.forms['soAdditionalDateForm'].elements['serviceOrder.sequenceNumber'].value;
    	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
    	 http5.open("GET", url, true); 
         http5.onreadystatechange = handleHttpResponseOtherShip; 
         http5.send(null); 
       }       
     function goNext() {
    	var soIdNum =document.forms['soAdditionalDateForm'].elements['serviceOrder.id'].value;
    	var seqNm =document.forms['soAdditionalDateForm'].elements['serviceOrder.sequenceNumber'].value;
    	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
    	 http5.open("GET", url, true); 
         http5.onreadystatechange = handleHttpResponseOtherShip; 
         http5.send(null); 
       }       
      function handleHttpResponseOtherShip(){
                 if (http5.readyState == 4)
                 {
                   var results = http5.responseText
                   results = results.trim();
                   location.href = 'soAdditionalDateDetails.html?sid='+results;
                 }
           }
      var http5 = getHTTPObject52();
  	function getHTTPObject52()
  {
      var xmlhttp;
      if(window.XMLHttpRequest)
      {
          xmlhttp = new XMLHttpRequest();
      }
      else if (window.ActiveXObject)
      {
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          if (!xmlhttp)
          {
              xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
          }
      }
      return xmlhttp;
  }
  </script>  
    
 </head>	
<s:form id="soAdditionalDateForm" name="soAdditionalDateForm" action="saveSoAdditionalDateForm" method="post" validate="true">
<s:hidden name="soAdditionalDate.id" value="%{soAdditionalDate.id}"/>
<s:hidden name="soAdditionalDate.corpID" />
<s:hidden name="sid" value="${serviceOrder.id}" />
<s:hidden name="customerFile.id" value="%{customerFile.id}"/> 
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <div id="Layer1" style="width:100%"> 
   <div id="newmnav"> 
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			  <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <c:if test="${serviceOrder.job !='RLO'}"> 
			  <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			  </c:if>
			  </c:if>			  
			  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
			  <c:if test="${serviceOrder.job !='RLO'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
              <c:if test="${serviceOrder.job =='INT'}">
                <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
              </c:if>
              </sec-auth:authComponent>
			  	 <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
				<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
				<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </c:if>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.ticketTab">
			  <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
				<c:if test="${serviceOrder.job =='RLO'}"> 
					 <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li> 
				</c:if>
				</c:if>
				</sec-auth:authComponent>	
				<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.claimsTab">
				<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
				<c:if test="${serviceOrder.job =='RLO'}"> 	
					<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
				</c:if>
				</c:if> 
				</sec-auth:authComponent>	
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>			
			<li id="newmnav1" style="background:#FFF"><a onmouseover=""	onclick=""	class="current"><span>Critical Dates</span></a></li>				
			<li><a onmouseover="" onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			<li><a onmouseover="" onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=serviceorder,miscellaneous&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		</ul>
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left" valign="top">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if></tr></table>
		<div class="spn">&nbsp;</div>	
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
</div>
 
<div class="spn">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div> 
   <div class="center-content"> 
 <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px;">
  <tbody>   
  	<tr>
					  	<td>
					  	  	<div  onClick="javascript:animatedcollapse.toggle('origin')" style="margin:0px;" >
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Origin Dates
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
						</div>
					  	<table class="detailTabLabel" border="0"  cellspacing="0"  cellpadding="2" style="margin:0px;padding:0px;margin-bottom:15px;">					  	
					  	<tr><td></td></tr>
					  	<tr>
					  	<td align="right" class="listwhitetext">Out&nbsp;By</td>
								<c:if test="${not empty soAdditionalDate.outByDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.outByDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="outByDate" name="soAdditionalDate.outByDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="outByDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.outByDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="outByDate" name="soAdditionalDate.outByDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="outByDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>						  	
					  	</tr>	
					  	<tr>
					  	<td colspan="3"></td>
					  	<td align="center" class="listwhitetext">No.&nbsp;Of Men</td>
					  	<td align="center" class="listwhitetext" style="padding-right:0px;">BDHrs</td>
					  	<td colspan="4"></td>
					  	<td align="center" class="listwhitetext" style="padding-right:10px;">No.&nbsp;Of Men</td>
					  	<td align="center" class="listwhitetext">BDHrs</td>
					  	</tr>				  	
					  	
					  	<tr>
					  	 	<td align="right" class="listwhitetext">Pre&nbsp;Pack</td>
								<c:if test="${not empty soAdditionalDate.prePackDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.prePackDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="prePackDate" name="soAdditionalDate.prePackDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="prePackDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.prePackDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="prePackDate" name="soAdditionalDate.prePackDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="prePackDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.prePackMen" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.prePackHrs" onblur="" size="10" maxlength="10" /></td>
					  	<td width="100"></td>
					  	
					  	<td align="right" class="listwhitetext">Pack</td>
								<c:if test="${not empty soAdditionalDate.packDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.packDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="packDate" name="soAdditionalDate.packDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td width="20"><img id="packDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.packDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="packDate" name="soAdditionalDate.packDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="packDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.packDayMen" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.packDayHrs" onblur="" size="10" maxlength="10" /></td>
					  	
					  	
					  	</tr>
					  	
					  	<tr>
					  	 	<td align="right" class="listwhitetext">Wrap&nbsp;Day</td>
								<c:if test="${not empty soAdditionalDate.wrapDay}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.wrapDay" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="wrapDay" name="soAdditionalDate.wrapDay" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="wrapDay_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.wrapDay}">
										<td align="left"><s:textfield cssClass="input-text"
											id="wrapDay" name="soAdditionalDate.wrapDay" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="wrapDay_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.wrapDayMen" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.wrapDayHrs" onblur="" size="10" maxlength="10" /></td>
					  	</tr>
					  	
					  						  	
					  	<tr>
					  	 	<td align="right" class="listwhitetext">Pre&nbsp;Load</td>
								<c:if test="${not empty soAdditionalDate.preLoadDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.preLoadDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="preLoadDate" name="soAdditionalDate.preLoadDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="preLoadDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.preLoadDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="preLoadDate" name="soAdditionalDate.preLoadDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="preLoadDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.preLoadMen" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.preLoadHrs" onblur="" size="10" maxlength="10" /></td>
					  	<td width="20"></td>			  						  	
					  	<td align="right" class="listwhitetext">Load</td>
								<c:if test="${not empty soAdditionalDate.loadDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.loadDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="loadDate" name="soAdditionalDate.loadDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="loadDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.loadDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="loadDate" name="soAdditionalDate.loadDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="loadDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.loadDayMen" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.loadDayHrs" onblur="" size="10" maxlength="10" /></td>
					  						  	
					  	</tr>
					 	<tr><td></td></tr>
					  	<tr>
					  	<td align="right" class="listwhitetext" width="100">Load&nbsp;Month</td>
						<td width="" colspan="2"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:65px;" name="soAdditionalDate.loadMonth" onblur="" size="9" maxlength="10" /></td>
						
						<td align="right" class="listwhitetext">Load Week</td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.loadWeek" onblur="" size="10" maxlength="10" /></td>
					  	</tr>
					  	<tr><td></td></tr>
					  	<tr>
					  	<td align="center"  colspan="2" class="listwhitetext" style="width: 100px; padding-left: 8px;">Elevator
					  	<c:set var="loadElevatorAccess" value="false"/>
							<c:if test="${soAdditionalDate.loadElevatorAccess}">
							<c:set var="loadElevatorAccess" value="true"/>
						</c:if>
						<s:checkbox cssStyle="vertical-align:middle;" name="soAdditionalDate.loadElevatorAccess" value="${loadElevatorAccess}" />
						</td>
						<td align="left"  colspan="2" class="listwhitetext" style="padding-left:19px;">Booked
						<c:set var="loadElevatorBooking" value="false"/>
							<c:if test="${soAdditionalDate.loadElevatorBooking}">
							<c:set var="loadElevatorBooking" value="true"/>
						</c:if>
						<s:checkbox cssStyle="vertical-align:middle;" name="soAdditionalDate.loadElevatorBooking" value="${loadElevatorBooking}" />
						</td>
					  	</tr>
					  	<tr><td></td></tr>
					  	<tr>
					  	 	<td align="right" class="listwhitetext">Time
					  	 	<img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
							<td><s:textfield cssClass="input-text" name="soAdditionalDate.loadTimeFrom" id="surveyTime" size="5" maxlength="5"  onchange ="completeTimeString();return IsTimeValid();" tabindex="16"/> </td>	
							<td align="right" class="listwhitetext">To</td>
							<td colspan="2" ><s:textfield cssClass="input-text" name="soAdditionalDate.loadTimeTo" id="surveyTime" size="5" maxlength="5"  onchange ="completeTimeString();return IsTimeValid();" tabindex="16"/>
							<img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" > </td>	
						</tr>
					  	</table>
					  	</td>
					  	</tr>
					  	<td>
					  	<div  onClick="javascript:animatedcollapse.toggle('destination')" style="margin: 0px">
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Destination Dates
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
						</div>
					  	<tr>
					  	<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="2" style="margin:0px;padding:0px;">
					  	<tr><td></td></tr>
					  	<tr>
					  	<td align="right" class="listwhitetext">Possession&nbsp;Date</td>
								<c:if test="${not empty soAdditionalDate.possessionDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.possessionDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="possessionDate" name="soAdditionalDate.possessionDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="possessionDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.possessionDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="possessionDate" name="soAdditionalDate.possessionDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="possessionDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
					  	<td align="right" class="listwhitetext">Time
					  	 	<img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
							<td><s:textfield cssClass="input-text" name="soAdditionalDate.possessionTime" id="surveyTime" size="5" maxlength="5"  onchange ="completeTimeString();return IsTimeValid();" tabindex="16"/>
							<img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" >  </td>
					  	<td align="right" class="listwhitetext">Pref.&nbsp;Delivery</td>
					  	<c:if test="${not empty soAdditionalDate.prefferedDelieveryDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.prefferedDelieveryDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="prefferedDelieveryDate" name="soAdditionalDate.prefferedDelieveryDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="prefferedDelieveryDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.prefferedDelieveryDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="prefferedDelieveryDate" name="soAdditionalDate.prefferedDelieveryDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="prefferedDelieveryDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
					  	</tr>
					  	<tr>
					  	<td colspan="3"></td>
					  	<td align="center" class="listwhitetext">No.&nbsp;Of Men</td>
					  	<td align="center" class="listwhitetext" style="padding-right:15px;">BDHrs</td>
					  	<td colspan="3"></td>
					  	<td align="center" class="listwhitetext" style="padding-right:10px;">No.&nbsp;Of Men</td>
					  	<td align="center" class="listwhitetext">BDHrs</td>
					  	</tr>
					  	<tr>
					  	 	<td align="right" class="listwhitetext">TTG&nbsp;From</td>
								<c:if test="${not empty soAdditionalDate.ttgFromDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.ttgFromDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="ttgFromDate" name="soAdditionalDate.ttgFromDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="ttgFromDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.ttgFromDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="ttgFromDate" name="soAdditionalDate.ttgFromDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="ttgFromDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.ttgFromMen" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.ttgFromHrs" onblur="" size="10" maxlength="10" /></td>
					   	<td align="right" class="listwhitetext" width="112">TTG&nbsp;To</td>
								<c:if test="${not empty soAdditionalDate.ttgToDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.ttgToDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="ttgToDate" name="soAdditionalDate.ttgToDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="ttgToDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.ttgToDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="ttgToDate" name="soAdditionalDate.ttgToDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="ttgToDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.ttgToDateMen" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.ttgToDateHrs" onblur="" size="10" maxlength="10" /></td>
										  	
					  	</tr>					  	
					  		<tr>					  	 		  	
					  	<td align="right" class="listwhitetext">Guaranteed&nbsp;Delivery</td>
								<c:if test="${not empty soAdditionalDate.guaranteedDeliveryDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.guaranteedDeliveryDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="guaranteedDeliveryDate" name="soAdditionalDate.guaranteedDeliveryDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="guaranteedDeliveryDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.guaranteedDeliveryDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="guaranteedDeliveryDate" name="soAdditionalDate.guaranteedDeliveryDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="guaranteedDeliveryDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.guaranteedDeliveryDateMen" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.guaranteedDeliveryDateHrs" onblur="" size="10" maxlength="10" /></td>
							
							
						 	<td align="right" class="listwhitetext">Actual&nbsp;Delivery</td>
								<c:if test="${not empty soAdditionalDate.actualDeliveryDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.actualDeliveryDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="actualDeliveryDate" name="soAdditionalDate.actualDeliveryDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="actualDeliveryDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.actualDeliveryDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="actualDeliveryDate" name="soAdditionalDate.actualDeliveryDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="actualDeliveryDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.actualDeliveryDateMen" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.actualDeliveryDateHrs" onblur="" size="10" maxlength="10" /></td>
										  	
					  	</tr>					  	
					  	<tr>					  	 		  	
					  	<td align="right" class="listwhitetext">Unpack&nbsp;/&nbsp;Unwrap</td>
								<c:if test="${not empty soAdditionalDate.unpackUnwrapDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.unpackUnwrapDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="unpackUnwrapDate" name="soAdditionalDate.unpackUnwrapDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="unpackUnwrapDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.unpackUnwrapDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="unpackUnwrapDate" name="soAdditionalDate.unpackUnwrapDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="unpackUnwrapDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.unpackUnwrapDateMen" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.unpackUnwrapDateHrs" onblur="" size="10" maxlength="10" /></td>
							
							
						 	<td align="right" class="listwhitetext">Setup</td>
								<c:if test="${not empty soAdditionalDate.setUpDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.setUpDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="setUpDate" name="soAdditionalDate.setUpDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="setUpDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.setUpDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="setUpDate" name="soAdditionalDate.setUpDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="setUpDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.setUpDateMen" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.setUpDateHrs" onblur="" size="10" maxlength="10" /></td>
										  	
					  	</tr>					  	
					  		<tr>					 
						 	<td align="right" class="listwhitetext">Debris Removal</td>
								<c:if test="${not empty soAdditionalDate.debrisRemovalDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="soAdditionalDate.debrisRemovalDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="debrisRemovalDate" name="soAdditionalDate.debrisRemovalDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="debrisRemovalDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty soAdditionalDate.debrisRemovalDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="debrisRemovalDate" name="soAdditionalDate.debrisRemovalDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="debrisRemovalDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.debrisRemovalDateMen" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.debrisRemovalDateHrs" onblur="" size="10" maxlength="10" /></td>
										  	
					  	</tr>
					  	  <tr><td></td></tr>
					  	<tr>
					  	<td align="right" class="listwhitetext">Delivery&nbsp;Month</td>
						<td width="" colspan="2"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:65px;" name="soAdditionalDate.deliveryMonth" onblur=""  size="9" maxlength="10" /></td>
						
						<td align="right" class="listwhitetext">Delivery Week</td>
						<td width=""><s:textfield cssClass="input-text" cssStyle="text-align:right" name="soAdditionalDate.deliveryWeek" onblur="" size="10" maxlength="10" /></td>
					  	</tr>
					  	<tr><td></td></tr>
					  	<tr>
					  	<td align="center"  colspan="2" class="listwhitetext" style="width: 100px; padding-left: 8px;">Elevator
					  	<c:set var="deliveryElevatorAccess" value="false"/>
							<c:if test="${soAdditionalDate.deliveryElevatorAccess}">
							<c:set var="deliveryElevatorAccess" value="true"/>
						</c:if>
						<s:checkbox cssStyle="vertical-align:middle;" name="soAdditionalDate.deliveryElevatorAccess" value="${deliveryElevatorAccess}" />
						</td>
						<td align="left"  colspan="2" class="listwhitetext" style="padding-left:19px;">Booked
						<c:set var="deliveryElevatorBooking" value="false"/>
							<c:if test="${soAdditionalDate.deliveryElevatorBooking}">
							<c:set var="deliveryElevatorBooking" value="true"/>
						</c:if>
						<s:checkbox cssStyle="vertical-align:middle;" name="soAdditionalDate.deliveryElevatorBooking" value="${deliveryElevatorBooking}" />
						</td>
					  	</tr>
					  	<tr><td></td></tr>
					  	<tr>
					  	 	<td align="right" class="listwhitetext">Time
					  	 	<img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
							<td><s:textfield cssClass="input-text" name="soAdditionalDate.deliveryTimeFrom" id="surveyTime" size="5" maxlength="5"  onchange ="completeTimeString();return IsTimeValid();" tabindex="16"/> </td>	
							<td align="right" class="listwhitetext">To</td>
							<td colspan="2"><s:textfield cssClass="input-text" name="soAdditionalDate.deliveryTimeTo" id="surveyTime" size="5" maxlength="5"  onchange ="completeTimeString();return IsTimeValid();" tabindex="16"/>
							<img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" > </td>	
						</tr>
					  	</table>
					  	</tr>
					  	
					  	</td>
					  	</tbody>
					  	</table>
					  	</div>
					  	<div class="bottom-header" style="margin-top:45px;"><span></span></div>					  	
					  	</div>
					  	</div>
					  	<table border="0" width="750px;">
				<tbody>
		<tr>
			<td align="left" class="listwhitetext" width="30px"></td>
			<td colspan="5"></td>
		</tr>
		<tr>
			<td align="left" class="listwhitetext" width="30px"></td>
			<td colspan="5"></td>
		</tr>
		<tr>
			<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
		<td valign="top">		
		</td>
		<td style="width:120px">
		<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${soAdditionalDate.createdOn}" 
		pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="soAdditionalDate.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
		<fmt:formatDate value="${soAdditionalDate.createdOn}" pattern="${displayDateTimeFormat}"/>
		</td>		
		<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
		<c:if test="${not empty soAdditionalDate.id}">
			<s:hidden name="soAdditionalDate.createdBy"/>
			<td style="width:85px"><s:label name="createdBy" value="%{soAdditionalDate.createdBy}"/></td>
		</c:if>
		<c:if test="${empty soAdditionalDate.id}">
			<s:hidden name="soAdditionalDate.createdBy" value="${pageContext.request.remoteUser}"/>
			<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
		
		<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
		<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${soAdditionalDate.updatedOn}" 
		pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="soAdditionalDate.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
		<td style="width:120px"><fmt:formatDate value="${soAdditionalDate.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
		<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
		<c:if test="${not empty soAdditionalDate.id}">
			<s:hidden name="soAdditionalDate.updatedBy"/>
			<td style="width:85px"><s:label name="updatedBy" value="%{soAdditionalDate.updatedBy}"/></td>
		</c:if>
		<c:if test="${empty soAdditionalDate.id}">
			<s:hidden name="soAdditionalDate.updatedBy" value="${pageContext.request.remoteUser}"/>
			<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
		</tr>
	</tbody>
</table>
<table class="detailTabLabel" border="0">
		<tbody> 	
			<tr>
				<td align="left" height="3px"></td>
			</tr>	  	
			<tr>
		       	<td><s:submit cssClass="cssbutton" method="" id="saveButton" key="button.save" cssStyle="width:60px;" tabindex="38"/></td>
		       	<td align="right"><s:reset cssClass="cssbutton1" type="button" key="Reset"/></td>
			</tr>		  	
		</tbody>
	</table>
					  	
					  	

</s:form>

<script type="text/javascript"> 
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>