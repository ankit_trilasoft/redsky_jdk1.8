

package com.trilasoft.app.service.impl;

import com.trilasoft.app.dao.BookStorageDao;
import com.trilasoft.app.model.BookStorage;   
import com.trilasoft.app.service.BookStorageManager;
import org.appfuse.service.impl.GenericManagerImpl;   
   
import java.util.List;   
  
public class BookStorageManagerImpl extends GenericManagerImpl<BookStorage, Long> implements BookStorageManager {   
	BookStorageDao bookStorageDao;   
  
    public BookStorageManagerImpl(BookStorageDao bookStorageDao) {   
        super(bookStorageDao);   
        this.bookStorageDao = bookStorageDao;   
    }   
  
    public List<BookStorage> findByLocation(String locationId) {   
        return bookStorageDao.findByLocation(locationId);   
    }
    
    public List findMaximum(){
    	return bookStorageDao.findMaximum();
    }
    
    public List getStorageList(String ticket , String corpID){
    	return bookStorageDao.getStorageList(ticket, corpID);
    }

	public List getStorageLibraryList(String ticket, String corpID) {
		return bookStorageDao.getStorageLibraryList(ticket, corpID);
	}
}

