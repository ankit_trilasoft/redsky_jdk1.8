// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `authorizedCreditNote` BIT(1) NULL DEFAULT b'0'  AFTER `managerApprovalName` ;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('ServiceOrder','serviceType' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');

ALTER TABLE `redsky`.`dspdetails`  
ADD COLUMN TEN_propertyName varchar(82),
ADD COLUMN TEN_city varchar(82),
ADD COLUMN TEN_zipCode varchar(10),
ADD COLUMN TEN_addressLine1 varchar(82),
ADD COLUMN TEN_addressLine2 varchar(82),
ADD COLUMN TEN_state varchar(82),
ADD COLUMN TEN_country varchar(82),
ADD COLUMN TEN_leasedBy varchar(82),
ADD COLUMN TEN_rentCurrency varchar(3),
ADD COLUMN TEN_rentAmount decimal(19,2),
ADD COLUMN TEN_rentAllowance decimal(19,2),
ADD COLUMN TEN_allowanceCurrency varchar(3),
ADD COLUMN TEN_utilitiesIncluded varchar(5),
ADD COLUMN TEN_followUpNeeded varchar(5),
ADD COLUMN TEN_termOfNotice varchar(5),
ADD COLUMN TEN_rentPaidTo varchar(82),
ADD COLUMN TEN_rentalComment varchar(5000),
ADD COLUMN TEN_rentalIncreaseDate datetime default null,
ADD COLUMN TEN_checkInMoveIn datetime default null,
ADD COLUMN TEN_preCheckOut datetime default null,
ADD COLUMN TEN_checkOutMoveOut datetime default null,
ADD COLUMN TEN_swiftCode varchar(30),
ADD COLUMN TEN_paymentDescription varchar(1000),
ADD COLUMN TEN_allowanceAmountUtility decimal(19,2),
ADD COLUMN TEN_vendorNameUtility varchar(82),
ADD COLUMN TEN_vendorRefUtility varchar(30),
ADD COLUMN TEN_actualCostUtility decimal(19,2),
ADD COLUMN TEN_allowanceCurrencyUtility varchar(3),
ADD COLUMN TEN_billingCycleUtility varchar(3),
ADD COLUMN TEN_billingCycleMonthUtility varchar(30),
ADD COLUMN TEN_exceptionComments varchar(5000),
ADD COLUMN TEN_depositCurrency varchar(3),
ADD COLUMN TEN_depositReturned varchar(5),
ADD COLUMN TEN_depositAmount decimal(19,2),
ADD COLUMN TEN_depositReturnedAmount decimal(19,2),
ADD COLUMN TEN_depositPaidBy varchar(82),
ADD COLUMN TEN_depositReturnedCurrency varchar(3),
ADD COLUMN TEN_depositComment varchar(5000),
ADD COLUMN TEN_refundableTo varchar(82),
ADD COLUMN `TEN_Gas_Water` bit(1) DEFAULT b'0' ,
ADD COLUMN `TEN_Gas_Electricity` bit(1) DEFAULT b'0' ,
ADD COLUMN `TEN_TV_Internet_Phone` bit(1) DEFAULT b'0' ,
ADD COLUMN `TEN_mobilePhone` bit(1) DEFAULT b'0' ,
ADD COLUMN `TEN_furnitureRental` bit(1) DEFAULT b'0' ,
ADD COLUMN `TEN_cleaningServices` bit(1) DEFAULT b'0' ,
ADD COLUMN `TEN_parkingPermit` bit(1) DEFAULT b'0' ,
ADD COLUMN `TEN_communityTax` bit(1) DEFAULT b'0' ,
ADD COLUMN `TEN_insurance` bit(1) DEFAULT b'0' ,
ADD COLUMN `TEN_gardenMaintenance` bit(1) DEFAULT b'0' ,
ADD COLUMN `TEN_toBePaidBy` VARCHAR(82) ,
ADD COLUMN `TEN_directDebit` bit(1) DEFAULT b'0' ,
ADD COLUMN `TEN_BIC_SWIFT` VARCHAR(45) ,
ADD COLUMN `TEN_IBAN_BankAccountNumber` VARCHAR(45) ,
ADD COLUMN `TEN_description` VARCHAR(1000) ,
ADD COLUMN `TEN_assigneeContributionCurrency` VARCHAR(3),
ADD COLUMN `TEN_assigneeContributionAmount` DECIMAL(19,2) DEFAULT 0 ;

INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('VOER','RELOTENANCYPROPERTY',50,true,now(),'VOER_SETUP',now(),'VOER_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('VOER', 'Assignee', 'Assignee', 50, 'RELOTENANCYPROPERTY','en',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('VOER', 'Account', 'Account', 50, 'RELOTENANCYPROPERTY','en',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('VOER', 'Property Man', 'Property Man. Comp.', 50, 'RELOTENANCYPROPERTY','en',now(),'VOER_SETUP',now(),'VOER_SETUP');

ALTER TABLE `redsky`.`dspdetails` CHANGE COLUMN `TEN_leasedBy` `TEN_leasedBy` VARCHAR(50) NULL DEFAULT NULL  , 
                                                            CHANGE COLUMN `TEN_depositPaidBy` `TEN_depositPaidBy` VARCHAR(50) NULL DEFAULT NULL  , 
                                                            CHANGE COLUMN `TEN_refundableTo` `TEN_refundableTo` VARCHAR(50) NULL DEFAULT NULL  ;
                                                            

                                                         
                                                            
INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('VOER','RELOTENANCYTERMOFNOTICE',5,true,now(),'VOER_SETUP',now(),'VOER_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('VOER', '1', '1', 5, 'RELOTENANCYTERMOFNOTICE','en',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('VOER', '2', '2', 5, 'RELOTENANCYTERMOFNOTICE','en',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('VOER', '3', '3', 5, 'RELOTENANCYTERMOFNOTICE','en',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('VOER', '4', '4', 5, 'RELOTENANCYTERMOFNOTICE','en',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('VOER', '5', '5', 5, 'RELOTENANCYTERMOFNOTICE','en',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('VOER', '6', '6', 5, 'RELOTENANCYTERMOFNOTICE','en',now(),'VOER_SETUP',now(),'VOER_SETUP');


INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('VOER','RELOTENANCYBILLINGCYCLE',3,true,now(),'VOER_SETUP',now(),'VOER_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('VOER', '1', '1', 3, 'RELOTENANCYBILLINGCYCLE','en',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('VOER', '3', '3', 3, 'RELOTENANCYBILLINGCYCLE','en',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('VOER', '6', '6', 3, 'RELOTENANCYBILLINGCYCLE','en',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('VOER', '12', '12', 3, 'RELOTENANCYBILLINGCYCLE','en',now(),'VOER_SETUP',now(),'VOER_SETUP');

/* Move to below give procedures and trigger on prod . 
file path is 
-- procedure
Project Explorer=>sql=>copy_charges.sql
Project Explorer=>sql=>copy_countrydeviation.sql
Project Explorer=>sql=>copy_rategrid.sql
--trigger
Project Explorer=>sql=>dspdetails_Trigger.sql



*/



