package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="truckdrivers")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class TruckDrivers extends BaseObject implements ListLinkData{
	private String corpID;
	private Long id;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Boolean primaryDriver;
	private String truckNumber;
	private String partnerCode;
	private String driverName;
	private String driverCode;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("createdBy", createdBy).append("updatedBy",
				updatedBy).append("createdOn", createdOn).append("updatedOn",
				updatedOn).append("primaryDriver", primaryDriver).append(
				"truckNumber", truckNumber).append("partnerCode", partnerCode)
				.append("driverName", driverName).append("driverCode",
						driverCode).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof TruckDrivers))
			return false;
		TruckDrivers castOther = (TruckDrivers) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(createdBy, castOther.createdBy).append(
				updatedBy, castOther.updatedBy).append(createdOn,
				castOther.createdOn).append(updatedOn, castOther.updatedOn)
				.append(primaryDriver, castOther.primaryDriver).append(
						truckNumber, castOther.truckNumber).append(partnerCode,
						castOther.partnerCode).append(driverName,
						castOther.driverName).append(driverCode,
						castOther.driverCode).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).append(primaryDriver).append(truckNumber)
				.append(partnerCode).append(driverName).append(driverCode)
				.toHashCode();
	}
	/**
	 * @return the corpID
	 */
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	/**
	 * @param corpID the corpID to set
	 */
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	/**
	 * @return the createdBy
	 */
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the partnerCode
	 */
	@Column(length=15)
	public String getPartnerCode() {
		return partnerCode;
	}
	/**
	 * @param partnerCode the partnerCode to set
	 */
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	/**
	 * @return the truckNumber
	 */
	@Column(length=20)
	public String getTruckNumber() {
		return truckNumber;
	}
	/**
	 * @param truckNumber the truckNumber to set
	 */
	public void setTruckNumber(String truckNumber) {
		this.truckNumber = truckNumber;
	}
	/**
	 * @return the updatedBy
	 */
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the updatedOn
	 */
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the driverCode
	 */
	@Column(length = 8)
	public String getDriverCode() {
		return driverCode;
	}
	/**
	 * @param driverCode the driverCode to set
	 */
	public void setDriverCode(String driverCode) {
		this.driverCode = driverCode;
	}
	/**
	 * @return the driverName
	 */
	@Column
	public String getDriverName() {
		return driverName;
	}
	/**
	 * @param driverName the driverName to set
	 */
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	@Transient
	public String getListCode() {
		// TODO Auto-generated method stub
		return driverCode;
	}
	@Transient
	public String getListDescription() {
		String lName = this.driverName.replaceAll("&", "%26");
		lName = lName.replaceAll("#", "%23");
		return lName;
	}
	@Transient
	public String getListEigthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListFifthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListFourthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListNinthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListSecondDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListSeventhDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListSixthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListTenthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListThirdDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * @return the primaryDriver
	 */
	@Column
	public Boolean getPrimaryDriver() {
		return primaryDriver;
	}
	/**
	 * @param primaryDriver the primaryDriver to set
	 */
	public void setPrimaryDriver(Boolean primaryDriver) {
		this.primaryDriver = primaryDriver;
	}
}
