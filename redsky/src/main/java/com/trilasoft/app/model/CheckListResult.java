package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="checklistresult")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class CheckListResult extends BaseObject{
	
	private Long id;
	private Long checkListId;
	private Long resultId;
	private String corpID;
	private Long duration;
	private String owner;
	private String messageDisplayed;
	private String url;
	private String shipper;
	private String resultNumber;
	private String checkResultForDel;
	private Long toDoResultId;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String docType;
	private String ruleNumber;
	private String rolelist;
	private String checkedUserName;
	private String billToName;
	private String emailNotification;
	private String manualEmail;
	private Boolean isNotesAdded;
	private String noteType;
	private String noteSubType;
	private String billToCode;;
	private String bookingAgentCode;
	private Boolean isAccount = new Boolean(false);

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("checkListId", checkListId)
				.append("resultId", resultId).append("corpID", corpID)
				.append("duration", duration).append("owner", owner)
				.append("messageDisplayed", messageDisplayed)
				.append("url", url).append("shipper", shipper)
				.append("resultNumber", resultNumber)
				.append("checkResultForDel", checkResultForDel)
				.append("toDoResultId", toDoResultId)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("docType", docType).append("ruleNumber", ruleNumber)
				.append("rolelist", rolelist)
				.append("checkedUserName", checkedUserName)
				.append("billToName", billToName)
				.append("emailNotification", emailNotification)
				.append("manualEmail", manualEmail)
				.append("isNotesAdded", isNotesAdded)
				.append("noteType", noteType)
				.append("noteSubType", noteSubType)
				.append("billToCode", billToCode)
				.append("bookingAgentCode", bookingAgentCode)
				.append("isAccount", isAccount)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CheckListResult))
			return false;
		CheckListResult castOther = (CheckListResult) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(checkListId, castOther.checkListId)
				.append(resultId, castOther.resultId)
				.append(corpID, castOther.corpID)
				.append(duration, castOther.duration)
				.append(owner, castOther.owner)
				.append(messageDisplayed, castOther.messageDisplayed)
				.append(url, castOther.url).append(shipper, castOther.shipper)
				.append(resultNumber, castOther.resultNumber)
				.append(checkResultForDel, castOther.checkResultForDel)
				.append(toDoResultId, castOther.toDoResultId)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(docType, castOther.docType)
				.append(ruleNumber, castOther.ruleNumber)
				.append(rolelist, castOther.rolelist)
				.append(checkedUserName, castOther.checkedUserName)
				.append(billToName, castOther.billToName)
				.append(emailNotification, castOther.emailNotification)
				.append(manualEmail, castOther.manualEmail)
				.append(isNotesAdded, castOther.isNotesAdded)
				.append(noteType, castOther.noteType)
				.append(noteSubType, castOther.noteSubType)
				.append(billToCode, castOther.billToCode)
				.append(bookingAgentCode, castOther.bookingAgentCode)
				.append(isAccount, castOther.isAccount)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(checkListId)
				.append(resultId).append(corpID).append(duration).append(owner)
				.append(messageDisplayed).append(url).append(shipper)
				.append(resultNumber).append(checkResultForDel)
				.append(toDoResultId).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(docType)
				.append(ruleNumber).append(rolelist).append(checkedUserName)
				.append(billToName).append(emailNotification)
				.append(manualEmail).append(isNotesAdded).append(noteType)
				.append(noteSubType).append(billToCode).append(bookingAgentCode)
				.append(isAccount)
				.toHashCode();
	}
	@Column
	public Long getCheckListId() {
		return checkListId;
	}
	public void setCheckListId(Long checkListId) {
		this.checkListId = checkListId;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getMessageDisplayed() {
		return messageDisplayed;
	}
	public void setMessageDisplayed(String messageDisplayed) {
		this.messageDisplayed = messageDisplayed;
	}
	@Column
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Column
	public Long getResultId() {
		return resultId;
	}
	public void setResultId(Long resultId) {
		this.resultId = resultId;
	}
	@Column
	public String getShipper() {
		return shipper;
	}
	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	public String getResultNumber() {
		return resultNumber;
	}
	public void setResultNumber(String resultNumber) {
		this.resultNumber = resultNumber;
	}
	public String getCheckResultForDel() {
		return checkResultForDel;
	}
	public void setCheckResultForDel(String checkResultForDel) {
		this.checkResultForDel = checkResultForDel;
	}
	public Long getToDoResultId() {
		return toDoResultId;
	}
	public void setToDoResultId(Long toDoResultId) {
		this.toDoResultId = toDoResultId;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getRuleNumber() {
		return ruleNumber;
	}
	public void setRuleNumber(String ruleNumber) {
		this.ruleNumber = ruleNumber;
	}
	public String getRolelist() {
		return rolelist;
	}
	public void setRolelist(String rolelist) {
		this.rolelist = rolelist;
	}
	public String getCheckedUserName() {
		return checkedUserName;
	}
	public void setCheckedUserName(String checkedUserName) {
		this.checkedUserName = checkedUserName;
	}
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	public String getEmailNotification() {
		return emailNotification;
	}
	public void setEmailNotification(String emailNotification) {
		this.emailNotification = emailNotification;
	}
	public String getManualEmail() {
		return manualEmail;
	}
	public void setManualEmail(String manualEmail) {
		this.manualEmail = manualEmail;
	}
	public Boolean getIsNotesAdded() {
		return isNotesAdded;
	}
	public void setIsNotesAdded(Boolean isNotesAdded) {
		this.isNotesAdded = isNotesAdded;
	}
	public String getNoteType() {
		return noteType;
	}
	public void setNoteType(String noteType) {
		this.noteType = noteType;
	}
	public String getNoteSubType() {
		return noteSubType;
	}
	public void setNoteSubType(String noteSubType) {
		this.noteSubType = noteSubType;
	}
	public String getBillToCode() {
		return billToCode;
	}
	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	public String getBookingAgentCode() {
		return bookingAgentCode;
	}
	public void setBookingAgentCode(String bookingAgentCode) {
		this.bookingAgentCode = bookingAgentCode;
	}
	public Boolean getIsAccount() {
		return isAccount;
	}
	public void setIsAccount(Boolean isAccount) {
		this.isAccount = isAccount;
	}
	
	
	
	
}
