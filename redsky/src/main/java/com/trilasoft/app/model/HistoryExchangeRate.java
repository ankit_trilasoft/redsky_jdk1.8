/*package com.trilasoft.app.model;

public class HistoryExchangeRate {

}*/



package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="history_exchangerate")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class HistoryExchangeRate extends BaseObject{
	
	private String corpID;
	private Long id;	
	private String currency;
	private String baseCurrency;
	private BigDecimal currencyBaseRate= new BigDecimal(0);
	private BigDecimal baseCurrencyRate= new BigDecimal(0);	
	private Date valueDate;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private BigDecimal officialRate= new BigDecimal(0);
	private BigDecimal marginApplied= new BigDecimal(0);
	private String manualUpdate;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("currency", currency)
				.append("baseCurrency", baseCurrency)
				.append("currencyBaseRate", currencyBaseRate)
				.append("baseCurrencyRate", baseCurrencyRate)
				.append("valueDate", valueDate).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("officialRate", officialRate)
				.append("marginApplied", marginApplied)
				.append("manualUpdate", manualUpdate).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof HistoryExchangeRate))
			return false;
		HistoryExchangeRate castOther = (HistoryExchangeRate) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(id, castOther.id).append(currency, castOther.currency)
				.append(baseCurrency, castOther.baseCurrency)
				.append(currencyBaseRate, castOther.currencyBaseRate)
				.append(baseCurrencyRate, castOther.baseCurrencyRate)
				.append(valueDate, castOther.valueDate)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(officialRate, castOther.officialRate)
				.append(marginApplied, castOther.marginApplied)
				.append(manualUpdate, castOther.manualUpdate).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(currency)
				.append(baseCurrency).append(currencyBaseRate)
				.append(baseCurrencyRate).append(valueDate).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(officialRate).append(marginApplied)
				.append(manualUpdate).toHashCode();
	}
	
	@Column(length=25)
	public BigDecimal getBaseCurrencyRate() {
		return baseCurrencyRate;
	}
	public void setBaseCurrencyRate(BigDecimal baseCurrencyRate) {
		this.baseCurrencyRate = baseCurrencyRate;
	}
	@Column(length=20)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length=25)
	public BigDecimal getCurrencyBaseRate() {
		return currencyBaseRate;
	}
	public void setCurrencyBaseRate(BigDecimal currencyBaseRate) {
		this.currencyBaseRate = currencyBaseRate;
	}
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	@Column(length=20)
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@Column(length=20)
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	@Column(length=20)
	public BigDecimal getOfficialRate() {
		return officialRate;
	}
	public void setOfficialRate(BigDecimal officialRate) {
		this.officialRate = officialRate;
	}
	@Column(length=20)
	public BigDecimal getMarginApplied() {
		return marginApplied;
	}
	public void setMarginApplied(BigDecimal marginApplied) {
		this.marginApplied = marginApplied;
	}
	public String getManualUpdate() {
		return manualUpdate;
	}
	public void setManualUpdate(String manualUpdate) {
		this.manualUpdate = manualUpdate;
	}
	
	
	

	
}
