package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "commissionstructure")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class CommissionStructure extends BaseObject {
	private Long id;

	private BigDecimal  grossMarginFrom = new BigDecimal("0.00");
	
	private BigDecimal  grossMarginTo = new BigDecimal("0.00");
	
	private BigDecimal  salesBa = new BigDecimal("0.00");
	
	private BigDecimal  backOffice = new BigDecimal("0.00");
	
	private BigDecimal  forwarding = new BigDecimal("0.00");	

	public String contractName;
	
	public String chargeCode;
	
	private String corpID;

	private Date createdOn;
	
	private String createdBy;
	
	private Date updatedOn;
	
	private String updatedBy;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("grossMarginFrom", grossMarginFrom)
				.append("grossMarginTo", grossMarginTo)
				.append("salesBa", salesBa).append("backOffice", backOffice)
				.append("forwarding", forwarding)
				.append("contractName", contractName)
				.append("chargeCode",chargeCode).append("corpID", corpID)
				.append("createdOn", createdOn).append("createdBy", createdBy)
				.append("updatedOn", updatedOn).append("updatedBy", updatedBy)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CommissionStructure))
			return false;
		CommissionStructure castOther = (CommissionStructure) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(grossMarginFrom, castOther.grossMarginFrom)
				.append(grossMarginTo, castOther.grossMarginTo)
				.append(salesBa, castOther.salesBa)
				.append(backOffice, castOther.backOffice)
				.append(forwarding, castOther.forwarding)
				.append(contractName, castOther.contractName)
				.append(chargeCode, castOther.chargeCode)
				.append(corpID, castOther.corpID)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(grossMarginFrom)
				.append(grossMarginTo).append(salesBa).append(backOffice)
				.append(forwarding).append(contractName).append(chargeCode).append(corpID)
				.append(createdOn).append(createdBy).append(updatedOn)
				.append(updatedBy).toHashCode();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public BigDecimal getGrossMarginFrom() {
		return grossMarginFrom;
	}

	public void setGrossMarginFrom(BigDecimal grossMarginFrom) {
		this.grossMarginFrom = grossMarginFrom;
	}
	@Column
	public BigDecimal getGrossMarginTo() {
		return grossMarginTo;
	}

	public void setGrossMarginTo(BigDecimal grossMarginTo) {
		this.grossMarginTo = grossMarginTo;
	}
	@Column
	public BigDecimal getSalesBa() {
		return salesBa;
	}

	public void setSalesBa(BigDecimal salesBa) {
		this.salesBa = salesBa;
	}
	@Column
	public BigDecimal getBackOffice() {
		return backOffice;
	}

	public void setBackOffice(BigDecimal backOffice) {
		this.backOffice = backOffice;
	}
	@Column
	public BigDecimal getForwarding() {
		return forwarding;
	}

	public void setForwarding(BigDecimal forwarding) {
		this.forwarding = forwarding;
	}
	@Column(length = 50)
	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	
	
}
