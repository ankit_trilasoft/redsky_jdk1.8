package com.trilasoft.app.dao.hibernate;

import java.util.List;

import com.trilasoft.app.model.TruckTrailers;
import com.trilasoft.app.dao.TruckTrailersDao;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
public class TruckTrailersDaoHibernate extends GenericDaoHibernate<TruckTrailers,Long> implements TruckTrailersDao{

	public TruckTrailersDaoHibernate(){
		super(TruckTrailers.class);
		}
	public List getPrimaryFlagStatus(String truckNumber, String corpId){
		return getHibernateTemplate().find("select id,primaryTrailer from TruckTrailers where primaryTrailer=true and corpID='"+corpId+"' and truckNumber=?",truckNumber);
	}
	public List  findTruckTrailersList(String truckNumber,String corpId){
		return getHibernateTemplate().find("from TruckTrailers where corpID='"+corpId+"' and truckNumber=?" ,truckNumber);
	}
	public List trailersPopup(){
		return getHibernateTemplate().find("from Truck where type='TT' and truckStatus ='A' ");
	}
	public List searchTrailerPopupList(String truckNumber,String ownerPayTo, String corpId){
		return getHibernateTemplate().find("from Truck where type='TT' and truckStatus ='A' and corpID like '"+corpId+"%'and localNumber like '"+truckNumber+"%' and ownerPayTo like '"+ownerPayTo+"%'");
	}
}
