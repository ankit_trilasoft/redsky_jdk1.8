package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.InventoryPathDao;
import com.trilasoft.app.model.InventoryPath;
import com.trilasoft.app.service.InventoryPathManager;

public class InventoryPathManagerImpl extends GenericManagerImpl<InventoryPath,Long> implements InventoryPathManager{
	InventoryPathDao inventoryPathDao;
	public InventoryPathManagerImpl(InventoryPathDao inventoryPathDao) {
		super(inventoryPathDao);
		this.inventoryPathDao=inventoryPathDao;
	}
	public List getListByInventoryId(Long id) {
		return inventoryPathDao.getListByInventoryId(id);
	}
	public  List getListByAccessInfoId(Long id,String type){
		return inventoryPathDao.getListByAccessInfoId(id, type);
	   }
	public List getInventoryPackingImg(Long id){
		return inventoryPathDao.getInventoryPackingImg(id);
	}
	public void deleteAllWorkTicketInventory(String field, String corpID,Long fieldVal){
		 inventoryPathDao.deleteAllWorkTicketInventory(field, corpID, fieldVal);
	}
	public List getListByLocId(Long id, String fieldName){
		return inventoryPathDao.getListByLocId(id,fieldName);
	}
	public void deleteLInkedImgs(Long id){
		inventoryPathDao.deleteLInkedImgs(id);
	}
}
