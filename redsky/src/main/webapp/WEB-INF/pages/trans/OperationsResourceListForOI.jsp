<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>
var http204 = getHTTPObject();
var http203 = getHTTPObject3();
var http255 = getHTTPObject();
var http201 = getHTTPObject();
var http207 = getHTTPObject();
function applyImage(){
     var workorderFlag="";
    <c:forEach var="entry" items="${operationResourceListForOI}">
     var id="scope${entry.id}";
     if(workorderFlag=="" || workorderFlag != '${entry.workorder}' )
         {
         workorderFlag="${entry.workorder}";
        
      if(${entry.scopeOfWorkOrder != null && entry.scopeOfWorkOrder != ''})
         {
         document.getElementById(id).src = 'images/externalScope.png'; 
         }else{
             document.getElementById(id).src = 'images/externalScope.png';
         } }
</c:forEach> 
}
applyImage();

function removeZero(id){
    var quantityValue= document.getElementById("resourceQuantity"+id).value
    if(quantityValue=='0' || quantityValue=='0.0')    {
        document.getElementById("resourceQuantity"+id).value='';
    }}
    
function validateWorkOrder(id){
    var workOrder=document.getElementById("workOrderOI"+id).value;
    if(workOrder==''){
        alert("Please enter WO# "); 
    }
}

function validateWorkTicketStatus(id,value,target,ticket,sessionCorpID,shipNumber,checkFlag){
    var estQty = 0;
    try{
        estQty=target.value;
        var letters = /[^0-9]/g;  
        var decimal=  /^[-+]?[0-9]+\.[0-9]+$/;   
         if( (target.value.match(letters) != null) && (target.value.match(decimal) == null) && checkFlag)
         {
             alert("Please Enter Valid Value")
             target.value = value;
             return false;
             }      
    }catch(e){
        estQty=parseInt(target);
    }   
    
    if(ticket != null && ticket != ""){
    showOrHide(0); 
     var url = "statusOfWorkTicket.html?ajax=1&decorator=simple&popup=true&id="+id+"&ticket="+ticket+"&sessionCorpID="+sessionCorpID;
     http204.open("GET", url, true); 
     http204.onreadystatechange = function() {handleHttpResponse204(target.id,id,value,shipNumber,ticket,target,checkFlag)};
     http204.send(null);
        }else{
            if(checkFlag){
            var estQty =document.getElementById('resourceQuantity'+id).value ;
            var estHour1 = "resourceEstHour";
            var estimateQty1 = "estimateQuantity"; 
            var estimateQtyId = estimateQty1.concat(id);
            var estHourId = estHour1.concat(id);
            var estHour = document.getElementById(estHourId).value;
            document.getElementById(estimateQtyId).value = (estHour * estQty);
            var estimateExpenseId = "estimateExpense";
            var estimateRevenueId = "estimateRevenue";
           var estimateExpenseId1 = estimateExpenseId.concat(id);
            var estimateRevenueId1 = estimateRevenueId.concat(id);
            var estimateBuyRateId = 'estimateBuyRate';
            var estimateBuyRateId1 = estimateBuyRateId.concat(id);
            var estimateSellRateId = 'estimateSellRate';
            var estimateSellRateId1 = estimateSellRateId.concat(id);
            var estimateExpense = (estHour * estQty) * (document.getElementById(estimateBuyRateId1).value);
            var estimateRevenue =  (estHour * estQty) * (document.getElementById(estimateSellRateId1).value);
            document.getElementById(estimateExpenseId1).value =  estimateExpense;
            document.getElementById(estimateRevenueId1).value =  estimateRevenue;
            if(estHour == null || estHour == '' ){
                estHour=0.0;
                }
            }
            if(target ==""){
            deleteDataFromId(id,shipNumber,value,ticket,sessionCorpID);
                }
        }
}


function preventWorkOrder(target,id,sessionCorpID,shipNumber,fieldName,ticket,workorder){
    if(ticket != null && ticket != ''){
        alert(" Work ticket has already been created, We cannot edit the WorkOrder.")
        document.getElementById('workOrderOI'+id).value = workorder;    
        return false;
        }
    var workorder =  document.getElementById('workOrderOI'+id).value;
    var resource =  document.getElementById('type'+id).value;
    if(fieldName == 'type'){
    if(((workorder !=null && workorder != '')) && resource !='T'){
    var url = "statusOfWorkTicketByWO.html?ajax=1&decorator=simple&popup=true&id="+id+"&workorder="+workorder+"&sessionCorpID="+sessionCorpID+"&shipNumber="+shipNumber;
    http201.open("GET", url, true); 
     http201.onreadystatechange = function() {handleHttpResponse201(target.id,id,target.value,shipNumber)};
     http201.send(null);
        }}
    if(fieldName == 'workorder'){
        if(((workorder !=null && workorder != '')) && (resource =='V' ||resource == 'C' || resource == 'M' ||resource == 'E' )){
    var url = "statusOfWorkTicketByWO.html?ajax=1&decorator=simple&popup=true&id="+id+"&workorder="+workorder+"&sessionCorpID="+sessionCorpID+"&shipNumber="+shipNumber;
    http201.open("GET", url, true); 
     http201.onreadystatechange = function() {handleHttpResponse201(target.id,id,target.value,shipNumber)};
     http201.send(null);
        }
        }
}

function checkWorkOrderValidation(id,fieldName,target,tableName,shipNumber){
    var value=target.value;
    var oneIncNumber=0;
    if(value.indexOf('WO_')>-1){
        oneIncNumber=parseInt(value.replace('WO_',''))
    }
    var found=false;
    var findMax=checkAddWorkOrderValidation();
    <c:forEach var="entry" items="${operationResourceListForOI}"> 
    var workNo="${entry.workorder}";    
    if(workNo==value && !found){
        found=true;
    }
    </c:forEach>
     if(found){
    }else{
        var maxN=parseInt(findMax.split("~")[1]);
        maxN++;
        if((value.indexOf('WO_')>-1) && findMax.split("~")[0]=='T' && maxN==oneIncNumber){
        }else{
            alert("Please enter valid WO#");
            document.getElementById("workOrderOI"+id).value='';         
        }
    } 
}

function handleHttpResponse204(id,id1,value,shipNumber,ticket,target,checkFlag){
    if (http204.readyState == 4){       
            var results = http204.responseText
            results = results.trim(); 
            if(results.length>0){
                if(results == 'T' || results == 'P' || results==""){
                    if(checkFlag){
                    var estQty =document.getElementById('resourceQuantity'+id1).value ;
                    var estHour1 = "resourceEstHour";
                    var estimateQty1 = "estimateQuantity"; 
                    var estimateQtyId = estimateQty1.concat(id1);
                    var estHourId = estHour1.concat(id1);
                    var estHour = document.getElementById(estHourId).value;
                    document.getElementById(estimateQtyId).value = (estHour * estQty);
                    var estimateExpenseId = "estimateExpense";
                    var estimateRevenueId = "estimateRevenue";
                    var estimateExpenseId1 = estimateExpenseId.concat(id1);
                    var estimateRevenueId1 = estimateRevenueId.concat(id1);
                    
                    var estimateBuyRateId = 'estimateBuyRate';
                    var estimateBuyRateId1 = estimateBuyRateId.concat(id1);
                    var estimateSellRateId = 'estimateSellRate';
                    var estimateSellRateId1 = estimateSellRateId.concat(id1);
                    var estimateExpense = (estHour * estQty) * (document.getElementById(estimateBuyRateId1).value);
                    var estimateRevenue =  (estHour * estQty) * (document.getElementById(estimateSellRateId1).value);
                    document.getElementById(estimateExpenseId1).value =  estimateExpense;
                    document.getElementById(estimateRevenueId1).value =  estimateRevenue;
                    if(estHour == null || estHour == '' ){
                        estHour=0.0;
                        }
                    }
                    if(target == ""){
                    deleteDataFromId(id1,shipNumber,value,ticket,sessionCorpID);
                    }
                    if(document.getElementById(id).value == 'T' || value == 'T'){
                        //document.getElementById(id).value=value;
                        alert("Third Party resource can't  be Changed to other resource.Vice Varsa is also not Allowed ");
                        reloadDivById(id1);
                        return false;
                        }
                    }else{
                        validateWorkTicketStatusForResources(id1,ticket,sessionCorpID,shipNumber);
                        document.getElementById('resourcedescriptionTemp'+id1).value=1;
                        if(results == 'A'){results="Actual";}
                        if(results == 'C'){results="Cancelled";}
                        if(results == 'F'){results="Forecasting";}
                        if(results == 'R'){results="Rejected";}
                        if(results == 'D'){results="Dispatching";}
                        if(target == "")
                        {
                            alert("Work ticket in " +results+" Status, cannot delete the resources. Please contact Operations");    
                        }else{
                            
                        alert("Work ticket in " +results+" Status, cannot edit the resources. Please contact Operations");
                        document.getElementById(id).readOnly = true;
                        document.getElementById(id).value=value;
                        if(id =='type'+id1)
                            {
                            reloadDivById(id1);
                            }
                        return false;
                    }
                }
                }
}}

function validateWorkTicketStatusForResources(id,ticket,sessionCorpID,shipNumber)
{
    showOrHide(0); 
     var url = "statusOfWorkTicket.html?ajax=1&decorator=simple&popup=true&id="+id+"&ticket="+ticket+"&sessionCorpID="+sessionCorpID;
     http207.open("GET", url, true); 
     http207.onreadystatechange = function() {handleHttpResponse207(id,ticket)};
     http207.send(null);
}
function handleHttpResponse207(id,ticket){
    if (http207.readyState == 4){       
            var results = http204.responseText
            results = results.trim(); 
            if(results.length>0)
                {
                if(results == 'T' || results == 'P' || results=="")
                    {
                    }else{
                        <c:forEach var="entry" items="${resourceListForOI}">
                         var id="${entry.id}";
                         var CheckTicket="${entry.ticket}";
                        if(CheckTicket == ticket)
                            {
                            document.getElementById('resourcedescriptionTemp'+id).value=1;
                            }
                         </c:forEach>
                }
                }
}}

function validateWorkOrderForType(id){
   var workOrder=document.getElementById("workOrderOI"+id).value;
    if(workOrder==''){
        alert("Please enter WO# "); 
       document.getElementById("type"+id).value='';
    }
}

function findResource(type,descriptId,id){
    if(type.length >= 0 ){
        document.getElementById(descriptId).value='';
        document.getElementById('resourceQuantity'+id).value='';
        document.getElementById('estimateQuantity'+id).value='';
        document.getElementById('resourceEstHour'+id).value='';
        document.getElementById('estimateBuyRate'+id).value='';
        document.getElementById('estimateSellRate'+id).value=''
        document.getElementById('estimateExpense'+id).value='';
        document.getElementById('estimateRevenue'+id).value='';
       /*  document.getElementById('revisionrevenue'+id).value='';
        document.getElementById('revisionbuyrate'+id).value='';
        document.getElementById('revisionrevenue'+id).value='';
        document.getElementById('revisionexpense'+id).value='';
        document.getElementById('revisionsellrate'+id).value='';
        document.getElementById('revisionQuantity'+id).value=''; */
    }else{
    }
}
 
function checkChar(descript,descriptId,categoryId,divId,id,target,value,ticket,corpid,shipNumber) {
    var resourcedescriptionTemp=document.getElementById('resourcedescriptionTemp'+id).value;
    if(ticket != null && ticket !='')
        {
    validateWorkTicketStatus(id,value,target,ticket,sessionCorpID,shipNumber,false);
    if(flagForAutoCommit="true"  && resourcedescriptionTemp!="1"){
        AutoCompleterAjaxCall(descriptId,categoryId,divId);
        }}else{
    AutoCompleterAjaxCall(descriptId,categoryId,divId);
    }
    }

function AutoCompleterAjaxCall(descriptId,categoryId,divId){
    var descriptVal = document.getElementById(descriptId).value;
    var type = document.getElementById(categoryId).value;
    document.getElementById('operationResourceForm').setAttribute("AutoComplete","off");
     document.forms['operationResourceForm'].elements['tempId1'].value=descriptId;
     document.forms['operationResourceForm'].elements['tempId2'].value=divId; 
     new Ajax.Request('/redsky/resourceAutocomplete.html?ajax=1&tempCategory='+type+'&tempresource='+encodeURIComponent(descriptVal)+'&decorator=simple&popup=true',
              {
                method:'get',
                onSuccess: function(transport){
                  var response = transport.responseText || "no response text";
                  var mydiv = document.getElementById(divId);
                  document.getElementById(divId).style.display = "block";                
                  mydiv.innerHTML = response;
                  mydiv.show(); 
                },
                onFailure: function(){ alert('Something went wrong...') }
              });
}

function checkValue(result){
    var targetID=document.forms['operationResourceForm'].elements['tempId1'].value;
    var targetID2=document.forms['operationResourceForm'].elements['tempId2'].value;
    document.getElementById(targetID).value=result.innerHTML;
    document.getElementById(targetID2).style.display = "none";
    var onlytemp = targetID.replace(/[^0-9]/g,"");
    var target = result.innerHTML
    sellReteAndBuyRateByDescription(onlytemp,'${noteID}');
}

function sellReteAndBuyRateByDescription(id,shipNumber){
    var type=validateFieldValueString('type'+id)
    var description=validateFieldValueString('resourcedescription'+id)
    var resourceDescription;
    /* var shipNumber='${serviceOrder.shipNumber}'; */
    showOrHide(0);
    <c:forEach var="entry" items="${operationResourceListForOI}">
     var checkId="${entry.id}";
     if(checkId==id){
     var resourceDescription = "${entry.description}";
     }
     </c:forEach>
     if(resourceDescription != description)
     {
     var url = "sellReteAndBuyRateByDescriptionAjax.html?ajax=1&decorator=simple&popup=true&id="+id+"&type="+type+"&description="+encodeURIComponent(description)+"&shipNumber="+shipNumber;
     http203.open("GET", url, true); 
     http203.onreadystatechange = function() {handleHttpResponse203(id)};
     http203.send(null);
     }
}
function handleHttpResponse203(id){
    if (http203.readyState == 4){       
            var results = http203.responseText
            results = results.trim();
            if(results.length>0)
                {
            document.getElementById('resourceQuantity'+id).value=results.split("~")[0];            
            document.getElementById('estimateBuyRate'+id).value=results.split("~")[2];
            document.getElementById('estimateSellRate'+id).value=results.split("~")[1];
            document.getElementById('resourceEstHour'+id).value=1.0;
            fillEstimateQuantity(results.split("~")[0],id);
          //  document.getElementById('revisionbuyrate'+id).value=results.split("~")[2];
            document.getElementById('revisionsellrate'+id).value=results.split("~")[1];

           // document.getElementById('revisionexpense'+id).value= (results.split("~")[2]) *  (document.getElementById('revisionQuantity'+id).value);
            //document.getElementById('revisionrevenue'+id).value = (results.split("~")[1]) *  (document.getElementById('revisionQuantity'+id).value)
            showOrHide(0);                      
    }}
}

function fillEstimateQuantity(target,onlytemp,value)
{
    var estQty = 0;
    try{
        estQty=target.value;
        var letters = /[^0-9]/g;  
         if(target.value.match(letters) != null)
             {
             alert("Please Enter Valid Value")
             target.value = value;
             return false;
             }      
    }catch(e){
        estQty=parseInt(target);
    }
    var estHour1 = "resourceEstHour";
    var estimateQty1 = "estimateQuantity"; 
    var estimateQtyId = estimateQty1.concat(onlytemp);
    var estHourId = estHour1.concat(onlytemp);
    var estHour = document.getElementById(estHourId).value;
    document.getElementById(estimateQtyId).value = (estHour * estQty);
    var estimateExpenseId = "estimateExpense";
    var estimateRevenueId = "estimateRevenue";
    var estimateExpenseId1 = estimateExpenseId.concat(onlytemp);
    var estimateRevenueId1 = estimateRevenueId.concat(onlytemp);
    
    var estimateBuyRateId = 'estimateBuyRate';
    var estimateBuyRateId1 = estimateBuyRateId.concat(onlytemp);
    var estimateSellRateId = 'estimateSellRate';
    var estimateSellRateId1 = estimateSellRateId.concat(onlytemp);
    
    var estimateExpense = (estHour * estQty) * (document.getElementById(estimateBuyRateId1).value);
    var estimateRevenue =  (estHour * estQty) * (document.getElementById(estimateSellRateId1).value);
    
    document.getElementById(estimateExpenseId1).value =  estimateExpense;
    document.getElementById(estimateRevenueId1).value =  estimateRevenue;
    if(estHour == null || estHour == '' )
        {
        estHour=0.0;
        }
}

function validateFieldValueString(fieldId){
    var result1 =" ";
  
    try {
        var result = document.getElementById(fieldId);
        if(result == null || result ==  undefined  || result.value ==  ''){
               return result1;
           }else{
               return result.value;
           }
    }catch(e) {
        return result1;
    }
}

function validateFieldValueForDate(fieldId){
    var result1 =null;
    try {
        var result = document.getElementById(fieldId);
        if(result == null || result ==  undefined  || result.value ==  ''){
               return result1;
           }else{
               return convertItProperDateFormate1(result.value);
           }
    }catch(e) {
        return result1;
    }
}

function validateFieldValueForDouble(fieldId){
    var result1 ="0.00";
    try {
        var result = document.getElementById(fieldId);
        if(result == null || result ==  undefined  || result.value ==  ''){
               return result1;
           }else{
               return result.value;
           }
    }catch(e) {
        return result1;
    }
}
function validateFieldValueForNumber(fieldId){
var result1 ="0";
    try {
        var result = document.getElementById(fieldId);
           if(result == null || result ==  undefined  || result.value ==  ''){
               return result1;
           }else{     
               return (result.value.indexOf(".")>-1?result.value.substring(0,result.value.indexOf(".")):result.value);
           }
    }catch(e) {
        return result1;
    }
}



function getHTTPObject3() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}

    function fillRevnueBySellRate(target){
    var letters = /[^0-9]/g;  
    var decimal=  /^[-+]?[0-9]+\.[0-9]+$/;   
     if( (target.value.match(letters) != null) && (target.value.match(decimal) == null) )
     {
     alert("Please Enter Valid Value")
     target.value = 0;
     }
    var estimateSellRate = target.value;
    var onlytemp = target.name.replace(/[^0-9]/g,"");
    var estimateQuantityName = "estimateQuantity";
    var estimateExpense1 = "estimateRevenue"; 
    var estimateQuantityId = estimateQuantityName.concat(onlytemp);
    var estimateExpenseId = estimateExpense1.concat(onlytemp);
    var estimateQuantityValue =  document.getElementById(estimateQuantityId).value;
    var revisionxpensevalue = estimateSellRate * estimateQuantityValue;
    document.getElementById(estimateExpenseId).value = estimateSellRate * estimateQuantityValue;
}
    
    function saveOI(){
    	     var delimeter1 = "-@-";
    	     var delimeter = "-,-";
    	     var oiValue = '';
    	     <c:forEach var="entry" items="${operationResourceListForOI}"> 
    	     var id="${entry.id}";
    	        if(oiValue == ''){
    	            oiValue  =  getOIValue(id.trim());
    	        }else{
    	            oiValue  =  oiValue +delimeter1+ getOIValue(id.trim());
    	        }
    	     </c:forEach>
    	      if(oiValue!='' && !saveValidation()){
    	         showOrHide(1);
    	     $.ajax({
    	          type: "POST",
    	          url: "autoSaveOIForAccPortal.html?ajax=1&decorator=simple&popup=true",
    	          data: { sid: '${serviceOrder.id}', oiValue:oiValue, delimeter:delimeter, delimeter1:delimeter1},
    	          success: function (data, textStatus, jqXHR) {
    	              if(data.trim() != null && data.trim() != ''){
    	                  var msg = data.trim().replace(/'/g, "");
    	                 // alertMsg = "The hub limit for resource is exceeding the limit for ticket "+msg+". The Ticket will be sent in Dispatch Queue for Approval.";
    	              }
    	                  showOrHide(0);
    	              location.reload(true);
    	          },
    	        error: function (data, textStatus, jqXHR) {
    	                    showOrHide(0);
    	                }
    	        });
    	     }else{
    	         alert("Please enter valid WO#");
    	     } 
    	}
    
    function getOIValue(id){
        var delimeter = "-,-";
          var values = id;
          values = values + delimeter + validateFieldValueString('workOrderOI'+id); //01
          values = values + delimeter + validateFieldValueForDate('date'+id);//02
          values = values + delimeter + validateFieldValueString('type'+id);//03
          values = values + delimeter + validateFieldValueString('resourcedescription'+id);//04
          values = values + delimeter + validateFieldValueForNumber('resourceQuantity'+id);//05
          values = values + delimeter + validateFieldValueForDouble('resourceEstHour'+id);//06
          values = values + delimeter + validateFieldValueString('resourceComments'+id);//07
          values = values + delimeter + validateFieldValueForNumber('estimateQuantity'+id);//08
          values = values + delimeter + validateFieldValueForDouble('estimateBuyRate'+id);//09
          values = values + delimeter + validateFieldValueForDouble('estimateExpense'+id);//10
          values = values + delimeter + validateFieldValueForDouble('estimateSellRate'+id);//11
          values = values + delimeter + validateFieldValueForDouble('estimateRevenue'+id);//12
          values = values + delimeter + validateFieldValueForNumber('revisionQuantity'+id);//13
          values = values + delimeter + validateFieldValueForDouble('revisionbuyrate'+id);//14
          values = values + delimeter + validateFieldValueForDouble('revisionexpense'+id);//15
          values = values + delimeter + validateFieldValueForDouble('revisionsellrate'+id);//16
          values = values + delimeter + validateFieldValueForDouble('revisionrevenue'+id);//17
          values = values + delimeter + validateFieldValueForDouble('ticket'+id);//18
          //values = values + delimeter + validateFieldValueForDouble('workTicketId'+id);//18
          return values;
    }
    
    function convertItProperDateFormate1(target){ 
        var calArr=target.split("-");
        var year=calArr[2];
        year="20"+year;
        var month=calArr[1];
               if(month == 'Jan') { month = "01"; }  
          else if(month == 'Feb') { month = "02"; } 
          else if(month == 'Mar') { month = "03"; } 
          else if(month == 'Apr') { month = "04"; } 
          else if(month == 'May') { month = "05"; } 
          else if(month == 'Jun') { month = "06"; }  
          else if(month == 'Jul') { month = "07"; } 
          else if(month == 'Aug') { month = "08"; }  
          else if(month == 'Sep') { month = "09"; }  
          else if(month == 'Oct') { month = "10"; }  
          else if(month == 'Nov') { month = "11"; }  
          else if(month == 'Dec') { month = "12"; } 
        var day=calArr[0];
        var date=year+"-"+month+"-"+day;
        return date;
    }
    
    function saveValidation(){
        var found=false;
        <c:forEach var="entry" items="${operationResourceListForOI}"> 
        var id="${entry.id}";
        var workNo=document.getElementById('workOrderOI'+id).value;
        if((workNo=='' || workNo==null || workNo==undefined) && !found){
            found=true;
        }
        </c:forEach>
        return found;
    }
    
    function reloadDivById(id){
       <c:forEach var="entry" items="${operationResourceListForOI}">
         var checkId="${entry.id}";
         if(checkId==id){
             document.getElementById('workOrderOI'+id).value="${entry.workorder}";
             document.getElementById('type'+id).value="${entry.type}";
             document.getElementById('resourcedescription'+id).value="${entry.description}";
             document.getElementById('resourceQuantity'+id).value="${entry.quantitiy}";
             document.getElementById('resourceEstHour'+id).value="${entry.esthours}";
             document.getElementById('resourceComments'+id).value="${entry.comments}";
             document.getElementById('estimateQuantity'+id).value="${entry.estimatedquantity}";
             document.getElementById('estimateBuyRate'+id).value="${entry.estimatedbuyrate}";
             document.getElementById('estimateExpense'+id).value="${entry.estimatedexpense}";
             document.getElementById('estimateSellRate'+id).value="${entry.estimatedsellrate}";
             document.getElementById('estimateRevenue'+id).value="${entry.estimatedrevenue}";
            /*  document.getElementById('revisionQuantity'+id).value="${entry.revisionquantity}";
             document.getElementById('revisionbuyrate'+id).value="${entry.revisionbuyrate}";
             document.getElementById('revisionexpense'+id).value="${entry.revisionexpense}";
             document.getElementById('revisionsellrate'+id).value="${entry.revisionsellrate}";
             document.getElementById('revisionrevenue'+id).value="${entry.revisionrevenue}"; */
             }
         </c:forEach> 
    }
    
    function auditSetUpForOI(id){
     window.open('auditList.html?decorator=popup&popup=true&id='+id+'&tableName=operationsintelligence&decorator=popup&popup=true','audit','height=400,width=790,top=100,left=150, scrollbars=yes,resizable=yes').focus();
    }
    
    function deleteDataFromId(id,shipNumber,itemsJbkEquipId,ticket,sessionCorpID){
        var agree=confirm("Are you sure you wish to remove it?");
        if (agree){
        showOrHide(0);                      
        var url = "deletefromOIResorces.html?id="+id+"&shipNumber="+shipNumber+"&itemsJbkEquipId="+itemsJbkEquipId+"&workTicket="+ticket+"&sessionCorpID="+sessionCorpID;
         http255.open("GET", url, true); 
         http255.onreadystatechange = function() {handleHttpResponse255(id)};
         http255.send(null);
    }}
    function handleHttpResponse255(id){
        if (http255.readyState == 4){       
                var results = http255.responseText
                showOrHide(0); 
                location.reload(true);
        }
    }

    function openScopeForWO(id,workorder,ticket,shipNumber){
        var url = 'scopeOfWOForOI.html?decorator=popup&popup=true&id='+id+"&workorder="+workorder+"&shipNumber="+shipNumber+"&ticket="+ticket;
        window.open(url,'ScopeForWO','height=250,width=500,top=250,left=150, scrollbars=yes,resizable=yes'); 
    }
    

    function fillExpenseByRevQty(target){
        var letters = /[^0-9]/g;  
         if(target.value.match(letters) != null){
             alert("Please Enter Valid Value")
             target.value = 0;
             }
        var revisionQty = target.value;
        var onlytemp = target.name.replace(/[^0-9]/g,"");
        //var estBuyRate1 = "revisionbuyrate";
        //var estimateExpense1 = "revisionexpense"; 
        var revisionSellRate1 = "revisionsellrate";
        var revisionrevenue1 = "revisionrevenue";
       // var estBuyRateId = estBuyRate1.concat(onlytemp);
       // var estimateExpenseId = estimateExpense1.concat(onlytemp);
        var revisionSellRateId = revisionSellRate1.concat(onlytemp);
        var revisionrevenueId = revisionrevenue1.concat(onlytemp);
      //  var revisionBuyRateValue = document.getElementById(estBuyRateId).value;
      //  var revexpense = revisionQty * revisionBuyRateValue;
      //  document.getElementById(estimateExpenseId).value = revexpense;
        var revisionSellRateValue =  document.getElementById(revisionSellRateId).value;
        var revisionrevenue = revisionQty * revisionSellRateValue;
        document.getElementById(revisionrevenueId).value = revisionrevenue;
    }
    
    function changerevenueByRevSellRate(target){
        var letters = /[^0-9]/g;  
        var decimal=  /^[-+]?[0-9]+\.[0-9]+$/;   
         if( (target.value.match(letters) != null) && (target.value.match(decimal) == null) ){
         alert("Please Enter Valid Value")
         target.value = 0;
         }
        var estimateSellRate = target.value;
        var onlytemp = target.name.replace(/[^0-9]/g,"");
        var estimateQuantityName = "revisionQuantity";
        var estimateExpense1 = "revisionrevenue"; 
        var estimateQuantityId = estimateQuantityName.concat(onlytemp);
        var estimateExpenseId = estimateExpense1.concat(onlytemp);
        var estimateQuantityValue =  document.getElementById(estimateQuantityId).value;
        var revisionxpensevalue = estimateSellRate * estimateQuantityValue;
        document.getElementById(estimateExpenseId).value = estimateSellRate * estimateQuantityValue;
    } 
    
    function checkAddWorkOrderValidation(){
        var found='F';
        var initialVal=0;
        <c:forEach var="entry" items="${operationResourceListForOI}"> 
        var id="${entry.id}";
        var workNo="${entry.workorder}";
        if(workNo.indexOf('WO_')>-1){
            var tempNo=parseInt(workNo.replace('WO_',''));
            if(tempNo>initialVal){
                initialVal=tempNo;
            }
        }
        if((workNo=='' || workNo==null || workNo==undefined) && found!='T'){
            found='T';
        }
        </c:forEach>
        found=found+"~"+initialVal;
        return found;
    }
    
    function handleHttpResponse201(id,id1,value,shipNumber){
        if (http201.readyState == 4){       
                var results = http201.responseText
                results = results.trim(); 
              if(results == 'T' || results == 'P' || results==""){
                  }else{
            var msgForWO = document.getElementById('workOrderOI'+id1).value
                      if(results == 'A'){results="Actual";}
                    if(results == 'C'){results="Cancelled";}
                    if(results == 'F'){results="Forecasting";}
                    if(results == 'R'){results="Rejected";}
                    if(results == 'D'){results="Dispatching";}
                    alert("For "+msgForWO+", Work ticket in " +results+" Status, We cannot add/edit the resources, Please contact Operations. ")
                    document.getElementById(id).value="";
                  }
     }
    }
    
    function showScopeForWO(id,workorder,position,shipNumber){
        /* window.open("scopeOfWOForOI.html?id="+id+"&decorator=popup&popup=true','hiiii','height=400,width=770,top=100, left=120, scrollbars=yes,resizable=yes'); */
        var url = "showScopeOfWOForOI.html?decorator=simple&popup=true&id="+id+"&workorder="+workorder+"&shipNumber="+shipNumber;
        ajax_showTooltip(url,position);
         var containerdiv =  document.getElementById("operationResourceListForOI").scrollLeft;
            var newposition = document.getElementById("ajax_tooltipObj").offsetLeft-containerdiv ;  
            document.getElementById("ajax_tooltipObj").style.left = ((parseInt(newposition))+10)+'px';   
    }
    
    var currentClickId;
    function setFieldId(id){
        currentClickId = id;
    }
    
    function checkLimitFoTicket(){
        if(currentClickId!=undefined && currentClickId!=null && currentClickId!=''){
            var id=currentClickId;
             var workTicket = document.getElementById ('ticket'+id).value;
             var date = document.getElementById ('date'+id).value;
             var wo = document.getElementById ('workOrderOI'+id).value;
             checkLimitOfWorkTicket(id,workTicket,date,wo);
        }   
    }

    function checkLimitOfWorkTicket(id,workTicket,date,wo){
        var updateValue = false;
        var dataForCheckLimit = ""+','+""+","+date+","+wo+","+workTicket;
        $.ajax({
          type: "POST",
          url: "checkLimitForOI.html?ajax=1&decorator=simple&popup=true",
          data: { id: id,shipNumber:'${shipNumber}',dataForCheckLimit:dataForCheckLimit},
          success: function (data, textStatus, jqXHR) {
                 data = data.trim();
                 if(data=='NoDateChange'){
                    alert('Work Ticket is not in Target Status,cannot edit the date.')
                      showOrHide(1);
                    window.location.reload();
                 }else{
                 if(data=='thresholdMsg'){
                     /* alert("Ticket is being saved for "+wo+" but your date "+date+" are close to the operational service limit.") */
                     updateDateByTicket(workTicket,document.getElementById ('date'+id).value,false);
                     }else if(data.length>0){
                        var checkForMsg =data.split(",")[0];
                             if(data.trim() == "checkMinimumServiceDays" ){
                                 updateDateByTicket(workTicket,document.getElementById ('date'+id).value,true);
                                 }else if(checkForMsg.trim() == "exceedLimit" ){
                                    if(parseInt(data.split(",")[1].trim()) < 0){
                                       /*  alert("The hub limit for resource is exceeding the limit for "+workTicket+". Ticket for "+workTicket+" will be sent in Dispatch Queue for Approval.") */
                                        updateDateByTicket(workTicket,document.getElementById ('date'+id).value,true);
                                     }else if(parseInt(data.split(",")[1].trim()) == 0){
                                         updateDateByTicket(workTicket,document.getElementById ('date'+id).value,false);
                                     }else{
                                     }  
                                }else if(parseInt(data) <= 0){
                                   /*  alert("Ticket for "+workTicket+" is exceeding the limit. Ticket will be sent in Dispatch Queue for Approval.") */
                                    updateDateByTicket(workTicket,document.getElementById ('date'+id).value,true);
                            }else{
                         }
                    }else if(data.length==0){
                        updateDateByTicket(workTicket,document.getElementById ('date'+id).value,false);
                    }
                 }
            },
            error: function (data, textStatus, jqXHR) {
            }
        }); 
    }
    
    function updateDateByTicket(workTicket,date,updateValue){
    	showOrHide(1);
        new Ajax.Request('/redsky/updateDateByTicketAjax.html?ajax=1&ticket='+workTicket+'&date1='+date+'&updateValue='+updateValue+'&decorator=simple&popup=true',
                 {
                   method:'get',
                   onSuccess: function(transport){
                     var response = transport.responseText || "no response text";
                     window.location.reload();
                     showOrHide(1);
                   },
                   onFailure: function(){ 
                       }
                 });
   }
</script>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <c:set value="00" var="workOrder"/> 
 <table cellspacing="0" cellpadding="0" border="0" style="width:100%;margin-top:2px;margin:0px;padding:0px;height:23px;background:#BCD2EF url(images/bg_listheader.png) repeat-x scroll 0 0;border:2px solid #74B3DC; border-bottom:none;">
<tbody>
<c:if test="${not empty operationResourceListForOI}">
<c:choose>
<c:when test="${uIFlagForOI}">
<tr> 
<td width="2%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td width="36%" align="center" style="font-size:13px;font-weight:bold;" class="listwhitetext">
<div style="line-height: 25px; border-right: 3px solid #003366;margin-right:-4px;">Resource</div>
</td>
<td width="10%" align="center" style="font-size:13px;font-weight:bold;" class="listwhitetext">
<div style="line-height: 25px; border-right: 3px solid #003366;margin-right:-14px;">Estimate</div>
</td>
<td width="15%" align="center" style="font-size:13px;font-weight:bold;" class="listwhitetext">Revision</td>                      
</tr>
</c:when>
<c:otherwise>
<tr> 
<td width="2%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td width="35%" align="center" style="font-size:13px;font-weight:bold;" class="listwhitetext">
<div style="line-height: 25px; border-right: 3px solid #003366;margin-right:-5px;">Resource</div>
</td>
<td width="10%" align="center" style="font-size:13px;font-weight:bold;" class="listwhitetext">
<div style="line-height: 25px; border-right: 3px solid #003366;margin-right:-9px;">Estimate</div>
</td>
<td width="15%" align="center" style="font-size:13px;font-weight:bold;" class="listwhitetext">Revision</td>                      
</tr>
</c:otherwise>
</c:choose>
</c:if>
 <c:if test="${empty operationResourceListForOI}">
<tr> 
<td width="4%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td width="51%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">
<div style="line-height: 25px; border-right: 3px solid rgb(0, 51, 102); margin-right:4px;">Resource</div>
</td>
<td width="11%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">
<div style="line-height: 25px; border-right: 3px solid rgb(0, 51, 102); margin-right: -13px;">Estimate</div>
</td>
<td width="18%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Revision</td>                      
</tr>
</c:if> 
</tbody>
</table>
 <c:set var="myStyle" value="background:#CBEBFF none repeat scroll 0 0;" />
<c:set var="myStylesec" value="background:#FBFBFB none repeat scroll 0 0;" />
<s:set name="operationResourceListForOI" value="operationResourceListForOI" scope="request"/>
 <c:if test="${operationResourceListForOI!='[]'}"> 
 <display:table name="operationResourceListForOI" class="table" requestURI="" id="operationResourceListForOI" style="margin-bottom:5px;"> 
  <c:choose>
<c:when test="${myVar !=operationResourceListForOI.workorder}">
 <c:set var="myVar" value="${operationResourceListForOI.workorder}"></c:set> 
  <display:column style="width:5px;${myStyle}" >
 <img  id="scope${operationResourceListForOI.id}" src="<c:url value='/images/ticker_space.gif'/>" onmouseover="showScopeForWO('${operationResourceListForOI.id}','${operationResourceListForOI.workorder}',this,'${operationResourceListForOI.shipNumber}');" onmouseout="ajax_hideTooltip();" onclick="openScopeForWO('${operationResourceListForOI.id}','${operationResourceListForOI.workorder}','${operationResourceListForOI.ticket}','${operationResourceListForOI.shipNumber}');"/> 
  </display:column>
<display:column  title="WO#" style="width:30px;${myStyle}">
   <input type="text" class="input-text pr-f11" name="workOrderOI${operationResourceListForOI.id}" id="workOrderOI${operationResourceListForOI.id}"  value="${operationResourceListForOI.workorder}" onclick="validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.workorder}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false)"  onchange ="preventWorkOrder(this,'${operationResourceListForOI.id}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}','workorder','${operationResourceListForOI.ticket}','${operationResourceListForOI.workorder}'),validateWorkOrder('${operationResourceListForOI.id}');checkWorkOrderValidation('${operationResourceListForOI.id}','workorder',this,'OperationsIntelligence','${operationResourceListForOI.shipNumber}');" style="width:37px" />
  </display:column>
    <display:column  title='Date'  style="width:60px;${myStyle}">
    <fmt:parseDate pattern="yyyy-MM-dd" value="${operationResourceListForOI.date}" var="parsedAtDepartDate" />
    <fmt:formatDate pattern="dd-MMM-yy" value="${parsedAtDepartDate}" var="formattedAtDepartDate" />
    <c:if test="${operationResourceListForOI.ticket != null && operationResourceListForOI.ticket !=  ''}"> 
        <s:textfield cssClass="input-textUpper pr-f11" id="date${operationResourceListForOI.id}" name="date${operationResourceListForOI.id}" value="${formattedAtDepartDate}" onselect="checkLimitFoTicket()" required="true" cssStyle="width:52px" maxlength="11" readonly="true" tabindex="72"/>
        <img id="date${operationResourceListForOI.id}-trigger" style="vertical-align:bottom" name="date" src="${pageContext.request.contextPath}/images/calender.png" textelementname="date${operationResourceListForOI.id}" HEIGHT=20 WIDTH=17   onclick="setFieldId('${operationResourceListForOI.id}')" />
    </c:if>
    <c:if test="${operationResourceListForOI.ticket == null || operationResourceListForOI.ticket ==''}"> 
        <s:textfield cssClass="input-textUpper pr-f11" id="date${operationResourceListForOI.id}" name="date${operationResourceListForOI.id}" value="${formattedAtDepartDate}" required="true" cssStyle="width:52px" maxlength="11" readonly="true" tabindex="72"/>
        <img id="date${operationResourceListForOI.id}-trigger" style="vertical-align:bottom" class="avoid-clicks" name="date" src="${pageContext.request.contextPath}/images/calender.png" textelementname="date${operationResourceListForOI.id}" HEIGHT=20 WIDTH=17 />
    </c:if>
    </display:column>
    
  <display:column  title="Ticket" style="width:40px;${myStyle}">

  <c:if test="${operationResourceListForOI.ticket != null && operationResourceListForOI.ticket !=  ''}">
  <input type="button" class="input-text pr-f11 " id="ticket${operationResourceListForOI.id}" name="ticket${operationResourceListForOI.id}" value="${operationResourceListForOI.ticket}" 
      /> 
 </c:if>
 <c:if test="${operationResourceListForOI.ticket == null || operationResourceListForOI.ticket ==  ''}">
  <input type="button" class="input-textUpper pr-f11"  id="ticket${operationResourceListForOI.id}" name="ticket${operationResourceListForOI.id}" value="${operationResourceListForOI.ticket}"  style="width:51px;height:16px;" /> 
 </c:if>
  </display:column>

    <display:column title="Category" style="width:77px;margin-top:2px;${myStyle}"> 
    <select id="type${operationResourceListForOI.id}" name ="type${operationResourceListForOI.id}" style="width:77px" class="list-menu pr-f11" onmousedown="validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.type}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false);" onclick="validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.type}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false);"  onchange="validateWorkOrderForType('${operationResourceListForOI.id}');findResource(this.value,'resourcedescription${operationResourceListForOI.id}','${operationResourceListForOI.id}');validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.type}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false);"> 
    <c:forEach var="chrms" items="${resourceCategory}" varStatus="loopStatus">
    <c:choose>
    <c:when test="${chrms.key == operationResourceListForOI.type}">
    <c:set var="selectedInd" value=" selected"></c:set>
    </c:when>
    <c:otherwise>
    <c:set var="selectedInd" value=""></c:set>
    </c:otherwise>
    </c:choose>
    <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
    <c:out value="${chrms.value}"></c:out>
    </option>
    </c:forEach> 
    </select>
    </display:column>
 <display:column  title="Resource" style="width:35px;${myStyle}">
    <s:hidden id="checkLimit${operationResourceListForOI.id}" name="checkLimit${resourceTransferOI.id}"/>
 <input type="hidden" value="" id="resourcedescriptionTemp${operationResourceListForOI.id}"/>
  <input type="text" class="input-text pr-f11" id="resourcedescription${operationResourceListForOI.id}" name="resourcedescription${operationResourceListForOI.id}"  value="${operationResourceListForOI.description}" style="width:90px" onclick="validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.description}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false);" onkeyup="checkChar(this,'resourcedescription${operationResourceListForOI.id}','type${operationResourceListForOI.id}','choices${operationResourceListForOI.id}','${operationResourceListForOI.id}',this,'${operationResourceListForOI.description}','${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}');" onchange="validateWorkOrder('${operationResourceListForOI.id}');validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.description}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false);" />
  <div id="choices${operationResourceListForOI.id}" class="listwhitetext" ></div>
 </display:column>
 <display:column title="Est. Quantity" style="width:40px;${myStyle}">
 <input type="text" class="input-text pr-f11" id="resourceQuantity${operationResourceListForOI.id}" name="resourceQuantity${operationResourceListForOI.id}"  value="${operationResourceListForOI.quantitiy}" style="width:40px;text-align:right;" onclick="removeZero('${operationResourceListForOI.id}')" onchange="validateWorkOrder('${operationResourceListForOI.id}');return validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.quantitiy}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',true);" />  
 </display:column> 
 <display:column  title="Est. Hour" style="width:30px;${myStyle}"> 
 <input type="text" class="input-text pr-f11" id="resourceEstHour${operationResourceListForOI.id}" name="resourceEstHour${operationResourceListForOI.id}"  value="${operationResourceListForOI.esthours}" style="width:30px;text-align:right;" onchange="validateWorkOrder('${operationResourceListForOI.id}');return validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.esthours}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',true);"/>
 </display:column>
 
 <display:column  title="Comments" headerClass="header-rt" style="width:100px;border-right:medium solid #003366;${myStyle}"> 
 <%-- <input type="text" class="input-text pr-f11" id="resourceComments${operationResourceListForOI.id}" name="resourceComments${operationResourceListForOI.id}" value="<c:out value='${operationResourceListForOI.comments}'/>"  onchange="validateWorkOrder('${operationResourceListForOI.id}');" style="width:100px" /> --%>
    <textarea class="textarea-comment pr-f11" title="<c:out value="${operationResourceListForOI.comments}"></c:out>" maxlength="200" id="resourceComments${operationResourceListForOI.id}" name="resourceComments${operationResourceListForOI.id}" onmouseover="" onmouseout="checkMsg(this);" onclick="checkMsg1(this)" onchange="validateWorkOrder('${operationResourceListForOI.id}');" style="width:100px" ><c:out value='${operationResourceListForOI.comments}'/></textarea>
 </display:column>
 
  <display:column  title="Qty" style="width:38px;${myStyle}"> 
  <input type="text" class="input-textUpper pr-f11" id='estimateQuantity${operationResourceListForOI.id}' name="estimateQuantity${operationResourceListForOI.id}" readonly="true" value="${operationResourceListForOI.quantitiy*operationResourceListForOI.esthours}"  style="width:38px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');fillExpenseByQty(this);"/>
   <input type="hidden" class="input-text pr-f11" id="estimateBuyRate${operationResourceListForOI.id}" name="estimateBuyRate${operationResourceListForOI.id}" value="${operationResourceListForOI.estimatedbuyrate}" style="width:45px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');fillExpenseByRate(this);calculateRevenueForOI();"/>
    <input type="hidden" class="input-textUpper pr-f11" id="estimateExpense${operationResourceListForOI.id}" name="estimateExpense${operationResourceListForOI.id}" value="${operationResourceListForOI.estimatedexpense}" readonly="true" style="width:47px;text-align:right;"   onchange="validateWorkOrder('${operationResourceListForOI.id}');"/>
 </display:column>
  <%-- <display:column  title="Buy Rate" style="width:45px;${myStyle}"> 
  <input type="text" class="input-text pr-f11" id="estimateBuyRate${operationResourceListForOI.id}" name="estimateBuyRate${operationResourceListForOI.id}" value="${operationResourceListForOI.estimatedbuyrate}" style="width:45px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');fillExpenseByRate(this);calculateRevenueForOI();"/>
 </display:column> 
   <display:column  title="Expense" style="width:47px;${myStyle}"> 
  <input type="text" class="input-textUpper pr-f11" id="estimateExpense${operationResourceListForOI.id}" name="estimateExpense${operationResourceListForOI.id}" value="${operationResourceListForOI.estimatedexpense}" readonly="true" style="width:47px;text-align:right;"   onchange="validateWorkOrder('${operationResourceListForOI.id}');"/>
 </display:column> --%> 
   <display:column  title="Rate" style="width:45px;${myStyle}"> 
  <input type="text" class="input-textUpper pr-f11" readonly="true" id="estimateSellRate${operationResourceListForOI.id}" name="estimateSellRate${operationResourceListForOI.id}" value="${operationResourceListForOI.estimatedsellrate}" style="width:45px;text-align:right;" onchange="validateWorkOrder('${operationResourceListForOI.id}');fillRevnueBySellRate(this);" />
 </display:column>
  <display:column  title="Cost" headerClass="header-rt" style="width:47px;border-right:medium solid #003366;${myStyle}"> 
  <input type="text" class="input-textUpper pr-f11" id="estimateRevenue${operationResourceListForOI.id}" name="estimateRevenue${operationResourceListForOI.id}" readonly="true" value="${operationResourceListForOI.estimatedrevenue}"  style="width:47px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');"/>
 </display:column>
  <display:column  title="Qty" style="width:40px;${myStyle}"> 
  <input type="text" class="input-textUpper pr-f11" readonly="true" id="revisionQuantity${operationResourceListForOI.id}" name="revisionQuantity${operationResourceListForOI.id}"  value="${operationResourceListForOI.revisionquantity}"  style="width:40px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');fillExpenseByRevQty(this);"/>
 </display:column>
<%--   <display:column  title="Buy Rate" style="width:45px;${myStyle}"> 
  <input type="text" class="input-text pr-f11" id="revisionbuyrate${operationResourceListForOI.id}" name="revisionbuyrate${operationResourceListForOI.id}"  value="${operationResourceListForOI.revisionbuyrate}" style="width:45px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');changeExpanseBuyRevRate(this);"/>
 </display:column>
  <display:column  title="Expense" style="width:47px;${myStyle}"> 
  <input type="text" class="input-textUpper pr-f11" id="revisionexpense${operationResourceListForOI.id}" name="revisionexpense${operationResourceListForOI.id}"  value="${operationResourceListForOI.revisionexpense}" readonly="true" style="width:47px;text-align:right;"  />
 </display:column> --%>
  <display:column  title="Rate" style="width:45px;${myStyle}"> 
  <input type="text" class="input-textUpper pr-f11" readonly="true" id="revisionsellrate${operationResourceListForOI.id}" name="revisionsellrate${operationResourceListForOI.id}"  value="${operationResourceListForOI.revisionsellrate}" style="width:45px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');changerevenueByRevSellRate(this);"/>
 </display:column>
  <display:column  title="Cost" style="width:47px;${myStyle}"> 
  <input type="text" class="input-textUpper pr-f11" id="revisionrevenue${operationResourceListForOI.id}" name="revisionrevenue${operationResourceListForOI.id}"  value="${operationResourceListForOI.revisionrevenue}" readonly="true" style="width:47px;text-align:right;" />
 </display:column> 
  <display:column title="Audit" style="text-align:center;width:30px;${myStyle}"> 
    <a><img align="middle" onclick="auditSetUpForOI('${operationResourceListForOI.id}');" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/report-ext.png"/></a>
    </display:column>
 <display:column title="" style="width:5px;${myStyle}">
<a><img align="middle" onclick="validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.itemsJbkEquipId}','','${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false);" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
</display:column>
  <display:column title="" class="hidden" headerClass="hidden" > 
    <input type="hidden" id="workTicketId${resourceTransferOI.id}" name="workTicketId${resourceTransferOI.id}" value="${resourceTransferOI.workTicketId}"> 
    </display:column>
    </c:when>
    <c:otherwise>
  <display:column style="width:5px;${myStylesec}" >
 <img  id="scope${operationResourceListForOI.id}" src="<c:url value='/images/ticker_space.gif'/>" title="<c:out value="${operationResourceListForOI.scopeOfWorkOrder}"></c:out>" onclick="openScopeForWO('${operationResourceListForOI.id}','${operationResourceListForOI.workorder}','${operationResourceListForOI.ticket}','${operationResourceListForOI.shipNumber}','${operationResourceListForOI.shipNumber}');"/> 
  </display:column>
<display:column  title="WO#" style="width:30px;${myStylesec}">
   <input type="text" class="input-text pr-f11" name="workOrderOI${operationResourceListForOI.id}" id="workOrderOI${operationResourceListForOI.id}"  value="${operationResourceListForOI.workorder}" onclick="validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.workorder}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false)"  onchange ="preventWorkOrder(this,'${operationResourceListForOI.id}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}','workorder','${operationResourceListForOI.ticket}','${operationResourceListForOI.workorder}'),validateWorkOrder('${operationResourceListForOI.id}');checkWorkOrderValidation('${operationResourceListForOI.id}','workorder',this,'OperationsIntelligence','${operationResourceListForOI.shipNumber}');" style="width:37px" />
  </display:column>
    <display:column  title='Date' style="width:60px;${myStylesec}">
    <fmt:parseDate pattern="yyyy-MM-dd" value="${operationResourceListForOI.date}" var="parsedAtDepartDate" />
    <fmt:formatDate pattern="dd-MMM-yy" value="${parsedAtDepartDate}" var="formattedAtDepartDate" /> 
        <c:if test="${operationResourceListForOI.ticket!= null && operationResourceListForOI.ticket!=''}"> 
            <s:textfield cssClass="input-textUpper pr-f11" id="date${operationResourceListForOI.id}" name="date${operationResourceListForOI.id}" value="${formattedAtDepartDate}" onselect="checkLimitFoTicket()" required="true" cssStyle="width:52px" maxlength="11" readonly="true" tabindex="72"/>
            <img id="date${operationResourceListForOI.id}-trigger" style="vertical-align:bottom" name="date" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldId('${operationResourceListForOI.id}')" textelementname="date${operationResourceListForOI.id}" HEIGHT=20 WIDTH=17 />
        </c:if>
        <c:if test="${operationResourceListForOI.ticket == null || operationResourceListForOI.ticket ==''}"> 
            <s:textfield cssClass="input-textUpper pr-f11" id="date${operationResourceListForOI.id}" name="date${operationResourceListForOI.id}" value="${formattedAtDepartDate}" required="true" cssStyle="width:52px" maxlength="11" readonly="true" tabindex="72"/>
            <img id="date${operationResourceListForOI.id}-trigger" style="vertical-align:bottom" class="avoid-clicks" name="date" src="${pageContext.request.contextPath}/images/calender.png" textelementname="date${operationResourceListForOI.id}" HEIGHT=20 WIDTH=17 />
        </c:if> 
    </display:column>
    
  <display:column  title="Ticket" style="width:40px;${myStylesec}">
  <c:if test="${operationResourceListForOI.ticket != null && operationResourceListForOI.ticket !=  ''}">
  <input type="button" class="input-text pr-f11" id="ticket${operationResourceListForOI.id}" name="ticket${operationResourceListForOI.id}" value="${operationResourceListForOI.ticket}" 
     /> 
 </c:if>
 <c:if test="${operationResourceListForOI.ticket == null || operationResourceListForOI.ticket ==  ''}">
  <input type="button" class="input-textUpper pr-f11"  id="ticket${operationResourceListForOI.id}" name="ticket${operationResourceListForOI.id}" value="${operationResourceListForOI.ticket}"  style="width:51px;height:16px;" /> 
 </c:if>
  </display:column>

    <display:column title="Category" style="width:77px;margin-top:2px;${myStylesec}"> 
    <select id="type${operationResourceListForOI.id}" name ="type${operationResourceListForOI.id}" style="width:77px" class="list-menu pr-f11" onmousedown="validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.type}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false);" onclick="validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.type}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false);"  onchange="validateWorkOrderForType('${operationResourceListForOI.id}');findResource(this.value,'resourcedescription${operationResourceListForOI.id}','${operationResourceListForOI.id}');validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.type}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false);"> 
    <c:forEach var="chrms" items="${resourceCategory}" varStatus="loopStatus">
    <c:choose>
    <c:when test="${chrms.key == operationResourceListForOI.type}">
    <c:set var="selectedInd" value=" selected"></c:set>
    </c:when>
    <c:otherwise>
    <c:set var="selectedInd" value=""></c:set>
    </c:otherwise>
    </c:choose>
    <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
    <c:out value="${chrms.value}"></c:out>
    </option>
    </c:forEach> 
    </select>
    </display:column>
 <display:column  title="Resource" style="width:35px;${myStylesec}">
 <input type="hidden" value="" id="resourcedescriptionTemp${operationResourceListForOI.id}"/>
  <input type="text" class="input-text pr-f11" id="resourcedescription${operationResourceListForOI.id}" name="resourcedescription${operationResourceListForOI.id}"  value="${operationResourceListForOI.description}" style="width:90px" onclick="validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.description}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false);" onkeyup="checkChar(this,'resourcedescription${operationResourceListForOI.id}','type${operationResourceListForOI.id}','choices${operationResourceListForOI.id}','${operationResourceListForOI.id}',this,'${operationResourceListForOI.description}','${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}');" onchange="validateWorkOrder('${operationResourceListForOI.id}');validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.description}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false);" />
  <div id="choices${operationResourceListForOI.id}" class="listwhitetext" ></div>
 </display:column>
 <display:column title="Est. Quantity" style="width:40px;${myStylesec}">
 <input type="text" class="input-text pr-f11" id="resourceQuantity${operationResourceListForOI.id}" name="resourceQuantity${operationResourceListForOI.id}"  value="${operationResourceListForOI.quantitiy}" style="width:40px;text-align:right;" onclick="removeZero('${operationResourceListForOI.id}')" onchange="validateWorkOrder('${operationResourceListForOI.id}');return validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.quantitiy}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',true);" />  
 </display:column> 
 <display:column  title="Est. Hour" style="width:30px;${myStylesec}"> 
 <input type="text" class="input-text pr-f11" id="resourceEstHour${operationResourceListForOI.id}" name="resourceEstHour${operationResourceListForOI.id}"  value="${operationResourceListForOI.esthours}" style="width:30px;text-align:right;" onchange="validateWorkOrder('${operationResourceListForOI.id}');return validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.esthours}',this,'${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',true);"/>
 </display:column>
 
 <display:column  title="Comments" headerClass="header-rt" style="width:100px;border-right:medium solid #003366;${myStylesec}"> 
 <%-- <input type="text" class="input-text pr-f11" id="resourceComments${operationResourceListForOI.id}" name="resourceComments${operationResourceListForOI.id}" value="<c:out value='${operationResourceListForOI.comments}'/>"  onchange="validateWorkOrder('${operationResourceListForOI.id}');" style="width:100px" /> --%>
    <textarea class="textarea-comment pr-f11" title="<c:out value="${operationResourceListForOI.comments}"></c:out>"  maxlength="200"  id="resourceComments${operationResourceListForOI.id}" name="resourceComments${operationResourceListForOI.id}" onmouseover="" onmouseout="checkMsg(this);" onclick="checkMsg1(this)" onchange="validateWorkOrder('${operationResourceListForOI.id}');" style="width:100px" ><c:out value='${operationResourceListForOI.comments}'/></textarea>
 </display:column>
 
  <display:column  title="Qty" style="width:38px;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" id='estimateQuantity${operationResourceListForOI.id}' name="estimateQuantity${operationResourceListForOI.id}" readonly="true" value="${operationResourceListForOI.quantitiy*operationResourceListForOI.esthours}"  style="width:38px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');fillExpenseByQty(this);"/>
  <input type="hidden" class="input-text pr-f11" id="estimateBuyRate${operationResourceListForOI.id}" name="estimateBuyRate${operationResourceListForOI.id}" value="${operationResourceListForOI.estimatedbuyrate}" style="width:45px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');fillExpenseByRate(this);"/>  
  <input type="hidden" class="input-textUpper pr-f11" id="estimateExpense${operationResourceListForOI.id}" name="estimateExpense${operationResourceListForOI.id}" value="${operationResourceListForOI.estimatedexpense}" readonly="true" style="width:47px;text-align:right;"   onchange="validateWorkOrder('${operationResourceListForOI.id}');"/>
 </display:column>
  <%-- <display:column  title="Buy Rate" style="width:45px;${myStylesec}"> 
  <input type="text" class="input-text pr-f11" id="estimateBuyRate${operationResourceListForOI.id}" name="estimateBuyRate${operationResourceListForOI.id}" value="${operationResourceListForOI.estimatedbuyrate}" style="width:45px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');fillExpenseByRate(this);"/>
 </display:column> 
   <display:column  title="Expense" style="width:47px;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" id="estimateExpense${operationResourceListForOI.id}" name="estimateExpense${operationResourceListForOI.id}" value="${operationResourceListForOI.estimatedexpense}" readonly="true" style="width:47px;text-align:right;"   onchange="validateWorkOrder('${operationResourceListForOI.id}');"/>
 </display:column>  --%>
   <display:column  title="Rate" style="width:45px;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" readonly="true" id="estimateSellRate${operationResourceListForOI.id}" name="estimateSellRate${operationResourceListForOI.id}" value="${operationResourceListForOI.estimatedsellrate}" style="width:45px;text-align:right;" onchange="validateWorkOrder('${operationResourceListForOI.id}');fillRevnueBySellRate(this);" />
 </display:column>
  <display:column  title="Cost" headerClass="header-rt" style="width:47px;border-right:medium solid #003366;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" id="estimateRevenue${operationResourceListForOI.id}" name="estimateRevenue${operationResourceListForOI.id}" readonly="true" value="${operationResourceListForOI.estimatedrevenue}"  style="width:47px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');"/>
 </display:column>
  <display:column  title="Qty" style="width:40px;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" readonly="true" id="revisionQuantity${operationResourceListForOI.id}" name="revisionQuantity${operationResourceListForOI.id}"  value="${operationResourceListForOI.revisionquantity}"  style="width:40px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');fillExpenseByRevQty(this);"/>
 </display:column>
  <%-- <display:column  title="Buy Rate" style="width:45px;${myStylesec}"> 
  <input type="text" class="input-text pr-f11" id="revisionbuyrate${operationResourceListForOI.id}" name="revisionbuyrate${operationResourceListForOI.id}"  value="${operationResourceListForOI.revisionbuyrate}" style="width:45px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');changeExpanseBuyRevRate(this);calculateRevisionRevenue();"/>
 </display:column>
  <display:column  title="Expense" style="width:47px;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" id="revisionexpense${operationResourceListForOI.id}" name="revisionexpense${operationResourceListForOI.id}"  value="${operationResourceListForOI.revisionexpense}" readonly="true" style="width:47px;text-align:right;"  />
 </display:column> --%>
  <display:column  title="Rate" style="width:45px;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" readonly="true" id="revisionsellrate${operationResourceListForOI.id}" name="revisionsellrate${operationResourceListForOI.id}"  value="${operationResourceListForOI.revisionsellrate}" style="width:45px;text-align:right;"  onchange="validateWorkOrder('${operationResourceListForOI.id}');changerevenueByRevSellRate(this);"/>
 </display:column>
  <display:column  title="Cast" style="width:47px;${myStylesec}"> 
  <input type="text" class="input-textUpper pr-f11" id="revisionrevenue${operationResourceListForOI.id}" name="revisionrevenue${operationResourceListForOI.id}"  value="${operationResourceListForOI.revisionrevenue}" readonly="true" style="width:47px;text-align:right;" />
 </display:column> 
  <display:column title="Audit" style="width:40px;text-align:center;${myStylesec}"> 
    <a><img align="middle" onclick="auditSetUpForOI('${operationResourceListForOI.id}');" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/report-ext.png"/></a>
    </display:column>
 <display:column title="" style="width:5px;${myStylesec}">
<a><img align="middle" onclick="validateWorkTicketStatus('${operationResourceListForOI.id}','${operationResourceListForOI.itemsJbkEquipId}','','${operationResourceListForOI.ticket}','${operationResourceListForOI.corpID}','${operationResourceListForOI.shipNumber}',false);" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
</display:column>
  <display:column title="" class="hidden" headerClass="hidden" > 
    <input type="hidden" id="workTicketId${resourceTransferOI.id}" name="workTicketId${resourceTransferOI.id}" value="${resourceTransferOI.workTicketId}"> 
    </display:column>
</c:otherwise>
</c:choose>
  
  <%--  <display:footer>                               
            
                  <tr> 
              <td align="right" colspan="9"><b><div align="right">Sales&nbsp;Tax</div></b></td>
              <td align="left" colspan="2" ></td>
               <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="estmatedSalesTax" name="estmatedSalesTax" value="${operationResourceListForOI.estmatedSalesTax}" onchange="onlyFloatAllowed(this);" size="6" class="input-text pr-f11 "/>
               </td>              
              <td></td>
              <td></td>
            <td></td>    
          <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="revisionSalesTax" name="revisionSalesTax" value="${operationResourceListForOI.revisionSalesTax}" maxlength="17" onchange="onlyFloatAllowed(this);" size="6" class="input-text pr-f11 "/>
               </td>  
          <td align="left" width="70px">
          </td> 
           <td align="left"></td>
              <td align="left"></td>
          </tr>
          <tr> 
              <td align="right" colspan="9"><b><div align="right">Valuation</div></b></td>
              <td align="left" colspan="2" ></td>
               <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="estmatedValuation" name="estmatedValuation" value="${operationResourceListForOI.estmatedValuation}" onchange="onlyFloatAllowed(this);" size="6" class="input-text pr-f11 "/>
               </td>              
              <td></td>
              <td></td>
            <td></td>    
          <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="revisionValuation" name="revisionValuation" value="${operationResourceListForOI.revisionValuation}" maxlength="17" onchange="onlyFloatAllowed(this);" size="6" class="input-text pr-f11 "/>
               </td>  
          <td align="left" width="70px">
          </td> 
           <td align="left"></td>
              <td align="left"></td>
          </tr>
          <tr> 
              <td align="right" colspan="9"><b><div align="right">Consumables</div></b></td>
              <td align="left" colspan="2" ></td>
               <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="estmatedConsumables" name="estmatedConsumables" value="${operationResourceListForOI.estmatedConsumables}" onchange="onlyFloatAllowed(this);" size="6" class="input-text pr-f11 "/>
               </td>              
              <td></td>
              <td></td>
            <td></td>    
          <td align="center" colspan="2">
               <input type="text" style="text-align:right;width:50px;" id="revisionConsumables" name="revisionConsumables" value="${operationResourceListForOI.revisionConsumables}" maxlength="17" onchange="onlyFloatAllowed(this);" size="6" class="input-text pr-f11 "/>
               </td>  
          <td align="left" width="70px">
          </td> 
           <td align="left"></td>
              <td align="left"></td>
          </tr>
            
            
             <tr> 
              <td align="right" colspan="9"><b><div align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></div></b></td>
              <td align="left" colspan="2" ></td>
               <td align="left" colspan="1">
               <input type="text" style="text-align:right;width:50px;" id="estimatedTotalExpense" name="estimatedTotalExpense${resourceTransferOI.id}"  readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
               </td>
               <td></td>
              <td><input type="text" style="text-align:right;width:50px;" id="estimatedTotalRevenue" name="estimatedTotalRevenue${resourceTransferOI.id}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/></td>
              <td></td>
            <td></td>    
          <td align="left" width="40px">
          <input type="text" style="text-align:right;width:50px;" name="revTotalExpense" id="revTotalExpense" value="" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
          </td>
          <td align="left"></td>
          <td align="left" width="70px">
          <input type="text" style="text-align:right;width:50px;" name="revTotalRevenue" id="revTotalRevenue" value="" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
          </td> 
           <td align="left"></td>
              <td align="left"></td>
          </tr>
                   
           <tr>
            <td align="right" colspan="9"><b><div align="right">Gross&nbsp;Margin</div></b></td>
           <td align="left" colspan="2" ></td>                               
           <td align="left" colspan="1">
           <input type="text" style="text-align:right;width:50px;" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
           </td>
           <td></td>
           <td>
           <input type="text" style="text-align:right;width:50px;" id="estGrossMarginForRevenue" name="estGrossMarginForRevenue" value="" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
           </td>
           <td></td><td></td>
          <td align="left" width="40px">
          <input type="text" style="text-align:right;width:50px;" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
          </td>
          <td align="left"></td>
          <td align="left" width="70px" >
          <input type="text" style="text-align:right;width:50px;" id="revGrossMarginForRevenue" name="revGrossMarginForRevenue" value="" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
          </td>
           <td align="left"></td>
              <td align="left"></td>
           </tr>                  
                   
            <tr>
             <td align="right" colspan="9"><b><div align="right">Gross&nbsp;Margin%</div></b></td>
            <td align="left" colspan="2" ></td>
            <td align="left" colspan="1">
            <input type="text" style="text-align:right;width:50px;" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
            </td>
            <td></td>
            <td><input type="text" style="text-align:right;width:50px;" id="estGrossMarginForRevenuePercentage" name="estGrossMarginForRevenuePercentage" value="" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
            </td>
            <td></td><td></td><td></td>
          <td align="left"> </td>
          <td align="left" width="40px">
          <input type="text" style="text-align:right;width:50px;" id="revGrossMarginForRevenuePercentage" name="revGrossMarginForRevenuePercentage" value="" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
          </td>
         </tr>                           
     </display:footer> --%>
 
 </display:table> 
 </c:if>
 <c:if test="${operationResourceListForOI=='[]'}"> 
 <display:table name="operationResourceListForOI" class="table" requestURI="" id="operationResourceListForOI" style="margin-bottom:5px;"> 
<display:column  title="WO#" style="width:30px; ">
  </display:column>
    <display:column  title='Date'  style=" ">
    </display:column>
  <display:column  title="Ticket" style="width:40px; ">
  </display:column>

    <display:column title="Category" style="width:77px;margin-top:2px; "> 
    </display:column>
 <display:column  title="Resource" style="width:35px; ">
 </display:column>
 <display:column title="Est. Quantity" style="width:40px; ">
 </display:column> 
 <display:column  title="Est. Hour" style="width:40px; "> 
 </display:column>
 <display:column  title="Comments" headerClass="header-rt" style="width:100px;border-right:medium solid #003366; "> 
 </display:column>
 
  <display:column  title="Qty" style="width:38px; "> 
 </display:column>
<%--   <display:column  title="Buy Rate" style="width:50px; "> 
 </display:column> --%> 
 <%--   <display:column  title="Expense" style="width:50px; "> 
 </display:column>  --%>
   <display:column  title="Rate" style="width:50px; "> 
 </display:column>
  <display:column  title="Cost" headerClass="header-rt" style="width:50px;border-right:medium solid #003366; "> 
 </display:column>
  <display:column  title="Qty" style="width:40px; "> 
 </display:column>
  <display:column  title="Rate" style="width:50px; "> 
 </display:column>
  <%-- <display:column  title="Expense" style="width:50px; "> 
 </display:column> --%>
  <display:column  title="Rate" style="width:50px; "> 
 </display:column>
  <display:column  title="Cost" style="width:50px; "> 
 </display:column> 
 <display:column title="" style="width:5px; ">
</display:column>
  <display:column title="" class="hidden" headerClass="hidden" > 
    </display:column>
 </display:table> 
 </c:if>
 <input type="button" id="resourceTemplate" class="update-btn-OI" onclick="return contractForOI(this);getDayTemplate();" value="Add Resource Template" style="width:150px; margin-bottom:10px; " tabindex=""/>
 <c:if test="${operationResourceListForOI!='[]'}"> 
 <input type="button" class="update-btn-OI" name="OiSave" value="Save" onclick="saveOI();" class="update-btn-OI" style="width:60px;margin-bottom:10px;" tabindex="" />
 <input type="button" class="update-btn-OI" onclick="transferResourceToWorkTicket('${operationResourceListForOI.shipNumber}');" value="Transfer Resource to WorkTicket" style="width:210px; margin-bottom:10px;" tabindex="" />
 </c:if>
 
 <script type="text/javascript"> 
 setOnSelectBasedMethods(["checkLimitFoTicket()"]);
setCalendarFunctionality();
</script> 