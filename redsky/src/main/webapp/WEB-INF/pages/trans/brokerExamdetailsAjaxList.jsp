<%@ include file="/common/taglibs.jsp"%>  
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<%@page import="com.trilasoft.app.model.Company" %> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<head>   
    <title></title>   
    <meta name="heading" content=""/> 
    <style type="text/css">
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
  
</head> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="ServiceOrderID" value="${serviceOrder.id}"/>
<s:hidden name="ServiceOrderID" value="${serviceOrder.id}"/>
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.registrationNumber" value="%{serviceOrder.registrationNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/> 
<s:hidden name="customerFile.id" />
<c:set var="mydate" value="{0,date,dd-MMM-yyyy}"/>
<div id="Layer3" style="width:80%">

<s:set name="brokerExamDetailsList" value="brokerExamDetailsList" scope="request"/>
<display:table name="brokerExamDetailsList" class="table" requestURI="" id="brokerExamDetailsList"  defaultsort="0" style="width:auto;margin-bottom:2px;">
	 <display:column  title="ID #" style="width:20px;" >
	 <c:out value='${brokerExamDetailsList_rowNum}'/>
    </display:column>   
     <display:column  titleKey="brokerExamDetails.exam" style="text-align:left;">
    	<input type="text" class="input-text" style="text-align:left;width: 200px;" name="brokerExamDetailsList.examName${brokerExamDetailsList.id}" onchange="updateBrokerExamDetails('${brokerExamDetailsList.id}','examName',this,'T');" value="${brokerExamDetailsList.examName}"
    </display:column>
    <display:column  titleKey="brokerExamDetails.location" style="text-align:left;">
    	<input type="text" class="input-text" style="text-align:left;width: 200px;"" name="brokerExamDetailsList.examLocation${brokerExamDetailsList.id}" onchange="updateBrokerExamDetails('${brokerExamDetailsList.id}','examLocation',this,'T');" value="${brokerExamDetailsList.examLocation}"
    </display:column> 
	
	
    <display:column  titleKey="brokerExamDetails.dateIn" >    
	    <fmt:parseDate pattern="yyyy-MM-dd" value="${brokerExamDetailsList.dateIn}" var="parseddateIn" />
		<fmt:formatDate pattern="dd-MMM-yy" value="${parseddateIn}" var="formatteddateIn" />    
    	<input type="text" class="input-textUpper" style="text-align:left;width:60px;" id="dateIn${brokerExamDetailsList.id}" name="brokerExamDetailsList.dateIn${brokerExamDetailsList.id}" readonly="true" value="${formatteddateIn}" onblur="updateBrokerDateExamDetails('${brokerExamDetailsList.id}','dateIn',this,'B');"  onkeydown="return onlyDel(event,this);"/>
    	<img id="dateIn${brokerExamDetailsList.id}_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="updateDatethroughAuto('${brokerExamDetailsList.id}','dateIn');" />
    </display:column>
    
	<display:column  titleKey="brokerExamDetails.dateOut" >
	<fmt:parseDate pattern="yyyy-MM-dd" value="${brokerExamDetailsList.dateOut}" var="parseddateOut" />
		<fmt:formatDate pattern="dd-MMM-yy" value="${parseddateOut}" var="formatteddateOut" />    
    	<input type="text" class="input-textUpper" style="text-align:left;width:60px;" id="dateOut${brokerExamDetailsList.id}" name="brokerExamDetailsList.dateOut${brokerExamDetailsList.id}" readonly="true" value="${formatteddateOut}" onblur="updateBrokerDateExamDetails('${brokerExamDetailsList.id}','dateOut',this,'B');" onkeydown="return onlyDel(event,this);"/>
    	<img id="dateOut${brokerExamDetailsList.id}_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="updateDatethroughAuto('${brokerExamDetailsList.id}','dateOut');" />
	</display:column>
	
	<display:column  titleKey="brokerExamDetails.amount" style="text-align:left;">
    	<input type="text" class="input-text" style="text-align:right;width:50px" id="brokerExamDetailsList.amount${brokerExamDetailsList.id}" name="brokerExamDetailsList.amount${brokerExamDetailsList.id}" onchange="onlyFloatNew('${brokerExamDetailsList.id}','amount',this,'T');" value="${brokerExamDetailsList.amount}"
    </display:column>
    <display:column  titleKey="brokerExamDetails.billOrCod" style="text-align:left;">
    <select name ="brokerExamDetailsList.billOrCod" style="width:90px" onchange="updateBrokerExamDetails('${brokerExamDetailsList.id}','billOrCod',this,'T');" class="list-menu">
					<option value=""></option>	
			      <c:forEach var="chrms" items="${refExamPay}" varStatus="loopStatus">
			           <c:choose>
		                       <c:when test="${chrms.key == brokerExamDetailsList.billOrCod}">
		                        <c:set var="selectedInd" value=" selected"></c:set>
		                        </c:when>
		                       	<c:otherwise>
		                 		<c:set var="selectedInd" value=""></c:set>
		                  		</c:otherwise>
	                            </c:choose>
			      				<option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
			                    <c:out value="${chrms.value}"></c:out>
			                    </option>
				</c:forEach>		      
		</select>
    </display:column> 
    
	</display:table>
	
	<input type="button" name="brokerExamAddLine" value="Add Line" onclick="addBrokerExamDetailsLine();" class="cssbuttonA" style="width:65px; height:25px;margin-bottom:8px;" tabindex=""/>
   
  </div>
	<s:hidden name="id" ></s:hidden>
	
 <script type="text/javascript">
 <configByCorp:fieldVisibility componentId="component.section.forwarding.brokerDetails">
setOnSelectBasedMethods(["calcDays(),calcDays1(),updateDateExamDetails()"]);
setCalendarFunctionality();
 </configByCorp:fieldVisibility> 
</script>

<script type="text/javascript">
function getHTTPObjectInlandAgent()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var httpexamdetails = getHTTPObjectInlandAgent();
var httpexamdetails1 = getHTTPObjectInlandAgent();
var httpexamDate = getHTTPObjectInlandAgent();
var httpAutoLineCreate = getHTTPObjectInlandAgent();
function updateBrokerExamDetails(columnId,columnName,columnObject,fieldType){
	showOrHide(1); 
	var fieldValue = columnObject.value;
	if(columnName=='amount'){
		if(fieldValue==''){
			fieldValue=0.00;
			document.getElementById(columnObject.id).value="0.00";
		}
	}
	 var url = "updateBrokerExamDetailsAjax.html?ajax=1&id="+columnId+"&fieldName="+columnName+"&fieldValue="+fieldValue+"&fieldType="+fieldType+"&shipNumber="+'${serviceOrder.shipNumber}';
	 httpexamdetails.open("GET", url, true); 
	 httpexamdetails.onreadystatechange = handleHttpResponseExamDetails;
	 httpexamdetails.send(null);	  
}

function handleHttpResponseExamDetails(){
	if (httpexamdetails.readyState == 4){
            var resultsContainer = httpexamdetails.responseText
            resultsContainer = resultsContainer.trim();
            getBrokerExamListDetails();
            showOrHide(0);
	}
}

var autoidInland="";
var dateValueInland="";
var dateFieldInland="";
function updateDatethroughAuto(id2Inland,fieldNameInland){
	autoidInland=id2Inland;
	 dateFieldInland=fieldNameInland;
	 
	}


 function updateDateExamDetails() {        
	 if(dateFieldInland=='dateIn' || dateFieldInland=='dateOut'){
	 showOrHide(1);
	 var dateId=autoidInland;
     var type="D";
     if(document.getElementById(dateFieldInland+dateId)!=null){
     dateValueInland = document.getElementById(dateFieldInland+dateId).value;
     var url = "updateBrokerExamDetailsAjax.html?ajax=1&id="+dateId+"&fieldName="+dateFieldInland+"&fieldValue="+dateValueInland+"&fieldType="+type+"&shipNumber="+'${serviceOrder.shipNumber}';
     httpexamDate.open("GET", url, true); 
     httpexamDate.onreadystatechange = handleHttpResponseExamDate;
     httpexamDate.send(null);
     }
	 }
} 
 
 
 function handleHttpResponseExamDate(){
		if (httpexamDate.readyState == 4){
	            var resultsDate = httpexamDate.responseText
	            resultsDate = resultsDate.trim();
	             autoidInland="";
	             dateValueInland="";
	             dateFieldInland="";
	             getBrokerExamListDetails();
	            showOrHide(0);
		}
	}
 
 
 function updateBrokerDateExamDetails(columnId,columnName,columnObject,fieldType){
		showOrHide(1); 
		var fieldValue = columnObject.value;
		if(fieldValue==''){
			fieldValue="null";
		}
		 var url = "updateBrokerExamDetailsAjax.html?ajax=1&id="+columnId+"&fieldName="+columnName+"&fieldValue="+fieldValue+"&fieldType="+fieldType+"&shipNumber="+'${serviceOrder.shipNumber}';
		 httpexamdetails1.open("GET", url, true); 
		 httpexamdetails1.onreadystatechange = handleHttpResponseExamDetails1;
		 httpexamdetails1.send(null);	  
	}
 
 function handleHttpResponseExamDetails1(){
		if (httpexamdetails1.readyState == 4){
	            var resultsContainer = httpexamdetails1.responseText
	            resultsContainer = resultsContainer.trim();
	            getBrokerExamListDetails();
	            showOrHide(0);
		}
	}
 
  
  function addBrokerExamDetailsLine() {
	  var serviceOrderId='${serviceOrder.id}';
	  $.get("addBrokerExamLine.html?ajax=1&decorator=simple&popup=true", 
			  { sid:serviceOrderId},
				function(data){
					$("#brokerExamListAjax").html(data);
			});
	  
  }
  
	    
  
</script>
<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>
<script type="text/javascript">
function onlyFloatNew(columnId,columnName,columnObject,fieldType){
	  
	var value=columnObject.value;
	var pattern = /^\d+.?\d*$/;
	if(value!=''){
		var n = value.indexOf(".");
		if(n==0){
				 value=0+value;
				 document.getElementById(columnObject.id).value=value;
			 }
		if(n>=2){
			value =parseFloat(value).toFixed(2);
			document.getElementById(columnObject.id).value=value; 
		 }
		if (checkFloatList('brokerDetailsForm',columnObject.id,'Enter valid Number') == false)
	      {
			document.getElementById(columnObject.id).value='0'; 
			document.getElementById(columnObject.id).value.focus();
	         return false;
	      }
		
	if ( value.match(pattern)==null ){
		document.getElementById(columnObject.id).value="";
		alert("Enter valid Number");
		document.getElementById(columnObject.id).focus();
	}else{
		updateBrokerExamDetails(columnId,columnName,columnObject,fieldType);
	}
	}else{
		updateBrokerExamDetails(columnId,columnName,columnObject,fieldType);
	}
}

function checkFloatList(e,t,n){
  var r;
  var i=document.forms[e].elements[t];
  if(i!="undefined"&&i!=null){
	  var s=document.forms[e].elements[t].value;
	  var o=0;
	  for(r=0;r<s.length;r++)
	  {
		  var u=s.charAt(r);if(u==".")
		  {o=o+1}
		  if((u<"0"||u>"9")&&u!="."||o>"1")
		  {
			  alert(n);
			  document.forms[e].elements[t].value="";
			  document.forms[e].elements[t].focus();
			  return false}}}
  return true;
}
</script>
