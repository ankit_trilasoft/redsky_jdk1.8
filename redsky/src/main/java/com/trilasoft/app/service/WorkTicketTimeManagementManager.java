package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import org.hibernate.annotations.GenericGenerators;

import com.trilasoft.app.model.WorkTicketTimeManagement;

public interface WorkTicketTimeManagementManager extends GenericManager<WorkTicketTimeManagement, Long> {
	public List getTimeManagementManagerDetails(Long Id, Long ticket, String sessionCorpID);

}
