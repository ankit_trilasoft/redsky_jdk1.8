<%@ include file="/common/taglibs.jsp"%>  

<head>   
    <title><fmt:message key="costingList.title"/></title>   
    <meta name="heading" content="<fmt:message key='costingList.heading'/>"/>   
</head>   
  <!-- By Madhu -->
  <s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
  <!--  -->  
      
<s:form id="searchForm" method="post" validate="true"> 
<div id="Layer1">


<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>"/></td>
	<td width="100" class="content-tab" align="center"><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}">ServiceOrder</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>"/></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="100" class="content-tab" align="center"><a href="editBilling.html?id=${serviceOrder.id}">Bill</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>"/></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="100" class="content-tab" align="center"><a href="servicePartners.html?id=${serviceOrder.id}" >Partner</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>"/></td>
	<td width="100" class="content-tab" align="center"><a href="containers.html?id=${serviceOrder.id}">Pack</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>"/></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="100" class="content-tab"><a href="editMiscellaneous.html?id=${serviceOrder.id}">Domestic</td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<c:if test="${serviceOrder.job =='RLO'}"> 
	<td width="100" class="content-tab" align="center"><a href="editDspDetails.html?id=${serviceOrder.id}"/>Status</a></td>
	</c:if>
	<c:if test="${serviceOrder.job !='RLO'}"> 
	<td width="100" class="content-tab" align="center"><a href="editTrackingStatus.html?id=${serviceOrder.id}"/>Status</a></td>
	</c:if>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>"/></td>
	<td width="100" class="content-tab" align="center"><a href="customerWorkTickets.html?id=${serviceOrder.id}">Ticket</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>"/></td>
	<td width="4"></td>	
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/head-left.jpg'/>"/></td>
	<td width="100" class="detailActiveTabLabel content-tab" align="center">Invoicing</td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/head-right.jpg'/>"/></td>
	<td width="4"></td>
	<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>"/></td>
	<td width="100" class="content-tab" align="center"><a href="claims.html?id=${serviceOrder.id}" >Claim</a></td>
	<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
 </tr>
</tbody>
</table>  
 
 <table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	<tr><td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td><td align="left" colspan="2"><s:textfield name="serviceOrder.firstName" onfocus="checkDate();"  size="15"  cssClass="input-textUpper" readonly="true"/><td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td><td align="left"><s:textfield name="serviceOrder.originCity"  cssClass="input-textUpper" size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td><td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td><td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="3" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.routing"/></td><td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="1" readonly="true"/></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td><td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="9" readonly="true"/></td><td align="right"><fmt:message key="billing.registrationNo"/></td><td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td><td align="left"><s:textfield name="serviceOrder.destinationCity" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td><td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td><td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="21" readonly="true"/></td></tr>
		  	<tr><td align="left" height="5px"></td></tr>
   		  </tbody>
	  </table>
	  </td></tr>
	</tbody>
 </table>

 
<table cellspacing="0" cellpadding="0" border="0" width="700px">
	<tbody>
	<tr><td height="10px"></td></tr>
	<tr><td>
   	<display:table name="partnerQuotes" class="table" id="quotationCostingList" defaultsort="1" export="true" pagesize="10">   
   
    <display:column property="quoteType" sortable="true" titleKey="jobcosting.quoteType"/>

    <display:column titleKey="jobcosting.vendorCode">
	    <s:select cssClass="list-menu"  cssStyle="width:67px" name="billing.contract" list="{'vendor Code'}"/>
	</display:column>

    <display:column titleKey="jobcosting.name">
	    <s:textfield name="billing.discount" maxlength="15" required="true" cssClass="input-text" size="7"/>
	</display:column>
    

    <display:column titleKey="jobcosting.chargeCode">
	    <s:select cssClass="list-menu"  cssStyle="width:67px" name="billing.contract" list="{'Charge Code'}"/>
	</display:column>

    <display:column property="quote" sortable="true" titleKey="jobcosting.quoteNumber" />
    
    
</display:table>   
  </td></tr></tbody></table>
  </div>
<c:out value="${buttons}" escapeXml="false" />   
</s:form>  

<script type="text/javascript">   
    highlightTableRows("quotationCostingList");   
</script>  
