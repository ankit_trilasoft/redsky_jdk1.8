<%@ include file="/common/taglibs.jsp"%> 

<head>

	<meta name="heading" content="Complaints Extract"/> 
	<title>Complaints Extract</title>
	
<style type="text/css">
h2 {background-color: #FBBFFF}		
</style>

<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
	
<style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;
font-weight: bold;
color: #15428B;
text-decoration: none;
background:url(images/collapsebg.gif) #BCD2EF;
padding:2px 3px 2px 5px;
 height:23px;
 border:1px solid #99BBE8; 
 border-right:none; border-left:none;
  border-top:none;
  margin-top:0px} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}
</style>

</head>
<body style="background-color:#444444;">
<s:form id="complaintsExtractForm" name="complaintsExtractForm" action="complaintsExtractForm" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:90%">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Complaints Extract</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="0" cellpadding="0" border="0" style="width:800px"> 
 	<tbody>
		  	
		  	<tr>
		  		<td align="left" height="10px"></td>
		  	</tr>
			
	  	  	<tr>		  		
		  		<td align="right" width="120px" class="listwhitetext" style="padding-bottom:5px">Date&nbsp;Complaint&nbsp;Received&nbsp;From</td>	
		  		
		  		<td align="left" colspan="7" class="listwhitetext">
		  		
		  		<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	  	  	  <tr>
	  	  	  <td width="160px" style="padding-bottom:5px; padding-left:5px;"> 	 
		  		
		  		<s:textfield cssClass="input-text" id="openfromdate" name="openfromdate" value="%{openfromdate}" size="8" maxlength="11" readonly="true" /> 
		  		<img id="openfromdate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			
				<td align="right" class="listwhitetext" style="padding-bottom:5px">To</td>	
		  		
		  		<td align="left" class="listwhitetext" style="padding-bottom:5px; padding-left:5px;"><s:textfield cssClass="input-text" id="opentodate" name="opentodate" value="%{opentodate}" size="8" maxlength="11" readonly="true" /> 
		  		<img id="opentodate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			
			 </tr>
	  	  	  </table>
	  	  	  </td>
			</tr>
		  	<tr>		  	  		
	  			<td width="50px"></td>			
				<td colspan="4"  style="padding-left:4px;">
				<s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " align="top" method="complaintExtractForm" value="Extract" onclick="return valbutton(complaintsExtractForm);" />	  		 				           	
				<s:reset cssClass="cssbutton" cssStyle="width:55px; height:25px" key="Reset" />
				</td>
        	</tr>
        	<tr>
		  		<td align="left" style="height:10px; !height:20px;"></td>
		  	</tr>
        	</tbody>
        	</table>  
	
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</s:form>

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

<script language="javascript" type="text/javascript">
	function valbutton(thisform) {
	var date1 = document.forms['complaintsExtractForm'].elements['openfromdate'].value;
	var date2 = document.forms['complaintsExtractForm'].elements['opentodate'].value;
	
	if(date1=='' && date2=='')					
	{	
	 return true;	 
	}
	// for open date
	if(date1!=''&& date2=='')
	{ 	
		alert('Please enter open Complaint Received to date');
		document.forms['complaintsExtractForm'].elements['opentodate'].focus();
		return false;
	}
	if(date1==''&& date2!='')
	{ 	
		alert('Please enter open Complaint Received from date');
		document.forms['complaintsExtractForm'].elements['openfromdate'].focus();
		return false; 
	}
		
}
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>