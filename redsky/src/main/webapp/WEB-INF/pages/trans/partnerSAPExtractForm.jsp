<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title><fmt:message key="partnerExtractDetail.title" /></title>
<meta name="heading" content="<fmt:message key='partnerExtractDetail.heading'/>" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script>
<script language="javascript" type="text/javascript">
function fieldValidate(){
 	if(document.forms['partnerExtractForm'].elements['beginDate'].value==''){
 		alert("Please enter the begin date"); 
 		return false;
 	}
 	if(document.forms['partnerExtractForm'].elements['endDate'].value==''){
 		alert("Please enter the end date "); 
 		return false;
 	}
 	//if(document.forms['partnerExtractForm'].elements['companyDivision'].value==''){
 	//alert("Please enter the company Division"); 
 	//return false;
 //	}
 	else{
 		document.forms['partnerExtractForm'].action = 'financeSAPPartnerExtracts.html';
    	document.forms['partnerExtractForm'].submit();
 		return true;
 	}
} 
      
function submitByFile(){  
  	if(document.forms['partnerFileExtractForm'].elements['invoiceFileName'].value==''){
    	alert("Please enter the Extracted Invoice File Name"); 
 		return false;
  	}else {
       	document.forms['partnerFileExtractForm'].action = 'partnerSAPFileExtracts.html';
    	document.forms['partnerFileExtractForm'].submit(); 
     	return true;
   }
}

function submitByInvoice(){
  	if(document.forms['partnerSynInvExtractForm'].elements['sysInvoiceDate'].value==''){
    	alert("Please enter the from date"); 
 		return false;
  	}
  	if(document.forms['partnerSynInvExtractForm'].elements['invCompanyDivision'].value==''){
       	alert("Please enter the company division "); 
		return false;
	}else{
       	document.forms['partnerSynInvExtractForm'].action = 'partnerSAPSynInvExtracts.html';
    	document.forms['partnerSynInvExtractForm'].submit(); 
  		return true;
   	}
}
  
function fieldValidateAgent(){
       if(document.forms['partnerAgentExtractForm'].elements['beginDateAgent'].value == ''){
       		alert("Please enter the begin date"); 
       		return false;
       }
       if(document.forms['partnerAgentExtractForm'].elements['endDateAgent'].value == ''){
       		alert("Please enter the end date "); 
       		return false;
       }else{
       		document.forms['partnerAgentExtractForm'].action = 'agentFinanceSAPExtracts.html';
          	document.forms['partnerAgentExtractForm'].submit();
       		return true;
       }
}    
</script>
</head>
<div id="Layer1" style="width:85%;">
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top:10px;"><span></span></div>
<div class="center-content">
<table border="0"><tr><td>
<s:form name="partnerExtractForm" id="partnerExtractForm" action="financeSAPPartnerExtracts" method="post" validate="true">
	<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>
<table class="" cellspacing="1" cellpadding="3" border="0" style="margin-bottom:2px">
<tr>
<td width="5px"></td>
<td colspan="7" class="bgblue">Scenario # 1 : Partner Data Extract </td></tr>

	  		<tr>
	  			<td height="5px"></td>
	  			
	  		</tr>
  			<tr> 
  			<td width=""></td>
				<td class="listwhitetext" align="right">Begin Date<font color="red" size="2">*</font></td>
					<c:if test="${not empty beginDate}">
						<s:text id="beginDate" name="${FormDateValue}"><s:param name="value" value="beginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" value="%{beginDate}" size="10" maxlength="11" readonly="true" /> <img id="beginDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
					</c:if>
					<c:if test="${empty beginDate}">
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" required="true" size="10" maxlength="11" readonly="true" /> <img id="beginDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
					</c:if>
				      	
				<td class="listwhitetext" align="right">End Date<font color="red" size="2">*</font></td>
		 			<c:if test="${not empty endDate}">
						<s:text id="endDate" name="${FormDateValue}"> <s:param name="value" value="endDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" size="10" maxlength="11" readonly="true" /> <img id="endDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
					</c:if>
					<c:if test="${empty endDate}" >
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" required="true" size="10" maxlength="11" readonly="true" /> <img id="endDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
					</c:if>	
				<td align="right" class="listwhitetext">Company Division</td>
				<td align="left"><s:select cssClass="list-menu" id="companyDivision" name="companyDivision" list="%{companyDivis}" cssStyle="width:100px" headerKey="" headerValue=""/></td>		 	
			</tr> 
			<tr>
	  			<td width="" height="10px"></td>
	  		</tr> 
	  		<tr>	
	  			<td width=""></td>			
				<td colspan="4" style="padding-left: 20px"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" value="Extract" onclick=" return fieldValidate();"/></td>
	    	</tr>	
	    	 				
	</table> 
<s:text id="customerFileMoveDateFormattedValue" name="FormDateValue"><s:param name="value" value="refMaster.stopDate"/></s:text>
<s:hidden name="refMaster.stopDate" /> 
</s:form>
</td></tr>
<tr><td>
<s:form name="partnerFileExtractForm" id="partnerFileExtractForm" action="partnerSAPFileExtracts" method="post" validate="true">
<table class="" cellspacing="1" cellpadding="3" border="0" style="">
<tr><td height="10px"></td></tr>
<tr>
<td width="5px"></td>
<td colspan="7" class="bgblue">Scenario # 2 : Partner Data Extract With Invoice File</td></tr>


	  		<td height="5px"></td>
	  		 
  			<tr> 
  			<td width=""></td> 
				<td align="right" width="150px"  class="listwhitetext">Extracted Invoice File Name</td>
				<td align="left" width="600px"><s:textfield cssClass="input-text" id="invoiceFileName" name="invoiceFileName" size="10" maxlength="20" /> </td>
			</tr> 
			 <tr><td height="5px"></td></tr>
	  		<tr>	
	  			<td width=""></td>			
				<td colspan="4" style="padding-left: 20px"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" value="Extract" onclick=" return submitByFile();"/></td>
	    	
	    	</tr>	 
	    				
	
	</table>  
	
</s:form>
</td></tr>

<tr><td>
<s:form name="partnerSynInvExtractForm" id="partnerSynInvExtractForm" action="partnerSAPSynInvExtracts" method="post" validate="true">
<c:set var="FormDateValue1" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat1" name="dateFormat1" value="dd-NNN-yy"/>
<table class="" cellspacing="1" cellpadding="3" border="0" style="">

<tr>
<td width="5px"></td>
<td colspan="7" class="bgblue">Scenario # 3 : Partner Data Extract With Synchronize Invoice</td></tr>
             <tr>
          <td height="5px"></td>   
          </tr>
  			<tr>
           <td width=""></td>
  			<td class="listwhitetext" align="right">From Date<font color="red" size="2">*</font></td>
		 			<c:if test="${not empty sysInvoiceDate}">
						<s:text id="sysInvoiceDate" name="${FormDateValue1}"> <s:param name="value" value="sysInvoiceDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="sysInvoiceDate" name="sysInvoiceDate" value="%{sysInvoiceDate}" size="10" maxlength="11" readonly="true" /> <img id="sysInvoiceDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
					</c:if>
					<c:if test="${empty sysInvoiceDate}" >
						<td><s:textfield cssClass="input-text" id="sysInvoiceDate" name="sysInvoiceDate" required="true" size="10" maxlength="11" readonly="true" /> <img id="sysInvoiceDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
					</c:if>	
				<td align="right" class="listwhitetext">Company Division</td>
				<td align="left"><s:select cssClass="list-menu" id="invCompanyDivision" name="invCompanyDivision" list="%{companyDivis}" cssStyle="width:100px" headerKey="" headerValue=""/></td>		 	
	  		<td width="200"></td><td></td>
	  		</tr> 
	  		 <tr>
          <td height="5px"></td>   
          </tr>
	  		<tr>	
	  			<td width=""></td>			
				<td colspan="4" style="padding-left: 20px"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" value="Extract" onclick=" return submitByInvoice();"/></td>
	    	
	    	</tr>	 				
	
	</table>  
</s:form>
</td></tr>

<tr><td>
<s:form name="partnerAgentExtractForm" id="partnerAgentExtractForm" action="agentFinanceSAPExtracts" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<table class="" cellspacing="1" cellpadding="3" border="0" style="margin:0px;">

	<tr>
		<td width="5px"></td>
		<td colspan="7" class="bgblue">Scenario # 4 : Agent Data Extract</td>
	</tr>
	<tr><td height="5px"></td></tr>
  	<tr> 
  		<td width=""></td>
		<td class="listwhitetext" align="right">Begin Date<font color="red" size="2">*</font></td>
			<c:if test="${not empty beginDateAgent}">
				<s:text id="beginDateAgent" name="${FormDateValue}"><s:param name="value" value="beginDateAgent" /></s:text>
				<td><s:textfield cssClass="input-text" id="beginDateAgent" name="beginDateAgent" value="%{beginDateAgent}" size="10" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/> <img id="beginDateAgent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
			</c:if>
			<c:if test="${empty beginDateAgent}">
				<td><s:textfield cssClass="input-text" id="beginDateAgent" name="beginDateAgent" required="true" size="10" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/> <img id="beginDateAgent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
			</c:if>
				      	
		<td class="listwhitetext" align="right">End Date<font color="red" size="2">*</font></td>
			<c:if test="${not empty endDateAgent}">
				<s:text id="endDateAgent" name="${FormDateValue}"> <s:param name="value" value="endDateAgent" /></s:text>
				<td><s:textfield cssClass="input-text" id="endDateAgent" name="endDateAgent" value="%{endDateAgent}" size="10" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/> <img id="endDateAgent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
			</c:if>
			<c:if test="${empty endDateAgent}" >
				<td><s:textfield cssClass="input-text" id="endDateAgent" name="endDateAgent" required="true" size="10" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/> <img id="endDateAgent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
			</c:if>
				
		<td align="right" class="listwhitetext">Company Division</td>
		<td align="left"><s:select cssClass="list-menu" id="companyDivision" name="companyDivision" list="%{companyDivis}" cssStyle="width:100px" headerKey="" headerValue=""/></td>		 	
	</tr> 
	<tr><td width="" height="10px"></td></tr> 
	<tr>	
		<td width=""></td>			
		<td colspan="4" style="padding-left: 20px"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" value="Extract" onclick="return fieldValidateAgent();"/></td>
 	</tr>	
	<tr><td width="" height="5px"></td></tr>
</table> 
</s:form>
</td></tr>

</table>
</div>
<div class="bottom-header"><span></span></div> 
</div>
</div>
</div>
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>