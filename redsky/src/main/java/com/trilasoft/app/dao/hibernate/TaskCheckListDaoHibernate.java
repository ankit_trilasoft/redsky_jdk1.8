package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.TaskCheckListDao;
import com.trilasoft.app.model.TaskCheckList;

public class TaskCheckListDaoHibernate extends GenericDaoHibernate<TaskCheckList, Long> implements TaskCheckListDao{

	private List taskCheckList;
	
	public TaskCheckListDaoHibernate() {
		super(TaskCheckList.class);
	}

	public List getById() {
		return null;
	}

	
	public TaskCheckList getTaskCheckListData(String shipnumber, Long id){
		taskCheckList = getHibernateTemplate().find("From TaskCheckList WHERE fileNumber= '"+shipnumber+"' and todoRuleId= '"+id+"' ORDER BY resultRecordType");
		if(taskCheckList!=null && !taskCheckList.isEmpty() && taskCheckList.get(0)!=null && !taskCheckList.get(0).toString().equalsIgnoreCase("")){
		return (TaskCheckList) taskCheckList.get(0);
		}else{
			return null;
		}
	}
	
}
