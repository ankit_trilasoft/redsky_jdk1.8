/**
 * This class represents the basic "TrackingStatus" object in Redsky that allows for TrackingStatus of Shipment.
 * @Class Name	TrackingStatus
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.model;

public class TrackingStatusView  {
	
	 
	private Long id; 
	private String shipNumber; 
    private String contractType=new String ("");
    private Boolean soNetworkGroup = new Boolean(false);
    private Boolean accNetworkGroup = new Boolean(false);
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public Boolean getSoNetworkGroup() {
		return soNetworkGroup;
	}
	public void setSoNetworkGroup(Boolean soNetworkGroup) {
		this.soNetworkGroup = soNetworkGroup;
	}
	public Boolean getAccNetworkGroup() {
		return accNetworkGroup;
	}
	public void setAccNetworkGroup(Boolean accNetworkGroup) {
		this.accNetworkGroup = accNetworkGroup;
	}
    
}
	 

	 
	


	


	