<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%> 
<style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}

table.pickList111 td select {
    width: 400px;
}
</style>

<head>
<script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>
    <title><fmt:message key="userProfile.title"/></title>
    <meta name="heading" content="<fmt:message key='userProfile.heading'/>"/>
    
    <script type="text/javascript">
   window.onload = function(){
   var testnew='${sentMailFlag}';
       if(testnew!='Y'){
    	var defaultValNameList = ['user.phoneNumber','user.faxNumber','user.address.address',
    								'user.address.city','user.address.country','user.address.province',
    								'user.address.postalCode'];
    	var Address='${partnerPublic.billingAddress1}'+" "+'${partnerPublic.billingAddress2}'+" "+'${partnerPublic.billingAddress3}'+" "+'${partnerPublic.billingAddress4}';							

    	var defaultValueList = ['${partnerPublic.billingPhone}','${partnerPublic.billingFax}',
    							Address,
    							'${partnerPublic.billingCity}','${partnerPublic.billingCountry}',
    							'${partnerPublic.billingState}','${partnerPublic.billingZip}'];		
		for(var i = 0; i < defaultValNameList.length; i++){
			if(document.getElementsByName(defaultValNameList[i])[0].type == 'text'){
				document.getElementsByName(defaultValNameList[i])[0].value = defaultValueList[i];
			}
			else{
				var dropDownList = document.getElementsByName(defaultValNameList[i])[0];
				for(var j = 0 ; j < dropDownList.length; j++){
					if(dropDownList[j].text == defaultValueList[i])
						dropDownList[j].selected = true;
				}
			}
		}
    }
   }
    function showOrHide(value) { 
        if (value==0) {
           	if (document.layers)
               document.layers["overlay"].visibility='hide';
            else
               document.getElementById("overlay").style.visibility='hidden';
       	}else if (value==1) {
       		if (document.layers)
              document.layers["overlay"].visibility='show';
           	else
              document.getElementById("overlay").style.visibility='visible';
       	} }
   	function fieldValidate(){
   		selectAll('userPermissionValue');
 		 <c:if test="${partnerType=='AG'}">
 		selectAll('userJobType');
 		</c:if>
 		 <c:if test="${partnerType=='VN'}">
  		selectAll('userJobType');
  		</c:if>
 	  	var tempType='${partnerType}';
 	   var ListBoxOptions = document.getElementsByName('userJobType')[0].options;
 	  	
 		if(document.forms['userForm'].elements['user.firstName'].value=='')
 		{
 			alert("Please enter the first Name");
 			document.forms['userForm'].elements['user.firstName'].focus();
 			return false;
 		}
 		if(document.forms['userForm'].elements['user.lastName'].value=='')
 		{
 			alert("Please enter the last Name");
 			document.forms['userForm'].elements['user.lastName'].focus();
 			return false;
 		}
 		if(document.forms['userForm'].elements['user.userTitle'].value=='' && (tempType=='AC' || tempType=='AG' || tempType=='VN')){
 	        alert('Please enter the Job Title');
 	        alert(document.forms['userForm'].elements['user.userTitle'].value)
 	        document.forms['userForm'].elements['user.userTitle'].focus();
 	        return false;
 	    }
 		if(document.forms['userForm'].elements['user.email'].value=='')
 		{
 			alert("Please enter the email");
 			document.forms['userForm'].elements['user.email'].focus();
 			return false;
 		}
		
 		if(document.forms['userForm'].elements['user.address.city'].value=='')
 		{
 			alert("Please enter the city");
 			document.forms['userForm'].elements['user.address.city'].focus();
 			return false;
 		}
 		if(document.forms['userForm'].elements['user.address.country'].value=='')
 		{
 			alert("Please enter the country");
 			document.forms['userForm'].elements['user.address.country'].focus();
 			return false;
 		}
 	 	var total = '0';
       for(var i = 0; i < ListBoxOptions.length; i++) {
              
              	total = total+1;
          
      }
      if(total == 0 && tempType=='AG')
      {
      	alert("Please select job function.");
      	return false;
      }
      if(total == 0 && tempType=='VN')
      {
      	alert("Please select job function.");
      	return false;
      }
	return echeck();	
   	}  
  	function validateFields()
  	{ 
  		selectAll('userRolesValue');
  		selectAll('userPermissionValue');
  		 <c:if test="${partnerType=='AG'}">
  		selectAll('userJobType');
  		</c:if>
  	  	var tempType='${partnerType}';
  	  	
  		if(document.forms['userForm'].elements['user.firstName'].value=='')
  		{
  			alert("Please enter the first Name");
  			document.forms['userForm'].elements['user.firstName'].focus();
  			return false;
  		}
  		if(document.forms['userForm'].elements['user.lastName'].value=='')
  		{
  			alert("Please enter the last Name");
  			document.forms['userForm'].elements['user.lastName'].focus();
  			return false;
  		}
  		if(document.forms['userForm'].elements['user.username'].value=='' )
  		{
  			alert("Please enter the userName");
  			document.forms['userForm'].elements['user.username'].focus();
  			return false;
  		}
  		
  		//if(document.forms['userForm'].elements['user.password'].value=='')
  		//{
  		//	alert("Please enter the password");
  		//	document.forms['userForm'].elements['user.password'].focus();
  		//	return false;
  		//}
  		
  		//if(document.forms['userForm'].elements['user.confirmPassword'].value=='')
  		//{
  		//	alert("Please enter the confirm passworsd");
  		//	document.forms['userForm'].elements['user.confirmPassword'].focus();
  		//	return false;
  		//}
  		
  		//if(document.forms['userForm'].elements['user.password'].value!=document.forms['userForm'].elements['user.confirmPassword'].value)
  		//{
  		//	alert("Please enter the same password in the confirm passworsd");
  		///	document.forms['userForm'].elements['user.confirmPassword'].value='';
  		//	document.forms['userForm'].elements['user.confirmPassword'].focus();
  		//	return false;
  		
  		//}
  		
  		if(document.forms['userForm'].elements['user.passwordHintQues'].value!=' DEFAULT')
  		{
  			if(document.forms['userForm'].elements['user.passwordHint'].value=='')
	  		{
	  			alert("Please enter the password hint ans");
	  			document.forms['userForm'].elements['user.passwordHint'].focus();
	  			return false;
	  		}
  			
  		}
  		
  		if(document.forms['userForm'].elements['user.passwordHint'].value=='' )
  		{
  			alert("Please enter the password hint ans");
  			document.forms['userForm'].elements['user.passwordHint'].focus();
  			return false;
  		}
  		if(document.forms['userForm'].elements['user.userTitle'].value=='' && (tempType=='AC' || tempType=='AG') ){
  	        alert('Please enter the Job Title');
  	        document.forms['userForm'].elements['user.userTitle'].focus();
  	        return false;
  	    }
  		if(document.forms['userForm'].elements['user.email'].value=='')
  		{
  			alert("Please enter the email");
  			document.forms['userForm'].elements['user.email'].focus();
  			return false;
  		}
		
  		if(document.forms['userForm'].elements['user.address.city'].value=='')
  		{
  			alert("Please enter the city");
  			document.forms['userForm'].elements['user.address.city'].focus();
  			return false;
  		}
  		
  		//if(document.forms['userForm'].elements['user.address.postalCode'].value=='')
  		//{
  			//alert("Please enter the zip");
  			//document.forms['userForm'].elements['user.address.postalCode'].focus();
  			//return false;
  		//}
  		
  		if(document.forms['userForm'].elements['user.address.country'].value=='')
  		{
  			alert("Please enter the country");
  			document.forms['userForm'].elements['user.address.country'].focus();
  			return false;
  		}
  		var per = document.getElementById('userPermissionValue');
  		 var check=false;
  		
  	     for(var i = 0; i < per.length; i++)
  	      {
  	    	 if(per[i].value.indexOf('DATA_SECURITY_SET_SO_BILLTOCODE_')>-1 && tempType=='AC')
  	    		{ 
  	    		check=true;
  	    		 break;
  	    		 }
  	        }
  	   if(!check && tempType=='AC'){
      	 alert("Please assign Permissions set for account portal.");
      	 return false;
       } 
  		var listBoxOptions = document.getElementsByName('userJobType')[0].options;
  		try{
	  	 	var total = '0';
	        for(var i = 0; i < listBoxOptions.length; i++) {
	           total = total+1;
	        }
  		}catch(e){}
       if(total == 0 && tempType=='AG')
       {
       	alert("Please select job function.");
       	return false;
       }
     
       var listuserRoles = document.getElementsByName('userRolesValue')[0].options;
       var count='0';
       try{
    		for(var i = 0; i < listuserRoles.length; i++) {
    			count = count+1;
            }
       }catch(e){}
   if(count == '0')
    {
  	  alert("Please select Roles.");
  	  return false;
      }
	return echeck();
}
  		

  	
  	
  	function findUserName()
  	{
  		var userName=document.forms['userForm'].elements['user.username'].value;
  		if(userName==''){
  			 alert('Please enter the Username');	
  		}else{
  			var url="validateUser.html?decorator=simple&popup=true&userName=" + encodeURI(userName);
  			http2.open("GET", url, true);
  	    	http2.onreadystatechange = handleHttpResponseValidateUser;
  	     	http2.send(null);
  		}
  		
  	
  	}
  	
  	function handleHttpResponseValidateUser()
        {
             if (http2.readyState == 4)
             {
             	 var results = http2.responseText
                 results = results.trim();
                 alert(results);
             }
        }
        
        
    String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g,"");
	}
	String.prototype.ltrim = function() {
	    return this.replace(/^\s+/,"");
	}
	String.prototype.rtrim = function() {
	    return this.replace(/\s+$/,"");
	}
	String.prototype.lntrim = function() {
	    return this.replace(/^\s+/,"","\n");
	}
	
	
	function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
	}
    var http2 = getHTTPObject();


    function getResetPwdAndSendMail(){
      var enabled = document.forms['userForm'].elements['enabled'].value;
      var userId = document.forms['userForm'].elements['userId'].value;
      var partnerType = document.forms['userForm'].elements['partnerType'].value;
      var emailId = document.forms['userForm'].elements['user.email'].value;
      if(enabled=='true'){
      if(partnerType=='AG' || partnerType=='AC' || partnerType=='PP'){
	      if(emailId!=''){
	     	 window.open('resetPwdAndSendEmail.html?userId='+userId+'&emailTo='+emailId+'&partnerType='+partnerType+'&decorator=popup&popup=true&from=file','','width=550,height=170') ;
	     	 document.forms['userForm'].elements['save'].disabled=true;
		    }else{
	       	alert("Please enter user Email Id!");
	      } 
        }
      }else{
    		alert("Agent is not activated!");
      }
    }
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==190) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}
	function findbasedAt(){
		showOrHide(1);
	    var basedAtCode = document.forms['userForm'].elements['user.basedAt'].value;
	       if(basedAtCode==''){
	    	document.forms['userForm'].elements['user.basedAtName'].value="";
	    	showOrHide(0);
		}else{
	     var url="basedAtName.html?decorator=simple&popup=true&basedAtCode=" + encodeURI(basedAtCode);
	     http9.open("GET", url, true);
	     http9.onreadystatechange = handleHttpResponse2;
	     http9.send(null);
	    }
	}	
		function handleHttpResponse2(){
	             if (http9.readyState == 4){
	                var results = http9.responseText
	                results = results.trim();
	                results=results.replace("[",'');
	                results=results.replace("]",'');
	                if(results.length >= 1){ 
	                document.forms['userForm'].elements['user.basedAtName'].value=results;
	                showOrHide(0);
	                return true;
	                }
	                else{
	                alert("Based at code not valid");    
					document.forms['userForm'].elements['user.basedAt'].value="";
					document.forms['userForm'].elements['user.basedAtName'].value="";
					showOrHide(0);
					return false;
	                }
	           }
	                
	}
	var http9 = getHTTPObject();
	function getHTTPObject()
	{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}
	function updatePartnerPrivate(){
		 var excludeFPU = document.getElementById("excludeFPU");	
		 if(excludeFPU.checked==true)
		 {
			 var email=document.forms['userForm'].elements['user.email'].value;
			 if(email=="")
			 {
				 alert("Please Enter E-mail.");
				 document.getElementById("excludeFPU").checked=false;
			 }else{
				 document.forms['userForm'].elements['user.username'].value=email;
			 }
		 }
	}	
	function checkContractField()
	{
		showOrHide(1);
		<c:if test="${agentContact!='true'}">
		var userN=document.forms['userForm'].elements['user.username'].value;
		var email=document.forms['userForm'].elements['user.email'].value;
   	    var excludeFPU = document.getElementById("excludeFPU");	
	        if((excludeFPU.checked==true)&&(email!=userN)){
	        	 var agree=confirm("User Name and email address are different, do you still want to continue?");		        
	            if(!agree){
	            showOrHide(0);
	            return false;
	            }
	        } else{
	        	submit_form();
		        }
	    	</c:if>
	        
	}
	 function disabledAll(){
			var elementsLen=document.forms['userForm'].elements.length;
			for(i=0;i<=elementsLen-1;i++){
				if(document.forms['userForm'].elements[i].type=='text'){
					document.forms['userForm'].elements[i].readOnly =true;
					document.forms['userForm'].elements[i].className = 'input-textUpper';
				}else if(document.forms['userForm'].elements[i].type=='textarea'){
					document.forms['userForm'].elements[i].readOnly =true;
					document.forms['userForm'].elements[i].className = 'textareaUpper';
				}else{
					document.forms['userForm'].elements[i].disabled=true;
				} 
			}
		}
	 var httpRole = getHTTPRole();
	 function getHTTPRole(){
	     var xmlhttp;
	     if(window.XMLHttpRequest)    {
	         xmlhttp = new XMLHttpRequest();
	     }
	     else if (window.ActiveXObject)    {
	         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	         if (!xmlhttp)        {
	             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	         }  }
	     return xmlhttp;
	 }

	 function feedRoleOnChange(){
		   var category='${partnerType}';
		     if(category=='AC'){
		    	 category='ACCOUNT';
		     }else if(category=='AG'){
		    	 category='AGENT';
		     }else if(category=='AG'){
		    	 category='PARTNER'; 
		     }
	 		var url="ajaxRoleBasedOnCategory.html?decorator=simple&popup=true&category=" + category + "&leftId=availRoles && !userRolesValue&rightId=userRolesValue";		
	 		httpRole.open("GET", url, true);
	 		httpRole.onreadystatechange = httpRoleName;
	 		httpRole.send(null);		
	 	} 
	 function httpRoleName(){
	 	 if (httpRole.readyState == 4){
	          var results = httpRole.responseText;
	          results = results.trim();
	          if(document.getElementById('availRoles && !userRolesValue') != null){
	         	 document.getElementById('availRoles && !userRolesValue').innerHTML = results;
	          }else{
	         	 document.getElementById('availRoles').innerHTML = results;
	          }
	 	 }
	 }
	 function validateRole()
	 {
		 <c:if test="${partnerType=='AG'}">
		 <c:if test="${agentContact!='true'}">
	 	 var ListPermissionOptions = document.getElementsByName('userPermissionValue')[0].options;
	         var setTotal='0';
	             for(var i = 0; i < ListPermissionOptions.length; i++) {           
	               
	                 if((ListPermissionOptions[i].value.indexOf('DATA_SECURITY_SET_AGENT_')> -1)  || (ListPermissionOptions[i].value.indexOf('DATA_SECURITY_SET_SO_')> -1))
	                 {
	                 	setTotal = setTotal+1;
	                 }
	         }
	             if(setTotal == 0)
	             {
	             	alert("Please select Any Set For AGENT Portal");
	             	return false;
	             }
	         </c:if>
	         </c:if>
	         return true;
	 }
    </script>
</head>
<s:form name="userForm" id="userForm" action="saveUserFromPartner" enctype="multipart/form-data" onsubmit="return checkContractField();" method="post" validate="true"  >
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<c:if test="${partnerType==''}">
<c:set var="partnerType" value="AG" />
</c:if>
<c:if test="${not empty user.pwdexpiryDate}">
<s:text id="pwdexpiryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="user.pwdexpiryDate"/></s:text>
<s:hidden id="pwdexpiryDate" name="user.pwdexpiryDate" value="%{pwdexpiryDateFormattedValue}" cssStyle="width:65px" />
</c:if>	
<c:set var="NPSAccess" value="false" />
 <configByCorp:fieldVisibility componentId="component.user.venderAccess.showHSRG">
 <c:set var="NPSAccess" value="true" />
 </configByCorp:fieldVisibility>							
<div id="newmnav">
  	<ul>
  	<c:if test="${partnerType=='AG'}">
  		<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
  			<li><a href="editPartnerPublic.html?partnerType=${partnerType}&id=${partnerPublic.id}"><span>Agent Detail</span></a></li>
  			<%-- Added By kunal for ticket number: 6176 --%>
	  			<c:if test="${not empty partnerPublic.id}">
					<configByCorp:fieldVisibility componentId="component.button.partnverPublicScript">
						<c:if test="${partnerType == 'AG'}">
							<li><a href="findPartnerProfileList.html?code=${partnerPublic.partnerCode}&partnerType=${partnerType}&id=${partnerPublic.id}"><span>Agent Profile</span></a></li>
						</c:if>
					</configByCorp:fieldVisibility>
				</c:if>
				<c:if test="${sessionCorpID!='TSFT' }">
		  	<li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Additional Info</span></a></li>
		  	</c:if>
		  	<c:if test="${partnerType == 'AG'}">
				<c:if test="${not empty partnerPublic.id}">
					<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
					<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
					<li><a href="baseList.html?id=${partnerPublic.id}"><span>Base</span></a></li>
					<c:if test="${paramValue == 'View'}">
						<li><a href="partnerView.html"><span>Partner List</span></a></li>
					</c:if>
					<c:if test="${paramValue != 'View'}">
						<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
					</c:if>	
					<!--<c:if test='${partnerPublic.latitude == ""  || partnerPublic.longitude == "" || partnerPublic.latitude == null  || partnerPublic.longitude == null}'>
						<li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>
					</c:if>
					<c:if test='${partnerPublic.latitude != "" && partnerPublic.longitude != "" && partnerPublic.latitude != null && partnerPublic.longitude != null}'>
						<li><a href="partnerRateGrids.html?partnerId=${partnerPublic.id}"><span>Rate Matrix</span></a></li>
					</c:if>
				  	--><li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
			   </c:if>
	  	   </c:if>	
		  	<li><a href="partnerUsersList.html?id=${id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
		  	<configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
		  	<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
		  	</configByCorp:fieldVisibility><!--
		    <li><a href="partnerUserUnassignedList.html?id=${id}&partnerType=${partnerType}"><span>Unassigned Agent Users</span></a></li>
		    --><li id="newmnav1" style="background:#FFF"><a class="current""><span>Portal Users & Contacts Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		     <sec-auth:authComponent componentId="module.script.partnerUser.RequestedPartnerAccess">
			  	<li><a href="requestedPartnerAccessList.html?id=${id}&partnerType=${partnerType}"><span>Requested Partner Access</span></a></li>
	  	        </sec-auth:authComponent>
	        </sec-auth:authComponent>
	    <sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
		   	<li><a href="editPartnerPublic.html?partnerType=AG&id=${partnerPublic.id}"><span>Agent Details</span></a></li>
		   	<li><a href="partnerUsersList.html?id=${partnerPublic.id}"><span>Portal Users & contacts</span></a></li>
			<li id="newmnav1" style="background:#FFF"><a class="current""><span>User Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		   	<!--<c:if test='${partnerPublic.latitude == ""  || partnerPublic.longitude == "" || partnerPublic.latitude == null  || partnerPublic.longitude == null}'>
			   <li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
			 </c:if>
			 <c:if test='${partnerPublic.latitude != "" && partnerPublic.longitude != "" && partnerPublic.latitude != null && partnerPublic.longitude != null}'>
			   <li><a href="partnerRateGrids.html?partnerId=${partnerPublic.id}"><span>Rate Matrix</span></a></li>
			 </c:if>
		   	
		    --><li><a href="partnerAgent.html?partnerType=${partnerType}"><span>Agent List</span></a></li>
		</sec-auth:authComponent>
	</c:if>
	<configByCorp:fieldVisibility componentId="component.user.venderAccess.showHSRG">
	<c:if test="${partnerType == 'VN'}">
	 <li><a href="editPartnerPublic.html?partnerType=${partnerType}&id=${partnerPublic.id}"><span>Vendor Detail</span></a></li>
	 <c:if test="${sessionCorpID!='TSFT' }">
	 <li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerCode=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Additional Info</span></a></li>
	 </c:if>
	 <li><a href="partnerUsersList.html?id=${id}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
	 <li id="newmnav1" style="background:#FFF"><a class="current""><span>Portal Users & Contacts Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	 <li><a href="partnerUsersList.html?id=${id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
	 </c:if>
	 </configByCorp:fieldVisibility>
	<c:if test="${partnerType=='AC'}">
	<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
  			<li><a href="editPartnerPublic.html?partnerType=${partnerType}&id=${partnerPublic.id}"><span>Account Detail</span></a></li>
  			<li><a href="editNewAccountProfile.html?id=${id}&partnerType=${partnerType}"><span>Account Profile</span></a></li>
  			<c:if test="${sessionCorpID!='TSFT' }">
		  	<li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Additional Info</span></a></li>
		  	</c:if>	
		  	<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
				<li><a href="accountContactList.html?id=${id}&partnerType=${partnerType}"><span>Account Contact</span></a></li>
				<c:if test="${checkTransfereeInfopackage==true}">
					<li><a href="editContractPolicy.html?id=${id}&partnerType=${partnerType}"><span>Policy</span></a></li>
				</c:if>	  	
		  	<li><a href="partnerUsersList.html?id=${id}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
		  	<c:if test="${paramValue == 'View'}">
					<li><a href="searchPartnerView.html"><span>Public List</span></a></li>
				</c:if>
				<c:if test="${paramValue != 'View'}">
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'AC'}">
					<li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
				</c:if>
					<c:if test="${partnerType == 'AC'}">
				 <c:if test="${not empty partnerPublic.id}">
				<c:url value="frequentlyAskedQuestionsList.html" var="url">
				<c:param name="partnerCode" value="${partnerPublic.partnerCode}"/>
				<c:param name="partnerType" value="AC"/>
				<c:param name="partnerId" value="${partnerPublic.id}"/>
				<c:param name="lastName" value="${partnerPublic.lastName}"/>
				<c:param name="status" value="${partnerPublic.status}"/>
				</c:url>				 
				<li><a href="${url}"><span>FAQ</span></a></li>
				  </c:if>
				</c:if>	
					<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
		    <li><a href="partnerUserUnassignedList.html?id=${id}&partnerType=${partnerType}"><span>Unassigned Portal Users</span></a></li>
		    <li id="newmnav1" style="background:#FFF"><a class="current""><span>Portal User Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	    </sec-auth:authComponent>
	    <sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
		   	<li><a href="editPartnerPublic.html?partnerType=AG&id=${partnerPublic.id}"><span>Account Details</span></a></li>
		   	<li><a href="partnerUsersList.html?id=${partnerPublic.id}"><span>Portal Users</span></a></li>
			<li id="newmnav1" style="background:#FFF"><a class="current""><span>Portal User Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		   	<!--<c:if test='${partnerPublic.latitude == ""  || partnerPublic.longitude == "" || partnerPublic.latitude == null  || partnerPublic.longitude == null}'>
			   <li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
			 </c:if>
			 <c:if test='${partnerPublic.latitude != "" && partnerPublic.longitude != "" && partnerPublic.latitude != null && partnerPublic.longitude != null}'>
			   <li><a href="partnerRateGrids.html?partnerId=${partnerPublic.id}"><span>Rate Matrix</span></a></li>
			 </c:if>
		   	
		    --><li><a href="partnerAgent.html?partnerType=${partnerType}"><span>Portal List</span></a></li>
		</sec-auth:authComponent>
	</c:if>
	<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${user.id}&tableName=app_user,user_role,userdatasecurity&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
	</ul>
	</div><div class="spn">&nbsp;</div>
<s:hidden name="user.id"/>
<s:hidden name="user.version"/>
<s:hidden name="user.corpID"/>
<s:hidden name="user.doNotEmail"/>
<s:hidden name="user.login_time"/>
<s:hidden name="user.login_status"/>
<s:hidden name="user.password" value="${user.password}"/>
<s:hidden name="user.confirmPassword" value="${user.confirmPassword}"/>
<s:hidden name="enabled" value="${user.enabled}"/>
<c:set var="agentContact" value="<%= request.getParameter("agentContact")%>" />
<s:hidden name="agentContact" value="<%= request.getParameter("agentContact")%>" />
<c:set var="id" value="<%= request.getParameter("id")%>" />
<s:hidden name="id" value="<%= request.getParameter("id")%>" />	

<s:hidden name="userId" value="<%=request.getParameter("userId") %>" />	
	<s:if test="user.version == null">
    	<input type="hidden" name="encryptPass" value="true" />
	</s:if>
<configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
<c:if test="${partnerType=='AG'}">
	<c:set var="buttons">
	<s:submit cssClass="cssbuttonA" key="button.save" name="save" onclick="return validateFields();"/>

		<s:reset type="button" cssClass="cssbutton1" key="Reset" cssStyle="!margin-left:8px;"/>
	</c:set>
</c:if>
</configByCorp:fieldVisibility>
<c:if test="${partnerType=='AG' && agentContact=='true'}">
<c:set var="buttons">
<s:submit cssClass="cssbuttonA" key="button.save" name="save" onclick="return fieldValidate();" onmouseover="return validateRole();"/>
<s:reset type="button" cssClass="cssbutton1" key="Reset" cssStyle="!margin-left:8px;"/>
</c:set>
</c:if>
<c:if test="${partnerType=='VN' && agentContact=='true'}">
<c:set var="buttons">
<s:submit cssClass="cssbuttonA" key="button.save" name="save" onclick="return fieldValidate();" onmouseover="return validateRole();"/>
<s:reset type="button" cssClass="cssbutton1" key="Reset" cssStyle="!margin-left:8px;"/>
</c:set>
</c:if>
<c:if test="${partnerType!='AG' && partnerType!='VN'}">
	<c:set var="buttons">
	<s:submit cssClass="cssbuttonA" key="button.save" name="save" onclick="return validateFields();"/>
	<s:reset type="button" cssClass="cssbutton1" key="Reset" cssStyle="!margin-left:8px;"/>
	</c:set>
</c:if>
<div id="Layer1" style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
 
	<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<c:if test="${partnerType=='AC'}">
<td class="headtab_center" >&nbsp;Account User Profile</td>
</c:if>
<c:if test="${partnerType!='AC'}">
<td class="headtab_center" >&nbsp;Agent User & Contact Profile</td>
</c:if>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
<table class="" cellspacing="0" cellpadding="0" border="0" style="margin: 0px;" >
  <tbody>
 	<tr><td align="left" class="listwhitetext">
       		<table  class="detailTabLabel" border="0">
	 		  	<tbody>
	 		  			<tr>
	 		  			<td align="left" width="90px"></td>
	 		  			<td align="left" colspan="15">
	 		  			<table class="detailTabLabel" border="0">
	 		  			<tr>	
	 		  			
			 		  			<td align="left"><fmt:message key="user.firstName"/><font color="red" size="2">*</font></td>
			 		  			<td></td>
			 		  			<td align="left"><fmt:message key="user.initial"/></td>
			 		  			<td align="left"><fmt:message key="user.lastName"/><font color="red" size="2">*</font></td>
			 		  			<c:if test="${agentContact!='true'}">
			 		  			<td align="left" width="70"><fmt:message key="user.username"/><font color="red" size="2">*</font></td>
			 		  			<td align="right">Check&nbsp;availability&nbsp;of&nbsp;UserName</td>			 		  			
			 		  			<c:if test="${sentMailFlag=='Y'}">
			 		  			<td align="left" colspan="3"><input type="button" class="cssbutton1" value="Reset Password and email" style="width:170px;height:22px;" name="user.resetPwdAndSendMail" onclick="getResetPwdAndSendMail();"></td>
			 		  			</c:if>
			 		  			</c:if>	 		  			
			 		  			  
			 		  	    </tr>
			 		  	    <tr>
	 		  				
			 		  			<td align="left" colspan="2"><s:textfield key="user.firstName"  required="true" cssClass="input-text" maxlength="40" onkeydown="return onlyCharsAllowed(event)"/></td>
			 		  			<td align="left"><s:textfield key="user.initial"  cssClass="input-text" size="1" maxlength="5" onkeydown="return onlyCharsAllowed(event)"/></td>
			 		  			<td align="left"><s:textfield key="user.lastName"  required="true" cssStyle="width:99px;" cssClass="input-text" size="21" maxlength="40" onkeydown="return onlyCharsAllowed(event)"/></td>
			 		  			<c:if test="${agentContact!='true'}">
			 		  			<td align="left" colspan="2"><s:textfield key="user.username" cssClass="input-text" required="true" cssStyle="font-weight:bold;color:#385f9f" size="30" maxlength="25" onkeydown="return onlyAlphaNumericAllowed(event)"/></td>
			 		  			<td align="left">
			 		  			<div  id="hhid">
			 		  			<img id="rateImage" class="openpopup" height="20" align="top" width="17" onclick="findUserName();" src="${pageContext.request.contextPath}/images/image.jpg"/>
			 		  			</div>
			 		  			<div  id="hhid1">
			 		  			<img id="rateImage" class="openpopup" height="20" align="top" width="17"   src="${pageContext.request.contextPath}/images/image.jpg"/>
			 		  			</div>
			 		  			</td>
			 		  			</c:if>
			 		  			 <c:set var="defaultContactPerson" value="false" />
                                 <c:if test="${user.isDefaultContactPerson}">
                                 <c:set var="defaultContactPerson" value="true" />
                                 </c:if>			 		  			
			 		  	    <td align="right" class="listwhitetext" width="115">Default Contact Person</td>
                            <td class="listwhitetext" colspan="" width="10px">
                            <c:choose>
                            	<c:when test="${defaultContactPerson && defaultContactPersonCount=='1'}">
                            		<s:checkbox key="user.isDefaultContactPerson" value="${defaultContactPerson}"  tabindex=""   />
                            	</c:when>
                            	<c:otherwise>
                            		<c:choose>
                            			<c:when test="${defaultContactPerson && defaultContactPersonCount<'1'}">
                            				<s:checkbox key="user.isDefaultContactPerson" value="${defaultContactPerson}"  tabindex=""   />
                            			</c:when>
                            			<c:otherwise>
                            				<c:choose>
                            					<c:when test="${(!defaultContactPerson) && (defaultContactPersonCount<'1')}">
                            						<s:checkbox key="user.isDefaultContactPerson" value="${defaultContactPerson}"  tabindex=""   />
                            					</c:when>
                            					<c:otherwise>
                            						<s:checkbox key="user.isDefaultContactPerson" value="${defaultContactPerson}"  tabindex=""  onclick="return false" />
                            					</c:otherwise>
                            				</c:choose>
                            			</c:otherwise>
                            		</c:choose>
                            	</c:otherwise>
                            </c:choose>
                            </td>
	 	                </tr>
	 		  			</table>
	 		  			</td></tr>
	 	                <tr>
	 	               <td align="left" width="90px"></td>
	 	                <td>
	 	                <table class="detaiLtabLabel" style="margin:0px;padding:0px;">
	 	                <tr> 	 
	 	                <td align="left" width="90px"><fmt:message key="user.alias"/></td>	                
	 	                
	 	                <c:if test="${ partnerType=='AC' || partnerType=='AG' }">
	 		  			<td align="left">Gender</td>
	 		  			</c:if>
	 	                </tr>
	 	             
	 	                <tr>
	 	             
	 	               	<td align="left" ><s:textfield name="user.alias"  required="true" cssClass="input-text" size="48" maxlength="30" cssStyle="width:261px;" /></td>
	 	               	<c:if test="${partnerType=='AC' || partnerType=='AG'}">
			 		  	   <td align="left"><s:select cssClass="list-menu" name="user.gender" list="%{gender}" headerKey="" headerValue="" cssStyle="width:80px" /></td>
	 		  			</c:if>
						<c:if test="${partnerType=='AG' && agentContact!='true'}">
							<c:set var="isContact" value="false"/>
							<c:if test="${user.contact}">
									<c:set var="isContact" value="true"/>
							</c:if>				 		  	   	 	               	
							<td class="listwhitetext" align="left"><s:checkbox key="user.contact" id="excludeFPU" value="${isContact}" onclick="updatePartnerPrivate();" fieldValue="true"/></td><td class="listwhitetext" width="8px" align="right">Contact</td>			 		  	   	 	               	
			 		  	</c:if>
			 		  	<c:if test="${partnerType!='AG' && agentContact!='true'}">
			 		  	<s:hidden name="user.contact" id="excludeFPU" value="false"/>
			 		  	</c:if>
			 		  	<c:if test="${ agentContact=='true'}">
			 		  	<s:hidden name="user.contact" id="excludeFPU" value="true"/>
			 		  	</c:if>
			 		  	<!--
			 		  	<c:if test="${partnerType=='AG'}">
							<c:set var="portalAccess" value="false"/>
							<c:if test="${user.enabled}">
									<c:set var="portalAccess" value="true"/>
							</c:if>		 		  	   	 	               	
							<td class="listwhitetext" align="left"><s:checkbox  name="user.enabled"  value="${portalAccess}" /></td><td class="listwhitetext" width="" align="right">Portal Access</td>			 		  	   	 	               	
			 		  	</c:if>
			 		  	<c:if test="${partnerType!='AG'}">
			 		  	<s:hidden name="user.enabled" value="false"/>
			 		  	</c:if>
			 		  	  --></tr>
	 	                </table>
	 	                </td>
	 	                
	 	                </tr>
			 		  
			 		  	 	
			 		</tbody>
	 	  	</table></td></tr>
	 	  	
    		<tr><td align="left" class="listwhitetext">
    		<table  class="detailTabLabel" border="0">
	 		  	<tbody>

	 		   			<%--<tr>
	 		  			  		<td align="left" width="90px"></td>
			 		  			<td align="left"><fmt:message key="user.password"/><font color="red" size="2">*</font></td>
			 		  			<td align="left"><fmt:message key="user.confirmPassword"/><font color="red" size="2">*</font></td>
			 		  	</tr>
	 		  			
						<tr>
			 		  			<td align="left"></td>
			 		  			<td align="left"><s:password key="user.password" showPassword="true" required="true" cssClass="input-text" onchange="passwordChanged(this)"/></td>
			 		  			<td align="left"><s:password key="user.confirmPassword" required="true" showPassword="true" cssClass="input-text" onchange="passwordChanged(this)"/></td>
			 		  			
			 		  	</tr>

			 		  	--%>
			 		  	<tr>
	 		  			  		<td align="left" width="90px"></td>
			 		  			<%--<td align="left" colspan="2"><fmt:message key="user.passwordHintQues"/><font color="red" size="2">*</font></td>
			 		  			<td align="left" colspan="2"><fmt:message key="user.passwordHint"/><font color="red" size="2">*</font></td>	 --%>
			 		  	</tr>
			 		  	<tr>	
			 		  	<td align="left" width="90px"></td>
			 		  	<%--
				 		  	<td align="left" colspan="2"><s:select cssClass="list-menu" key="user.passwordHintQues" list="%{PWD_HINT}"  cssStyle="width:270px" onclick="changeStatus()"/></td>
				 		  	<td align="left" colspan="2"><s:textfield key="user.passwordHint" required="true" cssClass="input-text" size="20" maxlength="40" onkeydown="return onlyAlphaNumericAllowed(event)"/></td>
			 		  	 --%>
			 		  	 <s:hidden name="user.passwordHintQues"/>
			 		  	 <s:hidden name="user.passwordHint"/>
			 		  	</tr>		 		  		
	 		  	</tbody>
	 	  	</table></td></tr>
	 	
	 	  	<tr><td align="left" class="listwhitetext">
	 	  	<table  class="detailTabLabel" border="0">
	 		  	<tbody>	 		  			
						<tr>
							<td align="right" width="94px">Job Title<font color="red" size="2">*</font></td>
			 		  		<td align="left"><s:textfield name="user.userTitle" cssClass="input-text" size="48" maxlength="50" cssStyle="width:261px;" onkeydown="return onlyAlphaNumericAllowed(event)"/></td>
			 		  	<!--<c:if test="${partnerType=='AG'}">
                      <td align="right" width="90px">Job Function</td>
                      <td align="left"><s:select cssClass="list-menu" name="user.jobFunction" list="%{JobFunction}"  headerKey="" headerValue="" cssStyle="width:200px" /></td>
                    </c:if>
			 		  	-->			 		 
			 		  	<c:if test="${NPSAccess=='true'}">
			 		  		<c:set var="isRelocationContact" value="false"/>
		                    <c:if test="${user.relocationContact}">
		                   <c:set var="isRelocationContact" value="true"/>
		                   </c:if>
			 		  	<td align="right" class="listwhitetext">Relocation&nbsp;Contact</td>
			 		  	<td align="left"><s:checkbox key="user.relocationContact" value="${isRelocationContact}" onclick="" fieldValue="true" cssStyle="margin:0px;"/></td>
			 		  	</c:if>
			 		  	<c:if test="${NPSAccess=='false'}">
			 		  	<s:hidden name="user.relocationContact" />
			 		  	</c:if>
			 		  	</tr>
			 		  	
			 		  	<tr>		 		  			
			 		  			<td align="right" style="width:88px;" valign="bottom"><fmt:message key="user.phoneNumber"/></td>
			 		  			<td align="left" valign="bottom"><s:textfield name="user.phoneNumber" cssClass="input-text" maxlength="20"  onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
			 		  			 		  			
			 		  			<td align="right" style="width:88px;" valign="bottom"><fmt:message key="user.mobileNumber"/></td>
			 		  			<td align="left" valign="bottom"><s:textfield name="user.mobileNumber" cssClass="input-text" maxlength="20" cssStyle="width:151px;" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
			 		  	</tr>
			 		  	
			 		  	<tr>
			 		  			<td align="right"><fmt:message key="user.email"/><font color="red" size="2">*</font></td>
			 		  			<td align="left"><s:textfield name="user.email" cssClass="input-text" size="48" maxlength="60" cssStyle="width:261px;" /></td>
			 		  			<td align="right"><fmt:message key="user.faxNumber"/></td>
			 		  			<td align="left"><s:textfield name="user.faxNumber" cssClass="input-text" maxlength="20" cssStyle="width:151px;" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
			 		  	</tr>
			 		  	<tr>
			 		  			<td align="right"><fmt:message key="user.website"/></td>
			 		  			<td align="left"><s:textfield name="user.website" cssClass="input-text" size="48"  cssStyle="width:261px;" maxlength="40"/></td>
			 		  	</tr>
			 			<tr>
			 		  			<td align="right"><fmt:message key="user.address.address"/></td>
			 		  			<td align="left" colspan="7"><s:textfield name="user.address.address" cssClass="input-text" size="71" cssStyle="width:261px;" maxlength="100" /></td>
			 		  			
			 		  	</tr>
			 		  	<tr>
			 		  			<td align="right" width="90px"><fmt:message key="user.address.city"/><font color="red" size="2">*</font></td>
			 		  			
			 		  	<td colspan="8">	
			 		  			<table class="detailTabLabel" border="0" style="margin-left:-3px;">
			 		  			<tr>
			 		  			<td align="left"><s:textfield name="user.address.city" cssClass="input-text" maxlength="50" /></td>
			 		  			<td align="right" width="36"><fmt:message key="user.address.province"/></td>
			 		  			<td align="left"><s:textfield name="user.address.province" cssClass="input-text" size="16"  cssStyle="width:93px;" maxlength="40"/></td>
			 		  			<td align="right" width="25px"><fmt:message key="user.address.postalCode"/></td>
			 		  			<td align="left"><s:textfield name="user.address.postalCode" cssClass="input-text" size="10" maxlength="10" onkeydown="return onlyNumsAllowed(event)"/></td>
			 		  			<td align="right" width="40px"><fmt:message key="user.address.country"/><font color="red" size="2">*</font></td>
			 		  			
			 		  			<s:set name="country" value="user.address.country" scope="page"/>
                	  			<td align="left"><s:select cssClass="list-menu" name="user.address.country" list="%{ocountry}" headerKey="" headerValue="" cssStyle="width:188px" /></td>
			 		  	    	</tr>
			 		  	    	</table>
			 		  	    	</td>
			 		  	    	</tr>
			 		  	    	
	 		  	</tbody>
	 	  	</table></td></tr>
	 	  	
	 	  	<tr><td align="left" class="listwhitetext">
	 	  	<table  class="detailTabLabel" border="0">
	 		  	<tbody>
	 		  			
			 		  	    	<tr>
			 		  	    	<c:if test="${partnerType=='AC'|| partnerType=='AG' || partnerType=='VN' }">
			 		  	    	<td colspan="11">	
			 		  			<table class="detailTabLabel" border="0">
			 		  			<tr>
			 		  			<td align="right" width="42px"></td>
			 		  	        <td align="right" width="">Based At</td>
			 		  			<td align="left"><s:textfield name="user.basedAt" cssClass="input-text" cssStyle="width:56px;" onchange="valid(this,'special');" onblur="findbasedAt();"  maxlength="10" />
			 		  			</td>
			 		  			<td>
			 		  			<c:if test="${agentContact!='true'}">
			 		  			<div id="hhidBase" style="float:right;padding-right:4px;">
			 		  		   <img class="openpopup" id="openPopUp1" width="17" height="20" align="top" onclick="javascript:openWindow('searchPartner.html?&partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=user.basedAtName&fld_code=user.basedAt');" src="<c:url value='/images/open-popup.gif'/>" />
			 		  			</div>
			 		  			<div id="hhidBase1" style="float:right;padding-right:4px;">
			 		  			<img class="openpopup" id="openPopUp1" width="17" height="20" align="top"  src="<c:url value='/images/open-popup.gif'/>" />
			 		  			</div>
			 		  			</c:if>
			 		  			<c:if test="${agentContact=='true'}">
			 		  			<div style="float:right;padding-right:4px;">
			 		  			<img class="openpopup" id="openPopUp1" width="17" height="20" align="top" onclick="javascript:openWindow('searchPartner.html?&partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=user.basedAtName&fld_code=user.basedAt');" src="<c:url value='/images/open-popup.gif'/>" />
			 		  			</div>
			 		  			</c:if>
			 		  			</td>
			 		  			<td align="right" width="75px">Based At Name</td>
			 		  			<td align="left"><s:textfield name="user.basedAtName" cssClass="input-text" cssStyle="width:200px;" maxlength="10" /></td>
			 		  			<c:if test="${NPSAccess=='true'}">
			 		  	       <td align="right" class="listwhitetext" width="60px" colspan="">NPS score</td>
			                   <td align="left"><s:textfield cssClass="input-textUpper" name="user.NPSscore"  size="15" maxlength="15" readonly="true" /></td>
			 		  	       </c:if>
			 		  	       <c:if test="${NPSAccess=='false'}">
			 		  	       <s:hidden name="user.NPSscore"/>
			 		  	       </c:if>
			 		  	       </tr>
			 		  	       </table>
			 		  	       </td>
			 		  	       
			 		  	        </c:if>		  	
			 		  	        </tr>
			 		  	        
			 		  	        
			 		  	       
			  	</tbody>
	 	  	</table>
    	</td></tr>

	<tr>
	<td align="left" height="10px"></td>
	</tr>
   </tbody>
  </table>
  <table class="detailTabLabel" border="0">
   <c:if test="${partnerType=='AG'}">
			 		  	        <tr>
			 		  	        <td align="right" width=""></td>
			 		  	        <td align="left" class="colored_bg"  colspan="9">
		  	 
       	 
			        <fieldset >
			            <legend>Job Functions<font color="red" size="2">*</font></legend>
			            <table class="pickList111" >
			                <tr>
			                    <th class="pickLabel">
			                        <label class="required">Function List</label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required">Selected Functions</label>
			                    </th>
			                </tr>
			          
			                <c:set var="leftList" value="${JobFunction}" scope="request"/>
			                <c:import url="/WEB-INF/pages/pickList12.jsp">
			                    <c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="JobFunction && !userJobType"/>
			                    <c:param name="rightId" value="userJobType"/>
			                </c:import>
			            </table>
			        </fieldset>
			               </td>
			           </tr>
			 		</c:if>
			 <configByCorp:fieldVisibility componentId="component.user.venderAccess.showHSRG">		
				    <c:if test="${partnerType == 'VN' }">
				      	        <tr>
			 		  	        <td align="right" width=""></td>
			 		  	        <td align="left" class="colored_bg"  colspan="9">
		  	 
       	 
			        <fieldset >
			            <legend>Job Functions<font color="red" size="2">*</font></legend>
			            <table class="pickList111" >
			                <tr>
			                    <th class="pickLabel">
			                        <label class="required">Function List</label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required">Selected Functions</label>
			                    </th>
			                </tr>
			          
			                <c:set var="leftList" value="${JobFunction}" scope="request"/>
			                <c:import url="/WEB-INF/pages/pickList12.jsp">
			                    <c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="JobFunction && !userJobType"/>
			                    <c:param name="rightId" value="userJobType"/>
			                </c:import>
			            </table>
			        </fieldset>
			               </td>
			           </tr>
				    
				    </c:if>
	    </configByCorp:fieldVisibility>
			 <c:if test="${ agentContact!='true'}">
			 		  	     <tr>
			 		  	        <td align="right" width=""></td>
			 		  	        <td align="left" class="colored_bg"  colspan="9">
       	 
			           <fieldset >
			            <legend>Assign Roles<font color="red" size="2">*</font></legend>
			                <table class="pickList111" >
			                   <tr>
			                     <th class="pickLabel">
			                        <label class="required">Available Roles</label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required">Current Roles</label>
			                     </th>
			                  </tr>
			                 <c:set var="leftList" value="${availRoles}" scope="request"/>
			                <s:set name="rightList" value="user.roleList" scope="request"/>
			                <c:import url="/WEB-INF/pages/pickList.jsp">
			                    <c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="availRoles && !userRolesValue"/>
			                    <c:param name="rightId" value="userRolesValue"/>
			                </c:import>
			                </table>
			                </fieldset>
			                   </td>
			 		  	</tr>
	         </c:if>
				<tr>
			 		  	        <td align="right" width=""></td>
			 		  	        <td align="left" class="colored_bg"  colspan="9">
       	 
			           <fieldset >
			            <legend>Permissions<font color="red" size="2">*</font></legend>
			                <table class="pickList111" >
			                   <tr>
			                     <th class="pickLabel">
			                        <label class="required">Available Set</label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required">Current Set</label>
			                     </th>
			                  </tr>
			                 <c:set var="leftList" value="${availPermissions}" scope="request"/>
			                <s:set name="rightList" value="user.permissionList" scope="request"/>
			                <c:import url="/WEB-INF/pages/pickList.jsp">
			                    <c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="availPermissions && !userPermissionValue"/>
			                    <c:param name="rightId" value="userPermissionValue"/>
			                </c:import>
			                </table>
			                </fieldset>
			                   </td>
			 		  	</tr>
			 		  		         
  </table>
    </div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div>
</div>
</div>


  <table>
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
						<fmt:formatDate var="cartonCreatedOnFormattedValue" value="${user.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='user.createdOn'/></b></td>
							<s:hidden name="user.createdOn" value="${cartonCreatedOnFormattedValue}" />
							<td style="width:140px"><fmt:formatDate value="${user.createdOn}" pattern="${displayDateTimeFormat}"/></td>	
								
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='user.createdBy' /></b></td>
							<c:if test="${not empty user.id}">
								<s:hidden name="user.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{user.createdBy}"/></td>
							</c:if>
							<c:if test="${empty user.id}">
								<s:hidden name="user.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${user.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='user.updatedOn'/></b></td>
							<s:hidden name="user.updatedOn" value="${cartonUpdatedOnFormattedValue}" />
							<td style="width:140px"><fmt:formatDate value="${user.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='user.updatedBy' /></b></td>
						 	<c:if test="${not empty user.id}">
								<s:hidden name="user.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{user.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty user.id}">
								<s:hidden name="user.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						
						</tr>
					</tbody>
				</table>

 		  <c:out value="${buttons}" escapeXml="false"/>
    </div>
	
	<s:hidden key="user.userStatus"/>
	<s:hidden key="user.passwordReset"/>	
	<s:hidden key="user.accountExpired"/>	
	<s:hidden key="user.accountLocked"/>	
	<s:hidden key="user.credentialsExpired"/>	
	<s:hidden key="user.supervisor"/>	
	<s:hidden key="user.warehouse"/>	
	<s:hidden key="user.branch"/>	
	<s:hidden key="user.department"/>	
	<s:hidden key="user.rank"/>	
	<s:hidden key="user.signature"/>	
	<s:hidden key="user.location"/>	
	<s:hidden key="user.userType"/>	
	<s:hidden key="user.jobType"/>	
	<s:hidden key="user.workStartTime"/>
	<s:hidden key="user.workEndTime"/>
	<s:hidden name="user.defaultSearchStatus"/>
	<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
	<c:if test="${partnerType!='AC' && partnerType!='AG'}">
	<s:hidden name="user.gender"/>
	</c:if>
	<c:if test="${partnerType!='AG'}">
	<s:hidden name="userJobType"/>
	</c:if>
	<c:if test="${agentContact=='true'}">
	<s:hidden name="userRolesValue" value="ROLE_USER"/>	
	</c:if>
	<c:if test="${partnerType!='AC' && partnerType!='AG' && partnerType!='VN' }">
	<s:hidden name="user.basedAt"/>
	<s:hidden name="user.basedAtName"/>
	</c:if>
	<c:if test="${hitFlag == 1}" >
	   <c:if test="${userId!=null && userId!=''}">
			<c:redirect url="/addUsersFromPartner.html?id=${id}&userId=${user.id}&partnerType=${partnerType}" />
		</c:if>
		<c:if test="${userId==null || userId==''}">
			<c:redirect url="/addUsersFromPartner.html?id=${id}&partnerType=${partnerType}" />
		</c:if>
	</c:if>		 		  			
</s:form>

<script language="javascript">
try{
	feedRoleOnChange();
	var checkCorpID= 0;
	//document.forms['userForm'].elements['save'].disabled=false;
		var linkDiv = document.getElementById("hhid");
		var linkDiv2 = document.getElementById("hhid1");
		var linkBADiv = document.getElementById("hhidBase");
		var linkBADiv2 = document.getElementById("hhidBase1");
	<c:if test="${partnerType=='AG'}">
	
	    <sec-auth:authComponent componentId="module.tab.partner.AgentDEV">
	checkCorpID = 14;
	   </sec-auth:authComponent>
	if(checkCorpID < 14){
	<c:if test="${agentContact!='true'}">
		disabledAll();
	</c:if>
		linkDiv2.style.display = 'block';
		linkDiv.style.display = 'none';
		linkBADiv.style.display = 'none';
		linkBADiv2.style.display = 'block';
	}else{
		linkDiv.style.display = 'block';
		linkDiv2.style.display = 'none';
		linkBADiv2.style.display = 'none';
		linkBADiv.style.display = 'block';
	}
	</c:if>
	<c:if test="${partnerType=='AC'}">
		if(checkCorpID < 14){
			linkDiv.style.display = 'block';
			linkDiv2.style.display = 'none';
			linkBADiv2.style.display = 'none';
			linkBADiv.style.display = 'block';
		}else{
			linkDiv2.style.display = 'block';
			linkDiv.style.display = 'none';
			linkBADiv.style.display = 'none';
			linkBADiv2.style.display = 'block';	
		}
	</c:if>
	
}catch(e){}
</script>
