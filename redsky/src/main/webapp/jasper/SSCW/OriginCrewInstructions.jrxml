<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="OriginCrewInstructions"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="535"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Service Order Number" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT "OriginCrewInstructions-SSCW-pdf.jrxml",
     serviceorder.`firstName` AS serviceorder_firstName,
     serviceorder.`lastName` AS serviceorder_lastName,
     serviceorder.`shipNumber` AS serviceorder_shipNumber,
     date_format(trackingstatus.`packA`,'%d-%b-%Y') AS trackingstatus_packA,
     date_format(trackingstatus.`loadA`,'%d-%b-%Y') AS trackingstatus_loadA,
     date_format(trackingstatus.`deliveryA`,'%d-%b-%Y') AS trackingstatus_deliveryA,
     billing.`billtoname` AS billing_billtoname,
     app_user.phone_number AS app_user_phone_number, 
     CONCAT(if(trim(app_user.first_name)="" || app_user.first_name is null,"",trim(app_user.first_name)),if(trim(app_user.last_name)="" || app_user.last_name is null,"",if((trim(app_user.first_name)="" || app_user.first_name is null),trim(app_user.last_name),concat(" ",trim(app_user.last_name))))) AS serviceorder_coordinator,
     notes.note AS notes_note
FROM serviceorder serviceorder
	INNER JOIN `trackingstatus` trackingstatus ON serviceorder.`id` = trackingstatus.`id` and trackingstatus.corpID=$P{Corporate ID}
	INNER JOIN `billing` billing ON serviceorder.`id` = billing.`id` and billing.corpID=$P{Corporate ID}
	LEFT OUTER JOIN `app_user` app_user ON serviceorder.`coordinator` = app_user.`username` AND app_user.`corpID` = $P{Corporate ID}
	LEFT OUTER JOIN notes notes  on  notes.noteskeyid = serviceorder.id and notes.corpid = $P{Corporate ID} and notetype='Service Order' and notesubtype='Critical to Quality Note'
WHERE serviceorder.corpID = $P{Corporate ID}
AND serviceorder.`shipNumber` = $P{Service Order Number}]]></queryString>

	<field name="OriginCrewInstructions-SSCW-pdf.jrxml" class="java.lang.String"/>
	<field name="serviceorder_firstName" class="java.lang.String"/>
	<field name="serviceorder_lastName" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="trackingstatus_packA" class="java.lang.String"/>
	<field name="trackingstatus_loadA" class="java.lang.String"/>
	<field name="trackingstatus_deliveryA" class="java.lang.String"/>
	<field name="billing_billtoname" class="java.lang.String"/>
	<field name="app_user_phone_number" class="java.lang.String"/>
	<field name="serviceorder_coordinator" class="java.lang.String"/>
	<field name="notes_note" class="java.lang.String"/>


		<group  name="Instruction" >
			<groupExpression><![CDATA[]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="315"  isSplitAllowed="true" >
				<rectangle>
					<reportElement
						x="5"
						y="204"
						width="530"
						height="50"
						forecolor="#0019A9"
						key="rectangle-1"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="5"
						y="1"
						width="530"
						height="34"
						key="textField-8"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="18" isBold="true" isUnderline="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["General instructions:"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="5"
						y="35"
						width="530"
						height="187"
						key="staticText-1"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[• If you are going to be late (outside start time window) you must call the client and notify the Counselor listed below.
• Minimum of two crew members must be assigned to every shipment.
• Full packing is required.
• On Air shipments, goods must be packed into boxes before loading inside air containers.
• Detailed packing list of all items in the shipment. No PBO’s (Packed by Owner) are allowed.
• All electrical items must be on packing list with make, model and serial number.
• Client must sign all pages of packing list.
• Final walk-through with transferee to verify nothing left behind unintentionally.
• If client requests any additional services not previously authorized, call the Counselor listed below from residence
  before carrying out any additional services; otherwise you may not get paid for this service.
• Bring a portable scale to residence anytime the client’s allowance is 1,000 lbs or under.
• For full container shipments container must be loaded floor to ceiling and a bulkhead created with appropriate
  materials.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="5"
						y="204"
						width="530"
						height="58"
						forecolor="#0019A9"
						key="staticText-2"/>
					<box leftPadding="3" topPadding="5">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement>
						<font size="10"/>
					</textElement>
				<text><![CDATA[• A 7-Point inspection must be done prior to loading of container. Please see instructions and form attached.
• Container must be loaded floor to ceiling and a bulkhead built with proper materials.
• A C-TPAT/ISO Compliant High-Security SEAL (Cable lock, Cable seal or Bolt Seal) must be used.]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="5"
						y="256"
						width="530"
						height="59"
						key="textField-9"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["IF AT ANY TIME YOU HAVE ANY QUESTIONS ABOUT WHAT GOES WHERE, OR HOW SOMETHING SHOULD BE HANDLED, PLEASE CALL "+($F{serviceorder_coordinator}==null ? "" : $F{serviceorder_coordinator})+" at "+($F{app_user_phone_number}==null ? "" : $F{app_user_phone_number})]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="225"  isSplitAllowed="true" >
				<image  scaleImage="RetainShape" vAlign="Top" hAlign="Left" onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="7"
						width="240"
						height="78"
						key="image-1"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/SUDD_International.jpg"]]></imageExpression>
				</image>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="255"
						y="22"
						width="274"
						height="34"
						key="textField-1"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica" size="18" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Origin Crew Instructions"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="255"
						y="57"
						width="274"
						height="34"
						key="textField-2"/>
					<box></box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Reg # "+($F{serviceorder_shipNumber}==null ? "" : $F{serviceorder_shipNumber})]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="5"
						y="85"
						width="250"
						height="34"
						key="textField-3"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica" size="8" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["815 South Main Street, 4th Floor Jacksonville, Florida 32207"+"\n"+
"Phone 800-874-3833 Fax 904-858-1201"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="5"
						y="127"
						width="530"
						height="56"
						key="textField-4"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[("Suddath Reg # "+($F{serviceorder_shipNumber}==null ? "" : $F{serviceorder_shipNumber}))+"\n"+
("Transferee Name: "+($F{serviceorder_firstName}==null ? "" : ($F{serviceorder_firstName}+" "))+($F{serviceorder_lastName}==null ? "" : $F{serviceorder_lastName}))+"\n"+
("Account Name: "+($F{billing_billtoname}==null ? "" : $F{billing_billtoname}))+"\n"+
("Pack Date: "+($F{trackingstatus_packA}==null ? "" : ($F{trackingstatus_packA}+", ")))+("Load Date: "+($F{trackingstatus_loadA}==null ? "" : $F{trackingstatus_loadA}))]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="5"
						y="191"
						width="274"
						height="34"
						forecolor="#800000"
						key="textField-5"/>
					<box leftPadding="3"></box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="18" isBold="true" isUnderline="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Critical Items"]]></textFieldExpression>
				</textField>
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="5"
						y="0"
						width="530"
						height="18"
						forecolor="#800000"
						key="textField-10"
						stretchType="RelativeToTallestObject"
						isRemoveLineWhenBlank="true"
						isPrintWhenDetailOverflows="true"/>
					<box leftPadding="3" topPadding="1" bottomPadding="1">					<pen lineWidth="1.0"/>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{notes_note}==null ? "" : ("* "+ $F{notes_note}.trim()))]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
