package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="refzipgeocodemap")
public class RefZipGeoCodeMap extends BaseObject implements ListLinkData{
	private Long id;
	private String zipcode;
	private String stateabbreviation;
	private String city;
	private String state;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private String latitude;
	private String longitude;
	private Date updatedOn;
	private Date createdOn;
	private String primaryRecord;
	private String phoneAreaCode;
	private String country;
	private String county;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("zipcode",
				zipcode).append("stateabbreviation", stateabbreviation).append(
				"city", city).append("state", state).append("corpID", corpID)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("latitude", latitude).append("longitude", longitude)
				.append("updatedOn", updatedOn).append("createdOn", createdOn)
				.append("primaryRecord", primaryRecord).append("phoneAreaCode",
						phoneAreaCode).append("country", country).append(
						"county", county).toString();
	}
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof RefZipGeoCodeMap))
			return false;
		RefZipGeoCodeMap castOther = (RefZipGeoCodeMap) other;
		return new EqualsBuilder().append(id, castOther.id).append(zipcode,
				castOther.zipcode).append(stateabbreviation,
				castOther.stateabbreviation).append(city, castOther.city)
				.append(state, castOther.state)
				.append(corpID, castOther.corpID).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(latitude,
						castOther.latitude).append(longitude,
						castOther.longitude).append(updatedOn,
						castOther.updatedOn).append(createdOn,
						castOther.createdOn).append(primaryRecord,
						castOther.primaryRecord).append(phoneAreaCode,
						castOther.phoneAreaCode).append(country,
						castOther.country).append(county, castOther.county)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(zipcode).append(
				stateabbreviation).append(city).append(state).append(corpID)
				.append(createdBy).append(updatedBy).append(latitude).append(
						longitude).append(updatedOn).append(createdOn).append(
						primaryRecord).append(phoneAreaCode).append(country)
				.append(county).toHashCode();
	}
	
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	
	/**
	 * @return the city
	 */
	@Column(length=30)
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	@Column(length=30)
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the stateabbreviation
	 */
	@Column(length=3)
	public String getStateabbreviation() {
		return stateabbreviation;
	}
	/**
	 * @param stateabbreviation the stateabbreviation to set
	 */
	public void setStateabbreviation(String stateabbreviation) {
		this.stateabbreviation = stateabbreviation;
	}
	/**
	 * @return the zipcode
	 */
	@Column(length=10)
	public String getZipcode() {
		return zipcode;
	}
	/**
	 * @param zipcode the zipcode to set
	 */
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	/**
	 * @return the latitude
	 */
	@Column(length=50)
	public String getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the longitude
	 */
	@Column(length=50)
	public String getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	@Column(length=55)
	public String getPhoneAreaCode() {
		return phoneAreaCode;
	}
	public void setPhoneAreaCode(String phoneAreaCode) {
		this.phoneAreaCode = phoneAreaCode;
	}
	@Column(length=1)
	public String getPrimaryRecord() {
		return primaryRecord;
	}
	public void setPrimaryRecord(String primaryRecord) {
		this.primaryRecord = primaryRecord;
	}
	@Transient
	public String getListCode() {
		// TODO Auto-generated method stub
		return zipcode;
	}
	@Transient
	public String getListDescription() {
		// TODO Auto-generated method stub
		return city;
	}
	@Transient
	public String getListEigthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListFifthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListFourthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListNinthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListSecondDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListSeventhDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListSixthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListTenthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListThirdDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Column( length=200)
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	@Column( length=45 )
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}	
}
