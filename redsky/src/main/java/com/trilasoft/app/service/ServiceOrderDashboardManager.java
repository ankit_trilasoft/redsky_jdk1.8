/**
 *  @Class Name	 ServiceOrderDashboardManager
 *  @author      Surya 
 *  @Date        14-Jul-2014
 */
package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ServiceOrderDashboard;



public interface ServiceOrderDashboardManager extends GenericManager<ServiceOrderDashboard, Long>{
	public List soDashboardSearchList(String shipNumber,String registrationNumber,String job1,String coordinator,String status,String corpId,String role,String lastName,String firstName,String cmpdiv,String billtoName,Boolean actstatus,String bookingAgentShipNumber,String serviceOrderMoveType,String recordLimit,String cityCountryZipOption,String originCityOrZip,String destinationCityOrZip,String originCountry,String destinationCountry,String salesMan,String certificateNumber,String daShipmentNumber,String estimator);
}
