package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.InventoryPackingDao;
import com.trilasoft.app.model.InventoryPacking;

public class InventoryPackingDaoHibernate extends GenericDaoHibernate<InventoryPacking, Long> implements InventoryPackingDao{

	public InventoryPackingDaoHibernate() {
		super(InventoryPacking.class);
	}
	public class InventoryDTO{
		private Object pieceID;
		private Object location;
		private Object items;
		private Object itemQuantity;
		private Object volume;
		private Object weight;
		private Object value;
		private Object id;
		private Object item;
		
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getItemQuantity() {
			return itemQuantity;
		}
		public void setItemQuantity(Object itemQuantity) {
			this.itemQuantity = itemQuantity;
		}
		public Object getItems() {
			return items;
		}
		public void setItems(Object items) {
			this.items = items;
		}
		public Object getLocation() {
			return location;
		}
		public void setLocation(Object location) {
			this.location = location;
		}
		public Object getPieceID() {
			return pieceID;
		}
		public void setPieceID(Object pieceID) {
			this.pieceID = pieceID;
		}
		public Object getValue() {
			return value;
		}
		public void setValue(Object value) {
			this.value = value;
		}
		public Object getVolume() {
			return volume;
		}
		public void setVolume(Object volume) {
			this.volume = volume;
		}
		public Object getWeight() {
			return weight;
		}
		public void setWeight(Object weight) {
			this.weight = weight;
		}
		public Object getItem() {
			return item;
		}
		public void setItem(Object item) {
			this.item = item;
		}
	}
	
	public List searchCriteria(Long sid,String room,String  article,String sessionCorpID){
		String query="";
		/*
		List inventoryList = new ArrayList();
		if(room.trim().equals("") && article.trim().equals("")){
			//query="select id, pieceID,location,GROUP_CONCAT( item SEPARATOR ','),itemQuantity,volume,weight,value  from inventorypacking where serviceOrderID='"+sid+"' group by pieceID ";
			query="select id, pieceID,location,count(item)as item,sum(itemQuantity)as itemQuantity,volume,weight from inventorypacking where serviceOrderID='"+sid+"' group by pieceID";
		}else if(room.trim().equals("") || article.trim().equals("")){
			if(!room.trim().equals("") ){
				//query="select id, pieceID,location,GROUP_CONCAT( item SEPARATOR ','),itemQuantity,volume,weight,value  from inventorypacking where serviceOrderID='"+sid+"' and location='"+room+"' group by pieceID";
				query="select id, pieceID,location,count(item)as item,sum(itemQuantity)as itemQuantity,volume,weight  from inventorypacking where serviceOrderID='"+sid+"' and location='"+room+"' group by pieceID";
				
			}else{
				//query="select id, pieceID,location,GROUP_CONCAT( item SEPARATOR ','),itemQuantity,volume,weight,value  from inventorypacking where serviceOrderID='"+sid+"' and item like'%"+article+"%' group by pieceID";
				query="select id, pieceID,location,count(item)as item,sum(itemQuantity)as itemQuantity,volume,weight  from inventorypacking where serviceOrderID='"+sid+"' and item like'%"+article+"%' group by pieceID";
			}
		}else{
			//query="select id, pieceID,location,GROUP_CONCAT( item SEPARATOR ','),itemQuantity,volume,weight,value  from inventorypacking where serviceOrderID='"+sid+"' and item like '%"+article+"%' and location='"+room+"' group by pieceID";
			query="select id, pieceID,location,count(item)as item,sum(itemQuantity)as itemQuantity,volume,weight  from inventorypacking where serviceOrderID='"+sid+"' and item like '%"+article+"%' and location='"+room+"' group by pieceID";
		}
		
		List list =this.getSession().createSQLQuery(query).list();
		Iterator itr=list.iterator();
		while(itr.hasNext()){
			String listVal ="";
			InventoryDTO inventoryDTO= new InventoryDTO(); 
			Object[] object=(Object[]) itr.next();
			inventoryDTO.setId(object[0]);
			inventoryDTO.setPieceID(object[1]);
			inventoryDTO.setLocation(object[2]);
			inventoryDTO.setItems(object[3]);
			inventoryDTO.setItemQuantity(object[4]);
			inventoryDTO.setVolume(object[5]);
			inventoryDTO.setWeight(object[6]);
			//inventoryDTO.setValue(object[7]);
			List tempItem =this.getSession().createSQLQuery("select id,item,if(itemQuantity is null or itemQuantity='','0',itemQuantity) as quantity ,value , imagesAvailablityFlag,conditon from inventorypacking where serviceorderid='"+sid+"'and pieceID='"+object[1]+"'").list(); 
			Iterator itrItem=tempItem.iterator();
			while(itrItem.hasNext()){
				Object[] obj = (Object[]) itrItem.next();
				listVal=listVal+"^"+obj[0]+"~"+obj[1]+"~"+obj[2]+"~"+obj[3]+"~"+obj[4]+"~"+obj[5];
			}
			inventoryDTO.setItem(listVal);
			inventoryList.add(inventoryDTO);
		}
		return  inventoryList;*/
		if(room.trim().equals("") && article.trim().equals("")){
			query="from InventoryPacking where serviceOrderID='"+sid+"'";
		}else if(room.trim().equals("") || article.trim().equals("")){
			if(!room.trim().equals("") ){
				query="from InventoryPacking where serviceOrderID='"+sid+"' and location='"+room+"'";
				
			}else{
				query="from InventoryPacking where serviceOrderID='"+sid+"' and item like'%"+article+"%'";
			}
		}
		else{
			query="from InventoryPacking where serviceOrderID='"+sid+"' and item like '%"+article+"%' and location='"+room+"'";
			}
		return getHibernateTemplate().find(query);
	}
	public List seachInventoryThroughPieceId(Long sid,String pieceID,String sessionCorpID){
		List inventoryPieceList = new ArrayList();
		String query="select distinct(pieceID) as pieceID,location,volume,weight,createdBy,createdOn,updatedBy,updatedOn,boxName from inventorypacking where pieceID='"+pieceID+"' and serviceorderId='"+sid+"' and corpID='"+sessionCorpID+"' group by pieceID ";
		List list =this.getSession().createSQLQuery(query).list();
		Iterator itr=list.iterator();
		while(itr.hasNext()){
			InventoryPieceDTO inventoryPieceDTO= new InventoryPieceDTO(); 
			Object[] object=(Object[]) itr.next();
			inventoryPieceDTO.setPieceID(object[0]);
			inventoryPieceDTO.setLocation(object[1]);
			inventoryPieceDTO.setVolume(object[2]);
			inventoryPieceDTO.setWeight(object[3]);
			inventoryPieceDTO.setCreatedBy(object[4]);
			inventoryPieceDTO.setCreatedOn(object[5]);
			inventoryPieceDTO.setUpdatedBy(object[6]);
			inventoryPieceDTO.setUpdatedOn(object[7]);
			inventoryPieceDTO.setBoxName(object[8]);
			inventoryPieceList.add(inventoryPieceDTO);
		}
		return inventoryPieceList;
		//return getHibernateTemplate().find("from InventoryPacking where serviceOrderID='"+sid+"' and pieceID='"+pieceID+"'");
	}
	public List findArticleByPieceId(Long sid, String pieceID,String sessionCorpID){
		return getHibernateTemplate().find("from InventoryPacking where serviceOrderID='"+sid+"' and pieceID='"+pieceID+"'");
	}
	public class InventoryPieceDTO{
		private Object pieceID;
		private Object location;
		private Object volume;
		private Object weight;
		private Object createdBy;
		private Object createdOn;
		private Object updatedBy;
		private Object updatedOn;
		private Object boxName;
		public Object getPieceID() {
			return pieceID;
		}
		public void setPieceID(Object pieceID) {
			this.pieceID = pieceID;
		}
		public Object getLocation() {
			return location;
		}
		public void setLocation(Object location) {
			this.location = location;
		}
		public Object getVolume() {
			return volume;
		}
		public void setVolume(Object volume) {
			this.volume = volume;
		}
		public Object getWeight() {
			return weight;
		}
		public void setWeight(Object weight) {
			this.weight = weight;
		}
		public Object getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(Object createdBy) {
			this.createdBy = createdBy;
		}
		public Object getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Object createdOn) {
			this.createdOn = createdOn;
		}
		public Object getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(Object updatedBy) {
			this.updatedBy = updatedBy;
		}
		public Object getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Object updatedOn) {
			this.updatedOn = updatedOn;
		}
		public Object getBoxName() {
			return boxName;
		}
		public void setBoxName(Object boxName) {
			this.boxName = boxName;
		}
		
	}
	
}
