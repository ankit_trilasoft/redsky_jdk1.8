<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="DeliveryCertificate" pageWidth="595" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="535" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="d039d54b-8137-4e28-aa73-908481bd4001">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<parameter name="Service Order Number" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT 
	"DeliveryCertificate-CWMS.jrxml",
	concat(if(serviceorder.firstName is null || serviceorder.firstName="","",Concat(serviceorder.firstName,', ')),serviceorder.lastName) as serviceorder_shippername,
	concat(if(serviceorder.destinationAddressLine1=""||serviceorder.destinationAddressLine1 is null,"",serviceorder.destinationAddressLine1),if(serviceorder.destinationAddressLine2=""||serviceorder.destinationAddressLine2 is null,"",if(serviceorder.destinationAddressLine1=""||serviceorder.destinationAddressLine1 is null,serviceorder.destinationAddressLine2,concat(", ",serviceorder.destinationAddressLine2)))) AS serviceorder_destinationAddressLine1_2,
	serviceorder.destinationAddressLine3 AS serviceorder_destinationAddressLine3,
	Concat(if(serviceorder.destinationCity is null || serviceorder.destinationCity='',"",serviceorder.destinationCity),"",if(serviceorder.destinationState is null || serviceorder.destinationState='',"",if(serviceorder.destinationCity is null || serviceorder.destinationCity='',serviceorder.destinationState,concat(", ",serviceorder.destinationState))),"",if(serviceorder.destinationZip is null || serviceorder.destinationZip='',"",if((serviceorder.destinationCity is null || serviceorder.destinationCity='') AND (serviceorder.destinationState is null || serviceorder.destinationState=''),serviceorder.destinationZip,concat(" ",serviceorder.destinationZip)))) AS serviceorder_destination_CityStateZip,
	IF(serviceorder.destinationCountryCode = 'USA',"",serviceorder.destinationCountry) AS serviceorder_destinationCountry,
	if(serviceorder.destinationCountryCode is null || serviceorder.destinationCountryCode='',"",serviceorder.destinationCountryCode) AS serviceorder_destinationCountryCode,
	if(miscellaneous.unit1='Lbs',concat(miscellaneous.netEstimateCubicFeet,' ',miscellaneous.unit2),concat(miscellaneous.netEstimateCubicMtr,' ',miscellaneous.unit2)) as miscellaneous_netEstimateCubicFeet,
	serviceorder.shipNumber AS serviceorder_shipNumber,
        systemdefault.company as systemdefault_Company
from `serviceorder` serviceorder
	INNER JOIN `miscellaneous` miscellaneous on serviceorder.id=miscellaneous.id and miscellaneous.corpId=$P{Corporate ID}
        LEFT OUTER JOIN systemdefault systemdefault on systemdefault.corpID=$P{Corporate ID}
where serviceorder.corpId=$P{Corporate ID}
and serviceorder.shipNumber=$P{Service Order Number}]]>
	</queryString>
	<field name="DeliveryCertificate-CWMS.jrxml" class="java.lang.String"/>
	<field name="serviceorder_shippername" class="java.lang.String"/>
	<field name="serviceorder_destinationAddressLine1_2" class="java.lang.String"/>
	<field name="serviceorder_destinationAddressLine3" class="java.lang.String"/>
	<field name="serviceorder_destination_CityStateZip" class="java.lang.String"/>
	<field name="serviceorder_destinationCountry" class="java.lang.String"/>
	<field name="serviceorder_destinationCountryCode" class="java.lang.String"/>
	<field name="miscellaneous_netEstimateCubicFeet" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="systemdefault_Company" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="802" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="1" y="13" width="404" height="31" uuid="b0f85371-3aa1-4673-a9bb-5ec424933b85"/>
				<textElement>
					<font fontName="Verdana" size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Delivery Certificate & Property Damage Release]]></text>
			</staticText>
			<image onErrorType="Blank">
				<reportElement key="image-1" x="327" y="3" width="205" height="71" uuid="6e9756a5-8345-4082-82af-b9324715d731"/>
				<imageExpression><![CDATA["../../images/CWMS_logo.jpg"]]></imageExpression>
			</image>
			<staticText>
				<reportElement key="staticText-2" x="1" y="127" width="534" height="17" uuid="f2f6a6d8-bb92-4df3-9e2b-0e469f812491"/>
				<textElement>
					<font size="12"/>
				</textElement>
				<text><![CDATA[***************************************************************************************************************************]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" x="1" y="138" width="166" height="17" uuid="5c9b4fad-f487-40ae-a8d1-b41b6f76b66f"/>
				<textElement>
					<font size="11" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[DELIVERY CERTIFICATE]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" x="1" y="155" width="534" height="30" uuid="886cfe40-9c2c-47e2-9bcd-d3bf12ded0c6"/>
				<textElement textAlignment="Justified">
					<font fontName="Verdana"/>
				</textElement>
				<text><![CDATA[Destination services include full unpacking and debris removal at the time of delivery unless other arrangements have been specifically made in advance.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" x="1" y="195" width="534" height="30" uuid="32b50e5c-846c-47bd-9e93-c5ae4fc32292"/>
				<textElement textAlignment="Justified">
					<font fontName="Verdana"/>
				</textElement>
				<text><![CDATA[If full unpacking is waived at the time of delivery, future debris removals will result in an additional charge that could be at your personal expense.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" x="0" y="230" width="534" height="30" uuid="5b6ef4cb-b83e-42d1-b627-c7159905b067"/>
				<textElement textAlignment="Justified">
					<font fontName="Verdana"/>
				</textElement>
				<text><![CDATA[I hereby certify that I received the above items in good condition except as noted on the inventory.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="81" y="268" width="454" height="15" uuid="a7bc7ac9-d582-4122-9766-3e5f66e3fae1"/>
				<textElement textAlignment="Justified">
					<font fontName="Verdana"/>
				</textElement>
				<text><![CDATA[Full unpacking and debris removal was performed during delivery.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="81" y="288" width="454" height="15" uuid="4acd4ca9-ed74-4f89-b03d-24c0a9abfc71"/>
				<textElement textAlignment="Justified">
					<font fontName="Verdana"/>
				</textElement>
				<text><![CDATA[Full unpacking and debris removal is waived per my instructions.]]></text>
			</staticText>
			<line>
				<reportElement key="line-1" x="1" y="319" width="205" height="1" uuid="0dfb55a7-fda0-49b9-82d2-858d90ff16a2"/>
			</line>
			<line>
				<reportElement key="line-2" x="313" y="319" width="205" height="1" uuid="9a6b8b12-59e7-485e-9f19-c060e41f4708"/>
			</line>
			<staticText>
				<reportElement key="staticText-9" x="1" y="320" width="78" height="15" uuid="db869d88-1c8b-46c0-83b7-bcde2cac89de"/>
				<text><![CDATA[Shipper]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="313" y="320" width="78" height="15" uuid="dae1992c-1b86-426e-97db-0149e707dee3"/>
				<text><![CDATA[Date]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-11" x="1" y="343" width="534" height="72" uuid="26a7103d-70a0-435d-8617-b318c0f01880"/>
				<textElement textAlignment="Justified">
					<font fontName="Verdana"/>
				</textElement>
				<text><![CDATA[Please note all damages/missing items on the inventory at the time of delivery.  Failure to provide the insurance carrier with adequate notice of a claim may void your insurance coverage.  Please check with your insurance carrier as soon as possible to ascertain the allowable timeframe for claims submission.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-12" x="2" y="401" width="533" height="15" uuid="64e32e98-bc9c-42c0-92e3-c9a2557d53d1"/>
				<textElement textAlignment="Justified">
					<font fontName="Verdana"/>
				</textElement>
				<text><![CDATA[Photographs of damaged items may be required by the insurance carrier.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-13" x="1" y="419" width="533" height="30" uuid="c6f163b5-86c0-499d-861b-61bc5e8a52d8"/>
				<textElement textAlignment="Justified">
					<font fontName="Verdana" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[PLEASE DO NOT DISPOSE OF BROKEN ITEMS WITHOUT WRITTEN CONSENT FROM THE INSURANCE CARRIER]]></text>
			</staticText>
			<line>
				<reportElement key="line-3" x="1" y="479" width="534" height="1" uuid="f18bbb19-1624-4bd3-a125-fb5937527763"/>
				<graphicElement fill="Solid">
					<pen lineWidth="1.0" lineStyle="Dashed"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement key="staticText-15" x="1" y="483" width="533" height="15" uuid="af290db1-21ff-4911-8f19-cd610174febd"/>
				<textElement textAlignment="Justified">
					<font fontName="Verdana" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[PROPERTY DAMAGE RELEASE AT DESTINATION]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-16" x="1" y="506" width="534" height="102" uuid="c82b7fca-ba5f-4450-bdc9-f8fa90b4e0bb"/>
				<textElement>
					<font fontName="Verdana"/>
				</textElement>
				<text><![CDATA[I, _________________________, acknowledge that I am responsible for the final walk-through at my destination residence.  It is my obligation to personally check the residence for damages.
Additionally, any floor protection which includes masonite, carpet runners and or plastic masking has been removed.  If floor protection is not removed immediately upon completion of the move, I assume all liability.  During the final walkthrough of the destination residence, there was no evidence of property damage including home driveway, landscape, etc. except as noted below.
]]></text>
			</staticText>
			<line>
				<reportElement key="line-4" x="2" y="656" width="532" height="1" uuid="7bf829cd-7111-48f7-ae2e-1cd7e7110221"/>
			</line>
			<line>
				<reportElement key="line-5" x="2" y="680" width="532" height="1" uuid="5192e654-3faf-41ff-8e28-2c8af27e65ef"/>
			</line>
			<line>
				<reportElement key="line-6" x="2" y="706" width="532" height="1" uuid="c1afc803-7747-40be-883f-d0cf06f3ce22"/>
			</line>
			<line>
				<reportElement key="line-7" x="1" y="741" width="205" height="1" uuid="8a14eaba-a93c-4ce3-858a-3a14636d0ca9"/>
			</line>
			<line>
				<reportElement key="line-8" x="313" y="741" width="205" height="1" uuid="f28e964b-3272-4d75-876c-2c0f54acc384"/>
			</line>
			<staticText>
				<reportElement key="staticText-18" x="1" y="742" width="126" height="15" uuid="79ea906f-8866-49f1-b8fa-35028b9daede"/>
				<textElement>
					<font fontName="Verdana"/>
				</textElement>
				<text><![CDATA[Customer Signature	]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-19" x="313" y="742" width="78" height="15" uuid="77a30fa4-26da-43d8-9753-19122c6e688c"/>
				<textElement>
					<font fontName="Verdana"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-20" x="1" y="774" width="533" height="28" forecolor="#333333" uuid="e9d1e10b-8623-447f-9fb5-9cbc46a79b71"/>
				<textElement textAlignment="Justified">
					<font fontName="Verdana"/>
				</textElement>
				<text><![CDATA[The client should be aware that the moving companies liability is limited according to the conditions in NSAB Moving.]]></text>
			</staticText>
			<rectangle>
				<reportElement key="rectangle-1" x="57" y="268" width="15" height="14" uuid="96dc4fea-48a1-4da9-8ff8-cfa17a6f2f28"/>
			</rectangle>
			<rectangle>
				<reportElement key="rectangle-2" x="57" y="288" width="15" height="14" uuid="3e1dbc21-2c33-4123-8159-72e1e8f0a482"/>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="1" y="38" width="303" height="15" uuid="7bec6a23-8d6f-4bd7-b1fe-486012a5e57e"/>
				<textElement>
					<font fontName="Verdana" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_shippername}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="1" y="53" width="303" height="46" uuid="455d9f04-f506-43ce-bbdc-0920250ba04e"/>
				<textElement>
					<font fontName="Verdana" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{serviceorder_destinationAddressLine1_2}.equals("") || $F{serviceorder_destinationAddressLine1_2}==null ? "" : ($F{serviceorder_destinationAddressLine1_2}+"\n"))+($F{serviceorder_destinationAddressLine3}.equals("") || $F{serviceorder_destinationAddressLine3}== null ? "" : ($F{serviceorder_destinationAddressLine3}+"\n"))+$F{serviceorder_destination_CityStateZip}+"\n"+($F{serviceorder_destinationCountryCode}.equals("USA") ? "" :($F{serviceorder_destinationCountry}))]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="36" y="99" width="254" height="15" uuid="4f708b3e-773b-476b-a9a8-c2f6905b0891"/>
				<textElement>
					<font fontName="Verdana" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{miscellaneous_netEstimateCubicFeet}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-21" x="1" y="114" width="78" height="15" uuid="cf931a3f-da0b-49bb-9b2f-7a41bac2c757"/>
				<textElement textAlignment="Left">
					<font fontName="Verdana" size="9"/>
				</textElement>
				<text><![CDATA[Ref #:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="27" y="114" width="253" height="15" uuid="c2d0c997-bdc4-4eb8-ba90-83b998c41057"/>
				<textElement>
					<font fontName="Verdana" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-22" x="1" y="99" width="54" height="15" uuid="100c7193-5b98-4cfa-afc9-dc5db4114d88"/>
				<textElement>
					<font fontName="Verdana" size="9"/>
				</textElement>
				<text><![CDATA[Volume:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" x="1" y="447" width="533" height="31" uuid="ebebd1ad-e3ad-4991-ba12-4381953364eb"/>
				<textFieldExpression><![CDATA["If you have any questions or need additional information, please contact "+($F{systemdefault_Company}==null ? "" : $F{systemdefault_Company})+" AS."]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-2" x="1" y="599" width="534" height="57" uuid="71070759-9ca9-4125-86a7-30b10b47cb08"/>
				<textFieldExpression><![CDATA["There is no evidence of any leaks or running water at my residence.  Any damage claims to soft flooring, plants/pots, assembled particle board items, unpacked lamps, pictures, paintings, glass/marble tops, mirrors/unstable furniture and any non-third party serviced items, will not be "+($F{systemdefault_Company}==null ? "" : $F{systemdefault_Company})+" AS responsibility."]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
