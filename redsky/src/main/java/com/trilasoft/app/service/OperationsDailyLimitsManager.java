package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.OperationsDailyLimits;

public interface OperationsDailyLimitsManager  extends GenericManager<OperationsDailyLimits, Long> {
	
	public List isExisted(Date workDate, String hub, String sessionCorpID);

	public List getDailyLimitsByHub(String hub, Date newWorkDate, String sessionCorpID, String category, String resource);
	public void updateWorkDate(String hub, String newWorkDate,Long id, String sessionCorpID);
	public List getListById(Long id, String sessionCorpID);
	public void updateDailyLimitParent(Integer tempDailyLimit,Integer tempDailyLimitNew,Integer tempDailyLimitOld, String sessionCorpID,String hubID,Date workDate);
	public void updateDailyMaxEstWtParent(BigDecimal tempDailymaxWt,BigDecimal tempDailyMaxWtNew,BigDecimal tempDailyMaxWtOld, String sessionCorpID,String hubID,Date workDate);
	public void updateDailyMaxEstVolParent(BigDecimal tempDailyMaxEstVol,BigDecimal tempDailyMaxEstVolNew,BigDecimal tempDailyMaxEstVolOld, String sessionCorpID,String hubID,Date workDate);
	public void updateDailyCrewLimitParent(Integer tempDailyCrewLimit,Integer tempDailyCrewLimitNew,Integer tempDailyCrewLimitOld, String sessionCorpID,String hubID,Date workDate);
}
