package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.JobStatus;

public interface JobStatusManager extends GenericManager<JobStatus, Long>{
	public List findMaximumId();
	public void increaseToTalCount(Long id, String sessionCorpID);
	public void updatePresentCount(Long countTotalNumber, String sessionCorpID, Long id, Long totCount);
	public List getTotalCount(String sessionCorpID, Long id);
	public List getPresentCount(String sessionCorpID, Long id);
	public List getList(String sessionCorpID);
	public List getStatus(String sessionCorpID);
}
