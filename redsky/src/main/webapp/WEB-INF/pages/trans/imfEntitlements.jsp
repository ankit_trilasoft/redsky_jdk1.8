<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Entitlement</title> 
<meta name="heading" content="Entitlement"/>

<style type="text/css">h2 {background-color: #FBBFFF}
.listwhitetext-h1 {
color:#003366;
font-family:arial,verdana;
font-size:12px;
font-weight:normal;
text-decoration:none;
}
input[type="checkbox"] {
vertical-align:middle;
}

</style>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
    <script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/common/formCalender.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
	<script language="javascript" type="text/javascript">
			var cal = new CalendarPopup(); 
			cal.showYearNavigation(); 
			cal.showYearNavigationInput();  
	</script>

<script language="JavaScript">
	function CalulateTotalNumber()
	{
		//alert("H");
		var TotalNumber;
		var N1=document.forms['imfEntitlements'].elements['imfEntitlements.airEntitlementsFreight'].value;
		var N2=document.forms['imfEntitlements'].elements['imfEntitlements.airEntitlementsOrigin'].value;
		var N3=document.forms['imfEntitlements'].elements['imfEntitlements.airEntitlementsDestination'].value;
		var N4=document.forms['imfEntitlements'].elements['imfEntitlements.surfaceEntitlementsFreight'].value;
		var N5=document.forms['imfEntitlements'].elements['imfEntitlements.surfaceEntitlementsOrigin'].value;
		var N6=document.forms['imfEntitlements'].elements['imfEntitlements.surfaceEntitlementsDestination'].value;
		
		TotalNumber=Number(N1)+Number(N2)+Number(N3)+Number(N4)+Number(N5)+Number(N6);
		document.forms['imfEntitlements'].elements['imfEntitlements.totalFreight'].value=TotalNumber.toFixed(2);
	  
	}
	
	function CalulateTotalAirEntitlements()
	{
		var TotalNumber=0*1;
		var N1=(document.forms['imfEntitlements'].elements['imfEntitlements.airEntitlementsFreight'].value);
		var N2=(document.forms['imfEntitlements'].elements['imfEntitlements.airEntitlementsOrigin'].value);
		var N3=(document.forms['imfEntitlements'].elements['imfEntitlements.airEntitlementsDestination'].value);
		TotalNumber=(N1*1+N2*1+N3*1);
		
		document.forms['imfEntitlements'].elements['imfEntitlements.airEntitlementsTotal'].value=TotalNumber.toFixed(2);
	  	
	}
	
	function CalulateTotalSurfaceEntitlements()
	{
		//alert("H");
		var TotalNumber=0*1;
		var N4=document.forms['imfEntitlements'].elements['imfEntitlements.surfaceEntitlementsFreight'].value;
		var N5=document.forms['imfEntitlements'].elements['imfEntitlements.surfaceEntitlementsOrigin'].value;
		var N6=document.forms['imfEntitlements'].elements['imfEntitlements.surfaceEntitlementsDestination'].value;
		
		TotalNumber=N4*1+N5*1+N6*1;
		document.forms['imfEntitlements'].elements['imfEntitlements.surfaceEntitlementsTotal'].value=TotalNumber.toFixed(2);
	  
	}
	
	function CalulateTotalOfficeEntitlements()
	{
		//alert("H");
		var TotalNumber=0*1;
		var N1=document.forms['imfEntitlements'].elements['imfEntitlements.officeEntitlementsFreight'].value;
		var N2=document.forms['imfEntitlements'].elements['imfEntitlements.officeEntitlementsOrigin'].value;
		var N3=document.forms['imfEntitlements'].elements['imfEntitlements.officeEntitlementsDestination'].value;
		TotalNumber=N1*1+N2*1+N3*1;
		document.forms['imfEntitlements'].elements['imfEntitlements.officeEntitlementsTotal'].value=TotalNumber.toFixed(2);
	  
	}
	
	function CalulateTotalAutoEntitlements()
	{
		//alert("H");
		var TotalNumber=0*1;
		var N1=document.forms['imfEntitlements'].elements['imfEntitlements.autoEntitlementsFreight'].value;
		var N2=document.forms['imfEntitlements'].elements['imfEntitlements.autoEntitlementsOrigin'].value;
		var N3=document.forms['imfEntitlements'].elements['imfEntitlements.autoEntitlementsDestination'].value;
		TotalNumber=N1*1+N2*1+N3*1;
		document.forms['imfEntitlements'].elements['imfEntitlements.autoEntitlementsTotal'].value=TotalNumber.toFixed(2);
	  
	}
	
	function isFloat(targetElement)
{   var i;
	var s = targetElement.value;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        if (c == ".") {
        
        }else{
        alert("Only numbers are allowed here");
        //alert(targetElement.id);
        document.getElementById(targetElement.id).value='';
        document.getElementById(targetElement.id).select();
        return false;
        }
    }
    }
    return true;
}



var r={
 'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\,'&'\`'&'\=']/g,
 'quotes':/['\''&'\"']/g,
 'notnumbers':/[^\d]/g
};

function valid(targetElement,w){
 targetElement.value = targetElement.value.replace(r[w],'');
}

function chkSelect()
	{
	
		if (checkFloat('imfEntitlements','imfEntitlements.airEntitlementsWeight','Invalid data in Air Entitlements Weight') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.airEntitlementsWeight'].focus();
              return false
           }
        if (checkFloat('imfEntitlements','imfEntitlements.airEntitlementsFreight','Invalid data in Air Entitlements Freight') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.airEntitlementsFreight'].focus();
              return false
           }
        if (checkFloat('imfEntitlements','imfEntitlements.airEntitlementsOrigin','Invalid data in Air Entitlements Origin') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.airEntitlementsOrigin'].focus();
              return false
           }
        if (checkFloat('imfEntitlements','imfEntitlements.airEntitlementsDestination','Invalid data in Air Entitlements Destination') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.airEntitlementsDestination'].focus();
              return false
           }
         if (checkFloat('imfEntitlements','imfEntitlements.surfaceEntitlementsWeight','Invalid data in Surface Entitlements Weight') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.surfaceEntitlementsWeight'].focus();
              return false
           }
         if (checkFloat('imfEntitlements','imfEntitlements.surfaceEntitlementsFreight','Invalid data in Surface Entitlements Freight') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.surfaceEntitlementsFreight'].focus();
              return false
           }
         if (checkFloat('imfEntitlements','imfEntitlements.surfaceEntitlementsOrigin','Invalid data in Surface Entitlements Origin') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.surfaceEntitlementsOrigin'].focus();
              return false
           }
         if (checkFloat('imfEntitlements','imfEntitlements.surfaceEntitlementsDestination','Invalid data in Surface Entitlements Destination') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.surfaceEntitlementsDestination'].focus();
              return false
           }
         if (checkFloat('imfEntitlements','imfEntitlements.autoEntitlementsWeight','Invalid data in Auto Entitlements Weight') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.autoEntitlementsWeight'].focus();
              return false
           }
         if (checkFloat('imfEntitlements','imfEntitlements.autoEntitlementsFreight','Invalid data in Auto Entitlements Freight') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.autoEntitlementsFreight'].focus();
              return false
           }
           
           if (checkFloat('imfEntitlements','imfEntitlements.autoEntitlementsOrigin','Invalid data in Auto Entitlements Origin') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.autoEntitlementsOrigin'].focus();
              return false
           }
         if (checkFloat('imfEntitlements','imfEntitlements.autoEntitlementsDestination','Invalid data in Auto Entitlements Destination') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.autoEntitlementsDestination'].focus();
              return false
           }
         if (checkFloat('imfEntitlements','imfEntitlements.officeEntitlementsWeight','Invalid data in Office Entitlements Weight') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.officeEntitlementsWeight'].focus();
              return false
           }
         if (checkFloat('imfEntitlements','imfEntitlements.officeEntitlementsFreight','Invalid data in Office Entitlements Freight') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.officeEntitlementsFreight'].focus();
              return false
           } 
           
           if (checkFloat('imfEntitlements','imfEntitlements.officeEntitlementsOrigin','Invalid data in Office Entitlements Origin') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.officeEntitlementsOrigin'].focus();
              return false
           }
         if (checkFloat('imfEntitlements','imfEntitlements.officeEntitlementsDestination','Invalid data in Office Entitlements Destination') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.officeEntitlementsDestination'].focus();
              return false
           }
         if (checkFloat('imfEntitlements','imfEntitlements.insurance','Invalid data in Insurance') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.insurance'].focus();
              return false
           }
         if (checkFloat('imfEntitlements','imfEntitlements.rate','Invalid data in Rate') == false)
           {
              document.forms['imfEntitlements'].elements['imfEntitlements.rate'].focus();
              return false
           }  
             
         
     }
     
   function focusDate(target)
{
	document.forms['imfEntitlements'].elements[target].focus();
}

function changeStatus(){
	document.forms['imfEntitlements'].elements['formStatus'].value = '1';
}

function autoSave(clickType){
	
	if ('${autoSavePrompt}' == 'No'){
	
		document.forms['imfEntitlements'].action = 'saveImfEntitlements!saveOnTabChange.html';
		document.forms['imfEntitlements'].submit();
	
	}
	else{
	if(!(clickType == 'save')){
	var id1 = document.forms['imfEntitlements'].elements['customerFile.id'].value;
	var code= document.forms['imfEntitlements'].elements['customerFile.billToCode'].value;	
	if (document.forms['imfEntitlements'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='imfEntitlements.heading'/>");
		if(agree){
			document.forms['imfEntitlements'].action = 'saveImfEntitlements!saveOnTabChange.html';
			document.forms['imfEntitlements'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['imfEntitlements'].elements['gotoPageString'].value == 'gototab.customerFile'){
				location.href = 'editCustomerFile.html?id='+id1;
				}
			if(document.forms['imfEntitlements'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'customerServiceOrders.html?id='+id1;
				}
			if(document.forms['imfEntitlements'].elements['gotoPageString'].value == 'gototab.contract'){
				location.href = 'customerRateOrders.html?id='+id1;
				}
			if(document.forms['imfEntitlements'].elements['gotoPageString'].value == 'gototab.surveys'){
				location.href = 'surveysList.html?id='+id1;
				}
			if(document.forms['imfEntitlements'].elements['gotoPageString'].value == 'gototab.accountPolicy'){
				location.href = 'showAccountPolicy.html?id='+id1+'&code='+code;
				}
			if(document.forms['imfEntitlements'].elements['gotoPageString'].value == 'gototab.reports'){
				//location.href = 'sortCustomerReportss.html?id='+id1;
				location.href = 'subModuleReports.html?id='+id1+'&jobNumber='+jobNumber+'&reportModule=customerFile&reportSubModule=customerFile';
				}
			
					
		}
		}
	}else{
	if(id1 != ''){
		if(document.forms['imfEntitlements'].elements['gotoPageString'].value == 'gototab.customerFile'){
				location.href = 'editCustomerFile.html?id='+id1;
				}
		if(document.forms['imfEntitlements'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'customerServiceOrders.html?id='+id1;
				}		
			if(document.forms['imfEntitlements'].elements['gotoPageString'].value == 'gototab.contract'){
				location.href = 'customerRateOrders.html?id='+id1;
				}
			if(document.forms['imfEntitlements'].elements['gotoPageString'].value == 'gototab.surveys'){
				location.href = 'surveysList.html?id='+id1;
				}
			if(document.forms['imfEntitlements'].elements['gotoPageString'].value == 'gototab.accountPolicy'){
				location.href = 'showAccountPolicy.html?id='+id1+'&code='+code;
				}
			if(document.forms['imfEntitlements'].elements['gotoPageString'].value == 'gototab.reports'){
				//location.href = 'sortCustomerReportss.html?id='+id1;
				location.href = 'subModuleReports.html?id='+id1+'&jobNumber='+jobNumber+'&reportModule=customerFile&reportSubModule=customerFile';
				}
			
			
	}
	}
}
}
}
   
</script>


</head>

<s:form id="imfEntitlements" name="imfEntitlements" action="saveImfEntitlements" method="post" validate="true">
 <s:hidden name="imfEntitlements.sequenceNumber" value="${customerFile.sequenceNumber }" />
 <s:hidden name="customerFile.id" />
 <s:hidden name="customerFile.billToCode" />
 <s:hidden name="imfEntitlements.id" />
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
  <s:hidden id="countImfEntitlementNotes" name="countImfEntitlementNotes" value="<%=request.getParameter("countImfEntitlementNotes") %>"/>
 <c:set var="countImfEntitlementNotes" value="<%=request.getParameter("countImfEntitlementNotes") %>" />

<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
<c:if test="${empty customerFile.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty customerFile.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>

<input type="hidden" name="encryptPass" value="true" />

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:choose>
<c:when test="${gotoPageString == 'gototab.customerFile' }">
    <c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
    <c:redirect url="/customerServiceOrders.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.rateRequest' }">
    <c:redirect url="/customerRateOrders.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.surveys' }">
    <c:redirect url="/surveysList.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountPolicy' }">
    <c:redirect url="/showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.reports' }">
    <c:redirect url="/subModuleReports.html?id=${customerFile.id}&jobNumber=${customerFile.sequenceNumber}&reportModule=customerFile&reportSubModule=customerFile"/>
</c:when>

<c:otherwise>
</c:otherwise>
</c:choose>

<div id="Layer1" style="width:973px" onkeydown="changeStatus();"> 

		<div id="newmnav">
		  <ul>
		  	
		  	<li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.customerFile');return autoSave();"><span>Customer File</span></a></li>
		  	<li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.serviceorder');return autoSave();"><span>Service Order</span></a></li>
		  	<li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.rateRequest');return autoSave();"><span>Rate Request</span></a></li>
		  	<!--li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.surveys');return autoSave();"><span>Surveys</span></a></li>-->
		  	<li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.accountPolicy');return autoSave();"><span>Account Policy</span></a></li>
		  	<!-- <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.reports');return autoSave();"><span>Forms</span></a></li> -->
		  	<!-- <li><a onclick="openWindow('subModuleReports.html?id=${customerFile.id}&jobNumber=${customerFile.sequenceNumber}&reportModule=customerFile&reportSubModule=customerFile&formReportFlag=F&decorator=popup&popup=true',750,400)"><span>Forms</span></a></li> -->
		  	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=customerFile&reportSubModule=customerFile&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		  	
		  	<!--<li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
		    <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		    <li><a href="surveysList.html?id=${customerFile.id} "><span>Surveys</span></a></li>
		    <li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" ><span>Account Policy</span></a></li> 
		  	<li><a href="sortCustomerReportss.html?id=${customerFile.id}"><span>Forms</span></a></li>  
		  	<li><a onclick="openWindow('myFiles.html?id=${customerFile.id }&myFileFor=CF&decorator=popup&popup=true',740,400);" ><span>Docs</span></a></li>-->
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Entitlement<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  	<sec-auth:authComponent componentId="module.tab.trackingStatus.auditTab">
           	 <li><a onclick="window.open('auditList.html?id=${imfEntitlements.id}&tableName=imfentitlements&decorator=popup&popup=true','audit','height=400,width=770,top=100, left=120, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
           	  </sec-auth:authComponent>
		  </ul>
		</div><div class="spn">&nbsp;</div>
		<div style="padding-bottom:3px;"></div>
	<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:-3px "><span></span></div>
   <div class="center-content">

<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:98%">
		<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>
					<tr>
					    <td width="20px"></td>
						<td align="right" class="listwhitebox">Cust#</td>
						<td colspan="2"><s:textfield name="customerFile.sequenceNumber" size="28" readonly="true" cssClass="input-textUpper" /></td>
						
						<td width="20px"></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" size="15" readonly="true" cssClass="input-textUpper"/></td>
						<td></td>
						<td width="20px"></td>
						<td align="left" class="listwhitebox">IMF&nbsp;SMIS&nbsp;#</td>
						<td colspan="2"><s:textfield name="customerFile.billToReference" required="true" size="27" readonly="true" cssClass="input-textUpper" /></td>
						
						
						
					</tr>
					<tr>
						<td width="20px"></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td ><s:textfield name="customerFile.originCountryCode" required="true" size="5" readonly="true" cssClass="input-textUpper"/></td>
						<td width="20px"></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" size="10" readonly="true" cssClass="input-textUpper" /></td>
						<td></td>
						<td width="20px"></td>
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" size="10" readonly="true" cssClass="input-textUpper" /></td>
						
						
						
						</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:-3px "><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="1" cellpadding="1" border="0" style="width:98%">
	  <tbody>
	  <tr>
	  	<td align="left" class="listwhitetext">
	  	<table class="detailTabLabel" border="0">
			  <tbody>		  	
				<tr>
				<c:set var="ischecked" value="false"/>
				<c:if test="${imfEntitlements.checkAirEntitlementsWeight}">
				<c:set var="ischecked" value="true"/>
				</c:if>
				<c:set var="ischecked1" value="false"/>
				<c:if test="${imfEntitlements.checkSurfaceEntitlementsWeight}">
				<c:set var="ischecked1" value="true"/>
				</c:if>
				<c:set var="ischecked2" value="false"/>
				<c:if test="${imfEntitlements.checkAutoEntitlementsWeight}">
				<c:set var="ischecked2" value="true"/>
				</c:if>
				<c:set var="ischecked3" value="false"/>
				<c:if test="${imfEntitlements.checkOfficeEntitlementsWeight}">
				<c:set var="ischecked3" value="true"/>
				</c:if>
				<c:set var="ischecked4" value="false"/>
				<c:if test="${imfEntitlements.permStorage}">
				<c:set var="ischecked4" value="true"/>
				</c:if>
			<td style="width:390px">
				<table border="0" style="width:330px">
					<tr>
						<td  align="right" colspan="2" class="listwhitetext-h1" style="padding-right:5px;"><B>Air&nbsp;PERS&nbsp;Entitlements</B></td> 
			    	</tr>
			    	<tr>
				    	<td align="right"><fmt:message key="imfEntitlements.weight"/></td>
						<td align="left"><s:textfield name="imfEntitlements.airEntitlementsWeight"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" maxlength="12" size="12" cssClass="input-text" readonly="false" cssStyle="text-align:right"/></td>
						<td align="left" valign="top" class="listwhitetext"><s:checkbox key="imfEntitlements.checkAirEntitlementsWeight" value="${ischecked}" fieldValue="true" onclick="changeStatus();"/>Required&nbsp;?
						
					</tr>
					<tr>	
						<td align="right"><fmt:message key="imfEntitlements.freight"/></td>
						<td align="left"><s:textfield name="imfEntitlements.airEntitlementsFreight"  onkeydown="return onlyNumsAllowed(event)"  onblur="valid(this,'special');CalulateTotalNumber();CalulateTotalAirEntitlements();" maxlength="12" size="12" cssClass="input-text" cssStyle="text-align:right"/></td>
					</tr>
					<tr>	
						<td align="right"><fmt:message key="imfEntitlements.origin"/></td>
						<td align="left"><s:textfield name="imfEntitlements.airEntitlementsOrigin"  onkeydown="return onlyNumsAllowed(event)"   onblur="valid(this,'special');CalulateTotalNumber();CalulateTotalAirEntitlements();" maxlength="12" size="12" cssClass="input-text" cssStyle="text-align:right"/></td>
					</tr>
					<tr>
						<td align="right"><fmt:message key="imfEntitlements.destination"/></td>
						<td align="left"><s:textfield name="imfEntitlements.airEntitlementsDestination"  onkeydown="return onlyNumsAllowed(event)"  onblur="valid(this,'special');CalulateTotalNumber();CalulateTotalAirEntitlements();" maxlength="12" size="12" cssClass="input-text" cssStyle="text-align:right"/></td>
			    	</tr>
			    	<tr>
						<td align="right"><B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;Air&nbsp;Entitlements</B></td>
						<td align="left"><s:textfield name="imfEntitlements.airEntitlementsTotal"  maxlength="12" size="12" cssClass="input-textUpper" readonly="true" cssStyle="text-align:right"/></td>
			    	</tr>
				</table>
			</td>
			<td style="width:400px">
				<table  border="0" style="width:340px">
					<tr>
						<td align="center" colspan="2" class="listwhitetext-h1" style="padding-left: 95px;"><B>Surface&nbsp;HHG&nbsp;Entitlements</B></td> 
			    	</tr>
			    	<tr>
				    	<td align="right"><fmt:message key="imfEntitlements.weight"/></td>
						<td align="left"><s:textfield name="imfEntitlements.surfaceEntitlementsWeight"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" maxlength="12" size="12" cssClass="input-text" readonly="false" cssStyle="text-align:right"/></td>
						<td align="left" valign="top" class="listwhitetext"><s:checkbox key="imfEntitlements.checkSurfaceEntitlementsWeight" value="${ischecked1}" fieldValue="true" onclick="changeStatus();"/>Required&nbsp;?
					</tr>
					<tr>
						<td align="right"><fmt:message key="imfEntitlements.freight"/></td>
						<td align="left"><s:textfield name="imfEntitlements.surfaceEntitlementsFreight"  onkeydown="return onlyNumsAllowed(event)"   onblur="valid(this,'special');CalulateTotalNumber();CalulateTotalSurfaceEntitlements();" maxlength="12" size="12" cssClass="input-text" cssStyle="text-align:right"/></td>
					</tr>
					<tr>
						<td align="right"><fmt:message key="imfEntitlements.origin"/></td>
						<td align="left"><s:textfield name="imfEntitlements.surfaceEntitlementsOrigin"  onkeydown="return onlyNumsAllowed(event)"   onblur="valid(this,'special');CalulateTotalNumber();CalulateTotalSurfaceEntitlements();" maxlength="12" size="12" cssClass="input-text" cssStyle="text-align:right"/></td>
					</tr>
					<tr>
						<td align="right"><fmt:message key="imfEntitlements.destination"/></td>
						<td align="left"><s:textfield name="imfEntitlements.surfaceEntitlementsDestination"  onkeydown="return onlyNumsAllowed(event)"   onblur="valid(this,'special');CalulateTotalNumber();CalulateTotalSurfaceEntitlements();" maxlength="12" size="12" cssClass="input-text" cssStyle="text-align:right"/></td>
			    	</tr>
			    	<tr>
						<td align="right"><B>Total&nbsp;Surface&nbsp;Entitlements</B></td>
						<td align="left"><s:textfield name="imfEntitlements.surfaceEntitlementsTotal"  maxlength="12" size="12" cssClass="input-textUpper" readonly="true" cssStyle="text-align:right"/></td>
			    	</tr>
				</table>	
		</td>
		<td align="left">
			<table width="240" border="0">
				<tr>
					<c:if test="${empty customerFile.id}">
					<td align="right" colspan="2"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
					</c:if>
					<c:if test="${not empty customerFile.id}">
					<c:choose>
					<c:when test="${countImfEntitlementNotes == '0' || countImfEntitlementNotes == '' || countImfEntitlementNotes == null}">
					<td align="right" colspan="2"><img id="countImfEntitlementNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=IMF Entitlement Notes&imageId=countImfEntitlementNotesImage&fieldId=countImfEntitlementNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=IMF Entitlement Notes&imageId=countImfEntitlementNotesImage&fieldId=countImfEntitlementNotes&decorator=popup&popup=true',800,600);" ></a></td>
					</c:when>
					<c:otherwise>
					<td align="right" colspan="2"><img id="countImfEntitlementNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=IMF Entitlement Notes&imageId=countImfEntitlementNotesImage&fieldId=countImfEntitlementNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=IMF Entitlement Notes&imageId=countImfEntitlementNotesImage&fieldId=countImfEntitlementNotes&decorator=popup&popup=true',800,600);" ></a></td>
					</c:otherwise>
					</c:choose> 
					</c:if>
				</tr>
				<tr><td height="10"></td></tr>	
				<tr>
					<td align="right" class="listwhitebox"><B>Air&nbsp;+&nbsp;Sea&nbsp;$</B></td>
					<td align="left" width="190px"><s:textfield name="imfEntitlements.totalFreight" required="true" maxlength="16" size="15" readonly="true" cssClass="input-textUpper"  cssStyle="text-align:right"/></td>
				</tr>
				
				<tr>
			    	<td align="right" width="85"><fmt:message key="imfEntitlements.readyToSend"/></td>
					<c:if test="${not empty imfEntitlements.readyToSend}">
					<s:text id="imfEntitlementsDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="imfEntitlements.readyToSend"/></s:text>
					<td><s:textfield cssClass="input-textUpper" id="readyToSend" name="imfEntitlements.readyToSend" value="%{imfEntitlementsDateFormattedValue}" readonly="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="readyToSend_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty imfEntitlements.readyToSend}">
					<td><s:textfield cssClass="input-textUpper" id="readyToSend" name="imfEntitlements.readyToSend"  size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="readyToSend_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				</tr>
	    		<tr>
	    			<td align="right"><fmt:message key="imfEntitlements.sent"/></td>
					<c:if test="${not empty imfEntitlements.sent}">
					<s:text id="sentDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="imfEntitlements.sent"/></s:text>
					<td><s:textfield cssClass="input-textUpper" id="sent" name="imfEntitlements.sent" value="%{sentDateFormattedValue}" readonly="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="sent_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty imfEntitlements.sent}">
					<td><s:textfield cssClass="input-textUpper" id="sent" name="imfEntitlements.sent"  size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="sent_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				</tr>
				
				
				
			</table>	
		</td>
	</tr>
	<tr>
		<td style="width:390px">
			<table border="0" width="330px">
				<tr>
					<td  align="right" colspan="2" class="listwhitetext-h1" style="padding-right:5px;"><B>Office&nbsp;Entitlements</B></td>
		    	</tr>
		    	<tr>
			    	<td align="right"><fmt:message key="imfEntitlements.weight"/></td>
					<td align="left"><s:textfield name="imfEntitlements.officeEntitlementsWeight"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" maxlength="12" size="12" cssClass="input-text" readonly="false" cssStyle="text-align:right"/></td>
					<td align="left" valign="top" class="listwhitetext"><s:checkbox key="imfEntitlements.checkOfficeEntitlementsWeight" value="${ischecked3}" fieldValue="true" onclick="changeStatus();"/>Required&nbsp;?
				</tr>
		    	<tr>
			    	<td align="right"><fmt:message key="imfEntitlements.freight"/></td>
					<td align="left"><s:textfield name="imfEntitlements.officeEntitlementsFreight" onblur="valid(this,'special');CalulateTotalOfficeEntitlements();" onkeydown="return onlyNumsAllowed(event)"   maxlength="12" size="12" cssClass="input-text" readonly="false" cssStyle="text-align:right"/></td>
				</tr>
		    	<tr>
		    		<td align="right"><fmt:message key="imfEntitlements.origin"/></td>
					<td align="left"><s:textfield name="imfEntitlements.officeEntitlementsOrigin" onblur="valid(this,'special');CalulateTotalOfficeEntitlements();" onkeydown="return onlyNumsAllowed(event)"   maxlength="12" size="12" cssClass="input-text" readonly="false" cssStyle="text-align:right"/></td>
				</tr>
		    	<tr>
		    		<td align="right"><fmt:message key="imfEntitlements.destination"/></td>
					<td align="left"><s:textfield name="imfEntitlements.officeEntitlementsDestination" onblur="valid(this,'special');CalulateTotalOfficeEntitlements();" onkeydown="return onlyNumsAllowed(event)"   maxlength="12" size="12" cssClass="input-text" readonly="false" cssStyle="text-align:right"/></td>
		    	</tr>
		    	<tr>
		    		<td align="right"><B>Total&nbsp;Office&nbsp;Entitlements</B></td>
					<td align="left"><s:textfield name="imfEntitlements.officeEntitlementsTotal"   maxlength="12" size="12" cssClass="input-textUpper" readonly="true" cssStyle="text-align:right"/></td>
			    	</tr>
			</table>	
		</td>
		
		<td style="width:420px">
			<table border="0" width="340px">
				<tr>
					<td  align="right" colspan="2" class="listwhitetext-h1" style="padding-right:5px;"><B>Auto&nbsp;Entitlements</B></td> 
		    	</tr>
		    	<tr>
			    	<td align="right"><fmt:message key="imfEntitlements.weight"/></td>
					<td align="left"><s:textfield name="imfEntitlements.autoEntitlementsWeight"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" maxlength="12" size="12" cssClass="input-text" readonly="false" cssStyle="text-align:right"/></td>
					<td align="left" valign="top" class="listwhitetext"><s:checkbox key="imfEntitlements.checkAutoEntitlementsWeight" value="${ischecked2}" fieldValue="true" onclick="changeStatus();"/>Required&nbsp;?
				</tr>
		    	<tr>
			    	<td align="right"><fmt:message key="imfEntitlements.freight"/></td>
					<td align="left"><s:textfield name="imfEntitlements.autoEntitlementsFreight" onblur="valid(this,'special');CalulateTotalAutoEntitlements();" onkeydown="return onlyNumsAllowed(event)"   maxlength="12" size="12" cssClass="input-text" readonly="false" cssStyle="text-align:right"/></td>
				</tr>
		    	<tr>
		    		<td align="right"><fmt:message key="imfEntitlements.origin"/></td>
					<td align="left"><s:textfield name="imfEntitlements.autoEntitlementsOrigin" onblur="valid(this,'special');CalulateTotalAutoEntitlements();" onkeydown="return onlyNumsAllowed(event)"   maxlength="12" size="12" cssClass="input-text" readonly="false" cssStyle="text-align:right"/></td>
				</tr>
		    	<tr>
		    		<td align="right"><fmt:message key="imfEntitlements.destination"/></td>
					<td align="left"><s:textfield name="imfEntitlements.autoEntitlementsDestination" onblur="valid(this,'special');CalulateTotalAutoEntitlements();" onkeydown="return onlyNumsAllowed(event)"   maxlength="12" size="12" cssClass="input-text" readonly="false" cssStyle="text-align:right"/></td>
		    	</tr>
				<tr>
					<td align="right"><B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;Auto&nbsp;Entitlements</B></td>
					<td align="left"><s:textfield name="imfEntitlements.autoEntitlementsTotal"  maxlength="12" size="12" cssClass="input-textUpper" readonly="true" cssStyle="text-align:right"/></td>
			    </tr>
			
			
			
			
			</table>	
		</td>
		
		<td style="width:350px">
			<table>
			
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr><tr>
					<td>&nbsp;</td>
				</tr>
				
	    		
	    	<!--<tr>
		    	<td align="right"><fmt:message key="imfEntitlements.insurance"/></td>
				<td align="left"><s:textfield name="imfEntitlements.insurance"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" maxlength="12" size="12" cssClass="input-text" readonly="false"/></td>
			</tr>
	    	<tr>
				<td align="right"><fmt:message key="imfEntitlements.rate"/></td>
				<td align="left"><s:textfield name="imfEntitlements.rate"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" maxlength="12" size="12" cssClass="input-text" readonly="false"/></td>
			</tr>
	    	--><tr>
	    		
	    		<td align="right"><fmt:message key="imfEntitlements.permStorage"/></td>
	    		<td align="left" valign="top" class="listwhitetext"><s:checkbox key="imfEntitlements.permStorage" value="${ischecked4}" fieldValue="true" onclick="changeStatus();"/>
				</tr>
	    	<tr>
	    		<td align="right" width="80px"><fmt:message key="imfEntitlements.costShare"/></td>
				<td align="left"><s:select cssClass="list-menu"   name="imfEntitlements.costShare" list="{'NO','ED','Res Rep'}" headerKey="" headerValue="" cssStyle="width:89px" onchange="changeStatus();"/></td>
	    	</tr>
				<tr>
					<td  align="center" colspan="2" class="listwhitetext">&nbsp;</td>
		    	</tr>
		    	
				
			</table>	
		</td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
		  
		<tr><td>
 		  <table class="detailTabLabel" border="0" style="width:730px">
				<tbody>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="imfEntitlementsCreatedOnFormattedValue" value="${imfEntitlements.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="imfEntitlements.createdOn" value="${imfEntitlementsCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${imfEntitlements.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty imfEntitlements.id}">
								<s:hidden name="imfEntitlements.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{imfEntitlements.createdBy}"/></td>
							</c:if>
							<c:if test="${empty imfEntitlements.id}">
								<s:hidden name="imfEntitlements.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="imfEntitlementsupdatedOnFormattedValue" value="${imfEntitlements.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="imfEntitlements.updatedOn" value="${imfEntitlementsupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${imfEntitlements.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty imfEntitlements.id}">
								<s:hidden name="imfEntitlements.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{imfEntitlements.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty imfEntitlements.id}">
								<s:hidden name="imfEntitlements.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table></td></tr>
		  
		  <table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" key="button.save" onmouseover="return chkSelect();"/>  
        		</td>
       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		       		
       	  	</tr>		  	
		  </tbody>
		  </table>
		
		
		
		
		</div>
		
		
		
</s:form>

<script type="text/javascript">
	try{
	var f = document.getElementById('imfEntitlements'); 
	}
	catch(e){}
	f.setAttribute("autocomplete", "off"); 
	try{
	CalulateTotalNumber();
	}
	catch(e){}
	try{
	CalulateTotalAirEntitlements();
	}
	catch(e){}
	try{
	CalulateTotalSurfaceEntitlements();
	}
	catch(e){}
	try{
	CalulateTotalOfficeEntitlements();
	}
	catch(e){}
	try{
	CalulateTotalAutoEntitlements();
	}
	catch(e){}
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
		