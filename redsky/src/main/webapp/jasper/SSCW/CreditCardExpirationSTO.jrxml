<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="CreditCardExpirationSTO"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="559"
		 columnSpacing="0"
		 leftMargin="18"
		 rightMargin="18"
		 topMargin="18"
		 bottomMargin="18"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.zoom" value="1.0" />
	<property name="ireport.x" value="0" />
	<property name="ireport.y" value="0" />
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT
     'CreditCardExpirationSTO-R-SSCW.jrxml',
     billing.`billtoname` AS billing_billtoname,
     billing.`billToCode` AS billing_billToCode,
     creditcard.`expireYear` AS creditcard_expireYear,
     creditcard.`expireMonth` AS creditcard_expireMonth,
     creditcard.`ccName` AS creditcard_ccName,
     concat('**** **** **** ',right(creditcard.`ccNumber`,4)) AS creditcard_ccNumber,
     creditcard.`ccType` AS creditcard_ccType,
     creditcard.`primaryFlag` AS creditcard_primaryFlag,
     billing.`shipNumber` AS billing_shipNumber,
     serviceorder.companydivision AS serviceorder_companydivision,
     serviceorder.lastname AS serviceorder_lastname,
     creditcard.`shipNumber` AS creditcard_shipNumber,
     billing.`isAutomatedStorageBillingProcessing` AS billing_isAutomatedStorageBillingProcessing,
     billing.`isCreditCard` AS billing_isCreditCard,
     if(billing.personBilling is null || billing.personBilling='',"ZZZZZ",billing.personBilling) AS billing_personBilling,
     datediff(date_format(concat(expireyear,'-',left(expiremonth,2),'-01'),'%Y-%m-%d'),sysdate()) as datedifferent
FROM `creditcard` creditcard 
INNER JOIN `billing` billing ON creditcard.`shipNumber` = billing.`shipNumber` and billing.`corpID` = $P{Corporate ID}
INNER JOIN `serviceorder` serviceorder ON serviceorder.`id` = billing.`id` and serviceorder.`corpID` = $P{Corporate ID}
WHERE creditcard.`corpID` = $P{Corporate ID}
 AND billing.`isautomatedstoragebillingprocessing` IS true
 AND creditcard.`primaryFlag` IS true
 AND billing.`storageout` is null
 and datediff(date_format(concat(expireyear,'-',left(expiremonth,2),'-01'),'%Y-%m-%d'),sysdate()) <= 70]]></queryString>

	<field name="CreditCardExpirationSTO-R-SSCW.jrxml" class="java.lang.String"/>
	<field name="billing_billtoname" class="java.lang.String"/>
	<field name="billing_billToCode" class="java.lang.String"/>
	<field name="creditcard_expireYear" class="java.lang.String"/>
	<field name="creditcard_expireMonth" class="java.lang.String"/>
	<field name="creditcard_ccName" class="java.lang.String"/>
	<field name="creditcard_ccNumber" class="java.lang.String"/>
	<field name="creditcard_ccType" class="java.lang.String"/>
	<field name="creditcard_primaryFlag" class="java.lang.String"/>
	<field name="billing_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_companydivision" class="java.lang.String"/>
	<field name="serviceorder_lastname" class="java.lang.String"/>
	<field name="creditcard_shipNumber" class="java.lang.String"/>
	<field name="billing_isAutomatedStorageBillingProcessing" class="java.lang.String"/>
	<field name="billing_isCreditCard" class="java.lang.String"/>
	<field name="billing_personBilling" class="java.lang.String"/>
	<field name="datedifferent" class="java.lang.Long"/>

	<sortField name="billing_personBilling" />
	<sortField name="creditcard_expireMonth" />
	<sortField name="serviceorder_lastname" />


		<group  name="name" >
			<groupExpression><![CDATA[$F{billing_personBilling}]]></groupExpression>
			<groupHeader>
			<band height="19"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="1"
						width="89"
						height="18"
						forecolor="#0066CC"
						key="staticText-61"/>
					<box></box>
					<textElement textAlignment="Left">
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Billing Person]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="89"
						y="1"
						width="163"
						height="18"
						forecolor="#0066CC"
						key="textField-13"/>
					<box></box>
					<textElement textAlignment="Left">
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_personBilling}.equalsIgnoreCase("ZZZZZ") ? "" : $F{billing_personBilling}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="68"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="52"
						y="0"
						width="457"
						height="27"
						forecolor="#CC0066"
						key="staticText-53"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Times New Roman" pdfFontName="Times-Bold" size="14" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Automatic Storage Credit Card which will expire in 2 months]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="51"
						width="140"
						height="17"
						key="staticText-55"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="140"
						y="51"
						width="123"
						height="17"
						key="staticText-56"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[CC #]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="263"
						y="51"
						width="40"
						height="17"
						key="staticText-57"/>
					<box></box>
					<textElement textAlignment="Right">
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Month]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="317"
						y="51"
						width="32"
						height="17"
						key="staticText-58"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Year]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="364"
						y="51"
						width="100"
						height="17"
						key="staticText-59"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Service Order #]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="263"
						y="40"
						width="103"
						height="17"
						key="staticText-60"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Times-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Expiration]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="464"
						y="51"
						width="95"
						height="17"
						key="staticText-59"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Times-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Company Division]]></text>
				</staticText>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="19"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="1"
						width="140"
						height="18"
						forecolor="#000000"
						key="textField-7"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{creditcard_ccName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="140"
						y="1"
						width="123"
						height="18"
						forecolor="#000000"
						key="textField-8"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{creditcard_ccNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="263"
						y="1"
						width="44"
						height="18"
						forecolor="#000000"
						key="textField-9"/>
					<box></box>
					<textElement textAlignment="Right">
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{creditcard_expireMonth}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="320"
						y="1"
						width="43"
						height="18"
						forecolor="#000000"
						key="textField-10"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{creditcard_expireYear}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="364"
						y="1"
						width="100"
						height="18"
						forecolor="#000000"
						key="textField-12"/>
					<box></box>
					<textElement>
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{creditcard_shipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="464"
						y="1"
						width="95"
						height="18"
						forecolor="#000000"
						key="textField-12"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_companydivision}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="17"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="166"
						y="3"
						width="226"
						height="14"
						key="staticText-54"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Times-Roman"/>
					</textElement>
				<text><![CDATA[CreditCardExpirationSTO.jrxml]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="403"
						y="3"
						width="117"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-4"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" pdfFontName="Times-Roman" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="3"
						y="3"
						width="129"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-5"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" pdfFontName="Times-Roman" size="10"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="530"
						y="3"
						width="29"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-6"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" pdfFontName="Times-Roman" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="559"
						height="1"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
