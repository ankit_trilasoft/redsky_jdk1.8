package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.PartnerBankInfoDao;
import com.trilasoft.app.model.PartnerBankInformation;

public class PartnerBankInfoDaoHibernate  extends GenericDaoHibernate <PartnerBankInformation, Long> implements PartnerBankInfoDao
{
	public PartnerBankInfoDaoHibernate() {
		super(PartnerBankInformation.class);
	}
	public List findBankData(String partnerCode, String sessionCorpID) {

		try
		{
		
		return  getHibernateTemplate().find("from PartnerBankInformation where partnerCode =?",partnerCode);
		
	}
catch (Exception e)
	{
	logger.error("Error executing query "+ e.getStackTrace()[0]);
	}

return null;

}
}
