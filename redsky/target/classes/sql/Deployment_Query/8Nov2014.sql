// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`accountline` 
     ADD COLUMN `estExpVatAmt` DECIMAL(19,2) DEFAULT 0.00 AFTER `dynamicNavXfer`,
     ADD COLUMN `revisionExpVatAmt` DECIMAL(19,2) DEFAULT 0.00 AFTER `estExpVatAmt`,
     ADD COLUMN `qstRecVatAmt` DECIMAL(19,2) DEFAULT 0.00 AFTER `revisionExpVatAmt`,
     ADD COLUMN `qstPayVatAmt` DECIMAL(19,2) DEFAULT 0.00 AFTER `qstRecVatAmt`,
     ADD COLUMN `qstRecVatGl` VARCHAR(20) AFTER `qstPayVatAmt`,
     ADD COLUMN `qstPayVatGl` VARCHAR(20) AFTER `qstRecVatGl`,
     ADD COLUMN `rollUpInInvoice` BIT(1) NULL DEFAULT b'0'  AFTER `qstPayVatGl` ;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `relocationContact` BIT(1) NULL DEFAULT b'0' ;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `NPSscore` DECIMAL(19,2) DEFAULT 0.00 AFTER `excludeFinancialExtract`;

ALTER TABLE `redsky`.`charges` ADD COLUMN `rollUpInInvoice` BIT(1) NULL DEFAULT b'0'  AFTER `earnout` ;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `NPSscore` DECIMAL(19,2) DEFAULT 0.00 AFTER `relocationContact`;

ALTER TABLE `redsky`.`company` ADD COLUMN `subOADASoftValidation` bit(1) DEFAULT b'0' ,
ADD COLUMN `oAdAWeightVolumeMandatoryValidation` bit(1) DEFAULT b'0' ;

ALTER TABLE `redsky`.`customersurveyfeedback` ADD COLUMN `vendorCode` VARCHAR(8) AFTER `serviceForDestEmail`,
ADD COLUMN `vendorContact` VARCHAR(70) AFTER `vendorCode`;

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('HSRG','PAYMENTREFERRAL',50,true,now(),'subrat',now(),'subrat',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('HSRG', 'PartnerDirect', 'Partner Direct', 50, 'PAYMENTREFERRAL','en',now(),'subrat',now(),'subrat'),
('HSRG', 'MaxwellSouthstar', 'Maxwell Southstar', 50, 'PAYMENTREFERRAL','en',now(),'subrat',now(),'subrat')


ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `moveType` VARCHAR(25);

insert  into networkdatafields(modelName, fieldName, transactionType, 
createdBy, createdOn, updatedBy, updatedOn, 
useSysDefault,toModelName,toFieldName,type)
values ('AccountLine','estVatAmt' 
,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
        ('AccountLine','estVatAmt' 
,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
        ('AccountLine','estVatAmt' 
,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estExpVatAmt','CMM'),
        ('AccountLine','estVatAmt' 
,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estExpVatAmt','CMM'),

        ('AccountLine','revisionVatAmt' 
,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
        ('AccountLine','revisionVatAmt' 
,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
        ('AccountLine','revisionVatAmt' 
,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionExpVatAmt','CMM'), 

        ('AccountLine','revisionVatAmt' 
,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionExpVatAmt','CMM'); 


// As per mintu
SELECT s.shipnumber,clearCustomTA,customFlag FROM sodashboard so,trackingstatus s
where s.id=so.serviceorderid
and clearCustomTA is not null and clearCustomTA<>''
and ( customFlag is null or customFlag='');

select concat("update sodashboard set customFlag='",clearCustomTA,"' where id='",so.id,"' and serviceorderid='",s.id,"';")
FROM sodashboard so,trackingstatus s
where s.id=so.serviceorderid
and clearCustomTA is not null and clearCustomTA<>''
and ( customFlag is null or customFlag='');
//
