package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "partnerprivate")
@FilterDefs({
	@FilterDef(name = "partnerPrivateCorpID", parameters = { @ParamDef(name = "privateCorpID", type = "string") })
})

@Filters( {
	@Filter(name = "partnerPrivateCorpID", condition = "corpID in (:privateCorpID) ")
} )
public class PartnerPrivate extends BaseObject { 

	private Long id;

	private String firstName;

	private String lastName;

	private Long partnerPublicId;

	private String corpID;

	private String partnerCode;

	private String abbreviation;

	private String billingInstruction;

	private Boolean qc;

	private String billPayType;

	private String billPayOption;

	private String salesMan;

	private String updatedBy;

	private String validNationalCode;

	private String warehouse;

	private String coordinator;

	private String billingInstructionCode;

	private String billToGroup;

	private String driverAgency;

	private String createdBy;

	private Date createdOn;

	private Date updatedOn;

	private BigDecimal longPercentage;

	private String companyDivision;

	private String needAuth;

	private String storageBillingGroup;

	private String accountHolder;

	private String paymentMethod;

	private String payOption;

	private String multiAuthorization;

	private Boolean payableUploadCheck;

	private Boolean invoiceUploadCheck;

	private String billingUser;

	private String payableUser;

	private String pricingUser;

	private Boolean partnerPortalActive;

	private String partnerPortalId;

	private String associatedAgents;

	private Boolean viewChild;

	private String creditTerms;

	private Boolean accountingDefault;

	private String acctDefaultJobType;
	
	private String status;
	/* Field Added By Kunal Ticket Number: 6006 */
	private String claimsUser;
	/* End Changes Here */
	
	private Boolean stopNotAuthorizedInvoices;
	
	private Boolean doNotCopyAuthorizationSO;
	
	private Boolean emailSentAlert;
	
	private String cardNumber;
	
	private String cardStatus;
	
	private String accountId;
	
	private String customerId;
	
	private String licenseNumber;
	
	private String licenseState;
	
	private Boolean cardSettelment;
	
	private Boolean directDeposit;
	
	private Boolean fuelPurchase;
	
	private Boolean expressCash;
	
	private Boolean yruAccess;
	
	private String extReference;
	
	private Boolean creditCheck;
	
	private BigDecimal creditAmount = new BigDecimal(0);
	
	private String creditCurrency;
	
	private String taxId;
	
	private String taxIdType;
	
	private String accountManager;
	
	private Date creditExpiry;
	
	private Boolean insuranceAuthorized;
	private Boolean progressiveType;
	
	private String auditor;
	private boolean usaFlagRequired;
	
	private Date creditDateCheck;	
	private String emergencyContact;
	private String emergencyEmail;
	private String emergencyPhone;	
	private String commission;
	private String fleet;
	private Date hire;
	private Date termination;
	private Date birth;
	private String truckID;
	private String driverID;
	private String payTo;
	private String trailerID;
	private String corp;
	private BigDecimal cargoInsurance;
	private BigDecimal liabilityInsurance;
	private String mc;
	private Integer dot;
	private String classcode;
	private Boolean accountingHold ;
	private Boolean grpAllowed;
	private BigDecimal companyEscrow;
	private BigDecimal personalEscrow;
	private String driverType;
	private Boolean noDispatch;
	private Boolean soAllDrivers;
	private String UTSCompanyType;
	private BigDecimal availableCredit = new BigDecimal(0);
	private Boolean portalDefaultAccess;
	private Boolean noTransfereeEvaluation;
	private Boolean oneRequestPerCF;
	private String ratingScale;
	private String customerFeedback;
	private String description1;
	private String description2;
	private String description3;
	private String link1;
	private String link2;
	private String link3;
	private Boolean excludeFromParentUpdate;
	private Boolean excludeFromParentPolicyUpdate;
	private Boolean excludeFromParentFaqUpdate;
	private Boolean excludeFromParentQualityUpdate;
	private Boolean excludeFromParentQualityUpdateRelo;
	private Boolean excludePublicFromParentUpdate;
	private Boolean excludePublicReloSvcsParentUpdate;
	private String commissionPlan;
	private Boolean lumpSum;
	private Boolean detailedList;
	private Boolean noInsurance;
	private BigDecimal lumpSumAmount= new BigDecimal(0);
	private String lumpPerUnit;
	private String minReplacementvalue;
	private String storageEmail;
	private Integer claimBy;
	private String collection;
	private Boolean converted;
	private String localName;
	private Boolean customerSurveyEmail;
	private BigDecimal sellRate = new BigDecimal(0);
	private BigDecimal buyRate = new BigDecimal(0);
	
	
	/***
	 * Bug 8927 - To add the 'Default Roles' section for the Account type partners for relocation job type
	 */
	
	private String accountHolderForRelo;
	private String billingUserForRelo;
	private String accountManagerForRelo ;
	private String payableUserForRelo;
	private String pricingUserForRelo;
	private String auditorForRelo;
	private String claimsUserForRelo;
	private String coordinatorForRelo;
	private String emailPrintOption;
	private String contract;
	private String networkPartnerCode;
	private String source;
	private String vendorCode;
	private String insuranceHas;
	private String insuranceOption;
	private String defaultVat;
	private String billToAuthorization;
	private String networkPartnerName;
	private String vendorName;
	private Boolean excludeFinancialExtract;
	private BigDecimal NPSscore= new BigDecimal(0);
	private String vatBillingGroup;
	/*To Add "Informatiom" section for partner,only for agent*/
	private String privateCurrency;
	private String privateBankCode;
	private String privateBankAccountNumber;
	private String privateVatNumber;
	private String agentClassification;
	private BigDecimal minimumMargin= new BigDecimal(0);
	private String rddBased;
	private String rddDays;
	private String internalBillingPerson;
	private Boolean doNotInvoice=false;
	private Integer compensationYear;
	private String rutTaxNumber;
	private Boolean doNotSayYesEmail=false;
	private String  defaultContact;
	private String  contactName;
	private String  phone;
	private String  email;
	private String storageEmailType;
	private String billingMoment;
	private BigDecimal consumablePercentage= new BigDecimal(0.00);
	private String multipleCompanyDivision;
	private Boolean billToExtractAccess=true;
	private Boolean vendorExtractAccess=true ;
	private Boolean reloPartnerPortal;
    private String billingCycle;



	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("firstName", firstName)
				.append("lastName", lastName)
				.append("partnerPublicId", partnerPublicId)
				.append("corpID", corpID)
				.append("partnerCode", partnerCode)
				.append("abbreviation", abbreviation)
				.append("billingInstruction", billingInstruction)
				.append("qc", qc)
				.append("billPayType", billPayType)
				.append("billPayOption", billPayOption)
				.append("salesMan", salesMan)
				.append("updatedBy", updatedBy)
				.append("validNationalCode", validNationalCode)
				.append("warehouse", warehouse)
				.append("coordinator", coordinator)
				.append("billingInstructionCode", billingInstructionCode)
				.append("billToGroup", billToGroup)
				.append("driverAgency", driverAgency)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("longPercentage", longPercentage)
				.append("companyDivision", companyDivision)
				.append("needAuth", needAuth)
				.append("storageBillingGroup", storageBillingGroup)
				.append("accountHolder", accountHolder)
				.append("paymentMethod", paymentMethod)
				.append("payOption", payOption)
				.append("multiAuthorization", multiAuthorization)
				.append("payableUploadCheck", payableUploadCheck)
				.append("invoiceUploadCheck", invoiceUploadCheck)
				.append("billingUser", billingUser)
				.append("payableUser", payableUser)
				.append("pricingUser", pricingUser)
				.append("partnerPortalActive", partnerPortalActive)
				.append("partnerPortalId", partnerPortalId)
				.append("associatedAgents", associatedAgents)
				.append("viewChild", viewChild)
				.append("creditTerms", creditTerms)
				.append("accountingDefault", accountingDefault)
				.append("acctDefaultJobType", acctDefaultJobType)
				.append("status", status)
				.append("claimsUser", claimsUser)
				.append("stopNotAuthorizedInvoices", stopNotAuthorizedInvoices)
				.append("doNotCopyAuthorizationSO", doNotCopyAuthorizationSO)
				.append("emailSentAlert", emailSentAlert)
				.append("cardNumber", cardNumber)
				.append("cardStatus", cardStatus)
				.append("accountId", accountId)
				.append("customerId", customerId)
				.append("licenseNumber", licenseNumber)
				.append("licenseState", licenseState)
				.append("cardSettelment", cardSettelment)
				.append("directDeposit", directDeposit)
				.append("fuelPurchase", fuelPurchase)
				.append("expressCash", expressCash)
				.append("yruAccess", yruAccess)
				.append("extReference", extReference)
				.append("creditCheck", creditCheck)
				.append("creditAmount", creditAmount)
				.append("creditCurrency", creditCurrency)
				.append("taxId", taxId)
				.append("taxIdType", taxIdType)
				.append("accountManager", accountManager)
				.append("creditExpiry", creditExpiry)
				.append("insuranceAuthorized", insuranceAuthorized)
				.append("progressiveType", progressiveType)
				.append("auditor", auditor)
				.append("usaFlagRequired", usaFlagRequired)
				.append("creditDateCheck", creditDateCheck)
				.append("emergencyContact", emergencyContact)
				.append("emergencyEmail", emergencyEmail)
				.append("emergencyPhone", emergencyPhone)
				.append("commission", commission)
				.append("fleet", fleet)
				.append("hire", hire)
				.append("termination", termination)
				.append("birth", birth)
				.append("truckID", truckID)
				.append("driverID", driverID)
				.append("payTo", payTo)
				.append("trailerID", trailerID)
				.append("corp", corp)
				.append("cargoInsurance", cargoInsurance)
				.append("liabilityInsurance", liabilityInsurance)
				.append("mc", mc)
				.append("dot", dot)
				.append("classcode", classcode)
				.append("accountingHold", accountingHold)
				.append("grpAllowed", grpAllowed)
				.append("companyEscrow", companyEscrow)
				.append("personalEscrow", personalEscrow)
				.append("driverType", driverType)
				.append("noDispatch", noDispatch)
				.append("soAllDrivers", soAllDrivers)
				.append("UTSCompanyType", UTSCompanyType)
				.append("availableCredit", availableCredit)
				.append("portalDefaultAccess", portalDefaultAccess)
				.append("noTransfereeEvaluation", noTransfereeEvaluation)
				.append("oneRequestPerCF", oneRequestPerCF)
				.append("ratingScale", ratingScale)
				.append("customerFeedback", customerFeedback)
				.append("description1", description1)
				.append("description2", description2)
				.append("description3", description3)
				.append("link1", link1)
				.append("link2", link2)
				.append("link3", link3)
				.append("excludeFromParentUpdate", excludeFromParentUpdate)
				.append("excludeFromParentPolicyUpdate",
						excludeFromParentPolicyUpdate)
				.append("excludeFromParentFaqUpdate",
						excludeFromParentFaqUpdate)
				.append("excludeFromParentQualityUpdate",
						excludeFromParentQualityUpdate)
				.append("excludeFromParentQualityUpdateRelo",
						excludeFromParentQualityUpdateRelo)
				.append("excludePublicFromParentUpdate",
						excludePublicFromParentUpdate)
				.append("excludePublicReloSvcsParentUpdate",
						excludePublicReloSvcsParentUpdate)
				.append("commissionPlan", commissionPlan)
				.append("lumpSum", lumpSum)
				.append("detailedList", detailedList)
				.append("noInsurance", noInsurance)
				.append("lumpSumAmount", lumpSumAmount)
				.append("lumpPerUnit", lumpPerUnit)
				.append("minReplacementvalue", minReplacementvalue)
				.append("storageEmail", storageEmail)
				.append("claimBy", claimBy).append("collection", collection)
				.append("converted", converted).append("localName", localName)
				.append("customerSurveyEmail", customerSurveyEmail)
				.append("accountHolderForRelo", accountHolderForRelo)
				.append("billingUserForRelo", billingUserForRelo)
				.append("accountManagerForRelo", accountManagerForRelo)
				.append("payableUserForRelo", payableUserForRelo)
				.append("pricingUserForRelo", pricingUserForRelo)
				.append("auditorForRelo", auditorForRelo)
				.append("claimsUserForRelo", claimsUserForRelo)
				.append("coordinatorForRelo", coordinatorForRelo)
				.append("emailPrintOption", emailPrintOption)
				.append("contract", contract)
				.append("networkPartnerCode", networkPartnerCode)
				.append("source", source).append("vendorCode", vendorCode)
				.append("insuranceHas", insuranceHas)
				.append("insuranceOption", insuranceOption)
				.append("defaultVat", defaultVat)
				.append("billToAuthorization", billToAuthorization)
				.append("networkPartnerName", networkPartnerName)
				.append("vendorName", vendorName)
				.append("excludeFinancialExtract",excludeFinancialExtract)
				.append("NPSscore",NPSscore)
				.append("vatBillingGroup",vatBillingGroup) 
				.append("privateCurrency",privateCurrency) 
				.append("privateBankCode",privateBankCode) 
				.append("privateBankAccountNumber",privateBankAccountNumber) 
				.append("privateVatNumber",privateVatNumber)
				.append("agentClassification",agentClassification)
				.append("minimumMargin",minimumMargin)
				.append("rddBased",rddBased)
				.append("rddDays",rddDays)
				.append("internalBillingPerson",internalBillingPerson)
				.append("buyRate", buyRate)
				.append("sellRate", sellRate)
				.append("doNotInvoice", doNotInvoice)
				.append("compensationYear", compensationYear)
				.append("rutTaxNumber", rutTaxNumber)
				.append("doNotSayYesEmail", doNotSayYesEmail)
				.append("defaultContact", defaultContact)
				.append("contactName", contactName)
			    .append("phone", phone)
				.append("email", email)
				.append("storageEmailType", storageEmailType)
				.append("billingMoment", billingMoment)
				.append("consumablePercentage", consumablePercentage)
				.append("multipleCompanyDivision", multipleCompanyDivision)
				.append("billToExtractAccess", billToExtractAccess)
				.append("vendorExtractAccess", vendorExtractAccess)
				.append("reloPartnerPortal", reloPartnerPortal)
				.append("billingCycle", billingCycle)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerPrivate))
			return false;
		PartnerPrivate castOther = (PartnerPrivate) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(firstName, castOther.firstName)
				.append(lastName, castOther.lastName)
				.append(partnerPublicId, castOther.partnerPublicId)
				.append(corpID, castOther.corpID)
				.append(partnerCode, castOther.partnerCode)
				.append(abbreviation, castOther.abbreviation)
				.append(billingInstruction, castOther.billingInstruction)
				.append(qc, castOther.qc)
				.append(billPayType, castOther.billPayType)
				.append(billPayOption, castOther.billPayOption)
				.append(salesMan, castOther.salesMan)
				.append(updatedBy, castOther.updatedBy)
				.append(validNationalCode, castOther.validNationalCode)
				.append(warehouse, castOther.warehouse)
				.append(coordinator, castOther.coordinator)
				.append(billingInstructionCode,
						castOther.billingInstructionCode)
				.append(billToGroup, castOther.billToGroup)
				.append(driverAgency, castOther.driverAgency)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(longPercentage, castOther.longPercentage)
				.append(companyDivision, castOther.companyDivision)
				.append(needAuth, castOther.needAuth)
				.append(storageBillingGroup, castOther.storageBillingGroup)
				.append(accountHolder, castOther.accountHolder)
				.append(paymentMethod, castOther.paymentMethod)
				.append(payOption, castOther.payOption)
				.append(multiAuthorization, castOther.multiAuthorization)
				.append(payableUploadCheck, castOther.payableUploadCheck)
				.append(invoiceUploadCheck, castOther.invoiceUploadCheck)
				.append(billingUser, castOther.billingUser)
				.append(payableUser, castOther.payableUser)
				.append(pricingUser, castOther.pricingUser)
				.append(partnerPortalActive, castOther.partnerPortalActive)
				.append(partnerPortalId, castOther.partnerPortalId)
				.append(associatedAgents, castOther.associatedAgents)
				.append(viewChild, castOther.viewChild)
				.append(creditTerms, castOther.creditTerms)
				.append(accountingDefault, castOther.accountingDefault)
				.append(acctDefaultJobType, castOther.acctDefaultJobType)
				.append(status, castOther.status)
				.append(claimsUser, castOther.claimsUser)
				.append(stopNotAuthorizedInvoices,
						castOther.stopNotAuthorizedInvoices)
				.append(doNotCopyAuthorizationSO,
						castOther.doNotCopyAuthorizationSO)
				.append(emailSentAlert, castOther.emailSentAlert)
				.append(cardNumber, castOther.cardNumber)
				.append(cardStatus, castOther.cardStatus)
				.append(accountId, castOther.accountId)
				.append(customerId, castOther.customerId)
				.append(licenseNumber, castOther.licenseNumber)
				.append(licenseState, castOther.licenseState)
				.append(cardSettelment, castOther.cardSettelment)
				.append(directDeposit, castOther.directDeposit)
				.append(fuelPurchase, castOther.fuelPurchase)
				.append(expressCash, castOther.expressCash)
				.append(yruAccess, castOther.yruAccess)
				.append(extReference, castOther.extReference)
				.append(creditCheck, castOther.creditCheck)
				.append(creditAmount, castOther.creditAmount)
				.append(creditCurrency, castOther.creditCurrency)
				.append(taxId, castOther.taxId)
				.append(taxIdType, castOther.taxIdType)
				.append(accountManager, castOther.accountManager)
				.append(creditExpiry, castOther.creditExpiry)
				.append(insuranceAuthorized, castOther.insuranceAuthorized)
				.append(progressiveType, castOther.progressiveType)
				.append(auditor, castOther.auditor)
				.append(usaFlagRequired, castOther.usaFlagRequired)
				.append(creditDateCheck, castOther.creditDateCheck)
				.append(emergencyContact, castOther.emergencyContact)
				.append(emergencyEmail, castOther.emergencyEmail)
				.append(emergencyPhone, castOther.emergencyPhone)
				.append(commission, castOther.commission)
				.append(fleet, castOther.fleet)
				.append(hire, castOther.hire)
				.append(termination, castOther.termination)
				.append(birth, castOther.birth)
				.append(truckID, castOther.truckID)
				.append(driverID, castOther.driverID)
				.append(payTo, castOther.payTo)
				.append(trailerID, castOther.trailerID)
				.append(corp, castOther.corp)
				.append(cargoInsurance, castOther.cargoInsurance)
				.append(liabilityInsurance, castOther.liabilityInsurance)
				.append(mc, castOther.mc)
				.append(dot, castOther.dot)
				.append(classcode, castOther.classcode)
				.append(accountingHold, castOther.accountingHold)
				.append(grpAllowed, castOther.grpAllowed)
				.append(companyEscrow, castOther.companyEscrow)
				.append(personalEscrow, castOther.personalEscrow)
				.append(driverType, castOther.driverType)
				.append(noDispatch, castOther.noDispatch)
				.append(soAllDrivers, castOther.soAllDrivers)
				.append(UTSCompanyType, castOther.UTSCompanyType)
				.append(availableCredit, castOther.availableCredit)
				.append(portalDefaultAccess, castOther.portalDefaultAccess)
				.append(noTransfereeEvaluation,
						castOther.noTransfereeEvaluation)
				.append(oneRequestPerCF, castOther.oneRequestPerCF)
				.append(ratingScale, castOther.ratingScale)
				.append(customerFeedback, castOther.customerFeedback)
				.append(description1, castOther.description1)
				.append(description2, castOther.description2)
				.append(description3, castOther.description3)
				.append(link1, castOther.link1)
				.append(link2, castOther.link2)
				.append(link3, castOther.link3)
				.append(excludeFromParentUpdate,
						castOther.excludeFromParentUpdate)
				.append(excludeFromParentPolicyUpdate,
						castOther.excludeFromParentPolicyUpdate)
				.append(excludeFromParentFaqUpdate,
						castOther.excludeFromParentFaqUpdate)
				.append(excludeFromParentQualityUpdate,
						castOther.excludeFromParentQualityUpdate)
				.append(excludeFromParentQualityUpdateRelo,
						castOther.excludeFromParentQualityUpdateRelo)
				.append(excludePublicFromParentUpdate,
						castOther.excludePublicFromParentUpdate)
				.append(excludePublicReloSvcsParentUpdate,
						castOther.excludePublicReloSvcsParentUpdate)
				.append(commissionPlan, castOther.commissionPlan)
				.append(lumpSum, castOther.lumpSum)
				.append(detailedList, castOther.detailedList)
				.append(noInsurance, castOther.noInsurance)
				.append(lumpSumAmount, castOther.lumpSumAmount)
				.append(lumpPerUnit, castOther.lumpPerUnit)
				.append(minReplacementvalue, castOther.minReplacementvalue)
				.append(storageEmail, castOther.storageEmail)
				.append(claimBy, castOther.claimBy)
				.append(collection, castOther.collection)
				.append(converted, castOther.converted)
				.append(localName, castOther.localName)
				.append(customerSurveyEmail, castOther.customerSurveyEmail)
				.append(accountHolderForRelo, castOther.accountHolderForRelo)
				.append(billingUserForRelo, castOther.billingUserForRelo)
				.append(accountManagerForRelo, castOther.accountManagerForRelo)
				.append(payableUserForRelo, castOther.payableUserForRelo)
				.append(pricingUserForRelo, castOther.pricingUserForRelo)
				.append(auditorForRelo, castOther.auditorForRelo)
				.append(claimsUserForRelo, castOther.claimsUserForRelo)
				.append(coordinatorForRelo, castOther.coordinatorForRelo)
				.append(emailPrintOption, castOther.emailPrintOption)
				.append(contract, castOther.contract)
				.append(networkPartnerCode, castOther.networkPartnerCode)
				.append(source, castOther.source)
				.append(vendorCode, castOther.vendorCode)
				.append(insuranceHas, castOther.insuranceHas)
				.append(insuranceOption, castOther.insuranceOption)
				.append(defaultVat, castOther.defaultVat)
				.append(billToAuthorization, castOther.billToAuthorization)
				.append(networkPartnerName, castOther.networkPartnerName)
				.append(vendorName, castOther.vendorName)
				.append(excludeFinancialExtract, castOther.excludeFinancialExtract)
				.append(NPSscore, castOther.NPSscore)
				.append(vatBillingGroup, castOther.vatBillingGroup) 
				.append(privateCurrency, castOther.privateCurrency) 
				.append(privateBankCode, castOther.privateBankCode) 
				.append(privateBankAccountNumber, castOther.privateBankAccountNumber) 
				.append(privateVatNumber, castOther.privateVatNumber)
				.append(agentClassification, castOther.agentClassification)
				.append(minimumMargin,castOther.minimumMargin)
				.append(rddBased,castOther.rddBased)
				.append(rddDays,castOther.rddDays)
				.append(internalBillingPerson,castOther.internalBillingPerson)
				.append(buyRate, castOther.buyRate)
				.append(sellRate, castOther.sellRate)
				.append(doNotInvoice, castOther.doNotInvoice)
				.append(compensationYear, castOther.compensationYear)
				.append(rutTaxNumber, castOther.rutTaxNumber)
				.append(doNotSayYesEmail, castOther.doNotSayYesEmail)
				.append(defaultContact, castOther.defaultContact)
				.append(contactName, castOther.contactName)
				.append(phone, castOther.phone)
				.append(email, castOther.email)
				.append(storageEmailType, castOther.storageEmailType)
				.append(billingMoment, castOther.billingMoment)
				.append(consumablePercentage, castOther.consumablePercentage)
				.append(multipleCompanyDivision, castOther.multipleCompanyDivision)
				.append(billToExtractAccess, castOther.billToExtractAccess)
				.append(vendorExtractAccess, castOther.vendorExtractAccess)
				.append(reloPartnerPortal, castOther.reloPartnerPortal)
				.append(billingCycle, castOther.billingCycle)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(firstName)
				.append(lastName).append(partnerPublicId).append(corpID)
				.append(partnerCode).append(abbreviation)
				.append(billingInstruction).append(qc).append(billPayType)
				.append(billPayOption).append(salesMan).append(updatedBy)
				.append(validNationalCode).append(warehouse)
				.append(coordinator).append(billingInstructionCode)
				.append(billToGroup).append(driverAgency).append(createdBy)
				.append(createdOn).append(updatedOn).append(longPercentage)
				.append(companyDivision).append(needAuth)
				.append(storageBillingGroup).append(accountHolder)
				.append(paymentMethod).append(payOption)
				.append(multiAuthorization).append(payableUploadCheck)
				.append(invoiceUploadCheck).append(billingUser)
				.append(payableUser).append(pricingUser)
				.append(partnerPortalActive).append(partnerPortalId)
				.append(associatedAgents).append(viewChild).append(creditTerms)
				.append(accountingDefault).append(acctDefaultJobType)
				.append(status).append(claimsUser)
				.append(stopNotAuthorizedInvoices)
				.append(doNotCopyAuthorizationSO).append(emailSentAlert)
				.append(cardNumber).append(cardStatus).append(accountId)
				.append(customerId).append(licenseNumber).append(licenseState)
				.append(cardSettelment).append(directDeposit)
				.append(fuelPurchase).append(expressCash).append(yruAccess)
				.append(extReference).append(creditCheck).append(creditAmount)
				.append(creditCurrency).append(taxId).append(taxIdType)
				.append(accountManager).append(creditExpiry)
				.append(insuranceAuthorized).append(progressiveType)
				.append(auditor).append(usaFlagRequired)
				.append(creditDateCheck).append(emergencyContact)
				.append(emergencyEmail).append(emergencyPhone)
				.append(commission).append(fleet).append(hire)
				.append(termination).append(birth).append(truckID)
				.append(driverID).append(payTo).append(trailerID).append(corp)
				.append(cargoInsurance).append(liabilityInsurance).append(mc)
				.append(dot).append(classcode).append(accountingHold)
				.append(grpAllowed).append(companyEscrow)
				.append(personalEscrow).append(driverType).append(noDispatch)
				.append(soAllDrivers).append(UTSCompanyType)
				.append(availableCredit).append(portalDefaultAccess)
				.append(noTransfereeEvaluation).append(oneRequestPerCF)
				.append(ratingScale).append(customerFeedback)
				.append(description1).append(description2).append(description3)
				.append(link1).append(link2).append(link3)
				.append(excludeFromParentUpdate)
				.append(excludeFromParentPolicyUpdate)
				.append(excludeFromParentFaqUpdate)
				.append(excludeFromParentQualityUpdate)
				.append(excludeFromParentQualityUpdateRelo)
				.append(excludePublicFromParentUpdate)
				.append(excludePublicReloSvcsParentUpdate)
				.append(commissionPlan).append(lumpSum).append(detailedList)
				.append(noInsurance).append(lumpSumAmount).append(lumpPerUnit)
				.append(minReplacementvalue).append(storageEmail)
				.append(claimBy).append(collection).append(converted)
				.append(localName).append(customerSurveyEmail)
				.append(accountHolderForRelo).append(billingUserForRelo)
				.append(accountManagerForRelo).append(payableUserForRelo)
				.append(pricingUserForRelo).append(auditorForRelo)
				.append(claimsUserForRelo).append(coordinatorForRelo)
				.append(emailPrintOption).append(contract)
				.append(networkPartnerCode).append(source).append(vendorCode)
				.append(insuranceHas).append(insuranceOption)
				.append(defaultVat).append(billToAuthorization).append(networkPartnerName)
				.append(vendorName)
				.append(excludeFinancialExtract)
				.append(NPSscore)
				.append(vatBillingGroup)
				.append(privateCurrency) 
				.append(privateBankCode) 
				.append(privateBankAccountNumber) 
				.append(privateVatNumber)
				.append(agentClassification)
				.append(minimumMargin).append(rddBased).append(rddDays)
				.append(internalBillingPerson)
				.append(buyRate)
				.append(sellRate)
				.append(doNotInvoice)
				.append(compensationYear).append(rutTaxNumber)
				.append(doNotSayYesEmail)
				.append(defaultContact)
				.append(contactName)
				.append(phone)
				.append(email).append(storageEmailType).append(billingMoment)
				.append(consumablePercentage).append(multipleCompanyDivision)
				.append(billToExtractAccess).append(vendorExtractAccess).append(reloPartnerPortal)
				.append(billingCycle)
				.toHashCode();
	}

	@Column(length=45)
	public String getRatingScale() {
		return ratingScale;
	}

	public void setRatingScale(String ratingScale) {
		this.ratingScale = ratingScale;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public Long getPartnerPublicId() {
		return partnerPublicId;
	}

	public void setPartnerPublicId(Long partnerPublicId) {
		this.partnerPublicId = partnerPublicId;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 15)
	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	@Column(length = 150)
	public String getBillingInstruction() {
		return billingInstruction;
	}

	public void setBillingInstruction(String billingInstruction) {
		this.billingInstruction = billingInstruction;
	}

	@Column(length = 7)
	public String getBillingInstructionCode() {
		return billingInstructionCode;
	}

	public void setBillingInstructionCode(String billingInstructionCode) {
		this.billingInstructionCode = billingInstructionCode;
	}

	@Column(length = 3)
	public String getBillPayOption() {
		return billPayOption;
	}

	public void setBillPayOption(String billPayOption) {
		this.billPayOption = billPayOption;
	}

	@Column(length = 3)
	public String getBillPayType() {
		return billPayType;
	}

	public void setBillPayType(String billPayType) {
		this.billPayType = billPayType;
	}

	@Column(length = 82)
	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	@Column(length = 8)
	public String getDriverAgency() {
		return driverAgency;
	}

	public void setDriverAgency(String driverAgency) {
		this.driverAgency = driverAgency;
	}

	@Column(length = 15)
	public String getBillToGroup() {
		return billToGroup;
	}

	public void setBillToGroup(String billToGroup) {
		this.billToGroup = billToGroup;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(nullable = true, precision = 15, scale = 5)
	public BigDecimal getLongPercentage() {
		return longPercentage;
	}

	public void setLongPercentage(BigDecimal longPercentage) {
		this.longPercentage = longPercentage;
	}

	@Column(length = 1)
	public String getNeedAuth() {
		return needAuth;
	}

	public void setNeedAuth(String needAuth) {
		this.needAuth = needAuth;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column
	public Boolean getQc() {
		return qc;
	}

	public void setQc(Boolean qc) {
		this.qc = qc;
	}

	@Column(length = 15)
	public String getSalesMan() {
		return salesMan;
	}

	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(length = 15)
	public String getValidNationalCode() {
		return validNationalCode;
	}

	public void setValidNationalCode(String validNationalCode) {
		this.validNationalCode = validNationalCode;
	}

	@Column(length = 2)
	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(length = 50)
	public String getStorageBillingGroup() {
		return storageBillingGroup;
	}

	public void setStorageBillingGroup(String storageBillingGroup) {
		this.storageBillingGroup = storageBillingGroup;
	}

	@Column(length = 82)
	public String getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}

	@Column(length = 50)
	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	@Column(length = 50)
	public String getPayOption() {
		return payOption;
	}

	public void setPayOption(String payOption) {
		this.payOption = payOption;
	}

	@Column(length = 5)
	public String getMultiAuthorization() {
		return multiAuthorization;
	}

	public void setMultiAuthorization(String multiAuthorization) {
		this.multiAuthorization = multiAuthorization;
	}

	@Column
	public Boolean getPayableUploadCheck() {
		return payableUploadCheck;
	}

	public void setPayableUploadCheck(Boolean payableUploadCheck) {
		this.payableUploadCheck = payableUploadCheck;
	}

	@Column(length = 10)
	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	@Column
	public Boolean getInvoiceUploadCheck() {
		return invoiceUploadCheck;
	}

	public void setInvoiceUploadCheck(Boolean invoiceUploadCheck) {
		this.invoiceUploadCheck = invoiceUploadCheck;
	}

	@Column(length = 82)
	public String getBillingUser() {
		return billingUser;
	}

	public void setBillingUser(String billingUser) {
		this.billingUser = billingUser;
	}

	@Column(length = 82)
	public String getPayableUser() {
		return payableUser;
	}

	public void setPayableUser(String payableUser) {
		this.payableUser = payableUser;
	}

	@Column(length = 82)
	public String getPricingUser() {
		return pricingUser;
	}

	public void setPricingUser(String pricingUser) {
		this.pricingUser = pricingUser;
	}

	@Column
	public Boolean getPartnerPortalActive() {
		return partnerPortalActive;
	}

	public void setPartnerPortalActive(Boolean partnerPortalActive) {
		this.partnerPortalActive = partnerPortalActive;
	}

	@Column
	public String getPartnerPortalId() {
		return partnerPortalId;
	}

	public void setPartnerPortalId(String partnerPortalId) {
		this.partnerPortalId = partnerPortalId;
	}

	@Column
	public String getAssociatedAgents() {
		return associatedAgents;
	}

	public void setAssociatedAgents(String associatedAgents) {
		this.associatedAgents = associatedAgents;
	}

	@Column
	public Boolean getViewChild() {
		return viewChild;
	}

	public void setViewChild(Boolean viewChild) {
		this.viewChild = viewChild;
	}

	@Column(length = 10)
	public String getCreditTerms() {
		return creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	@Column
	public Boolean getAccountingDefault() {
		return accountingDefault;
	}

	public void setAccountingDefault(Boolean accountingDefault) {
		this.accountingDefault = accountingDefault;
	}

	@Column(length = 50)
	public String getAcctDefaultJobType() {
		return acctDefaultJobType;
	}

	public void setAcctDefaultJobType(String acctDefaultJobType) {
		this.acctDefaultJobType = acctDefaultJobType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getClaimsUser() {
		return claimsUser;
	}

	public void setClaimsUser(String claimsUser) {
		this.claimsUser = claimsUser;
	}

	public Boolean getDoNotCopyAuthorizationSO() {
		return doNotCopyAuthorizationSO;
	}

	public void setDoNotCopyAuthorizationSO(Boolean doNotCopyAuthorizationSO) {
		this.doNotCopyAuthorizationSO = doNotCopyAuthorizationSO;
	}

	public Boolean getStopNotAuthorizedInvoices() {
		return stopNotAuthorizedInvoices;
	}

	public void setStopNotAuthorizedInvoices(Boolean stopNotAuthorizedInvoices) {
		this.stopNotAuthorizedInvoices = stopNotAuthorizedInvoices;
	}

	public Boolean getEmailSentAlert() {
		return emailSentAlert;
	}

	public void setEmailSentAlert(Boolean emailSentAlert) {
		this.emailSentAlert = emailSentAlert;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Boolean getCardSettelment() {
		return cardSettelment;
	}

	public void setCardSettelment(Boolean cardSettelment) {
		this.cardSettelment = cardSettelment;
	}

	public String getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Boolean getDirectDeposit() {
		return directDeposit;
	}

	public void setDirectDeposit(Boolean directDeposit) {
		this.directDeposit = directDeposit;
	}

	public Boolean getExpressCash() {
		return expressCash;
	}

	public void setExpressCash(Boolean expressCash) {
		this.expressCash = expressCash;
	}

	public Boolean getFuelPurchase() {
		return fuelPurchase;
	}

	public void setFuelPurchase(Boolean fuelPurchase) {
		this.fuelPurchase = fuelPurchase;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getLicenseState() {
		return licenseState;
	}

	public void setLicenseState(String licenseState) {
		this.licenseState = licenseState;
	}

	public Boolean getYruAccess() {
		return yruAccess;
	}

	public void setYruAccess(Boolean yruAccess) {
		this.yruAccess = yruAccess;
	}

	public String getExtReference() {
		return extReference;
	}

	public void setExtReference(String extReference) {
		this.extReference = extReference;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Boolean getCreditCheck() {
		return creditCheck;
	}

	public void setCreditCheck(Boolean creditCheck) {
		this.creditCheck = creditCheck;
	}

	public String getCreditCurrency() {
		return creditCurrency;
	}

	public void setCreditCurrency(String creditCurrency) {
		this.creditCurrency = creditCurrency;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getTaxIdType() {
		return taxIdType;
	}

	public void setTaxIdType(String taxIdType) {
		this.taxIdType = taxIdType;
	}

	public String getAccountManager() {
		return accountManager;
	}

	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}

	public Date getCreditExpiry() {
		return creditExpiry;
	}

	public void setCreditExpiry(Date creditExpiry) {
		this.creditExpiry = creditExpiry;
	}

	public Boolean getInsuranceAuthorized() {
		return insuranceAuthorized;
	}

	public void setInsuranceAuthorized(Boolean insuranceAuthorized) {
		this.insuranceAuthorized = insuranceAuthorized;
	}

	public String getAuditor() {
		return auditor;
	}

	public void setAuditor(String auditor) {
		this.auditor = auditor;
	}
    @Column
	public Date getCreditDateCheck() {
		return creditDateCheck;
	}

	public void setCreditDateCheck(Date creditDateCheck) {
		this.creditDateCheck = creditDateCheck;
	}

	@Column(length=20)
	public String getEmergencyContact() {
		return emergencyContact;
	}

	@Column(length = 10)
	public String getCommission() {
		return commission;
	}

	public void setEmergencyContact(String emergencyContact) {
		this.emergencyContact = emergencyContact;
	}
	@Column(length=30)
	public String getEmergencyEmail() {
		return emergencyEmail;
	}

	public void setEmergencyEmail(String emergencyEmail) {
		this.emergencyEmail = emergencyEmail;
	}
	
	@Column(length=30)
	public String getEmergencyPhone() {
		return emergencyPhone;
	}

	public void setEmergencyPhone(String emergencyPhone) {
		this.emergencyPhone = emergencyPhone;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}
	@Column
	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}
	@Column(length=10)
	public String getFleet() {
		return fleet;
	}

	public void setFleet(String fleet) {
		this.fleet = fleet;
	}
	@Column
	public Date getHire() {
		return hire;
	}

	public void setHire(Date hire) {
		this.hire = hire;
	}
	@Column(length=20)
	public String getDriverID() {
		return driverID;
	}

	public void setDriverID(String driverID) {
		this.driverID = driverID;
	}
	@Column(length=20)
	public String getTruckID() {
		return truckID;
	}

	public void setTruckID(String truckID) {
		this.truckID = truckID;
	}
	@Column(length=20)
	public String getPayTo() {
		return payTo;
	}

	public void setPayTo(String payTo) {
		this.payTo = payTo;
	}
	@Column(length=20)
	public String getCorp() {
		return corp;
	}

	public void setCorp(String corp) {
		this.corp = corp;
	}
	@Column(nullable = true, precision = 15, scale = 5)
	public BigDecimal getCargoInsurance() {
		return cargoInsurance;
	}

	public void setCargoInsurance(BigDecimal cargoInsurance) {
		this.cargoInsurance = cargoInsurance;
	}
	@Column(length=15)
	public Integer getDot() {
		return dot;
	}

	public void setDot(Integer dot) {
		this.dot = dot;
	}
	@Column(nullable = true, precision = 15, scale = 5)
	public BigDecimal getLiabilityInsurance() {
		return liabilityInsurance;
	}

	public void setLiabilityInsurance(BigDecimal liabilityInsurance) {
		this.liabilityInsurance = liabilityInsurance;
	}
	@Column(length=25)
	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}
	@Column(length=20)
	public String getTrailerID() {
		return trailerID;
	}

	public void setTrailerID(String trailerID) {
		this.trailerID = trailerID;
	}
	@Column(length=25)
	public String getClasscode() {
		return classcode;
	}

	public void setClasscode(String classcode) {
		this.classcode = classcode;
	}
	
	@Column
	public Boolean getProgressiveType() {
		return progressiveType;
	}

	public void setProgressiveType(Boolean progressiveType) {
		this.progressiveType = progressiveType;
	}

	public Boolean getAccountingHold() {
		return accountingHold;
	}

	public void setAccountingHold(Boolean accountingHold) {
		this.accountingHold = accountingHold;
	}
	@Column
	public BigDecimal getCompanyEscrow() {
		return companyEscrow;
	}

	public void setCompanyEscrow(BigDecimal companyEscrow) {
		this.companyEscrow = companyEscrow;
	}
	@Column
	public BigDecimal getPersonalEscrow() {
		return personalEscrow;
	}

	public void setPersonalEscrow(BigDecimal personalEscrow) {
		this.personalEscrow = personalEscrow;
	}
	@Column
	public Boolean getGrpAllowed() {
		return grpAllowed;
	}

	public void setGrpAllowed(Boolean grpAllowed) {
		this.grpAllowed = grpAllowed;
	}
	@Column(length=15)
	public String getDriverType() {
		return driverType;
	}

	public void setDriverType(String driverType) {
		this.driverType = driverType;
	}
	@Column
	public Boolean getNoDispatch() {
		return noDispatch;
	}

	public void setNoDispatch(Boolean noDispatch) {
		this.noDispatch = noDispatch;
	}
	@Column
	public String getUTSCompanyType() {
		return UTSCompanyType;
	}

	public void setUTSCompanyType(String uTSCompanyType) {
		UTSCompanyType = uTSCompanyType;
	}
	
	@Column
	public BigDecimal getAvailableCredit() {
		return availableCredit;
	}
	public void setAvailableCredit(BigDecimal availableCredit) {
		this.availableCredit = availableCredit;
	}
	@Column
	public Date getTermination() {
		return termination;
	}

	public void setTermination(Date termination) {
		this.termination = termination;
	}
	@Column
	public Boolean getPortalDefaultAccess() {
		return portalDefaultAccess;
	}

	public void setPortalDefaultAccess(Boolean portalDefaultAccess) {
		this.portalDefaultAccess = portalDefaultAccess;
	}
	
	@Column
	public Boolean getNoTransfereeEvaluation() {
		return noTransfereeEvaluation;
	}

	public void setNoTransfereeEvaluation(Boolean noTransfereeEvaluation) {
		this.noTransfereeEvaluation = noTransfereeEvaluation;
	}
	@Column
	public Boolean getOneRequestPerCF() {
		return oneRequestPerCF;
	}

	public void setOneRequestPerCF(Boolean oneRequestPerCF) {
		this.oneRequestPerCF = oneRequestPerCF;
	}	
	@Column(length=45)
	public String getCustomerFeedback() {
		return customerFeedback;
	}

	public void setCustomerFeedback(String customerFeedback) {
		this.customerFeedback = customerFeedback;
	}

	public String getDescription1() {
		return description1;
	}

	public void setDescription1(String description1) {
		this.description1 = description1;
	}

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public String getDescription3() {
		return description3;
	}

	public void setDescription3(String description3) {
		this.description3 = description3;
	}

	public String getLink1() {
		return link1;
	}

	public void setLink1(String link1) {
		this.link1 = link1;
	}

	public String getLink2() {
		return link2;
	}

	public void setLink2(String link2) {
		this.link2 = link2;
	}

	public String getLink3() {
		return link3;
	}

	public void setLink3(String link3) {
		this.link3 = link3;
	}
	@Column
	public Boolean getExcludeFromParentUpdate() {
		return excludeFromParentUpdate;
	}

	public void setExcludeFromParentUpdate(Boolean excludeFromParentUpdate) {
		this.excludeFromParentUpdate = excludeFromParentUpdate;
	}
	@Column
	public Boolean getExcludeFromParentPolicyUpdate() {
		return excludeFromParentPolicyUpdate;
	}

	public void setExcludeFromParentPolicyUpdate(
			Boolean excludeFromParentPolicyUpdate) {
		this.excludeFromParentPolicyUpdate = excludeFromParentPolicyUpdate;
	}
	@Column
	public Boolean getExcludeFromParentFaqUpdate() {
		return excludeFromParentFaqUpdate;
	}
	
	public void setExcludeFromParentFaqUpdate(Boolean excludeFromParentFaqUpdate) {
		this.excludeFromParentFaqUpdate = excludeFromParentFaqUpdate;
	}
	@Column
	public Boolean getSoAllDrivers() {
		return soAllDrivers;
	}

	public void setSoAllDrivers(Boolean soAllDrivers) {
		this.soAllDrivers = soAllDrivers;
	}
	@Column
	public Boolean getExcludeFromParentQualityUpdate() {
		return excludeFromParentQualityUpdate;
	}

	public void setExcludeFromParentQualityUpdate(
			Boolean excludeFromParentQualityUpdate) {
		this.excludeFromParentQualityUpdate = excludeFromParentQualityUpdate;
	}
	@Column
	public Boolean getExcludeFromParentQualityUpdateRelo() {
		return excludeFromParentQualityUpdateRelo;
	}

	public void setExcludeFromParentQualityUpdateRelo(
			Boolean excludeFromParentQualityUpdateRelo) {
		this.excludeFromParentQualityUpdateRelo = excludeFromParentQualityUpdateRelo;
	}
	@Column
	public Boolean getExcludePublicFromParentUpdate() {
		return excludePublicFromParentUpdate;
	}

	public void setExcludePublicFromParentUpdate(
			Boolean excludePublicFromParentUpdate) {
		this.excludePublicFromParentUpdate = excludePublicFromParentUpdate;
	}
	@Column
	public Boolean getExcludePublicReloSvcsParentUpdate() {
		return excludePublicReloSvcsParentUpdate;
	}

	public void setExcludePublicReloSvcsParentUpdate(
			Boolean excludePublicReloSvcsParentUpdate) {
		this.excludePublicReloSvcsParentUpdate = excludePublicReloSvcsParentUpdate;
	}
	@Column
	public String getCommissionPlan() {
		return commissionPlan;
	}

	public void setCommissionPlan(String commissionPlan) {
		this.commissionPlan = commissionPlan;
	}
	
	@Column
	public Boolean getDetailedList() {
		return detailedList;
	}

	public void setDetailedList(Boolean detailedList) {
		this.detailedList = detailedList;
	}
	@Column
	public Boolean getNoInsurance() {
		return noInsurance;
	}

	public void setNoInsurance(Boolean noInsurance) {
		this.noInsurance = noInsurance;
	}
	@Column
	public Boolean getLumpSum() {
		return lumpSum;
	}

	public void setLumpSum(Boolean lumpSum) {
		this.lumpSum = lumpSum;
	}
	@Column
	public BigDecimal getLumpSumAmount() {
		return lumpSumAmount;
	}

	public void setLumpSumAmount(BigDecimal lumpSumAmount) {
		this.lumpSumAmount = lumpSumAmount;
	}
	@Column
	public String getLumpPerUnit() {
		return lumpPerUnit;
	}

	public void setLumpPerUnit(String lumpPerUnit) {
		this.lumpPerUnit = lumpPerUnit;
	}
	@Column
	public String getMinReplacementvalue() {
		return minReplacementvalue;
	}

	public void setMinReplacementvalue(String minReplacementvalue) {
		this.minReplacementvalue = minReplacementvalue;
	}
    @Column
	public String getStorageEmail() {
		return storageEmail;
	}

	public void setStorageEmail(String storageEmail) {
		this.storageEmail = storageEmail;
	}

	public Integer getClaimBy() {
		return claimBy;
	}

	public void setClaimBy(Integer claimBy) {
		this.claimBy = claimBy;
	}
	@Column
	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}
	@Column
	public Boolean getConverted() {
		return converted;
	}

	public void setConverted(Boolean converted) {
		this.converted = converted;
	}
	@Column
	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	@Column
	public boolean isUsaFlagRequired() {
		return usaFlagRequired;
	}

	public void setUsaFlagRequired(boolean usaFlagRequired) {
		this.usaFlagRequired = usaFlagRequired;
	}
	@Column
	public Boolean getCustomerSurveyEmail() {
		return customerSurveyEmail;
	}

	public void setCustomerSurveyEmail(Boolean customerSurveyEmail) {
		this.customerSurveyEmail = customerSurveyEmail;
	}
	@Column
	public String getAccountHolderForRelo() {
		return accountHolderForRelo;
	}

	public void setAccountHolderForRelo(String accountHolderForRelo) {
		this.accountHolderForRelo = accountHolderForRelo;
	}
	@Column
	public String getBillingUserForRelo() {
		return billingUserForRelo;
	}

	public void setBillingUserForRelo(String billingUserForRelo) {
		this.billingUserForRelo = billingUserForRelo;
	}
	@Column
	public String getAccountManagerForRelo() {
		return accountManagerForRelo;
	}

	public void setAccountManagerForRelo(String accountManagerForRelo) {
		this.accountManagerForRelo = accountManagerForRelo;
	}
	@Column
	public String getPayableUserForRelo() {
		return payableUserForRelo;
	}

	public void setPayableUserForRelo(String payableUserForRelo) {
		this.payableUserForRelo = payableUserForRelo;
	}
	@Column
	public String getPricingUserForRelo() {
		return pricingUserForRelo;
	}

	public void setPricingUserForRelo(String pricingUserForRelo) {
		this.pricingUserForRelo = pricingUserForRelo;
	}
	@Column
	public String getAuditorForRelo() {
		return auditorForRelo;
	}

	public void setAuditorForRelo(String auditorForRelo) {
		this.auditorForRelo = auditorForRelo;
	}
	@Column
	public String getClaimsUserForRelo() {
		return claimsUserForRelo;
	}

	public void setClaimsUserForRelo(String claimsUserForRelo) {
		this.claimsUserForRelo = claimsUserForRelo;
	}
	@Column
	public String getCoordinatorForRelo() {
		return coordinatorForRelo;
	}

	public void setCoordinatorForRelo(String coordinatorForRelo) {
		this.coordinatorForRelo = coordinatorForRelo;
	}

	@Column
	public String getEmailPrintOption() {
		return emailPrintOption;
	}

	public void setEmailPrintOption(String emailPrintOption) {
		this.emailPrintOption = emailPrintOption;
	}
	@Column
	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}
	@Column
	public String getNetworkPartnerCode() {
		return networkPartnerCode;
	}

	public void setNetworkPartnerCode(String networkPartnerCode) {
		this.networkPartnerCode = networkPartnerCode;
	}
	@Column
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	@Column
	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	@Column
	public String getInsuranceHas() {
		return insuranceHas;
	}

	public void setInsuranceHas(String insuranceHas) {
		this.insuranceHas = insuranceHas;
	}
	@Column
	public String getInsuranceOption() {
		return insuranceOption;
	}

	public void setInsuranceOption(String insuranceOption) {
		this.insuranceOption = insuranceOption;
	}
	@Column
	public String getDefaultVat() {
		return defaultVat;
	}

	public void setDefaultVat(String defaultVat) {
		this.defaultVat = defaultVat;
	}
	@Column
	public String getBillToAuthorization() {
		return billToAuthorization;
	}

	public void setBillToAuthorization(String billToAuthorization) {
		this.billToAuthorization = billToAuthorization;
	}

	public String getNetworkPartnerName() {
		return networkPartnerName;
	}

	public void setNetworkPartnerName(String networkPartnerName) {
		this.networkPartnerName = networkPartnerName;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	@Column
	public Boolean getExcludeFinancialExtract() {
		return excludeFinancialExtract;
	}

	public void setExcludeFinancialExtract(Boolean excludeFinancialExtract) {
		this.excludeFinancialExtract = excludeFinancialExtract;
	}
	@Column
	public BigDecimal getNPSscore() {
		return NPSscore;
	}

	public void setNPSscore(BigDecimal nPSscore) {
		NPSscore = nPSscore;
	}
	
	@Column
	public String getVatBillingGroup() {
		return vatBillingGroup;
	}

	public void setVatBillingGroup(String vatBillingGroup) {
		this.vatBillingGroup = vatBillingGroup;
	}
	@Column
	public String getPrivateCurrency() {
		return privateCurrency;
	}

	public void setPrivateCurrency(String privateCurrency) {
		this.privateCurrency = privateCurrency;
	}
	@Column
	public String getPrivateBankCode() {
		return privateBankCode;
	}

	public void setPrivateBankCode(String privateBankCode) {
		this.privateBankCode = privateBankCode;
	}
	@Column
	public String getPrivateBankAccountNumber() {
		return privateBankAccountNumber;
	}

	public void setPrivateBankAccountNumber(String privateBankAccountNumber) {
		this.privateBankAccountNumber = privateBankAccountNumber;
	}
	@Column
	public String getPrivateVatNumber() {
		return privateVatNumber;
	}

	public void setPrivateVatNumber(String privateVatNumber) {
		this.privateVatNumber = privateVatNumber;
	}
	@Column
	public String getAgentClassification() {
		return agentClassification;
	}

	public void setAgentClassification(String agentClassification) {
		this.agentClassification = agentClassification;
	}
	@Column
	public BigDecimal getMinimumMargin() {
		return minimumMargin;
	}

	public void setMinimumMargin(BigDecimal minimumMargin) {
		this.minimumMargin = minimumMargin;
	}

	public String getRddBased() {
		return rddBased;
	}

	public void setRddBased(String rddBased) {
		this.rddBased = rddBased;
	}

	public String getRddDays() {
		return rddDays;
	}

	public void setRddDays(String rddDays) {
		this.rddDays = rddDays;
	}

	public String getInternalBillingPerson() {
		return internalBillingPerson;
	}

	public void setInternalBillingPerson(String internalBillingPerson) {
		this.internalBillingPerson = internalBillingPerson;
	}

	@Column
	public BigDecimal getSellRate() {
		return sellRate;
	}

	public void setSellRate(BigDecimal sellRate) {
		this.sellRate = sellRate;
	}

	@Column
	public BigDecimal getBuyRate() {
		return buyRate;
	}

	public void setBuyRate(BigDecimal buyRate) {
		this.buyRate = buyRate;
	}

	@Column
	public Boolean getDoNotInvoice() {
		return doNotInvoice;
	}

	public void setDoNotInvoice(Boolean doNotInvoice) {
		this.doNotInvoice = doNotInvoice;
	}

	public Integer getCompensationYear() {
		return compensationYear;
	}

	public void setCompensationYear(Integer compensationYear) {
		this.compensationYear = compensationYear;
	}

	public String getRutTaxNumber() {
		return rutTaxNumber;
	}

	public void setRutTaxNumber(String rutTaxNumber) {
		this.rutTaxNumber = rutTaxNumber;
	}

    @Column
	public Boolean getDoNotSayYesEmail() {
		return doNotSayYesEmail;
	}

	public void setDoNotSayYesEmail(Boolean doNotSayYesEmail) {
		this.doNotSayYesEmail = doNotSayYesEmail;
	}
	
	public String getDefaultContact() {
		return defaultContact;
	}

	public void setDefaultContact(String defaultContact) {
		this.defaultContact = defaultContact;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStorageEmailType() {
		return storageEmailType;
	}

	public void setStorageEmailType(String storageEmailType) {
		this.storageEmailType = storageEmailType;
	}

	public String getBillingMoment() {
		return billingMoment;
	}

	public void setBillingMoment(String billingMoment) {
		this.billingMoment = billingMoment;
	}

	public BigDecimal getConsumablePercentage() {
		return consumablePercentage;
	}

	public void setConsumablePercentage(BigDecimal consumablePercentage) {
		this.consumablePercentage = consumablePercentage;
	}

	public String getMultipleCompanyDivision() {
		return multipleCompanyDivision;
	}

	public void setMultipleCompanyDivision(String multipleCompanyDivision) {
		this.multipleCompanyDivision = multipleCompanyDivision;
	}
	public Boolean getBillToExtractAccess() {
		return billToExtractAccess;
	}

	public void setBillToExtractAccess(Boolean billToExtractAccess) {
		this.billToExtractAccess = billToExtractAccess;
	}

	public Boolean getVendorExtractAccess() {
		return vendorExtractAccess;
	}

	public void setVendorExtractAccess(Boolean vendorExtractAccess) {
		this.vendorExtractAccess = vendorExtractAccess;
	}

	@Column
	public Boolean getreloPartnerPortal() {
		return reloPartnerPortal;
	}

	public void setreloPartnerPortal(Boolean reloPartnerPortal) {
		this.reloPartnerPortal = reloPartnerPortal;
	}
	public String getBillingCycle() {
		return billingCycle;
	}

	public void setBillingCycle(String billingCycle) {
		this.billingCycle = billingCycle;
	}
	
}
