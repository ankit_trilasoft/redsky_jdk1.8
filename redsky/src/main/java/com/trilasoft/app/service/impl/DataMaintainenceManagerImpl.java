package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DataMaintainenceDao;
import com.trilasoft.app.model.DataMaintainence;
import com.trilasoft.app.service.DataMaintainenceManager;

public class DataMaintainenceManagerImpl extends GenericManagerImpl<DataMaintainence, Long> implements DataMaintainenceManager {

	DataMaintainenceDao dataMaintainenceDao; 
	
	public DataMaintainenceManagerImpl(DataMaintainenceDao dataMaintainenceDao) {
		super(dataMaintainenceDao);
		this.dataMaintainenceDao=dataMaintainenceDao;
		
	}
	public List executeQuery(String query ){
	  return dataMaintainenceDao.executeQuery(query);
	}
	public String bulkUpdate(String query){
		return dataMaintainenceDao.bulkUpdate(query);
	}
}
