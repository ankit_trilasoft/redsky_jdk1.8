/**
 * WSQuestionResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.result;

public class WSQuestionResponse  extends com.keysurvey.api.v68.WSObject  implements java.io.Serializable {
    private com.keysurvey.api.v68.form.result.WSAnswerResponse[] answerResponses;

    private long questionId;

    private long respondentId;

    public WSQuestionResponse() {
    }

    public WSQuestionResponse(
           long versionId,
           com.keysurvey.api.v68.form.result.WSAnswerResponse[] answerResponses,
           long questionId,
           long respondentId) {
        super(
            versionId);
        this.answerResponses = answerResponses;
        this.questionId = questionId;
        this.respondentId = respondentId;
    }


    /**
     * Gets the answerResponses value for this WSQuestionResponse.
     * 
     * @return answerResponses
     */
    public com.keysurvey.api.v68.form.result.WSAnswerResponse[] getAnswerResponses() {
        return answerResponses;
    }


    /**
     * Sets the answerResponses value for this WSQuestionResponse.
     * 
     * @param answerResponses
     */
    public void setAnswerResponses(com.keysurvey.api.v68.form.result.WSAnswerResponse[] answerResponses) {
        this.answerResponses = answerResponses;
    }

    public com.keysurvey.api.v68.form.result.WSAnswerResponse getAnswerResponses(int i) {
        return this.answerResponses[i];
    }

    public void setAnswerResponses(int i, com.keysurvey.api.v68.form.result.WSAnswerResponse _value) {
        this.answerResponses[i] = _value;
    }


    /**
     * Gets the questionId value for this WSQuestionResponse.
     * 
     * @return questionId
     */
    public long getQuestionId() {
        return questionId;
    }


    /**
     * Sets the questionId value for this WSQuestionResponse.
     * 
     * @param questionId
     */
    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }


    /**
     * Gets the respondentId value for this WSQuestionResponse.
     * 
     * @return respondentId
     */
    public long getRespondentId() {
        return respondentId;
    }


    /**
     * Sets the respondentId value for this WSQuestionResponse.
     * 
     * @param respondentId
     */
    public void setRespondentId(long respondentId) {
        this.respondentId = respondentId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSQuestionResponse)) return false;
        WSQuestionResponse other = (WSQuestionResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.answerResponses==null && other.getAnswerResponses()==null) || 
             (this.answerResponses!=null &&
              java.util.Arrays.equals(this.answerResponses, other.getAnswerResponses()))) &&
            this.questionId == other.getQuestionId() &&
            this.respondentId == other.getRespondentId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAnswerResponses() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAnswerResponses());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAnswerResponses(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += new Long(getQuestionId()).hashCode();
        _hashCode += new Long(getRespondentId()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSQuestionResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://result.form.v68.api.keysurvey.com", "WSQuestionResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("answerResponses");
        elemField.setXmlName(new javax.xml.namespace.QName("", "answerResponses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://result.form.v68.api.keysurvey.com", "WSAnswerResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("questionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "questionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respondentId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "respondentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
