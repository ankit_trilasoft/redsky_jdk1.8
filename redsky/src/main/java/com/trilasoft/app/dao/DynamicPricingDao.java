package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DynamicPricing;

public interface DynamicPricingDao extends GenericDao<DynamicPricing, Long>{
public DynamicPricing getDynamicPricingByType(String type,String sessionCorpID);
public List getDynamicPricingCalenderByType(String type,String sessionCorpID);
}
