package org.appfuse.service.impl;

import java.util.Date;
import java.util.List;

import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.appfuse.dao.UserDao;
import org.appfuse.model.User;
import org.appfuse.service.UserExistsException;
import org.appfuse.service.UserManager;
import org.springframework.dao.DataIntegrityViolationException;

import com.trilasoft.app.dao.CustomerFileDao;


/**
 * Implementation of UserManager interface.</p>
 * 
 * <p>
 * <a href="UserManagerImpl.java.html"><i>View Source</i></a>
 * </p>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
public class UserManagerImpl extends BaseManager implements UserManager {
     private UserDao dao;

    /**
     * Set the Dao for communication with the data layer.
     * @param dao
     */
    
    public UserManagerImpl(UserDao dao) {   
        this.dao = dao;   
    } 
    
    public void setUserDao(UserDao dao) {
        this.dao = dao;
    }

    /**
     * @see org.appfuse.service.UserManager#getUser(java.lang.String)
     */
    public User getUser(String userId) {
        return dao.getUser(new Long(userId));
    }

    /**
     * @see org.appfuse.service.UserManager#getUsers(org.appfuse.model.User)
     */
    public List getUsers(User user) {
        return dao.getUsers(user);
    }

    /**
     * @see org.appfuse.service.UserManager#saveUser(org.appfuse.model.User)
     */
    public void saveUser(User user) throws UserExistsException {
        // if new user, lowercase userId
        if (user.getVersion() == null) {
            user.setUsername(user.getUsername().toLowerCase());
        }
        try {
            dao.saveUser(user);
        } catch (DataIntegrityViolationException e) {
            throw new UserExistsException("User '" + user.getUsername() + "' already exists!" + e.toString());
        }
    }

    /**
     * @see org.appfuse.service.UserManager#removeUser(java.lang.String)
     */
    public void removeUser(String userId) {
        if (log.isDebugEnabled()) {
            log.debug("removing user: " + userId);
        }

        dao.removeUser(new Long(userId));
    }

    public User getUserByUsername(String username) throws UsernameNotFoundException {
        return (User) dao.loadUserByUsername(username);
    }
    
    public List findUserRoles(String corpID){
    	return dao.findUserRoles(corpID);
    }
    
    public List findUserRoles(String corpID,String category){
    	return dao.findUserRoles(corpID, category);
    }
    
    public List findUserPassword(String corpID){
        return dao.findUserPassword(corpID);
        }
    public List findUserDetails(String userName){
		return dao.findUserDetails(userName);
	}
	public void resetPassword(String passwordNew,String confirmPasswordNew,String pwdHintQues,String pwdHint,String userName){
		dao.resetPassword(passwordNew,confirmPasswordNew,pwdHintQues,pwdHint,userName);
	}
	public void findPwordHintQ(String pwdHintQues,String userName){
		dao.findPwordHintQ(pwdHintQues,userName);	
	}
	public void forgotPassword(String pwdHintQues,String pwdHint,String userName){
	dao.forgotPassword(pwdHintQues,pwdHint,userName);
	}
	public void removeUserPhotograph(String location,String userName){
		dao.removeUserPhotograph(location,userName);
	}

	public List getDateSecuritySet() {
		return dao.getDateSecuritySet();
	}

	public List getPermissionList() {
		return dao.getPermissionList();
	}

	public List getPermission(String permissionName) {
		return dao.getPermission(permissionName);
	}

	public List getSuperVisor(String corpID) {
		return dao.getSuperVisor(corpID);
	}

	public List getPartnerExpirationDate(String sessionCorpID, String username) {
		return dao.getPartnerExpirationDate(sessionCorpID, username);
	}

	public void updatePasswordExpiryDate(String username, String sessionCorpID, Date compareWithDate) {
		dao.updatePasswordExpiryDate(username, sessionCorpID, compareWithDate);
	}

	public void setExpirePassword(String username, String sessionCorpID) {
		dao.setExpirePassword(username, sessionCorpID);	
	}

	public void updatePassword(String passwordNew, Long id) {
		dao.updatePassword(passwordNew, id);
	}
    
	public void updateUserNewsUpdateFlag(String corpId,Boolean flag,String user) {
		dao.updateUserNewsUpdateFlag(corpId,flag,user);
	}
	
	public List findDateDiff(Date pwdexpiryDate, Date compareWithDate) {
		return dao.findDateDiff(pwdexpiryDate, compareWithDate);
	}

	public List getFromEmailAddress(String corpID) {
		return dao.getFromEmailAddress(corpID);
	}

	public List findUserByUsername(String coordinator) {
		return dao.findUserByUsername(coordinator);
	}
	public List getAllUserList(String corpID){
		return dao.getAllUserList(corpID);
	}
	public List getAllUserRole(String userList,String sessionCorpID,String activeStatus){
		return dao.getAllUserRole(userList, sessionCorpID,activeStatus);
	}
	public List pickAllUser(String roleList,String sessionCorpID,String userList,String activeStatus){
		return dao.pickAllUser(roleList, sessionCorpID,userList,activeStatus);
	}
	public int updateUserWithRoles(String transferFrom,String roleTransfer,String transferTo,String sessionCorpID){
		return dao.updateUserWithRoles(transferFrom, roleTransfer, transferTo,sessionCorpID);
	}

	public List getAllUserContactListByPartnerCode(String partnerCode,String corpID) {
		return dao.getAllUserContactListByPartnerCode(partnerCode,corpID);
	}
	public List getContactListByPartnerCode(String partnerCode,String corpID) {
		return dao.getContactListByPartnerCode(partnerCode,corpID);
	}
	
	public List getSearchContactListByPartnerCode(String partnerCode,String corpID,String name, String emailId){
		return dao.getSearchContactListByPartnerCode(partnerCode, corpID, name, emailId);
	}
	/* Modified by Kunal Kumar Sharma for ticket number: 6838 */
	public List getAllUserContactListByPartnerCode(String partnerCode,String corpID,String name, String emailId){
		return dao.getAllUserContactListByPartnerCode(partnerCode, corpID, name, emailId);
	}
	/* Modification ends */
	public List findCoordinatorByUsername(String coordinator,String sessionCorpID) {
		return dao.findCoordinatorByUsername(coordinator, sessionCorpID);
	}

	public List findCoordinatorByNetworkCoordinator(String coordinator, String corpID) {
		return dao.findCoordinatorByNetworkCoordinator(coordinator, corpID);
	}
	public String doNotEmailFlag(String email){
		return dao.doNotEmailFlag(email);
	}
	public String findToolTip(String username, String corpID){
		return dao.findToolTip(username,corpID);
	}

	public List findUserDetail(String coordinator, String sessionCorpID) { 
		return dao.findUserDetail(coordinator, sessionCorpID);
	}
	
	public void updateUserInfo(String updateBy,String userID,String datasecurityID) {
		dao.updateUserInfo(updateBy,userID,datasecurityID);
	}
	public void updateUserRoleInfo(String updateBy,String userID,String roleID){
		dao.updateUserRoleInfo(updateBy,userID,roleID);
	}
	
	public void updateUserLoginFailureReason(String userName,String chng,String chng1){
		dao.updateUserLoginFailureReason(userName, chng, chng1);
	}
	public int getDefaultContactPersonCount(String partnerCode, String corpID){
		return dao.getDefaultContactPersonCount(partnerCode,corpID);
	}

	
	public List getUsersByParentAgent(String parentAgent) {
		return dao.getUsersByParentAgent(parentAgent);
	}
	public List getAllInactiveUserList(String corpID)
	{
		return dao.getAllInactiveUserList(corpID);
	}	
	/* sonu contact search*/
	public List getContactList(String partnerCode,String corpID,String name, String emailId,String phone_number){
		return dao.getContactList(partnerCode, corpID, name, emailId, phone_number);
	}
	public List getagentRequestList(String partnerCode,String corpID,String name, String emailId,String phone_number,String status){
		return dao.getagentRequestList(partnerCode, corpID, name, emailId, phone_number,status);
	}
	public List getUserRequestDetailList(String email)
	{return dao.getUserRequestDetailList(email);
		
	}

	public List findUserUsername(String coordinator) {
		// TODO Auto-generated method stub
		return dao.findUserUsername(coordinator);
	}
}
