package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.service.QuotationCostingManager;
import com.trilasoft.app.dao.QuotationCostingDao;
import com.trilasoft.app.model.QuotationCosting;


public class QuotationCostingManagerImpl  extends GenericManagerImpl<QuotationCosting, Long> implements QuotationCostingManager {

	QuotationCostingDao quotationCostingDao; 
    
    
    public QuotationCostingManagerImpl(QuotationCostingDao quotationCostingDao) { 
        super(quotationCostingDao); 
        this.quotationCostingDao = quotationCostingDao; 
    } 

    public List checkById(Long id) { 
        return quotationCostingDao.checkById(id); 
    }
    
    public List<QuotationCosting> findBySequenceNumber(String sequenceNumber) { 
        return quotationCostingDao.findBySequenceNumber(sequenceNumber); 
    }
   
}
