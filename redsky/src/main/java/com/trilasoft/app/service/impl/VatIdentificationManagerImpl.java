package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.VatIdentificationDao;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.VatIdentification;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.VatIdentificationManager;

public class VatIdentificationManagerImpl extends GenericManagerImpl<VatIdentification, Long> implements VatIdentificationManager {
	VatIdentificationDao vatIdentificationDao;
	public VatIdentificationManagerImpl(VatIdentificationDao vatIdentificationDao){
			 super(vatIdentificationDao);   
        this.vatIdentificationDao = vatIdentificationDao;
		// TODO Auto-generated constructor stub
	}
	public List findAllVat() {
		return vatIdentificationDao.findAllVat();
	}
	public List isCountryExisted(String Country ,String corpid) {
		return vatIdentificationDao.isCountryExisted( Country ,corpid);
	}
	public List searchAll(String vatCountryCode ) {
		return vatIdentificationDao.searchAll( vatCountryCode);
	}
	public String getIdVatCode(String vatCountryCode){
		return vatIdentificationDao. getIdVatCode(vatCountryCode);
	}
}
