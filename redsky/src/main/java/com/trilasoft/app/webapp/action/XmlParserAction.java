package com.trilasoft.app.webapp.action;
import java.util.Date;
import java.util.List;

import org.apache.log4j.PropertyConfigurator;

import org.apache.log4j.Logger;


import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.XmlParser;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.XmlParserManager;

public class XmlParserAction extends BaseAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String file;
	private String sessionCorpID;
	private XmlParser xmlParser;
	private XmlParserManager xmlParserManager;
	private SystemDefaultManager systemDefaultManager;
	  Date currentdate = new Date();
      static final Logger logger = Logger.getLogger(XmlParserAction.class);
	
	public XmlParserAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	public String uploadXmlData(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveData(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage(getText(Key)); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	
	public String importXmlData(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveData(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}

	// deleted URl for this method from struts file we can delete this method from this action also 
	public String updateNokRate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveNokRate(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	/*public String updateRateUSD(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveUsdRate(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}*/
	/*public String updateRateGBP(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveGBPRate(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}*/
	/*public String updateRateCNY(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveCNYRate(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}*/
	/*public String uploadINRRate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveINRRate(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}*/
	/*public String uploadKRWRate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveKRWRate(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}*/
	// deleted URl for this method from struts file we can delete this method from this action also
	public String uploadCADRate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveCADRate(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	/*public String uploadSGDRate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveSGDRate(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}*/
	/*public String uploadJODRate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveJODRate(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}*/
	/*public String uploadIDRRate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveIDRRate(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}	*/
	
	// deleted URl for this method from struts file we can delete this method from this action also
	public String uploadDynamicRate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String baseCurrency="";
		List<SystemDefault> sysDefaultDetail = systemDefaultManager.findByCorpID(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault1 : sysDefaultDetail) {				
				baseCurrency=systemDefault1.getBaseCurrency();
			}
		}
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveDynamicRate(sessionCorpID,file,userName,baseCurrency); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}	
	/*public String uploadBRLRate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xmlParserManager.saveBRLRate(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage("Uploaded Successfully"); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	*/
	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public XmlParser getXmlParser() {
		return xmlParser;
	}

	public void setXmlParser(XmlParser xmlParser) {
		this.xmlParser = xmlParser;
	}

	public XmlParserManager getXmlParserManager() {
		return xmlParserManager;
	}

	public void setXmlParserManager(XmlParserManager xmlParserManager) {
		this.xmlParserManager = xmlParserManager;
	}
	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

}
