<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="JobsNotInvoiced"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="742"
		 pageHeight="842"
		 columnWidth="682"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT	"JobsNotInvoiced-R-P-GMOV.jrxml",
	if(serviceorder.companyDivision is null || trim(serviceorder.companyDivision)='','',trim(serviceorder.companyDivision)) AS serviceorder_companyDivision,
	serviceorder.shipNumber AS serviceorder_shipNumber,
	serviceorder.status AS serviceorder_status,
	DATE_FORMAT(now(),'%d-%b-%y') AS currentdate,
	if((serviceorder.firstName is null || trim(serviceorder.firstName)=''),trim(serviceorder.lastName),concat(trim(serviceorder.firstName)," ",trim(serviceorder.lastName))) AS Shipper,
	billing.billToCode AS billing_billToCode,
	billing.billToName AS billing_billToName,
	serviceorder.job AS serviceorder_job,
	DATE_FORMAT(trackingstatus.loadA,'%d-%b-%y') AS trackingstatus_LoadA,
	DATE_FORMAT(trackingstatus.deliveryA,'%d-%b-%y') AS trackingstatus_DeliveryA,
	sum(if(accountline.actualRevenue is null || accountline.actualRevenue='',0,accountline.actualRevenue)) AS invAmount,
	if(accountline.recRateCurrency is null || accountline.recRateCurrency='','',accountline.recRateCurrency) as accountline_recRateCurrency,
	-- if(systemdefault.baseCurrency is null || trim(systemdefault.baseCurrency)='','',trim(systemdefault.baseCurrency)) AS systemdefault_baseCurrency,
	trim(notes.subject) AS notes_subject
From serviceorder serviceorder
	INNER JOIN billing billing ON serviceorder.id = billing.id AND billing.corpId = $P{Corporate ID}
	INNER JOIN trackingstatus trackingstatus ON serviceorder.id = trackingstatus.id AND trackingstatus.corpId = $P{Corporate ID}
	INNER JOIN systemdefault systemdefault ON systemdefault.corpid=serviceorder.corpid AND systemdefault.corpID=$P{Corporate ID}
	LEFT OUTER JOIN accountline accountline ON serviceorder.id = accountline.serviceOrderId AND accountline.corpId = $P{Corporate ID} AND accountline.status is true
	LEFT OUTER JOIN notes notes ON serviceorder.id = notes.notesKeyId AND notes.corpId = $P{Corporate ID} AND (notes.noteSubType is null OR notes.noteSubType='' OR notes.noteSubType = 'Accounting Issue' ) AND (notes.notestatus<>'CMP' or notes.notestatus is null)
WHERE	serviceorder.corpId = $P{Corporate ID}
	AND if(serviceorder.routing='IMP',(trackingstatus.deliveryA <= sysdate()),(trackingstatus.loadA <= sysdate() OR trackingstatus.deliveryA <= sysdate()))
	AND serviceorder.status not in ('CNCL','CANCEL','DWNLD','DWN','HOLD','CLSD')
	AND serviceorder.job!=('DWN')
	AND billing.billComplete is null
	AND serviceorder.actualrevenue=0
GROUP BY serviceorder.shipNumber
ORDER BY serviceorder.companyDivision,accountline.shipNumber,accountline.recRateCurrency,trackingstatus.loadA desc]]></queryString>

	<field name="JobsNotInvoiced-R-P-GMOV.jrxml" class="java.lang.String"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_status" class="java.lang.String"/>
	<field name="currentdate" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="billing_billToCode" class="java.lang.String"/>
	<field name="billing_billToName" class="java.lang.String"/>
	<field name="serviceorder_job" class="java.lang.String"/>
	<field name="trackingstatus_LoadA" class="java.lang.String"/>
	<field name="trackingstatus_DeliveryA" class="java.lang.String"/>
	<field name="invAmount" class="java.math.BigDecimal"/>
	<field name="accountline_recRateCurrency" class="java.lang.String"/>
	<field name="notes_subject" class="java.lang.String"/>


		<group  name="CompDiv" isStartNewPage="true" >
			<groupExpression><![CDATA[$F{serviceorder_companyDivision}]]></groupExpression>
			<groupHeader>
			<band height="20"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="102"
						y="0"
						width="152"
						height="20"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_companyDivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="0"
						width="102"
						height="20"
						key="staticText-13"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="Shipnumber" >
			<groupExpression><![CDATA[$F{serviceorder_shipNumber}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="82"
						y="0"
						width="600"
						height="18"
						key="textField"
						printWhenGroupChanges="Shipnumber"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="8" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{notes_subject}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="0"
						width="82"
						height="18"
						key="staticText-11"
						printWhenGroupChanges="Shipnumber"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Accounting Notes :]]></text>
				</staticText>
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="37"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="5"
						width="682"
						height="32"
						key="staticText-1"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="14" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Jobs Not Invoiced But Loaded Or Delivered]]></text>
				</staticText>
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="29"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="1"
						y="9"
						width="87"
						height="20"
						key="staticText-2"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[SO #]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="88"
						y="9"
						width="58"
						height="20"
						key="staticText-3"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Status]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="146"
						y="9"
						width="117"
						height="20"
						key="staticText-4"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Shipper]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="263"
						y="9"
						width="69"
						height="20"
						key="staticText-5"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Bill To Code]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="332"
						y="9"
						width="122"
						height="20"
						key="staticText-6"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Bill To Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="454"
						y="9"
						width="40"
						height="20"
						key="staticText-7"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Job]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="494"
						y="9"
						width="59"
						height="20"
						key="staticText-8"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Load Act.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="553"
						y="9"
						width="67"
						height="20"
						key="staticText-9"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Delivery Act.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="609"
						y="9"
						width="73"
						height="20"
						key="staticText-10"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Inv Amt]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="0"
						width="87"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="88"
						y="0"
						width="58"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_status}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="146"
						y="0"
						width="117"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Shipper}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="263"
						y="0"
						width="69"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_billToCode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="332"
						y="0"
						width="122"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_billToName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="454"
						y="0"
						width="40"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_job}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="494"
						y="0"
						width="59"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{trackingstatus_LoadA}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="553"
						y="0"
						width="56"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{trackingstatus_DeliveryA}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="609"
						y="0"
						width="73"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{invAmount}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="17"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="552"
						y="3"
						width="57"
						height="14"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="609"
						y="3"
						width="73"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Helvetica" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="CP1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="3"
						width="145"
						height="14"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Times-Roman" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{currentdate}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="246"
						y="3"
						width="190"
						height="14"
						key="staticText-12"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="8"/>
					</textElement>
				<text><![CDATA[JobsNotInvoiced.jrxml]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="678"
						height="0"
						key="line-1"/>
					<graphicElement stretchType="NoStretch" fill="Solid" />
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
