/**
 * Implementation of <strong>ActionSupport</strong> that contains convenience methods.
 * This class represents the basic actions on "ServiceOrder" object in Redsky that allows for Job management.
 * @Class Name	ServiceOrder Manager Impl
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */


package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.trilasoft.app.dao.ServiceOrderDao;
import com.trilasoft.app.model.BillingView;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatusView;
import com.trilasoft.app.service.ServiceOrderManager;

import org.appfuse.service.impl.GenericManagerImpl;

public class ServiceOrderManagerImpl extends GenericManagerImpl<ServiceOrder, Long> implements ServiceOrderManager {
	ServiceOrderDao serviceOrderDao;

		public ServiceOrderManagerImpl(ServiceOrderDao serviceOrderDao) {
		super(serviceOrderDao); 
		this.serviceOrderDao = serviceOrderDao;
	}
	public List<ServiceOrder> findByShipNumber(String lastName, String firstName, String shipNumber, String status, Date statusDate, String job, String coordinator) {
		return serviceOrderDao.findByShipNumber(lastName, firstName, shipNumber, status, statusDate, job, coordinator);
	}
	public List<ServiceOrder> findByStorage(String shipNumber) {
		return serviceOrderDao.findByStorage(shipNumber);
	}	public List findMaximumId() {
		return serviceOrderDao.findMaximumId();
	}
	public List findStorageId(String shipNumber){
		return serviceOrderDao.findStorageId(shipNumber);
	}
	public List findMaximumCarrierNumber(String shipNum) {
		return serviceOrderDao.findMaximumCarrierNumber(shipNum);
	}
	public List findMaximumIdNumber(String shipNum) {
		return serviceOrderDao.findMaximumIdNumber(shipNum);
	}
	public List findShipNumber(String shipNum) {
		return serviceOrderDao.findShipNumber(shipNum);
	}
	public List findMaximum(String shipNum) {
		return serviceOrderDao.findMaximum(shipNum);
	}
	public List findIdByShipNumber(String shipNumber) {
		return serviceOrderDao.findIdByShipNumber(shipNumber);
	}
	public List findMaxDate(String shipNumber) {
		return serviceOrderDao.findMaxDate(shipNumber);
	}
	public int disableCommissionZeroEntry(Long sid, String sessionCorpID){
		return serviceOrderDao.disableCommissionZeroEntry( sid,  sessionCorpID);
	}
	public List claimDate(String shipNumber){
		return serviceOrderDao.claimDate(shipNumber);
	}
	public List findMinDate(String shipNumber) {
		return serviceOrderDao.findMinDate(shipNumber);
	}
	public void updateFromAccountLine(ServiceOrder serviceOrder) {
		serviceOrderDao.updateFromAccountLine(serviceOrder);
	}
	public List findMaximumLineNumber(String shipNum) {
		return serviceOrderDao.findMaximumLineNumber(shipNum);
	}
	public List findByCoordinator(String userName) {
		return serviceOrderDao.findByCoordinator(userName);
	}
	public List findBySysDefault(String userName) {
		return serviceOrderDao.findBySysDefault(userName);
	}
	public List findSysDefault(String userName){
		return serviceOrderDao.findSysDefault(userName);
	}
	public List findAccountLineCompDiv(Long sid, String sessionCorpID){
		return serviceOrderDao.findAccountLineCompDiv(sid, sessionCorpID);
	}
	public List getReverseInvoiceIds(String chargeCode1, String sessionCorpID,Long sid,String aidList){
		return serviceOrderDao.getReverseInvoiceIds(chargeCode1, sessionCorpID, sid,aidList);
	}
	public List findAccountLineListByCompDiv(Long sid, String sessionCorpID,String compDiv,String aidList){
		return serviceOrderDao.findAccountLineListByCompDiv(sid, sessionCorpID, compDiv,aidList);
	}
	public List getCommissionById(Long accId, String sessionCorpID ,String commLine1 ,String commLine2){
		return serviceOrderDao.getCommissionById(accId, sessionCorpID,commLine1,commLine2);
	}
	public int updateCustomer(String sequenceNumber,String bookingAgentCode,String bookingAgentName){
		return serviceOrderDao.updateCustomer(sequenceNumber,bookingAgentCode,bookingAgentName);
	}
	public List financeSummary(String jobtype, String corpID)
	{
		return serviceOrderDao.financeSummary(jobtype, corpID);
	}
	public List activeShipments(String jobtype, String corpID)
	{
		return serviceOrderDao.activeShipments(jobtype,corpID);
	}
	public List dynamicFinanceDetails(String jobtype, String corpID)
	{
		return serviceOrderDao.dynamicFinanceDetails(jobtype, corpID);
	}
	public List<ServiceOrder> serviceOrderList(){
		return serviceOrderDao.serviceOrderList();
	}
	public List forAgent(String shipNumber,String corpId)
	{
		return serviceOrderDao.forAgent(shipNumber, corpId);
	}
	public List findmiscVlSystemDefault(String corpID) {
		return serviceOrderDao.findmiscVlSystemDefault(corpID);
	}
	public List findStorageSystemDefault(String corpID) {
		return serviceOrderDao.findStorageSystemDefault(corpID); 
	}
	public List findAccountInterface(String corpID) {
		return serviceOrderDao.findAccountInterface(corpID);
	}
	public List findByPaySysDefault(String corpId) {
		return serviceOrderDao.findByPaySysDefault(corpId);
	}
	public String findJobSeqNumber(String corpID,String bookingAgentCode,String jobType) {
		return serviceOrderDao.findJobSeqNumber(corpID,bookingAgentCode,jobType);
	} 
	
	public List findGrossMarginThreshold(String corpID) {
		return serviceOrderDao.findGrossMarginThreshold(corpID);
	}
	public List getCommissionAmountTotalValue(String sessionCorpID,String fieldName, Long accId, String commLineId){
		return serviceOrderDao.getCommissionAmountTotalValue(sessionCorpID, fieldName, accId,commLineId);
	}
	public List findSalesCommisionRate(String corpID) {
		return serviceOrderDao.findSalesCommisionRate(corpID);
	}
	public ArrayList getBillingCompleteStatus(String shipnumber) {
		return serviceOrderDao.getBillingCompleteStatus(shipnumber);
	}
	public List findWorkTicketExtract(String query){
		return serviceOrderDao.findWorkTicketExtract(query);
	}
	public List findWorldBankExtract(String query,String track,String balance,String sessionCorpID){
		return serviceOrderDao.findWorldBankExtract(query,track,balance,sessionCorpID);
	}
	public List checkRegistrationNumber(String registrationNumber, String sessionCorpID) {
		return serviceOrderDao.checkRegistrationNumber(registrationNumber, sessionCorpID);
	}
	public List validateRegistrationNumber(String corpID,String regNum) {
		return serviceOrderDao.validateRegistrationNumber(corpID,regNum);
	}
	public List validateRegistrationNumberLogic(String corpID,String regNum,String shipNumber){
		return serviceOrderDao.validateRegistrationNumberLogic(corpID, regNum, shipNumber);
	}
	public List getSameRegNumber(Long id, String sessionCorpID) { 
		return serviceOrderDao.getSameRegNumber(id, sessionCorpID);
	}
	public List storageAnalysis(String query, String corpID){
		return serviceOrderDao.storageAnalysis(query, corpID);
	}
	public List findBookingAgent(String shipNumber, String corpID) {
		
		return serviceOrderDao.findBookingAgent(shipNumber, corpID);
	}
	public int updateBookingAgent(String bookingAgentCheckedBy, String shipNumber, String corpID) {
		return serviceOrderDao.updateBookingAgent(bookingAgentCheckedBy, shipNumber, corpID);
	}
	public int updateRevisedStatus(String revisedStatus,Long ids,String userName,String revisedDate){
		return serviceOrderDao.updateRevisedStatus(revisedStatus, ids,userName,revisedDate);
	}
	public List getOldStatus(Long id, String corpID) {
		
		return serviceOrderDao.getOldStatus(id, corpID);
	}
	public List getStorageData(Long id, String corpID) {
		
		return serviceOrderDao.getStorageData(id, corpID);
	}
	public List getATADate(Long id) {
		
		return serviceOrderDao.getATADate(id);
	}
	public List getATDDate(Long id) { 
		return serviceOrderDao.getATDDate(id);
	}
	public int updateStorageJob(Long id,String status,int statusNumber, String corpID,String userName) {
		return serviceOrderDao.updateStorageJob(id, status, statusNumber, corpID,userName);
	}
	public List partnerActiveShipments(String query) {
		
		return serviceOrderDao.partnerActiveShipments(query);
	}
	public List getUserDateSecurity(Long id, String sessionCorpID) {
		return serviceOrderDao.getUserDateSecurity(id, sessionCorpID);
	}
	public List findByBooking(String bookingAgentCode, String corpID) {
		
		return serviceOrderDao.findByBooking(bookingAgentCode, corpID);
	}
	public List findTicketStatus(Long sid, String corpID) {
		
		return serviceOrderDao.findTicketStatus(sid, corpID);
	}
	public List storageExtractCredit(String corpID, String recInvoiceDates, String companyDivision) {
		return serviceOrderDao.storageExtractCredit(corpID, recInvoiceDates,companyDivision);
		
	}
	public List getStatusNumberCount(String sequenceNumber, Long id, String corpID) {
		
		return serviceOrderDao.getStatusNumberCount(sequenceNumber, id, corpID);
	}
	public List getStatusNumberCount(String sequenceNumber, String corpID) {
		
		return serviceOrderDao.getStatusNumberCount(sequenceNumber, corpID);
	}
	public List getStatusNumberList(String sequenceNumber, String corpID, Long id) {
		
		return serviceOrderDao.getStatusNumberList(sequenceNumber, corpID, id);
	}
	public List getJobstatusNumberList(String sequenceNumber, String corpID, Long id) {
		
		return serviceOrderDao.getJobstatusNumberList(sequenceNumber, corpID, id);
	}
	public List dynamicActiveShipments(String selectCondition, String query, String corpID) {
		
		return serviceOrderDao.dynamicActiveShipments(selectCondition, query, corpID);
	}
	public List dynamicFinancialSummary(String selectCondition, String query, String corpID) {
		
		return serviceOrderDao.dynamicFinancialSummary(selectCondition, query, corpID) ;
	}
	public List dynamicDataFinanceDetails(String selectCondition, String query, String corpID) {
		
		return serviceOrderDao.dynamicDataFinanceDetails(selectCondition, query, corpID);
	}
	public List dynamicDataDomesticAnalysis(String selectCondition, String query, String corpID) {
		
		return serviceOrderDao.dynamicDataDomesticAnalysis(selectCondition, query, corpID);
	}
	public List dynamicStorageAnalysis(String selectCondition, String query, String corpID) {
		
		return serviceOrderDao.dynamicStorageAnalysis(selectCondition, query, corpID);
	}
	public List goCustomerSO(String sequenceNumber, String corpID, Long id, String job){
		return serviceOrderDao.goCustomerSO(sequenceNumber, corpID, id, job);
	}
	public List goNextCustomerSO(String sequenceNumber, String corpID, Long id,String partnerCodePopup, String companydivisionSetCode){
		return serviceOrderDao.goNextCustomerSO(sequenceNumber, corpID, id ,partnerCodePopup,companydivisionSetCode);
	}
	public List goPrevCustomerSO(String sequenceNumber, String corpID, Long id,String partnerCodePopup, String companydivisionSetCode){
		return serviceOrderDao.goPrevCustomerSO(sequenceNumber, corpID, id,partnerCodePopup,companydivisionSetCode)	;
	}
	public List gettransferDateList(String corpID, Long soIdNum) {
		return serviceOrderDao.gettransferDateList(corpID, soIdNum);
	}
	public List getbillingList() {		
		return null;
	}
	public List getbillingdetails(String corpID, int year) {		
		return serviceOrderDao.getbillingdetails(corpID, year);
	}
	public List getbillingList(String corpIDParam,int month ,int year) {		
		return serviceOrderDao.getbillingList(corpIDParam ,month,year);
	}
	public List gettotalCountOfSo(String corpID, int year) {		
		return serviceOrderDao.gettotalCountOfSo(corpID, year);
	}
	public List getReciprocityAnalysisList(String corpID, String fromDates, String toDates, String jobtypeMultiple, String companyDivisionMultiple, Boolean militaryShipment, String subOADAReciprocityAnalysis) {
		
		return serviceOrderDao.getReciprocityAnalysisList(corpID, fromDates, toDates, jobtypeMultiple, companyDivisionMultiple, militaryShipment, subOADAReciprocityAnalysis);
	}
	public int updateWorkTicketOriginAddress(String originAddressLine1,String originAddressLine2,String originAddressLine3,String originCompany,String originCountry,String originState,String originCity,String originZip, String originPreferredContact, String originDayPhone,String originDayExtn,String originHomePhone,String originMobile,String originFax,String originContactName,String originContactWork,String shipNumber)
	{
		return serviceOrderDao.updateWorkTicketOriginAddress(originAddressLine1, originAddressLine2, originAddressLine3, originCompany, originCountry, originState, originCity, originZip, originPreferredContact, originDayPhone, originDayExtn, originHomePhone, originMobile, originFax,originContactName,originContactWork, shipNumber);
	}
	public int updateWorkTicketDestinationAddress(String destinationAddressLine1,String destinationAddressLine2,String destinationAddressLine3,String destinationCompany,String destinationCountry,String destinationState,String destinationCity,String destinationZip,String destPreferredContact, String destinationDayPhone,String destinationDayExtn,String destinationHomePhone,String destinationMobile,String destinationFax,String contactName,String contactPhone,String shipNumber)
	{
		return serviceOrderDao.updateWorkTicketDestinationAddress(destinationAddressLine1, destinationAddressLine2, destinationAddressLine3, destinationCompany, destinationCountry, destinationState, destinationCity, destinationZip,destPreferredContact, destinationDayPhone, destinationDayExtn, destinationHomePhone, destinationMobile, destinationFax,contactName,contactPhone, shipNumber);
	}
    public List usOriginGisOutput(String query, String corpID) {
		
		return serviceOrderDao.usOriginGisOutput(query, corpID);
	}
	
	public List usDestGisOutput(String query, String sessionCorpID){
		return serviceOrderDao.usDestGisOutput(query, sessionCorpID);
	}
	public int getDoNotCopyAuthorizationSO(String billToCode, String sessionCorpID) {
		return serviceOrderDao.getDoNotCopyAuthorizationSO(billToCode, sessionCorpID);
	}
	public List GSTStorageExtracts(String beginDate, String endDate, String sessionCorpID) {
		return serviceOrderDao.GSTStorageExtracts(beginDate, endDate, sessionCorpID);
	}
	public List stateStorageExtracts(String beginDate, String endDate, String sessionCorpID) {
		return serviceOrderDao.stateStorageExtracts(beginDate, endDate, sessionCorpID);
	}
	public List deptOfCommerce(String beginDate, String endDate, String sessionCorpID){
		return serviceOrderDao.deptOfCommerce(beginDate, endDate, sessionCorpID);
	}
	public List usAidStorageExtracts(String beginDate, String endDate, String sessionCorpID){
		return serviceOrderDao.usAidStorageExtracts(beginDate, endDate, sessionCorpID);
	}
	public double getOnhandOldvalue(long billingId) {
		return serviceOrderDao.getOnhandOldvalue(billingId);
	}
	public List getDeliveryReportList(String sessionCorpID) {		
		return serviceOrderDao.getDeliveryReportList(sessionCorpID);
	}
	public int updateSeviceOrdersentcomp(String shpnumber) {		
		return serviceOrderDao.updateSeviceOrdersentcomp(shpnumber);
	}
	public int getTotalUSDeliveries(String dashboardType) {
		return serviceOrderDao.getTotalUSDeliveries(dashboardType);
	}
	public List getUSDeliveriesList(String dashboardType) {
		return serviceOrderDao.getUSDeliveriesList(dashboardType);
	}
	public List getLonLat(String code, String parameter, String sessionCorpID) {
		return serviceOrderDao.getLonLat(code, parameter,sessionCorpID) ;
	}
	public List getZipLonLat(String sessionCorpID, String zipCode) {
		return serviceOrderDao.getZipLonLat(sessionCorpID, zipCode);
	}
	public List goNextCustomerSOByJob(String seqNm, String sessionCorpID, Long soIdNum){
		return serviceOrderDao.goNextCustomerSOByJob(seqNm, sessionCorpID, soIdNum);
	}
	public List goPrevCustomerSOByJob(String seqNm, String sessionCorpID, Long soIdNum){
	    return serviceOrderDao.goPrevCustomerSOByJob(seqNm, sessionCorpID, soIdNum);
	}
	public int updateWorkticketCoord(String coord,String shipNumber, String sessionCorpID){
		 return serviceOrderDao.updateWorkticketCoord(coord,shipNumber, sessionCorpID);
	}
	public int findBookingAgentcompanyDivision(String bookingAgentCode, String sessionCorpID) {
		return serviceOrderDao.findBookingAgentcompanyDivision(bookingAgentCode, sessionCorpID);
	}
	public List getLinkedShipNumber(String shipNumber, String recordType) {
		return serviceOrderDao.getLinkedShipNumber(shipNumber, recordType);
	}
	public Long findRemoteServiceOrder(String shipNumber) {
		return serviceOrderDao.findRemoteServiceOrder(shipNumber);
	}
	public int quoteAcceptanceUpdate(String soid, String requestedAQP, String sessionCorpID, String ruser){
		return serviceOrderDao.quoteAcceptanceUpdate(soid, requestedAQP, sessionCorpID, ruser);
	}
	public int quoteFileUpdate(String soid, String requestedAQP, String sessionCorpID, String remoteUser, Long customerFileId,String comptetive,Boolean checkAccessQuotation){
		return serviceOrderDao.quoteFileUpdate(soid, requestedAQP, sessionCorpID, remoteUser,customerFileId,comptetive,checkAccessQuotation);
	}
	public List findCompanyDivisionAcctgCodeUnique(String sessionCorpID) { 
		return serviceOrderDao.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
	}
	public List serviceOrderBySeqNumber(String sessionCorpID,String sequenceNumber, String controlFlag) {
		return serviceOrderDao.serviceOrderBySeqNumber(sessionCorpID, sequenceNumber,controlFlag);
	}
	public int updateCustomerfileQuotationStatus(Long customerFileId, String quotationStatus,String remoteUser,Long sid,String comptetive,Boolean checkAccessQuotation) {
		return serviceOrderDao.updateCustomerfileQuotationStatus(customerFileId, quotationStatus,remoteUser, sid,comptetive,checkAccessQuotation);
		
	}
	public int updateQuoteStatus(Long id, String status, String statusNumber,String quoteAccept,String remoteUser,String quoteStatus) {
		return serviceOrderDao.updateQuoteStatus(id, status, statusNumber,quoteAccept,remoteUser,quoteStatus);
		
	}
	public List USDAStorageExtracts(String beginDate, String endDate,String sessionCorpID){
		return serviceOrderDao.USDAStorageExtracts(beginDate, endDate, sessionCorpID);
	}
	public List getShipmentNumber(String sessionCorpID, String sequenceNumber){
		return serviceOrderDao.getShipmentNumber(sessionCorpID,sequenceNumber);
	}
	public List findGroupOrdersList(String groupSequenceNumber,String groupOriginCountry, String groupDestinationCountry, Date groupPreferredDelivery, String groupMode, String groupAgeStatus, String sessionCorpID,String firstName,String lastName){
		return serviceOrderDao.findGroupOrdersList(groupSequenceNumber,groupOriginCountry,  groupDestinationCountry, groupPreferredDelivery, groupMode, groupAgeStatus, sessionCorpID,firstName,lastName);
		
	}
	public List findGroupOrdersMap(String groupSequenceNumber,String groupOriginCountry, String groupDestinationCountry, Date groupPreferredDelivery, String groupMode, String groupAgeStatus, String sessionCorpID,String addressFlag){
		return serviceOrderDao.findGroupOrdersMap(groupSequenceNumber,groupOriginCountry, groupDestinationCountry, groupPreferredDelivery, groupMode, groupAgeStatus, sessionCorpID,addressFlag);
		
	}
	public List getModeFromChild(String idList,String corpid){
		return serviceOrderDao.getModeFromChild(idList, corpid);
	}
	public List weightOfOrdersToGrouped(String idList,String corpid){
		return serviceOrderDao.weightOfOrdersToGrouped(idList, corpid);
		
	}
	public void updateGroupAgeStatus(String newIdList,Long grpID, String grpStatus, String sessionCorpID){
		
		serviceOrderDao.updateGroupAgeStatus(newIdList, grpID,grpStatus, sessionCorpID);
	}
	public String findContainerSizeByMasterShip(String masterShipNo,String sessionCorpID){
		return serviceOrderDao.findContainerSizeByMasterShip(masterShipNo, sessionCorpID);
		
	}
	public List childServiceOrdersOfGroup(String sessionCorpId, Long id){
		return serviceOrderDao.childServiceOrdersOfGroup(sessionCorpId, id);
		
	}
	public String findShipNumberById(Long id){
		return serviceOrderDao.findShipNumberById(id);
	}
	public void finalizeMasterOrders(String oldIdList, String corpId){
		 serviceOrderDao.finalizeMasterOrders(oldIdList, corpId);
	}
	public List orderDetailsByGroupStatus(String groupSequenceNumber,String groupOriginCountry, String groupDestinationCountry, Date groupPreferredDelivery, String groupMode, String groupAgeStatus, String sessionCorpID){
		return serviceOrderDao.orderDetailsByGroupStatus(groupSequenceNumber,groupOriginCountry, groupDestinationCountry, groupPreferredDelivery, groupMode, groupAgeStatus, sessionCorpID);
	}
	public List<ServiceOrder> findServiceOrderByGrpId(String grpId, String sessionCorpId){
		return serviceOrderDao.findServiceOrderByGrpId(grpId, sessionCorpId);
	}
	public String getAllChildId(String grpId, String sessionCorpId){
		return serviceOrderDao.getAllChildId(grpId, sessionCorpId);
	}
	public List getWorkTicketList(Long id) {
		return serviceOrderDao.getWorkTicketList(id);
	}
	public List findPartnerCode(String sessionCorpID,String billToCode){
		return serviceOrderDao.findPartnerCode(sessionCorpID,billToCode);
	}
	public List findValuationList(String sequenceNumber,String shipNumber){
		return serviceOrderDao.findValuationList(sequenceNumber, shipNumber);
	}
	public List findPackingList(String sequenceNumber,String shipNumber){
		return serviceOrderDao.findPackingList(sequenceNumber, shipNumber);
	}
	/*public List findPackingContList(String sequenceNumber,String shipNumber){
		return serviceOrderDao.findPackingContList(sequenceNumber, shipNumber);
	}
	public List findPackingUnpackList(String sequenceNumber,String shipNumber){
		return serviceOrderDao.findPackingUnpackList(sequenceNumber, shipNumber);
	}*/
	public List findUnderLyingHaulList(String sequenceNumber,String shipNumber){
		return serviceOrderDao.findUnderLyingHaulList(sequenceNumber, shipNumber);
	}
	public List findOtherMiscellaneousList(String sequenceNumber,String shipNumber){
		return serviceOrderDao.findOtherMiscellaneousList(sequenceNumber, shipNumber);
	}
	public List findPackingSumList(String sequenceNumber,String shipNumber){
		return serviceOrderDao.findPackingSumList(sequenceNumber, shipNumber);
	}
	public List findContainerSumList(String sequenceNumber,String shipNumber){
		return serviceOrderDao.findContainerSumList(sequenceNumber, shipNumber);
	}
	public List findUnpackingSumList(String sequenceNumber,String shipNumber){
		return serviceOrderDao.findUnpackingSumList(sequenceNumber, shipNumber);
	}
	public List findTransportationSumList(String sequenceNumber,String shipNumber)
	{
		return serviceOrderDao.findTransportationSumList(sequenceNumber, shipNumber);
	}
	public List findOthersSumList(String sequenceNumber,String shipNumber){
		return serviceOrderDao.findOthersSumList(sequenceNumber, shipNumber);
	}
	public List findValuationSumList(String sequenceNumber,String shipNumber){
		return serviceOrderDao.findValuationSumList(sequenceNumber, shipNumber);
	}
	public String companyDivOfOrdersToGrouped(String idList,String corpid){
		return serviceOrderDao.companyDivOfOrdersToGrouped(idList, corpid);
	}
	public String billToCodOfOrdersToGrouped(String companyDiv,String corpid){
		return serviceOrderDao.billToCodOfOrdersToGrouped(companyDiv, corpid);
	}
	public List checkById(Long id){
		return serviceOrderDao.checkById(id);
	}
	public String findRemoteMasterServiceOrderId(Long childId,String sessionCorpId){
		return serviceOrderDao.findRemoteMasterServiceOrderId(childId, sessionCorpId);
	}
	public String billToNameOfOrdersToGrouped(String partnerCode,String corpid){
		return serviceOrderDao.billToNameOfOrdersToGrouped(partnerCode, corpid);
	}
	public int updateEstStatus(String estStatus,Long ids,String userName){
		return serviceOrderDao.updateEstStatus(estStatus, ids,userName);
	}
	public List findComponentMillitaryShipment(String sessionCorpID) {
	        return serviceOrderDao.findComponentMillitaryShipment(sessionCorpID);
	}
	public int quoteStatusReasonUpdate(String soid, String requestedAQP, String sessionCorpID, String ruser,String qStatus){
		return serviceOrderDao.quoteStatusReasonUpdate(soid, requestedAQP, sessionCorpID, ruser,qStatus);
	}
	public List findWesiteAddress(String sessionCorpID){
		return serviceOrderDao.findWesiteAddress(sessionCorpID);
	}
	public List findSummryViewList(String ship){
		return serviceOrderDao.findSummryViewList(ship);
	}
	public List findSummryViewCarrierList(String ship){
		return serviceOrderDao.findSummryViewCarrierList(ship);
	}
	public List findmiscVlSystemDefaultUTSI(String corpID) {
		return serviceOrderDao.findmiscVlSystemDefault(corpID);
	}
	public List findSummaryReloServicesList(Long id, String services, String sessionCorpID){
		return serviceOrderDao.findSummaryReloServicesList(id, services, sessionCorpID);
	}
	public BigDecimal findTotalExpenseOfChargeACC(String chargeCodeCom1,String sessionCorpID, String shipNumber,String compDiv){
		return serviceOrderDao.findTotalExpenseOfChargeACC(chargeCodeCom1, sessionCorpID, shipNumber, compDiv);
	}
	public List partnerBillingEmail(String BilltoCode){
		return serviceOrderDao.partnerBillingEmail(BilltoCode);
	}
	public List findExternalMaximumLineNumber(String shipNumber, String externalCorpID) {
		return serviceOrderDao.findExternalMaximumLineNumber(shipNumber, externalCorpID);
	}
	public List findAccountlines(Long sid){
		return serviceOrderDao.findAccountlines(sid); 
	}
	public void updateSetteledDateOfToCharge(String chargeCode, Long sid){
		serviceOrderDao.updateSetteledDateOfToCharge(chargeCode, sid);
	}
	public List getTPChargeDetail(Long sid,String job,String compDiv,String chargeCode,String sessionCorpID,String aidList){
		return serviceOrderDao.getTPChargeDetail(sid,job,compDiv, chargeCode, sessionCorpID,aidList);
	}
	public List getAllTPAccId(String chargeCode1, String sessionCorpID, Long sid,String aidList){
		return serviceOrderDao.getAllTPAccId(chargeCode1, sessionCorpID, sid,aidList);
	}
	public List <Charges> getCommissionableValue(String contact, String chargeCode,String sessionCorpID,String job){
		return serviceOrderDao.getCommissionableValue(contact, chargeCode, sessionCorpID,job);
	}
	public List getParentAgentCodeFormUser(String estimator){
		return serviceOrderDao.getParentAgentCodeFormUser(estimator);
	}
	public List getPartnerDetails(String vendorCode, String sessionCorpID){
		return serviceOrderDao.getPartnerDetails(vendorCode,sessionCorpID);
	}
	public List getRateGirdForComm(String charge, String contract,String sessionCorpID){
		return serviceOrderDao.getRateGirdForComm(charge, contract, sessionCorpID);
	}

	public String getAgentContractData(String externalCorpID, String bookingAgentCode, String sessionCorpID, String contract) {
		return serviceOrderDao.getAgentContractData(externalCorpID, bookingAgentCode,  sessionCorpID, contract);
	}
	public String getBillingCurrencyValue(String billToCode,String sessionCorpID){
		return serviceOrderDao.getBillingCurrencyValue(billToCode, sessionCorpID);
	}
	public String findPayRecgl(String charge, String sessionCorpID,String contract){
		return serviceOrderDao.findPayRecgl(charge, sessionCorpID, contract);
	}
	public List findAccountLineByCharge(String charge, String sessionCorpID,String shipNumber,String compDiv){
		return serviceOrderDao.findAccountLineByCharge(charge, sessionCorpID, shipNumber , compDiv);
	}
	public List getInternalCostContract(String compDiv, String sessionCorpID, String job, String chargeCodeCom1){
		return serviceOrderDao.getInternalCostContract(compDiv, sessionCorpID, job, chargeCodeCom1);
	}
	public String getRatingOption(String billTocode,String corpID){
		return serviceOrderDao.getRatingOption(billTocode, corpID);
	}
	public String getCustomerFeedBackOption(String billTocode,String corpID){
		return serviceOrderDao.getCustomerFeedBackOption(billTocode, corpID);
	}
	public Boolean getUsaFlagCarrier(String flagBillTocode ,String corpID){
		return serviceOrderDao.getUsaFlagCarrier(flagBillTocode,corpID);
	}
	
	public List findAddressList(String shipNumber, String sessionCorpID){
		return serviceOrderDao.findAddressList(shipNumber,sessionCorpID);	
	}
	public List findCustomerResourceMgmtDoc(String Ship,String sessionCorpID,String usertype){
		return serviceOrderDao.findCustomerResourceMgmtDoc(Ship,sessionCorpID,usertype);
	}
	public List getCommissionableJobs(String sessionCorpID){
		return serviceOrderDao.getCommissionableJobs(sessionCorpID);
	}
	public String findByBookingLastName(String partnerCode, String corpid) {
		return serviceOrderDao.findByBookingLastName(partnerCode, corpid);
	}
	public List findBookingAgentShipNumber(String bookingAgentShipNumber){
		return serviceOrderDao.findBookingAgentShipNumber(bookingAgentShipNumber);
	}
	public List getChildServiceOrdersOfGroup(Long grpId) {
		return serviceOrderDao.getChildServiceOrdersOfGroup(grpId);
		 
	}
	public String getUTSIContractData(String bookingAgentCode, String sessionCorpID, String contract) {
		return serviceOrderDao.getUTSIContractData(bookingAgentCode, sessionCorpID, contract);
	}
	public List<ServiceOrder> findServiceOrderByDateRange(Date date,String corpId){
		return serviceOrderDao.findServiceOrderByDateRange( date, corpId);
	}
	public List getRevenueReciprocityAnalysisList(String corpID, String fromDates, String toDates, String jobtypeMultiple, String companyDivisionMultiple, Boolean militaryShipment, String subOADAReciprocityAnalysis, Boolean projectedRevenue,Boolean includeChildCorpids, String childCompanyMultiple) {
		return serviceOrderDao.getRevenueReciprocityAnalysisList(corpID, fromDates, toDates, jobtypeMultiple, companyDivisionMultiple, militaryShipment,subOADAReciprocityAnalysis,projectedRevenue, includeChildCorpids,  childCompanyMultiple);
	}
	public List getGivenReciprocityDetailReportList(String corpID, String fromDates, String toDates, String jobtypeMultiple, String companyDivisionMultiple, Boolean militaryShipment, String agent, String subOADAReciprocityAnalysis, Boolean projectedRevenue,Boolean includeChildCorpids, String childCompanyMultiple) {
		return serviceOrderDao.getGivenReciprocityDetailReportList(corpID, fromDates, toDates, jobtypeMultiple, companyDivisionMultiple, militaryShipment, agent, subOADAReciprocityAnalysis, projectedRevenue, includeChildCorpids,  childCompanyMultiple);
	}
	public List getrecvReciprocityDetailReportList(String corpID, String fromDates, String toDates, String jobtypeMultiple, String companyDivisionMultiple, Boolean militaryShipment, String agent, String subOADAReciprocityAnalysis, Boolean projectedRevenue,Boolean includeChildCorpids,String childCompanyMultiple) {
		return serviceOrderDao.getrecvReciprocityDetailReportList(corpID, fromDates, toDates, jobtypeMultiple, companyDivisionMultiple, militaryShipment, agent,  subOADAReciprocityAnalysis,  projectedRevenue, includeChildCorpids,childCompanyMultiple);
	}
	public List countByGeneratedInvoice(String shipNumber, String corpId){
		return serviceOrderDao.countByGeneratedInvoice(shipNumber,corpId);
	}
	public List getReciprocityDetailReportList(String corpID, String fromDates, String toDates, String jobtypeMultiple, String companyDivisionMultiple, Boolean militaryShipment, String agent, String subOADAReciprocityAnalysis, Boolean projectedRevenue,Boolean includeChildCorpids,String childCompanyMultiple){
		return serviceOrderDao.getReciprocityDetailReportList(corpID, fromDates, toDates, jobtypeMultiple, companyDivisionMultiple, militaryShipment, agent, subOADAReciprocityAnalysis, projectedRevenue, includeChildCorpids, childCompanyMultiple);
	}
	public Date findByProperty(Long id,String... name) {
		return serviceOrderDao.findByProperty( id, name);
	}
	public List getChargeDetail(Long sid,String sessionCorpID){
		return serviceOrderDao.getChargeDetail(sid, sessionCorpID);
	}
	public List getAccLineAcComDiv(Long sid,String sessionCorpID,String compDiv){
		return serviceOrderDao.getAccLineAcComDiv(sid, sessionCorpID, compDiv);
	}
	public Map<String,String> getCommMap(Long sid,String sessionCorpID){
		return serviceOrderDao.getCommMap(sid, sessionCorpID);
	}
	public BigDecimal findCommissionDifference(Long sid,String sessionCorpID,BigDecimal commAmt,String type,String companyDiv){
		return serviceOrderDao.findCommissionDifference(sid, sessionCorpID, commAmt, type,companyDiv);
	}
	public BigDecimal findCommissionDifferenceSenToAcc(Long sid,String sessionCorpID,BigDecimal commAmt,String type,String companyDiv){
		return serviceOrderDao.findCommissionDifferenceSenToAcc(sid, sessionCorpID, commAmt, type, companyDiv);
	}
	public List checkSentToAccLineInSoList(String sessionCorpID,Long sid,String chargeCode,String companyDiv){
		return serviceOrderDao.checkSentToAccLineInSoList(sessionCorpID, sid, chargeCode, companyDiv);
	}
	public String getSentAccIdList(Long sid,String sessionCorpID,String type,String companyDiv){
		return serviceOrderDao.getSentAccIdList(sid, sessionCorpID, type, companyDiv);
	}
	public List getConsultantAccLine(Long sid,String sessionCorpID,String chargeCode,String companyDiv){
		return serviceOrderDao.getConsultantAccLine(sid, sessionCorpID, chargeCode, companyDiv);
	}
	public List getCommissionableChargeGrossMarginPersentage(Long sid,String sessionCorpID){
		return serviceOrderDao.getCommissionableChargeGrossMarginPersentage(sid, sessionCorpID);
	}
	public List findCompanyDivisionBookAgent(String sessionCorpID) {
		return serviceOrderDao.findCompanyDivisionBookAgent(sessionCorpID);
	}
	public List findExternalSOList(String sessionCorpID,String shipNumValue,String bookAgentShipNumValue,String regNumValue,String lNameValue,String fNameValue,String statusValue,Date statusDate,String jobValue,String billToNameValue,String coordinatorValue,String gblValue,String sSNumValue,String serviceOrderSearchVal,Boolean activeValue,String compDivValue,String moveType,String cityCountryZipOption,String originCityOrZip,String destinationCityOrZip,String originCountry,String destinationCountry){
		return serviceOrderDao.findExternalSOList(sessionCorpID,shipNumValue,bookAgentShipNumValue,regNumValue,lNameValue,fNameValue,statusValue,statusDate,jobValue,billToNameValue,coordinatorValue,gblValue,sSNumValue,serviceOrderSearchVal,activeValue,compDivValue,moveType,cityCountryZipOption,originCityOrZip,destinationCityOrZip,originCountry,destinationCountry);
	}
	public List findRevenueTrackerList(String sessionCorpID, String job, String companyDivision,String billingPerson, String billToName, Date billingRecogDate,Date loadAfterDate,Date delAfterDate,Date serviceCompDate,Boolean billingComplete,String showRecord,String reviewStatus){
		return serviceOrderDao.findRevenueTrackerList(sessionCorpID, job ,companyDivision,billingPerson, billToName, billingRecogDate, loadAfterDate, delAfterDate,serviceCompDate,billingComplete,showRecord, reviewStatus);
	}
	public List findSOListForExtract(String sessionCorpID){
		return serviceOrderDao.findSOListForExtract(sessionCorpID);
	}
	public List findDecaSOListForExtract(String sessionCorpID,String sDate,String eDate){
		return serviceOrderDao.findDecaSOListForExtract(sessionCorpID,sDate, eDate);
	}
	public List getPayableTrackerList(String sessionCorpID, String job, String companyDivision, String venderName,String payableTrackBillToName,Date loadAfterDate,Date delAfterDate,Date serviceCompDate,String payablePersonName, String showRecord,Date fileCretedOn,Boolean decomuntCount){
		return serviceOrderDao.getPayableTrackerList(sessionCorpID,job,companyDivision, venderName,payableTrackBillToName, loadAfterDate, delAfterDate, serviceCompDate, payablePersonName,  showRecord ,fileCretedOn,decomuntCount);
	}
	public void updateIsSOExtract(String ship){
		 serviceOrderDao.updateIsSOExtract(ship);
	}
	public List getPricPointDetails(String shipNumber,String sessionCorpID){
		return serviceOrderDao.getPricPointDetails(shipNumber,sessionCorpID);
	}
	public Map<String,String> getMarketArea(String marketCountryCode){
		return serviceOrderDao.getMarketArea(marketCountryCode);
	}
	public Map findMarketPlace(String marketCountryCode){
		return serviceOrderDao.findMarketPlace(marketCountryCode);
	}
	public void tariffIdUpdate(Long AccId,String priceId , String sessionCorpID){
		 serviceOrderDao.tariffIdUpdate(AccId,priceId ,sessionCorpID);
	}
	public List findTariffIdDetails(String shipNumber,String sessionCorpID){
		return serviceOrderDao.findTariffIdDetails(shipNumber,sessionCorpID);
	}
	public List getPricingDetails(Map marketplaceMap,String pricingWeight,String pricingVolume,String pricingMode ,String sessionCorpID,String pricingStorageUnit,String marketArea ,String corpIdBaseCurrency,String baseCurrencySysDefault){
		return serviceOrderDao.getPricingDetails(marketplaceMap,pricingWeight,pricingVolume,pricingMode,sessionCorpID,pricingStorageUnit,marketArea ,corpIdBaseCurrency,baseCurrencySysDefault);
	}
	public List findPriceCode(String pricingMode,String parameter,String sessionCorpID){
		return serviceOrderDao.findPriceCode(pricingMode, parameter, sessionCorpID);
	}
	public Map<String,String> getInclusionExlusionlist(String job,String mode,String routing,String sessionCorpID,String lang,String companyDivision,String commodity,String packingMode,String originCountry,String destinationCountry,String serviceIncExc,String contractIncExc){
		return serviceOrderDao.getInclusionExlusionlist(job, mode, routing, sessionCorpID, lang, companyDivision, commodity, packingMode, originCountry, destinationCountry,serviceIncExc,contractIncExc);
	}
	public Map<String,String> getSingleSectionInclusionExlusionlist(String job,String mode,String routing,String sessionCorpID,String lang,String companyDivision,String commodity,String packingMode,String originCountry,String destinationCountry,String serviceIncExc,String contractIncExc){
		return serviceOrderDao.getSingleSectionInclusionExlusionlist(job, mode, routing, sessionCorpID, lang, companyDivision, commodity, packingMode, originCountry, destinationCountry, serviceIncExc, contractIncExc);
	}
	public List<ServiceOrder> getCurrentDetail(Long soid) {
		return serviceOrderDao.getCurrentDetail(soid);
	}
	public void linkedQuoteFileUpdate(Long id, String requestedAQP, String corpID, String remoteUser, Long customerFileId, String comptetive, boolean checkAccessQuotation,String sessionCorpID) {
		serviceOrderDao.linkedQuoteFileUpdate(id, requestedAQP, corpID, remoteUser, customerFileId, comptetive, checkAccessQuotation,sessionCorpID);
		
	}
	public void linkedUpdateQuoteStatus(Long id, String status, String statusNumber, String quoteAccept, String remoteUser, String quoteStatus,String sessionCorpID) {
		serviceOrderDao.linkedUpdateQuoteStatus(id, status, statusNumber, quoteAccept, remoteUser, quoteStatus,sessionCorpID);
		
	}
	public void updateAgentCustomerfileQuotationStatus(Long customerFileId, String quotationStatus, String remoteUser, Long sid, String comptetive, Boolean checkAccessQuotation,String sessionCorpID) {
		serviceOrderDao.updateAgentCustomerfileQuotationStatus(customerFileId, quotationStatus, remoteUser, sid, comptetive, checkAccessQuotation,sessionCorpID);
	}
	public int updateAllQuoteStatus(String allIdOFSo, String status, String statusNumber,String quoteAccept,String remoteUser,String quoteStatus){
		return serviceOrderDao.updateAllQuoteStatus(allIdOFSo, status, statusNumber, quoteAccept, remoteUser,quoteStatus);
	}
	public Map<String,String> checkShipEntryInCommission(String sessionCorpID,String type,String shipNumber){
		return serviceOrderDao.checkShipEntryInCommission(sessionCorpID, type,shipNumber);
	}
	public String findAccIdNeedToBlock(Long sid,String sessionCorpID){
		return serviceOrderDao.findAccIdNeedToBlock(sid, sessionCorpID);
	}
	public String getRatingOptionForCMMDMM(String shipNumber,String corpIdd){
		return serviceOrderDao.getRatingOptionForCMMDMM(shipNumber, corpIdd);
	}
	public String findToolTip(String username, String corpID){
		return serviceOrderDao.findToolTip( username, corpID);
	}
	public void autoSaveIncusionExclusionAjax(String sessionCorpID,String incExcServiceType,Long serviceId){
		serviceOrderDao.autoSaveIncusionExclusionAjax(sessionCorpID, incExcServiceType, serviceId);
	}
	public Map<String, String> getBillingCurrencyMap(String sessionCorpID) {
		return  serviceOrderDao.getBillingCurrencyMap(sessionCorpID);
	}
	public Map<String, TrackingStatusView> getLinkedShipnumberMap(String sessionCorpID, String condition) {
		return  serviceOrderDao.getLinkedShipnumberMap(sessionCorpID, condition);
	}
	public Map<Long, ServiceOrder> getServiceOrderObjectMap( Set serviceorderIdSet) {
		return  serviceOrderDao.getServiceOrderObjectMap(serviceorderIdSet);
	}
	public Map<String, Object> getObjectMap(Set serviceorderIdSet){
		return  serviceOrderDao.getObjectMap(serviceorderIdSet);
	}
	public Map<String, BillingView> getLinkedBillingMap(String sessionCorpID, String condition) {
		return  serviceOrderDao.getLinkedBillingMap(sessionCorpID, condition);
	}
	public Map<String, String> companyDivisionAcctgCodeUniqueMap() {
		return  serviceOrderDao.companyDivisionAcctgCodeUniqueMap();
	}
	public Map<String, String> getGlcodeGridMap() {
		return  serviceOrderDao.getGlcodeGridMap();
	}
	public Map<String, String> getGlcodeCostelementMap() {
		return  serviceOrderDao.getGlcodeCostelementMap();
	}
	public ServiceOrder findServiceOrderObj(Long id, String corpID){
		return  serviceOrderDao.findServiceOrderObj(id,corpID);
	}
	public String findNetworkEmail(String partnerCode, String sessionCorpID) {
		return serviceOrderDao.findNetworkEmail(partnerCode, sessionCorpID);
	}
	public List getNewRevenueReciprocityAnalysisList(String corpID, String fromDates, String toDates, String jobtypeMultiple, String companyDivisionMultiple, Boolean militaryShipment, Boolean projectedRevenue) {
		return serviceOrderDao.getNewRevenueReciprocityAnalysisList(corpID, fromDates, toDates, jobtypeMultiple, companyDivisionMultiple, militaryShipment, projectedRevenue);
	}
	public List getGivenNewReciprocityDetailReportList(String corpID, String fromDates, String toDates, String jobtypeMultiple, String companyDivisionMultiple, Boolean militaryShipment, String agent, Boolean projectedRevenue) {
		return serviceOrderDao.getGivenNewReciprocityDetailReportList(corpID, fromDates, toDates, jobtypeMultiple, companyDivisionMultiple, militaryShipment, agent, projectedRevenue);
	}
	public List getrecvNewReciprocityDetailReportList(String corpID, String fromDates, String toDates, String jobtypeMultiple, String companyDivisionMultiple, Boolean militaryShipment, String agent, Boolean projectedRevenue) {
		return serviceOrderDao.getrecvNewReciprocityDetailReportList(corpID, fromDates, toDates, jobtypeMultiple, companyDivisionMultiple, militaryShipment, agent, projectedRevenue);
	}
	public List getNewReciprocityDetailReportList(String corpID, String fromDates, String toDates, String jobtypeMultiple, 	String companyDivisionMultiple, Boolean militaryShipment, String agent, Boolean projectedRevenue) {
		return serviceOrderDao.getNewReciprocityDetailReportList(corpID, fromDates, toDates, jobtypeMultiple, companyDivisionMultiple, militaryShipment,agent, projectedRevenue);
	}
	public void updateAccCompanyDivision(Long id, String companyDivision, String oldSOCompanyDivision,String updatedBy) {
		serviceOrderDao.updateAccCompanyDivision(id, companyDivision, oldSOCompanyDivision,updatedBy);
	}
	public void updateSaveContractChangeChargeAjax(String sessionCorpID,Long serviceId,String billingContract,String shipNumber,Long id){
		serviceOrderDao.updateSaveContractChangeChargeAjax(sessionCorpID, serviceId, billingContract,shipNumber,id);
	}
	public List findChildCompanyList(String sessionCorpID) {
		return serviceOrderDao.findChildCompanyList(sessionCorpID);
	}	
	public void updateAccrualFields(Long id, Date date1, String userName,String sessionCorpID,String roleSupervisor, String accrualDateDel){
		serviceOrderDao.updateAccrualFields(id, date1, userName, sessionCorpID,roleSupervisor, accrualDateDel);
	}	
	public String JobForCoordinator(String shipnumber,String sessionCorpID){
		return serviceOrderDao.JobForCoordinator(shipnumber,sessionCorpID);
	}
	public List findClosedCompanyDivision(String sessionCorpID){
		return serviceOrderDao.findClosedCompanyDivision(sessionCorpID);
	}
	public List getIncExcDocList(String job,String mode,String routing,String sessionCorpID,String lang,String companyDivision,String commodity,String packingMode,String originCountry,String destinationCountry){
		return serviceOrderDao.getIncExcDocList(job, mode, routing, sessionCorpID, lang, companyDivision, commodity, packingMode, originCountry, destinationCountry);
	}
	public void autoSaveIncusionExclusionDoc(String sessionCorpID,String incExcServiceType,Long serviceId){
		serviceOrderDao.autoSaveIncusionExclusionDoc(sessionCorpID,incExcServiceType,serviceId);
	}
	public List getIncExcDocListDefaultTrue(String job,String mode,String routing,String sessionCorpID,String lang,String companyDivision,String commodity,String packingMode,String originCountry,String destinationCountry){
		return serviceOrderDao.getIncExcDocListDefaultTrue(job, mode, routing, sessionCorpID, lang, companyDivision, commodity, packingMode, originCountry, destinationCountry);
	}
	public List getRevenueReciprocityAnalysisCSVList(String corpID, String fromDates, String toDates, String jobtypeMultiple, String companyDivisionMultiple, Boolean militaryShipment, String subOADAReciprocityAnalysis, Boolean projectedRevenue, Boolean includeChildCorpids, String childCompanyMultiple) {
		return serviceOrderDao.getRevenueReciprocityAnalysisCSVList(corpID, fromDates, toDates, jobtypeMultiple, companyDivisionMultiple, militaryShipment, subOADAReciprocityAnalysis, projectedRevenue, includeChildCorpids, childCompanyMultiple);
	}
	public List getPostMoveSurvey(String corpId,Long id){
		return serviceOrderDao.getPostMoveSurvey(corpId, id);
	}
	public List findAvailableAccrueList(String corpId){
		return serviceOrderDao.findAvailableAccrueList(corpId);
	}
	public void updateSoAccrueChecked(String sidList, String corpId, String userName){
		serviceOrderDao.updateSoAccrueChecked(sidList,corpId,userName);
	}
	
	public List getTasksCheckList(String shipnumber){
		return serviceOrderDao.getTasksCheckList(shipnumber);
	}
	
	public String valueOfControlField(String controlDate, Long id)
	{
		return serviceOrderDao.valueOfControlField(controlDate,id);
	}
	public List baseCurrencyCompanyDivision(String companyDivision, String corpId){
		return serviceOrderDao.baseCurrencyCompanyDivision(companyDivision, corpId);
	}
	public List getAccExchangeRateList(String baseCurrencyCompanyDivision, String corpId){
		return serviceOrderDao.getAccExchangeRateList(baseCurrencyCompanyDivision, corpId);
	}
	public void disableReloService(String forDesible, String shipNumber){
		serviceOrderDao.disableReloService(forDesible, shipNumber);
	}
	public void enableReloService(String forUpdateAccountLineForReloTrue, String shipNumber){
		serviceOrderDao.enableReloService(forUpdateAccountLineForReloTrue, shipNumber);
	}
	public Map<String,String> findChargeCode(String shipNumber){
		return serviceOrderDao.findChargeCode(shipNumber);
	}
	/*public List<String> findChargeCode(String shipNumber){
		return serviceOrderDao.findChargeCode(shipNumber);
	}*/
	public List<Charges> getChargeLists(String sessionCorpID, String contractsType, String charge){
		return serviceOrderDao.getChargeLists(sessionCorpID, contractsType, charge);
	}
	public List findServiceBaseContractList(String contracts, String corpId){
		return serviceOrderDao.findServiceBaseContractList(contracts, corpId);
	}
	public String findPersonNameFromRefJob(String job,String compDivision,String corpId, String fieldName){
		return serviceOrderDao.findPersonNameFromRefJob(job,compDivision,corpId,fieldName);
	}
	public void updateEstimatedOverAllMarkUpValue(Integer markupVal,String shipNumber, String sessionCorpID, String userName){
		serviceOrderDao.updateEstimatedOverAllMarkUpValue(markupVal,shipNumber,sessionCorpID, userName);
	}
	
	public List dynamicReloServices(String saveSelectCondition, String string, String sessionCorpID){
		return serviceOrderDao.dynamicReloServices(saveSelectCondition, string, sessionCorpID);
	}
	
	public List dynamicStorageAnalysisForRelo(String selectQuery,String query, String sessionCorpID){
		return serviceOrderDao.dynamicStorageAnalysisForRelo(selectQuery, query, sessionCorpID);
	}
	public int getCompensationYear(String cfAccountCode, String sessionCorpID) {
		return serviceOrderDao.getCompensationYear(cfAccountCode, sessionCorpID);
	}
	public double getCompensationPercentage(String contract,int compensationYear, String sessionCorpID) {
		return serviceOrderDao.getCompensationPercentage(contract, compensationYear, sessionCorpID);
	}
	public List getMaxClientId(String sessionCorpID){
		return serviceOrderDao.getMaxClientId(sessionCorpID);
	}
	public String findOtherShipNumberInvoicedLines(String sequenceNumber, String shipNumber, String contractType, String corpId){
		return serviceOrderDao.findOtherShipNumberInvoicedLines(sequenceNumber, shipNumber, contractType, corpId);
	}
	public List findTotalRevenueExpense(String sessionCorpID, String RecInvoiceNumber) {
		
		return serviceOrderDao.findTotalRevenueExpense(sessionCorpID, RecInvoiceNumber);
	}
	public int getServiceOrdersSize(Long cid, String sessionCorpID){
		return serviceOrderDao.getServiceOrdersSize(cid,sessionCorpID);
	}
	public void updateWelcomeMailSentDate(String shipNumber, String fieldName){
		serviceOrderDao.updateWelcomeMailSentDate(shipNumber, fieldName);
	}
	public List findsurveyScheduleList(String estimator,String surveyDate,String consultantTime,String consultantTime2,String corpID)
	{
		return serviceOrderDao.findsurveyScheduleList(estimator,surveyDate,consultantTime,consultantTime2,corpID);
	}
	public List surveySchedule(String job, String corpID,String DateInterval,String fromDate,String toDate,String surveyTime,String surveyTime2,String consultant)
	{
		return serviceOrderDao.surveySchedule(job, corpID, DateInterval, fromDate,toDate, surveyTime, surveyTime2, consultant);
	
	}
	//13674 - Create dashboard
	public List getWorkTickestList(Long id ,String corpID) {
		return serviceOrderDao.getWorkTicketsList(id, corpID);
	}
	public String findRegNumber(String corpId,Long id){
		return serviceOrderDao.findRegNumber(corpId, id);
	}
	public String findAccIdNeedToBlockCommissionCloseDate(Long sid, String sessionCorpID, String commissionCloseDateFinal) {
		return serviceOrderDao.findAccIdNeedToBlockCommissionCloseDate(sid, sessionCorpID, commissionCloseDateFinal);
	}
	public BigDecimal findTotalExpenseOfConsultantAmount(String sessionCorpID, String shipNumber, String aidList,String compDiv) {
		return serviceOrderDao.findTotalExpenseOfConsultantAmount(sessionCorpID, shipNumber, aidList,compDiv);
	}
	public BigDecimal findTotalExpenseOfSalesPersonAmount(String sessionCorpID, String shipNumber, String aidList,String compDiv) {
		return serviceOrderDao.findTotalExpenseOfSalesPersonAmount(sessionCorpID, shipNumber, aidList,compDiv);
	}

} 

//End of Class.   