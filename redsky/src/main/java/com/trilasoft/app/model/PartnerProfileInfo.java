package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;






@Entity
@Table(name="partnerprofileinfo")
public class PartnerProfileInfo extends BaseObject {
	
	
	private Long id;
	private String partnerCode;
	private Long partnerPrivateId;
	private String corpId;
	private Boolean dom;
	private Boolean intl;
	private Boolean rlo;
	private Boolean com;
	private Boolean oth;
	private String othComnts;
	private String agentCompanyName;
	private String agentContact;
	private String agentPhone;
	private String agentEmail;
	private String auditfirmCompanyName;
	private String auditfirmContact;
	private String auditfirmPhone;
	private String auditfirmEmail;
	private String hhgProviderDomCompanyName;
	private String hhgProviderDomContact;
	private String hhgProviderDomPhone;
	private String hhgProvideDomEmail;
	private String hhgProviderIntlCompanyName;
	private String hhgProviderIntlContact;
	private String hhgProviderIntlPhone;
	private String hhgProvideIntlEmail;
	private String variousContacts;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String hhgProviderIntlCompanyName2;
	private String hhgProviderIntlContact2;
	private String hhgProviderIntlPhone2;
	private String hhgProvideIntlEmail2;
	private String hhgProviderIntlCompanyName3;
	private String hhgProviderIntlContact3;
	private String hhgProviderIntlPhone3;
	private String hhgProvideIntlEmail3;
	private String hhgProviderIntlCompanyName4;
	private String hhgProviderIntlContact4;
	private String hhgProviderIntlPhone4;
	private String hhgProvideIntlEmail4;
	private String agent;
	private String superGroupName;
	private String customer;
	private String superGroup; 
	private String username;
	private String password;
	private String mPointOfContact;
	private String intlTermsPhone;
	private String intlTermsEmail;
	private Boolean contractInPlaceY ;
	private Date startDate;
	private String lnthOfContract ;
	private Boolean indRequestforBids;
	private Boolean cld;
	private Boolean rloProviders;
	private Boolean ts;
	private Boolean rap;
	private String fixPricingforOrigOrDest;
	private Boolean air;
	private Boolean sea;
	private Boolean permStorage;
	private String sit;
	private Boolean single;
	private Boolean married;
	private Boolean marriedWithChild;
	private String seaAllowanceMatrix;
	private Boolean contractInPlaceN ;
	private String airDesc;
	private String seaDesc;
	private String singleDesc;
	private String marriedDesc;
	private String marriedWithChildDesc;
	private String accountPointOfContact;
	private String accountEmail;
	private String accountPhone;
	private String rloCompanyInvolved;
	private String rloCompanyInvolvedPhone;
	private String mainContact;
	private String mainContactEmail;
	private Boolean billDeliveryPreferenceMail;
	private Boolean billDeliveryPreferenceEmail;
	private Boolean billDeliveryPreferenceEDI;
	private String billingCutOff;
	private Integer billingCutOffDays;
	private Boolean daysfromDelivery;
	private Boolean supplimentalBillingAllowed;
	private Boolean approvalRequired;
	private String billingContact;
	private String billingContactEmail;
	private String billingContactPhone;
	private Boolean access;
	private Boolean billOfLading;
	private Boolean cubeSheet;
	private Boolean airwaysBill;
	private Boolean rated;
	private Boolean unrated;
	private Boolean proofOfPmt;
	private Boolean approvalRequiredRated;
	private Boolean approvalRequiredUnrated;
	private Boolean approvalRequiredProofOfPmt;
	private Boolean excApprovals;
	private Boolean billingOthers;
	private Boolean tarrifRateSheet;
	private Boolean auth;
	private Boolean ops;
	private Boolean insp;
	private Boolean nvtd;
	private Boolean nvtv;
	private Boolean wtct;
	private Boolean officialReFAdditional;
	private Boolean crateList;
	private Boolean thirdPartyDocuments;
	private Boolean backUp;
	private String frieghtInvoice;
	private String specialInvoiceInstructions;
	private String  pathforPricing;
	private Boolean contratedYes;
	private Boolean contratedNo;
	private Boolean triRegional;
	private Boolean indBids;
	private Boolean costPls;
	private Boolean rateMatrix;
	private Boolean onlineRateFill;
	private Boolean pricingOther;
	private String  pricingOtherDesc;
	private String  triRegionalModelDesc;
	private Boolean oA;
	private Boolean dA;
	private Boolean freight;
	private Boolean servicesOther;
	private Double minMargin;
	private Double adminFee;
	private Boolean providedRequuired;
	private Boolean discontAgrement;
	private Boolean providertoBeUsed;
	private Boolean weighttktRequired;
	private Boolean fortyfivePer;
	private Boolean fiftyFivePer;
	private Boolean cbmRequired;
	private Boolean reWeight;
	private Boolean dtoDALL;
	private Boolean dtoD;
	private Boolean seperateLinePricing;
	private Boolean dtoDWithOceanFreight;
	private Boolean dtoDWithOcean;
	private Boolean atSurvey;
	private Boolean atSurveyWith;
	private Boolean withFinals;
	private String  restrItemsDesc;
	private String  restrItemsList;
	private Boolean tranInsY;
	private Boolean tranInsN;
	private Boolean bipY;
	private Boolean bipN;
	private Boolean selfInsY;
	private Boolean selfInsN;
	private String  insurer;
	private Double subrogationAmnt;
	private Boolean valuedInv;
	private Boolean highValuedInv;
	private Boolean documentReqOthr;
	private String documentReqOthrDesc;
	private Double maxInsurance;
	private Boolean collectY;
	private Boolean collectN;
	private String InsSendDesc;
	private String selfInsuredInv;
	private Boolean byUs;
	private Boolean byAcc;
	private Boolean throughCarr;
	private Boolean inHouse;
	private String selfInsured;
	private Boolean claimY;
	private Boolean claimN;
	private String siInsured;
	private String typeIns;
	private String chargeDesc;
	private Boolean fullValuedInv;
	private String valueBased;
	private String valueBasedWHL;
	private String otherCarrier;
	private Boolean unInsurableItems;
	private String listUnSurableItems;
	private Boolean permStOY;
	private Boolean permStON;
	private Boolean electronic;
	private Boolean hardCopy;
	private Boolean batchInvY;
	private Boolean batchInvN;
	private Boolean within30Days;
	private Boolean within45Days;
	private Boolean within60Days;
	private Boolean within90Days;
	private Boolean withinOther;
	private Boolean creditY;
	private Boolean creditN;
	private String uploadDetails;
	private String invoiceDelDateDesc;
	private String creditTerms;
	private String amountApproved;
	private String auditResponseDesc;
	private String arContact;
	private String arContactEmail;
	private String arContactPhone;
	private Boolean statusUpdateRequiredY;
	private Boolean statusUpdateRequiredN;
	private Boolean reportY;
	private Boolean reportN;
	private String reportName1;
	private String reportName2;
	private String reportName3;
	private String reportName4;
	private String deliverTo1;
	private String deliverTo2;
	private String deliverTo3;
	private String deliverTo4;
	private String frequency1;
	private String frequency2;
	private String frequency3;
	private String frequency4;
	private String dueDate1;
	private String dueDate2;
	private String dueDate3;
	private String dueDate4;
	private String kpi;
	private String bipYComments;
	private String intlBillingAdditionalInfo;
	private String authorizationsExceptions;
	private String intlTransitInsAddInfo;
	private String reportAddInfo;
	private String invoiceAddInfo;
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("partnerCode",partnerCode)
				.append("partnerPrivateId", partnerPrivateId)
				.append("corpId", corpId)
				.append("dom", dom)
				.append("intl", intl)
				.append("rlo", rlo)
				.append("com", com)
				.append("oth", oth)
				.append("othComnts", othComnts)
				.append("agentCompanyName",agentCompanyName)
				.append("agentContact", agentContact)
				.append("agentPhone",agentPhone)
				.append("agentEmail", agentEmail)
				.append("auditfirmCompanyName", auditfirmCompanyName)
				.append("auditfirmContact", auditfirmContact)
				.append("auditfirmPhone", auditfirmPhone)
				.append("auditfirmEmail", auditfirmEmail)
				.append("hhgProviderDomCompanyName", hhgProviderDomCompanyName)
				.append("hhgProviderDomContact", hhgProviderDomContact)
				.append("hhgProviderDomPhone", hhgProviderDomPhone)
				.append("hhgProvideDomEmail", hhgProvideDomEmail)
				.append("hhgProviderIntlCompanyName",hhgProviderIntlCompanyName)
				.append("hhgProviderIntlContact", hhgProviderIntlContact)
				.append("hhgProviderIntlPhone", hhgProviderIntlPhone)
				.append("hhgProvideIntlEmail", hhgProvideIntlEmail)
				.append("variousContacts", variousContacts)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("hhgProviderIntlCompanyName2", hhgProviderIntlCompanyName2)
				.append("hhgProviderIntlCompanyName3", hhgProviderIntlCompanyName3)
				.append("hhgProviderIntlCompanyName4", hhgProviderIntlCompanyName4)
				.append("agent", agent)
				.append("hhgProviderIntlContact2", hhgProviderIntlContact2)
				.append("hhgProviderIntlPhone2", hhgProviderIntlPhone2)
				.append("hhgProvideIntlEmail2", hhgProvideIntlEmail2)
				.append("hhgProviderIntlContact3", hhgProviderIntlContact3)
				.append("hhgProviderIntlPhone3", hhgProviderIntlPhone3)
				.append("hhgProvideIntlEmail3", hhgProvideIntlEmail3)
				.append("hhgProviderIntlContact4", hhgProviderIntlContact4)
				.append("hhgProviderIntlPhone4", hhgProviderIntlPhone4)
				.append("hhgProvideIntlEmail4", hhgProvideIntlEmail4)
								.append("superGroupName",superGroupName)
								.append("customer",customer)
								.append("superGroup ",superGroup )
								.append("username",username)
								.append("password",password)
								.append("mPointOfContact",mPointOfContact)
								.append("intlTermsPhone",intlTermsPhone)
								.append("intlTermsEmail",intlTermsEmail)
								.append("contractInPlaceY ",contractInPlaceY )
								.append("startDate",startDate)
								.append("lnthOfContract",lnthOfContract )
								.append("indRequestforBids",indRequestforBids)
								.append("cld",cld)
								.append("rloProviders",rloProviders)
								.append("ts",ts)
								.append("rap",rap)
								.append("fixPricingforOrigOrDest",fixPricingforOrigOrDest)
								.append("air",air)
								.append("sea",sea)
								.append("permStorage",permStorage)
								.append("sit",sit)
								.append("single",single)
								.append("married",married)
								.append("marriedWithChild",marriedWithChild)
								.append("seaAllowanceMatrix",seaAllowanceMatrix)
								.append("contractInPlaceN ",contractInPlaceN )
								.append("airDesc",airDesc )
								.append("seaDesc",seaDesc )
								.append("singleDesc",singleDesc )
								.append("marriedDesc",marriedDesc )
								.append("marriedWithChildDesc",marriedWithChildDesc)
								.append("accountPointOfContact",accountPointOfContact )
								.append("accountEmail",accountEmail )
								.append("accountPhone",accountPhone )
								.append("rloCompanyInvolved",rloCompanyInvolved )
								.append("rloCompanyInvolvedPhone",rloCompanyInvolvedPhone )
								.append("mainContact",mainContact )
								.append("mainContactEmail",mainContactEmail )
								.append("billDeliveryPreferenceMail",billDeliveryPreferenceMail)
								.append("billDeliveryPreferenceEmail",billDeliveryPreferenceEmail)
								.append("billDeliveryPreferenceEDI",billDeliveryPreferenceEDI)
								.append("billingCutOff",billingCutOff)
								.append("billingCutOffDays",billingCutOffDays)
								.append("daysfromDelivery",daysfromDelivery)
								.append("supplimentalBillingAllowed",supplimentalBillingAllowed)
								.append("approvalRequired",approvalRequired)
								.append("billingContact",billingContact)
								.append("billingContactEmail",billingContactEmail)
								.append("billingContactPhone",billingContactPhone)
								.append("access",access)
								.append("billOfLading",billOfLading)
								.append("cubeSheet",cubeSheet)
								.append("airwaysBill",airwaysBill)
								.append("rated",rated)
								.append("unrated",unrated)
								.append("proofOfPmt",proofOfPmt)
								.append("approvalRequiredRated",approvalRequiredRated)
								.append("approvalRequiredUnrated",approvalRequiredUnrated)
								.append("approvalRequiredProofOfPmt",approvalRequiredProofOfPmt)
								.append("excApprovals",excApprovals)
								.append("billingOthers",billingOthers)
								.append("tarrifRateSheet",tarrifRateSheet)
								.append("auth",auth)
								.append("ops",ops)
								.append("insp",insp)
								.append("nvtd",nvtd)
								.append("nvtv",nvtv)
								.append("wtct",wtct)
								.append("officialReFAdditional",officialReFAdditional)
								.append("crateList",crateList)
								.append("thirdPartyDocuments",thirdPartyDocuments)
								.append("backUp",backUp)
								.append("frieghtInvoice",frieghtInvoice)
								.append("specialInvoiceInstructions",specialInvoiceInstructions)
								.append("restrItemsList",restrItemsList)
								.append("restrItemsDesc",restrItemsDesc )
								.append("withFinals",withFinals )
								.append("atSurveyWith",atSurveyWith )
								.append("atSurvey",atSurvey )
								.append("dtoDWithOcean",dtoDWithOcean )
								.append("dtoDWithOceanFreight",dtoDWithOceanFreight )
								.append("seperateLinePricing",seperateLinePricing )
								.append("dtoD",dtoD)
								.append("dtoDALL",dtoDALL)
								.append("reWeight",reWeight)
								.append("cbmRequired",cbmRequired)
								.append("fiftyFivePer",fiftyFivePer)
								.append("fortyfivePer",fortyfivePer)
								.append("weighttktRequired",weighttktRequired)
								.append("providertoBeUsed",providertoBeUsed)
								.append("discontAgrement",discontAgrement)
								.append("providedRequuired",providedRequuired)
								.append("adminFee",adminFee)
								.append("minMargin",minMargin)
								.append("servicesOther",servicesOther)
								.append("freight",freight)
								.append("dA",dA)
								.append("oA",oA)
								.append("triRegionalModelDesc",triRegionalModelDesc)
								.append("pricingOtherDesc",pricingOtherDesc)
								.append("pricingOther",pricingOther)
								.append("onlineRateFill",onlineRateFill)
								.append("rateMatrix",rateMatrix)
								.append("costPls",costPls)
								.append("indBids",indBids)
								.append("triRegional",triRegional)
								.append("contratedNo",contratedNo)
								.append("contratedYes",contratedYes)
								.append("pathforPricing",pathforPricing)
								.append("tranInsY",tranInsY)
								.append("tranInsN",tranInsN)
								.append("bipY",bipY)
								.append("bipN",bipN)
								.append("selfInsY",selfInsY)
								.append("selfInsN",selfInsN)
								.append("insurer",insurer)
								.append("subrogationAmnt",subrogationAmnt)
								.append("valuedInv",valuedInv)
								.append("highValuedInv",highValuedInv)
								.append("documentReqOthr",documentReqOthr)
								.append("documentReqOthrDesc",documentReqOthrDesc)
								.append("maxInsurance", maxInsurance).append("collectY", collectY).append("collectN", collectN)
								.append("InsSendDesc", InsSendDesc).append("InsSendDesc", InsSendDesc).append("InsSendDesc", InsSendDesc)
								.append("selfInsuredInv", selfInsuredInv).append("byUs", byUs).append("byAcc", byAcc)
								.append("throughCarr", throughCarr).append("inHouse", inHouse).append("selfInsured", selfInsured)
								.append("claimY", claimY).append("claimN", claimN).append("siInsured", siInsured)
								.append("typeIns", typeIns)
								.append("chargeDesc", chargeDesc)
								.append("fullValuedInv", fullValuedInv)
								.append("valueBased", valueBased)
								.append("valueBasedWHL", valueBasedWHL)
								.append("otherCarrier", otherCarrier)
								.append("unInsurableItems", unInsurableItems)
								.append("listUnSurableItems", listUnSurableItems)
								.append("permStOY", permStOY).append("permStON", permStON)
								.append("electronic", electronic)
								.append("hardCopy", hardCopy)
								.append("batchInvY", batchInvY)
								.append("batchInvN", batchInvN)
								.append("within30Days", within30Days)
								.append("within45Days", within45Days)
								.append("within60Days", within60Days)
								.append("within90Days", within90Days)
								.append("withinOther", withinOther)
								.append("creditY", creditY)
								.append("creditN", creditN)
								.append("uploadDetails", uploadDetails)
								.append("invoiceDelDateDesc", invoiceDelDateDesc)
								.append("creditTerms", creditTerms)
								.append("amountApproved", amountApproved)
								.append("auditResponseDesc", auditResponseDesc)
								.append("arContact", arContact)
								.append("arContactEmail", arContactEmail)
								.append("arContactPhone", arContactPhone)
								.append("statusUpdateRequiredY", statusUpdateRequiredY)
								.append("statusUpdateRequiredN", statusUpdateRequiredN)
								.append("reportY", reportY)
								.append("reportN", reportN)
								.append("reportName1", reportName1)
								.append("reportName2", reportName2)
								.append("reportName3", reportName3)
								.append("reportName4", reportName4)
								.append("deliverTo1", deliverTo1).append("deliverTo2", deliverTo2)
								.append("deliverTo3", deliverTo3)
								.append("deliverTo4", deliverTo4)
								.append("frequency1", frequency1)
								.append("frequency2", frequency2)
								.append("frequency3", frequency3)
								.append("frequency4", frequency4)
								.append("dueDate1", dueDate1)
								.append("dueDate2", dueDate2)
								.append("dueDate3", dueDate3)
								.append("dueDate4", dueDate4)
								.append("kpi", kpi)
								.append("bipYComments", bipYComments)
								.append("intlBillingAdditionalInfo", intlBillingAdditionalInfo)
								.append("authorizationsExceptions", authorizationsExceptions)
								.append("intlTransitInsAddInfo", intlTransitInsAddInfo)
								.append("reportAddInfo", reportAddInfo)
								.append("invoiceAddInfo", invoiceAddInfo)
								.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerProfileInfo ))
			return false;
		PartnerProfileInfo castOther=(PartnerProfileInfo)other;
		return new EqualsBuilder().append(id, castOther.id)
		.append(partnerCode,castOther.partnerCode)
		.append(partnerPrivateId, castOther.partnerPrivateId)
		.append(corpId, castOther.corpId)
		.append(dom, castOther.dom)
		.append(intl, castOther.intl)
		.append(rlo, castOther.rlo)
		.append(com, castOther.com)
		.append(oth, castOther.oth)
		.append(othComnts, castOther.othComnts)
		.append(agentCompanyName,castOther.agentCompanyName)
		.append(agentContact, castOther.agentContact)
		.append(agentPhone,castOther.agentPhone)
		.append(agentEmail, castOther.agentEmail)
		.append(auditfirmCompanyName, castOther.auditfirmCompanyName)
		.append(auditfirmContact, castOther.auditfirmContact)
		.append(auditfirmPhone, castOther.auditfirmPhone)
		.append(auditfirmEmail, castOther.auditfirmEmail)
		.append(hhgProviderDomCompanyName, castOther.hhgProviderDomCompanyName)
		.append(hhgProviderDomContact, castOther.hhgProviderDomContact)
		.append(hhgProviderDomPhone, castOther.hhgProviderDomPhone)
		.append(hhgProvideDomEmail, castOther.hhgProvideDomEmail)
		.append(hhgProviderIntlCompanyName,castOther.hhgProviderIntlCompanyName)
		.append(hhgProviderIntlContact, castOther.hhgProviderIntlContact)
		.append(hhgProviderIntlPhone, castOther.hhgProviderIntlPhone)
		.append(hhgProvideIntlEmail, castOther.hhgProvideIntlEmail)
		.append(variousContacts, castOther.variousContacts)
		.append(createdBy, castOther.createdBy)
		.append(createdOn, castOther.createdOn)
		.append(updatedBy, castOther.updatedBy)
		.append(updatedOn, castOther.updatedOn)
		.append(hhgProviderIntlCompanyName2, castOther.hhgProviderIntlCompanyName2)
		.append(hhgProviderIntlCompanyName3, castOther.hhgProviderIntlCompanyName3)
		.append(hhgProviderIntlCompanyName4, castOther.hhgProviderIntlCompanyName4)
		.append(agent, castOther.agent)
		.append(hhgProviderIntlContact2, castOther.hhgProviderIntlContact2)
		.append(hhgProviderIntlPhone2, castOther.hhgProviderIntlPhone2)
		.append(hhgProvideIntlEmail2, castOther.hhgProvideIntlEmail2)
		.append(hhgProviderIntlContact3, castOther.hhgProviderIntlContact3)
		.append(hhgProviderIntlPhone3, castOther.hhgProviderIntlPhone3)
		.append(hhgProvideIntlEmail3, castOther.hhgProvideIntlEmail3)
		.append(hhgProviderIntlContact4, castOther.hhgProviderIntlContact4)
		.append(hhgProviderIntlPhone4, castOther.hhgProviderIntlPhone4)
		.append(hhgProvideIntlEmail4, castOther.hhgProvideIntlEmail4)
								.append(superGroupName, castOther.superGroupName)
								.append(customer, castOther.customer)
								.append(superGroup, castOther.superGroup )
								.append(username, castOther.username)
								.append(password, castOther.password)
								.append(mPointOfContact, castOther.mPointOfContact)
								.append(intlTermsPhone, castOther.intlTermsPhone)
								.append(intlTermsEmail, castOther.intlTermsEmail)
								.append(contractInPlaceY, castOther.contractInPlaceY )
								.append(startDate, castOther.startDate)
								.append(lnthOfContract, castOther.lnthOfContract )
								.append(indRequestforBids, castOther.indRequestforBids)
								.append(cld, castOther.cld)
								.append(rloProviders, castOther.rloProviders)
								.append(ts, castOther.ts)
								.append(rap, castOther.rap)
								.append(fixPricingforOrigOrDest, castOther.fixPricingforOrigOrDest)
								.append(air, castOther.air)
								.append(sea, castOther.sea)
								.append(permStorage, castOther.permStorage)
								.append(sit, castOther.sit)
								.append(single, castOther.single)
								.append(married, castOther.married)
								.append(marriedWithChild, castOther.marriedWithChild)
								.append(seaAllowanceMatrix, castOther.seaAllowanceMatrix)
								.append(contractInPlaceN, castOther.contractInPlaceN )
								.append(airDesc, castOther.airDesc )
								.append(seaDesc, castOther.seaDesc )
								.append(singleDesc , castOther.singleDesc)
								.append(marriedDesc, castOther.marriedDesc )
								.append(marriedWithChildDesc, castOther.marriedWithChildDesc)
								.append(accountPointOfContact, castOther.accountPointOfContact )
								.append(accountEmail, castOther.accountEmail )
								.append(accountPhone, castOther.accountPhone )
								.append(rloCompanyInvolved, castOther.rloCompanyInvolved )
								.append(rloCompanyInvolvedPhone, castOther.rloCompanyInvolvedPhone )
								.append(mainContact, castOther.mainContact )
								.append(mainContactEmail, castOther.mainContactEmail )
								.append(billDeliveryPreferenceMail, castOther.billDeliveryPreferenceMail)
								.append(billDeliveryPreferenceEmail, castOther.billDeliveryPreferenceEmail)
								.append(billDeliveryPreferenceEDI, castOther.billDeliveryPreferenceEDI)
								.append(billingCutOff, castOther.billingCutOff)
								.append(billingCutOffDays, castOther.billingCutOffDays)
								.append(daysfromDelivery, castOther.daysfromDelivery)
								.append(supplimentalBillingAllowed, castOther.supplimentalBillingAllowed)
								.append(approvalRequired, castOther.approvalRequired)
								.append(billingContact, castOther.billingContact)
								.append(billingContactEmail, castOther.billingContactEmail)
								.append(billingContactPhone, castOther.billingContactPhone)
								.append(access, castOther.access)
								.append(billOfLading, castOther.billOfLading)
								.append(cubeSheet, castOther.cubeSheet)
								.append(airwaysBill, castOther.airwaysBill)
								.append(rated, castOther.rated)
								.append(unrated, castOther.unrated)
								.append(proofOfPmt, castOther.proofOfPmt)
								.append(approvalRequiredRated, castOther.approvalRequiredRated)
								.append(approvalRequiredUnrated, castOther.approvalRequiredUnrated)
								.append(approvalRequiredProofOfPmt, castOther.approvalRequiredProofOfPmt)
								.append(excApprovals, castOther.excApprovals)
								.append(billingOthers, castOther.billingOthers)
								.append(tarrifRateSheet, castOther.tarrifRateSheet)
								.append(auth, castOther.auth)
								.append(ops, castOther.ops)
								.append(insp, castOther.insp)
								.append(nvtd, castOther.nvtd)
								.append(nvtv, castOther.nvtv)
								.append(wtct, castOther.wtct)
								.append(officialReFAdditional, castOther.officialReFAdditional)
								.append(crateList, castOther.crateList)
								.append(thirdPartyDocuments, castOther.thirdPartyDocuments)
								.append(backUp, castOther.backUp)
								.append(frieghtInvoice, castOther.frieghtInvoice)
								.append(specialInvoiceInstructions, castOther.specialInvoiceInstructions)
								.append(restrItemsList, castOther.restrItemsList)
								.append(restrItemsDesc, castOther.restrItemsDesc )
								.append(withFinals, castOther.withFinals )
								.append(atSurveyWith, castOther.atSurveyWith )
								.append(atSurvey, castOther.atSurvey )
								.append(dtoDWithOcean, castOther.dtoDWithOcean )
								.append(dtoDWithOceanFreight, castOther.dtoDWithOceanFreight )
								.append(seperateLinePricing, castOther.seperateLinePricing )
								.append(dtoD, castOther.dtoD)
								.append(dtoDALL, castOther.dtoDALL)
								.append(reWeight, castOther.reWeight)
								.append(cbmRequired, castOther.cbmRequired)
								.append(fiftyFivePer, castOther.fiftyFivePer)
								.append(fortyfivePer, castOther.fortyfivePer)
								.append(weighttktRequired, castOther.weighttktRequired)
								.append(providertoBeUsed, castOther.providertoBeUsed)
								.append(discontAgrement, castOther.discontAgrement)
								.append(providedRequuired, castOther.providedRequuired)
								.append(adminFee, castOther.adminFee)
								.append(minMargin, castOther.minMargin)
								.append(servicesOther, castOther.servicesOther)
								.append(freight, castOther.freight)
								.append(dA, castOther.dA)
								.append(oA, castOther.oA)
								.append(triRegionalModelDesc, castOther.triRegionalModelDesc)
								.append(pricingOtherDesc, castOther.pricingOtherDesc)
								.append(pricingOther, castOther.pricingOther)
								.append(onlineRateFill, castOther.onlineRateFill)
								.append(rateMatrix, castOther.rateMatrix)
								.append(costPls, castOther.costPls)
								.append(indBids, castOther.indBids)
								.append(triRegional, castOther.triRegional)
								.append(contratedNo, castOther.contratedNo)
								.append(contratedYes, castOther.contratedYes)
								.append(pathforPricing, castOther.pathforPricing)
								.append(tranInsY, castOther.tranInsY)
								.append(tranInsN, castOther.tranInsN)
								.append(bipY, castOther.bipY)
								.append(bipN, castOther.bipN)
								.append(selfInsY, castOther.selfInsY)
								.append(selfInsN, castOther.selfInsN)
								.append(insurer, castOther.insurer)
								.append(subrogationAmnt, castOther.subrogationAmnt)
								.append(valuedInv, castOther.valuedInv)
								.append(highValuedInv, castOther.highValuedInv)
								.append(documentReqOthr, castOther.documentReqOthr)
								.append(documentReqOthrDesc, castOther.documentReqOthrDesc)
								.append(maxInsurance, castOther.maxInsurance).append(collectY, castOther.collectY).append(collectN, castOther. collectN)
								.append(InsSendDesc, castOther. InsSendDesc).append(InsSendDesc, castOther.InsSendDesc).append(InsSendDesc, castOther.InsSendDesc)
								.append(selfInsuredInv, castOther.selfInsuredInv).append(byUs, castOther. byUs).append(byAcc, castOther.byAcc)
								.append(throughCarr, castOther.throughCarr).append(inHouse, castOther. inHouse).append(selfInsured, castOther.selfInsured)
								.append(claimY, castOther. claimY).append(claimN, castOther. claimN).append(siInsured, castOther. siInsured)
								.append(typeIns, castOther. typeIns)
								.append(chargeDesc, castOther. chargeDesc)
								.append(fullValuedInv, castOther.fullValuedInv)
								.append(valueBased, castOther. valueBased)
								.append(valueBasedWHL, castOther.valueBasedWHL)
								.append(otherCarrier, castOther.otherCarrier)
								.append(unInsurableItems, castOther.unInsurableItems)
								.append(listUnSurableItems, castOther.listUnSurableItems)
								.append(permStOY, castOther.permStOY).append(permStON, castOther. permStON)
								.append(electronic, castOther.electronic)
								.append(hardCopy, castOther.hardCopy)
								.append(batchInvY, castOther.batchInvY)
								.append(batchInvN, castOther.batchInvN)
								.append(within30Days, castOther.within30Days)
								.append(within45Days, castOther.within45Days)
								.append(within60Days, castOther.within60Days)
								.append(within90Days, castOther.within90Days)
								.append(withinOther, castOther.withinOther)
								.append(creditY, castOther.creditY)
								.append(creditN, castOther.creditN)
								.append(uploadDetails, castOther.uploadDetails)
								.append(invoiceDelDateDesc, castOther.invoiceDelDateDesc)
								.append(creditTerms, castOther.creditTerms)
								.append(amountApproved, castOther.amountApproved)
								.append(auditResponseDesc, castOther.auditResponseDesc)
								.append(arContact, castOther.arContact)
								.append(arContactEmail, castOther.arContactEmail)
								.append(arContactPhone, castOther.arContactPhone)
								.append(statusUpdateRequiredY, castOther.statusUpdateRequiredY)
								.append(statusUpdateRequiredN, castOther.statusUpdateRequiredN)
								.append(reportY, castOther.reportY)
								.append(reportN, castOther.reportN)
								.append(reportName1, castOther.reportName1)
								.append(reportName2, castOther.reportName2)
								.append(reportName3, castOther.reportName3)
								.append(reportName4, castOther.reportName4)
								.append(deliverTo1, castOther.deliverTo1).append(deliverTo2, castOther.deliverTo2)
								.append(deliverTo3, castOther.deliverTo3)
								.append(deliverTo4, castOther.deliverTo4)
								.append(frequency1, castOther.frequency1)
								.append(frequency2, castOther.frequency2)
								.append(frequency3, castOther.frequency3)
								.append(frequency4, castOther.frequency4)
								.append(dueDate1, castOther.dueDate1)
								.append(dueDate2, castOther.dueDate2)
								.append(dueDate3, castOther.dueDate3)
								.append(dueDate4, castOther.dueDate4)
								.append(kpi, castOther.kpi)
								.append(bipYComments, castOther.bipYComments)
								.append(intlBillingAdditionalInfo, castOther.intlBillingAdditionalInfo)
								.append(authorizationsExceptions, castOther.authorizationsExceptions)
								.append(intlTransitInsAddInfo, castOther.intlTransitInsAddInfo)
								.append(reportAddInfo, castOther.reportAddInfo)
								.append(invoiceAddInfo, castOther.invoiceAddInfo)
								.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id)
		.append(partnerCode)
		.append(partnerPrivateId)
		.append(corpId)
		.append(dom)
		.append(intl)
		.append(rlo)
		.append(com)
		.append(oth)
		.append(othComnts)
		.append(agentCompanyName)
		.append(agentContact)
		.append(agentPhone)
		.append(agentEmail)
		.append(auditfirmCompanyName)
		.append(auditfirmContact)
		.append(auditfirmPhone)
		.append(auditfirmEmail)
		.append(hhgProviderDomCompanyName)
		.append(hhgProviderDomContact)
		.append(hhgProviderDomPhone)
		.append(hhgProvideDomEmail)
		.append(hhgProviderIntlCompanyName)
		.append(hhgProviderIntlContact)
		.append(hhgProviderIntlPhone)
		.append(hhgProvideIntlEmail)
		.append(variousContacts)
		.append(createdBy)
		.append(createdOn)
		.append(updatedBy)
		.append(updatedOn)
		.append(hhgProviderIntlCompanyName2)
		.append(hhgProviderIntlCompanyName3)
		.append(hhgProviderIntlCompanyName4)
		.append(agent)
		.append(hhgProviderIntlContact2)
		.append(hhgProviderIntlPhone2)
		.append(hhgProvideIntlEmail2)
		.append(hhgProviderIntlContact3)
		.append(hhgProviderIntlPhone3)
		.append(hhgProvideIntlEmail3)
		.append(hhgProviderIntlContact4)
		.append(hhgProviderIntlPhone4)
		.append(hhgProvideIntlEmail4)
		.append(superGroupName)
								.append(customer)
								.append(superGroup)
								.append(username)
								.append(password)
								.append(mPointOfContact)
								.append(intlTermsPhone)
								.append(intlTermsEmail)
								.append(contractInPlaceY)
								.append(startDate)
								.append(lnthOfContract)
								.append(indRequestforBids)
								.append(cld)
								.append(rloProviders)
								.append(ts)
								.append(rap)
								.append(fixPricingforOrigOrDest)
								.append(air)
								.append(sea)
								.append(permStorage)
								.append(sit)
								.append(single)
								.append(married)
								.append(marriedWithChild)
								.append(seaAllowanceMatrix)
								.append(contractInPlaceN)
								.append(airDesc)
								.append(seaDesc)
								.append(singleDesc)
								.append(marriedDesc)
								.append(marriedWithChildDesc)
								.append(accountPointOfContact)
								.append(accountEmail)
								.append(accountPhone)
								.append(rloCompanyInvolved)
								.append(rloCompanyInvolvedPhone)
								.append(mainContact)
								.append(mainContactEmail)
								.append(billDeliveryPreferenceMail)
								.append(billDeliveryPreferenceEmail)
								.append(billDeliveryPreferenceEDI)
								.append(billingCutOff)
								.append(billingCutOffDays)
								.append(daysfromDelivery)
								.append(supplimentalBillingAllowed)
								.append(approvalRequired)
								.append(billingContact)
								.append(billingContactEmail)
								.append(billingContactPhone)
								.append(access)
								.append(billOfLading)
								.append(cubeSheet)
								.append(airwaysBill)
								.append(rated)
								.append(unrated)
								.append(proofOfPmt)
								.append(approvalRequiredRated)
								.append(approvalRequiredUnrated)
								.append(approvalRequiredProofOfPmt)
								.append(excApprovals)
								.append(billingOthers)
								.append(tarrifRateSheet)
								.append(auth)
								.append(ops)
								.append(insp)
								.append(nvtd)
								.append(nvtv)
								.append(wtct)
								.append(officialReFAdditional)
								.append(crateList)
								.append(thirdPartyDocuments)
								.append(backUp)
								.append(frieghtInvoice)
								.append(specialInvoiceInstructions)
								.append(restrItemsList)
								.append(restrItemsDesc)
								.append(withFinals)
								.append(atSurveyWith)
								.append(atSurvey)
								.append(dtoDWithOcean)
								.append(dtoDWithOceanFreight)
								.append(seperateLinePricing)
								.append(dtoD)
								.append(dtoDALL)
								.append(reWeight)
								.append(cbmRequired)
								.append(fiftyFivePer)
								.append(fortyfivePer)
								.append(weighttktRequired)
								.append(providertoBeUsed)
								.append(discontAgrement)
								.append(providedRequuired)
								.append(adminFee)
								.append(minMargin)
								.append(servicesOther)
								.append(freight)
								.append(dA)
								.append(oA)
								.append(triRegionalModelDesc)
								.append(pricingOtherDesc)
								.append(pricingOther)
								.append(onlineRateFill)
								.append(rateMatrix)
								.append(costPls)
								.append(indBids)
								.append(triRegional)
								.append(contratedNo)
								.append(contratedYes)
								.append(pathforPricing)
								.append(tranInsY)
								.append(tranInsN)
								.append(bipY)
								.append(bipN)
								.append(selfInsY)
								.append(selfInsN)
								.append(insurer)
								.append(subrogationAmnt)
								.append(valuedInv)
								.append(highValuedInv)
								.append(documentReqOthr)
								.append(documentReqOthrDesc)
								.append(maxInsurance ).append(collectY).append(collectN)
								.append(InsSendDesc).append(InsSendDesc).append(InsSendDesc)
								.append(selfInsuredInv).append(byUs).append(byAcc)
								.append(throughCarr).append(inHouse).append(selfInsured)
								.append(claimY).append(claimN).append(siInsured)
								.append(typeIns)
								.append(chargeDesc)
								.append(fullValuedInv)
								.append(valueBased)
								.append(valueBasedWHL)
								.append(otherCarrier)
								.append(unInsurableItems)
								.append(listUnSurableItems)
								.append(permStOY).append(permStON)
								.append(electronic)
								.append(hardCopy)
								.append(batchInvY)
								.append(batchInvN)
								.append(within30Days)
								.append(within45Days)
								.append(within60Days)
								.append(within90Days)
								.append(withinOther)
								.append(creditY)
								.append(creditN)
								.append(uploadDetails)
								.append(invoiceDelDateDesc)
								.append(creditTerms)
								.append(amountApproved)
								.append(auditResponseDesc)
								.append(arContact)
								.append(arContactEmail)
								.append(arContactPhone)
								.append(statusUpdateRequiredY)
								.append(statusUpdateRequiredN)
								.append(reportY)
								.append(reportN)
								.append(reportName1)
								.append(reportName2)
								.append(reportName3)
								.append(reportName4)
								.append(deliverTo1).append(deliverTo2)
								.append(deliverTo3)
								.append(deliverTo4)
								.append(frequency1)
								.append(frequency2)
								.append(frequency3)
								.append(frequency4)
								.append(dueDate1)
								.append(dueDate2)
								.append(dueDate3)
								.append(dueDate4)
								.append(kpi)
								.append(bipYComments)
								.append(intlBillingAdditionalInfo)
								.append(authorizationsExceptions)
								.append(intlTransitInsAddInfo)
								.append(reportAddInfo)
								.append(invoiceAddInfo)
								.toHashCode();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	@Column
	public Long getPartnerPrivateId() {
		return partnerPrivateId;
	}

	public void setPartnerPrivateId(Long partnerPrivateId) {
		this.partnerPrivateId = partnerPrivateId;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public Boolean getDom() {
		return dom;
	}

	public void setDom(Boolean dom) {
		this.dom = dom;
	}
	@Column
	public Boolean getIntl() {
		return intl;
	}

	public void setIntl(Boolean intl) {
		this.intl = intl;
	}
	@Column
	public Boolean getRlo() {
		return rlo;
	}

	public void setRlo(Boolean rlo) {
		this.rlo = rlo;
	}
	@Column
	public Boolean getCom() {
		return com;
	}

	public void setCom(Boolean com) {
		this.com = com;
	}
	@Column
	public Boolean getOth() {
		return oth;
	}

	public void setOth(Boolean oth) {
		this.oth = oth;
	}
	@Column
	public String getOthComnts() {
		return othComnts;
	}

	public void setOthComnts(String othComnts) {
		this.othComnts = othComnts;
	}
	@Column
	public String getAgentCompanyName() {
		return agentCompanyName;
	}

	public void setAgentCompanyName(String agentCompanyName) {
		this.agentCompanyName = agentCompanyName;
	}
	@Column
	public String getAgentContact() {
		return agentContact;
	}

	public void setAgentContact(String agentContact) {
		this.agentContact = agentContact;
	}
	@Column
	public String getAgentPhone() {
		return agentPhone;
	}

	public void setAgentPhone(String agentPhone) {
		this.agentPhone = agentPhone;
	}
	@Column
	public String getAgentEmail() {
		return agentEmail;
	}

	public void setAgentEmail(String agentEmail) {
		this.agentEmail = agentEmail;
	}
	@Column
	public String getAuditfirmCompanyName() {
		return auditfirmCompanyName;
	}

	public void setAuditfirmCompanyName(String auditfirmCompanyName) {
		this.auditfirmCompanyName = auditfirmCompanyName;
	}
	@Column
	public String getAuditfirmContact() {
		return auditfirmContact;
	}

	public void setAuditfirmContact(String auditfirmContact) {
		this.auditfirmContact = auditfirmContact;
	}
	@Column
	public String getAuditfirmPhone() {
		return auditfirmPhone;
	}

	public void setAuditfirmPhone(String auditfirmPhone) {
		this.auditfirmPhone = auditfirmPhone;
	}
	@Column
	public String getAuditfirmEmail() {
		return auditfirmEmail;
	}

	public void setAuditfirmEmail(String auditfirmEmail) {
		this.auditfirmEmail = auditfirmEmail;
	}
	@Column
	public String getHhgProviderDomCompanyName() {
		return hhgProviderDomCompanyName;
	}

	public void setHhgProviderDomCompanyName(String hhgProviderDomCompanyName) {
		this.hhgProviderDomCompanyName = hhgProviderDomCompanyName;
	}
	@Column
	public String getHhgProviderDomContact() {
		return hhgProviderDomContact;
	}

	public void setHhgProviderDomContact(String hhgProviderDomContact) {
		this.hhgProviderDomContact = hhgProviderDomContact;
	}
	@Column
	public String getHhgProviderDomPhone() {
		return hhgProviderDomPhone;
	}

	public void setHhgProviderDomPhone(String hhgProviderDomPhone) {
		this.hhgProviderDomPhone = hhgProviderDomPhone;
	}
	@Column
	public String getHhgProvideDomEmail() {
		return hhgProvideDomEmail;
	}

	public void setHhgProvideDomEmail(String hhgProvideDomEmail) {
		this.hhgProvideDomEmail = hhgProvideDomEmail;
	}
	@Column
	public String getHhgProviderIntlCompanyName() {
		return hhgProviderIntlCompanyName;
	}

	public void setHhgProviderIntlCompanyName(String hhgProviderIntlCompanyName) {
		this.hhgProviderIntlCompanyName = hhgProviderIntlCompanyName;
	}
	@Column
	public String getHhgProviderIntlContact() {
		return hhgProviderIntlContact;
	}

	public void setHhgProviderIntlContact(String hhgProviderIntlContact) {
		this.hhgProviderIntlContact = hhgProviderIntlContact;
	}
	@Column
	public String getHhgProviderIntlPhone() {
		return hhgProviderIntlPhone;
	}

	public void setHhgProviderIntlPhone(String hhgProviderIntlPhone) {
		this.hhgProviderIntlPhone = hhgProviderIntlPhone;
	}
	@Column
	public String getHhgProvideIntlEmail() {
		return hhgProvideIntlEmail;
	}

	public void setHhgProvideIntlEmail(String hhgProvideIntlEmail) {
		this.hhgProvideIntlEmail = hhgProvideIntlEmail;
	}
	@Column
	public String getVariousContacts() {
		return variousContacts;
	}

	public void setVariousContacts(String variousContacts) {
		this.variousContacts = variousContacts;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getHhgProviderIntlCompanyName2() {
		return hhgProviderIntlCompanyName2;
	}

	public void setHhgProviderIntlCompanyName2(String hhgProviderIntlCompanyName2) {
		this.hhgProviderIntlCompanyName2 = hhgProviderIntlCompanyName2;
	}
	@Column
	public String getHhgProviderIntlCompanyName3() {
		return hhgProviderIntlCompanyName3;
	}

	public void setHhgProviderIntlCompanyName3(String hhgProviderIntlCompanyName3) {
		this.hhgProviderIntlCompanyName3 = hhgProviderIntlCompanyName3;
	}
	@Column
	public String getHhgProviderIntlCompanyName4() {
		return hhgProviderIntlCompanyName4;
	}

	public void setHhgProviderIntlCompanyName4(String hhgProviderIntlCompanyName4) {
		this.hhgProviderIntlCompanyName4 = hhgProviderIntlCompanyName4;
	}

	@Column
	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}
	@Column
	public String getHhgProviderIntlContact2() {
		return hhgProviderIntlContact2;
	}

	public void setHhgProviderIntlContact2(String hhgProviderIntlContact2) {
		this.hhgProviderIntlContact2 = hhgProviderIntlContact2;
	}
	@Column
	public String getHhgProviderIntlPhone2() {
		return hhgProviderIntlPhone2;
	}
	@Column
	public void setHhgProviderIntlPhone2(String hhgProviderIntlPhone2) {
		this.hhgProviderIntlPhone2 = hhgProviderIntlPhone2;
	}
	@Column
	public String getHhgProvideIntlEmail2() {
		return hhgProvideIntlEmail2;
	}

	public void setHhgProvideIntlEmail2(String hhgProvideIntlEmail2) {
		this.hhgProvideIntlEmail2 = hhgProvideIntlEmail2;
	}
	@Column
	public String getHhgProviderIntlContact3() {
		return hhgProviderIntlContact3;
	}

	public void setHhgProviderIntlContact3(String hhgProviderIntlContact3) {
		this.hhgProviderIntlContact3 = hhgProviderIntlContact3;
	}
	@Column
	public String getHhgProviderIntlPhone3() {
		return hhgProviderIntlPhone3;
	}

	public void setHhgProviderIntlPhone3(String hhgProviderIntlPhone3) {
		this.hhgProviderIntlPhone3 = hhgProviderIntlPhone3;
	}
	@Column
	public String getHhgProvideIntlEmail3() {
		return hhgProvideIntlEmail3;
	}

	public void setHhgProvideIntlEmail3(String hhgProvideIntlEmail3) {
		this.hhgProvideIntlEmail3 = hhgProvideIntlEmail3;
	}
	@Column
	public String getHhgProviderIntlContact4() {
		return hhgProviderIntlContact4;
	}

	public void setHhgProviderIntlContact4(String hhgProviderIntlContact4) {
		this.hhgProviderIntlContact4 = hhgProviderIntlContact4;
	}
	@Column
	public String getHhgProviderIntlPhone4() {
		return hhgProviderIntlPhone4;
	}

	public void setHhgProviderIntlPhone4(String hhgProviderIntlPhone4) {
		this.hhgProviderIntlPhone4 = hhgProviderIntlPhone4;
	}
	@Column
	public String getHhgProvideIntlEmail4() {
		return hhgProvideIntlEmail4;
	}

	public void setHhgProvideIntlEmail4(String hhgProvideIntlEmail4) {
		this.hhgProvideIntlEmail4 = hhgProvideIntlEmail4;
	}
	@Column
	public String getSuperGroupName() {
		return superGroupName;
	}

	public void setSuperGroupName(String superGroupName) {
		this.superGroupName = superGroupName;
	}
	@Column
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	@Column
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	@Column
	public String getmPointOfContact() {
		return mPointOfContact;
	}

	public void setmPointOfContact(String mPointOfContact) {
		this.mPointOfContact = mPointOfContact;
	}
	@Column
	public String getIntlTermsPhone() {
		return intlTermsPhone;
	}

	public void setIntlTermsPhone(String intlTermsPhone) {
		this.intlTermsPhone = intlTermsPhone;
	}
	@Column
	public String getIntlTermsEmail() {
		return intlTermsEmail;
	}

	public void setIntlTermsEmail(String intlTermsEmail) {
		this.intlTermsEmail = intlTermsEmail;
	}
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	@Column
	public Boolean getIndRequestforBids() {
		return indRequestforBids;
	}

	public void setIndRequestforBids(Boolean indRequestforBids) {
		this.indRequestforBids = indRequestforBids;
	}
	@Column
	public Boolean getCld() {
		return cld;
	}

	public void setCld(Boolean cld) {
		this.cld = cld;
	}
	@Column
	public Boolean getRloProviders() {
		return rloProviders;
	}

	public void setRloProviders(Boolean rloProviders) {
		this.rloProviders = rloProviders;
	}
	@Column
	public Boolean getTs() {
		return ts;
	}

	public void setTs(Boolean ts) {
		this.ts = ts;
	}
	@Column
	public Boolean getRap() {
		return rap;
	}

	public void setRap(Boolean rap) {
		this.rap = rap;
	}
	@Column
	public String getFixPricingforOrigOrDest() {
		return fixPricingforOrigOrDest;
	}

	public void setFixPricingforOrigOrDest(String fixPricingforOrigOrDest) {
		this.fixPricingforOrigOrDest = fixPricingforOrigOrDest;
	}
	@Column
	public Boolean getAir() {
		return air;
	}

	public void setAir(Boolean air) {
		this.air = air;
	}
	@Column
	public Boolean getSea() {
		return sea;
	}

	public void setSea(Boolean sea) {
		this.sea = sea;
	}
	@Column
	public Boolean getPermStorage() {
		return permStorage;
	}

	public void setPermStorage(Boolean permStorage) {
		this.permStorage = permStorage;
	}
	@Column
	public String getSit() {
		return sit;
	}

	public void setSit(String sit) {
		this.sit = sit;
	}
	@Column
	public Boolean getSingle() {
		return single;
	}

	public void setSingle(Boolean single) {
		this.single = single;
	}
	@Column
	public Boolean getMarried() {
		return married;
	}

	public void setMarried(Boolean married) {
		this.married = married;
	}
	@Column
	public Boolean getMarriedWithChild() {
		return marriedWithChild;
	}

	public void setMarriedWithChild(Boolean marriedWithChild) {
		this.marriedWithChild = marriedWithChild;
	}
	@Column
	public String getSeaAllowanceMatrix() {
		return seaAllowanceMatrix;
	}

	public void setSeaAllowanceMatrix(String seaAllowanceMatrix) {
		this.seaAllowanceMatrix = seaAllowanceMatrix;
	}

	public Boolean getContractInPlaceY() {
		return contractInPlaceY;
	}

	public void setContractInPlaceY(Boolean contractInPlaceY) {
		this.contractInPlaceY = contractInPlaceY;
	}

	public Boolean getContractInPlaceN() {
		return contractInPlaceN;
	}

	public void setContractInPlaceN(Boolean contractInPlaceN) {
		this.contractInPlaceN = contractInPlaceN;
	}
	@Column
	public String getAirDesc() {
		return airDesc;
	}

	public void setAirDesc(String airDesc) {
		this.airDesc = airDesc;
	}
	@Column
	public String getSeaDesc() {
		return seaDesc;
	}

	public void setSeaDesc(String seaDesc) {
		this.seaDesc = seaDesc;
	}
	@Column
	public String getSingleDesc() {
		return singleDesc;
	}

	public void setSingleDesc(String singleDesc) {
		this.singleDesc = singleDesc;
	}
	@Column
	public String getMarriedDesc() {
		return marriedDesc;
	}

	public void setMarriedDesc(String marriedDesc) {
		this.marriedDesc = marriedDesc;
	}
	@Column
	public String getMarriedWithChildDesc() {
		return marriedWithChildDesc;
	}

	public void setMarriedWithChildDesc(String marriedWithChildDesc) {
		this.marriedWithChildDesc = marriedWithChildDesc;
	}
	@Column
	public String getAccountPointOfContact() {
		return accountPointOfContact;
	}

	public void setAccountPointOfContact(String accountPointOfContact) {
		this.accountPointOfContact = accountPointOfContact;
	}
	@Column
	public String getAccountEmail() {
		return accountEmail;
	}

	public void setAccountEmail(String accountEmail) {
		this.accountEmail = accountEmail;
	}
	@Column
	public String getAccountPhone() {
		return accountPhone;
	}

	public void setAccountPhone(String accountPhone) {
		this.accountPhone = accountPhone;
	}
	@Column
	public String getRloCompanyInvolved() {
		return rloCompanyInvolved;
	}

	public void setRloCompanyInvolved(String rloCompanyInvolved) {
		this.rloCompanyInvolved = rloCompanyInvolved;
	}
	@Column
	public String getRloCompanyInvolvedPhone() {
		return rloCompanyInvolvedPhone;
	}

	public void setRloCompanyInvolvedPhone(String rloCompanyInvolvedPhone) {
		this.rloCompanyInvolvedPhone = rloCompanyInvolvedPhone;
	}
	@Column
	public String getMainContact() {
		return mainContact;
	}

	public void setMainContact(String mainContact) {
		this.mainContact = mainContact;
	}
	@Column
	public String getMainContactEmail() {
		return mainContactEmail;
	}

	public void setMainContactEmail(String mainContactEmail) {
		this.mainContactEmail = mainContactEmail;
	}

	public Boolean getBillDeliveryPreferenceMail() {
		return billDeliveryPreferenceMail;
	}

	public void setBillDeliveryPreferenceMail(Boolean billDeliveryPreferenceMail) {
		this.billDeliveryPreferenceMail = billDeliveryPreferenceMail;
	}

	public Boolean getBillDeliveryPreferenceEmail() {
		return billDeliveryPreferenceEmail;
	}

	public void setBillDeliveryPreferenceEmail(Boolean billDeliveryPreferenceEmail) {
		this.billDeliveryPreferenceEmail = billDeliveryPreferenceEmail;
	}

	public Boolean getBillDeliveryPreferenceEDI() {
		return billDeliveryPreferenceEDI;
	}

	public void setBillDeliveryPreferenceEDI(Boolean billDeliveryPreferenceEDI) {
		this.billDeliveryPreferenceEDI = billDeliveryPreferenceEDI;
	}

	public String getBillingCutOff() {
		return billingCutOff;
	}

	public void setBillingCutOff(String billingCutOff) {
		this.billingCutOff = billingCutOff;
	}

	public Integer getBillingCutOffDays() {
		return billingCutOffDays;
	}

	public void setBillingCutOffDays(Integer billingCutOffDays) {
		this.billingCutOffDays = billingCutOffDays;
	}

	public Boolean getDaysfromDelivery() {
		return daysfromDelivery;
	}

	public void setDaysfromDelivery(Boolean daysfromDelivery) {
		this.daysfromDelivery = daysfromDelivery;
	}

	public Boolean getSupplimentalBillingAllowed() {
		return supplimentalBillingAllowed;
	}

	public void setSupplimentalBillingAllowed(Boolean supplimentalBillingAllowed) {
		this.supplimentalBillingAllowed = supplimentalBillingAllowed;
	}

	public Boolean getApprovalRequired() {
		return approvalRequired;
	}

	public void setApprovalRequired(Boolean approvalRequired) {
		this.approvalRequired = approvalRequired;
	}

	public String getBillingContact() {
		return billingContact;
	}

	public void setBillingContact(String billingContact) {
		this.billingContact = billingContact;
	}

	public String getBillingContactEmail() {
		return billingContactEmail;
	}

	public void setBillingContactEmail(String billingContactEmail) {
		this.billingContactEmail = billingContactEmail;
	}

	public String getBillingContactPhone() {
		return billingContactPhone;
	}

	public void setBillingContactPhone(String billingContactPhone) {
		this.billingContactPhone = billingContactPhone;
	}

	public Boolean getAccess() {
		return access;
	}

	public void setAccess(Boolean access) {
		this.access = access;
	}

	public Boolean getBillOfLading() {
		return billOfLading;
	}

	public void setBillOfLading(Boolean billOfLading) {
		this.billOfLading = billOfLading;
	}

	public Boolean getCubeSheet() {
		return cubeSheet;
	}

	public void setCubeSheet(Boolean cubeSheet) {
		this.cubeSheet = cubeSheet;
	}

	public Boolean getAirwaysBill() {
		return airwaysBill;
	}

	public void setAirwaysBill(Boolean airwaysBill) {
		this.airwaysBill = airwaysBill;
	}

	public Boolean getRated() {
		return rated;
	}

	public void setRated(Boolean rated) {
		this.rated = rated;
	}


	public Boolean getProofOfPmt() {
		return proofOfPmt;
	}

	public void setProofOfPmt(Boolean proofOfPmt) {
		this.proofOfPmt = proofOfPmt;
	}

	public Boolean getApprovalRequiredRated() {
		return approvalRequiredRated;
	}

	public void setApprovalRequiredRated(Boolean approvalRequiredRated) {
		this.approvalRequiredRated = approvalRequiredRated;
	}

	public Boolean getApprovalRequiredUnrated() {
		return approvalRequiredUnrated;
	}

	public void setApprovalRequiredUnrated(Boolean approvalRequiredUnrated) {
		this.approvalRequiredUnrated = approvalRequiredUnrated;
	}

	public Boolean getApprovalRequiredProofOfPmt() {
		return approvalRequiredProofOfPmt;
	}

	public void setApprovalRequiredProofOfPmt(Boolean approvalRequiredProofOfPmt) {
		this.approvalRequiredProofOfPmt = approvalRequiredProofOfPmt;
	}

	public Boolean getExcApprovals() {
		return excApprovals;
	}

	public void setExcApprovals(Boolean excApprovals) {
		this.excApprovals = excApprovals;
	}

	public Boolean getBillingOthers() {
		return billingOthers;
	}

	public void setBillingOthers(Boolean billingOthers) {
		this.billingOthers = billingOthers;
	}

	public Boolean getTarrifRateSheet() {
		return tarrifRateSheet;
	}

	public void setTarrifRateSheet(Boolean tarrifRateSheet) {
		this.tarrifRateSheet = tarrifRateSheet;
	}

	public Boolean getAuth() {
		return auth;
	}

	public void setAuth(Boolean auth) {
		this.auth = auth;
	}

	public Boolean getOps() {
		return ops;
	}

	public void setOps(Boolean ops) {
		this.ops = ops;
	}

	public Boolean getInsp() {
		return insp;
	}

	public void setInsp(Boolean insp) {
		this.insp = insp;
	}

	public Boolean getNvtd() {
		return nvtd;
	}

	public void setNvtd(Boolean nvtd) {
		this.nvtd = nvtd;
	}

	public Boolean getNvtv() {
		return nvtv;
	}

	public void setNvtv(Boolean nvtv) {
		this.nvtv = nvtv;
	}

	public Boolean getWtct() {
		return wtct;
	}

	public void setWtct(Boolean wtct) {
		this.wtct = wtct;
	}

	public Boolean getOfficialReFAdditional() {
		return officialReFAdditional;
	}

	public void setOfficialReFAdditional(Boolean officialReFAdditional) {
		this.officialReFAdditional = officialReFAdditional;
	}

	public Boolean getCrateList() {
		return crateList;
	}

	public void setCrateList(Boolean crateList) {
		this.crateList = crateList;
	}

	public Boolean getThirdPartyDocuments() {
		return thirdPartyDocuments;
	}

	public void setThirdPartyDocuments(Boolean thirdPartyDocuments) {
		this.thirdPartyDocuments = thirdPartyDocuments;
	}

	public Boolean getBackUp() {
		return backUp;
	}

	public void setBackUp(Boolean backUp) {
		this.backUp = backUp;
	}

	public String getSpecialInvoiceInstructions() {
		return specialInvoiceInstructions;
	}

	public void setSpecialInvoiceInstructions(String specialInvoiceInstructions) {
		this.specialInvoiceInstructions = specialInvoiceInstructions;
	}

	public Boolean getUnrated() {
		return unrated;
	}

	public void setUnrated(Boolean unrated) {
		this.unrated = unrated;
	}

	public String getPathforPricing() {
		return pathforPricing;
	}

	public void setPathforPricing(String pathforPricing) {
		this.pathforPricing = pathforPricing;
	}

	public Boolean getContratedYes() {
		return contratedYes;
	}

	public void setContratedYes(Boolean contratedYes) {
		this.contratedYes = contratedYes;
	}

	public Boolean getContratedNo() {
		return contratedNo;
	}

	public void setContratedNo(Boolean contratedNo) {
		this.contratedNo = contratedNo;
	}

	public Boolean getTriRegional() {
		return triRegional;
	}

	public void setTriRegional(Boolean triRegional) {
		this.triRegional = triRegional;
	}

	public Boolean getIndBids() {
		return indBids;
	}

	public void setIndBids(Boolean indBids) {
		this.indBids = indBids;
	}

	public Boolean getCostPls() {
		return costPls;
	}

	public void setCostPls(Boolean costPls) {
		this.costPls = costPls;
	}

	public Boolean getRateMatrix() {
		return rateMatrix;
	}

	public void setRateMatrix(Boolean rateMatrix) {
		this.rateMatrix = rateMatrix;
	}

	public Boolean getOnlineRateFill() {
		return onlineRateFill;
	}

	public void setOnlineRateFill(Boolean onlineRateFill) {
		this.onlineRateFill = onlineRateFill;
	}

	public Boolean getPricingOther() {
		return pricingOther;
	}

	public void setPricingOther(Boolean pricingOther) {
		this.pricingOther = pricingOther;
	}

	public String getPricingOtherDesc() {
		return pricingOtherDesc;
	}

	public void setPricingOtherDesc(String pricingOtherDesc) {
		this.pricingOtherDesc = pricingOtherDesc;
	}

	public String getTriRegionalModelDesc() {
		return triRegionalModelDesc;
	}

	public void setTriRegionalModelDesc(String triRegionalModelDesc) {
		this.triRegionalModelDesc = triRegionalModelDesc;
	}

	public Boolean getoA() {
		return oA;
	}

	public void setoA(Boolean oA) {
		this.oA = oA;
	}

	public Boolean getdA() {
		return dA;
	}

	public void setdA(Boolean dA) {
		this.dA = dA;
	}

	public Boolean getFreight() {
		return freight;
	}

	public void setFreight(Boolean freight) {
		this.freight = freight;
	}

	public Boolean getServicesOther() {
		return servicesOther;
	}

	public void setServicesOther(Boolean servicesOther) {
		this.servicesOther = servicesOther;
	}

	public Double getMinMargin() {
		return minMargin;
	}

	public void setMinMargin(Double minMargin) {
		this.minMargin = minMargin;
	}

	public Double getAdminFee() {
		return adminFee;
	}

	public void setAdminFee(Double adminFee) {
		this.adminFee = adminFee;
	}

	public Boolean getProvidedRequuired() {
		return providedRequuired;
	}

	public void setProvidedRequuired(Boolean providedRequuired) {
		this.providedRequuired = providedRequuired;
	}

	public Boolean getDiscontAgrement() {
		return discontAgrement;
	}

	public void setDiscontAgrement(Boolean discontAgrement) {
		this.discontAgrement = discontAgrement;
	}

	public Boolean getProvidertoBeUsed() {
		return providertoBeUsed;
	}

	public void setProvidertoBeUsed(Boolean providertoBeUsed) {
		this.providertoBeUsed = providertoBeUsed;
	}

	public Boolean getWeighttktRequired() {
		return weighttktRequired;
	}

	public void setWeighttktRequired(Boolean weighttktRequired) {
		this.weighttktRequired = weighttktRequired;
	}

	public Boolean getFortyfivePer() {
		return fortyfivePer;
	}

	public void setFortyfivePer(Boolean fortyfivePer) {
		this.fortyfivePer = fortyfivePer;
	}

	public Boolean getFiftyFivePer() {
		return fiftyFivePer;
	}

	public void setFiftyFivePer(Boolean fiftyFivePer) {
		this.fiftyFivePer = fiftyFivePer;
	}

	public Boolean getCbmRequired() {
		return cbmRequired;
	}

	public void setCbmRequired(Boolean cbmRequired) {
		this.cbmRequired = cbmRequired;
	}

	public Boolean getReWeight() {
		return reWeight;
	}

	public void setReWeight(Boolean reWeight) {
		this.reWeight = reWeight;
	}


	

	public Boolean getSeperateLinePricing() {
		return seperateLinePricing;
	}

	public void setSeperateLinePricing(Boolean seperateLinePricing) {
		this.seperateLinePricing = seperateLinePricing;
	}

	

	

	public Boolean getAtSurvey() {
		return atSurvey;
	}

	public void setAtSurvey(Boolean atSurvey) {
		this.atSurvey = atSurvey;
	}

	public Boolean getAtSurveyWith() {
		return atSurveyWith;
	}

	public void setAtSurveyWith(Boolean atSurveyWith) {
		this.atSurveyWith = atSurveyWith;
	}

	public Boolean getWithFinals() {
		return withFinals;
	}

	public void setWithFinals(Boolean withFinals) {
		this.withFinals = withFinals;
	}

	public String getRestrItemsDesc() {
		return restrItemsDesc;
	}

	public void setRestrItemsDesc(String restrItemsDesc) {
		this.restrItemsDesc = restrItemsDesc;
	}

	public String getRestrItemsList() {
		return restrItemsList;
	}

	public void setRestrItemsList(String restrItemsList) {
		this.restrItemsList = restrItemsList;
	}

	public Boolean getDtoDALL() {
		return dtoDALL;
	}

	public void setDtoDALL(Boolean dtoDALL) {
		this.dtoDALL = dtoDALL;
	}

	public Boolean getDtoD() {
		return dtoD;
	}

	public void setDtoD(Boolean dtoD) {
		this.dtoD = dtoD;
	}

	public Boolean getDtoDWithOceanFreight() {
		return dtoDWithOceanFreight;
	}

	public void setDtoDWithOceanFreight(Boolean dtoDWithOceanFreight) {
		this.dtoDWithOceanFreight = dtoDWithOceanFreight;
	}

	public Boolean getDtoDWithOcean() {
		return dtoDWithOcean;
	}

	public void setDtoDWithOcean(Boolean dtoDWithOcean) {
		this.dtoDWithOcean = dtoDWithOcean;
	}

	public String getFrieghtInvoice() {
		return frieghtInvoice;
	}

	public void setFrieghtInvoice(String frieghtInvoice) {
		this.frieghtInvoice = frieghtInvoice;
	}

	public Boolean getTranInsY() {
		return tranInsY;
	}

	public void setTranInsY(Boolean tranInsY) {
		this.tranInsY = tranInsY;
	}

	public Boolean getTranInsN() {
		return tranInsN;
	}

	public void setTranInsN(Boolean tranInsN) {
		this.tranInsN = tranInsN;
	}

	public Boolean getBipY() {
		return bipY;
	}

	public void setBipY(Boolean bipY) {
		this.bipY = bipY;
	}

	public Boolean getBipN() {
		return bipN;
	}

	public void setBipN(Boolean bipN) {
		this.bipN = bipN;
	}

	public Boolean getSelfInsY() {
		return selfInsY;
	}

	public void setSelfInsY(Boolean selfInsY) {
		this.selfInsY = selfInsY;
	}

	public Boolean getSelfInsN() {
		return selfInsN;
	}

	public void setSelfInsN(Boolean selfInsN) {
		this.selfInsN = selfInsN;
	}
	@Column
	public String getInsurer() {
		return insurer;
	}

	public void setInsurer(String insurer) {
		this.insurer = insurer;
	}

	public Double getSubrogationAmnt() {
		return subrogationAmnt;
	}

	public void setSubrogationAmnt(Double subrogationAmnt) {
		this.subrogationAmnt = subrogationAmnt;
	}

	public Boolean getValuedInv() {
		return valuedInv;
	}

	public void setValuedInv(Boolean valuedInv) {
		this.valuedInv = valuedInv;
	}

	public Boolean getHighValuedInv() {
		return highValuedInv;
	}

	public void setHighValuedInv(Boolean highValuedInv) {
		this.highValuedInv = highValuedInv;
	}

	public Boolean getDocumentReqOthr() {
		return documentReqOthr;
	}

	public void setDocumentReqOthr(Boolean documentReqOthr) {
		this.documentReqOthr = documentReqOthr;
	}
	@Column
	public String getDocumentReqOthrDesc() {
		return documentReqOthrDesc;
	}

	public void setDocumentReqOthrDesc(String documentReqOthrDesc) {
		this.documentReqOthrDesc = documentReqOthrDesc;
	}

	public Double getMaxInsurance() {
		return maxInsurance;
	}

	public void setMaxInsurance(Double maxInsurance) {
		this.maxInsurance = maxInsurance;
	}

	public Boolean getCollectY() {
		return collectY;
	}

	public void setCollectY(Boolean collectY) {
		this.collectY = collectY;
	}

	public Boolean getCollectN() {
		return collectN;
	}

	public void setCollectN(Boolean collectN) {
		this.collectN = collectN;
	}

	public String getInsSendDesc() {
		return InsSendDesc;
	}

	public void setInsSendDesc(String insSendDesc) {
		InsSendDesc = insSendDesc;
	}


	public Boolean getByUs() {
		return byUs;
	}

	public void setByUs(Boolean byUs) {
		this.byUs = byUs;
	}

	public Boolean getByAcc() {
		return byAcc;
	}

	public void setByAcc(Boolean byAcc) {
		this.byAcc = byAcc;
	}

	public Boolean getThroughCarr() {
		return throughCarr;
	}

	public void setThroughCarr(Boolean throughCarr) {
		this.throughCarr = throughCarr;
	}

	public Boolean getInHouse() {
		return inHouse;
	}

	public void setInHouse(Boolean inHouse) {
		this.inHouse = inHouse;
	}

	public Boolean getClaimY() {
		return claimY;
	}

	public void setClaimY(Boolean claimY) {
		this.claimY = claimY;
	}

	public Boolean getClaimN() {
		return claimN;
	}

	public void setClaimN(Boolean claimN) {
		this.claimN = claimN;
	}

	public String getTypeIns() {
		return typeIns;
	}

	public void setTypeIns(String typeIns) {
		this.typeIns = typeIns;
	}

	public String getChargeDesc() {
		return chargeDesc;
	}

	public void setChargeDesc(String chargeDesc) {
		this.chargeDesc = chargeDesc;
	}

	public Boolean getFullValuedInv() {
		return fullValuedInv;
	}

	public void setFullValuedInv(Boolean fullValuedInv) {
		this.fullValuedInv = fullValuedInv;
	}

	public String getValueBased() {
		return valueBased;
	}

	public void setValueBased(String valueBased) {
		this.valueBased = valueBased;
	}

	public String getValueBasedWHL() {
		return valueBasedWHL;
	}

	public void setValueBasedWHL(String valueBasedWHL) {
		this.valueBasedWHL = valueBasedWHL;
	}

	public String getOtherCarrier() {
		return otherCarrier;
	}

	public void setOtherCarrier(String otherCarrier) {
		this.otherCarrier = otherCarrier;
	}

	public Boolean getUnInsurableItems() {
		return unInsurableItems;
	}

	public void setUnInsurableItems(Boolean unInsurableItems) {
		this.unInsurableItems = unInsurableItems;
	}

	public String getListUnSurableItems() {
		return listUnSurableItems;
	}

	public void setListUnSurableItems(String listUnSurableItems) {
		this.listUnSurableItems = listUnSurableItems;
	}

	public Boolean getPermStOY() {
		return permStOY;
	}

	public void setPermStOY(Boolean permStOY) {
		this.permStOY = permStOY;
	}

	public Boolean getPermStON() {
		return permStON;
	}

	public void setPermStON(Boolean permStON) {
		this.permStON = permStON;
	}
	@Column
	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}
	@Column
	public String getSuperGroup() {
		return superGroup;
	}

	public void setSuperGroup(String superGroup) {
		this.superGroup = superGroup;
	}

	public Boolean getElectronic() {
		return electronic;
	}

	public void setElectronic(Boolean electronic) {
		this.electronic = electronic;
	}

	public Boolean getHardCopy() {
		return hardCopy;
	}

	public void setHardCopy(Boolean hardCopy) {
		this.hardCopy = hardCopy;
	}

	public Boolean getBatchInvY() {
		return batchInvY;
	}

	public void setBatchInvY(Boolean batchInvY) {
		this.batchInvY = batchInvY;
	}

	public Boolean getBatchInvN() {
		return batchInvN;
	}

	public void setBatchInvN(Boolean batchInvN) {
		this.batchInvN = batchInvN;
	}

	public Boolean getWithin30Days() {
		return within30Days;
	}

	public void setWithin30Days(Boolean within30Days) {
		this.within30Days = within30Days;
	}

	public Boolean getWithin45Days() {
		return within45Days;
	}

	public void setWithin45Days(Boolean within45Days) {
		this.within45Days = within45Days;
	}

	public Boolean getWithin60Days() {
		return within60Days;
	}

	public void setWithin60Days(Boolean within60Days) {
		this.within60Days = within60Days;
	}

	public Boolean getWithin90Days() {
		return within90Days;
	}

	public void setWithin90Days(Boolean within90Days) {
		this.within90Days = within90Days;
	}

	public Boolean getWithinOther() {
		return withinOther;
	}

	public void setWithinOther(Boolean withinOther) {
		this.withinOther = withinOther;
	}

	public Boolean getCreditY() {
		return creditY;
	}

	public void setCreditY(Boolean creditY) {
		this.creditY = creditY;
	}

	public Boolean getCreditN() {
		return creditN;
	}

	public void setCreditN(Boolean creditN) {
		this.creditN = creditN;
	}

	public String getUploadDetails() {
		return uploadDetails;
	}

	public void setUploadDetails(String uploadDetails) {
		this.uploadDetails = uploadDetails;
	}

	public String getInvoiceDelDateDesc() {
		return invoiceDelDateDesc;
	}

	public void setInvoiceDelDateDesc(String invoiceDelDateDesc) {
		this.invoiceDelDateDesc = invoiceDelDateDesc;
	}

	public String getCreditTerms() {
		return creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public String getAmountApproved() {
		return amountApproved;
	}

	public void setAmountApproved(String amountApproved) {
		this.amountApproved = amountApproved;
	}

	public String getAuditResponseDesc() {
		return auditResponseDesc;
	}

	public void setAuditResponseDesc(String auditResponseDesc) {
		this.auditResponseDesc = auditResponseDesc;
	}

	public String getArContact() {
		return arContact;
	}

	public void setArContact(String arContact) {
		this.arContact = arContact;
	}

	public String getArContactEmail() {
		return arContactEmail;
	}

	public void setArContactEmail(String arContactEmail) {
		this.arContactEmail = arContactEmail;
	}

	public String getArContactPhone() {
		return arContactPhone;
	}

	public void setArContactPhone(String arContactPhone) {
		this.arContactPhone = arContactPhone;
	}

	public Boolean getStatusUpdateRequiredY() {
		return statusUpdateRequiredY;
	}

	public void setStatusUpdateRequiredY(Boolean statusUpdateRequiredY) {
		this.statusUpdateRequiredY = statusUpdateRequiredY;
	}

	public Boolean getStatusUpdateRequiredN() {
		return statusUpdateRequiredN;
	}

	public void setStatusUpdateRequiredN(Boolean statusUpdateRequiredN) {
		this.statusUpdateRequiredN = statusUpdateRequiredN;
	}

	public Boolean getReportY() {
		return reportY;
	}

	public void setReportY(Boolean reportY) {
		this.reportY = reportY;
	}

	public Boolean getReportN() {
		return reportN;
	}

	public void setReportN(Boolean reportN) {
		this.reportN = reportN;
	}

	public String getReportName1() {
		return reportName1;
	}

	public void setReportName1(String reportName1) {
		this.reportName1 = reportName1;
	}

	public String getReportName2() {
		return reportName2;
	}

	public void setReportName2(String reportName2) {
		this.reportName2 = reportName2;
	}

	public String getReportName3() {
		return reportName3;
	}

	public void setReportName3(String reportName3) {
		this.reportName3 = reportName3;
	}

	public String getReportName4() {
		return reportName4;
	}

	public void setReportName4(String reportName4) {
		this.reportName4 = reportName4;
	}

	public String getDeliverTo1() {
		return deliverTo1;
	}

	public void setDeliverTo1(String deliverTo1) {
		this.deliverTo1 = deliverTo1;
	}

	public String getDeliverTo2() {
		return deliverTo2;
	}

	public void setDeliverTo2(String deliverTo2) {
		this.deliverTo2 = deliverTo2;
	}

	public String getDeliverTo3() {
		return deliverTo3;
	}

	public void setDeliverTo3(String deliverTo3) {
		this.deliverTo3 = deliverTo3;
	}

	public String getDeliverTo4() {
		return deliverTo4;
	}

	public void setDeliverTo4(String deliverTo4) {
		this.deliverTo4 = deliverTo4;
	}

	public String getFrequency1() {
		return frequency1;
	}

	public void setFrequency1(String frequency1) {
		this.frequency1 = frequency1;
	}

	public String getFrequency2() {
		return frequency2;
	}

	public void setFrequency2(String frequency2) {
		this.frequency2 = frequency2;
	}

	public String getFrequency3() {
		return frequency3;
	}

	public void setFrequency3(String frequency3) {
		this.frequency3 = frequency3;
	}

	public String getFrequency4() {
		return frequency4;
	}

	public void setFrequency4(String frequency4) {
		this.frequency4 = frequency4;
	}

	public String getDueDate1() {
		return dueDate1;
	}

	public void setDueDate1(String dueDate1) {
		this.dueDate1 = dueDate1;
	}

	public String getDueDate2() {
		return dueDate2;
	}

	public void setDueDate2(String dueDate2) {
		this.dueDate2 = dueDate2;
	}

	public String getDueDate3() {
		return dueDate3;
	}

	public void setDueDate3(String dueDate3) {
		this.dueDate3 = dueDate3;
	}

	public String getDueDate4() {
		return dueDate4;
	}

	public void setDueDate4(String dueDate4) {
		this.dueDate4 = dueDate4;
	}

	public String getKpi() {
		return kpi;
	}

	public void setKpi(String kpi) {
		this.kpi = kpi;
	}

	public String getSelfInsuredInv() {
		return selfInsuredInv;
	}

	public void setSelfInsuredInv(String selfInsuredInv) {
		this.selfInsuredInv = selfInsuredInv;
	}

	public String getSelfInsured() {
		return selfInsured;
	}

	public void setSelfInsured(String selfInsured) {
		this.selfInsured = selfInsured;
	}

	public String getSiInsured() {
		return siInsured;
	}

	public void setSiInsured(String siInsured) {
		this.siInsured = siInsured;
	}

	public String getLnthOfContract() {
		return lnthOfContract;
	}

	public void setLnthOfContract(String lnthOfContract) {
		this.lnthOfContract = lnthOfContract;
	}
	@Column
	public String getBipYComments() {
		return bipYComments;
	}

	public void setBipYComments(String bipYComments) {
		this.bipYComments = bipYComments;
	}
	@Column
	public String getIntlBillingAdditionalInfo() {
		return intlBillingAdditionalInfo;
	}

	public void setIntlBillingAdditionalInfo(String intlBillingAdditionalInfo) {
		this.intlBillingAdditionalInfo = intlBillingAdditionalInfo;
	}
	@Column
	public String getAuthorizationsExceptions() {
		return authorizationsExceptions;
	}

	public void setAuthorizationsExceptions(String authorizationsExceptions) {
		this.authorizationsExceptions = authorizationsExceptions;
	}
	@Column
	public String getIntlTransitInsAddInfo() {
		return intlTransitInsAddInfo;
	}

	public void setIntlTransitInsAddInfo(String intlTransitInsAddInfo) {
		this.intlTransitInsAddInfo = intlTransitInsAddInfo;
	}
	@Column
	public String getReportAddInfo() {
		return reportAddInfo;
	}

	public void setReportAddInfo(String reportAddInfo) {
		this.reportAddInfo = reportAddInfo;
	}
	@Column
	public String getInvoiceAddInfo() {
		return invoiceAddInfo;
	}

	public void setInvoiceAddInfo(String invoiceAddInfo) {
		this.invoiceAddInfo = invoiceAddInfo;
	}

}
