<%@ include file="/common/taglibs.jsp"%>
 
<head>

    <title><fmt:message key="userIDHelp.title"/></title>
    <meta name="heading" content="<fmt:message key='userIDHelp.heading'/>"/>
    <meta name="menu" content="UserIDHelp"/>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
  
    <script language="javascript" type="text/javascript">
     
 function validateUser(){
	var email = document.forms['userIDHelpForm'].elements['email'].value.replace(/^\s*|\s*$/g,'');
	var fName = document.forms['userIDHelpForm'].elements['fName'].value.replace(/^\s*|\s*$/g,'');
    var lName = document.forms['userIDHelpForm'].elements['lName'].value.replace(/^\s*|\s*$/g,'');
    if(trim(email) =="" || trim(fName) =="" || trim(lName) ==""){
	alert("Please fill the required field.");
	return false;
	}
	else{
	 location.href= "signup.html?email="+email+"&bypass_sessionfilter=YES";
	 document.forms['userIDHelpForm'].submit();
	}
}

function validateUserManadatoryFields(){
	var hintAns = document.forms['userIDHelpForm'].elements['hintAns'].value.replace(/^\s*|\s*$/g,'');
	var username = document.forms['userIDHelpForm'].elements['username'].value.replace(/^\s*|\s*$/g,'');
	if(trim(username) ==""){
	alert("Please fill the required field.");
	return false;
	}
	if(trim(hintAns) ==""){
	alert("Please fill the required field.");
	return false;
	}
	else{
	 location.href= "signup.html?bypass_sessionfilter=YES";
	 document.forms['userIDHelpForm'].submit();
	}
}

function validateUserNames(){
	var username = document.forms['userIDHelpForm'].elements['username'].value.replace(/^\s*|\s*$/g,'');
	if(trim(username) ==""){
	alert("Please fill the required field.");
	return false;
	}
	else{
	 location.href= "signup.html?bypass_sessionfilter=YES";
	 document.forms['userIDHelpForm'].submit();
	}
}

function trim(str) 
{ 
if (str != null) 
{
var i;
for (i=0; i<str; i++)
{
if(str.charAt(i)!=" ")
{
str=str.substring(0,i+1);
break;
}
}
if
(str.charAt(0)==" ")
{
return "";
}
else
{return str;
}}}
function validateUserPassword(){
	var target = document.forms['userIDHelpForm'].elements['target'].value;
	 location.href= "signup.html?target="+target+"&bypass_sessionfilter=YES";
	 document.forms['userIDHelpForm'].submit();
}
function validateManadatoryField(){
	var email = document.forms['userIDHelpForm'].elements['email'].value;
	var fName = document.forms['userIDHelpForm'].elements['fName'].value;
    var lName = document.forms['userIDHelpForm'].elements['lName'].value;
   
    if(email =="")
    {
	alert("Please fill the required field.");
	document.forms['userIDHelpForm'].elements['email'].value="";
	}
	if(fName =="")
	{
	alert("Please fill the required field.");
	document.forms['userIDHelpForm'].elements['fName'].value="";
	}
	if(lName =="")
	{
	alert("Please fill the required field.");
	document.forms['userIDHelpForm'].elements['lName'].value="";
	}
	
}
function findHintQuestion()
{
    var username = document.forms['userIDHelpForm'].elements['username'].value;
	document.forms['userIDHelpForm'].submit();
}


 </script>

<style>
div#branding{float: left;width: 100%;margin: 0;text-align: left;height:0px;}        
div#footer div#divider div {border-top:5px solid #EBF5FC;margin:1px 0 0;}
#clearfooter {height:0;}

body{ background-color:#dee9f6; text-align:left; margin:0px; padding:0px; background-image:url(images/indexbg-final.gif); background-repeat:repeat-x;}

#logincontainer{ width:925px; height:375px; background-image:url(images/loginbg-opt2.png); background-repeat:no-repeat; margin-top:-7px;}
#errormessage {width:300px; list-style:none; padding-right:20px; float:right; position:absolute; top:95px;!top:100px; left:520px;!left:510px;}
		
.cssbutton{width: 55px;height: 25px;padding:0px;font-family:'arial';font-weight:normal;color:#000;font-size:12px;background:url(/redsky/images/btn_bg.gif) repeat-x;
background-color:#e7e7e7;border:1px solid #cccccc;}
.cssbutton:hover{color: black; background:url(/redsky/images/btn_hoverbg.gif) repeat-x; background-color: #c1c2bf; }

.textbox_username {background-color:#E7E7E7;border:1px solid #B2B2B2;color:#616161;font-family:Arial,Helvetica,sans-serif;font-size:12px;width:135px;
background: #E7E7E7 url('images/user.png') no-repeat;background-position: 1 1;padding-left: 19px;}
.textbox_password {background-color:#E7E7E7;border:1px solid #B2B2B2;color:#616161;font-family:Arial,Helvetica,sans-serif;font-size:12px;width:135px;
background: #E7E7E7 url('images/icon_password.png') no-repeat;background-position: 1 1;padding-left: 19px;}

div.error, span.error, li.error, div.message {-moz-background-clip:border;-moz-background-inline-policy:continuous;-moz-background-origin:padding;
background:#B3DAF4 none repeat scroll 0 0;border:1px solid #000000;color:#000000;font-family:Arial,Helvetica,sans-serif;font-weight:normal;margin:10px auto;
margin-top:80px;padding:3px;text-align:center;vertical-align:bottom;width:500px;z-index:1000;}

.supportalert{ background:#FFDFDF none repeat scroll 0%;border:1px solid #DF5353;color:#000000;font-family:Arial,Helvetica,sans-serif;font-weight:normal;
font-size:1em;padding:2px;text-align:center;vertical-align:bottom;margin-top: 5px;margin-bottom: 0px;}

#indexlogo {width:800px;float:left;	text-align:right;margin-right:0px;margin-top:10px;padding-top:0px;padding-right:40px;padding-bottom:0px;padding-left:0px;}

#leftcolumn {float:left;margin-top:50px;top:150px;width:380px;padding-right: 40px;}

#rightcolumn { width:450px; float:left; margin-top:25px;}

.login-heading {border-bottom:1px solid #71B9C3;color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:13px;font-weight:bold;margin:0 3px 0 10px;padding:5px 0 4px;
text-align:left;}

.signup-heading {color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;padding:5px 0 3px;text-align:right;}

.left-head {color:#71B9C3;font-family:Arial,Helvetica,sans-serif;font-size:28px;font-weight:bold;line-height:35px;}
  </style>
  <noscript>
  <style type="text/css">
  #overlay{ display:none !important;}
 .opaque{ display:none !important;}
 </style>
 </noscript>
    
    <script>
    function clear_fileds(){
       	document.forms['loginForm'].elements['j_username'].value = "";
    	document.forms['loginForm'].elements['j_password'].value = "";
    }
    function goToLink(){
       	alert("ghjsad");
       	location.href='passwordHelp.html';
    }
    </script>
    <script language="javascript" type="text/javascript">
 	function forgotPassword()
 	{
 		var username = document.forms['loginForm'].elements['j_username'].value;
 		if(username != "")
 		{
 			document.forms['userIDHelpForm'].elements['j_username'].value = document.forms['loginForm'].elements['j_username'].value
 			document.forms['userIDHelpForm'].submit();
 		}
 		else
 		{
 			location.href="signup.html?target=pwd1&bypass_sessionfilter=YES";
 		}
 	}

 </script>
</head>
<body id="userIDHelp"/>
<div class="separator"></div>
<div id="wrapper">

<div id="logincontainer">

<div id="indexlogo"><img src="${pageContext.request.contextPath}/images/indexlogo.gif"  style="cursor: default;"/></div>

<div id="leftcolumn" >
  <table width="360" border="0" align="right" cellpadding="0" cellspacing="0">
    <tr>
      <td align="right" class="left-head" colspan="2">&nbsp;<img src="images/redsky-text.png" style="cursor: default;"/></td>
    </tr>
    <tr>
      <td align="left" class="signup-heading">Username & Password Assistance</td>
      <td width="23">&nbsp;<img src="images/right-arrow.gif" width="19" height="17" style="cursor: default;vertical-align:top;"/></td>
    </tr>
    </table></td>
    </tr>
  </table>
</div>

     <c:if  test="${key=='Incorrect Information. Please contact'}">  <div class="supportalert" style="float: left;margin-left:10px; !margin-left:45px;" ><img src="images/alert.gif" style="">
    ${key} <a href="<c:url value='mailto:support@redskymobility.com'/>" /><i>support@redskymobility.com</i></a></div>
   <br/>
</c:if>

<%if(request.getParameter("target").equalsIgnoreCase("upwd")){ %>
		<%-- <s:form name="userIDHelpForm" action="signup.html?bypass_sessionfilter=YES" method="post" validate="true" >
		<s:hidden name="countVar" value="${countVar}"/>
		<c:set var="target" value="<%=request.getParameter("target") %>" />
		<s:hidden name="target" value="<%=request.getParameter("target") %>" />
<div id="rightcolumn">
 <table  style="border:1px dotted #CFCFCF;" cellspacing="0" cellpadding="0" >
      <tr>
        <td height="160" valign="top" align="left">	
  <table  border="0" cellspacing="1" cellpadding="2">
    <tr>
      <td  class="login-heading" colspan="4" >Please enter your information in the form below.</td>
      
    </tr>
    <tr>
      <td height="15px">&nbsp;</td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td class="content" width="100px" align="right">First Name<font color="#ff0000">*</font></td>
      <td colspan="2"><input type="text" class="textbox" id="j_fName" name="fName" tabindex="1"/></td>
    </tr>
     <tr>
      <td class="content" align="right">Last Name<font color="#ff0000">*</font></td>
      <td colspan="2"><input type="text" class="textbox" id="j_lName" name="lName" tabindex="2"/></td>
    </tr>
     <tr>
      <td class="content" align="right">Primary E-mail<font color="#ff0000">*</font></td>
      <td colspan="2"><input type="text" class="textbox" id="j_email" name="email" tabindex="3"/></td>
    </tr>
    <tr>
      <td colspan="3" class="content" height="5"></td>
      </tr>
     
   <tr>
    <td height="10"></td>
    <td width="60px" align="left"><s:submit cssClass="cssbutton" key="button.submit" cssStyle="width:55px; height:25px" align="left" onclick="return validateUser();"/></td>
    <td align="left" width="200px"><input class="cssbutton" type="reset" name="Clear" value="Clear" ></td>       
   </tr>
  
    <tr>
      <td class="content">&nbsp;</td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td class="content">&nbsp;</td>
      <td colspan="2">&nbsp;</td>
    </tr>
  
  </table>
  </td>
  </tr>
  </table>
  </div>
  </s:form> --%>
   <%}else{ %>
	<s:form name="userIDHelpForm" action="signup.html?bypass_sessionfilter=YES" method="post" validate="true" >
<s:hidden name="countVar" value="${countVar}"/>
<s:hidden name="maxAttempt" value="${maxAttempt}"/>
	<c:if test="${target == 'pwd1'}">
	<s:hidden name="target" value="pwd1" />
	</c:if>
	<c:if test="${target =='pwd'}">
	<s:hidden name="target" value="pwd" />
	</c:if>
	<div id="rightcolumn" >
	 <table  style="border:1px dotted #CFCFCF;" cellspacing="0" cellpadding="0" >
      <tr>
        <td height="160" valign="top" align="left">
   <table  border="0" cellspacing="1" cellpadding="2">
    <tr>
      <td  class="login-heading" colspan="4">Please enter your information in the form below.</td>
      
    </tr>
    <tr>
      <td height="15px">&nbsp;</td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td class="content" width="100px" align="right">User Name<font color="#ff0000">*</font></td>
      <td colspan="2"><c:if test="${target == 'pwd1'}">
	<input type="text" class="textbox" id="j_username" name="username" tabindex="1"/>
	<tr>
	<td height="10"></td>
	<td width="60px" align="left"><s:submit cssClass="cssbutton" key="button.submit" cssStyle="width:55px; height:25px" align="left" onclick="return validateUserNames();"/></td>
    <td align="left" width="250px"><input class="cssbutton" type="reset" name="Clear" value="Clear"></td>
	</tr>
	</c:if>
	<c:if test="${target =='pwd'}">
	<input type="text" class="textbox" id="j_username" name="username" readonly="readonly" tabindex="1"  value="${usernamePersist}"/>
	</c:if>
	</td>
    </tr>
     <c:if test="${target =='pwd'}">
     <tr>
      <td class="content" align="right">Hint Question</td>
      <td colspan="2"> <input type="text" class="textbox" id="j_hintQuestion1" name="hintQuestion1" tabindex="2" readonly="readonly" style="width:300px" value="${hintQues}"/>
      <input type="hidden" class="textbox" id="j_hintQuestion" name="hintQuestion" tabindex="2" value="${hintQues1}"/>
      </td>
    </tr>
     <tr>
      <td class="content" align="right">Hint Answer<font color="#ff0000">*</font></td>
      <td colspan="2"><input type="text" class="textbox" id="j_hintAns" name="hintAns" tabindex="3" maxlength="40" style="width:300px; background-color: #ffffff;"/> </td>
    </tr>
    </c:if>
    <tr>
      <td colspan="3" class="content" height="5"></td>
      </tr>
     
   <tr>
   <c:if test="${target =='pwd'}">
    <td height="10"></td>
    <td width="60px" align="left"><s:submit cssClass="cssbutton" key="button.submit" cssStyle="width:55px; height:25px" align="left" onclick="return validateUserManadatoryFields();"/></td>
    <td align="left" width="250px"><input class="cssbutton" type="reset" name="Clear" value="Clear"></td>
       </c:if>
  </tr>
  
    <tr>
      <td class="content">&nbsp;</td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td class="content">&nbsp;</td>
      <td colspan="2">&nbsp;</td>
    </tr>
  
  </table>
  </td>
  </tr>
  </table>
  </s:form>     
   <%} %>               
 </div>
  </td>
  </tr>
  </table>
  </div>
  