package com.trilasoft.app.webapp.tags;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.util.ExpressionEvaluationUtils;

import com.trilasoft.app.webapp.action.CorpComponentPermissionUtil;

public class AuthorizeByCorpIdTag extends TagSupport {

	// ~ Instance fields
	// ================================================================================================

	private String ifAllGranted = "";

	private String ifAnyGranted = "";

	private String ifNotGranted = "";

	private String componentId = "";

	private CorpComponentPermissionUtil corpComponentPermissionUtil = null;

	private String userCorpID;
	
	public AuthorizeByCorpIdTag(){
	}

	// ~ Methods
	// ========================================================================================================

	public int doStartTag() throws JspException {

		if (((null == ifAllGranted) || "".equals(ifAllGranted))
				&& ((null == ifAnyGranted) || "".equals(ifAnyGranted))
				&& ((null == ifNotGranted) || "".equals(ifNotGranted))) {
			return Tag.SKIP_BODY;
		}
		
		ServletContext context = pageContext.getServletContext();
		XmlWebApplicationContext ctx = (XmlWebApplicationContext) context
				.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);

		corpComponentPermissionUtil = (CorpComponentPermissionUtil) ctx.getBean("corpComponentPermissionUtil");		
		

		final List granted = getCorpAuthoritiesForComponent(componentId);
		
		if (granted == null || granted.isEmpty()) return Tag.EVAL_BODY_INCLUDE;

		final String evaledIfNotGranted = ExpressionEvaluationUtils
				.evaluateString("ifNotGranted", ifNotGranted, pageContext);

		if ((null != evaledIfNotGranted) && !"".equals(evaledIfNotGranted)) {
			List grantedCopy = retainAll(granted,
					parseAuthoritiesString(evaledIfNotGranted));

			if (!grantedCopy.isEmpty()) {
				return Tag.SKIP_BODY;
			}
		}

		final String evaledIfAllGranted = ExpressionEvaluationUtils
				.evaluateString("ifAllGranted", ifAllGranted, pageContext);

		if ((null != evaledIfAllGranted) && !"".equals(evaledIfAllGranted)) {
			if (!granted
					.containsAll(parseAuthoritiesString(evaledIfAllGranted))) {
				return Tag.SKIP_BODY;
			}
		}

		final String evaledIfAnyGranted = ExpressionEvaluationUtils
				.evaluateString("ifAnyGranted", ifAnyGranted, pageContext);

		if ((null != evaledIfAnyGranted) && !"".equals(evaledIfAnyGranted)) {
			List grantedCopy = retainAll(granted,
					parseAuthoritiesString(evaledIfAnyGranted));

			if (grantedCopy.isEmpty()) {
				return Tag.SKIP_BODY;
			}
		}

		return Tag.EVAL_BODY_INCLUDE;
	}

	public String getIfAllGranted() {
		return ifAllGranted;
	}

	public String getIfAnyGranted() {
		return ifAnyGranted;
	}

	public String getIfNotGranted() {
		return ifNotGranted;
	}

	private List getCorpAuthoritiesForComponent(String componentId) {
		Authentication currentUser = SecurityContextHolder.getContext()
				.getAuthentication();

		if (null == currentUser) {
			return Collections.EMPTY_LIST;
		}

		if ((null == currentUser.getAuthorities())
				|| (currentUser.getAuthorities().length < 1)) {
			return Collections.EMPTY_LIST;
		}

		User user = (User) currentUser.getPrincipal();
		String userCorpID = user.getCorpID();

		//List granted = corpComponentPermissionUtil
		//		.getCorpAuthoritiesForComponent(userCorpID, componentId);

		return null;
	}

	private List parseAuthoritiesString(String authorizationsString) {
		final List requiredAuthorities = new ArrayList();
		final String[] authorities = StringUtils
				.commaDelimitedListToStringArray(authorizationsString);

		for (int i = 0; i < authorities.length; i++) {
			String authority = authorities[i];

			String permissionAsString = StringUtils.replace(authority, " ", "");
			permissionAsString = StringUtils.replace(permissionAsString, "\t",
					"");
			permissionAsString = StringUtils.replace(permissionAsString, "\r",
					"");
			permissionAsString = StringUtils.replace(permissionAsString, "\n",
					"");
			permissionAsString = StringUtils.replace(permissionAsString, "\f",
					"");
			Integer permission = (permissionAsString.equals("READ")) ? 2
					: (permissionAsString.equals("READ_WRITE")) ? 6
							: (permissionAsString.equals("NONE")) ? 0 : -1;
			requiredAuthorities.add(permission);
		}

		return requiredAuthorities;
	}

	private List retainAll(final List granted, final List required) {
		List grantedCopy = new ArrayList(granted);
		grantedCopy.retainAll(required);
		return grantedCopy;
	}

	public void setIfAllGranted(String ifAllGranted) throws JspException {
		this.ifAllGranted = ifAllGranted;
	}

	public void setIfAnyGranted(String ifAnyGranted) throws JspException {
		this.ifAnyGranted = ifAnyGranted;
	}

	public void setIfNotGranted(String ifNotGranted) throws JspException {
		this.ifNotGranted = ifNotGranted;
	}

	public void setComponentId(String componentId) throws JspException {
		this.componentId = componentId;
	}

	public void setCorpComponentPermissionUtil(
			CorpComponentPermissionUtil corpComponentPermissionUtil) {
		this.corpComponentPermissionUtil = corpComponentPermissionUtil;
	}

	public String getComponentId() {
		return componentId;
	}
}
