<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Network Partner Control List</title>
<meta name="heading" content="Network Partner Control List" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style type="text/css">
	.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:4px 3px 1px 5px; height:15px;width:598px; border:1px solid #99BBE8; border-right:none; border-left:none} 
	a.dsphead{text-decoration:none;color:#000000;}
	.dspchar2{padding-left:0px;}
</style>
<style>
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}


</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script>

<script language="javascript" type="text/javascript">
function getNetworkPartnerList(){
	var selectedCorpId=document.forms['networkPartnerCompany'].elements['corpId'].value;
	if(selectedCorpId.trim()==''){
		alert('Please Select corpId');
		return false;
	}
	//agree= confirm("Do you want to get list of network Partner for "+selectedCorpId);
	 location.href="networkPartnerList.html?selectedCorpId="+selectedCorpId;
}
function maintNetworkPartnerList(){
	var selectedCorpId=document.forms['networkPartnerCompany'].elements['manageCorpId'].value;
	if(selectedCorpId.trim()==''){
		alert('Please Select corpId');
		return false;
	}
	agree= confirm("Do you want to maintain network Partner for "+selectedCorpId);
	if(agree){
		 location.href="maintainNetworkPartnerList.html?selectedCorpId="+selectedCorpId;
	}else{
		return false;
	}
}
</script>

</head> 
<s:form name="networkPartnerCompany" id="networkPartnerCompany" action="" method="post" validate="true">
	<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="dd-NNN-yy"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="dd-NNN-yy"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>
<div id="Layer1" style="width:100%;">
<div id="otabs" class="form_magn">
			  <ul>
			    <li><a class="current"><span>Network Partner Control</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top: 10px;!margin-top: -2px"><span></span></div>
   <div class="center-content">
    <table  border="0" class="" style="width:50%;" cellspacing="0" cellpadding="2">	
	  <tr><td height="5px"></td></tr> 
  	  <tr>  
		 <td align="left" width="250;"><s:select cssClass="list-menu" id="hub" name="corpId" list="%{corpidList}" cssStyle="width:200px; margin-left:15px;" headerKey="" headerValue="" /></td>
		 <td align="left"><input type="button" class="cssbutton" style="width:155px; height:23px" onclick="return  getNetworkPartnerList()" value="Get Network Partner List"/></td>
         
	  </tr>
	  <tr><td height="20px"></td></tr> 
<!-- 	  <tr>  
		 <td align="right"><input type="button" class="cssbutton" style="width:183px; height:23px" onclick="return maintNetworkPartnerList()" value="Maintain Network Partner List"/></td>
         <td align="left"><s:select cssClass="list-menu" id="hub" name="manageCorpId" list="%{corpidList}" cssStyle="width:100px" headerKey="" headerValue="" /></td>
	  </tr>  -->
	  <tr><td height="20px"></td></tr> 
    </table>
  </div>
  <div class="bottom-header"><span></span></div> 
</div>
</div>
</div>  
</s:form>

<script type="text/javascript">  
</script>

