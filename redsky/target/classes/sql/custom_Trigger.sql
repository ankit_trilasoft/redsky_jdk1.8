DELIMITER $$
create trigger redsky.trigger_add_history_custom BEFORE UPDATE on redsky.custom
for each row BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";
      
     
IF (OLD.corpID <> NEW.corpID ) THEN
       select CONCAT(fieldNameList,'custom.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
       select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
   END IF;

IF (OLD.movement <> NEW.movement ) THEN
       select CONCAT(fieldNameList,'custom.movement~') into fieldNameList;
       select CONCAT(oldValueList,OLD.movement,'~') into oldValueList;
       select CONCAT(newValueList,NEW.movement,'~') into newValueList;
   END IF;
IF (OLD.ticket <> NEW.ticket ) THEN
       select CONCAT(fieldNameList,'custom.ticket~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ticket,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ticket,'~') into newValueList;
   END IF;
IF (OLD.transactionId <> NEW.transactionId ) THEN
       select CONCAT(fieldNameList,'custom.transactionId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.transactionId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.transactionId,'~') into newValueList;
   END IF;
IF (OLD.status <> NEW.status ) THEN
       select CONCAT(fieldNameList,'custom.status~') into fieldNameList;
       select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
       select CONCAT(newValueList,NEW.status,'~') into newValueList;
   END IF;
IF (OLD.documentType <> NEW.documentType ) THEN
       select CONCAT(fieldNameList,'custom.documentType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.documentType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.documentType,'~') into newValueList;
   END IF;
IF (OLD.documentRef <> NEW.documentRef ) THEN
       select CONCAT(fieldNameList,'custom.documentRef~') into fieldNameList;
       select CONCAT(oldValueList,OLD.documentRef,'~') into oldValueList;
       select CONCAT(newValueList,NEW.documentRef,'~') into newValueList;
   END IF;
IF (OLD.goods <> NEW.goods ) THEN
       select CONCAT(fieldNameList,'custom.goods~') into fieldNameList;
       select CONCAT(oldValueList,OLD.goods,'~') into oldValueList;
       select CONCAT(newValueList,NEW.goods,'~') into newValueList;
   END IF;
IF (OLD.volumeUnit <> NEW.volumeUnit ) THEN
       select CONCAT(fieldNameList,'custom.volumeUnit~') into fieldNameList;
       select CONCAT(oldValueList,OLD.volumeUnit,'~') into oldValueList;
       select CONCAT(newValueList,NEW.volumeUnit,'~') into newValueList;
   END IF;
IF (OLD.weightUnit <> NEW.weightUnit ) THEN
       select CONCAT(fieldNameList,'custom.weightUnit~') into fieldNameList;
       select CONCAT(oldValueList,OLD.weightUnit,'~') into oldValueList;
       select CONCAT(newValueList,NEW.weightUnit,'~') into newValueList;
   END IF;
IF (OLD.pieces <> NEW.pieces ) THEN
       select CONCAT(fieldNameList,'custom.pieces~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pieces,'~') into oldValueList;
       select CONCAT(newValueList,NEW.pieces,'~') into newValueList;
   END IF;
IF (OLD.serviceOrderId <> NEW.serviceOrderId ) THEN
       select CONCAT(fieldNameList,'custom.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.serviceOrderId,'~') into newValueList;
   END IF;
IF (OLD.networkId <> NEW.networkId ) THEN
       select CONCAT(fieldNameList,'custom.networkId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.networkId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.networkId,'~') into newValueList;
   END IF;
  
   IF (OLD.entryDate <> NEW.entryDate ) THEN
       select CONCAT(fieldNameList,'custom.entryDate~') into fieldNameList;
       IF(OLD.entryDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.entryDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.entryDate is not null) THEN
        	select CONCAT(oldValueList,OLD.entryDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.entryDate is not null) THEN
        	select CONCAT(newValueList,NEW.entryDate,'~') into newValueList;
      	END IF;
   END IF;
 IF ((OLD.volume <> NEW.volume) or (OLD.volume is null and NEW.volume is not null) or (OLD.volume is not null and NEW.volume is null)) THEN
		select CONCAT(fieldNameList,'custom.volume~') into fieldNameList;
		IF(OLD.volume is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.volume is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.volume is not null) then
			select CONCAT(oldValueList,OLD.volume,'~') into oldValueList;
		END IF;
		IF(NEW.volume is not null) then
			select CONCAT(newValueList,NEW.volume,'~') into newValueList;
		END IF; 
	END IF;
	 IF ((OLD.weight <> NEW.weight) or (OLD.weight is null and NEW.weight is not null) or (OLD.weight is not null and NEW.weight is null)) THEN
		select CONCAT(fieldNameList,'custom.weight~') into fieldNameList;
		IF(OLD.weight is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.weight is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.weight is not null) then
			select CONCAT(oldValueList,OLD.weight,'~') into oldValueList;
		END IF;
		IF(NEW.weight is not null) then
			select CONCAT(newValueList,NEW.weight,'~') into newValueList;
		END IF; 
	END IF;
if(OLD.updatedOn <> NEW.updatedOn)then
		CALL add_tblHistory (OLD.id,"custom", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
	end if;
	if(OLD.updatedOn = NEW.updatedOn)then
		CALL add_tblHistory (OLD.id,"custom", fieldNameList, oldValueList, newValueList, 'System', OLD.corpID, now());
	end if;

END
$$

