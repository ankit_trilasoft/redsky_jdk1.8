package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.HolidayMaintenanceDao;
import com.trilasoft.app.model.HolidayMaintenance;
import com.trilasoft.app.service.HolidayMaintenanceManager;

public class HolidayMaintenanceManagerImpl extends GenericManagerImpl<HolidayMaintenance, Long> implements HolidayMaintenanceManager{
	HolidayMaintenanceDao holidayMaintenanceDao;
	public HolidayMaintenanceManagerImpl(HolidayMaintenanceDao holidayMaintenanceDao) {
		super(holidayMaintenanceDao);
		this.holidayMaintenanceDao=holidayMaintenanceDao;
	}
	public List checkHoliDayList(String type,Date hDayDate,String corpID){
		return holidayMaintenanceDao.checkHoliDayList(type, hDayDate, corpID);
	}
	public List getHoliDayMaintenanceList(String corpID){
		return holidayMaintenanceDao.getHoliDayMaintenanceList(corpID);
	}
}
