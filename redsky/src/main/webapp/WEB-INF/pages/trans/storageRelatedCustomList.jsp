<%@ include file="/common/taglibs.jsp"%>

<head>
<title>Customs List</title>
<meta name="heading" content="Customs List" />


<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-18px;
padding:2px 0;
text-align:right;
width:100%;
}
</style>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="storageListForm" action="" method="post">
<div id="Layer1" style="width:100%">
<s:hidden name="serviceOrder.id" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
			<div id="newmnav">
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose> 
		      </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		      <c:choose>
			   
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>
		      </sec-auth:authComponent> 
			  <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			  
			  <c:if test="${serviceOrder.job !='INT'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
			  <c:if test="${serviceOrder.job =='INT'}">
			    <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  </sec-auth:authComponent>
			  <c:if test="${serviceOrder.job =='RLO'}">
			  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			  </c:if>
			  <c:if test="${serviceOrder.job !='RLO'}">
			  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			  </c:if>
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket</span></a></li>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div>
	
	<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
		<div id="newmnav">
		   			 <ul>
					  	<li><a href="editWorkTicketUpdate.html?id=${workTicket.id}"><span>Work Ticket</span></a></li>
					    <li><a href="bookStorageLibraries.html?id=${workTicket.id}"><span>Storage List</span></a></li>
						<li><a href="searchUnitLocates.html?locationId=&type=&occupied=&warehouse=${workTicket.warehouse}&id=${workTicket.id}"><span>Add Storage</span></a></li>
					  	<li><a href="storageUnit.html?id=${workTicket.id}"><span>Access/Release Storage</span></a></li>
					  	<li><a href="storageUnitMove.html?id=${workTicket.id}"><span>Move Storage Location</span></a></li>
					  	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Customs</span></a></li>
					  	<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
					  	
					</ul>
		</div><div class="spn">&nbsp;</div>
	
	
	<table width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
			<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
				<table>
					<tbody>
						<tr>
							
							<td class="listwhite">Ticket Number</td>
							<td><s:textfield name="workTicket.ticket" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
							<td width="10px"></td>
							<td class="listwhite">Warehouse</td>
							<td><s:textfield name="workTicket.warehouse" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
						</tr>
					</tbody>
				</table>
				</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

<s:set name="storageCustoms" value="storageCustoms" scope="request"/>
	<div id="otabs">
				  <ul>
				    <li><a class="current"><span>List</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
				
 
<display:table name="storageCustoms" class="table" requestURI="" id="customList" export="true" pagesize="25"> 
	 <display:column property="movement" sortable="true" title="Movement" href="editStorageRelatedCustoms.html?sid=${serviceOrder.id}&wtId=${workTicket.id}" paramId="id" paramProperty="id" style="width:100px" />
     <display:column property="entryDate" sortable="true" title="Entry Date" format="{0,date,dd-MMM-yyyy}" /> 
     <display:column property="documentType" sortable="true" title="Document Type"/> 
     <display:column property="documentRef" sortable="true" title="Document Ref"/> 
     <display:column property="pieces" sortable="true" title="Pieces"/> 
     <display:column property="volume" sortable="true" title="Volume"/> 
            
    <display:setProperty name="paging.banner.item_name" value="custom"/> 
    <display:setProperty name="paging.banner.items_name" value="people"/> 
 
    <display:setProperty name="export.excel.filename" value="Custom List.xls"/> 
    <display:setProperty name="export.csv.filename" value="Custom List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="Custom List.pdf"/> 
</display:table> 

	</td>
		</tr>
	</tbody>
</table>
</div>   
	
	<c:set var="buttons">
	

	
	
	</c:set>
	<c:out value="${buttons}" escapeXml="false" />

</s:form>

<script type="text/javascript"> 
    highlightTableRows("bookStorageLibraryList");  
</script>
<script language="JavaScript" type="text/javascript">
try{
<script type="text/javascript">   
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/storageCustoms.html?wtId=${workTicket.id}" ></c:redirect>
		</c:if>
}
catch(e){}		
		
</script>
