package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CustomerServiceSurveyDao;
import com.trilasoft.app.dao.MiscellaneousDao;
import com.trilasoft.app.model.CustomerServiceSurvey;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;

public class CustomerServiceSurveyDaoHibernate extends GenericDaoHibernate<CustomerServiceSurvey, Long> implements CustomerServiceSurveyDao{
	 public CustomerServiceSurveyDaoHibernate()
     {   
       super(CustomerServiceSurvey.class);   
   } 
	 
	public  Map<String,String> getCustomerServiceList(Long id){
		    Map<String,String> list1=new HashMap();
			List list = this.getSession().createSQLQuery("select concat(questionID,'~',answerID,'*',id) from surveyresult where soID='"+id+"' ").list();
			Iterator it = list.iterator();
			while (it.hasNext()) {
				String type = (String)it.next();				
				String obj[]=type.split("~");
				list1.put(obj[0],obj[1]);
			}
			return list1;
	    	
	    }
	
}
