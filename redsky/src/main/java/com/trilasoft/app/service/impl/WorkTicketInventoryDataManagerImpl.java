package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.WorkTicketInventoryDataDao;
import com.trilasoft.app.model.WorkTicketInventoryData;
import com.trilasoft.app.service.WorkTicketInventoryDataManager;

public class WorkTicketInventoryDataManagerImpl extends GenericManagerImpl<WorkTicketInventoryData, Long> implements WorkTicketInventoryDataManager {

	WorkTicketInventoryDataDao workTicketInventoryDataDao;
	public WorkTicketInventoryDataManagerImpl(WorkTicketInventoryDataDao workTicketInventoryDataDao) {
		super(workTicketInventoryDataDao);
		this.workTicketInventoryDataDao=workTicketInventoryDataDao;
		// TODO Auto-generated constructor stub
	}
	public List checkInventoryData(String corpID, String barcode,String shipNumber){
		return workTicketInventoryDataDao.checkInventoryData(corpID, barcode, shipNumber);
	}
	public List getAllInventory(String sessionCorpID, String shipno) {
		return workTicketInventoryDataDao.getAllInventory(sessionCorpID, shipno);
	}

}
