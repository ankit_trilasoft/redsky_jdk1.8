<%@ include file="/common/taglibs.jsp"%>
<head>
<title>Basic Information</title> 
<meta name="heading" content="Basic Information"/>

<style><%@ include file="/common/calenderStyle.css"%></style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here --> 
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>

<script language="javascript" type="text/javascript">
 		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
</script>
 
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('address', 'hide=0')
animatedcollapse.addDiv('origin', 'hide=0')
animatedcollapse.addDiv('destination', 'hide=0')
animatedcollapse.addDiv('notes', 'hide=0')
animatedcollapse.init()
</script>
<script language="javascript" type="text/javascript">
window.onload = function() { 
		 getOriginState(document.forms['basicInformation'].elements['customerFile.originCountry']);
		getDestinationState(document.forms['basicInformation'].elements['customerFile.destinationCountry']);
		  
	}
var namesVec = new Array("tab_open.png", "tab_close.png");
var root='images/';
function swapImg(ima){
// divides the path
nr = ima.getAttribute('src').split('/');
// gets the last part of path, ie name
nr = nr[nr.length-1]
// former was .split('.')[0];
 
if(nr==namesVec[0]){ima.setAttribute('src',root+namesVec[1]);}
else{ima.setAttribute('src',root+namesVec[0]);}
 
}

function getOriginState(country){
	var country = country.value;
	if(country=='United States' || country == 'India' || country == 'Canada'){
		document.forms['basicInformation'].elements['customerFile.originState'].disabled=false;
	}else{
		document.forms['basicInformation'].elements['customerFile.originState'].disabled=true;
	}
	
	var countryCode = "";
	if(country == 'United States'){
		countryCode = "USA";
	}
	if(country == 'India'){
		countryCode = "IND";
	}
	if(country == 'Canada'){
		countryCode = "CAN";
	}
		var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
		 httpState.open("GET", url, true);
	     httpState.onreadystatechange = handleHttpResponse5;
	     httpState.send(null)
}

 function handleHttpResponse5(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['basicInformation'].elements['customerFile.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['basicInformation'].elements['customerFile.originState'].options[i].text = '';
					document.forms['basicInformation'].elements['customerFile.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['basicInformation'].elements['customerFile.originState'].options[i].text = stateVal[1];
					document.forms['basicInformation'].elements['customerFile.originState'].options[i].value = stateVal[0];
					
					if (document.getElementById("originState").options[i].value == '${customerFile.originState}'){
					   document.getElementById("originState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("originState").value = '${customerFile.originState}';
             }
        }
   
function getDestinationState(country){
	var country = country.value;
	if(country=='United States' || country == 'India' || country == 'Canada'){
		document.forms['basicInformation'].elements['customerFile.destinationState'].disabled=false;
	}else{
		document.forms['basicInformation'].elements['customerFile.destinationState'].disabled=true;
	}
	
	var countryCode = "";
	if(country == 'United States'){
		countryCode = "USA";
	}
	if(country == 'India'){
		countryCode = "IND";
	}
	if(country == 'Canada'){
		countryCode = "CAN";
	}
		var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
		 httpStateDest.open("GET", url, true);
	     httpStateDest.onreadystatechange = handleHttpResponse55;
	     httpStateDest.send(null)
}

 function handleHttpResponse55(){
             if (httpStateDest.readyState == 4){
                var results = httpStateDest.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['basicInformation'].elements['customerFile.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['basicInformation'].elements['customerFile.destinationState'].options[i].text = '';
					document.forms['basicInformation'].elements['customerFile.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['basicInformation'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
					document.forms['basicInformation'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${customerFile.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("destinationState").value = '${customerFile.originState}';
             }
        }

function enableAll() {
	
	var elementsLen=document.forms['basicInformation'].elements.length;
			for(i=0;i<=elementsLen-1;i++)
			{
				
						document.forms['basicInformation'].elements[i].disabled=false;
			}
	
	}
	
function getOriginCountryCode(){
	var countryName=document.forms['basicInformation'].elements['customerFile.originCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponseCountryName;
    http2.send(null);
}

 
        function handleHttpResponseCountryName()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					document.forms['basicInformation'].elements['customerFile.originCountryCode'].value = results; 					
				 	document.forms['basicInformation'].elements['customerFile.originCountry'].select();	 	 
 				 }
                 else
                 {
                     
                 }
             }
        }


function getDestinationCountryCode(){
	var countryName=document.forms['basicInformation'].elements['customerFile.destinationCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http2.open("GET", url, true);
    http2.onreadystatechange = httpDestinationCountryName;
    http2.send(null);
}

function httpDestinationCountryName()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					document.forms['basicInformation'].elements['customerFile.destinationCountryCode'].value = results;
 					document.forms['basicInformation'].elements['customerFile.destinationCountry'].select();
					 }
                 else
                 {
                     
                 }
             }
        }

function checkFields(){
	var originCountry=document.forms['basicInformation'].elements['customerFile.originCountry'].value;
	var destinationCountry=document.forms['basicInformation'].elements['customerFile.destinationCountry'].value;
	if(originCountry=='United States' || originCountry == 'India' || originCountry == 'Canada'){
			if(document.forms['basicInformation'].elements['customerFile.originState'].value==''){
				alert('Please select the origin state');
				return false;
			}
	}
	if(destinationCountry=='United States' || destinationCountry == 'India' || destinationCountry == 'Canada'){
			if(document.forms['basicInformation'].elements['customerFile.destinationState'].value==''){
				alert('Please select the destination state');
				return false;
			}
	}
	enableAll(); 

}
String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g,"");
	}
	String.prototype.ltrim = function() {
	    return this.replace(/^\s+/,"");
	}
	String.prototype.rtrim = function() {
	    return this.replace(/\s+$/,"");
	}
	String.prototype.lntrim = function() {
	    return this.replace(/^\s+/,"","\n");
	}
	
	
	function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
	}
    var http2 = getHTTPObject();
    var httpState = getHTTPObjectState();
    var httpStateDest = getHTTPObjectStateDest()
   
     function getHTTPObjectState()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

    function getHTTPObjectStateDest()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function openOriginLocation() {
		var city = document.forms['basicInformation'].elements['customerFile.originCity'].value;
		var dd = document.forms['basicInformation'].elements['customerFile.originCountry'].selectedIndex;
		var country = document.forms['basicInformation'].elements['customerFile.originCountry'].options[dd].text;
		var zip = document.forms['basicInformation'].elements['customerFile.originZip'].value;
		var address = document.forms['basicInformation'].elements['customerFile.originAddress1'].value;
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+country);
}
	function openDestinationLocation() {
		var city = document.forms['basicInformation'].elements['customerFile.destinationCity'].value;
		
		var dd = document.forms['basicInformation'].elements['customerFile.destinationCountry'].selectedIndex;
		var country = document.forms['basicInformation'].elements['customerFile.destinationCountry'].options[dd].text;
		
		var zip = document.forms['basicInformation'].elements['customerFile.destinationZip'].value;
		var address = document.forms['basicInformation'].elements['customerFile.destinationAddress1'].value;
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+country);
}

</script>
</head>
<s:form id="basicInformation" name="basicInformation" action='saveBasicInformation' method="post" validate="true">
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    <s:hidden name="customerFile.id"/>
    <s:hidden name="customerFile.companyDivision" />
    <s:hidden name="customerFile.status" />	
    <s:hidden name="customerFile.quotationStatus" />	
    <s:hidden name="customerFile.statusNumber" />	
    <s:hidden name="customerFile.comptetive" />	
    <s:hidden name="customerFile.corpID" />	
    <s:hidden name="customerFile.surveyTime" />	
    <s:hidden name="customerFile.surveyTime2" />	
    <s:hidden name="customerFile.assignmentType" />	
    <s:hidden name="customerFile.controlFlag" />	
    <s:hidden name="customerFile.job" />	
    <s:hidden name="customerFile.bookingAgentCode" />
    <s:hidden name="customerFile.createdBy" />	
    <s:hidden name="customerFile.updatedBy" />
    <s:hidden name="customerFile.originCountryCode" />
    <s:hidden name="customerFile.destinationCountryCode" />
    <s:hidden name="customerFile.billToCode" />
    <s:hidden name="customerFile.contract" />
    <s:hidden name="userCheck" />
    
<div id="Layer1"  style="width:100%;">	
<div id="newmnav">
		  <ul>
		    	
		  	<li><a href="agentPricingList.html"><span>Price Engine List</span></a></li>
		  	<li><a href="agentPricingAcceptedQuotes.html"><span>Accept Quote List</span></a></li>
		  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Information<img src="images/navarrow.gif" align="absmiddle" /></span></a></li> 
		  </ul>
		</div><div class="spn">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
  
   <table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%">
		<tbody>
		<tr height="10px">
	 	 	<td align="left" class="listwhitetext" colspan="4"  ><span style="font-style:italic"><span style="color: #ff0000;">*</span> Please fill information below to submit your quote.</span></td>
	 		<td align="left" height="15px"></td>
   		</tr>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;" border="0">
						<tr>
							<td class="headtab_left">
						</td>
							<td NOWRAP class="headtab_center">&nbsp;Basic Information
						</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
			   </table>
			   <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="11"></td></tr>
						
						<tr>
							<td align="right" class="listwhitetext" style="width:105px">&nbsp;</td>
							<td align="left" class="listwhitetext" valign="bottom" colspan="">First Name</td>
							<td width="10px"></td>	
							<td width="10px" class="listwhitetext"> Initial</td>
							<td width="10px"></td>		
							<td align="left" class="listwhitetext" valign="bottom">Last Name</td>
							<td width="10px"></td>	
							<td align="left" class="listwhitetext" style="width:10px">Move By Date</td>
							
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:105px">&nbsp;</td>
							<td align="left" class="listwhitetext" valign="top" colspan=""> 
								<s:textfield cssClass="input-text" key="customerFile.firstName" required="true" maxlength="15" cssStyle="width:220px"/> 
							</td>
							<td width="10px"></td>	
							<td width="10px">
								<s:textfield cssClass="input-text" key="customerFile.middleInitial" maxlength="1" required="true" cssStyle="width:37px"/> </td>
							</td>
							<td width="10px"></td>	
							<td align="right" class="listwhitetext">
								<s:textfield cssClass="input-text" key="customerFile.lastName" required="true" maxlength="80" cssStyle="width:237px"/> </td>
							</td>
							<td width="10px"></td>	
							<c:if test="${not empty customerFile.moveDate}">
							<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
							<td><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" cssStyle="width:105px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
							<td align="left" width="30px"><img id="moveDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty customerFile.moveDate}">
							<td><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" cssStyle="width:105px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
							<td width="30px">&nbsp;<img id="moveDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
						</tr>
					</tbody>
				</table>
				<tr style="height: 20px">
				
				</tr>
				
				<tr>
										<td height="10" width="100%" align="left" >
										<!--<div class="subcontent-tab" onClick="javascript:animatedcollapse.toggle('address')" style="cursor: pointer"><a href="javascript:;"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" align="absmiddle" border="0"/></span>&nbsp;&nbsp;Address Details</a></div>
										-->
										<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
										<tr>
										<td class="headtab_left">
										</td>
										<td NOWRAP class="headtab_center">&nbsp;Address Details
										</td>
										<td width="28" valign="top" class="headtab_bg"></td>
										<td class="headtab_bg_center">&nbsp;
										</td>
										<td class="headtab_right">
										</td>
										</tr>
										</table>
									
										
										<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
											<tbody>
											<tr><td class="subcontenttabChild" colspan="7"><font color="black"><b>&nbsp;Origin</b></font></td></tr>
												<tr>
													<td valign="top" align="left" class="listwhitetext">
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>															
															<tr>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originAddress1'/></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style="width:35px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCompany'/></td>
															</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.originAddress1" size="38" maxlength="100" tabindex="17" /></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.originCompany" size="38" maxlength="70" onblur="copyCompanyToDestination(this)" tabindex="18" /></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.originAddress2" size="38" maxlength="100" tabindex="19" /></td>
																<td align="left" class="listwhitetext"><font color="gray">&nbsp;&nbsp;Address2</font></td>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext"></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.originAddress3" size="38" maxlength="100" tabindex="20" /></td>
																<td align="left" class="listwhitetext"><font color="gray">&nbsp;&nbsp;Address3</font></td>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:105px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCountry'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originState'/></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext" onmouseover ="gettooltip('customerFile.originCity');" onmouseout="hideddrivetip();"><fmt:message key='customerFile.originCity'/></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originZip' /></td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:105px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="customerFile.originCountry" list="%{ocountry}" cssStyle="width:222px" onchange="getOriginState(this);getOriginCountryCode(this);"  headerKey="" headerValue="" tabindex="21" /></td><td><img src="${pageContext.request.contextPath}/images/globe.jpg" HEIGHT=20 WIDTH=20 onclick="openOriginLocation();"/></td>
																<td><s:select cssClass="list-menu" id="originState"  name="customerFile.originState" list="%{ostates}" cssStyle="width:130px" onchange="" headerKey="" headerValue="" tabindex="22" /></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originCity" size="15" maxlength="30" onkeydown="return onlyCharsAllowed(event)" onchange="autoPopulate_customerFile_originCityCode(this);" tabindex="23" /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originZip" size="15" maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="24" /></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originDayPhone'/></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.originDayPhoneExt'/>&nbsp;</td>
																
																<td align="right" class="listwhitetext" style="width:20px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originHomePhone'/></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originMobile'/></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originFax'/></td>
															</tr>
															<tr>	
																<td align="right" class="listwhitetext" style="width:105px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.originDayPhone" size="27" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="25" /></td>
																<td align="right" class="listwhitetext" style="width:8px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originDayPhoneExt" size="4" maxlength="3" onkeydown="return onlyNumsAllowed(event)" tabindex="26" /></td>
																<td align="right" class="listwhitetext" style="width:20px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originHomePhone" size="20" maxlength="16" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="27" /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originMobile" size="15" maxlength="25" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="28"/></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originFax" size="15" maxlength="16" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="29"/></td>
																<td width="10px"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:104px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.email'/></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.email2'/></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext"></td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:104px">&nbsp;</td>
																<td ><s:textfield cssClass="input-text" name="customerFile.email" size="31" maxlength="65" tabindex="30" /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.email2" size="30" maxlength="65" tabindex="31" /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td ></td>
																<td align="left" class="listwhitetext" style="width:14px"></td>
															</tr>
														</tbody>
													</table>
													</td>
													
												</tr>
												<tr><td class="listwhitetext" style="width:100px; height:5px;" colspan="7"></td></tr>
												<tr>
													<td align="center" colspan="10" class="vertlinedata"></td>
												</tr>
												<tr><td class="subcontenttabChild" colspan="7"><font color="black"><b>&nbsp;Destination</b></font></td></tr>
												<tr>
													<td valign="top" align="left" class="listwhitetext" >
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationAddress1'/></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style="width:109px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCompany'/></td>
															</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.destinationAddress1" size="38" maxlength="100" tabindex="33" /></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style="width:109px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.destinationCompany" size="38" maxlength="70" tabindex="34" /></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.destinationAddress2" size="38" maxlength="100" tabindex="35" /></td>
																<td align="left" class="listwhitetext"><font color="gray">&nbsp;&nbsp;Address2</font></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style="width:109px"></td>
																<td align="left" class="listwhitetext"></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.destinationAddress3" size="38" maxlength="100" tabindex="36" /></td>
																<td align="left" class="listwhitetext"><font color="gray">&nbsp;&nbsp;Address3</font></td>
																<td align="left" class="listwhitetext" style="width:109px"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:106px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCountry'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationState'/></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCity'/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationZip' /></td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:106px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="customerFile.destinationCountry" list="%{ocountry}" cssStyle="width:222px" onchange="getDestinationCountryCode();getDestinationState(this)"  headerKey="" headerValue="" tabindex="37" /></td><td><img src="${pageContext.request.contextPath}/images/globe.jpg" HEIGHT=20 WIDTH=20 onclick="openDestinationLocation();"/></td>
																<td><s:select cssClass="list-menu" id="destinationState" name="customerFile.destinationState" list="%{dstates}" cssStyle="width:130px" onchange="" headerKey="" headerValue="" tabindex="38" /></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationCity" size="15" maxlength="30" onkeydown="return onlyCharsAllowed(event)" onchange="autoPopulate_customerFile_destinationCityCode(this);" tabindex="39" /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationZip" size="15" maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="40" /></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:106px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationDayPhone'/></td>
																<td align="left" class="listwhitetext" width="8px"></td>
																<td align="left" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.destinationDayPhoneExt'/>&nbsp;</td>
																<td align="right" class="listwhitetext" style="width:20px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationHomePhone'/></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationMobile'/></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationFax'/></td>
																
															</tr>
															<tr>	
																<td align="right" class="listwhitetext" style="width:106px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationDayPhone" size="27" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="41" /></td>
																<td align="left" class="listwhitetext" width="8px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationDayPhoneExt" size="4" maxlength="3" onkeydown="return onlyNumsAllowed(event)" tabindex="42" /></td>
																<td align="right" class="listwhitetext" style="width:20px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationHomePhone" size="20" maxlength="16" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="43" /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationMobile" size="15" maxlength="25" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="44" /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationFax" size="15" maxlength="16" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="45" /></td>
																
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationEmail'/></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationEmail2'/></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext"></td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:105px"></td>
																<td align="left"><s:textfield cssClass="input-text" name="customerFile.destinationEmail" size="31" maxlength="65" tabindex="46" /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.destinationEmail2" size="30" maxlength="65" tabindex="47" /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td ></td>
																<td align="left" class="listwhitetext" style="width:14px"></td>
														  </tr>
															<tr><td class="listwhitetext" style="width:100px; height:5px;" colspan="7"></td></tr>
														</tbody>
													</table>
													</td>
												</tr>
											</tbody>
										</table>
										
										</td>
									</tr>
								<tr>
										<td height="10" width="100%" align="left" >
										<!--<div class="subcontent-tab" onClick="javascript:animatedcollapse.toggle('address')" style="cursor: pointer"><a href="javascript:;"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" align="absmiddle" border="0"/></span>&nbsp;&nbsp;Address Details</a></div>
										-->
										<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
										<tr>
										<td class="headtab_left">
										</td>
										<td NOWRAP class="headtab_center">&nbsp;Notes
										</td>
										<td width="28" valign="top" class="headtab_bg"></td>
										<td class="headtab_bg_center">&nbsp;
										</td>
										<td class="headtab_right">
										</td>
										</tr>
										</table>
										<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
											<tbody>
												<tr>
													<td valign="top" align="left" class="listwhitetext">
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>															
															<tr>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext">Notes:</td>
															</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:105px"></td>
																<td align="left" class="listwhitetext"><s:textarea name="pricingNotes"  rows="4" cols="112" readonly="false" cssClass="textarea"/></td>
																
															</tr>
															
															
															
														</tbody>
													</table>
													
													
													
													</td>
													
												</tr>
											</tbody>
										</table>
										
										</td>
									</tr>
			</td></tr></tbody></table>	
					
   </div>
</div>
<div class="bottom-header"><span></span></div>
			
			<tr>
              <td colspan="" align="center"><table class="detailTabLabel"  border="0" style="margin: 0px;padding: 0px;" >
                  <tbody>
                    <tr>
                     <td align="left"><s:submit cssClass="cssbutton1" type="button" onclick="return checkFields()"/> </td>
                     <td align="left"><s:reset type="button" cssClass="cssbutton1" key="Reset" /></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
</div>	
</div>
</s:form>
<script type="text/javascript"> 
try{
<c:if test="${hitFlag == 55}" >
		<c:redirect url="/agentPricingList.html"  />
</c:if>
}
catch(e){}
</script> 
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>