<%@ include file="/common/taglibs.jsp"%> 
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Map"%>

<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="margin: 0px; padding: 0px;">
	<tr style="margin: 0px; padding:0px;line-height:1.5em; " >
		<td width="50px" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Year</b></td>
		<td align="center" width="80px" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>${corpId} Booking</b></td>
		<td align="center" width="80px" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Weight</b></td>
		<td align="center" width="130px" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Revenue (In Thousand)</b></td>
	</tr>
	
	<% 
		SortedMap <String, BigDecimal> yearMapSSCW = (SortedMap <String, BigDecimal>)request.getAttribute("yearMapSSCW");
		Iterator yearIteratorSSCW = yearMapSSCW.entrySet().iterator();
		
		SortedMap <String, BigDecimal> weightMapSSCW = (SortedMap <String, BigDecimal>)request.getAttribute("weightMapSSCW");
		Iterator weightIteratorSSCW = weightMapSSCW.entrySet().iterator();
		
		SortedMap <String, BigDecimal> revenueMapSSCW = (SortedMap <String, BigDecimal>)request.getAttribute("revenueMapSSCW");
		Iterator revenueIteratorSSCW = revenueMapSSCW.entrySet().iterator();
		
		BigDecimal sscwActivity = new BigDecimal("0");
		BigDecimal sscwActualWeight = new BigDecimal("0");
		BigDecimal sscwActualRevenue = new BigDecimal("0");
		
		while (yearIteratorSSCW.hasNext()) {
			Map.Entry entrySSCW = (Map.Entry) yearIteratorSSCW.next();	
			String year = (String) entrySSCW.getKey();
			sscwActivity = yearMapSSCW.get(year);
			
			if (weightIteratorSSCW.hasNext()) {	
				Map.Entry entryWeightSSCW = (Map.Entry) weightIteratorSSCW.next();	
				String weightSSCW = (String) entryWeightSSCW.getKey();
				sscwActualWeight = weightMapSSCW.get(weightSSCW);
			}
			
			if (revenueIteratorSSCW.hasNext()) {	
				Map.Entry entryRevenueSSCW = (Map.Entry) revenueIteratorSSCW.next();	
				String revenueSSCW = (String) entryRevenueSSCW.getKey();
				sscwActualRevenue = revenueMapSSCW.get(revenueSSCW);
			}
	%>
	
	<tr style="margin: 0px; padding: 0px;line-height:1.5em ;">	
		<td style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><%=year%></td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><%=sscwActivity%></td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><%=sscwActualWeight%></td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><%=sscwActualRevenue%></td>
		
	</tr>
	<%}	%>	
	<tr style="margin: 0px; padding: 0px;line-height:1.5em ;">	
		<td colspan="7" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">Note : Given weight is in Lbs.</td>
	</tr>
</table>

