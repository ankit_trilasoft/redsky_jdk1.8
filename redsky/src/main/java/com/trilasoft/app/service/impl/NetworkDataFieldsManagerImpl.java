package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.NetworkDataFieldsDao;
import com.trilasoft.app.model.NetworkDataFields;
import com.trilasoft.app.service.NetworkDataFieldsManager;

public class NetworkDataFieldsManagerImpl extends GenericManagerImpl<NetworkDataFields,Long> implements NetworkDataFieldsManager {
	NetworkDataFieldsDao networkDataFieldsDao;
	public NetworkDataFieldsManagerImpl(NetworkDataFieldsDao networkDataFieldsDao) {
		super(networkDataFieldsDao);
		this.networkDataFieldsDao = networkDataFieldsDao;
	}
    public List findForCombo1Map() {
			return networkDataFieldsDao.findForCombo1Map();
		}

	public List search(String modelName, String fieldName,String types,String tranType) {
		
		return networkDataFieldsDao.search(modelName,fieldName,types,tranType);
	}
	
	public List getFieldList(String modelName){
		return networkDataFieldsDao.getFieldList(modelName);
	}

	public boolean checkDuplicateRecord(String modelName, String fieldName, String transactionType){
		return networkDataFieldsDao.checkDuplicateRecord(modelName, fieldName, transactionType);
	}
	}

