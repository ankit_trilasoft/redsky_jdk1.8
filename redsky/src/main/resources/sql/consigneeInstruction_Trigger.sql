
DELIMITER $$
create trigger redsky.trigger_add_history_consigneeInstruction BEFORE UPDATE on redsky.consigneeInstruction
for each row BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";
   
   
   
      IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
   END IF;

  

   IF (OLD.ship <> NEW.ship ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.ship~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ship,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ship,'~') into newValueList;
   END IF;

   IF (OLD.shipNumber <> NEW.shipNumber ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
   END IF;

     IF (OLD.consigneeAddress <> NEW.consigneeAddress ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.consigneeAddress~') into fieldNameList;
       select CONCAT(oldValueList,OLD.consigneeAddress,'~') into oldValueList;
       select CONCAT(newValueList,NEW.consigneeAddress,'~') into newValueList;
   END IF;

   IF (OLD.consignmentInstructions <> NEW.consignmentInstructions ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.consignmentInstructions~') into fieldNameList;
       select CONCAT(oldValueList,OLD.consignmentInstructions,'~') into oldValueList;
       select CONCAT(newValueList,NEW.consignmentInstructions,'~') into newValueList;
   END IF;

   IF (OLD.notification <> NEW.notification ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.notification~') into fieldNameList;
       select CONCAT(oldValueList,OLD.notification,'~') into oldValueList;
       select CONCAT(newValueList,NEW.notification,'~') into newValueList;
   END IF;

  IF (OLD.notify <> NEW.notify ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.notify~') into fieldNameList;
       IF(OLD.notify = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.notify = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.notify = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.notify = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   IF (OLD.shipperAddress <> NEW.shipperAddress ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.shipperAddress~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipperAddress,'~') into oldValueList;
       select CONCAT(newValueList,NEW.shipperAddress,'~') into newValueList;
   END IF;

    IF (OLD.specialInstructions <> NEW.specialInstructions ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.specialInstructions~') into fieldNameList;
       select CONCAT(oldValueList,OLD.specialInstructions,'~') into oldValueList;
       select CONCAT(newValueList,NEW.specialInstructions,'~') into newValueList;
   END IF;

 
   IF (OLD.serviceOrderId <> NEW.serviceOrderId ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.serviceOrderId,'~') into newValueList;
   END IF;

      IF (OLD.shipmentLocation <> NEW.shipmentLocation ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.shipmentLocation~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipmentLocation,'~') into oldValueList;
       select CONCAT(newValueList,NEW.shipmentLocation,'~') into newValueList;
   END IF;


   IF (OLD.expressRelease <> NEW.expressRelease ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.expressRelease~') into fieldNameList;
       select CONCAT(oldValueList,OLD.expressRelease,'~') into oldValueList;
       select CONCAT(newValueList,NEW.expressRelease,'~') into newValueList;
   END IF;


     IF (OLD.collect <> NEW.collect ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.collect~') into fieldNameList;
       select CONCAT(oldValueList,OLD.collect,'~') into oldValueList;
       select CONCAT(newValueList,NEW.collect,'~') into newValueList;
   END IF;

IF (OLD.ugwIntId <> NEW.ugwIntId ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.ugwIntId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ugwIntId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ugwIntId,'~') into newValueList;
   END IF;

IF (OLD.corpID <> NEW.corpID ) THEN
       select CONCAT(fieldNameList,'consigneeInstruction.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
       select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
   END IF;
  

   CALL add_tblHistory (OLD.serviceorderid,"consigneeInstruction", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END
$$