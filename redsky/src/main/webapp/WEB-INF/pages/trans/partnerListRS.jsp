<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>


<head>
<title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>  
    
 
<style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:8px;
!margin-bottom:2px;
margin-top:-15px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:100%;
}
</style>	    
</head>
<script language="javascript" type="text/javascript">
function clear_fields(){
			document.forms['partnerListForm'].elements['partner.lastName'].value = "";
			document.forms['partnerListForm'].elements['partner.partnerCode'].value = "";
			document.forms['partnerListForm'].elements['partner.aliasName'].value = "";				
			document.forms['partnerListForm'].elements['partner.billingCountryCode'].value = "";
			document.forms['partnerListForm'].elements['partner.billingState'].value = "";
			document.forms['partnerListForm'].elements['partner.billingCountry'].value = "";			
}
function removeSpace(){
	var abc = document.forms['partnerListForm'].elements['partner.partnerCode'].value;
	document.forms['partnerListForm'].elements['partner.partnerCode'].value=abc.trim();
	}
 </script> 
<div id="layer1" style="width:100%;">
<div id="newmnav" style="!padding-bottom:5px;">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span><font color="#fe000c">Red</font><font color="#000000">Sky</font></span></a></li>  				  
		  <li><a href="partnerView.html" ><span>Partner View</span></a></li>
		  <li><a href="geoCodeMaps.html?partnerOption=view" ><span>Geo Search</span></a></li>					  	
	  </ul>
</div>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" onclick="" cssStyle="width:55px; height:25px" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>
<s:form id="partnerListForm" onsubmit="" action="searchPartnerRS.html?partnerType=RSKY&search=YES" method="post" >
<div id="layer1" style="width:100%;">

<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" border="0" style="width:100%;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th><fmt:message key="partner.name"/></th>
<th>Alias Name</th>
<th>Country Code</th>
<th>Country Name</th>
<th><fmt:message key="partner.billingState"/></th>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partner.partnerCode" size="20" onkeydown="removeSpace()" cssClass="input-text"/>
			</td>
			<td>
			    <s:textfield name="partner.lastName" size="20" cssClass="input-text" />
			</td>
			<td>
			    <s:textfield name="partner.aliasName" size="20" cssClass="input-text" />
			</td>			
			 
						<td>
						    <s:textfield name="partner.billingCountryCode" size="20" cssClass="input-text"/>
						</td>
						<td>
						    <s:textfield name="partner.billingCountry" size="20" cssClass="input-text"/>
						</td>
						<td>
						    <s:textfield name="partner.billingState" size="20" cssClass="input-text"/>
						</td>						
				</tr>
			<tr>
			<td colspan="5"></td>
			<td width="130px" style="border:none;"><c:out value="${searchbuttons}" escapeXml="false"/></td>   
			
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	</div>
	<div class="spn" style="width:100%">&nbsp;</div><br>

<s:set name="partners" value="partners" scope="request"/>
<display:table name="partners" class="table" requestURI="" id="partnerList"  defaultsort="2" pagesize="50" style="width:100%;"  >   
	<display:column property="partnerCode" sortable="true" title="Agent PartnerCode"  /> 	  
    <display:column title="Agent Name" style="width:250px" property="lastName" sortable="true"/>
    <display:column title="Alias Name" style="width:250px" property="aliasName" sortable="true"/>
     <display:column title="Address" style="width:250px" property="billingAddress1" sortable="true"/>   
     <display:column title="Country" style="width:50px" property="billingCountryCode" sortable="true"/>   
    <display:column title="State" style="width:50px" property="billingState" sortable="true"/>
    <display:column title="City" style="width:100px" property="billingCity" sortable="true"/>
    <display:column title="Status" style="width:50px" property="status" sortable="true"/>
</display:table>
</s:form>