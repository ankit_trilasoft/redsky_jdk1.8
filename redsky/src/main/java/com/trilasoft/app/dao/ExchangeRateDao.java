/**
 * ExchangeRate Data Access Object (Dao) interface.
 * Gets ExchangeRate information.
 * @param 			  
 * This interface represents the basic actions on "ExchangeRate" object in Redsky .
 * @Interface Name	  ExchangeRateDao
 * @Author            Ravi Mishra
 * @Version           V01.0
 * @Since             1.0
 * @Date              08-Dec-2008
 */


package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ExchangeRate;

public interface ExchangeRateDao extends GenericDao<ExchangeRate, Long>  {
			
		public List searchBaseCurrency(String corpID);
		public List findMaximumId();
		public List findAccExchangeRate(String corpId,String country);	
		public List searchAll(String currency);
		public List searchID();
		public Map<String, String> getExchangeRateWithCurrency(String corpID);
		public Map<String, String> getAgentExchangeRateWithCurrency();
		public List getExchangeHistoryList(String corpID);
		public List searchAll(String currency,String valueDate);
		public List getHistoryFromid(Long id);
		public Map<String, String> getExchangeHistoryListByDate(String corpID,String fxValueDate);
}
