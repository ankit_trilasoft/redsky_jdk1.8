<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<head>

<%-- <script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghRGCK2dYb6E9n4B1E2585HyXOYBiBTkm8DZjmBAmvbTx4LAPz_RRfRfSg" type="text/javascript"></script>--%>
	
		<title><fmt:message key="agentRequest.title"/></title>   
    	<meta name="heading" content="<fmt:message key="agentRequset.heading"/>"/> 
 <link rel="stylesheet" type="text/css"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<style>

<%@ include file="/common/calenderStyle.css"%>
</style>
<style>
.textareaUpper {
    background-color: #E3F1FE;
    border: 1px solid #ABADB3;
    color: #000000;
    font-family: arial,verdana;
    font-size: 12px;   
    text-decoration: none;
}
div.wrapper-img{ width:570px;}
div.thumbline
  {
  margin:2px;
  border:none;
  float:left;
  text-align:center;
 
  background:transparent url(images/agent_prf_bg.png) no-repeat scroll 0 0;
  height:152px;
  padding:20px 20px 15px;
  width:238px;
  }
div.thumbline img
  {
  display:inline;
  
  margin:3px 40px 0px 0px;
  border:1px solid #ffffff;
  }
.ui-autocomplete {
    max-height: 250px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }
  
  .input-textUpperRed {
    font-family: arial,verdana;
    font-size: 12px;
    color: rgb(00,00,00);
    border: 1px solid rgb(171,173,179);
    height: 16px;
    text-decoration: none;
    /* background-color: rgb(214,236,236); */
    background-color:#f7d4d4;
}

div.upload-preview{ padding:5px 10px; background:#fff; border:1px solid #0099cc; position:relative;line-height:20px;
  			  color:#0099cc; border-radius:2px; text-align:left;  cursor:pointer; font-size:12px; font-weight:bold; font-family:arial,helvetica,serif; }
div.upload-preview:hover{background:#0099cc;color:#fff;}
div.upload-preview i{font-size:15px;vertical-align:tex-top;}
.hide_file { position: absolute; z-index: 1000; opacity: 0; cursor: pointer; right: 0; top: 0; height: 100%; font-size: 24px; width: 100%;}
.preview {border: 5px solid #F5F5F5; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.255); cursor:default;}
</style>
<style type="text/css">
		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		
		.modal-header { border-bottom: 1px solid #e5e5e5; min-height: 5.43px; padding: 10px;}		
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
		.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;}
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		a.tooltips {
  position: relative;
  display: inline;
} 
      .cropit-preview {
        background-color: #f8f8f8;
        display:none;
        background-size: cover;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 200px;
        height: 200px;
      }

      .cropit-preview-image-container {
        cursor: move;
      }

      .image-size-label {
        margin-top: 10px;
        display: none;
      }
      .custom-file-upload {
        border: 1px solid #bfdbff;
        border-radius: 4px;
        padding: 6px 12px;
     }
      
     .image-editor {
    	margin-left: 20px;
	   }
    .cropit-preview {
        background-color: #f8f8f8;
        display:none;
        background-size: cover;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 200px;
        height: 200px;
      }

      .cropit-preview-image-container {
        cursor: move;
      }

      .image-size-label {
        margin-top: 10px;
        display: none;
      }
      .custom-file-upload {
        border: 1px solid #bfdbff;
        border-radius: 4px;
        padding: 6px 12px;
     }
      
     .image-editor {
    	margin-left: 20px;
	   }
    </style>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/demo.css'/>" /> 
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery.cropit.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/demo.js"></script>
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
<script language="javascript" type="text/javascript"> 
 function findChangeFields() { 
	var fields='${changeFields}'; 
	fields=fields.replace('{','').replace('}','');
	var fieldsArray =fields.split(",");
	for(var i=0;i<fieldsArray.length;i++){
	try{
		var fieldName=fieldsArray[i];
		var formVariableName ="agentRequest."+fieldName+"";
		if(fieldName=='companyLogo'){
			document.forms['agentRequestForm'].elements['fileLogo'].className='input-textUpperRed';		
		}else{
		document.forms['agentRequestForm'].elements[formVariableName].className='input-textUpperRed';
		}
	}catch(e){}
	}

	
	<sec-auth:authComponent componentId="module.script.agentRequestForm.AgentScript">
	var elementsLen=document.forms['agentRequestForm'].elements.length;
	for(i=0;i<=elementsLen-1;i++){
		if(document.forms['agentRequestForm'].elements[i].type=='text'){
				document.forms['agentRequestForm'].elements[i].readOnly =false;
				document.forms['agentRequestForm'].elements[i].className = 'input-text';					
		}else if(document.forms['agentRequestForm'].elements[i].type=='textarea'){
			document.forms['agentRequestForm'].elements[i].readOnly =false;
			document.forms['agentRequestForm'].elements[i].className = 'textarea';
		}else{					
				document.forms['agentRequestForm'].elements[i].disabled=false;
		}
	 }
	
	autoPopulate_partner_billingCountry(document.forms['agentRequestForm'].elements['agentRequest.billingCountry']);
	autoPopulate_partner_terminalCountry(document.forms['agentRequestForm'].elements['agentRequest.terminalCountry']);
	autoPopulate_partner_mailingCountry(document.forms['agentRequestForm'].elements['agentRequest.mailingCountry']);
	</sec-auth:authComponent>
} 
function ShowCropitDetails() {
<c:if test="${!(agentRequest.companyLogo == null || agentRequest.companyLogo == '')}">
  document.getElementById('companyLogoShowHide').style.display = 'none';
 </c:if> 
  document.getElementById('cropit-previewShowHide').style.display = 'block';
  document.getElementById('image-size-labelShowHide').style.display = 'block';
  document.getElementById('cropit-image-zoom-inputShowHide').style.display = 'block'; 
}


function getHTTPObject() {
  var xmlhttp;
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (e) {
      xmlhttp = false;
    }
  }
  return xmlhttp;
}
var http = getHTTPObject();

function regExMatch(element,evt){
	//alert(element.readOnly);
	if(!element.readOnly){
		var key='';
		if (window.event)
			 key = window.event.keyCode;
		if (evt)
			  key = evt.which;
		var alphaExp = /^[a-zA-Z0-9-_\.'&'\\'&'\/'&'\"'&'\+'&'\,'&'\''\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\`'&'\='\' ']+$/;
		if(element.value.match(alphaExp)){
			return true;
		}
		else if ((key==null)||(key==0)||(key==8)||(key==9)||(key==13)||(key==27)||(key==20)||(key==16)||(key==17)||(key==32)||(key==18)||(key==27)||(key==35)||(key==36)||(key==45)||(key==39)||(key==37)||(key==38)||(key==40)||(key==116)){
			   return true;
		}
		else{
			//close non english validation because INTM want to add german language.. // 18-Dec-13
			//alert('Only English Alphabets are allowed.');
			//element.value = '';//element.value.substring(0,element.value.length-2);
			return true;
		}
	}else{
		return false;
	}
}
</script>

<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">

animatedcollapse.addDiv('pics', 'fade=1,hide=1')
animatedcollapse.addDiv('description', 'fade=1,hide=1')
animatedcollapse.addDiv('info', 'fade=1,hide=1')
animatedcollapse.addDiv('accinfo', 'fade=1,hide=1')
animatedcollapse.addDiv('vninfo', 'fade=1,hide=1')
animatedcollapse.addDiv('driver', 'fade=1,hide=1')
animatedcollapse.addDiv('comData', 'fade=1,hide=1')
animatedcollapse.addDiv('partnerportal', 'fade=0,hide=1')
animatedcollapse.init()

</script>
<script language="javascript" type="text/javascript">
function onLoad() {
	var f = document.getElementById('agentRequestForm'); 
	f.setAttribute("autocomplete", "off"); 
}
</script>
<script type="text/javascript"> 
  var loadFile = function(event) {
    var output = document.getElementById('companyLogo1');
    companyLogo1.src = URL.createObjectURL(event.target.files[0]);
  };


 function Upload(event) { 
    var fileUpload = document.getElementById("fileLogo"); 
    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
    if (regex.test(fileUpload.value.toLowerCase())) {
 
        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
 
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result; 
                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width = this.width; 
                    if (height > 400 || width > 400) {
                      alert("Height and Width must not exceed 400px."); 
                      document.forms['agentRequestForm'].elements['fileLogoUploadFlag'].value="NO";
                      
                      return false; 
                    }else{ 
                    loadFile(event)
                    return true;
                    }
                };
 
            }
        } else {
            alert("This browser does not support HTML5.");
            return false;
        }
    } else {
        alert("Please select a valid Image file.");
        return false;
    }
}

function copyBillToState(){
var abc=document.forms['agentRequestForm'].elements['agentRequest.billingState'].value;
document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value=abc;
}
function copyMailToState(){
var abc=document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value;
document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value=abc;
}
function copyTerToState(){
var abc=document.forms['agentRequestForm'].elements['agentRequest.billingState'].value;
document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value=abc;
}
function copyMailToTerToState(){
var abc=document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value;
document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value=abc;
}
		function copyBillToMail(){ 
			document.forms['agentRequestForm'].elements['agentRequest.mailingAddress1'].value=document.forms['agentRequestForm'].elements['agentRequest.billingAddress1'].value;
			if(document.forms['agentRequestForm'].elements['agentRequest.billingAddress2'].value==undefined){
				document.forms['agentRequestForm'].elements['agentRequest.billingAddress2'].value="";
			}
			document.forms['agentRequestForm'].elements['agentRequest.mailingAddress2'].value=document.forms['agentRequestForm'].elements['agentRequest.billingAddress2'].value;
			if(document.forms['agentRequestForm'].elements['agentRequest.billingAddress3'].value==undefined){
				document.forms['agentRequestForm'].elements['agentRequest.billingAddress3'].value="";
			}
			document.forms['agentRequestForm'].elements['agentRequest.mailingAddress3'].value=document.forms['agentRequestForm'].elements['agentRequest.billingAddress3'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.mailingAddress4'].value=document.forms['agentRequestForm'].elements['agentRequest.billingAddress4'].value;
			if(document.forms['agentRequestForm'].elements['agentRequest.billingEmail'].value==undefined){
				document.forms['agentRequestForm'].elements['agentRequest.billingEmail'].value="";
			}
			document.forms['agentRequestForm'].elements['agentRequest.mailingEmail'].value=document.forms['agentRequestForm'].elements['agentRequest.billingEmail'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.mailingCity'].value=document.forms['agentRequestForm'].elements['agentRequest.billingCity'].value;
			if(document.forms['agentRequestForm'].elements['agentRequest.billingFax'].value==undefined){
				document.forms['agentRequestForm'].elements['agentRequest.billingFax'].value="";
			}
			document.forms['agentRequestForm'].elements['agentRequest.mailingFax'].value=document.forms['agentRequestForm'].elements['agentRequest.billingFax'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.mailingZip'].value=document.forms['agentRequestForm'].elements['agentRequest.billingZip'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.mailingCountry'].value=document.forms['agentRequestForm'].elements['agentRequest.billingCountry'].value;
			if(document.forms['agentRequestForm'].elements['agentRequest.billingPhone'].value==undefined){
				document.forms['agentRequestForm'].elements['agentRequest.billingPhone'].value="";
			}
			document.forms['agentRequestForm'].elements['agentRequest.mailingPhone'].value=document.forms['agentRequestForm'].elements['agentRequest.billingPhone'].value;		
			//document.forms['agentRequestForm'].elements['agentRequest.mailingTelex'].value=document.forms['agentRequestForm'].elements['agentRequest.billingTelex'].value;	
			
			var oriCountry = document.forms['agentRequestForm'].elements['agentRequest.mailingCountry'].value;
				var enbState = '${enbState}';
			  	var index = (enbState.indexOf(oriCountry)> -1);
			  	if(index != ''){		
				var billingStateIndex = document.forms['agentRequestForm'].elements['agentRequest.billingState'].selectedIndex;
				var billingStateText = document.forms['agentRequestForm'].elements['agentRequest.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['agentRequestForm'].elements['agentRequest.billingState'].value;
				var x=document.getElementById("partnerAddForm_partner_mailingState")		    				
				getMailingState(document.forms['agentRequestForm'].elements['agentRequest.mailingCountry']);
				document.forms['agentRequestForm'].elements['agentRequest.mailingState'].disabled = false;
				setTimeout("copyBillToState()",1000);
				}else{
				var billingStateIndex = document.forms['agentRequestForm'].elements['agentRequest.billingState'].selectedIndex;
				var billingStateText = document.forms['agentRequestForm'].elements['agentRequest.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['agentRequestForm'].elements['agentRequest.billingState'].value;
				var x=document.getElementById("partnerAddForm_partner_mailingState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				document.forms['agentRequestForm'].elements['agentRequest.mailingState'].disabled = true;
			}
			  	changeAddressPin();
		}
		function copyTermToMail(){ 
			document.forms['agentRequestForm'].elements['agentRequest.mailingAddress1'].value=document.forms['agentRequestForm'].elements['agentRequest.terminalAddress1'].value;
			if(document.forms['agentRequestForm'].elements['agentRequest.terminalAddress2'].value==undefined){
				document.forms['agentRequestForm'].elements['agentRequest.terminalAddress2'].value="";
			}
			document.forms['agentRequestForm'].elements['agentRequest.mailingAddress2'].value=document.forms['agentRequestForm'].elements['agentRequest.terminalAddress2'].value;
			if(document.forms['agentRequestForm'].elements['agentRequest.terminalAddress3'].value==undefined){
				document.forms['agentRequestForm'].elements['agentRequest.terminalAddress3'].value="";
			}
			document.forms['agentRequestForm'].elements['agentRequest.mailingAddress3'].value=document.forms['agentRequestForm'].elements['agentRequest.terminalAddress3'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.mailingAddress4'].value=document.forms['agentRequestForm'].elements['agentRequest.terminalAddress4'].value;
			if(document.forms['agentRequestForm'].elements['agentRequest.terminalEmail'].value==undefined){
				document.forms['agentRequestForm'].elements['agentRequest.terminalEmail'].value="";
			}
			document.forms['agentRequestForm'].elements['agentRequest.mailingEmail'].value=document.forms['agentRequestForm'].elements['agentRequest.terminalEmail'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.mailingCity'].value=document.forms['agentRequestForm'].elements['agentRequest.terminalCity'].value;
			if(document.forms['agentRequestForm'].elements['agentRequest.terminalFax'].value==undefined){
				document.forms['agentRequestForm'].elements['agentRequest.terminalFax'].value="";
			}
			document.forms['agentRequestForm'].elements['agentRequest.mailingFax'].value=document.forms['agentRequestForm'].elements['agentRequest.terminalFax'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.mailingZip'].value=document.forms['agentRequestForm'].elements['agentRequest.terminalZip'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.mailingCountry'].value=document.forms['agentRequestForm'].elements['agentRequest.terminalCountry'].value;
			if(document.forms['agentRequestForm'].elements['agentRequest.terminalPhone'].value==undefined){
				document.forms['agentRequestForm'].elements['agentRequest.terminalPhone'].value="";
			}
			document.forms['agentRequestForm'].elements['agentRequest.mailingPhone'].value=document.forms['agentRequestForm'].elements['agentRequest.terminalPhone'].value;		
			//document.forms['agentRequestForm'].elements['agentRequest.mailingTelex'].value=document.forms['agentRequestForm'].elements['agentRequest.terminalTelex'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value=document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value;		
			
			var oriCountry = document.forms['agentRequestForm'].elements['agentRequest.mailingCountry'].value;
				var enbState = '${enbState}';
			  	var index = (enbState.indexOf(oriCountry)> -1);
			  	if(index != ''){			
				try{
				var billingStateIndex = document.forms['agentRequestForm'].elements['agentRequest.terminalState'].selectedIndex;
				var billingStateText = document.forms['agentRequestForm'].elements['agentRequest.terminalState'].options[billingStateIndex].text;
				}catch(e){}
				var billingStateValue = document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value;
				var x=document.getElementById("partnerAddForm_partner_mailingState")		    	
		    	getMailingState(document.forms['agentRequestForm'].elements['agentRequest.mailingCountry']);
				document.forms['agentRequestForm'].elements['agentRequest.mailingState'].disabled = false;
				setTimeout("copyMailToState()",1000);
			}else{
				try{
				var billingStateIndex = document.forms['agentRequestForm'].elements['agentRequest.terminalState'].selectedIndex;
				var billingStateText = document.forms['agentRequestForm'].elements['agentRequest.terminalState'].options[billingStateIndex].text;
				}catch(e){}
				var billingStateValue = document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value;
				var x=document.getElementById("partnerAddForm_partner_mailingState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				document.forms['agentRequestForm'].elements['agentRequest.mailingState'].disabled = true;
			}
			  	changeAddressPin();
			
		}
		function copyBillToTerm(){ 
			document.forms['agentRequestForm'].elements['agentRequest.terminalAddress1'].value=document.forms['agentRequestForm'].elements['agentRequest.billingAddress1'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalAddress2'].value=document.forms['agentRequestForm'].elements['agentRequest.billingAddress2'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalAddress3'].value=document.forms['agentRequestForm'].elements['agentRequest.billingAddress3'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalAddress4'].value=document.forms['agentRequestForm'].elements['agentRequest.billingAddress4'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalEmail'].value=document.forms['agentRequestForm'].elements['agentRequest.billingEmail'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalCity'].value=document.forms['agentRequestForm'].elements['agentRequest.billingCity'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalFax'].value=document.forms['agentRequestForm'].elements['agentRequest.billingFax'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalZip'].value=document.forms['agentRequestForm'].elements['agentRequest.billingZip'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalCountry'].value=document.forms['agentRequestForm'].elements['agentRequest.billingCountry'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalPhone'].value=document.forms['agentRequestForm'].elements['agentRequest.billingPhone'].value;		
			//document.forms['agentRequestForm'].elements['agentRequest.terminalTelex'].value=document.forms['agentRequestForm'].elements['agentRequest.billingTelex'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value=document.forms['agentRequestForm'].elements['agentRequest.billingState'].value;		
			
			var oriCountry = document.forms['agentRequestForm'].elements['agentRequest.terminalCountry'].value;
				var enbState = '${enbState}';
			  	var index = (enbState.indexOf(oriCountry)> -1);
			  	if(index != ''){
				var billingStateIndex = document.forms['agentRequestForm'].elements['agentRequest.billingState'].selectedIndex;
				var billingStateText = document.forms['agentRequestForm'].elements['agentRequest.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['agentRequestForm'].elements['agentRequest.billingState'].value;
				var x=document.getElementById("partnerAddForm_partner_terminalState")
		    	getTerminalState(document.forms['agentRequestForm'].elements['agentRequest.terminalCountry']);
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].disabled = false;
				setTimeout("copyTerToState()",1000);
			}else{
				var billingStateIndex = document.forms['agentRequestForm'].elements['agentRequest.billingState'].selectedIndex;
				var billingStateText = document.forms['agentRequestForm'].elements['agentRequest.billingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['agentRequestForm'].elements['agentRequest.billingState'].value;
				var x=document.getElementById("partnerAddForm_partner_terminalState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].disabled = true;
			
			}
		}
		function copyMailToTerm(){ 
			document.forms['agentRequestForm'].elements['agentRequest.terminalAddress1'].value=document.forms['agentRequestForm'].elements['agentRequest.mailingAddress1'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalAddress2'].value=document.forms['agentRequestForm'].elements['agentRequest.mailingAddress2'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalAddress3'].value=document.forms['agentRequestForm'].elements['agentRequest.mailingAddress3'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalAddress4'].value=document.forms['agentRequestForm'].elements['agentRequest.mailingAddress4'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalEmail'].value=document.forms['agentRequestForm'].elements['agentRequest.mailingEmail'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalCity'].value=document.forms['agentRequestForm'].elements['agentRequest.mailingCity'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalFax'].value=document.forms['agentRequestForm'].elements['agentRequest.mailingFax'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalZip'].value=document.forms['agentRequestForm'].elements['agentRequest.mailingZip'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalCountry'].value=document.forms['agentRequestForm'].elements['agentRequest.mailingCountry'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalPhone'].value=document.forms['agentRequestForm'].elements['agentRequest.mailingPhone'].value;		
			//document.forms['agentRequestForm'].elements['agentRequest.terminalTelex'].value=document.forms['agentRequestForm'].elements['agentRequest.mailingTelex'].value;		
			document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value=document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value;		
			
			var oriCountry = document.forms['agentRequestForm'].elements['agentRequest.terminalCountry'].value;
				var enbState = '${enbState}';
			  	var index = (enbState.indexOf(oriCountry)> -1);
			  	if(index != ''){	
				var billingStateIndex = document.forms['agentRequestForm'].elements['agentRequest.mailingState'].selectedIndex;
				var billingStateText = document.forms['agentRequestForm'].elements['agentRequest.mailingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value;
				var x=document.getElementById("partnerAddForm_partner_terminalState")
		    	getTerminalState(document.forms['agentRequestForm'].elements['agentRequest.terminalCountry']);
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].disabled = false;
				setTimeout("copyMailToTerToState()",1000);
			}else{
				var billingStateIndex = document.forms['agentRequestForm'].elements['agentRequest.mailingState'].selectedIndex;
				var billingStateText = document.forms['agentRequestForm'].elements['agentRequest.mailingState'].options[billingStateIndex].text;
		
				var billingStateValue = document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value;
				var x=document.getElementById("partnerAddForm_partner_terminalState")
		    	x.options[x.selectedIndex].text='';
		    	x.options[x.selectedIndex].value='';
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].disabled = true;
			}
		}
	</script>
<script type="text/javascript">
	function autoPopulate_partner_billingCountry1(targetElement) {
		
			var billingCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['agentRequestForm'].elements['agentRequest.billingCountryCode'].value=billingCountryCode.substring(0,billingCountryCode.indexOf(":")-1);
			targetElement.form.elements['agentRequest.billingCountry'].value=billingCountryCode.substring(billingCountryCode.indexOf(":")+2,billingCountryCode.length);
		}
		function autoPopulate_partner_terminalCountry1(targetElement) {
		
			var terminalCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['agentRequestForm'].elements['agentRequest.terminalCountryCode'].value=terminalCountryCode.substring(0,terminalCountryCode.indexOf(":")-1);
			targetElement.form.elements['agentRequest.terminalCountry'].value=terminalCountryCode.substring(terminalCountryCode.indexOf(":")+2,terminalCountryCode.length);
		}
		function autoPopulate_partner_mailingCountry1(targetElement) {
			
			var mailingCountryCode=targetElement.options[targetElement.selectedIndex].value;
			document.forms['agentRequestForm'].elements['agentRequest.mailingCountryCode'].value=mailingCountryCode.substring(0,mailingCountryCode.indexOf(":")-1);
			targetElement.form.elements['agentRequest.mailingCountry'].value=mailingCountryCode.substring(mailingCountryCode.indexOf(":")+2,mailingCountryCode.length);
	   }
 
	function autoPopulate_partner_billingInstruction1(targetElement) {
		
	}
	</script>
<script language="JavaScript">
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
</script>
<script type="text/javascript">

// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
var s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;	
	if (checkInternationalPhone(Phone)==false){
		alert("Please Enter a Valid Phone Number")
		targetElelemnt.value="";
		//document.forms['agentRequestForm'].elements[targetElelemnt].focus();
		//targetElement.focus = true;
		return false;
	}
	return true;
 }
</script>
<script type="text/javascript">
function requiredState(){
    var originCountry = document.forms['agentRequestForm'].elements['agentRequest.billingCountry'].value; 
	var r2=document.getElementById('redHiddenFalse');
	var r1=document.getElementById('redHidden');
	if(originCountry == 'United States' || originCountry == 'Canada' || originCountry == 'India'){	
	    var enbState = '${enbState}';
		var index = (enbState.indexOf(originCountry)> -1);
		if(index != ''){
		r1.style.display = 'block';
		r2.style.display = 'none';
		}else{	
			r1.style.display = 'none';
			r2.style.display = 'block';		
		}
	}else{	
		r1.style.display = 'none';
		r2.style.display = 'block';		
	}
}

function autoPopulate_partner_billingCountry(targetElement) {	
   	var oriCountry = targetElement.value;
    var enbState = '${enbState}';
	var index = (enbState.indexOf(oriCountry)> -1);
	if(index != ''){
		document.forms['agentRequestForm'].elements['agentRequest.billingState'].disabled = false;		
		if('${partnerType}' == 'OO'){
			document.forms['agentRequestForm'].elements['partnerPrivate.licenseState'].disabled = false;
		}
	}else{
		document.forms['agentRequestForm'].elements['agentRequest.billingState'].disabled = true;		
		document.forms['agentRequestForm'].elements['agentRequest.billingState'].value = '';
		if('${partnerType}' == 'OO'){
			document.forms['agentRequestForm'].elements['partnerPrivate.licenseState'].disabled = true;
			document.forms['agentRequestForm'].elements['partnerPrivate.licenseState'].value = '';
		}
	}
		
}
function autoPopulate_partner_terminalCountry(targetElement) {   
		var dCountry = targetElement.value;
		 var enbState = '${enbState}';
		var index = (enbState.indexOf(dCountry)> -1);
		if(index != ''){
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].disabled = false;		
		}else{
			document.forms['agentRequestForm'].elements['agentRequest.terminalState'].disabled = true;
			document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value = '';
		}
	}	
	
function autoPopulate_partner_mailingCountry(targetElement) {   
	var dCountry = targetElement.value;
	var enbState = '${enbState}';
	var index = (enbState.indexOf(dCountry)> -1);
	if(index != ''){
			document.forms['agentRequestForm'].elements['agentRequest.mailingState'].disabled = false;
		}else{
			document.forms['agentRequestForm'].elements['agentRequest.mailingState'].disabled = true;
			document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value = '';
		}
	}
var map;
   
function getBillingCountryCode(targetElement){
	var countryName=document.forms['agentRequestForm'].elements['agentRequest.billingCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseBillingCountry;
    http4.send(null);
}

function handleHttpResponseBillingCountry(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                if(results.length>=1){
 					document.forms['agentRequestForm'].elements['agentRequest.billingCountryCode'].value = results;
 					//document.forms['agentRequestForm'].elements['agentRequest.billingCountryCode'].select();
					 } else{                     
                 }
             }
        }
         
 function getMailingCountryCode(targetElement){
	var countryName=document.forms['agentRequestForm'].elements['agentRequest.mailingCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http5.open("GET", url, true);
    http5.onreadystatechange = handleHttpResponseMailingCountry;
    http5.send(null);
}

function handleHttpResponseMailingCountry(){
             if (http5.readyState == 4){
                var results = http5.responseText
                results = results.trim();
                if(results.length>=1){
 					document.forms['agentRequestForm'].elements['agentRequest.mailingCountryCode'].value = results;
 					//document.forms['agentRequestForm'].elements['agentRequest.mailingCountryCode'].select();
 					 }else{                     
                 }
             }
        }
         
function getTerminalCountryCode(){
	var countryName=document.forms['agentRequestForm'].elements['agentRequest.terminalCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponseTerminalCountry;
    http6.send(null);
}

function handleHttpResponseTerminalCountry(){
             if (http6.readyState == 4){
                var results = http6.responseText
                results = results.trim();
                if(results.length>=1){
 					document.forms['agentRequestForm'].elements['agentRequest.terminalCountryCode'].value = results;
 					//document.forms['agentRequestForm'].elements['agentRequest.terminalCountryCode'].select();
					 } else{                     
                 }
             }
        }
        
function checkVendorName(){
	document.forms['agentRequestForm'].elements['agentRequest.agentParentName'].value=""; 
    var vendorId = document.forms['agentRequestForm'].elements['agentRequest.agentParent'].value;
    if(vendorId==''){
    	document.forms['agentRequestForm'].elements['agentRequest.agentParentName'].value=""; 
    }else{
    	document.forms['agentRequestForm'].elements['parent'].value="found";
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http22.open("GET", url, true);
     http22.onreadystatechange = handleHttpResponsePA;
     http22.send(null);
    }
}
function handleHttpResponsePA(){
      if (http22.readyState == 4){
    	  		var partnerType = document.forms['agentRequestForm'].elements['partnerType'].value;
                var results = http22.responseText
                results = results.trim();
                var res = results.split("#");
                document.forms['agentRequestForm'].elements['parent'].value="";
                if(res.length>2){
                	if(res[2] == 'Approved'){
	           			document.forms['agentRequestForm'].elements['agentRequest.agentParentName'].value = res[1];
	           			
                	}else{
	           			if(partnerType ==  'AC'){
                			alert("Parent Account code is not approved" ); 
                		}else{
                			alert("Agent Parent code is not approved" );
                		}
					    document.forms['agentRequestForm'].elements['agentRequest.agentParent'].value="";
					    document.forms['agentRequestForm'].elements['agentRequest.agentParentName'].value="";
					    document.forms['agentRequestForm'].elements['agentRequest.agentParent'].select();
					    showAddressImageSub2();
					   // showOrHide(0);
	           		}  
               	}else{
                     if(partnerType ==  'AC'){
                			alert("Parent Account code is not valid" ); 
                		}else{
                			alert("Agent Parent code is not valid" );
                		}
                 	 document.forms['agentRequestForm'].elements['agentRequest.agentParent'].value="";
                 	 document.forms['agentRequestForm'].elements['agentRequest.agentParentName'].value="";
                 	 document.forms['agentRequestForm'].elements['agentRequest.agentParent'].select();
                 	 showAddressImageSub2();
                 	// showOrHide(0);
			   }
      }
}

</script>
<SCRIPT LANGUAGE="JavaScript">
function autoPopulate_partner_billingCountry1(targetElement) {
	       var red=document.getElementById("redHidden");
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['agentRequestForm'].elements['agentRequest.billingState'].disabled = false;
			red.style.display='block';
		}else{
			document.forms['agentRequestForm'].elements['agentRequest.billingState'].disabled = true;
			red.style.display='none';
			document.forms['agentRequestForm'].elements['agentRequest.billingState'].value = '';
		}
	}
	function autoPopulate_partner_terminalCountry1(targetElement) {
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].disabled = false;
		}else{
			document.forms['agentRequestForm'].elements['agentRequest.terminalState'].disabled = true;
			document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value = '';
		}
	}
	function autoPopulate_partner_mailingCountry1(targetElement) {
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
				document.forms['agentRequestForm'].elements['agentRequest.mailingState'].disabled = false;
		}else{
			document.forms['agentRequestForm'].elements['agentRequest.mailingState'].disabled = true;
			document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value = '';
		}
	}
</script>
<SCRIPT LANGUAGE="JavaScript">
function getBillingState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
	
	}
function getMailingState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse3;
     http3.send(null);
	
	}	
function getTerminalState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse4;
     http4.send(null);
	
	}
	
</script>
<SCRIPT LANGUAGE="JavaScript">	

function getBillingState(targetElement) {
	var country = document.forms['agentRequestForm'].elements['agentRequest.billingCountry'].value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
	
	}
	
function getMailingState(targetElement) {
	//#7148 -To add the status drop down for private party partner
	//var country = targetElement.value;
	var country=document.forms['agentRequestForm'].elements['agentRequest.mailingCountry'].value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
	countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse3;
     http3.send(null);
	
	}	
	
function getTerminalState(targetElement) {
	//#7148 -To add the status drop down for private party partner
	//var country = targetElement.value;
	var country=document.forms['agentRequestForm'].elements['agentRequest.terminalCountry'].value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
	countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse4;
     http4.send(null);
	
	}	

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}


function handleHttpResponse2(){
	if (http2.readyState == 4){
        var results = http2.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['agentRequestForm'].elements['agentRequest.billingState'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['agentRequestForm'].elements['agentRequest.billingState'].options[i].text = '';
				document.forms['agentRequestForm'].elements['agentRequest.billingState'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				document.forms['agentRequestForm'].elements['agentRequest.billingState'].options[i].text = stateVal[1];
				document.forms['agentRequestForm'].elements['agentRequest.billingState'].options[i].value = stateVal[0];				
				document.forms['agentRequestForm'].elements['agentRequest.billingState'].value='${agentRequest.billingState}';
				}
		}			
		if('${partnerType}' == 'OO'){
			var targetElementLic = document.forms['agentRequestForm'].elements['partnerPrivate.licenseState'];
			targetElementLic.length = res.length;
			for(i=0;i<res.length;i++){
				if(res[i] == ''){
					document.forms['agentRequestForm'].elements['partnerPrivate.licenseState'].options[i].text = '';
					document.forms['agentRequestForm'].elements['partnerPrivate.licenseState'].options[i].value = '';
				}else{
					stateVal = res[i].split("#");
					document.forms['agentRequestForm'].elements['partnerPrivate.licenseState'].options[i].text = stateVal[1];
					document.forms['agentRequestForm'].elements['partnerPrivate.licenseState'].options[i].value = stateVal[0];
					document.forms['agentRequestForm'].elements['partnerPrivate.licenseState'].value='${partnerPrivate.licenseState}';
					}
			}
		}
	}
}  
        
function handleHttpResponse3(){

             if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['agentRequestForm'].elements['agentRequest.mailingState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['agentRequestForm'].elements['agentRequest.mailingState'].options[i].text = '';
					document.forms['agentRequestForm'].elements['agentRequest.mailingState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['agentRequestForm'].elements['agentRequest.mailingState'].options[i].text = stateVal[1];
					document.forms['agentRequestForm'].elements['agentRequest.mailingState'].options[i].value = stateVal[0];
					document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value='${agentRequest.mailingState}';
					}
					}
             }
        }  

function handleHttpResponse4(){

             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['agentRequestForm'].elements['agentRequest.terminalState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					try{
					if(res[i] == ''){
					document.forms['agentRequestForm'].elements['agentRequest.terminalState'].options[i].text = '';
					document.forms['agentRequestForm'].elements['agentRequest.terminalState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['agentRequestForm'].elements['agentRequest.terminalState'].options[i].text = stateVal[1];
					document.forms['agentRequestForm'].elements['agentRequest.terminalState'].options[i].value = stateVal[0];
					document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value='${agentRequest.terminalState}';
					}
					}catch(e){}
					}
             }
        }  
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http2 = getHTTPObject();
var http22 = getHTTPObject22();

function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}


function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1();
    var http5 = getHTTPObject1()
    var http6 = getHTTPObject1()
function getHTTPObject2()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject2();    
    var updateBankRecordHttp = getHTTPObject2();    
</script>
<script>
function changeStatus(){
	document.forms['agentRequestForm'].elements['formStatus'].value = '1';
}

function validateForm(){
	
	
	document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = false;
	document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled =false;
	
	var x = document.getElementById("file").value;
	//alert(x)
	if(document.forms['agentRequestForm'].elements['agentRequest.lastName'].value.trim() == ''){
			alert('Please enter the last name');
			document.forms['agentRequestForm'].elements['agentRequest.lastName'].focus();
			//document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
			document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled =true;
			return false;
	}

	if(document.forms['agentRequestForm'].elements['agentRequest.billingAddress1'].value.trim() == ''){
			alert('Please enter the billing address');
			//document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
			document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled =true;
			document.forms['agentRequestForm'].elements['agentRequest.billingAddress1'].focus();
			return false;
	}
	if(document.forms['agentRequestForm'].elements['agentRequest.billingCountry'].value == ''){
			alert('Please select the billing country');
			//document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
			document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled =true;
			document.forms['agentRequestForm'].elements['agentRequest.billingCountry'].focus();
			return false;
	}
	var billingCountry = document.forms['agentRequestForm'].elements['agentRequest.billingCountry'].value;
		if(billingCountry == 'United States' || billingCountry == 'Canada' || billingCountry == 'India' ){
	    	var enbState = '${enbState}';
			var index = (enbState.indexOf(billingCountry)> -1);
			if(index != ''){
		if(document.forms['agentRequestForm'].elements['agentRequest.billingState'].value == ''){
			alert('Please select the billing State.');
			//document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
			document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled =true;
			document.forms['agentRequestForm'].elements['agentRequest.billingState'].focus();
			return false;
		}
		}
	}
	if('${partnerType}' == 'AG' || '${partnerType}' == 'VN' || '${partnerType}' == 'CR'){
	var terminalCountry = document.forms['agentRequestForm'].elements['agentRequest.terminalCountry'].value;
		if(terminalCountry == 'United States'){
			if(document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value == ''){
				alert('Please select the terminal state.');
				//document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
				document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled =true;
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].focus();
				return false;
			}
		}
	}
	var mailingCountry = document.forms['agentRequestForm'].elements['agentRequest.mailingCountry'].value;
	if(mailingCountry == 'United States'){
		if(document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value == ''){
			alert('Please select the mailing state.');
			//document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
			document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled =true;
			document.forms['agentRequestForm'].elements['agentRequest.mailingState'].focus();
			return false;
		}
	}
	if(document.forms['agentRequestForm'].elements['agentRequest.billingCity'].value.trim() == ''){
			alert('Please select the billing city');
			//document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
			document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled =true;
			document.forms['agentRequestForm'].elements['agentRequest.billingCity'].focus();
			return false;
	}
	if(document.forms['agentRequestForm'].elements['agentRequest.billingPhone'].value.trim() == ''){
			alert('Please select the billing phone');
			//document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
			document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled =true;
			document.forms['agentRequestForm'].elements['agentRequest.billingPhone'].focus();
			return false;
	}
	if('${partnerType}' == 'CR'){
	var checkBoxSea = document.forms['agentRequestForm'].elements['agentRequest.sea'].checked;
	var checkBoxSurface = document.forms['agentRequestForm'].elements['agentRequest.surface'].checked;
	var checkBoxAir = document.forms['agentRequestForm'].elements['agentRequest.air'].checked;	
	if(checkBoxSea==false && checkBoxSurface==false && checkBoxAir==false){
	alert('Please Select the Carrier Detail As Sea, Surface or Air.');
	//document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
	document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled =true;
	return false;
	}
	}
	<configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">
	var partnerType=document.forms['agentRequestForm'].elements['partnerType'].value
	 if( document.forms['agentRequestForm'].elements['agentRequest.bankAccountNumber'].value==''&& partnerType=='AG'){
		alert('Please enter the Bank Account Number');
		//document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
		document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled =true;
		return false;
	}
	</configByCorp:fieldVisibility>
	
	var returnChk=document.forms['agentRequestForm'].elements['chkVatValidation'].value;
		if(returnChk=='Y'){
		document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = false;
		return checkLength();
	}else{
		document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
		document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled =true;
		return false;
	}

		
		
}




function removePhotograph1(){
	document.forms['agentRequestForm'].elements['agentRequest.location1'].value = '';
	document.getElementById("userImage1").src = "${pageContext.request.contextPath}/images/no-image.png";
	}
function removePhotograph2(){
	document.forms['agentRequestForm'].elements['agentRequest.location2'].value = '';
	document.getElementById("userImage2").src = "${pageContext.request.contextPath}/images/no-image.png";
	}
function removePhotograph3(){
	document.forms['agentRequestForm'].elements['agentRequest.location3'].value = '';
	document.getElementById("userImage3").src = "${pageContext.request.contextPath}/images/no-image.png";
	}
function removePhotograph4(){
	document.forms['agentRequestForm'].elements['agentRequest.location4'].value = '';
	document.getElementById("userImage4").src = "${pageContext.request.contextPath}/images/no-image.png";
	}
	
function isURL(){
	var urlStr=document.forms['agentRequestForm'].elements['agentRequest.url'].value;
	
	if (urlStr.indexOf(" ") != -1) {
		alert("Spaces are not allowed in a URL");
		document.forms['agentRequestForm'].elements['agentRequest.url'].value="";
		return false;
	}

	if (urlStr.value == "" || urlStr.value == null) {
		return true;
	}

  	urlStr = urlStr.value.toLowerCase();

	var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var atom=validChars + '+';
	var urlPat=/^http:\/\/(\w*)\.([\-\+a-z0-9]*)\.(\w*)/;
	var matchArray=urlStr.match(urlPat);

	if(matchArray == null){
		alert("The URL seems incorrect check it begins with http:/\/ and it has 2 .'s");
		document.forms['agentRequestForm'].elements['agentRequest.url'].value="";
		return false;
	}
	return true;
}
	


function  checkLength(){
	
	var  partnerType=document.forms['agentRequestForm'].elements['partnerType'].value;
	
 	if(partnerType=='AG')
	{
	var txt = document.forms['agentRequestForm'].elements['agentRequest.companyProfile'].value;
	var txt1 = document.forms['agentRequestForm'].elements['agentRequest.companyFacilities'].value;
	var txt2 = document.forms['agentRequestForm'].elements['agentRequest.companyCapabilities'].value;
	var txt3 = document.forms['agentRequestForm'].elements['agentRequest.companyDestiantionProfile'].value;
	if(txt.length >1500){
		alert('You can not enter more than 1500 characters in companyProfile');
		document.forms['agentRequestForm'].elements['agentRequest.companyProfile'].value = txt.substring(0,1498);
		return false;
	}
	if(txt1.length >1500){
		alert('You can not enter more than 1500 characters in companyFacilities');
		document.forms['agentRequestForm'].elements['agentRequest.companyFacilities'].value = txt1.substring(0,1498);
		return false;
	}
	if(txt2.length >1500){
		alert('You can not enter more than 1500 characters in companyCapabilities');
		document.forms['agentRequestForm'].elements['agentRequest.companyCapabilities'].value = txt2.substring(0,1498);
		return false;
	}
	if(txt3.length >1500){
		alert('You can not enter more than 1500 characters in companyDestiantionProfile');
		document.forms['agentRequestForm'].elements['agentRequest.companyDestiantionProfile'].value = txt3.substring(0,1498);
		return false;
	}
	}
}

function pinLocation(){
	  var $j = jQuery.noConflict();
	  
	    $j('#mapModal').modal({show:true,backdrop: 'static',keyboard: false});
	    
      var lat =  parseFloat(document.forms['agentRequestForm'].elements['agentRequest.latitude'].value);    
      var lng =  parseFloat(document.forms['agentRequestForm'].elements['agentRequest.longitude'].value);
      var uluru = {lat: lat, lng: lng};
      
      var lastName=document.getElementById('lastName').value;
      var geocoder = new google.maps.Geocoder();
  	 var map = new google.maps.Map(
       document.getElementById('map2'), {zoom: 16, center: uluru});
  	 var marker = new google.maps.Marker({position: uluru, map: map, title: lastName,draggable: true});
  	 var contentString ='<h5>'+lastName+'</h5>'+
       '<div id="bodyContent">'+getMailingAdress()+
       '</div>'+
       '</div>';

  	google.maps.event.addListener(marker, 'dragend', function(e) {

  	document.getElementById('lat').value = e.latLng.lat().toFixed(4) ; 
  	document.getElementById('lng').value = e.latLng.lng().toFixed(4);
  	
  	var latlng = {lat:parseFloat(document.getElementById('lat').value), lng: parseFloat(document.getElementById('lng').value)};
    	 geocoder.geocode({'location': latlng}, function(results, status) {
    	
     
    		marker.addListener('click', function() {
    			title: lastName,
              infowindow.open(map, marker);
            });
    		infowindow.setContent('<h5>'+lastName+'</h5>'+
    		         '<div id="bodyContent">Pin Address:'+results[0].formatted_address+
    		         '</div>'+
    		         '</div>');
    	 });     	
    	 
});
  	marker.addListener('click', function() {
         infowindow.open(map, marker);
       });
  	
  	 var infowindow = new google.maps.InfoWindow({
         content: contentString
       });      
}


function convertToMiles(){
	var km=document.forms['agentRequestForm'].elements['agentRequest.serviceRangeKms'].value;
	var kms=0.621371192*km;
	var rounded=Math.round(kms);
	document.forms['agentRequestForm'].elements['agentRequest.serviceRangeMiles'].value=rounded;
}
function convertToKm(){
	var miles=document.forms	['agentRequestForm'].elements['agentRequest.serviceRangeMiles'].value;
	var miless=1.609344*miles;
	var rounded=Math.round(miless);
	document.forms['agentRequestForm'].elements['agentRequest.serviceRangeKms'].value=rounded;
}

function convertToFeet(){
	var meters=document.forms['agentRequestForm'].elements['agentRequest.facilitySizeSQMT'].value;
	var feet=10.77*meters;
	var rounded=Math.round(feet);
	document.forms['agentRequestForm'].elements['agentRequest.facilitySizeSQFT'].value=rounded;
}

function convertToMeters(){
	var feet=document.forms['agentRequestForm'].elements['agentRequest.facilitySizeSQFT'].value;
	var meters=0.093*feet;
	var rounded=Math.round(meters);
	document.forms['agentRequestForm'].elements['agentRequest.facilitySizeSQMT'].value=rounded;
}



function openWebSite(){
var  www="www.";
var	http= "http://";
var theUrl = document.forms['agentRequestForm'].elements['agentRequest.url'].value;
	if(theUrl.substring(0,4)==www)
	{
		theUrl=http+theUrl;
	}
if(theUrl.match(/^(http|ftp)\:\/\/\w+([\.\-]\w+)*\.\w{2,4}(\:\d+)*([\/\.\-\?\&\%\#]\w+)*\/?$/i)){
   if(theUrl.substring(7,11)==www)
    {
	   	window.open(theUrl);
	}
	else{
		alert("Please enter a valid URL");
	  	theUrl.select();
    	theUrl.focus();
     	document.forms['agentRequestForm'].elements['agentRequest.url'].value='';
	  	return false;
		}
}
else
{
	alert("Please enter a valid URL");
    theUrl.select();
    theUrl.focus();
     document.forms['agentRequestForm'].elements['agentRequest.url'].value='';
    return false;
}
}


</script>

<script language="JavaScript">	
</script>


<style type="text/css">
.myClass {
	height: 150px;
	width: auto
}

.urClass {
	height: auto;
	width: 150px;
}

input[type=checkbox] {
	vertical-align: middle;
}
/* collapse */
.subcontent-tab {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #15428B;
	text-decoration: none;
	background: url(images/collapsebg.gif) #BCD2EF;
	padding: 2px 3px 3px 5px;
	height: 15px;
	border: 1px solid #99BBE8;
	border-right: none;
	border-left: none;
}

a.dsphead {
	text-decoration: none;
	color: #000000;
}

.dspchar2 {
	padding-left: 0px;
}
</style>
<script language="javascript">
function secImage1(){


if(document.getElementById("userImage1").height > 150)
{
 document.getElementById("userImage1").style.height = "120px";
 document.getElementById("userImage1").style.width = "auto";
}
}
function secImage2(){

if(document.getElementById("userImage2").height > 150 )
{
 document.getElementById("userImage2").style.height = "120px";
 document.getElementById("userImage2").style.width = "auto";
}        

}
function secImage3(){
if(document.getElementById("userImage3").height > 150)
{
 document.getElementById("userImage3").style.height = "120px";
 document.getElementById("userImage3").style.width = "auto";
} 
}
function secImage4(){
if(document.getElementById("userImage4").height > 150)
{
 document.getElementById("userImage4").style.height = "120px";
 document.getElementById("userImage4").style.width = "auto";
} 
}





function checkDate(){
//alert("hi");
showOrHide(1);
	var parent=document.forms['agentRequestForm'].elements['parent'].value;
	var parentdescription=document.forms['agentRequestForm'].elements['agentRequest.agentParentName'].value;

	if((parent=="found")&&(parentdescription.trim()=="")){
		return false;
	}	
	try{
		
 var date1 = document.forms['agentRequestForm'].elements['hire'].value;	
 var date2 = document.forms['agentRequestForm'].elements['termination'].value;
 var mySplitResult = date1.split("-");
 var day = mySplitResult[0];
 var month = mySplitResult[1];
 var year = mySplitResult[2];
if(month == 'Jan')
 {
     month = "01";
 }
 else if(month == 'Feb')
 {
     month = "02";
 }
 else if(month == 'Mar')
 {
     month = "03"
 }
 else if(month == 'Apr')
 {
     month = "04"
 }
 else if(month == 'May')
 {
     month = "05"
 }
 else if(month == 'Jun')
 {
     month = "06"
 }
 else if(month == 'Jul')
 {
     month = "07"
 }
 else if(month == 'Aug')
 {
     month = "08"
 }
 else if(month == 'Sep')
 {
     month = "09"
 }
 else if(month == 'Oct')
 {
     month = "10"
 }
 else if(month == 'Nov')
 {
     month = "11"
 }
 else if(month == 'Dec')
 {
     month = "12";
 }
 var finalDate = day+"-"+month+"-"+year;
 var mySplitResult2 = date2.split("-");
 var day2 = mySplitResult2[0];
 var month2 = mySplitResult2[1];
 var year2 = mySplitResult2[2];
 if(month2 == 'Jan')
 {
     month2 = "01";
 }
 else if(month2 == 'Feb')
 {
     month2 = "02";
 }
 else if(month2 == 'Mar')
 {
     month2 = "03"
 }
 else if(month2 == 'Apr')
 {
     month2 = "04"
 }
 else if(month2 == 'May')
 {
     month2 = "05"
 }
 else if(month2 == 'Jun')
 {
     month2 = "06"
 }
 else if(month2 == 'Jul')
 {
     month2 = "07"
 }
 else if(month2 == 'Aug')
 {
     month2 = "08"
 }
 else if(month2 == 'Sep')
 {
     month2 = "09"
 }
 else if(month2 == 'Oct')
 {
     month2 = "10"
 }
 else if(month2 == 'Nov')
 {
     month2 = "11"
 }
 else if(month2 == 'Dec')
 {
     month2 = "12";
 }
 /*if(date1 == null || date1 == ''){
	alert("Please Select Hire Date.");
	document.forms['agentRequestForm'].elements['termination'].value='';
	return false;
 }*/
var finalDate2 = day2+"-"+month2+"-"+year2;
date1 = finalDate.split("-");
date2 = finalDate2.split("-");
var sDate = Calendar.parseDate(finalDate,false);
var eDate = Calendar.parseDate(finalDate2,false);
var daysApart = 0;
if((eDate != null && eDate != undefined))
	daysApart = Math.round((eDate-sDate)/86400000);

if(daysApart<1)
{
	if(document.forms['agentRequestForm'].elements['termination'].value != ''){
 		 alert("Termination Date should be greater than Hire Date.");
  			document.forms['agentRequestForm'].elements['termination'].value='';
  			return false;
	}
} 
	}catch(e){}
return true;
}

function notExists(){
	alert("The Account information has not been saved yet, please save account information to continue");
	
}

 function resetBillingState(target) {
	 	var country = target;
	 	var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
		</c:forEach>
		var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	    //alert(url);
	     http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponse12;
	     http2.send(null);
		
		}
 function handleHttpResponse12(){
		if (http2.readyState == 4){
	        var results = http2.responseText
	        results = results.trim();
	        res = results.split("@");
	        targetElement = document.forms['agentRequestForm'].elements['agentRequest.billingState'];
			targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(res[i] == ''){
					document.forms['agentRequestForm'].elements['agentRequest.billingState'].options[i].text = '';
					document.forms['agentRequestForm'].elements['agentRequest.billingState'].options[i].value = '';
				}else{
					stateVal = res[i].split("#");
					//document.forms['agentRequestForm'].elements['agentRequest.billingState'].selectedIndex=true;
					document.forms['agentRequestForm'].elements['agentRequest.billingState'].options[i].text = stateVal[1];
					document.forms['agentRequestForm'].elements['agentRequest.billingState'].options[i].value = stateVal[0];
					document.forms['agentRequestForm'].elements['agentRequest.billingState'].value='${agentRequest.billingState}';
				}
			}	
			
		}
	}  
 function resetmailingState(target) {
		var country = target;
	 	var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
		</c:forEach>
		var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	    //alert(url);
	     http4.open("GET", url, true);
	     http4.onreadystatechange = handleHttpResponse13;
	     http4.send(null);
		
		}
function handleHttpResponse13(){
		if (http4.readyState == 4){
	        var results = http4.responseText
	        results = results.trim();
	        res = results.split("@");
	        targetElement = document.forms['agentRequestForm'].elements['agentRequest.mailingState'];
			targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(res[i] == ''){
					document.forms['agentRequestForm'].elements['agentRequest.mailingState'].options[i].text = '';
					document.forms['agentRequestForm'].elements['agentRequest.mailingState'].options[i].value = '';
				}else{
					stateVal = res[i].split("#");
					//document.forms['agentRequestForm'].elements['agentRequest.billingState'].selectedIndex=true;
					document.forms['agentRequestForm'].elements['agentRequest.mailingState'].options[i].text = stateVal[1];
					document.forms['agentRequestForm'].elements['agentRequest.mailingState'].options[i].value = stateVal[0];
					document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value='${agentRequest.mailingState}';
				}
			}	
			
		}
	}
function resetterminalState(target) {
	var country = target;
 	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http14.open("GET", url, true);
     http14.onreadystatechange = handleHttpResponse144;
     http14.send(null);
	
	}
function handleHttpResponse144(){
	if (http14.readyState == 4){
        var results = http14.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['agentRequestForm'].elements['agentRequest.terminalState'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].options[i].text = '';
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				//document.forms['agentRequestForm'].elements['agentRequest.billingState'].selectedIndex=true;
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].options[i].text = stateVal[1];
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].options[i].value = stateVal[0];
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value='${agentRequest.terminalState}';
			}
		}	
		
	}
}
var http14 = getHTTPObject();
function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 function reset_partner_billingCountry() {  
	   	var oriCountry ='${agentRequest.billingCountry}';
	   	var red=document.getElementById("redHidden");
	   	var enbState = '${enbState}';
		var index = (enbState.indexOf(oriCountry)> -1);
		if(index != '' && oriCountry != ''){
			document.forms['agentRequestForm'].elements['agentRequest.billingState'].disabled = false;
			/* if(enbState != ''){
				document.getElementById("redHiddenFalse").style.display = 'none';
				red.style.display='block';
			} */
			resetBillingState(oriCountry);
		}else{
			document.forms['agentRequestForm'].elements['agentRequest.billingState'].disabled = true;
			red.style.display='none';
			document.getElementById("redHiddenFalse").style.display='block';
			document.forms['agentRequestForm'].elements['agentRequest.billingState'].value = '';
			
		}
			
	}
 function reset_partner_mailingCountry() {   
		var dCountry ='${agentRequest.mailingCountry}';
		var enbState = '${enbState}';
		var index = (enbState.indexOf(dCountry)> -1);
		if(index != '' && dCountry != ''){
				document.forms['agentRequestForm'].elements['agentRequest.mailingState'].disabled = false;
				resetmailingState(dCountry);	
			}else{
				document.forms['agentRequestForm'].elements['agentRequest.mailingState'].disabled = true;
				document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value = '';
			}
		}
 function reset_partner_terminalCountry() {   
		var dCountry ='${agentRequest.terminalCountry}';
		var enbState = '${enbState}';
		var index = (enbState.indexOf(dCountry)> -1);
		if(index != ''){
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].disabled = false;
				resetterminalState(dCountry);	
			}else{
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].disabled = true;
				document.forms['agentRequestForm'].elements['agentRequest.terminalState'].value = '';
			}
		}
 function disabledAll(){
		var elementsLen=document.forms['agentRequestForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++){
			if(document.forms['agentRequestForm'].elements[i].type=='text'){
				document.forms['agentRequestForm'].elements[i].readOnly =true;
				document.forms['agentRequestForm'].elements[i].className = 'input-textUpper';
			}else if(document.forms['agentRequestForm'].elements[i].type=='textarea'){
				document.forms['agentRequestForm'].elements[i].readOnly =true;
				document.forms['agentRequestForm'].elements[i].className = 'textareaUpper';
			}else{
				document.forms['agentRequestForm'].elements[i].disabled=true;
			} 
		}
	}

 
 function handleHttpResponseVatValidation(){
 	if (httpVat.readyState == 4) {
 	 	var results = httpVat.responseText;
 		results = results.trim(); 
        var res = results.split('^'); 
        if(res[0]=='N'){
            var type=res[1];
 		   var agree = confirm(""+type+"\n"+"Click Ok to continue,"+"\n"+" or Click Cancel. ");
 			if(agree){
 	 			document.forms['agentRequestForm'].elements['chkVatValidation'].value="Y";
 				return true;
 			}else{
 			document.forms['agentRequestForm'].elements['chkVatValidation'].value="N";
 			   return false;	
 			}
     	 }else{
     		document.forms['agentRequestForm'].elements['chkVatValidation'].value="Y";
         	return true;
 		}
 	}
      progressBarAutoSave('0');
 }


 function getHTTPObject()
 {
     var xmlhttp;
     if(window.XMLHttpRequest)
     {
         xmlhttp = new XMLHttpRequest();
     }
     else if (window.ActiveXObject)
     {
         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
         if (!xmlhttp)
         {
             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
         }
     }
     return xmlhttp;
 }
 var httpVat = getHTTPObject();
 var httpCC = getHTTPObject();

 
 function enableStateListBill(){ 
		var billCon=document.forms['agentRequestForm'].elements['agentRequest.billingCountry'].value;
		  var enbState = '${enbState}';
		  var index = (enbState.indexOf(billCon)> -1);
		  if(index != ''){
		  		document.forms['agentRequestForm'].elements['agentRequest.billingState'].disabled = false;
		  }else{
			  document.forms['agentRequestForm'].elements['agentRequest.billingState'].disabled = true;
		  }	        
	}
 function enableStateListTerm(){ 
		var termCon=document.forms['agentRequestForm'].elements['agentRequest.terminalCountry'].value;
		  var enbState = '${enbState}';
		  var index = (enbState.indexOf(termCon)> -1);
		  if(index != ''){
		  		document.forms['agentRequestForm'].elements['agentRequest.terminalState'].disabled = false;
		  }else{
			  document.forms['agentRequestForm'].elements['agentRequest.terminalState'].disabled = true;
		  }	        
	}
 function enableStateListMail(){ 
		var mailCon=document.forms['agentRequestForm'].elements['agentRequest.mailingCountry'].value;
		  var enbState = '${enbState}';
		  var index = (enbState.indexOf(mailCon)> -1);
		  if(index != ''){
		  		document.forms['agentRequestForm'].elements['agentRequest.mailingState'].disabled = false;
		  }else{
			  document.forms['agentRequestForm'].elements['agentRequest.mailingState'].disabled = true;
		  }	        
	}
 
 function handleHttpResponse223444(){
	   if (http12.readyState == 4)
	   {
	   			  var results = http12.responseText
		               results = results.trim();					               
					if(results=="")	{
					
					}
					else{
					
					} 
	   } 
	}
	var http12 = getHTTPObject();
	function getHTTPObject()
	{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}

	
	function autoCompleterAjaxBillingCity(){
		var countryName = document.forms['agentRequestForm'].elements['agentRequest.billingCountry'].value;
		var stateNameOri = document.forms['agentRequestForm'].elements['agentRequest.billingState'].value;
		var cityNameOri = document.forms['agentRequestForm'].elements['agentRequest.billingCity'].value;
		var countryNameOri = "";
		<c:forEach var="entry" items="${countryCod}">
		if(countryName=="${entry.value}"){
			countryNameOri="${entry.key}";
		}
		</c:forEach>
		if(cityNameOri!=''){
			var data = 'leadCaptureAutocompleteOriAjax.html?ajax=1&stateNameOri='+stateNameOri+'&countryNameOri='+countryNameOri+'&cityNameOri='+cityNameOri+'&decorator=simple&popup=true';
			$("#billingCity").autocomplete({				 
			      source: data		      
			    });
		}
	}
	function autoCompleterAjaxMailingCity(){
		var countryName = document.forms['agentRequestForm'].elements['agentRequest.mailingCountry'].value;
		var stateNameDes = document.forms['agentRequestForm'].elements['agentRequest.mailingState'].value;
		var cityNameDes = document.forms['agentRequestForm'].elements['agentRequest.mailingCity'].value;		
		var countryNameDes = "";
		<c:forEach var="entry" items="${countryCod}">
		if(countryName=="${entry.value}"){
			countryNameDes="${entry.key}";
		}
		</c:forEach>
		if(cityNameDes!=''){
		var data = 'leadCaptureAutocompleteDestAjax.html?ajax=1&stateNameDes='+stateNameDes+'&countryNameDes='+countryNameDes+'&cityNameDes='+cityNameDes+'&decorator=simple&popup=true';
		$( "#mailingCity" ).autocomplete({				 
		      source: data		      
		    });
		}
	}
	
</script>


<script>
      $(function() {
     
      
      var options = {
      type: 'image/png',
      originalSize: 'false',
      smallImage: 'stretch', 
      minZoom: 'fit'
      }
      
       $('.image-editor').cropit(options);
        $("#agentRequestForm").submit(function() {  
          // Move cropped image data to hidden input
          var imageData = $('.image-editor').cropit('export');
          $('.hidden-image-data').val(imageData);

        });
      });
            	
      
    </script>

</head>

<c:if
	test="${partnerType == 'AG' || partnerType == 'AC'  || partnerType == 'PP' || partnerType == 'CR' || partnerType == 'VN' || partnerType == 'OO'}">
	<s:hidden name="fileNameFor" id="fileNameFor" value="PO" />
	<s:hidden name="fileID" id="fileID" value="%{agentRequest.id}" />
	<c:set var="fileID" value="%{agentRequest.id}" />
	<c:set var="PPID" value="%{agentRequest.id}" />
	<s:hidden name="PPID" id="PPID" value="%{agentRequest.id}" />
	<s:hidden name="ppType" id="ppType"
		value="<%=request.getParameter("partnerType")%>" />
	<c:set var="ppType" value="<%=request.getParameter("partnerType")%>" />
	<s:hidden name="noteFor" id="noteFor" value="Partner"></s:hidden>
</c:if>
<div class="modal fade" id="mapModal" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content" style="width:500px;>
			 
				    <div class="modal-body" style="font-weight:600;">
				    <div class="modal-header header-bdr">
        <button type="button" class="close" style="text-align:right !important;" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="line-height:1.32 !important">Location Finder</h4>
      </div>
					<div id="map2" style="text-align:center;width: 500px; height: 400px"></div>
					<s:hidden  id="lat" name="lat"/>
					<s:hidden id="lng" name="lng" />
				
			      <div class="modal-footer">
				        <button type="button" id="btnSave" class="btn btn-danger" data-dismiss="modal" onclick="return saveNewPinLocation();">OK</button>
				   </div>
			    </div>
				    
			</div>
</div>

<s:form id="agentRequestForm" name="agentRequestForm" enctype="multipart/form-data" onsubmit="return checkDate();" action="saveAgentRequestFinal"  method="post" validate="true">
    <s:hidden  name="changeFields" id="changeFields" value=""/>
    <s:hidden name="formStatus" value="" />
	<s:hidden name="agentRequest.id" value="%{agentRequest.id}"/>
	<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
	<s:hidden name="agentRequest.location1" />
	<s:hidden name="agentRequest.location2" />
	<s:hidden name="agentRequest.location3" />
	<s:hidden name="agentRequest.location4" />
	<s:hidden name="agentRequest.companyLogo" />
	<s:hidden name="agentRequest.mailingCountryCode" />
	<s:hidden name="agentRequest.terminalCountryCode" />
    <s:hidden name="agentRequest.billingCountryCode" />
    <s:hidden name="agentRequest.partnerCode" />
	<s:hidden name="agentRequest.partnerType"/>
	<s:hidden name="agentRequest.partnerId"/>
	<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
	<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
	<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
	<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
				
				
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
	
	<s:hidden id="partnerNotes" name="partnerNotes" value="<%=request.getParameter("partnerNotes") %>"/>
		
	<%
    String value=(String)pageContext.getAttribute("pCode") ;
    Cookie cookie = new Cookie("TempnotesId",value);
    cookie.setMaxAge(3600);
    response.addCookie(cookie);
    %>
	
<div id="Layer1" style="width:100%">
<div id="newmnav" style="float: left;">
	 <ul>  
		   <c:if test="${paramValue == 'View'}">
					<li><a href="partnerView.html"><span>Partner List</span></a></li>
				</c:if>
				<c:if test="${paramValue != 'View'}">
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				</c:if>
		   <c:if test="${sessionCorpID=='TSFT'}">
		  <li> <a href="findAgentRequest.html"><span>Agent  Change Request</span></a></li>
		  </c:if>
		   <c:if test="${sessionCorpID!='TSFT'}">
		   <c:if test="${paramValue == 'View'}">
		   <li> <a href="agentList.html?paramView=View"><span>Agent  Change Request</span></a></li>
		   </c:if>
		   <c:if test="${paramValue != 'View'}">
	       <li> <a href="agentList.html"><span>Agent  Change Request</span></a></li>
	       </c:if>
          </c:if>
		<li style="background:#FFF" id="newmnav1"><a class="current"><span>Agent Detail</span></a></li>
	  </ul>
	  </div>
	<div class="spn" style="!line-height:0px;">&nbsp;</div>


	<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
	<div id="content" align="center">
	<div id="liquid-round-top" >
	<div class="top"><span></span></div>
	<div class="center-content">
	<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="width:57%">
		<tbody>
			<tr>
			
			<td> 
		<div class="image-editor">
         <label class="custom-file-upload" onclick="ShowCropitDetails();">
        <input type="file" class="cropit-image-input" hidden="true" >
        <i class="fa fa-cloud-upload"></i> Upload Logo
         </label>
         <div class="cropit-preview" id= "cropit-previewShowHide"></div> 
        <div class="image-size-label" id= "image-size-labelShowHide" >
          Resize image
        </div>
        <input type="range" class="cropit-image-zoom-input" id= "cropit-image-zoom-inputShowHide"  style="width:200px;display:none">
        <input type="hidden" name="imageDataFileLogo" class="hidden-image-data" />
        <c:if test="${!(agentRequest.companyLogo == null || agentRequest.companyLogo == '')}">	
	   <div  style="position: relative; width: 200px; height: 200px;margin-top: 8px;" id="companyLogoShowHide">
       <div style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;" ><img id="companyLogo1"  src="UserImage?location=${agentRequest.companyLogo}" alt=""  /> </div>
       </div>
       </c:if>
      </div>
	
			</td>
				<td>
				
						<table cellspacing="0" cellpadding="3" border="0">
							<tbody>
								<tr>
								<td width="12px"></td>
									<td align="left" class="listwhitetext" colspan="4">Legal Name<font color="red" size="2">*</font></td>									
									<c:if test="${partnerType == 'VN' || partnerType == 'CR'}">
										<td align="left" class="listwhitetext">Start Date</td>
										<td align="left" class="listwhitetext"></td>
									</c:if>	
								</tr>
								<tr>
								<td width="12px"></td>
								<td align="left" class="listwhitetext" colspan="4"><s:textfield id="lastName" key="agentRequest.lastName" required="true" cssClass="input-text" cssStyle="width:295px;" maxlength="80" tabindex="" onchange="validCheck5(this);changeAddressPin();" /></td>
								<%-- --%>
								</tr>								
								<tr>
								<td width="12px"></td>
								<td align="left" class="listwhitetext" colspan="4">Alias Name</td>								
								</tr>
								<tr>
								<td width="12px"></td>
								<td align="left" class="listwhitetext" colspan="4"><s:textfield key="agentRequest.aliasName" required="true" cssClass="input-text" cssStyle="width:295px;" maxlength="80" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								</tr>
								<tr><td width="12px"></td></tr>
								<tr>
								<td width="12px"></td>
								<td align="left" class="listwhitetext"><fmt:message key='partner.status' /></td>
								<td align="left" class="listwhitetext">External Ref.</td>
								<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
								<td align="left" class="listwhitetext" width="">Do&nbsp;Not&nbsp;Merge</td>
								</sec-auth:authComponent>
								</tr>
								<tr>
								<td width="12px"></td>
								<td><s:select cssClass="list-menu" name="agentRequest.status" list="%{agentRequestStatus}" cssStyle="width:105px" onchange="return checkStatus(this),changeStatus();" tabindex="" /></td>
								<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.extReference" required="true" cssClass="input-textUpper" maxlength="15" size="10" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true" /></td>
								<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
										<c:set var="ischeckedMerge" value="false"/>
										<c:if test="${agentRequest.doNotMerge}">
											<c:set var="ischeckedMerge" value="true"/>
										</c:if>
										<td class="listwhitetext" width="" align="center"><s:checkbox key="agentRequest.doNotMerge" onclick="changeStatus();"  value="${ischeckedMerge}" fieldValue="true"/></td>
									</sec-auth:authComponent>
									<c:if test="${not empty agentRequest.partnerId && partnerType == 'AG'}">
								<td width="100" ><a onclick="window.open('editPartnerPublic.html?id=${agentRequest.partnerId}&partnerType=AG')" >Old Agent Data</a></td>
								
								</c:if>
								
								</tr>
							</tbody>
						</table>
						</td>
				
						</tr>
						</tbody></table>
			<%-- 	 <div style="float:left;margin-left:70px;">
				<table>
				<tr>
				<c:if test="${!(agentRequest.companyLogo == null || agentRequest.companyLogo == '')}">
					<td align="left" class="listwhitetext" colspan=""><img id="companyLogo1" class="urClass preview" src="UserImage?location=${agentRequest.companyLogo}" alt=""  /></td>
				</c:if>
				<c:if test="${agentRequest.companyLogo == null || agentRequest.companyLogo == ''}">
					<td align="left" class="listwhitetext" colspan=""><img id="companyLogo1" src="${pageContext.request.contextPath}/images/no-picture-agent.jpg" alt="" width="150" height="100" class="preview" /></td>
				</c:if> 
				</tr>
				 <c:if test="${agentRequest.companyLogo == null || agentRequest.companyLogo == ''}">
				<td align="left" valign="middle" class="listwhitetext" style="width:90px"><div style="padding:8.8px 5px;border:1px solid #50869f;">Company&nbsp;Logo</div></td>
			</c:if>
			 <c:if test="${!(agentRequest.companyLogo == null || agentRequest.companyLogo == '')}">
				<td align="left" valign="middle" class="listwhitetext" ><div style="padding:8.8px 5px;border:1px solid #50869f;">Company&nbsp;Logo</div></td>
			</c:if> 
			<td align="left" valign="bottom" class="listwhitetext">
			<div class="upload-preview"><i class="fa fa-cloud-upload"></i> &nbsp;CHOOSE FILES <s:file name="fileLogo" cssStyle="width:128px;" id="fileLogo" cssClass="hide_file" required="true" />
				 </div>
				 <input type="file" accept="image/*" name="fileLogo" id="fileLogo"  onchange="return Upload(event)" >
				 <s:hidden name="fileLogoUploadFlag" />
<!-- <img id="output"/> -->
			
			<!-- <input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph1();" /> -->
			</td> 				
				</tr>
				</table>
				
				
          
          		</div> --%> 
      
      
      
      
      
      
       
      
       
          		
          		
          		
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
					<tbody>
						<tr>
							<td align="center" colspan="10" class="vertlinedata"></td>
						</tr>
						<tr>
							<td class="listwhitetext" style="width:50px; height:5px;"></td>
						</tr>
					</tbody>
				</table>
				<table cellspacing="3" cellpadding="2" border="0">
					<tbody>
						<tr>
							<td width="10px"></td>
							<td class="listwhitetext"><b>Billing Address</b><font color="red" size="2">*</font></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td width="10px"></td>
							<td colspan="2"><s:textfield cssClass="input-text" name="agentRequest.billingAddress1" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingCountryCode' /><font color="red" size="2">*</font></td>
							<td><s:select cssClass="list-menu" name="agentRequest.billingCountry" list="%{countryDesc}" cssStyle="width:162px" headerKey="" headerValue="" onchange="requiredState();changeStatus();getBillingCountryCode(this);autoPopulate_partner_billingCountry(this);getBillingState(this);getVatCountryCode(this);enableStateListBill();" tabindex="" /></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingFax' /></td>
							<td><s:textfield cssClass="input-text" name="agentRequest.billingFax" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							
							
							
						</tr>
						<tr>
							<td width="10px"></td>
							<td colspan="2"><s:textfield cssClass="input-text" name="agentRequest.billingAddress2" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							<td align="right" id="redHiddenFalse" class="listwhitetext"><fmt:message key='partner.billingState' /></td>
							<td align="right" id="redHidden" class="listwhitetext"><fmt:message key='partner.billingState' /><font  color="red" size="2">*</font></td>
							<td><s:select id="billState" name="agentRequest.billingState" cssClass="list-menu" list="%{bstates}" cssStyle="width:162px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingPhone' /><font color="red" size="2">*</font></td>
							<td><s:textfield cssClass="input-text" name="agentRequest.billingPhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							
						</tr>
						<tr>
							<td width="10px"></td>
							<td colspan="2"><s:textfield cssClass="input-text" name="agentRequest.billingAddress3" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingCity' /><font color="red" size="2">*</font></td>
							<td><s:textfield cssClass="input-text" name="agentRequest.billingCity" id="billingCity" cssStyle="width:160px" maxlength="50" onkeydown="return onlyCharsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);autoCompleterAjaxBillingCity();"/></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingEmail' /></td>
							<td><s:textfield cssClass="input-text" name="agentRequest.billingEmail" size="30" maxlength="65" tabindex="" onchange="checkBillingEmails(this);"/></td>
								
						</tr>
						<tr>
							<td width="10px"></td>
							<td colspan="2"><s:textfield cssClass="input-text" name="agentRequest.billingAddress4" size="40" maxlength="40" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingZip' /></td>
							<td><s:textfield cssClass="input-text" name="agentRequest.billingZip" cssStyle="width:160px" maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							
						</tr>
					</tbody>
				</table>
				
					<table cellspacing="3" cellpadding="2" border="0">
						<tbody>
							<tr>
								<td width="10px"></td>
								<td class="listwhitetext"><b>Terminal Address</b></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="agentRequest.terminalAddress1" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalCountryCode' /></td>
								<td><s:select cssClass="list-menu" name="agentRequest.terminalCountry" list="%{countryDesc}" cssStyle="width:162px" headerKey="" headerValue="" onchange="changeStatus();getTerminalCountryCode(this);autoPopulate_partner_terminalCountry(this);getTerminalState(this);enableStateListTerm();"
									tabindex="" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalFax' /></td>
								<td><s:textfield cssClass="input-text" name="agentRequest.terminalFax" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="agentRequest.terminalAddress2" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalState' /></td>
								<td><s:select id="partnerAddForm_partner_terminalState" name="agentRequest.terminalState" list="%{tstates}" cssClass="list-menu" cssStyle="width:162px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalPhone' /></td>
								<td><s:textfield cssClass="input-text" name="agentRequest.terminalPhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="agentRequest.terminalAddress3" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalCity' /></td>
								<td><s:textfield cssClass="input-text" name="agentRequest.terminalCity" cssStyle="width:160px" maxlength="50" onkeydown="return onlyCharsAllowed(event)" onkeyup="regExMatch(this,event);" tabindex="" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalEmail' /></td>
								<td><s:textfield cssClass="input-text" name="agentRequest.terminalEmail" size="30" maxlength="65" tabindex="" onchange="checkTerminalEmails(this);"/></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="2"><s:textfield cssClass="input-text" name="agentRequest.terminalAddress4" size="40" maxlength="30" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='partner.terminalZip' /></td>
								<td><s:textfield cssClass="input-text" name="agentRequest.terminalZip" cssStyle="width:160px" maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
							</tr>
							<tr>
								<td width="10px"></td>
								<td colspan="1"></td>
								
							</tr>
						</tbody>
					</table>
			
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px;padding:0px;">
					<tbody>
						<tr>
							<td align="center" colspan="10" class="vertlinedata"></td>
						</tr>
						<tr>
							<td class="listwhitetext" style="width:50px; height:5px;"></td>
						</tr>
					</tbody>
				</table>
				
			<table border="0" style="margin:0px;padding:0px;">
					<tr>
						<td valign="top">
						<table cellspacing="3" cellpadding="2" border="0" style="margin:0px;padding:0px;">
							<tbody>
								<tr>
									<td width="10px"></td>
									<td class="listwhitetext"><b>Mailing Address</b></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" id="mailingAddress1" name="agentRequest.mailingAddress1" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);" onchange="changeAddressPin();"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCountryCode' />&nbsp;</td>
									<td><s:select cssClass="list-menu" id="mailingCountry"  name="agentRequest.mailingCountry" list="%{countryDesc}" cssStyle="width:175px" headerKey="" headerValue="" onchange="changeStatus();getMailingCountryCode(this);autoPopulate_partner_mailingCountry(this);getMailingState(this);enableStateListMail();changeAddressPin();"
										tabindex="" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingFax' />&nbsp;</td>
									<td><s:textfield cssClass="input-text" name="agentRequest.mailingFax" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" id="mailingAddress2" name="agentRequest.mailingAddress2" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);" onchange="changeAddressPin();"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingState' /></td>
									<td><s:select id="mailingState" name="agentRequest.mailingState" cssClass="list-menu" list="%{mstates}" cssStyle="width:175px" headerKey="" headerValue="" onchange="changeStatus();changeAddressPin();" tabindex="" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingPhone' /></td>
									<td><s:textfield cssClass="input-text" name="agentRequest.mailingPhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="agentRequest.mailingAddress3" size="40" maxlength="100" tabindex="" onkeyup="regExMatch(this,event);"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingCity' /></td>
									<td><s:textfield cssClass="input-text" name="agentRequest.mailingCity" id="mailingCity" cssStyle="width:173px" maxlength="50" onkeydown="return onlyCharsAllowed(event)" onkeyup="regExMatch(this,event);autoCompleterAjaxMailingCity();" onchange="changeAddressPin();" tabindex="" /></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingEmail' /></td>
									<td><s:textfield cssClass="input-text" name="agentRequest.mailingEmail" size="30" maxlength="65" tabindex=""  onchange="checkMailingEmails(this);"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="2"><s:textfield cssClass="input-text" name="agentRequest.mailingAddress4" size="40" maxlength="40" tabindex="" onkeyup="regExMatch(this,event);"/></td>
									<td align="right" class="listwhitetext"><fmt:message key='partner.mailingZip' /></td>
									<td><s:textfield cssClass="input-text" id="mailingZip" name="agentRequest.mailingZip" cssStyle="width:173px" maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="" onkeyup="regExMatch(this,event);" onchange="changeAddressPin();"/></td>
								</tr>
								<tr>
									<td width="10px"></td>
									<td colspan="3"></td>
									    <c:if test="${usertype!='ACCOUNT'}">
										<td colspan="2" align="left"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from terminal address" onClick="copyTermToMail();changeStatus();" /></td>
										<td colspan="2" align="left"><input type="button" class="cssbutton" style="width:175px; height:25px" value="Copy from billing address" onClick="copyBillToMail();changeStatus();" /></td>
										</c:if>
										<tr>
											<td></td>
											<td colspan="6" style="margin: 0px;padding: 0px;">
											<table style="margin: 0px">
												<tr>
													<td align="right" class="listwhitetext" width="">Latitude</td>
													<td><s:textfield cssClass="input-text" name="agentRequest.latitude" size="18" onkeyup="regExMatch(this,event);" readonly="true"/></td>
													<td align="right" class="listwhitetext" width="">Longitude</td>
													<td><s:textfield cssClass="input-text" name="agentRequest.longitude" size="18" onkeyup="regExMatch(this,event);" readonly="true"/></td>
													<td align="right" class="listwhitetext" width=""></td>
                                                    <td align="left" class="listwhitetext"><input type="button" class="cssbutton" onclick="pinLocation();" name="Location Pin" value="Location Pin" style="width:100px; height:25px" /></td>
										           	</tr>
											</table>
											</td>
										</tr>
										<tr>
											<td></td>
											<td colspan="8" style="margin: 0px;padding: 0px;">
											<table style="margin: 0px">
												<tr>
													<td align="right" class="listwhitetext">Website Url:</td>
													<td align="left"><s:textfield cssClass="input-text" name="agentRequest.url" size="57" onkeyup="regExMatch(this,event);"/></td>
													<td align="left"><c:if test="${usertype!='ACCOUNT'}"><input type="button" class="cssbutton" onclick="return openWebSite();" value="Go to site" style="width:75px; height:25px" /></c:if></td>
													<td align="right" class="listwhitetext" width="8">&nbsp;</td>
													<td align="right" class="listwhitetext" width="103">Year Established</td>
													<td align="left"><s:textfield cssClass="input-text" name="agentRequest.yearEstablished" size="9" maxlength="4" onchange="onlyNumeric(this)" /></td>
												</tr>
											</table>
											</td>
										</tr>
									
								
					
					
					
				</tbody>
			</table>
			</td>
			<td valign="top">
			<div id="map" style="width: 200px; height: 200px"></div>
			
			</td>
			</tr>		
				
					
				</table>
		
				
				
				
		
				
								
			<tr>
				<td height="" align="left" class="listwhitetext" colspan="2">
				
				 </td>
			</tr>
		
				<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('pics'); secImage1();secImage2();secImage3(); secImage4()" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Photographs</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="pics">
					<table border="0" cellpadding="1" cellspacing="1" style="margin:0px;">
						<tbody>
							<tr>
								<td colspan="2">
								<table style="margin:0px;" border="0">
								</table>
								</td>
							</tr>
							<tr>
								<td>
								<table>
									<tr>
										<c:if test="${!(agentRequest.location1 == null || agentRequest.location1 == '')}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage1" class="urClass" src="UserImage?location=${agentRequest.location1}" alt="" style="border:thin solid #219DD1;" /></td>
										</c:if>
										<c:if test="${agentRequest.location1 == null || agentRequest.location1 == ''}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage1" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110" style="border:thin solid #219DD1;" /></td>
										</c:if>
									</tr>
									<tr>
										<c:if test="${agentRequest.location1 == null || agentRequest.location1 == ''}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph1</td>
										</c:if>
										<c:if test="${!(agentRequest.location1 == null || agentRequest.location1 == '')}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph1</td>
										</c:if>
										<td align="left" valign="bottom" class="listwhitetext"><s:file name="file" id="file" cssClass="text file" required="true" />
										<!-- <input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph1();" /> -->
										</td>
									</tr>
								</table>
								</td>
								<td>
								<table>
									<tr>
										<c:if test="${!(agentRequest.location2 == null || agentRequest.location2 == '')}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage2" class="urClass" src="UserImage?location=${agentRequest.location2}" alt="" style="border:thin solid #219DD1;" /></td>
										</c:if>
										<c:if test="${agentRequest.location2 == null || agentRequest.location2 == ''}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage2" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110" style="border:thin solid #219DD1;" /></td>
										</c:if>
									</tr>
									<tr>
										<c:if test="${agentRequest.location2 == null || agentRequest.location2 == ''}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph2</td>
										</c:if>
										<c:if test="${!(agentRequest.location2 == null || agentRequest.location2 == '')}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph2</td>
										</c:if>
										<td align="left" valign="bottom" class="listwhitetext"><s:file name="file1" cssClass="text file" required="true" /><c:if test="${usertype!='ACCOUNT'}">
										<!-- <input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph2();" /> -->
										</c:if></td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td>
								<table>
									<tr>
										<c:if test="${!(agentRequest.location3 == null || agentRequest.location3 == '')}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage3" class="urClass" src="UserImage?location=${agentRequest.location3}" alt="" style="border:thin solid #219DD1;" /></td>
										</c:if>
										<c:if test="${agentRequest.location3 == null || agentRequest.location3 == ''}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage3" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110" style="border:thin solid #219DD1;" /></td>
										</c:if>
									</tr>
									<tr>
										<c:if test="${agentRequest.location3 == null || agentRequest.location3 == ''}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph3</td>
										</c:if>
										<c:if test="${!(agentRequest.location3 == null || agentRequest.location3 == '')}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph3</td>
										</c:if>
										<td align="left" valign="bottom" class="listwhitetext"><s:file name="file2" cssClass="text file" required="true" /><c:if test="${usertype!='ACCOUNT'}">
										<!-- <input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph3();" />
										 --></c:if></td>
									</tr>
								</table>
								</td>
								<td>
								<table>
									<tr>
										<c:if test="${!(agentRequest.location4 == null || agentRequest.location4 == '')}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage4" class="urClass" src="UserImage?location=${agentRequest.location4}" alt="" style="border:thin solid #219DD1;" /></td>
										</c:if>
										<c:if test="${agentRequest.location4 == null || agentRequest.location4 == ''}">
											<td align="center" class="listwhitetext" colspan="2"><img id="userImage4" src="${pageContext.request.contextPath}/images/no-image.png" alt="" width="150" height="110" style="border:thin solid #219DD1;" /></td>
										</c:if>
									</tr>
									<tr>
										<c:if test="${agentRequest.location4 == null || agentRequest.location4 == ''}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Upload&nbsp;Photograph4</td>
										</c:if>
										<c:if test="${!(agentRequest.location4 == null || agentRequest.location4 == '')}">
											<td align="right" valign="bottom" class="listwhitetext" style="width:90px">Change&nbsp;Photograph4</td>
										</c:if>
										<td align="left" valign="bottom" class="listwhitetext"><s:file name="file3" cssClass="text file" required="true" /><c:if test="${usertype!='ACCOUNT'}">
										<!-- <input type="button" class="cssbutton" class="remove" value="Remove" style="width:60px; height:24px" onclick="removePhotograph4();" /> -->
										</c:if></td>
									</tr>
								</table>
								</td>
							</tr>
						</tbody>
					</table>
					</div>
					</td>
				</tr>
				<tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('description')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Description</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="description">
					<table>
						<tr>
							<td align="right" valign="top" class="listwhitetext" width="100">Company Profile</td>
							<td align="left"><s:textarea cssClass="textarea" name="agentRequest.companyProfile" rows="4" cols="118" readonly="false" onkeydown="return checkLength();" onkeyup="regExMatch(this,event);"/></td>
						</tr>
						<tr>
							<td align="right" valign="top" class="listwhitetext" width="100">Facilities</td>
							<td align="left"><s:textarea cssClass="textarea" name="agentRequest.companyFacilities" rows="4" cols="118" readonly="false" onkeydown="return checkLength();" onkeyup="regExMatch(this,event);"/></td>
						</tr>
						<tr>
							<td align="right" valign="top" class="listwhitetext" width="100">Capabilities</td>
							<td align="left"><s:textarea cssClass="textarea" name="agentRequest.companyCapabilities" rows="4" cols="118" readonly="false" onkeydown="return checkLength();" onkeyup="regExMatch(this,event);"/></td>
						</tr>
						<tr>
							<td align="right" valign="top" class="listwhitetext" width="100">Destination Profile</td>
							<td align="left"><s:textarea cssClass="textarea" name="agentRequest.companyDestiantionProfile" rows="4" cols="118" readonly="false" onkeydown="return checkLength();" onkeyup="regExMatch(this,event);"/></td>
						</tr>
					</table>
					</div>
					</td>
				</tr>
					 <tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('info')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Information</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="info">
					<table width="100%" class="detailTabLabel">
						<tr>
							<td valign="top">
							<table border="0" class="detailTabLabel">
								<tr>
									<td colspan="9" width="100%" valign="top">
									<table width="100%" cellspacing="0" cellpadding="2" class="detailTabLabel">
										<tr height="5px"></tr>
										<tr>
											<td align="right"  class="listwhitetext" width="">Facility Size</td>
											<td><s:textfield cssClass="input-text" name="agentRequest.facilitySizeSQMT" size="14" onchange="onlyFloat(this);convertToFeet()" /><b>(in SQMT)</b></td>
											<td align="right"  class="listwhitetext" width="">Facility Size</td>
											<td><s:textfield cssClass="input-text" name="agentRequest.facilitySizeSQFT" size="15" onchange="onlyFloat(this);convertToMeters()" /><b>(in SQFT)</b></td>											
											<td align="right"  class="listwhitetext" width="">Harmony</td>
											<td><s:select cssClass="list-menu" name="agentRequest.utsNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
											
											<td align="left" colspan="2" class="listwhitetext" style="padding-left:21px;" >UGWW Network Group<s:checkbox key="agentRequest.ugwwNetworkGroup " onclick="changeStatus();"  value="${agentRequest.ugwwNetworkGroup}" fieldValue="true" /></td>
										</tr>
										 <tr>
											<td align="right"  class="listwhitetext" width="">FIDI</td>
											<td><s:select cssClass="list-menu" name="agentRequest.fidiNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
											<td align="right"  class="listwhitetext" width="">OMNI Number</td>
											<td><s:textfield cssClass="input-text" name="agentRequest.OMNINumber"  maxlength="10" size="16" onkeyup="regExMatch(this,event);"/></td>
											<configByCorp:fieldVisibility componentId="component.field.Alternative.UTSCompanyType">
											<td align="right"  class="listwhitetext" width="">Harmony Member Type</td>
											<td colspan="5"><s:select cssStyle="width:250px;" cssClass="list-menu" name="agentRequest.UTSmovingCompanyType" list="%{UTSmovingCompanyType}"  /></td>
										    </configByCorp:fieldVisibility>										    
										</tr>
										<tr>
											<td align="right" class="listwhitetext" width="">AMSA</td>
											<td><s:select cssClass="list-menu" name="agentRequest.AMSANumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
											<td align="right" class="listwhitetext" width="">WERC</td>
											<td><s:select cssClass="list-menu" name="agentRequest.WERCNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>
											<td align="right" class="listwhitetext" width="">IAM</td>
											<td><s:select cssClass="list-menu" name="agentRequest.IAMNumber" list="{'Y','N'}" headerKey="" headerValue="" /></td>											
											
										</tr>
										<tr>
										
											<td align="right" class="listwhitetext" width="">PAIMA</td>
											<td><s:select cssClass="list-menu" name="agentRequest.PAIMA" list="{'Y','N'}" headerKey="" headerValue="" /></td>
											<td align="right" class="listwhitetext" width="">LACMA</td>
											<td><s:select cssClass="list-menu" name="agentRequest.LACMA" list="{'Y','N'}" headerKey="" headerValue="" /></td>
											<td align="right" class="listwhitetext" width="">Eurovan Network</td>
											<td><s:select cssClass="list-menu" name="agentRequest.eurovanNetwork" list="{'Y','N'}" headerKey="" headerValue="" /></td>											
											
										</tr>
										
										<tr>
											<td align="right" valign="top"class="listwhitetext" width="">VanLine Affiliation(s)</td>
											<td><s:select cssClass="list-menu" name="agentRequest.vanLineAffiliation" list="%{vanlineAffiliations}" value="%{multiplVanlineAffiliations}" multiple="true" cssStyle="width:120px; height:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
											<td align="right" valign="top" class="listwhitetext" width="">Service Lines</td>
											<td><s:select cssClass="list-menu" name="agentRequest.serviceLines" list="%{serviceLines}" multiple="true" value="%{multiplServiceLines}" cssStyle="width:120px; height:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
											<td align="right" valign="top" class="listwhitetext" width="">Quality Certifications</td>
											<td colspan="3"><s:select cssClass="list-menu" name="agentRequest.qualityCertifications" list="%{qualityCertifications}" value="%{multiplequalityCertifications}" multiple="true" cssStyle="width:120px; height:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
										</tr>
										<tr>
											<td colspan="8" height="2px"></td>
										</tr>
										<tr>
										<configByCorp:fieldVisibility componentId="component.field.Alternative.BillingCurrency">
										<td align="right"  class="listwhitetext" width="">Default&nbsp;Billing&nbsp;Currency</td>
										<td colspan="2"><s:select cssClass="list-menu" name="agentRequest.billingCurrency" list="%{billingCurrency}" headerKey="" headerValue="" /></td>
										</configByCorp:fieldVisibility>
										<td colspan="3" align="left" class="listwhitetext" width=""><font style="font-style: italic; font: bold; ">* Use Control + mouse to select multiple <b>Selections</b></font></td>
										</tr>								
										<tr>
											<td></td>
											
										</tr>
										<tr>
										
										<td align="right" class="listwhitetext" width="72">Bank Code
							  			 <configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">
							   			<font color="red" size="2">*</font>
							  			 </configByCorp:fieldVisibility>
							  			 </td>
										<td class="listwhitetext"><s:textfield cssClass="input-text" name="agentRequest.bankCode" size="20" maxlength="45" tabindex="10" onkeyup="regExMatch(this,event);"/></td>	
																			
 										<td align="right" class="listwhitetext">VAT #
							  			 </td>
										<td class="listwhitetext" ><s:textfield cssClass="input-text" name="agentRequest.vatNumber" size="25" maxlength="23" tabindex="12" onkeyup="regExMatch(this,event);"/></td>
	 									
	 									<td align="right" class="listwhitetext">Bank Account #
						    				<configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">
						    					<font color="red" size="2">*</font>
						    				</configByCorp:fieldVisibility>
						    			</td>
						    			 
						    			<td class="listwhitetext" colspan="5"><s:textfield cssClass="input-text" name="agentRequest.bankAccountNumber" size="35" maxlength="40" tabindex="12" onkeyup="regExMatch(this,event);"/>
						    			</td>
										</tr> 
					</table>
					</td>
					</tr>
					</table>
					</td>
					</tr>
					</table>
					</div>
					</td>
				</tr> 
			

				 <tr>
					<td height="10" align="left" class="listwhitetext" style="margin: 0px;">
					<div onClick="javascript:animatedcollapse.toggle('vninfo')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Corporate Hierarchy</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="vninfo">
					<table class="detailTabLabel" border="0" style="margin: 0px;">
					<tr>
							<td align="left" height="5px" ></td>
						</tr>					
						<tr>
							<td align="right" class="listwhitetext"><fmt:message key='partner.agentParent' /></td>
							<td width="100px"><s:textfield cssClass="input-textUpper" name="agentRequest.agentParent" id="agentParentAccount" size="7" maxlength="8" readonly="true"   tabindex="" onkeyup="regExMatch(this,event);"/> <img class="openpopup" id="openPopUp1" width="17" height="20" align="top" onclick="javascript:openWindow('venderPartners.html?&partnerType=AG&parentDataUpdate=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=agentRequest.agentParentName&fld_code=agentRequest.agentParent');document.getElementById('agentParentAccount').focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td align="left"><s:textfield  cssStyle="width:250px" cssClass="input-textUpper"  key="agentRequest.agentParentName"  required="true" readonly="true"  onkeyup="regExMatch(this,event);"/> </td>
						<td align="right" class="listwhitetext"  width="80px">Agent Group</td>
						<td width="100px"><s:textfield cssClass="input-text"  name="agentRequest.agentGroup" size="13"  maxlength="30"  tabindex="" />
						<td align="right" class="listwhitetext"  width="80px">Merge Into</td>
						<td width="100px"><s:textfield cssClass="input-textUpper"  name="agentRequest.mergeInto" size="10" readonly="true"  maxlength="8"  tabindex="" />
						</tr>
						
					</table>
					</div>
					</td>
				</tr>
				
		<tr>
					<td height="10" align="left" class="listwhitetext" colspan="10">
					<div onClick="javascript:animatedcollapse.toggle('partnerportal')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Partner Portal Detail</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="partnerportal">
					<table  cellpadding="1" cellspacing="1" class="detailTabLabel">
					<tr>
							<td align="left" height="5px" ></td>
						</tr>
						<tr>
							<c:set var="isActiveFlag1" value="false" />
							<c:if test="${agentRequest.partnerPortalActive}">
								<c:set var="isActiveFlag1" value="true" />
							</c:if>
							<c:set var="isActiveFlag" value="false" />
							<c:if test="${agentRequest.viewChild}">
								<c:set var="isActiveFlag" value="true" />
							</c:if>
							<td width="129" align="right" valign="middle" class="listwhitetext">Partner Portal:</td>
							<td width="1px"><s:checkbox key="agentRequest.partnerPortalActive" value="${isActiveFlag1}" fieldValue="true" onclick="changeStatus()"  cssStyle="margin:0px" tabindex="21"  /></td>
							<td width="180" align="left"><fmt:message key='customerFile.portalIdActive' /></td>
							
							<c:set var="isActiveDonotSendEmailToAgent" value="false" />
							<c:if test="${agentRequest.doNotSendEmailtoAgentUser}">
								<c:set var="isActiveDonotSendEmailToAgent" value="true" />
							</c:if>
							
							<td width="129" align="right" valign="middle" class="listwhitetext">Do Not Send Email:</td>
							<td width="1px"><s:checkbox key="agentRequest.doNotSendEmailtoAgentUser" value="${isActiveDonotSendEmailToAgent}" fieldValue="true" onclick="changeStatus()"  tabindex="21"  /></td>
							<td width="18" align="left">Agent</td>
							
						</tr>
						<tr>
						   
							<td align="right" class="listwhitetext">Associated Agent&nbsp;</td>
							
							<td colspan="2"><s:textfield cssClass="input-text" name="agentRequest.associatedAgents" size="30" maxlength="65" tabindex="22" onkeyup="regExMatch(this,event);"/></td>
							
							<td align="right" valign="middle" class="listwhitetext" width="200px">Allow access to child Agent Records:</td>
							
							<td width="16"><s:checkbox key="agentRequest.viewChild" value="${isActiveFlag}" fieldValue="true" tabindex="23"  /></td>
							<td><fmt:message key='customerFile.portalIdActive' /></td>
							<c:if test="${not empty agentRequest.id}">
							<c:if test="${usertype!='ACCOUNT'}">
								
								
									<td align="left"><input type="button"  id="childAgent" tabindex="24" value="Child Agent" class="cssbuttonB"  onclick="window.open('getChildAgents.html?id=${agentRequest.id}&partnerType=${partnerType}&decorator=popup&popup=true','','width=750,height=400,scrollbars=yes');"/></td>
								
								</c:if>
							</c:if>
							
						</tr>
						
					</table>
					</div>
					</td>
				</tr>
				<tr>
					<table style="margin-bottom: 0px;width: 100%;">
							<tbody>
								<tr>
									<td align="center" colspan="15" class="vertlinedata"></td>
								</tr>
								<tr>
									<td height="10px"></td>
								</tr>
								<tr>
									<c:set var="ischeckedPP" value="false" />
									<c:if test="${agentRequest.isPrivateParty}">
										<c:set var="ischeckedPP" value="true" />
									</c:if>
									<td align="center" class="listwhitetext"><fmt:message key='partner.private' /><s:checkbox key="agentRequest.isPrivateParty" value="${ischeckedPP}" fieldValue="true" onclick="changeStatus();" /></td>
									
									<c:if test="${empty agentRequest.id}">
										<c:if test="${partnerType == 'AC'}">
											<c:set var="ischeckedAC" value="true" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.accounts' /><s:checkbox key="agentRequest.isAccount" value="${ischeckedAC}" fieldValue="true" onclick="changeStatus();"  /></td>
										</c:if>
										<c:if test="${partnerType != 'AC'}">
											<c:set var="ischeckedAC" value="false" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.accounts' /><s:checkbox key="agentRequest.isAccount" value="${ischeckedAC}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty agentRequest.id}">
										<c:set var="ischeckedAC" value="false" />
										<c:if test="${agentRequest.isAccount}">
											<c:set var="ischeckedAC" value="true" />
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.accounts' /><s:checkbox key="agentRequest.isAccount" value="${ischeckedAC}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
									
									<c:if test="${empty agentRequest.id}">
										<c:if test="${partnerType == 'AG'}">
											<c:set var="ischeckedAG" value="true" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.agents' /><s:checkbox key="agentRequest.isAgent" value="${ischeckedAG}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
										<c:if test="${partnerType != 'AG'}">
											<c:set var="ischeckedAG" value="false" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.agents' /><s:checkbox key="agentRequest.isAgent" value="${ischeckedAG}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
								
									<s:hidden name="agentRequest.isAgent"  />
									</c:if>
									<c:if test="${not empty agentRequest.id}">
										<c:set var="ischeckedAG" value="false" />
										<c:if test="${agentRequest.isAgent}">
											<c:set var="ischeckedAG" value="true" />
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.agents' /><s:checkbox key="agentRequest.isAgent" value="${ischeckedAG}" fieldValue="true" onclick="changeStatus();" /></td>
								
									<s:hidden name="agentRequest.isAgent"  />
									</c:if>
									
									<c:if test="${empty agentRequest.id}">
										<c:if test="${partnerType == 'VN'}">
											<c:set var="ischeckedVN" value="true" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.vendors' /><s:checkbox key="agentRequest.isVendor" value="${ischeckedVN}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
										<c:if test="${partnerType != 'VN'}">
										<c:set var="ischeckedVN" value="false" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.vendors' /><s:checkbox key="agentRequest.isVendor" value="${ischeckedVN}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty agentRequest.id}">
										<c:set var="ischeckedVN" value="false" />
										<c:if test="${agentRequest.isVendor}">
											<c:set var="ischeckedVN" value="true" />
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.vendors' /><s:checkbox key="agentRequest.isVendor" value="${ischeckedVN}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
									
									<c:if test="${empty agentRequest.id}">
										<c:if test="${partnerType == 'CR'}">
											<c:set var="ischeckedCR" value="true" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.carriers' /><s:checkbox key="agentRequest.isCarrier" value="${ischeckedCR}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
										<c:if test="${partnerType != 'CR'}">
										<c:set var="ischeckedCR" value="false" />
											<td align="left" class="listwhitetext"><fmt:message key='partner.carriers' /><s:checkbox key="agentRequest.isCarrier" value="${ischeckedCR}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty agentRequest.id}">
										<c:set var="ischeckedCR" value="false" />
										<c:if test="${agentRequest.isCarrier}">
											<c:set var="ischeckedCR" value="true" />
										</c:if>
										<td align="left" class="listwhitetext"><fmt:message key='partner.carriers' /><s:checkbox key="agentRequest.isCarrier" value="${ischeckedCR}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
									
									<c:if test="${empty agentRequest.id}">
										<c:if test="${partnerType == 'OO'}">
											<c:set var="ischeckedOO" value="true" />
											<td align="center" class="listwhitetext"><fmt:message key='partner.owner' /><s:checkbox key="agentRequest.isOwnerOp" value="${ischeckedOO}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
										<c:if test="${partnerType != 'OO'}">
											<c:set var="ischeckedOO" value="false" />
											<td align="center" class="listwhitetext"><fmt:message key='partner.owner' /><s:checkbox key="agentRequest.isOwnerOp" value="${ischeckedOO}" fieldValue="true" onclick="changeStatus();" /></td>
										</c:if>
									</c:if>
									<c:if test="${not empty agentRequest.id}">
										<c:set var="ischeckedOO" value="false" />
										<c:if test="${agentRequest.isOwnerOp}">
											<c:set var="ischeckedOO" value="true" />
										</c:if>
										<td align="center" class="listwhitetext"><fmt:message key='partner.owner' /><s:checkbox key="agentRequest.isOwnerOp" value="${ischeckedOO}" fieldValue="true" onclick="changeStatus();" /></td>
									</c:if>
								</tr>
							</tbody>
						</table>
				</tr>
					
		</tbody>
	</table>
	
	</div>
	<div class="bottom-header" style="margin-top:48px;"><span></span></div>
	</div>
	</div>
	</div>
	<table border="0" style="width:800px;">
		<tbody>
			<tr>
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn' /></b></td>
				<fmt:formatDate var="containerCreatedOnFormattedValue" value="${agentRequest.createdOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="agentRequest.createdOn" value="${containerCreatedOnFormattedValue}" />
				<td width="120px"><fmt:formatDate value="${agentRequest.createdOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></b></td>
				<c:if test="${not empty agentRequest.id}">
					<s:hidden name="agentRequest.createdBy" />
					<td><s:label name="createdBy" value="%{agentRequest.createdBy}" /></td>
				</c:if>
				<c:if test="${empty agentRequest.id}">
					<s:hidden name="agentRequest.createdBy" value="${pageContext.request.remoteUser}" />
					<td><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
				</c:if>
				<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn' /></b></td>
				<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${agentRequest.updatedOn}" pattern="${displayDateTimeEditFormat}" />
				<s:hidden name="agentRequest.updatedOn" value="${containerUpdatedOnFormattedValue}" />
				<td width="120px"><fmt:formatDate value="${agentRequest.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
				<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></b></td>
				<c:if test="${not empty agentRequest.id}">
					<s:hidden name="agenRequest.updatedBy" />
					<td style="width:85px"><s:label name="updatedBy" value="%{agentRequest.updatedBy}" /></td>
				</c:if>
				<c:if test="${empty agentRequest.id}">
					<s:hidden name="agenRequest.updatedBy" value="${pageContext.request.remoteUser}" />
					<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}" /></td>
				</c:if>
			</tr>
		</tbody>
	</table>
			<s:hidden name="agentRequest.isPrivateParty" />
			<s:hidden name="agentRequest.isAccount" />
			<s:hidden name="agentRequest.isAgent" />
			<s:hidden name="agentRequest.isVendor" />
			<s:hidden name="agentRequest.isCarrier" />
			<s:hidden name="agentRequest.isOwnerOp" />
			
		<c:set var="button1"><c:if test="${usertype!='ACCOUNT'}">
           <c:if test="${agentRequest.status != 'Approved'}">
            <c:if test="${sessionCorpID !='TSFT'}">
            <s:submit cssClass="cssbutton" key="Send Request" cssStyle="width:100px;height:25px" onclick="return validateForm();"  />
		     </c:if>
		      <c:if test="${sessionCorpID =='TSFT'}">
			   <s:submit cssClass="cssbutton" key="Save" cssStyle="width:60px;height:25px" onclick="return validateForm();"  />
		  </c:if>
			<s:reset cssClass="cssbutton" key="Reset" onclick="reset_partner_billingCountry();reset_partner_mailingCountry();reset_partner_terminalCountry();"  cssStyle="width:60px; height:25px " />
			</c:if>
			</c:if>
	</c:set>			
	<c:out value="${button1}" escapeXml="false" />
	<c:set var="isTrue" value="false" scope="session" />
	
		
		
	

</s:form>

<script type="text/javascript">

try{
var param = '<%=session.getAttribute("paramView")%>';
if(param == 'View'){
	document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
}
}
catch(e){}
try{
<c:if test="${not empty agentRequest.id}"> 
<c:if test="${agentRequest.corpID =='TSFT'}"> 
document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = false;
</c:if>
<c:if test="${agentRequest.corpID !='TSFT'}">
document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
</c:if>
</c:if>
<c:if test="${empty agentRequest.id}">
document.forms['agentRequestForm'].elements['agentRequest.status'].disabled = true;
</c:if>
}
catch(e){}
</script>
<script type="text/javascript">
try{
	var  permissionTest1  = "";
	<sec-auth:authComponent componentId="module.agentRequest.section.partnerPortalDetail.edit">  		
	 	permissionTest1  = <%=(Integer)request.getAttribute("module.agentRequest.section.partnerPortalDetail.edit" + "Permission")%>;
	</sec-auth:authComponent>
    if(permissionTest1 > 2){
    	<sec-auth:authComponent componentId="component.standard.accountPotralActivation">
		document.forms['agentRequestForm'].elements['agentRequest.partnerPortalActive'].disabled = false;
		</sec-auth:authComponent>
		document.forms['agentRequestForm'].elements['agentRequest.associatedAgents'].disabled = false;
		document.forms['agentRequestForm'].elements['agentRequest.viewChild'].disabled = false;
		document.getElementById("childAgent").disabled = false;
	}else{
		<sec-auth:authComponent componentId="component.standard.accountPotralActivation">
		document.forms['agentRequestForm'].elements['agentRequest.partnerPortalActive'].disabled = true;
		</sec-auth:authComponent>
		document.forms['agentRequestForm'].elements['agentRequest.associatedAgents'].readOnly = true;
		document.forms['agentRequestForm'].elements['agentRequest.associatedAgents'].className = 'input-textUpper';
		document.forms['agentRequestForm'].elements['agentRequest.viewChild'].disabled = true;
		document.getElementById("childAgent").disabled = true;
	}
  }catch(e){}
</script>

<script type="text/javascript">
	try{
	requiredState();	
	autoPopulate_partner_billingCountry(document.forms['agentRequestForm'].elements['agentRequest.billingCountry']);
	}catch(e){}
	try{
		 var enbState = '${enbState}';
		 var billCon=document.forms['agentRequestForm'].elements['agentRequest.billingCountry'].value;
		 if(billCon=="") {
				document.getElementById('billState').disabled = true;
				document.getElementById('billState').value ="";
		 }else{	
		  		if(enbState.indexOf(billCon)> -1){
		  			getBillingState();
					document.getElementById('billState').disabled = false; 
					document.getElementById('billState').value='${agentRequest.billingState}';
				}else{
					document.getElementById('billState').disabled = true;
					document.getElementById('billState').value ="";
				} 
			}
		 }catch(e){} 
 </script>
<script type="text/javascript">  
 	try{
 		requiredState();
 	autoPopulate_partner_terminalCountry(document.forms['agentRequestForm'].elements['agentRequest.terminalCountry']);
 	}catch(e){}
	try{
		 var enbState = '${enbState}';
		 var termCon=document.forms['agentRequestForm'].elements['agentRequest.terminalCountry'].value;
		 if(termCon=="")	 {
			
				document.getElementById('partnerAddForm_partner_terminalState').disabled = true;
				document.getElementById('partnerAddForm_partner_terminalState').value ="";
		 }else{
		  		if(enbState.indexOf(termCon)> -1){
			  		//#7148 -To add the status drop down for private party partner
		  			getTerminalState();
					document.getElementById('partnerAddForm_partner_terminalState').disabled = false; 
					document.getElementById('partnerAddForm_partner_terminalState').value='${agentRequest.terminalState}';
				}else{
					document.getElementById('partnerAddForm_partner_terminalState').disabled = true;
					document.getElementById('partnerAddForm_partner_terminalState').value ="";
				} 
			}
		 }catch(e){}
</script>
<script type="text/javascript">  
	try{
		requiredState();
	autoPopulate_partner_mailingCountry(document.forms['agentRequestForm'].elements['agentRequest.mailingCountry']);        
	}catch(e){}
	try{
		 var enbState = '${enbState}';
		 var mailCon=document.forms['agentRequestForm'].elements['agentRequest.mailingCountry'].value;
		 if(mailCon=="")	 {
				document.getElementById('partnerAddForm_partner_mailingState').disabled = true;
				document.getElementById('partnerAddForm_partner_mailingState').value ="";
		 }else{
		  		if(enbState.indexOf(mailCon)> -1){
		  		//#7148 -To add the status drop down for private party partner
		  			getMailingState();
					document.getElementById('partnerAddForm_partner_mailingState').disabled = false; 
					document.getElementById('partnerAddForm_partner_mailingState').value='${agentRequest.mailingState}';
				}else{
					document.getElementById('partnerAddForm_partner_mailingState').disabled = true;
					document.getElementById('partnerAddForm_partner_mailingState').value ="";
				} 
			}
		 }catch(e){}	
</script>


<script type="text/javascript">
try{
<c:if test="${donoFlag=='1'}">
document.forms['agentRequestForm'].elements['agentRequest.doNotMerge'].disabled = true;
</c:if>
}catch(e){}
</script>
<script type="text/javascript">
try{
secImage1();
}
catch(e){}
try{
secImage2();
}
catch(e){}
try{
secImage3();
}
catch(e){}

try{
secImage4();
}
catch(e){}

</script>
<script type="text/javascript">
try{
	 <sec-auth:authComponent componentId="module.script.form.corpAccountScript">
	 // disabledAll();
	 	</sec-auth:authComponent>
}
catch(e){}
</script>

 <script type="text/javascript">
   setOnSelectBasedMethods(['checkDate()']);
   setCalendarFunctionality();
</script>
 <script type="text/javascript">
try{
<c:if test="${not empty agentRequest.id && agentRequest.status=='Approved' && partnerType=='AG'}">
var checkRoleAG = 0;
//Modified By subrat BUG #6471 
<sec-auth:authComponent componentId="module.tab.partner.AgentDEV">
	checkRoleAG  = 14;
</sec-auth:authComponent>
if(checkRoleAG < 14){
	//disabledAll();
}
</c:if>

<c:if test="${not empty agentRequest.id && agentRequest.corpID=='TSFT' && partnerType=='AC'}">
	var checkRole = 0;
	<sec-auth:authComponent componentId="module.field.partner.statusAccount">  
		checkRole  = 14;
	</sec-auth:authComponent>

	if(checkRole < 14){
		//disabledAll();
	}
</c:if>
}
catch(e){}

function checkCmmDmmDisable(sessionCorpID){
	if(sessionCorpID != 'TSFT'){
		if(sessionCorpID != 'UTSI'){
			if('${agentRequest.partnerType}' == 'DMM' || '${agentRequest.partnerType}' == 'CMM'){
					var elementsLen=document.forms['agentRequestForm'].elements.length;
					for(i=0;i<=elementsLen-1;i++){
						if(document.forms['agentRequestForm'].elements[i].type=='text'){
							document.forms['agentRequestForm'].elements[i].readOnly =true;
							document.forms['agentRequestForm'].elements[i].className = 'input-textUpper';
						}else if(document.forms['agentRequestForm'].elements[i].type=='textarea'){
							document.forms['agentRequestForm'].elements[i].readOnly =true;
							document.forms['agentRequestForm'].elements[i].className = 'textareaUpper';
						}else if(document.forms['agentRequestForm'].elements[i].type=='submit' || document.forms['agentRequestForm'].elements[i].type=='reset' || document.forms['agentRequestForm'].elements[i].type=='button'){
							document.forms['agentRequestForm'].elements[i].style.display='none';
						}else{
							document.forms['agentRequestForm'].elements[i].disabled=true;
						} 
				}
			}
		}
	}
}

<c:if test="${not empty agentRequest.id && partnerType=='AC'}">
		checkCmmDmmDisable('${sessionCorpID}');
</c:if>
getVatCountryCode(document.forms['agentRequestForm'].elements['agentRequest.billingCountry']);
function getVatCountryCode(country){
 	var country = country.value;
 	country=country.trim();
 	var flag = "0"; 
	<c:forEach var="entry" items="${countryWithBranch}">
	var countryName="${entry.key}";	
	countryName=countryName.trim();
	var vatFlag="${entry.value}";
	vatFlag=vatFlag.trim();
	var vatArr=vatFlag.split("#");
	if((country==countryName) && (flag !="1")){
		document.forms['agentRequestForm'].elements['agentCountryCode'].value=vatArr[1] ;
		document.forms['agentRequestForm'].elements['vatAllowCountry'].value=vatArr[0] ;
		flag ="1"
	}
	</c:forEach>
}
function replaceLocation(){
	 var getURL = window.location.href;
	 var aPosition = getURL.indexOf("saveagentRequest");
	 if(aPosition != -1 ){
	  var replString = getURL.substr(aPosition,getURL.length-1);
	  getURL=getURL.replace(replString,"agentRequestFinal.html");
	  var urlTemp = getURL+"?id=${agentRequest.id}&partnerType=${partnerType}";
	     location.replace(urlTemp); 
	 }else{
	  window.location.reload();
	 }
	}
	
function saveValidationForBankInfoById(id){
	 var flag =true;
	if(document.getElementById("ban"+id).value=='' || document.getElementById("cur"+id).value==''){
		flag=false;
	}
	return flag;
}
	
	function saveValidationForBankInfo(){
			var bankNoList='';
			var currencyList='';
			var id='';
			var valCheck='';
			var flag=true;
			
		    if(document.forms['partnerBankInfoDetail'].bankNoList!=undefined && document.forms['partnerBankInfoDetail'].bankNoList.size!=0){
		        if(document.forms['partnerBankInfoDetail'].bankNoList.length!=undefined){
		 	      for (i=0; i<document.forms['partnerBankInfoDetail'].bankNoList.length; i++){	        	           
		         	   id=document.forms['partnerBankInfoDetail'].bankNoList[i].id;
		         	  valCheck=document.getElementById(id).value;
		         	  if(valCheck==null || valCheck ==undefined || valCheck==''){
		         	   if(bankNoList==''){
		         		  bankNoList=id+": "+valCheck;
		               }else{
		            	   bankNoList= bankNoList+"~"+id+": "+valCheck;
		               }
		         	  }
		 	       }	
		 	     }else{	   
		 	         id=document.forms['partnerBankInfoDetail'].bankNoList.id;
		 	         valCheck=document.forms['partnerBankInfoDetail'].bankNoList.value;
		 	        if(valCheck==null || valCheck ==undefined || valCheck==''){
		 	       	 bankNoList=id+": "+valCheck;
		 	        }
		 	     }	        
		 	 }
			 currencyList='';
			 id='';
			 valCheck='';
			
		    if(document.forms['partnerBankInfoDetail'].bankNoList!=undefined && document.forms['partnerBankInfoDetail'].bankNoList.size!=0){
		        if(document.forms['partnerBankInfoDetail'].bankNoList.length!=undefined){
		 	      for (i=0; i<document.forms['partnerBankInfoDetail'].bankNoList.length; i++){	        	           
		         	   id=document.forms['partnerBankInfoDetail'].bankNoList[i].id;
		         	  id=id.replace('ban','cur');
		         	  valCheck=document.getElementById(id).value;
		         	  if(valCheck==null || valCheck ==undefined || valCheck==''){
		         	   if(currencyList==''){
		         		  currencyList=id+": "+valCheck;
		               }else{
		            	   currencyList= currencyList+"~"+id+": "+valCheck;
		               }
		         	  }
		 	       }	
		 	     }else{	   
		 	         id=document.forms['partnerBankInfoDetail'].currencyList.id;
		 	         valCheck=document.forms['partnerBankInfoDetail'].currencyList.value;
		 	        if(valCheck==null || valCheck ==undefined || valCheck==''){
		 	       	 currencyList=id+":"+valCheck;
		 	        }
		 	     }	        
		 	 }
		   
		    if(bankNoList!=''){
		    	var bankArray=bankNoList.split("~");
				for ( var i=0; i<bankArray.length && flag; i++){
					id=bankArray[i].split(":")[0].trim();
					id=id.replace("ban","");
					id=id.replace("cur","");
					try{
					if(document.getElementById("ban"+id).value=='' || document.getElementById("cur"+id).value==''){
						flag=false;
					}
					}catch(e){
						flag=false;
					}
				}
		    }
		    if(currencyList!=''){
			    var currArray=currencyList.split("~");
				for ( var i=0; i<currArray.length && flag; i++){
					id=currArray[i].split(":")[0].trim();
					id=id.replace("ban","");
					id=id.replace("cur","");
					try{

					if(document.getElementById("ban"+id).value=='' || document.getElementById("cur"+id).value==''){
						flag=false;
					}
					}catch(e){
						flag=false;
					}
				}
		    }
		   
			return flag;
		
	}
	
function add(tableID){
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount); 
	var newlineid=document.getElementById('newlineid').value; 
	if(newlineid.trim()==''){
	    newlineid=rowCount;
	  }else{
	  newlineid=newlineid+"~"+rowCount;
      
	  }
	
	document.getElementById('newlineid').value=newlineid;
	 
		var cell1 = row.insertCell(0);
		var element1 = document.createElement("select");
	 element1.setAttribute("class", "list-menu" );
	 element1.id="cur"+rowCount;
	 element1.name="currencyList";
	 element1.style.width="60px"
	var catogeryList='${billingCurrency}';
	catogeryList=catogeryList.replace('{','').replace('}','');
	var catogeryarray=catogeryList.split(",");
	var optioneleBlankCategory= document.createElement("option");
	optioneleBlankCategory.value="";
	optioneleBlankCategory.text=""; 
	element1.options.add(optioneleBlankCategory);  
	for(var i=0;i<catogeryarray.length;i++){
		var value=catogeryarray[i].split("=");
	var optionele= document.createElement("option");
	optionele.value=value[0];
	optionele.text=value[1]; 
	element1.options.add(optionele);             
	}
	cell1.appendChild(element1);

	var cell2 = row.insertCell(1);
	var element2 = document.createElement("input");
	element2.type = "text";
	element2.style.width="100px";
	element2.setAttribute("class", "input-text" );
	element2.id='ban'+rowCount;
	element2.name="bankNoList";
	cell2.appendChild(element2);
	
	var cell3 = row.insertCell(2);
	var element3 = document.createElement("input");
	element3.type ="checkbox";
	element3.style.width="30px";
	element3.id='chk'+rowCount;
	element3.name="checkList";
	element3.value=true;
	element3.checked=true;
	cell3.appendChild(element3);

}


function getXMLHttpRequestObject()
{
  var xmlhttp;
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (e) {
      xmlhttp = false;
    }
  }
  return xmlhttp;
}

</script>
<script type="text/javascript">
try{
	
}catch(e){}

</script>

<script type="text/javascript">
function validCheck5(targetElement){
	
	/* var chArr = new Array("#");
 	for ( var i = 0; i < chArr.length; i++ ) {
 		var origString = document.forms['agentRequestForm'].elements['agentRequest.lastName'].value;
 		origString = origString.split(chArr[i]).join(''); 
 		document.forms['agentRequestForm'].elements['agentRequest.lastName'].value = origString;
 		} */
	}
</script>

<script type="text/javascript">
var position1='';

		
function checkStatus(target){
	var publicStatus=target.value;
	var checkStatus='${containsData}';
	//alert(publicStatus+"~~~~~~~~~"+checkStatus[0]);
	//alert(publicStatus+"--"+checkStatus[1]);
	//alert(publicStatus+"~~~~~~~~~"+checkStatus[2]);
	var CheckPartnerType=checkStatus[2];
	var checkPartnerName="";
	if(CheckPartnerType=='V'){
		checkPartnerName="Vendor Code";
	}
	if(CheckPartnerType=='B'){
		checkPartnerName="Bill To Code";
		
	}
	    	if(publicStatus=='Inactive' && checkStatus[0]=='Y'){
	    		var retVal = confirm("Partnercode has been added in Default Account Line. If you want to continue ? "+checkPartnerName +" would be removed from Default Account Line. ");
	               if( retVal == true ){
	            	   document.forms['agentRequestForm'].elements['checkCodefromDefaultAccountLine'].value="Y"+"~"+CheckPartnerType;
	                  return true;
	               }
	               else{
	            	   document.forms['agentRequestForm'].elements['checkCodefromDefaultAccountLine'].value="N"+"~"+CheckPartnerType;
	            	   document.forms['agentRequestForm'].elements['agentRequest.status'].value='${agentRequest.status}';
	                  return false;
	               }
	    		
	    		 return false;
	    	}else{
	    		document.forms['agentRequestForm'].elements['checkCodefromDefaultAccountLine'].value="N"+"~"+CheckPartnerType;
	   				return true;
	    	}
	  return true;
	  }

    </script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
<script language="javascript">

function showOrHide(value) { 
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	} } 

</script>

<script type="text/javascript">
try{
	<c:if test="${flagHit=='2'}">
	 <c:redirect url="/agentRequestFinal.html?id=${agentRequest.id}&partnerType=AG"/>
	</c:if>
}catch(e){}
function checkBillingEmails(temp){
		 var reg =/^(([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([,](([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/;

		 if (reg.test(temp.value)){
			
		 return true; 
		}
		 else{
			 alert("Invalid Email")
			 document.forms['agentRequestForm'].elements['agentRequest.billingEmail'].value="";
		        
		 return false;
		 } 
	}
	function checkTerminalEmails(temp){
		 var reg =/^(([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([,](([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/;

		 if (reg.test(temp.value)){
			
		 return true; 
		}
		 else{
			 alert("Invalid Email")
			 document.forms['agentRequestForm'].elements['agentRequest.terminalEmail'].value="";
		        
		 return false;
		 } 
	}
	function checkMailingEmails(temp){
		 var reg =/^(([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([,](([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/;

		 if (reg.test(temp.value)){
			
		 return true; 
		}
		 else{
			 alert("Invalid Email")
			 document.forms['agentRequestForm'].elements['agentRequest.mailingEmail'].value="";
		        
		 return false;
		 } 
	}
	
	function changeAddressPin()
	{ 

		var lastName=document.getElementById('lastName').value;

	    var lookupAddress = lastName+" "+getMailingAdress(); 
	    var geocoder = new google.maps.Geocoder();
	    geocoder.geocode({'address': lookupAddress}, function(results, status) {
	        if (status === 'OK') {
	     	   document.forms['agentRequestForm'].elements['agentRequest.latitude'].value=results[0].geometry.location.lat();
	           document.forms['agentRequestForm'].elements['agentRequest.longitude'].value=results[0].geometry.location.lng();
	           
	           initMap();
	           }});
	}


	function getMailingAdress(){

	    var address1= document.getElementById('mailingAddress1').value; 
		var address2=document.getElementById('mailingAddress2').value;
		var mailingCity=document.getElementById('mailingCity').value;
		var mailingZip=document.getElementById('mailingZip').value;
		var mailingState=document.getElementById('mailingState').value;
		var mailingCountry=document.getElementById('mailingCountry').value;
		
		var mailingAdress = address1+" "+address2+" "+mailingCity+" "+address2+" "+mailingState+" "+mailingCountry;
		
		return mailingAdress;
	}
	findChangeFields();   
	var GOOGLE_MAP_KEY = googlekey;
	window.onload = function() { 
		try{
		  var script = document.createElement('script');
		  script.type = 'text/javascript';
		  script.src = 'https://maps.googleapis.com/maps/api/js?v=3&callback=initMap&key='+GOOGLE_MAP_KEY; 
		  document.body.appendChild(script);
		}catch(e){}
	}


	function initMap() { 
		
		if (document.forms['agentRequestForm'].elements['agentRequest.latitude'].value != '' && document.forms['agentRequestForm'].elements['agentRequest.longitude'].value != ''){
		
		var lat = parseFloat(document.forms['agentRequestForm'].elements['agentRequest.latitude'].value);    
	    var lng = parseFloat(document.forms['agentRequestForm'].elements['agentRequest.longitude'].value);
	    
	    var uluru = {lat: lat, lng: lng};
		var lastName=document.getElementById('lastName').value;

	  	var contentString ='<h5>'+lastName+'</h5>'+
	      '<div id="bodyContent">'+getMailingAdress()+
	      '</div>'+
	      '</div>';
	      
	   	var map = new google.maps.Map(document.getElementById('map'), {zoom: 16, mapTypeControl: false,   streetViewControl: false, center: uluru});
	        
	   	var marker = new google.maps.Marker({position: uluru, map: map});

	    marker.addListener('click', function() {
	             infowindow.open(map, marker);           
	             });
	  
	  	var infowindow = new google.maps.InfoWindow({
	              content: contentString
	             });
		
	}}
   	function saveNewPinLocation()
   	{
   	if(document.getElementById("lat").value!='' && document.getElementById("lng").value!='')
   	{
   	 document.forms['agentRequestForm'].elements['agentRequest.latitude'].value=document.getElementById("lat").value;
     document.forms['agentRequestForm'].elements['agentRequest.longitude'].value=document.getElementById("lng").value;
   
     initMap();
     }
   	}
  

</script>