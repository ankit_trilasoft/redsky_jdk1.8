<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>


<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    

<script language="javascript" type="text/javascript">
function defaultclear_fields(){
			document.forms['partnerListForm'].elements['partner.lastName'].value = "";
			document.forms['partnerListForm'].elements['partner.partnerCode'].value = "";
			document.forms['partnerListForm'].elements['partner.aliasName'].value = "";
			document.forms['partnerListForm'].elements['partner.extReference'].value = "";
			<c:if test="${partnerType == 'AG' || partnerType == 'VN'  || partnerType == 'CR'}"> 
				document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value = "";
				document.forms['partnerListForm'].elements['partner.terminalState'].value = "";
				document.forms['partnerListForm'].elements['partner.terminalCountry'].value = "";
			</c:if>  
   			<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''|| partnerType == 'DF'}">  
				document.forms['partnerListForm'].elements['partner.billingCountryCode'].value = "";
				document.forms['partnerListForm'].elements['partner.billingState'].value = "";
				document.forms['partnerListForm'].elements['partner.billingCountry'].value = "";
			</c:if> 
			
}
function clear_fields(){
		document.forms['partnerListForm'].elements['partner.lastName'].value = '';
		document.forms['partnerListForm'].elements['partner.firstName'].value = '';
		document.forms['partnerListForm'].elements['partner.aliasName'].value = "";
		document.forms['partnerListForm'].elements['partner.partnerCode'].value = '';
		document.forms['partnerListForm'].elements['countryCodeSearch'].value = '';
		document.forms['partnerListForm'].elements['stateSearch'].value = '';
		document.forms['partnerListForm'].elements['countrySearch'].value = ''; 
		document.forms['partnerListForm'].elements['partner.extReference'].value = "";
		document.forms['partnerListForm'].elements['partner.isPrivateParty'].checked = false;
		document.forms['partnerListForm'].elements['partner.isAccount'].checked = false;
		document.forms['partnerListForm'].elements['partner.isAgent'].checked = false;
		document.forms['partnerListForm'].elements['partner.isVendor'].checked = false;
		document.forms['partnerListForm'].elements['partner.isCarrier'].checked = false;
		document.forms['partnerListForm'].elements['partner.isOwnerOp'].checked = false;
} 
function openOriginLocation(address1,address2,city,zip,state,country) {
 		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address1+','+address2+','+city+','+zip+','+state+','+country);
	}
function findDefault(){
      var sid=document.forms['partnerListForm'].elements['sid'].value; 
      document.forms['partnerListForm'].elements['partnerType'].value='DF';  
      document.forms['partnerListForm'].action ='findVendorCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode&fld_seventhDescription=seventhDescription';
      document.forms['partnerListForm'].submit();  	  

}

function findPartnerList(){
      var sid=document.forms['partnerListForm'].elements['sid'].value;  
      document.forms['partnerListForm'].elements['partnerType'].value='PA'; 
      document.forms['partnerListForm'].action ='findVendorCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode&fld_seventhDescription=seventhDescription';
      document.forms['partnerListForm'].submit();  	
}
</script>
<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="scripts/ajax.js"></script>
	<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
	<link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
	<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">
	
<script language="javascript" type="text/javascript">
function findUserPermission(name,position) { 
  var url="findAcctRefNumList.html?ajax=1&decorator=simple&popup=true&code=" + encodeURI(name);
  ajax_showTooltip(url,position);	
  }

</script>

<script>
this.onclick = function() {
   new Draggable('ajax_tooltipObj', 
                {starteffect: effectFunction('ajax_tooltipObj')});
   ajax_tooltipObj.style.cursor = "move";
}

function effectFunction(element)
{
   new Effect.Opacity(element, {from:0, to:1.0, duration:0.8});
}
</script>
<style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-left:385px;
margin-bottom:22px;
!margin-bottom:2px;
margin-top:-38px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:60%;
!width:98%;
}

div.error, span.error, li.error, div.message {

width:250px;
margin: -10px auto;
font-size: 1.05em;

}
</style>
</head> 
<c:set var="buttons">   
	<c:if test="${not empty param.popup && partnerType == 'PP'}">  
		<input type="button" class="cssbutton" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerAddFormPopup.html?partnerType=${partnerType}&decorator=popup&popup=true"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	</c:if>
	<c:if test="${empty param.popup}">  
    <input type="button" class="cssbutton" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerAddForm.html?partnerType=${partnerType}"/>'"  
        value="<fmt:message key="button.add"/>"/>
    </c:if> 
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="" key="button.search"/> 
	<c:if test="${partnerType == 'DF'}">
	<input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="defaultclear_fields();"/>  
	</c:if>
	<c:if test="${partnerType == 'PA'}">   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
    </c:if>
</c:set>  
<s:form id="partnerListForm" action='${empty param.popup?"searchPartnerAdmin.html":"searchPartnerPayableProcessing.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<s:hidden name="popupFrom" value="<%= request.getParameter("popupFrom")%>" />
<s:hidden name="defaultListFlag" value="<%= request.getParameter("defaultListFlag")%>" />
<c:set var="defaultListFlag" value="<%= request.getParameter("defaultListFlag")%>" scope="request" /> 
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<c:set var="sid" value="<%= request.getParameter("sid")%>" />
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="partnerType" value="<%=request.getParameter("partnerType")%>"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" /> 
<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden  name="fld_seventhDescription" value="${param.fld_seventhDescription}" />
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" /> 
    <c:set var="fld_seventhDescription" value="${param.fld_seventhDescription}" />	
</c:if>

<div id="layer1" style="width:100%;">
<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  
				  </ul>
				</div> 
				<div class="spnblk">&nbsp;</div> 
				  <c:if test="${partners=='[]'}"> 
				    <div class="message" style="">Please enter your search criteria below</div>
				    </c:if>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
    <c:if test="${partnerType == 'DF'}"> 
    <table class="table" border="0" style="width:100%;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th>External Ref.</th>
<th><fmt:message key="partner.name"/></th>
<th>Alias Name</th>
<th>Country Code</th>
<th>Country Name</th>
<th><fmt:message key="partner.billingState"/></th>

<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partner.partnerCode" size="15" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="partner.extReference" size="15" cssClass="text medium" />
			</td>
			<td>
			    <s:textfield name="partner.lastName" size="15" cssClass="text medium" />
			</td>
			<td>
			    <s:textfield name="partner.aliasName" size="15" cssClass="text medium" />
			</td>
			<c:if test="${partnerType == 'AG' || partnerType == 'VN'  || partnerType == 'CR'}">  
			 			<td>
						    <s:textfield name="partner.terminalCountryCode" size="15" cssClass="text medium"/>
						</td>
						<td>
						    <s:textfield name="partner.terminalCountry" size="24" cssClass="text medium"/>
						</td>
						<td>
						    <s:textfield name="partner.terminalState" size="24" cssClass="text medium"/>
						</td>
			</c:if>
			<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == '' || partnerType == 'DF'}">  
						<td>
						    <s:textfield name="partner.billingCountryCode" size="15" cssClass="text medium"/>
						</td>
						<td>
						    <s:textfield name="partner.billingCountry" size="24" cssClass="text medium"/>
						</td>
						<td>
						    <s:textfield name="partner.billingState" size="24" cssClass="text medium"/>
						</td>
			</c:if>
		</tr>
		<tr>
			<td colspan="5"></td>
			<td width="130px" style="border-left: hidden;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
    </c:if>
    <c:if test="${partnerType == 'PA'}"> 
<table class="table" border="0" style="width:100%;">
	<thead>
		<tr>
			<th><fmt:message key="partner.partnerCode"/></th>
			<th>External Ref.</th>
			<th><fmt:message key="partner.firstName"/></th>
			<th>Alias Name</th>
			<th>Last/Company Name</th>
			<th>Country Code</th>
			<th>Country Name</th>
			<th><fmt:message key="partner.billingState"/></th>			
			
			<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td><s:textfield name="partner.partnerCode" size="15" cssClass="input-text"/></td>
			<td>
			    <s:textfield name="partner.extReference" size="15" cssClass="input-text" />
			</td>
			<td><s:textfield name="partner.firstName" size="15" cssClass="input-text" /></td>
			<td><s:textfield name="partner.aliasName" size="15" cssClass="input-text" /></td>
			<td><s:textfield name="partner.lastName" size="15" cssClass="input-text" /></td>
			<td><s:textfield name="countryCodeSearch" size="15" cssClass="input-text"/></td>
			<td><s:textfield name="countrySearch" size="15" cssClass="input-text"/></td>
			<td><s:textfield name="stateSearch" size="10" cssClass="input-text"/></td>
			<s:hidden  name="partner.status" value="Approved"/></td>
		</tr>
		<tr>
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isPrivateParty}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			
			<td align="right" class="listwhitetext" style="border-right:hidden;width:100px;">Private party<s:checkbox key="partner.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>				
			
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isAccount}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="right" class="listwhitetext" style="border-right:hidden;width:100px;"><fmt:message key='partner.accounts'/><s:checkbox key="partner.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="12"/></td>
			
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isAgent}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="right" class="listwhitetext" style="border-right:hidden;width:100px;"><fmt:message key='partner.agents'/><s:checkbox key="partner.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="13"/></td>
										
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isVendor}">
			<c:set var="ischecked" value="true"/>
			</c:if>					
			<td align="center" class="listwhitetext" style="border-right:hidden;width:100px;"><fmt:message key='partner.vendors'/><s:checkbox key="partner.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="14"/></td>
											
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isCarrier}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="center" class="listwhitetext" style="border-right:hidden;width:100px;"><fmt:message key='partner.carriers'/><s:checkbox key="partner.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="15"/></td>
					
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isOwnerOp}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="center" class="listwhitetext" style="border-right:hidden;width:100px;"><fmt:message key='partner.owner'/><s:checkbox key="partner.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
		
			<td width="130px" style="border-left:none;text-align:right;" colspan="2"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		</tr>
	</tbody>
</table>
</c:if>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	</div>
	</s:form> 
<div id="layer2" style="width:100%;">
<div id="newmnav">   
 <ul>  
		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="findPartnerList();"><span>Partner List <img src="images/navarrow.gif" align="absmiddle" /></span></a></li> 
 </ul>
</div><div class="spn" style="width:100%">&nbsp;</div><br> 
<s:set name="partners" value="partners" scope="request"/>  
<display:table name="partners" class="table" requestURI="" id="partnerList" export="${empty param.popup}" defaultsort="2" pagesize="10" 
		style="width:100%;margin-top:-10px;" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode"
		href="editPartnerAddForm.html?partnerType=${partnerType}" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>    
    <display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
    <display:column title="Alias Name" sortable="true" style="width:190px"><c:out value="${partnerList.aliasName}"/></display:column>
    <display:column title="PParty" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerList.isPrivateParty == true}">  
    		<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
    	</c:if>
	</display:column>
	<display:column title="Account" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerList.isAccount == true}">  
    		<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
    	</c:if>
	</display:column>
	<display:column title="Agent" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerList.isAgent == true}">  
    		<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
    	</c:if>
	</display:column>
	<display:column title="Carrier" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerList.isCarrier == true}">  
    		<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
    	</c:if>
	</display:column>
	<display:column title="Vendor" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerList.isVendor == true}">  
    		<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
    	</c:if>
	</display:column>
	<display:column title="Driver" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerList.isOwnerOp == true}">  
    		<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
    	</c:if>
	</display:column>		
    <display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:120px"/>
    <c:if test="${param.popup}">
     <c:if test="${requestScope.defaultListFlag !='show' }">
    	<display:column style="width:120px;cursor:pointer;"><A onclick="location.href='<c:url value="/viewPartner.html?id=${partnerList.id}&sid=${sid}&partnerType=${partnerType}&type=ZZ&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}&defaultListFlag=${requestScope.defaultListFlag}"/>'">View Detail</A></display:column>
     </c:if>
    </c:if> 
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/> 
    <display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table> 
</div>
<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:if test="${not empty param.popup && partnerType == 'PP' && (flag==0 || flag==1 || flag==2 || flag==3 || flag==4)}">  
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td><c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:set var="isTrue" value="false" scope="session"/>



<script type="text/javascript">    
   Form.focusFirstElement($("partnerListForm")); 
  try{
   	if('<%=session.getAttribute("lastName")%>'=='null'){
   		document.forms['partnerListForm'].elements['partner.lastName'].value='';
   	}
   	}
   	catch(e){}
   try{
   	if('<%=session.getAttribute("lastName")%>'!='null'){
   		document.forms['partnerListForm'].elements['partner.lastName'].value='<%=session.getAttribute("lastName")%>';
   	}
   	}
   	catch(e){}
   try{
   	if('<%=session.getAttribute("partnerCode")%>'=='null'){	
   		document.forms['partnerListForm'].elements['partner.partnerCode'].value='';
   	}
   	}
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("partnerCode")%>'!='null'){	
   		document.forms['partnerListForm'].elements['partner.partnerCode'].value='<%=session.getAttribute("partnerCode")%>';
   	}
   	}
   	catch(e){}
   	try{
   	<c:if test="${partnerType == 'AG' || partnerType == 'VN'  || partnerType == 'CR'}"> 
   		if('<%=session.getAttribute("billingCountryCode")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value='';
	   	}
		if('<%=session.getAttribute("billingCountryCode")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.terminalCountryCode'].value='<%=session.getAttribute("billingCountryCode")%>';
	   	}
	   	
	   	if('<%=session.getAttribute("billingCountry")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.terminalCountry'].value='';
	   	}
		if('<%=session.getAttribute("billingCountry")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.terminalCountry'].value='<%=session.getAttribute("billingCountry")%>';
	   	}
	   	
	   	if('<%=session.getAttribute("billingStateCode")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.terminalState'].value='';
	   	}
	   	if('<%=session.getAttribute("billingStateCode")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.terminalState'].value='<%=session.getAttribute("billingStateCode")%>';
	   	}
   	</c:if> 
   	}
   	catch(e){}
   	try{
   	<c:if test="${partnerType == 'PP' || partnerType == 'AC' || partnerType == 'OO' || partnerType == ''|| partnerType == 'DF'}">   
   	   	if('<%=session.getAttribute("billingCountryCode")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value='';
	   	}
		if('<%=session.getAttribute("billingCountryCode")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.billingCountryCode'].value='<%=session.getAttribute("billingCountryCode")%>';
	   	}
	   	
	   	if('<%=session.getAttribute("billingCountry")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.billingCountry'].value='';
	   	}
		if('<%=session.getAttribute("billingCountry")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.billingCountry'].value='<%=session.getAttribute("billingCountry")%>';
	   	}
	   	
	   	if('<%=session.getAttribute("billingStateCode")%>'=='null'){
	   		document.forms['partnerListForm'].elements['partner.billingState'].value='';
	   	}
	   	if('<%=session.getAttribute("billingStateCode")%>'!='null'){
	   		document.forms['partnerListForm'].elements['partner.billingState'].value='<%=session.getAttribute("billingStateCode")%>';
	   	}
	</c:if>
   }
   catch(e){}
    
	
	
	
       
</script>