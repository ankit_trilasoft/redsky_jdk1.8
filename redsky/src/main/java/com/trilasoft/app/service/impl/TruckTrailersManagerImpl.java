package com.trilasoft.app.service.impl;

import java.util.List;

import com.trilasoft.app.model.TruckTrailers;
import com.trilasoft.app.dao.TruckTrailersDao;
import com.trilasoft.app.service.TruckTrailersManager;
import org.appfuse.service.impl.GenericManagerImpl;
public class TruckTrailersManagerImpl extends GenericManagerImpl<TruckTrailers, Long> implements TruckTrailersManager{

	TruckTrailersDao truckTrailersDao;
	public TruckTrailersManagerImpl(TruckTrailersDao truckTrailersDao){
		super(truckTrailersDao);
		this.truckTrailersDao=truckTrailersDao;
		}
	public List getPrimaryFlagStatus(String truckNumber, String corpId){
		return truckTrailersDao.getPrimaryFlagStatus(truckNumber, corpId);
	}
	 public List  findTruckTrailersList(String truckNumber,String corpId){
	    	return truckTrailersDao.findTruckTrailersList(truckNumber, corpId);
	    }
	 public List trailersPopup(){
		 return truckTrailersDao.trailersPopup();
	 }
	 public List searchTrailerPopupList(String truckNumber,String ownerPayTo, String corpId){
		 return truckTrailersDao.searchTrailerPopupList(truckNumber, ownerPayTo, corpId);
	 }
}
