package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.PolicyDocumentDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.PolicyDocument;

public class PolicyDocumentDaoHibernate extends GenericDaoHibernate<PolicyDocument, Long> implements PolicyDocumentDao {

   private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}
	public PolicyDocumentDaoHibernate(){
		super(PolicyDocument.class);
	}
	
	public List checkPolicyId(Long fid){
		return getHibernateTemplate().find("select id from ContractPolicy where  id=?", fid);
	}
	public List getMaxId(){
		return getHibernateTemplate().find("select max(id) from ContractPolicy");
	}
    public int checkPolicyDocumentName(String documentName,String sectionName,String language,String partnerCode,String sessionCorpID){
		if(documentName==null) documentName="";
		if(sectionName==null) sectionName="";
		if(language==null) language="";
		if(partnerCode==null) partnerCode="";
		String temp=this.getSession().createSQLQuery("select count(*) from policydocument where partnerCode='"+partnerCode+"' and sectionName='"+sectionName+"' and documentName='"+documentName+"' and language='"+language+"' and  corpId in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') ").list().get(0).toString();
		int countValue=Integer.parseInt(temp);
		return countValue;
    }
}
