package com.trilasoft.app.webapp.action;

import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.KeySurveyQuestionMap;
import com.trilasoft.app.service.KeySurveyQuestionMapManager;

public class KeySurveyQuestionMapAction extends BaseAction{
	
	private KeySurveyQuestionMapManager keySurveyQuestionMapManager;
	private KeySurveyQuestionMap keySurveyQuestionMap;
	private List keySurveyQuestionMapList;
	private String sessionCorpID;
	private Long id;
	static final Logger logger = Logger.getLogger(KeySurveyQuestionMap.class);
	public KeySurveyQuestionMapAction()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		this.sessionCorpID=user.getCorpID();	
	}
	public KeySurveyQuestionMap getKeySurveyQuestionMap() {
		return keySurveyQuestionMap;
	}
	public void setKeySurveyQuestionMap(KeySurveyQuestionMap keySurveyQuestionMap) {
		this.keySurveyQuestionMap = keySurveyQuestionMap;
	}
	public List getKeySurveyQuestionMapList() {
		return keySurveyQuestionMapList;
	}
	public void setKeySurveyQuestionMapList(List keySurveyQuestionMapList) {
		keySurveyQuestionMapList = keySurveyQuestionMapList;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setKeySurveyQuestionMapManager(
			KeySurveyQuestionMapManager keySurveyQuestionMapManager) {
		this.keySurveyQuestionMapManager = keySurveyQuestionMapManager;
	}

}
