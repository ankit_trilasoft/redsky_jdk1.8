<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
<script language="javascript" type="text/javascript">

function clear_fields(){
			document.forms['partnerListForm'].elements['partner.lastName'].value = "";
			document.forms['partnerListForm'].elements['partner.partnerCode'].value = "";
			document.forms['partnerListForm'].elements['partner.billingCountryCode'].value = "";
			document.forms['partnerListForm'].elements['partner.billingState'].value = "";
}

</script>
<style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:5px;
margin-top:-33px;
padding:2px 0px;
text-align:right;
width:80%;
}
</style>
</head>
<!-- 
<c:set var="forms"> 
<s:form id="partnerListDetailForm" action='${empty param.popup?"editPartner.html":"editPartner.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="id"/>
<s:submit cssClass="button" method="edit" key="button.viewDetail"/>
</s:form>
</c:set>
 -->
  
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerAddForm.html?partnerType=${partnerType}"/>'"  
        value="<fmt:message key="button.add"/>"/>   
     <%--   
    <input type="button" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/> 
      --%>    
</c:set> 
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="popupList" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerListForm" action='${empty param.popup?"searchVendorPartner.html":"searchVendorPartner.html?decorator=popup&popup=true"}' method="post" >  

<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<s:hidden name="popupFrom" value="<%= request.getParameter("popupFrom")%>" />
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />


<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
</c:if>
<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 

<table class="table" style="width:800px;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th><fmt:message key="partner.name"/></th>
<th><fmt:message key="partner.billingCountryCode"/></th>
<th><fmt:message key="partner.billingState"/></th>
<th style="background:none;border:none;"></th>
<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partner.partnerCode" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="partner.lastName" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="partner.billingCountryCode" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="partner.billingState" cssClass="text medium"/>
			</td>
			<td width="130px">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
</s:form>

<div id="newmnav">   
 <ul>
 <c:choose>
 	<c:when test="${partnerType == 'VN'}"> 
		<li id="newmnav1" style="background:#FFF"><a class="current" ><span>Vendors List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose>
</ul>
</div><div class="spn" style="width:800px">&nbsp;</div><br>

<s:set name="partners" value="partners" scope="request"/>  
<display:table name="partners" class="table" requestURI="" id="partnerList" export="${empty param.popup}" defaultsort="1" pagesize="10" 
		style="width:800px" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode"
		href="editPartnerAddForm.html?partnerType=${partnerType}" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>    
    <display:column titleKey="partner.name" style="width:345px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
    <display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:140px"/>
   	
    
    
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>   
  
    <display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>  
</div>
<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:set var="isTrue" value="false" scope="session"/>
<!--

			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" align="center" class="content-tab"><a href="editPartner.html?id=${myMessageList.id} " >Addresses</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" align="center" class="content-tab"><a href="editPartnerDetail.html?id=${myMessageList.id} " >Details</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" align="center" class="content-tab"><a href="editPartnerRemarks.html?id=${myMessageList.id} " >Remarks</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
 -->


<script type="text/javascript">   
   //highlightTableRows("partnerList"); 
   Form.focusFirstElement($("partnerListForm"));      
</script>