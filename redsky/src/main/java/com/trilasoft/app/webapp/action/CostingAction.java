package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.lang.reflect.Array;
import java.util.*;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction; 
  
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Costing;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CostingManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
   

  
public class CostingAction extends BaseAction {   
  
    private List costings;  
   
  
    private ServiceOrderManager serviceOrderManager;
    private BillingManager billingManager;
    private CostingManager costingManager;
    
    private String sessionCorpID;
    Date currentdate = new Date();
    static final Logger logger = Logger.getLogger(CostingAction.class);

     

    public CostingAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
    
	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
        this.serviceOrderManager = serviceOrderManager;
    }

	public void setCostingManager(CostingManager costingManager) {
		this.costingManager = costingManager;
	}
 
    private Long id;
    private Long IdMax;
    private Long newInvoiceNumber;
	private ServiceOrder serviceOrder;
	private Costing costing;
	private Long sid;
	private Billing billing;
	public int flag;


	private int rowAffected;


	private CustomerFile customerFile;

	public List getCostings() {
		return costings;
	}

	public void setCostings(List costings) {
		this.costings = costings;
	}

	public Costing getCosting() {
		return costing;
	}
      
    public void setCosting(Costing costing) {
		this.costing = costing;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public void setSid(Long sid) {   
	       this.sid = sid;   
    } 

	public void setId(Long id) {   
        this.id = id;   
    }   
      
    public ServiceOrder getServiceOrder() {   
        return serviceOrder;   
    }   
      
    public void setServiceOrder(ServiceOrder serviceOrder  ) {   
        this.serviceOrder = serviceOrder;   
    }   
          
    public String list() { 
    	costings=costingManager.getAll();
        
        return SUCCESS;   
    }

    public String edit() { 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	
      //System.out.println("\n\n Service Order ID is :\t"+sid);
       if (id != null)
        { 
    	  	// For Existing Invoice Line
			costing = costingManager.get(id);
			serviceOrder = costing.getServiceOrder();
        }
       else 
         {   
    	   //    	 For New Invoice Line
        	costing = new Costing();   
			serviceOrder=serviceOrderManager.get(sid);
			billing=billingManager.get(sid);
			costing.setShipNumber(serviceOrder.getShipNumber());
			costing.setCreatedOn(new Date());
	        costing.setUpdatedOn(new Date());
	        costing.setBillToCode(serviceOrder.getCustomerFile().getBillToCode());
	        costing.setBillToName(serviceOrder.getCustomerFile().getBillToName());
	        costing.setContract(billing.getContract());
	        costing.setPoNumber(billing.getBillToAuthority());
	        costing.setStorageBegin(billing.getBillThrough());
	        costing.setStorageEnd(billing.getStorageOut());
	        costing.setWarehouse(billing.getWarehouse());
	        costing.setCommodity(serviceOrder.getCommodity());
	        flag=1;
	        
         }   
    customerFile=serviceOrder.getCustomerFile();
	if(flag==1)
	{
		//System.out.println("\n\n Flag set \n\n");
	}
	else
	{
		//System.out.println("\n\n Flag not set \n\n");
	}
	
	getComboList(sessionCorpID,serviceOrder.getShipNumber());
      //System.out.println("\n\n Costing record is\n\n"+costing);
     // System.out.println("\n\n Service Order is\n\n"+serviceOrder);
      //System.out.println("\n\n In Costing Edit\n\n");
      costing.setCorpID(sessionCorpID);
     	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
     
    }
    
    
@SkipValidation   
public String generateInvoiceNumber() throws Exception {   
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID,serviceOrder.getShipNumber());

        boolean isNew = (costing.getId() == null);

        costing.setServiceOrder(serviceOrder);
        costing.setUpdatedOn(new Date());
        
        //System.out.println("\n\n Costing records going to save :\t"+costing);
        
        costingManager.save(costing); 

        //System.out.println("\n\n Maximum Invoice Number :\t"+costingManager.findInvoiceNumber().get(0));
        
        if((costingManager.findInvoiceNumber()).get(0).equals(""))
    	{
    		//System.out.println("\n\n Invoice Number not Found :\t");
    		//Long tempInvoiceNumber = (10001);
        	newInvoiceNumber=Long.parseLong("10001");
    	}
    	else
    	{
    		newInvoiceNumber=Long.parseLong((costingManager.findInvoiceNumber()).get(0).toString())+1;
    	}
        
        
			 //newInvoiceNumber=Long.parseLong((costingManager.findInvoiceNumber()).get(0).toString())+1;
		     //System.out.println("\n\n New invoice Nos is \t"+newInvoiceNumber+"\n\n");
		     rowAffected=costingManager.genrateInvoiceNumber(newInvoiceNumber,serviceOrder.getShipNumber());
		     
		     String key;
		     if(rowAffected > 0)
		     {
		    	 key="Invoice number is assign against "+rowAffected+" Invoice line";	 
		     }
		     else
		     {
		    	 key="Sorry no Invoice line found for assign new Invoice number";
		     }
		     	 
           
        saveMessage(getText(key));
        
        if (!isNew) {   
        	//System.out.println("\n\n Not New Costing Record :\t");
        	IdMax = Long.parseLong((costingManager.findMaxId()).get(0).toString());
            costing = costingManager.get(IdMax);
            costing.setInvoiceNumber(costing.getInvoiceNumber());
           logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return INPUT;   
        } else {   
        	//System.out.println("\n\n New Costing Record :\t");
        	IdMax = Long.parseLong((costingManager.findMaxId()).get(0).toString());
        	//System.out.println("\n\n ID Max :\t"+IdMax);
            costing = costingManager.get(IdMax);
            //System.out.println("\n\n Costing Record are:\t"+costing);
            costing.setInvoiceNumber(costing.getInvoiceNumber());
            logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;   
        }
    }     
    

@SkipValidation
public String editAfterSubmitPartnerType() throws Exception {
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	//System.out.println(save);
	if ( save != null ) {
		return save();
	}
	//System.out.println("\n\n PayeeType"+costing.getExpPayeeType()+"\n\n");
	getComboList(sessionCorpID,serviceOrder.getShipNumber());
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;
	
}

public String save() throws Exception {  
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	//System.out.println("\n\n Costing records at just enter in save :\t"+costing);
		
		getComboList(sessionCorpID,serviceOrder.getShipNumber());

    	
        boolean isNew = (costing.getId() == null);
        if(isNew)
        {
        	costing.setCreatedOn(new Date());
        }
	      else
	        {
	    	  costing.setCreatedOn(costing.getCreatedOn());
	        }
        costing.setServiceOrder(serviceOrder);
        costing.setUpdatedOn(new Date());
        
        //System.out.println("\n\n Costing records going to save :\t"+costing);
        
        //System.out.println("\n\n In Costing Save\n\n");
        
        costingManager.save(costing); 

        String key = (isNew) ? "costing.added" : "costing.updated";
    
 	    saveMessage(getText(key));
        
        if (!isNew) { 
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return INPUT;   
        } else {   
        	IdMax = Long.parseLong((costingManager.findMaxId()).get(0).toString());
            costing = costingManager.get(IdMax);
            serviceOrder=costing.getServiceOrder();
            customerFile=serviceOrder.getCustomerFile();
            logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;   
        } 
    } 
    


 //FOR COMBOBOX
    
    private List refMasters;
	private RefMasterManager refMasterManager;


	private List ls;


	private ServicePartnerManager servicePartnerManager;


	private static Map<String, String> glCodes;
	private static Map<String, String> house;
	private static Map<String, String> commodit;
	private static Map<String, String> payeetyp;
	private static Map<String, String> action;
	private static Map<String, String> partnerType;
	private static Map<String, String> countrycode;
	private static List vendorPartnerType;
	
	
  
	public static List getVendorPartnerType() {
		return vendorPartnerType;
	}

	public static void setVendorPartnerType(List vendorPartnerType) {
		CostingAction.vendorPartnerType = vendorPartnerType;
	}

	public static Map<String, String> getCountrycode() {
		return countrycode;
	}

	public static Map<String, String> getCommodit() {
		return commodit;
	}

	public static void setCommodit(Map<String, String> commodit) {
		CostingAction.commodit = commodit;
	}
	
	public static Map<String, String> getHouse() {
		return house;
	}

	public static void setHouse(Map<String, String> house) {
		CostingAction.house = house;
	}


	public static Map<String, String> getGlCodes() {
		return glCodes;
	}

	public static void setGlCodes(Map<String, String> glCodes) {
		CostingAction.glCodes = glCodes;
	}
	public static Map<String, String> getAction() {
		return action;
	}

	public static Map<String, String> getPayeetyp() {
		return payeetyp;
	}


	public void setRefMasterManager(RefMasterManager refMasterManager) {
        this.refMasterManager = refMasterManager;
    }
    
    public List getRefMasters(){
    	return refMasters;
    }
    //String shipNumberForPartner=serviceOrder.getShipNumber();

    public String getComboList(String corpID, String shipNumberForPartner){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	glCodes = refMasterManager.findByParameter(corpID, "GLCODES");
    	house = refMasterManager.findByParameter(corpID, "HOUSE");
    	commodit = refMasterManager.findByParameter(corpID, "COMMODIT");
 	   payeetyp = refMasterManager.findByParameter(corpID, "PAYEETYP");
	   action = refMasterManager.findByParameter(corpID, "ACTION");
	   countrycode = refMasterManager.findByParameter(corpID, "COUNTRY");
	   partnerType = refMasterManager.findByParameter(corpID, "partnerType");
	   
	   vendorPartnerType=servicePartnerManager.findForParter(corpID,shipNumberForPartner);
	   //System.out.println("\n\n costing.getShipNumber() is :\t");
	   //System.out.println("\n\n vendorPartnerType from service Partner:\t"+servicePartnerManager.findForParter(corpID,costing.getShipNumber()));
	   //System.out.println("\n\n vendorPartnerType :\n\n"+vendorPartnerType);
  	   
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
  	   return SUCCESS;
     }

    public  Map<String, String> getPartnerType() {
		return partnerType;
	}

	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}


  }
        