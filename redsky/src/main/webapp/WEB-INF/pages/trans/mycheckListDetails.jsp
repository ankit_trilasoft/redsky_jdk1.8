<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>  
<title><fmt:message key="myFileList.title"/></title>   
<meta name="heading" content="<fmt:message key='myFileList.heading'/>"/>  
</head>
<s:form id="myFileCheckListForm" action="" method="post" >
<c:set var="fileForId" value="<%=request.getParameter("fileForId") %>"/>
<s:hidden name="fileForId" value="<%=request.getParameter("fileForId") %>"/>

<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
<c:set var="fileId" value="<%=request.getParameter("id") %>"/>

<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>" />
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>" />
<c:set var="salesPortalAccess" value="false" />
<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
<c:set var="salesPortalAccess" value="true" />
</sec-auth:authComponent>

<s:hidden name="noteFor" value="<%=request.getParameter("noteFor") %>" />
<c:set var="noteFor" value="<%=request.getParameter("noteFor") %>"/>
<c:if test="${myFileFor =='CF'}">
	<c:set var="idOfWhom" value="<%=request.getParameter("id") %>" scope="session"/>
	<c:set var="noteID" value="${TempnotesId}" scope="session"/>
	<c:set var="custID" value="" scope="session"/>
	<c:set var="noteFor" value="${noteFor}" scope="session"/>
	<c:if test="${empty customerFile.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty customerFile.id}">
		<c:set var="isTrue" value="true" scope="request"/>
	</c:if>
</c:if>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:if test="${myFileFor!='CF'}"> 
<s:hidden name="customerFile.firstName" />
<s:hidden name="customerFile.lastName" />
<s:hidden name="customerFile.sequenceNumber" />
<c:set var="idOfWhom" value="<%=request.getParameter("id") %>" scope="session"/>
<c:set var="noteID" value="${TempnotesId}" scope="session"/>
<c:set var="noteFor" value="${noteFor}" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<c:if test="${myFileFor!='PO'}">
<div id="layer6" style="width:100%; ">

<div id="newmnav" style="float:left; ">
            <c:choose>
	        <c:when test="${forQuotation!='QC'}">
            <ul>
               <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
				<s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
				<c:set var="relocationServicesKey" value="" />
				<c:set var="relocationServicesValue" value="" /> 
			    <c:forEach var="entry" items="${relocationServices}">
					<c:if test="${relocationServicesKey==''}">
					<c:if test="${entry.key==serviceOrder.serviceType}">
					<c:set var="relocationServicesKey" value="${entry.key}" />
					<c:set var="relocationServicesValue" value="${entry.value}" /> 
					</c:if>
					</c:if> 
               </c:forEach>
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
	            <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	            	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			    </c:if>
			    <c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
	            	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>Quotes</span></a></li>
			    </c:if>
			    </sec-auth:authComponent>
	            
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
		             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
		             	<li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
		             </sec-auth:authComponent>
	            </sec-auth:authComponent>
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
		              <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		             <c:choose>
					    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
					      	<li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
					    </c:when> --%>
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 					<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose>
				     </c:if> 
			     </sec-auth:authComponent>
			     <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		             <c:choose> 
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 					<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose>
				     </c:if> 
			     </sec-auth:authComponent>
			        <sec-auth:authComponent componentId="module.tab.serviceorder.accountingPortalTab">	
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	              <li><a href="accountLineSalesPortalList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	              </c:if>
	              </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	         	 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	         	 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         	 </sec-auth:authComponent>
	          	 </c:if>
			     <sec-auth:authComponent componentId="module.tab.trackingStatus.forwardingTab">
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			     <c:if test="${serviceOrder.job !='RLO'}"> 
			     		<c:if test="${forwardingTabVal!='Y'}"> 
	   						<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  					</c:if>
	  					<c:if test="${forwardingTabVal=='Y'}">
	  						<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  					</c:if>
			     </c:if>
			     </c:if>	
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.domesticTab">
		             <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
		             <c:if test="${serviceOrder.job !='RLO'}"> 
		             	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
		             </c:if>	
		             </c:if>
	             </sec-auth:authComponent>
                 <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
                 <c:if test="${serviceOrder.job =='INT'}">
                   <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
                 </c:if>
                 </sec-auth:authComponent>
	              <c:if test="${serviceOrder.job =='RLO'}">  
                  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
                 </c:if>
                 <c:if test="${serviceOrder.job !='RLO'}"> 
	             <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
	            </c:if>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.ticketTab">
	              <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	              <c:if test="${serviceOrder.job !='RLO'}"> 
	             	<li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
	             </c:if>
	             </c:if>	
	             </sec-auth:authComponent>
	            <configByCorp:fieldVisibility componentId="component.standard.claimTab">
	             <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
	             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	              <c:if test="${serviceOrder.job !='RLO'}"> 
	             	<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	             	</c:if>
	             	</c:if>
	             </sec-auth:authComponent>
	             </configByCorp:fieldVisibility>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">
	             	<li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
	           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
	           	 </sec-auth:authComponent>
	       </ul>
	        </c:when>
	       <c:otherwise>
		   <ul>
		    <li ><a href="QuotationFileForm.html?id=${serviceOrder.customerFileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${serviceOrder.customerFileId}&forQuotation=QC"><span>Quotes</span></a></li>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li>  	
		   </ul>
		</c:otherwise></c:choose>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
<div class="spn">&nbsp;</div>
<div style="!margin-top:8px; ">
      <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
      </c:if>
</div>
</div>
 </c:if>
  <c:if test="${myFileFor=='CF'}"> 
 <div id="Layer5" style="width:95%">	
	<c:choose>
	<c:when test="${forQuotation!='QC'}">
	<div id="newmnav">
		   <ul>
		  <c:if test="${customerFile.controlFlag=='A'}">
		    <li><a href="editOrderManagement.html?id=${customerFile.id}" ><span>Order Detail</span></a></li>
		    </c:if> 
		    <c:if test="${customerFile.controlFlag!='A'}">
		    <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
		    <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Service Orders</span></a></li> 
		    </c:if>
		    <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Quotes</span></a></li>
		    </c:if>
		    <c:if test="${salesPortalAccess=='false'}"> 
		    <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		    <!-- <li><a href="surveysList.html?id=${customerFile.id} "><span>Surveys</span></a></li> -->
		    <li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" ><span>Account Policy</span></a></li> 
		  	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>  
		   </c:if>
		    </c:if> 
          </ul>
		</div>
	 </c:when>
	 <c:otherwise>
		<div id="newmnav">
		   <ul>
            <li ><a href="QuotationFileForm.html?id=${fileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${fileId}&forQuotation=QC"><span>Quotes</span></a></li>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li> 
          </ul>
		</div></c:otherwise></c:choose><div class="spn">&nbsp;</div>
		 <div style="padding-bottom:0px;"></div>
 <div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="!margin-top:3px;"><span></span></div>
   <div class="center-content">
<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:90%">
	<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>
					<tr>
						<td align="right" class="listwhitebox">Cust#</td>
						<td><s:textfield name="customerFile.sequenceNumber" size="21" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" size="18" readonly="true" cssClass="input-textUpper"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td ><s:textfield name="customerFile.originCountryCode" required="true" size="13" readonly="true" cssClass="input-textUpper"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" size="18" readonly="true" cssClass="input-textUpper" /></td>
						<td align="left" class="listwhitebox"><fmt:message key='customerFile.billToCode'/></td>
						<td colspan="2"><s:textfield name="customerFile.billToName" required="true" size="35" readonly="true" cssClass="input-textUpper" /></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</c:if>  
<div id="Layer1" style="width:100%;">
		<div id="fc-newmnav">
			<ul>
				<li><a href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&noteFor=${noteFor}&forQuotation=${forQuotation}"><span>Document List</span></a></li>
				<li><a href="relatedFiles.html?id=${fileId}&myFileFrom=${fileNameFor}&active=true&secure=false&noteFor=${noteFor}&forQuotation=${forQuotation}"><span>Related Docs</span></a></li>
				<li><a href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&relatedDocs=No&active=false&noteFor=${noteFor}&forQuotation=${forQuotation}"><span>Waste Basket</span></a></li>
				<sec-auth:authComponent componentId="module.tab.myfile.securedocTab">
				<li ><a  href="secureFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=true&forQuotation=${forQuotation}"><span>Secure List<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
				</sec-auth:authComponent>
				<li id="fc-newmnav1" style="background:#FFF " ><a class="current" href="checkListFiles.html?id=${fileId}&myFileFrom=${fileNameFor}&myFileFor=${fileNameFor}&noteFor=${noteFor}&relatedDocs=No&active=false&forQuotation=${forQuotation}"><span>Check List</span></a></li>
			
			<c:if test="${myFileFor=='PO'}">
				        <c:if test="${!param.popup}"> 
						<li><a href="editPartnerPublic.html?id=${fileId}&partnerType=AG" ><span>Agent Detail</span></a></li>
					</c:if>
				    </c:if>	
				<%-- <li><a href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document Centre<img src="images/navarrow.gif" align="absmiddle" /></span></a></li> --%>
			</ul>
		</div>
		<div id="chkAllButton"  class="listwhitetext" style="display:none;float: left; !position: absolute; !margin-left: 450px; !margin-top: -25px;" >
			<input type="radio"  name="chk" onClick="checkAll()" /><strong>Check All</strong>
			<input type="radio"  name="chk" onClick="uncheckAll()"  /><strong>Uncheck All</strong>
		</div>
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;"></div>
		<table width="100%" cellspacing="0" cellpadding="0" style="margin: 0px">
<tbody><tr>
<td height="30" style=" background-image:url(images/bg.png); background-repeat:repeat-x;">
<div style="background-image: url(images/scope_bg.png); background-repeat:no-repeat; text-align:left;">

</div>
    </td>
  </tr>
</tbody></table>
		<table style="width: 100%;margin:0px;padding:0px;">
		<tr>
		<td>
		<tr>
<td colspan="3" align="left">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;margin:0px;padding:0px;">
<tr>
<td width="200px"><img src="${pageContext.request.contextPath}/images/check-List.jpg" border="1" style="cursor:auto;"/></td>
</tr>
</table>
</td>
</tr>
  <tr>
	<td height="10" width="100%" align="left" >
	 <div onClick="javascript:animatedcollapse.toggle('dueTodaychild2')" style="margin: 0px">
	 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Due Today</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countCheckListTodayResult}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	</div>
	<div id="dueTodaychild2">
	<display:table name="checkListTodayList" class="table" requestURI="" id="checkListTodayListId"  defaultsort="3" export="" pagesize="10">
	<display:column sortable="true" title="File ID" sortProperty="resultNumber">
	<c:out value="${checkListTodayListId.resultNumber}" />
	</display:column>
	<display:column property="messageDisplayed"  style="width:20%" sortable="true"     titleKey="toDoRulemessage.message"/>
	<display:column property="docType" sortable="true"   title="Document to Validate"/>
   	<display:column headerClass="containeralign" style="text-align: right" property="duration" sortable="true"   title="OD"/>
	<display:column property="shipper" sortable="true"   title="Shipper's Name"/>
	<display:column property="owner" sortable="true"   title="Name"/>
	</display:table>
	
</div>
</td>
</tr>
<tr>
	<td height="10" width="100%" align="left" >
	 <div onClick="javascript:animatedcollapse.toggle('overduechild2')" style="margin: 0px">
	 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Over Due </font> &nbsp;&nbsp;(<font color="red">&nbsp;${countCheckListOverDueResult}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	</div>
	<div id="overduechild2">
	<display:table name="checkListOverDueList" class="table" requestURI="" id="checkListOverDueListId"  defaultsort="3" export="" pagesize="10">
	<display:column sortable="true" title="File ID" sortProperty="resultNumber">
	<c:out value="${checkListOverDueListId.resultNumber}" />
	</display:column>
	<display:column property="messageDisplayed"  style="width:20%" sortable="true"     titleKey="toDoRulemessage.message"/>
	<display:column property="docType" sortable="true"   title="Document to Validate"/>
   	<display:column headerClass="containeralign" style="text-align: right" property="duration" sortable="true"   title="OD"/>
	<display:column property="shipper" sortable="true"   title="Shipper's Name"/>
	<display:column property="owner" sortable="true"   title="Name"/>
	</display:table>
	
</div>
</td>
</tr> 
		</td>
		</tr>
		</table>

</s:form> 
