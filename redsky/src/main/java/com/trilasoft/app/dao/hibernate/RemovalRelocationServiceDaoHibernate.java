package com.trilasoft.app.dao.hibernate;


import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.RemovalRelocationServiceDao;
import com.trilasoft.app.model.RemovalRelocationService;


public class RemovalRelocationServiceDaoHibernate extends GenericDaoHibernate<RemovalRelocationService, Long> implements RemovalRelocationServiceDao {   

    public RemovalRelocationServiceDaoHibernate() {   
        super(RemovalRelocationService.class);   
    } 
}
