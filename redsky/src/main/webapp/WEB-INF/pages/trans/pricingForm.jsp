<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ include file="/common/taglibs.jsp"%>
<%
    response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
%>

<style>
table-fc tbody th, .table tbody td {white-space:nowrap;}
.table td, .table th, .tableHeaderTable td {padding: 0.3em;}
</style>
<div id="para1" >
<s:hidden id="aidListIds" name="aidListIds" value="${aidListIds}"/>
<table id="accInnerTable" width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">
	<tr>
		<td>
        	<display:table name="accountLineList" id="accountLineList" class="table" style="width:100%;margin-top:2px;margin-left:0px;margin-right:0px;" pagesize="100" >
            	<display:column title="#" style="width:10px;">
            		<c:out value="${accountLineList_rowNum}"/>
            	</display:column>      
				<display:column titleKey="accountLine.accountLineNumber"  style="width:10px; font-size:12px;" > 
					<input type="text" style="text-align:right" id="accountLineNumber${accountLineList.id}" name="accountLineNumber${accountLineList.id}" value="${accountLineList.accountLineNumber }" size="1" maxlength="3" class="input-text" onkeydown="return onlyNumberAllowed(event)" onchange="checkAccountLineNumber('accountLineNumber${accountLineList.id}');" />
				</display:column>
				<display:column titleKey="accountLine.category" style="width:70px" > 
					<select id="category${accountLineList.id}" name ="category${accountLineList.id}" onchange="selectPricingMarketPlace('category${accountLineList.id}','${accountLineList.id}');" style="width:70px" class="list-menu"> 
						<c:forEach var="chrms" items="${category}" varStatus="loopStatus">
	                    	<c:choose>
	                               <c:when test="${chrms.key == accountLineList.category}">
	                               		<c:set var="selectedInd" value=" selected"></c:set>
	                               </c:when>
	                              <c:otherwise>
	                               		<c:set var="selectedInd" value=""></c:set>
	                              </c:otherwise>
	                        </c:choose>
	                        <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
	                        	<c:out value="${chrms.value}"></c:out>
	                        </option>
	                   </c:forEach> 
					</select>
				</display:column>
				<display:column title="Charge &nbsp;Code" style="width:100px;!width:155px;" >
					<input type="text" id="chargeCode${accountLineList.id}" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode }" size="20"  maxlength="25"  class="input-text" onkeyup="autoCompleterAjaxCallChargeCode(${accountLineList.id},'ChargeNameDivId${accountLineList.id}','${accountLineList.contract}')" onblur="fillDiscription('quoteDescription${accountLineList.id}','chargeCode${accountLineList.id}','category${accountLineList.id}','recGl${accountLineList.id}','payGl${accountLineList.id}','contractCurrency${accountLineList.id}','contractExchangeRate${accountLineList.id}','contractValueDate${accountLineList.id}','payableContractCurrency${accountLineList.id}','payableContractExchangeRate${accountLineList.id}','payableContractValueDate${accountLineList.id}','${accountLineList.id}');checkChargeCode('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')"/>
					<img class="openpopup" onclick="chk('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');findInternalCostVendorCode('${accountLineList.id}');" style="text-align:right;vertical-align:top;" width="17" height="20"  src="<c:url value='/images/open-popup.gif'/>" />
					<div id="ChargeNameDivId${accountLineList.id}" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
					<input type="hidden"  id="contractCurrency${accountLineList.id}" name="contractCurrency${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
					<input type="hidden"  id="recGl${accountLineList.id}" name="recGl${accountLineList.id}" value="${accountLineList.recGl}"/>
					<input type="hidden"  id="payGl${accountLineList.id}" name="payGl${accountLineList.id}" value="${accountLineList.payGl}"/>
					<input type="hidden"  id="contractExchangeRate${accountLineList.id}" name="contractExchangeRate${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>
					<input type="hidden"  id="contractValueDate${accountLineList.id}" name="contractValueDate${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
					<input type="hidden"  id="payableContractCurrency${accountLineList.id}" name="payableContractCurrency${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
					<input type="hidden"  id="payableContractExchangeRate${accountLineList.id}" name="payableContractExchangeRate${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
					<input type="hidden"  id="payableContractValueDate${accountLineList.id}" name="payableContractValueDate${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
					<input type="hidden"  id="division${accountLineList.id}" name="division${accountLineList.id}" value="${accountLineList.division}"/>
					<input type="hidden"  id="VATExclude${accountLineList.id}" name="VATExclude${accountLineList.id}" value="${accountLineList.VATExclude}"/>
					<c:if test="${systemDefaultVatCalculationNew!='Y'}">
						<input type="hidden"  id="recVatDescr${accountLineList.id}" name="recVatDescr${accountLineList.id}" value="${accountLineList.recVatDescr}"/>								
						<input type="hidden"  id="recVatPercent${accountLineList.id}" name="recVatPercent${accountLineList.id}" value="${accountLineList.recVatPercent}"/>
						<input type="hidden"  id="payVatDescr${accountLineList.id}" name="payVatDescr${accountLineList.id}" value="${accountLineList.payVatDescr}"/>								
						<input type="hidden"  id="payVatPercent${accountLineList.id}" name="payVatPercent${accountLineList.id}" value="${accountLineList.payVatPercent}"/>
						<input type="hidden"  id="vatAmt${accountLineList.id}" name="vatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>
						<input type="hidden"  id="estExpVatAmt${accountLineList.id}" name="estExpVatAmt${accountLineList.id}" value="${accountLineList.estExpVatAmt}"/>								
					</c:if>
					<c:if test="${contractType}">
						<input type="hidden"  id="estimatePayableContractCurrencyNew${accountLineList.id}" name="estimatePayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimatePayableContractCurrency}"/>
						<input type="hidden"  id="estimatePayableContractValueDateNew${accountLineList.id}" name="estimatePayableContractValueDateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractValueDate}"/>
						<input type="hidden"  id="estimatePayableContractExchangeRateNew${accountLineList.id}" name="estimatePayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractExchangeRate}"/>
						<input type="hidden"  id="estimatePayableContractRateNew${accountLineList.id}" name="estimatePayableContractRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRate}"/>
						<input type="hidden"  id="estimatePayableContractRateAmmountNew${accountLineList.id}" name="estimatePayableContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRateAmmount}"/>
						<input type="hidden"  id="estimateContractCurrencyNew${accountLineList.id}" name="estimateContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimateContractCurrency}"/>
						<input type="hidden"  id="estimateContractValueDateNew${accountLineList.id}" name="estimateContractValueDateNew${accountLineList.id}" value="${accountLineList.estimateContractValueDate}"/>
						<input type="hidden"  id="estimateContractExchangeRateNew${accountLineList.id}" name="estimateContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimateContractExchangeRate}"/>
						<input type="hidden"  id="estimateContractRateNew${accountLineList.id}" name="estimateContractRateNew${accountLineList.id}" value="${accountLineList.estimateContractRate}"/>
						<input type="hidden"  id="estimateContractRateAmmountNew${accountLineList.id}" name="estimateContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimateContractRateAmmount}"/>
						<input type="hidden"  id="revisionContractCurrencyNew${accountLineList.id}" name="revisionContractCurrencyNew${accountLineList.id}" value="${accountLineList.revisionContractCurrency}"/>
						<input type="hidden"  id="revisionContractValueDateNew${accountLineList.id}" name="revisionContractValueDateNew${accountLineList.id}" value="${accountLineList.revisionContractValueDate}"/>
						<input type="hidden"  id="revisionContractExchangeRateNew${accountLineList.id}" name="revisionContractExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionContractExchangeRate}"/>
		
						<input type="hidden"  id="revisionPayableContractCurrencyNew${accountLineList.id}" name="revisionPayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.revisionPayableContractCurrency}"/>
						<input type="hidden"  id="revisionPayableContractValueDateNew${accountLineList.id}" name="revisionPayableContractValueDateNew${accountLineList.id}" value="${accountLineList.revisionPayableContractValueDate}"/>
						<input type="hidden"  id="revisionPayableContractExchangeRateNew${accountLineList.id}" name="revisionPayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionPayableContractExchangeRate}"/>
		
						<input type="hidden"  id="contractCurrencyNew${accountLineList.id}" name="contractCurrencyNew${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
						<input type="hidden"  id="contractValueDateNew${accountLineList.id}" name="contractValueDateNew${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
						<input type="hidden"  id="contractExchangeRateNew${accountLineList.id}" name="contractExchangeRateNew${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>
		
						<input type="hidden"  id="payableContractCurrencyNew${accountLineList.id}" name="payableContractCurrencyNew${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
						<input type="hidden"  id="payableContractValueDateNew${accountLineList.id}" name="payableContractValueDateNew${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
						<input type="hidden"  id="payableContractExchangeRateNew${accountLineList.id}" name="payableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
					</c:if>						
					<input type="hidden"  id="estCurrencyNew${accountLineList.id}" name="estCurrencyNew${accountLineList.id}" value="${accountLineList.estCurrency}"/>
					<input type="hidden"  id="estValueDateNew${accountLineList.id}" name="estValueDateNew${accountLineList.id}" value="${accountLineList.estValueDate}"/>
					<input type="hidden"  id="estExchangeRateNew${accountLineList.id}" name="estExchangeRateNew${accountLineList.id}" value="${accountLineList.estExchangeRate}"/>
					<input type="hidden"  id="estLocalRateNew${accountLineList.id}" name="estLocalRateNew${accountLineList.id}" value="${accountLineList.estLocalRate}"/>
					<input type="hidden"  id="estLocalAmountNew${accountLineList.id}" name="estLocalAmountNew${accountLineList.id}" value="${accountLineList.estLocalAmount}"/>
					<input type="hidden"  id="estSellCurrencyNew${accountLineList.id}" name="estSellCurrencyNew${accountLineList.id}" value="${accountLineList.estSellCurrency}"/>
					<input type="hidden"  id="estSellValueDateNew${accountLineList.id}" name="estSellValueDateNew${accountLineList.id}" value="${accountLineList.estSellValueDate}"/>
					<input type="hidden"  id="estSellExchangeRateNew${accountLineList.id}" name="estSellExchangeRateNew${accountLineList.id}" value="${accountLineList.estSellExchangeRate}"/>
					<input type="hidden"  id="estSellLocalRateNew${accountLineList.id}" name="estSellLocalRateNew${accountLineList.id}" value="${accountLineList.estSellLocalRate}"/>
					<input type="hidden"  id="estSellLocalAmountNew${accountLineList.id}" name="estSellLocalAmountNew${accountLineList.id}" value="${accountLineList.estSellLocalAmount}"/>
					<input type="hidden"  id="buyDependSellNew${accountLineList.id}" name="buyDependSellNew${accountLineList.id}" value="${accountLineList.buyDependSell}"/>
					<input type="hidden"  id="oldEstimateSellDeviationNew${accountLineList.id}" name="oldEstimateSellDeviationNew${accountLineList.id}" />								
					<input type="hidden"  id="oldEstimateDeviationNew${accountLineList.id}" name="oldEstimateDeviationNew${accountLineList.id}" />
	
	
					<input type="hidden" id="revisionCurrencyNew${accountLineList.id}" name="revisionCurrencyNew${accountLineList.id}" value="${accountLineList.revisionCurrency}"/>
					<input type="hidden" id="revisionValueDateNew${accountLineList.id}" name="revisionValueDateNew${accountLineList.id}" value="${accountLineList.revisionValueDate}"/>
					<input type="hidden" id="revisionExchangeRateNew${accountLineList.id}" name="revisionExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionExchangeRate}"/>
					<input type="hidden" id="countryNew${accountLineList.id}" name="countryNew${accountLineList.id}" value="${accountLineList.country}"/>
					<input type="hidden" id="valueDateNew${accountLineList.id}" name="valueDateNew${accountLineList.id}" value="${accountLineList.valueDate}"/>
					<input type="hidden" id="exchangeRateNew${accountLineList.id}"  name="exchangeRateNew${accountLineList.id}" value="${accountLineList.exchangeRate}"/>
					<input type="hidden" id="accountLineCostElement${accountLineList.id}" name="accountLineCostElement${accountLineList.id}" value="${accountLineList.accountLineCostElement}"/>
					<input type="hidden" id="accountLineScostElementDescription${accountLineList.id}" name="accountLineScostElementDescription${accountLineList.id}" value="${accountLineList.accountLineScostElementDescription}"/>
				</display:column>
				<display:column   title="Vendor Code" style="width:80px;!width:120px;" > 
					<input type="text" id="vendorCode${accountLineList.id}"  name="vendorCode${accountLineList.id}"  value="${accountLineList.vendorCode }" size="6" class="input-text" onchange="checkVendorName('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}');fillCurrencyByChargeCode('${accountLineList.id}');" onfocus="enablePreviousFunction('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','${accountLineList.contract}');"/>
					<img  class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="javascript:winOpenForActgCode('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');" src="<c:url value='/images/open-popup.gif'/>" />
					<img  class="openpopup"  id="hidMarketPlace${accountLineList.id}" style="text-align:right;vertical-align:top;display:none;" onclick="javascript:marketPlaceCode('vendorCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','estimateVendorName${accountLineList.id}','estimateRate${accountLineList.id}','${contractType}');" src="<c:url value='/images/market-areaPP1.png'/>" />
					
					<c:if test="${accountLineList.category=='Destin' || accountLineList.category=='Origin' }">
					<c:if test="${pricePointFlag}">
					<c:if test="${accountLineList.externalIntegrationReference=='PP'}">
					<!--<a class="tooltips" style="text-decoration:none;"><img  class="openpopup"  id="hidMarketPlace11${accountLineList.id}" style="text-align:right;float:right;vertical-align:top;" onclick="javascript:marketPlaceCode('vendorCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','estimateVendorName${accountLineList.id}','estimateRate${accountLineList.id}');"  src="<c:url value='/images/market-areaPP1.png'/>" /><span>Pricing Point Ref# :<br/>${accountLineList.reference}</span></a>
				    -->
				    <img  class="openpopup"  id="hidMarketPlace11${accountLineList.id}" style="text-align:right;vertical-align:top;" onclick="javascript:marketPlaceCode('vendorCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','estimateVendorName${accountLineList.id}','estimateRate${accountLineList.id}','${contractType}');"  src="<c:url value='/images/market-areaPP1.png'/>" />
				    </c:if>
				    <c:if test="${accountLineList.externalIntegrationReference!='PP'}">
					<img  class="openpopup"  id="hidMarketPlace11${accountLineList.id}" style="text-align:right;vertical-align:top;" onclick="javascript:marketPlaceCode('vendorCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','estimateVendorName${accountLineList.id}','estimateRate${accountLineList.id}','${contractType}');"  src="<c:url value='/images/market-areaPP1.png'/>" />
				    </c:if>
				    </c:if>
				    </c:if>
				    <input type="hidden" id="pricePointAgent${accountLineList.id}"  name="pricePointAgent${accountLineList.id}" />
				    <input type="hidden" id="pricePointMarket${accountLineList.id}"  name="pricePointMarket${accountLineList.id}" />
				    <input type="hidden" id="pricePointTariff${accountLineList.id}"  name="pricePointTariff${accountLineList.id}" />
				</display:column>
				<display:column  title="Vendor &nbsp;Name" style="width:50px" >
					<input type="text" id="estimateVendorName${accountLineList.id}" name="estimateVendorName${accountLineList.id}"  value="${accountLineList.estimateVendorName }" onkeyup="findPartnerDetailsPriceing('estimateVendorName${accountLineList.id}','vendorCode${accountLineList.id}','pricingestimateVendorNameDiv${accountLineList.id}',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','${accountLineList.id}',event);" onchange="findPartnerDetailsByName('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}')" size="15" class="input-text" /> 
				<div id="pricingestimateVendorNameDiv${accountLineList.id}" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</display:column>
				<display:column   title="Basis"  style="width:70px;!width:85px;"> 
					<select id="basis${accountLineList.id}" name="basis${accountLineList.id}" style="width:50px" class="list-menu" onchange="changeBasis('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','','vatAmt${accountLineList.id}','${accountLineList.id}')"> 
						<c:forEach var="chrms" items="${basis}" varStatus="loopStatus">
							<c:choose>
								<c:when test="${chrms.value == accountLineList.basis}">
									<c:set var="selectedInd" value=" selected"></c:set>
								</c:when>
								<c:otherwise>
									<c:set var="selectedInd" value=""></c:set>
								</c:otherwise>
							</c:choose>
							<option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
								<c:out value="${chrms.value}"></c:out>
							</option>
	                    </c:forEach> 
					</select>
					<input type="hidden" id="oldbasis${accountLineList.id}" name="oldbasis${accountLineList.id}" value="${accountLineList.basis}"/><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="findRevisedEstimateQuantitys('chargeCode${accountLineList.id}','category${accountLineList.id}','estimateExpense${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','deviation${accountLineList.id}','estimateDeviation${accountLineList.id}','estimateSellDeviation${accountLineList.id}','${accountLineList.id}');"/>
					<input type="hidden" id ="deviation${accountLineList.id}" name ="deviation${accountLineList.id}"  value="${accountLineList.deviation}" />
					<input type="hidden" id="estimateDeviation${accountLineList.id}" name="estimateDeviation${accountLineList.id}"  value="${accountLineList.estimateDeviation}" />
					<input type="hidden" id="estimateSellDeviation${accountLineList.id}" name="estimateSellDeviation${accountLineList.id}"  value="${accountLineList.estimateSellDeviation}" /> 		               		               
				 
				 </display:column>
				 <c:if test="${systemDefaultVatCalculationNew=='Y'}">
				 <display:column title="Rec VAT&nbsp;Desc"  style="width:50px"> 
						<select id="recVatDescr${accountLineList.id}" name ="recVatDescr${accountLineList.id}" style="width:75px" class="list-menu" onchange="changeVatAmt('REVENUE','${accountLineList.id}');">
							<c:forEach var="chrms" items="${estVatList}" varStatus="loopStatus">
	                              <c:choose>
		                               <c:when test="${chrms.key == accountLineList.recVatDescr}">
		                               		<c:set var="selectedInd" value="selected"></c:set>	                                  
		                               </c:when>
		                              <c:otherwise>
		                               		<c:set var="selectedInd" value=""></c:set>
		                              </c:otherwise>
	                              </c:choose>
	                             <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
	                            		<c:out value="${chrms.value}"></c:out>
	                             </option>
		                    </c:forEach>                                 
						</select>
					<input type="hidden" id="recVatPercent${accountLineList.id}"  name="recVatPercent${accountLineList.id}" value="${accountLineList.recVatPercent}"/>
				</display:column>
				 <display:column title="Pay VAT&nbsp;Desc"  style="width:50px"> 
						<select id="payVatDescr${accountLineList.id}" name ="payVatDescr${accountLineList.id}" style="width:75px" class="list-menu" onchange="changeVatAmt('EXPENSE','${accountLineList.id}');">
							<c:forEach var="chrms" items="${payVatList}" varStatus="loopStatus">
	                              <c:choose>
		                               <c:when test="${chrms.key == accountLineList.payVatDescr}">
		                               		<c:set var="selectedInd" value="selected"></c:set>	                                  
		                               </c:when>
		                              <c:otherwise>
		                               		<c:set var="selectedInd" value=""></c:set>
		                              </c:otherwise>
	                              </c:choose>
	                             <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
	                            		<c:out value="${chrms.value}"></c:out>
	                             </option>
		                    </c:forEach>                                 
						</select>
					<input type="hidden" id="payVatPercent${accountLineList.id}"  name="payVatPercent${accountLineList.id}" value="${accountLineList.payVatPercent}"/>
				</display:column>
				 </c:if>
				 <display:column title="Quantity"  style="width:18px" >
					<input type="text" id="estimateQuantity${accountLineList.id}" style="text-align:right" name="estimateQuantity${accountLineList.id}"  value="${accountLineList.estimateQuantity }" size="4" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloatQuantity(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','','vatAmt${accountLineList.id}','${accountLineList.id}');calculateExpense('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','','vatAmt${accountLineList.id}','${accountLineList.id}','','','');changeVatAmt('REVENUE','${accountLineList.id}');changeVatAmt('EXPENSE','${accountLineList.id}');"/>
					<input type="hidden" id="oldestimateQuantity${accountLineList.id}" name="oldestimateQuantity${accountLineList.id}" value="${accountLineList.estimateQuantity }"/>
				</display:column>
				
				<display:column title="Buy Rate" headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
					<input type="text" style="text-align:right" id="estimateRate${accountLineList.id}" name="estimateRate${accountLineList.id}" value="${accountLineList.estimateRate }" size="5" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat(this)" onchange="calculateExpense('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','','vatAmt${accountLineList.id}','${accountLineList.id}')"/>
				</display:column>  
				<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;">
					<c:choose>
                       <c:when test="${accountLineList.estCurrency!=baseCurrency  }">
                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
                       </c:when> 
                       <c:otherwise>
                         <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
                       </c:otherwise>
                    </c:choose> 
				</display:column> 
				<c:if test="${contractType}">
				 <display:column title="Quantity"  style="width:18px" >
					<input type="text" id="estimateSellQuantity${accountLineList.id}" style="text-align:right" name="estimateSellQuantity${accountLineList.id}"  value="${accountLineList.estimateSellQuantity }" size="4" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloatQuantity(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','','vatAmt${accountLineList.id}','${accountLineList.id}','','','');changeVatAmt('REVENUE','${accountLineList.id}');changeVatAmt('EXPENSE','${accountLineList.id}');"/>
					<input type="hidden" id="oldEstimateSellQuantity${accountLineList.id}" name="oldEstimateSellQuantity${accountLineList.id}" value="${accountLineList.estimateSellQuantity }"/>
				</display:column>
				</c:if>																									
				<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
					<input type="text" style="text-align:right" id="estimateSellRate${accountLineList.id}" name="estimateSellRate${accountLineList.id}" value="${accountLineList.estimateSellRate }" size="5" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="return checkFloat(this)" onchange="clearDecimal();calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','','vatAmt${accountLineList.id}','${accountLineList.id}','estSellCurrencyNew${accountLineList.id}~estimateContractCurrencyNew${accountLineList.id}','estSellValueDateNew${accountLineList.id}~estimateContractValueDateNew${accountLineList.id}','estSellExchangeRateNew${accountLineList.id}~estimateContractExchangeRateNew${accountLineList.id}');changeVatAmt('REVENUE','${accountLineList.id}');"/>
				</display:column>
				<c:if test="${multiCurrency=='Y'}">
					<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
						<c:choose>
	                       <c:when test="${accountLineList.estSellCurrency!=baseCurrency}">			                           
	                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
	                       </c:when> 
	                       <c:otherwise>
	                         <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
	                       </c:otherwise>
	                    </c:choose> 
					</display:column>					
				</c:if>			
				<display:column title="Expense"  style="width:20px" >
					<input type="text" style="text-align:right" id="estimateExpense${accountLineList.id}" name="estimateExpense${accountLineList.id}" value="${accountLineList.estimateExpense }" readonly="readonly" size="6" class="input-textUpper"/>
				</display:column>
				<display:column title="Mk%"  style="width:10px" >
					<input type="text" style="text-align:right" id="estimatePassPercentage${accountLineList.id}" name="estimatePassPercentage${accountLineList.id}" value="${accountLineList.estimatePassPercentage }" maxlength="4"  size="2" class="input-text" onkeydown="return onlyNumsAllowedPersent(event)"  onchange="changeEstimatePassPercentage('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','','vatAmt${accountLineList.id}');calculateRevenueNew('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('REVENUE','${accountLineList.id}');changeVatAmt('EXPENSE','${accountLineList.id}');" />
				</display:column> 
				<display:column title="Revenue" style="width:20px" >
					<input type="text" style="text-align:right" id="estimateRevenueAmount${accountLineList.id}" name="estimateRevenueAmount${accountLineList.id}" value="${accountLineList.estimateRevenueAmount }" readonly="readonly" size="6" class="input-textUpper"/>
				</display:column>

				<c:if test="${systemDefaultVatCalculationNew=='Y'}">
					<display:column   title="Rec VAT Amt"  style="width:50px;">
					 <input type="text" name ="vatAmt${accountLineList.id}"  id ="vatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}" style="width:50px;text-align:right;" class="input-textUpper" readonly="true">
					</display:column>	
					<display:column   title="Pay VAT Amt"  style="width:50px;">
					<input type="text" name ="estExpVatAmt${accountLineList.id}"  id ="estExpVatAmt${accountLineList.id}" value="${accountLineList.estExpVatAmt}" style="width:50px;text-align:right;" class="input-textUpper" readonly="true"> 
					</display:column>																				
				</c:if>
				<display:column title="Description" style="width:100px" >
					<textarea  name="quoteDescription${accountLineList.id}" id="quoteDescription${accountLineList.id}" style="overflow:auto;" class="input-text" onselect="discriptionView('quoteDescription${accountLineList.id}')" onblur="discriptionView1('quoteDescription${accountLineList.id}')" value ="${accountLineList.quoteDescription }"  cols="15" rows="1">${accountLineList.quoteDescription }</textarea>
				</display:column>
				<display:column   title="Div" style="width:70px" > 	
				<c:if test="${accountLineList.category!='Internal Cost' && companyDivisionAcctgCodeUnique!='Y'}">
				<select name ="companyDivision${accountLineList.id}" id ="companyDivision${accountLineList.id}" style="width:70px" onchange="checkCompanyDiv('${accountLineList.id}');" class="list-menu"> 
				<c:forEach var="chrms" items="${companyDivis}" >
                              <c:choose>
                               <c:when test="${chrms == accountLineList.companyDivision}">
                               <c:set var="selectedInd" value=" selected"></c:set>
                               </c:when>
                              <c:otherwise>
                               <c:set var="selectedInd" value=""></c:set>
                              </c:otherwise>
                              </c:choose>
                             <option value="<c:out value='${chrms}' />" <c:out value='${selectedInd}' />>
                            <c:out value="${chrms}"></c:out>
                            </option>
                            </c:forEach> 
				</select>
				<s:hidden name="tempCompanyDivision${accountLineList.id}" id="tempCompanyDivision${accountLineList.id}"  value="${accountLineList.companyDivision}" />
				<s:hidden name="recAccDate${accountLineList.id}" id="recAccDate${accountLineList.id}"  value="${accountLineList.recAccDate}" />
				<s:hidden name="payAccDate${accountLineList.id}" id="payAccDate${accountLineList.id}"  value="${accountLineList.payAccDate}" />
				</c:if>
				<c:if test="${accountLineList.category=='Internal Cost' || companyDivisionAcctgCodeUnique=='Y'}">
				<input type="text" style="width:70px" name="companyDivision${accountLineList.id}" id="companyDivision${accountLineList.id}" value="${accountLineList.companyDivision}"  maxlength="3" class="input-textUpper"  readonly="true" />
				</c:if>
				 </display:column>
				<display:column style="width:55px;padding:0px;text-align:center;"
				 title="Display&nbsp;On Quote <br><input type='checkbox' name='selectall' id='selectall' style='vertical-align:middle;margin-left:30px;' onclick='selectAll(this);'/>All" media="html" >  
					<c:if test="${accountLineList.displayOnQuote}"> 
						<input type="checkbox" style="text-align:center;" name="displayOnQuote${accountLineList.id}" id="displayOnQuote${accountLineList.id}" checked="checked"  value="${accountLineList.status}" /> 
					</c:if>
					<c:if test="${accountLineList.displayOnQuote==false}">
						<input type="checkbox" style="text-align:center;" name="displayOnQuote${accountLineList.id}" id="displayOnQuote${accountLineList.id}" value="${accountLineList.status}" /> 
					</c:if>
				</display:column>
				 <configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
                     <display:column title="Roll Up In Invoice  <input type='checkbox' name='selectall' id='selectall' style='margin-left:20px; vertical-align:middle;' onclick='selectAllRollInvoice(this);'/>All" media="html" style="width:50px;padding:0px;text-align:center;">
                        <c:choose>
                           <c:when test="${accountLineList.rollUpInInvoice}">
                           <input type="checkbox" style="text-align:center;" name="rollUpInInvoice${accountLineList.id}" id="rollUpInInvoice${accountLineList.id}"  checked="checked"  value="${accountLineList.status}" />
                           </c:when>
                           <c:otherwise>
                           <input type="checkbox"  style="text-align:center;" name="rollUpInInvoice${accountLineList.id}" id="rollUpInInvoice${accountLineList.id}" value="${accountLineList.status}" />
                           </c:otherwise>
                         </c:choose>
                     </display:column>
                 </configByCorp:fieldVisibility>
				<configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">	
					<display:column title="Additional Services <input type='checkbox' style='margin-left:29px; vertical-align:middle;' name='selectall' onclick='selectAdd(this);'/>All" media="html"    style="width:50px;padding:0px;text-align:center;" > 
						<c:if test="${accountLineList.additionalService}"> 
							<input type="checkbox"  name="additionalService${accountLineList.id}"  id="additionalService${accountLineList.id}" checked="checked"  value="${accountLineList.status}" /> 
						</c:if>
						<c:if test="${accountLineList.additionalService==false}">
							<input type="checkbox"  name="additionalService${accountLineList.id}"  id="additionalService${accountLineList.id}"  value="${accountLineList.status}" /> 
						</c:if>
					</display:column>
				</configByCorp:fieldVisibility>
				
				<display:column style="width:65px;padding:0px;text-align:center;"  title="<font style='padding-left:30px;'>Act </font><br><input type='checkbox' style='margin-left:26px; vertical-align:middle;' checked='checked' id='selectallActive' name='selectallActive' onclick='selectAllAct(this);'/>All"  media="html">
					<c:if test="${accountLineList.status}"> 
						<input type="checkbox"  name="statusCheck${accountLineList.id}" id="statusCheck${accountLineList.id}" checked="checked"  value="${accountLineList.status}" onchange="inactiveStatusCheck(${accountLineList.id},this)"/> 
					</c:if>
				</display:column>	                            
		
				<display:footer>
					<tr> 
						<td align="right" colspan="11"><b><div align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></div></b></td>
						<c:if test="${systemDefaultVatCalculationNew=='Y'}">
							<td align="left" colspan="2" ></td>
						</c:if> 						
						<c:if test="${multiCurrency=='Y'}">
							<td align="left" ></td>
						</c:if>
						 <c:if test="${contractType}">
		  	                    <td></td>
		  	             </c:if>	
						<td align="left" width="40px">
							<input type="text" style="text-align:right" id="estimatedTotalExpense${serviceOrder.id}" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper"/>
				 		</td>
				 		<td align="left"> </td>
						<td align="left" width="70px">
							<input type="text" style="text-align:right" id="estimatedTotalRevenue${serviceOrder.id}" name="estimatedTotalRevenue${serviceOrder.id}" value="${serviceOrder.estimatedTotalRevenue}" readonly="readonly" size="6" class="input-textUpper"/>
						</td>
						 <td></td>
						<c:if test="${systemDefaultVatCalculationNew=='Y'}">
							<td align="left" colspan="2" ></td>
						</c:if> 
						<td></td>
						<configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
							<td></td>	
						</configByCorp:fieldVisibility>
						<configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
							<td></td>	
						</configByCorp:fieldVisibility>
						<td colspan="2" align="right"><input type="button" id="Inactivate" name="Inactivate" class="cssbuttonA" style="width:60px; height:25px; margin-top:-4px;" value="Inactivate" onclick="findAllPricingLineId('Inactive');"/></td>
				
					</tr>
					<tr>
				 		<td align="right" colspan="11"><b><div align="right">Gross&nbsp;Margin</div></b></td>
				 		<c:if test="${systemDefaultVatCalculationNew=='Y'}">
							<td align="left" colspan="2" ></td>
						</c:if> 
				 		
						<c:if test="${multiCurrency=='Y'}">
							<td align="left" ></td>
						</c:if>
						 <c:if test="${contractType}">
		  	                    <td></td>
		  	             </c:if>						
				 		<td align="left" width="40px"></td>
				 		<td align="left" ></td>
				 		
				 		<td align="left" width="70px" >
							<input type="text" style="text-align:right" id="estimatedGrossMargin${serviceOrder.id}" name="estimatedGrossMargin${serviceOrder.id}" value="${serviceOrder.estimatedGrossMargin}" readonly="readonly" size="6" class="input-textUpper"/>
						</td>
						<c:if test="${systemDefaultVatCalculationNew=='Y'}">
							<td align="left" colspan="2" ></td>
						</c:if>  
						<td colspan="4"></td>
						<configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
							<td></td>	
						</configByCorp:fieldVisibility>
						<configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
							<td></td>	
						</configByCorp:fieldVisibility>
				 	</tr>
				 	
				 	<tr>
				 		<td align="right" colspan="11"><b><div align="right">Gross&nbsp;Margin%</div></b></td>
				 		<c:if test="${systemDefaultVatCalculationNew=='Y'}">
							<td align="left" colspan="2" ></td>
						</c:if> 
				 		
						<c:if test="${multiCurrency=='Y'}">
							<td align="left" ></td>
						</c:if>
						 <c:if test="${contractType}">
		  	                    <td></td>
		  	             </c:if>
				 		<td align="left" width="40px"></td>
				 		<td align="left" ></td>
				 	
				 		<td align="left" width="40px">
							<input type="text" style="text-align:right" id="estimatedGrossMarginPercentage${serviceOrder.id}" name="estimatedGrossMarginPercentage${serviceOrder.id}" value="${serviceOrder.estimatedGrossMarginPercentage}" readonly="readonly" size="6" class="input-textUpper"/>
						</td>
						
						<c:if test="${systemDefaultVatCalculationNew=='Y'}">
							<td align="left" colspan="2" ></td>
						</c:if> 
						<td colspan="4"></td>
						<configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
							<td></td>	
						</configByCorp:fieldVisibility>
				 		<configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
							<td></td>	
						</configByCorp:fieldVisibility>
					</tr>
				
				</display:footer>
			</display:table>
		</td>
	</tr>
</table>
</div>
<table cellpadding="0" cellspacing="0"  class="detailTabLabel" border="0" style="margin:5px;padding:0px;" >
	<tr>
	  	<td>
			<input type="button" name="pricingAddLine" value="Add Line" onclick="findAllPricingLineId('AddLine');" class="cssbuttonA" style="width:65px; height:25px" tabindex=""/> 
			
			<c:if test="${not empty accountLineList}">
				<input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" onclick="findAllPricingLineId('');" tabindex=""/>
			</c:if>
			<c:if test="${empty accountLineList}">
				<input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" onclick="" disabled="disabled" tabindex=""/>
			</c:if>
			 
            <input type="button" class="cssbuttonA" style="width:140px; height:25px" onclick="addDefaultPricingLineAjax('${serviceOrder.defaultAccountLineStatus}','AddTemplate');" 	value="Add Default Template" tabindex="" />
                 <c:if test="${serviceOrder.corpID!='CWMS'}">
            <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}"> 
           		<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
               	 	<input type="button" class="cssbutton1" onclick="createPrice()" value="Create Pricing" style="width:120px; height:25px; margin-left:5px;margin-bottom:5px;" tabindex=""/>
                </configByCorp:fieldVisibility>
            </c:if>
            </c:if>
            <a><img align="middle" title="Forms" style="vertical-align:top;" onclick="findQuotationForms(this);" src="${pageContext.request.contextPath}/images/invoiceList.png"/></a>
		</td>
	</tr> 	
</table>
