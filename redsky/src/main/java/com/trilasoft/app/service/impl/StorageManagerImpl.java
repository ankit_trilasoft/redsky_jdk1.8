
package com.trilasoft.app.service.impl;

import com.trilasoft.app.dao.StorageDao;
import com.trilasoft.app.model.Storage;   
import com.trilasoft.app.service.StorageManager;
import org.appfuse.service.impl.GenericManagerImpl;      
import java.util.List;   
  
public class StorageManagerImpl extends GenericManagerImpl<Storage, Long> implements StorageManager {   
	StorageDao storageDao;   
  
    public StorageManagerImpl(StorageDao storageDao) {   
        super(storageDao);   
        this.storageDao = storageDao;   
    } 
    public List findMaximum() {
    	return storageDao.findMaximum();
    }
  
    public List<Storage> findByMeasQuantity(Double measQuantity) {   
        return storageDao.findByMeasQuantity(measQuantity);   
    }
    public List<Storage> findByLocation(String locationId) {   
        return storageDao.findByLocation(locationId);   
    }
    public List<Storage> findByItemNumber(String itemNumber) {   
        return storageDao.findByItemNumber(itemNumber);
    }
    public List<Storage> findByItemNumber(String itemNumber, String jobNumber, String locationId, Long ticket) {   
        return storageDao.findByItemNumber(itemNumber,jobNumber, locationId, ticket);   
    }
	
    public List getByIdNum(Long idNum){
    	return storageDao.getByIdNum(idNum);   
    }
    
    public List getTraceStorageList(){
    	return storageDao.getTraceStorageList();   	
    }
    
    public List<Storage> search(String itemNumber, String jobNumber, String locationId, String storageId, Long ticket, String firstName, String lastName, String tag, String sessionCorpID){
    	return storageDao.search(itemNumber, jobNumber, locationId, storageId, ticket, firstName, lastName, tag, sessionCorpID);
    }
	
    public List findStorageByShipNumber(String shipNumber){
    	return storageDao.findStorageByShipNumber(shipNumber);
    }
	public List storageExtract(String corpId,String jobSTLFlag ,String salesPortalStorageReport) {
		return storageDao.storageExtract(corpId,jobSTLFlag ,salesPortalStorageReport);
	}
	public List findLocationByStorage(String storageId, String sessionCorpID) {
		return storageDao.findLocationByStorage(storageId, sessionCorpID);
	}
	public List<Storage> findByLocationRearrange(String storageId,String locationId){
		return storageDao.findByLocationRearrange(storageId,locationId);
	}
	public List storageLibraryVolumeList(String storageId) {
		return storageDao.storageLibraryVolumeList(storageId);
	}
	public List dummyStorageList(String storageId,String corpId){
		return storageDao.dummyStorageList(storageId, corpId);
	}
	public void deleteDummyUsedRecord(String storageId, String sessionCorpID){
		storageDao.deleteDummyUsedRecord(storageId, sessionCorpID);
	}
	public List handOutList(String ticket, String sessionCorpID){
		return storageDao.handOutList(ticket, sessionCorpID);
	}
	public void updateStorageForLocation(String sessionCorpID,String storageId,String locationId){
		storageDao.updateStorageForLocation(sessionCorpID, storageId, locationId);
		
	}
	public List getRecordsForBookSto(String sessionCorpID, String storageId,Long ticket){
		return storageDao.getRecordsForBookSto(sessionCorpID, storageId, ticket);
		
	}
	public List getLocationId(String storageId){
		return storageDao.getLocationId(storageId);
		
	}
	public List<Storage> findByLocationInternalRearrange(String storageId,String locationId, Long ticket, String mode){
		
		return storageDao.findByLocationInternalRearrange(storageId,locationId,ticket, mode);
	}
	
	public List<Storage> findStoragesByLocation(String locationId){
		return storageDao.findStoragesByLocation(locationId);
	}
}

