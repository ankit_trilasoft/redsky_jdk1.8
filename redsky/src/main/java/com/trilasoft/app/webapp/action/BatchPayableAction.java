package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DspDetailsManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ExchangeRateManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.PartnerAccountRefManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.impl.DspDetailsManagerImpl;

import org.acegisecurity.Authentication;

public class BatchPayableAction extends BaseAction implements Preparable{
	private String sessionCorpID;
	private Map country;
	private Map countryCorpid;
	public String getBillingCountry() {
		return billingCountry;
	}
	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}
	public String getExtReference() {
		return extReference;
	}
	public void setExtReference(String extReference) {
		this.extReference = extReference;
	}
	public String getBillingCountryCode() {
		return billingCountryCode;
	}
	public void setBillingCountryCode(String billingCountryCode) {
		this.billingCountryCode = billingCountryCode;
	}
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	private RefMasterManager refMasterManager;
	private String vendorCode;
	private String vendorInvoice;
	private String currency;
	private String exchangeRate;
	private String amount;
	private Date invoiceDate;
	private Date recievedDate;
	private Set partners;
	private PartnerManager partnerManager;
	private Company company; 
	private CompanyManager companyManager;
	private ChargesManager chargesManager;
	private List previewAccountLines;
	private AccountLineManager accountLineManager;
	private  Map<String, String> category;
	private  Map<String,String>payingStatus;
	private String soNameAndContract;
	private String sequenceNumber;
	private String invoiceNumberListServer;
	private String actualExpenseListServer;
	private String payingStatusListServer;
	private String  payVatDescrListServer ="";
	private String  payVatPerListServer ="";
	private String  payVatAmtListServer ="";
	private String  actgCodeListServer;
	private String accountlinelist;
	private String unchaccountlinelist;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private Billing billing;
	private BillingManager billingManager;
	private AccountLine accountLine;
	private  Map<String,String>basis;
	private String payPostingFlag="";
	private String accountInterface;
	private List accountVedorInvoiceList;
	private String payAccDateTo;
	private Date payAccDateToFormat;
	private String invoiceCheck;
	private String payAccDateForm;
	private String InvoicePayPostDate;
	private String hitflag="";
	private  List findCompanyDivisionByCorpId;
	private PartnerAccountRefManager partnerAccountRefManager;
	private String companyDivisionAcctgCodeUnique;
	private String companyDivision;
	private String actgCode;	
	private  ExchangeRateManager exchangeRateManager;
	private String fieldName;
	private String sortType;
	private String shipNo;
	private String systemDefaultVatCalculation;
	private  Map<String,String>payVatList;
	private  Map<String,String>payVatPercentList;
	private  Map<String,String>euVatPercentList;
	private boolean postingDateStop=false;
	private Boolean costElementFlag;
	private CustomerFileManager customerFileManager;
	private String bookCode;
	private String usertype;
	private String vatAmount;
	private List companyDivis=new ArrayList();
	private boolean accountLineAccountPortalFlag=false;
	private List<SystemDefault> sysDefaultDetail;
	private boolean allowAgentInvoiceUpload;
	
	private  Map<String,String> vendorCodeVatMap;
	public void setExchangeRateManager(ExchangeRateManager exchangeRateManager) {
		this.exchangeRateManager = exchangeRateManager;
	}
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(BatchPayableAction.class);

	

	public BatchPayableAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        usertype=user.getUserType();
	}
	
	public List soCorpid; 
	public List corpIDS;
	@SkipValidation
	 public String payableProcessingpage(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		companyDivis = customerFileManager.findCompanyDivision(sessionCorpID);
		 country=refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");		 
		 if(currency==null){
		 currency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();		 
		 }
		 List eRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,currency);
		 if((eRate!=null)&&(!eRate.isEmpty()))
		 {
		 exchangeRate=eRate.toString().replace("[", "").replace("]","");
		 }else{
			 exchangeRate="1"; 
		 } 
		 if(sessionCorpID.equalsIgnoreCase("TSFT"))
		 {
		 soCorpid=accountLineManager.getAccountLinesCorpId();
		 }
		 corpIDS=accountLineManager.getAllCorpId();
		 openAgentBillToCode();
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		 return SUCCESS;
	 }
	
	@SkipValidation	
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax"));
		countryCorpid = refMasterManager.findCodeOnleByParameterCountry(sessionCorpID, "CURRENCY");
        if(getRequest().getParameter("ajax") == null){ 
			accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString();	
			findCompanyDivisionByCorpId=partnerAccountRefManager.findCompanyDivisionByCorpId(sessionCorpID);
			 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
			 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
				 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
			 }
			 systemDefaultVatCalculation =accountLineManager.findVatCalculation(sessionCorpID).get(0).toString();
			 payVatList = refMasterManager.findByParameterWithoutParent (sessionCorpID, "PAYVATDESC");
			 payVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "PAYVATDESC");
			 company=companyManager.findByCorpID(sessionCorpID).get(0);
			 if(company.getPostingDateStop()!=null){
				 postingDateStop=company.getPostingDateStop();
				 if(company.getAllowAgentInvoiceUpload()!=null && company.getAllowAgentInvoiceUpload()){
					 allowAgentInvoiceUpload = company.getAllowAgentInvoiceUpload();
				}else{
					allowAgentInvoiceUpload = false;
				}
			}
			costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
			sysDefaultDetail = serviceOrderManager.findSysDefault(sessionCorpID);
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getAccountLineAccountPortalFlag() !=null ){
					accountLineAccountPortalFlag = 	systemDefault.getAccountLineAccountPortalFlag();
				}	
			}
		}
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	private String htFlag;
	public String getHtFlag() {
		return htFlag;
	}
	public void setHtFlag(String htFlag) {
		this.htFlag = htFlag;
	}
	private String vendorDescription;
	public String getVendorDescription() {
		return vendorDescription;
	}
	public void setVendorDescription(String vendorDescription) {
		this.vendorDescription = vendorDescription;
	}
	private String cDivision;
	@SkipValidation
	 public String previewInvoicelines(){
		companyDivis = customerFileManager.findCompanyDivision(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String fieldNameSort="";
		if(!fieldName.trim().equalsIgnoreCase(""))
		 {
			 if(!sortType.equalsIgnoreCase(""))
			 {
				 fieldNameSort = fieldName+" "+sortType;
			 }else{
				 fieldNameSort = fieldName+" asc"; 
			 }
		 }
		 previewAccountLines=accountLineManager.getAccountLineForBatchInvoicing(sessionCorpID,vendorCode,vendorInvoice,accountInterface,currency,fieldNameSort,shipNo,cDivision);
		 category = refMasterManager.findByParameter(sessionCorpID, "ACC_CATEGORY");
		 payingStatus=refMasterManager.findByParameter(sessionCorpID, "ACC_STATUS");
		 basis=refMasterManager.findByParameter(sessionCorpID, "ACC_BASIS");
		 findCompanyDivisionByCorpId=partnerAccountRefManager.findCompanyDivisionByCorpId(sessionCorpID);
		 country=refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");		 
		 accountlinelist="";
		 payingStatusListServer="";
		 actualExpenseListServer="";
		 invoiceNumberListServer="";
		 payVatDescrListServer ="";
		 payVatPerListServer ="";
		 payVatAmtListServer ="";
		 actgCodeListServer ="";
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		 return SUCCESS;
	}	
	private List<AccountLine> acc;
	@SkipValidation
	 public String previewAgentInvoicelines(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String fieldNameSort="";
		if(!fieldName.trim().equalsIgnoreCase(""))
		 {
			 if(!sortType.equalsIgnoreCase(""))
			 {
				 fieldNameSort = fieldName+" "+sortType;
			 }else{ 
				 fieldNameSort = fieldName+" asc"; 
			 }
		 }
		 //previewAccountLines=accountLineManager.getAccountLineForBatchInvoicing(sessionCorpID,vendorCode,vendorInvoice,accountInterface,currency,fieldNameSort,shipNo,cDivision);
		 previewAccountLines=accountLineManager.getAgentLineForBatchInvoicing(sessionCorpID,vendorCode,vendorInvoice,accountInterface,currency,fieldNameSort,shipNo.replace("-",""));

		 payingStatus=refMasterManager.findByParameter(sessionCorpID, "ACC_STATUS"); 
		 country=refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
		 countryCorpid = refMasterManager.findCodeOnleByParameterCountry(sessionCorpID, "CURRENCY");
		 accountlinelist="";
		 payingStatusListServer="";
		 actualExpenseListServer="";
		 invoiceNumberListServer="";
		 payVatDescrListServer ="";
		 payVatPerListServer ="";
		 payVatAmtListServer ="";
		 actgCodeListServer ="";
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		 return SUCCESS;
	}	
	private String defaultVatVendorCode;

	@SkipValidation
	 public String soInfoForBatchPAyProcessing(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		soNameAndContract=accountLineManager.getSOInfoFromSequenceNumber(sessionCorpID,sequenceNumber);	
		if(!soNameAndContract.equalsIgnoreCase("")){
			vendorCodeVatMap=accountLineManager.getVendorVatCodeMap(sessionCorpID, sequenceNumber);
			String vatCode=vendorCodeVatMap.get(defaultVatVendorCode);			
			payVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "PAYVATDESC");
			if((vatCode!=null)&&(!vatCode.equalsIgnoreCase(""))){
				soNameAndContract=soNameAndContract+"#"+vatCode;
				vatCode=payVatPercentList.get(vatCode);
				soNameAndContract=soNameAndContract+"#"+vatCode;
			}else{
				soNameAndContract=soNameAndContract+"# #0.00";
			}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String payablePosting(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString();
		accountVedorInvoiceList =accountLineManager.getListForBatchPayablePosting(sessionCorpID,vendorCode,vendorInvoice,accountInterface,currency,shipNo,cDivision);
		try{
	    	List payableDate =serviceOrderManager.findByPaySysDefault(sessionCorpID);
	    	payAccDateTo =new String("");
	    	if(!payableDate.isEmpty());
	    	{
	    		payAccDateTo=(serviceOrderManager.findByPaySysDefault(sessionCorpID).get(0).toString());
			    		try{
					    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
						    		Date dt1=new Date();
						    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						    		String dt11=sdfDestination.format(dt1);
						    		dt1=sdfDestination.parse(dt11);
						    		Date dt2=sdfDestination.parse(payAccDateTo);
						    		if(dt1.compareTo(dt2)>0){
						    			payAccDateTo=sdfDestination.format(dt2);
						    		}else{
						    			payAccDateTo=sdfDestination.format(dt1);
						    		}
					    		}
			    		   }catch(Exception e){e.printStackTrace();}	    		
	    	} 
	    	SimpleDateFormat psdf = new SimpleDateFormat("yyyy-MM-dd");
			 
			if(!payAccDateTo.equals("")){	
				payAccDateToFormat=psdf.parse(payAccDateTo);  
				}
			else{
				 payAccDateToFormat =null; 
				}
			}
			catch( Exception e)
			{
				e.printStackTrace();
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
	}
	
	@SkipValidation
	public String savePayPostDate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString();
		String loginUser = getRequest().getRemoteUser();
		if(invoiceCheck.equals("")||invoiceCheck.equals("`"))
  	  {
  	  }
  	 else
  	  {
  		 invoiceCheck=invoiceCheck.trim();
  	    if(invoiceCheck.indexOf("`")==0)
  		 {
  	    	invoiceCheck=invoiceCheck.substring(1);
  		 }
  		if(invoiceCheck.lastIndexOf("`")==invoiceCheck.length()-1)
  		 {
  			invoiceCheck=invoiceCheck.substring(0, invoiceCheck.length()-1);
  		 }
  	    String[] arrayInvoice=invoiceCheck.split("`");
  	    int arrayLength = arrayInvoice.length;
  		for(int i=0;i<arrayLength;i++)
  		 {	
  		   String vendorInvoiceTemp=arrayInvoice[i]; 
  		   String[] arrayVendorInvoice= vendorInvoiceTemp.split("~");
  		   String soNumber=arrayVendorInvoice[0];
  		   String VendorCode=arrayVendorInvoice[1];
  		   String invoiceNumber=vendorInvoice;
  		   if(!arrayVendorInvoice[2].equalsIgnoreCase("NO"))
  		   {
  			 invoiceNumber=arrayVendorInvoice[2];
  		   }
  		   
  	       try
             {
			      SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
                Date du = new Date();
                du = df.parse(payAccDateForm);
                df = new SimpleDateFormat("yyyy-MM-dd");
                InvoicePayPostDate = df.format(du); 
             } 
		      catch (ParseException e)
            {
              e.printStackTrace();
            }
		   accountLineManager.batchPayPostDate(soNumber,VendorCode,invoiceNumber,InvoicePayPostDate,loginUser,currency,accountInterface,sessionCorpID);   
  		 }
  	  }
		hitflag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private CompanyDivisionManager companyDivisionManager;
	public void setCompanyDivisionManager(
			CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}
	@SkipValidation
	 public String savePayProcessing(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
//		accountLineManager.saveAccountLineFromBatchPayable(sessionCorpID,invoiceNumberListServer,actualExpenseListServer,payingStatusListServer,invoiceDate,recievedDate,exchangeRate);
		euVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "EUVAT");
		accountLineManager.saveAccountLineBatchPayableForInvoiceNoDate(sessionCorpID,invoiceNumberListServer,actualExpenseListServer,payingStatusListServer,invoiceDate,recievedDate,exchangeRate,vendorInvoice,payVatDescrListServer,payVatPerListServer,payVatAmtListServer,actgCodeListServer);
		List maxLineNumber = new ArrayList();
		String accountLineNumber = new String();
		Long autoLineNumber;
		String[] str= new String[25];		
		String[] str2= new String[25];
		Double estExp;
		Double revExp;
		if(!accountlinelist.trim().equals("")){
		str=accountlinelist.split("#");
		for (String string : str) {
			    str2=string.split(":");
				List list= serviceOrderManager.findIdByShipNumber(str2[0].trim());
				if(!list.isEmpty() && list!=null && list.get(0)!=null){
					serviceOrder=serviceOrderManager.get(new Long(list.get(0).toString().trim()));
					billing=  billingManager.get(new Long(list.get(0).toString().trim()));
				}else{
					serviceOrder=new ServiceOrder();
				}
				    accountLine = new AccountLine();
				    boolean activateAccPortal =true;
		    		try{
		    		if(accountLineAccountPortalFlag){		
		    		activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
		    		accountLine.setActivateAccPortal(activateAccPortal);
		    		}
		    		}catch(Exception e){
		    			e.printStackTrace();
		    		}
				    accountLine.setCategory(str2[1].trim());
				    accountLine.setChargeCode(str2[2].trim());
					try {
						String ss=chargesManager.findPopulateCostElementData(str2[2].trim(), billing.getContract(),sessionCorpID);
						if(!ss.equalsIgnoreCase("")){
							accountLine.setAccountLineCostElement(ss.split("~")[0]);
							accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				    
				    if(!str2[6].toString().trim().equals("")){
				    accountLine.setLocalAmount(new BigDecimal(str2[6].toString().trim()));
				    }else{
				    	accountLine.setLocalAmount(new BigDecimal(0));
				    }
				    if(!str2[12].trim().equals("")){
			    		String actCode="";
			    		if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
			    			actCode=partnerManager.getAccountCrossReference(vendorCode,str2[12].trim(),sessionCorpID);
			    		}else{
			    			actCode=partnerManager.getAccountCrossReferenceUnique(vendorCode,sessionCorpID);
			    		}
			    		if(!actCode.equalsIgnoreCase(""))
			    		{
			    			accountLine.setUpdatedOn(new Date());
							accountLine.setUpdatedBy(getRequest().getRemoteUser()); 
			    			accountLine.setPayingStatus("A");
			    		}
			    		else
			    		{
			    			accountLine.setUpdatedOn(new Date());
							accountLine.setUpdatedBy(getRequest().getRemoteUser()); 
			    			accountLine.setPayingStatus(str2[7].trim());		
			    		}
			    	}else{	
			    		accountLine.setUpdatedOn(new Date());
						accountLine.setUpdatedBy(getRequest().getRemoteUser()); 
			    		accountLine.setPayingStatus(str2[7].trim());
			    	}
				    if(!costElementFlag){
				    	accountLine.setPayGl(str2[8].trim());
				    	accountLine.setRecGl(str2[9].trim());
				    }else{
				    	String chargeStr="";
						List glList = accountLineManager.findChargeDetailFromSO(billing.getContract(),str2[2].trim(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
						if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
							  chargeStr= glList.get(0).toString();
						  }
						   if(!chargeStr.equalsIgnoreCase("")){
							  String [] chrageDetailArr = chargeStr.split("#");
							  accountLine.setRecGl(chrageDetailArr[1]);
							  accountLine.setPayGl(chrageDetailArr[2]);
							}else{
							  accountLine.setRecGl("");
							  accountLine.setPayGl("");
						  }
				    }
				    try{
				    List chargeid=chargesManager.findChargeId(str2[2].trim(),billing.getContract());
					Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
					accountLine.setVATExclude(charge.getVATExclude());
					if(charge.getPrintOnInvoice()!=null && charge.getPrintOnInvoice()){
						accountLine.setIgnoreForBilling(true);
					}else{
						accountLine.setIgnoreForBilling(false);	
					}
					accountLine.setContract(billing.getContract());
					accountLine.setBillToCode(billing.getBillToCode()); 
		    		accountLine.setBillToName(billing.getBillToName());
		    		accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
		    		accountLine.setNetworkBillToName(billing.getNetworkBillToName());
					if(!(accountLine.getVATExclude())){
					if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
			    		accountLine.setRecVatDescr(billing.getPrimaryVatCode());
			    		if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
				    		String recVatPercent="0";
				    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
					    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
					    		}
		    				accountLine.setRecVatPercent(recVatPercent);
				    		}
			    	}
					}
				    }catch(Exception e){
				    	e.printStackTrace();
				    }
				    boolean contractType=false;
		     		if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
		     		String contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
		     			if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
		     				contractType=true;	
		     			}
		     		}
				    accountLine.setBasis(str2[10].trim());
				    accountLine.setNote(str2[11].trim());
				    if(!str2[3].trim().equals("")){
				        estExp=new Double(str2[3]);
				       accountLine.setEstimateExpense(new BigDecimal(estExp));
				    }else{
				    	estExp=new Double(0.00);
				    	accountLine.setEstimateExpense(new BigDecimal(estExp));
				    }
				    if(!str2[4].trim().equals("")){
				    	revExp=new Double(str2[4]);
				    	accountLine.setRevisionExpense(new BigDecimal(revExp));
				    }else{
				    	revExp=new Double(0.00);
				    	accountLine.setRevisionExpense(new BigDecimal(revExp));
				    }
				    if(accountLine.getBasis().equals("%age") || accountLine.getBasis().equals("cwt")){
				    	accountLine.setEstimateQuantity(new BigDecimal(1));
				    	accountLine.setRevisionQuantity(new BigDecimal(1));
				    	accountLine.setEstimateRate(new BigDecimal(estExp*100));	
				    	accountLine.setRevisionRate(new BigDecimal(revExp*100));
				    	if(contractType){
				    	accountLine.setEstimateSellQuantity(new BigDecimal(1));	
				    	accountLine.setRevisionSellQuantity(new BigDecimal(1));
				    	}
				    }else if (accountLine.getBasis().equals("per 1000")) {
				    	accountLine.setEstimateQuantity(new BigDecimal(1));
				    	accountLine.setEstimateRate(new BigDecimal(estExp*1000));
				    	accountLine.setRevisionQuantity(new BigDecimal(1));	
				    	accountLine.setRevisionRate(new BigDecimal(revExp*1000));
				    	if(contractType){
					    	accountLine.setEstimateSellQuantity(new BigDecimal(1));
					    	accountLine.setRevisionSellQuantity(new BigDecimal(1));	
					    	}
					}else{
						accountLine.setEstimateQuantity(new BigDecimal(1));
				    	accountLine.setEstimateRate(new BigDecimal(estExp));
				    	accountLine.setRevisionQuantity(new BigDecimal(1));	
				    	accountLine.setRevisionRate(new BigDecimal(revExp));
				    	if(contractType){
					    	accountLine.setEstimateSellQuantity(new BigDecimal(1));	
					    	accountLine.setRevisionSellQuantity(new BigDecimal(1));	
					    	}
					}
				    
				    accountLine.setCorpID(sessionCorpID);
					accountLine.setStatus(true);
					accountLine.setServiceOrder(serviceOrder);
					accountLine.setServiceOrderId(serviceOrder.getId());
					accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
					accountLine.setShipNumber(serviceOrder.getShipNumber());
					accountLine.setContract(billing.getContract());
					accountLine.setCreatedOn(new Date());
					accountLine.setCreatedBy(getRequest().getRemoteUser());
					accountLine.setUpdatedOn(new Date());
					accountLine.setUpdatedBy(getRequest().getRemoteUser()); 	
					String actCode="";
					if(!str2[12].trim().equals("")){ 
			    		if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
			    			actCode=partnerManager.getAccountCrossReference(vendorCode,str2[12].trim(),sessionCorpID);
			    		}else{
			    			actCode=partnerManager.getAccountCrossReferenceUnique(vendorCode,sessionCorpID);
			    		}}
					accountLine.setActgCode(actCode);
					accountLine.setCompanyDivision(str2[12].trim());
					accountLine.setCountry(str2[14].trim());
					if(systemDefaultVatCalculation.equalsIgnoreCase("true")){
						try{
						accountLine.setPayVatDescr(str2[15].trim());
						accountLine.setPayVatPercent(str2[16].trim());
						accountLine.setPayVatAmt(new BigDecimal(str2[17].trim()));
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
		             if ( maxLineNumber.get(0) == null ) {          
		             	accountLineNumber = "001";
		             }else {
		             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
		                 if((autoLineNumber.toString()).length() == 2) {
		                 	accountLineNumber = "0"+(autoLineNumber.toString());
		                 }
		                 else if((autoLineNumber.toString()).length() == 1) {
		                 	accountLineNumber = "00"+(autoLineNumber.toString());
		                 } 
		                 else {
		                 	accountLineNumber=autoLineNumber.toString();
		                 }
		             }
		            
		             accountLine.setVendorCode(vendorCode);
		             List l=accountLineManager.vendorName(vendorCode, sessionCorpID, "",false);
		             if(!l.isEmpty() && l!=null && l.get(0)!=null){
		            	 Object[] obj=(Object[]) l.get(0);
		            	  if(obj[0]!=null){
		            	    accountLine.setEstimateVendorName(obj[0].toString());
		            	  }
		             }
		             accountLine.setAccountLineNumber(accountLineNumber);
		             
		            // #7562 - Batch Payable Posting - Extensions
		             accountLine.setEstCurrency(currency);
		             accountLine.setRevisionCurrency(currency);
		             accountLine.setCountry(currency);
		             try{
							if(!str2[13].trim().equalsIgnoreCase("")){
								accountLine.setEstExchangeRate(new BigDecimal(str2[13].trim()));
								accountLine.setRevisionExchangeRate(new BigDecimal(str2[13].trim()));
								accountLine.setExchangeRate(Double.parseDouble(str2[13].trim()));
							}else{
								accountLine.setEstExchangeRate(new BigDecimal("1.00"));
								accountLine.setRevisionExchangeRate(new BigDecimal("1.00"));
								accountLine.setExchangeRate(1.00);
							}
			          }catch (Exception e) {
			            	 accountLine.setEstExchangeRate(new BigDecimal("1.00"));
			            	 accountLine.setRevisionExchangeRate(new BigDecimal("1.00"));
			            	 accountLine.setExchangeRate(1.00);
					   }
			             accountLine.setEstValueDate(new Date());	
			             accountLine.setRevisionValueDate(new Date());	
			             accountLine.setValueDate(new Date());	
			             
			             if(accountLine.getExchangeRate()==1.00){
			            	 accountLine.setActualExpense(accountLine.getLocalAmount());
			             }else{
			            	 Double  D1= accountLine.getLocalAmount().doubleValue();
			            	 Double D2=D1/accountLine.getExchangeRate();
			            	 accountLine.setActualExpense(new BigDecimal(D2));
			             }			             
		            	
			     	String billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getBillToCode(),sessionCorpID);
			     	String multiCurrency=accountLineManager.findmultiCurrency(sessionCorpID).get(0).toString();
			 		String systemDefaultCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
					String companyDivisionCurrency=companyDivisionManager.searchBaseCurrencyCompanyDivision(accountLine.getCompanyDivision(),sessionCorpID);
			     	 
			     	if(multiCurrency.equalsIgnoreCase("Y")){
			     		String billingCurrencyTemp="";
			     		if(contractType){
			     			if(!billingCurrency.equalsIgnoreCase("")){
			     				billingCurrencyTemp=billingCurrency;
			     			}else if(!companyDivisionCurrency.equalsIgnoreCase("")){
			     				billingCurrencyTemp=companyDivisionCurrency;
			     			}else{
			     				billingCurrencyTemp=systemDefaultCurrency;
			     			}
			     		}else{
			     			if(!companyDivisionCurrency.equalsIgnoreCase("")){
			     				billingCurrencyTemp=companyDivisionCurrency;
			     			}else{
			     				billingCurrencyTemp=systemDefaultCurrency;
			     			}
			     		}
			             accountLine.setEstSellCurrency(billingCurrencyTemp);
			             accountLine.setRevisionSellCurrency(billingCurrencyTemp);
			             accountLine.setRecRateCurrency(billingCurrencyTemp);
			             try{
			            	 List exchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,billingCurrencyTemp);
			            	 if(exchangeRateList != null && (!(exchangeRateList.isEmpty())) && exchangeRateList.get(0)!= null && (!(exchangeRateList.get(0).toString().equals("")))){
			     				accountLine.setRecRateExchange(new BigDecimal(exchangeRateList.get(0).toString()));	
			    				accountLine.setEstSellExchangeRate(new BigDecimal(exchangeRateList.get(0).toString()));
			    				accountLine.setRevisionSellExchangeRate(new BigDecimal(exchangeRateList.get(0).toString()));
			            	 }else{
			     				accountLine.setRecRateExchange(new BigDecimal(1.0000));
			    				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
			    				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
			            	 }
				          }catch (Exception e) {
								accountLine.setRecRateExchange(new BigDecimal(1.0000));
								accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
								accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
						  }
				             accountLine.setEstSellValueDate(new Date());	
				             accountLine.setRevisionSellValueDate(new Date());	
				             accountLine.setRacValueDate(new Date());			     		
			     	}
			     		if(contractType){
			     			List quotationDiscriptionList=null;
			  			  if(!costElementFlag){
								quotationDiscriptionList=	accountLineManager.findQuotationDiscription(billing.getContract(),accountLine.getChargeCode(),sessionCorpID);
						  }else{
								quotationDiscriptionList = accountLineManager.findChargeDetailFromSO(billing.getContract(),accountLine.getChargeCode(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
						  }	
			  			String payableContactCurrency=""  ;
			  			String contactCurrency = "";
			  			String chargeStr = "";
						  if(quotationDiscriptionList!=null && !quotationDiscriptionList.isEmpty() && quotationDiscriptionList.get(0)!=null && !quotationDiscriptionList.get(0).toString().equalsIgnoreCase("NoDiscription")){
							  chargeStr= quotationDiscriptionList.get(0).toString();
						  }
						   if(!chargeStr.equalsIgnoreCase("")){
							  String [] chrageDetailArr = chargeStr.split("#");
							  contactCurrency = chrageDetailArr[3];
							  try{
							  payableContactCurrency = chrageDetailArr[4];
							  }catch(Exception e){
								  payableContactCurrency="";
								  e.printStackTrace();
							  }
						  }	
						   accountLine.setEstimatePayableContractCurrency(payableContactCurrency);
						   accountLine.setRevisionPayableContractCurrency(payableContactCurrency);
						   accountLine.setPayableContractCurrency(payableContactCurrency);
				             try{
				            	 List exchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,payableContactCurrency);
				            	 if(exchangeRateList != null && (!(exchangeRateList.isEmpty())) && exchangeRateList.get(0)!= null && (!(exchangeRateList.get(0).toString().equals("")))){
				     				accountLine.setPayableContractExchangeRate(new BigDecimal(exchangeRateList.get(0).toString()));	
				    				accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(exchangeRateList.get(0).toString()));
				    				accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(exchangeRateList.get(0).toString()));
				            	 }else{
				     				accountLine.setPayableContractExchangeRate(new BigDecimal(1.0000));	
				    				accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(1.0000));
				    				accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(1.0000));
				            	 }
					          }catch (Exception e) {
				     				accountLine.setPayableContractExchangeRate(new BigDecimal(1.0000));	
				    				accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(1.0000));
				    				accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(1.0000));
							  }
					             accountLine.setPayableContractValueDate(new Date());	
					             accountLine.setEstimatePayableContractValueDate(new Date());	
					             accountLine.setRevisionPayableContractValueDate(new Date());
					             
							   accountLine.setEstimateContractCurrency(contactCurrency); 
							   accountLine.setRevisionContractCurrency(contactCurrency);
							   accountLine.setContractCurrency(contactCurrency);
					             try{
					            	 List exchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,contactCurrency);
					            	 if(exchangeRateList != null && (!(exchangeRateList.isEmpty())) && exchangeRateList.get(0)!= null && (!(exchangeRateList.get(0).toString().equals("")))){
					     				accountLine.setEstimateContractExchangeRate(new BigDecimal(exchangeRateList.get(0).toString()));	
					    				accountLine.setContractExchangeRate(new BigDecimal(exchangeRateList.get(0).toString()));
					    				accountLine.setRevisionContractExchangeRate(new BigDecimal(exchangeRateList.get(0).toString()));
					            	 }else{
					     				accountLine.setEstimateContractExchangeRate(new BigDecimal(1.0000));	
					    				accountLine.setContractExchangeRate(new BigDecimal(1.0000));
					    				accountLine.setRevisionContractExchangeRate(new BigDecimal(1.0000));
					            	 }
						          }catch (Exception e) {
					     				accountLine.setEstimateContractExchangeRate(new BigDecimal(1.0000));	
					    				accountLine.setContractExchangeRate(new BigDecimal(1.0000));
					    				accountLine.setRevisionContractExchangeRate(new BigDecimal(1.0000));
								  }
						             accountLine.setEstimateContractValueDate(new Date());	
						             accountLine.setContractValueDate(new Date());	
						             accountLine.setRevisionContractValueDate(new Date());
			     		}
			     		Float f;
        		        DecimalFormat df;
        		        String revAmt="";
        		        
			            f= (accountLine.getEstimateRate().floatValue()*accountLine.getEstExchangeRate().floatValue());
			            accountLine.setEstLocalRate(new BigDecimal(f.toString()));
			            BigDecimal f1=null;
					    if(accountLine.getBasis().equals("%age") || accountLine.getBasis().equals("cwt")){
					    	 f1=(new BigDecimal(f.toString()).multiply(accountLine.getEstimateQuantity())).divide(new BigDecimal("100"));
					    }else if (accountLine.getBasis().equals("per 1000")) {
					    	 f1=(new BigDecimal(f.toString()).multiply(accountLine.getEstimateQuantity())).divide(new BigDecimal("1000"));					    	
						}else{
							 f1=(new BigDecimal(f.toString()).multiply(accountLine.getEstimateQuantity())).divide(new BigDecimal("1"));							
						}
					 	df = new DecimalFormat("#.####");
					 	revAmt = df.format(f1);
					 	accountLine.setEstLocalAmount(new BigDecimal(revAmt));
					 	
					 	f= (accountLine.getRevisionRate().floatValue()*accountLine.getRevisionExchangeRate().floatValue());
			            accountLine.setRevisionLocalRate(new BigDecimal(f.toString()));
			            f1=null;
					    if(accountLine.getBasis().equals("%age") || accountLine.getBasis().equals("cwt")){
					    	 f1=(new BigDecimal(f.toString()).multiply(accountLine.getRevisionQuantity())).divide(new BigDecimal("100"));
					    }else if (accountLine.getBasis().equals("per 1000")) {
					    	 f1=(new BigDecimal(f.toString()).multiply(accountLine.getRevisionQuantity())).divide(new BigDecimal("1000"));					    	
						}else{
							 f1=(new BigDecimal(f.toString()).multiply(accountLine.getRevisionQuantity())).divide(new BigDecimal("1"));							
						}
					 	df = new DecimalFormat("#.####");
					 	revAmt = df.format(f1);
					 	accountLine.setRevisionLocalAmount(new BigDecimal(revAmt));
					 	
					 	if(contractType){
					            f= (accountLine.getEstimateRate().floatValue()*accountLine.getEstimatePayableContractExchangeRate().floatValue());
					            accountLine.setEstimatePayableContractRate(new BigDecimal(f.toString()));
					            f1=null;
							    if(accountLine.getBasis().equals("%age") || accountLine.getBasis().equals("cwt")){
							    	 f1=(new BigDecimal(f.toString()).multiply(accountLine.getEstimateQuantity())).divide(new BigDecimal("100"));
							    }else if (accountLine.getBasis().equals("per 1000")) {
							    	 f1=(new BigDecimal(f.toString()).multiply(accountLine.getEstimateQuantity())).divide(new BigDecimal("1000"));					    	
								}else{
									 f1=(new BigDecimal(f.toString()).multiply(accountLine.getEstimateQuantity())).divide(new BigDecimal("1"));							
								}
							 	df = new DecimalFormat("#.####");
							 	revAmt = df.format(f1);
							 	accountLine.setEstimatePayableContractRateAmmount(new BigDecimal(revAmt));
							 	
					            f= (accountLine.getRevisionRate().floatValue()*accountLine.getRevisionPayableContractExchangeRate().floatValue());				            
					            accountLine.setRevisionPayableContractRate(new BigDecimal(f.toString()));
					            f1=null;
							    if(accountLine.getBasis().equals("%age") || accountLine.getBasis().equals("cwt")){
							    	 f1=(new BigDecimal(f.toString()).multiply(accountLine.getRevisionQuantity())).divide(new BigDecimal("100"));
							    }else if (accountLine.getBasis().equals("per 1000")) {
							    	 f1=(new BigDecimal(f.toString()).multiply(accountLine.getRevisionQuantity())).divide(new BigDecimal("1000"));					    	
								}else{
									 f1=(new BigDecimal(f.toString()).multiply(accountLine.getRevisionQuantity())).divide(new BigDecimal("1"));							
								}
							 	df = new DecimalFormat("#.####");
							 	revAmt = df.format(f1);
							 	accountLine.setRevisionPayableContractRateAmmount(new BigDecimal(revAmt));
					            
					            f= (accountLine.getActualExpense().floatValue()*accountLine.getPayableContractExchangeRate().floatValue());
					            accountLine.setPayableContractRateAmmount(new BigDecimal(f.toString()));
					 	}
					 // #7562 - Batch Payable Posting - Extensions		             

		             accountLine.setInvoiceDate(invoiceDate);
		             accountLine.setReceivedDate(recievedDate);
		             accountLine.setInvoiceNumber(vendorInvoice);
		             accountLineManager.save(accountLine);
		  }	
		}
		previewInvoicelines();
		payPostingFlag="y";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String accountlineship;
	private String checkedList;
     private DspDetailsManager dspDetailsManager;;
     private Map<String, String> currencyExchangeRate;
     DecimalFormat decimalFormat = new DecimalFormat("#.####");
     private String hitFlag;
	@SkipValidation
	 public String saveAgentPayProcessing(){
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//accountLineManager.saveAgentLineBatchPayableForInvoiceNoDate(invoiceNumberListServer,actualExpenseListServer,payingStatusListServer,invoiceDate,recievedDate,exchangeRate,vendorInvoice,payVatPerListServer,payVatAmtListServer,actgCodeListServer);
	
		List list= serviceOrderManager.findIdByShipNumber(accountlineship);
		if(!list.isEmpty() && list!=null && list.get(0)!=null){
			serviceOrder=serviceOrderManager.getForOtherCorpid(new Long(list.get(0).toString().trim()));
			billing=  billingManager.getForOtherCorpid(new Long(list.get(0).toString().trim()));
		}
		boolean contractType=false;
 		if(billing!=null && billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
 		String contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),billing.getCorpID());
 			if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("CMM"))) ){
 				contractType=true;	
 			}
 		}
// 		//accountLineManager.updateInvoiceDetail(accountlinelist,billing.getCorpID());
 		
 		currencyExchangeRate=accountLineManager.getExchangeRateWithCurrency(billing.getCorpID());
		Float f;
        DecimalFormat df;
		String[] str= new String[25];	
		String[] str1= new String[25];		
		String[] str2= new String[25];
		if(rowId!=null && !rowId.toString().trim().equals(""))
		{
		if( unchaccountlinelist!=null &&  (!(unchaccountlinelist.isEmpty())))
		{
		str1=unchaccountlinelist.split("#");
		
		for (String string : str1) {
			    str2=string.split(",");
				accountLine=accountLineManager.getForOtherCorpid(new Long(str2[4].toString().trim()));
			
				accountLine.setInvoiceNumber("");
				accountLine.setMyFileFileName("");
				accountLine.setInvoiceDate(null);
				accountLine.setReceivedDate(null);
				accountLine.setUpdatedOn(new Date());
				accountLine.setUpdatedBy(getRequest().getRemoteUser());
				accountLine.setPayingStatus(""); 
				accountLine=accountLineManager.save(accountLine);
		}}}
		str=accountlinelist.split("#");
		for (String string : str) {
			    str2=string.split(",");
				accountLine=accountLineManager.getForOtherCorpid(new Long(str2[3].toString().trim()));
				if(rowId==null || rowId.toString().trim().equals("") )
				{
				
				}
				else
				{
				accountLine.setInvoiceNumber(vendorInvoice);
				}
				accountLine.setMyFileFileName(rowId);
				accountLine.setInvoiceDate(invoiceDate);
				accountLine.setReceivedDate(recievedDate);
				if(rowId!=null && !rowId.toString().trim().equals(""))
				{
				accountLine.setUpdatedOn(new Date());
				accountLine.setUpdatedBy(getRequest().getRemoteUser()); 	
				accountLine.setPayingStatus("D"); 
				}
				if(accountLine.getLocalAmount()!=null && str2[1]!=null  && accountLine.getLocalAmount().doubleValue() != Double.parseDouble((str2[1])))
				{
					if(rowId!=null && !rowId.toString().trim().equals(""))
					{
					accountLine.setUpdatedOn(new Date());
					accountLine.setUpdatedBy(getRequest().getRemoteUser());	
					accountLine.setPayingStatus("W"); 
					}
					accountLine.setLocalAmount(new BigDecimal(str2[1]));
					accountLine.setActualExpense(new BigDecimal(decimalFormat.format(accountLine.getLocalAmount().doubleValue()/accountLine.getExchangeRate())));
					if(contractType){
					accountLine.setPayableContractRateAmmount(accountLine.getActualExpense().multiply(accountLine.getPayableContractExchangeRate()))  ;
					}
					if(contractType){
						 String payVatPercent=accountLine.getPayVatPercent();
						    BigDecimal payVatPercentBig=new BigDecimal("0.00");
						    BigDecimal payVatAmmountBig=new BigDecimal("0.00");
						    if(payVatPercent!=null && (!(payVatPercent.trim().equals(""))) && (!(payVatPercent.trim().equalsIgnoreCase("null"))) ){
						    	payVatPercentBig=new BigDecimal(payVatPercent);
						    }  
						 payVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLine.getLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
						 accountLine.setPayVatAmt(payVatAmmountBig);
					}else{

						 String payVatPercent=accountLine.getPayVatPercent();
						    BigDecimal payVatPercentBig=new BigDecimal("0.00");
						    BigDecimal payVatAmmountBig=new BigDecimal("0.00");
						    if(payVatPercent!=null && (!(payVatPercent.trim().equals(""))) && (!(payVatPercent.trim().equalsIgnoreCase("null"))) ){
						    	payVatPercentBig=new BigDecimal(payVatPercent);
						    }  
						 payVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLine.getActualExpense().multiply(payVatPercentBig)).doubleValue())/100)));
						 accountLine.setPayVatAmt(payVatAmmountBig);
					
					}
					
				} 
				
				if(!accountLine.getCountry().equals(str2[2]))
				{
					if(rowId!=null && !rowId.toString().trim().equals(""))
					{
					accountLine.setUpdatedOn(new Date());
					accountLine.setUpdatedBy(getRequest().getRemoteUser()); 
					accountLine.setPayingStatus("W"); 
					}
					accountLine.setCountry(str2[2]);
					if(contractType){
						if(accountLine.getCountry().equalsIgnoreCase(accountLine.getPayableContractCurrency()) && accountLine.getPayableContractExchangeRate()!=null){
							accountLine.setExchangeRate(new Double(accountLine.getPayableContractExchangeRate().toString()));
							accountLine.setValueDate(accountLine.getPayableContractValueDate());
							
						}else{
							if(currencyExchangeRate != null && (!(currencyExchangeRate.isEmpty()))){
								try{
									accountLine.setExchangeRate(new Double(currencyExchangeRate.get(accountLine.getCountry())));
									accountLine.setValueDate(new Date()); 
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
						
					}else{

						if(currencyExchangeRate != null && (!(currencyExchangeRate.isEmpty()))){
							try{
								accountLine.setExchangeRate(new Double(currencyExchangeRate.get(accountLine.getCountry())));
								accountLine.setValueDate(new Date()); 
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					
					} 
					accountLine.setActualExpense(new BigDecimal(decimalFormat.format(accountLine.getLocalAmount().doubleValue()/accountLine.getExchangeRate())));
					if(contractType){
					accountLine.setPayableContractRateAmmount(accountLine.getActualExpense().multiply(accountLine.getPayableContractExchangeRate()))  ;
					}
					if(contractType){
						 String payVatPercent=accountLine.getPayVatPercent();
						    BigDecimal payVatPercentBig=new BigDecimal("0.00");
						    BigDecimal payVatAmmountBig=new BigDecimal("0.00");
						    if(payVatPercent!=null && (!(payVatPercent.trim().equals(""))) && (!(payVatPercent.trim().equalsIgnoreCase("null"))) ){
						    	payVatPercentBig=new BigDecimal(payVatPercent);
						    }  
						 payVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLine.getLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
						 accountLine.setPayVatAmt(payVatAmmountBig);
					}else{

						 String payVatPercent=accountLine.getPayVatPercent();
						    BigDecimal payVatPercentBig=new BigDecimal("0.00");
						    BigDecimal payVatAmmountBig=new BigDecimal("0.00");
						    if(payVatPercent!=null && (!(payVatPercent.trim().equals(""))) && (!(payVatPercent.trim().equalsIgnoreCase("null"))) ){
						    	payVatPercentBig=new BigDecimal(payVatPercent);
						    }  
						 payVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLine.getActualExpense().multiply(payVatPercentBig)).doubleValue())/100)));
						 accountLine.setPayVatAmt(payVatAmmountBig);
					
					}  
		        }
				accountLine.setUpdatedOn(new Date());
				accountLine.setUpdatedBy(getRequest().getRemoteUser());
				accountLine=accountLineManager.save(accountLine);
		}
		
		
		previewAgentInvoicelines();
		hitFlag="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String partnerCodePopup;
	private List partnerPublicChild;
	private List filecabinetFiles; 
	@SkipValidation
		public String openAgentBillToCode(){
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal();  
			Map filterMap=user.getFilterMap(); 
	    	List list = new ArrayList();
	    	Set partnerSet = new HashSet();
	    	Set allPartnerSet = new HashSet();
	    	Set finalPartnerSet = new HashSet();
	    	List partnerCorpIDList = new ArrayList(); 
	    	if(filterMap.containsKey("agentFilter")){
		    	 HashSet valueSet=	(HashSet)filterMap.get("agentFilter");	
		    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
		    	Iterator iterator = valueSet.iterator();
		    	while (iterator.hasNext()) {
		    		String  mapkey = (String)iterator.next(); 
				    if(filterMap.containsKey(mapkey)){	
					
					HashMap valueMap=	(HashMap)filterMap.get(mapkey);
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					}
				    }}}
					
	    	Iterator listIterator= partnerCorpIDList.iterator();
	    	while (listIterator.hasNext()) {
	    		String code=listIterator.next().toString();
	    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
	    			list.add("'" + code + "'");	
	    		
	    		} 	
	    	} 
	    	//companydivisionSetCode= list.toString().replace("[","").replace("]", "");
	    	} 
			partnerCodePopup = list.toString().replace("[","").replace("]", "");  
			partnerPublicChild = customerFileManager.findAgentVendors(partnerCodePopup);
			return SUCCESS;
		}
		

	private String billingCountry;
	private String extReference;
	private String billingCountryCode;

	
	@SkipValidation
	public String searchAgentPortalBillToCode()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		Set partnerSet = new HashSet();
    	Set allPartnerSet = new HashSet();
    	Set finalPartnerSet = new HashSet(); 
		Map filterMap=user.getFilterMap(); 
    	List list = new ArrayList();
    	List partnerCorpIDList = new ArrayList(); 
    	if(filterMap.containsKey("agentFilter")){
	    	 HashSet valueSet=	(HashSet)filterMap.get("agentFilter");	
	    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
	    	Iterator iterator = valueSet.iterator();
	    	while (iterator.hasNext()) {
	    		String  mapkey = (String)iterator.next(); 
			    if(filterMap.containsKey(mapkey)){	
				
				HashMap valueMap=	(HashMap)filterMap.get(mapkey);
				if(valueMap!=null){
					Iterator mapIterator = valueMap.entrySet().iterator();
					while (mapIterator.hasNext()) {
						Map.Entry entry = (Map.Entry) mapIterator.next();
						String key = (String) entry.getKey(); 
					    partnerCorpIDList= (List)entry.getValue(); 
					}
				}
			    }}}
    	Iterator listIterator= partnerCorpIDList.iterator();
    	while (listIterator.hasNext()) {
    		String code=listIterator.next().toString();
    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
    			list.add("'" + code + "'");	
    			partnerSet.add(code);
    		}
    		//list.add("'" + listIterator.next().toString() + "'");	
    	}
    	//companydivisionSetCode= list.toString().replace("[","").replace("]", "");
    	}
 
		partnerCodePopup = list.toString().replace("[","").replace("]", "");
		partnerPublicChild = customerFileManager.searchAgentVendors(sessionCorpID, partnerCodePopup, partnerCode, lastName,billingCountryCode, billingCountry,"","");
		getCurrencyAccToVendorCode();
		return SUCCESS;
	}
	private String bCode;
	@SkipValidation
	public String findAgentBillToCode(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();  
		Map filterMap=user.getFilterMap(); 
    	List list = new ArrayList();
    	Set partnerSet = new HashSet();
    	Set allPartnerSet = new HashSet();
    	Set finalPartnerSet = new HashSet();
    	List partnerCorpIDList = new ArrayList(); 
    	if(filterMap.containsKey("agentFilter")){
	    	 HashSet valueSet=	(HashSet)filterMap.get("agentFilter");	
	    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
	    	Iterator iterator = valueSet.iterator();
	    	while (iterator.hasNext()) {
	    		String  mapkey = (String)iterator.next(); 
			    if(filterMap.containsKey(mapkey)){	
				
				HashMap valueMap=	(HashMap)filterMap.get(mapkey);
				if(valueMap!=null){
					Iterator mapIterator = valueMap.entrySet().iterator();
					while (mapIterator.hasNext()) {
						Map.Entry entry = (Map.Entry) mapIterator.next();
						String key = (String) entry.getKey(); 
					    partnerCorpIDList= (List)entry.getValue(); 
					}
				}
			    }}}
    	Iterator listIterator= partnerCorpIDList.iterator();
    	while (listIterator.hasNext()) {
    		String code=listIterator.next().toString();
    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
    			list.add("'" + code + "'");	
    			partnerSet.add(code);
    		}
    		//list.add("'" + listIterator.next().toString() + "'");	
    	}
    	//companydivisionSetCode= list.toString().replace("[","").replace("]", "");
    	}
 
		partnerCodePopup = list.toString().replace("[","").replace("]", ""); 
		partnerPublicChild = customerFileManager.findAgentBillToCode(partnerCodePopup,bCode);
		return SUCCESS;
	}
	private Map<String, String> docsList;
	private Map<String, String> mapList;
	private Map<String, String> relocationServices;
	private String transDocSysDefault;
	private Map<String, String> categoryList;
	
	private String voxmeIntergartionFlag;
	private String mmValidation = "NO";
	private String categorySearchValue="";
	private User user1;
	private MyFileManager myFileManager;
	private String myFileFor;
	private Map<String ,List<MyFile>> docTypeFiles;private String secure = "";
	private Map<String, String> defaultSortforFileCabinet;
	private Map<String, String> emailStatusList;
	private EmailSetupManager emailSetupManager;
	private String documentType;
	private String  agentCorpId;
	private String sid;
	private List myFiles;
	private String myFileJspName;
	private String resultType;


	private boolean selectiveInvoice ;
	private List selectiveInvoiceList;
	private Long  accId;
	private String rowId;
	private List checkRegisteredNumber;
    private List  checkInvoiceNumber;
	
	private String getCurrencyAcctoVendorCode;
	public String checkRegisteredInvoice(){ 
	
		 try {
			checkRegisteredNumber =accountLineManager.checkRegisteredInvoice(vendorCode,sessionCorpID,vendorInvoice);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	 
		}
	    	 return SUCCESS; 	
	    	
		
	}
	public String checkInvoiceNumberStatus(){ 
		
		 try {
			 checkInvoiceNumber =accountLineManager.checkInvoiceNumberStatus(vendorCode,sessionCorpID,vendorInvoice);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	 
		}
	    	 return SUCCESS; 	
	    	
		
	}
	public String getCurrencyAccToVendorCode(){ 
		
		 try {
			getCurrencyAcctoVendorCode =accountLineManager.getCurrencyAccToVendorCode(vendorCode,sessionCorpID);
			System.out.println(checkRegisteredNumber);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	 
		}
	    	 return SUCCESS; 	
	    	
		
	}
	private int checkRegisteredInvoice;
	private String accountlineFile;
	public String checkInvoice(){ 
		 try {
			checkRegisteredInvoice =accountLineManager.checkInvoice(sid,rowId,agentCorpId);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	 return "errorlog";
		}
	    	 return SUCCESS; 	
	}
	
	private Miscellaneous miscellaneous;
	private CustomerFile customerFile;
	private String transDocJobType;
	private TrackingStatus trackingStatus;
	private Long id;
	private String active;
	private String shipNumber;
	public Date getZeroTimeDate(Date dt) {
	    Date res = dt;
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(dt);
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    res = calendar.getTime();
	    return res;
	}
	
	
	private String partnerCode;
	private String lastName;

	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Date getRecievedDate() {
		return recievedDate;
	}
	public void setRecievedDate(Date recievedDate) {
		this.recievedDate = recievedDate;
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public String getVendorInvoice() {
		return vendorInvoice;
	}
	public void setVendorInvoice(String vendorInvoice) {
		this.vendorInvoice = vendorInvoice;
	}
	public Map getCountry() {
		return country;
	}
	public void setCountry(Map country) {
		this.country = country;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Set getPartners() {
		return partners;
	}
	public void setPartners(Set partners) {
		this.partners = partners;
	}
	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}
	public List getPreviewAccountLines() {
		return previewAccountLines;
	}
	public void setPreviewAccountLines(List previewAccountLines) {
		this.previewAccountLines = previewAccountLines;
	}
	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}
	public Map<String, String> getCategory() {
		return category;
	}
	public void setCategory(Map<String, String> category) {
		this.category = category;
	}
	public Map<String, String> getPayingStatus() {
		return payingStatus;
	}
	public void setPayingStatus(Map<String, String> payingStatus) {
		this.payingStatus = payingStatus;
	}
	public String getSoNameAndContract() {
		return soNameAndContract;
	}
	public void setSoNameAndContract(String soNameAndContract) {
		this.soNameAndContract = soNameAndContract;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getAccountlinelist() {
		return accountlinelist;
	}
	public void setAccountlinelist(String accountlinelist) {
		this.accountlinelist = accountlinelist;
	}
	public String getActualExpenseListServer() {
		return actualExpenseListServer;
	}
	public void setActualExpenseListServer(String actualExpenseListServer) {
		this.actualExpenseListServer = actualExpenseListServer;
	}
	public String getInvoiceNumberListServer() {
		return invoiceNumberListServer;
	}
	public void setInvoiceNumberListServer(String invoiceNumberListServer) {
		this.invoiceNumberListServer = invoiceNumberListServer;
	}
	public String getPayingStatusListServer() {
		return payingStatusListServer;
	}
	public void setPayingStatusListServer(String payingStatusListServer) {
		this.payingStatusListServer = payingStatusListServer;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public AccountLine getAccountLine() {
		return accountLine;
	}
	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;
	}
	public Map<String, String> getBasis() {
		return basis;
	}
	public void setBasis(Map<String, String> basis) {
		this.basis = basis;
	}
	public String getPayPostingFlag() {
		return payPostingFlag;
	}
	public void setPayPostingFlag(String payPostingFlag) {
		this.payPostingFlag = payPostingFlag;
	}
	public String getAccountInterface() {
		return accountInterface;
	}
	public void setAccountInterface(String accountInterface) {
		this.accountInterface = accountInterface;
	}
	public List getAccountVedorInvoiceList() {
		return accountVedorInvoiceList;
	}
	public void setAccountVedorInvoiceList(List accountVedorInvoiceList) {
		this.accountVedorInvoiceList = accountVedorInvoiceList;
	}
	public Date getPayAccDateToFormat() {
		return payAccDateToFormat;
	}
	public void setPayAccDateToFormat(Date payAccDateToFormat) {
		this.payAccDateToFormat = payAccDateToFormat;
	}
	public String getInvoiceCheck() {
		return invoiceCheck;
	}
	public void setInvoiceCheck(String invoiceCheck) {
		this.invoiceCheck = invoiceCheck;
	}
	public String getInvoicePayPostDate() {
		return InvoicePayPostDate;
	}
	public void setInvoicePayPostDate(String invoicePayPostDate) {
		InvoicePayPostDate = invoicePayPostDate;
	}
	public String getPayAccDateForm() {
		return payAccDateForm;
	}
	public void setPayAccDateForm(String payAccDateForm) {
		this.payAccDateForm = payAccDateForm;
	}
	public String getPayAccDateTo() {
		return payAccDateTo;
	}
	public void setPayAccDateTo(String payAccDateTo) {
		this.payAccDateTo = payAccDateTo;
	}
	public String getHitflag() {
		return hitflag;
	}
	public void setHitflag(String hitflag) {
		this.hitflag = hitflag;
	}
	public List getFindCompanyDivisionByCorpId() {
		return findCompanyDivisionByCorpId;
	}
	public void setFindCompanyDivisionByCorpId(List findCompanyDivisionByCorpId) {
		this.findCompanyDivisionByCorpId = findCompanyDivisionByCorpId;
	}
	public void setPartnerAccountRefManager(
			PartnerAccountRefManager partnerAccountRefManager) {
		this.partnerAccountRefManager = partnerAccountRefManager;
	}
	public String getCompanyDivisionAcctgCodeUnique() {
		return companyDivisionAcctgCodeUnique;
	}
	public void setCompanyDivisionAcctgCodeUnique(
			String companyDivisionAcctgCodeUnique) {
		this.companyDivisionAcctgCodeUnique = companyDivisionAcctgCodeUnique;
	}
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	public String getActgCode() {
		return actgCode;
	}
	public void setActgCode(String actgCode) {
		this.actgCode = actgCode;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getSortType() {
		return sortType;
	}
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
	public String getShipNo() {
		return shipNo;
	}
	public void setShipNo(String shipNo) {
		this.shipNo = shipNo;
	}
	public String getSystemDefaultVatCalculation() {
		return systemDefaultVatCalculation;
	}
	public void setSystemDefaultVatCalculation(String systemDefaultVatCalculation) {
		this.systemDefaultVatCalculation = systemDefaultVatCalculation;
	}
	public Map<String, String> getPayVatList() {
		return payVatList;
	}
	public void setPayVatList(Map<String, String> payVatList) {
		this.payVatList = payVatList;
	}
	public Map<String, String> getPayVatPercentList() {
		return payVatPercentList;
	}
	public void setPayVatPercentList(Map<String, String> payVatPercentList) {
		this.payVatPercentList = payVatPercentList;
	}
	public String getPayVatAmtListServer() {
		return payVatAmtListServer;
	}
	public void setPayVatAmtListServer(String payVatAmtListServer) {
		this.payVatAmtListServer = payVatAmtListServer;
	}
	public String getPayVatDescrListServer() {
		return payVatDescrListServer;
	}
	public void setPayVatDescrListServer(String payVatDescrListServer) {
		this.payVatDescrListServer = payVatDescrListServer;
	}
	public String getPayVatPerListServer() {
		return payVatPerListServer;
	}
	public void setPayVatPerListServer(String payVatPerListServer) {
		this.payVatPerListServer = payVatPerListServer;
	}
	public String getActgCodeListServer() {
		return actgCodeListServer;
	}
	public void setActgCodeListServer(String actgCodeListServer) {
		this.actgCodeListServer = actgCodeListServer;
	}
	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}
	public Billing getBilling() {
		return billing;
	}
	public void setBilling(Billing billing) {
		this.billing = billing;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public boolean isPostingDateStop() {
		return postingDateStop;
	}
	public void setPostingDateStop(boolean postingDateStop) {
		this.postingDateStop = postingDateStop;
	}
	public Boolean getCostElementFlag() {
		return costElementFlag;
	}
	public void setCostElementFlag(Boolean costElementFlag) {
		this.costElementFlag = costElementFlag;
	}
	public void setChargesManager(ChargesManager chargesManager) {
		this.chargesManager = chargesManager;
	}
	public Map<String, String> getEuVatPercentList() {
		return euVatPercentList;
	}
	public void setEuVatPercentList(Map<String, String> euVatPercentList) {
		this.euVatPercentList = euVatPercentList;
	}
	public String getDefaultVatVendorCode() {
		return defaultVatVendorCode;
	}
	public void setDefaultVatVendorCode(String defaultVatVendorCode) {
		this.defaultVatVendorCode = defaultVatVendorCode;
	}
	@SkipValidation
	public List getCompanyDivis() {
		return (companyDivis!=null && !companyDivis.isEmpty())?companyDivis:customerFileManager.findCompanyDivisionByBookAg(sessionCorpID, bookCode);
	}
	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}
	public String getBookCode() {
		return bookCode;
	}
	public void setBookCode(String bookCode) {
		this.bookCode = bookCode;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public String getcDivision() {
		return cDivision;
	}
	public void setcDivision(String cDivision) {
		this.cDivision = cDivision;
	}
	public boolean isAccountLineAccountPortalFlag() {
		return accountLineAccountPortalFlag;
	}
	public void setAccountLineAccountPortalFlag(boolean accountLineAccountPortalFlag) {
		this.accountLineAccountPortalFlag = accountLineAccountPortalFlag;
	}
	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}
	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getPartnerCodePopup() {
		return partnerCodePopup;
	}
	public void setPartnerCodePopup(String partnerCodePopup) {
		this.partnerCodePopup = partnerCodePopup;
	}
	public List getPartnerPublicChild() {
		return partnerPublicChild;
	}
	public void setPartnerPublicChild(List partnerPublicChild) {
		this.partnerPublicChild = partnerPublicChild;
	}
	public String getbCode() {
		return bCode;
	}
	public void setbCode(String bCode) {
		this.bCode = bCode;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}
	public List getFilecabinetFiles() {
		return filecabinetFiles;
	}
	public void setFilecabinetFiles(List filecabinetFiles) {
		this.filecabinetFiles = filecabinetFiles;
	}
	public String getVatAmount() {
		return vatAmount;
	}
	public void setVatAmount(String vatAmount) {
		this.vatAmount = vatAmount;
	}
	public String getAgentCorpId() {
		return agentCorpId;
	}
	public void setAgentCorpId(String agentCorpId) {
		this.agentCorpId = agentCorpId;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public List getMyFiles() {
		return myFiles;
	}
	public void setMyFiles(List myFiles) {
		this.myFiles = myFiles;
	}
	public String getMyFileJspName() {
		return myFileJspName;
	}
	public void setMyFileJspName(String myFileJspName) {
		this.myFileJspName = myFileJspName;
	}
	public String getResultType() {
		return resultType;
	}
	public void setResultType(String resultType) {
		this.resultType = resultType;
	}
	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}
	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}
	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}
	public boolean isSelectiveInvoice() {
		return selectiveInvoice;
	}
	public void setSelectiveInvoice(boolean selectiveInvoice) {
		this.selectiveInvoice = selectiveInvoice;
	}
	public List getSelectiveInvoiceList() {
		return selectiveInvoiceList;
	}
	public void setSelectiveInvoiceList(List selectiveInvoiceList) {
		this.selectiveInvoiceList = selectiveInvoiceList;
	}
	public Long getAccId() {
		return accId;
	}
	public void setAccId(Long accId) {
		this.accId = accId;
	} 
	
	public String getCheckedList() {
		return checkedList;
	}
	public void setCheckedList(String checkedList) {
		this.checkedList = checkedList;
	}
	public String getAccountlineship() {
		return accountlineship;
	}
	public void setAccountlineship(String accountlineship) {
		this.accountlineship = accountlineship;
	}
	public void setDspDetailsManager(DspDetailsManager dspDetailsManager) {
		this.dspDetailsManager = dspDetailsManager;
	}
	public Map<String, String> getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}
	public void setCurrencyExchangeRate(Map<String, String> currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}
	public int getCheckRegisteredInvoice() {
		return checkRegisteredInvoice;
	}
	public void setCheckRegisteredInvoice(int checkRegisteredInvoice) {
		this.checkRegisteredInvoice = checkRegisteredInvoice;
	}
	public String getAccountlineFile() {
		return accountlineFile;
	}
	public void setAccountlineFile(String accountlineFile) {
		this.accountlineFile = accountlineFile;
	}
	public Map getCountryCorpid() {
		return countryCorpid;
	}
	public void setCountryCorpid(Map countryCorpid) {
		this.countryCorpid = countryCorpid;
	}
	public List getSoCorpid() {
		return soCorpid;
	}
	public void setSoCorpid(List soCorpid) {
		this.soCorpid = soCorpid;
	}
	public String getRowId() {
		return rowId;
	}
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}
	public String getHitFlag() {
		return hitFlag;
	}
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	
	public List getCheckRegisteredNumber() {
		return checkRegisteredNumber;
	}
	public void setCheckRegisteredNumber(List checkRegisteredNumber) {
		this.checkRegisteredNumber = checkRegisteredNumber;
	}
	public String getUnchaccountlinelist() {
		return unchaccountlinelist;
	}
	public void setUnchaccountlinelist(String unchaccountlinelist) {
		this.unchaccountlinelist = unchaccountlinelist;
	}
	public List getCheckInvoiceNumber() {
		return checkInvoiceNumber;
	}
	public void setCheckInvoiceNumber(List checkInvoiceNumber) {
		this.checkInvoiceNumber = checkInvoiceNumber;
	}
	public String getGetCurrencyAcctoVendorCode() {
		return getCurrencyAcctoVendorCode;
	}
	public void setGetCurrencyAcctoVendorCode(String getCurrencyAcctoVendorCode) {
		this.getCurrencyAcctoVendorCode = getCurrencyAcctoVendorCode;
	}
	public List getCorpIDS() {
		return corpIDS;
	}
	public void setCorpIDS(List corpIDS) {
		this.corpIDS = corpIDS;
	}
	public boolean isAllowAgentInvoiceUpload() {
		return allowAgentInvoiceUpload;
	}
	public void setAllowAgentInvoiceUpload(boolean allowAgentInvoiceUpload) {
		this.allowAgentInvoiceUpload = allowAgentInvoiceUpload;
	}
	
	
	
}
