  <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
  <%@ include file="/common/taglibs.jsp"%>   
  <%@ taglib prefix="s" uri="/struts-tags" %>
    <script type="text/javascript">

      function pick() { 
  		parent.window.opener.document.location.reload();  
  		window.close();
	  } 
    </script> 
  <head>   
    <title><fmt:message key="resetSendtoDatesForm.title"/></title>   
    <meta name="heading" content="<fmt:message key='resetSendtoDatesForm.heading'/>"/> 
    <style type="text/css"> 

  .subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
  a.dsphead{text-decoration:none;color:#000000;}
  .dspchar2{padding-left:550px;}

   .table tfoot td {
      border:1px solid #E0E0E0;
    }
  .table2 thead th, table2HeaderTable td {
  background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
  border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
  border-style:solid;
  border-right:medium;
  color:#99BBE8;
  font-family:arial,verdana;
  font-size:11px;
  font-weight:bold;
  height:25px;
  padding:2px 3px 3px 5px;
  text-decoration:none;
  }

  .table tfoot th, tfoot td {
   background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
  border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
  border-style:solid solid solid none;
  border-width:1px 1px 1px medium;
  color:#15428B;
  font-family:arial,verdana;
  font-size:13px;
  font-weight:bold;
  font-color:#003366;
  height:25px;
  padding:2px 3px 3px 5px;
  text-decoration:none;
  } 
  </style> 
   <style type="text/css">h2 {background-color: #FBBFFF}</style>  
   </head>
      <s:form id="resetSendtoDatesForm" action="resetSendtoDatesForm" method="post">
      <c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>  
        <s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/> 
        <s:hidden name="recInvoiceNumber" /> 
        <s:hidden name="shipNumber" value="${shipNumber}"/> 
        <s:hidden   name="sid" value="<%=request.getParameter("sid")%>"/> 
     <table>
     <tr><td height="10"></td></tr> 
       <tr>
         <td width="10"></td>
        <td>
          <input type="button" class="cssbuttonA" style="width:160px; height:25px"   value="Reset all estimate Dates" onclick="location.href='<c:url value="/resetAllEstimateDates.html?btntype=yes&decorator=simple&popup=true&sid=${ServiceOrderID}"/>'"/>
        </td>
      </tr>
      <tr><td height="10"></td></tr> 
      <tr>
      <td width="10"></td>
       <td>
         <input type="button" class="cssbuttonA" style="width:210px; height:25px"   value="Reset Actual Sent to Client Dates" onclick="location.href='<c:url value="/resetActualSenttoDates.html?decorator=popup&popup=true&sid=${ServiceOrderID}"/>'"/>
       </td>
      </tr>
      <tr><td height="10"></td></tr> 
       <tr>
      <td width="10"></td>
      <td align="left"><input type="button" class="cssbutton1" style="" value="Cancel" onclick="window.close();"/>  
   </td></tr>
    </table> 
   </s:form>
  <script type="text/javascript">  
   try{
     if(document.forms['resetSendtoDatesForm'].elements['btntype'].value=='yes'){
      pick();
       }
       }
       catch(e){}  
   </script>  
		  	