DELIMITER $$
CREATE TRIGGER trigger_add_history_delete_container BEFORE DELETE ON container
FOR EACH ROW BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

   IF (OLD.size <> '') THEN
       select CONCAT(fieldNameList,'container.size~') into fieldNameList;
       select CONCAT(oldValueList,OLD.size,'~') into oldValueList;
   END IF;
   IF (OLD.cntnrNumber <> '') THEN
       select CONCAT(fieldNameList,'container.cntnrNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cntnrNumber,'~') into oldValueList;
   END IF;
   IF (OLD.sequenceNumber <> '') THEN
       select CONCAT(fieldNameList,'container.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
   END IF;

   IF (OLD.ship <> '') THEN
       select CONCAT(fieldNameList,'container.ship~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ship,'~') into oldValueList;
   END IF;
   IF (OLD.shipNumber <> '') THEN
       select CONCAT(fieldNameList,'container.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
   END IF;

   IF (OLD.volume <> '' or OLD.volume is not null) THEN
       select CONCAT(fieldNameList,'container.volume~') into fieldNameList;
       select CONCAT(oldValueList,OLD.volume,'~') into oldValueList;
   END IF;

   IF (OLD.idNumber <> '') THEN
       select CONCAT(fieldNameList,'container.idNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.idNumber,'~') into oldValueList;
   END IF;


IF (OLD.createdOn <> '' or OLD.createdOn is not null) THEN
       select CONCAT(fieldNameList,'container.createdOn~') into fieldNameList;
       select CONCAT(oldValueList,OLD.createdOn,'~') into oldValueList;
   END IF;

IF (OLD.corpID <> '') THEN
       select CONCAT(fieldNameList,'container.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
   END IF;

   IF (OLD.createdBy <> '') THEN
       select CONCAT(fieldNameList,'container.createdBy~') into fieldNameList;
       select CONCAT(oldValueList,OLD.createdBy,'~') into oldValueList;
   END IF;

IF (OLD.updatedOn <> '' or OLD.updatedOn is not null) THEN
       select CONCAT(fieldNameList,'container.updatedOn~') into fieldNameList;
       select CONCAT(oldValueList,OLD.updatedOn,'~') into oldValueList;
   END IF;

   IF (OLD.updatedBy <> '') THEN
       select CONCAT(fieldNameList,'container.updatedBy~') into fieldNameList;
       select CONCAT(oldValueList,OLD.updatedBy,'~') into oldValueList;
   END IF;

   IF (OLD.tripNumber <> '') THEN
       select CONCAT(fieldNameList,'container.tripNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.tripNumber,'~') into oldValueList;
   END IF;
   IF (OLD.unit1 <> '') THEN
       select CONCAT(fieldNameList,'container.unit1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit1,'~') into oldValueList;
   END IF;
   IF (OLD.unit2 <> '') THEN
       select CONCAT(fieldNameList,'container.unit2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit2,'~') into oldValueList;
   END IF;
   IF (OLD.containerNumber <> '') THEN
       select CONCAT(fieldNameList,'container.containerNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.containerNumber,'~') into oldValueList;
   END IF;

   IF (OLD.deliver <> '' or OLD.deliver is not null) THEN
       select CONCAT(fieldNameList,'container.deliver~') into fieldNameList;
       select CONCAT(oldValueList,OLD.deliver,'~') into oldValueList;
   END IF;

   IF (OLD.density <> '' or OLD.density is not null) THEN
       select CONCAT(fieldNameList,'container.density~') into fieldNameList;
       select CONCAT(oldValueList,OLD.density,'~') into oldValueList;
   END IF;
   IF (OLD.emptyContWeight <> '' or OLD.emptyContWeight is not null) THEN
       select CONCAT(fieldNameList,'container.emptyContWeight~') into fieldNameList;
       select CONCAT(oldValueList,OLD.emptyContWeight,'~') into oldValueList;
   END IF;
   IF (OLD.grossWeight <> '' or OLD.grossWeight is not null) THEN
       select CONCAT(fieldNameList,'container.grossWeight~') into fieldNameList;
       select CONCAT(oldValueList,OLD.grossWeight,'~') into oldValueList;
   END IF;
   IF (OLD.netWeight <> '' or OLD.netWeight is not null) THEN
       select CONCAT(fieldNameList,'container.netWeight~') into fieldNameList;
       select CONCAT(oldValueList,OLD.netWeight,'~') into oldValueList;
   END IF;

   IF (OLD.numberOfContainer <> '' or OLD.numberOfContainer is not null) THEN
       select CONCAT(fieldNameList,'container.numberOfContainer~') into fieldNameList;
       select CONCAT(oldValueList,OLD.numberOfContainer,'~') into oldValueList;
   END IF;
   IF (OLD.pickup <> '' or OLD.pickup is not null) THEN
       select CONCAT(fieldNameList,'container.pickup~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pickup,'~') into oldValueList;
   END IF;
   IF (OLD.pieces <> '' or OLD.pieces is not null) THEN
       select CONCAT(fieldNameList,'container.pieces~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pieces,'~') into oldValueList;
   END IF;

   IF (OLD.sealNumber <> '') THEN
       select CONCAT(fieldNameList,'container.sealNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sealNumber,'~') into oldValueList;
   END IF;
   IF (OLD.topier <> '' or OLD.topier is not null) THEN
       select CONCAT(fieldNameList,'container.topier~') into fieldNameList;
       select CONCAT(oldValueList,OLD.topier,'~') into oldValueList;
   END IF;

   IF (OLD.totalLine <> '') THEN
       select CONCAT(fieldNameList,'container.totalLine~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalLine,'~') into oldValueList;
   END IF;

   IF (OLD.useDsp <> '' or OLD.useDsp is not null) THEN
       select CONCAT(fieldNameList,'container.useDsp~') into fieldNameList;
       select CONCAT(oldValueList,OLD.useDsp,'~') into oldValueList;
   END IF;
   IF (OLD.totalPieces <> '' or OLD.totalPieces is not null) THEN
       select CONCAT(fieldNameList,'container.totalPieces~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalPieces,'~') into oldValueList;
   END IF;
   IF (OLD.totalVolume <> '' or OLD.totalVolume is not null) THEN
       select CONCAT(fieldNameList,'container.totalVolume~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalVolume,'~') into oldValueList;
   END IF;
   IF (OLD.totalNetWeight <> '' or OLD.totalNetWeight is not null) THEN
       select CONCAT(fieldNameList,'container.totalNetWeight~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalNetWeight,'~') into oldValueList;
   END IF;
   IF (OLD.grossWeightTotal <> '' or OLD.grossWeightTotal is not null) THEN
       select CONCAT(fieldNameList,'container.grossWeightTotal~') into fieldNameList;
       select CONCAT(oldValueList,OLD.grossWeightTotal,'~') into oldValueList;
   END IF;

   IF (OLD.customSeal <> '') THEN
       select CONCAT(fieldNameList,'container.customSeal~') into fieldNameList;
       select CONCAT(oldValueList,OLD.customSeal,'~') into oldValueList;
   END IF;

   IF (OLD.serviceOrderId <> '' or OLD.serviceOrderId is not null) THEN
       select CONCAT(fieldNameList,'container.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
   END IF;
 IF (OLD.densityMetric <> '' or OLD.densityMetric is not null) THEN
       select CONCAT(fieldNameList,'container.densityMetric~') into fieldNameList;
       select CONCAT(oldValueList,OLD.densityMetric,'~') into oldValueList;
   END IF;
IF (OLD.volumeCbm <> '' or OLD.volumeCbm is not null) THEN
       select CONCAT(fieldNameList,'container.volumeCbm~') into fieldNameList;
       select CONCAT(oldValueList,OLD.volumeCbm,'~') into oldValueList;
   END IF;
IF (OLD.netWeightKilo <> '' or OLD.netWeightKilo is not null) THEN
       select CONCAT(fieldNameList,'container.netWeightKilo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.netWeightKilo,'~') into oldValueList;
   END IF;
   IF (OLD.emptyContWeightKilo <> '' or OLD.emptyContWeightKilo is not null) THEN
       select CONCAT(fieldNameList,'container.emptyContWeightKilo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.emptyContWeightKilo,'~') into oldValueList;
   END IF;
   
    IF (OLD.grossWeightKilo <> '' or OLD.grossWeightKilo is not null) THEN
       select CONCAT(fieldNameList,'container.grossWeightKilo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.grossWeightKilo,'~') into oldValueList;
   END IF;
   
   CALL add_tblHistory (OLD.serviceOrderId,"container", fieldNameList, oldValueList, newValueList, OLD.updatedby, OLD.corpID, now());
END
$$