package com.trilasoft.app.webapp.action;

import java.util.*;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;



import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.QuotationCosting;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.QuotationCostingManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;

public class QuotationCostingAction extends BaseAction {
	

    private Long sid;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private QuotationCosting quotationCosting;
	private QuotationCostingManager quotationCostingManager;
	private Long id;
	private CustomerFile customerFile;
	
	private String sessionCorpID;
	
	public QuotationCostingAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public QuotationCosting getQuotationCosting() {
		return quotationCosting;
	}
	public void setQuotationCosting(QuotationCosting quotationCosting) {
		this.quotationCosting = quotationCosting;
	}
	
	
	public void setQuotationCostingManager(
			QuotationCostingManager quotationCostingManager) {
		this.quotationCostingManager = quotationCostingManager;
	}

	
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	
	
	public String edit() {   
		  
	if (id != null)
	     {  
    		serviceOrder=serviceOrderManager.get(id);
    		customerFile=serviceOrder.getCustomerFile();
		    getComboList(sessionCorpID);
		    //System.out.println("\n\n ServiceOrder record is :\t"+serviceOrder);
		 
		    if((quotationCostingManager.checkById(id)).isEmpty())
		    	{
    			  	//Billing against new service Order
    			  
		    		//System.out.println("Congrat New record");
		    		quotationCosting = new QuotationCosting();
		    		
		    		//quotationCosting.setShipNumber(serviceOrder.getShipNumber());
		    		quotationCosting.setCreatedOn(new Date());
		    		quotationCosting.setUpdatedOn(new Date());
			    	//billing.setId(id);
		    	}
		    	else
		    	{
		    		//Billing against existing service Order
		    		quotationCosting = quotationCostingManager.get(id);
		    	}
		    quotationCosting.setContract(serviceOrder.getCustomerFile().getContract());
		    quotationCosting.setCorpID(sessionCorpID);
  		}
    	
    	return SUCCESS;
    } 
	
	
	
    public String save() throws Exception {   

        boolean isNew = ((quotationCostingManager.checkById(id)).isEmpty());   
        quotationCosting.setUpdatedOn(new Date());

        //System.out.println("\n\n Billing records going to save :\t"+quotationCosting);
        //System.out.println("\n\n Billing id at Save time :\t"+quotationCosting.getId());
        quotationCostingManager.save(quotationCosting);   
      
        //System.out.println("\n\n After quotationCosting save method :\t");
        
        String key = (isNew) ? "quotationCosting.added" : "quotationCosting.updated";   
        saveMessage(getText(key));   
        if (!isNew) {   
        	return INPUT;   
        } else {
        	return SUCCESS;
        } 
    }
    
    
	
	
// for lookup and combobox

    private List refMasters;
	private RefMasterManager refMasterManager;
	private Map<String, String> category;
	
    
    public void setRefMasterManager(RefMasterManager refMasterManager) {
        this.refMasterManager = refMasterManager;
    }
    
    public List getRefMasters(){
    	return refMasters;
    }
    


    public String getComboList(String corpID){

	   category = refMasterManager.findByParameter(corpID, "CATEGORY");
 	   return SUCCESS;
    }

    public Map<String, String> getCategory() {
		return category;
	}
	public void setCategory(Map<String, String> category) {
		this.category = category;
	}
	public Long getSid() {
		return sid;
	}
	public void setSid(Long sid) {
		this.sid = sid;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

    
	

}
