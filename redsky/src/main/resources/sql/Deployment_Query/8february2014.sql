// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`partnerpublic` ADD COLUMN `isPartnerExtract` BIT(1) AFTER `agentGroup`;

DROP VIEW IF EXISTS `redsky`.`partner`;
CREATE VIEW `partner` AS select `a`.`id` AS `id`,`a`.`parent` AS `parent`,`a`.`middleInitial` AS `middleInitial`,`a`.`billingAddress1` AS `billingAddress1`,`a`.`billingAddress2` AS `billingAddress2`,`a`.`billingAddress3` AS `billingAddress3`,`a`.`billingAddress4` AS `billingAddress4`,`a`.`billingCity` AS `billingCity`,`a`.`billingCountry` AS `billingCountry`,`a`.`billingCountryCode` AS `billingCountryCode`,`a`.`billingEmail` AS `billingEmail`,`a`.`billingFax` AS `billingFax`,`a`.`billingPhone` AS `billingPhone`,`a`.`billingState` AS `billingState`,`a`.`billingTelex` AS `billingTelex`,`a`.`billingZip` AS `billingZip`,`a`.`effectiveDate` AS `effectiveDate`,`a`.`isAccount` AS `isAccount`,`a`.`isAgent` AS `isAgent`,`a`.`isCarrier` AS `isCarrier`,`a`.`isVendor` AS `isVendor`,`a`.`mailingAddress1` AS `mailingAddress1`,`a`.`mailingAddress2` AS `mailingAddress2`,`a`.`mailingAddress3` AS `mailingAddress3`,`a`.`mailingAddress4` AS `mailingAddress4`,`a`.`mailingCity` AS `mailingCity`,`a`.`mailingCountry` AS `mailingCountry`,`a`.`mailingCountryCode` AS `mailingCountryCode`,`a`.`mailingEmail` AS `mailingEmail`,`a`.`mailingFax` AS `mailingFax`,`a`.`mailingPhone` AS `mailingPhone`,`a`.`mailingState` AS `mailingState`,`a`.`mailingTelex` AS `mailingTelex`,`a`.`mailingZip` AS `mailingZip`,`a`.`partnerPrefix` AS `partnerPrefix`,`a`.`partnerSuffix` AS `partnerSuffix`,`a`.`terminalAddress1` AS `terminalAddress1`,`a`.`terminalAddress2` AS `terminalAddress2`,`a`.`terminalAddress3` AS `terminalAddress3`,`a`.`terminalAddress4` AS `terminalAddress4`,`a`.`terminalCity` AS `terminalCity`,`a`.`terminalCountry` AS `terminalCountry`,`a`.`terminalCountryCode` AS `terminalCountryCode`,`a`.`terminalEmail` AS `terminalEmail`,`a`.`terminalFax` AS `terminalFax`,`a`.`terminalPhone` AS `terminalPhone`,`a`.`terminalState` AS `terminalState`,`a`.`terminalTelex` AS `terminalTelex`,`a`.`terminalZip` AS `terminalZip`,`a`.`isPrivateParty` AS `isPrivateParty`,`a`.`air` AS `air`,`a`.`sea` AS `sea`,`a`.`surface` AS `surface`,`a`.`isOwnerOp` AS `isOwnerOp`,`a`.`typeOfVendor` AS `typeOfVendor`,`a`.`agentParent` AS `agentParent`,`a`.`location1` AS `location1`,`a`.`location2` AS `location2`,`a`.`location3` AS `location3`,`a`.`location4` AS `location4`,`a`.`companyProfile` AS `companyProfile`,`a`.`url` AS `url`,`a`.`latitude` AS `latitude`,`a`.`longitude` AS `longitude`,`a`.`trackingUrl` AS `trackingUrl`,`a`.`yearEstablished` AS `yearEstablished`,`a`.`companyFacilities` AS `companyFacilities`,`a`.`companyCapabilities` AS `companyCapabilities`,`a`.`companyDestiantionProfile` AS `companyDestiantionProfile`,`a`.`serviceRangeKms` AS `serviceRangeKms`,`a`.`serviceRangeMiles` AS `serviceRangeMiles`,`a`.`fidiNumber` AS `fidiNumber`,`a`.`OMNINumber` AS `OMNINumber`,`a`.`IAMNumber` AS `IAMNumber`,`a`.`AMSANumber` AS `AMSANumber`,`a`.`WERCNumber` AS `WERCNumber`,`a`.`facilitySizeSQFT` AS `facilitySizeSQFT`,`a`.`qualityCertifications` AS `qualityCertifications`,`a`.`vanLineAffiliation` AS `vanLineAffiliation`,`a`.`serviceLines` AS `serviceLines`,`a`.`facilitySizeSQMT` AS `facilitySizeSQMT`,`b`.`corpid` AS `corpid`,`b`.`warehouse` AS `warehouse`,`a`.`createdBy` AS `createdBy`,`a`.`createdOn` AS `createdOn`,`a`.`updatedBy` AS `updatedBy`,`a`.`updatedOn` AS `updatedOn`,`a`.`firstName` AS `firstName`,`a`.`lastName` AS `lastName`,if((`a`.`isPrivateParty` is true),`a`.`status`,`b`.`status`) AS `status`,`a`.`nextVanLocation` AS `nextVanLocation`,`a`.`nextReportOn` AS `nextReportOn`,`b`.`billingInstruction` AS `billingInstruction`,`b`.`billingInstructionCode` AS `billingInstructionCode`,`b`.`coordinator` AS `coordinator`,`b`.`salesMan` AS `salesMan`,`b`.`qc` AS `qc`,`b`.`abbreviation` AS `abbreviation`,`b`.`billPayOption` AS `billPayOption`,`b`.`billPayType` AS `billPayType`,`b`.`billToGroup` AS `billToGroup`,`b`.`longPercentage` AS `longPercentage`,`b`.`needAuth` AS `needAuth`,`a`.`partnerCode` AS `partnerCode`,`b`.`storageBillingGroup` AS `storageBillingGroup`,`b`.`accountHolder` AS `accountHolder`,`b`.`paymentMethod` AS `paymentMethod`,`b`.`payOption` AS `payOption`,`b`.`multiAuthorization` AS `multiAuthorization`,`b`.`payableUploadCheck` AS `payableUploadCheck`,`b`.`companyDivision` AS `companyDivision`,`b`.`invoiceUploadCheck` AS `invoiceUploadCheck`,`b`.`billingUser` AS `billingUser`,`b`.`payableUser` AS `payableUser`,`b`.`pricingUser` AS `pricingUser`,`b`.`partnerPortalActive` AS `partnerPortalActive`,`b`.`partnerPortalId` AS `partnerPortalId`,`b`.`associatedAgents` AS `associatedAgents`,`b`.`viewChild` AS `viewChild`,`b`.`creditTerms` AS `creditTerms`,`b`.`accountingDefault` AS `accountingDefault`,`b`.`acctDefaultJobType` AS `acctDefaultJobType`,`b`.`stopNotAuthorizedInvoices` AS `stopNotAuthorizedInvoices`,`b`.`doNotCopyAuthorizationSO` AS `doNotCopyAuthorizationSO`,`b`.`partnerPublicId` AS `partnerPublicId`,`b`.`driverAgency` AS `driverAgency`,`b`.`validNationalCode` AS `validNationalCode`,`b`.`cardNumber` AS `cardNumber`,`b`.`cardStatus` AS `cardStatus`,`b`.`accountId` AS `accountId`,`b`.`customerId` AS `customerId`,`b`.`licenseNumber` AS `licenseNumber`,`b`.`licenseState` AS `licenseState`,`b`.`cardSettelment` AS `cardSettelment`,`b`.`directDeposit` AS `directDeposit`,`b`.`fuelPurchase` AS `fuelPurchase`,`b`.`expressCash` AS `expressCash`,`b`.`creditCheck` AS `creditCheck`,`b`.`creditAmount` AS `creditAmount`,`b`.`creditCurrency` AS `creditCurrency`,`b`.`yruAccess` AS `yruAccess`,`b`.`taxId` AS `taxId`,`b`.`taxIdType` AS `taxIdType`,`b`.`extReference` AS `extReference`,`a`.`startDate` AS `startDate`,`b`.`accountManager` AS `accountManager`,`b`.`classcode` AS `classcode`,`a`.`vatNumber` AS `vatNumber`,`a`.`utsNumber` AS `utsNumber`,`a`.`isNetworkPartner` AS `isNetworkPartner`,`a`.`vanLastLocation` AS `vanLastLocation`,`a`.`vanLastReportOn` AS `vanLastReportOn`,`a`.`vanAvailCube` AS `vanAvailCube`,`a`.`vanLastReportTime` AS `vanLastReportTime`,`a`.`currentVanAgency` AS `currentVanAgency`,`a`.`currentVanID` AS `currentVanID`,`a`.`currentTractorAgency` AS `currentTractorAgency`,`a`.`currentTractorID` AS `currentTractorID`,`b`.`driverType` AS `driverType`,`b`.`noDispatch` AS `noDispatch`,`a`.`aliasName` AS `aliasName`,`a`.`networkGroup` AS `networkGroup`,`b`.`insuranceAuthorized` AS `insuranceAuthorized`,`b`.`mc` AS `mc`,`b`.`soAllDrivers` AS `soAllDrivers`,`a`.`contactName` AS `contactName`,`a`.`billingCurrency` AS `billingCurrency`,`a`.`bankCode` AS `bankCode`,`a`.`bankAccountNumber` AS `bankAccountNumber`,`a`.`agentParentName` AS `agentParentName`,`a`.`ugwwNetworkGroup` AS `ugwwNetworkGroup`,`b`.`collection` AS `collection`,`a`.`isPartnerExtract` AS `isPartnerExtract`,`a`.`agentGroup` AS `agentGroup` from (`partnerpublic` `a` join `partnerprivate` `b`) where (`a`.`id` = `b`.`partnerPublicId`);

//Already moved to prod
ALTER TABLE `redsky`.`surveyemailaudit` ADD COLUMN `sequenceNumber` VARCHAR(15) AFTER `shipNumber`;
//

DROP TABLE IF EXISTS `redsky`.`dynamicpricingcalender`;
CREATE TABLE  `redsky`.`dynamicpricingcalender` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `corpID` varchar(30) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `menRate2` varchar(10) DEFAULT NULL,
  `menRate3` varchar(10) DEFAULT NULL,
  `menRate4` varchar(10) DEFAULT NULL,
  `extraManRate` varchar(10) DEFAULT NULL,
  `currentDate` datetime DEFAULT NULL,
  `dynamicPricingType` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `redsky`.`itemsjbkequip` ADD COLUMN `customerName` VARCHAR(60) DEFAULT '' AFTER `equipMaterialsId`, ADD COLUMN `payMethod` VARCHAR(45) DEFAULT '' AFTER `customerName`;

CREATE TABLE  `redsky`.`refquoteservices` (
   `id` bigint(20) NOT NULL AUTO_INCREMENT,
   `corpId` varchar(5) DEFAULT NULL,
   `createdBy` varchar(82) DEFAULT NULL,
   `createdOn` datetime DEFAULT NULL,
   `langauge` varchar(2) DEFAULT NULL,
   `mode` varchar(20) DEFAULT NULL,
   `job` varchar(3) DEFAULT NULL,
   `routing` varchar(5) DEFAULT NULL,
   `code` varchar(45) DEFAULT NULL,
   `description` text,
   `checkIncludeExclude` varchar(1) DEFAULT NULL,
   `refQuoteServicesDefault` bit(1) DEFAULT NULL,
   `updatedOn` datetime DEFAULT NULL,
   `updatedBy` varchar(82) DEFAULT NULL,
   PRIMARY KEY (`id`)
);

ALTER TABLE `redsky`.`company` ADD COLUMN `cportalAccessCompanyDivisionLevel` BIt(1) DEFAULT b'0';
 
CREATE  TABLE `redsky`.`soadditionaldate` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `corpID` VARCHAR(5) NULL ,
  `createdBy` VARCHAR(82) NULL ,
  `createdOn` DATETIME NULL ,
  `updatedBy` VARCHAR(82) NULL ,
  `updatedOn` DATETIME NULL ,
  `serviceOrderId` BIGINT(20) NULL ,
  `outByDate` DATETIME NULL ,
  `prePackDate` DATETIME NULL ,
  `prePackMen` INT(10) NULL ,
  `prePackHrs` VARCHAR(15) NULL ,
  `wrapDay` DATETIME NULL ,
  `wrapDayMen` INT(10) NULL ,
  `wrapDayHrs` VARCHAR(15) NULL ,
  `preLoadDate` DATETIME NULL ,
  `preLoadMen` INT(10) NULL ,
  `preLoadHrs` VARCHAR(15) NULL ,
  `packDate` DATETIME NULL ,
  `packDayMen` INT(10) NULL ,
  `packDayHrs` VARCHAR(15) NULL ,
  `loadDate` DATETIME NULL ,
  `loadDayMen` INT(10) NULL ,
  `loadDayHrs` VARCHAR(15) NULL ,
  `loadMonth` VARCHAR(10) NULL ,
  `loadWeek` VARCHAR(10) NULL ,
  `loadElevatorAccess` BIT(1) NULL ,
  `loadElevatorBooking` BIT(1) NULL ,
  `loadTimeFrom` VARCHAR(5) NULL ,
  `loadTimeTo` VARCHAR(5) NULL ,
  `possessionDate` DATETIME NULL ,
  `possessionTime` VARCHAR(15) NULL ,
  `prefferedDelieveryDate` DATETIME NULL ,
  `ttgFromDate` DATETIME NULL ,
  `ttgFromMen` INT(10) NULL ,
  `ttgFromHrs` VARCHAR(15) NULL ,
  `guaranteedDeliveryDate` DATETIME NULL ,
  `guaranteedDeliveryDateMen` INT(10) NULL ,
  `guaranteedDeliveryDateHrs` VARCHAR(15) NULL ,
  `unpackUnwrapDate` DATETIME NULL ,
  `unpackUnwrapDateMen` INT(10) NULL ,
  `unpackUnwrapDateHrs` VARCHAR(15) NULL ,
  `debrisRemovalDate` DATETIME NULL ,
  `debrisRemovalDateMen` INT(10) NULL ,
  `debrisRemovalDateHrs` VARCHAR(15) NULL ,
  `ttgToDate` DATETIME NULL ,
  `ttgToDateMen` INT(10) NULL ,
  `ttgToDateHrs` VARCHAR(15) NULL ,
  `actualDeliveryDate` DATETIME NULL ,
  `actualDeliveryDateMen` INT(10) NULL ,
  `actualDeliveryDateHrs` VARCHAR(15) NULL ,
  `setUpDate` DATETIME NULL ,
  `setUpDateMen` INT(10) NULL ,
  `setUpDateHrs` VARCHAR(15) NULL ,
  `deliveryMonth` VARCHAR(10) NULL ,
  `deliveryWeek` VARCHAR(10) NULL ,
  `deliveryElevatorAccess` BIT(1) NULL ,
  `deliveryElevatorBooking` BIT(1) NULL ,
  `deliveryTimeFrom` VARCHAR(5) NULL ,
  `deliveryTimeTo` VARCHAR(5) NULL ,
  PRIMARY KEY (`id`) );



ALTER TABLE `redsky`.`defaultaccountline` ADD COLUMN `originCountry` VARCHAR(45) DEFAULT NULL AFTER 
`estVatAmt`, ADD COLUMN `destinationCountry` VARCHAR(45) DEFAULT NULL AFTER `originCountry`;

ALTER TABLE `redsky`.`defaultaccountline` MODIFY COLUMN `destinationCountry` VARCHAR(3) DEFAULT NULL,
MODIFY COLUMN `originCountry` VARCHAR(3) DEFAULT NULL AFTER `estVatAmt`;

ALTER TABLE `redsky`.`soadditionaldate` CHANGE COLUMN `loadTimeFrom` `loadTimeFrom` VARCHAR(15) NULL DEFAULT NULL  , CHANGE COLUMN `loadTimeTo` `loadTimeTo` VARCHAR(15) NULL DEFAULT NULL  , CHANGE COLUMN `deliveryTimeFrom` `deliveryTimeFrom` VARCHAR(15) NULL DEFAULT NULL  , CHANGE COLUMN `deliveryTimeTo` `deliveryTimeTo` VARCHAR(15) NULL DEFAULT NULL  ;

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `incExcServiceType` TEXT AFTER `isSOExtract`,
ADD COLUMN `incExcServiceTypeLanguage` VARCHAR(5) AFTER `incExcServiceType`;

ALTER TABLE `redsky`.`refquoteservices` ADD UNIQUE `Index_2` USING HASH(`code`, `corpId`);

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `accountHolderForRelo` VARCHAR(82);

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `billingUserForRelo` VARCHAR(82);

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `accountManagerForRelo` VARCHAR(82);

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `payableUserForRelo` VARCHAR(82);

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `pricingUserForRelo` VARCHAR(82);

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `auditorForRelo` VARCHAR(82);

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `claimsUserForRelo` VARCHAR(82);

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `coordinatorForRelo` VARCHAR(82);

ALTER TABLE `redsky`.`timesheet` ADD COLUMN `dailySheetNotes` TEXT NULL  AFTER `isCrewUsed`;

ALTER TABLE `redsky`.`workticket` DROP COLUMN `dailySheetNotes` ;

ALTER TABLE `redsky`.`company` ADD COLUMN `chargeDiscountSetting` bit(1)  DEFAULT b'0' AFTER `cportalAccessCompanyDivisionLevel`;

ALTER TABLE `redsky`.`refquoteservices` DROP INDEX `Index_2`;

// run in next deployment as per dilip
ALTER TABLE `redsky`.`accountline` ADD COLUMN `estimateDiscount` DECIMAL(6,2) DEFAULT 0.00 AFTER `serviceTaxInput`,
 ADD COLUMN `actualDiscount` DECIMAL(6,2) DEFAULT 0.00 AFTER `estimateDiscount`,
 ADD COLUMN `revisionDiscount` DECIMAL(6,2) DEFAULT 0.00 AFTER `actualDiscount`;
//


//8947 as per surya
ALTER TABLE `redsky`.`equipmaterialslimits` DROP INDEX `uc_resource`,
ADD UNIQUE INDEX `uc_resource` USING BTREE(`resource`, `branch`, `division`);

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `prefSurveyDate1` DATETIME AFTER `cportalEmailLanguage`,ADD COLUMN `prefSurveyDate2` DATETIME AFTER `prefSurveyDate1`;

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `prefSurveyTime1` VARCHAR(5) AFTER `prefSurveyDate2`,
ADD COLUMN `prefSurveyTime2` VARCHAR(5) AFTER `prefSurveyTime1`;

CREATE TABLE `redsky`.`discount` (
   `id` BIGINT(20)  NOT NULL AUTO_INCREMENT, `corpID` 
VARCHAR(25),`createdBy` VARCHAR(82),
   `createdOn` DATETIME,`updatedBy` VARCHAR(82), `updatedOn` DATETIME, 
`contract` VARCHAR(100),
   `chargeCode` VARCHAR(20),  `discount` DECIMAL(6,2),`description` 
VARCHAR(45),
   PRIMARY KEY(`id`)
);

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `prefSurveyTime3` VARCHAR(5) AFTER `prefSurveyTime2`,ADD COLUMN `prefSurveyTime4` VARCHAR(5) AFTER `prefSurveyTime3`;

ALTER TABLE `redsky`.`t20VisionLogInfo` ADD COLUMN `checkLHF` VARCHAR(10) DEFAULT '' AFTER `serviceOrderId`;

ALTER TABLE `redsky`.`miscellaneous` ADD COLUMN `discount` DECIMAL(6,2) AFTER `assignDate`;

ALTER TABLE `redsky`.`company` ADD COLUMN `accessQuotationFromCustomerFile` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `moveType` VARCHAR(45) ;

ALTER TABLE `redsky`.`miscellaneous` MODIFY COLUMN `discount` DECIMAL(7,2) DEFAULT NULL;

ALTER TABLE `redsky`.`dspdetails`  ADD COLUMN `mailServiceType` TEXT AFTER `TEN_displyOtherVendorCode`;

update company set chargediscountsetting=false ;

ALTER TABLE `redsky`.`company` MODIFY COLUMN `surveyEmailSign` VARCHAR(500) ;


************
//As per Neha and Ashish Sir
select payableUploadCheck from partnerprivate
where partnercode in(select partnercode from partnerpublic
where isOwnerOp is true and corpid in ('TSFT','SSCW')) and corpid='SSCW' and payableUploadCheck != true;

update partnerprivate set payableUploadCheck = true where partnercode in(select partnercode from partnerpublic
where isOwnerOp is true and corpid in ('TSFT','SSCW')) and corpid='SSCW';

//As per Kundan #8933
Need to run FindsumofActualRevenueofallserviceorder and FindsumofActualRevenue_Customerfile_Initiation from dev1.




