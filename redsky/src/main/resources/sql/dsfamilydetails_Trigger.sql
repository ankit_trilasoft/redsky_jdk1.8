DELIMITER $$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `redsky`.`trigger_add_history_dsfamilydetails`
BEFORE UPDATE ON `redsky`.`dsfamilydetails`
FOR EACH ROW
BEGIN
DECLARE fieldNameList LONGTEXT;
DECLARE oldValueList LONGTEXT;
DECLARE newValueList LONGTEXT;
DECLARE relation LONGTEXT;
DECLARE sid LONGTEXT;


SET fieldNameList = " ";
SET oldValueList = " ";
SET newValueList = " ";
SET relation = " ";
SET sid = " ";

  select relationship into relation from dsfamilydetails where id=NEW.id;

  IF(relation='Spouse') then
IF (OLD.firstName <> NEW.firstName ) THEN
    select CONCAT(fieldNameList,'serviceorder.firstName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.firstName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.firstName,'~') into newValueList;
END IF;
END IF;

  IF(relation='Spouse') then
IF (OLD.lastName <> NEW.lastName ) THEN
    select CONCAT(fieldNameList,'serviceorder.lastName~') into fieldNameList;
    select CONCAT(oldValueList,OLD.lastName,'~') into oldValueList;
    select CONCAT(newValueList,NEW.lastName,'~') into newValueList;
END IF;
END IF;

END
$$
