<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="fn1" uri="http://java.sun.com/jsp/jstl/functions" %>
<head>
<title>Driver Commission</title>
</head>
<s:form name="driverCommissionForm" id="driverCommissionForm" action="savedriverCommissionForm" method="post" validate="true">
<s:hidden name="driverCommissionPlan.id"></s:hidden>
<s:hidden name="dummPlan" />
<s:hidden name="dummCharges" />
<div id="newmnav" style="float:left;">
     <ul>
		<li ><a href=""><span>Driver Commission</span></a>
		</li>
		
	</ul>
</div>
<div class="spn spnSF">&nbsp;</div> 
        <div id="content" align="center">
         <div id="liquid-round">
         <div class="top" style="margin-top:0px;!margin-top:5px;"><span></span></div>
           <div class="center-content">
           <table cellspacing="1" cellpadding="2" border="0" style="margin:0px; ">
             <tbody>
                <tr>
                <td colspan="10">
                <table style="margin:0px;padding:0px;"><!--
                <tr>
                   <td width="39"></td>
                   <td align="left" class="listwhitetext" width="138"><input value="newPlan" type="radio" checked="checked" name="newPlan" onclick="enablePlan('newPlan');" tabindex="">New Plan</td>
                   <td align="left" class="listwhitetext"><input value="existingPlan" type="radio"  name="newPlan" onclick="enablePlan('existingPlan');" tabindex="">Existing Plan</td>
                </tr>
                -->
                </table>                
                </td>
                </tr>
                
                <tr>
                <td colspan="10">
                <table style="margin:0px;padding:0px;">
                <tr>
                <td class="listwhitetext" align="right">Plan<font color="red" size="2">*</font></td>
                <td colspan="2"> <s:select cssClass="list-menu" name="driverCommissionPlan.plan" id="ExnewPlans"  list="%{planCommissionList}"  headerKey="" headerValue=""  cssStyle="width:105px;" tabindex=""/></td>
                </tr>
                 <tr>
                   <td></td>
                   </tr>
                <tr>
                  <td class="listwhitetext" align="right">Charge code<font color="red" size="2">*</font></td>
                 <td id="charge1" width="127"><s:textfield name="driverCommissionPlan.charge" id="newcharge" readonly="true" cssClass="input-textUpper" cssStyle="width:100px;" maxlength="15" onchange="setContractValue();"  tabindex=""/>
                  <div id="charge3" style="float:right;">&nbsp;<img class="openpopup" width="17" height="20" onclick="openCharge();"  id="openpopup5.img" src="<c:url value='/images/open-popup.gif'/>" /></div></td>
                  <td class="listwhitetext" align="right">Description</td>             
                 <td><s:textfield name="driverCommissionPlan.description" id="newDescription" readonly="true" cssClass="input-textUpper" cssStyle="width:205px;" maxlength="15"   tabindex=""/></td>
                </tr>
                <tr><td height="0" align="left"></td></tr>
                <tr>
                <td class="listwhitetext" align="right">Contract</td>                
                <!--<td colspan="3"><s:textfield name="driverCommissionPlan.contract" cssClass="input-text" cssStyle="width:320px;"  maxlength="50"   tabindex="6"/></td>
                -->
                <td colspan="3"><s:select cssClass="list-menu"  name="driverCommissionPlan.contract" id="contractId" list="%{chargess}" value="%{multiplContracts}"  multiple="true" headerKey="" headerValue="" cssStyle="width:398px;height:80px"/></td>
                </tr>
                <tr><td height="0" align="left"></td></tr>
                <tr>
                <td class="listwhitetext" align="right">Percent<font color="red" size="2">*</font></td>
                <td colspan="3"><s:textfield name="driverCommissionPlan.percent" id="percent123" cssClass="input-text"cssStyle="width:126px;"  maxlength="50" onchange="onlyFloat(this);"   tabindex=""/></td>
                <td width="60"></td>
                </tr>
                 <tr><td height="0" align="left"></td></tr>
                <tr>
                <td class="listwhitetext" align="right">Amount Basis</td>
                <td colspan="2"><s:select cssClass="list-menu" name="driverCommissionPlan.amountBasis" id="ExnewPlans"  list="%{amtList}"    cssStyle="width:130px;" tabindex=""/></td>
                </tr>
                </table>
                </td>
                </tr>                
                
                <tr>
                
               <%-- <td align="left" class="listwhitetext"><s:radio name="Amount1" value="%{Amount1}" list="%{amtList}" /></td> --%>
             
                <!-- <td align="left" width="120" class="listwhitetext"><input value="grossRevenue" type="radio"  name="Amount1"  tabindex="">&nbsp;Gross Revenue</td>
                <td align="left" class="listwhitetext"><input value="distributionAmount" type="radio"  name="Amount1"  tabindex="">&nbsp;Distribution Amount</td> -->
<%--                 <td align="left" width="120" class="listwhitetext"><s:checkbox key="driverCommissionPlan.grossRevenue" fieldValue="true" tabindex="27"/>&nbsp;Gross Revenue</td>
               <td align="left" class="listwhitetext"><s:checkbox key="driverCommissionPlan.distributionAmount" fieldValue="true" tabindex="27"/>&nbsp;Distribution Amount</td> --%>
                </tr>
             </tbody>
           </table>
        </div>
		<div class="bottom-header" style="margin-top:45px;"><span></span></div>
		</div>
		</div>
        <table width="800px">
			<tbody>
				<tr>
				      <td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="containerCreatedOnFormattedValue" value="${driverCommissionPlan.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="driverCommissionPlan.createdOn" value="${containerCreatedOnFormattedValue}" />
						<td><fmt:formatDate value="${driverCommissionPlan.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
					
					
					<td align="right" class="listwhitetext" width="100"><b><fmt:message key='contractPolicy.createdBy' /></b></td>
					
					<c:if test="${not empty driverCommissionPlan.id}">
						<s:hidden name="driverCommissionPlan.createdBy"/>
						<td ><s:label name="createdBy" value="%{driverCommissionPlan.createdBy}"/></td>
					</c:if>
					<c:if test="${empty driverCommissionPlan.id}">
						<s:hidden name="driverCommissionPlan.createdBy" value="${pageContext.request.remoteUser}"/>
						<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
					</c:if>
					<td align="right" class="listwhitetext" width="90"><b><fmt:message key='contractPolicy.updatedOn'/></b></td>
					<s:hidden name="driverCommissionPlan.updatedOn"/>
					<td width="130"><fmt:formatDate value="${driverCommissionPlan.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
					<td align="right" class="listwhitetext" width="90"><b><fmt:message key='contractPolicy.updatedBy' /></b></td>
					<c:if test="${not empty driverCommissionPlan.id}">
								<s:hidden name="driverCommissionPlan.updatedBy"/>
								<td><s:label name="updatedBy" value="%{driverCommissionPlan.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty driverCommissionPlan.id}">
								<s:hidden name="driverCommissionPlan.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
				</tr>
			</tbody>
			</table>  
        <s:submit cssClass="cssbutton" method="save" key="button.save"   cssStyle="width:60px; height:25px " onclick="return checkValidation();" />
	    <s:reset cssClass="cssbutton" key="Reset" cssStyle="width:60px; height:25px "/>
         <input id="goToCommissionPlanList" class="cssbutton" type="button" name="goToCommissionPlanList" style="margin-right: 5px;height: 25px;width:180px; font-size:15" value="Go To Commission PlanList"
			onclick="location.href='driverCommissionList.html'" />
        <c:if test="${not empty driverCommissionPlan.id}">
        <c:if test="${hitFlag=='3'}">
		 <c:redirect url="/driverCommissionList.html"/>
		</c:if>
		</c:if>
</s:form>
<script type="text/javascript">


function openCharge(){
	openWindow('driverCommissionCharges.html?decorator=popup&popup=true',755,500);
}

function checkValidation(){
	if(document.forms['driverCommissionForm'].elements['driverCommissionPlan.plan'].value==''){
		alert("Select Plan to continue.....");
		return false;
	}else if(document.forms['driverCommissionForm'].elements['driverCommissionPlan.charge'].value==''){
		alert("Select Charge Code to continue.....");
		return false;
	}else if(document.forms['driverCommissionForm'].elements['driverCommissionPlan.percent'].value=='0'){
		alert("Select Percent to continue.....");
		return false;
	}
	return true;
}
function setPlanValue(target){
	var testvalue=target.value;
	var url="planSearch.html?decorator=simple&popup=true&planValue="+encodeURI(testvalue);
	http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponseSetPlanValue;
    http2.send(null);
}
function handleHttpResponseSetPlanValue(){
	 var results = http2.responseText
     results = results.trim();
	 var res = results.split("#");
	 var targetElement=document.getElementById("charge5");
		targetElement.length= res.length;
     	 for(i=0; i<res.length; i++){
     		if(res[i] == ''){
     		}else{
              document.getElementById("charge5").options[i].text =res[i];
              document.getElementById("charge5").options[i].value= res[i];
         		}
     		}
}
function setChargeValue(target){
	var testcharge=target.value;
	var testvalue=document.getElementById("ExnewPlans").value;	
	var url="chargeSearch.html?decorator=simple&popup=true&chargeValue="+encodeURI(testcharge)+"&planValue="+encodeURI(testvalue);
	http3.open("GET", url, true);
    http3.onreadystatechange = handleHttpResponseSetChargeValue;
    http3.send(null);
	
}
function handleHttpResponseSetChargeValue(){
	 var results = http3.responseText
     results = results.trim();
	 var res = results.split("#");
	    if(res.length>1)
        {
        document.forms['driverCommissionForm'].elements['driverCommissionPlan.description'].value= res[0];  
        document.forms['driverCommissionForm'].elements['driverCommissionPlan.contract'].value= res[1];
        document.forms['driverCommissionForm'].elements['driverCommissionPlan.percent'].value= res[2];
        if(res[3]=='1'){
        	document.forms['driverCommissionForm'].elements['driverCommissionPlan.distributionAmount'].checked=true;
        }else{
        	document.forms['driverCommissionForm'].elements['driverCommissionPlan.distributionAmount'].checked=false;
        }
        if(res[4]=='1'){
        	document.forms['driverCommissionForm'].elements['driverCommissionPlan.grossRevenue'].checked= true 
        }else{
        	document.forms['driverCommissionForm'].elements['driverCommissionPlan.grossRevenue'].checked= false 
        }
              
	    }
}
function setContractValue(){
	var testcharge=document.getElementById("newcharge").value;
	if(testcharge==''){
		
	}else{
		var url="searchDriverChargesList.html?decorator=simple&popup=true&chargeValue="+encodeURI(testcharge);
		http4.open("GET", url, true);
	    http4.onreadystatechange = handleHttpResponseSetChargeValue11;
	    http4.send(null);
	}
}
function handleHttpResponseSetChargeValue11(){
	 var results = http4.responseText
     results = results.trim();
	var res = results.split("#");
    var targetElement=document.forms['driverCommissionForm'].elements['driverCommissionPlan.contract'];
	     targetElement.length= res.length+1;
		 for(i=0; i<res.length; i++){
	     		if(res[i] == ''){
	     		}else{
	     		document.forms['driverCommissionForm'].elements['driverCommissionPlan.contract'].options[i].text =res[i];
	     		document.forms['driverCommissionForm'].elements['driverCommissionPlan.contract'].options[i].value= res[i];
	     		if (document.getElementById("contractId").options[i].value == '${driverCommissionPlan.contract}'){
					   document.getElementById("contractId").options[i].defaultSelected = true;
					}
		         }
     }
}

var http4 = getHTTPObject();
var http3 = getHTTPObject();
var http2 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

try{
	//setContractValue();
		
}catch(e){
	
}
</script>
