<%@ include file="/common/taglibs.jsp"%>
  
<html lang="en">
<head>
</head>
<body>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>

<style type="text/css"> 

h4 {
    color: #444;
    font-size: 14px;
    line-height: 1.3em;
    margin: 0 0 0.25em;
    padding: 0 0 0 15px;
    font-weight:bold;
}
.modal-body {
    padding: 0 20px;
    max-height: calc(100vh - 140px);
    overflow-y: auto;
    margin-top: 10px
}
.modal-header {
    border-bottom: 1px solid #e5e5e5;
    min-height: 5.43px;
    padding: 10px;
}
.modal-footer {
    border-top: 1px solid #e5e5e5;
    padding: 10px;
    text-align: right;
}
.modal-header .close {
    margin-right: 15px;
    margin-top: -2px;
    text-align: right;
}
@media screen and (min-width: 768px) {
    .custom-class {
        width: 70%;
        /* either % (e.g. 60%) or px (400px) */
    }
}
.hide{
	display:none;
}
.show{
	display:block;
}
.list-menu {
    font-family: arial,verdana;
    font-size: 12px;
    color: rgb(00,00,00);
    border: 1px solid rgb(33,157,209);
    height: 19px;
    !height: 20px;
    overflow: hidden;
    text-decoration: none;
    padding: 1px;
}
</style>
<script type="text/javascript">

function saveSurveyByUserDetails(){
		var surveyByUserVal = document.forms['surveyUserForm'].elements['surveyAnsByUserVal'].value;
		var $j = jQuery.noConflict();
		if(surveyByUserVal!=''){
			var cid = document.forms['surveyUserForm'].elements['cid'].value;
			if(cid!=null && cid!=''){
				 $j.ajax({
						type: "POST",
						url: "saveSurveyByUser.html?bypass_sessionfilter=YES&decorator=modal&popup=true",
						data: {cid:cid,surveyByUserVal:surveyByUserVal},
						success: function (result) {
							$j("#closeQuestionAnswerModal").hide('hide');
							$j("#getResponseThankYouMsg").html(result.trim());
							$j("#getResponseThankYou").modal('show');
				           },
							error: function (data, textStatus, jqXHR) {
								
							}
				         });
			}else{
				var sid = document.forms['surveyUserForm'].elements['sid'].value;
				$j.ajax({
					type: "POST",
					url: "saveSurveyByUser.html?bypass_sessionfilter=YES&decorator=modal&popup=true",
					data: {sid:sid,surveyByUserVal:surveyByUserVal},
					success: function (result) {
						$j("#closeQuestionAnswerModal").hide('hide');
						$j("#getResponseThankYouMsg").html(result.trim());
						$j("#getResponseThankYou").modal('show');
			           },
						error: function (data, textStatus, jqXHR) {
							
						}
			         });
			}
		}else{
			alert('Nothing found for save.');
		}
}

function getFieldId(qid,target,type){
	var ansid;
	if(target.value==undefined){
		ansid = target;
	}else{
		ansid = target.value;
	}
	var idVal = qid+'~'+ansid;
	
	if(type!='dropDown' && type!='radio' && type!='text'){
		var surveyVal = document.forms['surveyUserForm'].elements['surveyAnsByUserVal'].value;
			     
	     if(surveyVal == ''){
	    	 document.forms['surveyUserForm'].elements['surveyAnsByUserVal'].value = idVal;
	     }else{
	    	var check = surveyVal.indexOf(idVal);
	 		if(check > -1){
	 			var values = surveyVal.split("@");
	 		   for(var i = 0 ; i < values.length ; i++) {
	 		      if(values[i]== idVal) {
	 		    	values.splice(i, 1);
	 		    	surveyVal = values.join("@");
	 		    	document.forms['surveyUserForm'].elements['surveyAnsByUserVal'].value = surveyVal;
	 		      }
	 		   }
	 		}else{
	 			surveyVal = surveyVal + "@" +idVal;
	 			document.forms['surveyUserForm'].elements['surveyAnsByUserVal'].value = surveyVal.replace('@ @',"@");
	 		}
	     }
	}else{
		var surveyVal = document.forms['surveyUserForm'].elements['surveyAnsByUserVal'].value;
	     
	     if(surveyVal == ''){
	    	 document.forms['surveyUserForm'].elements['surveyAnsByUserVal'].value = idVal;
	     }else{
	    	var check = surveyVal.indexOf(qid+'~');
	 		if(check > -1){
	 			var values = surveyVal.split("@");
	 			var sanitizedArray = [];
	 		   for(var i = 0 ; i < values.length ; i++) {
	 			  if (qid+'~' !== values[i] && values[i].indexOf(qid+'~') == -1) {
	 				 sanitizedArray.push(values[i]);
	 		        }
	 		    }
	 		 surveyVal = sanitizedArray.join("@");
	 		 if(surveyVal!="" && ansid!=""){
	 		 	document.forms['surveyUserForm'].elements['surveyAnsByUserVal'].value = surveyVal+ "@" +idVal;
	 		 }else if(surveyVal=="" && ansid!=""){
	 			document.forms['surveyUserForm'].elements['surveyAnsByUserVal'].value = idVal;
	 		 }else{
	 			document.forms['surveyUserForm'].elements['surveyAnsByUserVal'].value = surveyVal;
	 		 }
	 		}else{
	 			surveyVal = surveyVal + "@" +idVal;
	 			document.forms['surveyUserForm'].elements['surveyAnsByUserVal'].value = surveyVal.replace('@ @',"@");
	 		}
	     }
	}
}
</script>

<c:if test="${surveyUserList!='{}' && surveyUserList!=null}">
<div id="closeQuestionAnswerModal" >
<div class="modal-dialog" style="width:40%;">
 <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Survey&nbsp;Questions</h4>
      </div>
<div class="modal-body">
<s:form name="surveyUserForm" id="surveyUserForm" action="">
<s:hidden name="surveyAnsByUserVal" id="surveyAnsByUserVal" />
<s:hidden name="sid" id="sid" value="<%=request.getParameter("sid") %>" />
<s:hidden name="cid" id="cid" value="<%=request.getParameter("cid") %>" />

	<table class="detailTabLabel" style="margin:0px;width:100%;" border="0">
 		<tr>
			<td style="text-align:right !important;font-size:11px;color:#003366;" ><b>Order No. ${sequenceNumber}<br>${addressDetails}</b><br></td>
		</tr>
	</table>
	<table class="detailTabLabel" style="margin:0px;width:100%;" border="0">
 		<tr>
			<td align="center" style="font-size:11px;color:#003366;" ><b>Dear ${userName}</b></td>
		</tr>
		<tr>
			<td align="center" style="padding-bottom:5px;border-bottom:1px solid #ddd;font-size:11px;color:#003366;"><b>${welcomeMsg}</b></td>
		</tr>
	</table>
<div style="overflow-y:auto;height:360px;">

	<c:forEach var="individualItem" items="${surveyUserList}" >
		<c:forEach var="surveyQuestionListValue" items="${individualItem.key}" >
			
				<c:if test="${(surveyQuestionListValue.surveyQuestionCategoryType!=null && surveyQuestionListValue.surveyQuestionCategoryType!='') && (prevCategory!=surveyQuestionListValue.surveyQuestionCategoryType)}">
					<div style="border-bottom:1px solid #b7b5b5;padding-top:5px"><b class="listwhite" style="color:#003366;font-size:13px;">${surveyQuestionListValue.surveyQuestionCategoryType}</b></div>
				</c:if>

				<c:if test="${surveyQuestionListValue.surveyQuestionQuestion!=null && surveyQuestionListValue.surveyQuestionQuestion!=''}">
					<div style="padding-top:5px"><b class="listwhite" style="color:#15428b;font-size:11px;">Q:&nbsp;${surveyQuestionListValue.surveyQuestionQuestion}</b></div>
				</c:if>
						
				<c:if test="${surveyQuestionListValue.surveyQuestionAnswerType=='SingleAnswerDropdown'}">
					    <select name="surveyByUserDropDown${surveyQuestionListValue.surveyQuestionId}" onchange="getFieldId('drp${surveyQuestionListValue.surveyQuestionId}',this,'dropDown')" style="margin-top:5px;margin-bottom:5px;width:258px" class="list-menu" id="drp${surveyQuestionListValue.surveyQuestionId}">
						</select>
						<script type="text/javascript">
						try{
							var results = '${individualItem.value}';
			                results = results.trim();
			                var res = results.replace("{",'');
			                res = res.replace("}",'');
			                res = res.split(",");
			                targetElement = document.getElementById('drp${surveyQuestionListValue.surveyQuestionId}');
							targetElement.length = res.length;
			 				for(i=0;i<res.length;i++){
			 					var idValue = res[i];
			 					idValue = idValue.split("=");
			 					document.getElementById('drp${surveyQuestionListValue.surveyQuestionId}').options[i].text =idValue[1].trim();
			 					document.getElementById('drp${surveyQuestionListValue.surveyQuestionId}').options[i].value =idValue[0].trim();
							}
			 				var opt = document.createElement('option');    
		 					var surveyList = document.getElementById('drp${surveyQuestionListValue.surveyQuestionId}');
		 					opt.selected = 'selected';
		 					surveyList.insertBefore(opt, surveyList[0]);
						}catch(e){
							//alert(e)
						}
                       	</script>
				</c:if>
				
				<c:if test="${surveyQuestionListValue.surveyQuestionAnswerType=='SingleAnswerRadio'}">
					<c:forEach var="surveyAnswerListValue" items="${individualItem.value}" >
						<input type="radio" style="vertical-align:middle;margin-top:5px;margin-bottom:5px;" name="surveyByUserRadio${surveyQuestionListValue.surveyQuestionId}" id="${surveyAnswerListValue.key}" value="${surveyAnswerListValue.value}" onclick="getFieldId('rad${surveyQuestionListValue.surveyQuestionId}','${surveyAnswerListValue.key}','radio')"/>
						<span style="color:#15428b;font-size:11px;">&nbsp;${surveyAnswerListValue.value}</span>
					</c:forEach>
				</c:if>
				
				<c:if test="${surveyQuestionListValue.surveyQuestionAnswerType=='MultipleCheckbox'}">
					<c:forEach var="surveyAnswerListValue" items="${individualItem.value}" >
						<input type="checkbox" style="vertical-align:middle;margin-top:5px;margin-bottom:5px;" name="surveyByUserCheckbox${surveyQuestionListValue.surveyQuestionId}" id="${surveyAnswerListValue.key}" value="${surveyAnswerListValue.value}" onclick="getFieldId('chk${surveyQuestionListValue.surveyQuestionId}','${surveyAnswerListValue.key}','checkBox')"/>
						<span style="color:#15428b;font-size:11px;">&nbsp;${surveyAnswerListValue.value}</span>
					</c:forEach>
				</c:if>
				
				<c:if test="${surveyQuestionListValue.surveyQuestionAnswerType=='Text'}">
						<textarea class="textarea" style="margin-top:5px;margin-bottom:5px;" rows="4" cols="40" maxlength="1000" name="surveyByUserTextarea${surveyQuestionListValue.surveyQuestionId}" id="${surveyAnswerListValue.key}" onchange="getFieldId('txt${surveyQuestionListValue.surveyQuestionId}',this,'text')"></textarea>
				</c:if>
				<c:set var="prevCategory" value="${surveyQuestionListValue.surveyQuestionCategoryType}" />
		</c:forEach>			 
	</c:forEach>
</div>


</s:form>
</div>
	<div class="modal-footer" style="padding-bottom:10px !important;">
		<button type="button" class="btn btn-sm btn-primary" value="" onclick="saveSurveyByUserDetails()">Submit</button>
		<!-- <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button> -->
	</div>
</div></div></div>
</c:if>

<c:if test="${surveyUserList=='{}' || surveyUserList==null}">
	<c:if test="${returnAjaxStringValue=='' || returnAjaxStringValue==null}">
		<div class="modal-dialog modal-sm">
		      <div class="modal-content">
			      <div class="modal-header" style="min-height:5.43px;padding-top:2px;">
			      	<button type="button" class="close" style="text-align:right !important;margin-bottom:10px;margin-top:1px;" data-dismiss="modal" >&times;</button>
			      </div>
			       <div class="modal-body" style="min-height:75px;color:#003366;font-weight:600;overflow-y:visible !important">
			       		<td align="center" class="listwhitetext">No&nbsp;Set&nbsp;Up&nbsp;Found&nbsp;For&nbsp;Survey.</td>
			       </div>
		    </div>
	   </div>
	</c:if>
	<c:if test="${returnAjaxStringValue!='' && returnAjaxStringValue!=null}">
		<div class="modal-dialog modal-sm">
		      <div class="modal-content">
			       <div class="modal-body" style="min-height:100px;color:#003366;font-weight:600;">
			       		<td align="center" class="listwhitetext">${returnAjaxStringValue}</td>
			       </div>
		    </div>
	   </div>
	</c:if>
</c:if>
<div class="modal fade" id="getResponseThankYou" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
		    <!-- <div class="modal-header" style="min-height:5.43px;padding-top:2px;">
		      <button type="button" class="close" style="text-align:right !important;margin-bottom:10px;margin-top:1px;" data-dismiss="modal" >&times;</button>
		    </div> -->
			    <div class="modal-body" id="getResponseThankYouMsg" style="min-height:100px;color:#003366;font-weight:600;">
			      	<td align="center" class="listwhitetext">${returnAjaxStringValue}</td>
			    </div>
		</div>
	</div>
</div>

</body>
</html>
