package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.EmailSetupDao;
import com.trilasoft.app.model.EmailSetup;

public class EmailSetupDaoHibernate extends GenericDaoHibernate<EmailSetup, Long> implements EmailSetupDao{
	public EmailSetupDaoHibernate(){
		super(EmailSetup.class);
	}
	public Map<String, String> getEmailStatusWithId(String ids,String sessionCorpID){
		List<EmailSetup> list;
		Map<String, String> parameterMap = new LinkedHashMap<String, String>(); 
		if(ids!=null && !ids.equalsIgnoreCase("")){
		list = getHibernateTemplate().find("from EmailSetup where id in ("+ids+")"); 
			for (EmailSetup emailSetup : list) {
				parameterMap.put(emailSetup.getId()+"", emailSetup.getRetryCount()+"^"+emailSetup.getEmailStatus().replaceAll("\"", ""));
			} 
		}
		return parameterMap;
	}
	public List getMaxId() {
		return getHibernateTemplate().find("select max(id) from EmailSetup");
	}
	public void globalEmailSetupProcess(String from,String to,String cc,String bcc,String location,String body,String subject,String corpId,String module,String fileNumber ,String signaturePart){		
//		getHibernateTemplate().bulkUpdate("Insert into EmailSetup ('retryCount','recipientTo','recipientCc','recipientBcc','subject','body','signature','attchedFileLocation','dateSent','emailStatus','corpId','createdOn','createdBy','updatedOn','updatedBy','signaturePart') VALUES (0,'"+to+"','"+cc+"','"+bcc+"','"+subject+"','"+body+"','"+from+"','"+location+"',now(),'SaveForEmail','"+corpId+"',now(),'EMAILSETUP',now(),'EMAILSETUP','')");
		if(to!=null && !to.equals("")){
		EmailSetup emailSetup = new EmailSetup();
		emailSetup.setRetryCount(0);
		emailSetup.setRecipientTo(to);
		emailSetup.setRecipientCc(cc);
		emailSetup.setRecipientBcc(bcc);
		emailSetup.setSubject(subject);
		emailSetup.setBody(body);
		emailSetup.setSignature(from);
		emailSetup.setAttchedFileLocation(location);
		emailSetup.setDateSent(new Date());
		emailSetup.setEmailStatus("SaveForEmail");
		emailSetup.setCorpId(corpId);
		emailSetup.setCreatedOn(new Date());
		emailSetup.setCreatedBy("EMAILSETUP");
		emailSetup.setUpdatedOn(new Date());
		emailSetup.setUpdatedBy("EMAILSETUP");
		emailSetup.setSignaturePart(signaturePart);
		emailSetup.setModule(module);
		emailSetup.setFileNumber(fileNumber);		
		
		this.getSession().save(emailSetup);
		}
	}
	
	public void insertMailFromEmailSetupTemplate(String from,String to,String cc,String bcc,String location,String body,String subject,String corpId,String module,String fileNumber,String signaturePart,String saveAs,String lastSentMailBody){
		EmailSetup emailSetup = new EmailSetup();
		emailSetup.setRetryCount(0);
		emailSetup.setRecipientTo(to);
		emailSetup.setRecipientCc(cc);
		emailSetup.setRecipientBcc(bcc);
		emailSetup.setSubject(subject);
		emailSetup.setBody(body);
		emailSetup.setSignature(from);
		emailSetup.setAttchedFileLocation(location);
		emailSetup.setDateSent(new Date());
		emailSetup.setEmailStatus("SaveForEmail");
		emailSetup.setCorpId(corpId);
		emailSetup.setCreatedOn(new Date());
		emailSetup.setCreatedBy("EMAILSETUP");
		emailSetup.setUpdatedOn(new Date());
		emailSetup.setUpdatedBy("EMAILSETUP");
		emailSetup.setSignaturePart(signaturePart);
		emailSetup.setModule(module);
		emailSetup.setFileNumber(fileNumber);
		emailSetup.setSaveAs(saveAs);
		emailSetup.setLastSentMailBody(lastSentMailBody);
		
		this.getSession().save(emailSetup);
	}
	
	public List findEmailSetUpList(String recipientTo,String fileNumber,String signature,String subject,Date dateSent,String type,String sessionCorpID){
		StringBuilder formatedDate1=new StringBuilder() ;
		List lnkList=new LinkedList();
		try{
			if(dateSent!=null){
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( dateSent ) );
			}
		}catch(Exception ex){}
		 String query="";
		 String idList="";
		if(type.equalsIgnoreCase("PENDING")){
			query="select SUBSTRING_INDEX(GROUP_CONCAT(id ORDER BY dateSent desc SEPARATOR ','), ',', 1500) from emailsetup where corpId='"+sessionCorpID+"' AND emailStatus='SaveForEmail'";			
		}else if(type.equalsIgnoreCase("FAILED")){
			query="select SUBSTRING_INDEX(GROUP_CONCAT(id ORDER BY dateSent desc SEPARATOR ','), ',', 1500) from emailsetup where corpId='"+sessionCorpID+"' AND emailStatus like '%SaveForEmail' AND emailStatus!='SaveForEmail'";
		}else if(type.equalsIgnoreCase("SENT")){
			query="select SUBSTRING_INDEX(GROUP_CONCAT(id ORDER BY dateSent desc SEPARATOR ','), ',', 1500) from emailsetup where corpId='"+sessionCorpID+"' AND emailStatus not like '%SaveForEmail%'";
		}else{
			query="select SUBSTRING_INDEX(GROUP_CONCAT(id ORDER BY dateSent desc SEPARATOR ','), ',', 1500) from emailsetup where corpId='"+sessionCorpID+"'";
		}
		if(fileNumber!=null && !fileNumber.equalsIgnoreCase("")){
			query=query+" AND (fileNumber like '%"+fileNumber+"%') ";
		}
		if(recipientTo!=null && !recipientTo.equalsIgnoreCase("")){
			query=query+" AND (recipientTo like '%"+recipientTo+"%') ";
		}
		if(signature!=null && !signature.equalsIgnoreCase("")){
			query=query+" AND (signature like '%"+signature+"%') ";
		}
		if(subject!=null && !subject.equalsIgnoreCase("")){
			query=query+" AND (subject like '%"+subject+"%') ";
		}
		if(formatedDate1!=null && !formatedDate1.toString().equalsIgnoreCase("")){
			query=query+" AND ((DATE_FORMAT(dateSent,'%Y-%m-%d')) like '%"+formatedDate1+"%') ";
		}
		List al=this.getSession().createSQLQuery(query).list();
		if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().equalsIgnoreCase(""))){
			idList =   al.get(0).toString();
		}
		if(!idList.equalsIgnoreCase("")){
			lnkList= getHibernateTemplate().find("from EmailSetup where id in ("+idList+") order by id desc");
		}else{
			lnkList= null;
		}
		return lnkList;
	}
	public String reverseString(String source){
        String reverse = "";
        for(int i = source.length() -1; i>=0; i--){
            reverse = reverse + source.charAt(i);
        }
        return reverse;
	}
	public String findEmailStatus(String sessionCorpID ,String fileNumber)
	{
		String emailStatus1="";
		List l1 =  this.getSession().createSQLQuery("select emailStatus from emailsetup where corpID='"+ sessionCorpID + "' and fileNumber='" + fileNumber+ "' ").list();
		if((l1!=null) && (!l1.isEmpty()) && (l1.get(0)!=null)){
		
			emailStatus1= l1.get(l1.size() - 1).toString();
		}
		emailStatus1 =emailStatus1.trim();
		return emailStatus1;
		}
	 public int getEmailSetupCount(String sessionCorpID ,String fileNumber){
		 int count1=0;
		 String query="select count(*) from emailsetup where corpID='"+ sessionCorpID + "' and fileNumber='" + fileNumber+ "' ";
			List al=this.getSession().createSQLQuery(query).list();
			if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
			count1 =   Integer.parseInt(al.get(0).toString());
			}
			return count1;		
	 } 
		 
	 }

