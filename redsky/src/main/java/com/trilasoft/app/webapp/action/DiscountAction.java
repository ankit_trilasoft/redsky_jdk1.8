package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Discount;
import com.trilasoft.app.service.DiscountManager;

public class DiscountAction extends BaseAction implements Preparable {
	private Discount discounts;
	private String sessionCorpID;
	private String companyCode;
	private DiscountManager discountManager;
	Date currentdate = new Date();
	private Long  contractId;
	private Long id;
	private Long chargeId;
	private Charges charges;
	private String contract;
	private String chargeCode;
	private List discountList;
    static final Logger logger = Logger.getLogger(DiscountAction.class); 

	
	public DiscountAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		companyCode=user.getCompanyDivision();
			}
	@SkipValidation
	public String list(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		discountList = discountManager.discountList(contract,chargeCode,sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String edit() {
		if (id != null) {
			discounts = discountManager.get(id);
		} else {
			discounts= new Discount();			
			discounts.setCreatedBy(getRequest().getRemoteUser());
			discounts.setChargeCode(chargeCode);
			discounts.setContract(contract);
			discounts.setCorpID(sessionCorpID);
		}
		return SUCCESS;
    }
	public String save() throws Exception {	
		boolean isNew=(discounts.getId()==null);
		 discounts.setCreatedOn(new Date());
		 discounts.setCreatedBy(getRequest().getRemoteUser());
		 discounts.setUpdatedOn(new Date());
		 discounts.setUpdatedBy(getRequest().getRemoteUser());
		 discounts.setCorpID(sessionCorpID);
		 discounts=discountManager.save(discounts);
		 chargeCode= discounts.getChargeCode();
		 contract=discounts.getContract();
		String key = (isNew) ? "Discount has been added successfully." : "Discount has been updated successfully.";
		saveMessage(getText(key));
		return SUCCESS;
	}
    public String deleteDiscount() {
    	discountManager.remove(id);
        return SUCCESS;
    }
	public Long getChargeId() {
		return chargeId;
	}
	public void setChargeId(Long chargeId) {
		this.chargeId = chargeId;
	}

	public void setDiscountManager(DiscountManager discountManager) {
		this.discountManager = discountManager;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	public String getChargeCode() {
		return chargeCode;
	}
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}
	public Charges getCharges() {
		return charges;
	}
	public void setCharges(Charges charges) {
		this.charges = charges;
	}
	
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public Long getContractId() {
		return contractId;
	}
	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}
	public Date getCurrentdate() {
		return currentdate;
	}
	public void setCurrentdate(Date currentdate) {
		this.currentdate = currentdate;
	}
	public Discount getDiscounts() {
		return discounts;
	}
	public void setDiscounts(Discount discounts) {
		this.discounts = discounts;
	}
	public void prepare() throws Exception {
		
	}
	public List getDiscountList() {
		return discountList;
	}
	public void setDiscountList(List discountList) {
		this.discountList = discountList;
	}
	

}
