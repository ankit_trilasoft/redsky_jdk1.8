package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.GenericSurveyAnswer;

public interface GenericSurveyAnswerDao extends GenericDao<GenericSurveyAnswer, Long>{
	
	public List getSurveyAnswerList(Long parentId, String sessionCorpID);

}
