package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.EmailSetup;

public interface EmailSetupDao extends GenericDao<EmailSetup, Long>{
	public Map<String, String> getEmailStatusWithId(String ids,String sessionCorpID);
	public List getMaxId();
	public void globalEmailSetupProcess(String from,String to,String cc,String bcc,String location,String body,String subject,String corpId,String module,String fileNumber,String signaturePart);
	public List findEmailSetUpList(String recipientTo,String fileNumber ,String signature,String subject,Date dateSent,String type,String sessionCorpID);
	public String findEmailStatus(String sessionCorpID ,String fileNumber);
	public int getEmailSetupCount(String sessionCorpID ,String fileNumber);
	public void insertMailFromEmailSetupTemplate(String from,String to,String cc,String bcc,String location,String body,String subject,String corpId,String module,String fileNumber,String signaturePart,String saveAs,String lastSentMailBody);
}
