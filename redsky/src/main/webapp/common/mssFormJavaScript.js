
<script type="text/javascript">
$('#ABC11').slimscroll({
	  height: '200px',
	  railVisible: true,
	  alwaysVisible: true,
	  
	});
$('#Origin11').slimscroll({
	  height: '248px',
	  railVisible: true,
	  alwaysVisible: true
	});
$('#destinationCat111').slimscroll({
	  height: '248px',
	  railVisible: true,
	  alwaysVisible: true
	});
$('#originCatsc').slimscroll({
	  height: '248px',
	  width: '450px',
	  railVisible: true,
	  alwaysVisible: true
	});
$('#destinationCatsc').slimscroll({
	  height: '248px',
	  width: '450px',
	  railVisible: true,
	  alwaysVisible: true
	});

function check(){
	var radioType 	= 	document.getElementsByName('mss.transportTypeRadio');
    var t='';
	for (var i = 0, length = radioType.length; i < length; i++) {
	    if (radioType[i].checked) {
	    	t=radioType[i].value;
	    }
	}
  var service = document.forms['mssForm'].elements['mss.serviceOverMultipleDays'].checked;
  if(t=='O' && service!=true){
	  document.forms['mssForm'].elements['mss.loadingStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.loadingStartDate'].className ='input-text';
	  document.forms['mssForm'].elements['mss.packingStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.packingStartDate'].className ='input-text';
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].className ='input-text';
	  ////
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].readonly='readonly';
	  document.forms['mssForm'].elements['mss.loadingEndDate'].readonly='readonly';
	  document.forms['mssForm'].elements['mss.packingEndDate'].readonly='readonly';
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].readonly='readonly';
	  ///
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].className ="input-textUpper";
	  document.forms['mssForm'].elements['mss.loadingEndDate'].className ='input-textUpper';
	  document.forms['mssForm'].elements['mss.packingEndDate'].className ='input-textUpper';
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].className ='input-textUpper';
	  
	  document.getElementById('loadingStartDate_trigger').style.display="block";
	  document.getElementById('packingStartDate_trigger').style.display="block";
	  document.getElementById('requestedOriginDate_trigger').style.display="block";
	  document.getElementById('deliveryStartDate_trigger').style.display="none";
	  document.getElementById('loadingEndDate_trigger').style.display="none";
	  document.getElementById('packingEndDate_trigger').style.display="none";
	  document.getElementById('deliveryEndDate_trigger').style.display="none";
	  document.getElementById('requestedDestinationDate_trigger').style.display="none";
	  document.forms['mssForm'].elements['mss.loadingEndDate'].value = '';
	  document.forms['mssForm'].elements['mss.packingEndDate'].value = '';
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].value = '';
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].value = '';
	  document.forms['mssForm'].elements['mss.requestedDestinationDate'].value = '';
	  document.forms['mssForm'].elements['mss.requestedDestinationDate'].className ='input-textUpper';
  }
  if(t=='D' && service!=true){
	  document.forms['mssForm'].elements['mss.loadingEndDate'].readonly='readonly';
	  document.forms['mssForm'].elements['mss.packingEndDate'].readonly='readonly';
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].readonly='readonly';
	  
	  document.forms['mssForm'].elements['mss.loadingEndDate'].className ='input-textUpper';
	  document.forms['mssForm'].elements['mss.packingEndDate'].className='input-textUpper';
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].className ='input-textUpper';
	  
	  document.forms['mssForm'].elements['mss.loadingStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.loadingStartDate'].className ='input-text';
	  document.forms['mssForm'].elements['mss.packingStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.packingStartDate'].className ='input-text';
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].className ='input-text';
	  
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].className ='input-text';
	  
	  document.forms['mssForm'].elements['mss.requestedDestinationDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.requestedDestinationDate'].className ='input-text';
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].readonly='readonly';
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].className ='input-textUpper';
	  
	  document.getElementById('loadingStartDate_trigger').style.display="block";
	  document.getElementById('packingStartDate_trigger').style.display="block";
	  document.getElementById('deliveryStartDate_trigger').style.display="block";
	  document.getElementById('requestedDestinationDate_trigger').style.display="block";
	  document.getElementById('loadingEndDate_trigger').style.display="none";
	  document.getElementById('packingEndDate_trigger').style.display="none";
	  document.getElementById('deliveryEndDate_trigger').style.display="none";
	  document.getElementById('requestedOriginDate_trigger').style.display="none";
	  document.forms['mssForm'].elements['mss.loadingEndDate'].value = '';
	  document.forms['mssForm'].elements['mss.packingEndDate'].value = '';
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].value = '';	 
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].value = document.forms['mssForm'].elements['delieveryStartDate'].value;
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].value = '';
  }
  if(t=='B' && service!=true){
	  document.forms['mssForm'].elements['mss.loadingEndDate'].readonly='readonly';
	  document.forms['mssForm'].elements['mss.packingEndDate'].readonly='readonly';
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].readonly='readonly';
	  
	  //document.forms['mssForm'].elements['mss.deliveryEndDate'].className ='input-text';
	  document.forms['mssForm'].elements['mss.requestedDestinationDate'].className ='input-text';
	 //document.forms['mssForm'].elements['mss.requestedDestinationDate'].disabled = false;
	  
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].className ='input-text';
	  //forms['mssForm'].elements['mss.requestedOriginDate'].disabled = false;
	  
	  document.forms['mssForm'].elements['mss.loadingEndDate'].className ="input-textUpper";
	  document.forms['mssForm'].elements['mss.packingEndDate'].className ='input-textUpper';
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].className ='input-textUpper';
	  
	  document.forms['mssForm'].elements['mss.loadingStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.loadingStartDate'].className ='input-text';
	  document.forms['mssForm'].elements['mss.packingStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.packingStartDate'].className ='input-text';
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].className ='input-text';
	
	
	  document.getElementById('loadingStartDate_trigger').style.display="block";
	  document.getElementById('packingStartDate_trigger').style.display="block";
	  document.getElementById('deliveryStartDate_trigger').style.display="block";
	  document.getElementById('requestedDestinationDate_trigger').style.display="block";
	  document.getElementById('loadingEndDate_trigger').style.display="none";
	  document.getElementById('packingEndDate_trigger').style.display="none";
	  document.getElementById('deliveryEndDate_trigger').style.display="none";
	  document.getElementById('requestedOriginDate_trigger').style.display="block";
	  document.forms['mssForm'].elements['mss.loadingEndDate'].value = '';
	  document.forms['mssForm'].elements['mss.packingEndDate'].value = '';
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].value = '';
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].value = '';
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].value = document.forms['mssForm'].elements['delieveryStartDate'].value;
	  
	 
  }
  if(t=='O' && service == true){
	  document.forms['mssForm'].elements['mss.loadingStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.loadingStartDate'].className ="input-text";
	  
	  document.forms['mssForm'].elements['mss.packingStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.packingStartDate'].className ="input-text";
	  
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].readonly='readonly';
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].className ="input-textUpper";
	  
	  document.forms['mssForm'].elements['mss.loadingEndDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.loadingEndDate'].className ="input-text";
	  document.forms['mssForm'].elements['mss.packingEndDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.packingEndDate'].className ="input-text";
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].readonly='readonly';
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].className ="input-textUpper";
	  
	  document.forms['mssForm'].elements['mss.requestedDestinationDate'].readonly='readonly';
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].className ="input-text";
	  document.getElementById('loadingStartDate_trigger').style.display="block";
	  document.getElementById('packingStartDate_trigger').style.display="block";
	  document.getElementById('requestedOriginDate_trigger').style.display="block";
	  document.getElementById('deliveryStartDate_trigger').style.display="none";
	  document.getElementById('loadingEndDate_trigger').style.display="block";
	  document.getElementById('packingEndDate_trigger').style.display="block";
	  document.getElementById('deliveryEndDate_trigger').style.display="none";
	  document.getElementById('requestedDestinationDate_trigger').style.display="none";	  
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].value = '' ;
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].value = '';
	  document.forms['mssForm'].elements['mss.requestedDestinationDate'].value = '' ;
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].value = document.forms['mssForm'].elements['requestedOriginDate'].value;
	  document.forms['mssForm'].elements['mss.loadingStartDate'].value =document.forms['mssForm'].elements['loadingStartDate'].value ;
	  document.forms['mssForm'].elements['mss.packingStartDate'].value=document.forms['mssForm'].elements['packingStartDate'].value;
	  document.forms['mssForm'].elements['mss.loadingEndDate'].value= document.forms['mssForm'].elements['loadingEndDate'].value ;
	  document.forms['mssForm'].elements['mss.packingEndDate'].value= document.forms['mssForm'].elements['packingEndDate'].value  ;
  }
  if(t=='D' && service==true){
	  document.forms['mssForm'].elements['mss.loadingEndDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.loadingEndDate'].className ="input-text";
	  document.forms['mssForm'].elements['mss.packingEndDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.packingEndDate'].className ="input-text";
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].className ="input-text";
	  
	  document.forms['mssForm'].elements['mss.loadingStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.loadingStartDate'].className ="input-text";
	  document.forms['mssForm'].elements['mss.packingStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.packingStartDate'].className ="input-text";
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].className ="input-text";
	  
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].readonly='readonly';
	  document.forms['mssForm'].elements['mss.requestedDestinationDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.requestedDestinationDate'].className ="input-text";
	  document.getElementById('requestedDestinationDate_trigger').style.display="block";
	  document.getElementById('loadingEndDate_trigger').style.display="block";
	  document.getElementById('packingEndDate_trigger').style.display="block";
	  document.getElementById('deliveryEndDate_trigger').style.display="block";
	  document.getElementById('requestedOriginDate_trigger').style.display="none";
	  document.getElementById('deliveryStartDate_trigger').style.display="block";
	  document.getElementById('loadingEndDate_trigger').style.display="block";
	  document.getElementById('packingEndDate_trigger').style.display="block";
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].value=document.forms['mssForm'].elements['delieveryStartDate'].value;
	  document.forms['mssForm'].elements['mss.loadingEndDate'].value= document.forms['mssForm'].elements['loadingEndDate'].value ;
	  document.forms['mssForm'].elements['mss.packingEndDate'].value= document.forms['mssForm'].elements['packingEndDate'].value ;
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].value = document.forms['mssForm'].elements['deliveryEndDate'].value ;
	 document.forms['mssForm'].elements['mss.requestedOriginDate'].value = '';
	 document.forms['mssForm'].elements['mss.requestedDestinationDate'].value= document.forms['mssForm'].elements['requestedDestinationDate'].value  ;
	 document.forms['mssForm'].elements['mss.loadingStartDate'].value= document.forms['mssForm'].elements['loadingStartDate'].value ;
	 document.forms['mssForm'].elements['mss.packingStartDate'].value= document.forms['mssForm'].elements['packingStartDate'].value;
  }
  if(t=='B' && service==true){
	  document.forms['mssForm'].elements['mss.loadingEndDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.loadingEndDate'].className ="input-text";
	  document.forms['mssForm'].elements['mss.packingEndDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.packingEndDate'].className ="input-text";
	  
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].className ="input-text";
	  document.forms['mssForm'].elements['mss.loadingStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.loadingStartDate'].className ="input-text";
	  document.forms['mssForm'].elements['mss.packingStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.packingStartDate'].className ="input-text";
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].className ="input-text";
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].className ="input-text";
	  document.forms['mssForm'].elements['mss.requestedDestinationDate'].disabled = false;
	  document.forms['mssForm'].elements['mss.requestedDestinationDate'].className ="input-text";
	  document.getElementById('requestedDestinationDate_trigger').style.display="block";
	  document.getElementById('loadingEndDate_trigger').style.display="block";
	  document.getElementById('packingEndDate_trigger').style.display="block";
	  document.getElementById('deliveryEndDate_trigger').style.display="block";
	  document.getElementById('requestedOriginDate_trigger').style.display="block";
	  document.getElementById('deliveryStartDate_trigger').style.display="block";
	  document.getElementById('loadingEndDate_trigger').style.display="block";
	  document.getElementById('packingEndDate_trigger').style.display="block";	  
	  document.forms['mssForm'].elements['mss.deliveryStartDate'].value = document.forms['mssForm'].elements['delieveryStartDate'].value;
	  document.forms['mssForm'].elements['mss.loadingEndDate'].value = document.forms['mssForm'].elements['loadingEndDate'].value;
	  document.forms['mssForm'].elements['mss.packingEndDate'].value = document.forms['mssForm'].elements['packingEndDate'].value ;
	  document.forms['mssForm'].elements['mss.deliveryEndDate'].value = document.forms['mssForm'].elements['deliveryEndDate'].value ;
	  document.forms['mssForm'].elements['mss.requestedOriginDate'].value = document.forms['mssForm'].elements['requestedOriginDate'].value;
	  document.forms['mssForm'].elements['mss.requestedDestinationDate'].value = document.forms['mssForm'].elements['requestedDestinationDate'].value ;
	  document.forms['mssForm'].elements['mss.loadingStartDate'].value = document.forms['mssForm'].elements['loadingStartDate'].value;
	  document.forms['mssForm'].elements['mss.packingStartDate'].value = document.forms['mssForm'].elements['packingStartDate'].value;
	  
  }
} 
function notExists(){
	alert("This Form has not been saved yet, please save to continue");
}
function addRow(tableID) {

    var table = document.getElementById(tableID);
    var rowCount = table.rows.length;
    document.forms['mssForm'].elements['rowCountvalue'].value=rowCount;
	var row = table.insertRow(rowCount); 
    var newlineid=document.getElementById('newlineid').value;              
    if(newlineid==''){
        newlineid=rowCount;
      }else{
      newlineid=newlineid+"~"+rowCount;
      }
    document.getElementById('newlineid').value=newlineid;
    
 	var cell15 = row.insertCell(0);
 	var element1 = document.createElement("select");
     element1.setAttribute("class", "list-menu" );
     element1.id="tra"+rowCount;
     element1.style.width="50px"
    var transportList='${transportTypeValue}';
    transportList=transportList.replace('[','').replace(']','');
    var transportarray=transportList.split(",");
   
    var optioneleBlankCategory= document.createElement("option");
    optioneleBlankCategory.value="";
    optioneleBlankCategory.text=""; 
    element1.options.add(optioneleBlankCategory); 
	for(var i=0;i<transportarray.length;i++){
	   	var value=transportarray[i].split(",");	   	
	    var optionele= document.createElement("option");
	    optionele.text=value[0]; 	    
    element1.options.add(optionele);             
 }
 cell15.appendChild(element1);

var cell1 = row.insertCell(1);
var element2 = document.createElement("input");
element2.type = "text";
element2.style.width="200px";
element2.setAttribute("class", "input-text" );
element2.setAttribute("maxlength", "94" );
element2.id='des'+rowCount;
cell1.appendChild(element2);


var cell2 = row.insertCell(2);
var element3 = document.createElement("input");
element3.type = "text";
element3.style.width="70px";
element3.setAttribute("class", "input-text" );    
element3.id='len'+rowCount;
element3.setAttribute("onblur","ValidateDecimal(this)")
cell2.appendChild(element3);

var cell3 = row.insertCell(3);
var element4 = document.createElement("input");
element4.type = "text";
element4.style.width="70px";
element4.setAttribute("class", "input-text" );    
element4.id='wid'+rowCount;
element4.setAttribute("onblur","ValidateDecimal(this)")
cell3.appendChild(element4);

var cell4 = row.insertCell(4);
var element5 = document.createElement("input");
element5.type = "text";
element5.style.width="70px";
element5.setAttribute("class", "input-text" );    
element5.id='hei'+rowCount;
element5.setAttribute("onblur","ValidateDecimal(this)")
cell4.appendChild(element5);


var cell5 = row.insertCell(5);
var element6 = document.createElement("input");
element6.type = "checkbox";
element6.style.width="30px";
element6.id='ply'+rowCount;
element6.value=false;
//element6.setAttribute("onclick","valueChecked(\"ply"+rowCount+"\",this)");
cell5.appendChild(element6);

var cell6 = row.insertCell(6);
var element7 = document.createElement("input");
element7.type = "checkbox";
element7.style.width="30px";
element7.id='int'+rowCount;
element7.value=false;
//element7.setAttribute("onclick","valueChecked(\"int"+rowCount+"\",this)");
cell6.appendChild(element7);

var cell7 = row.insertCell(7);
var element8 = document.createElement("input");
element8.type = "checkbox";
element8.style.width="30px";
element8.id='app'+rowCount;
element8.checked=true;
element8.setAttribute("onchange","onlyCheckAppOrCod('app',"+rowCount+")");
cell7.appendChild(element8);

var cell8 = row.insertCell(8);
var element9 = document.createElement("input");
element9.type = "checkbox";
element9.style.width="30px";
element9.id='cod'+rowCount;
element9.value=false;
element9.setAttribute("onchange","onlyCheckAppOrCod('cod',"+rowCount+")");
cell8.appendChild(element9);

}


function valueChecked(target,event){	
	if(event.checked){
		document.getElementById(target).value=true;
		}else{
			document.getElementById(target).value=false;
	}
}
function cratingValidation(){
	var rowCountvalue =document.forms['mssForm'].elements['rowCountvalue'].value;
	 for(var i=1;i<=rowCountvalue;i++){
	 var test=document.getElementById('tra'+i).value;
	 var des=document.getElementById('des'+i).value;
	 var len=document.getElementById('len'+i).value;
	 var wid=document.getElementById('wid'+i).value;
	 var hei=document.getElementById('hei'+i).value;
	 var ply=document.getElementById('ply'+i).checked;
	 var int=document.getElementById('int'+i).checked;
	 if(test==''){
		 alert("Please select O/D/B for line # "+i);
		 return false;
	 }
	 if(des==''){
		 alert("Please select Description for line # "+i);
		 return false;
	 }
	 if(len==''){
		 alert("Please select Length for line # "+i);
		 return false;
	 }
	 if(wid==''){
		 alert("Please select Width for line # "+i);
		 return false;
	 }
	 if(hei==''){
		 alert("Please select Height for line # "+i);
		 return false;
	 }
    if(ply==false && int==false){
		 alert("Please select INTL-ISPMIS or ply for line # "+i);
		 return false; 
	 }
	 }
	 return true;
}

function validationField(){	 
var submitAs =document.forms['mssForm'].elements['mss.submitAs'].value; 
var billingDivision =document.forms['mssForm'].elements['mss.billingDivision'].value ;
var affiliatedTo=document.forms['mssForm'].elements['mss.affiliatedTo'].value ;
var weight=document.forms['mssForm'].elements['mss.weight'].value ;
var checkedOption="";
var ss=cratingValidation();
if(ss){
if(submitAs==''){
checkedOption=checkedOption+" submit As";
}
if(billingDivision==''){
	if(checkedOption==''){
		checkedOption=checkedOption+" Billing Division";
	}else{
		checkedOption=checkedOption+" , Billing Division";
	}
}
if(affiliatedTo==''){
	if(checkedOption==''){
		checkedOption=checkedOption+" Affiliated To";	
	}else{
		checkedOption=checkedOption+" , Affiliated To";
	}
	
}
if(weight==''){
	if(checkedOption==''){
		checkedOption=checkedOption+" Weight";	
	}else{
		checkedOption=checkedOption+" , Weight";	
	}
}
if(checkedOption!=''){
	checkedOption="Please select "+checkedOption;
}
if(checkedOption!=''){
	alert(checkedOption);
	return false;
}
return true;
}
}
function saveMssRowList(type,tempId){
	var ss=validationField();
	if(ss){
	var transportListServer="";
    var descriptionListServer="";
   	var lengthListServer="";
   	var widthListServer="";
   	var heightListServer="";
   	var plyListServer="";
   	var intlIspmisListServer="";
   	var apprListServer="";
   	var codListServer="";
	 	     
   	var id='';	 
		   if(document.forms['mssForm'].transportList!=undefined){
		        if(document.forms['mssForm'].transportList.size!=0){
		 	      for (i=0; i<document.forms['mssForm'].transportList.length; i++){	
		 	    	 id=document.forms['mssForm'].transportList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(transportListServer==''){
	 	            	transportListServer=id+": "+document.forms['mssForm'].transportList[i].value;
	 	             }else{
	 	            	transportListServer= transportListServer+"~"+id+": "+document.forms['mssForm'].transportList[i].value;
	 	             }
		 	        }	
		 	      }else{
		 	    	 id=document.forms['mssForm'].transportList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	        transportListServer=id+": "+document.forms['mssForm'].transportList.value;
		 	      }	        
		       }  
		    if(document.forms['mssForm'].descriptionList!=undefined){
		        if(document.forms['mssForm'].descriptionList.size!=0){
		 	      for (i=0; i<document.forms['mssForm'].descriptionList.length; i++){	
		 	    	 id=document.forms['mssForm'].descriptionList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(descriptionListServer==''){
	 	            	descriptionListServer=id+": "+document.forms['mssForm'].descriptionList[i].value;
	 	             }else{
	 	            	descriptionListServer= descriptionListServer+"~"+id+": "+document.forms['mssForm'].descriptionList[i].value;
	 	             }
		 	        }	
		 	      }else{	
		 	    	 id=document.forms['mssForm'].descriptionList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	        descriptionListServer=id+": "+document.forms['mssForm'].descriptionList.value;
		 	      }	        
		       }
		    
		    if(document.forms['mssForm'].lengthList!=undefined){
		        if(document.forms['mssForm'].lengthList.length!=undefined){
		 	      for (i=0; i<document.forms['mssForm'].lengthList.length; i++){	        	           
	             	   id=document.forms['mssForm'].lengthList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             	   if(lengthListServer==''){
	             		  lengthListServer=id+": "+document.forms['mssForm'].lengthList[i].value;
	                   }else{
	                	   lengthListServer= lengthListServer+"~"+id+": "+document.forms['mssForm'].lengthList[i].value;
	                   }
		 	       }	
		 	     }else{	   
		 	          id=document.forms['mssForm'].lengthList.id;
		 	          id=id.replace(id.substring(0,3),'');     
		 	         lengthListServer=id+": "+document.forms['mssForm'].lengthList.value;
		 	     }	        
		 	 } 

		    if(document.forms['mssForm'].widthList!=undefined){
		        if(document.forms['mssForm'].widthList.length!=undefined){
		 	      for (i=0; i<document.forms['mssForm'].widthList.length; i++){	        	           
	             	   id=document.forms['mssForm'].widthList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             	   if(widthListServer==''){
	             		  widthListServer=id+": "+document.forms['mssForm'].widthList[i].value;
	                   }else{
	                	   widthListServer= widthListServer+"~"+id+": "+document.forms['mssForm'].widthList[i].value;
	                   }
		 	       }	
		 	     }else{	   
		 	          id=document.forms['mssForm'].widthList.id;
		 	          id=id.replace(id.substring(0,3),'');     
		 	         widthListServer=id+": "+document.forms['mssForm'].widthList.value;
		 	     }	        
		 	 } 

		    if(document.forms['mssForm'].heightList!=undefined){
		        if(document.forms['mssForm'].heightList.length!=undefined){
		 	      for (i=0; i<document.forms['mssForm'].heightList.length; i++){	        	           
	             	   id=document.forms['mssForm'].heightList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             	   if(heightListServer==''){
	             		  heightListServer=id+": "+document.forms['mssForm'].heightList[i].value;
	                   }else{
	                	   heightListServer= heightListServer+"~"+id+": "+document.forms['mssForm'].heightList[i].value;
	                   }
		 	       }	
		 	     }else{	   
		 	          id=document.forms['mssForm'].heightList.id;
		 	          id=id.replace(id.substring(0,3),'');     
		 	         heightListServer=id+": "+document.forms['mssForm'].heightList.value;
		 	     }	        
		 	 }			    

		    if(document.forms['mssForm'].plyList!=undefined){
		    	
		        if(document.forms['mssForm'].plyList.length!=undefined){		        	
		 	      for (i=0; i<document.forms['mssForm'].plyList.length; i++){	        	           
	             	   id=document.forms['mssForm'].plyList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             	   if(plyListServer==''){
	             		  plyListServer=id+": "+document.forms['mssForm'].plyList[i].checked;
	                   }else{
	                	   plyListServer= plyListServer+"~"+id+": "+document.forms['mssForm'].plyList[i].checked;
	                   }
		 	       }	
		 	     }else{	 
		 	    	  
		 	          id=document.forms['mssForm'].plyList.id;
		 	         
		 	          id=id.replace(id.substring(0,3),'');     
		 	         plyListServer=id+": "+document.forms['mssForm'].plyList.checked;
		 	     }	        
		 	 }

		    if(document.forms['mssForm'].intlIspmisList!=undefined){
		        if(document.forms['mssForm'].intlIspmisList.length!=undefined){
		 	      for (i=0; i<document.forms['mssForm'].intlIspmisList.length; i++){	        	           
	             	   id=document.forms['mssForm'].intlIspmisList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             	   if(intlIspmisListServer==''){
	             		  intlIspmisListServer=id+": "+document.forms['mssForm'].intlIspmisList[i].checked;
	                   }else{
	                	   intlIspmisListServer= intlIspmisListServer+"~"+id+": "+document.forms['mssForm'].intlIspmisList[i].checked;
	                	  
	                   }
		 	       }	
		 	     }else{	   
		 	          id=document.forms['mssForm'].intlIspmisList.id;
		 	          id=id.replace(id.substring(0,3),'');     
		 	         intlIspmisListServer=id+": "+document.forms['mssForm'].intlIspmisList.checked;
		 	     }	        
		 	 }

		    if(document.forms['mssForm'].apprList!=undefined){
		        if(document.forms['mssForm'].apprList.length!=undefined){
		 	      for (i=0; i<document.forms['mssForm'].apprList.length; i++){	        	           
	             	   id=document.forms['mssForm'].apprList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             	   if(apprListServer==''){
	             		  apprListServer=id+": "+document.forms['mssForm'].apprList[i].checked;
	                   }else{
	                	   apprListServer= apprListServer+"~"+id+": "+document.forms['mssForm'].apprList[i].checked;
	                   }
		 	       }	
		 	     }else{	   
		 	          id=document.forms['mssForm'].apprList.id;
		 	          id=id.replace(id.substring(0,3),'');     
		 	         apprListServer=id+": "+document.forms['mssForm'].apprList.checked;
		 	     }	        
		 	 }

		    if(document.forms['mssForm'].codList!=undefined){
		        if(document.forms['mssForm'].codList.length!=undefined){
		 	      for (i=0; i<document.forms['mssForm'].codList.length; i++){	        	           
	             	   id=document.forms['mssForm'].codList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             	   if(codListServer==''){
	             		  codListServer=id+": "+document.forms['mssForm'].codList[i].checked;
	                   }else{
	                	   codListServer= codListServer+"~"+id+": "+document.forms['mssForm'].codList[i].checked;
	                   }
		 	       }	
		 	     }else{	   
		 	          id=document.forms['mssForm'].codList.id;
		 	          id=id.replace(id.substring(0,3),'');     
		 	         codListServer=id+": "+document.forms['mssForm'].codList.checked;
		 	     }	        
		 	 }
		    
			 document.getElementById('transportListServer').value=transportListServer;
			 document.getElementById('descriptionListServer').value=descriptionListServer;  
			 document.getElementById('lengthListServer').value=lengthListServer;
			 document.getElementById('widthListServer').value=widthListServer;
			 document.getElementById('heightListServer').value=heightListServer;  
			 document.getElementById('plyListServer').value=plyListServer;
			 document.getElementById('intlIspmisListServer').value=intlIspmisListServer;
			 document.getElementById('apprListServer').value=apprListServer;  
			 document.getElementById('codListServer').value=codListServer; 		     		       

		     var mssRowlist="";
		     var newlineid=document.getElementById('newlineid').value;
		     if(newlineid!=''){
		            var arrayLine=newlineid.split("~");
		            for(var i=0;i<arrayLine.length;i++){		            	              
		                if(mssRowlist==''){
		                	mssRowlist=document.getElementById('tra'+arrayLine[i]).value+":"+document.getElementById('des'+arrayLine[i]).value+":"+document.getElementById('len'+arrayLine[i]).value+":"+document.getElementById('wid'+arrayLine[i]).value+" :"+document.getElementById('hei'+arrayLine[i]).value+" :"+document.getElementById('ply'+arrayLine[i]).checked+" :"+document.getElementById('int'+arrayLine[i]).checked+" :"+document.getElementById('app'+arrayLine[i]).checked+" :"+document.getElementById('cod'+arrayLine[i]).checked;
		                }else{
		                	mssRowlist=mssRowlist+"~"+document.getElementById('tra'+arrayLine[i]).value+" :"+document.getElementById('des'+arrayLine[i]).value+" :"+document.getElementById('len'+arrayLine[i]).value+" :"+document.getElementById('wid'+arrayLine[i]).value+" :"+document.getElementById('hei'+arrayLine[i]).value+" :"+document.getElementById('ply'+arrayLine[i]).checked+" :"+document.getElementById('int'+arrayLine[i]).checked+" :"+document.getElementById('app'+arrayLine[i]).checked+" :"+document.getElementById('cod'+arrayLine[i]).checked;
		                }
		              }
		         }
		     
		    document.getElementById('mssRowlist').value =mssRowlist;
		    	saveMssOriServiceList();
		    	saveMssDestServiceList();
			    document.forms['mssForm'].action="saveMss.html";
			    document.forms['mssForm'].submit();	 
	}
}
function confirmSubmit(gId,wid){
	var agree=confirm("Are you sure you wish to remove this Row ?");
	if (agree){
		location.href="deleteMssGridRow.html?gId="+gId+"&wid="+wid;
	}else{
		return false;
	}
}
function onlyCheckAppOrCod(fieldId,rowCount){
	document.getElementById('app'+rowCount).checked=false;
	document.getElementById('cod'+rowCount).checked=false;
	
	if(fieldId.match(/^[^\d]*$/)) {   // check any digit in fieldId exist or not
			document.getElementById(fieldId+rowCount).checked=true;		
		}else{
			document.getElementById(fieldId).checked=true;
		}	
}

function updateMssCratingGridAjax(rowCount,id,fieldId){
	
	var transportListServer=document.getElementById('tra'+rowCount).value;
	var descriptionListServer=document.getElementById('des'+rowCount).value;  
	var lengthListServer=document.getElementById('len'+rowCount).value;
	var widthListServer=document.getElementById('wid'+rowCount).value;
	var heightListServer=document.getElementById('hei'+rowCount).value;  
	var plyListServer=document.getElementById('ply'+rowCount).checked;
	var intlIspmisListServer=document.getElementById('int'+rowCount).checked;
	var apprListServer=document.getElementById('app'+rowCount).checked;  
	var codListServer=document.getElementById('cod'+rowCount).checked;	
	
	var url = "updateMssCratingGridAjax.html?ajax=1&id="+id+"&codListServer="+codListServer+"&transportListServer="+transportListServer+"&descriptionListServer="+descriptionListServer+"&lengthListServer="+lengthListServer+"&widthListServer="+widthListServer+"&heightListServer="+heightListServer+"&plyListServer="+plyListServer+"&intlIspmisListServer="+intlIspmisListServer+"&apprListServer="+apprListServer;
	  http2.open("GET", url, true); 
	  http2.onreadystatechange = handleHttpResponse2;
	  http2.send(null);	 
}

function handleHttpResponse2(){
	if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();     	  
	}
}
function getXMLHttpRequestObject()
{
  var xmlhttp;
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (e) {
      xmlhttp = false;
    }
  }
  return xmlhttp;
}
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
var http2 = getXMLHttpRequestObject();

function copyRow(tableID,id){
	var trans = document.getElementById('tra'+id).value;
	var descript = document.getElementById('des'+id).value;
	var length = document.getElementById('len'+id).value;
	var width = document.getElementById('wid'+id).value;
	var height = document.getElementById('hei'+id).value;
	var plys = document.getElementById('ply'+id).checked;
	var intls = document.getElementById('int'+id).checked;
	var apprs = document.getElementById('app'+id).checked;
	var cods = document.getElementById('cod'+id).checked;


	var table = document.getElementById(tableID);
    var rowCount = table.rows.length;
	var row = table.insertRow(rowCount); 
    var newlineid=document.getElementById('newlineid').value;              
    if(newlineid==''){
        newlineid=rowCount;
      }else{
      newlineid=newlineid+"~"+rowCount;
      }
    document.getElementById('newlineid').value=newlineid;
    
 	var cell15 = row.insertCell(0);
 	var element1 = document.createElement("select");
     element1.setAttribute("class", "list-menu" );
     element1.id="tra"+rowCount;     
     element1.style.width="100px" 
     var transportList='${transportTypeValue}';
     transportList=transportList.replace('[','').replace(']','');
     var transportarray=transportList.split(",");
    
     var optioneleBlankCategory= document.createElement("option");
     optioneleBlankCategory.value="";
     optioneleBlankCategory.text=""; 
     element1.options.add(optioneleBlankCategory); 
 	 for(var i=0;i<transportarray.length;i++){
 	   	var value=transportarray[i].split(",");	   	
 	    var optionele= document.createElement("option");
 	    optionele.text=value[0]; 	    
     element1.options.add(optionele);             
  } 
 	element1.value = trans;        	    
 	cell15.appendChild(element1);

var cell1 = row.insertCell(1);
var element2 = document.createElement("input");
element2.type = "text";
element2.style.width="200px";
element2.setAttribute("class", "input-text" );
element2.id='des'+rowCount;
element2.value = descript;
cell1.appendChild(element2);


var cell2 = row.insertCell(2);
var element3 = document.createElement("input");
element3.type = "text";
element3.style.width="100px";
element3.setAttribute("class", "input-text" );    
element3.id='len'+rowCount;
element3.value = length;
cell2.appendChild(element3);

var cell3 = row.insertCell(3);
var element4 = document.createElement("input");
element4.type = "text";
element4.style.width="100px";
element4.setAttribute("class", "input-text" );
element4.value = width;
element4.id='wid'+rowCount;
cell3.appendChild(element4);

var cell4 = row.insertCell(4);
var element5 = document.createElement("input");
element5.type = "text";
element5.style.width="100px";
element5.setAttribute("class", "input-text" );    
element5.id='hei'+rowCount;
element5.value = height;
cell4.appendChild(element5);


var cell5 = row.insertCell(5);
var element6 = document.createElement("input");
element6.type = "checkbox";
element6.style.width="30px";
element6.id='ply'+rowCount;
element6.checked=plys;
//element6.setAttribute("onclick","valueChecked(\"ply"+rowCount+"\",this)");
cell5.appendChild(element6);

var cell6 = row.insertCell(6);
var element7 = document.createElement("input");
element7.type = "checkbox";
element7.style.width="30px";
element7.id='int'+rowCount;
element7.checked=intls;
//element7.setAttribute("onclick","valueChecked(\"int"+rowCount+"\",this)");
cell6.appendChild(element7);

var cell7 = row.insertCell(7);
var element8 = document.createElement("input");
element8.type = "checkbox";
element8.style.width="30px";
element8.id='app'+rowCount;
element8.checked=apprs;
element8.setAttribute("onchange","onlyCheckAppOrCod('app',"+rowCount+")");
cell7.appendChild(element8);

var cell8 = row.insertCell(8);
var element9 = document.createElement("input");
element9.type = "checkbox";
element9.style.width="30px";
element9.id='cod'+rowCount;
element9.checked=cods;
element9.setAttribute("onchange","onlyCheckAppOrCod('cod',"+rowCount+")");
cell8.appendChild(element9); 

var cell9 = row.insertCell(9);
var a = document.createElement('a');
var linkText = document.createTextNode("Copy");
a.appendChild(linkText);
a.title = "Copy";
a.setAttribute ("onclick","copyRow('dataTable',"+rowCount+")");
cell9.appendChild(a);
}
</script>
<script type="text/javascript">
var flagValue = 0;
var val = 0;
var lastId ='';
var origin='';
function displayResultOrigin(originCat,controlFlag,position,id){	
		var table=document.getElementById("originCat");
		var rownum = document.getElementById(id);
		lastId = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val = newrow+1;
		var row=table.insertRow(val);
		var rowId = "ori-"+val;
		row.setAttribute("id",rowId);		 
		 
		new Ajax.Request('findOriChildRecord.html?ajax=1&decorator=simple&popup=true&parentCode='+originCat+'&decorator=simple&popup=true',
				  {
			
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				     origin=originCat.split(" ",1);
				      var container = document.getElementById(origin);
				    
				      container.innerHTML = response;
				      container.show();
				     
				    },
				    onFailure: function(){
					    //alert('Something went wrong...') 
					    }
				  });
		    origin=originCat.split(" ",1);
		row.innerHTML = "<td colspan=\"7\"><div id="+origin+"></div></td>";
		var mdown = position.id.replace("mdown","mup");
		  var mup = document.getElementById(mdown);
		  
		  mup.style.display = "block";	  
		  position.style.display = "none";
	}

function closeDisplayResultOrigin(object){
	var myrow = object.parentNode.parentNode;
	var next = myrow.nextElementSibling; 
	var newrow = myrow.parentNode.rowIndex;
	next.parentNode.removeChild(next);
	 var plusId = object.id.replace("mup","mdown");
	 var mup = document.getElementById(plusId);
	 mup.style.display = "block"; 
	 object.style.display = "none";
}

</script>
<script type="text/javascript">
function addRowOrigin(tableId) {
	
    var table = document.getElementById(tableId);
    var rowCount = table.rows.length;
	var row = table.insertRow(rowCount); 
    var newlineid=document.getElementById('newlineIdOri').value;
    if(newlineid==''){
        newlineid=rowCount;
      }else{
      	newlineid=newlineid+"~"+rowCount;
      }
    document.getElementById('newlineIdOri').value=newlineid;
    
 	
	var cell1 = row.insertCell(0);
	var element1 = document.createElement("input");
	element1.type = "text";
	element1.style.width="200px";
	element1.setAttribute("class", "input-text" );
	element1.setAttribute("readonly","true");
	element1.id='ori'+rowCount;
	element1.name='oriItemList';
	element1.setAttribute("onkeydown","onlyDel(event,this)");
	cell1.appendChild(element1);


	var cell2 = row.insertCell(1);
	var element2 = document.createElement("input");
	element2.type = "checkbox";
	element2.style.width="30px";
	element2.id='orp'+rowCount;
	element2.name='oriApprList';
	element2.checked=true;
	element2.setAttribute("onclick","checkOriAppOrCod("+rowCount+",this,'orp')");
	cell2.appendChild(element2);


	var cell3 = row.insertCell(2);
	var element3 = document.createElement("input");
	element3.type = "checkbox";
	element3.style.width="30px";
	element3.id='orc'+rowCount;
	element3.name='oriCodList';
	element3.setAttribute("onclick","checkOriAppOrCod("+rowCount+",this,'orc')");
	cell3.appendChild(element3);

}

function saveMssOriServiceList(){

    var oriItemListServer="";   	
   	var oriApprListServer="";
   	var oriCodListServer="";
	 	     
   	var id='';	 
		   
		    if(document.forms['mssForm'].oriItemList!=undefined){
		        if(document.forms['mssForm'].oriItemList.size!=0){
		 	      for (i=0; i<document.forms['mssForm'].oriItemList.length; i++){	
		 	    	 id=document.forms['mssForm'].oriItemList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(oriItemListServer==''){
	 	            	oriItemListServer=id+": "+document.forms['mssForm'].oriItemList[i].value;
	 	             }else{
	 	            	oriItemListServer= oriItemListServer+"~"+id+": "+document.forms['mssForm'].oriItemList[i].value;
	 	             }
		 	        }	
		 	      }else{	
		 	    	 id=document.forms['mssForm'].oriItemList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	        oriItemListServer=id+": "+document.forms['mssForm'].oriItemList.value;
		 	      }	        
		       }		   

		    if(document.forms['mssForm'].oriApprList!=undefined){
		        if(document.forms['mssForm'].oriApprList.length!=undefined){
		 	      for (i=0; i<document.forms['mssForm'].oriApprList.length; i++){	        	           
	             	   id=document.forms['mssForm'].oriApprList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             	   if(oriApprListServer==''){
	             		  oriApprListServer=id+": "+document.forms['mssForm'].oriApprList[i].checked;
	                   }else{
	                	   oriApprListServer= oriApprListServer+"~"+id+": "+document.forms['mssForm'].oriApprList[i].checked;
	                   }
		 	       }	
		 	     }else{	   
		 	          id=document.forms['mssForm'].oriApprList.id;
		 	          id=id.replace(id.substring(0,3),'');     
		 	         oriApprListServer=id+": "+document.forms['mssForm'].oriApprList.checked;
		 	     }	        
		 	 }

		    if(document.forms['mssForm'].oriCodList!=undefined){
		        if(document.forms['mssForm'].oriCodList.length!=undefined){
		 	      for (i=0; i<document.forms['mssForm'].oriCodList.length; i++){	        	           
	             	   id=document.forms['mssForm'].oriCodList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             	   if(oriCodListServer==''){
	             		  oriCodListServer=id+": "+document.forms['mssForm'].oriCodList[i].checked;
	                   }else{
	                	   oriCodListServer= oriCodListServer+"~"+id+": "+document.forms['mssForm'].oriCodList[i].checked;
	                   }
		 	       }	
		 	     }else{	   
		 	          id=document.forms['mssForm'].oriCodList.id;
		 	          id=id.replace(id.substring(0,3),'');     
		 	         oriCodListServer=id+": "+document.forms['mssForm'].oriCodList.checked;
		 	     }	        
		 	 }
		    
			 document.getElementById('oriItemListServer').value=oriItemListServer;			 
			 document.getElementById('oriApprListServer').value=oriApprListServer;  
			 document.getElementById('oriCodListServer').value=oriCodListServer; 		     		       

		     var oriServicelist="";
		     var newlineid=document.getElementById('newlineIdOri').value;
		     if(newlineid!=''){
		            var arrayLine=newlineid.split("~");
		            for(var i=0;i<arrayLine.length;i++){		            	              
		                if(oriServicelist==''){
		                	oriServicelist=document.getElementById('ori'+arrayLine[i]).value+":"+document.getElementById('orp'+arrayLine[i]).checked+" :"+document.getElementById('orc'+arrayLine[i]).checked;
		                }else{
		                	oriServicelist=oriServicelist+"~"+document.getElementById('ori'+arrayLine[i]).value+" :"+document.getElementById('orp'+arrayLine[i]).checked+" :"+document.getElementById('orc'+arrayLine[i]).checked;
		                }
		              }
		         }
		     
		    document.getElementById('oriServicelist').value =oriServicelist;			   
}

function checkOriAppOrCod(id,target,type){	
	if(target.checked){
		if(type=='orc'){
			document.getElementById('orp'+id).checked=false;
		}else{
			document.getElementById('orc'+id).checked=false;
		}
	}else{
		if(type=='orc'){
			document.getElementById('orp'+id).checked=true;
		}else{
			document.getElementById('orc'+id).checked=true;
		}
	}
}
function deletrOriSerRow(oId,wid){
	var agree=confirm("Are you sure you wish to remove this Row ?");
	if (agree){
		location.href="deleteOriServiceRow.html?oId="+oId+"&wid="+wid;
	}else{
		return false;
	}
}
function goToOriginChildDetails(childName){ 
	childName=childName.id
    if(document.forms['mssForm'].oriItemList!=undefined){
        if(document.forms['mssForm'].oriItemList.size!=0){
        	if(document.forms['mssForm'].oriItemList.length!=undefined){
		 	      for (i=0; i<document.forms['mssForm'].oriItemList.length; i++){	
		 	 	      if(document.forms['mssForm'].oriItemList[i].value=='')
			             document.forms['mssForm'].oriItemList[i].value=childName;
		 	        }
        	 }else{	
        		 if(document.forms['mssForm'].oriItemList.value=='')   
        		 document.forms['mssForm'].oriItemList.value=childName;
	         }	
 	      }else{
 	    	 if(document.forms['mssForm'].oriItemList[i].value=='')	
 	       document.forms['mssForm'].oriItemList.value=childName;
 	      }	 
       }
   
}
</script>
<script type="text/javascript">
var flagValue = 0;
var val = 0;
var lastId ='';
var destination='';
function displayResultDestination(destinationCat,controlFlag,position,id){
			
		var table=document.getElementById("destinationCat");
		var rownum = document.getElementById(id);
		lastId = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val = newrow+1;
		var row=table.insertRow(val);
		var rowId = "des-"+val;
		row.setAttribute("id",rowId);		 
		 
		new Ajax.Request('findDestinationChildRecord.html?ajax=1&decorator=simple&popup=true&parentCode='+destinationCat+'&decorator=simple&popup=true',
				  {
			
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      destination=destinationCat.split(" ",1);
				      var container = document.getElementById(destination);
				    
				      container.innerHTML = response;
				      container.show();
				     
				    },
				    onFailure: function(){
					    //alert('Something went wrong...') 
					    }
				  });
		destination=destinationCat.split(" ",1);
		row.innerHTML = "<td colspan=\"7\"><div id="+destination+"></div></td>";		   
		var mdown1 = position.id.replace("mdown1","mup1");
		  var mup1 = document.getElementById(mdown1);
		  
		  mup1.style.display = "block";	  
		  position.style.display = "none";
	}

function closeDisplayResultDestination(object){
	var myrow = object.parentNode.parentNode;
	var next = myrow.nextElementSibling; 
	var newrow = myrow.parentNode.rowIndex;
	next.parentNode.removeChild(next);
	 var plusId = object.id.replace("mup1","mdown1");
	 var mup1 = document.getElementById(plusId);
	 mup1.style.display = "block"; 
	 object.style.display = "none";
}

function addRowDestination(tableId) {
	
    var table = document.getElementById(tableId);
    var rowCount = table.rows.length;
	var row = table.insertRow(rowCount); 
    var newlineid=document.getElementById('newlineIdDes').value;              
    if(newlineid==''){
        newlineid=rowCount;
      }else{
      	newlineid=newlineid+"~"+rowCount;
      }
    document.getElementById('newlineIdDes').value=newlineid;
    
 	
	var cell1 = row.insertCell(0);
	var element1 = document.createElement("input");
	element1.type = "text";
	element1.style.width="200px";
	element1.setAttribute("class", "input-text" );
	element1.id='dei'+rowCount;
	element1.name='destItemList';
	element1.setAttribute("onkeydown","onlyDel(event,this)");
	cell1.appendChild(element1);


	var cell2 = row.insertCell(1);
	var element2 = document.createElement("input");
	element2.type = "checkbox";
	element2.style.width="30px";
	element2.id='dep'+rowCount;
	element2.name='destApprList';
	element2.checked=true;
	element2.setAttribute("onclick","checkDesAppOrCod("+rowCount+",this,'dep')");
	cell2.appendChild(element2);


	var cell3 = row.insertCell(2);
	var element3 = document.createElement("input");
	element3.type = "checkbox";
	element3.style.width="30px";
	element3.id='dec'+rowCount;
	element3.name='destCodList';
	element3.setAttribute("onclick","checkDesAppOrCod("+rowCount+",this,'dec')");
	cell3.appendChild(element3);

}

function saveMssDestServiceList(){

    var destItemListServer="";   	
   	var destApprListServer="";
   	var destCodListServer="";
	 	     
   	var id='';	 
		   
		    if(document.forms['mssForm'].destItemList!=undefined){
		        if(document.forms['mssForm'].destItemList.size!=0){
		 	      for (i=0; i<document.forms['mssForm'].destItemList.length; i++){	
		 	    	 id=document.forms['mssForm'].destItemList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(destItemListServer==''){
	 	            	destItemListServer=id+": "+document.forms['mssForm'].destItemList[i].value;
	 	             }else{
	 	            	destItemListServer= destItemListServer+"~"+id+": "+document.forms['mssForm'].destItemList[i].value;
	 	             }
		 	        }	
		 	      }else{	
		 	    	 id=document.forms['mssForm'].destItemList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	        destItemListServer=id+": "+document.forms['mssForm'].destItemList.value;
		 	      }	        
		       }		   

		    if(document.forms['mssForm'].destApprList!=undefined){
		        if(document.forms['mssForm'].destApprList.length!=undefined){
		 	      for (i=0; i<document.forms['mssForm'].destApprList.length; i++){	        	           
	             	   id=document.forms['mssForm'].destApprList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             	   if(destApprListServer==''){
	             		  destApprListServer=id+": "+document.forms['mssForm'].destApprList[i].checked;
	                   }else{
	                	   destApprListServer= destApprListServer+"~"+id+": "+document.forms['mssForm'].destApprList[i].checked;
	                   }
		 	       }	
		 	     }else{	   
		 	          id=document.forms['mssForm'].destApprList.id;
		 	          id=id.replace(id.substring(0,3),'');     
		 	         destApprListServer=id+": "+document.forms['mssForm'].destApprList.checked;
		 	     }	        
		 	 }

		    if(document.forms['mssForm'].destCodList!=undefined){
		        if(document.forms['mssForm'].destCodList.length!=undefined){
		 	      for (i=0; i<document.forms['mssForm'].destCodList.length; i++){	        	           
	             	   id=document.forms['mssForm'].destCodList[i].id;
	                   id=id.replace(id.substring(0,3),'');
	             	   if(destCodListServer==''){
	             		  destCodListServer=id+": "+document.forms['mssForm'].destCodList[i].checked;
	                   }else{
	                	   destCodListServer= destCodListServer+"~"+id+": "+document.forms['mssForm'].destCodList[i].checked;
	                   }
		 	       }	
		 	     }else{	   
		 	          id=document.forms['mssForm'].destCodList.id;
		 	          id=id.replace(id.substring(0,3),'');     
		 	         destCodListServer=id+": "+document.forms['mssForm'].destCodList.checked;
		 	     }	        
		 	 }
		    
			 document.getElementById('destItemListServer').value=destItemListServer;			 
			 document.getElementById('destApprListServer').value=destApprListServer;  
			 document.getElementById('destCodListServer').value=destCodListServer; 		     		       

		     var desServicelist="";
		     var newlineid=document.getElementById('newlineIdDes').value;
		     if(newlineid!=''){
		            var arrayLine=newlineid.split("~");
		            for(var i=0;i<arrayLine.length;i++){		            	              
		                if(desServicelist==''){
		                	desServicelist=document.getElementById('dei'+arrayLine[i]).value+":"+document.getElementById('dep'+arrayLine[i]).checked+" :"+document.getElementById('dec'+arrayLine[i]).checked;
		                }else{
		                	desServicelist=desServicelist+"~"+document.getElementById('dei'+arrayLine[i]).value+" :"+document.getElementById('dep'+arrayLine[i]).checked+" :"+document.getElementById('dec'+arrayLine[i]).checked;
		                }
		              }
		         }
		     
		    document.getElementById('desServicelist').value =desServicelist;			   
}

function checkDesAppOrCod(id,target,type){	
	if(target.checked){
		if(type=='dec'){
			document.getElementById('dep'+id).checked=false;
		}else{
			document.getElementById('dec'+id).checked=false;
		}
	}else{
		if(type=='dec'){
			document.getElementById('dep'+id).checked=true;
		}else{
			document.getElementById('dec'+id).checked=true;
		}
	}
}
function deletrDestSerRow(dId,wid){
	var agree=confirm("Are you sure you wish to remove this Row ?");
		if (agree){
			location.href="deleteDestServiceRow.html?dId="+dId+"&wid="+wid;
		}else{
			return false;
	}
}

function goToDestinationChildDetails(destChildName){
	destChildName=destChildName.id
	if(document.forms['mssForm'].destItemList!=undefined){
        if(document.forms['mssForm'].destItemList.size!=0){
        	if(document.forms['mssForm'].destItemList.length!=undefined){
		 	      for (i=0; i<document.forms['mssForm'].destItemList.length; i++){	
		 	 	      if(document.forms['mssForm'].destItemList[i].value=='')
			             document.forms['mssForm'].destItemList[i].value=destChildName;
		 	        }
        	}else{	
	       		 if(document.forms['mssForm'].destItemList.value=='')   
	       		 document.forms['mssForm'].destItemList.value=destChildName;
	         }		
 	      }else{
 	    	 if(document.forms['mssForm'].destItemList[i].value=='')	
 	       document.forms['mssForm'].destItemList.value=destChildName;
 	      }	        
       }	
}
function checkTransValueOrigin(targetElement){
	var value = targetElement.value;	
	var ori = document.getElementById("OriServ");
		if(value=='O' || value=='B'){
			ori.style.display="block";		
		}else{
			ori.style.display="none";
	}	
}
function alertMsgOrigin(){
	var tRadio = document.getElementsByName('mss.transportTypeRadio');
    var des='';
	for (var i = 0, length = tRadio.length; i < length; i++) {
	    if (tRadio[i].checked) {
	        des=tRadio[i].value;		        
		}
	}
	if(des=='' || des=='D'){
		alert('Please select Service type Origin or Both.');
	}
}
function alertMsgDestination(){
	var tRadio = document.getElementsByName('mss.transportTypeRadio');
    var des='';
	for (var i = 0, length = tRadio.length; i < length; i++) {
	    if (tRadio[i].checked) {
	        des=tRadio[i].value;		        
		}
	}
	if(des=='' || des=='O'){
		alert('Please select Service type Destination or Both.');
	}
}
function checkTransValueDestination(targetElement){
	var value = targetElement.value;
	var dest = document.getElementById("DestServ");
		if(value=='D' || value=='B'){
			dest.style.display="block";		
		}else{
			dest.style.display="none";
		}	
}
function checkMandatoryPricePreview(){
	var vip			=	document.forms['mssForm'].elements['mss.vip'].value;
	var pOrderNum	=	document.forms['mssForm'].elements['mss.purchaseOrderNumber'].value;
	var bDivision	=	document.forms['mssForm'].elements['bDivision'].value;
	var affTo 		=	document.forms['mssForm'].elements['affTo'].value;
	var loadSDate	=	document.forms['mssForm'].elements['loadSDate'].value;
	var packSDate	=	document.forms['mssForm'].elements['packSDate'].value;
		
	var weight		=	document.forms['mssForm'].elements['weight'].value;	
	var fName		=	document.forms['mssForm'].elements['mss.shipperFirstName'].value;
	var lName		=	document.forms['mssForm'].elements['mss.shipperLastName'].value;
	var checkedOption="";
/*	if(vip=='false'){
	checkedOption=checkedOption+" Vip at C/F.";
	}*/
	if(pOrderNum==''){
     if(checkedOption==''){
    	 checkedOption=checkedOption+" Ship Number";
        }else{
        	 checkedOption=checkedOption+" , Ship Number";	
        }
	}
    if(bDivision==''){
    	 if(checkedOption==''){
        	 checkedOption=checkedOption+" Bill To Division";
            }else{
            	 checkedOption=checkedOption+" , Bill To Division";	
            }
	} 
    if(affTo==''){
    	 if(checkedOption==''){
        	 checkedOption=checkedOption+" Affliated To";
            }else{
            	 checkedOption=checkedOption+" , Affliated To";	
            }
	}
	if(loadSDate==''){
		 if(checkedOption==''){
        	 checkedOption=checkedOption+" Load Start Date";
            }else{
            checkedOption=checkedOption+" , Load Start Date";	
            }
	}
    if(packSDate==''){
    	if(checkedOption==''){
       	 checkedOption=checkedOption+" Pack Start Date";
           }else{
           checkedOption=checkedOption+" , Pack Start Date";	
           }
	}
    if(weight==''){
    	if(checkedOption==''){
          	 checkedOption=checkedOption+" Weight";
              }else{
              checkedOption=checkedOption+" , Weight";	
              }
	}
    if(fName==''){
    	if(checkedOption==''){
         	 checkedOption=checkedOption+" First Name at S/O";
             }else{
             checkedOption=checkedOption+" , First Name at S/O";	
             }
	}
    if(lName==''){
      	if(checkedOption==''){
        	 checkedOption=checkedOption+" Last Name at S/O";
            }else{
            checkedOption=checkedOption+" , Last Name at S/O";	
            }
	}
    
    if(checkedOption!=''){
    	checkedOption="Please select "+checkedOption;
    }
    if(checkedOption!=''){
    	alert(checkedOption);
    	return false;
    }

	return true;
}
function validatePricePreview(){
	var ss=checkMandatoryPricePreview();
	if(ss){
	var oriAdd1		=	document.forms['mssForm'].elements['mss.shipperOriginAddress1'].value;
	var oriAdd2		=	document.forms['mssForm'].elements['mss.shipperOriginAddress2'].value;
	var oriCity		=	document.forms['mssForm'].elements['mss.shipperOriginCity'].value;
	var oriState	=	document.forms['mssForm'].elements['mss.shipperOriginState'].value;
	var oriZip		=	document.forms['mssForm'].elements['mss.shipperOriginZip'].value;
	var reqestODate	=	document.forms['mssForm'].elements['mss.requestedOriginDate'].value;
	
	var destAdd1	=	document.forms['mssForm'].elements['mss.shipperDestinationAddress1'].value;
	var destAdd2	=	document.forms['mssForm'].elements['mss.shipperDestinationAddress2'].value;
	var destCity	=	document.forms['mssForm'].elements['mss.shipperDestinationCity'].value;
	var destState	=	document.forms['mssForm'].elements['mss.shipperDestinationState'].value;
	var destZip		=	document.forms['mssForm'].elements['mss.shipperDestinationZip'].value;
	var reqDesDate	=	document.forms['mssForm'].elements['mss.requestedDestinationDate'].value;
	var delSDate	=	document.forms['mssForm'].elements['mss.deliveryStartDate'].value;
	
	var radioType 	= 	document.forms['mssForm'].elements['transRadio'].value;    
	var checkedOption="";
	if(radioType==''){
		alert("Please select Service type and Save.");
		return false;
	}else if(radioType=='B'){
				if(oriAdd1==''){
					checkedOption=checkedOption+" Origin Address1 at Work Ticket.";
				}
			/*	if(oriAdd2==''){
				 	if(checkedOption==''){
			        	 checkedOption=checkedOption+" Origin Address2 at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Origin Address2 at Work Ticket";	
			            }
				}*/
				if(oriCity==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Origin City at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Origin City at Work Ticket";	
			            }
				}
				if(oriState==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Origin State at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Origin State at Work Ticket";	
			            }
				}
				if(oriZip==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Origin Postal Code at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Origin Postal Code at Work Ticket";	
			            }
				}
				if(reqestODate==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+"   Requested Origin Date";
			            }else{
			            checkedOption=checkedOption+" ,   Requested Origin Date";	
			            }
				}
				if(destAdd1==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Destination Address1 at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Destination Address1 at Work Ticket";	
			            }
				}
			/*	if(destAdd2==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Destination Address2 at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Destination Address2 at Work Ticket";	
			            }
				}*/
				if(destCity==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Destination City at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Destination City at Work Ticket";	
			            }
				}
				if(destState==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Destination State at Work Tickett";
			            }else{
			            checkedOption=checkedOption+" , Destination State at Work Ticket";	
			            }
				}
				if(destZip==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Destination Postal Code at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Destination Postal Code at Work Ticket";	
			            }
				}
				if(reqDesDate==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+"  Requested Destination Date";
			            }else{
			            checkedOption=checkedOption+" ,  Requested Destination Date";	
			            }
				}
				if(delSDate==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Delivery Start Date";
			            }else{
			            checkedOption=checkedOption+" , Delivery Start Date";	
			            }
				}
				   if(checkedOption!=''){
				    	checkedOption="Please select  "+checkedOption;
				    }
				    if(checkedOption!=''){
				    	alert(checkedOption);
				    	return false;
				    }	
				return true;
				
	}else if(radioType=='O'){
				if(oriAdd1==''){
					checkedOption=checkedOption+" Origin Address1 at Work Ticket.";
				}
			/*	if(oriAdd2==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Origin Address2 at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Origin Address2 at Work Ticket";	
			            }
				}*/
				if(oriCity==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Origin City at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Origin City at Work Ticket";	
			            }
				}
				if(oriState==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Origin State at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Origin State at Work Ticket";	
			            }
				}
				if(oriZip==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Origin Postal Code at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Origin Postal Code at Work Ticket";	
			            }
				}
				if(reqestODate==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+"   Requested Origin Date";
			            }else{
			            checkedOption=checkedOption+" ,   Requested Origin Date";	
			            }
				}
				if(checkedOption!=''){
			    	checkedOption="Please select "+checkedOption;
			    }
			    if(checkedOption!=''){
			    	alert(checkedOption);
			    	return false;
			    }
					return true;
				
	}else if(radioType=='D'){
				if(destAdd1==''){
					checkedOption=checkedOption+" Destination Address1 at Work Ticket.";
				
				}
			/*	if(destAdd2==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Destination Address2 at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Destination Address2 at Work Ticket";	
			            }
				}*/
				if(destCity==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Destination City at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Destination City at Work Ticket";	
			            }
				}
				if(destState==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Destination State at Work Tickett";
			            }else{
			            checkedOption=checkedOption+" , Destination State at Work Ticket";	
			            }
				}
				if(destZip==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Destination Postal Code at Work Ticket";
			            }else{
			            checkedOption=checkedOption+" , Destination Postal Code at Work Ticket";	
			            }
				}
				if(reqDesDate==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+"  Requested Destination Date";
			            }else{
			            checkedOption=checkedOption+" ,  Requested Destination Date";	
			            }
				}
				if(delSDate==''){
					if(checkedOption==''){
			        	 checkedOption=checkedOption+" Delivery Start Date";
			            }else{
			            checkedOption=checkedOption+" , Delivery Start Date";	
			            }				
				}
				if(checkedOption!=''){
			    	checkedOption="Please select "+checkedOption;
			    }
			    if(checkedOption!=''){
			    	alert(checkedOption);
			    	return false;
			    }	
	return true;
	}else{
		return true;
	}
}else{
	return false;
}
}


function findQuotePDFValuesAjax(){
	var ab=validatePricePreview();
	var id='${mss.id}';
	if(ab){
		//alert(id)
		 new Ajax.Request('findQuotePDFValuesAjax.html?id='+id+'&decorator=simple&popup=true',
				  {
				    method:'get',
				    onSuccess: function(transport){
				     window.location.reload();
				    //showOrHideAutoSave(0);
				    },
				    
				    onLoading: function()
				       {
				    	//window.location.reload();
				    },
				    
				    onFailure: function(){ 
					    //alert('Something went wrong...')
					     }
				  });
	}
		
	}

function findImportFromVoxme(){
	var id='${mss.id}';
	var wid='${workTicket.id}';
if(id!=''){
	showOrHideAutoSave(1);
$.ajax({
	  type: "POST",
	  url: "findImportFromVoxme.html?ajax=1&inventoryFlag=True&decorator=simple&popup=true",
	  data: { id: id, wid:wid},
	  success: function (data, textStatus, jqXHR) {
		  var data1='';
		  data1=encodeURI(data);
		  if(data1.indexOf('y') > -1)
		  {
			  showOrHideAutoSave(0);
			  var agree = confirm("Do you want to import crating items?");
			  if(agree){
			  location.reload(true);
		  }}
		  else{
			  showOrHideAutoSave(0);
			  alert("No crating items have found.");
			  return false;
		  }  
	  },
   error: function (data, textStatus, jqXHR) {
		    	//showOrHide(0);
			}
	});

}
}
function placeOrderNumber(){
	var ab=validatePricePreview();
	var id='${mss.id}';
	if(ab){
		//alert(id)
		 new Ajax.Request('placeOrderNumberValuesAjax.html?id='+id+'&decorator=simple&popup=true',
				  {
				    method:'get',
				    onSuccess: function(transport){
				     window.location.reload();
				    //showOrHideAutoSave(0);
				    },
				    
				    onLoading: function()
				       {
				    	//window.location.reload();
				    },
				    
				    onFailure: function(){ 
					    //alert('Something went wrong...')
					     }
				  });	
	}
	
}
</script>
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
try{
	 document.forms['mssForm'].elements['delieveryStartDate'].value=document.forms['mssForm'].elements['mss.deliveryStartDate'].value;
	 document.forms['mssForm'].elements['loadingEndDate'].value = document.forms['mssForm'].elements['mss.loadingEndDate'].value;
	 document.forms['mssForm'].elements['packingEndDate'].value = document.forms['mssForm'].elements['mss.packingEndDate'].value ;
	 document.forms['mssForm'].elements['deliveryEndDate'].value = document.forms['mssForm'].elements['mss.deliveryEndDate'].value ;
	 document.forms['mssForm'].elements['requestedOriginDate'].value = document.forms['mssForm'].elements['mss.requestedOriginDate'].value;
	 document.forms['mssForm'].elements['requestedDestinationDate'].value = document.forms['mssForm'].elements['mss.requestedDestinationDate'].value ;
	 document.forms['mssForm'].elements['loadingStartDate'].value = document.forms['mssForm'].elements['mss.loadingStartDate'].value ;
	 document.forms['mssForm'].elements['packingStartDate'].value = document.forms['mssForm'].elements['mss.packingStartDate'].value;
}catch(e){}
try{	
	var tRadio = document.getElementsByName('mss.transportTypeRadio');
    var des='';
	for (var i = 0, length = tRadio.length; i < length; i++) {
	    if (tRadio[i].checked) {
	        des=tRadio[i].value;		        
		}
	}
var ori = document.getElementById("OriServ");
	if(des=='O' || des=='B'){
		ori.style.display="block";
	}else{
		ori.style.display="none";
    }	
}catch(e){}

try{	
	var tRadio = document.getElementsByName('mss.transportTypeRadio');		
    var des='';
	for (var i = 0, length = tRadio.length; i < length; i++) {
	    if (tRadio[i].checked) {
	        des=tRadio[i].value;		        
		}
	}
var dest = document.getElementById("DestServ");
	if(des=='D' || des=='B'){
		dest.style.display="block";		
	}else{
		dest.style.display="none";		        
}	
}catch(e){}
</script>
<script type="text/javascript">
var weight=document.forms['mssForm'].elements['mss.weight'].value;
if(weight=='0'){
	document.forms['mssForm'].elements['mss.weight'].value='';
}
</script>
<script type="text/javascript">
function CratingGridPagination(tableName, itemsPerPage) {
  this.tableName = tableName;
  this.itemsPerPage = itemsPerPage;
  this.currentPage = 1;
  this.pages = 0;
  this.inited = false;
  
  this.showRecords = function(from, to) {        
      var rows = document.getElementById(tableName).rows;
      // i starts from 1 to skip table header row
      for (var i = 1; i < rows.length; i++) {
          if (i < from || i > to)  
              rows[i].style.display = 'none';
          else
              rows[i].style.display = '';
      }
  }
  
  this.showPage = function(pageNumber) {
  	if (! this.inited) {
  		alert("not inited");
  		return;
  	}

      var oldPageAnchor = document.getElementById('pg_Crating'+this.currentPage);
      oldPageAnchor.className = 'pg-normal';
      
      this.currentPage = pageNumber;
      var newPageAnchor = document.getElementById('pg_Crating'+this.currentPage);
      newPageAnchor.className = 'pg-selected';
      
      var from = (pageNumber - 1) * itemsPerPage + 1;
      var to = from + itemsPerPage - 1;
      this.showRecords(from, to);
  }   
  
  this.prev = function() {
      if (this.currentPage > 1)
          this.showPage(this.currentPage - 1);
  }
  
  this.next = function() {
      if (this.currentPage < this.pages) {
          this.showPage(this.currentPage + 1);
      }
  }                        
  
  this.init = function() {
      var rows = document.getElementById(tableName).rows;
      var records = (rows.length - 1); 
      this.pages = Math.ceil(records / itemsPerPage);
      this.inited = true;
  }

  this.showPageNav = function(pagerName, positionId) {
  	if (! this.inited) {
  		alert("not inited");
  		return;
  	}
  	var element = document.getElementById(positionId);
  	
  	var pagerHtml = '<span onclick="' + pagerName + '.prev();" class="pg-normal"> &#171 Prev </span> | ';
      for (var page = 1; page <= this.pages; page++) 
          pagerHtml += '<span id="pg_Crating' + page + '" class="pg-normal" onclick="' + pagerName + '.showPage(' + page + ');">' + page + '</span> | ';
      pagerHtml += '<span onclick="'+pagerName+'.next();" class="pg-normal"> Next &#187;</span>';            
      
      element.innerHTML = pagerHtml;
  }
}
</script> 
<script type="text/javascript">
var cGridPage = new CratingGridPagination('dataTable', 5); 
cGridPage.init(); 
cGridPage.showPageNav('cGridPage', 'pageNavPosition'); 
cGridPage.showPage(1);
</script>

<script type="text/javascript">
function OriServGridPagination(tableName, itemsPerPage) {
  this.tableName = tableName;
  this.itemsPerPage = itemsPerPage;
  this.currentPage = 1;
  this.pages = 0;
  this.inited = false;
  
  this.showRecords = function(from, to) {        
      var rows = document.getElementById(tableName).rows;
      // i starts from 1 to skip table header row
      for (var i = 1; i < rows.length; i++) {
          if (i < from || i > to)  
              rows[i].style.display = 'none';
          else
              rows[i].style.display = '';
      }
  }
  
  this.showPage = function(pageNumber) {
  	if (! this.inited) {
  		alert("not inited");
  		return;
  	}

      var oldPageAnchor = document.getElementById('pg_Ori'+this.currentPage);
      oldPageAnchor.className = 'pg-normal';
      
      this.currentPage = pageNumber;
      var newPageAnchor = document.getElementById('pg_Ori'+this.currentPage);
      newPageAnchor.className = 'pg-selected';
      
      var from = (pageNumber - 1) * itemsPerPage + 1;
      var to = from + itemsPerPage - 1;
      this.showRecords(from, to);
  }   
  
  this.prev = function() {
      if (this.currentPage > 1)
          this.showPage(this.currentPage - 1);
  }
  
  this.next = function() {
      if (this.currentPage < this.pages) {
          this.showPage(this.currentPage + 1);
      }
  }                        
  
  this.init = function() {
      var rows = document.getElementById(tableName).rows;
      var records = (rows.length - 1); 
      this.pages = Math.ceil(records / itemsPerPage);
      this.inited = true;
  }

  this.showPageNav = function(pagerName, positionId) {
  	if (! this.inited) {
  		alert("not inited");
  		return;
  	}
  	var element = document.getElementById(positionId);
  	
  	var pagerHtml = '<span onclick="' + pagerName + '.prev();" class="pg-normal"> &#171 Prev </span> | ';
      for (var page = 1; page <= this.pages; page++) 
          pagerHtml += '<span id="pg_Ori' + page + '" class="pg-normal" onclick="' + pagerName + '.showPage(' + page + ');">' + page + '</span> | ';
      pagerHtml += '<span onclick="'+pagerName+'.next();" class="pg-normal"> Next &#187;</span>';            
      
      element.innerHTML = pagerHtml;
  }
}
</script> 
<script type="text/javascript">
var oGridPage = new OriServGridPagination('dataTableO', 5); 
oGridPage.init(); 
oGridPage.showPageNav('oGridPage', 'pageNavPosition1'); 
oGridPage.showPage(1);
</script>
<script type="text/javascript">

function DestServGridPagination(tableName, itemsPerPage) {
  this.tableName = tableName;
  this.itemsPerPage = itemsPerPage;
  this.currentPage = 1;
  this.pages = 0;
  this.inited = false;
  
  this.showRecords = function(from, to) {        
      var rows = document.getElementById(tableName).rows;
      // i starts from 1 to skip table header row
      for (var i = 1; i < rows.length; i++) {
          if (i < from || i > to)  
              rows[i].style.display = 'none';
          else
              rows[i].style.display = '';
      }
  }
  
  this.showPage = function(pageNumber) {
  	if (! this.inited) {
  		alert("not inited");
  		return;
  	}

      var oldPageAnchor = document.getElementById('pg_Dest'+this.currentPage);
      oldPageAnchor.className = 'pg-normal';
      
      this.currentPage = pageNumber;
      var newPageAnchor = document.getElementById('pg_Dest'+this.currentPage);
      newPageAnchor.className = 'pg-selected';
      
      var from = (pageNumber - 1) * itemsPerPage + 1;
      var to = from + itemsPerPage - 1;
      this.showRecords(from, to);
  }   
  
  this.prev = function() {
      if (this.currentPage > 1)
          this.showPage(this.currentPage - 1);
  }
  
  this.next = function() {
      if (this.currentPage < this.pages) {
          this.showPage(this.currentPage + 1);
      }
  }                        
  
  this.init = function() {
      var rows = document.getElementById(tableName).rows;
      var records = (rows.length - 1); 
      this.pages = Math.ceil(records / itemsPerPage);
      this.inited = true;
  }

  this.showPageNav = function(pagerName, positionId) {
  	if (! this.inited) {
  		alert("not inited");
  		return;
  	}
  	var element = document.getElementById(positionId);
  	
  	var pagerHtml = '<span onclick="' + pagerName + '.prev();" class="pg-normal"> &#171 Prev </span> | ';
      for (var page = 1; page <= this.pages; page++) 
          pagerHtml += '<span id="pg_Dest' + page + '" class="pg-normal" onclick="' + pagerName + '.showPage(' + page + ');">' + page + '</span> | ';
      pagerHtml += '<span onclick="'+pagerName+'.next();" class="pg-normal"> Next &#187;</span>';            
      
      element.innerHTML = pagerHtml;
  }
}
function ValidateDecimal(o) {
    if (o.value.length > 0) {
        var objReg = /^\d+(\.\d{1,2})?$/;
        if (objReg.test(o.value))
        	 return true;
        else
        	alert("Enter valid Number");
        document.getElementById(o.id).value=0;
        document.getElementById(o.id).select();
        return false;
    }
}
</script> 
<script type="text/javascript">
var dGridPage = new DestServGridPagination('dataTableD', 5); 
dGridPage.init(); 
dGridPage.showPageNav('dGridPage', 'pageNavPosition2'); 
dGridPage.showPage(1);
</script>