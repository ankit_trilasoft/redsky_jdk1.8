/**
 * WSAnswerRestriction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class WSAnswerRestriction  implements java.io.Serializable {
    private com.keysurvey.api.v68.form.design.WSAnswerRestrictionType answerRestrictionType;

    private int maxNumber;

    private int minNumber;

    public WSAnswerRestriction() {
    }

    public WSAnswerRestriction(
           com.keysurvey.api.v68.form.design.WSAnswerRestrictionType answerRestrictionType,
           int maxNumber,
           int minNumber) {
           this.answerRestrictionType = answerRestrictionType;
           this.maxNumber = maxNumber;
           this.minNumber = minNumber;
    }


    /**
     * Gets the answerRestrictionType value for this WSAnswerRestriction.
     * 
     * @return answerRestrictionType
     */
    public com.keysurvey.api.v68.form.design.WSAnswerRestrictionType getAnswerRestrictionType() {
        return answerRestrictionType;
    }


    /**
     * Sets the answerRestrictionType value for this WSAnswerRestriction.
     * 
     * @param answerRestrictionType
     */
    public void setAnswerRestrictionType(com.keysurvey.api.v68.form.design.WSAnswerRestrictionType answerRestrictionType) {
        this.answerRestrictionType = answerRestrictionType;
    }


    /**
     * Gets the maxNumber value for this WSAnswerRestriction.
     * 
     * @return maxNumber
     */
    public int getMaxNumber() {
        return maxNumber;
    }


    /**
     * Sets the maxNumber value for this WSAnswerRestriction.
     * 
     * @param maxNumber
     */
    public void setMaxNumber(int maxNumber) {
        this.maxNumber = maxNumber;
    }


    /**
     * Gets the minNumber value for this WSAnswerRestriction.
     * 
     * @return minNumber
     */
    public int getMinNumber() {
        return minNumber;
    }


    /**
     * Sets the minNumber value for this WSAnswerRestriction.
     * 
     * @param minNumber
     */
    public void setMinNumber(int minNumber) {
        this.minNumber = minNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSAnswerRestriction)) return false;
        WSAnswerRestriction other = (WSAnswerRestriction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.answerRestrictionType==null && other.getAnswerRestrictionType()==null) || 
             (this.answerRestrictionType!=null &&
              this.answerRestrictionType.equals(other.getAnswerRestrictionType()))) &&
            this.maxNumber == other.getMaxNumber() &&
            this.minNumber == other.getMinNumber();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAnswerRestrictionType() != null) {
            _hashCode += getAnswerRestrictionType().hashCode();
        }
        _hashCode += getMaxNumber();
        _hashCode += getMinNumber();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSAnswerRestriction.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswerRestriction"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("answerRestrictionType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "answerRestrictionType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswerRestrictionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "maxNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("minNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "minNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
