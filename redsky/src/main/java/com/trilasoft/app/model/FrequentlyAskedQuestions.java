package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="frequentlyaskedquestions")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class FrequentlyAskedQuestions extends BaseObject{
	private Long id;
	private String corpId;	
	private String partnerCode;
	private String question;
	private String answer;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private Long partnerId;
	private String language;
	private Long parentId=0L;
	private Long sequenceNumber;
	private String jobType;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpId", corpId).append("partnerCode", partnerCode)
				.append("question", question).append("answer", answer)
				.append("createdOn", createdOn).append("createdBy", createdBy)
				.append("updatedOn", updatedOn).append("updatedBy", updatedBy)
				.append("partnerId", partnerId).append("language", language)
				.append("parentId", parentId)
				.append("sequenceNumber", sequenceNumber).append("jobType", jobType).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof FrequentlyAskedQuestions))
			return false;
		FrequentlyAskedQuestions castOther = (FrequentlyAskedQuestions) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(partnerCode, castOther.partnerCode)
				.append(question, castOther.question)
				.append(answer, castOther.answer)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(partnerId, castOther.partnerId)
				.append(language, castOther.language)
				.append(parentId, castOther.parentId)
				.append(sequenceNumber, castOther.sequenceNumber).append(jobType, castOther.jobType).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(partnerCode).append(question).append(answer)
				.append(createdOn).append(createdBy).append(updatedOn)
				.append(updatedBy).append(partnerId).append(language)
				.append(parentId).append(sequenceNumber).append(jobType).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=30)
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column(length = 8)
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	@Column(length=5000)
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	@Column(length=5000)
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column( length=82 )
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column( length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column(length=8)
	public Long getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}
	@Column(length=20)
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Long getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(Long sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	
}
