package com.trilasoft.app.webapp.tags;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;

import com.trilasoft.app.webapp.action.ComponentConfigByCorpUtil;

public class AutoSaveByCorpTag  extends TagSupport {

	private ComponentConfigByCorpUtil componentConfigByCorpUtil = null;

	private String userCorpID;
	
	public AutoSaveByCorpTag(){
	}

	// ~ Methods
	// ========================================================================================================

	public int doStartTag() throws JspException {
		
		if(pageContext.getSession()==null){
			return Tag.EVAL_BODY_INCLUDE;
		}
		String autoSavePrompt = "";
		

		ServletContext context = pageContext.getServletContext();
		XmlWebApplicationContext ctx = (XmlWebApplicationContext) context
				.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);

		componentConfigByCorpUtil = (ComponentConfigByCorpUtil) ctx.getBean("componentConfigByCorpUtil");	
		Authentication currentUser = SecurityContextHolder.getContext()
		.getAuthentication();
		if (currentUser.getPrincipal() instanceof String && currentUser.getPrincipal().equals("anonymous")  ) return Tag.EVAL_BODY_INCLUDE;;
		User user = (User) currentUser.getPrincipal();
		String userCorpID = user.getCorpID();
		autoSavePrompt = componentConfigByCorpUtil.getAutoSavePrompt(userCorpID);
		if((user.getUserType().equalsIgnoreCase("AGENT")) || (user.getUserType().equalsIgnoreCase("PARTNER")) || (user.getUserType().equalsIgnoreCase("ACCOUNT"))){	
			autoSavePrompt = "No";
		}
		//pageContext.getSession("sessionTimeZone", timeZone);
		
		pageContext.getSession().setAttribute("autoSavePrompt", autoSavePrompt);
		return Tag.EVAL_BODY_INCLUDE;
	}

	
}
