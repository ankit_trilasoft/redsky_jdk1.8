<%@ include file="/common/taglibs.jsp"%>
 
<head>
<title><fmt:message key="sqlExtractList.title" /></title>
<meta name="heading" content="<fmt:message key='sqlExtractList.heading'/>" />
<script language="javascript" type="text/javascript">

function clear_fields(){  		    	
	document.forms['sqlExtractList'].elements['sqlExtract.fileName'].value = "";
	document.forms['sqlExtractList'].elements['sqlExtract.description'].value ="";
}

function extractData(targetElement){
	var did = targetElement;
	location.href="extractSQLData.html?id="+did;
}
function sendMail(targetElement){
	alert('Invalid query, Make it correct to send mail.');

return false;
}
function missingMail(){
alert('Email is not fill in Query');
}
</script>

<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-top:-18px;
!margin-top:-7px;
padding:2px 0;
text-align:right;
width:100%;
}
</style>
</head>

<s:form cssClass="form_magn" id="sqlExtractList" name="sqlExtractList" action="sqlExtractUserSearch" method="post" >
<div id="Layer1" style="width:100%; !margin-bottom:-5px;">
<div id="otabs">
	<ul>
		<li><a class="current"><span>Search</span></a></li>
	</ul>
</div>
<div class="spnblk">&nbsp;</div>
<table class="table" width="90%" style="!margin-top:-2px;">
	<thead>
		<tr>
		    <th>File Name</th>
			<th>Description</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
		    <td><s:textfield name="sqlExtract.fileName" required="true" cssClass="text medium" size="50"/></td>
			<td><s:textfield name="sqlExtract.description" required="true" cssClass="text medium" size="50"/></td>
			<td width="520px;">
				<s:submit cssClass="cssbutton" method="" cssStyle="width:60px; height:25px" key="button.search"/>
		   	 	<input type="button" class="cssbutton" value="Clear" style="width:60px; height:25px" onclick="clear_fields();"/>
		   	</td> 
		</tr>
	</tbody>
</table>
<c:set var="extractbuttons"/>
<c:out value="${extractbuttons}" escapeXml="false" />


<div id="otabs">
	<ul>
	    <li><a class="current"><span>SQL Extract List</span></a></li>
	</ul>
</div>
<div class="spn">&nbsp;</div>
<s:set name="extractList" value="extractList" scope="request" />
	
	<display:table name="extractList" class="table" requestURI="" id="extractListRow"  pagesize="10" style="width:100%;">
		<display:column property="fileName" sortable="true" titleKey="sqlExtract.fileName" style="width:130px"/>
		<display:column property="description" sortable="true" titleKey="sqlExtract.description"/>
		<display:column property="fileType" sortable="true" titleKey="sqlExtract.fileType" style="width:70px"/>
		<display:column property="tested" sortable="true" title="Status" style="width:70px"/>
		<display:column title="Extract" style="width:50px">
			<c:choose>
				<c:when test="${empty extractListRow.whereClause1 and empty extractListRow.whereClause2 and empty extractListRow.whereClause3 and empty extractListRow.whereClause4 and empty extractListRow.whereClause4 and empty extractListRow.orderBy and empty extractListRow.groupBy}">
					<a><img align="middle"  title ="Extract" onclick="javascript:window.location='sqlExtractInputForm.html?decorator=popup&popup=true&id=${extractListRow.id}&whereClause1=${extractListRow.whereClause1}&whereClause2=${extractListRow.whereClause2}&whereClause3=${extractListRow.whereClause3}&whereClause4=${extractListRow.whereClause4}&whereClause5=${extractListRow.whereClause5}&orderBy=${extractListRow.orderBy}&groupBy=${extractListRow.groupBy}&isBlank=yes';" style="margin: 0px 0px 0px 8px;" src="images/extract-para.png"/></a>
				</c:when>
				<c:otherwise>
					<a><img align="middle"  title ="Extract" onclick="window.open('sqlExtractInputForm.html?decorator=popup&popup=true&id=${extractListRow.id}&whereClause1=${extractListRow.whereClause1}&whereClause2=${extractListRow.whereClause2}&whereClause3=${extractListRow.whereClause3}&whereClause4=${extractListRow.whereClause4}&whereClause5=${extractListRow.whereClause5}&orderBy=${extractListRow.orderBy}&groupBy=${extractListRow.groupBy}&isBlank=no','sqlExtractInputForm','height=500,width=575,top=0, left=610, scrollbars=yes,resizable=yes').focus();" style="margin: 0px 0px 0px 8px;" src="images/extract-para.png"/></a>
				</c:otherwise>
			</c:choose>
		</display:column>
		<display:column title="Extract to FTP Location" >${ company.ftpDir}</display:column>
		<display:column title="Send Mail" style="width:50px">
		<c:if test="${extractListRow.email != ''}">	
			<c:if test="${extractListRow.tested == 'Tested'}">
			<a><img align="middle"  title ="send mail" onclick="window.open('sqlExtractSendMail.html?decorator=popup&popup=true&id=${extractListRow.id}&whereClause1=${extractListRow.whereClause1}&whereClause2=${extractListRow.whereClause2}&whereClause3=${extractListRow.whereClause3}&whereClause4=${extractListRow.whereClause4}&whereClause5=${extractListRow.whereClause5}&orderBy=${extractListRow.orderBy}&groupBy=${extractListRow.groupBy}','sqlExtractInputForm','height=500,width=575,top=0, left=610, scrollbars=yes,resizable=yes').focus();" style="margin: 0px 0px 0px 8px;" src="images/email_go.png"/></a>
		 </c:if>
		<c:if test="${extractListRow.tested != 'Tested'}">
			<a><img title="send mail" align="middle" onclick="sendMail();" style="margin: 0px 0px 0px 8px;"  src="images/email_go.png"/></a>
		</c:if>
		</c:if>
		<c:if test="${extractListRow.email == ''}">
		<a><img title="send mail" align="middle" onclick="" style="margin: 0px 0px 0px 8px;"  src="images/email_disable.png"/></a>	
		</c:if>
		</display:column>
	</display:table>
	<c:set var="buttons"/>
	<c:out value="${buttons}" escapeXml="false" />
</div>
</s:form>
