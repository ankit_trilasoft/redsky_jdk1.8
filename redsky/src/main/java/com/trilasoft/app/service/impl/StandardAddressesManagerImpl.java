package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.StandardAddressesDao;
import com.trilasoft.app.model.StandardAddresses;
import com.trilasoft.app.service.StandardAddressesManager;

public class StandardAddressesManagerImpl extends GenericManagerImpl<StandardAddresses, Long> implements StandardAddressesManager { 
		StandardAddressesDao standardAddressesDao; 
	    
	 
	    public StandardAddressesManagerImpl(StandardAddressesDao standardAddressesDao) { 
	        super(standardAddressesDao); 
	        this.standardAddressesDao = standardAddressesDao; 
	    }


		public List searchByDetail(String job, String addressLine1,String city, String countryCode, String state) {
			// TODO Auto-generated method stub
			return standardAddressesDao.searchByDetail(job, addressLine1, city,countryCode, state);
		}


		public List getByJobType(String jobType) {
			// TODO Auto-generated method stub
			return standardAddressesDao.getByJobType(jobType);
		}


		public List searchByDetailForPopup(String jobType, String addressLine1, String city, String countryCode, String state) {
			// TODO Auto-generated method stub
			return standardAddressesDao.searchByDetailForPopup(jobType, addressLine1, city, countryCode, state);
		}

		public List getAddressLine1(String jobType, String corpid){
			return standardAddressesDao.getAddressLine1(jobType, corpid);
		}
		
		public StandardAddresses getStandardAddressDetail(String residenceName){
			return standardAddressesDao.getStandardAddressDetail(residenceName);
		}
		 
}
