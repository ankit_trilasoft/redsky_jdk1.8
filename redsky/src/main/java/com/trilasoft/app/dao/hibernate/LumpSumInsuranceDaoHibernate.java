package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.LumpSumInsuranceDao;
import com.trilasoft.app.model.LumpSumInsurance;

public class LumpSumInsuranceDaoHibernate extends GenericDaoHibernate<LumpSumInsurance, Long> implements LumpSumInsuranceDao {

	public LumpSumInsuranceDaoHibernate(){
		super(LumpSumInsurance.class);
	}
	   public List getItemList(Long sid,String sessionCorpID){
		   return getHibernateTemplate().find("from LumpSumInsurance where  serviceOrderID='"+sid+"'  and corpID='"+sessionCorpID+"'");  
	   }
	   public List getVehicle(Long sid,String sessionCorpID){
	   return getHibernateTemplate().find("from Vehicle where serviceOrderId='"+sid+"'");
		   
	   }
	   public void updateInsurance(String newField,String fieldValue,Long id,Long sid,Long cid,String sessionCorpID,String updatedBy){
				 
		   getHibernateTemplate().bulkUpdate("update LumpSumInsurance set "+newField+"='"+fieldValue+"',updatedBy='"+updatedBy+"',updatedOn = now() where id='"+id+"' and serviceOrderID='"+sid+"' and corpID='"+sessionCorpID+"'");
			
		   }
		   public void updateVehicle(String newField,String fieldValue,Long id,Long sid,String vehicleIdNumber,String sessionCorpID){
			   getHibernateTemplate().bulkUpdate("update Vehicle set "+newField+"='"+fieldValue+"' where id='"+id+"' and serviceOrderId='"+sid+"' and idNumber='"+vehicleIdNumber+"' and corpID='"+sessionCorpID+"'");
		   }
		public String getLinkedLumpSumInsuranceData(Long id, Long sid) {
			String lumpsumInsuranceId="";
			List list = this.getSession().createSQLQuery("select id from lumpsumInsurance where networkSynchedId = '"+id+"' and serviceOrderID = '"+sid+"' ").list();
			if(list !=null && (!(list.isEmpty())) && (list.get(0)!=null) && (!(list.get(0).toString().trim().equals("")))){
				lumpsumInsuranceId =list.get(0).toString().trim();
			}
			return lumpsumInsuranceId;
		}
		public void deleteNetworkLumpsuminsurance(Long id, Long networkSynchedId) {
			this.getSession().createSQLQuery("delete from lumpsumInsurance where networkSynchedId='"+networkSynchedId+"' and serviceOrderID="+id+"").executeUpdate();	
			
		}
		public void updateLinkedInsurance(String newField, String fieldValue, Long networkSynchedId, Long sid) {
			this.getSession().createSQLQuery("update lumpsumInsurance set "+newField+"='"+fieldValue+"' where  serviceOrderID='"+sid+"' and networkSynchedId='"+networkSynchedId+"' ").executeUpdate();		
			
		}
		public void updateLinkedVehicleData(String newField, String fieldValue, Long id, String idNumber) {
			this.getSession().createSQLQuery("update vehicle set "+newField+"='"+fieldValue+"' where  serviceOrderId='"+id+"' and idNumber='"+idNumber+"' ").executeUpdate();		
			
		} 
}
