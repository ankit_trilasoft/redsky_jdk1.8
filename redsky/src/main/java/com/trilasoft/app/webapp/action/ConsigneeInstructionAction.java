package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.ConsigneeInstruction;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ConsigneeInstructionManager;
import com.trilasoft.app.service.CustomManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.IntegrationLogInfoManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class ConsigneeInstructionAction extends BaseAction implements Preparable{

	private ConsigneeInstructionManager consigneeInstructionManager;

	private ConsigneeInstruction consigneeInstruction;
	private RefMasterManager refMasterManager;
	private ServiceOrderManager serviceOrderManager;
	private ToDoRuleManager toDoRuleManager;
	private ServiceOrder serviceOrder;
	private Miscellaneous miscellaneous;
	private TrackingStatus trackingStatus;
	private CustomerFile customerFile;
	private MiscellaneousManager miscellaneousManager;
	private TrackingStatusManager trackingStatusManager;
	private CustomerFileManager customerFileManager;
	private Long id;
	private Long soId;
	private Long sid;
	private String sessionCorpID;
	private String consigneeInstructionCode="";
	private List consigneeInstructionList;
	private List consigneeInstructionCodeList;
	private String consigneeInstructionCodeValue;
	private String shipNum;
	private String DaCode;
	private String sACode;
	private String osACode;
	private List consigneeInstructionAddList;
	private List consigneeShipmentLocationList;
	private List consigneeShiperList;
	private List consigneeInstructionAddJobList;
	private List consigneeInstructionNCJobList;
	private String consigneeInstructionAddValue;
	private  Map<String, String> specific;
    private String shipSize;
	private String minShip;
	private String countShip;
	private List customerSO;
	private CustomManager customManager;
	private String countBondedGoods;
	private CustomerFileAction customerFileAction;
	private  IntegrationLogInfoManager  integrationLogInfoManager;
	Date currentdate = new Date();
	private String voxmeIntergartionFlag;
	private Company company;
	private CompanyManager companyManager;
	private String  usertype;
	private Boolean surveyTab = new Boolean(false);
	static final Logger logger = Logger.getLogger(ConsigneeInstructionAction.class);
	private  Map<String, String> specific_isactive;
	private String brokerCode;
	// private List day;
	private String oiJobList;
	public ConsigneeInstructionAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		 usertype=user.getUserType();
	}
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		company=companyManager.findByCorpID(sessionCorpID).get(0);
        if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }
        if(company!=null){
		if(company.getVoxmeIntegration()!=null){
			voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
			}
		}
		try {
			 if((usertype.trim().equalsIgnoreCase("AGENT"))){ 
			id = new Long(getRequest().getParameter("sid").toString());
			 if(id!=null){
				 ServiceOrder agentServiceOrderCombo = serviceOrderManager.getForOtherCorpid(id);
				 String corpId=agentServiceOrderCombo.getCorpID();
				 TrackingStatus agentTS = trackingStatusManager.getForOtherCorpid(id);
				 boolean chkCompanySurveyFlag = companyManager.getCompanySurveyFlag(agentServiceOrderCombo.getCorpID()); 
				 if(agentServiceOrderCombo.getIsNetworkRecord() && (agentTS.getSoNetworkGroup() || chkCompanySurveyFlag)){
						surveyTab= true;
				}
				}
			 }
		 } catch (Exception e) {
			e.printStackTrace();
		 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		}
	public void setConsigneeInstructionManager(ConsigneeInstructionManager consigneeInstructionManager) {
		this.consigneeInstructionManager = consigneeInstructionManager;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getComboList(String corpID){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		specific = refMasterManager.findByParameter(corpID, "SPECIFIC");
		LinkedHashMap <String,String>tempParemeterMap = new LinkedHashMap<String, String>();
	   	specific_isactive  = new LinkedHashMap<String, String>();
	  	  List <RefMasterDTO> allParamValue1 = refMasterManager.getAllParameterValue(corpID,"'SPECIFIC'");
	  	   for (RefMasterDTO  refObj : allParamValue1) {
	  	    	    		 	if(refObj.getParameter().trim().equalsIgnoreCase("SPECIFIC")){
	  	    	    		 		specific_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());}}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
	private Billing billing;
	private BillingManager billingManager;
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		serviceOrder = serviceOrderManager.get(sid);
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(sid);
    	trackingStatus = trackingStatusManager.get(sid); 
    	billing = billingManager.get(sid);
    	customerFile = serviceOrder.getCustomerFile();
		shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        if(!customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).isEmpty() && customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).get(0)!=null){
            countBondedGoods = customManager.countBondedGoods(serviceOrder.getId(), serviceOrder.getCorpID()).get(0).toString();
        }
		if (sid != null) {
			 consigneeInstructionList = consigneeInstructionManager.getByShipNumber(serviceOrder.getShipNumber());
			if(!consigneeInstructionList .isEmpty()){
				consigneeInstruction = (ConsigneeInstruction)consigneeInstructionList.get(0);
				consigneeInstruction.getConsigneeAddress();
				consigneeInstruction.getNotification();
				consigneeInstruction.getSpecialInstructions();
				consigneeInstruction.getShipmentLocation();
			}else {
				consigneeInstruction = new ConsigneeInstruction();
     			consigneeInstruction.setCreatedOn(new Date());
				consigneeInstruction.setUpdatedOn(new Date());
				
				consigneeShiperList = consigneeInstructionManager.getShiperAddress(serviceOrder.getCorpID(),serviceOrder.getShipNumber());
				if((consigneeShiperList!=null) && (!(consigneeShiperList.isEmpty()))){
					String[] str1 = consigneeShiperList.get(0).toString().split("~");
					if(str1[0].equalsIgnoreCase("1")){
						str1[0]="";
					}
					else{
						str1[0]=str1[0]+"\n";
					}
					if(str1[1].equalsIgnoreCase("1")){
						str1[1]="";
					}
					else{
						str1[1]=str1[1]+" ";
					}
					if(str1[2].equalsIgnoreCase("1")){
						str1[2]="";
					}
					else{
						str1[2]=str1[2]+"\n";
					}
					if(str1[3].equalsIgnoreCase("1")){
						str1[3]="";
					}
					else{
						str1[3]=str1[3]+"\n";
					}
					if(str1[4].equalsIgnoreCase("1")){
						str1[4]="";
					}
					else{
						str1[4]=str1[4]+"\n";
					}
					if(str1[5].equalsIgnoreCase("1")){
						str1[5]="";
					}
					else{
						str1[5]=str1[5]+"\n";
					}
					if(str1[6].equalsIgnoreCase("1")){
						str1[6]="";
					}
					else{
						str1[6]=str1[6]+",";
					}
					if(str1[7].equalsIgnoreCase("1")){
						str1[7]="";
					}
					else{
						str1[7]=str1[7]+" ";
					}
					if(str1[8].equalsIgnoreCase("1")){
						str1[8]="";
					}
					else{
						str1[8]= str1[8]+"\n";
					}
					if(str1[9].equalsIgnoreCase("1")){
						str1[9]="";
					}
					else{
						str1[9]=str1[9]+"\n";
					}
					if(str1[10].equalsIgnoreCase("1")){
						str1[10]="";
					}
					else{
						str1[10]= "Phn :" +str1[10];
					}
					consigneeInstruction.setShipperAddress(str1[0]+"For :"+str1[1]+str1[2]+str1[3]+str1[4]+str1[5]+str1[6]+str1[7]+str1[8]+str1[9]+str1[10]);
				}	
			}
				
				
				
				if((trackingStatus.getOriginAgentCode() != null) && (!(trackingStatus.getOriginAgentCode().equalsIgnoreCase(""))))
					consigneeInstructionCode=trackingStatus.getOriginAgentCode();
				if((trackingStatus.getOriginSubAgentCode() != null) && (!(trackingStatus.getOriginSubAgentCode().equalsIgnoreCase(""))))	
					consigneeInstructionCode=trackingStatus.getOriginSubAgentCode(); 
				if(sid == null){
					
				
				consigneeShipmentLocationList=consigneeInstructionManager.findShipmentLocation(consigneeInstructionCode,serviceOrder.getCorpID());
				
				if((consigneeShipmentLocationList!=null) && (!(consigneeShipmentLocationList.isEmpty()))){
				String[] str1 = consigneeShipmentLocationList.get(0).toString().split("~");
				if(str1[0].equalsIgnoreCase("1")){
					str1[0]="";
				}
				else{
					str1[0]=str1[0]+"\n";
				}
				if(str1[1].equalsIgnoreCase("1")){
					str1[1]="";
				}
				else{
					str1[1]=str1[1]+"\n";
				}
				if(str1[2].equalsIgnoreCase("1")){
					str1[2]="";
				}
				else{
					str1[2]=str1[2]+"\n";
				}
				if(str1[3].equalsIgnoreCase("1")){
					str1[3]="";
				}
				else{
					str1[3]=str1[3]+"\n";
				}
				if(str1[4].equalsIgnoreCase("1")){
					str1[4]="";
				}
				else{
					str1[4]=str1[4]+"\n";
				}
				if(str1[5].equalsIgnoreCase("1")){
					str1[5]="";
				}
				else{
					str1[5]=str1[5]+" ";
				}
				if(str1[6].equalsIgnoreCase("1")){
					str1[6]="";
				}
				else{
					str1[6]=str1[6]+" ";
				}
				if(str1[7].equalsIgnoreCase("1")){
					str1[7]="";
				}
				else{
					str1[7]=str1[7]+"\n";
				}
				if(str1[8].equalsIgnoreCase("1")){
					str1[8]="";
				}
				else{
					str1[8]="Phn : "+str1[8];
				}
				if(str1[9].equalsIgnoreCase("1")){
					str1[9]="";
				}
				else{
					str1[9]=str1[9]+"\n";
				}
				consigneeInstruction.setShipmentLocation(str1[0]+str1[1]+str1[2]+str1[3]+str1[4]+str1[5]+str1[6]+str1[9]+str1[7]+str1[8]);
			}
				}
				
			sACode=trackingStatus.getDestinationSubAgentCode();
			if((trackingStatus.getOriginAgentCode() != null) && (!(trackingStatus.getOriginAgentCode().equalsIgnoreCase(""))))
				osACode=trackingStatus.getOriginAgentCode();
			if((trackingStatus.getOriginSubAgentCode() != null) && (!(trackingStatus.getOriginSubAgentCode().equalsIgnoreCase(""))))	
				osACode=trackingStatus.getOriginSubAgentCode(); 
			if((trackingStatus.getBrokerCode()!= null) && (!(trackingStatus.getBrokerCode().equalsIgnoreCase(""))))	
				brokerCode=trackingStatus.getBrokerCode(); 
		}
		consigneeInstruction.setCorpID(serviceOrder.getCorpID());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String msgClicked;
	private String forwardingAjaxUrl;
	private String forwardingAjaxVal;
	private String hitFlag;
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		serviceOrder = serviceOrderManager.get(sid);
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(sid);
    	trackingStatus = trackingStatusManager.get(sid); 
    	billing=billingManager.get(sid);
    	customerFile = serviceOrder.getCustomerFile();
		//getComboList(sessionCorpID);
		boolean isNew = (consigneeInstruction.getId() == null);
		if(isNew){
			consigneeInstruction.setCreatedOn(new Date());
        }
        
		consigneeInstruction.setUpdatedOn(new Date());
		consigneeInstruction.setUpdatedBy(getRequest().getRemoteUser());
		consigneeInstruction.setShipNumber(serviceOrder.getShipNumber());
		consigneeInstruction.setShip(serviceOrder.getShip());
		consigneeInstruction.setSequenceNumber(serviceOrder.getSequenceNumber());
		consigneeInstruction.setCorpID(serviceOrder.getCorpID());
		consigneeInstruction.setId(serviceOrder.getId());
		consigneeInstruction.setShipmentLocation(consigneeInstruction.getShipmentLocation());
		 if(sessionCorpID.equalsIgnoreCase("UGWW")){
			 consigneeInstruction.setUgwIntId(serviceOrder.getId().toString());
	      }
		consigneeInstruction=consigneeInstructionManager.save(consigneeInstruction);
		try{
		if(serviceOrder.getIsNetworkRecord()){
			List linkedShipNumber=findLinkedShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
			synchornizeConsigneeInstruction(serviceOrderRecords,consigneeInstruction,isNew,serviceOrder);
			
		}
		/*Long StartTime = System.currentTimeMillis();
		if(!serviceOrder.getJob().toString().trim().equalsIgnoreCase("RLO")){
			String agentShipNumber = toDoRuleManager.getLinkedShipnumber(serviceOrder.getShipNumber(),sessionCorpID);
			if(agentShipNumber!=null && !agentShipNumber.equals(""))
				customerFileAction.createIntegrationLogInfo(agentShipNumber,serviceOrder.getStatus());
   			
   		}
			
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
   
		}catch(Exception ex){
			System.out.println("\n\n\n\n error------->"+ex);
			ex.printStackTrace();
		}
		
		String key = (isNew) ? "consigneeInstruction.added" : "consigneeInstruction.updated";
		saveMessage(getText(key));
		shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        hitFlag="1";
        if((forwardingAjaxVal!=null && !forwardingAjaxVal.equalsIgnoreCase("")) && forwardingAjaxVal.equalsIgnoreCase("Yes")){
        	String permKey = sessionCorpID +"-"+"component.claim.claimStatus.propertyAmount";
    		boolean checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
				if(checkFieldVisibility){
					forwardingAjaxUrl = "?id="+serviceOrder.getId()+"&msgClicked="+msgClicked;
				}else{
					forwardingAjaxUrl = "?id="+serviceOrder.getId();
				}
        }
		if (!isNew) {
			//System.out.println("\n\n\n\n\n in input"+isNew);
			return INPUT;
			
		} else {
			//System.out.println("\n\n\n\n\n in success"+isNew);
			return SUCCESS;
		}		
	}
	private void synchornizeConsigneeInstruction(List<Object> serviceOrderRecords,ConsigneeInstruction consigneeInstruction, boolean isNew,ServiceOrder serviceOrderold) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrder=(ServiceOrder)it.next();
    		try{
    			if(!(serviceOrder.getShipNumber().equals(serviceOrderold.getShipNumber()))){	
    			if(isNew){
    				ConsigneeInstruction consigneeInstructionNew= new ConsigneeInstruction();
    				BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(consigneeInstructionNew, consigneeInstruction);
    				
    				consigneeInstructionNew.setId(serviceOrder.getId());
    				consigneeInstructionNew.setCorpID(serviceOrder.getCorpID());
    				consigneeInstructionNew.setShipNumber(serviceOrder.getShipNumber());
    				consigneeInstructionNew.setSequenceNumber(serviceOrder.getSequenceNumber());
    				consigneeInstructionNew.setServiceOrderId(serviceOrder.getId());
    				consigneeInstructionNew.setCreatedBy("Networking");
    				consigneeInstructionNew.setUpdatedBy("Networking");
    				consigneeInstructionNew.setCreatedOn(new Date());
    				consigneeInstructionNew.setUpdatedOn(new Date()); 
    				if(consigneeInstructionNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
						 consigneeInstructionNew.setUgwIntId(consigneeInstructionNew.getId().toString());
						 }
    				consigneeInstructionNew = consigneeInstructionManager.save(consigneeInstructionNew); 
    				
    				
    			}else{
    				ConsigneeInstruction consigneeInstructionTo= consigneeInstructionManager.getForOtherCorpid(consigneeInstructionManager.findRemoteConsigneeInstruction(serviceOrder.getId()));  
        			Long id=consigneeInstructionTo.getId();
        			String createdBy=consigneeInstructionTo.getCreatedBy();
        			Date createdOn=consigneeInstructionTo.getCreatedOn();
        			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(consigneeInstructionTo, consigneeInstruction);
    				
    				consigneeInstructionTo.setId(id);
    				consigneeInstructionTo.setCreatedBy(createdBy);
    				consigneeInstructionTo.setCreatedOn(createdOn);
    				consigneeInstructionTo.setCorpID(serviceOrder.getCorpID());
    				consigneeInstructionTo.setShipNumber(serviceOrder.getShipNumber());
    				consigneeInstructionTo.setSequenceNumber(serviceOrder.getSequenceNumber());
    				consigneeInstructionTo.setServiceOrderId(serviceOrder.getId());
    				consigneeInstructionTo.setUpdatedBy(consigneeInstruction.getCorpID()+":"+getRequest().getRemoteUser());
    				consigneeInstructionTo.setUpdatedOn(new Date()); 
    				if(consigneeInstructionTo.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
    					consigneeInstructionTo.setUgwIntId(consigneeInstructionTo.getId().toString());
						 }
    				consigneeInstructionManager.save(consigneeInstructionTo);
    			}
    			}
    		}catch(Exception ex){
    			System.out.println("\n\n\n\n error while synchronizing consigneeInstruction "+ex);
    			ex.printStackTrace();
    		}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	private List<Object> findServiceOrderRecords(List linkedShipNumber,ServiceOrder serviceOrderLocal) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List<Object> recordList= new ArrayList();
		Iterator it =linkedShipNumber.iterator();
		while(it.hasNext()){
			String shipNumber= it.next().toString();
			if(!serviceOrderLocal.getShipNumber().equalsIgnoreCase(shipNumber)){
    			ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
    			recordList.add(serviceOrderRemote);
    		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return recordList;
	}
	private List findLinkedShipNumber(ServiceOrder serviceOrder) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List linkedShipNumberList= new ArrayList();
		if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
			linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
		}else{
			linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return linkedShipNumberList;
	}
	@SkipValidation
	public String consigneeInstructionCode()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		consigneeInstructionCodeList=consigneeInstructionManager.findConsigneeCode(shipNum);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String shipmentLocationAddress()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		consigneeShipmentLocationList=consigneeInstructionManager.findShipmentLocation(osACode,sessionCorpID);
		if((consigneeShipmentLocationList!=null) && (!(consigneeShipmentLocationList.isEmpty()))){
			String[] str1 = consigneeShipmentLocationList.get(0).toString().split("~");
			if(str1[0].equalsIgnoreCase("1")){
				str1[0]="";
			}
			else{
				str1[0]=str1[0]+"\n";
			}
			if(str1[1].equalsIgnoreCase("1")){
				str1[1]="";
			}
			else{
				str1[1]=str1[1]+"\n";
			}
			if(str1[2].equalsIgnoreCase("1")){
				str1[2]="";
			}
			else{
				str1[2]=str1[2]+"\n";
			}
			if(str1[3].equalsIgnoreCase("1")){
				str1[3]="";
			}
			else{
				str1[3]=str1[3]+"\n";
			}
			if(str1[4].equalsIgnoreCase("1")){
				str1[4]="";
			}
			else{
				str1[4]=str1[4]+"\n";
			}
			if(str1[5].equalsIgnoreCase("1")){
				str1[5]="";
			}
			else{
				str1[5]=str1[5]+" ";
			}
			if(str1[6].equalsIgnoreCase("1")){
				str1[6]="";
			}
			else{
				str1[6]=str1[6]+" ";
			}
			if(str1[7].equalsIgnoreCase("1")){
				str1[7]="";
			}
			else{
				str1[7]=str1[7]+"\n";
			}
			if(str1[8].equalsIgnoreCase("1")){
				str1[8]="";
			}
			else{
				str1[8]="Phn : "+str1[8];
			}
			if(str1[9].equalsIgnoreCase("1")){
				str1[9]="";
			}
			else{
				str1[9]=str1[9]+"\n";
			}
			consigneeInstructionAddValue = str1[0]+str1[1]+str1[2]+str1[3]+str1[4]+str1[5]+str1[6]+str1[9]+str1[7]+str1[8];
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String consigneeInstructionAddress()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		consigneeInstructionAddList=consigneeInstructionManager.findConsigneeAddress(DaCode);
		String[] str1 = consigneeInstructionAddList.get(0).toString().split("~");
		consigneeInstructionAddValue = str1[0]+"~"+str1[1] +"~" +str1[2]+"~"+str1[3]+"~" +str1[4]+"~"+str1[5];
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String consigneeInstructionJobAddress()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		consigneeInstructionAddJobList=consigneeInstructionManager.findConsigneeJobAddress(DaCode, shipNum);
		String[] str1 = consigneeInstructionAddJobList.get(0).toString().split("~");
		consigneeInstructionAddValue = str1[0]+"~"+str1[1] +"~" +str1[2]+"~"+str1[3]+"~" +str1[4]+"~"+str1[5]+"~" +str1[6]+"~"+str1[7]+"~"+str1[8]+"~" +str1[9]+"~"+str1[10]+"~"+str1[11]+"~" +str1[12]+"~"+str1[13]+"~"+str1[14]+"~"+str1[15]+"~"+str1[16]+"~"+str1[17]+"~"+str1[18]+"~"+str1[19]+"~"+str1[20]+"~"+str1[21];
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String consigneeInstructionNCAddress()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		consigneeInstructionNCJobList=consigneeInstructionManager.findConsigneeNCAddress(shipNum);
		String[] str1 = consigneeInstructionNCJobList.get(0).toString().split("~");
		consigneeInstructionAddValue = str1[0]+"~"+str1[1] +"~" +str1[2]+"~"+str1[3]+"~" +str1[4]+"~"+str1[5]+"~" +str1[6]+"~"+str1[7]+"~"+str1[8]+"~" +str1[9]+"~"+str1[10]+"~"+str1[11];
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String consigneeDestinationSubAgentAddress()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		consigneeInstructionAddList=consigneeInstructionManager.findConsigneeSubAgentAddress(sACode,shipNum);
		String[] str1 = consigneeInstructionAddList.get(0).toString().split("~");
		consigneeInstructionAddValue = str1[0]+"~"+str1[1] +"~" +str1[2]+"~"+str1[3]+"~" +str1[4]+"~"+str1[5]+"~" +str1[6]+"~"+str1[7]+"~"+str1[8]+"~"+str1[9]+"~"+str1[10]+"~"+str1[11];
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public  Map<String, String> getSpecific() {
		
		if(serviceOrder !=null && serviceOrder.getId()!=null && serviceOrder.getShipNumber()!=null && consigneeInstruction.getConsignmentInstructions()!=null && !consigneeInstruction.getConsignmentInstructions().equals(""))
		{
		if(!specific.containsValue(consigneeInstruction.getConsignmentInstructions()) && !specific.containsKey(consigneeInstruction.getConsignmentInstructions()))
		{specific.put(consigneeInstruction.getConsignmentInstructions(), consigneeInstruction.getConsignmentInstructions());
		}
		}
		return specific;
	}

	/**
	 * @return the consigneeInstruction
	 */
	public ConsigneeInstruction getConsigneeInstruction() {
		return consigneeInstruction;
	}

	/**
	 * @param consigneeInstruction the consigneeInstruction to set
	 */
	public void setConsigneeInstruction(ConsigneeInstruction consigneeInstruction) {
		this.consigneeInstruction = consigneeInstruction;
	}

	/**
	 * @param refMasterManager the refMasterManager to set
	 */
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	/**
	 * @return the serviceOrder
	 */
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	/**
	 * @param serviceOrder the serviceOrder to set
	 */
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	/**
	 * @param serviceOrderManager the serviceOrderManager to set
	 */
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	/**
	 * @return the sid
	 */
	public Long getSid() {
		return sid;
	}

	/**
	 * @param sid the sid to set
	 */
	public void setSid(Long sid) {
		this.sid = sid;
	}

	/**
	 * @return the consigneeInstructionList
	 */
	public List getConsigneeInstructionList() {
		return consigneeInstructionList;
	}

	/**
	 * @param consigneeInstructionList the consigneeInstructionList to set
	 */
	public void setConsigneeInstructionList(List consigneeInstructionList) {
		this.consigneeInstructionList = consigneeInstructionList;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the consigneeInstructionCodeList
	 */
	public List getConsigneeInstructionCodeList() {
		return consigneeInstructionCodeList;
	}

	/**
	 * @param consigneeInstructionCodeList the consigneeInstructionCodeList to set
	 */
	public void setConsigneeInstructionCodeList(List consigneeInstructionCodeList) {
		this.consigneeInstructionCodeList = consigneeInstructionCodeList;
	}

	/**
	 * @return the consigneeInstructionCodeValue
	 */
	public String getConsigneeInstructionCodeValue() {
		return consigneeInstructionCodeValue;
	}

	/**
	 * @param consigneeInstructionCodeValue the consigneeInstructionCodeValue to set
	 */
	public void setConsigneeInstructionCodeValue(
			String consigneeInstructionCodeValue) {
		this.consigneeInstructionCodeValue = consigneeInstructionCodeValue;
	}

	/**
	 * @return the shipNum
	 */
	public String getShipNum() {
		return shipNum;
	}

	/**
	 * @param shipNum the shipNum to set
	 */
	public void setShipNum(String shipNum) {
		this.shipNum = shipNum;
	}

	/**
	 * @return the consigneeInstructionAddList
	 */
	public List getConsigneeInstructionAddList() {
		return consigneeInstructionAddList;
	}

	/**
	 * @param consigneeInstructionAddList the consigneeInstructionAddList to set
	 */
	public void setConsigneeInstructionAddList(List consigneeInstructionAddList) {
		this.consigneeInstructionAddList = consigneeInstructionAddList;
	}

	/**
	 * @return the consigneeInstructionAddValue
	 */
	public String getConsigneeInstructionAddValue() {
		return consigneeInstructionAddValue;
	}

	/**
	 * @param consigneeInstructionAddValue the consigneeInstructionAddValue to set
	 */
	public void setConsigneeInstructionAddValue(String consigneeInstructionAddValue) {
		this.consigneeInstructionAddValue = consigneeInstructionAddValue;
	}

	/**
	 * @return the daCode
	 */
	public String getDaCode() {
		return DaCode;
	}

	/**
	 * @param daCode the daCode to set
	 */
	public void setDaCode(String daCode) {
		DaCode = daCode;
	}

	/**
	 * @return the consigneeInstructionAddJobList
	 */
	public List getConsigneeInstructionAddJobList() {
		return consigneeInstructionAddJobList;
	}

	/**
	 * @param consigneeInstructionAddJobList the consigneeInstructionAddJobList to set
	 */
	public void setConsigneeInstructionAddJobList(
			List consigneeInstructionAddJobList) {
		this.consigneeInstructionAddJobList = consigneeInstructionAddJobList;
	}

	/**
	 * @return the customerFile
	 */
	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	/**
	 * @param customerFile the customerFile to set
	 */
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	/**
	 * @return the miscellaneous
	 */
	public Miscellaneous getMiscellaneous() {
		return( miscellaneous !=null )?miscellaneous :miscellaneousManager.get(soId);
	}

	/**
	 * @param miscellaneous the miscellaneous to set
	 */
	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	/**
	 * @return the trackingStatus
	 */
	public TrackingStatus getTrackingStatus() {
		return( trackingStatus!=null )?trackingStatus:trackingStatusManager.get(soId);
	}

	/**
	 * @param trackingStatus the trackingStatus to set
	 */
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	/**
	 * @param miscellaneousManager the miscellaneousManager to set
	 */
	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	/**
	 * @param trackingStatusManager the trackingStatusManager to set
	 */
	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}
	public Long getSoId() {
		return soId;
	}
	public void setSoId(Long soId) {
		this.soId = soId;
	}
	/**
	 * @return the consigneeShipmentLocationList
	 */
	public List getConsigneeShipmentLocationList() {
		return consigneeShipmentLocationList;
	}
	/**
	 * @param consigneeShipmentLocationList the consigneeShipmentLocationList to set
	 */
	public void setConsigneeShipmentLocationList(List consigneeShipmentLocationList) {
		this.consigneeShipmentLocationList = consigneeShipmentLocationList;
	}
	/**
	 * @return the consigneeInstructionCode
	 */
	public String getConsigneeInstructionCode() {
		return consigneeInstructionCode;
	}
	/**
	 * @param consigneeInstructionCode the consigneeInstructionCode to set
	 */
	public void setConsigneeInstructionCode(String consigneeInstructionCode) {
		this.consigneeInstructionCode = consigneeInstructionCode;
	}
	/**
	 * @return the sACode
	 */
	public String getSACode() {
		return sACode;
	}
	/**
	 * @param code the sACode to set
	 */
	public void setSACode(String code) {
		sACode = code;
	}
	/**
	 * @return the osACode
	 */
	public String getOsACode() {
		return osACode;
	}
	/**
	 * @param osACode the osACode to set
	 */
	public void setOsACode(String osACode) {
		this.osACode = osACode;
	}
	public String getCountShip() {
		return countShip;
	}
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}
	public String getMinShip() {
		return minShip;
	}
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}
	public String getShipSize() {
		return shipSize;
	}
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}
	public CustomerFileManager getCustomerFileManager() {
		return customerFileManager;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public List getCustomerSO() {
		return customerSO;
	}
	public void setCustomerSO(List customerSO) {
		this.customerSO = customerSO;
	}
	public String getCountBondedGoods() {
		return countBondedGoods;
	}
	public void setCountBondedGoods(String countBondedGoods) {
		this.countBondedGoods = countBondedGoods;
	}
	public void setCustomManager(CustomManager customManager) {
		this.customManager = customManager;
	}
	public List getConsigneeInstructionNCJobList() {
		return consigneeInstructionNCJobList;
	}
	public void setConsigneeInstructionNCJobList(List consigneeInstructionNCJobList) {
		this.consigneeInstructionNCJobList = consigneeInstructionNCJobList;
	}
	public List getConsigneeShiperList() {
		return consigneeShiperList;
	}
	public void setConsigneeShiperList(List consigneeShiperList) {
		this.consigneeShiperList = consigneeShiperList;
	}
	public Billing getBilling() {
		return billing;
	}
	public void setBilling(Billing billing) {
		this.billing = billing;
	}
	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}
	public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
		this.toDoRuleManager = toDoRuleManager;
	}
	
	public void setCustomerFileAction(CustomerFileAction customerFileAction) {
		this.customerFileAction = customerFileAction;
	}
	public void setIntegrationLogInfoManager(
			IntegrationLogInfoManager integrationLogInfoManager) {
		this.integrationLogInfoManager = integrationLogInfoManager;
	}
	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}
	public Company getCompany() {
		return company;
	}
	public CompanyManager getCompanyManager() {
		return companyManager;
	}
	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public Boolean getSurveyTab() {
		return surveyTab;
	}
	public void setSurveyTab(Boolean surveyTab) {
		this.surveyTab = surveyTab;
	}	
	public String getHitFlag() {
		return hitFlag;
	}
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	public String getForwardingAjaxUrl() {
		return forwardingAjaxUrl;
	}
	public void setForwardingAjaxUrl(String forwardingAjaxUrl) {
		this.forwardingAjaxUrl = forwardingAjaxUrl;
	}
	public String getForwardingAjaxVal() {
		return forwardingAjaxVal;
	}
	public void setForwardingAjaxVal(String forwardingAjaxVal) {
		this.forwardingAjaxVal = forwardingAjaxVal;
	}
	public String getMsgClicked() {
		return msgClicked;
	}
	public void setMsgClicked(String msgClicked) {
		this.msgClicked = msgClicked;
	}
	public Map<String, String> getSpecific_isactive() {
		return specific_isactive;
	}
	public void setSpecific_isactive(Map<String, String> specific_isactive) {
		this.specific_isactive = specific_isactive;
	}

	public String getBrokerCode() {
		return brokerCode;
	}
	public void setBrokerCode(String brokerCode) {
		this.brokerCode = brokerCode;
	}
	public String getOiJobList() {
		return oiJobList;
	}
	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}
	
}
