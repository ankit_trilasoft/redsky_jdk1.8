package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.model.ClaimLossTicket;
import com.trilasoft.app.model.Loss;

import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.service.ClaimLossTicketManager;
import com.trilasoft.app.service.LossManager;
import com.trilasoft.app.service.RefMasterManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ClaimLossTicketAction extends BaseAction {
    private ClaimLossTicketManager claimLossTicketManager;
    private Set claimLossTickets;
    private ClaimLossTicket claimLossTicket;
    private Long id;
    private String reqWT;
	private String requestedCLT;
    private String sessionCorpID;
	private RefMasterManager refMasterManager;
	private List refMasters;
	private Loss loss;
	private LossManager lossManager;
	private List ls;
	private static Map<String, String> yesno;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(ClaimLossTicketAction.class);

	public ClaimLossTicketAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
   
    public void setClaimLossTicketManager(
			ClaimLossTicketManager claimLossTicketManager) {
		this.claimLossTicketManager = claimLossTicketManager;
	}
	public void setLossManager(LossManager lossManager) {
		this.lossManager = lossManager;
	}
	public Set getClaimLossTickets() {
        return claimLossTickets;
    }
    public void setRefMasterManager(RefMasterManager refMasterManager) {
    	this.refMasterManager = refMasterManager;
    	}
    	public List getRefMasters(){
    	return refMasters;
    	}
    @SuppressWarnings("unchecked")
	public String list() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList(sessionCorpID);
    	claimLossTickets = new HashSet(claimLossTicketManager.getAll());
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public ClaimLossTicket getClaimLossTicket() {
        return claimLossTicket;
    }
    public void setClaimLossTicket(ClaimLossTicket claimLossTicket) {
        this.claimLossTicket = claimLossTicket;
    }
    public String getRequestedCLT() {
		return requestedCLT;
	}
	public void setRequestedCLT(String requestedCLT) {
		this.requestedCLT = requestedCLT;
	}
	public String getReqWT() {
		return reqWT;
	}
	public void setReqWT(String reqWT) {
		this.reqWT = reqWT;
	}
    public String getSessionCorpID() {
		return sessionCorpID;
	}

    public String delete() { 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	claimLossTicketManager.remove(id);   
        ///saveMessage(getText("loss.deleted")); 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    } 
    
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	
    public String edit() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList(sessionCorpID);
    	if (id != null) {
            claimLossTicket = claimLossTicketManager.get(id);
            claimLossTicket.setUpdatedOn(new Date());
            claimLossTicket.setCorpID(sessionCorpID);
            claimLossTicket.setCreatedBy(getRequest().getRemoteUser());
     		claimLossTicket.setUpdatedBy(getRequest().getRemoteUser());
     		claimLossTicket.setId(id);
         } else {
            claimLossTicket = new ClaimLossTicket();
           
            claimLossTicket.setCreatedOn(new Date());
            claimLossTicket.setUpdatedOn(new Date());
            claimLossTicket.setCreatedBy(getRequest().getRemoteUser());
		    claimLossTicket.setUpdatedBy(getRequest().getRemoteUser());
		    claimLossTicket.setCorpID(sessionCorpID);
         }
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
    }
    public String save() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	 boolean isNew = (claimLossTicket.getId() == null);
         if(isNew){
        	 claimLossTicket.setCreatedOn(new Date());
         }
      	 claimLossTicket.setUpdatedOn(new Date());
         claimLossTicket.setCorpID(sessionCorpID);
         claimLossTicket = claimLossTicketManager.save(claimLossTicket);
         String key = (isNew) ? "claimLossTicket.added" : "claimLossTicket.updated";
         saveMessage(getText(key));
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        if (!isNew) {
        	return INPUT;
        } else {
            return SUCCESS;
        }
    }
    public String getComboList(String corpId){
    	yesno = refMasterManager.findByParameter(corpId, "YESNO");
    	return SUCCESS;
    }
    public  Map<String, String> getYesno() {
		return yesno;
	}
	public Loss getLoss() {
		return loss;
	}
	public void setLoss(Loss loss) {
		this.loss = loss;
	}
	public void setClaimLossTickets(Set claimLossTickets) {
		this.claimLossTickets = claimLossTickets;
	}
	public void setRefMasters(List refMasters) {
		this.refMasters = refMasters;
	}
	public Long getId() {
		return id;
	}
	public List getLs() {
		return ls;
	}
	public void setLs(List ls) {
		this.ls = ls;
	}
	
}
