<%@ include file="/common/taglibs.jsp"%> 
<head> 
<title><fmt:message key="roleBasedPermissionForm.title"/></title> 
<meta name="heading" content="<fmt:message key='roleBasedPermissionForm.heading'/>"/>
<script type="text/javascript">

function autoSave(){
 if ('${autoSavePrompt}' == 'No'){
 
 if(document.forms['roleBasedPermissionForm'].elements['gotoPageString'].value == 'gototab.rolePermissionList'){
		noSaveAction = 'roleBasedPermissionList.html';
	}
	processAutoSave(document.forms['roleBasedPermissionForm'], 'saveRoleBasedPermissionForm!saveOnTabChange.html', noSaveAction);
 
 }else
 {
 	if(document.forms['roleBasedPermissionForm'].elements['gotoPageString'].value == 'gototab.rolePermissionList'){
		noSaveAction = 'roleBasedPermissionList.html';
	}
	processAutoSave(document.forms['roleBasedPermissionForm'], 'saveRoleBasedPermissionForm!saveOnTabChange.html', noSaveAction);
 }

}

</script>
</head>

 <s:form cssClass="form_magn" id="roleBasedPermissionForm" name="roleBasedPermissionForm" action="saveRoleBasedPermissionForm" method="post" validate="true">
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
 <c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.rolePermissionList' }">
    <c:redirect url="/roleBasedPermissionList.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
 
 <div id="Layer1" style="width:100%" onkeydown="changeStatus();"> 

		<div id="newmnav" class="nav_tabs"><!-- sandeep -->
		  <ul>
		  	<li><a onclick="setReturnString('gototab.rolePermissionList');return autoSave();"><span>Role Permission List</span></a></li>
		  	<!--<li><a href="roleBasedPermissionList.html"><span>Role Permission List</span></a></li>-->		  	
		  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Role Permission Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  	<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${roleBasedComponentPermission.id}&tableName=rolebased_comp_permission&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		  	
		  </ul>
		</div><div class="spn" style="!line-height:5px;">&nbsp;</div>
	
<table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%;margin:0px;">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
 <div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
  	<table width="100%" border="0" cellpadding="5" cellspacing="0" class="detailTabLabel">
		  <tbody>  	
		  	 <tr>
		  		<td width="13%" height="3" align="left"></td>
		  	</tr>
		  	<tr>
		  		<td align="right"><fmt:message key="companyPermission.componentId"/><font color="red" size="2">*</font></td>
	  		  <td width="47%" align="left"><s:textfield name="roleBasedComponentPermission.componentId"  maxlength="100" cssStyle="width:328px;!width:300px;" cssClass="input-text" readonly="false"/></td>
		  		<td width="38%" align="right"></td>
	  		</tr>
		  	<tr>
		  	  <td align="right"><fmt:message key="companyPermission.mask"/><font color="red" size="2">*</font></td>
		  	  <td align="left"><s:select cssClass="list-menu"   name="roleBasedComponentPermission.mask" list="%{roleAccess}" headerKey="" headerValue="" cssStyle="width:332px" onchange="changeStatus()"/></td>
		  	  <td align="right">&nbsp;</td>
	  	    </tr>
		  	<tr>
		  	  <td align="right">Role<font color="red" size="2">*</font></td>
		  	  <td align="left"><s:select cssClass="list-menu"   name="roleBasedComponentPermission.role" list="%{role}" headerKey="" headerValue="" cssStyle="width:332px" onchange="changeStatus()"/></td>
		  	  <td align="right">&nbsp;</td>
	  	    </tr>
	  	    <tr>
		  		<td align="right" ><fmt:message key="companyPermission.description"/></td><td colspan="2" align="left" colspan="3"><s:textarea cssClass="textarea" name="roleBasedComponentPermission.description"  rows="4" cols="57" readonly="false" cssStyle="width:328px;"/></td>
		  	</tr>
		  	<tr>
		  		<td align="right" >URLs</td><td colspan="2" align="left" colspan="3"><s:textarea cssClass="textarea" name="roleBasedComponentPermission.url"  rows="4" cols="57" readonly="false" cssStyle="width:328px;"/></td>
		  	</tr>
		  	<tr><td height="15px"></td></tr>
  </tbody>
</table>
	 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</td>
</tr>
</tbody>
</table>

 		  <table class="detailTabLabel" border="0" style="width:750px">
				<tbody>					
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							<s:text id="roleBasedComponentPermissionFormattedValue" name="${FormDateValue}"><s:param name="value" value="roleBasedComponentPermission.createdOn" /></s:text>
							<td valign="top"><s:hidden name="roleBasedComponentPermission.createdOn" value="%{roleBasedComponentPermissionFormattedValue}" /></td>
							<td style="width:130px"><fmt:formatDate value="${roleBasedComponentPermission.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty roleBasedComponentPermission.id}">
								<s:hidden name="roleBasedComponentPermission.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{roleBasedComponentPermission.createdBy}"/></td>
							</c:if>
							<c:if test="${empty roleBasedComponentPermission.id}">
								<s:hidden name="roleBasedComponentPermission.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:85px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<s:text id="roleBasedComponentPermissionupdatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="roleBasedComponentPermission.updatedOn" /></s:text>
							<td valign="top"><s:hidden name="roleBasedComponentPermission.updatedOn" value="%{roleBasedComponentPermissionupdatedOnFormattedValue}" /></td>
							<td style="width:130px"><fmt:formatDate value="${roleBasedComponentPermission.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty roleBasedComponentPermission.id}">
								<s:hidden name="roleBasedComponentPermission.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{roleBasedComponentPermission.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty roleBasedComponentPermission.id}">
								<s:hidden name="roleBasedComponentPermission.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:85px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
						</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table>
		  
		  <table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save"/>  
        		</td>
       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
        		 		
        		<td align="right">
        		<c:if test="${not empty roleBasedComponentPermission.id}">
		       <input type="button" class="cssbutton1" value="Add New Record" style="width:120px;" onclick="location.href='<c:url value="/roleBasedPermissionForm.html"/>'" />   
	            </c:if>
	            </td>
       	  	</tr>		  	
		  </tbody>
		  </table>
</div>
<s:hidden name="roleBasedComponentPermission.id"/>
</s:form>