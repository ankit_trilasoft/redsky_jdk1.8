package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.EntitlementDao;
import com.trilasoft.app.model.Entitlement;
import com.trilasoft.app.service.EntitlementManager;

public class EntitlementManagerImpl extends GenericManagerImpl<Entitlement, Long> implements EntitlementManager {   
	
	EntitlementDao entitlementDao;   
	  
	public EntitlementManagerImpl(EntitlementDao entitlementDao) {   
        super(entitlementDao);   
        this.entitlementDao = entitlementDao;   
    }

	public Map<String, String> findEntitlementAll() {
		return entitlementDao.findEntitlementAll();
	}
	
	 public EntitlementDao getEntitlementDao() {
			return entitlementDao;
		}

	public void setEntitlementDao(EntitlementDao entitlementDao) {
			this.entitlementDao = entitlementDao;
		}

	public List findEntitlementByPartnerId(String corpId,Long partnerId) {
		return entitlementDao.findEntitlementByPartnerId(corpId,partnerId);
	}

	public Map<String, String> findEntitlementByParentAgent(String corpId,String parentAgent, Boolean networkRecord, String contractType) {
		return entitlementDao.findEntitlementByParentAgent(corpId,parentAgent, networkRecord, contractType);
	}

	public List checkOptionDuplicate(String PartnerCode, String corpId,String Option) {
		return entitlementDao.checkOptionDuplicate(PartnerCode, corpId,Option);
	}
	public List getEntitlementReference(String partnerCode, String corpID,Long parentId){
		return entitlementDao.getEntitlementReference(partnerCode, corpID, parentId);
	}

	public List findLinkEntitlementByPartner(long parseLong) {
		return entitlementDao.findLinkEntitlementByPartner(parseLong);
	}
}
