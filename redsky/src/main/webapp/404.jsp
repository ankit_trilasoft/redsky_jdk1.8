<%@ include file="/common/taglibs.jsp"%>

<page:applyDecorator name="default">

<head>
    <title><fmt:message key="404.title"/></title>
    <meta name="heading" content="<fmt:message key='404.title'/>"/>   
    
 <style>
#errorDiv {background:url(/redsky/images/404-bg-opt.png) top left no-repeat; height:248px; width:703px; position:relative; margin:0px auto;} 
#headMessage {
	font-family:Arial, Helvetica, sans-serif;font-size:30px;font-weight:normal;	color:#1C53CB;line-height:1;
	position:absolute;	top:82px;	text-align:center;	text-shadow:2px 4px 4px #969696;left:170px;
}

#subheadMessage {
	font-family:Arial, Helvetica, sans-serif;font-size:18px;font-weight:normal;	line-height:1.2;
	color:#3B3B3B;	position:absolute;	top:145px;	left:142px;	text-align:center;
}
#subheadMessage a{	
	color:#1C53CB;	text-decoration:underline;	
}
</style>
</head>
<div id="errorDiv">
<div id="headMessage">
Error 404. Page not Found.
</div>
<div id="subheadMessage">
The system was not able to find the requested page. <br/>
Please contact the helpdesk/support for troubleshooting.<br/>
<a href="mailto:info@trilasoft.com">info@trilasoft.com</a></div>
</div>

</page:applyDecorator>