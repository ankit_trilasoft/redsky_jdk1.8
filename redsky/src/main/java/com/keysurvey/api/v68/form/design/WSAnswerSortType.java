/**
 * WSAnswerSortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class WSAnswerSortType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected WSAnswerSortType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _NOSORT = "NOSORT";
    public static final java.lang.String _ROTARING = "ROTARING";
    public static final java.lang.String _ALFABETICAL_ASC = "ALFABETICAL_ASC";
    public static final java.lang.String _ALFABETICAL_DESC = "ALFABETICAL_DESC";
    public static final java.lang.String _HASHING = "HASHING";
    public static final WSAnswerSortType NOSORT = new WSAnswerSortType(_NOSORT);
    public static final WSAnswerSortType ROTARING = new WSAnswerSortType(_ROTARING);
    public static final WSAnswerSortType ALFABETICAL_ASC = new WSAnswerSortType(_ALFABETICAL_ASC);
    public static final WSAnswerSortType ALFABETICAL_DESC = new WSAnswerSortType(_ALFABETICAL_DESC);
    public static final WSAnswerSortType HASHING = new WSAnswerSortType(_HASHING);
    public java.lang.String getValue() { return _value_;}
    public static WSAnswerSortType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        WSAnswerSortType enumeration = (WSAnswerSortType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static WSAnswerSortType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSAnswerSortType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswerSortType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
