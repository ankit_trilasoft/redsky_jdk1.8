package com.trilasoft.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="inventoryarticle")
public class InventoryArticle {
	private String corpID;
	private String volume;
	private String atricle;
	private Long id;
	private String weight;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append(
				"volume", volume).append("atricle", atricle).append("id", id)
				.append("weight", weight).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof InventoryArticle))
			return false;
		InventoryArticle castOther = (InventoryArticle) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(
				volume, castOther.volume).append(atricle, castOther.atricle)
				.append(id, castOther.id).append(weight, castOther.weight)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(volume).append(
				atricle).append(id).append(weight).toHashCode();
	}
	public String getAtricle() {
		return atricle;
	}
	public void setAtricle(String atricle) {
		this.atricle = atricle;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	
}
