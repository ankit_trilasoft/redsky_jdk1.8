<%@ include file="/common/taglibs.jsp"%>
<head>
   <title>Parameter Control Form</title>
   <script language="javascript" type="text/javascript">
   
    function ContainerAutoSave(clickType){
      if(!(clickType == 'save')){
         if(document.forms['editParameterControl'].elements['gotoPageString'].value == 'gototab.tab_2'){
           location.href = 'parameterControls.html';
         }
       }
     }
    </script>
   
</head>


<s:form name="editParameterControl" id="editParameterControl" action="saveParameterControl" >
  <s:hidden name="gotoPageString" id="gotoPageString" value="" />
  <s:hidden name="parameterControl.id" value="${id}" />
  <c:set var="var" value="1" />
     <c:if test="${(ParameterControl.active=='false')}">
        <c:set var="var" value="0" />
      </c:if>

  <c:choose>

    <c:when test="${gotoPageString == 'gototab.tab_2' }">

      <c:redirect url="/parameterControls.html?id=${ParameterControl.id}"/>

    </c:when>
  </c:choose>

<div id="Layer1" style="width:85%">
  <div id="newmnav">
<ul>
  <li><a href="parameterControls.html"><span>Control List</span></a></li>
  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Control Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>  
</ul>
</div>      
<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >

 <div id="liquid-round">
   <div class="top" style="margin-top:11px;!margin-top:-13px;"><span></span></div>
   <div class="center-content">

			<table cellspacing="2" cellpadding="2" border="0" >

  			<!--<tr>
	  	   	  <td  class="listwhitetext" align="center" height="21px">CorpId</td>
	  	          <td  width="2px"><s:select name="parameterControl.corpId" list="{'SSCW','STAR'}" /></td>
	  		   				  		
  	        </tr>	-->		
  	        	<tr>
	  	   	  	<td  class="listwhitetext" align="right">Parameter</td>
	  	       	<td colspan="3"> <s:textfield name="parameterControl.parameter" maxlength="50" cssStyle="width:200px;" cssClass="input-text"/></td>	  		   				  		
	  	        </tr>	  	        
	  	        <tr>
	  	        <td class="listwhitetext" align="right" >Active</td>
	  	        <td colspan="6" >
	  	        <table style="margin:0px;padding:0px;">
	  	        <tr>
	  	        <td><s:checkbox cssStyle="margin:0px;padding:0px;" name="parameterControl.active" id="parameterControl.active" value="${var}" /> 
	  	        <td width="5"></td>     
	  	        <td class="listwhitetext" align="right" >TSFT Flag</td>
	  	        <td><s:checkbox cssStyle="margin:0px;padding:0px;" key="parameterControl.tsftFlag" fieldValue="true"/></td>
	  	        <td width="5"></td> 
	  	        <td class="listwhitetext" align="right" >Hybrid Flag</td>
	  	        <td><s:checkbox cssStyle="margin:0px;padding:0px;" key="parameterControl.hybridFlag" fieldValue="true"/></td>
	  	        </tr>
	  	        </table>
	  	        </td>
	  	        </tr>	  	        
	  	        <tr>
	  	   	  	<td  class="listwhitetext" align="right" >Field Length</td>
	  	        <td ><s:textfield name="parameterControl.fieldLength" cssStyle="width:90px;" maxlength="11" cssClass="input-text"/>
				</td>
				<td  class="listwhitetext"  >Child Parameter</td>
				<td><s:select name="parameterControl.childParameter" list="%{childParameter}" headerKey="" headerValue="" cssStyle="width:221px;" cssClass="list-menu"/>	  		   				  		
	  	        </tr>
	  	         
	  	         <tr>
	  	   	  	<td  class="listwhitetext" align="right" >Custom Type</td>
	  	        <td ><s:select name="parameterControl.customType" list="{'PAYROLL','VANLINE'}" headerKey="" headerValue="" cssStyle="width:94px;" cssClass="list-menu"/>	  	          
				</td>
				<td  class="listwhitetext" align="right" >Description</td>
	  	        <td ><s:textfield name="parameterControl.description" title="${parameterControl.description}" cssStyle="width:239px;" maxlength="255" cssClass="input-text"/>
				</td>	  		   				  		
	 			</tr>
	 			<tr>
				<td  class="listwhitetext" align="right" >Name&nbsp;of&nbsp;Field</td>
	  	        <td ><s:textfield name="parameterControl.nameonform"  cssStyle="width:111px;" maxlength="255" cssClass="input-text"/>
				</td>	  		   				  		
	 			</tr>
		 		<tr>
		 		<td  class="listwhitetext" align="right" >Comments</td>
		  	    <td colspan="3"><s:textarea rows="3" cols="75" name="parameterControl.comments" cssClass="textarea" /></td>
		 		</tr>
			</table> 
			 </div>
			<div class="bottom-header"><span></span></div> 
			</div>
						 <table>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="parameterControlCreatedOnFormattedValue" value="${parameterControl.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="parameterControl.createdOn" value="${parameterControlCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${parameterControl.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty parameterControl.id}">
								<s:hidden name="parameterControl.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{parameterControl.createdBy}"/></td>
							</c:if>
							<c:if test="${empty parameterControl.id}">
								<s:hidden name="parameterControl.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="parameterControlupdatedOnFormattedValue" value="${parameterControl.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="parameterControl.updatedOn" value="${parameterControlupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${parameterControl.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty parameterControl.id}">
								<s:hidden name="parameterControl.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{parameterControl.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty parameterControl.id}">
								<s:hidden name="parameterControl.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
						 </table>
						  <table>
						<tr>
	  	   	  		<td  class="listwhitetext" align="center" height="21px"><s:submit value="Save" method="save" cssClass="cssbutton1" type="button" /></td>
	  	          <td  width="2px"><s:reset cssClass="cssbutton1" type="button"/></td>  		   				  		
			 	</tr>
			  </table>
			 </s:form>
			</div>
			</div>
