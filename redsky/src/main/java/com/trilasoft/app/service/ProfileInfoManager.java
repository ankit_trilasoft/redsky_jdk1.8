package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ProfileInfo;

public interface ProfileInfoManager extends GenericManager<ProfileInfo, Long> {

	public List getProfileInfoListByPartnerCode(String sessionCorpID, String partnerCode);
}
