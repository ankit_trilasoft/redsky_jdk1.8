package com.trilasoft.app.webapp.tags;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;

import com.trilasoft.app.webapp.action.UserGuidesAction;

public class UserGuideVisibility extends TagSupport {

	// ~ Instance fields
	// ================================================================================================
	private String userType;
	
	private String corpId;
	
	public UserGuideVisibility(){
	}

	// ~ Methods
	// ========================================================================================================

	public int doStartTag() throws JspException {
		Set<String> userTypeList = new HashSet<String>();
		userTypeList.add("AGENT");userTypeList.add("ACCOUNT");
		
		boolean visible = true;
		if (userTypeList.contains(userType)) {
			ServletContext context = pageContext.getServletContext();
			XmlWebApplicationContext ctx = (XmlWebApplicationContext) context
					.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);

			UserGuidesAction action = (UserGuidesAction) ctx.getBean("userGuidesAction");
			
			List list = action.getUserGuidesManager().findModuleCount(corpId);
			visible = (list == null || list.isEmpty()) ? false:true;
		}
		if(!visible) return  Tag.SKIP_BODY;
		
		return Tag.EVAL_BODY_INCLUDE;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
}
