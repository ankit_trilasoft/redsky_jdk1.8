<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="haulingGrid.title"/></title>   
    <meta name="heading" content="<fmt:message key='haulingGrid.heading'/>"/>  
    
<style>

form {margin-top:-20px;!margin-top:0px;}
.tab{
border:1px solid #74B3DC;

}
span.pagelinks {
display:block;
font-size:0.9em;
margin-top:-10px;
!margin-top:0px;
text-align:right;
}
</style> 
<script language="javascript" type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Base SCAC?");
	var did = targetElement;
	if (agree){
		location.href="deleteBaseSCAC.html?baseId=${agentBase.id}&id="+did;
	}else{
		return false;
	}
}
    function confirmSubmit1(targetElement){
	var agree=confirm("Are you sure you wish to remove this Partner Hauling Grid Item?");
	var did = targetElement;
	if (agree){
		location.href="deletePartnerHaulingGrid.html?id="+did+"";
	}
	else{
		return false;
	}
}
  function clear_fields(){  		    	

	document.getElementById('lowDistance').value= "";
   	document.getElementById('highDistance').value ="";
    document.getElementById('rateMile').value = "";
    document.getElementById('rateFlat').value ="";
  document.getElementById('grid').value ="";
  document.getElementById('unit').value ="";
  document.getElementById('countryCode').value ="";
}
function onlyFloat(targetElement)
{   var i;
	var s = targetElement.value;
	var count = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if (((c < '0') || (c > '9')) && (c != '.') || (count>'1')) 
        {
        	alert("Enter valid Number");
	        document.getElementById(targetElement.id).value='';
            document.getElementById(targetElement.id).select();
	        return false;
        }
    }
    return true;
}	

function quotesNotAllowed(evt){
  var keyCode = evt.which ? evt.which : evt.keyCode;
  if(keyCode==222){
  return false;
  }
  else{
  return true;
  }
}	
</script>
</head>
<s:form id="haultingGridListForm" action="searchPartnerHaulingGridList" method="post" validate="true">	
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton1" cssStyle="width:58px;" align="top"  key="button.search"  />   
    <input type="button" class="cssbutton1" value="Clear" style="width:58px;" onclick="clear_fields();"/> 
</c:set>
<div id="otabs"><ul><li><a class="current"><span>Search</span></a></li></ul></div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 11px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
	<table class="table" style="width:99%;">
	<thead>
<tr>
<th>Low Distance</th>
<th>High Distance</th>
<th>Rate Mile</th>
<th>Rate Flat</th>
<th>Grid</th>
<th>Unit</th>
<th>Country Code</th>
</tr></thead>	
		<tbody>
		<tr>	
			<td>
			    <s:textfield name="partnerRefHaulingGrid.lowDistance" id="lowDistance" size="10" required="true" cssClass="input-text" onkeydown="onlyFloat(this);return quotesNotAllowed(event);"/>
			</td>
			<td>
			    <s:textfield name="partnerRefHaulingGrid.highDistance" id="highDistance" size="10" required="true" cssClass="input-text" onchange="onlyFloat(this)" onkeydown="return quotesNotAllowed(event);"/>
			</td>
			<td>
			    <s:textfield name="partnerRefHaulingGrid.rateMile" id="rateMile" size="10" required="true" cssClass="input-text" onchange="onlyFloat(this)" onkeydown="return quotesNotAllowed(event);"/>
			</td>
			<td>
			    <s:textfield name="partnerRefHaulingGrid.rateFlat" id="rateFlat" size="10" required="true" cssClass="input-text" onchange="onlyFloat(this)" onkeydown="return quotesNotAllowed(event);"/>
			</td>
			<td>
			   <s:select name="partnerRefHaulingGrid.grid" list="%{grid}" id="grid" headerKey="" headerValue="" cssClass="list-menu" cssStyle="width:120px"/></td>
			</td>
			<td>
			    <s:textfield name="partnerRefHaulingGrid.unit" id="unit" size="10" required="true" cssClass="input-text" onkeydown="return quotesNotAllowed(event);"/>
			</td>
			<td>
			<s:select name="partnerRefHaulingGrid.countryCode" list="%{country}" cssClass="list-menu" id="countryCode" headerKey="" headerValue="" cssStyle="width:180px"/></td>
			</tr>
			<tr>
			<td colspan="6" style="border-right:hidden;"></td>
			<td width="135px" style="text-align:right;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		
		</tr>
		</tbody>
	</table>
	</div>
	<c:out value="${searchresults}" escapeXml="false" /> 
<div class="bottom-header" style="!margin-top:44px;"><span></span></div>
</div>
</div>
	
<div id="newmnav">
	 <ul>
	 	<li id="newmnav1" style="background:#FFF "><a class="current" style="!margin-bottom:-15px;"><span>Hauling Grid List</span></a></li>
	 </ul>
</div>
<div style="width: 100%" >
<div class="spn">&nbsp;</div>
</div>

<div id="Layer1" style="width: 100%;">
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:60px; height:25px" onclick="location.href='<c:url value="/editHaulingGrid.html"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>  

<table  width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td >				
				<s:set name="partnerRefHaulingGridList" value="partnerRefHaulingGridList" scope="request"/>  
				<display:table name="partnerRefHaulingGridList" class="table" requestURI="" id="partnerRefHaulingGridLists" export="true" defaultsort="1" pagesize="10" style="width: 100%; " >   
					<display:column property="countryCode" sortable="true" href="editHaulingGrid.html" paramId="id" paramProperty="id" titleKey="partnerRefHaulingGrid.countryCode"/>
					<display:column property="grid" sortable="true" titleKey="partnerRefHaulingGrid.grid"/>
					<display:column property="lowDistance" sortable="true" headerClass="containeralign" style="text-align:right" titleKey="partnerRefHaulingGrid.lowDistance" />
					<display:column property="highDistance" sortable="true" headerClass="containeralign" style="text-align:right" titleKey="partnerRefHaulingGrid.highDistance"/>
					<display:column property="rateMile" sortable="true" headerClass="containeralign" style="text-align:right" titleKey="partnerRefHaulingGrid.rateMile"/>
					<display:column property="rateFlat" sortable="true" headerClass="containeralign" style="text-align:right" titleKey="partnerRefHaulingGrid.rateFlat"/>
					<display:column property="unit" sortable="true" titleKey="partnerRefHaulingGrid.unit"/>
					<display:column title="Remove" style="width:45px;">
				    	<a><img align="middle" onclick="confirmSubmit1(${partnerRefHaulingGridLists.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
					</display:column>
				    <display:setProperty name="paging.banner.item_name" value="Hauling Grid"/>   
				    <display:setProperty name="paging.banner.items_name" value="Hauling Grid"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Hauling Grid List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Hauling Grid List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Hauling Grid List.pdf"/>   
				</display:table>  
			</td>
		</tr>
	</tbody>
</table> 
<table> 
<c:out value="${buttons}" escapeXml="false" /> 
	<tr>
		<td style="height:70px; !height:100px"></td>
	</tr>
</table>
</div>
</s:form> 
